package atraxo.sone.extensions;

import seava.j4e.api.extensions.IExtensionFile;

public class TypeExtensionFile_Ops implements IExtensionFile {

	@Override
	public boolean isJs() throws Exception {
		return true;
	}

	@Override
	public boolean isCss() {
		return false;
	}

	@Override
	public String getFileExtension() {
		return ".js";
	}

	@Override
	public String getLocation() {
		return "atraxo/ops/ui/extjs/ops_type.js";
	}

	@Override
	public void setLocation(String location) {

	}

	@Override
	public boolean isRelativePath() {
		return true;
	}

	@Override
	public void setRelativePath(boolean relativePath) {

	}

}
