package atraxo.sone.extensions;

import java.util.ArrayList;
import java.util.List;

import seava.j4e.api.extensions.IExtensionFile;
import seava.j4e.api.extensions.IExtensionProvider;
import seava.j4e.api.extensions.IExtensions;

public class SoneFileContentProvider implements IExtensionProvider {

	@Override
	public List<IExtensionFile> getFiles(String targetName) throws Exception {
		List<IExtensionFile> extensionFileList = new ArrayList<IExtensionFile>();

		if (targetName.equals(UI_EXTJS_MAIN)) {
			extensionFileList.add(new SoneApplicationMenuExtensionFile());
			extensionFileList.add(new SoneDashboardExtensionFile());

			// -----------------------------------------------
			// Dan: 05.02.2015
			// Start: Add support for Font Awesome glyph icons
			// -----------------------------------------------

			extensionFileList.add(new FontAwesomeExtensionFile());
			extensionFileList.add(new OpenSansExtensionFile()); // Support for Open Sans
			extensionFileList.add(new FinanceSolidExtensionFile());

			// -----------------------------------------------
			// Dan: 06.02.2015
			// Start: Add support for Application menu types
			// -----------------------------------------------

			extensionFileList.add(new SoneApplicationMenuTypesExtensionFile());
		}

		else if (!targetName.matches(IExtensions.UI_EXTJS_MAIN) && !targetName.matches(IExtensions.UI_EXTJS_DASHBOARD)) {

			extensionFileList.add(new FontAwesomeExtensionFile());
			extensionFileList.add(new OpenSansExtensionFile()); // Support for Open Sans
			extensionFileList.add(new FinanceSolidExtensionFile());
			extensionFileList.add(new SoneTypesExtensionFile());
			extensionFileList.add(new TypeExtensionFile_Acc());
			extensionFileList.add(new TypeExtensionFile_Ad());
			extensionFileList.add(new TypeExtensionFile_Fmbas());
			extensionFileList.add(new TypeExtensionFile_Cmm());
			extensionFileList.add(new TypeExtensionFile_Ops());			
		}

		// ---------------------------------------------
		// End: Add support for Font Awesome glyph icons
		// ---------------------------------------------

		return extensionFileList;
	}

}
