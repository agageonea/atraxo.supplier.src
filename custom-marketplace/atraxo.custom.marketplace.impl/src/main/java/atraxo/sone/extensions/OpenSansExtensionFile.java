package atraxo.sone.extensions;

import seava.j4e.api.extensions.IExtensionFile;

public class OpenSansExtensionFile implements IExtensionFile {

	@Override
	public boolean isJs() throws Exception {
		return false;
	}

	@Override
	public boolean isCss() {
		return true;
	}

	@Override
	public String getFileExtension() {
		return ".css";
	}

	@Override
	public String getLocation() {
		// TODO Auto-generated method stub
		return "font-open-sans/css/open-sans.css";
	}

	@Override
	public void setLocation(String location) {

	}

	@Override
	public boolean isRelativePath() {
		return true;
	}

	@Override
	public void setRelativePath(boolean relativePath) {

	}

}
