Ext.override(e4e.base.HomePanel, {
	
	_westPanelId_ : Ext.id(),
	_centerPanelId_ : Ext.id(),
	_eastPanelId_ : Ext.id(),
	
	_buildGaugeChart_ : function() {
		var chart = Ext.create({
			
            xtype: 'polar',
            width:245,
            height: 125,
            animate: true,
            animation: Ext.isIE8 ? false : {
                easing: "backOut",
                duration: 500
            },
            background: '#FCFCFC',
            store: {
		        fields: ['val'],
		        data: [{
		            val: 85
		        }]
		    },
            axes: {
            	type: 'numeric',
            	majorTickSteps: 1,
                position: 'bottom'
            },
            series: {
            	colors: ["#F36148","#D7D7D7"],
                type: 'gauge',
                angleField: 'val',
                donut: 60
            },
            sprites: [{
            	type: "text",
                text: "85",
                font: "40px",
                x: 100,
                y: 100
            },{
            	type: "text",
                text: "average",
                font: "12px",
                x: 100,
                y: 113
            }]
		});
		return chart;
	},
	
	_buildColumnChart_ : function() {
		var chart = Ext.create({
			xtype: 'cartesian',
			flex: 1,
		    width: "100%",
		    //height: "100%",
			background: "#FCFCFC",
	        margin: 20,
	        interactions: "itemhighlight",
	        animation: Ext.isIE8 ? false : {
	            easing: "backOut",
	            duration: 500
	        },
		    store: {
		        fields: ["name", "g1"],
		        data: [
		            
		            {"name": "AAA", "g1": 22.96},
		            {"name": "BBB", "g1": 10.5},
		            {"name": "CCC", "g1": 20.87},
		            {"name": "DDD", "g1": 25.10},
		            {"name": "EEE", "g1": 16.87}
		        ]
		    },  
		
		    //set legend configuration
		    legend: {
		        docked: "bottom"
		    },
		
		    //define the x and y-axis configuration.
	
		    axes: [{
		        type: "category",
		        position: "bottom"
		    }, {
		        type: "numeric",
		        position: "left",
				grid: true
		    }],
		
		    //define the actual bar series.
	
	
			series: [{
	            type: "bar",
	            xField: "name",
				fill: true,
				title: ["Customer"],
	            yField: ["g1"],
				colors: ["#5D9BEE"],
	            style: {
	            	lineWidth: 1,
					fillOpacity: 0.7,
					minGapWidth: 20
	            },
	            highlight: {
	                fillStyle: "#5D9BEE",
					fillOpacity: 1,
	                radius: 0,
	                lineWidth: 1,
	                strokeStyle: "#5D9BEE"
	            },
	            tooltip: {
	                trackMouse: true,
	                style: "background: #fff",
	                showDelay: 0,
	                dismissDelay: 0,
	                hideDelay: 0,
	                renderer: function(tooltip, record, item) {
	                	tooltip.setHtml(item.field + ": " + record.get(item.field));
	                }
	            }
	        }]
		});
		return chart;
	},

	_buildLineChart_ : function() {
		var chart = Ext.create({
			xtype: 'cartesian',
			flex: 1,
		    width: "100%",
		    //height: "100%",
			background: "#FCFCFC",
	        margin: 20,
	        interactions: "itemhighlight",
	        animation: Ext.isIE8 ? false : {
	            easing: "backOut",
	            duration: 500
	        },
		    store: {
		        fields: ["name", "g1", "g2"],
		        data: [
		            {"name": "Jan.", "g1": 18.34,"g2": 0.04},
		            {"name": "Feb.", "g1": 2.67, "g2": 14.87},
		            {"name": "Mar.", "g1": 1.90, "g2": 5.72},
		            {"name": "Apr.", "g1": 21.37,"g2": 2.13},
		            {"name": "May", "g1": 2.67, "g2": 8.53},
		            {"name": "June", "g1": 18.22,"g2": 4.62},
		            {"name": "July", "g1": 28.51, "g2": 12.43},
		            {"name": "Aug.", "g1": 34.43, "g2": 4.40},
		            {"name": "Sep.", "g1": 21.65, "g2": 13.87},
		            {"name": "Oct.", "g1": 12.98, "g2": 35.44},
		            {"name": "Nov.", "g1": 22.96, "g2": 38.70},
		            {"name": "Dec.", "g1": 0.49, "g2": 51.90}
		        ]
		    },  
		
		    //set legend configuration
		    legend: {
		        docked: "bottom"
		    },
		
		    //define the x and y-axis configuration.
	
		    axes: [{
		        type: "category",
		        position: "bottom"
		    }, {
		        type: "numeric",
		        position: "left",
				grid: true
		    }],
		
		    //define the actual bar series.
	
	
			series: [{
	            type: "line",
	            xField: "name",
				fill: true,
				title: ["Previous period"],
	            yField: ["g1"],
				colors: ["#FFCE55"],
	            style: {
	                lineWidth: 2,
					fillOpacity: 0.7
	            },
	            marker: {
	                radius: 4
	            },
	            highlight: {
	                fillStyle: "#FB6E52",
					fillOpacity: 1,
	                radius: 5,
	                lineWidth: 2,
	                strokeStyle: "#FB6E52"
	            },
	            tooltip: {
	                trackMouse: true,
	                style: "background: #fff",
	                showDelay: 0,
	                dismissDelay: 0,
	                hideDelay: 0,
	                renderer: function(tooltip, record, item) {
	                	tooltip.setHtml(item.field + ": " + record.get(item.field));
	                }
	            }
	        },{
	            type: "line",
	            xField: "name",
				fill: true,
				title: ["Current period"],
	            yField: ["g2"],
				colors: ["#AC92ED"],
	            style: {
	                lineWidth: 2,
					fillOpacity: 0.7
	            },
	            marker: {
	                radius: 4
	            },
	            highlight: {
	                fillStyle: "#FB6E52",
					fillOpacity: 1,
	                radius: 5,
	                lineWidth: 2,
	                strokeStyle: "#FB6E52"
	            },
	            tooltip: {
	                trackMouse: true,
	                style: "background: #fff",
	                showDelay: 0,
	                dismissDelay: 0,
	                hideDelay: 0,
	                renderer: function(tooltip, record, item) {
	                	tooltip.setHtml(item.field + ": " + record.get(item.field));
	                }
	            }
	        }]
		});
		return chart;
	},
	
	_buildSecondLineChart_ : function() {
		var chart = Ext.create({
			xtype: 'cartesian',
			flex: 1,
		    width: "100%",
		    //height: "100%",
			background: "#FCFCFC",
	        margin: 20,
	        interactions: "itemhighlight",
	        animation: Ext.isIE8 ? false : {
	            easing: "backOut",
	            duration: 500
	        },
		    store: {
		        fields: ["name", "g1", "g2"],
		        data: [
		            {"name": "Jan.", "g1": 18.34,"g2": 0.04},
		            {"name": "Feb.", "g1": 2.67, "g2": 14.87},
		            {"name": "Mar.", "g1": 1.90, "g2": 5.72},
		            {"name": "Apr.", "g1": 21.37,"g2": 2.13},
		            {"name": "May", "g1": 22.96, "g2": 38.70},
		            {"name": "June", "g1": 0.49, "g2": 51.90},
		            {"name": "July", "g1": 20.87, "g2": 62.07},
		            {"name": "Aug.", "g1": 25.10, "g2": 78.46},
		            {"name": "Sep.", "g1": 16.87, "g2": 56.80},
		            {"name": "Oct.", "g1": 32, "g2": 8.53},
		            {"name": "Nov.", "g1": 18.22,"g2": 24.62},
		            {"name": "Dec.", "g1": 32.15,"g2": 10.5}
		           
		        ]
		    },  
		
		    //set legend configuration
		    legend: {
		        docked: "bottom"
		    },
		
		    //define the x and y-axis configuration.
	
		    axes: [{
		        type: "category",
		        position: "bottom"
		    }, {
		        type: "numeric",
		        position: "left",
				grid: true
		    }],
		
		    //define the actual bar series.
	
	
			series: [{
	            type: "line",
	            xField: "name",
				fill: true,
				title: ["Previous period"],
	            yField: ["g1"],
				colors: ["#A0D468"],
	            style: {
	                lineWidth: 2,
					fillOpacity: 0.7
	            },
	            marker: {
	                radius: 4
	            },
	            highlight: {
	                fillStyle: "#FB6E52",
					fillOpacity: 1,
	                radius: 5,
	                lineWidth: 2,
	                strokeStyle: "#FB6E52"
	            },
	            tooltip: {
	                trackMouse: true,
	                style: "background: #fff",
	                showDelay: 0,
	                dismissDelay: 0,
	                hideDelay: 0,
	                renderer: function(tooltip, record, item) {
	                	tooltip.setHtml(item.field + ": " + record.get(item.field));
	                }
	            }
	        },{
	            type: "line",
	            xField: "name",
				fill: true,
				title: ["Current period"],
	            yField: ["g2"],
				colors: ["#4FC0E8"],
	            style: {
	                lineWidth: 2,
					fillOpacity: 0.7
	            },
	            marker: {
	                radius: 4
	            },
	            highlight: {
	                fillStyle: "#FB6E52",
					fillOpacity: 1,
	                radius: 5,
	                lineWidth: 2,
	                strokeStyle: "#FB6E52"
	            },
	            tooltip: {
	                trackMouse: true,
	                style: "background: #fff",
	                showDelay: 0,
	                dismissDelay: 0,
	                hideDelay: 0,
	                renderer: function(tooltip, record, item) {
	                	tooltip.setHtml(item.field + ": " + record.get(item.field));
	                }
	            }
	        }]
		});
		return chart;
	},
	
	_buildGrid_ : function() {
		var grid = Ext.create('Ext.grid.Panel', {
			flex: 1,
			cls: "sone-child-grid",
	        store: {
		        fields: ["dueDate", "subject", "type", "status"],
		        data: [
		            {"dueDate": "10-10-2015", "subject": "New tender","type": "Task", "status": "New"},
		            {"dueDate": "15-10-2015", "subject": "Bid", "type": "Task", "status": "Awarded"},
		            {"dueDate": "17-10-2015", "subject": "New tender","type": "Task", "status": "Bidded"},
		            {"dueDate": "20-10-2015", "subject": "Bid","type": "Task", "status": "Awarded"},
		            {"dueDate": "25-10-2015", "subject": "Bid","type": "Task", "status": "Awarded"},
		            {"dueDate": "27-10-2015", "subject": "New tender","type": "Task", "status": "Bidded"}
		        ]
		    },
	        columns: [
	            {text: "Due date", width:100, dataIndex: 'dueDate'},
	            {text: "Subject", width:100, dataIndex: 'subject'},
	            {text: "Type", width:100, dataIndex: 'type'},
	            {text: "Status", width:100, dataIndex: 'status'}
	        ]
	    });
		return grid;
	},
	
	_createItems_ : function() {
		var items = [{
			region : 'west',
			id: this._westPanelId_,
			frame : false,
			margin: '8px 8px 8px 0px',
			width: 405,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'panel',
				height: 160,
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'panel',
					flex: 1,
					cls: 'sone-chart-widget',
					title: '<div style="font-weight:bold">Contracted Fuel</div><div style="text-transform:none">past 12 months</div>',
					margin: '0px 4px 0px 0px',
					layout: {
						type: 'vbox',
						align: 'center',
						pack: 'center'
					},
					items: {
						xtype: 'container',
						html: '<span style="font-size:30px; font-weight: 600">1.230.450</span><span style="font-size:12px; text-transform:uppercase; padding-left:3px">ug</span>'
					}
				},{
					xtype: 'panel',
					flex: 1,
					cls: 'sone-chart-widget',
					title: '<div style="font-weight:bold">Tenders</div>',
					margin: '0px 0px 0px 4px',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: {
						xtype: 'panel',
						flex: 1,
						bodyStyle: 'padding: 25px 10px 5px 10px; box-zizing: border-box',
						html: ['<div style="display: table; height: 50%; width: 100%"><div style="display: table-cell; vertical-align:middle; text-align:right">',
						       '<div style="font-size: 36px; line-height: 30px">22</div><div>Effective</div>',
						       '</div></div>',
						       '<div style="display: table; height: 50%; width: 100%"><div style="display: table-cell; vertical-align:middle; text-align:right">',
						       '<div style="display:table; width:100%;">',
						       '<div style="display:table-cell; width: 50%; text-align:left; vertical-align:bottom">',
						       '<div>',
						       '<span style="font-weight:600; font-size: 16px">7</span><span style="padding-left:3px">new</span>',
						       '</div>',
						       '<div>',
						       '<span style="font-weight:600; font-size: 16px">15</span><span style="padding-left:3px">bidded</span>',
						       '</div>',
						       '</div>',
						       '<div style="display:table-cell; width: 50%; text-align:right; vertical-align:bottom" id="sparkLine1">test 2</div>',
						       '</div>',
						       '</div></div>',
						].join('\n'),
						listeners: {
							afterrender: {
								scope: this,
								fn: function() {
									$("#sparkLine1").sparkline([5,7,3,4,8,9,6,4], {
									    type: 'bar',
									    barWidth: 6,
									    barSpacing: 2,
									    height: 30,
									    barColor: '#A0D468',
									    negBarColor: '#A0D468'});
								}
							}
						}
					}
				}]
			},{
				xtype: 'panel',
				flex: 1,
				margin: '8px 0px 0px 0px',
				cls: 'sone-chart-widget',
				title: '<div style="font-weight:bold">My activities</div>',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: this._buildGrid_()
			}]
		}, {
			region : 'center',
			id: this._centerPanelId_,
			frame : false,	
			margin: '8px 0px 8px 0px',
			flex: 2,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'panel',
				flex: 1,
				cls: 'sone-chart-widget',
				title: '<div style="font-weight:bold">Upcoming payments</div><div style="text-transform:none">150.000 USD</div>',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: this._buildLineChart_()
				
			},{
				xtype: 'panel',
				flex: 1,
				margin: '8px 0px 0px 0px',
				cls: 'sone-chart-widget',
				title: '<div style="font-weight:bold">Upcoming bills</div><div style="text-transform:none">90.000 USD</div>',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: this._buildSecondLineChart_()
			}]
		}, {
			region : 'east',
			id: this._eastPanelId_,
			frame : false,
			margin: '8px 0px 8px 8px',
			width: 405,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'panel',
				height: 160,
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'panel',
					flex: 1,
					cls: 'sone-chart-widget',
					//title: '<div style="font-weight:bold">Fuel quotes</div>',
					title: ['<div style="display:table; width:100%; height: 100%">',
					        '<div style="display: table-cell; width: 50%; text-transform: uppercase; font-weight: bold">Submited Bids</div>',
					        '<div style="display: table-cell; width: 50%; text-align: right">-2%<span style="padding-left:3px"><i class="fa fa-caret-down" style="color:#ED5564; font-size: 18px"></i></span></div>',
					        '</div>'
					].join('\n'),
					margin: '0px 4px 0px 0px',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: {
						xtype: 'panel',
						flex: 1,
						bodyStyle: 'padding: 25px 10px 5px 10px; box-zizing: border-box',
						html: ['<div style="display: table; height: 50%; width: 100%"><div style="display: table-cell; vertical-align:middle; text-align:right">',
						       '<div style="font-size: 36px; line-height: 30px">89</div><div>Total</div>',
						       '</div></div>',
						       '<div style="display: table; height: 50%; width: 100%"><div style="display: table-cell; vertical-align:middle; text-align:right">',
						       '<div style="display:table; width:100%;">',
						       '<div style="display:table-cell; width: 50%; text-align:left; vertical-align:bottom">',
						       '<div>',
						       '<span style="font-weight:600; font-size: 16px">25</span><span style="padding-left:3px">new</span>',
						       '</div>',
						       '<div>',
						       '<span style="font-weight:600; font-size: 16px">20</span><span style="padding-left:3px">awarded</span>',
						       '</div>',
						       '</div>',
						       '<div style="display:table-cell; width: 50%; text-align:right; vertical-align:bottom" id="sparkLine2"></div>',
						       '</div>',
						       '</div></div>',
						].join('\n'),
						listeners: {
							afterrender: {
								scope: this,
								fn: function() {
									$("#sparkLine2").sparkline([5,7,6,4,3,4,8,9], {
									    type: 'bar',
									    barWidth: 6,
									    barSpacing: 2,
									    height: 30,
									    barColor: '#A0D468',
									    negBarColor: '#A0D468'});
								}
							}
						}
					}
				},{
					xtype: 'panel',
					flex: 1,
					cls: 'sone-chart-widget',
					title: ['<div style="display:table; width:100%; height: 100%">',
					        '<div style="display: table-cell; width: 50%; text-transform: uppercase; font-weight: bold">Awarded Bids</div>',
					        '<div style="display: table-cell; width: 50%; text-align: right">7%<span style="padding-left:3px"><i class="fa fa-caret-up" style="color:#A0D468; font-size: 18px"></i></span></div>',
					        '</div>'
					].join('\n'),
					margin: '0px 0px 0px 4px',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: {
						xtype: 'panel',
						flex: 1,
						bodyStyle: 'padding: 25px 10px 5px 10px; box-zizing: border-box',
						html: ['<div style="display: table; height: 50%; width: 100%"><div style="display: table-cell; vertical-align:middle; text-align:right">',
						       '<div style="font-size: 36px; line-height: 30px">132</div><div>Total</div>',
						       '</div></div>',
						       '<div style="display: table; height: 50%; width: 100%"><div style="display: table-cell; vertical-align:middle; text-align:right">',
						       '<div style="display:table; width:100%;">',
						       '<div style="display:table-cell; width: 50%; text-align:left; vertical-align:bottom">',
						       '<div>',
						       '<span style="font-weight:600; font-size: 16px">12</span><span style="padding-left:3px">confirmed</span>',
						       '</div>',
						       '<div>',
						       '<span style="font-weight:600; font-size: 16px">120</span><span style="padding-left:3px">released</span>',
						       '</div>',
						       '</div>',
						       '<div style="display:table-cell; width: 50%; text-align:right; vertical-align:bottom" id="sparkLine3"></div>',
						       '</div>',
						       '</div></div>',
						].join('\n'),
						listeners: {
							afterrender: {
								scope: this,
								fn: function() {
									$("#sparkLine3").sparkline([8,9,6,4,5,7,3,4], {
									    type: 'bar',
									    barWidth: 6,
									    barSpacing: 2,
									    height: 30,
									    barColor: '#A0D468',
									    negBarColor: '#A0D468'});
								}
							}
						}
					}
				}]
			},{
				xtype: 'panel',
				flex: 1,
				margin: '8px 0px 0px 0px',
				cls: 'sone-chart-widget',
				title: '<div style="font-weight:bold">Tender performance score</div>',
				layout: {
					type: 'vbox',
					align: 'center',
					pack: 'center'
				},
				items: this._buildGaugeChart_()
			}]
		}];
		return items;
	}
});