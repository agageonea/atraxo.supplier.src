<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sign-in: ${productName} | ${productVersion}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="${ctxpath}/statics/resources/css/jquery.maximage.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
    <link rel="stylesheet" href="${ctxpath}/statics/resources/css/screen.css?v=1.2" type="text/css" media="screen" charset="utf-8" />
    <link rel="stylesheet" href="${ctxpath}/statics/resources/css/font-awesome.min.css" type="text/css" media="screen" charset="utf-8" />
    <link rel="stylesheet" href="${ctxpath}/statics/resources/css/sone.css" type="text/css" media="screen" charset="utf-8" />
</head>

<body onload="javascript:onDocumentReady();">
    <form name="login" action="doLogin" method="post" onsubmit="return doLogin()">
        <div class="table">
            <div class="table-cell left">
                <div class="slide-nav">
                    <div class="inner">
                        <a href="" class="arrow_left"><i class="fa fa-chevron-left fa-2x"></i></a>
                        <a href="" class="arrow_right"><i class="fa fa-chevron-right fa-2x"></i></a>
                    </div>
                </div>
                <div class="shadow"></div>

                <!-- Begin IE hack -->

                <div class="slide-wrapper">
                    <div id="slider">
                        <img src="${ctxpath}/statics/resources/slides/1.jpg" alt="">
                        <img src="${ctxpath}/statics/resources/slides/2.jpg" alt="">
                        <img src="${ctxpath}/statics/resources/slides/3.jpg" alt="">
                    </div>
                </div>

                <!-- End IE hack -->

            </div>
            <div class="table-cell right">
                <div class="logo"></div>
                <div class="welcomer">Welcome! Please sign in with your user name and password</div>

                <input type="text" name="user" placeholder="User name">
                <input type="password" name="pswd" placeholder="Password">
                <input type="text" name="client" placeholder="Client">
                <input type="hidden" id="cacheTest"></input>
                <div class="errors"></div>
                <#if error??>
                    <div class="server_errors">${error}</div>
                </#if>
                <!-- JavaScript hack to solve SONE-533 -->
                <input id="submitButton" type="button" value="Sign in" name="save" class="submit" onclick="document.forms['login'].submit()">

            </div>
        </div>
    </form>
    <script src="${ctxpath}/statics/resources/js/jquery-1.11.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="${ctxpath}/statics/resources/js/jquery.cycle.all.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="${ctxpath}/statics/resources/js/jquery.maximage.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="${ctxpath}/statics/resources/js/jquery.placeholder.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $(function() {
            $('#slider').maximage({
                cycleOptions: {
                    fx: 'none',
                    speed: 800,
                    timeout: 8000,
                    prev: '.arrow_left',
                    next: '.arrow_right'
                },
                cssBackgroundSize: true
            });
        });
        $('.errors, .server_errors').click(function() {
            $(this).css({
                display: "none"
            });
        });

        function doLogin() {
            if (checkFields()) {
                document.forms['login'].save.disabled = true;
                document.forms['login'].submit();
            }
            return false;
        }

        function checkFields() {

            var e = $('.errors');

            if (document.forms['login'].user.value == '') {

                e.css({
                    display: "block"
                });
                e.html("Error: Please enter your username!");

                document.forms['login'].user.focus();
                return false;
            }
            if (document.forms['login'].pswd.value == '') {

                e.css({
                    display: "block"
                });
                e.html("Error: Please enter your password!");

                document.forms['login'].pswd.focus();
                return false;
            }
            return true;
        }

        function onDocumentReady() {
            document.forms['login'].user.focus();
            
        }
        $('input, textarea').placeholder();
        
        // JavaScript hack to solve SONE-533
        window.addEventListener('pageshow', function (event) {
		    if (event.persisted) {
		        
		    }
		}, false);
		
		// JavaScript hack to solve SONE-533
		
		document.onkeydown=function(evt){
		
	        var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
	        var e = $('.errors');
	        
	        if(keyCode == 13)
	        {
	        
	        
	        	if ($('input[name^="client"]').val() == "" && $('input[name^="user"]').val()!="sys") {
	        	
	        		e.css({
                    display: "block"
	                });
	                e.html("Error: Please specify the client name!");
	
	                document.forms['login'].client.focus();
	                return false;
	        	}
	        	else {
	        		if ($('input[name^="user"]').val()=="sys")  {
	        			document.forms['login'].submit();
	        		}
	        		else {
	        			$('input[name^="client"]').on("change", function() {
		        			document.forms['login'].submit();
		        		});
	        		}
	        		
	        	}
	        }
	    }
		        
    </script>
</body>

</html>