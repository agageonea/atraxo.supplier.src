package liquibase.ext.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import liquibase.logging.core.AbstractLogger;

public class LiquibaseLogger extends AbstractLogger {
	private static final int PRIORITY = 5;

	private Logger logger;

	/**
	 * Takes the given logger name argument and associates it with a SLF4J logger.
	 *
	 * @param name
	 *            The name of the logger.
	 */
	@Override
	public void setName(String name) {
		this.logger = LoggerFactory.getLogger(name);
	}

	/**
	 * This method does nothing in this implementation
	 *
	 * @param logLevel
	 *            Log level
	 * @param logFile
	 *            Log file
	 */
	@Override
	public void setLogLevel(String logLevel, String logFile) {
		// Do nothing
	}

	/**
	 * Logs an severe message. Calls SLF4J {@link Logger#error(String)}.
	 *
	 * @param message
	 *            The message to log.
	 */
	@Override
	public void severe(String message) {
		if (this.logger.isErrorEnabled()) {
			this.logger.error(this.buildMessage(message));
		}
	}

	/**
	 * Logs a severe message. Calls SLF4J {@link Logger#error(String, Throwable)}.
	 *
	 * @param message
	 *            The message to log
	 * @param throwable
	 *            The exception to log.
	 */
	@Override
	public void severe(String message, Throwable throwable) {
		if (this.logger.isErrorEnabled()) {
			this.logger.error(this.buildMessage(message), throwable);
		}
	}

	/**
	 * Logs a warning message. Calls SLF4J {@link Logger#warn(String)}
	 *
	 * @param message
	 *            The message to log.
	 */
	@Override
	public void warning(String message) {
		if (this.logger.isWarnEnabled()) {
			this.logger.warn(this.buildMessage(message));
		}
	}

	/**
	 * Logs a warning message. Calls SLF4J {@link Logger#warn(String, Throwable)}.
	 *
	 * @param message
	 *            The message to log.
	 * @param throwable
	 *            The exception to log.
	 */
	@Override
	public void warning(String message, Throwable throwable) {
		if (this.logger.isWarnEnabled()) {
			this.logger.warn(this.buildMessage(message), throwable);
		}
	}

	/**
	 * Log an info message. Calls SLF4J {@link Logger#info(String)}.
	 *
	 * @param message
	 *            The message to log.
	 */
	@Override
	public void info(String message) {
		if (this.logger.isInfoEnabled()) {
			this.logger.info(this.buildMessage(message));
		}
	}

	/**
	 * Log an info message. Calls SLF4J {@link Logger#info(String, Throwable)}.
	 *
	 * @param message
	 *            The message to log.
	 * @param throwable
	 *            The exception to log.
	 */
	@Override
	public void info(String message, Throwable throwable) {
		if (this.logger.isInfoEnabled()) {
			this.logger.info(this.buildMessage(message), throwable);
		}
	}

	/**
	 * Log a debug message. Calls SLF4J {@link Logger#debug(String)}.
	 *
	 * @param message
	 *            The message to log.
	 */
	@Override
	public void debug(String message) {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug(this.buildMessage(message));
		}
	}

	/**
	 * Log a debug message. Calls SLF4J {@link Logger#debug(String, Throwable)}.
	 *
	 * @param message
	 *            The message to log.
	 * @param throwable
	 *            The exception to log.
	 */
	@Override
	public void debug(String message, Throwable throwable) {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug(this.buildMessage(message), throwable);
		}
	}

	/**
	 * Gets the logger priority for this logger. The priority is used by Liquibase to determine which logger to use. The logger with the highest
	 * priority will be used. This implementation's priority is set to 5. Remove loggers with higher priority numbers if needed.
	 *
	 * @return An integer (5)
	 */
	@Override
	public int getPriority() {
		return PRIORITY;
	}
}