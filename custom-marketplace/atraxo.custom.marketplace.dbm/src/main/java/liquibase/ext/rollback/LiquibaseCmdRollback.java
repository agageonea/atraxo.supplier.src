package liquibase.ext.rollback;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

public class LiquibaseCmdRollback {

	private final static Logger LOG = LoggerFactory.getLogger(LiquibaseCmdRollback.class);

	public static void main(String[] args) {
		if (args.length == 0) {
			String usageExample = "Application must have one of the following parameters:\n" + "rollback {version_number}\nupdate";
			System.out.println(usageExample);
			System.exit(-1);
		}
		String cmd = args[0];
		String tag = null;
		String message;
		switch (cmd) {
		case "rollback":
			if (args.length < 2) {
				System.out.println("You must speciffy a version number to rollback to");
				System.exit(-1);
			}
			tag = args[1];
			message = "Rolling back to version: " + tag;
			System.out.println(message);
			LOG.info(message);
			break;
		case "update":
			message = "Updating to latest version";
			System.out.println(message);
			LOG.info(message);
			break;
		default:
			System.out.println("Invalid command");
			System.exit(-1);

		}
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = LiquibaseCmdRollback.class.getClassLoader().getResourceAsStream("seava.properties");
			prop.load(input);
		} catch (IOException e) {
			LOG.error("Could not read properties file", e);
			LOG.error(e.getMessage());
			System.exit(-1);
		}
		LOG.info("Read configuration file successful...");
		Liquibase liquibase = null;
		try {
			String connectionString = prop.getProperty("jdbc.url") + "?" + "user=" + prop.getProperty("jdbc.username") + "&" + "password="
					+ prop.getProperty("jdbc.password");
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection conn = DriverManager.getConnection(connectionString);
			Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(conn));
			liquibase = new Liquibase("changelog/db-changelog.xml", new ClassLoaderResourceAccessor(), database);
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException | LiquibaseException e) {
			LOG.error("Could not connect to the database", e);
			LOG.error(e.getMessage());
			System.exit(-1);
		}
		LOG.info("Connection to database established...");

		switch (cmd) {
		case "rollback":
			LOG.info("Starting rollback...");
			try {
				liquibase.rollback(tag, "");
			} catch (LiquibaseException e) {
				LOG.error("Could not rollback...", e);
				LOG.error(e.getMessage());
				System.out.println("Could not rollback. See log for details");
				System.exit(-1);
			}
			LOG.info("Rollback successful. Current version: " + tag);
			System.out.println("Rollback successful. Current version: " + tag);
			break;
		case "update":
			LOG.info("Starting update...");
			try {
				liquibase.update("");
			} catch (LiquibaseException e) {
				LOG.error("Could not update...", e);
				LOG.error(e.getMessage());
				System.out.println("Could not update. See log for details");
				System.exit(-1);
			}
			LOG.info("Update successful");
			System.out.println("Update successful");
			break;
		default:

		}
	}

}
