Ext.ns("e4e");
Notification = {
	openWorkflowInstanceDialog: function(params) {
		var bundle = "atraxo.mod.fmbas";
        var frame = "atraxo.fmbas.ui.extjs.frame.Workflow_Ui";
        
        var m = getApplication().menu;
        var p = Ext.getCmp(m._cmpNotificationSideBarId_);
        m._hideNotificationSideBar_(p); 
        
        getApplication().showFrame(frame,{
            url:Main.buildUiPath(bundle, frame, false),
            callback: function () {
                this._setInstanceActive_(params);
            }
        });
	},
	downloadZip: function(params) {
		window.open(params);
	},
	cancelFuelTicket: function(params) {
		
		var bundle = "atraxo.mod.ops";
        var frame = "atraxo.ops.ui.extjs.frame.FuelTicket_Ui";
        
        var m = getApplication().menu;
        var p = Ext.getCmp(m._cmpNotificationSideBarId_);
        m._hideNotificationSideBar_(p); 
        
        getApplication().showFrame(frame,{
            url:Main.buildUiPath(bundle, frame, false),
            callback: function () {
                this._setupRevokationFromNotification_(params);
            }
        });
	}
}