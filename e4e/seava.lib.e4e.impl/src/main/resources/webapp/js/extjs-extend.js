/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

/* ==================== general javascript overrides ======================== */

// string.endsWith 

/* ==================== Extjs overrides ======================== */

Ext.Ajax.timeout = 1000 * 60 * 60;

Ext.Component.INVALID_ID_CHARS_Re = /[\$\.,\s]/g;

/** 
 * Disable autoloading. 
 */
Ext.Loader.setConfig({
	enabled : false
});

/**
 * Change the clientRecordId. The default value `clientId` is in conflict with
 * our clientId field which represents the tenant-id.
 */
Ext.override(Ext.data.Model, {
	clientIdProperty : "__clientRecordId__"
});

Ext.JSON.encodeDate = function(d) {
	return Ext.Date.format(d, '"' + Main.MODEL_DATE_FORMAT + '"');
};

// in ExtJs 6.6.0 the response object is changed
// until now the ajax response contain in the responseText the data from the server in a string format
// from v 6.6.0 the response can contain different type of data
// part of implementation from Ext.data.request.Ajax
//		if (xhr.responseType === 'blob') {
//		    response.responseBlob = xhr.response;
//		} else if (xhr.responseType === 'json') {
//		    response.responseJson = xhr.response;
//		} else if (xhr.responseType === 'document') {
//		    response.responseXML = xhr.response;
//		} else {
//		    // an error is thrown when trying to access responseText or responseXML
//		    // on an xhr object with responseType with any value but "text" or "",
//		    // so only attempt to set these properties in the response if we're not
//		    // dealing with other specified response types
//		    response.responseText = xhr.responseText;
//		    response.responseXML = xhr.responseXML;
//		}
// getResponseDataInJSON function try return the data in JSON format if exists
// in other case try to decode other data and return the result
Ext.getResponseDataInJSON = function(response) {
	if( typeof response !== 'undefined' && response ){
	    if (typeof response.responseJson === 'object' && !Ext.isEmpty(response.responseJson)) {
	        return response.responseJson;
	    }
		var d = null;
	    try {
	        if (response.hasOwnProperty('responseText')) {
	        	d = response.responseText;
	        } else
	        if (response.hasOwnProperty('responseXML')) {
	        	d = response.responseXML;
	        } else
	        if (response.hasOwnProperty('responseBlob')) {
	        	d = response.responseBlob;
	        }
	    	if( d ){
	    		return Ext.decode(d);
	    	}
	    } catch (ex) {
	        Ext.Logger.warn('Unable to parse the JSON returned by the server. ' + ex.message);
	    }
	}
    return {};
};
// try to read error message - from object and from text (if not found in object)
// this function is based on the old error parsing and display logic
Ext.getResponseErrorText = function(response){
	if( typeof response !== 'undefined' && response ){
		if (typeof response.responseJson === 'object' && !Ext.isEmpty(response.responseJson) && response.responseJson.msg){
			return response.responseJson.msg;
		}
		var d = null;
	    try {
	        if (response.hasOwnProperty('responseText')) {
	        	d = response.responseText;
	        } else
	        if (response.hasOwnProperty('responseXML')) {
	        	d = response.responseXML;
	        } else
	        if (response.hasOwnProperty('responseBlob')) {
	        	d = response.responseBlob;
	        }
	    	if( d ){
	    		var o = Ext.decode(d);
	    		if( o.msg ){
	    			return o.msg;
	    		}
	    	}
	    } catch (e1) {
	    	try {
	    		if( d ){
		    		var errArr = d.split("\n||\n");
		    		if( errArr.length > 2 ){
		    			return errArr[2];
		    		}
		    		return d;
	    		}
	    	} catch (e2) {
	    	}
	    }
	}
	return "";
};
// return string instead of object
Ext.getResponseDataInText = function(response) {
	if( typeof response !== 'undefined' && response ){
	    if (response.hasOwnProperty('responseText')) {
	    	return response.responseText;
	    }
	    if (response.hasOwnProperty('responseXML')) {
	    	return ""+response.responseXML;
	    }
	    if (response.hasOwnProperty('responseBlob')) {
	    	return ""+response.responseBlob;
	    }
	    if (typeof response.responseJson === 'object' && !Ext.isEmpty(response.responseJson)) {
	        return Ext.JSON.encode(response.responseJson);
	    }
	}
    return "";
};
		
// USE_NATIVE_JSON is enabled in 6.6.0, but we can not use native JSON encoder
// because for DATE objects we have our format which is used at encode/decode 
Ext.USE_NATIVE_JSON = false;

Ext.override(Ext.data.Model, {

	constructor : function(data, session) {

		var me = this, cls = me.self, identifier = cls.identifier, Model = Ext.data.Model, modelIdentifier = Model.identifier, idProperty = me.idField.name, array, id, initializeFn, internalId, len, i, fields;

		me.data = data || (data = {});
		me.session = session || null;
		me.internalId = internalId = modelIdentifier.generate();

		// <debug> 
		if (session && !session.isSession) {
			Ext.Error.raise('Bad Model constructor argument 2 - "session" is not a Session');
		}
		// </debug> 

		if ((array = data) instanceof Array) {
			me.data = data = {};
			fields = me.getFields();
			len = Math.min(fields.length, array.length);
			for (i = 0; i < len; ++i) {
				data[fields[i].name] = array[i];
			}
		}

		if (!(initializeFn = cls.initializeFn)) {
			cls.initializeFn = initializeFn = Model.makeInitializeFn(cls);
		}
		if (!initializeFn.$nullFn) {
			cls.initializeFn(me);
		}

		// MyComment - fucking bullshit this all id shit ! 

		// Must do this after running the initializeFn due to 
		// converters on idField 
		if (!(me.id = id = data[idProperty]) && id !== 0) {
			if (session) {
				identifier = session.getIdentifier(cls);
				id = identifier.generate();
			} else if (modelIdentifier === identifier) {
				id = internalId;
			} else {
				id = identifier.generate();
			}

			// data[idProperty] = 
			me.id = id;
			me.phantom = true;
		}

		if (session) {
			session.add(me);
		}

		if (me.init && Ext.isFunction(me.init)) {
			me.init();
		}
	}

});

Ext.override(Ext.data.operation.Operation, {

	doProcess : function(resultSet) { // additional parameters: request, response
		var me = this, 
			commitSetOptions = me._commitSetOptions, 
			clientRecords = me.getRecords(), 
			clientLen = clientRecords.length, 
			clientIdProperty = clientRecords[0].clientIdProperty, 
			serverRecords = resultSet.getRecords(), 
			serverLen = serverRecords ? serverRecords.length : 0; 
		var clientMap, serverRecord, clientRecord, i;
		
		if (serverLen && clientIdProperty) {
			clientMap = Ext.Array.toValueMap(clientRecords, 'id');
			for (i = 0; i < serverLen; ++i) {
				serverRecord = serverRecords[i];
				var _key = serverRecord[clientIdProperty]
				if (!_key) {
					_key = serverRecord["id"]
				}

				clientRecord = clientMap[_key];
				if (clientRecord) {
					delete clientMap[clientRecord.id];
					delete serverRecord[clientIdProperty];
					// Tibi: E5
					var oldId = clientRecord.id; // keep the old id, because something is not set correct in data-collection
					clientRecord.set(serverRecord, commitSetOptions);
					clientRecord.id = oldId;
				} else {
					Ext.log.warn('Ignoring server record: ' + Ext.encode(serverRecord));
				}
			}
			for (i in clientMap) {
				clientMap[i].commit();
			}
		} else {
			for (i = 0; i < clientLen; ++i) {
				clientRecord = clientRecords[i];
				if (serverLen === 0 || !(serverRecord = serverRecords[i])) {
					clientRecord.commit();
				} else {
					clientRecord.set(serverRecord, commitSetOptions);
				}
			}
		}
	}
});


Ext.override(Ext.data.writer.Writer, {

	// Tibi: E5 & E6
	getRecordData : function(record, operation) {
		var me = this, nameProperty = me.getNameProperty(), mapping = nameProperty !== 'name', idField = record.self.idField, key = idField[nameProperty]
				|| idField.name, // setup for idField first
		value = record.id, writeAll = me.getWriteAllFields(), ret, dateFormat, phantom, options, clientIdProperty, fieldsMap, data, field;

		if (idField.serialize) {
			value = idField.serialize(value);
		}

		if (!writeAll && operation && operation.isDestroyOperation) {
			ret = {};
			ret[key] = value;
		} else {
			dateFormat = me.getDateFormat();
			phantom = record.phantom;
			options = (phantom || writeAll) ? me.getAllDataOptions() : me.getPartialDataOptions();
			// Tibi: E5	//clientIdProperty = phantom && me.getClientIdProperty();
			clientIdProperty = me.getClientIdProperty();

			fieldsMap = record.getFieldsMap();

			options.serialize = false; // we must take over this here
			data = record.getData(options);

			// If we are mapping we need to pour data into a new
			// object, otherwise we do
			// our work in-place:
			ret = mapping ? {} : data;

			if (clientIdProperty) { // if (phantom and have clientIdProperty)
				ret[clientIdProperty] = value; // must read data and write ret
				// Tibi: E5 //delete data[key];	// in case ret === data (must not send "id")  
			} else if (!me.getWriteRecordId()) {
				delete data[key];
			}

			for (key in data) {
				value = data[key];

				if (!(field = fieldsMap[key])) {
					// No defined field, so clearly no
					// nameProperty to look up for this field
					// but if we are mapping we need to copy
					// over the value. Also there is no
					// serializer to call in this case.
					if (mapping) {
						ret[key] = value;
					}
				} else {
					// Allow this Writer to take over formatting
					// date values if it has a
					// dateFormat specified. Only check isDate
					// on fields declared as dates
					// for efficiency.
					if (field.isDateField && dateFormat && Ext.isDate(value)) {
						value = Ext.Date.format(value, dateFormat);
					} else if (field.serialize) {
						value = field.serialize(value, record);
					}

					if (mapping) {
						key = field[nameProperty] || key;
					}

					ret[key] = value;
				}
			}
		}

		return ret;
	}
});


/**
 * Override page loading: - if totalCount is set check if next-page doesn't
 * overflow - check previous page isn't negative - helper filter methods
 * 
 */
Ext.override(Ext.data.Store, {

	filterAllNew : function(item) {
		return item.phantom === true;
	},

	getAllNewRecords : function() {
		return this.data.filterBy(this.filterAllNew).items;
	},

	filterUpdated : function(item) {
		return item.dirty === true && item.phantom !== true;
	},

	getModifiedRecords : function() {
		return [].concat(this.getAllNewRecords(), this.getUpdatedRecords());
	},

	rejectChanges : function() {
		this.callParent();
		this.fireEvent('changes_rejected', this);
	},

	loadData : function(data, append){
		this.fireEvent("beforeloaddata",this);
		this.callParent(arguments);
		if( Ext.isEmpty(data) ){
			this.fireEvent("loadempty",this);
		}
	},
	
	isSomethingToSync : function(){
		var me = this,
			toCreate = me.getNewRecords(),
			toUpdate = me.getUpdatedRecords(),
			toDestroy = me.getRemovedRecords(),
			needsSync = false;
		if (toCreate.length > 0 || toUpdate.length > 0 || toDestroy.length > 0){
            needsSync = true;
        }
		return needsSync;
	}
});

/** 
 * Disabled fields do not allow focus, so user cannot copy-paste values from 
 * them. This override allows to perform enable/disable as read-only based on 
 * the Main.viewConfig.DISABLE_AS_READONLY flag. All the field enable/disabe 
 * calls are routed through these dispatcher functions. 
 *  
 */
Ext.override(Ext.Img, {
	_enable_ : function() {
	},

	_disable_ : function() {
	},

	_setDisabled_ : function() {
	}
});

Ext.override(Ext.form.Label, {
	_enable_ : function() {
	},

	_disable_ : function() {
	},

	_setDisabled_ : function() {
	},
	
	initComponent : function() {
		if( this.fieldLabel ){
			this.text = this.fieldLabel;
		}
		this.callParent(arguments);
	}
});


// Tibi: E6
Ext.override(Ext.form.field.Base, {

	tooltip : null,
	
	msgTarget: 'none',

	enableRawFromatting: null, 	// set to true to enable formatting of displayed value; 
								// in this case need to implement formatValue and unFormatValue
								// see details of implementation in FpNumberField class
    _fpRawValue_: null,
	_inEditMode_: null,
	
    
	_autoLabelWidthDelta_: 10,
	_enableAutoLabelWidth_: null,
    
    initComponent: function() {
        var me = this;

    	if( !me.inputAttrTpl && me.tooltip ){
    		me.inputAttrTpl = ' data-qtip="' + (""+me.tooltip).replaceAll('"',"'") + '" ';
    	}

		if(me._enableAutoLabelWidth_===true && me.fieldLabel){
			var tm = new Ext.util.TextMetrics();
			me.labelWidth = tm.getWidth(me.fieldLabel) + me._autoLabelWidthDelta_;
			tm.destroy();
		}
    	
        me.callParent(arguments);
        me.subTplData = me.subTplData || {};
        
        me.initLabelable();
        me.initField();
        me.initDefaultName();
        // Add to protoEl before render
        if (me.readOnly) {
            me.addCls(me.readOnlyCls);
        }
        me.addCls(Ext.baseCSSPrefix + 'form-type-' + me.inputType);
    },
	
	_enable_ : function() {
		if (Main.viewConfig.DISABLE_AS_READONLY === true) {
			if (this.readOnly === true) {
				this.setReadOnly(false);
			}
		} else {
			if (this.disabled === true) {
				this.enable();
			}
		}
	},

	_disable_ : function() {
		if (Main.viewConfig.DISABLE_AS_READONLY === true) {
			if (this.readOnly === false) {
				this.setReadOnly(true);
			}
		} else {
			if (this.disabled === false) {
				this.disable();
			}
		}
	},
	
	_isDisabled_ : function() {
		var res;
		if (Main.viewConfig.DISABLE_AS_READONLY === true) {
			res = this.readOnly;
		} else {
			res = this.disabled;
		}
		return res;
	},

	_setDisabled_ : function(v) {
		if (Main.viewConfig.DISABLE_AS_READONLY === true) {
			if (this.readOnly !== v) {
				this.setReadOnly(v);
			}
		} else {
			if (this.disabled !== v) {
				this.setDisabled(v);
			}
		}
	},

	// Tibi: E6	
    publishValue: function() {
    	var me = this;
    	// SONE-1284
    	// ignore the errors to propagate value to the model: need to activate UI buttons and cancel functionality
    	// if value is not propagated to the model, buttons remains disabled
    	// even if the cancel button is enabled (by another change) the cancel action will not bring back the value
    	// in the field, because no change detected in the model
        if (me.rendered /*&& !me.getErrors().length*/) {
            me.publishState('value', me.getValue());
        }
    },

    _isDifferentValue_: function(v){
    	return (this.formatValue(this._fpRawValue_) !== v);
    },
    
    setInEditMode: function(inEdit){
    	if( this.enableRawFromatting === true && this.inputEl ){
        	var v = this.inputEl.getValue();
	    	if( inEdit ){
	    		// now the editing is started
	    		if( this.inputEl._valueIsFormatted_ === true ){
		    		if( !this._isDisabled_() && this._isDifferentValue_(v) ){
	    				this._fpRawValue_ = this.unFormatValue(v);
		    		}
	        		this.inputEl._valueIsFormatted_ = false;
	        		this.inputEl.dom.value = this._fpRawValue_;
	    		} else {
		    		if( !this._isDisabled_() ){
		    			this._fpRawValue_ = v;
		    		}
	    		}
	    	} else {
	    		// now the editing is finished
	    		if( !this._isDisabled_() ){
		    		if( this.inputEl._valueIsFormatted_ === true && this._isDifferentValue_(v) ){
	    				this._fpRawValue_ = this.unFormatValue(v);
		    		} else {
		    			this._fpRawValue_ = v;
		    		}
	    		}
    			var fv = this.formatValue(v);
	        	this.inputEl._valueIsFormatted_ = true;
	            this.inputEl.dom.value = fv;
	    	}
    	}
    	this._inEditMode_ = inEdit;
    },
    
    formatValue: function(v){
    	return v;
    },
    unFormatValue: function(v){
    	return v;
    },
    
    _valueFromInputElem_: function(){
    	if( this.enableRawFromatting !== true ){
    		return this.inputEl.getValue();
    	}
    	if( this._inEditMode_ ){
    		this._fpRawValue_ = this.inputEl.getValue();
    	}
    	return this._fpRawValue_;
    },
    _valueToInputElem_: function(v){
    	var fv = v;
    	if( !this._inEditMode_ ){
    		fv = this.formatValue(v);
    	} 
    	this.inputEl._valueIsFormatted_ = !!!this._inEditMode_;
        this.inputEl.dom.value = fv;
    },
    
    getRawValue: function() {
        var me = this,
        	v = (me.inputEl ? this._valueFromInputElem_() : Ext.valueFrom(me.rawValue, ''));
        me.rawValue = v;
        this._fpRawValue_ = v;
        return v;
    },
    
    setRawValue: function(value) {
        var me = this,
            rawValue = me.rawValue;
        if (!me.transformRawValue.$nullFn) {
            value = me.transformRawValue(value);
        }
        value = Ext.valueFrom(value, '');
        if (rawValue === undefined || rawValue !== value || me._fpRawValue_ !== value || me.valueContainsPlaceholder) {
            me.rawValue = value;
        	if( !this._inEditMode_ ){
        		this._fpRawValue_ = value;
        	}
            
            if (me.inputEl) {
                me.bindChangeEvents(false);
            	if( this.enableRawFromatting !== true ){
            		me.inputEl.dom.value = value;
            	} else {
            		this._valueToInputElem_(this._fpRawValue_);
            	}
                me.bindChangeEvents(true);
            }
            if (me.rendered && me.reference) {
                me.publishState('rawValue', value);
            }
        }
        return value;
    },

    _onFocusVal_ : null,
	onFocus: function() {
		this._onFocusVal_ = this.getValue();
		this.callParent(arguments);
	},
	onBlur: function() {
		this.callParent(arguments);
		Ext.defer(this._checkAndFireValueChange_, 50, this);
	},
	_checkAndFireValueChange_: function(){ 
		var crtVal = this.getValue();
		this._doCheckAndFireValueChange_(crtVal,this._onFocusVal_);
	},
	_doCheckAndFireValueChange_: function(newV, oldV){
		if( ( String(newV) !== String(oldV) ) && ( 
			( Ext.isEmpty(newV) && Ext.isEmpty(newV) !== Ext.isEmpty(oldV) ) ||
			( !Ext.isEmpty(newV) ) ) ) {
			this.fireEvent('fpchange', this, newV, oldV);
			this._onFocusVal_ = this.getValue();
		}
	},
	_setOnFocusVal_ : function(val) {
		this._onFocusVal_ = val;
	},
	
	_safeSetValue_ : function(val) {
		this.blur();
		return this.setValue(val);
	}
});

Ext.override(Ext.container.Container, {
	_enable_ : function(args) {
		this.enable(args);
	},

	_disable_ : function(args) {
		this.disable(args);
	},

	_setDisabled_ : function(args) {
		this.setDisabled(args);
	}
});

Ext.override(Ext.button.Button, {
	
	initComponent: function(){
		this.callParent(arguments);
		if( this.config.visible === false ){
			this.setVisible(false);
		}
	},
	
	_enable_ : function(args) {
		this.enable(args);
	},

	_disable_ : function(args) {
		this.disable(args);
	},

	_setDisabled_ : function(args) {
		this.setDisabled(args);
	},
	
	_checkMenuHandler_: function(scope,obj,evnt,keepMenuVisible){
		// evnt will be null when call is coming from a keyboard shortcut
		// default action will be click on button
		var ttype = "button";
		try{
			var trgId = evnt.getTarget().id+"";
			// logic of the handler changed, the menu will be displayed only when user click on the arrow; item: SONE-7061
			if( trgId.indexOf("btnWrap")>0 ){
				ttype = "arrow";
			} else {
				if( trgId.indexOf("InnerEl")>0 || trgId.indexOf("IconEl")>0 ){ 
					ttype = "button";
				} else {
					// calculate ttype based on click position relative to button size and position
					var click =	obj.translatePoints(evnt.pageX, evnt.pageY)
					var pos = obj.getPosition();
					var size = obj.getSize();
					if( pos[0] + size.width - size.height < click.left ){
						ttype = "arrow";
					} else {
						ttype = "button";
					}
				}
			}
		}catch(e){
			// nothing to do
		}
		if("button" === ttype){
			// execute handler only if click was on the button and not on the arrow
			if( this.menu ){
				if( this.menu.items ){
					for(var i=0;i<this.menu.items.length; i++){
						var m = this.menu.items.items[i];
						if( m._isDefaultHandler_+""==="true" && Ext.isFunction(m.handler) ){
							this.menu.hide();
							var s = scope || this;
							if(m._isAction_===true){
								s = m.config.scope;
							}
							if( !m.isDisabled() && m.isVisible()){
								m.handler.call(s,m,evnt);
							}
							return true;
						}
					}
				}
				if( keepMenuVisible !== true ){
					this.menu.hide();
				}
			}
			// if no default handler defined or no menu added, return false to execute the original code
			return false;
		} else {
			// click was on the arrow => don't execute any handler
			return true;
		}
	}
});


// Tibi: E6 - overrides and hack to work with ExtJS 5.1.1 & 6.x.x
Ext.override(Ext.form.field.ComboBox, {

	pageSize : Main.viewConfig.FETCH_SIZE_LOV,

	//OVERRIDE (Bug in ExtJS 5.1.0) - http://www.sencha.com/forum/showthread.php?298257
	checkChangeEvents : Ext.isIE ? ['change', 'propertychange', 'keyup'] : ['change', 'input', 'textInput', 'keyup', 'dragdrop'],

	initComponent : function(){
		if( !this.editable ){
			this.selectOnFocus = false;
		}
	
		this.mon(this, "select", function() {
			this.collapse();
		}, this);
		
		this.callParent(arguments);
		
		this.store.pageSize = this.pageSize;
		
		this.mon(this.getPicker(), "itemclick", function() {
			var selectedRecords = this.valueCollection.getRange(),
				selectedRecord = selectedRecords[0],
				selectionCount = selectedRecords.length;

			if( selectionCount ){
	            if (!this.multiSelect) {
	                selectedRecords = selectedRecord;
	            }
				this.fireEvent('select', this, selectedRecords);
			}
		}, this);
	},

	completeEdit: function(e) {
		var me = this;
		// skip the parent call from Ext.form.field.ComboBox, call directly the function from picker field
		this.superclass.superclass.completeEdit(e);
		if( me.queryMode === 'local' ){
			me.doQueryTask.cancel(); // Tibi: auto-lov
			if(Ext.isFunction(me.assertValueMy)){
				me.assertValueMy();
			} else {
				me.assertValue();
			}
		}
	},
 
	onTriggerClick : function() {
		var me = this;
		if (!me.readOnly && !me.disabled && !me.isExpanded) {
			if (me.triggerAction === 'all') {
				me.doQuery(me.allQuery, true);
			} else if (me.triggerAction === 'last') {
				me.doQuery(me.lastQuery, true);
			} else {
				me.doQuery(me.getRawValue(), false, true);
			}
		}
	}, 
	
	onValueCollectionEndUpdate: function() {
		var me = this,
			store = me.store,
			selectedRecords = me.valueCollection.getRange(),
            selectedRecord = selectedRecords[0],
            selectionCount = selectedRecords.length;
        me.updateBindSelection(me.pickerSelectionModel, selectedRecords);
        if (me.isSelectionUpdating()) {
            return;
        }
        Ext.suspendLayouts();
        me.lastSelection = selectedRecords;
        if (selectionCount) {
            me.lastSelectedRecords = selectedRecords;
        }
        me.updateValue();
        
        if (selectionCount && ((!me.multiSelect && store.contains(selectedRecord)) || me.collapseOnSelect || !store.getCount())) {
            me.updatingValue = true;
/*            
            // me.collapse(); // Dan: collapse bug
*/            
            me.updatingValue = false;
        }
        Ext.resumeLayouts(true);
/*        
//        if (selectionCount && !me.suspendCheckChange) { 
//            if (!me.multiSelect) {
//                selectedRecords = selectedRecord;
//            }
//			// fireEvent on select is made on itemClick; the monitor is set in initComponent 
//            me.fireEvent('select', me, selectedRecords);
//        }
*/
    },

	// https://www.sencha.com/forum/showthread.php?301017-5.1.1-combobox-forceSelection-is-too-aggressive-in-clearing-out-the-field-input
	// workaround for bug in ext 5.1.1 combo box
	onLoad: function(store, records, success) {
		var me = this,
			needsValueUpdating = !me.valueCollection.byValue.get(me.value);
		if (success && needsValueUpdating && !(store.lastOptions && 'rawQuery' in store.lastOptions)) {
			me.setValueOnData();
		}
		/*
		//me.checkValueOnChange();
		*/
	},

	onStoreUpdate: function() { // parameters: store, record
		/*
		// this.updateValue();
		*/
	},

	getDisplayValue: function(tplData) {
		tplData = tplData || this.displayTplData;
		return this.applyDisplayTpl().apply(tplData);
	},

    // Tibi: E5.1.1 - i use doSetValue from 5.1.0, because the function from 5.1.1 is not working how we expect
    // see issue SONE-1127, which is present with version 5.1.1
	doSetValue : function(value, add) { 
	
		if(value === "" && this.hasFocus){
			value = this.getRawValue();
		}
	
		var me = this, store = me.getStore(), 
			Model = store.getModel(), 
			matchedRecords = [], 
			valueArray = [], key, autoLoadOnValue = me.autoLoadOnValue, 
			isLoaded = store.getCount() > 0	|| store.isLoaded(), 
			pendingLoad = store.hasPendingLoad(), 
			unloaded = autoLoadOnValue && !isLoaded && !pendingLoad, 
			//forceSelection = me.forceSelection, 
			selModel = me.pickerSelectionModel, 
			displayTplData = me.displayTplData	|| (me.displayTplData = []), 
			displayIsValue = me.displayField === me.valueField, i, len, record, dataObj, raw;
			
		if (add && !me.multiSelect) {
			Ext.Error.raise('Cannot add values to non muiltiSelect ComboBox');
		}

		if (value != null && !displayIsValue && (pendingLoad || unloaded || !isLoaded || store.isEmptyStore)) {
			if (value.isModel) {
				displayTplData.length = 0;
				displayTplData.push(value.data);
				raw = me.getDisplayValue();
			}
			if (add) {
				me.value = Ext.Array.from(me.value).concat(value);
			} else {
				if (value.isModel) {
					value = value.get(me.valueField);
				}
				me.value = value;
			}
			me.setHiddenValue(me.value);

			me.setRawValue(raw || '');
            if (displayIsValue && !Ext.isEmpty(value) && me.inputEl && me.emptyText) {
                me.inputEl.removeCls(me.emptyUICls);
                me.valueContainsPlaceholder = false;
            }			
			if (unloaded && store.getProxy().isRemote) {
				store.load();
			}
			return me;
		}

		value = add ? Ext.Array.from(me.value).concat(value) : Ext.Array.from(value);

		for (i = 0, len = value.length; i < len; i++) {
			record = value[i];

			if (!record || !record.isModel) {
				record = me.findRecordByValue(key = record);

				if (!record) {
					record = me.valueCollection.find(me.valueField, key);
				}
			}

			if (!record) {
/*
//				if (!forceSelection) {
					// Tibi: E5 - when setValue is called create and add the record even if is not in the store list
					// when we have paginated list, the record could be on any page
//					if (!record) {
*/
						dataObj = {};
						dataObj[me.displayField] = value[i];
						if (me.valueField && me.displayField !== me.valueField) {
							dataObj[me.valueField] = value[i];
						}
						record = new Model(dataObj);
						record._fake_ = true; // mark record as a fake, to not become a valid value
/*						
//					}
//				}
//				else if (me.valueNotFoundRecord) {
//					record = me.valueNotFoundRecord;
//				}
*/
			}

			if (record) {
				matchedRecords.push(record);
				valueArray.push(record.get(me.valueField));
			}
		}
		me.lastSelection = matchedRecords;

		me.suspendEvent('select');
		me.valueCollection.beginUpdate();
		if (matchedRecords.length) {
			selModel.select(matchedRecords, false);
		} else {
			selModel.deselectAll();
		}
		me.valueCollection.endUpdate();
		me.resumeEvent('select');
		return me;
	},

	getValue: function() {
		var store = this.getStore();
		if (!store) {
			return this.getRawValue();
		}
        return this.callParent(arguments);
	},
	
	afterQuery: function(queryPlan) {
		var me = this;
		
		if (me.store.getCount()) {
			if (me.typeAhead) {
                me.doTypeAhead(queryPlan);
			}
			
			if (queryPlan.rawQuery) {
				if (me.picker && !me.picker.getSelectionModel().hasSelection()) {
					me.doAutoSelect();
				}
			} else {
				me.doAutoSelect();
			}
		}
		
		// doQuery is called upon field mutation, so check for change after the query has done its thing
		if( !Ext.isEmpty(me.getValue()) ){
			// check the change only if current value is not empty
			// when user is typing the text and the query is finished, sometime the editor contain some values but getValue return empty
			// if checkChange is executed in this condition, the text typed by the user will be removed
			me.checkChange();
		}
	},
	
	_beforeExpandVal_: null,
	expand : function(){
		this.callParent(arguments);
		this._beforeExpandVal_ = this.getValue();
	},
	collapse : function(){
		var check = this.isExpanded;
		this.callParent(arguments);
		if( check ){
			var crtVal = this.getValue();
			if( String(crtVal) !== String(this._beforeExpandVal_) ){
				this.fireEvent('fpchange', this, crtVal, this._beforeExpandVal_);
			}
		}
	}
});


// keep window inside in his container
Ext.override(Ext.window.Window, {
	
	initComponent: function() {
    	this.callParent();
    	// do not allow for window header to move outside of its container
    	// if header is not defined, keep the whole window inside of container
		if( this.header ){
			this.constrainHeader = true;
		} else {
			this.constrain = true;
		}
    },

	show: function(animateTarget, cb, scope) {
		var me = this;
		me.callParent(arguments);
		// SONE-1224: force all windows to the center of the screen; 
		//if need to show window on different position, other solution needed
		if( me._disableCentering_ !== true ){
			me.center();
		}
		// SONE-6088: force all windows to bring to front after show
		Ext.defer(me.toFront, 100, me);
	},
	
	showAt: function(x, y, animate) {
		this._disableCentering_ = true;
		this.callParent(arguments);
		delete this._disableCentering_;
	},
	
	/**
	 *  show the window relative to a component
	 *  opt is an object, which contain information to the position of window
	 *  - vertical: t,b,c,o,u // top, bottom, center, over, under
	 *  - horizontal: l,r,c // left, right, center
	 *  default: 
	 *  	opt: { vertical: "t", horizontal: "r" }
	 */
	showRelativeTo: function(startCmp,opt){
		
		opt = opt || {};
		var d = 5;
		var cmp = startCmp;
		var vp = opt.vertical || "t";
		var hp = opt.horizontal || "r";
		var pos = cmp.getPosition();
		var x = pos[0];
		var y = pos[1];

		var f = function(){
			if( hp === "l" ){
				x -= (d + this.getWidth());
			} else {
				if( hp === "c" ){
					x -= (this.getWidth()-cmp.getWidth())/2;
				} else {
					// hp === "r"
					x += d + cmp.getWidth();				
				}
			}
			// vp : default is top, nothing to do with y
			if( vp === "b" ){
				y -= this.getHeight();				
			} else 
			if( vp === "o" ){
				y -= this.getHeight() - cmp.getHeight();				
			} else 
			if( vp === "u" ){
				y += cmp.getHeight(); 
			} else 
			if( vp === "c" ){
				y -= (this.getHeight()-cmp.getHeight())/2;				
			}

			if( y < d ){
				y = d;
			}
			if( x < d ){
				x = d;
			}
			
			this._disableCentering_ = true;
			this.setPosition(x,y);
			Ext.defer(function(){delete this._disableCentering_;}, 100, this);
		}

		this.show(null,f,this);
	}
});


Ext.override(Ext.form.field.Text, {
	
	selectOnFocus : true,

    getRawValue : function() {
		var me = this, v = me.callParent();
		if (v === me.emptyText && me.valueContainsPlaceholder) {
			v = '';
		}
		if (this.caseRestriction && v !== '') {
			if (this.caseRestriction === "uppercase") {
				v = v.toUpperCase();
			} else {
				v = v.toLowerCase();
			}
			me.rawValue = v;
		}
		return v;
	}
});


/**
 * With an LOV type of editor do not complete edit on first ENTER, just collapse
 * the picker list view
 * 
 */
Ext.override(Ext.Editor, {

	onSpecialKey : function(field, event) {

						
        var me = this, key = event.getKey(), 
        	complete = me.completeOnEnter && key === event.ENTER, 
        	cancel = me.cancelOnEsc	&& key === event.ESC, 
        	task = me.specialKeyTask;

		if (field._isLov_ && field.isExpanded === true) {
			complete = false;
		}
		
		if (complete || cancel) {
			event.stopEvent();
			if (!task) {
				me.specialKeyTask = task = new Ext.util.DelayedTask();
			}
			var fSpecKey = function(){
				if (complete) {
					me.completeEdit();
					var _col = field.column;
					if (field.triggerBlur) {
						if (_col && _col._dcView_) {
							_col._dcView_.getView().focus();
						}
						field.triggerBlur(event);
					}
				} else {
					me.cancelEdit();
					if (field.triggerBlur) {
						field.triggerBlur(event);
					}
				}
			};
			// Must defer this slightly to prevent exiting edit mode
			// before the field's own
			// key nav can handle the enter key, e.g. selecting an item
			// in a combobox list
			task.delay(me.specialKeyDelay, fSpecKey, me);
			// <debug>
			// Makes unit testing easier
			if (me.specialKeyDelay === 0) {
				task.cancel();
				fSpecKey();
			}
			// </debug>
		}

		me.fireEvent('specialkey', me, field, event);
	}
});


Ext.override(Ext.grid.CellEditor, {

	// SONE-3379
	
	beforeItemUpdate: function(record, recordIndex, oldItemDom, columnsToUpdate) {
		if( this.context ){
			return this.callParent(arguments);
		}
	},

	onItemUpdate: function(record, recordIndex, oldItemDom) {
		if( this.context ){
			return this.callParent(arguments);
		}
	},
	
	cacheElement: function() {
	    if (!this.editing && !this.destroyed) {
	    	if (this.el) {
    			Ext.getDetachedBody().dom.appendChild(this.el.dom);
    		}
	    }
	}
});


Ext.override(Ext.grid.plugin.CellEditing, {

	/**
	 * Context content { grid : grid, record : record, field :
	 * columnHeader.dataIndex, value : record.get(columnHeader.dataIndex), row :
	 * view.getNode(rowIdx), column : columnHeader, rowIdx : rowIdx, colIdx :
	 * colIdx }
	 * 
	 * @param {}
	 *            context
	 * @return {Boolean}
	 */
	beforeEdit : function(context) {
		if (context.store && context.store.isLoading()) {
			return false;
		}
		if (context.grid && context.grid.beforeEdit) {
			return context.grid.beforeEdit(context);
		}
	},

	_isEditAllowed_ : function(record, column, field, grid) {
		if (field && field.noEdit) {
			return false;
		}
		if (field && field.noUpdate === true && !record.phantom) {
			return false;
		}
		if (field && field.noInsert === true && record.phantom) {
			return false;
		}
		if (field._enableFn_) {
			var fn = field._enableFn_;
			if (grid != null) {
				return fn.call(grid, grid._controller_, record, column, field);
			} else {
				return fn.call(this, null, record, column, field);
			}
		}
		return true;
	},

	
	getEditor : function(record, column) {
		
		var me = this, editor;
		
		if (me.grid._getCustomCellEditor_) {
			editor = me.grid._getCustomCellEditor_(record, column);
			if (editor != null) {
				if (!this._isEditAllowed_(record, column, editor.field || editor)) {
					return null;
				}

				if (!(editor instanceof Ext.grid.CellEditor)) {
					var editorId = column.id + record.id;
					editor = new Ext.grid.CellEditor({
						floating : true,
						editorId : editorId,
						field : editor
					});
				}
				var editorOwner = me.grid.ownerLockable || me.grid;
				editorOwner.add(editor);
				editor.on({
					scope : me,
					specialkey : me.onSpecialKey,
					complete : me.onEditComplete,
					canceledit : me.cancelEdit
				});

				column.on('removed', me.cancelActiveEdit, me);
				editor.field["_targetRecord_"] = record;

				editor.grid = me.grid;
				// Keep upward pointer correct for each use - editors are shared
				// between locking sides
				editor.editingPlugin = me;

				return editor;
			}
		}
		

		editor = this.callParent(arguments);

		var editAllowed;
		if (me.grid) {
			editAllowed = this._isEditAllowed_(record, column, editor.field || editor, me.grid);
		} else {
			editAllowed = this._isEditAllowed_(record, column, editor.field || editor, null);
		}

		if (editor && editAllowed === false) {
			return false;
		}

		if (editor.field.caseRestriction) {
			editor.field.fieldStyle = "text-transform:" + editor.field.caseRestriction + ";";
		}

		if (editor.field) {
			if (me.grid && (me.grid._dcViewType_ === "filter-propgrid")) {
				editor.field["_targetRecord_"] = me.grid._controller_.filter;
			} else {
				editor.field["_targetRecord_"] = record;
			}

		}

		return editor;
	},
    
    showEditor: function(ed, context, value) {
    	this.callParent(arguments);
        if(ed.field && Ext.isFunction(ed.field.validate)){
        	ed.field.validate();
        }
    },

	cacheDeactivatedEditors: function() {
		// in some situations the editor.el is null, so the original ExtJs code cause unhandled exceptions
		// this exceptions are avoided by the fix
		var me = this,
			editors = me.editors.items,
			len = editors.length,
			i, editor,
			detachedBody = Ext.getDetachedBody();
		for (i = 0; i < len; i++) {
			editor = editors[i];
			if(editor && !editor.isVisible()) {
				if(editor.el && editor.el.dom){
					detachedBody.dom.appendChild(editor.el.dom);
				}
				editor.container = detachedBody;
			}
		}
	},
    
    setColumnField: function(column, field) {
        // In Ext 6.5.0 when Ext.destroy(ed, column.field) is called the column are removed from grid
        // (the header remains) and the editing will be disabled until page is reloaded
        // and possible layout damages on other ui layers
        // oc: var ed = this.editors.getByKey(column.getItemId());
        // oc: Ext.destroy(ed, column.field);
        this.editors.removeAtKey(column.getItemId());
        this.callParent(arguments);
    }
});


// known issue in ExtJS 5.1.0
// http://www.sencha.com/forum/showthread.php?294953-Tab-switching-bug
// will be fixed in 5.1.1
// SONE-1229
// same code is in Ext 6
Ext.override(Ext.tab.Bar,{

	initComponent : function() {
		var me = this, 
			initialLayout = me.initialConfig.layout, 
			initialAlign = initialLayout && initialLayout.align, 
			initialOverflowHandler = initialLayout && initialLayout.overflowHandler; 

		if (me.plain) {
			me.addCls(me.baseCls + '-plain');
		}

		me.callParent();

		me.setLayout({
			align : initialAlign || (me.getTabStretchMax() ? 'stretchmax' : me._layoutAlign[me.dock]),
			overflowHandler : initialOverflowHandler || 'scroller'
		});

		// We have to use mousedown here as opposed to click event, because
		// Firefox will not fire click in certain cases if mousedown/mouseup
		// happens within btnInnerEl.
		me.on({
			mousedown : me.onClick,
			element : 'el',
			scope : me
		});
	}
});

Ext.override(Ext.form.field.Number,{
	
    valueToRaw: function(value) {
    	// SONE-1364
    	// value can be -0, but when is converted to string become 0 => need a trick to obtain the information about sign
		var me = this,
			decimalSeparator = me.decimalSeparator,
			strVal = String(me.getRawValue()).trim();
		// do the job from extjs
		value = me.parseValue(value);
		value = me.fixPrecision(value);
		value = Ext.isNumber(value) ? value : parseFloat(String(value).replace(decimalSeparator, '.'));
		value = isNaN(value) ? '' : String(value).replace('.', decimalSeparator);
		// add the '-' sign if necessary
		if( strVal.charAt(0)==='-' && value.charAt(0)!=='-' && value.length>0 ){
			value = '-' + value;
		}
        return value;
    },

	setMinValue : function(value) {
		var me = this,
			allowed;
		
		me.minValue = Ext.Number.from(value, Number.NEGATIVE_INFINITY);
		me.toggleSpinners();

		// Build regexes for masking and stripping based on the configured
		// options
		if (me.disableKeyFilter !== true) {
			allowed = me.baseChars + '';
		
			if (me.allowExponential) {
				allowed += me.decimalSeparator + 'e+-';
			}
			else {
				if (me.allowDecimals) {
					allowed += me.decimalSeparator;
				}
				if (me.minValue < 0) {
					allowed += '-';
				}
			}
			
			allowed = Ext.String.escapeRegex(allowed.replace(me.decimalSeparator, '.'));
			me.maskRe = new RegExp('[' + allowed + ']');
			if (me.autoStripChars) {
				me.stripCharsRe = new RegExp('[^' + allowed + ']', 'gi');
			}
		}
	}
});


Ext.override(Ext.grid.header.Container,{
	
    getColumnMenu: function(headerContainer) {
        var menuItems = [],
            i = 0,
            item,
            items = headerContainer.query('>gridcolumn[hideable]'),
            itemsLn = items.length,
            menuItem;
        
        for (; i < itemsLn; i++) {
            item = items[i];
            // show the column only if not configured to hide always
            if( item.alwaysHidden !== true ){
	            menuItem = new Ext.menu.CheckItem({
	                text: item.menuText || item.text,
	                checked: !item.hidden,
	                hideOnClick: false,
	                headerId: item.id,
	                menu: item.isGroupHeader ? this.getColumnMenu(item) : undefined,
	                checkHandler: this.onColumnCheckChange,
	                scope: this
	            });
	            menuItems.push(menuItem);
            }
        }
        
        return menuItems.length ? menuItems : null;
    }
    
});


Ext.override(Ext.grid.feature.Summary, {

	init: function(grid) {
		this.callParent(arguments);
		this.view.mon(this.view.store,{
			loadempty : this.clearSummaryData,
			clear : this.clearSummaryData,
			scope : this
		});
		grid.__summaryFeature__ = this; // hack to have a direct reference from grid to summary feature
	},
	
	// remote summary must be removed when data from store is removed
	clearSummaryData : function(){
    	if( this.remoteRoot ){
    		var rd = this.view.store.getProxy().getReader().rawData;
    		if( Ext.isObject(rd) ){
    			rd[this.remoteRoot] = {};
    		}
    	}
    	var sr = this.summaryRecord;
    	if( sr && Ext.isObject(sr.data) ){
    		for(var f in sr.data){
    			if( f !== "id" ){
    				sr.set(f,null);
    			}
    		}
    	}
    },
    
    setSummaryVal : function(name,val,silent){
    	var rd = this.view.store.getProxy().getReader().rawData;
    	if( !rd.summaries ){
    		rd.summaries = {};
    	}
    	rd.summaries[name] = val;
    	this.summaryRecord.set(name,val);
    	if( silent!==true ){
    		this.onStoreUpdate();
    	}
    },
    getSummaryVal : function(name){
    	return this.summaryRecord.get(name);
    }
});


Ext.override(Ext.toolbar.Paging, {
	
	_processAjaxResponse_ : null,

	moveFirst: function() {
		this._processAjaxResponse_ = true;
		return this.callParent(arguments);
	},
	
	movePrevious: function() {
		this._processAjaxResponse_ = true;
		return this.callParent(arguments);
	},
	
	moveNext: function() {
		this._processAjaxResponse_ = true;
		return this.callParent(arguments);
	},
	
	moveLast: function() {
		this._processAjaxResponse_ = true;
		return this.callParent(arguments);
	},
	
	doRefresh: function() {
		this._processAjaxResponse_ = true;
		return this.callParent(arguments);
	},

	onLoad : function(store, records, success, operation){
		if( this._processAjaxResponse_ && this.store._controller_ ){
			this._processAjaxResponse_ = false;
			if( operation && operation._response ){
				var r = Ext.getResponseDataInJSON(operation._response);
				if (r.params) {
					var dc = this.store._controller_;
					for ( var p in r.params ) {
						dc.setParamValue(p, r.params[p]);
					}
				}
			}
		}
		return this.callParent(arguments);
	},

	setChildDisabled: function(selector, disabled) {
		// do not let user to press refresh if no query executed before
		if( disabled !== true && ( !this.store || !this.store.lastOptions ) ){
			disabled = true;
		}
		this.callParent([selector,disabled]);
	}
});


Ext.override(Ext.data.operation.Destroy, {
	doProcess: function() {
		var clientRecords = this.getRecords(),
			clientLen = clientRecords.length,
			i;
		// fix for ExtJs bug: EXTJS-17916
		// delete crash if 2 or more records are deleted from a grid in one operation
		for (i=clientLen-1; i>=0; i--) {
			if(clientRecords[i]){
				clientRecords[i].setErased();
			}
		}
	}
});

/**
 * 	FpNumberField use formatValue and unFormatValue to display numbers formatted on the forms.
 * 	On the same template we can create further classes with different formatting patterns.
 */
Ext.define('Ext.form.field.FpNumberField', {
    extend: 'Ext.form.field.Number',
    alias: 'widget.fpnumberfield',
    
	thousandSeparator: null,
	decimalSeparator: null,

    initComponent: function(){
		this.thousandSeparator = Ext.util.Format.thousandSeparator;
		this.decimalSeparator = Ext.util.Format.decimalSeparator;
        this.enableRawFromatting = true; 
        this.callParent(arguments);
    },
    
    formatValue: function(v){
        if (Ext.isEmpty(v)) {
            return v;
        }
        var fv = Ext.util.Format.number(v,this.format);
        if( ( (""+v).trim().charAt(0) === '-' && (""+fv).charAt(0) !== '-' ) || Ext.isEmpty(fv)) {
        	fv = v;
        }
        return fv;
    },
    unFormatValue: function(v){
		var vv = String(v).replaceAll(this.thousandSeparator, "").replace(this.decimalSeparator,".");
		if( Ext.isEmpty(vv) && !Ext.isEmpty(v) ){
			vv = v;
		}
		return vv;
    },

    onFocus: function() {
    	var me = this;
    	me.callParent(arguments);
    	setTimeout(function(){
    		me.setInEditMode(true);
    	}, 10);
    },
    
    onBlur: function(){
    	var me = this;
        me.callParent(arguments);
    	me.setInEditMode(false);
    },
    
    valueToRaw: function(value) {
		var me = this,
			decimalSeparator = me.decimalSeparator;
		// do the job from extjs
		value = me.parseValue(value);
		value = me.fixPrecision(value);
		value = Ext.isNumber(value) ? value : parseFloat(String(value).replace(decimalSeparator, '.'));
		value = isNaN(value) ? '' : String(value);
		return value;
    }
});


Ext.override(Ext.view.Table, {
	
	processItemEvent: function(record, item, rowIndex, e) {
		var res = true;
		try{
			res = this.callParent(arguments);
		}catch(e){
			//nothing to do
		}
		return res;
	}
});

// Dan: single row expander plugin

Ext.define('Ext.grid.plugin.RowExpanderPlus', {
    extend: 'Ext.grid.plugin.RowExpander',

    alias: 'plugin.rowexpanderplus',
    
    pluginId: "rowexpanderplus",
    
    /**
     * @cfg {Boolean} expandOnlyOne
     * <tt>true</tt> to allow only one expanded row
     * (defaults to <tt>true</tt>).
     */
    expandOnlyOne: true, // keep always true
    grid: null,
    lastExpandedRowIdx: null,

    // Tibi: in some conditions (records are deleted, grid refresh, etc. 
    // these functions can produce null pointer exception; to avoid crash in GUI, 
    // I use try-catch statement (to not fill the code with a lot of if/then/else statements)
    
    // Overwrite RowExpander.toggleRow(rowIdx)
    toggleRow: function(rowIdx) {
    	try{
	        var row = Ext.get(this.view.getNode(rowIdx));
	        if (row.hasCls(this.rowCollapsedCls)) {
	            if (this.lastExpandedRowIdx !== null && this.expandOnlyOne === true) {
	                this.collapseRow(this.lastExpandedRowIdx);
	            }
	            this.expandRow(rowIdx);
	            this.lastExpandedRowIdx = rowIdx;
	        } else {
	            this.collapseRow(rowIdx);
	        }
    	}catch(e){
    		return false;
    	}
    },
    
    expandRow: function(rowIdx) {
    	try{
	        var view = this.view,
	            rowNode = view.getNode(rowIdx),
	            row = Ext.get(rowNode),
	            nextBd = Ext.get(row).down(this.rowBodyTrSelector),
	            record = view.getRecord(rowNode);
	
	            row.removeCls(this.rowCollapsedCls);
	            nextBd.removeCls(this.rowBodyHiddenCls);
	            this.recordsExpanded[record.internalId] = true;
	            view.refreshSize();
	            view.fireEvent('expandbody', rowNode, record, nextBd.dom);
    	}catch(e){
    		return false;
    	}
    },
    
    collapseRow: function(rowIdx) {
    	try{
	        var view = this.view,
	            rowNode = view.getNode(rowIdx),
	            row = Ext.get(rowNode),
	            nextBd = Ext.get(row).down(this.rowBodyTrSelector),
	            record = view.getRecord(rowNode);
	
	            row.addCls(this.rowCollapsedCls);
	            nextBd.addCls(this.rowBodyHiddenCls);
	            this.recordsExpanded[record.internalId] = false;
	            view.refreshSize();
	            view.fireEvent('collapsebody', rowNode, record, nextBd.dom);
    	}catch(e){
    		return false;
    	}
    },
    
    /**
     * @method
     * Collapse the last expanded row.
     */
    collapseLastRow: function() {
    	if( Ext.isNumber(this.lastExpandedRowIdx) ){
    		this.collapseRow(this.lastExpandedRowIdx);
        	this.lastExpandedRowIdx = null;
    	}
    }
    
});



Ext.override(Ext.form.field.Tag, {

	filterPicked: function(rec) {
		return !this.valueCollection.containsKey(rec.id);
	}
});

// Dan: multiselect

/**
 * A control that allows selection of multiple items in a list.
 */
Ext.define('Ext.ux.form.MultiSelect', {
    
    extend: 'Ext.form.FieldContainer',
    
    mixins: [
        'Ext.util.StoreHolder',
        'Ext.form.field.Field'
    ],
    
    alternateClassName: 'Ext.ux.Multiselect',
    alias: ['widget.multiselectfield', 'widget.multiselect'],
    
    requires: ['Ext.panel.Panel', 'Ext.view.BoundList', 'Ext.layout.container.Fit'],
    
    uses: ['Ext.view.DragZone', 'Ext.view.DropZone'],
    
    layout: 'anchor',
    
    /**
     * @cfg {String} [dragGroup=""] The ddgroup name for the MultiSelect DragZone.
     */

    /**
     * @cfg {String} [dropGroup=""] The ddgroup name for the MultiSelect DropZone.
     */
    
    /**
     * @cfg {String} [title=""] A title for the underlying panel.
     */
    
    /**
     * @cfg {Boolean} [ddReorder=false] Whether the items in the MultiSelect list are drag/drop reorderable.
     */
    ddReorder: false,

    /**
     * @cfg {Object/Array} tbar An optional toolbar to be inserted at the top of the control's selection list.
     * This can be a {@link Ext.toolbar.Toolbar} object, a toolbar config, or an array of buttons/button configs
     * to be added to the toolbar. See {@link Ext.panel.Panel#tbar}.
     */

    /**
     * @cfg {String} [appendOnly=false] `true` if the list should only allow append drops when drag/drop is enabled.
     * This is useful for lists which are sorted.
     */
    appendOnly: false,

    /**
     * @cfg {String} [displayField="text"] Name of the desired display field in the dataset.
     */
    displayField: 'text',

    /**
     * @cfg {String} [valueField="text"] Name of the desired value field in the dataset.
     */

    /**
     * @cfg {Boolean} [allowBlank=true] `false` to require at least one item in the list to be selected, `true` to allow no
     * selection.
     */
    allowBlank: true,

    /**
     * @cfg {Number} [minSelections=0] Minimum number of selections allowed.
     */
    minSelections: 0,

    /**
     * @cfg {Number} [maxSelections=Number.MAX_VALUE] Maximum number of selections allowed.
     */
    maxSelections: Number.MAX_VALUE,

    /**
     * @cfg {String} [blankText="This field is required"] Default text displayed when the control contains no items.
     */
    blankText: 'This field is required',

    /**
     * @cfg {String} [minSelectionsText="Minimum {0}item(s) required"] 
     * Validation message displayed when {@link #minSelections} is not met. 
     * The {0} token will be replaced by the value of {@link #minSelections}.
     */
    minSelectionsText: 'Minimum {0} item(s) required',
    
    /**
     * @cfg {String} [maxSelectionsText="Maximum {0}item(s) allowed"] 
     * Validation message displayed when {@link #maxSelections} is not met
     * The {0} token will be replaced by the value of {@link #maxSelections}.
     */
    maxSelectionsText: 'Maximum {0} item(s) required',

    /**
     * @cfg {String} [delimiter=","] The string used to delimit the selected values when {@link #getSubmitValue submitting}
     * the field as part of a form. If you wish to have the selected values submitted as separate
     * parameters rather than a single delimited parameter, set this to `null`.
     */
    delimiter: ',',
    
    /**
     * @cfg {String} [dragText="{0} Item{1}"] The text to show while dragging items.
     * {0} will be replaced by the number of items. {1} will be replaced by the plural
     * form if there is more than 1 item.
     */
    dragText: '{0} Item{1}',

    /**
     * @cfg {Ext.data.Store/Array} store The data source to which this MultiSelect is bound (defaults to `undefined`).
     * Acceptable values for this property are:
     * <div class="mdetail-params"><ul>
     * <li><b>any {@link Ext.data.Store Store} subclass</b></li>
     * <li><b>an Array</b> : Arrays will be converted to a {@link Ext.data.ArrayStore} internally.
     * <div class="mdetail-params"><ul>
     * <li><b>1-dimensional array</b> : (e.g., <tt>['Foo','Bar']</tt>)<div class="sub-desc">
     * A 1-dimensional array will automatically be expanded (each array item will be the combo
     * {@link #valueField value} and {@link #displayField text})</div></li>
     * <li><b>2-dimensional array</b> : (e.g., <tt>[['f','Foo'],['b','Bar']]</tt>)<div class="sub-desc">
     * For a multi-dimensional array, the value in index 0 of each item will be assumed to be the combo
     * {@link #valueField value}, while the value at index 1 is assumed to be the combo {@link #displayField text}.
     * </div></li></ul></div></li></ul></div>
     */
    
    ignoreSelectChange: 0,

    /**
     * @cfg {Object} listConfig
     * An optional set of configuration properties that will be passed to the {@link Ext.view.BoundList}'s constructor.
     * Any configuration that is valid for BoundList can be included.
     */
     
     // Dan: customization
     
     selectionCollection : [],

    initComponent: function(){
        var me = this;
        me.items = me.setupItems();
        me.layout = 'fit';
        me.bindStore(me.store, true);

        if (me.store.autoCreated) {
            me.valueField = me.displayField = 'field1';
            if (!me.store.expanded) {
                me.displayField = 'field2';
            }
        }

        if (!Ext.isDefined(me.valueField)) {
            me.valueField = me.displayField;
        }

        me.callParent();
        me.initField();
        
    },

    setupItems: function() {
        var me = this;
        me.boundList = Ext.create('Ext.view.BoundList', Ext.apply({
            //anchor: 'none 100%',
            border: 1,
            multiSelect: true,
            store: me.store,
            displayField: me.displayField,
            disabled: me.disabled
        }, me.listConfig));
        
        // Dan: customization
         me.boundList.on('itemclick', me.restoreSelections, me);

        // Boundlist expects a reference to its pickerField for when an item is selected (see Boundlist#onItemClick).
        me.boundList.pickerField = me;

        // Only need to wrap the BoundList in a Panel if we have a title.
        if (!me.title) {
            return me.boundList;
        }

        // Wrap to add a title
        me.boundList.border = false;
        return {
            border: true,
            anchor: 'none 100%',
            layout: 'anchor',
            title: me.title,
            tbar: me.tbar,
            items: me.boundList
        };
        
    },

        
    // Dan: customizations
    
    
    restoreSelections : function(el, record){ // additional parameters: item

    	var selections = this.selectionCollection;
    	
        var index = selections.indexOf(record.get(this.valueField));
        
        if (index > -1) {
          selections.splice(index, 1);
        }
        else {
        	selections.push(record.get(this.valueField));
        }
        
        this.setValue(selections);
    },
    
    getSelected: function(){
        return this.boundList.getSelectionModel().getSelection();
    },
    
    // compare array values
    isEqual: function(v1, v2) {
        var fromArray = Ext.Array.from,
            i = 0, 
            len;

        v1 = fromArray(v1);
        v2 = fromArray(v2);
        len = v1.length;

        if (len !== v2.length) {
            return false;
        }

        for(; i < len; i++) {
            if (v2[i] !== v1[i]) {
                return false;
            }
        }

        return true;
    },
    
    afterRender: function(){
        var me = this,
            records;
        
        me.callParent();
        if (me.selectOnRender) {
            records = me.getRecordsForValue(me.value);
            if (records.length) {
                ++me.ignoreSelectChange;
                me.boundList.getSelectionModel().select(records);
                --me.ignoreSelectChange;
            }
            delete me.toSelect;
            
        }    
        
        if (me.ddReorder && !me.dragGroup && !me.dropGroup){
            me.dragGroup = me.dropGroup = 'MultiselectDD-' + Ext.id();
        }

        if (me.draggable || me.dragGroup){
            me.dragZone = Ext.create('Ext.view.DragZone', {
                view: me.boundList,
                ddGroup: me.dragGroup,
                dragText: me.dragText
            });
        }
        if (me.droppable || me.dropGroup){
            me.dropZone = Ext.create('Ext.view.DropZone', {
                view: me.boundList,
                ddGroup: me.dropGroup,
                handleNodeDrop: function(data, dropRecord, position) {
                    var view = this.view,
                        store = view.getStore(),
                        records = data.records,
                        index;

                    // remove the Models from the source Store
                    data.view.store.remove(records);

                    index = store.indexOf(dropRecord);
                    if (position === 'after') {
                        index++;
                    }
                    store.insert(index, records);
                    view.getSelectionModel().select(records);
                    me.fireEvent('drop', me, records);
                }
            });
        }
    },
    
    isValid : function() {
        var me = this,
            disabled = me.disabled,
            validate = me.forceValidation || !disabled;
            
        
        return validate ? me.validateValue(me.value) : disabled;
    },
    
    validateValue: function(value) {
        var me = this,
            errors = me.getErrors(value),
            isValid = Ext.isEmpty(errors);
            
        if (!me.preventMark) {
            if (isValid) {
                me.clearInvalid();
            } else {
                me.markInvalid(errors);
            }
        }

        return isValid;
    },
    
    markInvalid : function(errors) {
        // Save the message and fire the 'invalid' event
        var me = this,
            oldMsg = me.getActiveError();
        me.setActiveErrors(Ext.Array.from(errors));
        if (oldMsg !== me.getActiveError()) {
            me.updateLayout();
        }
    },

    /**
     * Clear any invalid styles/messages for this field.
     *
     * __Note:__ this method does not cause the Field's {@link #validate} or {@link #isValid} methods to return `true`
     * if the value does not _pass_ validation. So simply clearing a field's errors will not necessarily allow
     * submission of forms submitted with the {@link Ext.form.action.Submit#clientValidation} option set.
     */
    clearInvalid : function() {
        // Clear the message and fire the 'valid' event
        var me = this,
            hadError = me.hasActiveError();
        me.unsetActiveError();
        if (hadError) {
            me.updateLayout();
        }
    },
    
    getSubmitData: function() {
        var me = this,
            data = null,
            val;
        if (!me.disabled && me.submitValue && !me.isFileUpload()) {
            val = me.getSubmitValue();
            if (val !== null) {
                data = {};
                data[me.getName()] = val;
            }
        }
        return data;
    },

    /**
     * Returns the value that would be included in a standard form submit for this field.
     *
     * @return {String} The value to be submitted, or `null`.
     */
    getSubmitValue: function() {
        var me = this,
            delimiter = me.delimiter,
            val = me.getValue();
        
        return Ext.isString(delimiter) ? val.join(delimiter) : val;
    },
    
    getValue: function(){
        return this.value || [];
    },
    
    getRecordsForValue: function(value){
        var me = this,
            records = [],
            all = me.store.getRange(),
            valueField = me.valueField,
            i = 0,
            allLen = all.length,
            rec,
            j,
            valueLen;
            
        for (valueLen = value.length; i < valueLen; ++i) {
            for (j = 0; j < allLen; ++j) {
                rec = all[j];   
                if (rec.get(valueField) === value[i]) {
                    records.push(rec);
                }
            }    
        }
            
        return records;
    },
    
    setupValue: function(value){
        var delimiter = this.delimiter,
            valueField = this.valueField,
            i = 0,
            out,
            len,
            item;
            
        if (Ext.isDefined(value)) {
            if (delimiter && Ext.isString(value)) {
                value = value.split(delimiter);
            } else if (!Ext.isArray(value)) {
                value = [value];
            }
        
            for (len = value.length; i < len; ++i) {
                item = value[i];
                if (item && item.isModel) {
                    value[i] = item.get(valueField);
                }
            }
            out = Ext.Array.unique(value);
        } else {
            out = [];
        }
        return out;
    },
    
    setValue: function(value){
    	
        var me = this,
            selModel = me.boundList.getSelectionModel(),
            store = me.store;
        
        me.selectionCollection = value;

        // Store not loaded yet - we cannot set the value
        if (!store.getCount()) {
            store.on({
                load: Ext.Function.bind(me.setValue, me, [value]),
                single: true
            });
            return;
        }

        value = me.setupValue(value);
        me.mixins.field.setValue.call(me, value);
        
        if (me.rendered) {
            ++me.ignoreSelectChange;
            selModel.deselectAll();
            if (value.length) {
                selModel.select(me.getRecordsForValue(value));
            }
            --me.ignoreSelectChange;
        } else {
            me.selectOnRender = true;
        }
    },
    
    clearValue: function(){
        this.setValue([]);    
    },
    
    onEnable: function(){
        var list = this.boundList;
        this.callParent();
        if (list) {
            list.enable();
        }
    },
    
    onDisable: function(){
        var list = this.boundList;
        this.callParent();
        if (list) {
            list.disable();
        }
    },
    
    getErrors : function(value) {
        var me = this,
            format = Ext.String.format,
            errors = [],
            numSelected;

        value = Ext.Array.from(value || me.getValue());
        numSelected = value.length;

        if (!me.allowBlank && numSelected < 1) {
            errors.push(me.blankText);
        }
        if (numSelected < me.minSelections) {
            errors.push(format(me.minSelectionsText, me.minSelections));
        }
        if (numSelected > me.maxSelections) {
            errors.push(format(me.maxSelectionsText, me.maxSelections));
        }
        return errors;
    },
    
    onDestroy: function(){
        var me = this;
        
        me.bindStore(null);
        Ext.destroy(me.dragZone, me.dropZone);
        me.callParent();
    },
    
    onBindStore: function(store){
        var boundList = this.boundList;
        
        if (boundList) {
            boundList.bindStore(store);
        }
    }
    
});


Ext.override(Ext.chart.series.Gauge, {
    config: {
        totalAngle: Math.PI
    },
    doUpdateShape: function(radius, donut) {
        var endRhoArray,
            sectors = this.getSectors(),
            sectorCount = (sectors && sectors.length) || 0,
            needleLength = this.getNeedleLength() / 90;
        endRhoArray = [
            radius * needleLength,
            radius
        ];
        while (sectorCount--) {
            endRhoArray.push(radius);
        }
        this.setSubStyle({
            endRho: endRhoArray,
            startRho: radius / 100 * donut
        });
        this.doUpdateStyles();
    }
});


Ext.override(Ext.form.field.FileButton, {
	privates: {
		getFocusEl: function() {
			return false;
		},
		getFocusClsEl: function() {
			return this.el;
		}
	}
});

Ext.override(Ext.form.field.File, {
	privates: {
	    getFocusEl: function() {
			if( this.button.isFocusable() ){ 
				return this.button; 
			} else { 
				return null;
			}
	    }
	}
});	


Ext.override(Ext.form.field.Radio, {
	onBoxClick: function() {
		// if the radio-fied is part from a group, check the values and fire the fpchange event if is the case
		if( this._groupObj_ ){
			// save the old values
			var oldGV = this._groupObj_.getValue();
			var oldV = this.getValue();
			// do the job from parent
			this.callParent(arguments);
			// set the focus on this field => cancel edit from other fields
			this._groupObj_.focus();
			// check if new value is different from old and fire the fpchange event on the master object
			var newV = this.getValue();
			if( oldV !== newV ){
				this._groupObj_.fireEvent('fpchange', this._groupObj_, this._groupObj_.getValue(), oldGV);
			}
		} else {
			this.callParent(arguments);
		}
    }
});

Ext.define('Ext.form.FpRadioGroup', {
    extend: 'Ext.form.RadioGroup',
    alias: 'widget.fpradiogroup',
    
    initComponent: function(){
        this.callParent(arguments);
        // inject this object instance in the child items
        var l = this.items.length;
        var i;
        for(i=0;i<l;i++){
        	this.items.items[i]._groupObj_ = this;
        }
        this._registerListeners_();
    },
    
    _registerListeners_ : function() {
    	this.mon(this, "afterrender", function(radioGroup) {    		
			var view = radioGroup._dcView_;
			var ctrl = view._controller_;
			var rec = ctrl.getRecord();
			if (rec) {
				if (radioGroup.dataIndex) {
					var currentRecord = rec.get(radioGroup.dataIndex);
					var currentItems = radioGroup.items.items[0];
					var firstItemValue = currentItems.boxLabel;
					if (Ext.isEmpty(currentRecord)) {
						rec.set(radioGroup.dataIndex,firstItemValue);
					}
				}
			}
		}, this);
    },
    
    setValue: function(v){
		var oldV = this.getValue();
    	if( !Ext.isObject(v) ){
    		var o = {};
    		o[this.name] = v;
    		v = o;
    	}
    	this.callParent([v]);
    	
		var newV = this.getValue();
		if( oldV !== newV ){
			this.fireEvent('fpchange', this, newV, oldV);
		}
    },
    
    getValue: function(){
    	var v = this.callParent(arguments);
    	if( Ext.isObject(v) ){
    		if( v.hasOwnProperty(this.name) ){
				v = v[this.name];
				if(Ext.isArray(v)){
					if(v.length>0){
						v = v[0];
					} else {
						v = null;
					}
				}
			} else {
	    		if( v.hasOwnProperty("inputValue") ){
					v = v["inputValue"];
					if(Ext.isArray(v)){
						if(v.length>0){
							v = v[0];
						} else {
							v = null;
						}
					}
	    		} else {
	    			v = null;
	    		}
			}
    	}
    	return v;
    }
});


Ext.override(Ext.grid.CellContext, {
	// in some situations row/column can be null, and the code from ExtJs crash in this case
	// this is fixed in the setRow and setColumn functions
	setRow: function(row) {
        var me = this,
            dataSource = me.view.dataSource;
        if (row !== undefined && row !== null) {
            // Row index passed
            if (typeof row === 'number') {
                me.rowIdx = Math.max(Math.min(row, dataSource.getCount() - 1), 0);
                me.record = dataSource.getAt(row);
            }
            // row is a Record
            else if (row.isModel) {
                me.record = row;
                me.rowIdx = dataSource.indexOf(row);
            }
            // row is a grid row, or Element wrapping row
            else if (row.tagName || row.isElement) {
                me.record = me.view.getRecord(row);
                me.rowIdx = dataSource.indexOf(me.record);
            }
        }
        return me;
    },
    setColumn: function(col) {
        var me = this,
            colMgr = me.view.getVisibleColumnManager();
        // Maintainer:
        // We MUST NOT update the context view with the column's view because this context
        // may be for an Ext.locking.View which spans two grid views, and a column references
        // its local grid view.
        if (col !== undefined && col !== null) {
            if (typeof col === 'number') {
                me.colIdx = col;
                me.column = colMgr.getHeaderAtIndex(col);
            } else if (col.isHeader) {
                me.column = col;
                // Must use the Manager's indexOf because view may be a locking view
                // And Column#getVisibleIndex returns the index of the column within its own header.
                me.colIdx = colMgr.indexOf(col);
            }
        }
        return me;
    }
});

Ext.define('ToggleComponent', {
	extend : 'Ext.slider.Single',
	alias : 'widget.togglebutton',
	config : {
		defaultBindProperty : 'value'
	},
	cls : 'toggleoff',
	width : 10,
	animate : false,
	maxValue : 1,
	initComponent : function() {
		this.callParent();
		this.on("specialkey", function(field, e){
			if (e.getKey() === e.ENTER) {
				field.setValue(1-field.getValue());
				e.stopEvent();
			}
		}, this);
		this.on("focus", function(field){
			field.addCls('togglefocused');
		}, this);
		this.on("blur", function(field){
			field.removeCls('togglefocused');
		}, this);
	},
	getValue : function() {
		var toggleValue = this.thumbs[0].value;
		if (toggleValue === 1) {
			this.removeCls('toggleoff');
			this.addCls('toggleon');
		} else {
			this.removeCls('toggleon');
			this.addCls('toggleoff');
		}
		return this.callParent([ 0 ]);
	},
	listeners : {
		afterRender : function(toggle) {
			var toggleValue = toggle.thumbs[0].value;
			if (toggleValue === 1) {
				this.removeCls('toggleoff');
				this.addCls('toggleon');
			}
        }
    }
});

Ext.override(Ext.form.field.Picker, {
	
	_beforeExpandVal_ : null,
	
	expand: function() {
		this._beforeExpandVal_ = this.getValue();
		this.callParent(arguments);
	},
	
	collapse: function() {
		this.callParent(arguments);
		var crtVal = this.getValue();
		this._doCheckAndFireValueChange_(crtVal, this._beforeExpandVal_);
		this._beforeExpandVal_ = crtVal;
	}
});

Ext.override(Ext.form.field.Date, {
   
    onBlur: function(e) {
        var me = this, v = me.rawToValue(me.getRawValue());
        if( !this._isDisabled_() ){
	        if (v) {
	        	if (Ext.isDate(v)) {
	        		me.setValue(v);
	        	}
	        }
	        else {
	        	me.setValue(null);
	        }
	        me.callParent([e]);
        }
    }
});


Ext.override(Ext.grid.NavigationModel, {
	
   onCellMouseDown: function(view, cell, cellIndex, record, row, recordIndex, mousedownEvent) {
      var targetComponent = Ext.Component.fromElement(mousedownEvent.target, cell),
         ac;

      if (view.actionableMode && (mousedownEvent.getTarget(null, null, true).isTabbable() || ((ac = Ext.ComponentManager.getActiveComponent()) && ac.owns(mousedownEvent)))) {
         return;
      }

      if (mousedownEvent.pointerType !== 'touch') {
         // for text selection do not execute this line of code: mousedownEvent.preventDefault();
         this.setPosition(mousedownEvent.position, null, mousedownEvent);
      }

      if (targetComponent && targetComponent.isFocusable && targetComponent.isFocusable()) {
         view.setActionableMode(true, mousedownEvent.position);
         targetComponent.focus();
      }
   }
});


Ext.override(Ext.toolbar.Toolbar, {
	
	privates: {
		// move focus to the next/prev item in the toolbar
		onFocusableContainerTabKey : function(e) {
			var res;
			if (e.shiftKey) {
				res = this.moveChildFocus(e, false);
			} else {
				res = this.moveChildFocus(e, true);
			}
			if( res ){
				e.preventDefault();
			}
			return res;
		},

        findNextFocusableChild : function(options) {
			// This method is private, so options should always be provided
			var beforeRender = options.beforeRender, items, item, child, step, idx, i, len;
			items = options.items || this.getFocusables();
			step = options.step != null ? options.step : 1;
			child = options.child;
			// If the child is null or undefined, idx will be -1.
			// The loop below will account for that, trying to find
			// the first focusable child from either end (depending on step)
			idx = Ext.Array.indexOf(items, child);
			// It's often easier to pass a boolean for 1/-1
			step = step === true ? 1 : step === false ? -1 : step;
			len = items.length;
			if( ( idx===0 && step<0 ) || ( idx===len && step>0 ) ){
				return null;
			}
			i = step > 0 ? (idx < len ? idx + step : 0) : (idx > 0 ? idx + step : len - 1);
			for (;; i += step) {
				// We're looking for the first or last focusable child
				// and we've reached the end of the items, so punt
				if( (idx < 0 && (i >= len || i < 0))
					|| ( (i >= len) || (i < 0) || (i === idx) ) ){
					// do not start from begin and from end - disable loop over
					return null;
				}
				item = items[i];
				if (!item || !item.focusable || item.disabled) {

					continue;
				}
				// This loop can be run either at FocusableContainer init time,
				// or later when we need to navigate upon pressing an arrow key.
				// When we're navigating, we have to know exactly if the child is
				// focusable or not, hence only rendered children will make the cut.
				// At the init time item.isFocusable() may return false incorrectly
				// just because the item has not been rendered yet and its focusEl
				// is not defined, so we don't bother to call isFocusable and return
				// the first potentially focusable child.
				if (beforeRender || (item.isFocusable && item.isFocusable())) {
					return item;
				}
			}
			return null;
		}
	}
});


/**
 * Extend interface to be compatible with Ext.data.Store 
 * Used in AbstractDc
 */
Ext.override(Ext.data.BufferedStore, {

	getAllNewRecords : function() {
		return this.getNewRecords();
	},
	
	loadData : function() {
		// loadData in our framework is used to replace the existing data in the store usual with nothing
		// for the buffered store this is not possible, so, simply just clear the data and do nothing else
		this.clearData();
	}
});


Ext.override(Ext.grid.plugin.BufferedRenderer, {

	// SONE-5163 : getFirstVisibleRowIndex function enter into an infinite loop, which cause problem on the frame
	// this infinite loop is fixed here
	getFirstVisibleRowIndex: function(startRow, endRow, viewportTop, viewportBottom) {
	    var me = this,
	        view = me.view,
	        rows = view.all,
	        elements = rows.elements,
	        clientHeight = me.viewClientHeight,
	        target, targetTop,
	        bodyTop = me.bodyTop;
	    // If variableRowHeight, we have to search for the first row who's bottom edge is within the viewport
	    if (rows.getCount() && me.variableRowHeight) {
	        if (!arguments.length) {
	            startRow = rows.startIndex;
	            endRow = rows.endIndex;
	            viewportTop = me.scrollTop;
	            viewportBottom = viewportTop + clientHeight;
	            // Teleported so that body is outside viewport: Use rowHeight calculation
	            if (bodyTop > viewportBottom || bodyTop + me.bodyHeight < viewportTop) {
	                me.teleported = true;
	                return Math.floor(me.scrollTop / me.rowHeight);
	            }
	            // In first, non-recursive call, begin targeting the most likely first row
	            target = startRow + Math.min(me.numFromEdge + ((me.lastScrollDirection === -1) ? me.leadingBufferZone : me.trailingBufferZone), Math.floor((endRow - startRow) / 2));
	        } else {
	            if (startRow === endRow) {
	                return endRow;
	            }
	            target = startRow + Math.floor((endRow - startRow) / 2);
	        }
	        targetTop = bodyTop + elements[target].offsetTop;
	        // If target is entirely above the viewport, chop downwards
	        // FP: Fix the infinite loop: complete the condition with: && (target+1)!==startRow
	        if (targetTop + elements[target].offsetHeight <= viewportTop && (target+1)!==startRow) {
	            return me.getFirstVisibleRowIndex(target + 1, endRow, viewportTop, viewportBottom);
	        }
	        // Target is first
	        if (targetTop <= viewportTop) {
	            return target;
	        }
	        // Not narrowed down to 1 yet; chop upwards
	        else if (target !== startRow) {
	            return me.getFirstVisibleRowIndex(startRow, target - 1, viewportTop, viewportBottom);
	        }
	    }
	    return Math.floor(me.scrollTop / me.rowHeight);
	},

	doRefreshView: function(range, startIndex) {
	    var me = this,
	        view = me.view,
	        scroller = me.scroller,
	        rows = view.all,
	        previousStartIndex = rows.startIndex,
	        previousEndIndex = rows.endIndex,
	        previousFirstItem, previousLastItem,
	        prevRowCount = rows.getCount(),
	        viewMoved = startIndex !== rows.startIndex && !me.isStoreLoading,
	        calculatedTop = -1,
	        scrollIncrement, restoreFocus;
	    me.isStoreLoading = false;
	    // So that listeners to the itemremove events know that its because of a refresh.
	    // And so that this class's refresh listener knows to ignore it.
	    view.refreshing = me.refreshing = true;
	    if (view.refreshCounter) {
	        // Give CellEditors or other transient in-cell items a chance to get out of the way.
	        if (view.hasListeners.beforerefresh && view.fireEvent('beforerefresh', view) === false) {
	            return view.refreshNeeded = view.refreshing = me.refreshing = false;
	        }
	        // If focus was in any way in the view, whether actionable or navigable, this will return
	        // a function which will restore that state.
	        restoreFocus = view.saveFocusState();
	        view.clearViewEl(true);
	        view.refreshCounter++;
	        if (range.length) {
	            view.doAdd(range, startIndex);
	            if (viewMoved) {
	                // Try to find overlap between newly rendered block and old block
	                previousFirstItem = rows.item(previousStartIndex, true);
	                previousLastItem = rows.item(previousEndIndex, true);
	                // Work out where to move the view top if there is overlap
	                if (previousFirstItem) {
	                    scrollIncrement = -previousFirstItem.offsetTop;
	                } else if (previousLastItem) {
	                    scrollIncrement = rows.last(true).offsetTop - previousLastItem.offsetTop;
	                }
	                // If there was an overlap, we know exactly where to move the view
	                if (scrollIncrement) {
	                    calculatedTop = Math.max(me.bodyTop + scrollIncrement, 0);
	                    me.scrollTop = calculatedTop ? me.scrollTop + scrollIncrement : 0;
	                } else // No overlap: calculate the a new body top and scrollTop.
	                {
	                    calculatedTop = startIndex * me.rowHeight;
	                    me.scrollTop = Math.max(calculatedTop + me.rowHeight * (calculatedTop < me.bodyTop ? me.leadingBufferZone : me.trailingBufferZone), 0);
	                }
	            }
	        } else // Clearing the view.
	        // Ensure we jump to top.
	        // Apply empty text.
	        {
	            me.scrollTop = calculatedTop = me.position = 0;
	            view.addEmptyText();
	        }
	        // Keep scroll and rendered block positions synched if there is scrolling.
	        if (scroller && calculatedTop !== -1) {
	            me.setBodyTop(calculatedTop);
	            scroller.suspendEvent('scroll');
	            scroller.scrollTo(null, me.position = me.scrollTop);
	            scroller.resumeEvent('scroll');
	        }
	        // Correct scroll range
	        me.refreshSize();
	        view.refreshSize(rows.getCount() !== prevRowCount);
	        view.fireItemMutationEvent('refresh', view, range);
	        // If focus was in any way in this view, this will restore it
	        restoreFocus();
	        view.headerCt.setSortState();
	    } else {
	        view.refresh();
	    }
	    // If there are columns to trigger rendering, and the rendered block os not either the view size
	    // or, if store count less than view size, the store count, then there's a bug.
	    if (view.getVisibleColumnManager().getColumns().length && rows.getCount() !== Math.min(me.store.getCount(), me.viewSize)) {
	        // do not raise error: Ext.raise('rendered block refreshed at ' + rows.getCount() + ' rows while BufferedRenderer view size is ' + me.viewSize);
	    	// do the refresh later; this bug appears in Ext 6.5.0, in 6.2.1 it works fine
	        // SONE-5687 : Workflow/ External reports
	    	me.view.on("resize", function(){
	    		this.refreshView(0); 
	    	}, me, {single: true});
	    }
	    view.refreshNeeded = view.refreshing = me.refreshing = false;
	},

    getThemeRowHeight: function() {
        var me = this,
            testEl;
        if (!me.themeRowHeight) {
            testEl = Ext.getBody().createChild({
                cls: Ext.baseCSSPrefix + 'theme-row-height-el'
            });
            me.self.prototype.themeRowHeight = testEl.dom.offsetHeight;
            testEl.destroy();
            // if 0 is returned infinite view size will be set
            if( me.self.prototype.themeRowHeight < 10 ){
            	me.self.prototype.themeRowHeight = 25;
            }
        }
        return me.themeRowHeight;
    }
});


Ext.override(Ext.Ajax,{
	
	request: function(options){
		if( options ){
			var originalFn = options.failure;
			options.failure = function(response){
				if( Ext.isFunction(originalFn) ){
					originalFn.apply(this, arguments)
				}
				try{
					getApplication().doReloadPageOrLockSession(response);
				}catch(e){
					Main.error(Main.translate("msg", "error_occured")+e);
				}
			};
		}
		return this.callParent([options]);
	} 
});


Ext.override(Ext.Base,{
	getConfig: function(name, peek, ifInitialized){
		var ret = {};
		try{
			ret = this.callParent(arguments);
		}catch(e){
			try{
				ret = this.getCurrentConfig();
			}catch(e2){ 
				// nothing to do
			}
		}
		return ret;
	}
});

Ext.override(Ext.dom.Helper, function() {
    var afterbegin = 'afterbegin',
        afterend = 'afterend',
        beforebegin = 'beforebegin',
        beforeend = 'beforeend',
        bbValues = [
            'BeforeBegin',
            'previousSibling'
        ],
        aeValues = [
            'AfterEnd',
            'nextSibling'
        ],
        bb_ae_PositionHash = {
            beforebegin: bbValues,
            afterend: aeValues
        },
        fullPositionHash = {
            beforebegin: bbValues,
            afterend: aeValues,
            afterbegin: [
                'AfterBegin',
                'firstChild'
            ],
            beforeend: [
                'BeforeEnd',
                'lastChild'
            ]
        };
    return {
        singleton: true,
        alternateClassName: [
            'Ext.DomHelper',
            'Ext.core.DomHelper'
        ],
        emptyTags: /^(?:br|frame|hr|img|input|link|meta|range|spacer|wbr|area|param|col)$/i,
        confRe: /^(?:tag|children|cn|html|tpl|tplData)$/i,
        endRe: /end/i,
        // Since cls & for are reserved words, we need to transform them
        attributeTransform: {
            cls: 'class',
            htmlFor: 'for'
        },
        closeTags: {},
        detachedDiv: document.createElement('div'),
        decamelizeName: function() {
            var camelCaseRe = /([a-z])([A-Z])/g,
                cache = {};
            function decamel(match, p1, p2) {
                return p1 + '-' + p2.toLowerCase();
            }
            return function(s) {
                return cache[s] || (cache[s] = s.replace(camelCaseRe, decamel));
            };
        }(),
        generateMarkup: function(spec, buffer) {
            var me = this,
                specType = typeof spec,
                attr, val, tag, i, closeTags;
            if (specType === "string" || specType === "number") {
                buffer.push(spec);
            } else if (Ext.isArray(spec)) {
                for (i = 0; i < spec.length; i++) {
                    if (spec[i]) {
                        me.generateMarkup(spec[i], buffer);
                    }
                }
            } else {
                tag = spec.tag || 'div';
                buffer.push('<', tag);
                for (attr in spec) {
                    if (spec.hasOwnProperty(attr)) {
                        val = spec[attr];
                        if (val !== undefined && !me.confRe.test(attr)) {
                            if (val && val.join) {
                                val = val.join(' ');
                            }
                            if (typeof val === "object") {
                                buffer.push(' ', attr, '="');
                                me.generateStyles(val, buffer, true).push('"');
                            } else {
                                buffer.push(' ', me.attributeTransform[attr] || attr, '="', val, '"');
                            }
                        }
                    }
                }
                // Now either just close the tag or try to add children and close the tag.
                if (me.emptyTags.test(tag)) {
                    buffer.push('/>');
                } else {
                    buffer.push('>');
                    // Apply the tpl html, and cn specifications
                    if ((val = spec.tpl)) {
                        val.applyOut(spec.tplData, buffer);
                    }
                    if ((val = spec.html)) {
                        buffer.push(val);
                    }
                    if ((val = spec.cn || spec.children)) {
                        me.generateMarkup(val, buffer);
                    }
                    // we generate a lot of close tags, so cache them rather than push 3 parts
                    closeTags = me.closeTags;
                    buffer.push(closeTags[tag] || (closeTags[tag] = '</' + tag + '>'));
                }
            }
            return buffer;
        },
        /**
         * Converts the styles from the given object to text. The styles are CSS style names
         * with their associated value.
         * 
         * The basic form of this method returns a string:
         * 
         *      var s = Ext.DomHelper.generateStyles({
         *          backgroundColor: 'red'
         *      });
         *      
         *      // s = 'background-color:red;'
         *
         * Alternatively, this method can append to an output array.
         * 
         *      var buf = [];
         *
         *      ...
         *
         *      Ext.DomHelper.generateStyles({
         *          backgroundColor: 'red'
         *      }, buf);
         *
         * In this case, the style text is pushed on to the array and the array is returned.
         * 
         * @param {Object} styles The object describing the styles.
         * @param {String[]} [buffer] The output buffer.
         * @param {Boolean} [encode] `true` to {@link Ext.String#htmlEncode} property values if they
         * are going to be inserted as HTML attributes.
         * @return {String/String[]} If buffer is passed, it is returned. Otherwise the style
         * string is returned.
         */
        generateStyles: function(styles, buffer, encode) {
            var a = buffer || [],
                name, val;
            for (name in styles) {
                if (styles.hasOwnProperty(name)) {
                    val = styles[name];
                    // Since a majority of attributes won't have html characters (basically
                    // restricted to fonts), we'll check first before we try and encode it
                    // because it's less expensive and this method gets called a lot.
                    name = this.decamelizeName(name);
                    if (encode && Ext.String.hasHtmlCharacters(val)) {
                        val = Ext.String.htmlEncode(val);
                    }
                    a.push(name, ':', val, ';');
                }
            }
            return buffer || a.join('');
        },
        /**
         * Returns the markup for the passed Element(s) config.
         * @param {Object} spec The DOM object spec (and children).
         * @return {String}
         */
        markup: function(spec) {
            if (typeof spec === "string") {
                return spec;
            }
            var buf = this.generateMarkup(spec, []);
            return buf.join('');
        },
        /**
         * Applies a style specification to an element.
         * 
         * Styles in object form should be a valid DOM element style property.  
         * [Valid style property names](http://www.w3schools.com/jsref/dom_obj_style.asp) 
         * (_along with the supported CSS version for each_)
         * 
         *     // <div id="my-el">Phineas Flynn</div>
         *     
         *     var el = Ext.get('my-el'),
         *         dh = Ext.dom.Helper;
         *     
         *     dh.applyStyles(el, 'color: white;');
         *     
         *     dh.applyStyles(el, {
         *         fontWeight: 'bold',
         *         backgroundColor: 'gray',
         *         padding: '10px'
         *     });
         *     
         *     dh.applyStyles(el, function () {
         *         if (name.initialConfig.html === 'Phineas Flynn') {
         *             return 'font-style: italic;';
         *             // OR return { fontStyle: 'italic' };
         *         }
         *     });
         * 
         * @param {String/HTMLElement/Ext.dom.Element} el The element to apply styles to
         * @param {String/Object/Function} styles A style specification string e.g. 'width:100px', or object in the form {width:'100px'}, or
         * a function which returns such a specification.
         */
        applyStyles: function(el, styles) {
            Ext.fly(el).applyStyles(styles);
        },
        /**
         * @private
         * Fix for browsers which do not support createContextualFragment
         */
        createContextualFragment: function(html) {
            var div = this.detachedDiv,
                fragment = document.createDocumentFragment(),
                length, childNodes;
            div.innerHTML = html;
            childNodes = div.childNodes;
            length = childNodes.length;
            // Move nodes into fragment, don't clone: http://jsperf.com/create-fragment
            while (length--) {
                fragment.appendChild(childNodes[0]);
            }
            return fragment;
        },
        /**
         * Creates new DOM element(s) without inserting them to the document.
         * @param {Object/String} o The DOM object spec (and children) or raw HTML blob
         * @param {HTMLElement} parentNode
         * @return {HTMLElement} The new un-inserted node
         */
        createDom: function(o, parentNode) {
            var me = this,
                markup = me.markup(o),
                div = me.detachedDiv,
                child;
            div.innerHTML = markup;
            child = div.firstChild;
            // Important to clone the node here, IE8 & 9 have an issue where the markup
            // in the first element will be lost.
            // var ct = document.createElement('div'),
            //     a, b;
            //     ct.innerHTML = '<div>markup1</div>';
            //     a = ct.firstChild;
            //     ct.innerHTML = '<div>markup2</div>';
            //     b = ct.firstChild;
            //     console.log(a.innerHTML, b.innerHTML);
            return Ext.supports.ChildContentClearedWhenSettingInnerHTML ? child.cloneNode(true) : child;
        },
        /**
         * Inserts an HTML fragment into the DOM.
         * @param {String} where Where to insert the html in relation to el - beforeBegin, afterBegin, beforeEnd, afterEnd.
         *
         * For example take the following HTML: `<div>Contents</div>`
         *
         * Using different `where` values inserts element to the following places:
         *
         * - beforeBegin: `<HERE><div>Contents</div>`
         * - afterBegin: `<div><HERE>Contents</div>`
         * - beforeEnd: `<div>Contents<HERE></div>`
         * - afterEnd: `<div>Contents</div><HERE>`
         *
         * @param {HTMLElement/TextNode} el The context element
         * @param {String} html The HTML fragment
         * @return {HTMLElement} The new node
         */
        insertHtml: function(where, el, html) {
            var me = this,
                hashVal, range, rangeEl, setStart, frag;
            where = where.toLowerCase();
            // Has fast HTML insertion into existing DOM: http://www.w3.org/TR/html5/apis-in-html-documents.html#insertadjacenthtml
            if (el && el.insertAdjacentHTML) {
                if (me.ieInsertHtml) {
                    // hook for IE table hack - impl in ext package override
                    frag = me.ieInsertHtml(where, el, html);
                    if (frag) {
                        return frag;
                    }
                }
                hashVal = fullPositionHash[where];
                if (hashVal) {
                    el.insertAdjacentHTML(hashVal[0], html);
                    return el[hashVal[1]];
                }
            } else // if (not IE and context element is an HTMLElement) or TextNode
            {
                // we cannot insert anything inside a textnode so...
                if (el.nodeType === 3) {
                    where = where === afterbegin ? beforebegin : where;
                    where = where === beforeend ? afterend : where;
                }
                range = Ext.supports.CreateContextualFragment ? el.ownerDocument.createRange() : undefined;
                setStart = 'setStart' + (this.endRe.test(where) ? 'After' : 'Before');
                if (bb_ae_PositionHash[where]) {
                    if (range) {
                        range[setStart](el);
                        frag = range.createContextualFragment(html);
                    } else {
                        frag = this.createContextualFragment(html);
                    }
                    el.parentNode.insertBefore(frag, where === beforebegin ? el : el.nextSibling);
                    return el[(where === beforebegin ? 'previous' : 'next') + 'Sibling'];
                } else {
                    rangeEl = (where === afterbegin ? 'first' : 'last') + 'Child';
                    if (el.firstChild) {
                        if (range) {
                            // Creating ranges on a hidden element throws an error, checking for
                            // visibility is expensive, so we'll catch the error and fall back to
                            // using the full fragment
                            try {
                                range[setStart](el[rangeEl]);
                                frag = range.createContextualFragment(html);
                            } catch (e) {
                                frag = this.createContextualFragment(html);
                            }
                        } else {
                            frag = this.createContextualFragment(html);
                        }
                        if (where === afterbegin) {
                            el.insertBefore(frag, el.firstChild);
                        } else {
                            el.appendChild(frag);
                        }
                    } else {
                        el.innerHTML = html;
                    }
                    return el[rangeEl];
                }
            }
            Ext.raise({
                sourceClass: 'Ext.DomHelper',
                sourceMethod: 'insertHtml',
                htmlToInsert: html,
                targetElement: el,
                msg: 'Illegal insertion point reached: "' + where + '"'
            });
        },
        /**
         * Creates new DOM element(s) and inserts them before el.
         * @param {String/HTMLElement/Ext.dom.Element} el The context element
         * @param {Object/String} o The DOM object spec (and children) or raw HTML blob
         * @param {Boolean} [returnElement] true to return a Ext.Element
         * @return {HTMLElement/Ext.dom.Element} The new node
         */
        insertBefore: function(el, o, returnElement) {
            return this.doInsert(el, o, returnElement, beforebegin);
        },
        /**
         * Creates new DOM element(s) and inserts them after el.
         * @param {String/HTMLElement/Ext.dom.Element} el The context element
         * @param {Object} o The DOM object spec (and children)
         * @param {Boolean} [returnElement] true to return a Ext.Element
         * @return {HTMLElement/Ext.dom.Element} The new node
         */
        insertAfter: function(el, o, returnElement) {
            return this.doInsert(el, o, returnElement, afterend);
        },
        /**
         * Creates new DOM element(s) and inserts them as the first child of el.
         * @param {String/HTMLElement/Ext.dom.Element} el The context element
         * @param {Object/String} o The DOM object spec (and children) or raw HTML blob
         * @param {Boolean} [returnElement] true to return a Ext.Element
         * @return {HTMLElement/Ext.dom.Element} The new node
         */
        insertFirst: function(el, o, returnElement) {
            return this.doInsert(el, o, returnElement, afterbegin);
        },
        /**
         * Creates new DOM element(s) and appends them to el.
         * @param {String/HTMLElement/Ext.dom.Element} el The context element
         * @param {Object/String} o The DOM object spec (and children) or raw HTML blob
         * @param {Boolean} [returnElement] true to return a Ext.Element
         * @return {HTMLElement/Ext.dom.Element} The new node
         */
        append: function(el, o, returnElement) {
            return this.doInsert(el, o, returnElement, beforeend);
        },
        /**
         * Creates new DOM element(s) and overwrites the contents of el with them.
         * @param {String/HTMLElement/Ext.dom.Element} el The context element
         * @param {Object/String} html The DOM object spec (and children) or raw HTML blob
         * @param {Boolean} [returnElement=false] true to return an Ext.Element
         * @return {HTMLElement/Ext.dom.Element} The new node
         */
        overwrite: function(el, html, returnElement) {
            var me = this,
                newNode;
            el = Ext.getDom(el);
            html = me.markup(html);
            if (me.ieOverwrite) {
                // hook for IE table hack - impl in ext package override
                newNode = me.ieOverwrite(el, html);
            }
            if (!newNode) {
                el.innerHTML = html;
                newNode = el.firstChild;
            }
            return returnElement ? Ext.get(newNode) : newNode;
        },
        doInsert: function(el, o, returnElement, where) {
            var me = this,
                newNode;
            el = el.dom || Ext.getDom(el);
            if ('innerHTML' in el) {
                // regular dom node
                // For standard HTMLElements, we insert as innerHTML instead of
                // createElement/appenChild because it is much faster in all versions of
                // IE: https://fiddle.sencha.com/#fiddle/tj
                newNode = me.insertHtml(where, el, me.markup(o));
            } else {
                // document fragment does not support innerHTML
                newNode = me.createDom(o, null);
                // we cannot insert anything inside a textnode so...
                if (el.nodeType === 3) {
                    where = where === afterbegin ? beforebegin : where;
                    where = where === beforeend ? afterend : where;
                }
                if (bb_ae_PositionHash[where]) {
                    el.parentNode.insertBefore(newNode, where === beforebegin ? el : el.nextSibling);
                } else if (el.firstChild && where === afterbegin) {
                    el.insertBefore(newNode, el.firstChild);
                } else {
                    el.appendChild(newNode);
                }
            }
            return returnElement ? Ext.get(newNode) : newNode;
        },
        /**
         * Creates a new Ext.Template from the DOM object spec.
         * @param {Object} o The DOM object spec (and children)
         * @return {Ext.Template} The new template
         */
        createTemplate: function(o) {
            var html = this.markup(o);
            return new Ext.Template(html);
        },
        /**
         * @method createHtml
         * Alias for {@link #markup}.
         * @deprecated 5.0.0 Please use {@link #markup} instead.
         */
        createHtml: function(spec) {
            return this.markup(spec);
        }
    };
});

// fixes for ExtJs 6.6.0
Ext.override(Ext.chart.legend.SpriteLegend, {
	xtype: 'sprite',
	
	isXType: function(xtype) {
		return xtype === 'sprite';
	}
});
