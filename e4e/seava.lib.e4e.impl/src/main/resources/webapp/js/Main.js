/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

/* ========================== GLOBALS =========================== */

__application__ = null;
if (typeof Constants === 'undefined') {
	Constants = {
		DATA_FORMAT_CSV : "csv",
		DATA_FORMAT_HTML : "html",
		DATA_FORMAT_JSON : "json",
		DATA_FORMAT_XML : "xml",
		DATA_FORMAT_PDF : "pdf",
		
		/**
		 * Request parameter names
		 */
		REQUEST_PARAM_THEME : "theme",
		REQUEST_PARAM_LANG : "lang",
		REQUEST_PARAM_ACTION : "action",
		REQUEST_PARAM_DATA : "data",
		REQUEST_PARAM_FILTER : "data",
		REQUEST_PARAM_FILTER_ATTRIBUTES : "filterAttr",
		REQUEST_PARAM_ADVANCED_FILTER : "filter",
		REQUEST_PARAM_PARAMS : "params",
		REQUEST_PARAM_SORT : "orderByCol",
		REQUEST_PARAM_SENSE : "orderBySense",
		REQUEST_PARAM_START : "resultStart",
		REQUEST_PARAM_SIZE : "resultSize",
		REQUEST_PARAM_ORDERBY : "orderBy",
		REQUEST_PARAM_SERVICE_NAME_PARAM : "rpcName",
		REQUEST_PARAM_EXPORT_INFO : "export_info",
		REQUEST_PARAM_REPORT_CODE : "code",
		REQUEST_PARAM_EXCLUDE_FIELDS : "exclude_fields", 
		REQUEST_PARAM_EXPORT_DOWNLOAD : "download",
		REQUEST_PARAM_DATA_FORMAT : "dataFormat",
		
		
		/**
		 * Data-source actions
		 */
		DS_INFO : "info",
		DS_QUERY : "find",
		DS_INSERT : "insert",
		DS_UPDATE : "update",
		DS_DELETE : "delete",
		DS_SAVE : "save",
		DS_IMPORT : "import",
		DS_EXPORT : "export",
		DS_REPORT : "report",
		DS_PRINT : "print",
		DS_RPC : "rpc",
		DS_NOTIFY : "notify",
		ASGN_QUERY_LEFT : "findLeft",
		ASGN_QUERY_RIGHT : "findRight",
		ASGN_MOVE_LEFT : "moveLeft",
		ASGN_MOVE_RIGHT : "moveRight",
		ASGN_MOVE_LEFT_ALL : "moveLeftAll",
		ASGN_MOVE_RIGHT_ALL : "moveRightAll",
		ASGN_SETUP : "setup",
		ASGN_RESET : "reset",
		ASGN_SAVE : "save",
		ASGN_CLEANUP : "cleanup",

		/**
		 * Session/security actions
		 */
		SESSION_LOGIN : "doLogin",
		SESSION_LOGOUT : "doLogout",
		SESSION_LOCK : "doLock",
		SESSION_CHANGEPASSWORD : "changePassword",

		/**
		 * Format masks
		 */
		EXTJS_DATE_FORMAT : "Y-m-d",
		EXTJS_TIME_FORMAT : "H:i",
		EXTJS_DATETIME_FORMAT : "Y-m-d H:i",
		EXTJS_DATETIMESEC_FORMAT : "Y-m-d H:i:s",
		EXTJS_MONTH_FORMAT : "Y-m",
		EXTJS_MODEL_DATE_FORMAT : "Y-m-d H:i:s",
		EXTJS_ALT_FORMATS : "j|j-n|d|d-m",

		THOUSAND_SEPARATOR : ",",
		DECIMAL_SEPARATOR : ".",

		/**
		 * Cookies
		 */
		COOKIE_NAME_THEME : "app-theme",
		COOKIE_NAME_LANG : "app-lang",
			
		/**
		 * Settings 
		 */
		MAX_BUTTONS_ON_TOOLBARS : 7
	}
}

/**
 * Apply client specific values.
 */
Ext.apply(Constants, {

	/**
	 * The maximum number of open tabs
	 */
	MAX_OPEN_TABS : 15,
	/**
	 * Component ID for application level views
	 */
	CMP_ID : {
		APP_VIEW_HOME : "dnet-application-view-home",
		APP_VIEW_HEADER : "dnet-application-view-header",
		APP_VIEW_FOOTER : "dnet-application-view-footer",
		APP_VIEW_BODY : "dnet-application-view-body",
		/**
		 * ID of the tabPanel in which the frame is opened
		 */
		FRAME_TAB_PREFIX : "view-body-tab-",
		/**
		 * ID of the HTML iframe in which the frame is opened in the tab-panel
		 */
		FRAME_IFRAME_PREFIX : "view-body-iframe-"
	},

	/**
	 * Customizations
	 */
	SHOW_VIEW_AND_FILTER_TOOLBAR : true,
	REQUEST_PARAM_DIALOG_REPORT_INFO : "dialogExportInfo"
});
/**
 * Date-time format mask names.
 * 
 * @type
 */
Masks = {
	DATE : "DATE_FORMAT",
	TIME : "TIME_FORMAT",
	DATETIME : "DATETIME_FORMAT",
	DATETIMESEC : "DATETIMESEC_FORMAT",
	MONTH : "MONTH_FORMAT"
}

/**
 * Main configuration class.
 */
Ext.ns("e4e");
Main = {

	productInfo : {
		name : "extjs4erp",
		description : "Framework for line of business applications with Extjs GUI",
		vendor : "Atraxo Consulting",
		url : "www.atraxo.com",
		version : "0.0.0"
	},

	/**
	 * Namespace aliases to be used in class declarations
	 * 
	 * @type
	 */
	ns : {},

	/**
	 * Application bundles which contain extjs based user interface components.
	 * 
	 * @type
	 */
	bundle : {},

	dsName : {
		VIEW_STATE_LOV : "ad_ViewStateRtLov_Ds",
		VIEW_STATE : "ad_ViewState_Ds",
		MENU : "ad_MenuRtLov_Ds",
		MENU_ITEM : "ad_MenuItemRtLov_Ds",
		COMPANY_LOV : "ad_OrgLov_Ds",
		DS_LOV : "ad_DataSourceLov_Ds"
	},

	msg : {

		AT_FIRST_RECORD : "AT_FIRST_RECORD",
		AT_LAST_RECORD : "AT_LAST_RECORD",

		AT_FIRST_PAGE : "AT_FIRST_PAGE",
		AT_LAST_PAGE : "AT_LAST_PAGE",

		DCCONTEXT_INVALID_SETUP : "DCCONTEXT_INVALID_SETUP",

		PARENT_RECORD_NEW : "PARENT_RECORD_NEW",
		NO_CURRENT_RECORD : "NO_CURRENT_RECORD",
		NO_SELECTED_RECORDS : "NO_SELECTED_RECORDS",
		DIRTY_DATA_FOUND : "DIRTY_DATA_FOUND",

		INVALID_FILTER : "INVALID_FILTER",
		INVALID_FORM : "INVALID_FORM",

		DC_ACTION_NOT_ALLOWED : "Action not allowed in current context",
		DC_NEW_NOT_ALLOWED : "DC_NEW_NOT_ALLOWED",
		DC_SAVE_NOT_ALLOWED : "DC_SAVE_NOT_ALLOWED",
		DC_NOTHING_TO_SAVE : "DC_NOTHING_TO_SAVE",
		DC_QUERY_NOT_ALLOWED : "DC_QUERY_NOT_ALLOWED",
		DC_COPY_NOT_ALLOWED : "DC_COPY_NOT_ALLOWED",
		DC_DELETE_NOT_ALLOWED : "DC_DELETE_NOT_ALLOWED",
		DC_EDIT_OUT_NOT_ALLOWED : "DC_EDIT_OUT_NOT_ALLOWED",
		DC_RECORD_CHANGE_NOT_ALLOWED : "DC_RECORD_CHANGE_NOT_ALLOWED",
		DC_RELOAD_RECORD_NOT_ALLOWED : "DC_RELOAD_RECORD_NOT_ALLOWED"

	},

	/**
	 * Default timeout for transactional requests
	 */
	ajaxTimeout : 1000 * 60 * 30,

	/**
	 * Application logo url.
	 * 
	 * @type String
	 */
	logo : "",

	// =======================================
	// Base URLs amd fragments
	// These are provided by the server when constructing the main entry point
	// html file.
	// =======================================

	/**
	 * Host url, protocol://domain:port
	 * 
	 * @type String
	 */
	urlHost : null,

	/**
	 * URL for data-source transactional requests.
	 * 
	 * @type String
	 */
	urlDs : null,
	
	/**
	 * URL for external report controller.
	 * 
	 * @type String
	 */
	urlExtRep : null,

	/**
	 * URL for assignment transactional requests.
	 * 
	 * @type String
	 */
	urlAsgn : null,

	/**
	 * URL for workflow requests.
	 * 
	 * @type String
	 */
	urlWorkflow : null,

	/**
	 * URL for Extjs user interface.
	 * 
	 * @type String
	 */
	urlUiExtjs : null,

	/**
	 * URL for session related requests
	 * 
	 * @type String
	 */
	urlSession : null,

	/**
	 * URL for file upload.
	 * 
	 * @type String
	 */
	urlUpload : null,

	/**
	 * URL for file upload.
	 * 
	 * @type String
	 */
	urlDownload : null,

	/**
	 * URL where the static resources are served from for the core framework
	 * 
	 * @type String
	 */
	urlStaticCore : null,

	/**
	 * URL where the static resources are served from for the application
	 * modules
	 * 
	 * @type String
	 */
	urlStaticModules : null,

	/**
	 * URL where the translation files are served from for the core framework
	 * 
	 * @type String
	 */
	urlStaticCoreI18n : null,

	/**
	 * Helper tag for the location of the application modules static resources
	 * 
	 * @type String
	 */
	urlStaticModuleSubpath : null,
	
	/**
	 * URL where the application help files are served from for the core framework
	 * 
	 * @type String
	 */
	urlHelp : "",

	/**
	 * Include bundle when build component url ?
	 * 
	 * @type String
	 */
	urlStaticModuleUseBundle : false,

	/**
	 * configuration variables
	 * 
	 * @type Object
	 */
	config : {

	},

	/**
	 * Default date and time formats. They are overwritten according to the user
	 * locale settings.
	 */
	/**
	 * Format for year + month + day
	 * 
	 * @type String
	 */
	DATE_FORMAT : Constants.EXTJS_DATE_FORMAT,
	/**
	 * Format for hour + minutes
	 * 
	 * @type String
	 */
	TIME_FORMAT : Constants.EXTJS_TIME_FORMAT,

	/**
	 * Format for year + month + day + hour + minutes
	 * 
	 * @type String
	 */
	DATETIME_FORMAT : Constants.EXTJS_DATETIME_FORMAT,

	/**
	 * Format for year + month + day + hour + minutes + seconds
	 * 
	 * @type String
	 */
	DATETIMESEC_FORMAT : Constants.EXTJS_DATETIMESEC_FORMAT,

	/**
	 * Format for year + month
	 * 
	 * @type String
	 */
	MONTH_FORMAT : Constants.EXTJS_MONTH_FORMAT,

	/**
	 * Format for communication with server.
	 * 
	 * @type String
	 */
	MODEL_DATE_FORMAT : Constants.EXTJS_MODEL_DATE_FORMAT,

	/**
	 * Alternative formats to be used in a date/time field to create a date from
	 * the user input
	 * 
	 * @type String
	 */
	DATE_ALTFORMATS : Constants.EXTJS_ALT_FORMATS,

	THOUSAND_SEP : Constants.THOUSAND_SEPARATOR,
	DECIMAL_SEP : Constants.DECIMAL_SEPARATOR,
	
	MAX_BUTTONS_ON_TOOLBARS : Constants.MAX_BUTTONS_ON_TOOLBARS,
	
	numberFormats : null,

	DEFAULT_THEME : "gray",
	DEFAULT_LANGUAGE : "en",
	
	navigationTreeMenus : null,

	navigationTopMenus : null,
	
	notificationQueue: [],
	
	hideNotificationTimer: 5000,

	/**
	 * Various global view configuration defaults
	 * 
	 * @type Object
	 */
	viewConfig : {
		BOOLEAN_COL_WIDTH : 60,
		DATE_COL_WIDTH : 80,
		DISABLE_AS_READONLY : true,
		USE_TOOLBAR_ICONS : true,
		USE_BUTTON_ICONS : true,
		GRID_EDITOR_CHANGE_EVT_BUFFER : 350,
		FETCH_SIZE : 30,
		FETCH_SIZE_LOV : 50,
		FETCH_SIZE_ASGN : 150,
		FETCH_SIZE_UNLIMITED : 10000,
		FETCH_SIZE_LOV_NO_PAGINATOR : 300,
		FETCH_SIZE_CFG_OBJ : 250, // for queries like views and filters
		FORM_LABEL_SEPARATOR : '',
		FORM_LABEL_SEPARATOR_FOR_MANDATORY : '',
		SHOW_VIEW_AND_FILTER_TOOLBAR : Constants.SHOW_VIEW_AND_FILTER_TOOLBAR
	},

	/**
	 * Creates a set of number formats up to 9 decimals according to the user
	 * locale.
	 */
	initFormats : function() {
		Ext.util.Format.decimalSeparator = this.DECIMAL_SEP;
		Ext.util.Format.thousandSeparator = this.THOUSAND_SEP;
		this.numberFormats = new Ext.util.MixedCollection();
		var _fmt = "0,000";
		this.numberFormats.add(0, _fmt);
		_fmt = _fmt + ".";
		for (var i = 1; i <= 9; i++) {
			var fmt = _fmt + Ext.util.Format.leftPad("0", i, "0");
			this.numberFormats.add(i, fmt);
		}
	},

	/**
	 * Returns a number format string based on the provided decimals number and
	 * the user locale.
	 * 
	 * @param {}
	 *            decimals
	 * @return {}
	 */
	getNumberFormat : function(decimals) {
		if (this.numberFormats == null) {
			this.initFormats();
		}
		return this.numberFormats.get(decimals);
	},
	
	encodeHTML : function(xmlData) {
		return xmlData.replace(/</g, "&lt;").replace(/>/g, "&gt;");
	},
	
	/**
	 * Dan: add support for the notifications center
	 * 
	 */
	
	pushNotification : function(type, title, message, time, recordId, id, methodName, methodParam) {
		// do not display same notification twice
		var found = false;
		var n = this.notificationQueue.length;
		var i;
		for( i=0; i<n; i++ ){
			var o = this.notificationQueue[i];
			if( o.type === type && o.title === title && o.time === time && o.text === message ){
				found = true;
				break;
			}
		}
		if( !found ){ 
			var appMenu = getApplication().menu;
			var notificationCounter = document.getElementById(appMenu._idCmpNotificationCounter_);
			this.notificationQueue.push({
				id: id,
				recordId: recordId, 
				type: type,
				title : title,
				text: message,
				time: time,
				methodName: methodName,
				methodParam: methodParam
			});
			var counter = this.countNotifications();
			notificationCounter.style.display="block";
			notificationCounter.innerHTML = counter;
			
			appMenu._getNotifications_();
		}
	},
	
	countNotifications: function() {
		var notifications = this.notificationQueue;
		return notifications.length;
	},
	
	removeAllNotifications: function(clearQueue) {
		
		var notifications = this.notificationQueue;
		var appMenu = getApplication().menu;
		var notificationCounter = document.getElementById(appMenu._idCmpNotificationCounter_);
		var notificationContainer = document.getElementById(appMenu._idCmpNotificationContainer_);
		
		Ext.each(notifications, function(notification) {
			var cmp = Ext.getCmp(notification.id);
			notification.visible = false;
			if (cmp) {
				cmp.destroy();
			}
			
		}, this);
		if (clearQueue && clearQueue === true) {
			
			var ids = [];
			Ext.each(notifications, function(n) {
				var o = {
					id : n.recordId
				};
				ids.push(o);
			}, this);
			
			Ext.Ajax.request({
				url: Main.dsAPI("fmbas_Notification_Ds", "json").service,
	            method: "POST",
	            scope: this,
	            params : {
	        		rpcName: "clean",
	        		rpcType: "data",
					data : Ext.encode({
						userCode: getApplication().session.user.code
					})
				},
	            success: function() {
	            	this.notificationQueue = [];
	            	var appMenu = getApplication().menu;
	            	var newWidth = Ext.getBody().getViewSize().width;
	            	
	    			notificationCounter.style.display="none";
	    			var notificationSideBar = Ext.getCmp(appMenu._cmpNotificationSideBarId_);
	    			while (notificationContainer.hasChildNodes()) {
	    				notificationContainer.removeChild(notificationContainer.lastChild);
	    			}
	    			
	    			if (!Ext.isEmpty(notificationSideBar)) {
	    	    		notificationSideBar.getEl().dom.style.display="none";
	    	    		notificationSideBar.setXY([newWidth, 0]);
	    	    		Ext.getBody().unmask();
	    	    	}
	    			
	            }
			});
		}
	},
	
	showAllNotifications : function() {
		var notifications = this.notificationQueue, l = notifications.length;
		if (l > 0) {
			this.removeAllNotifications();
			this.notify();
		}
	},
	
	removeNotification : function(id , timer) {
		
		var cmp = Ext.getCmp(id);
		var hideFn = function(cmp) {
			if (cmp) {
				cmp.destroy();
			}
		}
		
		if (timer) {
			if (cmp) {
				Ext.defer(hideFn, this.hideNotificationTimer, this, [cmp]);
			}
		}
		else {
			hideFn(cmp);
		}
	},
	
	removeNotificationFromSideBar : function(id) { 
    	
    	// Remove notification from database
    	
    	Ext.Ajax.request({
            url: Main.dsAPI("fmbas_Notification_Ds", "json").destroy,
            method: "POST",
            scope: this,
            params: {
              data: Ext.JSON.encode({
                  userCode: getApplication().session.user.code
              })
            },
            success: function() {
            	var notifications = this.notificationQueue;
        		var appMenu = getApplication().menu;
        		var notificationCounter = document.getElementById(appMenu._idCmpNotificationCounter_);
        		var index = -1;
        		
            	for(var i = 0, len = notifications.length; i < len; i++) {
            	    if (notifications[i].id === id) {
            	        index = i;
            	        break;
            	    }
            	}
            	
            	if (index !== -1) {
            		notifications.splice(index, 1);
            	}
            	
            	// Remove notification from sidebar
            	
            	var el = document.getElementById(id+"-notification");
            	el.parentElement.removeChild(el);
            	
            	// Update the notification counter
            	
            	var counter = this.countNotifications();
            	notificationCounter.innerHTML = counter;
            	
            	if (counter === 0) {
            		notificationCounter.style.display="none";
            	}
            }
    	});
	},
	
	notify : function() {

		var cfg = {
		    modalWidth: 350,
		    modalHeight: 100,
		    modalY: 60
		}
		var notifications = this.notificationQueue;
		var alreadySeenCounter = 0;
		for(var x = 0, length = notifications.length; x < length; x++) {
    	    if (notifications[x].visible == true ) {
    	    	alreadySeenCounter++;
    	    }
    	}
		
		var cls = "";
		var glyph = "";
		var right = Ext.getBody().getViewSize().width;

		if (!Ext.isEmpty(this.notificationQueue)) {

		    Ext.each(notifications, function(notification) {

		    	var currentTime;
		    	var timeStamp = (new Date(notification.time)).getTime() > 0;
		    	if (timeStamp === true) {
		    		currentTime = new Date(notification.time);
		    	}
		    	else {
		    		var theTime = notification.time;
		    		currentTime = new Date(theTime.replace(" ","T"));
		    	}
		    	
				var hours = currentTime.getHours();
				var minutes = currentTime.getMinutes();
				var suffix = "AM";

				if (minutes < 10) {
					minutes = "0" + minutes;
				}

				if (hours >= 12) {
				    suffix = "PM";
				    hours = hours - 12;
				}
				if (hours === 0) {
				    hours = 12;
				}

		        if (notification.type === "Error") {
		            cls = "modal-error";
		            glyph = "<i class='fa fa-2x fa-exclamation-triangle'></i>";
		        } else if (notification.type === "Job") {
		            cls = "modal-success";
		            glyph = "<i class='fa fa-2x fa-check'></i>";
		        } else if (notification.type === "Activity") {
		            cls = "modal-activity";
		            glyph = "<i class='fa fa-2x fa-check'></i>";
		        } else if (notification.type === "Process") {
		            cls = "modal-process";
		            glyph = "<i class='fa fa-2x fa-check'></i>";
		        }

		        var boxHtml = ['<div class="sone-display-table">',
		            '<div class="sone-display-table-cell sone-modal-icon">',
		            '<div>' + glyph + '</div>',
		            '</div>',
		            '<div class="sone-display-table-cell sone-modal-body">',
		            '<div class="sone-display-table">',
		            '<div class="sone-display-table-cell">',
		            '<div class="sone-modal-title">',
		            notification.title,
		            '</div>',
		            '</div>',
		            '<div class="sone-display-table-cell sone-modal-time">',
		            hours + ":" + minutes + " " + suffix,
		            '</div>',
		            '</div>',
		            '<div class="sone-modal-text">',
		            notification.text,
		            '</div>',
		            '</div>',
		            '</div>'
		        ].join('\n');

		        var animate = function(ctx) {
		        	
	        		var el = Ext.getCmp(notification.id);
	        		if (!Ext.isEmpty(el)) {
	        			var index = -1;
	                	for(var i = 0, len = notifications.length; i < len; i++) {
	                		
	                	    if (notifications[i].id === el.getId()) {
	                	        index = i;
	                	        break;
	                	    }
	                	}
	                	
	                	var numItems = notifications.length - index;
	                	var newY = cfg.modalHeight*(numItems-1)+(cfg.modalY+20*(numItems-1));
	                	var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
                    	
                    	if (newY > height * 0.75) {
                    		Ext.getCmp(notification.id).hide();
                    	}
	                	
	                	el.setXY([right - cfg.modalWidth - 40, newY], {
	                        scope: this,
	                        easing: 'easeIn',
	                        callback: function() {
	                        	notification.visible = true;
	                        	ctx.removeNotification(notification.id, ctx.hideNotificationTimer, notification.recordId);
	                        }
	                    }, this);
		        	}
	        	}
		        
		        if (!notification.visible) {		        	
		        	Ext.create("Ext.panel.Panel", {
			            xtype: "panel",
			            id: notification.id,
			            html: boxHtml,
			            frame: false,
			            floating: true,
			            shadow: false,
			            style: 'cursor: pointer',
			            height: cfg.modalHeight,
			            width: cfg.modalWidth,
			            x: right,
			            y: cfg.modalY,
			            cls: !Ext.isEmpty(cls) ? "sone-notify-modal " + cls : "sone-notify-modal",
			            renderTo: Ext.getBody(),
			            listeners: {
			                afterrender: {
			                    scope: this,
			                    fn: function(el) {
			                    	animate(this);
			                    	el.getEl().on("click", function() {
			                    		el.destroy();
			                    	}, this);
			                    }
			                }
			            }
			        });
		        }
		        else {
		        	animate(this);
		        }
		        
		    }, this);
		}
	},

	/**
	 * Request data formats
	 * 
	 * @type Object
	 */
	dataFormat : {
		HTML : Constants.DATA_FORMAT_HTML,
		CSV : Constants.DATA_FORMAT_CSV,
		PDF : Constants.DATA_FORMAT_XML,
		XML : Constants.DATA_FORMAT_XML,
		JSON : Constants.DATA_FORMAT_JSON
	},

	/**
	 * Request parameter mappings. These values must be kept in sync with the
	 * mappings at server-side, thus they derive their values from the Constants
	 * sent by the server.
	 * 
	 * @type Object
	 */
	requestParam : {
		ACTION : Constants.REQUEST_PARAM_ACTION,
		DATA : Constants.REQUEST_PARAM_DATA,
		FILTER : Constants.REQUEST_PARAM_FILTER,
		FILTER_ATTRIBUTES : Constants.REQUEST_PARAM_FILTER_ATTRIBUTES,
		PARAMS : Constants.REQUEST_PARAM_PARAMS,
		ADVANCED_FILTER : Constants.REQUEST_PARAM_ADVANCED_FILTER,
		SORT : Constants.REQUEST_PARAM_SORT,
		SENSE : Constants.REQUEST_PARAM_SENSE,
		START : Constants.REQUEST_PARAM_START,
		SIZE : Constants.REQUEST_PARAM_SIZE,
		ORDERBY : Constants.REQUEST_PARAM_ORDERBY,
		SERVICE_NAME_PARAM : Constants.REQUEST_PARAM_SERVICE_NAME_PARAM,
		EXPORT_INFO : Constants.REQUEST_PARAM_EXPORT_INFO,
		REPORT_CODE : Constants.REQUEST_PARAM_REPORT_CODE,
		EXCLUDE_FIELDS : Constants.REQUEST_PARAM_EXCLUDE_FIELDS ,
		DIALOG_REPORT_INFO : Constants.REQUEST_PARAM_DIALOG_REPORT_INFO,
		DATA_FORMAT : Constants.REQUEST_PARAM_DATA_FORMAT
	},

	/**
	 * Request actions for ds components
	 * 
	 * @type Object
	 */
	dsAction : {
		INFO : Constants.DS_INFO,
		QUERY : Constants.DS_QUERY,
		INSERT : Constants.DS_INSERT,
		UPDATE : Constants.DS_UPDATE,
		DELETE : Constants.DS_DELETE,
		SAVE : Constants.DS_SAVE,
		IMPORT : Constants.DS_IMPORT,
		EXPORT : Constants.DS_EXPORT,
		REPORT : Constants.DS_REPORT,
		PRINT : Constants.DS_PRINT,
		RPC : Constants.DS_RPC,
		NOTIFY : Constants.DS_NOTIFY
	},

	/**
	 * Request actions for assignment components
	 * 
	 * @type Object
	 */
	asgnAction : {
		QUERY_LEFT : Constants.ASGN_QUERY_LEFT,
		QUERY_RIGHT : Constants.ASGN_QUERY_RIGHT,
		MOVE_LEFT : Constants.ASGN_MOVE_LEFT,
		MOVE_RIGHT : Constants.ASGN_MOVE_RIGHT,
		MOVE_LEFT_ALL : Constants.ASGN_MOVE_LEFT_ALL,
		MOVE_RIGHT_ALL : Constants.ASGN_MOVE_RIGHT_ALL,
		SAVE : Constants.ASGN_SAVE,
		SETUP : Constants.ASGN_SETUP,
		RESET : Constants.ASGN_RESET,
		CLEANUP : Constants.ASGN_CLEANUP
	},

	/**
	 * Request actions for user's session management
	 * 
	 * @type Object
	 */
	sessionAction : {
		LOGIN : Constants.SESSION_LOGIN,
		LOGOUT : Constants.SESSION_LOGOUT,
		LOCK : Constants.SESSION_LOCK,
		CHANGEPSWD : Constants.SESSION_CHANGEPASSWORD
	},

	/**
	 * Creates the URL to load a frame.
	 */
	buildUiPath : function(bundle, frame, isSpecial) {
		var _b = "";
		if (!Ext.isEmpty(bundle)) {
			_b = bundle + "/";
		}
		if (isSpecial) {
			return this.urlUiExtjs + "/spframe/" + _b + frame;
		} else {
			return this.urlUiExtjs + "/frame/" + _b + frame;
		}
	},

	/**
	 * URLs for session management related requests.
	 */
	sessionAPI : function() { // parameter: format
		return {
			login : this.urlSession + "?" + this.requestParam.ACTION + "="
					+ this.sessionAction.LOGIN,
			logout : this.urlSession + "/" + this.sessionAction.LOGOUT,
			lock : this.urlSession + "/" + this.sessionAction.LOCK,
			changePassword : this.urlSession + "?" + this.requestParam.ACTION
					+ "=" + this.sessionAction.CHANGEPSWD,
			userSettings : this.urlSession + "?action=userSettings"
		};
	},

	/**
	 * URLs for data-source (DS) related requests.
	 */
	dsAPI : function(resource, format) {
		return {
			info : this.urlDs + "/" + resource + "." + format + "?"
					+ this.requestParam.ACTION + "=" + this.dsAction.INFO,
			read : this.urlDs + "/" + resource + "." + format + "?"
					+ this.requestParam.ACTION + "=" + this.dsAction.QUERY,
			load : this.urlDs + "/" + resource + "." + format + "?"
					+ this.requestParam.ACTION + "=" + this.dsAction.QUERY,
			print : this.urlDs + "/" + resource + "." + format + "?"
					+ this.requestParam.ACTION + "=" + this.dsAction.PRINT,
			exportdata : this.urlDs + "/" + resource + "." + format
					+ "?action=" + this.dsAction.EXPORT,
			report : this.urlDs + "/" + resource + "." + format
					+ "?action=report",//+ this.dsAction.REPORT,
			reportDownload : this.urlDs + "/" + resource + "." + format
					+ "?action=downloadReport&reportId=",//+ this.dsAction.REPORT,
			create : this.urlDs + "/" + resource + "." + format + "?"
					+ this.requestParam.ACTION + "=" + this.dsAction.INSERT,
			update : this.urlDs + "/" + resource + "." + format + "?"
					+ this.requestParam.ACTION + "=" + this.dsAction.UPDATE,
			save : this.urlDs + "/" + resource + "." + format + "?"
					+ this.requestParam.ACTION + "=" + this.dsAction.SAVE,
			destroy : this.urlDs + "/" + resource + "." + format + "?"
					+ this.requestParam.ACTION + "=" + this.dsAction.DELETE,
			service : this.urlDs + "/" + resource + "." + format + "?"
					+ this.requestParam.ACTION + "=" + this.dsAction.RPC,
			dialogReport : this.urlDs + "/" + resource + "." + format
					+ "?action=dialogReport",
			notify : this.urlDs + "/" + resource + "." + format
					+ "?action=notify"
		};
	},
	
	externalReportsAPI : function(resource) {
		return {
			run : this.urlExtRep + "/" + resource,
			exists: this.urlExtRep + "/" + resource
		}
	},

	/**
	 * URLs for assignment components requests for the available elements
	 * (available to be assigned).
	 */
	asgnLeftAPI : function(resource, format) {
		return {
			read : this.urlAsgn + "/" + resource + "." + format + "?"
					+ this.requestParam.ACTION + "="
					+ this.asgnAction.QUERY_LEFT

		};
	},

	/**
	 * URLs for assignment components requests for the selected elements
	 * (already assigned).
	 */
	asgnRightAPI : function(resource, format) {
		return {
			read : this.urlAsgn + "/" + resource + "." + format + "?"
					+ this.requestParam.ACTION + "="
					+ this.asgnAction.QUERY_RIGHT
		};
	},

	/**
	 * URLs for data-source RPC type requests.
	 */
	rpcAPI : function(resource, fnName, format) {
		return this.rpcUrl + "/" + resource + "." + format + "?"
				+ this.requestParam.ACTION + "=" + fnName
	},

	/**
	 * Workflow api: Process definition
	 */
	wfProcessDefinitionAPI : function(processDefinitionId) {
		return {
			form : this.urlWorkflow + "/process-definition/"
					+ processDefinitionId + "/form",
			diagram : this.urlWorkflow + "/process-definition/"
					+ processDefinitionId + "/diagram",
			xml : this.urlWorkflow + "/process-definition/"
					+ processDefinitionId + "/xml",
			properties : this.urlWorkflow + "/process-definition/"
					+ processDefinitionId + "/properties"
		};
	},

	/**
	 * Workflow api: Process instance
	 */
	wfProcessInstanceAPI : function(processInstanceId) {
		return {
			start : this.urlWorkflow + "/process-instance/start",
			diagram : this.urlWorkflow + "/process-instance/"
					+ processInstanceId + "/diagram"
		};
	},

	/**
	 * Workflow api: Task management
	 */
	wfTaskAPI : function(taskId) {
		return {
			form : this.urlWorkflow + "/task/" + taskId + "/form",
			complete : this.urlWorkflow + "/task/" + taskId + "/complete",
			properties : this.urlWorkflow + "/task/" + taskId + "/properties"
		};
	},

	/**
	 * Workflow api: Deployment management
	 */
	wfDeploymentAPI : function() { // parameter: deploymentId
		return {
			destroy : this.urlWorkflow + "/deployment/delete"
		};
	},

	/**
	 * Translate a group/key pair from the translations pack. Optionally replace
	 * place holders with given values.
	 */
	translate : function(group, key, params, defValue) {
		if (dnet.Translation === undefined || dnet.Translation === null
				|| dnet.Translation[group] === undefined || dnet.Translation[group] === null) {
			return key;
		}
		var v = dnet.Translation[group][key] || defValue || key;
		if (Ext.isArray(params)) {
			for (var i = 0, len = params.length; i < len; i++) {
				v = v.replace("{" + i + "}", params[i]);
			}
		}
		return v;
	},

	/**
	 * Translation for a model field. Tries to find a translation in the model
	 * RB or in the shared translations
	 * 
	 * @param {}
	 *            mrb - model resource bundle
	 * @param {}
	 *            name - field name
	 * @return {}
	 */
	translateModelField : function(mrb, name) {
		if (mrb != null && mrb[name + "__lbl"]) {
			return mrb[name + "__lbl"];
		} else {
			return Main.translate("ds", name);
		}
	},

	/**
	 * Translation for a form field. Tries to find a translation in the view RB,
	 * model RB or in the shared translations
	 * 
	 * @param {}
	 *            vrb - view resource bundle
	 * @param {}
	 *            mrb - model resource bundle
	 * @param {}
	 *            item - the field
	 * @return {Boolean}
	 */

	translateField : function(vrb, mrb, item) {

		var trl = function(vrb, mrb, item, cfg, sfx, ignoreSharedResource){
			// check if the view has its own resource bundle
			if (vrb !== undefined && vrb !== null && vrb[item.name + sfx]) {
				item[cfg] = vrb[item.name + sfx];
				return true;
			}
	
			// try to translate it from the model"s resource bundle
			if (item.dataIndex !== undefined && item.dataIndex !== null && mrb !== null && mrb !== undefined
					&& mrb[item.dataIndex + sfx]) {
				item[cfg] = mrb[item.dataIndex + sfx];
				return true;
			}
			if (item.paramIndex !== undefined && item.paramIndex !== null && mrb != null && mrb != undefined
					&& mrb[item.paramIndex + sfx]) {
				item[cfg] = mrb[item.paramIndex + sfx];
				return true;
			}
	
			// for built-in ranged filter field. It's a composite field but no
			// dataIndex
			// Use the name instead
			if (mrb != null && mrb[item.name + sfx]) {
				item[cfg] = mrb[item.name + sfx];
				return true;
			}
	
			if( ignoreSharedResource !== true ){
				// try to translate from the shared resource-bundle
				var v = Main.translate("ds", item.dataIndex || item.paramIndex);
				if( v !== undefined && v !== null ){
					item[cfg] = v;
				}
			}
		};
		trl(vrb, mrb, item, "fieldLabel", "__lbl");
		trl(vrb, mrb, item, "tooltip", "__tlp", true);
	},
	
	
	/**
	 * Translation for a grid column. Tries to find a translation in the view
	 * RB, model RB or in the shared translations
	 * 
	 * @param {}
	 *            vrb grid resource bundle
	 * @param {}
	 *            mrb model resource bundle
	 * @param {}
	 *            item - the column
	 * @return {Boolean}
	 */

	translateColumn : function(vrb, mrb, item) {
		
		var trl = function(vrb, mrb, item, cfg, sfx, ignoreSharedResource){
			// check if the view has its own resource bundle
			if (vrb !== undefined && vrb !== null && vrb[item.name + sfx]) {
				item[cfg] = vrb[item.name + sfx];
				return true;
			}
			// try to translate it from the model"s resource bundle
			if (item.dataIndex !== undefined && item.dataIndex !== null && mrb !== undefined && mrb !== null
					&& mrb[item.dataIndex + sfx]) {
				item[cfg] = mrb[item.dataIndex + sfx];
				return true;
			}
			if( ignoreSharedResource !== true ){
				// try to translate from the shared resource-bundle
				item[cfg] = Main.translate("ds", item.dataIndex);
			} else { 
				// copy header information into tooltip
				// information is usefull for columns which width is small and header text is large
				item[cfg] = item["header"];
			}
		};
		trl(vrb, mrb, item, "header", "__lbl");
		trl(vrb, mrb, item, "tooltip", "__tlp", true);
	},

	/**
	 * Create and return a yes/no store for comboboxes
	 * 
	 * @return {}
	 */
	createBooleanStore : function() {
		return Ext.create("Ext.data.Store", {
			fields : [ "bv", "tv" ],
			data : [ {
				"bv" : true,
				"tv" : Main.translate("msg", "bool_true")
			}, {
				"bv" : false,
				"tv" : Main.translate("msg", "bool_false")
			} ]
		});
	},

	/**
	 * Create a filter model from a record model. In many situations the filter
	 * model is very much the same as the record model, although there some
	 * differences so that the record model cannot be used.
	 * 
	 * The validations are different, the boolean usage, etc.
	 * 
	 * @param {}
	 *            cfg
	 */
	createFilterModelFromRecordModel : function(cfg) {
        var fmn = cfg.recordModelFqn + "Filter";
        if (Ext.ClassManager.isCreated(fmn)) {
        	return Ext.ClassManager.get(fmn);
        }
        
        var rm = Ext.create(cfg.recordModelFqn);
        var flds = [];
        var x = rm.fields.items;
        for (var i = 0; i < x.length; i++) {
			var f = {
					name : x[i].name,
					type : x[i].type,
					noSort   : x[i].noSort,
					noFilter : x[i].noFilter
			};
            if (f.type === "boolean" || f.type === "int" || f.type === "float") {
                f.allowNull = true;
            }
            if (f.type === "date") {
                f.dateFormat = x[i].dateFormat;
            }
            flds[i] = f;
        }
        if (cfg != null && cfg.fields != null) {
            for (var j = 0; j < cfg.fields.length; j++) {
                flds[flds.length] = cfg.fields[j];
            }
        }
        var newCfg = cfg;
        newCfg.fields = flds;
        newCfg.extend = "Ext.data.Model";
        newCfg.clientIdProperty = "__clientRecordId__"; // in plus
        delete newCfg.recordModelFqn;
        return Ext.define(fmn, newCfg);
    },
	
	/**
	 * Shorthand to send an ajax request of type GET and apply the specified
	 * function as callback
	 * 
	 * @param {}
	 *            url
	 * @param {}
	 *            params
	 * @param {}
	 *            fn
	 * @param {}
	 *            scope
	 */
	doWithGetResult : function(url, params, fn, scope) {
		Ext.Ajax.request({
			url : url,
			method : "GET",
			params : params,
			success : function(response, options) {
				var r = Ext.getResponseDataInJSON(response);
				fn.call(scope || window, r, response, options);
			},
			failure : function(response, options) {
				try {
					Ext.Msg.hide();
				} catch (e) {
					// nothing to do
				}
				e4e.dc.AbstractDc.prototype.showAjaxErrors(response, options)
			},
			scope : scope
		});
	},

	working : function(msg) {
		msg = "";
		Ext.Msg.wait(msg, "Please wait", {
			   interval: 300,
			   duration: 30000,
			   increment: 10,
			   text: Main.translate("msg","working")
			});
	},
	
	workingEnd : function(){
		try {
			Ext.Msg.hide();
		} catch (e) {
			// nothing to do
		}
	},
	
	error : function(msg, params, fn) {
		this.alert(Ext.Msg.ERROR, msg, params, fn);
	},

	warning : function(msg, params, fn) {
		this.alert(Ext.Msg.WARNING, msg, params, fn);
	},

	info : function(msg, params, fn) {
		this.alert(Ext.Msg.INFO, msg, params, fn);
	},
	
	rpcFailure : function(response, doNotRefreshDc) {
		var dc = response.request.scope.dc;
        var msg = Ext.getResponseErrorText(response);
		this.alert(Ext.Msg.ERROR, msg);
		if(!doNotRefreshDc) {
			dc.doReloadPage();
		}
		
	},

	alert : function(type, msg, params, fn) {
		var title = "Error";
		var cls = "msg-error";
		if (type === Ext.Msg.INFO) {
			title = "Info";
			cls = "msg-info";
		} else if (type === Ext.Msg.WARNING) {
			title = "Warning";
			cls = "msg-warning";
		}
		Ext.Msg.show({
			title : title,
			msg : Main.translate("msg", msg, null),
			buttons : Ext.Msg.OK,
			scope : this,
			//icon : type,
			cls : cls,
        	draggable: true,
        	closable: params && params.closable === true ? true : false,
        	fn: fn
		});
	},

	/**
	 * put spaces from step-to-step in a string to can be wraped in a message box
	 */
	_isertSpaces_ : function(s){
		var res="";
		var maxRowLen = 80;
		s = "" + s;
		var l = s.length;
		if( l <= maxRowLen ){
			res = s;
		} else {
			var i, lastSpace, lastForSplit, c;
			var a = s.split("");
			lastSpace = 0;
			lastForSplit = 0;
			i = 0;
			while(i<l){
				c = a[i];
				if( c === ' ' || c === '\t' || c === '\n' ){
					lastSpace = i;
					lastForSplit = i;
				} else {
					if( !((c >= 'a' && c <= 'z') ||
						(c >= 'A' && c <= 'Z') ||
						(c >= '0' && c <= '9')) ){
						lastForSplit = i;
					}
				}
				if( i-lastSpace > maxRowLen ){
					if( i-lastForSplit > maxRowLen ){
						lastForSplit = i;
					}
					lastSpace = lastForSplit;
					lastForSplit++;
					a.splice(lastForSplit, 0, ' ');
					i++;
					l = a.length;
				}
				i++;
			}
			
			for(i=0;i<l;i++){
				res += a[i];
			}
		}
		return res;
	},
	
	/**
	 * config = {code, title, message, details }
	 */
	serverMessage : function(msg, ajaxResp) {

		if( !Ext.isEmpty(ajaxResp) && ajaxResp.aborted === true && Ext.isEmpty(msg) ){
			return;
		}
		var app = getApplication();
		if( app.session.locked ){
			return;
		}
		if( app.doReloadPageOrLockSession(ajaxResp) ){
			return;
		}
		
		var config = null;
		if( Ext.isEmpty(msg) && !Ext.isEmpty(ajaxResp) ){
			if( ajaxResp && (""+ajaxResp.timeout === "true" || ""+ajaxResp.timedout === "true") ) {
				msg = "" + ajaxResp.statusText;
			} else {
				var rJson = Ext.getResponseDataInJSON(ajaxResp);
				if( !Ext.isEmpty(rJson.msg) ){
					config = {
						code : rJson.err_no,
						title : rJson.err_msg,
						message : rJson.msg,
						details : rJson.details || null
					};					
				} else {
					msg = Ext.getResponseDataInText(ajaxResp);
				}
			}
		}
		
		if( Ext.isEmpty(msg) && !config ){
			return;
		}

		if( !config ){
			var err = msg.split("\n||\n");
			if (err.length === 1) {
				config = {
					message : err[0]
				};
			} else {
				config = {
					code : err[0],
					title : err[1],
					message : err[2],
					details : err[3]
				};
			}
		}
		Ext.Msg.hide();

		var cfg = config || {};
		if( "null" === cfg.message ){
			cfg.message = null;
		}
		if( "null" === cfg.details ){
			cfg.details = null;
		}
		var c = cfg.code;
		var t = cfg.title || Main.translate("msg", "serverMessage");
		var m = cfg.message || "No response received from server.";
		var d = cfg.details;

		if (app.session.client && app.session.client.id) {
			m = m.replace(app.session.client.id + "-", "");
		}
		if (m.length > 2000) {
			if (!d) {
				d = m;
			}
			m = m.substr(0, 2000);
		}
		m = Main._isertSpaces_(m);

		var alertCfg = {
			title : t,
			msg : m,
			cls: 'msg-server-message',	//icon : (iconFlag == "true") ? Ext.Msg.ERROR : null,
			buttons : Ext.Msg.OK
		};
		if (d) {
			Ext.apply(alertCfg, {
				buttons : Ext.Msg.OK + Ext.Msg.YES,
				buttonText : { yes : "Details" },
				fn : function(btnId) {
					if (btnId === "yes") {
						Ext.Msg.show({
							title : c + " / " + t,
							msg : Main._isertSpaces_(d),
							cls: 'msg-server-message',	//icon : Ext.Msg.ERROR,
							buttons : Ext.Msg.OK
						});
					}
				}
			});
		}
		Ext.Msg.show(alertCfg);
	},
	

    getFrameTabId : function(frameFqn) {
    	if( frameFqn ){
    		return Constants.CMP_ID.FRAME_TAB_PREFIX + frameFqn.replaceAll(".","_")
    	}
    	return null;
    },

    getFrameIFrameId : function(frameFqn) {
    	if( frameFqn ){
    		return Constants.CMP_ID.FRAME_IFRAME_PREFIX + frameFqn.replaceAll(".","_")
    	}
    	return null;
    }	
};
