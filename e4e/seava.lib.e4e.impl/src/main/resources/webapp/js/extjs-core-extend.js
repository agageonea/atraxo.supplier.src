/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

/* ==================== general javascript overrides ======================== */

// string.endsWith 
if (!String.prototype.endsWith) {
	String.prototype.endsWith = function(suffix) {
		return this.indexOf(suffix, this.length - suffix.length) !== -1;
	};
}

// string.toFirstUpper 

if (!String.prototype.toFirstUpper) {
	String.prototype.toFirstUpper = function() {
		return this.substring(0, 1).toUpperCase() + this.substring(1, this.length);
	};
}

// string.trim 

if (!String.prototype.trim) {
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, "");
	}
}

if (!String.prototype.ltrim) {
	String.prototype.ltrim = function() {
		return this.replace(/^\s+/, "");
	}
}

if (!String.prototype.rtrim) {
	String.prototype.rtrim = function() {
		return this.replace(/\s+$/, "");
	}
}

if (!String.prototype.replaceAll) {
	String.prototype.replaceAll = function(find, replace) {
		return this.replace(new RegExp(find.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1"), 'g'), replace);
	}
}

// array.indexOf 

if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function(obj, start) {
		for (var i = (start || 0), j = this.length; i < j; i++) {
			if (this[i] === obj) {
				return i;
			}
		}
		return -1;
	}
}

Ext.fpApply = function(object, config, defaults) {
	if (object) {
		if (defaults) {
			Ext.fpApply(object, defaults);
		}
		if (config && typeof config === 'object') {
			var i;
			for (i in config) {
				if( typeof object[i] === 'object') {
					Ext.fpApply(object[i], config[i]);
				} else {
					object[i] = config[i];
				}
			}
		}
	}
	return object;
};
