/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.base.WorkflowFormWithHtmlWindow", {
	extend : "Ext.Window",

	constructor : function(config) {
		var cfg = {
			border : true,
			width : 500,
			height : 500,
			resizable : true,
			closable : true,
			constrain : true,
			layout : "fit",
			modal : true,
			buttonAlign : "center",
			buttons : [ {
				xtype : "button",
				text : Main.translate("tlbitem", "save__lbl"),
				scope : this,
				handler : this.doSave
			} ]
		};
		var resolved = false;
		var sh = null;
		var eh = null;
		var submitUrl;
		if (config._wfConfig_.type === "startform") {
			submitUrl = Main.wfProcessInstanceAPI().start;
			sh = "<form id='dnet-workflow-form' action='";
			sh += submitUrl;
			sh += "'>";
			sh += "<input type='hidden' name='processDefinitionId' value='";
			sh += config._wfConfig_.processDefinitionId;
			sh += "'>";
			sh += "<input type='hidden' name='processDefinitionKey' value=''>";
			sh += "<input type='hidden' name='businessKey' value=''>";
			eh = "</form>";
			resolved = true;
		}

		if (config._wfConfig_.type === "taskform") {
			submitUrl = Main.wfTaskAPI(config._wfConfig_.taskId).complete;
			sh = "<form id='dnet-workflow-form' action='" + submitUrl + "'>";
			eh = "</form>";
			resolved = true;
		}

		if (!resolved) {
			alert ("Invalid value for _wfConfig_.type in WorkflowFormWithHtmlWindow.");
		}
		config.html = sh + config.html + eh;
		Ext.apply(cfg, config);
		e4e.base.WorkflowFormWithHtmlWindow.superclass.constructor.call(this, cfg);
	},

	initComponent : function() {
		e4e.base.WorkflowFormWithHtmlWindow.superclass.initComponent
				.call(this);
	},

	getButton : function() {
		return this.items.get(0).buttons[0];
	},

	doSave : function() { // parameters: btn, evnt
		var frm = document.getElementById("dnet-workflow-form");
		var p = {};
		var elements = frm.elements;
		var len = elements.length;
		for ( var i = 0; i < len; i++) {
			p[elements[i].name] = elements[i].value;
		}
		Ext.Ajax.request({
			url : frm.action,
			method : "POST",
			params : p,
			success : this.onSaveSuccess,
			failure : this.onSaveFailure,
			scope : this
		});
	},

	onSaveSuccess : function() {
		this.close();
	},

	onSaveFailure : function(response) {
		Ext.MessageBox.hide();
		var msg, withDetails = false;
		var respTxt = Ext.getResponseErrorText(response);
		if (respTxt.length > 0) {
			if (respTxt.length > 2000) {
				msg = respTxt.substr(0, 2000);
				withDetails = true;
			} else {
				msg = respTxt;
			}
		} else {
			msg = "No response received from server.";
		}
		var alertCfg = {
			msg : msg,
			scope : this,
			icon : Ext.MessageBox.ERROR,
			buttons : Ext.MessageBox.OK
		}
		if (withDetails) {
			alertCfg.buttons['cancel'] = 'Details';
			alertCfg['detailedMessage'] = respTxt;
		}
		Ext.Msg.show(alertCfg);
	}
});