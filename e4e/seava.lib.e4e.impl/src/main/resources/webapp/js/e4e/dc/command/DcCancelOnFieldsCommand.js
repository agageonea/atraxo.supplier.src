/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.dc.command.DcCancelOnFieldsCommand", {
	extend : "e4e.dc.command.AbstractDcSyncCommand",

	dcApiMethod : e4e.dc.DcActionsFactory.CANCEL_ON_FIELDS,
	
	onExecute : function(options) {
		var dc = this.dc;
		var rec = dc.record;
		if( Ext.isEmpty(options) || !Ext.isArray(options.fields) || Ext.isEmpty(rec) ){
			return;
		}
		var oldV = options.oldValues;
		if( oldV ){
			var f = options.fields;
			var i, l = f.length;
			for(i=0; i<l; i++){
				rec.set(f[i],oldV[f[i]]);
			}
		}
	}
});
