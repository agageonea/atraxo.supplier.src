/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
/**
 * Singleton class used to implement the state management rules for the
 * registered actions.
 */
e4e.dc.DcActionsStateManager = {

	applyStates : function(dc) {
		var flags = dc.dcState;
		var names = dc.actionNames;
		
		if(flags.hasParent && flags.parentIsNull && !flags.parentCheckDisabled){ 
			this.disableAll(dc, names);
			return;
		}
		for (var i = 0, l = names.length; i < l; i++) {
			var n = names[i];
			var an = "do" + n;
			if (dc.commands[an].locked === true) {
				dc.actions[an].setDisabled(true);
			} else {
				var b = this["_is" + n + "Disabled"](flags);
				dc.actions[an].setDisabled(b);
			}
		}
	},

	disableAll : function(dc, names) {
		for (var i = 0, l = names.length; i < l; i++) {
			var n = names[i];
			dc.actions["do" + n].setDisabled(true);
		}
	},

	/* public helpers */

	isSaveEnabled : function(dc) {
		return this._isSaveEnabled(dc.dcState);
	},

	isSaveDisabled : function(dc) {
		return !this.isSaveEnabled(dc);
	},

	isSaveInChildEnabled : function(dc) {
		return this._isSaveInChildEnabled(dc.dcState);
	},

	isSaveInChildDisabled : function(dc) {
		return !this.isSaveInChildEnabled(dc);
	},
	
	isCancelEnabled : function(dc) {
		return this._isCancelEnabled(dc.dcState);
	},

	isCancelDisabled : function(dc) {
		return !this.isCancelEnabled(dc);
	},

	isCancelOnFieldsEnabled : function(dc,flist) {
		return this._isCancelOnFieldsEnabled(dc.dcState,flist,dc.record);
	},

	isCancelOnFieldsDisabled : function(dc,flist) {
		return !this.isCancelOnFieldsEnabled(dc,flist);
	},
	
	isCancelSelectedEnabled : function(dc) {
		return this._isCancelSelectedEnabled(dc.dcState);
	},

	isCancelSelectedDisabled : function(dc) {
		return !this.isCancelSelectedEnabled(dc);
	},
	
	isQueryDisabled : function(dc) {
		return this._isQueryDisabled(dc.dcState);
	},

	isQueryEnabled : function(dc) {
		return !this.isQueryDisabled(dc);
	},

	isClearQueryDisabled : function(dc) {
		return this._isClearQueryDisabled(dc.dcState);
	},

	isClearQueryEnabled : function(dc) {
		return !this.isClearQueryDisabled(dc);
	},

	isEnterQueryDisabled : function(dc) {
		return this._isEnterQueryDisabled(dc.dcState);
	},

	isEnterQueryEnabled : function(dc) {
		return !this.isEnterQueryDisabled(dc);
	},

	isNewDisabled : function(dc) {
		return this._isNewDisabled(dc.dcState);
	},

	isNewEnabled : function(dc) {
		return !this.isNewDisabled(dc);
	},
	
	isNewInChildDisabled : function(dc) {
		return this._isNewInChildDisabled(dc.dcState);
	},

	isNewInChildEnabled : function(dc) {
		return !this.isNewInChildDisabled(dc);
	},

	isCopyDisabled : function(dc) {
		return this._isCopyDisabled(dc.dcState);
	},

	isCopyEnabled : function(dc) {
		return !this.isCopyDisabled(dc);
	},

	isDeleteDisabled : function(dc) {
		return this._isDeleteDisabled(dc.dcState);
	},

	isDeleteEnabled : function(dc) {
		return !this.isDeleteDisabled(dc);
	},

	isPrevRecDisabled : function(dc) {
		return this._isPrevRecDisabled(dc.dcState);
	},

	isPrevRecEnabled : function(dc) {
		return !this.isPrevRecDisabled(dc);
	},

	isNextRecDisabled : function(dc) {
		return this._isNextRecDisabled(dc.dcState);
	},

	isNextRecEnabled : function(dc) {
		return !this.isNextRecDisabled(dc);
	},

	isReloadRecDisabled : function(dc) {
		return this._isReloadRecDisabled(dc.dcState);
	},

	isReloadRecEnabled : function(dc) {
		return !this.isReloadRecDisabled(dc);
	},

	isEditOutDisabled : function(dc) {
		return this._isEditOutDisabled(dc.dcState);
	},

	isEditOutEnabled : function(dc) {
		return !this.isEditOutDisabled(dc);
	},
	
	isCloseDisabled : function(dc) {
		return this._isCloseDisabled(dc.dcState);
	},
	
	isCloseEnabled : function(dc) {
		return !this.isCloseDisabled(dc);
	},

	/* private helpers - decision makers implementation */

	_isQueryDisabled : function(flags) {
		return (flags.isDisabled || flags.isDirty ||
				(flags.hasParent && !flags.parentCheckDisabled && (flags.parentIsNull || flags.parentIsNew )) || 
				(flags.canDo.canDoQuery === false));
	},

	_isQueryEnabled : function(flags) {
		return !this._isQueryDisabled(flags);
	},

	_isClearQueryDisabled : function(flags) {
		return (flags.isDisabled || flags.isDirty || (flags.canDo.canDoClearQuery === false));
	},

	_isClearQueryEnabled : function(flags) {
		return !this._isClearQueryDisabled(flags);
	},

	_isEnterQueryDisabled : function(flags) {
		return (flags.isDisabled || flags.canDo.canDoEnterQuery === false);
	},

	_isEnterQueryEnabled : function(flags) {
		return !this._isEnterQueryDisabled(flags);
	},

	_isNewDisabled : function(flags) {
		return ((flags.isCurrentRecordDirty && !flags.multiEdit) || flags.isDisabled || (flags.disabledRules.N===true)
				|| (flags.hasParent && !flags.parentCheckDisabled && (flags.parentIsNull || flags.parentIsNew || flags.parentIsDirty))
				|| flags.isAnyChildDirty || (flags.isReadOnly && flags.canDo.canDoNew !== true) || flags.canDo.canDoNew === false || flags.isQuerying === true);
	},

	_isNewEnabled : function(flags) {
		return !this._isNewDisabled(flags);
	},

	_isNewInChildDisabled : function(flags) {
		return ((flags.isCurrentRecordDirty && !flags.multiEdit) || flags.isDisabled
				|| (flags.hasParent && flags.parentIsNull && !flags.parentCheckDisabled)
				|| flags.isAnyChildDirty || flags.isReadOnly || flags.canDo.canDoNewInChild === false);
	},

	_isNewInChildEnabled : function(flags) {
		return !this._isNewInChildDisabled(flags);
	},
	
	_isCopyDisabled : function(flags) {
		return (flags.isReadOnly || flags.recordIsNull || flags.isDisabled
				|| flags.selectedRecordsLength !== 1 || flags.storeCount === 0 || flags.isAnyChildDirty 
				|| (flags.isCurrentRecordDirty && !flags.multiEdit) || (flags.canDo.canDoCopy === false));
	},

	_isCopyEnabled : function(flags) {
		return !this._isCopyDisabled(flags);
	},

	_isSaveEnabled : function(flags) {
		return (!flags.isReadOnly && !flags.isDisabled && (flags.disabledRules.S!==true) && (flags.isCurrentRecordDirty || flags.isStoreDirty) && (flags.canDo.canDoSave !== false));
	},
	_isSaveDisabled : function(flags) {
		return !this._isSaveEnabled(flags);
	},

	_isSaveInChildEnabled : function(flags) {
		return (!flags.isReadOnly && !flags.isDisabled && (flags.isDirty) && (flags.canDo.canDoSave !== false));
	},
	_isSaveInChildDisabled : function(flags) {
		return !this._isSaveInChildEnabled(flags);
	},
	
	_isCancelEnabled : function(flags) {
		return (flags.isDirty && (flags.canDo.canDoCancel !== false));
	},

	_isCancelDisabled : function(flags) {
		return !this._isCancelEnabled(flags);
	},
	
	_isCancelOnFieldsEnabled : function(flags,flist,record) {
		var res = (Ext.isArray(flist) && flags.isDirty && (flags.canDo.canDoCancelOnFields !== false));
		if( res ){
			res = false;
			// check fields list
			var mod = record.modified;
			if( mod ){
				var i, l = flist.length;
				for(i=0; i<l; i++){
					if( mod[flist[i]] ){
						res = true;
						break;
					}
				}
			}
		}
		return res;
	},
	
	_isCancelOnFieldsDisabled : function(flags,flist,record) {
		return !this._isCancelOnFieldsEnabled(flags,flist,record);
	},
	
	_isCancelSelectedEnabled : function(flags) {
		return (flags.isDirty && (flags.canDo.canDoCancelSelected !== false));
	},

	_isCancelSelectedDisabled : function(flags) {
		return !this._isCancelSelectedEnabled(flags);
	},

	_isDeleteDisabled : function(flags) {
		return (flags.isReadOnly || flags.isDisabled || flags.selectedRecordsLength === 0 || (flags.disabledRules.D===true)
				|| flags.isAnyChildDirty || flags.storeCount === 0 || flags.isDirty || (flags.canDo.canDoDelete === false));
	},

	_isDeleteEnabled : function(flags) {
		return !this._isDeleteDisabled(flags);
	},

	_isPrevRecDisabled : function(flags) {
		return (flags.storeCount === 0
				|| (flags.isCurrentRecordDirty && !flags.multiEdit) || flags.isAnyChildDirty || (flags.canDo.canDoPrevRec === false));
	},

	_isPrevRecEnabled : function(flags) {
		return !this._isPrevRecDisabled(flags);
	},
	
	_isNextRecDisabled : function(flags) {
		return (flags.storeCount === 0
				|| (flags.isCurrentRecordDirty && !flags.multiEdit) || flags.isAnyChildDirty || (flags.canDo.canDoNextRec === false)); // and
	},

	_isNextRecEnabled : function(flags) {
		return !this._isNextRecDisabled(flags);
	},

	_isReloadRecDisabled : function(flags) {
		return (flags.storeCount === 0 || (flags.isCurrentRecordDirty) || (flags.canDo.canDoReloadRec === false));
	},

	_isReloadRecEnabled : function(flags) {
		return !this._isReloadRecDisabled(flags);
	},

	_isReloadPageDisabled : function(flags) {
		return this._isQueryDisabled(flags);
	},

	_isReloadPageEnabled : function(flags) {
		return this._isQueryEnabled(flags);
	},

	_isEditInEnabled : function(flags) {
		!this._isEditInDisabled(flags);
	},
	_isEditInDisabled : function(flags) {
		return (flags.recordIsNull || flags.isDirty || (flags.canDo.canDoEditIn === false) || (flags.disabledRules.E===true));
	},

	_isEditOutDisabled : function(flags) {
		return (flags.isAnyChildDirty || (flags.isCurrentRecordDirty && !flags.multiEdit) || (flags.canDo.canDoEditOut === false));
	},

	_isEditOutEnabled : function(flags) {
		return !this._isEditOutDisabled(flags);
	},

	_isCloseDisabled : function(flags) {
		return this._isEditOutDisabled(flags);
	},
	
	_isCloseEnabled : function(flags) {
		return !this._isCloseDisabled(flags);
	}
};

