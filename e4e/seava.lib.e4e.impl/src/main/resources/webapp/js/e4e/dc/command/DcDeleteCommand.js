/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.dc.command.DcDeleteCommand", {
	extend : "e4e.dc.command.AbstractDcAsyncCommand",

	dcApiMethod : e4e.dc.DcActionsFactory.DELETE,
	
	customMsg: null,

	constructor : function(config) {
		this.callParent(arguments);
		this.confirmByUser = true;
		this.confirmMessageTitle = Main.translate("msg", "dc_confirm_action");
		this.confirmMessageBody = Main.translate("msg", "dc_confirm_delete_selection");
	},

	onExecute : function(options) {
		var dc = this.dc;
		dc.store.remove(dc.getSelectedRecords());
		// SONE-532 : In editable grids the delete requires save
		// go to the server with each delete request, don't wait for save
/*		
//		if (!dc.multiEdit) {
*/
			dc.store.sync({
				callback : function(batch, options) {
					this.onAjaxResult({
						batch : batch,
						options : options,
						success : !batch.exception
					});
					this.dc._setStoreIsLoading_(false);
				},
				scope : this,
				options : options
			});
/*
//		} else {
//			dc.doDefaultSelection();
//		}
*/
	},

	onAjaxSuccess : function(ajaxResult) {
		this.callParent(arguments);
		this.dc.fireEvent("afterDoCommitSuccess", this.dc, ajaxResult.options.options);
		this.dc.doReloadPage();
	},

	isActionAllowed : function() {
		if (e4e.dc.DcActionsStateManager.isDeleteDisabled(this.dc)) {
			this.dc.warning(Main.msg.DC_DELETE_NOT_ALLOWED, "msg");
			return false;
		}
		return true;
	}

});