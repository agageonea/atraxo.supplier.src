/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.dc.command.DcSaveInChildCommand", {
	extend : "e4e.dc.command.AbstractDcAsyncCommand",

	dcApiMethod : e4e.dc.DcActionsFactory.SAVE_IN_CHILD,

	errorTpl : Ext.create('Ext.XTemplate', [
			'<ul style="list-style-type: none;padding:0; margin:0;">',
			'<tpl for=".">', '<li style="list-style-type: none;">',
			'<span class="field-name">', Main.translate("ds", "fld"),
			' `{fieldTitle}` </span>', '<span class="error">{message}</span>',
			'</li></tpl></ul>' ]),

	onExecute : function(options) {
		if (this.dc.params != null) {
			this.dc.store.proxy.extraParams.params = Ext.JSON.encode(this.dc.params.data);
		}
		options.showWorking = this.showWorking = true;
		Main.working();
		
		this._prepareCallForChilds_();
		
		if (e4e.dc.DcActionsStateManager.isSaveEnabled(this.dc) && this.dc.store.isSomethingToSync() ) {
			this.dc.store.sync({
				callback : function(batch, options) {
					var r = this._getResponseFromBatch(batch);
					this.onAjaxResult({
						batch : batch,
						options : options,
						success : !batch.exception,
						response : r
					});
					this.dc._setStoreIsLoading_(false);
				},
				scope : this,
				options : options
			});
		} else {
			this._callNextChild_();
		}
	},

	onAjaxSuccess : function(ajaxResult) {
		this.callParent(arguments);
		this._doOnAjaxSuccess_(ajaxResult,"afterDoSaveSuccess");
		this.dc.fireEvent("afterDoCommitSuccess", this.dc, ajaxResult.options.options);
		this._callNextChild_();
	},

	onAjaxFailure : function(ajaxResult) {
		this.callParent(arguments);
		this._doOnAjaxFailure_(ajaxResult,"afterDoSaveFailure");
	},
	
	isActionAllowed : function() {
		if (e4e.dc.DcActionsStateManager.isSaveInChildDisabled(this.dc)) {
			this.dc.info(Main.msg.DC_SAVE_NOT_ALLOWED, "msg");
			return false;
		}
				
		var res = true;
		var dc = this.dc;
		if (!dc.multiEdit) {
			res = this.isValid(dc.getRecord());
		} else {
			if (!this.isValid(dc.store.getUpdatedRecords())) {
				res = false;
			} 
			if (!this.isValid(dc.store.getAllNewRecords())) {
				res = false;
			}
		}
		if (!res) {
			this.dc.info(Main.msg.INVALID_FORM, "msg");
			return false;
		} 		
		return true;
	},

	/**
	 * Add the translated field name to the error info.
	 * 
	 * @param {}
	 *            item
	 * @param {}
	 *            idx
	 * @param {}
	 *            len
	 */
	addFieldNameToError : function(item) {
		var v = Main.translateModelField(this.dc._trl_, item.field);
		item["fieldTitle"] = v;
	},

	/**
	 * Validate the given list of records
	 * 
	 * @param {}
	 *            recs
	 * @return {Boolean}
	 */
	isValid : function(recs) {
		if (!Ext.isArray(recs)) {
			recs = [ recs ];
		}
		var len = recs.length;
		var errors;
		for (var i = 0; i < len; i++) {
			errors = recs[i].validate();
			if (!errors.isValid()) {
				errors.each(this.addFieldNameToError, this);

				Ext.Msg.show({
					title : Main.translate("msg","invalid_data"),
					msg : this.errorTpl.apply(errors.getRange()),
					icon : Ext.MessageBox.ERROR,
					buttons : Ext.MessageBox.OK
				});
				return false;
			}
		}
		return true;
	},
	
	_nextChildInd_ : null,
	_totalCount_ : null,
	_childs_ : null,
	_oldRecStatus_ : null,
	
	
	/**
	 *  Prepare object state (set variables) to be able to call doSaveInChild method for all children DCs 
	 */
	_prepareCallForChilds_ : function(){
		this._nextChildInd_ = 0;
		this._childs_ = this.dc.getChildren();
		this._totalCount_ = this._childs_.length;
		this._oldRecStatus_ = this.dc.getRecordStatus();
	},

	/**
	 *  If current record is a newly created record, which has no ID at the moment when the child record was created,
	 *  need to be updated the child record with the ID received from database.
	 */
	_updateChildrenContext_ : function(){
		for(var i=0;i<this._totalCount_;i++){
			var cDc = this._childs_[i];
			if (cDc.dcContext) {
				var r = cDc.getRecord();
				if( r ) {
					cDc.dcContext._applyContextData_(r);
				}
			}
		}
	},
	
	/**
	 *  Call doSaveInChild for the next child
	 */
	_callNextChild_ : function(){
		if( this._nextChildInd_ === 0 && this._oldRecStatus_ === "insert" ){
			this._updateChildrenContext_();
		}
		while( this._nextChildInd_ < this._totalCount_ ){
			var cDc = this._childs_[this._nextChildInd_];
			this._nextChildInd_++;
			if (e4e.dc.DcActionsStateManager.isSaveEnabled(cDc)) {
				this.dc.mon(cDc, "afterDo" + this.dcApiMethod + "Success", this._afterDoSaveInChildSuccess_, this);
				this.dc.mon(cDc, "afterDo" + this.dcApiMethod + "Failure", this._afterDoSaveInChildFailure_, this);
				this.dc.mon(cDc, "cmdExecutionFailed", this._afterDoSaveInChildFailure_, this);
				cDc.doSaveInChild();
				break;
			}
		}
	},
	
	_afterDoSaveInChildSuccess_ : function(){ // parameters: dc,res
		// remove the listeners
		var cDc = this._childs_[this._nextChildInd_-1];
		this.dc.mun(cDc, "afterDo" + this.dcApiMethod + "Success", this._afterDoSaveInChildSuccess_, this);
		this.dc.mun(cDc, "afterDo" + this.dcApiMethod + "Failure", this._afterDoSaveInChildFailure_, this);
		this.dc.mun(cDc, "cmdExecutionFailed", this._afterDoSaveInChildFailure_, this);
		// call save for next child
		this._callNextChild_();
	},
	
	_afterDoSaveInChildFailure_ : function(){ // parameters: dc,res
		// just remove the listeners, nothing else to-do
		var cDc = this._childs_[this._nextChildInd_-1];
		this.dc.mun(cDc, "afterDo" + this.dcApiMethod + "Success", this._afterDoSaveInChildSuccess_, this);
		this.dc.mun(cDc, "afterDo" + this.dcApiMethod + "Failure", this._afterDoSaveInChildFailure_, this);
		this.dc.mun(cDc, "cmdExecutionFailed", this._afterDoSaveInChildFailure_, this);
	}
	
});
