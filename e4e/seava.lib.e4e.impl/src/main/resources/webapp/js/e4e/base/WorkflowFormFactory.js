/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
e4e.base.WorkflowFormFactory = {

	createStartForm : function(processDefinitionId) {

		var succesFn = function(response) { // additional parameter: options
			var w = new e4e.base.WorkflowFormWithHtmlWindow({
				html : Ext.getResponseDataInText(response),
				_wfConfig_ : {
					type : "startform",
					processDefinitionId : processDefinitionId
				}
			});
			w.show();
		};

		Ext.Ajax.request({
			url : Main.wfProcessDefinitionAPI(processDefinitionId).form,
			method : "GET",
			success : succesFn,
			failure : function() {
				alert('error');
			},
			scope : this
		});

	},

	createTaskForm : function(taskId) {

		var succesFn = function(response) { // additional parameter: options

			var w = new e4e.base.WorkflowFormWithHtmlWindow({
				html : Ext.getResponseDataInText(response),
				_wfConfig_ : {
					type : "taskform",
					taskId : taskId
				}
			});
			w.show();
		};

		Ext.Ajax.request({
			url : Main.wfTaskAPI(taskId).form,
			method : "GET",
			success : succesFn,
			failure : function() {
				alert('error');
			},
			scope : this
		});
	}
};
