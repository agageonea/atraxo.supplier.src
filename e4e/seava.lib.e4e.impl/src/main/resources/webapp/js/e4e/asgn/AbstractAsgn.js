/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.asgn.AbstractAsgn", {

	mixins : {
		observable : 'Ext.util.Observable'
	},

	/**
	 * Store for available records
	 */
	storeLeft : null,

	/**
	 * Store for selected records
	 */
	storeRight : null,

	/**
	 * Various runtime configuration properties.
	 */
	tuning : {

		/**
		 * Number of milliseconds before execute the query. Used if value>0
		 */
		queryDelay : 150,

		/**
		 * Page-size for a query
		 */
		fetchSize : Main.viewConfig.FETCH_SIZE_ASGN
	},

	/**
	 * Parameters model instance
	 */
	params : null,

	/**
	 * Filter object instance. Contains the left filter and right filter
	 */
	filter : null,

	/**
	 * Contextual filter for left
	 */
	leftFilter : null,
	leftAdvFilter : null,
	leftAdvUIFilter : null,

	/**
	 * Contextual filter for right
	 */
	rightFilter : null,
	rightAdvFilter : null,
	rightAdvUIFilter : null,

	/**
	 * Contextual parameters for left
	 */
	leftParams : null,

	/**
	 * Contextual parameters for right
	 */
	rightParams : null,

	/**
	 * Data model signature - record constructor.
	 */
	recordModel : null,

	/**
	 * Parameters model signature - record constructor.
	 */
	paramModel : null,

	/**
	 * Flag to show if any operation was executed
	 */
	_dirty_ : null,

	constructor : function(config) {
		config = config || {};
		Ext.apply(this, config);
		this.params = {
			objectId : 8,
			selectionId : 0,
			clientId : null
		}
		if (this.recordModel == null) {
			this.recordModel = Ext.ClassManager.get(this.$className + "$Model");
		}
		if (this.dsName == null) {
			this.dsName = this.recordModel.ALIAS;
		}
		this.filter = {
			left : {
				field : null,
				value : null
			},
			right : {
				field : null,
				value : null
			}
		}
		if (this.storeLeft == null) {
			this.storeLeft = this.createStore("Left");
		}

		if (this.storeRight == null) {
			this.storeRight = this.createStore("Right");
		}

		this.mixins.observable.constructor.call(this);
	},

	// **************** Public API *****************

	initAssignement : function() {
		this.doSetup();
	},

	doReset : function() {
		this.doResetImpl();
	},

	/**
	 * Call the setup server-side procedure which prepares a temporary data with
	 * the existing selections for this context.
	 */
	doSetup : function() {
		this.doSetupImpl();
	},

	doCleanup : function() {
		this.doCleanupImpl();
	},

	/**
	 * Save changes.
	 */
	doSave : function() {
		this.doSaveImpl();
	},

	/**
	 * Load the available records.
	 */
	doQueryLeft : function() {
		this.doQueryLeftImpl();
	},

	/**
	 * Query the selected records.
	 */
	doQueryRight : function() {
		this.doQueryRightImpl();
	},

	/**
	 * Select all available.
	 */
	doMoveRightAll : function() {
		this.doMoveRightAllImpl();
	},

	/**
	 * Remove all selected
	 */
	doMoveLeftAll : function() {
		this.doMoveLeftAllImpl();
	},

	/**
	 * Select those records which are selected by the user in the available
	 * options list.
	 * 
	 * @param {Ext.grid.Panel}
	 *            theLeftGrid
	 * @param {Ext.grid.Panel}
	 *            theRightGrid
	 */
	doMoveRight : function(theLeftGrid, theRightGrid) {
		this.doMoveRightImpl(theLeftGrid, theRightGrid);
	},

	/**
	 * Remove those records which are selected by the user in the selected
	 * options list.
	 * 
	 * @param {}
	 *            theLeftGrid
	 * @param {}
	 *            theRightGrid
	 */
	doMoveLeft : function(theLeftGrid, theRightGrid) {
		this.doMoveLeftImpl(theLeftGrid, theRightGrid);
	},

	/**
	 * Return the store for the specified side.
	 * 
	 * @param {String}
	 *            side
	 * @return {Ext.data.Store}
	 */
	getStore : function(side) {
		if (side === "left") {
			return this.storeLeft;
		}
		if (side === "right") {
			return this.storeRight;
		}
	},

	afterMoveLeftSuccess : function() {
		this.setDirty(true);
		this.doQueryLeft();
		this.doQueryRight();
	},

	afterMoveRightSuccess : function() {
		this.setDirty(true);
		this.doQueryLeft();
		this.doQueryRight();
	},

	afterMoveLeftAllSuccess : function() {
		this.setDirty(true);
		Main.workingEnd();
		this.doQueryLeft();
		this.doQueryRight();
	},

	afterMoveRightAllSuccess : function() {
		this.setDirty(true);
		Main.workingEnd();
		this.doQueryLeft();
		this.doQueryRight();
	},

	afterDoSetupSuccess : function(response) {
		this.setDirty(false);
		this.params["selectionId"] = Ext.getResponseDataInText(response);
		this.doQueryLeft();
		this.doQueryRight();
	},

	afterDoResetSuccess : function() {
		this.setDirty(false);
		this.doQueryLeft();
		this.doQueryRight();
	},

	afterDoSaveSuccess : function() {
		this.setDirty(false);
		Ext.Msg.hide();
		this.fireEvent("afterDoSaveSuccess", this);
	},

	/**
	 * Return the state of controller (are operations in pending: records are
	 * moved from left to right or right to left which are not saved)
	 */
	isDirty : function() {
		return (this._dirty_ === true);
	},

	/**
	 * Set the dirty flag and fire event
	 */
	setDirty : function(v){
		if( this._dirty_ !== v ) {
			this._dirty_ = v;
			this.fireEvent("dirtyChanged", this, v);
		}
	},
	
	/**
	 * Clean the dirty flag - ignore any changes before (has no any effect on
	 * save operations)
	 */
	cleanDirty : function() {
		this.setDirty(false);
	},

	// **************** Private API *****************

	doSetupImpl : function() {
		Ext.Ajax.request({
			params : this.params,
			method : "POST",
			failure : this.afterAjaxFailure,
			success : this.afterDoSetupSuccess,
			scope : this,
			url : Main.urlAsgn + "/" + this.dsName + ".json?" + Main.requestParam.ACTION + "=" + Main.asgnAction.SETUP
		});
	},

	doMoveLeftImpl : function(theLeftGrid, theRightGrid) {
		var selection = theRightGrid.getSelectionModel().getSelection();
		if (selection.length === 0) {
			return;
		}
		var p_selected_ids = "";
		for (var i = 0; i < selection.length; i++) {
			p_selected_ids += (i > 0) ? "," : "";
			p_selected_ids += selection[i].data.id;
		}
		var p = Ext.apply({
			p_selected_ids : p_selected_ids
		}, this.params);
		Ext.Ajax.request({
			params : p,
			method : "POST",
			failure : this.afterAjaxFailure,
			success : this.afterMoveLeftSuccess,
			scope : this,
			url : Main.urlAsgn + "/" + this.dsName + ".json?" + Main.requestParam.ACTION + "=" + Main.asgnAction.MOVE_LEFT,
			options : {
				action : "moveLeft",
				fnSuccess : null,
				fnSuccessScope : null,
				fnFailure : null,
				fnFailureScope : null,
				serviceName : name
			}
		});

	},

	doMoveRightImpl : function(theLeftGrid) {
		var selection = theLeftGrid.getSelectionModel().getSelection();
		if (selection.length === 0) {
			return;
		}
		var p_selected_ids = "";
		for (var i = 0; i < selection.length; i++) {
			p_selected_ids += (i > 0) ? "," : "";
			p_selected_ids += selection[i].data.id;
		}
		var p = Ext.apply({
			p_selected_ids : p_selected_ids
		}, this.params);
		Ext.Ajax.request({
			params : p,
			method : "POST",
			failure : this.afterAjaxFailure,
			success : this.afterMoveRightSuccess,
			scope : this,
			url : Main.urlAsgn + "/" + this.dsName + ".json?" + Main.requestParam.ACTION + "=" + Main.asgnAction.MOVE_RIGHT,
			options : {
				action : "moveRight",
				fnSuccess : null,
				fnSuccessScope : null,
				fnFailure : null,
				fnFailureScope : null,
				serviceName : name
			}
		});
	},

	doMoveLeftAllImpl : function() {
		
		var qs = Ext.apply({}, this.params);
		// add rightFilter, because we want to use the same filter which is used for query
		if (this.rightFilter) {
			qs[Main.requestParam.FILTER] = Ext.encode(this.rightFilter); 
		}
		var a = [];
		if( this.rightAdvFilter ){
			a = a.concat(this.rightAdvFilter);
		}
		if( this.rightAdvUIFilter ){
			a = a.concat(this.rightAdvUIFilter);
		}
		if( a.length > 0 ) {
			qs[Main.requestParam.ADVANCED_FILTER] = Ext.encode(a); 
		}
		
		Main.working();

		Ext.Ajax.request({
			params : qs,
			method : "POST",
			failure : this.afterAjaxFailure,
			success : this.afterMoveLeftAllSuccess,
			scope : this,
			url : Main.urlAsgn + "/" + this.dsName + ".json?" + Main.requestParam.ACTION + "=" + Main.asgnAction.MOVE_LEFT_ALL,
			timeout : Main.ajaxTimeout,
			options : {
				action : "moveLeftAll",
				fnSuccess : null,
				fnSuccessScope : null,
				fnFailure : null,
				fnFailureScope : null,
				serviceName : name
			}
		});

	},

	doMoveRightAllImpl : function() {

		var qs = Ext.apply({}, this.params);
		// add leftFilter, because we want to use the same filter which is used for query
		if (this.leftFilter) {
			qs[Main.requestParam.FILTER] = Ext.encode(this.leftFilter); 
		}
		
		var a = [];
		if( this.leftAdvFilter ){
			a = a.concat(this.leftAdvFilter);
		}
		if( this.leftAdvUIFilter ){
			a = a.concat(this.leftAdvUIFilter);
		}
		if( a.length > 0 ) {
			qs[Main.requestParam.ADVANCED_FILTER] = Ext.encode(a); 
		}
		
		Main.working();
		
		Ext.Ajax.request({
			params : qs,
			method : "POST",
			failure : this.afterAjaxFailure,
			success : this.afterMoveRightAllSuccess,
			scope : this,
			url : Main.urlAsgn + "/" + this.dsName + ".json?" + Main.requestParam.ACTION + "=" + Main.asgnAction.MOVE_RIGHT_ALL,
			timeout : Main.ajaxTimeout,
			options : {
				action : "moveRightAll",
				fnSuccess : null,
				fnSuccessScope : null,
				fnFailure : null,
				fnFailureScope : null,
				serviceName : name
			}
		});
	},

	doQueryLeftImpl : function() {

		this.storeLeft.removeAll();
		var lp = {};
		var data = {};
		var params = {};

		this.storeLeft.proxy.extraParams = Ext.apply({}, this.params);
		this.storeLeft.currentPage = 1;

		if (this.leftFilter) {
			Ext.apply(data, this.leftFilter);
		}
		if (this.leftParams) {
			Ext.apply(params, this.leftParams);
		}

		this.storeLeft.proxy.extraParams[Main.requestParam.FILTER] = Ext.encode(data);
		this.storeLeft.proxy.extraParams[Main.requestParam.PARAMS] = Ext.encode(params);
		
		var a = [];
		if( this.leftAdvFilter ){
			a = a.concat(this.leftAdvFilter);
		}
		if( this.leftAdvUIFilter ){
			a = a.concat(this.leftAdvUIFilter);
		}
		if( a.length > 0 ) {
			this.storeLeft.proxy.extraParams[Main.requestParam.ADVANCED_FILTER] = Ext.encode(a);
		}

		lp[Main.requestParam.START] = 0;
		lp[Main.requestParam.SIZE] = this.tuning.fetchSize;

		this.storeLeft.load({
			params : lp
		});
		return true;
	},

	doQueryRightImpl : function() {

		this.storeRight.removeAll();
		var lp = {};
		var data = {};
		var params = {};

		this.storeRight.proxy.extraParams = Ext.apply({}, this.params);
		this.storeRight.currentPage = 1;

		if (this.rightFilter) {
			Ext.apply(data, this.rightFilter);
		}
		if (this.rightParams) {
			Ext.apply(params, this.rightParams);
		}

		this.storeRight.proxy.extraParams[Main.requestParam.FILTER] = Ext.encode(data);
		this.storeRight.proxy.extraParams[Main.requestParam.PARAMS] = Ext.encode(params);
		
		var a = [];
		if( this.rightAdvFilter ){
			a = a.concat(this.rightAdvFilter);
		}
		if( this.rightAdvUIFilter ){
			a = a.concat(this.rightAdvUIFilter);
		}
		if( a.length > 0 ) {
			this.storeRight.proxy.extraParams[Main.requestParam.ADVANCED_FILTER] = Ext.encode(a);
		}

		lp[Main.requestParam.START] = 0;
		lp[Main.requestParam.SIZE] = this.tuning.fetchSize;

		this.storeRight.load({
			params : lp
		});
		return true;
	},

	doSaveImpl : function() {
		Ext.Ajax.request({
			params : this.params,
			method : "POST",
			failure : this.afterAjaxFailure,
			success : this.afterDoSaveSuccess,
			scope : this,
			url : Main.urlAsgn + "/" + this.dsName + ".json?" + Main.requestParam.ACTION + "=" + Main.asgnAction.SAVE,
			options : {
				action : "doSave",
				fnSuccess : null,
				fnSuccessScope : null,
				fnFailure : null,
				fnFailureScope : null,
				serviceName : name
			}
		});
		Ext.Msg.progress('Saving...');
	},

	doResetImpl : function() {
		Ext.Ajax.request({
			params : this.params,
			method : "POST",
			failure : this.afterAjaxFailure,
			success : this.afterDoResetSuccess,
			scope : this,
			url : Main.urlAsgn + "/" + this.dsName + ".json?" + Main.requestParam.ACTION + "=" + Main.asgnAction.RESET,
			options : {
				action : "doReset",
				fnSuccess : null,
				fnSuccessScope : null,
				fnFailure : null,
				fnFailureScope : null,
				serviceName : name
			}
		});
	},

	doCleanupImpl : function() {
		// remove data from client list / cancel current running load
		this.storeLeft.removeAll();
		this.storeRight.removeAll();
		// clean on server side
		Ext.Ajax.request({
			params : this.params,
			method : "POST",
			failure : this.afterAjaxFailure,
			scope : this,
			url : Main.urlAsgn + "/" + this.dsName + ".json?" + Main.requestParam.ACTION + "=" + Main.asgnAction.CLEANUP
		});
		this.setDirty(false);
	},
	
	createStore : function(side) {
		
		return Ext.create("Ext.data.BufferedStore", {
			id: Ext.id(),
			model : this.recordModel,
			remoteSort : true,
			autoLoad : false,
			autoSync : false,
			clearOnPageLoad : true,
			pageSize : this.tuning.fetchSize,
			leadingBufferZone : 2*this.tuning.fetchSize,
			trailingBufferZone : this.tuning.fetchSize,
			proxy : {
				type : 'ajax',
				api : Main["asgn" + side + "API"](this.dsName, "json"),
				model : this.recordModel,
				timeout : Main.ajaxTimeout,
				extraParams : {
					params : {}
				},
				actionMethods : {
					create : 'POST',
					read : 'POST',
					update : 'POST',
					destroy : 'POST'
				},
				reader : {
					type : 'json',
					rootProperty : 'data',
					idProperty : 'id',
					totalProperty : 'totalCount',
					messageProperty : 'message'
				},
				writer : {
					type : 'json',
					encode : true,
					allowSingle : false,
					writeAllFields : true
				},
				listeners : {
					"exception" : {
						fn : this.proxyException,
						scope : this
					}
				},
				startParam : Main.requestParam.START,
				limitParam : Main.requestParam.SIZE,
				sortParam : Main.requestParam.ORDERBY
			}
		});
	},
	
	/** ********************************************** */
	/** *********** MISCELLANEOUS HELPERS ************* */
	/** ********************************************** */

	afterAjaxFailure : function(response) {
		Main.serverMessage(null, response);
	},

	proxyException : function(dataProxy, response) {
		Main.serverMessage(null, response);
	}

});
