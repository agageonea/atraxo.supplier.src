/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

/**
 * Window component using for wizard dialogs 
 */
Ext.define("e4e.ui.WizardWindow", {
	extend : "Ext.window.Window",
	alias: 'widget.wizardwindow',
	
	closeAction : "hide", 
	resizable : false, 
	layout : "fit", 
	modal : true,
	
	_frame_ : null,
	_labelPanelName_ : null,
	_stepPanels_ : [],
	_idStepsPanel_ : null,
	_crtStep_ : 0,
	_maxSteps_ : 0,
	
	initComponent: function(config){

		this._idStepsPanel_ = Ext.id();
		
		var labels = this._frame_._elems_.get(this._labelPanelName_);
		labels._windowWidth_ = this.width;

		this.items = [ {
			layout : { type: "border" },
			defaults : { split : false },
			items : [ 
			{
				region : "north",
				layout : { type: "card" },
				items : [labels]
			}, 
			{
				id : this._idStepsPanel_,
				region : "center",
				layout : { type: "card"	},
				items : this._getStepPanelsConfig_()
			} 
			]
		} ];
		
		this.callParent(arguments);
		this.on("show", function(){
			this.setStep(0);
			this._disableFirstNavItemFocus_();
		}, this);
	},

	_disableFirstNavItemFocus_ : function(){
		var sp = this._getStepsPanel_();
		var n = sp.items.length;
		for(var i=0;i<n;i++){
			var it = sp.items.get(i);
			if( it && it._disableFirstNavItemFocus_ ){
				it._disableFirstNavItemFocus_();
			}
		}
	},

	_getStepPanelsConfig_ : function(){
		var res = [];
		var i; 
		this._maxSteps_ = this._stepPanels_.length;
		for(i=0;i<this._maxSteps_;i++){
			res.push(this._frame_._elems_.get(this._stepPanels_[i]));
		}
		return res;
	},
	
	_stepsPanelObj_ : null,
	_getStepsPanel_ : function() {
		if( !this._stepsPanelObj_ ){
			this._stepsPanelObj_ = Ext.getCmp(this._idStepsPanel_);
		}
		return this._stepsPanelObj_;
	},
	
	_labelPanelObj_ : null,
	_getLabelPanel_ : function(){
		if( !this._labelPanelObj_ ){
			this._labelPanelObj_ = this._frame_._getElement_(this._labelPanelName_);
		}
		return this._labelPanelObj_;
	},
	
	_updateSteps_ : function(){
		var sp = this._getStepsPanel_();
		var ai = this._crtStep_-1;
		if( ai < 0 ){
			ai = 0;
		}
		sp.setActiveItem(ai);
		var lp = this._getLabelPanel_();
		lp.setStep(this._crtStep_);
		var f = sp.items.get(ai);
		if( f && f._gotoFirstNavigationItem_ ){
			Ext.defer(f._gotoFirstNavigationItem_,200,f);
		}
	},
	
	nextStep : function(){
		if( this._crtStep_ < this._maxSteps_ ){
			this._crtStep_++;
			this._updateSteps_();
		}
		return this._maxSteps_;
	},
	
	prevStep : function(){
		if( this._crtStep_ > 0 ){
			this._crtStep_--;
			this._updateSteps_();
		}
		return this._maxSteps_;
	},
	
	getStep : function(){
		return this._crtStep_;
	},
	
	// step is working from 0 to panel numbers
	setStep : function(v){
		if( v >= 0 && v <= this._maxSteps_ ){
			this._crtStep_ = v;
			this._updateSteps_();
		}
		return this._crtStep_;
	}

});
