/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

/**
 * This class is inherits all of the methods from AbstractCombo, but the behavior is
 * very different on the mapped fields when more than one record is selected. So, be 
 * careful where and how it is used. It's can be used to replace the 'normal' LOV only 
 * when multiple selection is disabled.
 * Recommended usage: when need to assign more values (based on the displayed field) 
 * to a record. The values are separated with comma (by default) and in the server 
 * side custom BL functions can be made the operations in database tables.
 */
Ext.define("e4e.lov.LovAsgn", {
	
	extend : "Ext.form.field.Tag",
	alias : "widget.xlovasgn",

	delimiter : ',',
	filterPickList : true,
	cls: "sone-assign-lov",
	maxHeight : 56,

	// Properties

	/** 
	 * 
	 * Link to the dc-view fields to filter the records in this combo.
	 * 
	 * Example: ,filterFieldMapping: [{lovField:"...lovFieldName", dsField:
	 * "...dsFieldName"} ]
	 * 
	 * Or: ,filterFieldMapping: [{lovField:"...lovFieldName", value: "...static
	 * value"} ]
	 */
	filterFieldMapping : null,
	
	advFilter : null,

	/**
	 * Specify what values should this combo return to the dc-record.
	 * 
	 * @type Array
	 */
	retFieldMapping : null,

	_dataProviderFields_ : null,
	_dataProviderName_ : null,
	_dummyValue_ : null,
	_editFrame_ : null,
	openFrame : null,
	_isLov_ : true, // used in framework
	_targetRecord_ : null,

	/**
	 * Data-control view type this field belongs to. Injected by the
	 * corresponding view builder.
	 */
	_dcView_ : null,

	/**
	 * Data model signature - record constructor.
	 * 
	 * @type Ext.data.Model
	 */
	recordModel : null,

	recordModelFqn : null,
	/**
	 * Parameters model signature - record constructor.
	 * 
	 * @type Ext.data.Model
	 */
	paramModel : null,
	/**
	 * Parameters model instance
	 */
	params : null,

	// defaults
	triggerAction : "all",
//	matchFieldWidth : false,
	pageSize : Main.viewConfig.FETCH_SIZE_LOV,
	autoSelect : false,
	minChars : 0,
	queryMode : "remote",
	queryCaching : false, // for auto-complete behavior
	storeAutoLoad : false, 
	typeAhead: true,

	//trigger1Cls : Ext.baseCSSPrefix + 'x-form-trigger',

	defaultListConfig : {
		minWidth : 250,
		width : 250,
		shadow : "sides",
		autoScroll : true
	},

	autoScroll : true,

	_localCache_ : null,
	_initialQueryRunning_ : null,
	_initialQueryStarting_ : null,
	_columns_ : null,  

	initComponent : function() {
		
		if( this._localCache_ ){
			this.pageSize = Main.viewConfig.FETCH_SIZE_UNLIMITED;
			this.typeAhead = false;
		}
		
		this._createStore_();
		if (!Ext.isEmpty(this.paramModel)) {
			this.params = Ext.create(this.paramModel, {});
		}
		if (this.retFieldMapping == null) {
			this.retFieldMapping = [];
		}
		if (this.dataIndex) {
			this.retFieldMapping[this.retFieldMapping.length] = {
				lovField : this.displayField,
				dsField : this.dataIndex
			};
		} else if (this.paramIndex) {
			this.retFieldMapping[this.retFieldMapping.length] = {
				lovField : this.displayField,
				dsParam : this.paramIndex
			};
		}
		
		if( !this._columns_ ){
			// if columns info is not defined, add the displayField into the array
			this._columns_ = [this.displayField];
		}
		
		this.on("select", this.assertValueMy, this);
		this.on("blur", function(){
			this.assertValueMy();
		}, this);
		this.on("specialkey", function(field, e){
			if (e.getKey() === e.ESC) {
				this.setValue(this._onFocusVal_);
            }
		}, this);
		
		this.callParent(arguments);

		if( this._localCache_ ){

			Ext.defer(function(){
				this._initialQueryStarting_ = true;
				if( this.doQuery(null, true, true) ){
					this._initialQueryRunning_ = true;
					var fAfterLoad = function(){
						this._initialQueryRunning_ = false;
						this.queryMode = "local";
						this.queryDelay = 100;
						this.minChars = 0;
					};
					this.store.on("load", fAfterLoad, this, {single:true});
					this.store.proxy.on("exception", function(){this._initialQueryRunning_ = false;}, this, {single:true});
				}
				this._initialQueryStarting_ = false;
			}, 100, this, []);
		} else {
			var fAfterLoad = function(){
				this._highlightRecord_(this._searchForRecord_());
			};
			this.store.on("load", fAfterLoad, this);
		}
	},

	_searchForRecord_: function(){
		var record = null;
		if(this.store.getCount()>0){
			record = this.store.findRecord(this.displayField, this.getRawValue());
			if( !record ){
				record = this.store.getAt(0);
			}
		}
		return record;
	},
	_highlightRecord_: function(record){
		if(record){
			var boundList = this.getPicker();
			var navModel = boundList.getNavigationModel();
			boundList.highlightItem(boundList.getNode(record));
			var idx = boundList.indexOf(boundList.highlightedItem);
			navModel.setPosition(idx);
		}
	},
	
    onTypeAhead : function() {
    	this._highlightRecord_(this._searchForRecord_());
	},
    
	expand : function(){
		if( this._initialQueryRunning_ === true || this._initialQueryStarting_ === true ){
			return;
		}
		this.callParent(arguments);
	},

	onTrigger2Click : function() {
		if (this._editFrame_ == null) {
			alert("No destination frame specified.");
			return;
		} else {
			if (this._editFrame_.custom === undefined || this._editFrame_.custom === null) {
				this._editFrame_.custom = false;
			}
		}
		getApplication().showFrame(this._editFrame_.name, {
			url : Main.buildUiPath(this._editFrame_.name),
			tocElement : this._editFrame_.tocElement
		});
	},

	_createStore_ : function() {
		if (this._dataProviderName_ == null) {
			if (Ext.isFunction(this.recordModel)) {
				this.recordModelFqn = this.recordModel.$className;
			} else {
				this.recordModel = Ext.ClassManager.get(this.recordModel)
				this.recordModelFqn = this.recordModel.$className;
			}
			// this._dataProviderName_ = this.recordModelFqn.substring(
			// this.recordModelFqn.lastIndexOf('.') + 1,
			// this.recordModelFqn.length);
			this._dataProviderName_ = this.recordModel.ALIAS;
		}
		this.store = Ext.create("Ext.data.Store", {
			model : this.recordModel,
			remoteSort : true,
			autoLoad : this.storeAutoLoad,
			autoSync : false,
			clearOnPageLoad : true,
			pageSize : this.pageSize,
			proxy : {
				type : 'ajax',
				api : Main.dsAPI(this._dataProviderName_, "json"),
				model : this.recordModel,
				timeout : Main.ajaxTimeout,
				extraParams : {
					params : {}
				},
				actionMethods : {
					create : 'POST',
					read : 'POST',
					update : 'POST',
					destroy : 'POST'
				},
				reader : {
					type : 'json',
					rootProperty : 'data',
					idProperty : 'id',
					totalProperty : 'totalCount',
					messageProperty : 'message'
				},
				listeners : {
					"exception" : {
						fn : this.proxyException,
						scope : this
					}
				},
				startParam : Main.requestParam.START,
				limitParam : Main.requestParam.SIZE,
				sortParam : Main.requestParam.SORT,
				directionParam : Main.requestParam.SENSE
			}
		});
	},

	// Tibi: E5
	__inEditor__ : null,
	inEditor : function(){
		if(this.__inEditor__ === null){
			var dcv = this._dcView_;
			if( dcv ){
				var vt = dcv._dcViewType_;
				this.__inEditor__ = (vt === "edit-grid" || vt === "filter-propgrid" || vt === "edit-propgrid" || vt === "bulk-edit-field");
			} else {
				return false;
			}
		}
		return this.__inEditor__;
	},
	
	_getTargetRecord_ : function() {
		var drec = null;
		var dcv = this._dcView_;
		var vt = dcv._dcViewType_;
		var ctrl = dcv._controller_;

		if (this.inEditor() && vt !== "edit-propgrid" && vt !== "filter-propgrid") {
			if (dcv && vt === "bulk-edit-field") {
				/*
				 * is a bulk editor for one ds field in a property grid
				 */
				drec = dcv.getSource();
			} else {
				drec = this._targetRecord_;
			}
		} else {
			if (vt === "edit-form" || vt === "edit-propgrid") {
				drec = ctrl.getRecord();
			} else if (vt === "filter-form" || vt === "filter-propgrid") {
				drec = ctrl.getFilter();
			}
		}
		return drec;
	},

	/**
	 * Map fields from the combo-record received as argument (crec) to the
	 * target record according to fieldMappings
	 * 
	 * @param {}
	 *            crec Combo selected record
	 */
	_mapReturnFields_ : function(crec) {
		var dcv = this._dcView_;
		var vt = dcv._dcViewType_;
		var ctrl = dcv._controller_;
		var drec = null;
		var prec = ctrl.getParams();
		var targetIsFilter = false;

		if (this.inEditor() && vt !== "edit-propgrid" && vt !== "filter-propgrid") {
			if (dcv && vt === "bulk-edit-field") {
				/*
				 * is a bulk editor for one ds field in a property grid
				 */
				drec = dcv.getSource();
				this._mapReturnFieldsExecuteBulkEdit_(crec, drec);
			} else {
				drec = this._targetRecord_;
				this._mapReturnFieldsExecute_(crec, drec, prec);
			}
		} else {
			if (vt === "edit-form" || vt === "edit-propgrid") {
				drec = ctrl.getRecord();
			}
			if (vt === "filter-form" || vt === "filter-propgrid") {
				drec = ctrl.getFilter();
				targetIsFilter = true;
			}
			this._mapReturnFieldsExecute_(crec, drec, prec, targetIsFilter);
		}
	},

	/**
	 * Params:<br>
	 * crec: Combo selected record <br>
	 * drec - Controller data-record. <br>
	 * The current record or current filter based on the view-type context <br>
	 * prec - Controller params record
	 */
	_mapReturnFieldsExecute_ : function(crec, drec, prec, targetIsFilter) {
		if (!drec) {
			return;
		}
		if (this.retFieldMapping != null) {
			var nv, ov, isParam, rawv = this.getRawValue();

			for (var i = this.retFieldMapping.length - 1; i >= 0; i--) {
				var retDataIndex;
				isParam = !Ext.isEmpty(this.retFieldMapping[i]["dsParam"]);
				if (isParam) {
					retDataIndex = this.retFieldMapping[i]["dsParam"];
					ov = prec.get(retDataIndex);
				} else {
					retDataIndex = this.retFieldMapping[i]["dsField"];
					ov = drec.get(retDataIndex);
				}
				if (crec && crec.data) {
					nv = crec.data[this.retFieldMapping[i]["lovField"]];
					if (nv != ov) {
						if (isParam) {
							this._dcView_._controller_.setParamValue(retDataIndex, nv);
						} else {
							if (targetIsFilter) {
								this._dcView_._controller_.setFilterValue(retDataIndex, nv);
							} else {
								drec.set(retDataIndex, nv);
							}
						}
					}
				} else {
					if (retDataIndex === this.dataIndex) {
						if (this._validateListValue_ && rawv != ov) {
							rawv = null;
							this.setRawValue(rawv);
						}
						if (rawv != ov) {
							if (isParam) {
								this._dcView_._controller_.setParamValue(retDataIndex, rawv);
							} else {
								if (targetIsFilter) {
									this._dcView_._controller_.setFilterValue(retDataIndex, rawv);
								} else {
									drec.set(retDataIndex, rawv);
								}
							}
						}
					} else {
						if ((ov !== null && ov !== "")) {
							if (isParam) {
								this._dcView_._controller_.setParamValue(retDataIndex, null);
							} else {
								if (targetIsFilter) {
									this._dcView_._controller_.setFilterValue(retDataIndex, null);
								} else {
									drec.set(retDataIndex, null);
								}
							}
						}
					}
				}
			}
		}
	},

	_mapReturnFieldsExecuteBulkEdit_ : function(crec, recdata) {
		if (!recdata) {
			return;
		}
		if (this.retFieldMapping != null) {
			for (var i = this.retFieldMapping.length - 1; i >= 0; i--) {
				var retDataIndex;
				var nv;
				var isParam = !Ext.isEmpty(this.retFieldMapping[i]["dsParam"]);
				if (isParam) {
					retDataIndex = this.retFieldMapping[i]["dsParam"];
				} else {
					retDataIndex = this.retFieldMapping[i]["dsField"];
				}

				if (crec && crec.data) {
					nv = crec.data[this.retFieldMapping[i]["lovField"]];
					recdata[retDataIndex] = nv;
				}
			}
		}
	},

	_mapFilterFields_ : function(bp) {

		var drec = null;
		var dcv = this._dcView_;
		var vt = dcv._dcViewType_;
		var prec = dcv._controller_.getParams();

		if (this.inEditor()) {
			drec = this._targetRecord_;
			return this._mapFilterFieldsExecute_(bp, drec, prec);
		} else {
			if (vt === "edit-form") {
				drec = dcv._controller_.getRecord();
			}
			if (vt === "filter-form") {
				drec = dcv._controller_.getFilter();
			}
			return this._mapFilterFieldsExecute_(bp, drec, prec);
		}
	},

	/**
	 * Parameters: bp: base params for the store
	 * 
	 * drec - Controller data-record. The current record or current filter based
	 * on the view-type context prec - Controller params record
	 */
	_mapFilterFieldsExecute_ : function(bp, drec, prec) {
		if (!drec) {
			return;
		}
		var _view = this._dcView_;
		if (this.filterFieldMapping != null) {
			var len = this.filterFieldMapping.length;
			for (var i = 0; i < len; i++) {

				var _ffm = this.filterFieldMapping[i];
				var _lp = _ffm["lovParam"];
				var _lf = _ffm["lovField"];

				var _dsp = _ffm["dsParam"];
				var _dsf = _ffm["dsField"];

				var _v = _ffm["value"];
				var _nn = _ffm["notNull"];

				var isLovMemberParam = !Ext.isEmpty(_lp);
				var _val;

				if (_v !== undefined && _v !== null) {
					_val = _v;
				} else {
					if (!Ext.isEmpty(_dsp)) {
						_val = prec.get(_dsp)
					} else {
						_val = drec.get(_dsf)
					}
				}

				if (Ext.isEmpty(_val)) {
					_val = "";
					if (_nn === true) {
						if (_view) {
							_view._controller_.info("Select the context value for this list of values field.");
							return false;
						} else {
							alert("Select the context value for this list of" + "values field.");
						}
					}
				}

				if (isLovMemberParam) {
					this.params.set(_lp, _val);
				} else {
					bp[_lf] = _val;
				}

			}
		}
	},

	/**
	 * Default proxy-exception handler
	 */
	proxyException : function(proxy, response, operation, eOpts) {
		this.showAjaxErrors(response, eOpts);
	},

	/**
	 * Show errors to user. 
	 */
	showAjaxErrors : function(response) {
		Main.serverMessage(null,response);
	},

	// **************************************************
	// *********************** OVERRIDES ****************
	// **************************************************
    doLocalQuery: function(queryPlan) {
		var me = this,
	    	queryString = queryPlan.query,
	        store = me.getStore(),
            filter = me.queryFilter;

        me.queryFilter = null;

        // Must set changingFilters flag for this.checkValueOnChange.
        // the suppressEvents flag does not affect the filterchange event
        me.changingFilters = true;
        if (filter) {
            store.removeFilter(filter, true);
        }

        // Querying by a string...
        if (queryString) {
        	var _FC_ = this._columns_;
        	var _QS_ = (""+queryString).toUpperCase();
            filter = me.queryFilter = new Ext.util.Filter({
                filterFn: function(item) {
                	for(var i=0;i<_FC_.length;i++){
                		var v = ""+item.get(_FC_[i]);
                		if( v.toUpperCase().indexOf(_QS_) >= 0 ){
                			return true;
                		}
                	}
                	return false;
            	}
            });
            store.addFilter(filter, true);
        }
		me.changingFilters = false;

		// Expand after adjusting the filter if there are records or if emptyText is configured.
		if (me.store.getCount() || me.getPicker().emptyText) {
			// The filter changing was done with events suppressed, so
			// refresh the picker DOM while hidden and it will layout on show.
			me.getPicker().refresh(); 
			me.expand();
			this._highlightRecord_(this._searchForRecord_());
        } else {
			me.collapse();
		}

		me.afterQuery(queryPlan);
    },
    
	_getAdvFilter_ : function(queryPlan) {

		var af = [];
		var i, o;
		
		if( this.advFilter ){
			// preapre advanced filter set at LOV usage
			var dc = this._dcView_._controller_;
			if( dc ){
				var r = dc.getRecord();
				for(i=0;i<this.advFilter.length;i++){
					var f = this.advFilter[i];
					if( r || f.value1 ){
						o = {
							"id":null,
							"fieldName":f.lovFieldName,
							"operation":f.operation,
							"value1": !Ext.isEmpty(f.value1)?f.value1:r.get(f.fieldName1),
							"groupOp":"AND"+i
						};
						af.push(o);
					}
				}
			}
		}
		
		// prepare advanced filter generated at LOV definition
		for(i=0;i<this._columns_.length;i++){
			o = {
				"id":null,
				"fieldName":this._columns_[i],
				"operation":"like",
				"value1": "%"+queryPlan.query+"%",
				"groupOp":"OR1"
			};
			af.push(o);
		}
		return af;
	},
	
	doRemoteQuery : function(queryPlan) { 
		if( this._initialQueryRunning_ === true ){
			return;
		}
		var bp = {};
		var extraParams = this.store.proxy.extraParams;
		if( this._columns_.length <= 0 ){
			bp[this.displayField] = queryPlan.query + "*";
		}
		if (this.filterFieldMapping != null) {
			if (this._mapFilterFields_(bp) === false) {
				return false;
			}
			this.queryCaching = false;
		}
		extraParams[Main.requestParam.ADVANCED_FILTER] = Ext.encode(this._getAdvFilter_(queryPlan));
		extraParams[Main.requestParam.FILTER] = Ext.encode(bp);
		if (this.params != null) {
			extraParams[Main.requestParam.PARAMS] = Ext.encode(this.params.data);
		}
		this.callParent(arguments);
	},

	assertValueMy : function() {
		var me = this;
		var val = me.getRawValue(), rec;

		rec = this.findRecord(this.displayField, val);
		if(!rec){
			rec = this.valueCollection.find(this.displayField, val);
			if(rec && val == rec.get(this.displayField)) {
				return;
			}
		}

		if (rec) {
			this._mapReturnFields_(rec);
			if (val == rec.get(this.displayField) && this.value == rec.get(this.valueField)) {
				return;
			}
			val = rec.get(this.valueField || this.displayField);

			if (this.getRawValue() != val) {
				this.setValue(val);
			}
		} else {
			if (this.forceSelection) {

				if (val != this._onFocusVal_) {
					this.setRawValue(null);
					if (val.length > 0 && val !== this.emptyText) {
						this.applyEmptyText();
					} else {
						this.clearValue();
					}
					this._mapReturnFields_(null);
				}
			} else {
				if (Ext.isEmpty(val)) {
					this.setRawValue(null);
					if (val.length > 0 && val !== this.emptyText) {
						this.applyEmptyText();
					} else {
						this.clearValue();
					}
					this._mapReturnFields_(null);
				}
			}
		}
	},

	getValue: function() {
		// If the user has not changed the raw field value since a value was selected from the list,
		// then return the structured value from the selection. If the raw field
		// value is different
		// than what would be displayed due to selection, return that raw value.
		var me = this, picker = me.picker, rawValue = me.getRawValue(), // current value of text field
		value = me.value; // stored value from last selection or setValue() call

		if (me.getDisplayValue() !== rawValue) {
			me.displayTplData = undefined;
			if (picker) {
				// We do not need to hear about this clearing out of the value
				// collection, so suspend events.
				me.valueCollection.suspendEvents();
				picker.getSelectionModel().deselectAll();
				me.valueCollection.resumeEvents();
			}
			// If the raw input value gets out of sync in a multiple ComboBox,
			// then we have to give up.
			// Multiple is not designed for typing *and* displaying the comma
			// separated result of selection.
			// Same in the case of forceSelection.
			if (me.multiSelect || me.forceSelection) {
				value = me.value = undefined;
			} else {
				value = me.value = rawValue;
			}
		}

		// Return null if value is undefined/null, not falsy.
		me.value = value == null ? null : value;
		return me.value;
	}
});
