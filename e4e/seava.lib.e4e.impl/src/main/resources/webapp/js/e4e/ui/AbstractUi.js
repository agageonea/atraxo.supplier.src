/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.ui.AbstractUi", {
	extend : "Ext.panel.Panel",

	mixins : {
		elemBuilder : "e4e.base.Abstract_View"
	},

	// **************** Properties *****************

	/**
	 * Component builder
	 */
	_builder_ : null,

	/**
	 * Data-controls map
	 */
	_dcs_ : null,

	/**
	 * Data-control code which is treated as the current root dc. Used mainly
	 * when keyboard shortcut invoked from the main application context on the
	 * current frame, to delegate the action to be executed.
	 */
	_rootDc_ : null,

	/**
	 * Toolbar definitions map
	 */
	_tlbs_ : null,

	/**
	 * Toolbar items definitions map
	 */
	_tlbitms_ : null,

	/**
	 * Toolbar titles map. Used to retrieve the title element from a toolbar in
	 * a later stage, for example by the FrameInspector
	 */
	_tlbtitles_ : null,

	/**
	 * Frame monitoring objects. Used to monitor events from DCs from other frame instances.
	 */
	_frameMonList_ : null,
	
	/**
	 * Custom reports attached to this frame. It is an array of configuration
	 * objects with the following attributes: toolbar: the name of toolbar where
	 * the link to this report should be attached report: code of the report
	 * used to lookup the definition and rules to be invoked. title : title of
	 * the report to display in the menu
	 */
	_reports_ : null,

	/**
	 * Translations class
	 */
	_trl_ : null,

	/**
	 * Buttons state rules. Map with functions executed in frame context to
	 * apply enabled/disabled visible/hidden state to a button. Usually the
	 * framework invokes this functions whenever a state change occurs in the
	 * frame like record change in a data-control, save, selection, etc but it
	 * can be called programmatically also whenever is necessary.
	 * 
	 * The function is called with a DC as single argument
	 * 
	 */
	_buttonStateRules_ : {},

	/**
	 * Frame status bar
	 */
	_statusBar_ : null,

	/**
	 * Assignments called in this frame. This collects an array of assignment
	 * descriptors to be listed in the frame descriptor. As these components are
	 * not explicitely listed as elements, they are registerd here by the
	 * _showAsgnWindow_ function.
	 * 
	 */
	_asgns_ : null,

	// ************** to be reviewed

	/**
	 * Injected configuration variables map
	 */
	_configVars_ : null,

	/**
	 * Actions map
	 */
	_actions_ : null,

	_name_ : null,

	/**
	 * Frame title
	 */
	_title_ : null,
	_glyph_ : null,

	_header_ : null,

	/**
	 * The list of grids with view and filter status bar
	 */
	_gridsWithFilter_ : null,

	/**
	 * Define the frame parameters
	 */
	_frameParameters_ : {},
	_kpiParameters_ : [],
	
	/**
	 * frameOpenParams are set in _onReady_; the value are get from application
	 */
	frameOpenParams : {},
	
	/**
	 * user defined fields
	 */
	_userFieldsCfg_ : null,
	
	// **************** Public API *****************

	/**
	 * Get a data-control instance.
	 * 
	 * @param {String}
	 *            name
	 * @return {}
	 */
	_getDc_ : function(name) {
		return this._dcs_.get(name);
	},

	/**
	 * 
	 */
	_getRootDc_ : function() {
		if (this._rootDc_) {
			return this._dcs_.get(this._rootDc_);
		} else {
			return this._dcs_.getAt(0);
		}
	},

	/**
	 * Get a toolbar instance.
	 * 
	 * @param {String}
	 *            name
	 * @return {}
	 */
	_getToolbar_ : function(name) {
		var tlb = this._tlbs_.get(name);
		if( tlb && tlb._view_ ){
			var vw = Ext.getCmp(tlb._view_.id);
			if( vw ){
				var di = vw.getDockedItems('toolbar[dock="top"]');
				for(var i=0, l=di.length; i<l; i++){
					if( di[i].config.items === tlb._actions_ ){
						return di[i];
					}
				}
			}
		}
		return null;
	},

	/**
	 * Get a toolbar's configuration.
	 * 
	 * @param {String}
	 *            name
	 * @return {Object}
	 */
	_getToolbarConfig_ : function(name) {
		return this._tlbs_.get(name);
	},

	/**
	 * Get a toolbar item instance.
	 * 
	 * @param {String}
	 *            name
	 * @return {}
	 */
	_getToolbarItem_ : function(name) {
		return Ext.getCmp(this._tlbitms_.get(name).id);
	},

	/**
	 * Get a toolbar item's configuration object.
	 * 
	 * @param {String}
	 *            name
	 * @return {Object}
	 */
	_getToolbarItemConfig_ : function(name) {
		return this._tlbitms_.get(name);
	},

	/**
	 * Factory method to create the data-control instances used in this frame.
	 */
	_defineDcs_ : function() {
	},

	/**
	 * Template method checked during elements definition flow. Return false to
	 * skip the _defineDcs_ call.
	 * 
	 * @return {Boolean}
	 */
	_beforeDefineDcs_ : function() {
		return true;
	},

	/**
	 * Template method invoked after the elements definition flow. Used mainly
	 * to add overrides to existing components.
	 */
	_afterDefineDcs_ : function() {
	},

	/**
	 * Template method invoked after _afterDefineDcs_ to add overrides to
	 * existing components.
	 */
	_afterAfterDefineDcs_ : function() {
	},

	/**
	 * Custom rules/functions can be defined to control the enable/disable state of the data-control.
	 * These rules are registered in this function.
	 */
	_registerDcsInStateManager_ : function(){
	},

	/**
	 * Factory method to create the toolbars
	 */
	_defineToolbars_ : function() {
	},

	/**
	 * Template method checked during toolbarss definition flow. Return false to
	 * skip the _defineToolbars_ call.
	 * 
	 * @return {Boolean}
	 */
	_beforeDefineToolbars_ : function() {
		return true;
	},

	/**
	 * Template method invoked after the toolbar definition.
	 */
	_afterDefineToolbars_ : function() {
	},

	/**
	 * Link toolbars to view components.
	 * 
	 * @param {String}
	 *            tlbName Toolbar name
	 * @param {String}
	 *            viewName View name
	 */
	_linkToolbar_ : function(tlbName, viewName) {
		this._linkToolbarImpl_(tlbName, viewName)
	},
	
	
	/**
	 * Factory method to populate frame monitoring list in this frame.
	 */
	_defineFrameMonList_ : function() {
	},

	/**
	 * Template method checked during the frame monitoring list creation. Return false to skip the _defineFrameMonList_ call.
	 * 
	 * @return {Boolean}
	 */
	_beforeDefineFrameMonList_ : function() {
		return true;
	},

	/**
	 * Template method invoked after the frame monitoring list populated. Used mainly to add overrides to existing components.
	 */
	_afterDefineFrameMonList_ : function() {
	},
	
	/**
	 * Returns the builder associated with this type of component. If it doesn't
	 * exist yet attempts to create it.
	 * 
	 * @return {e4e.ui.FrameBuilder}
	 */
	_getBuilder_ : function() {
		if (this._builder_ == null) {
			this._builder_ = new e4e.ui.FrameBuilder({
				frame : this
			});
		}
		return this._builder_;
	},

	/**
	 * TOC stands for table of contents. Used when grouping multiple independent
	 * data-controls into one frame. Activates the canvas associated with the
	 * specified TOC element name.
	 * 
	 * @param {String}
	 *            name TOC element name
	 */
	_showTocElement_ : function(name) {
		var r, theToc
		if (Ext.isNumber(name)) {
			theToc = this._getElement_("_toc_").items.items[0];
			r = theToc.store.getAt(name);
			theToc.getSelectionModel().select(r);
		} else {
			theToc = this._getElement_("_toc_").items.items[0];
			r = theToc.store.findRecord("name", name);
			theToc.getSelectionModel().select(r);
		}

	},

	/**
	 * Programmatically invoke a toolbar item action.
	 * 
	 * @param {String}
	 *            name Toolbar-item name
	 * @param {String}
	 *            tlbName Toolbar name
	 */
	_invokeTlbItem_ : function(name, tlbName) {
		var b;
		if (!tlbName) {
			b = this._tlbitms_.get(name);
		} else {
			b = this._tlbitms_.get(tlbName + "__" + name);
		}
		if (b) {
			b.execute();
		}
	},

	/**
	 * Opens an assignment window component.
	 * 
	 * @param {String}
	 *            asgnWdwClass
	 * @param {Object}
	 *            cfg Extra configuration to apply
	 */
	_showAsgnWindow_ : function(asgnWdwClass, cfg, ctx) {
		var context = this;
		if (ctx) {
			context = ctx;
		}
		var _recData = context._dcs_.get(cfg.dc).record.data;
		var objectId = _recData[cfg.objectIdField];
		var objectDesc = "";
		if (!Ext.isEmpty(cfg.objectDescField)) {
			objectDesc = _recData[cfg.objectDescField];
		} else {
			if (_recData.hasOwnProperty("name")) {
				objectDesc = _recData["name"];
			}
		}

		var aw = Ext.create(asgnWdwClass, cfg);

		aw._controller_.params.objectId = objectId;

		aw._controller_.leftFilter = cfg.leftFilter;
		aw._controller_.rightFilter = cfg.rightFilter;
		aw._controller_.leftAdvFilter = cfg.leftAdvFilter;
		aw._controller_.rightAdvFilter = cfg.rightAdvFilter;
		aw._controller_.leftParams = cfg.leftParams;
		aw._controller_.rightParams = cfg.rightParams;
		
		if (context._asgns_ == null) {
			context._asgns_ = new Ext.util.MixedCollection();
		}
		if (!context._asgns_.containsKey(asgnWdwClass)) {
			context._asgns_.add(asgnWdwClass, {
				title : aw.title,
				className : asgnWdwClass
			});
		}
		aw.setTitle(aw.title + " | " + objectDesc);
		aw._controller_.initAssignement();
		aw.show();
	},

	// Private

	/**
	 * Add a toolbar to a view-element.
	 * 
	 * @param {}
	 *            tlbName
	 * @param {}
	 *            viewName
	 */
	_linkToolbarImpl_ : function(tlbName, viewName) {
		var tlb = this._tlbs_.get(tlbName);
		if (Ext.isEmpty(tlb)) {
			return;
		}
		var view = this._elems_.get(viewName);
		view["tbar"] = tlb._actions_;
		tlb._view_ = view;
	},

	/**
	 * Associate a grid with a toolbar to derive the _printTitle_ for the simple
	 * dynamic print list.
	 * 
	 * @param {}
	 *            dcvGridViewName
	 * @param {}
	 *            tlbName
	 */
	_setGridPrintTitle_ : function(dcvGridViewName, tlbName) {
		if (this._tlbtitles_ != null && this._tlbtitles_.containsKey(tlbName)) {
			var _t = this._tlbtitles_.get(tlbName);
			this._elems_.get(dcvGridViewName)["_printTitle_"] = _t;
		}
	},

	/**
	 * Template method invoked after the _onReady_ function is executed. Used
	 * mainly to add overrides to existing components.
	 */
	_afterOnReady_ : function() {},
	
	_beforeOnReady_ : function() {},
	
	_applyTabLoadRestriction_ : function() {},
	_applyContentLoadRestriction_ : function() {},
	_applyCustomStateManagers_ : function() {},

	_load_disabled_dc_rules_ : function() {
		try{
			if(_DISABLED_DC_RULES_) {
				// dc related actions
				this._dcs_.each(function(item) {
					var r = _DISABLED_DC_RULES_[item._instanceKey_];
					if( Ext.isObject(r) ){
						Ext.apply(item.disabledRules, r);
						item.updateDcState();
					}
				});
				
				// stand-alone buttons
				this._elems_.filterBy(function(o) {
					return o.xtype === "button";
				}).eachKey(function(key) { 
					var btn = this._getElement_(key);
					if (btn) {
						var r;
						if( btn.rpcDisabledRules ){
							r = _DISABLED_DC_RULES_[btn.dcRpcDisabledRules];
							if( Ext.isObject(r) && r[btn.rpcDisabledRules] === true ){
								btn._disabeldByRules_ = true;
								btn.disable();
							}
						}
						if( btn.actDisabledRules && btn._disabeldByRules_!==true ){
							r = _DISABLED_DC_RULES_[btn.dcActDisabledRules];
							if( Ext.isObject(r) && r[btn.actDisabledRules] === true ){
								btn._disabeldByRules_ = true;
								btn.disable();
							}
						}
					}
				}, this);
				
				// buttons on edit-forms
				this._elems_.filterBy(function(o) {
					return o.__dcViewType__ === "form";
				}).eachKey(function(key) {
					var form = this._getElement_(key);
					if( form && Ext.isFunction(form._load_disabled_dc_rules_) ){
						form._load_disabled_dc_rules_();
					}
				}, this);
			}
		}catch(e){
			//nothing to do
		}
	},
	
	_onReady_ : function() {
		var app = getApplication();
		var frameFqn = this.$className;
		this.frameOpenParams = app.getFrameOpenParameters(frameFqn);
		if( this.frameOpenParams.showWorking ){
			Main.working();
		}

		this._beforeOnReady_();

		this._applyCustomStateManagers_();
		
		app.setFrameTabTitle(frameFqn, this._title_);
		this._load_disabled_dc_rules_();
		this._dcs_.each(function(item) {
			item.updateDcState();
		});
		
		if ("" + Main.viewConfig.SHOW_VIEW_AND_FILTER_TOOLBAR !== "false") {
			this._setKpiAndTitle_();
		}

		var d = 1500;
		if( this.frameOpenParams.disableInitialQuery === true ){
			d = 10;
		}
		Ext.defer(this._initPhase2_,d,this);
		Ext.defer(app.applyFrameCallback,d+200,app,[frameFqn, this]);
		Ext.defer(this._configureUserFields_,d+300,this);
		Ext.defer(this.setInitializing,d+500,this,[false]);

		this._afterOnReady_();
		app.menu._appIsLoading_ = false;
	},

	_afterViewAndFilters_ : function() {},
	_config_ : function() {},
	_beforeViewAndFilterConfig_ : function() {},

	_createKeyBindingHandler_ : function(itemName, tlbName) {
		return function() {
			this._invokeTlbItem_(itemName, tlbName);
		}
	},

	_applyStateDc_ : function(obj) {
		if (obj.config ) {
			var sm = obj.config.stateManager;
			if( sm ) {
				for(var i=0;i<sm.length;i++){
					var bsr = this._buttonStateRules_[obj.smRuleName];
					var theDc = this._getDc_(sm[i].dc);
					var smstate = e4e.ui.FrameButtonStateManager["is_" + sm[i].name](theDc);
					if (smstate && bsr) {
						smstate = bsr.call(this, theDc);
					}
					if (smstate) {
						obj.dcMon.setDisabled(false);
					} else {
						obj.dcMon.setDisabled(true);
					}
				}
			}
		}
	},
	
	__verifyMenuBtnVisibility__ : function(btn,isVisible){
		if( btn.initialConfig._menuBtnId_ && !btn._menuBtn_ ){
			var mb = Ext.getCmp(btn.initialConfig._menuBtnId_);
			if( mb ){
				btn._menuBtn_ = mb;
			}
		}
		if( btn._menuBtn_ ){
			var bV = isVisible;
			var mbV = !btn._menuBtn_.isHidden();
			if( bV !== mbV ){
				// check if exists other items which are visible/invisible and set the menu button visibility
				if( bV ){
					btn._menuBtn_.setVisible(true);
				} else {
					var hasVisible = false;
					for(var i=0;i<btn._menuBtn_.menu.items.length;i++){
						if( !btn._menuBtn_.menu.items.getAt(i).isHidden() ){
							hasVisible = true;
							break;
						}
					}
					btn._menuBtn_.setVisible(hasVisible);
				}
			}
			btn.setVisible(isVisible);
		}
	},
	
	_applyStateButton_ : function(btnName,visibleAction) {
		var btn = this._getElement_(btnName);
		if (btn) {
			if( btn._disabeldByRules_ === true ){
				btn.disable();
			} else {
				var sm = btn.initialConfig.stateManager;
				if (sm) {
					var s = (visibleAction===true)?"_vr":"";
					var bsr = this._buttonStateRules_[btnName+s];
					for(var i=0;i<sm.length;i++){
						if( sm[i].visibleRule === visibleAction ){
							var theDc = this._getDc_(sm[i].dc);
							var smstate = e4e.ui.FrameButtonStateManager["is_" + sm[i].name](theDc);
							if (smstate && bsr) {
								smstate = bsr.call(this, theDc);
							}
							if( visibleAction === true ){
								btn.setVisible(!!smstate);
								this.__verifyMenuBtnVisibility__(btn,!!smstate);
							} else {
								btn.setDisabled(!!!smstate);
							}
						}
					}
				}
			}
		}
	},

	_applyStateButtons_ : function(buttonNames,visibleAction) {
		for (var i = 0, l = buttonNames.length; i < l; i++) {
			this._applyStateButton_(buttonNames[i],visibleAction);
		}
	},

	_applyStateAllButtons_ : function(visibleAction) {
		this._elems_.filterBy(function(o) {
			return o.xtype === "button"
		}).eachKey(function(key) {
			this._applyStateButton_(key,visibleAction);
		}, this);
	},

	/**
	 * Check if any of the data-controls is dirty
	 */
	isDirty : function() {
		var d = false;
		this._dcs_.each(function(item) {
			if (item.isDirty()) {
				d = true;
				return false;
			}
			return true;
		}, this);
		return d;
	},

	isInitializing : function() {
		return __dialogInitializing__;
	},
	setInitializing : function(value) {
		__dialogInitializing__ = value;
		if( !value ){
			// initializing done, restart check notification task if it is not running
			getApplication().startCheckNotificationTask(true);
		}
	},
	isDestroying : function() {
		return __dialogDestroying__;
	},
	setDestroying : function(value) {
		__dialogDestroying__ = value;
	},
	
	_startPerfTime_ : 0,
	perfInit : function(){
		this._startPerfTime_ = new Date().getTime();
	},
	perfLog : function(str){
		//var crtTime = new Date().getTime();
		//console.log(crtTime+" / "+(crtTime-this._startPerfTime_)+" - "+str);
	},
	
	_initPhase2Done_: null,
	_linkElementsPhase2_ : function(){},
	_afterLinkElementsPhase2_ : function(){},
	_initPhase2_ : function() {
		this.perfLog("UntilSecondInit");
		this.perfInit();
		this.perfLog("StartSecondInit");
		this._linkElementsPhase2_();
		this.mon(this, "afterrender", this._afterInitPhase2_, this, {single: true});
		Ext.defer(this._afterInitPhase2_, 500, this);
		this.perfLog("EndSecondInit");
	},

	_afterInitPhase2_ : function(){
		if( !this._initPhase2Done_ ){
			if( this.frameOpenParams.showWorking ){
				Main.workingEnd();
			}
			this._afterLinkElementsPhase2_();
			
			this._applyStateAllButtons_();
			this._applyTabLoadRestriction_();
			this._applyContentLoadRestriction_();
			this._checkForCollapseAndExpand_();
			
			this._initPhase2Done_ = true;
		}
	},

	_defineUiElements_ : function() {
		this._startDefine_();

		/* define data-controls */
		if (this._beforeDefineDcs_() !== false) {
			this._defineDcs_();
			this._afterDefineDcs_();
			this._afterAfterDefineDcs_();
		}

		/* define stand-alone user-interface elements */
		if (this._beforeDefineElements_() !== false) {
			this._defineElements_();
			this._afterDefineElements_();
		}

		/* define toolbars */
		if (this._beforeDefineToolbars_() !== false) {
			this._defineToolbars_();
			this._afterDefineToolbars_();
		}

		/* build the ui, linking elements */
		
		if (this._beforeLinkElements_() !== false) {
			this._linkElements_();
			this._afterLinkElements_();
		}
		
		this._registerDcsInStateManager_();

		/* build frame monitoring list */
		if (this._beforeDefineFrameMonList_() !== false) {
			this._defineFrameMonList_();
			this._afterDefineFrameMonList_();
		}
		
		this._endDefine_();
	},
	
	_attachAjaxMonitors_ : function(){
		var milisec=0;
		var seconds=0;
		
		function displayLoadingTime(){
			if (milisec>=9) {
				milisec=0;
				seconds+=1;
			} else {
				milisec+=1;
			}
		}
		
		function resetTimer() {
			milisec=0;
			seconds=0;
		}

		Ext.Ajax.on("beforerequest", function() {
			if( __dialogDestroying__ === true ){
				return;
			}
			this._statusBar_.setText(Main.translate("msg", "beforerequest_msg"));
			displayLoadingTime();
		}, this);

		Ext.Ajax.on("requestcomplete", function() { 
			if( __dialogDestroying__ === true ){
				return;
			}
			if (!Ext.Ajax.isLoading()) {
				this._statusBar_.setText(Main.translate("msg", "requestcomplete_msg")+" "+seconds+"."+milisec+" "+Main.translate("msg", "seconds_msg"));
				resetTimer();
			}
		}, this);

		Ext.Ajax.on("requestexception", function(conn, response) { 
			if( __dialogDestroying__ === true ){
				return;
			}
			if (!Ext.Ajax.isLoading()) {
				this._statusBar_.setText(Main.translate("msg", "requestcomplete_msg")+" "+seconds+"."+milisec+" "+Main.translate("msg", "seconds_msg"));
				resetTimer();
			}
			Main.serverMessage(null, response);
		}, this);
	},
	
	initComponent : function() {
		this.perfInit();
		this.perfLog("StartInit");
		this.setInitializing(true);
		
		if (getApplication().getSession().rememberViewState) {
			Ext.state.Manager.setProvider(new Ext.state.LocalStorageProvider({}));
		}
		this._mainViewName_ = "main";
		var _trlFqn = this.$className.replace(".ui.extjs.", ".i18n.");
		try {
			if (_trlFqn !== this.$className && Ext.ClassManager.get(_trlFqn)) {
				this._trl_ = Ext.create(_trlFqn);
			}
		} catch (e) {
			Ext.Msg.show({
				title : "Invalid language-pack",
				msg : "No translation file found for " + _trlFqn,
				icon : Ext.MessageBox.INFO,
				buttons : Ext.Msg.OK
			});
		}

		this._elems_ = new Ext.util.MixedCollection();
		this._configVars_ = new Ext.util.MixedCollection();
		this._tlbs_ = new Ext.util.MixedCollection();
		this._tlbitms_ = new Ext.util.MixedCollection();
		this._actions_ = new Ext.util.MixedCollection();
		this._dcs_ = new Ext.util.MixedCollection();
		this._frameMonList_ = new Ext.util.MixedCollection();
		this._buttonStateRules_ = {};
		this._gridsWithFilter_ = new Ext.util.MixedCollection();
		this._userFieldsCfg_ = new Ext.util.MixedCollection();

		this._statusBar_ = new Ext.ux.StatusBar({
			id : 'ui-status-bar',
			defaultText : 'Status bar. Watch for messages...',
			defaultIconCls : 'default-icon',
			text : 'Ready',
			iconCls : 'x-status-valid',
			ui : "status-bar",
			items : [ '-' ]
		});
		
		this._attachAjaxMonitors_();

		this._config_();

		this._defineUiElements_();

		this._beforeViewAndFilterConfig_();

		// try to figure out print tiles for grid views, if no explicit values set
		// look for a toolbar with name tlbViewName and get its title.
		var _ttl_fn = function(item) {
			if (!item._printTitle_) {
				this._setGridPrintTitle_(item.name, "tlb" + (""+item.name).toFirstUpper());
			}
		}

		this._elems_.each(_ttl_fn, this);

		Ext.apply(this, {
			layout : "fit",
			bbar : this._statusBar_,
			items : [ this._elems_.get(this._mainViewName_) ]
		});
		
		if (typeof __FRAMEPARAMETERS__ !== 'undefined' && Ext.isArray(__FRAMEPARAMETERS__) && __FRAMEPARAMETERS__.length>0) {
			var key = (__FRAMEPARAMETERS__[0].name+"").replace(/\s+/g, '').toLowerCase().replaceAll("-","_");
			this._title_ = Main.translate("appMenuItems", key, null, __FRAMEPARAMETERS__[0].title);
			this._glyph_ = __FRAMEPARAMETERS__[0].glyph;
		} else {
			if( this._trl_ ){
				this._title_ = this._trl_.title;
			}
		}
		
		this.callParent(arguments);
		this.mon(this, "afterrender", this._onReady_, this, {single: true});
		this.mon(this, "tabChanged", this._onTabChanged_, this);
		
		Ext.defer(this.doLinkWithFrames, 3000, this);
		
		this._registerKeyBindings_();
		this.perfLog("EndInit");
	},
	
	/**
	 * Give possibility to the developer to inject his code 
	 */
	_onCheckForCollapseAndExpand_ : function(){},
	_onTabChanged_ : function() {},
	
	/**
	 * Search for collapse and expand buttons and inject the menu items
	 */
	_checkForCollapseAndExpand_ : function(){
		// search if exists button for collapse and expand menu and prepare the data for it
		var _check_fn = function(item) {
			if( item.isBtnContainer === true && item.forClpsAndExp ){
				var pnl = this._getElementConfig_(item.forClpsAndExp);
				if( pnl ){ // forClpsAndExp
					this._prepareCollapseAndExpandMenu_(item, pnl);
				}
			}
		};
		this._elems_.each(_check_fn, this);
		this._onCheckForCollapseAndExpand_();
	},
	
	/**
	 * Search for button in the form and inject the expand/collapse menu items to button menu
	 */
	_prepareCollapseAndExpandMenuToFormButton_ : function(frmName, btnName, pnlCntName){
		var form = this._getElement_(frmName);
		if( form ){
			var pnlCfg = this._getElementConfig_(pnlCntName);
			var btnCfg = form._getElementConfig_(btnName);
			if( pnlCfg && btnCfg ){
				this._prepareCollapseAndExpandMenu_(btnCfg,pnlCfg);
			}
		}
	},
	
	/**
	 * Inject the menu items to the btn menu from pnl collapsible items
	 * @btn - config element of the button
	 * @pnl - config element of the panel which contain the list of collapsible panels
	 */
	_prepareCollapseAndExpandMenu_ : function(btn,pnl) {
		// prepare the menu for btn using data from pnl
		if(!pnl.items){
			return;
		}
		var items = [];
		var i;
		for(i=0;i<pnl.items.length;i++){
			var o = pnl.items[i];
			if(o.collapsible === true){
				items.push(o.id);
				btn.menu.add({
					text : o.title,
					_pnlId_ : o.id,
					_baseBtn_ : btn,
					handler : this._genericCollapseAndExpandHandler_,
					scope : this
				});
			}
		}
		btn._pnlList_ = items;
		btn.menu.add("-");
		btn.menu.add({text:"Collapse all", 
			_baseBtn_ : btn,
			_collapseAll_ : true,
			handler : this._genericCollapseAndExpandHandler_,
			scope: this
		});
		btn.menu.add({text:"Expand all", 
			_baseBtn_ : btn,
			_expandAll_ : true,
			handler : this._genericCollapseAndExpandHandler_,
			scope: this
		});
		btn.forClpsAndExpPnl = pnl.name;
	},
	
	/**
	 * Handle the collapse and expand actions.
	 */
	_genericCollapseAndExpandHandler_ : function(obj) {
		var list = obj._baseBtn_._pnlList_;
		var i, l = list.length;
		var p;
		if( obj._collapseAll_ === true ){
			for(i=0;i<l;i++){
				p = Ext.getCmp(list[i]);
				if(p){
					p.collapse();
				}
			}
		} else
		if( obj._expandAll_ === true ){
			for(i=0;i<l;i++){
				p = Ext.getCmp(list[i]);
				if(p){
					p.expand();
				}
			}
		} else 
		if( obj._pnlId_ ){
			p = Ext.getCmp(obj._pnlId_);
			if(p){
				if( !obj._baseBtn_._containerPnlObj_ ){
					obj._baseBtn_._containerPnlObj_ = this._getElement_(obj._baseBtn_.forClpsAndExpPnl);
				}
				var focusPanel = function(){
					var cp = p.getPosition();
					obj._baseBtn_._containerPnlObj_.scrollTo(cp[0],cp[1]);
				};
				if( p.getCollapsed() ){
					if( obj._baseBtn_._containerPnlObj_ ){
						p.on("expand", function(){
							focusPanel();
						}, this, {single:true});
					}
					p.expand();
				} else {
					focusPanel();
				}
			}
		}
	},
	
	_registerKeyBindings_ : function() {
		return new Ext.util.KeyMap({
			target : document.body,
			eventName : 'keyup',
			processEvent : function(event) { // additional parameters: source, options
				return event;
			},
			binding : [ Ext.apply(KeyBindings.values.app.gotoNextTab, {
				fn : function(keyCode, e) {
					getApplication().gotoNextTab();
					e.stopEvent();
				},
				scope : this
			}) ]
		});
	},

	/**
	 * Translate a frame element.
	 * 
	 * @param {}
	 *            key
	 * @return {}
	 */
	translate : function(key) {
		if (this._trl_ && this._trl_[key]) {
			return this._trl_[key];
		} else {
			return key;
		}
	},

	/**
	 * Handle messages published by data-controls. Write them into the status
	 * bar notifications area.
	 * 
	 * The options argument has the following content: { type : type, message :
	 * message, trlGroup : trlGroup, params : params }
	 * 
	 * If a translation group is specified, attempt to translate it using the
	 * message as a translation key for the given translation group and the
	 * params as replacement for the placeholders.
	 * 
	 */
	message : function(options) {
		var o = options || {};
		var msg = o.message;
		if (o.trlGroup) {
			msg = Main.translate(o.trlGroup, msg, o.params);
		}
		this._statusBar_.setText(msg);
	},

	
	beforeDestroy : function() {
		this._beforeDestroyDNetView_();
		this._tlbitms_.each(function(item) {
			try {
				Ext.destroy(item);
			} catch (e) {
				alert(e);
			}
		});
		this._tlbs_.each(function(item) {
			try {
				Ext.destroy(item);
			} catch (e) {
				alert(e);
			}
		});
		this._dcs_.each(function(item) {
			try {
				Ext.destroy(item);
			} catch (e) {
				alert(e);
			}
		});
	},

	destroyElement : function(elemCfg) {
		try {
			var c = Ext.getCmp(elemCfg.id);
			if (c) {
				Ext.destroy(c);
			}
		} catch (e) {
			// nothing to do
		}
	},
	
	/**
	 * Link with other UIs; monitoring actions
	 */
	onFrameOpened : function(frameName){
		var n=this._frameMonList_.getCount();
		if( n>0 ){
			frameName = frameName.toUpperCase();
			for(var i=0;i<n;i++){
				var fo = this._frameMonList_.getAt(i);
				if( frameName.endsWith(fo._frameName_) ){
					var app = getApplication();
					var frame = app.searchForFrameInstance(frameName);
					if( frame ){
						this._doLinkWithFrame_(frame,fo);
					}
					break;
				}
			}
		}
	},

	_doLinkWithFrame_ : function(frame, linkObj){
		var dcs = linkObj._dcs_;
		for(var i=0;i<dcs.length;i++){
			var o = dcs[i];
			var dc = frame._getDc_(o._dcName_);
			dc.on(o._eventName_,o._fn_,this);
		}
	},
	
	doLinkWithFrames : function() {
		var app = getApplication();
		this._frameMonList_.each(function(item){
			var frame = app.searchForFrameInstance(item._frameName_);
			if( frame ){
				this._doLinkWithFrame_(frame, item);
			}
		},this);
		app.notifyFrameOpen(this.$className);
	},
	
	_configureUserFields_ : function(){
		// implementation is in AbstractUiExtension
	}
	
});
