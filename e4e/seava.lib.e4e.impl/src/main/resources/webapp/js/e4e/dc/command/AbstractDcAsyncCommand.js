/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
/**
 * Abstract base class for asynchronous commands. An asynchronous command is one
 * which involves an AJAX call so that the result is not available immediately.
 */
Ext.define("e4e.dc.command.AbstractDcAsyncCommand", {
	extend : "e4e.dc.command.AbstractDcCommand",

	onAjaxResult : function(ajaxResult) {

		if( __dialogDestroying__ === true ){
			return;
		}
		
		if( ajaxResult.response && ajaxResult.success === true ){
			try {
				var r = Ext.getResponseDataInJSON(ajaxResult.response);
				// params
				if (r.params) {
					this._updateModel(this.dc.params, r.params, {targetType:"params"});
				}
			} catch( e ) {
				// on error in some cases the responseText is not a JSON string
				// nothing to do in this case
			}
		}
		
		var _m = "afterDo" + this.dcApiMethod + "Result";
		var m = this.dc[_m];
		if (m !== undefined && m !== null && Ext.isFunction(m)) {
			m.call(this.dc, ajaxResult);
		}
		if (ajaxResult.success === true) {
			this.onAjaxSuccess(ajaxResult);
		} else {
			this.onAjaxFailure(ajaxResult);
		}
	},

	_doOnAjaxSuccess_ : function(ajaxResult,method) {
		var o = ajaxResult.options.options || {};
		if( o.showWorking ){
			Main.workingEnd();
		}
		var dc = this.dc;
		var m = dc[method];
		if (m !== undefined && m !== null && Ext.isFunction(m)) {
			m.call(dc, ajaxResult);
		}
		dc.fireEvent(method, dc, ajaxResult);
	},
	
	onAjaxSuccess : function(ajaxResult) {
		var _m = "afterDo" + this.dcApiMethod + "Success";
		this._doOnAjaxSuccess_(ajaxResult,_m);
	},

	_doOnAjaxFailure_ : function(ajaxResult,method,apiMethod) {
		var o = ajaxResult.options.options || {};
		if( o.showWorking ){
			Main.workingEnd();
		}
		var dc = this.dc;
		this.showAjaxErrors(ajaxResult);		
		var m = dc[method];
		if (!Ext.isEmpty(apiMethod)) { // SONE-6965
			if (apiMethod === "Delete") {
				dc.doCancel();
			}
		}		
		if (m !== undefined && m !== null && Ext.isFunction(m)) {
			m.call(dc);			
		}
		dc.fireEvent(method, dc, ajaxResult);
	},
	
	onAjaxFailure : function(ajaxResult) {
		var _m = "afterDo" + this.dcApiMethod + "Failure";
		var _apiMethod = this.dcApiMethod;
		this._doOnAjaxFailure_(ajaxResult,_m,_apiMethod);
	},

	/**
	 * Show Ajax errors
	 */
	showAjaxErrors : function(ajaxResult) {
		var msg = null;
		var resp = ajaxResult.response;

		if (ajaxResult.response) {
			msg = null;
		} else if (ajaxResult.operation) {
			var err = ajaxResult.operation.error;
			if (err) {
				msg = Ext.getResponseErrorText(err.response);
				resp = err.response;
			}
		} else if (ajaxResult.batch) {
			var b = ajaxResult.batch;
			if (b.exceptions && b.exceptions[0]) {
				msg = Ext.getResponseErrorText(b.exceptions[0].error.response);
				resp = b.exceptions[0].error.response;
			}
		}
		Main.serverMessage(msg,resp);
	},

	/**
	 * Private helper function to update values of the given target model (can
	 * be the current filter, current record or the params instance ) with
	 * values returned from server after an AJAX call passed in the source
	 * argument
	 * 
	 * ctrl: Behavior control flags
	 */
	_updateModel : function(target, source, ctrl) {
		var dirty = target.dirty;
		var p;
		target.beginEdit();
		if (ctrl && ctrl.targetType === "filter") {
			for ( p in source) {
				this.dc.setFilterValue(p, source[p]);
			}
		} else if (ctrl && ctrl.targetType === "params") {
			for ( p in source) {
				this.dc.setParamValue(p, source[p]);
			}
		} else {
			for ( p in source) {
				target.set(p, source[p]);
			}
		}
		target.endEdit();
		if (!dirty) {
			target.commit();
		}
	},
	
	/**
	 * For some operations (like save) more requests are sent to the server and more response are received.
	 * Some of the operations can be done with success, other with error.
	 * In case of error, need to take the response from the object which contain the error message.
	 */
	_getResponseFromBatch : function(batch){
		if( Ext.isArray(batch.operations) && batch.operations.length>0 ){
			if( !batch.exception ){
				return batch.operations[0]._response;
			}
			var i = 0; 
			while(i<batch.operations.length){
				var x = batch.operations[i];
				if( x.exception && x.error && x.error.response ){
					return x.error.response;
				}
				i++;
			}
		}
		return null;
	}

});