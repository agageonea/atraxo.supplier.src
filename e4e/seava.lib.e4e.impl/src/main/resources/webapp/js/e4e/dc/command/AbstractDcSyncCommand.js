/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
/**
 * Abstract base class for synchronous commands.
 */
Ext.define("e4e.dc.command.AbstractDcSyncCommand", {
	extend : "e4e.dc.command.AbstractDcCommand"

});