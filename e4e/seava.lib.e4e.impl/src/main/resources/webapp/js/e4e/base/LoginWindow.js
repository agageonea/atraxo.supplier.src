/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
/**
 * Login window form
 */
Ext.define("e4e.base.LoginForm", {

	extend : "Ext.form.Panel",

	/**
	 * Action button rendered in the window, given as a reference to be managed
	 * from the form.
	 * 
	 * @type
	 */
	actionButton : null,
	
	logoutButton : null,
	

	initComponent : function(config) {

		this.actionButton = this.initialConfig.actionButton;
		this.actionButton.setHandler(this.doLogin, this);
		
		this.logoutButton = this.initialConfig.logoutButton;
		if( this.logoutButton ){
			this.logoutButton.setHandler(this.doLogout, this);
		}
		var cfg = {
			frame : true,
			bodyPadding : 10,			
			fieldDefaults : {
				labelAlign : 'right',
				labelWidth : 100,
				msgTarget : 'side'
			},
			defaults : {
				anchor : '-20',
				selectOnFocus : true,
				allowBlank : false
			},
			items : this._buildItems_() 
			
		};
		Ext.apply(this, cfg);
		this.callParent(arguments);
		
	},

	/**
	 * Builder method which constructs the form elements.
	 * 
	 * @return {Array}
	 */
	_buildItems_ : function() {
		return [ {
			xtype : "label",
			text : Main.translate("login", "msg")
		}, {
			margin: "20 0 5 0",
			xtype : "textfield",
			itemId : "usr",
			fieldLabel : Main.translate("login", "user"),
			value : getApplication().getSession().getUser().loginName,
			listeners : {
				change : {
					scope : this,
					fn : this.enableAction
				}
			}
		}, {
			margin: "5 0 5 0",
			xtype : "textfield",
			itemId : "pswd",
			fieldLabel : Main.translate("login", "pswd"),
			inputType : "password",
			listeners : {
				change : {
					scope : this,
					fn : this.enableAction
				},
				specialkey : {
					scope : this,
					fn : this.onSpecialKey
				}
			}
		}, {
			margin: "5 0 5 0",
			xtype : "textfield",
			itemId : "client",
			fieldLabel : Main.translate("login", "client"),
			value : getApplication().getSession().getClient().code,
			listeners : {
				change : {
					scope : this,
					fn : this.enableAction
				}
			}
		} ];
	},

	/**
	 * Getter for the login button.
	 * 
	 * @return {}
	 */
	getActionButton : function() {
		return this.actionButton;
	},
	
	getLogoutButton : function() {
		return this.logoutButton;
	},

	/**
	 * Getter for the username field.
	 * 
	 * @return {Ext.form.field.Text} The component (if found)
	 */
	getUserField : function() {
		return this.getComponent("usr");
	},

	/**
	 * Getter for the password field
	 * 
	 * @return {Ext.form.field.Text} The component (if found)
	 */
	getPasswordField : function() {
		return this.getComponent("pswd");
	},

	/**
	 * Getter for the client field
	 * 
	 * @return {Ext.form.field.Text} The component (if found)
	 */
	getClientField : function() {
		return this.getComponent("client");
	},

	/**
	 * Getter for the language field
	 * 
	 * @return {Ext.form.field.ComboBox} The component (if found)
	 */
	getLanguageField : function() {
		return this.getComponent("lang");
	},

	/**
	 * Callback invoked on unsuccessful login attempt.
	 * 
	 * @param {}
	 *            response
	 * @param {}
	 *            options
	 */
	onActionFailure : function(response) {
		Main.error(Ext.getResponseErrorText(response));		 
		this.getActionButton().enable();
	},

	/**
	 * Callback invoked on successful login.
	 * 
	 * @param {}
	 *            response
	 * @param {}
	 *            options
	 */
	onActionSuccess : function(response) {
		var r = Ext.getResponseDataInJSON(response);
		var s = getApplication().getSession();
		var u = s.getUser();
		var c = s.getClient();
		var accountChange = false;
		if (c.id != r.data.clientId || u.code != r.data.code) {
			accountChange = true;
		}
		s.roles = r.roles;
		u.name = r.data.name;
		c.id = r.data.clientId;

		if (r.data.extjsDateFormat) {
			Main.DATE_FORMAT = r.data.extjsDateFormat;
		}
		if (r.data.extjsTimeFormat) {
			Main.TIME_FORMAT = r.data.extjsTimeFormat;
		}
		if (r.data.extjsDateTimeFormat) {
			Main.DATETIME_FORMAT = r.data.extjsDateTimeFormat;
		}
		if (r.data.extjsDateTimeSecFormat) {
			Main.DATETIMESEC_FORMAT = r.data.extjsDateTimeSecFormat;
		}
		if (r.data.extjsMonthFormat) {
			Main.MONTH_FORMAT = r.data.extjsMonthFormat;
		}
		if (r.data.extjsAltFormats) {
			Main.DATE_ALTFORMATS = r.data.extjsAltFormats;
		}

		if (r.data.decimalSeparator) {
			Main.DECIMAL_SEP = r.data.decimalSeparator;
		}
		if (r.data.thousandSeparator) {
			Main.THOUSAND_SEP = r.data.thousandSeparator;
		}
		
		if (r.data.maxButtonsOnToolbars && Ext.isNumber(r.data.maxButtonsOnToolbars)) {
			Main.MAX_BUTTONS_ON_TOOLBARS = r.data.maxButtonsOnToolbars;
		}
		Main.initFormats();

		getApplication().onLoginSuccess(accountChange);
	},

	/**
	 * Execute login action. The login button click handler.
	 * 
	 * @param {}
	 *            btn
	 * @param {}
	 *            evnt
	 */
	doLogin : function() { // parameters: btn, evnt
		var s = getApplication().getSession();
		var u = s.getUser();

		u.loginName = this.getUserField().getValue();
		s.client.code = this.getClientField().getValue();
		var p = {};

		p["user"] = this.getUserField().getValue();
		p["pswd"] = this.getPasswordField().getValue();
		p["client"] = this.getClientField().getValue();

		Ext.Ajax.request({
			method : "POST",
			params : p,
			failure : this.onActionFailure,
			success : this.onActionSuccess,
			scope : this,
			url : Main.sessionAPI("json").login
		});
		this.getActionButton().disable();
	},

	
	doLogout : function() { // parameters: btn, evnt
		getApplication().doLogout();
	},

	/**
	 * Clear the form fields.
	 */
	clearFields : function() {
		this.items.each(function(item) {
			item.setValue(null);
		}, this);
	},

	clearInvalid : function() {
		this.getUserField().clearInvalid();
		this.getClientField().clearInvalid();
	},

	/**
	 * Apply states on form elements after logout.
	 */
	applyState_Logout : function() {
		this.getUserField().enable();

		this.getPasswordField().enable();
		this.getPasswordField().setValue(null);
		this.getPasswordField().clearInvalid();

		this.getClientField().enable();
		this.getClientField().setValue(null);
		this.getClientField().clearInvalid();

		this.getActionButton().disable();
	},

	/**
	 * Apply states on form elements after lock session.
	 */
	applyState_LockSession : function() {
		this.getUserField().disable();
		this.getPasswordField().setValue(null);
		this.getPasswordField().clearInvalid();
		this.getClientField().disable();

		this.getActionButton().disable();
	},

	/**
	 * Setter for the user-name field value.
	 * 
	 * @param {}
	 *            v
	 */
	setUserName : function(v) {
		this.getUserField().setValue(v);
	},

	/**
	 * Enable disable action button.
	 */
	enableAction : function() {
		if (this.getForm().isValid()) {
			this.getActionButton().enable();
		} else {
			this.getActionButton().disable();
		}
	},
	
	
	/**
	 * Handle ENTER on password field.
	 */
	onSpecialKey : function(cmp, e){
		if( e.getKey() === e.ENTER ){
			var btn = this.getActionButton();
			if( !btn.isDisabled() ){
				btn.focus();
				if (btn.handler) {
	            	btn.handler.call(btn.scope || btn, btn, e);
				}
			}
		}
	}
});
/**
 * Login window
 */
Ext.define("e4e.base.LoginWindow", {
	extend : "Ext.Window",

	initComponent : function(config) {
		
		var btn = Ext.create('Ext.Button', {
			text : Main.translate("login", "btn"),
			disabled : true
		});
		var btnLogout = Ext.create('Ext.Button', {
			text : Main.translate("login", "logoutBtn"),
			disabled : false
		});

		var cfg = {
			title : Main.translate("login", "title"),
			cls: "sone-on-top",
			border : true,
			width : 350,
			resizable : false,
			closeAction : "hide",
			closable : false,
			buttonAlign : "left",
			modal : true,
			constrain: true,
			renderTo: Ext.getBody(),
			items : new e4e.base.LoginForm({
				actionButton : btn,
				logoutButton : btnLogout
			}),
			buttons : [ btn, btnLogout ]
		};

		Ext.apply(cfg, config);
		Ext.apply(this, cfg);
		this.callParent(arguments);
	},

	/**
	 * Getter for the login form.
	 * 
	 * @return {}
	 */
	getLoginForm : function() {
		return this.items.getAt(0);
	},

	/**
	 * Wrapper around the login-form action button getter.
	 * 
	 * @return {}
	 */
	getActionButton : function() {
		return this.getLoginForm().getActionButton();
	},
	
	getLogoutButton : function() {
		return this.getLoginForm().getLogoutButton();
	},

	/**
	 * Wrapper around the login-form user field getter.
	 * 
	 * @return {}
	 */
	getUserField : function() {
		return this.getLoginForm().getUserField();
	},

	/**
	 * Wrapper around the login-form password field getter.
	 * 
	 * @return {}
	 */
	getPasswordField : function() {
		return this.getLoginForm().getPasswordField();
	},

	/**
	 * Wrapper around the login-form client field getter.
	 * 
	 * @return {}
	 */
	getClientField : function() {
		return this.getLoginForm().getClientField();
	},

	/**
	 * Wrapper around the login-form language field getter.
	 * 
	 * @return {}
	 */
	getLanguageField : function() {
		return this.getLoginForm().getLanguageField();
	},

	/**
	 * Wrapper around the login-form applyState_Logout function.
	 */
	applyState_Logout : function() {
		this.getLoginForm().applyState_Logout();
	},

	/**
	 * Wrapper around the login-form applyState_LockSession function.
	 */
	applyState_LockSession : function() {
		this.getLoginForm().applyState_LockSession();
	},

	/**
	 * Wrapper around the login-form clearInvalid function.
	 */
	clearInvalid : function() {
		this.getLoginForm().clearInvalid();
	}

});