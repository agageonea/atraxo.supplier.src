/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
/**
 * Builder for edit property grid views.
 */
Ext.define("e4e.dc.view.DcvEditPropGridBuilder", {
	extend : "Ext.util.Observable",

	dcv : null,

	addTextField : function(config) {
		if (!config.editor) {
			config.editor = {};
		}
		var e = config.editor;

		if (e.maxLength) {
			e.enforceMaxLength = true;
		}
		if (e.caseRestriction) {
			e.fieldStyle += "text-transform:" + e.caseRestriction + ";";
		}
		config.editorInstance = Ext.create('Ext.form.field.Text', e);
		config._default_ = "";
		this.applySharedConfig(config);
		return this;
	},

	addDateField : function(config) {
		config.xtype = "datefield";
		config.editorInstance = Ext.create('Ext.form.field.Date', Ext.apply(
				config.editor || {}, {
					format : Main.DATE_FORMAT,
					altFormats : Main.ALT_FORMATS
				}));
		config.renderer = config.renderer
				|| Ext.util.Format.dateRenderer(Main.DATE_FORMAT);
		this.applySharedConfig(config);
		return this;
	},

	addNumberField : function(config) {
		config.xtype = "numberfield";
		config.editorInstance = Ext.create('Ext.form.field.Number', Ext.apply(
				config.editor || {}, {
					fieldStyle : "text-align:right;"
				}));
		this.applySharedConfig(config);
		return this;
	},

	addLov : function(config) {
		if (config.editor && config.editor.xtype !== "textfield") {
			config.editorInstance = Ext.create(config.editor._fqn_, Ext.apply(
					config.editor, {
						selectOnFocus : true
					}));
		}
		this.applySharedConfig(config);
		return this;
	},

	addBooleanField : function(config) {
		config.editor = Ext.applyIf(config.editor || {}, {
			forceSelection : true
		});
		Ext.applyIf(config, {
			_default_ : null
		});
		var yesNoStore = Main.createBooleanStore();
		config.editorInstance = Ext.create('Ext.form.field.ComboBox', Ext
				.apply(config.editor || {}, {
					queryMode : "local",
					valueField : "bv",
					displayField : "tv",
					triggerAction : "all",
					store : yesNoStore
				}));

		config.renderer = function(v) {
			if (v == null) {
				return "";
			}
			return Main.translate("msg", "bool_" + ((!!v)));
		}
		this.applySharedConfig(config);
		return this;
	},

	addToggleField : function(config) {
		return this.addBooleanField(config);
	},
	
	// ==============================================

	applySharedConfig : function(config) {
		Ext.applyIf(config, {
			id : Ext.id()
		});
		config.editorInstance._dcView_ = this.dcv;
		if (config.allowBlank === false) {
			config.labelSeparator = Main.viewConfig.FORM_LABEL_SEPARATOR_FOR_MANDATORY;
		}
		this.dcv._elems_.add(config.name, config);
	}

});