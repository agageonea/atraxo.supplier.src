/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

/**
 * Components which provides support for pop-over editing.
 */

Ext.define("e4e.dc.view.FpText", {
    extend: "Ext.form.field.Text",
    alias: "widget.fptextfield",
    
    initComponent: function(){
        this.callParent(arguments);
        this.on("afterrender",this.onTapAttach,this,{single:true});
    },

    onTapAttach : function(){
    	this.el.on({scope : this, tap : "onTap"});
    },
    
	onTap : function(e){
		this.fireEvent("click",this,e);
	}
});

Ext.define("e4e.dc.view.FpTextArea", {
	extend: "Ext.form.field.TextArea",
	alias: "widget.fptextarea",
	
	initComponent: function(){
	    this.callParent(arguments);
	    this.on("afterrender",this.onTapAttach,this,{single:true});
	},
	
	onTapAttach : function(){
		this.el.on({scope : this, tap : "onTap"});
	},
	
	onTap : function(e){
		this.fireEvent("click",this,e);
	}
});

Ext.define("e4e.dc.view.FpPopoverText", {
	extend: "e4e.dc.view.FpText",
	alias: "widget.fppopovertextfield",

	fieldsValueSeparator : " ",
	fieldsList : null,
	formatList : null,
	customFormatFn : null,
	editorForm : null,
	oldValues : {},
	
	vPos: "t",
	hPos: "r",
	
	cls: "sone-popover-disabled",
    
	initComponent : function() {
		this.callParent(arguments);

		var v = this._getView_();
		v.on("afterbind", this._bindRecord_, this);
		v.on("afterunbind", this._bindRecord_, this);
		var dc = v._controller_;
		dc.on("afterDoSaveSuccess", this._bindRecord_, this);
		dc.on("afterDoCancel", this._bindRecord_, this);
		dc.on("afterDoCancelSelected", this._bindRecord_, this);
		dc.store.on("update", this._bindRecord_, this);

		if( !Ext.isArray(this.formatList) ){
			this.formatList = [];
		}
		while( this.formatList.length < this.fieldsList.length ){
			this.formatList.push("");
		}
	},

	isValid : function() {
		var me = this;
		var validate = me.forceValidation || (me.allowBlank===false);

		return validate ? me.validateValue(me.processRawValue(me.getRawValue())) : true;
	},

	onTap : function(e) {
		this.callParent(arguments);
		this._showEditor_();
	},
	
	_getView_ : function(){
		return this._dcView_;
	},
	
	_getFormInstance_ : function(){
		if( this._activeEditor_ ){
			return this._activeEditor_.items.items[0];
		}
		return null;
	},
	
	_saveCurrentValues_ : function(){
		var dc = this._getView_()._controller_;
		var rec = dc.getRecord();
		this.oldValues = {};
		if (rec) {
			var i, l = this.fieldsList.length;
			for (i = 0; i < l; i++) {
				this.oldValues[this.fieldsList[i]] = rec.get(this.fieldsList[i]);
			}
		}
	},
	
	_isFieldVisible_ : function(name, rec) {
		var res = true;
		var form = this._getView_();
		var item = form._elems_.get(name);
		if (item && item._visibleFn_) {
			res = form._canSetVisible_(name, rec);
		}
		return res;
	},
	
	_getDecimalPlaces_ : function(idx){
		var n = this.formatList[idx];
		try {
			if( Ext.isEmpty(n) ){
				var item = this._getView_()._elems_.get(this.fieldsList[idx]);
				if( Ext.isNumber(item.decimals) ){
					n = item.decimals;
				} else {
					n = 0;
				}
			}
		}catch(e){
			n = 0;
		}
		return n;
	},
	
	_formatVal_ : function(obj,v,idx) {
		if( !Ext.isEmpty(obj) ){
			if( Ext.isDate(obj) ){
				v += Ext.util.Format.date(obj, this.formatList[idx] || Main.DATE_FORMAT);
			} else {
				if( Ext.isNumber(obj) ){
					v += Ext.util.Format.number(obj, Main.getNumberFormat(this._getDecimalPlaces_(idx)));
				} else {
					v += obj;
				}
			}
		}
		return v;
	},
	
	_bindRecord_ : function(){
		var dc = this._getView_()._controller_;		
		var v = "";
		var rec = dc.getRecord();
		if( this.customFormatFn && Ext.isFunction(this.customFormatFn) ){
			v = this.customFormatFn(dc,rec,this);
		} else {
			if (rec) {
				var i, l = this.fieldsList.length;
				for (i = 0; i < l; i++) {
					if( this._isFieldVisible_(this.fieldsList[i], rec) ){
						if( !Ext.isEmpty(v) ){
							v += this.fieldsValueSeparator;
						}
						// If the field included is a displayField or it isn't in the record, try to get the value from the view
						if (typeof(rec.get(this.fieldsList[i])) === "undefined") {
							// get the field from the view
							var view = this._getView_();
							var theFieldValue, theField = view._get_(this.fieldsList[i]);
							if (typeof(theField) !== "undefined") {
								// if we found the field in the view get the field value
								theFieldValue = theField.fieldLabel;
								v = this._formatVal_(theFieldValue,v,i);
							}
						}
						else {
							v = this._formatVal_(rec.get(this.fieldsList[i]),v,i);						
						}
					}
				}
			}
		}
		this.setValue(v);
	},
	
	_showEditor_ : function(){
		var uiContext = this;
		if( uiContext._activeEditor_ ){
			return;
		}

		this._saveCurrentValues_();

		var btnOk = Ext.create({
			xtype: "button", 
			text: Main.translate("tlbitem", "ok__lbl"),
			tooltip : Main.translate("tlbitem", "ok__tlp"),
			uiContext: uiContext, 
			iconCls: "glyph-green",
			glyph: "xf00c@FontAwesome",
			handler: function(btn) {
				if( btn.uiContext._getFormInstance_().isValid() ){
					btn.uiContext._activeEditor_.close();
				}
			}
		});
		var btnCancel = Ext.create({
			xtype: "button", 
			text: Main.translate("tlbitem", "cancel_on_fields__lbl"),
			tooltip : Main.translate("tlbitem", "cancel_on_fields__tlp"),
			uiContext: uiContext, 
			iconCls: "glyph-yellow",
			glyph: "xf0e2@FontAwesome",
			handler: function(btn) {
				btn.uiContext._getView_()._controller_.doCancelOnFields({
					fields: uiContext.fieldsList,
					oldValues: uiContext.oldValues
				});
				btn.uiContext._activeEditor_.close();
			}
		});
		
		var win = new Ext.Window({
			draggable: false,
			modal: true,
			closable: true,
			resizable: false,
			bodyPadding: "10px 10px 0px 10px",
            cls: "sone-popover-modal-left",
			layout: {
			    pack: 'center',
			    type: 'hbox'
			},
            items: [ {
				xtype : this.editorForm,
				_temporaryEditorWindow_ : true,
				_controller_ : uiContext._getView_()._controller_
			}],
			dockedItems : [ {
				xtype: "toolbar",
				dock: "bottom",
				layout: {
				    pack: 'left',
				    type: 'hbox'
				},
				padding: "0px 10px 20px 20px",
				items: [ btnOk, btnCancel ]
			} ]
		});
		uiContext._activeEditor_ = win;
		win.showRelativeTo(this, {
			vertical: this.vPos,
			horizontal: this.hPos
		});

		var fnBtnState = function() {
			btnOk.setDisabled(!uiContext._getFormInstance_().isValid());
		};
		uiContext._getView_()._controller_.on("dcstatechange", fnBtnState, this, {buffer : 150});
		uiContext._getView_()._controller_.on("statusChange", fnBtnState, this, {buffer : 150});
		win.on("close", function() {
			uiContext._getView_()._controller_.un("dcstatechange", fnBtnState, this);
			uiContext._getView_()._controller_.un("statusChange", fnBtnState, this);
			uiContext._bindRecord_();
			uiContext._activeEditor_ = null;
		}, this);
		Ext.defer(fnBtnState,100);
	}
});

Ext.define("e4e.dc.view.FpPopoverTextArea", {
	extend: "e4e.dc.view.FpTextArea",
	alias: "widget.fppopovertextarea",

	fieldsValueSeparator : "\n",
	fieldsList : null, 
	formatList : null,
	customFormatFn : null,
	editorForm : null,
	oldValues : {},

	vPos: "t",
	hPos: "r",

	initComponent : function() {
		this.callParent(arguments);

		var v = this._getView_();
		v.on("afterbind", this._bindRecord_, this);
		v.on("afterunbind", this._bindRecord_, this);
		var dc = v._controller_;
		dc.on("afterDoSaveSuccess", this._bindRecord_, this);
		dc.on("afterDoCancel", this._bindRecord_, this);
		dc.on("afterDoCancelSelected", this._bindRecord_, this);
		dc.store.on("update", this._bindRecord_, this);

		if( !Ext.isArray(this.formatList) ){
			this.formatList = [];
		}
		while( this.formatList.length < this.fieldsList.length ){
			this.formatList.push("");
		}
	},

	isValid : function() {
		var me = this;
		var validate = me.forceValidation || (me.allowBlank===false);

		return validate ? me.validateValue(me.processRawValue(me.getRawValue())) : true;
	},

	onTap : function(e){
		this.callParent(arguments);
		this._showEditor_();
	},
	
	_getView_ : function(){
		return this._dcView_;
	},

	_getFormInstance_ : function(){
		if( this._activeEditor_ ){
			return this._activeEditor_.items.items[0];
		}
		return null;
	},
	
	_saveCurrentValues_ : function(){
		var dc = this._getView_()._controller_;
		var rec = dc.getRecord();
		this.oldValues = {};
		if (rec) {
			var i, l = this.fieldsList.length;
			for (i = 0; i < l; i++) {
				this.oldValues[this.fieldsList[i]] = rec.get(this.fieldsList[i]);
			}
		}
	},

	_isFieldVisible_ : function(name, rec) {
		var res = true;
		var form = this._getView_();
		var item = form._elems_.get(name);
		if (item && item._visibleFn_) {
			res = form._canSetVisible_(name, rec);
		}
		return res;
	},

	_getDecimalPlaces_ : function(idx){
		var n = this.formatList[idx];
		try {
			if( Ext.isEmpty(n) ){
				var item = this._getView_()._elems_.get(this.fieldsList[idx]);
				if( Ext.isNumber(item.decimals) ){
					n = item.decimals;
				} else {
					n = 0;
				}
			}
		}catch(e){
			n = 0;
		}
		return n;
	},
	
	_formatVal_ : function(obj,v,idx) {
		if( !Ext.isEmpty(obj) ){
			if( Ext.isDate(obj) ){
				v += Ext.util.Format.date(obj, this.formatList[idx] || Main.DATE_FORMAT);
			} else {
				if( Ext.isNumber(obj) ){
					v += Ext.util.Format.number(obj, Main.getNumberFormat(this._getDecimalPlaces_(idx)));
				} else {
					v += obj;
				}
			}
		}
		return v;
	},
	
	_bindRecord_ : function(){
		var dc = this._getView_()._controller_;
		var v = "";
		var rec = dc.getRecord();
		if( this.customFormatFn && Ext.isFunction(this.customFormatFn) ){
			v = this.customFormatFn(dc,rec,this);
		} else {
			if (rec) {
				var i, l = this.fieldsList.length;
				for (i = 0; i < l; i++) {
					if( this._isFieldVisible_(this.fieldsList[i], rec) ){
						if( !Ext.isEmpty(v) ){
							v += this.fieldsValueSeparator;
						}
						v = this._formatVal_(rec.get(this.fieldsList[i]),v,i);
					}
				}
			}
		}
		this.setValue(v);
	},
	
	_showEditor_ : function(){
		var uiContext = this;
		if( uiContext._activeEditor_ ){
			return;
		}
		this._editorActive_ = true;
		this._saveCurrentValues_();
		
		var btnOk = Ext.create({
			xtype: "button", 
			text: Main.translate("tlbitem", "ok__lbl"),
			tooltip : Main.translate("tlbitem", "ok__tlp"),
			uiContext: uiContext, 
			iconCls: "glyph-green",
			glyph: "xf00c@FontAwesome",
			handler: function(btn) {
				if( btn.uiContext._getFormInstance_().isValid() ){
					btn.uiContext._activeEditor_.close();
				}
			}
		});
		var btnCancel = Ext.create({
			xtype: "button", 
			text: Main.translate("tlbitem", "cancel_on_fields__lbl"),
			tooltip : Main.translate("tlbitem", "cancel_on_fields__tlp"),
			uiContext: uiContext, 
			iconCls: "glyph-yellow",
			glyph: "xf0e2@FontAwesome",
			handler: function(btn) {
				btn.uiContext._getView_()._controller_.doCancelOnFields({
					fields: uiContext.fieldsList,
					oldValues: uiContext.oldValues
				});
				btn.uiContext._activeEditor_.close();
				this._editorActive_ = false;
			}
		});
		
		var win = new Ext.Window({
			draggable: false,
			modal: true,
			closable: false,
			resizable: false,
			bodyPadding: "20px 20px 0px 20px",
            cls: "sone-popover-modal-top",
            items: [ {
				xtype: this.editorForm,
				_temporaryEditorWindow_: true,
				_controller_: uiContext._getView_()._controller_
			} ],
			dockedItems : [ {
				xtype: "toolbar",
				dock: "bottom",
				padding: "20px 20px 20px 45px",
				items: [ btnOk, btnCancel ]
			} ]
		});
		uiContext._activeEditor_ = win;
		win.showRelativeTo(this, {
			vertical: this.vPos,
			horizontal: this.hPos
		});

		var fnBtnState = function() {
			btnOk.setDisabled(!uiContext._getFormInstance_().isValid());
		};
		uiContext._getView_()._controller_.on("dcstatechange", fnBtnState, this, {buffer : 150});
		uiContext._getView_()._controller_.on("statusChange", fnBtnState, this, {buffer : 150});
		win.on("close", function() {
			uiContext._getView_()._controller_.un("dcstatechange", fnBtnState, this);
			uiContext._getView_()._controller_.un("statusChange", fnBtnState, this);
			uiContext._activeEditor_ = null;
			uiContext._bindRecord_();
		}, this);
		Ext.defer(fnBtnState,100);
	}	
});
