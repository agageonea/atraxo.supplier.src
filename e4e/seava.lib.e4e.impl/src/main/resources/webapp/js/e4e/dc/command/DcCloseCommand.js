/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.dc.command.DcCloseCommand", {
	extend : "e4e.dc.command.DcEditOutCommand"
	// same behavior with EditOut command
});
