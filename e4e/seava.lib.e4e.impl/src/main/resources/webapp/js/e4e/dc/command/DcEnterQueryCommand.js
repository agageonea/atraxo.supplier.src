/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.dc.command.DcEnterQueryCommand", {
	extend : "e4e.dc.command.AbstractDcSyncCommand",

	dcApiMethod : e4e.dc.DcActionsFactory.ENTER_QUERY,

	onExecute : function(options) {
		this.dc.fireEvent("onEnterQuery", this, options);
	}
});
