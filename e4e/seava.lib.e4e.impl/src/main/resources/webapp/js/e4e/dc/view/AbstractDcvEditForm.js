/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.dc.view.AbstractDcvEditForm", {
	extend : "e4e.dc.view.AbstractDc_Form",

	// **************** Properties *****************

	_wizardMaxSteps_ : 0,
	_wizardStepsCompleted_ : 0,

	/**
	 * Flag to automatically disable form if the data-control has no record selected.
	 */
	_skipMaskWhenNoRecordInDc_ : false,

	/**
	 * Flag to automatically disable form fields if the data-control is marked
	 * as read-only.
	 */
	_shouldDisableWhenDcIsReadOnly_ : true,
	
	/**
	 * Flag to automatically mask form fields if the data-control is marked
	 * as masked.
	 */
	_shouldDisableIfDcIsMasked_ : true,

	/**
	 * Specify how to apply the form disable when shouldDisableWhenDcIsReadOnly
	 * property is true. Possible values are : fields - call disable on all
	 * fields (default value) panel - disable the form panel elems - call
	 * disable on all elements
	 * 
	 */
	_disableModeWhenDcIsReadOnly_ : "fields",

	/**
	 * Component builder
	 */
	_builder_ : null,

	/**
	 * Helper property to identify this dc-view type as edit form.
	 */
	_dcViewType_ : "edit-form",

	/**
	 * Array of image fields. Kept separately to manage binding.
	 */
	_images_ : null,

	/**
	 * Array of number fields. Kept separately to manage binding.
	 */
	_numberfields_ : null,

	/**
	 * Flag to automatically acquire focus when create new record.
	 */
	_acquireFocusInsert_ : true,

	/**
	 * Flag to automatically acquire focus when edit a record
	 */
	_acquireFocusUpdate_ : true,
	
	/**
	 * Flag to show that the instance is created temporary used only to edit some fields
	 */
	_temporaryEditorWindow_ : false,
	
	// **************** Public API *****************

	/**
	 * Returns the builder associated with this type of component. Each
	 * predefined data-control view type has its own builder. If it doesn't
	 * exist yet attempts to create it.
	 */
	_getBuilder_ : function() {
		if (this._builder_ == null) {
			this._builder_ = new e4e.dc.view.DcvEditFormBuilder({
				dcv : this
			});
		}
		return this._builder_;
	},

	_setShouldDisableWhenDcIsReadOnly_ : function(v, immediate) {
		this._shouldDisableWhenDcIsReadOnly_ = v;
		if (immediate && v) {
			this._doDisableWhenDcIsReadOnly_();
		}
	},

	// **************** Defaults and overrides *****************

	frame : true,
	border : true,
	bodyPadding : '8px 5px 3px 5px',
	maskOnDisable : false,
	layout : "fit",
	buttonAlign : "left",
	bodyCls : 'dcv-edit-form',
	trackResetOnLoad : false,
	fieldDefaults : {
		labelSeparator : Main.viewConfig.FORM_LABEL_SEPARATOR,
		labelAlign : "right",
		labelWidth : 100
	},

	defaults : {
		frame : false,
		border : false,
		bodyBorder : false,
		bodyStyle : " background:transparent "
	},

	initComponent : function() {
		this._initTranslation_();
		this.setViewModel(Ext.create(Ext.app.ViewModel, {}));
		this._runElementBuilder_();
		this.callParent(arguments);
		this._registerListeners_();
		if( this._temporaryEditorWindow_ !== true ){
			this._registerCommandInterceptors_();
		}
	},

	/**
	 * After the form is rendered invoke the record binding. This is necessary
	 * as the form may be rendered lazily(delayed) and the data-control may
	 * already have a current record set.
	 */
	afterRender : function() {
		this.callParent(arguments);
		this._syncBinding_();
		var ctrl = this._controller_;
        this.viewModel.setData({
            d : ctrl.record,
            p : ctrl.params
        });
		this._registerKeyBindings_();

		// acquire first time focus

		if (this._controller_.record && this._controller_.record.phantom && this._acquireFocusInsert_) {
			(new Ext.util.DelayedTask(this._gotoFirstNavigationItem_, this)).delay(200);
		}
		if (this._controller_.record && !this._controller_.record.phantom && this._acquireFocusUpdate_) {
			(new Ext.util.DelayedTask(this._gotoFirstNavigationItem_, this)).delay(200);
		}
	},

	_syncBinding_ : function() {
		if (this._controller_ && this._controller_.getRecord()) {
			this._onBind_(this._controller_.getRecord());
		} else {
			this._onUnbind_(null);
		}
	},

	beforeDestroy : function() {
		this._elems_.each(function(item) {
			delete item._dcView_;
		}, this);
		this.callParent(arguments);
	},
	// **************** Private API *****************

	/**
	 * Register event listeners
	 */
	_registerListeners_ : function() {
		var ctrl = this._controller_;

		// controller listeners

		this.mon(ctrl, "onEditIn", function() {
			this._syncBinding_();
		}, this);
		this.mon(ctrl, "recordChange", this._onController_recordChange_, this);
		this.mon(ctrl, "recordReload", this._onController_recordReload_, this);
		this.mon(ctrl, "readOnlyChanged", function() {
			this._applyStates_(this._controller_.getRecord());
		}, this);
		this.mon(ctrl, "disableChanged", function() {
			this._applyStates_(this._controller_.getRecord());
		}, this);

		this.mon(ctrl, "afterDoCancel", function() {
			this._applyStates_(this._controller_.getRecord());
		}, this);
		
		if (this._acquireFocusInsert_) {
			this.mon(ctrl, "afterDoNew", this._gotoFirstNavigationItem_, this);
		}
		if (this._acquireFocusUpdate_) {
			this.mon(ctrl, "onEditIn", this._gotoFirstNavigationItem_, this);
			this.mon(ctrl, "afterDoSaveSuccess", this._gotoFirstNavigationItem_, this);
		}
	},

	_disableFirstNavItemFocus_ : function(){
		var ctrl = this._controller_;
		this.mun(ctrl, "afterDoNew", this._gotoFirstNavigationItem_, this);
		this.mun(ctrl, "onEditIn", this._gotoFirstNavigationItem_, this);
		this.mun(ctrl, "afterDoSaveSuccess", this._gotoFirstNavigationItem_, this);
	},

	_registerCommandInterceptors_ : function() {
		
		var ctrl = this._controller_;
		
		if (ctrl.commands.doSave) {
			ctrl.commands.doSave.beforeExecute = Ext.Function.createInterceptor(ctrl.commands.doSave.beforeExecute,
			function() {
				this._controller_.fireEvent("beforeDoSave", this);
				if (this._shouldValidate_() && this.isVisible(true) && !this.getForm().isValid()) {
					this._controller_.error(Main.msg.INVALID_FORM, "msg");
					return false;
				} else {
					return true;
				}
			}, this, -1);
		}
		
		if (ctrl.commands.doDelete) {
			ctrl.commands.doDelete.beforeExecute = Ext.Function.createInterceptor(ctrl.commands.doDelete.beforeExecute,
			function() {
				this._controller_.fireEvent("beforeDoDelete", this);						
			}, this, -1);
		}
	},

	_gotoFirstNavigationItem_ : function() {
		// SONE-1634 : After saving and creating a new record values are kept in fields.
		// The combo-box is not taken the value from the record if is focused. Future investigations are needed.
		// This workaround with defer solve the problem, but is not the real wanted solution.
		var df = function(){
			var f = null;
			if (this._firstFocusItemName_) {
				f = this.query("[name="+this._firstFocusItemName_+"]")[0];
			}
			if (!f) {
				f = this.down("textfield");
			}
			if (f) {
				f.focus();
			}
		};
		Ext.defer(df,100,this);
	},

	/**
	 * When the current record of the data-control is changed bind it to the
	 * form.
	 */
	_onController_recordChange_ : function(evnt) {
		var newRecord = evnt.newRecord;

		this.viewModel.setData({
			d : newRecord,
			p : this._controller_.params
		});
	
		if (newRecord !== this.getForm()._record) {
			this._onUnbind_();
			this._onBind_(newRecord);
		}
	},

	_onController_recordReload_ : function(evnt) {
		var r = evnt.record;
		if (this.getForm()._record === r) {
			this._applyStates_(r);
		}
	},

	_checkTrackEditMode_ : function(record) {
		var ctrl = this._controller_;
		var res = false;
		if ((ctrl.trackEditMode && !ctrl.isEditMode) && !(record && record.phantom)) {
			res = true;
		}
		return res;
	},

	/**
	 * Bind the current record of the data-control to the form.
	 * 
	 */
	_onBind_ : function(record) {
		var ctrl = this._controller_;
		if (ctrl.trackEditMode && !ctrl.isEditMode) {
			return;
		}
		if (record) {
			if(this.el){
				this.unmask();
			}
			var i, l;
			if (this._images_ != null) {
				l = this._images_.length;
				for (i = 0; i < l; i++) {
					var img = this._getElement_(this._images_[i]);
					if( img ){
						img.setSrc(record.get(img.dataIndex));
					}
				}
			}
			if (this._numberfields_ != null) {
				l = this._numberfields_.length;
				for (i = 0; i < l; i++) {
					var nf = this._getElement_(this._numberfields_[i]);
					if( nf ){
						nf._safeSetValue_(record.get(nf.dataIndex));
					}
				}
			}
			this.getForm()._record = record;
			this._applyStates_(record);
			this.getForm().isValid();
		}
		this._afterBind_(record);
	},

	/**
	 * Un-bind the record from the form.
	 */
	_onUnbind_ : function() {
		var ctrl = this._controller_;
		if (ctrl.trackEditMode && !ctrl.isEditMode) {
			return;
		}
		var _r = this.getForm()._record;
		if (_r) {
			if (this._images_ != null) {
				for (var i = 0, l = this._images_.length; i < l; i++) {
					var img = this._getElement_(this._images_[i]);
					img.setSrc("");
				}
			}
			if(this.el && this._skipMaskWhenNoRecordInDc_ !== true){
				this.mask();
			}
			this.getForm()._record = null;
			this._elems_.each(function(item_) {
				var item = this._get_(item_.name);
				if (!item) {
					return;
				}
				if( Ext.isFunction(item.setInEditMode) ){
					item.setInEditMode(false);
				}
			}, this);
		}
		this._afterUnbind_(_r);
	},


	__applyStatesForPhantomRecord__ : function(record){
		this._elems_.each(function(item_) {
			var item = this._get_(item_.name);
			if (!item) {
				return;
			}
			if( Ext.isFunction(item.clearUiFlags) ){
				item.clearUiFlags();
			}
			if (item._visibleFn_ !== undefined && item._visibleFn_ !== null) {
				item.setVisible(this._canSetVisible_(item.name, record));
			}
			if (item.noEdit === true || item.noInsert === true) {
				item._disable_();
			} else {
				if (item._enableFn_ !== undefined && item._enableFn_ !== null) {
					item._setDisabled_(!this._canSetEnabled_(item.name, record));
				} else {
					item._enable_();
				}
			}
		}, this);
	},
	
	__applyStatesForRealRecord__ : function(record){
		this._elems_.each(function(item_) {
			var item = this._get_(item_.name);
			if (!item) {
				return;
			}
			if( Ext.isFunction(item.clearUiFlags) ){
				item.clearUiFlags();
			}
			if (item._visibleFn_ !== undefined && item._visibleFn_ !== null) {
				item.setVisible(this._canSetVisible_(item.name, record));
			}
			if (item.noEdit === true || item.noUpdate === true) {
				item._disable_();
			} else {
				if (item._enableFn_ !== undefined && item._enableFn_ !== null) {
					item._setDisabled_(!this._canSetEnabled_(item.name, record));
				} else {
					item._enable_();
				}
			}
		}, this);
	},
	
	__setOnFocusVal__ : function(){
		Ext.defer(function(){
			this._elems_.each(function(item_) {
				var item = this._get_(item_.name);
				if (!item) {
					return;
				}
				if( item._setOnFocusVal_ ){
					item._setOnFocusVal_(item.getRawValue());
				}
			}, this);
		}, 200, this);
	},

	/**
	 * The edit-form specific state rules. The flow is: If the fields are marked
	 * with noInsert, noUpdate or noEdit these rules are applied and no other
	 * option checked If no such constraint, the _canSetEnabled_ function is
	 * checked for each element.
	 * 
	 */
	_onApplyStates_ : function(record) {
		// the form has been disabled by the (un)bind.
		// Nothing to change
		if (record === null || record === undefined) {
			return;
		}
		if (this._shouldDisableWhenDcIsReadOnly_ && this._controller_.isReadOnly() && this._hasVisibilityRules_ !== true) {
			this._doDisableWhenDcIsReadOnly_();
			return;
		}
		if (this._controller_.isDisabled() && this._hasVisibilityRules_ !== true) {
			this._doDisableWhenDcIsDisabled_();
			return;
		}
		if (record.phantom) {
			this.__applyStatesForPhantomRecord__(record);
		} else {
			this.__applyStatesForRealRecord__(record);
		}
		this.__setOnFocusVal__();
		
		if (this._shouldDisableWhenDcIsReadOnly_ && this._controller_.isReadOnly()) {
			this._doDisableWhenDcIsReadOnly_();
		}
		if (this._controller_.isDisabled()) {
			this._doDisableWhenDcIsDisabled_();
		}
		
		if (this._shouldDisableIfDcIsMasked_ && this._controller_._maskEditable_) {
			this._doMaskFields_();
		}
	},

	_doDisableWhenDcIsReadOnly_ : function() {
		if (this._shouldDisableWhenDcIsReadOnly_ && this._controller_.isReadOnly()) {
			this["_doDisableWhenDcIsReadOnly_" + this._disableModeWhenDcIsReadOnly_ + "_"]();
		}
	},
	
	_doMaskFields_ : function() {
		this.getForm().getFields().each(this._disableElement_);
	},
	
	_doDisableWhenDcIsDisabled_ : function(){
		if (this._controller_.isDisabled()) {
			this["_doDisableWhenDcIsReadOnly_" + this._disableModeWhenDcIsReadOnly_ + "_"]();
		}
	},

	_doDisableWhenDcIsReadOnly_fields_ : function() {
		this.getForm().getFields().each(this._disableElement_);
	},

	_doDisableWhenDcIsReadOnly_panel_ : function() {
		this.disable();
	},

	_doDisableWhenDcIsReadOnly_elems_ : function() {
		this._elems_.each(this._disableElement_);
	},

	_disableElement_ : function(e) {
		e._disable_();
	},

	_registerKeyBindings_ : function() {
		return new Ext.util.KeyMap({
			target : this.getEl(),
			eventName : 'keydown',
			processEvent : function(event) { // possible parameters: source, options
				return event;
			},
			binding : [ Ext.apply(KeyBindings.values.dc.doNew, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doNew();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doCancel, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doCancel();
					if (this._controller_.record == null) {
						this.focus();
					}
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doSave, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doSave();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doCopy, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doCopy();
					this._controller_.doEditIn();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doEditOut, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doEditOut();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.nextRec, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doNextRec();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.prevRec, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doPrevRec();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.nextPage, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doNextPage();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.prevPage, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doPrevPage();
				},
				scope : this
			}),

			Ext.apply(KeyBindings.values.dc.doEnterQuery, {
				fn : function(keyCode, e) {
					e.stopEvent();
					if (!this._controller_.trackEditMode || !this._controller_.isEditMode) {
						this._controller_.doEnterQuery();
					}
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doClearQuery, {
				fn : function(keyCode, e) {
					e.stopEvent();
					if (!this._controller_.trackEditMode || !this._controller_.isEditMode) {
						this._controller_.doClearQuery();
					}
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doQuery, {
				fn : function(keyCode, e) {
					e.stopEvent();
					if (!this._controller_.trackEditMode || !this._controller_.isEditMode) {
						this._controller_.doQuery();
					}
				},
				scope : this
			}), {
				key : Ext.event.Event.TAB,
				ctrl : false,
				shift : false,
				alt : false,
				fn : function(keyCode, e) {
					return this._processTabKey_(e, false);
				},
				scope : this
			}, {
				key : Ext.event.Event.TAB,
				ctrl : false,
				shift : true,
				alt : false,
				fn : function(keyCode, e) {
					return this._processTabKey_(e, true);
				},
				scope : this
			} ]
		});
	},
	
	_getFocusableFields_ : function(){
		var formFields = this.getForm().getFields().items;
		var ret = [];
		for(var i=0,l=formFields.length;i<l;i++){
			if(formFields[i].isFocusable()){
				ret.push(formFields[i]);
			}
		}
		return ret;
	},
	
	_processTabKey_ : function(e, shift){
		var focusNextInvalidField = function(formFields){
			var i, l=formFields.length;
			if( shift ){
				for(i=l-2;i>=0;i--){
					if(!formFields[i].wasValid){
						formFields[i].focus();
						break;
					}
				}
			} else {
				for(i=0;i<l-1;i++){
					if(!formFields[i].wasValid){
						formFields[i].focus();
						break;
					}
				}
			}
		};
		if( !this.isValid() ){
			var formFields = this._getFocusableFields_();
			if( formFields.length ){
				var focusedItem = Ext.ComponentManager.getActiveComponent();
				var lastField = (shift?formFields[0]:formFields[formFields.length-1]);
				if( lastField === focusedItem ){
					e.stopEvent();
					if( lastField.wasValid ){
						focusNextInvalidField(formFields);
					}
				}
			}
		}
	},
	

	// wizard steps
	
	_isStepDone_ : function(stepName, stepsCompleted){
		var a = stepName.split(" ");
		stepsCompleted = ""+stepsCompleted;
		for(var i=0, l=a.length; i<l; i++){
			if( a[i].indexOf("step-") === 0 ){
				var n = a[i].substring(5);
				if( n <= stepsCompleted){
					return true;
				}
				return false;
			}
		}
		return false;
	},
	
	_updateSteps_ : function(){
		var parent = Ext.get(this._topPnlId_);
		var wizardSteps = parent.select(".sone-wizard-step").elements;
		var wizardLine = parent.select(".sone-wizard-line").elements[0];
		
		Ext.each(wizardSteps, function(e) {
			var glyphContainer = e.getElementsByClassName("sone-step-indicator")[0];
			if (this._isStepDone_(e.className,this._wizardStepsCompleted_)) {
				e.classList.remove("step-pending");
				e.classList.add("step-done");
				glyphContainer.innerHTML = "<i class=\'fa fa-2x fa-check\' style=\'color: #FFFFFF\'></i>";
			} else {
				e.classList.remove("step-done");
				e.classList.add("step-pending");
				glyphContainer.innerHTML = "<i class=\'fa fa-2x fa-times\' style=\'color: #BBBBBB\'></i>";
			}
		}, this);
		var x = 0;
		if(this._wizardMaxSteps_ > 1){
			x = (100*(this._wizardStepsCompleted_-1))/(this._wizardMaxSteps_-1);
		}
		if(this._wizardStepsCompleted_>1){
			wizardLine.style.background = "linear-gradient(90deg, #a0d468 "+x+"%, #e5e5e5 "+x+"%)";
		} else {
			wizardLine.style.background = "#e5e5e5";
		}
	},
	
	nextStep : function(){
		if( this._wizardStepsCompleted_ < this._wizardMaxSteps_ ){
			this._wizardStepsCompleted_++;
			this._updateSteps_();
		}
		return this._wizardStepsCompleted_;
	},
	
	prevStep : function(){
		if( this._wizardStepsCompleted_ > 0 ){
			this._wizardStepsCompleted_--;
			this._updateSteps_();
		}
		return this._wizardStepsCompleted_;
	},
	
	getStep : function(){
		return this._wizardStepsCompleted_;
	},
	
	// step is working from 0 to label numbers
	// 0 is when nothing is marked with green
	setStep : function(crtStep){
		if( crtStep >= 0 && crtStep <= this._wizardMaxSteps_ ){
			this._wizardStepsCompleted_ = crtStep;
			this._updateSteps_();
		}
		return this._wizardStepsCompleted_;
	}

});
