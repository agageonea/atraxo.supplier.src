/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.dc.tools.DcCustomExportWindow", {
	extend : "Ext.Window",
	title : Main.translate("dcExp", "title"),
	border : true,
	width : 350,
	resizable : false,
	closable : true,
	constrain : true,
	modal : true,
	_grid_ : null,

	items : {
		xtype : "form",
		frame : true,
		defaultType : "textfield",
		bodyPadding : 10,
		defaults : {
			anchor : "-20"
		},
		buttonAlign : "center",
		fieldDefaults : {
			labelAlign : "right",
			labelWidth : 100,
			msgTarget : "side",
			selectOnFocus : true,
			allowBlank : false
		},

		buttons : [ {
			text : Main.translate("tlbitem", "ok__lbl"),
			formBind : true,
			disabled : true,
			handler : function() {
				this.up("form").executeTask();
				this.up("window").hide();
			}
		} ],

		items : [ {
			name : "fld_format",
			fieldLabel : Main.translate("dcExp", "format"),
			xtype : "combo",
			forceSelection : true,
			triggerAction : "all",
			store : [ "pdf", "html", "doc", "docx", "xls", "xlsx", "csv" ],
			value : "pdf"
		}, {
			fieldLabel : Main.translate("dcExp", "columns"),
			xtype : "radiogroup",
			columns : 1,
			items : [ {
				name : "fld_columns",
				boxLabel : Main.translate("dcExp", "col_visible"),
				inputValue : 'visible',
				checked : true
			}, {
				name : "fld_columns",
				boxLabel : Main.translate("dcExp", "col_all"),
				inputValue : 'all'
			} ]
		} ],

		/**
		 * Handler. Run in button scope
		 */
		executeTask : function() {

			var form = this.getForm();
			var wdw = this.up("window");
			var grid = wdw._grid_;
			var val = form.getValues();
			var ctrl = grid._controller_;

			var _exp = {
				title : ctrl.getFrame()._title_,
				layout : val.fld_layout
			};
			var url = Main.dsAPI(ctrl.dsName, val.fld_format);
			var _p = ctrl.buildRequestParamsForQuery();

			var sortCols = "";
			var sortDirs = "";
			var first = true;
			
			ctrl.store.sorters.each(function(item) {
				
				if (!first) {
					sortCols += ",";
					sortDirs += ",";
				}
				sortCols += item._property;
				sortDirs += item._direction || "ASC";
				first = false;
			}, this);

			_p[Main.requestParam.SORT] = sortCols;
			_p[Main.requestParam.SENSE] = sortDirs;

			var cm;
			if (val.fld_columns !== "all") {
				cm = grid.down('headercontainer').getVisibleGridColumns();
			} else {
				cm = grid.down('headercontainer').getGridColumns();
			}
			
//			var excludeColumns = [ "refid", "id", "modifiedBy", "modifiedAt", "createdAt", "createdBy", "" ];
			var excludeColumns = [ "id", "" ];

			var len = cm.length;
			var _cols = [];
			for (var i = 0; i < len; i++) {

				if (excludeColumns.indexOf(cm[i].dataIndex) === -1 && !Ext.isEmpty(cm[i].dataIndex) && !Ext.isEmpty(cm[i].text)) {
					_cols[_cols.length] = {
						name : cm[i].dataIndex,
						title : cm[i].text.replace(",", " "),
						width : cm[i].cellWidth,
						mask : cm[i]._mask_,
						align : cm[i].xtype === "numbercolumn" ? "right" : "left"
					}
				}

			}
			_exp.columns = _cols;
			
			var excludeFields = [];
			if (!Ext.isEmpty(ctrl.dcContext)) {
				var dcContext = ctrl.dcContext.ctxData;
				for (var z = 0; z<dcContext.length; z++) {
					excludeFields.push(dcContext[z].name);
				}
			}
			
			var excludeJson = {
					fields: excludeFields
			};
			
			_p[Main.requestParam.EXPORT_INFO] = Ext.encode(_exp);
			_p[Main.requestParam.EXCLUDE_FIELDS] = Ext.encode(excludeJson);
			_p["download"] = false;

			Ext.Msg.progress(Main.translate("msg", "working"));
			getApplication().startCheckNotificationTask(true);
			Ext.Ajax.request({
				url : url["report"],
				params : _p,
				success : function() {
					Ext.Msg.hide();
					Ext.Msg.show({
			       		title : "Report",
			       		msg : "The generation of the report has been initiated. A message will appear in the Notification Center when this process is finalized.",
			       		icon : Ext.MessageBox.INFO,
			       		buttons : Ext.MessageBox.OK,			       		
	       				scope : this
					});
				},
				failure : function(response) {
					Ext.Msg.hide();
					Main.serverMessage(null, response);
				}
			});
			return;
		}
	}

});
