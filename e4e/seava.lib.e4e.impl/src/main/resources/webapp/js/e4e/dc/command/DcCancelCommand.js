/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.dc.command.DcCancelCommand", {
	extend : "e4e.dc.command.AbstractDcSyncCommand",

	dcApiMethod : e4e.dc.DcActionsFactory.CANCEL,
	
	onExecute : function() { // parameters: options
		var dc = this.dc;
		var isNew = (dc.getRecordStatus()==="insert")?true:false;
		if (dc.store.getCount() === 0) {
			this.discardChanges();
		} else {
			this.discardChildrenChanges();
			this.discardChanges();
		}
		if(dc.record == null){
			Ext.Function.defer(this.selectRecord,100,this,[isNew]);
		}
	},

	discardChanges : function() {

		var dc = this.dc;
		var s = dc.store;

		if (dc.record && dc.record.phantom) {
			/*
			 * workaround to avoid the dirty check in AbstractDc.setRecord
			 */
			var cr = dc.record;
			cr.phantom = false;
			cr.dirty = false;
			dc.setRecord(null, true);
			cr.phantom = true;
			cr.dirty = true;
		}
		s.rejectChanges();
	},

	discardChildrenChanges : function() {
		var dc = this.dc;
		var l = dc.children.length;
		for ( var i = 0; i < l; i++) {
			if (dc.children[i].isDirty()) {
				dc.children[i].doCancel();
			}
		}
	},

	selectRecord : function(wasNew) {
		var dc = this.dc;
		if( !dc.record ){
			dc.doDefaultSelection();
		}
		if( (!dc.record || wasNew) && !dc._suspendEditOut_ ){
			dc.doEditOut();
		}
	}
});
