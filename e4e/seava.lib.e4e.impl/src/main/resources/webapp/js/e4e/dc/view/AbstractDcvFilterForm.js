/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.dc.view.AbstractDcvFilterForm", {
	extend : "e4e.dc.view.AbstractDc_Form",

	// **************** Properties *****************
	/**
	 * Component builder
	 */
	_builder_ : null,

	/**
	 * Helper property to identify this dc-view type as filter-form.
	 */
	_dcViewType_ : "filter-form",

	// **************** Public API *****************

	/**
	 * Returns the builder associated with this type of component. Each
	 * predefined data-control view type has its own builder. If it doesn't
	 * exist yet attempts to create it.
	 */
	_getBuilder_ : function() {
		if (this._builder_ == null) {
			this._builder_ = new e4e.dc.view.DcvFilterFormBuilder({
				dcv : this
			});
		}
		return this._builder_;
	},

	// **************** Defaults and overrides *****************

	frame : true,
	border : true,
	bodyPadding : '8px 5px 3px 5px',
	maskDisabled : false,
	layout : "fit",
	buttonAlign : "left",
	bodyCls : 'dcv-filter-form',
	fieldDefaults : {
		labelSeparator : Main.viewConfig.FORM_LABEL_SEPARATOR,
		labelAlign : "right",
		labelWidth : 100
	},

	defaults : {
		frame : false,
		border : false,
		bodyBorder : false,
		bodyStyle : " background:transparent "
	},

	initComponent : function(config) {
		this._initTranslation_();
		this.setViewModel(Ext.create(Ext.app.ViewModel, {}));
		this._runElementBuilder_();
		this.callParent(arguments);
		this._registerListeners_();
	},

	/**
	 * After the form is rendered invoke the record binding. This is necessary
	 * as the form may be rendered lazily(delayed) and the data-control may
	 * already have a current record set.
	 */
	afterRender : function() {
		
		this.callParent(arguments);
		var ctrl = this._controller_;
        this.viewModel.setData({
            d : ctrl.filter,
            p : ctrl.params
        });
        
		this._registerKeyBindings_();
	},

	beforeDestroy : function() {
		this._elems_.each(function(item) {
			delete item._dcView_;
		}, this)
		this.callParent(arguments);
	},

	// **************** Private API *****************

	/**
	 * Register event listeners
	 */
	_registerListeners_ : function() {
		var ctrl = this._controller_;

		// When enter-query requested, move the focus to the
		// first navigation item.
		var s = this;
		this.mon(ctrl, "onEnterQuery", this._gotoFirstNavigationItem_, s);		 
 

		var cmd = ctrl.commands.doQuery;
		if (cmd) {
			var fn = function() {
				if (this._shouldValidate_() && !this.getForm().isValid()) {
					ctrl.error(Main.msg.INVALID_FILTER, "msg");
					return false;
				} else {
					return true;
				}
			};
			cmd.beforeExecute = Ext.Function.createInterceptor(
					cmd.beforeExecute, fn, this, -1);
		}
	},

	_gotoFirstNavigationItem_ : function() {
		this.down(" textfield").focus();
	},

	/**
	 * Bind the current filter model of the data-control to the form.
	 * 
	 */
	_onBind_ : function(filter) {
		this._applyStates_(filter);
		this._afterBind_(filter);
	},

	/**
	 * Un-bind the filter from the form.
	 */
	_onUnbind_ : function(filter) {
		this._afterUnbind_(filter);
	},

	
	_registerKeyBindings_ : function() {
		return new Ext.util.KeyMap({
			target : this.getEl(),
			eventName : 'keydown',
			processEvent : function(event) {
				return event;
			},
			binding : [ Ext.apply(KeyBindings.values.dc.doEnterQuery, {
				fn : function(keyCode, e) {
					e.stopEvent();					
					this._controller_.doEnterQuery();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doClearQuery, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doClearQuery();
					this._controller_.doEnterQuery();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doQuery, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doQuery();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doEditOut, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doEditOut();
				},
				scope : this
			}) ]
		});
	}

});