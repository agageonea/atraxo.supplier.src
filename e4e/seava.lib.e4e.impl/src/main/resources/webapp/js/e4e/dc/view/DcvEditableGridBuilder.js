/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
/**
 * Builder for edit-grid views.
 */
Ext.define("e4e.dc.view.DcvEditableGridBuilder", {
	extend : "Ext.util.Observable",

	dcv : null,

	/**
	 * Create a text-column. If no editor specified and the column is not marked
	 * with noEdit:true, it creates a default text editor
	 */
	addTextColumn : function(config) {

		Ext.applyIf(config, {
			xtype : "gridcolumn",
			noInsert : false,
			noUpdate : false
		});

		if (!config.editor && config.noEdit !== true) {
			config.editor = {
				xtype : "textfield",
				selectOnFocus : true,
				noInsert : config.noInsert,
				noUpdate : config.noUpdate,
				allowBlank : ((config.allowBlank === false) ? false : true)
			}
			if (config.maxLength) {
				config.editor.maxLength = config.maxLength;
			}
		}

		if (config.editor) {
			if (config.editor.maxLength) {
				config.editor.enforceMaxLength = true;
			}
			if (config.caseRestriction) {
				config.editor.caseRestriction = config.caseRestriction;
			}
		}
		this.applySharedConfig(config);
		return this;
	},

	/**
	 * Creates a date-column. If no editor specified and the column is not
	 * marked with noEdit:true, it creates a default date editor
	 */
	addDateColumn : function(config) {
		var _mask_ = Masks.DATE;
		Ext.applyIf(config, {
			xtype : "datecolumn",
			noInsert : false,
			noUpdate : false,
			_mask_ : _mask_
		});

		config.format = Main[config._mask_];

		if (!config.editor && config.noEdit !== true) {
			config.editor = {
				xtype : "datefield",
				format : Main[config._mask_],
				altFormats : Main.ALT_FORMATS,
				_mask_ : config._mask_,
				selectOnFocus : true,
				noInsert : config.noInsert,
				noUpdate : config.noUpdate,
				allowBlank : ((config.allowBlank === false) ? false : true)
			}
		}
		
		if( config.editor ){
			config.editor.listeners = config.editor.listeners || {};
			Ext.applyIf(config.editor.listeners, config.listeners);
		}
		
		this.applySharedConfig(config);
		return this;
	},

	/**
	 * Creates a date-column. If no editor specified and the column is not
	 * marked with noEdit:true, it creates a default time editor
	 */
	addTimeColumn : function(config) {
		var _mask_ = Masks.TIME;
		Ext.applyIf(config, {
			xtype : "datecolumn",
			noInsert : false,
			noUpdate : false,
			_mask_ : _mask_
		});

		config.format = Main[config._mask_];

		if (!config.editor && config.noEdit !== true) {
			config.editor = {
				xtype : "timefield",
				format : Main[config._mask_],
				_mask_ : config._mask_,
				selectOnFocus : true,
				noInsert : config.noInsert,
				noUpdate : config.noUpdate,
				allowBlank : ((config.allowBlank === false) ? false : true),
				growToLongestValue : false,
				enforceMaxLength : true,
				maxLength : 11,
				increment : 1,
				forceSelection : true
			}
		}
		
		if( config.editor ){
			config.editor.listeners = config.editor.listeners || {};
			Ext.applyIf(config.editor.listeners, config.listeners);
		}
		
		this.applySharedConfig(config);
		return this;
	},
	
	/**
	 * Creates a number-column. If no editor specified and the column is not
	 * marked with noEdit:true, it creates a default number editor
	 */
	addNumberColumn : function(config) {
		if (typeof(_SYSTEMPARAMETERS_) !== "undefined" && typeof(_SYSTEMPARAMETERS_) !== "null" 
				&& config.sysDec && !config.decimals ){
			var d = _SYSTEMPARAMETERS_[config.sysDec];
			if( Ext.isNumeric(d) ){
				config.decimals = 1*d;
			}
		}
		
		Ext.applyIf(config, {
			xtype : "numbercolumn",
			format : Main.getNumberFormat(config.decimals || 0),
			align : "right",
			noInsert : false,
			noUpdate : false,
			maxLength : 19
		});

		if (!config.editor && config.noEdit !== true) {
			config.editor = {
				xtype : "numberfield",
				format : config.format,
				decimalPrecision : config.decimals,
				selectOnFocus : true,
				noInsert : config.noInsert,
				noUpdate : config.noUpdate,
				allowBlank : ((config.allowBlank === false) ? false : true),
				fieldStyle : "text-align:right;",
				hideTrigger : true,
				keyNavEnabled : false,
				mouseWheelEnabled : false 
			}
		}

		if( config.editor ){
			if (config.decimals) {
				config.editor.maxLength = config.maxLength + 2;
			} else {
				config.editor.maxLength = config.maxLength + 1;
			}
			config.editor.enforceMaxLength = true;
			
			config.editor.listeners = config.editor.listeners || {};
			Ext.applyIf(config.editor.listeners, config.listeners);
		}
		
		this.applySharedConfig(config);
		return this;
	},

	addButtonColumn : function(config) {
		Ext.applyIf(config, {
            xtype: "widgetcolumn",
			noSort : true,
			noFilter : true,
            widget: {
                xtype: "button",
                glyph: config.glyph,
                handler: config.handler,
                listeners : config.listeners,
                disabled : config.disabled,
                _enableFn_ : config._enableFn_,
                _getController_ : function(){
                	var ctr = null;
                	try {
                		ctr = this.getWidgetColumn().getView().grid._controller_;
                	}catch(ex){
                		// nothing to do
                	}
                	return ctr;
                },
                _getGrid_ : function(){
                	var g = null;
                	try {
                		g = this.getWidgetColumn().getView().grid;
                	}catch(ex){
                		// nothing to do
                	}
                	return g;
                }
            },
            onWidgetAttach : function(col,item,record){ 
            	if( item && Ext.isFunction(item._enableFn_) ){
        			if( item._enableFn_(record,item) ){
        				item.enable();
        			} else {
        				item.disable();
        			}
            	}
            }
		});
		this.applySharedConfig(config);
		return this;
	},
	
	addBooleanColumn : function(config) {
		Ext.applyIf(config, {
			width : Main.viewConfig.BOOLEAN_COL_WIDTH
		});

		if (config.noEdit !== true) {
			config.xtype = "checkcolumn";
			config.listeners = config.listeners || {};
			config.listeners.beforecheckchange = { fn: this.dcv.beforecheckchange, scope: this.dcv };			
		} else {
			config.xtype = "booleancolumn";
			Ext.apply(config, {
				trueText : Main.translate("msg", "bool_true"),
				falseText : Main.translate("msg", "bool_false")
			});
		}
		this.applySharedConfig(config);
		return this;
	},

	addComboColumn : function(config) {
		config.xtype = "gridcolumn";
		if( config.editor && Ext.isArray(config.editor.store) ){
			Ext.applyIf(config.editor, {
				pageSize : 0
			});
		}
		this.applySharedConfig(config);
		return this;
	},

	addLov : function(config) {		
		Ext.applyIf(config.editor, {
			forceSelection : true
		});
		this.applySharedConfig(config);
		return this;
	},

	shouldCreateField : function(r, cols, name) {
        return (r.fieldsMap[name] && !cols.containsKey(name));
    },

	add : function(config) {
		this.applySharedConfig(config);
		return this;
	},

	merge : function(name, config) {
		Ext.applyIf(this.dcv._columns_.get(name), config);
		return this;
	},

	change : function(name, config) {
		Ext.apply(this.dcv._columns_.get(name), config);
		return this;
	},

	remove : function(name) {
		this.dcv._columns_.remove(name);
		return this;
	},

	// private

	applySharedConfig : function(config) {
		// read the settings from recordModel
		var rm = this.dcv._controller_.recordModel;
		var rf = rm.fieldsMap[config.dataIndex] || {};
		var rcfg = {
			noSort : rf.noSort,
			noFilter : rf.noFilter,
			alwaysHidden : rf.alwaysHidden
		};
		// but keep the settings from given config if they exists
		Ext.applyIf(config,rcfg)
		
		Ext.applyIf(config, {
			id : Ext.id(),
			headerId : config.name,
			selectOnFocus : true,
			sortable : (config.noSort===true) ? false : true,
			hidden : (config.alwaysHidden===true) ? true : false,
			_dcView_ : this.dcv
		});
		if (config.editor) {
			Ext.applyIf(config.editor, {
				_dcView_ : this.dcv,
				_enableFn_ : config._enableFn_
			});
		}
		this.dcv._columns_.add(config.name, config);
	},

	addDefaults : function() {
		var r = Ext.create(this.dcv._controller_.recordModel, {});
		var cols = this.dcv._columns_;

		if (this.shouldCreateField(r, cols, "notes")) {
			this.addTextColumn({
				name : "notes",
				dataIndex : "notes",
				hidden : true,
				width : 150,
				alwaysHidden : true,
				noSort : true,
				noFilter : true
			});
		}

		if (this.shouldCreateField(r, cols, "createdAt")) {
			this.addDateColumn({
				name : "createdAt",
				dataIndex : "createdAt",
				hidden : true,
				noEdit : true,
				width : 140,
				_mask_ : Masks.DATETIMESEC,
				//alwaysHidden : true,
				noSort : false,
				//noFilter : true
			});
		}

		if (this.shouldCreateField(r, cols, "modifiedAt")) {
			this.addDateColumn({
				name : "modifiedAt",
				dataIndex : "modifiedAt",
				hidden : true,
				noEdit : true,
				width : 140,
				_mask_ : Masks.DATETIMESEC,
				//alwaysHidden : true,
				noSort : false,
				//noFilter : true
			});
		}

		if (this.shouldCreateField(r, cols, "createdBy")) {
			this.addTextColumn({
				name : "createdBy",
				dataIndex : "createdBy",
				hidden : true,
				noEdit : true,
				width : 100,
				//alwaysHidden : true,
				noSort : false,
				//noFilter : true
			});
		}

		if (this.shouldCreateField(r, cols, "modifiedBy")) {
			this.addTextColumn({
				name : "modifiedBy",
				dataIndex : "modifiedBy",
				hidden : true,
				noEdit : true,
				width : 100,
				//alwaysHidden : true,
				noSort : false,
				//noFilter : true
			});
		}

		if (this.shouldCreateField(r, cols, "id")) {
			this.addTextColumn({
				name : "id",
				dataIndex : "id",
				hidden : true,
				noEdit : true,
				width : 100,
				alwaysHidden : true,
				noSort : true,
				noFilter : true
			});
		}

		if (this.shouldCreateField(r, cols, "refid")) {
			this.addTextColumn({
				name : "refid",
				dataIndex : "refid",
				hidden : true,
				noEdit : true,
				width : 100,
				//alwaysHidden : true,
				noSort : true,
				//noFilter : true
			});
		}
		return this;
	},

	// view and filter toolbar related functions
	
	addDisplayFieldText : function(config) {
		config.xtype = "displayfieldtext";
		Ext.applyIf(config, {
			//anchor : "-20",
			cls : "sone-toolbar-displayfield"
		});
		this.tbiApplySharedConfig(config);
		return this;
	},

	addDisplayFieldNumber : function(config) {
		config.xtype = "displayfieldnumber";
		Ext.applyIf(config, {
			//anchor : "-20",
			format : Main.getNumberFormat(config.decimals || 0),
			cls : "sone-toolbar-displayfieldnumber"
		});
		this.tbiApplySharedConfig(config);
		return this;
	},

	addDisplayFieldDate : function(config) {
		config.xtype = "displayfielddate";
		Ext.applyIf(config, {
			//anchor : "-20",
			cls : "sone-toolbar-displayfield"
		});
		this.tbiApplySharedConfig(config);
		return this;
	},

	addDisplayFieldBoolean : function(config) {
		config.xtype = "displayfieldboolean";
		Ext.applyIf(config, {
			// anchor:"-20" ,
			cls : "sone-toolbar-displayfield"
		});
		this.tbiApplySharedConfig(config);
		return this;
	},
	
	tbiApplySharedConfig : function(config) {
		Ext.applyIf(config, {
			id : Ext.id(),
			_onViewToolbar_ : true,
			_enableAutoLabelWidth_ : true,
			_dcView_ : this.dcv,
			labelSeparator : Main.viewConfig.FORM_LABEL_SEPARATOR,
			labelAlign : "right",
			align : "left"
		});
		
		this.dcv._elems_.add(config.name, config);
	}

});
