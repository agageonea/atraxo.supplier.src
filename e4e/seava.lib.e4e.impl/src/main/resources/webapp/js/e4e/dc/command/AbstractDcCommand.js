/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
/**
 * Abstract base class for any kind of command. Do not directly subclass this
 * one but use one of the provided children depending on what type of command
 * you need. See the synchronous and asynchronous commands for details.
 */
Ext.define("e4e.dc.command.AbstractDcCommand", {

	/**
	 * Data-control on which this command is invoked.
	 * 
	 * @type e4e.dc.AbstractDc
	 */
	dc : null,

	/**
	 * DC API method which delegates to this command
	 * 
	 * @type String
	 */
	dcApiMethod : null,

	/**
	 * Flag to set if this command needs explicit confirmation from the user in
	 * order to execute.
	 */
	confirmByUser : false,

	locked : false,

	/**
	 * User confirmation message title.
	 */
	confirmMessageTitle : "",

	/**
	 * User confirmation message body.
	 */
	confirmMessageBody : "Please confirm action",

	/**
	 * Flag to display working message box
	 */
	showWorking : null,
	
	/**
	 * Constructor
	 */
	constructor : function(config) {
		config = config || {};
		Ext.apply(this, config);
		this.callParent(arguments);
	},

	/**
	 * Template method where subclasses or external contexts can provide
	 * additional logic. If it returns false the execution is stopped.
	 */
	
	beforeExecute : function(options) {
		var m = this.dc["beforeDo" + this.dcApiMethod];
		if (m !== undefined && m !== null && Ext.isFunction(m)) {
			return m.call(this.dc, options);
		} else {
			return true;
		}
	},

	/**
	 * Template method where subclasses or external contexts can provide
	 * additional logic. Called after the execution is finished. ATTENTION:
	 * Commands which initiate AJAX calls do not have the result of the remote
	 * call available here. Provide callbacks for such situations.
	 */
	afterExecute : function(options) {
		var m = this.dc["afterDo" + this.dcApiMethod];
		if (m !== undefined && m !== null && Ext.isFunction(m)) {
			m.call(this.dc, options);
		}
		this.dc.fireEvent("afterDo" + this.dcApiMethod, this.dc, options);
	},

	/**
	 * Provide whatever extra-logic to check if the command can be executed.
	 */
	canExecute : function(options) {
		var m = this.dc["canDo" + this.dcApiMethod];
		if (m !== undefined && m !== null) {
			if (Ext.isFunction(m)) {
				return m.call(this.dc, this.dc, options);
			} else {
				return m;
			}
		} else {
			return true;
		}
	},

	/**
	 * Default routine which ask confirmation from the user to proceed. Called
	 * if confirmByUser = true.
	 */
	confirmExecute : function(btn, options) {
		if (!btn) {
			Ext.Msg.show({
			    msg: this.confirmMessageBody,
			    buttons: Ext.MessageBox.YES + Ext.MessageBox.NO,
			    fn: function(btnId) {
			        if (btnId === "yes") {
			        	options.confirmed = true;
			        	this.execute(options);
			        }
			    },
			    scope: this
			});
		}
	},

	needsConfirm : function() { // parameter: options
		return this.confirmByUser;
	},

	/**
	 * Calls the appropriate method from the action-state manager.
	 */
	isActionAllowed : function() { // parameter: options
		return true;
	},
	
	_fireEvent_ : function(e,opt){
		this.dc.fireEvent(e, this.dc, opt);
	},

	_getTime_ : function(){
		return new Date().getTime();
	},
	
	_getRecId_ : function(){
		var id = null;
		var rec = this.dc.getRecord();
		if( rec ){
			id = rec.data.id;
		}
		return id;
	},
	
	_timeoutExpired_ : function(options){
		var res = true;
		if(options.modal && options.name){
			res = !this.dc._storeIsLoading_;
			if( res ){
				var c = this.dc._executedCommands_[options.name];
				if(c){
					res = (this.dc._cmdExecutionTimeout_ < (this._getTime_()-c.time)) ||
						(c.id !== this._getRecId_());
				}
			}
		}
		return res;
	},

	_setTimeoutOnExecute_ : function(options){
		if(options.modal && options.name){
			this.dc._executedCommands_[options.name] = {
				time: this._getTime_(),
				id: this._getRecId_()
			}
		}
	},
	
	/**
	 * Command execution entry point.
	 */
	execute : function(options) {
		options = options || {};

		if (!this._timeoutExpired_(options)){
			this.dc.info(Main.msg.DC_ACTION_NOT_ALLOWED, "msg");
			this._fireEvent_("cmdExecutionFailed",{msg:Main.msg.DC_ACTION_NOT_ALLOWED})
			return false;
		}
		
		if (this.canExecute(options) === false) {
			this.dc.info(Main.msg.DC_ACTION_NOT_ALLOWED, "msg");
			this._fireEvent_("cmdExecutionFailed",{msg:Main.msg.DC_ACTION_NOT_ALLOWED})
			return;
		}

		if (this.isActionAllowed(options) === false) {
			this._fireEvent_("cmdExecutionFailed",{msg:Main.msg.DC_ACTION_NOT_ALLOWED+" Action not allowed by BL!"})
			return;
		}

		/*
		 * workaround to enable createInterceptor return false being handled
		 * correctly Seems that returnValue=false in
		 * Ext.Function.createInterceptor returns null not the specified
		 * false Used by AbstractDcvForm to inject its own form validation
		 * result.
		 */
		var x = this.beforeExecute(options);
		if (x === false || x === -1) {
			this._fireEvent_("cmdExecutionFailed",{msg:Main.msg.DC_ACTION_NOT_ALLOWED+" Action stopped from BL!"})
			return false;
		}

		if (this.needsConfirm(options) && options.confirmed !== true) {
			this._fireEvent_("cmdExecutionFailed",{needConfirm: true, msg:Main.msg.DC_ACTION_NOT_ALLOWED+" Action need to be confirmed by user!"})
			this.confirmExecute(null, options);
			return;
		}
		options.confirmed = false;
		options.showWorking = this.showWorking;
		
		this.onExecute(options);
		this._setTimeoutOnExecute_(options);

		this.afterExecute(options);
	},

	/**
	 * Empty template method meant to be overriden by the subclasses.
	 * 
	 */
	onExecute : function() { // parameter: options
		alert("Unimplemented onExecute!");
	}

});
