/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
/**
 * Base grid used for data-control list views.
 */
Ext.define("e4e.dc.view.AbstractDc_Grid", {
	extend : "Ext.grid.Panel",
	
	mixins : {
		elemBuilder : "e4e.base.Abstract_View",
		dcViewSupport : "e4e.dc.view.AbstractDc_View"
	},
	
	selType: 'checkboxmodel',

	// **************** Properties *****************

	/**
	 * Columns definition map
	 */
	_columns_ : null,

	/**
	 * Flag to switch on/off advanced sort on multiple columns.
	 */
	_noSort_ : false,

	/**
	 * Flag to switch on/off advanced filter.
	 */
	_noFilter_ : false,

	/**
	 * Flag to switch on/off data export.
	 */
	_noExport_ : false,

	/**
	 * Flag to switch on/off data import.
	 */
	_noImport_ : false,

	/**
	 * Flag to switch on/off data printing.
	 */
	_noPrint_ : false,

	/**
	 * Flag to switch on/off chart.
	 */
	_noChart_ : true,

	/**
	 * Flag to switch on/off custom layout management.
	 */
	_noLayoutCfg_ : false,

	/**
	 * Flag to switch on/off paging toolbar
	 * 
	 */
	_noPaginator_ : false,

	/**
	 * Data export window.
	 */
	_exportWindow_ : null,
	
	/**
	 * Report window.
	 */
	_reportWindow_ : null,

	/**
	 * Data print window.
	 */
	_printWindow_ : null,

	/**
	 * Data import window.
	 */
	_importWindow_ : null,

	/**
	 * Custom views management window.
	 */
	_layoutWindow_ : null,

	/**
	 * Title to be used in the dynamically generated reports.
	 */
	_printTitle_ : null,

	/**
	 * Totals(summary), unit, currency, view and filter related fields.
	 */
	_summaryDefined_ : null,
	_summaryVisible_ : null,
	
	_showViewFilterToolbar_: null,
	
	_unitParamName_ : null,
	_currencyParamName_ : null,
	_dateParamName_ : null,

	_filterComboId_ : null,
	_viewComboId_ : null,
	_totalsComboId_ : null,
	_unitComboId_ : null,
	_currencyComboId_ : null,
	_datePickerId_ : null,
	_defaultViewId_ : null,
	
	
	// **************** Public API *****************

	_defineColumns_ : function() {
	},

	_beforeDefineColumns_ : function() {
		return true;
	},

	_afterDefineColumns_ : function() {
	},

	_defineToolbarItems_ : function() {
	},

	_beforeDefineToolbarItems_ : function() {
		return true;
	},

	_afterDefineToolbarItems_ : function() {
	},
	
	_afterInitComponent_ : function(){
	},
	
	// Dan: build the child grid
	
	_buildChildGrid_ : function(cfg) {
		
		var childDc = cfg.childDc;
		var gridCls = cfg.gridCls;
		var filterField = cfg.filterField;
		var callBackParams = cfg.callbackParams;
		var applyFilter = true;
		var cmpChildGrid = {};
		
		if (!Ext.isEmpty(cfg.applyFilter) && cfg.applyFilter === false) {
			applyFilter = false;
		}

		this.view.on("expandbody", function( rowNode, record ) { // additional parameters: expandRow, eOpts

			if( !rowNode._fpGrid_ ){
				// obtain the id of div (must be the same with the algorithm from rowBodyTpl)
				var id = "rec-" + record.get(filterField);
				var v = this.view;
				// create the data-control for the secondary grid 

				// ##########################################################
				var frame = this._controller_.getFrame();
				var dc = frame._getDc_(childDc);
				if (applyFilter === true) {
					dc.doClearAllFilters();
					dc.setFilterValue(filterField,record.get(filterField));	
				}
				v.select(record); //SONE-2558: Accrued receivables: in Details screen, for a specific month, the system raises the "null" message
				// ##########################################################

				// create the secondary grid
				var grid2 = Ext.create(gridCls,{
					id: "grid-"+id,
					_controller_: dc,
					renderTo: id,
					scrollable: true,
					cls: "sone-child-grid",
					flex: 1,
					selType: "rowmodel",
					viewConfig: {
						deferEmptyText: false,
					    emptyText: 'No data'
					}
				});

				// add grid object to the rowNode, to can be accessed next time
				rowNode._fpGrid_ = grid2;
				cmpChildGrid = grid2;
				// set parameters and run the query
				dc.doQuery(callBackParams);
			}
		}, this);

		this.view.on("collapsebody", function( rowNode ) { // additional parameters:  record, expandRow, eOpts
			
			if (cmpChildGrid) {
				Ext.destroy(cmpChildGrid);
			}
			
			if( rowNode._fpGrid_ ){
				// destroy the grid to recreate at next expand (refresh the data and grid size)
				
				// ##########################################################
				var frame = this._controller_.getFrame();
				var dc = frame._getDc_(childDc);
				dc.store.clearFilter();
				dc.fireEvent("rowcollpase");
				// ##########################################################

				Ext.destroy(rowNode._fpGrid_);
				rowNode._fpGrid_ = null;
			}
		}, this);
		
		var f = function(){ // parameter: store 
			var p = this.getPlugin("rowexpanderplus");
			var i = p.lastExpandedRowIdx;
			if(Ext.isNumber(i)){
				var rowNode = this.view.getNode(i);
				if( rowNode && rowNode._fpGrid_ ){
					Ext.destroy(rowNode._fpGrid_);
					rowNode._fpGrid_ = null;
				}
			}
		};
		this._controller_.store.on("beforeloaddata", f, this);
		this._controller_.store.on("beforeload", f, this);
		this._controller_.store.on("clear", f, this);
	},

	/**
	 * Open the data-import window
	 */
	_doImport_ : function() {
		if (this._importWindow_ == null) {
			this._importWindow_ = new e4e.base.FileUploadWindow(
					{
						_handler_ : "dsCsvImport",
						_fields_ : {
							separator : {
								xtype : "combo",
								store : [ ";", "," ],
								value : ",",
								fieldLabel : Main.translate("cmp", "csv_cfg_separator"),
								allowBlank : false,
								labelSeparator : Main.viewConfig.FORM_LABEL_SEPARATOR_FOR_MANDATORY
							},
							quoteChar : {
								xtype : "combo",
								store : [ '"' ],
								value : '"',
								fieldLabel : Main.translate("cmp", "csv_cfg_quote"),
								allowBlank : false,
								labelSeparator : Main.viewConfig.FORM_LABEL_SEPARATOR_FOR_MANDATORY
							},
							dsName : {
								xtype : "hidden",
								value : this._controller_.dsName
							}
						},
						_succesCallbackScope_ : this,
						_succesCallbackFn_ : function() {
							this._controller_.doQuery();
						}
					});
		}
		this._importWindow_.show();
	},
	
	/**
	 * Dan: Custom export function for "Sone" application
	 */
	_doReport_ : function() {
		if (this._reportWindow_ == null) {
			this._reportWindow_ = new e4e.dc.tools.DcCustomExportWindow({
				_grid_ : this,
				closeAction : "hide"
			});
		}
		this._reportWindow_.show();
	},

	/**
	 * Open the data-export window
	 */
	_doExport_ : function() {
		if (this._exportWindow_ == null) {
			this._exportWindow_ = new e4e.dc.tools.DcExportWindow({
				_grid_ : this,
				closeAction : "hide"
			});
		}
		this._exportWindow_.show();
	},

	/**
	 * Open the data-print window
	 */
	_doPrint_ : function() {
		if (this._printWindow_ == null) {
			this._printWindow_ = new e4e.dc.tools.DcPrintWindow({
				_grid_ : this,
				closeAction : "hide"
			});
		}
		this._printWindow_.show();
	},

	/**
	 * Open the chart window
	 */
	_doChart_ : function() {
		if (this._chartWindow_ == null) {
			this._chartWindow_ = new e4e.dc.tools.DcChartWindow({
				_grid_ : this,
				closeAction : "hide"
			});
		}
		this._chartWindow_.show();
	},

	/**
	 * Show the advanced sort window
	 */
	_doSort_ : function() {
		new e4e.dc.tools.DcSortWindow({
			_grid_ : this
		}).show();
	},

	/**
	 * Show the advanced filter window
	 */
	_doFilter_ : function() {
		new e4e.dc.tools.DcFilterWindow({
			_grid_ : this
		}).show();
	},

	/**
	 * Show the custom views management window
	 */
	_doLayoutManager_ : function() {
		if (this._layoutWindow_ == null) {
			this._layoutWindow_ = new e4e.dc.tools.DcGridLayoutWindow({
				_grid_ : this
			});
		}
		this._layoutWindow_.show();
	},

	// **************** Defaults and overrides *****************

	buttonAlign : "left",
	forceFit : false,
	scrollable : true,
	border : true,
	frame : true,
	deferRowRender : true,
	// enableLocking : true,
	loadMask : {
		msg : Main.translate("msg", "loading") + "..."
	},
	viewConfig : {
		loadMask : {
			msg : Main.translate("msg", "loading") + "..."
		},
		enableTextSelection : true,
		stripeRows : true,
		emptyText : Main.translate("msg", "grid_emptytext")
	},

	/**
	 * Redirect the default state management to our implementation.
	 */
	getState : function() {
		return this._getViewState_(); 
	},

	/**
	 * Redirect the default state management to our implementation.
	 */
	applyState : function(state) {
		return this._applyViewState_(state);
	},

	beforeDestroy : function() {
		// call the contributed helpers from mixins
		this._beforeDestroyDNetDcView_();
		this._beforeDestroyDNetView_();
		this.callParent(arguments);
	},
	
	// Dan: config the reset button
	
	_configResetButton_ : function() {
		var resetButton = Ext.getCmp(this._resetButtonId_);
		if (resetButton) {			
			resetButton.doSetup();
		}
	},

	// **************** Private methods *****************

	_initDcGrid_ : function() {
		this._dcViewInitEventHandlers_();
		this._initTranslation_();
		this._elems_ = new Ext.util.MixedCollection();
		this._columns_ = new Ext.util.MixedCollection();

		this._defineDefaultElements_();

		this._startDefine_();

		if (this._beforeDefineColumns_() !== false) {
			this._defineColumns_();
			this._afterDefineColumns_();
		}
		
		if (this._beforeDefineToolbarItems_() !== false) {
			this._defineToolbarItems_();
			this._afterDefineToolbarItems_();
		}

		if (this._beforeDefineElements_() !== false) {
			this._defineElements_();
			this._afterDefineElements_();
			
		}

		this._columns_.each(this._postProcessColumn_, this);
		this._elems_.each(this._postProcessElem_, this);
		this.mon(this, "boxready", function(){
			this._setViewAndFilters_();
			this.headerCt.el.on('scroll', function(){ 
				var dh = this.headerCt.el.dom;
				var dv = this.getView().el.dom;
				if( dh.scrollLeft !== dv.scrollLeft ){
					dh.scrollLeft = dv.scrollLeft;
				}
			},this);
		}, this);

		/*
		//this.mon(this, "columnhide", this._collapseAllIfRowExpander_, this);
		//this.mon(this, "activate", this._loadOptimized_, this);
		*/
		this._endDefine_();
	},
	
	// Dan: SONE-3376 Expend/Collapse button is not working in lists if a column is removed from list
	
	_collapseAllRows_ : function() {
        var store = this.getStore();
        var expander = this.getPlugin("rowexpanderplus");
        if (!Ext.isEmpty(expander)) {
        	for(var i = 0; i < store.getCount(); i++) {
                var record = store.getAt(i);
                if(expander.recordsExpanded[record.internalId]){
                    expander.toggleRow(i);
                }   
            }
        }
    },
	
	_collapseAllIfRowExpander_ : function() {
		var ctx = this;
		var f = function() {
			var plugins = ctx.plugins;
			Ext.each(plugins, function(plugin) {
				if (plugin.ptype === "rowexpanderplus") {
					ctx._collapseAllRows_();
				}
			}, ctx);
		}
		this.on("columnhide", function() {
			f();
		}, this);
		this.on("columnshow", function() {
			f();
		}, this);
	},
	
	_loadOptimized_ : function() {
		var ctrl = this._controller_.dcContext;
		if (ctrl && ctrl.parentDc.trackEditMode && ctrl.relation.fetchMode === "auto") {
			ctrl.doQueryTask.delay(ctrl.autoFetchDelay);
		}
	},
	
	_setViewAndFilters_ : function() {

		if ("" + Main.viewConfig.SHOW_VIEW_AND_FILTER_TOOLBAR !== "false") {

			var frame = this._controller_.getFrame();
			frame._gridsWithFilter_.removeAll();
			frame._matchGrids_(this);
			frame._configureViewsAndFilters_();	

			var dcContext = this._controller_.dcContext;
			if (dcContext) {
				var parentDc = dcContext.parentDc;
				if (parentDc._editTriggered_ === true) {
					parentDc.fireEvent("onAfterEditIn", parentDc);
					parentDc._editTriggered_ = null;
				}
			}
			
			this._loadOptimized_();
		}
		this._collapseAllIfRowExpander_();
	},

	/**
	 * Create the grid configuration object with the usual properties which are
	 * likely to be required by any subclass
	 */
	_createDefaultGridConfig_ : function() {
		var cfg = {
			store : this._controller_.store,
			columns : this._columns_.getRange()
/*			
//			,plugins: [].concat(this.plugins?this.plugins:[],[
//    			{
//    				ptype: "clipboard"
//    			}
//    		])
*/
		};

		if (this._noPaginator_ || this._controller_._infiniteScroll_) {
			this._noExport_ = true;
			this._noPrint_ = true;
			this._noImport_ = true;
			this._noSort_ = true;
			this._noLayoutCfg_ = true;
			if( !this._controller_._infiniteScroll_ ){
				this._controller_.store.pageSize = Main.viewConfig.FETCH_SIZE_UNLIMITED;
			}
		} else {
			cfg.bbar = {
				xtype : "pagingtoolbar",
				store : this._controller_.store,
				displayInfo : true,
				_controller_ : this._controller_,
				listeners : {
					"beforechange" : {
						scope : this,
						fn : function(tlb){ // additional parameters: page, eOpts
							if (e4e.dc.DcActionsStateManager.isQueryDisabled(tlb._controller_)) {
								Ext.Msg.show({
										title : Main.translate("msg", "save_or_cancel_before_query_title"),
										msg : Main.translate("msg", "save_or_cancel_before_query"),
										icon : Ext.Msg.WARNING,
										buttons : Ext.Msg.OK
								});
								return false; 
							}
						}
					}
				}
			}
			var bbitems = [];
			this._buildToolbox_(bbitems);

			if (bbitems.length > 0) {
				cfg["bbar"]["items"] = {
					text : "Tools",
					menu : bbitems
				};
			}
		}
		return cfg;
	},

	_gotoFirstNavigationItem_ : function() {
		this._configResetButton_();
/*		
//		var v = this.getView();
//		if (this._controller_.record != null) {
//			v.focusRow(this._controller_.record);
//		} else {
//			this._controller_.restoreSelection();
//			if (this._controller_.record != null) {
//				v.focusRow(this._controller_.record);
//			} else {
//				v.focus();
//			}
//		}
*/		
	},

	/**
	 * Handler for the data-control selectionChange event.
	 */
	_onController_selectionChange : function(evnt) {
		if (evnt.eOpts && evnt.eOpts.fromGrid === true && evnt.eOpts.grid === this) {
			return;
		}
		var s = evnt.dc.getSelectedRecords();
		if (s !== this.getSelectionModel().getSelection()) {
			this.getSelectionModel().select(s, false, true);
		}
	},
	
	_beforeStoreLoad_ :  function() {
		
	},

	/**
	 * Handler for the data-control's store load event.
	 */
	_onStore_load_ : function(store) { // additional parameters: records, successful, eOpts
		this._beforeStoreLoad_();
		if (!this._noExport_) {
			if (store.getCount() > 0) {
				this._get_("_btnExport_").enable();
			} else {
				this._get_("_btnExport_").disable();
			}
		}
		if (!this._noPrint_) {
			if (store.getCount() > 0) {
				this._get_("_btnPrint_").enable();
			} else {
				this._get_("_btnPrint_").disable();
			}
		}
		if (!this._noChart_) {
			if (store.getCount() > 0) {
				this._get_("_btnChart_").enable();
			} else {
				this._get_("_btnChart_").disable();
			}
		}
		return;
	},
	
	// Dan: SONE-2098: Export feature from Tools is not working when the 1st record is added into a list
	
	_onStore_write_ : function(store) {
		var exportBtn = this._get_("_btnExport_");
		if (exportBtn) {
			if (store.getCount() > 0) {
				this._get_("_btnExport_").enable();
			} else {
				this._get_("_btnExport_").disable();
			}
		}
	},

	/**
	 * Build default tools
	 */
	_buildToolbox_ : function(bbitems) {
		if( ""+Main.viewConfig.SHOW_VIEW_AND_FILTER_TOOLBAR === "false" ){
			// Andras & friends:
			// If the view and filters toolbar is active hide the following items in the tools menu

			if (!this._noLayoutCfg_) {
				bbitems.push(this._elems_.get("_btnLayout_"));
			}

			if (!this._noSort_) {
				bbitems.push("-");
				bbitems.push(this._elems_.get("_btnSort_"));
			}
			if (!this._noFilter_) {
				bbitems.push("-");
				bbitems.push(this._elems_.get("_btnFilter_"));
			}
			if (!this._noImport_) {
				bbitems.push("-");
				bbitems.push(this._elems_.get("_btnImport_"));
			}

			if (!this._noExport_) {
				bbitems.push("-");
				bbitems.push(this._elems_.get("_btnExport_"));
			}

			if (!this._noPrint_) {
				bbitems.push("-");
				bbitems.push(this._elems_.get("_btnPrint_"));
			}

			if (!this._noChart_) {
				bbitems.push("-");
				bbitems.push(this._elems_.get("_btnChart_"));
			}
		} else {
				if (!this._noLayoutCfg_) {
					bbitems.push(this._elems_.get("_btnLayout_"));
				}
		
				if (!this._noSort_) {
					bbitems.push(this._elems_.get("_btnSort_"));
				}
				if (!this._noFilter_) {
					bbitems.push("-");
					bbitems.push(this._elems_.get("_btnFilter_"));
				}
				if (!this._noImport_) {
					bbitems.push(this._elems_.get("_btnImport_"));
				}
		
				if (!this._noExport_) {
					bbitems.push(this._elems_.get("_btnExport_"));
				}
		
				if (!this._noPrint_) {
					bbitems.push("-");
					bbitems.push(this._elems_.get("_btnPrint_"));
				}
		
				if (!this._noChart_) {
					bbitems.push("-");
					bbitems.push(this._elems_.get("_btnChart_"));
				}
		}
	},

	_getBtnImportCfg_ : function() {
		return {
			id : Ext.id(),
			text : Main.translate("dcvgrid", "imp__tlp"),
			handler : this._doImport_,
			scope : this
		};
	},

	_getBtnExportCfg_ : function() {
		return {
			id : Ext.id(),
			disabled : true,
			text : Main.translate("dcvgrid", "exp__tlp"),
			handler : this._doExport_,
			scope : this
		};
	},

	_getBtnFilterCfg_ : function() {
		return {
			id : Ext.id(),
			text : Main.translate("dcvgrid", "filter__tlp"),
			handler : this._doFilter_,
			scope : this
		};
	},

	_getBtnSortCfg_ : function() {
		return {
			id : Ext.id(),
			text : Main.translate("dcvgrid", "sort__tlp"),
			handler : this._doSort_,
			scope : this
		};
	},

	_getBtnPrintCfg_ : function() {
		return {
			id : Ext.id(),
			disabled : true,
			text : Main.translate("dcvgrid", "print__tlp"),
			handler : this._doPrint_,
			scope : this
		};
	},

	_getBtnChartCfg_ : function() {
		return {
			id : Ext.id(),
			disabled : true,
			text : Main.translate("dcvgrid", "chart__tlp"),
			handler : this._doChart_,
			scope : this
		};
	},

	_getBtnLayoutCfg_ : function() {
		return {
			id : Ext.id(),
			text : Main.translate("dcvgrid", "layout__tlp"),
			handler : this._doLayoutManager_,
			scope : this
		};
	},

	/**
	 * Define defaults elements
	 */
	_defineDefaultElements_ : function() {
		this._elems_.add("_btnImport_", this._getBtnImportCfg_());
		this._elems_.add("_btnExport_", this._getBtnExportCfg_());
		this._elems_.add("_btnPrint_", this._getBtnPrintCfg_());
		this._elems_.add("_btnSort_", this._getBtnSortCfg_());
		
		if( ""+Main.viewConfig.SHOW_VIEW_AND_FILTER_TOOLBAR === "false" ){
			// Andras & friends:
			// If the view and filters toolbar is active hide the following items in the tools menu
			this._elems_.add("_btnFilter_", this._getBtnFilterCfg_());
			this._elems_.add("_btnLayout_", this._getBtnLayoutCfg_());
			

		}
		
		this._elems_.add("_btnChart_", this._getBtnChartCfg_());
	},

	/**
	 * Specific implementation to read the grid columns view-state to be stored
	 * as a custom view.
	 */
	_getViewState_ : function() {
		var me = this;
		var state = null;
		var colStates = [];
		var cm = this.headerCt;
		var cols = cm.items.items;

		for (var i = 0, len = cols.length; i < len; i++) {
			var c = cols[i];
			if( c.name ){ 
				// plugins (like row-expander or check-box selection type) are present in the cols list, but they have no col names 
				// and positions must not be changed
				colStates.push({
					n : c.name,
					h : c.hidden,
					w : c.width
				});
			}
		}
		state = me.addPropertyToState(state, 'columns', colStates);
		return state;
	},

	/**
	 * Apply a view-state read by _getViewState_
	 */
	_applyViewState_ : function(state) {
		
		if (!this.rendered) {
			this.on("afterrender", this._applyViewStateAfterRender_, this, {
				single : true,
				state : state
			});
			return;
		}

		var sCols = state.columns;
		var cm = this.headerCt;
		var cols = cm.items.items;
		var col = null;
		var checkCol = null;
		var expanderCol = null;
		var selModel = this.getSelectionModel();
		var selType = selModel.type;
		var z;
		var l = cols.length;
		
		if (selType === "checkboxmodel") {
			for (z = 0; z < l; z++) {
				if ((cols[z].cls+"").indexOf("x-selmodel-column") > -1) {
					checkCol = cols[z];
					break;
				}
			}
		}
		
		for (z = 0; z < l; z++) {
			if (!Ext.isEmpty(this.getPlugin("rowexpanderplus")) && cols[z].innerCls === "x-grid-cell-inner-row-expander") {
				expanderCol = cols[z];
				break;
			}
		}

		for (var i = 0, slen = sCols.length; i < slen; i++) {
			var sCol = sCols[i];
			var colIndex = -1;

			if( sCol.n ){	
				
				for (var j = 0, len = cols.length; j < len; j++) {
					if (cols[j].name === sCol.n) {
						colIndex = j;
						col = cols[j];	
						break;
					}
				}

				if (colIndex >= 0) {
					
					if (sCol.h) {
						col.hide();
					} else {
						col.show();
					}
					col.setWidth(sCol.w);
					if (colIndex !== i && !col.hidden) { // Dan: hidden columns twrow errors
						cm.move(colIndex, i);
					}
				}
			}
		}

		if (selType === "checkboxmodel") {
			if( checkCol ){
				cm.moveBefore(checkCol, cm.items.items[0]);
			}
			if (expanderCol) {
				cm.moveBefore(expanderCol, checkCol);
			}
		}	
		if (selType === "rowmodel" && expanderCol) {
			cm.moveBefore(expanderCol, cm.items.items[0]);
		}
		
		this.getView().refresh();
	},

	_applyViewStateAfterRender_ : function(cmp, eOpts) {
		this._applyViewState_(eOpts.state);
	},

	_selectionHandler_ : function() { // parameters: sm, selected, options
		var gridSel = this.getSelectionModel().getSelection();
		var ctrl = this._controller_;
		ctrl.setSelectedRecords(gridSel, {
			fromGrid : true,
			grid : this
		});
	},

	/**
	 * Postprocessor run to inject framework specific settings into the columns.
	 */
	_postProcessColumn_ : function(column) {
		if (column.header === undefined || column.header === null) {
			Main.translateColumn(this._trl_, this._controller_._trl_, column);
		}
	},
	
	/**
	 * Post-processor run to inject framework specific settings into the
	 * elements.
	 * 
	 */
	_postProcessElem_ : function(item) {
		item["_dcView_"] = this;
		if ((item.fieldLabel === undefined || item.fieldLabel === null) && item.noLabel !== true) {
			Main.translateField(this._trl_, this._controller_._trl_, item);
		}
		return true;
	},
	
	/**
	 * Initialize translation class.
	 */
	_initTranslation_ : function() {
		var _trlFqn = this.$className.replace(".ui.extjs.", ".i18n.");
		try {
			if (_trlFqn !== this.$className && Ext.ClassManager.get(_trlFqn) ){
				this._trl_ = Ext.create(_trlFqn);
			}
		} catch (e) {
			// no translation file, ignore
		}
	},
	
	/**
	 * Show or hide summary bar. First check for docked summary and do the operation on these. 
	 * If no docked summary is found, enable/disable the feature mainSummaryId - if exists.
	 */
	_showSummary_ : function(visible){
		// search for docked summary
		this._summaryVisible_ = visible;
		var dockedSum = null;
		var totalPanel = null;
		
		for(var i=0;i<this.dockedItems.length; i++){
			if( this.dockedItems.items[i].itemId === "summaryBar" ){
				dockedSum = this.dockedItems.items[i];
				break;
			}
		}
		if( dockedSum ){
			if(visible){
				dockedSum.show();
				if (!Ext.isEmpty(this._totalPanelId_)) {
					totalPanel = Ext.getCmp(this._totalPanelId_);
					totalPanel.show();
				}
			} else {
				dockedSum.hide();
				if (!Ext.isEmpty(this._totalPanelId_)) {
					totalPanel = Ext.getCmp(this._totalPanelId_);
					totalPanel.hide();
				}
			}
		} else {
			// visibility for non docked summaries can be changed easier
			var view = this.getView();
			var f = view.getFeature('mainSummaryId');
			if (!Ext.isEmpty(this._totalPanelId_)) {
				totalPanel = Ext.getCmp(this._totalPanelId_);
			}
			
			if(f){
				f.toggleSummaryRow(visible);
				view.refresh();
			} else {
				this._summaryVisible_ = false;
			}
			if (!Ext.isEmpty(totalPanel)) {
				totalPanel.setVisible(visible);
			}
		}
	},
	
	_getSummary_ : function() {
		var dockedSum = null;
		for(var i=0;i<this.dockedItems.length; i++){
			if( this.dockedItems.items[i].itemId === "summaryBar" ){
				dockedSum = this.dockedItems.items[i];
				break;
			}
		}
		return dockedSum;
	},
	
	_isSummaryVisible_ : function() {
		var visible = false;
		var summary = this._getSummary_();
		if (summary.hidden === false) {
			visible = true;
		}
		return visible;
	},

    setSummaryVal : function(name,val,silent){
    	if(this.__summaryFeature__){
    		this.__summaryFeature__.setSummaryVal(name,val,silent);
    	} else {
    		var c = this._controller_;
    		if( c.getParams().data.hasOwnProperty(name) ){
    			c.setParamValue(name,val,silent);
    		}
    	}
    },
    getSummaryVal : function(name){
    	var res = null;
    	if(this.__summaryFeature__){
    		res = this.__summaryFeature__.summaryRecord.get(name);
    	} else {
    		var c = this._controller_;
    		if( c.getParams().data.hasOwnProperty(name) ){
    			res = c.getParamValue(name);
    		}
    	}
    	return res;
    },
	
    
	// Dan: Add the export button to all the grids
    _getToolbar_ : function(){
		var toolbar = "";
		// If the docked item is docked on top and is a toolbar
		if (this.__dcViewType__ === "grid" || this._dcViewType_ === "edit-grid") {
			var items = this.dockedItems.items;
			
			for (var i = 0; i < items.length; i++) {
				var it = items[i];
				if (it.dock === "top" && it.$className === "Ext.toolbar.Toolbar") {
					toolbar = it;
					break;
				}
			}
		}
		return toolbar;
    },

    _findStdExpBtn_ : function(a){
    	var l = a.length - 1;
    	while( l>=0 ){
    		if( a[l]._stdExpBtn_ ){
    			return l;
    		}
    	}
    	return -1;
    },
    
    _addButtonToToolbar_ : function(btn, toolbar, beforeStdExpBtn){
		var toolbarItems = toolbar.items.items;
		var l = toolbarItems.length;
		var last = toolbarItems[l-1];
		// check if the last item is a ollector item for the buttons and add to that item in this case
		if(last && last.name==="btnMenu" && last.menu){
			delete btn.xtype;
			if( beforeStdExpBtn ){
				var i = this._findStdExpBtn_(last.menu.items.items);
				if( i>=0 ){
					last.menu.insert(i, btn);
				} else {
					last.menu.add(btn);
				}
			} else {
				last.menu.add(btn);
			}
		} else {
			if( beforeStdExpBtn ){
				var ii = this._findStdExpBtn_(toolbarItems);
				if( ii>=0 ){
					toolbar.insert(ii, btn);
				} else {
					toolbar.add(btn);
				}
			} else {
				toolbar.add(btn);
			}
		}
    },
	
	_addExportButton_ : function() {
		var toolbar = this._getToolbar_();
		if (!Ext.isEmpty(toolbar)) {
			
			var exportBtn = {
				xtype: "button",	
				glyph:"xf019@FontAwesome",
				text: Main.translate("inApplication", "exportButton__lbl"),
				hidden: this._noExport_ === true ? true :  false,
				scope: this,
				handler: this._doReport_,
				_stdExpBtn_: true
			};
			this._addButtonToToolbar_(exportBtn, toolbar);
		}
	},
	
	_onExternalReportExistsSuccess_ : function(response){
		var resp = Ext.getResponseDataInText(response);
		if( (""+resp).trim() === "true" ){
			// create the run report button with enable rule management and add to related toolbar
			var toolbar = this._getToolbar_();
			if (!Ext.isEmpty(toolbar)) {
				var frame = this._controller_.getFrame();
				var sm = {name: "selected_one", dc: this._controller_._instanceKey_};
				var id = Ext.id();
				var exportBtn = {
					name: "btnRunReport_"+id,
					id: id,
					xtype: "button",
					glyph:"xf019@FontAwesome",
					text: Main.translate("inApplication", "extReportButton__lbl"),
					scope: this,
					_ctrl_: this._controller_,
					_wdw_: null,
					stateManager: [sm],
					handler: function(btn){
						if( !btn._wdw_ ){
							this._createExternalReportWindow_(btn);
						}
						if( btn._wdw_ ){
							btn._wdw_.show();
						}
					}
				};
				this._addButtonToToolbar_(exportBtn, toolbar, true);
				frame._elems_.add(exportBtn.name, exportBtn);
				e4e.ui.FrameButtonStateManager.register(exportBtn.name, sm.name, sm.dc, frame, {});
			}
		}
	},
	
	_createExternalReportWindow_ : function(btn){
		var ExportFormatType = { _PDF_ : "PDF", _CSV_ : "CSV", _XLSX_ : "XLSX",	_DOC_ : "DOC" };
		var idFormat = Ext.id();
		var idReport = Ext.id();
		var idOkBtn = Ext.id();
		var dsName = this._controller_.dsName;
		
		var _enableButton_ = function(){
			var btn = Ext.getCmp(idOkBtn);
			if( !Ext.isEmpty(btn) ){
				var lovFormat = Ext.getCmp(idFormat);
				var lovReport = Ext.getCmp(idReport);
				var cnt = 0;
				if( !Ext.isEmpty(lovFormat) && !Ext.isEmpty(lovFormat.getValue()) ){
					cnt++;
				}
				if( !Ext.isEmpty(lovReport) && !Ext.isEmpty(lovReport.getValue()) ){
					cnt++;
				}
				btn.setDisabled(cnt<2);
			}
		};
		
		btn._wdw_ = new Ext.window.Window({
			_btn_ : btn,
			_idReportCmp_ : idReport,
			_idFormatCmp_ : idFormat,
			_controller_ : this._controller_,
			title: Main.translate("inApplication", "extReportWndTitle__lbl"),
			closeAction: "hide",
			resizable: true, 
			layout: "fit",
			modal: true,
			width: 350,
			buttonAlign: "left",
			items: [{
				xtype: "form",
				fieldDefaults: {
					labelWidth: 100,
					labelAlign: "right"
				},
				layout: {
					type: "vbox",
					align: "stretch"
				},
				frame : true,
				bodyPadding : 10,
				border: false,
				items: [{
					fieldLabel: Main.translate("inApplication", "extReportWndReport__lbl"),
					name: "reportName",
					id: idReport,
					_dcView_: this,
					paramIndex:	"reportName",
					xtype:	"ad_ExternalReportFWLov_Lov",
					allowBlank: false,
					forceSelection: true,
					filterFieldMapping: [{lovField:"dsName", value: dsName}],
					listeners:{	change:{scope:this, fn:_enableButton_} }		
				}, {
					fieldLabel: Main.translate("inApplication", "extReportWndFormat__lbl"),
					name: "format",
					id: idFormat,
					xtype: "combo",
					mode: "local",
					allowBlank: false,
					pageSize: 0,
					forceSelection: true,
					store:[ ExportFormatType._PDF_, ExportFormatType._CSV_, ExportFormatType._XLSX_, ExportFormatType._DOC_ ],
					listeners:{	change:{scope:this, fn:_enableButton_} }
				}]
			}],
		    buttons: [{
				_btn_: btn,
				id: idOkBtn,
				glyph: fp_asc.send_glyph.glyph,
				iconCls: fp_asc.send_glyph.css,
				disabled: true,
				text: Main.translate("inApplication", "extReportWndRunBtn__lbl"),
				handler: function(cmp){
					var w = cmp._btn_._wdw_;
					var lovFormat = Ext.getCmp(w._idFormatCmp_);
					var lovReport = Ext.getCmp(w._idReportCmp_);
					if( !Ext.isEmpty(lovFormat) && !Ext.isEmpty(lovReport) ){
						w._controller_.doExternalReport(lovReport.getSelection().id, lovFormat.getValue());
					}
					this._btn_._wdw_.hide();
				} 
			},{
				_btn_: btn,
				glyph: fp_asc.cancel_glyph.glyph,
				iconCls: fp_asc.cancel_glyph.css,
				text: Main.translate("inApplication", "extReportWndCancelBtn__lbl"),
				handler: function(){
					this._btn_._wdw_.hide();
				} 
			}]
		});
	},
	
	_addExternalReportsButton_ : function(){
		this._controller_.isExternalReportExists(this._onExternalReportExistsSuccess_, this);
	},

	_setPageSize_ : function(ps){
		if( !this._controller_._infiniteScroll_ ){
			this.getStore().pageSize = ps;
			return true;
		}
		return false;
	}
    
});
