/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.dc.command.DcReloadPageCommand", {
	extend : "e4e.dc.command.AbstractDcAsyncCommand",

	dcApiMethod : e4e.dc.DcActionsFactory.RELOAD_PAGE,

	onExecute : function(options) {
		var store = this.dc.store;
		store.on("load",function(st){ // additional parameters: records, successful, eOpts
			if(st.getCount()<1 && st.lastOptions && st.lastOptions.page>1){
				// requery data for first page if current page has no records (and is not the first page)
				this.dc.doQuery();
			}
		},this,{single:true});
		store.reload({
			callback : function(records, operation, success) {
				this.onAjaxResult({
					records : records,
					response : operation._response,
					operation : operation,
					options : options,
					success : success,
				});
			},
			scope : this
		});
	},
	
	isActionAllowed : function() {
		if (e4e.dc.DcActionsStateManager.isQueryDisabled(this.dc)) {
			this.dc.warning(Main.msg.DC_QUERY_NOT_ALLOWED, "msg");
			return false;
		}
		return true;
	}
});
