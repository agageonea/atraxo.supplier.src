/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.dc.command.DcEditInCommand", {
	extend : "e4e.dc.command.AbstractDcSyncCommand",

	dcApiMethod : e4e.dc.DcActionsFactory.EDIT_IN,

	onExecute : function(options) {
		var dc = this.dc;
		if( dc.getRecord() ) {
			if (dc.trackEditMode) {
				dc.isEditMode = true;
			}
			dc._editTriggered_ = true;
			dc.fireEvent("onEditIn", dc, options);
		}
	}
});
