/**
 * All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("e4e.dc.command.DcCancelSelectedCommand", {
	extend : "e4e.dc.command.AbstractDcSyncCommand",

	dcApiMethod : e4e.dc.DcActionsFactory.CANCELSELECTED,
	
	onExecute : function() { // parameters: options
		var dc = this.dc;
		if (dc.store.getCount() === 0) {
			this.discardChanges();
		} else {
			this.discardChildrenChanges();
			this.discardChanges();
		}
		if(dc.record == null){
			Ext.Function.defer(dc.doDefaultSelection,100,dc);
		}
	},

	discardChanges : function() {
		var dc = this.dc;
		var s = dc.store;

		var a = dc.getSelectedRecords();
		for(var i=a.length-1; i>=0; i--){
			if( a[i].phantom ){
				s.remove(a[i]);
			} else {
				a[i].reject();
			}
		}
	},

	discardChildrenChanges : function() {
		var dc = this.dc;
		var l = dc.children.length;
		for ( var i = 0; i < l; i++) {
			if (dc.children[i].isDirty()) {
				dc.children[i].doCancel();
			}
		}
	}

});
