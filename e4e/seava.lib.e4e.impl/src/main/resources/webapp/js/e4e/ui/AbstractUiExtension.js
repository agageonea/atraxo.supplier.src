/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.override(e4e.ui.AbstractUi, {

	// build the panels and do the configurations to display the user defined fields
	
	_configureUserFields_ : function(){
		if (typeof __DS_FOR_USER_DEFINED_FIELDS__ === 'undefined' || !Ext.isArray(__DS_FOR_USER_DEFINED_FIELDS__)){
			// no data received from server, nothing to do
			return;
		}
		var i = 0;
		this._userFieldsCfg_.each(function(item){
			
			var dcItem = this._getDc_(item.dcName);
			if( dcItem ){
				var j;
				for(j=0; j<__DS_FOR_USER_DEFINED_FIELDS__.length; j++){
					if( __DS_FOR_USER_DEFINED_FIELDS__[j].dsName === dcItem.dsName ){
						i++;
						item.id = "_" + i + "_" + Ext.id();;
						this._configurePanelForUserFields_(item);
						break;
					}
				}
			}
		},this);
	},

	_getAliasFromDc_ : function(dcName){
		var res = "";
		var dc = this._getDc_(dcName);
		if( dc ){
			var s = dc.dsName;
			res = s.substring(s.indexOf("_")+1, s.lastIndexOf("_"));
			if( Ext.isEmpty(res) ){
				res = dc.dsName;
			}
		}
		return res;
	},
	
	_configurePanelForUserFields_ : function(item){
		var bld = this._getBuilder_();
		// dc
		bld.addDc("userFieldValues"+item.id, Ext.create(atraxo.ad.ui.extjs.dc.UserFieldValues_Dc, {multiEdit: true}))
		// instead of childField:"targetAlias", parentField:"entityAlias" we need to compose alias from dsName
		// we can have more entities mapped to the same db table
		.linkDc("userFieldValues"+item.id, item.dcName, {fetchMode:"auto",fields:[
			{childField:"targetAlias", value:this._getAliasFromDc_(item.dcName)}, {childField:"targetRefid", parentField:"id"}]});
		// grid
		bld.addDcEditGridView("userFieldValues"+item.id, {
			name:"userFieldsList"+item.id, _hasTitle_:false, 
			title: Main.translate("appmenuitem", "user_defined_fields", null, "User fields"),
			xtype:"ad_UserFieldValues_Dc$DialogList", frame:true,  
			listeners:{ beforeedit: { scope: this, fn: function(editor, ctx) {
				this._setUserFieldsEditor_(editor, ctx, this._get_('userFieldsList'+item.id));
			}}}
		});
		// toolbar
		bld.beginToolbar("tlbUserFieldsList"+item.id, {dc: "userFieldValues"+item.id})
		.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"})
		.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph})
		.addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
		.addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
		.addReports()
		.end();
		bld.addToolbarTo("userFieldsList"+item.id, "tlbUserFieldsList"+item.id);
		// add list grid to tab-pannel
		bld.addChildrenTo2(item.tabPanelName, ["userFieldsList"+item.id]);
		// populate dc
		var dc = this._getDc_("userFieldValues"+item.id);
		dc.doQuery();
	},
	
	_setUserFieldsEditor_: function(editor,ctx,view) {
		var fieldIndex = ctx.field;
		var col = ctx.column;
		var cfg;
		var textEditor = {xtype:"textfield", maxLength:32};	
		var numberEditor = {xtype:"numberfield", maxLength:32};	
		var dateEditor = {
			xtype:"datefield",
			_initValue_ : this.value,
			format: Main.DATETIME_FORMAT,
			altFormats: Main.ALT_FORMATS,
			_mask_ : Main.DATETIME_FORMAT,
			listeners: {						
				boxready: {
					scope: this,
					fn: function(el) {
						if (fieldIndex === "value") {
							var dc = view._controller_;
							var r = dc.getRecord();
							if (r) {
								var value = r.get("value");										
								if (!Ext.isEmpty(value)) {
									el.setValue(new Date(value));
								}											
							}
						}								
					}
				}
			}					
		};	

		if (fieldIndex === "value") {
			var r = ctx.record;
			if (r) {
				var type = r.get("userFieldType");
				if (!Ext.isEmpty(type)) {
					if (type === __AD__.UserFieldType._DATETIME_) {
						cfg = dateEditor;						
					}
					else if (type === __AD__.UserFieldType._NUMERIC_ ) {
						cfg = numberEditor;
					}
					else {
						cfg = textEditor;
					}
					col.setEditor(cfg);			
				}
			}
		} else {
	
			// Clear the value on restriction type change
			if (fieldIndex === "userFieldType") {
				var ed = col.getEditor();
				ed.on("change", function() {
					var record = view._controller_.getRecord();
					record.set("value", null);
				}, this);
			}
		}
	},
	
	/**
	 * Dan: Charts
	 */
	buildColumnChart : function(store, cfg) {
		var xFields = cfg["series"].xFields;
		var yFields = cfg["series"].yFields;
		var yTitles = cfg["series"].yTitles;
		var barColors = cfg["series"].barColors;
		var strokeColors = cfg["series"].strokeColors;
		var labelsRenderer = cfg["series"].labelsRenderer;
		
		var chart = Ext.create({
			xtype: "cartesian",
		    width: "100%",
		    height: 200,
			background: "#EFF0F0",
			plugins: {
		        ptype: "chartitemevents",
		        moveEvents: true
		    },
			cls: "sone-chart sone-chart-pointer",		            
            interactions: "itemhighlight",
            animation: Ext.isIE8 ? false : {
                easing: "backOut",
                duration: 500
            },
			store: store,
	
		    //set legend configuration
		    legend: {
		        docked: "right"
		    },
		
		    //define the x and y-axis configuration.

		    axes: [{
		        type: "category",
		        position: "bottom",
				label: {
					fontSize: "20px !important"
				}
		    }, {
		        type: "numeric",
		        position: "left",
				grid: true
		    }],
		
		    //define the actual bar series.

			series: [{
                type: "bar",
                xField: xFields,
				scope: this,
				title: yTitles,
                yField: yFields,
				colors: (!Ext.isEmpty(barColors)) ? barColors : ["#A9AFBC","#4FC0E8", "#ffce55"],
				style: {
                    minGapWidth: 20,
					fillOpacity: 1
                },
				subStyle: {
                    strokeStyle: (!Ext.isEmpty(strokeColors)) ? strokeColors : ["#A9AFBC","#4FC0E8", "#ffce55"],
					lineWidth: 1
                },
				highlight: {
					strokeStyle: "#ed5564",
					strokeOpacity: 0,
					lineWidth: 0,
                    fillStyle: "#ed5564",
					fillOpacity: 1
                },
                tooltip: {
                    trackMouse: true,
                    style: "background: #fff",
                    renderer: labelsRenderer
                }
            }]
		});
		return chart;
	},
	
	
	// reports related functions
	
	/**
	 * Dan: Get field type
	 * 
	 */
	getColType: function(gridName, fieldIndex) {
	
		var grid = this._get_(gridName);
		var s = grid.getStore();
		var m = s.getModel();
		var fields = m.getFields();
		var type = "";
		
		for (var z =0; z < fields.length; z++) {
			if (fields[z].name === fieldIndex) {
				type = fields[z].type;
				break;
			}
		}
		
		return type;
	},
	
	
	/**
	 * Dan: SONE-1792: Create XML file to run special reports 
	 * 
	 */
	doCustomReport : function(gridName, format, reportCode, extraParams, record) {

		var fileFormat = format;
		var grid = this._get_(gridName);
		var code = reportCode;
		var ctrl = this._getDc_(grid.__dcName__);
		
		var s = grid.getStore();
		var m = s.getModel();
		var fields = m.getFields();

		var _exp = {
			title : this._title_
		};

		var cm = grid.down("headercontainer").getGridColumns();
		var excludeColumns = [""];
		// [ "refid", "id", "modifiedBy", "modifiedAt","createdAt", "createdBy", "" ];

		var len = cm.length, z;
		var _cols = [];
		for (var i = 0; i < len; i++) {

			if (excludeColumns.indexOf(cm[i].dataIndex) === -1) {
				var colType = "";
				for (z =0; z < fields.length; z++) {
					if (fields[z].name === cm[i].dataIndex) {
						colType = fields[z].type;
						break;
					}
				}
				
				_cols[_cols.length] = {
					name : cm[i].dataIndex,
					type : colType,
					mask : cm[i]._mask_
				}
			}

		}
		_exp.columns = _cols;
		var url = Main.dsAPI(ctrl.dsName, fileFormat);
		var _p = ctrl.buildRequestParamsForQuery();
		
		_p[Main.requestParam.EXPORT_INFO] = Ext.encode(_exp);
		_p[Main.requestParam.REPORT_CODE] = code;
		
		////////////////////////////////////////////////////////////////////////////
		
		if (extraParams) {

			var customCols = [];
			
			if (!record) {
				Main.error("Record is empty");
				return;
			}
			
			for (z =0; z < extraParams.length ; z++) {
				customCols[customCols.length] = {
					name : extraParams[z],
					type : this.getColType(gridName,extraParams[z]),
					value: record.get(extraParams[z])
				}
			}
			
			var customExpInfo = {
				title : this._title_
			};
			
			customExpInfo.dataList = customCols;
			_p[Main.requestParam.DIALOG_REPORT_INFO] = Ext.encode(customExpInfo);
		}
		
		///////////////////////////////////////////////////////////////////////////////

		Ext.Msg.progress(Main.translate("msg", "working"));
		Ext.Ajax.request({
			url : url["dialogReport"],
			params : _p,
			success : function(response) {
				Ext.Msg.hide();
				window.open(url["reportDownload"] + Ext.getResponseDataInText(response), "_blank");
			},
			failure : function(response) {
				Ext.Msg.hide();
				Main.serverMessage(null, response);
			}
		});
		return;
	},

	/**
	 * Run a report based on the given configuration parameters
	 * 
	 * queryBuilderClass optional title - report title
	 * 
	 * The following are according to report definition from administration
	 * module dcAlias - DC instance params - report parameters url -
	 * report-server url contextPath - report context path
	 * 
	 */
	runReport : function(cfg) {

		if (!cfg.queryBuilderClass) {
			cfg.queryBuilderClass = "e4e.dc.tools.DcReport";
		}

		var b = Ext.ClassManager.isCreated(cfg.queryBuilderClass);
		if (!b) {
			Main.error("Query builder class `" + cfg.queryBuilderClass + "` not found ");
			return;
		}
		var dcReport = Ext.create(cfg.queryBuilderClass);

		var rec = this._getDc_(cfg.dcAlias).record;
		if (!rec) {
			var _msg = Main.translate("msg", "no_current_record_for_report", [ cfg.title ]);

			Ext.Msg.show({
				title : Main.translate("msg", "no_current_record"),
				msg : _msg,
				icon : Ext.MessageBox.INFO,
				buttons : Ext.Msg.OK
			});
			return false;
		}
		dcReport.applyDsFieldValues(cfg.params, rec.data);
		dcReport.run({
			url : cfg.url,
			contextPath : cfg.contextPath,
			params : cfg.params
		});
	},

	getReport : function(reportName) {
		var reports = this._reports_;
		for (var i = 0; i < reports.length; ++i) {
			var report = reports[i];
			if (report.report === reportName) {
				return report;
			}
		}
		return null;
	},
	
	
	// view and filter toolbar and related functions

	_matchGrids_ : function(item) {
		// if this is a grid, find a filter for the same dc
		if (item.__dcViewType__ === "grid") {
			var gridDcName = item.__dcName__;
			var gridFilterPanel = this._elems_.findBy(function(it) {
				if (it.__dcViewType__ === "filter-form" && it.__dcName__ === gridDcName) {
					return true;
				}
			});
			if (gridFilterPanel) {
				var grid = this._get_(item.name);
				if (Ext.isEmpty(grid._viewFilterToolbarId_)) {
					var tlb = this._buildViewFilterToolbar_(item._controller_, item.name, gridFilterPanel.name, grid);
					
					grid.addDocked(tlb,0);
					grid._filterComboId_ = tlb._filterComboId_;
					grid._viewComboId_ = tlb._viewComboId_;
					grid._totalsComboId_ = tlb._totalsComboId_;
					grid._unitComboId_ = tlb._unitComboId_;
					grid._currencyComboId_ = tlb._currencyComboId_;
					grid._datePickerId_ = tlb._datePickerId_;
					grid._resetButtonId_ = tlb._resetButtonId_;
					grid._viewFilterToolbarId_ = tlb.getId();
					
					if (!Ext.isEmpty(grid._totalPanelId_)) {
						var totalsCombo = Ext.getCmp(grid._totalsComboId_); 
						totalsCombo.show();
					}
					
					gridFilterPanel.title = "";
					gridFilterPanel.collapsible = false;
					this._gridsWithFilter_.add(item.name, item);
				}
			}
		}
	},
	
	_configureViewsAndFilters_ : function() {
		if (typeof __DEFAULTELEMENTS__ !== 'undefined') {

		var fn = function(key, item) {
			var grid = this._get_(item.name);
			if(!grid){
				return;
			}
			var ctrl = grid._controller_;
			var filterCombo = Ext.ComponentQuery.query('[id=' + grid._filterComboId_ + ']')[0];
			var filterStore = filterCombo.getStore();
			var viewCombo = Ext.ComponentQuery.query('[id=' + grid._viewComboId_ + ']')[0];
			var resetButton = Ext.ComponentQuery.query('[id=' + grid._resetButtonId_ + ']')[0];
			var viewStore = viewCombo.getStore();
			var viewId = null;
			var filterId = null;
			var totalsCombo = Ext.ComponentQuery.query('[id=' + grid._totalsComboId_ + ']')[0];
			var unitCombo = Ext.ComponentQuery.query('[id=' + grid._unitComboId_ + ']')[0];
			var currencyCombo = Ext.ComponentQuery.query('[id=' + grid._currencyComboId_ + ']')[0];
			var datePicker = Ext.ComponentQuery.query('[id=' + grid._datePickerId_ + ']')[0];
			var currentStandardFilters = ctrl.filter.data;
			var noReset = ctrl.noReset;
			var forceQuery = false;

			// ----------- Start defining the default elements -----------
			var elem = null;
			
			for (var i = 0; i < __DEFAULTELEMENTS__.length; i++) {
				var x = __DEFAULTELEMENTS__[i];
				if (x.cmp === item.name) {
					elem = x;
					break;
				}
			}
			
			// Bug fix to prevent the dc from making multiple queries

			var hasFilters = function() {
				var result = false;
				if (!Ext.isEmpty(ctrl.advancedFilter)) {
					result = true;
				}
				for ( var i in currentStandardFilters) {
					if (!Ext.isEmpty(currentStandardFilters[i])) {
						result = true;
						break;
					}
				}
				return result;
			}

			var isFilterAlreadySet = hasFilters();

			// Setup the Reset button

			resetButton._viewCombo_ = viewCombo;
			resetButton._filterCombo_ = filterCombo;

			if (isFilterAlreadySet === true) {
				resetButton._currentAppliedFilter_ = ctrl.getFilter().data;
			} else {
				resetButton._currentAppliedFilter_ = null;
			}

			// End bug fix to prevent the dc from making multiple queries

			if (elem) {
				viewId = elem.view.id;
				filterId = elem.filter.id;
			}

			if (!Ext.isEmpty(viewId)) {
				
				var viewState = Ext.JSON.decode(elem.view.value);
				var pageSize = elem.pageSize;
				var totals = elem.totals;
				var viewName = elem.view.name;
				grid._defaultViewId_ = viewId;
				viewCombo.setValue(viewName);

				grid._setPageSize_(pageSize);
			
				grid._applyViewState_(viewState);

				if (totalsCombo) {
					totalsCombo.setValue(totals ? 1 : 0);
					
					if (totals === true) {
						grid._showSummary_(true);					
					} else {
						grid._showSummary_(false);
					}
				}
				if (unitCombo) {
					unitCombo.setValue(elem.unit);
				}
				if (currencyCombo) {
					currencyCombo.setValue(elem.currency);
				}
				if (grid._currencyParamName_) {
					grid._controller_.setParamValue(grid._currencyParamName_, elem.currency,null,null,true);
				}
				if (grid._unitParamName_) {
					grid._controller_.setParamValue(grid._unitParamName_, elem.unit,null,null,true);
				}
				// the value from datePicker is not yet saved in view in database
			}

			if (unitCombo || currencyCombo || datePicker) {
				forceQuery = true;

				if (datePicker) {
					var value = grid._controller_.getParamValue(grid._dateParamName_);
			        if( !Ext.isDate(value) ){
			        	value = new Date(); // force current date in parameter
			        	value = new Date(value.getFullYear(),value.getMonth(),value.getDate());
			        	grid._controller_.setParamValue(grid._dateParamName_,value,null,null,true);
			        }
			        datePicker.setValue(value);
				}
				
				grid._controller_.on("parameterValueChanged", function(dc, property, ov, newValue) { 
					// additional parameter: op
					if (property === grid._currencyParamName_ && currencyCombo) {
						currencyCombo.setValue(newValue);
					} else if (property === grid._unitParamName_ && unitCombo) {
						unitCombo.setValue(newValue);
					} else if (property === grid._dateParamName_ && datePicker) {
						datePicker.setValue(newValue);
					}
				}, this);
			}

			if (!Ext.isEmpty(filterId)) {
				var standardFilterValue = elem.filter.standardFilterValue;
				var advancedFilterValue = elem.filter.advancedFilterValue;
				var filterName = elem.filter.name;
				var standardFilter = Ext.decode(standardFilterValue);
				
				var applyCtxFilter = function() {			        
			        if (filterCombo._isCtrlInSomeContext_() === true) {			        	
			        	var ctxData = ctrl.getDcContextFields();			        	
			        	Ext.each(ctxData, function(item) {
				        	if(currentStandardFilters.hasOwnProperty(item) ){
				        		standardFilter[item] = currentStandardFilters[item];
				        	}
			        	}, this);
			        }
				}
				
				applyCtxFilter();
				
				filterCombo.setValue(filterName);
				ctrl.filter = Ext.create(ctrl.filterModel, standardFilter);
				ctrl.advancedFilter = Ext.decode(advancedFilterValue);
			}
			// ----------- End defining the default elements -----------
			filterStore.load({
				callback : function(records, operation, success) { 
					if( success === true ) {
						Ext.Array.each(records, function(record) { 
							if (record.get("id") === filterId && !Ext.isEmpty(filterId)) {
								filterCombo._selectedId_ = record.get("id");
							}
						});
					}
				}
			});
			viewStore.load({
				callback : function(records, operation, success) {
					if( success === true ) {
						Ext.Array.each(records, function(record, index) {
							if (record.get("id") === viewId && !Ext.isEmpty(viewId)) {
								viewCombo._selectedIndex_ = index;
								viewCombo._selectedId_ = record.get("id");
								viewCombo._pageSize_ = record.get("pageSize");
							}
						});
					}
				}
			});

			// Load the initial data if the data hasen't been loaded yet
			if (forceQuery || noReset === true || 
				(grid.store.getCount() === 0 && isFilterAlreadySet === false && this.frameOpenParams.disableInitialQuery !== true)	) { 
				ctrl.delayedQuery();
			}
		};
		this._gridsWithFilter_.eachKey(fn, this);
		
		this._callAfterViewAndFilters_();
		}
	},

	_callAfterViewAndFilters_ : function(){
		if( this._initPhase2Done_ ){
			// call the function only after each panel is created/linked in the DOM
			this._afterViewAndFilters_();
		} else {
			Ext.defer(this._callAfterViewAndFilters_, 1000, this);
		}
	},
	
	_buildViewFilterToolbar_ : function(controller, gridName, filterName, grid) {
		var filterCfg = this._getConfig_(filterName);
		var gridCfg = this._getConfig_(gridName);

		var toolbar = Ext.create("ViewFilter.Toolbar", {
			id : Ext.id(),
			_viewAndFilterTlb_ : true,
			_ctrl_ : controller,
			_filterCfg_ : filterCfg,
			_gridCfg_ : gridCfg,
			_gridName_ : gridName,
			_gridObj_ : grid,
			dock : "top"
		});

		return toolbar;
	},
	
	// KPI
	
	_setKpiAndTitle_ : function() {
		// ====================================================================
		// Dan: Fire the setupHeader event in the application menu
		// ====================================================================

		this._frameParameters_ = {
			// these informations are readed from __FRAMEPARAMETERS__
			glyph : this._glyph_, 
			title : this._title_,
			frame : this
		};
		
		if (typeof _KPIS_ !== 'undefined' && !Ext.isEmpty(_KPIS_)) {
			this._kpiParameters_  = _KPIS_;
		}

		getApplication().menu.fireEvent("setupHeader", this._frameParameters_, this._kpiParameters_);

		// ====================================================================
		// End Dan
		// ====================================================================
	}
	
});
