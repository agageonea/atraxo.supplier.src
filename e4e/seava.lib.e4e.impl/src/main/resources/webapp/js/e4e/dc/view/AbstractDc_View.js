/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
/**
 * Mixin which provides DC-view support.
 */
Ext.define("e4e.dc.view.AbstractDc_View", {

	// **************** Properties *****************

	/**
	 * DC-controller
	 */
	_controller_ : null,

	// **************** Private methods *****************
	
	_dcViewInitEventHandlers_ : function() {
		this._controller_.on("storeIsLoadingChanged", this._onDcStoreIsLoadingChanged_, this);
	},
	
	_onDcStoreIsLoadingChanged_ : function(dc, isLoading){
		var fDisableTB = function(tb){
			if( Ext.isFunction(tb.setDisabled) ){
				if( isLoading ){
					if( Ext.isEmpty(tb._oldDisabled_) ){
						tb._oldDisabled_ = tb.disabled;
					}
					tb.setDisabled(true);
				} else {
					if( !Ext.isEmpty(tb._oldDisabled_) ){
						tb.setDisabled(tb._oldDisabled_);
						if(tb._oldDisabled_ !== true && (tb.getMaskTarget() || tb.el)){
							tb.unmask();
						}
						delete tb._oldDisabled_;
					} else {
						tb.setDisabled(false);
						if( tb.getMaskTarget() || tb.el ){
							tb.unmask();
						}
					}
				}
			}
		};
		var di = this.getDockedItems('toolbar[dock="top"]');
		for(var i=0, l=di.length; i<l; i++){
			if( !di[i]._viewAndFilterTlb_ ){
				fDisableTB(di[i]);
			}
		}
	},

	// **************** Public API *****************

	/**
	 * Returns the controller of this view
	 */
	_getController_ : function() {
		return this._controller_;
	},

	/**
	 * Apply state rules to enable/disable or show/hide components.
	 * 
	 * The model parameter is either the current record or current filter bound
	 * to this form-view.
	 * 
	 */
	_applyStates_ : function(model) {
		if (this._beforeApplyStates_(model) !== false) {
			this._onApplyStates_(model);
		}
		this._afterApplyStates_(model);
	},

	/**
	 * Template method checked before applying states.
	 */
	_beforeApplyStates_ : function() {
		// parameter: model
		return true;
	},

	/**
	 * Template method invoked after the state rules are applied.
	 */
	_afterApplyStates_ : function() {
		// parameter: model
	},

	/**
	 * Implement the state control logic in subclasses.
	 * 
	 */
	_onApplyStates_ : function() {
		// parameter: model
	},

	/**
	 * Template method called after a new model is bound to the form. Add custom
	 * logic in subclasses if necessary.
	 * 
	 * Model parameter: Current record or current filter depending on the DC
	 * view type
	 */
	_afterBind_ : function(model) {
		this.fireEvent("afterbind",this,model);
	},

	/**
	 * Template method called after a model is un-bound from the form. Add
	 * custom logic in subclasses if necessary.
	 * 
	 * Model parameter: Current record or current filter depending on the DC
	 * view type
	 */
	_afterUnbind_ : function(model) {
		this.fireEvent("afterunbind",this,model);
	},

	// **************** Private methods *****************

	/**
	 * Get the translation from the resource bundle for the specified key.
	 * 
	 * @param {String}
	 *            k Key to be translated
	 * @return {String} Translation of the key or the key itself if not
	 *         translation found.
	 */
	_getRBValue_ : function(k) {
		if (this._trl_ != null && this._trl_[k]) {
			return this._trl_[k];
		}
		if (this._controller_._trl_ != null && this._controller_._trl_[k]) {
			return this._controller_._trl_[k];
		} else {
			return k;
		}
	},

	_beforeDestroyDNetDcView_ : function() {
		this._controller_ = null;
		if (this._builder_) {
			this._builder_.dcv = null;
		}
	}

});