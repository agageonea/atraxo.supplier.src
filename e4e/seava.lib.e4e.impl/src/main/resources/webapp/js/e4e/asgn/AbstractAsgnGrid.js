/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.asgn.AbstractAsgnGrid", {
	extend : "Ext.grid.Panel",

	mixins : {
		elemBuilder : "e4e.base.Abstract_View"
	},

	// **************** Properties *****************

	/**
	 * Component builder
	 */
	_builder_ : null,

	/**
	 * Assignment controller
	 */
	_controller_ : null,

	/**
	 * Which grid is implemented. Possible values are left(for available) and
	 * right(for selected)
	 */
	_side_ : null,

	_columns_ : null,

	// **************** Public API *****************

	/**
	 * Returns the builder.
	 */
	_getBuilder_ : function() {
		if (this._builder_ == null) {
			this._builder_ = new e4e.asgn.AsgnGridBuilder({
				asgnGrid : this
			});
		}
		return this._builder_;
	},

	// **************** Defaults and overrides *****************

	forceFit : true,
	loadMask : true,
	stripeRows : true,
	border : true,

	viewConfig : {
		emptyText : "No records found to match the selection criteria."
	},

	initComponent : function(config) {
		this._elems_ = new Ext.util.MixedCollection();
		this._columns_ = new Ext.util.MixedCollection();

		this._startDefine_();
		this._defineDefaultElements_();

		if (this._beforeDefineColumns_() !== false) {
			this._defineColumns_();
			this._afterDefineColumns_();
		}

		this._columns_.each(this._postProcessColumn_, this);
		this._endDefine_();

		var cfg = {
			columns : this._columns_.getRange(),
			store : this._controller_.getStore(this._side_),
			selModel : {
				mode : "MULTI"
			}
		};
		Ext.apply(cfg, config);
		Ext.apply(this, cfg);
		this.callParent(arguments);
		this._registerListeners_();
	},

	/**
	 * Register event listeners
	 */
	_registerListeners_ : function() {
		this.mon(this.store, "load", function(store) {
			if (store.getCount() > 0) {
				this.getSelectionModel().select(0);
			}
		}, this);
	},

	_defineColumns_ : function() {
	},

	_beforeDefineColumns_ : function() {
		return true;
	},

	_afterDefineColumns_ : function() {
	},

	_defineDefaultElements_ : function() {
	},

	_onStoreLoad_ : function() {
	},

	_afterEdit_ : function() {
	},

	/**
	 * Postprocessor run to inject framework specific settings into the columns.
	 * 
	 * @param {Object}
	 *            column Column's configuration object
	 * @param {Integer}
	 *            idx The index
	 * @param {Integer}
	 *            len Total length
	 */
	_postProcessColumn_ : function(column) {
		if (column.header === undefined || column.header === null) {
			column.header = Main.translateModelField(this._controller_._trl_, column.name);
		}
	}
});