/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
e4e.base.FrameNavigatorWithIframe = {

	/**
	 * Maximum number of tabs (application frames) which are allowed to be
	 * opened at a certain moment. Use -1 for unlimited.
	 * 
	 * @type Integer
	 */
	maxOpenTabs : -1,

	/**
	 * When open a new frame, until the translation files are loaded, set the
	 * tab title as the frame fully qualified name or its simple name
	 */
	titleAsSimpleName : true,

	/**
	 * Lookup the IFrame instance.
	 * 
	 * @param {}
	 *            frame
	 * @return {}
	 */
	getIFrameInstance : function(frameFqn) {
		var fr = document.getElementById(Main.getFrameIFrameId(frameFqn));
		if (Ext.isEmpty(fr)) {
			fr = window.frames[Main.getFrameIFrameId(frameFqn)];
		}
		return fr;
	},

	/**
	 * Lookup the application frame instance.
	 * 
	 * @param {}
	 *            frame
	 * @return {}
	 */
	getFrameInstance : function(frame) {
		var theIFrame = this.getIFrame(frame);
		var theFrameInstance = null;
		if (theIFrame) {
			theFrameInstance = theIFrame.theFrameInstance;
		}
		return theFrameInstance;
	},

	/**
	 * Lookup the IFrame.
	 * 
	 * @param {}
	 *            frame
	 * @return {}
	 */
	getIFrame : function(frameFqn) {
		return window.frames[Main.getFrameIFrameId(frameFqn)];
	},

	/**
	 * Check if the given frame is already open.
	 * 
	 * @param {}
	 *            frame Frame name
	 * @return {}
	 */
	isFrameOpened : function(frameFqn) {
		return !Ext.isEmpty(this.getIFrameInstance(frameFqn));
	},

	/**
	 * Check if the given application frame is active, i.e. the corresponding
	 * tab panel is active.
	 * 
	 * @param {}
	 *            frame
	 * @return {}
	 */
	isFrameActive : function(frameFqn) {
		return (getApplication().getViewBody().getActiveTab().getId() === Main.getFrameTabId(frameFqn));
	},

	/**
	 * Show the given frame. If it is open activate it otherwise open a new tab
	 * and load it there.
	 * 
	 * @param {}
	 *            frame
	 * @param {}
	 *            params
	 */
	showFrame : function(frame, params) {
		return this._showFrameImpl(frame, params);
	},

	/**
	 * Internal implementation function.
	 * 
	 * @param {}
	 *            frame
	 * @param {}
	 *            params
	 */
	_showFrameImpl : function(frameFqn, params) {

		if (!(params && params.url)) {
			alert("Programming error: params.url not specified in showFrame!");
			return false;
		}

		var tabID = Main.getFrameTabId(frameFqn);
		var ifrID = Main.getFrameIFrameId(frameFqn);
		var vb = getApplication().getViewBody();
		var tabTtl;
		tabTtl = params.frameTitle;
		if (!params.frameTitle) {
			tabTtl = "";
		}

		if (Ext.isEmpty(document.getElementById(ifrID)) && !Ext.isEmpty(window.frames[ifrID])) {
			delete window.frames[ifrID];
		}

		if (this.isFrameOpened(frameFqn)) {
			if (!this.isFrameActive(frameFqn)) {
				vb.setActiveTab(tabID);
			}
		} else {
			if (this.maxOpenTabs > 0 && ((vb.items.getCount() + 1) === this.maxOpenTabs)) {
				Ext.Msg.alert('Warning', 'You have reached the maximum number of opened tabs (' + (this.maxOpenTabs)
						+ ').<br> It is not allowed to open more tabs.');
				return false;
			}

			var _odfn = function() {
				Ext.destroy(window.frames[this.n21_iframeID].__theViewport__);
				Ext.destroy(window.frames[this.n21_iframeID].theFrameInstance);
				try {
					delete window.frames[this.n21_iframeID];
				} catch (e) {
					// nothing to do
				}
				//this.callParent();
			};

			var beforeCloseFn = function() { // parameters: tab, eOpts
				var _fr = window.frames[this.n21_iframeID];
				if (_fr && _fr.theFrameInstance) {
					_fr.theFrameInstance.fireEvent("fpclose",_fr.theFrameInstance);
					if(_fr.theFrameInstance.isDirty()) {
						return confirm(Main.translate("msg", "dirty_data_on_frame_close"));
					}
				}
				return true;
			};
			var onActivateFn = function() { // parameters: tab, eOpts
				var _fr = window.frames[this.n21_iframeID];
				if (_fr && _fr.theFrameInstance) {
					_fr.theFrameInstance.updateLayout();
				}
			};

			var _p = new Ext.Panel({
				onDestroy : _odfn,
				title : tabTtl,
				fqn : frameFqn,
				id : tabID,
				n21_iframeID : ifrID,
				autoScroll : true,
				layout : 'fit',
				closable : true,
				listeners : {
					beforeclose : {
						fn : beforeCloseFn
					},
					activate : {
						fn : onActivateFn
					},
					boxready : {
						scope: this,
						fn: function(panel) {
							var panelHeight = panel.getEl().dom.clientHeight;
//							panel.update('<div style="width:100%; height:'+panelHeight+'px; overflow: hidden; overflow-y:auto; -webkit-overflow-scrolling:touch" id="div_' + frameFqn + '" ><iframe id="' + ifrID + '" name="' + ifrID
//									+ '" src="' + params.url + '" style="border:0;width:100%;height:'+panelHeight+'px" FRAMEBORDER="no"></iframe></div>');
							
							panel.update('<div style="width:100%; height:100%; overflow: hidden; overflow-y:auto; -webkit-overflow-scrolling:touch" id="div_' + frameFqn + '" ><iframe id="' + ifrID + '" name="' + ifrID
									+ '" src="' + params.url + '" style="border:0;width:100%;height:100%" FRAMEBORDER="no"></iframe></div>');
							
						}
					}
				}
			});
			vb.add(_p);
			vb.setActiveTab(tabID);
		}
		return ifrID;
	}
};
