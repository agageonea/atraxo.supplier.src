/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define('e4e.ui.FrameBuilder$TocModel', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'name',
		type : 'string'
	}, {
		name : 'title',
		type : 'string'
	}, {
		name : 'dc',
		type : 'string'
	} ]
});

Ext.define("e4e.ui.FrameBuilder", {

	/**
	 * Frame instance
	 */
	frame : null,

	constructor : function(config) {
		config = config || {};
		Ext.apply(this, config);
		this.callParent(arguments);
	},

	/**
	 * Add frame monitoring object
	 */
	addFrameMon : function(name, dcObj) {
		name = name.toUpperCase();
		var fObj = this.frame._frameMonList_.get(name);
		if( !fObj ) {
			fObj = {
				_frameName_ : name,
				_dcs_ : []
			}
		}
		fObj._dcs_.push(dcObj);
		this.frame._frameMonList_.add(name,fObj);
		return this;
	},
	
	/**
	 * Add a data-control controller
	 */
	addDc : function(name, obj) {
		this.frame._dcs_.add(name, obj);
		obj._instanceKey_ = name;
		obj._frame_ = this.frame;
		this.frame.mon(obj, "message", this.frame.message, this.frame);
		return this;
	},
	
	registerDcInSM : function(name, config) {
		if (config && config.stateManager) {
			for(var i=0;i<config.stateManager.length;i++){
				var smRuleName = "dc__"+name;
				var options = {
					and : config.stateManager[i].and,
					dcMon : this.frame._dcs_.get(name),
					smRuleName : smRuleName,
					config : config
				};
				if (options.and) {
					this.frame._buttonStateRules_[smRuleName] = options.and;
				}
				e4e.ui.FrameButtonStateManager.register(smRuleName,
						config.stateManager[i].name, config.stateManager[i].dc,
						this.frame, options);
			}
		}
		return this;
	},

	/**
	 * Link data-controls with the given rules.l
	 */
	linkDc : function(childName, parentName, relation) {
		Ext.applyIf(relation, {
			fetchMode : "manual",
			strict : true
		});
		var c = this.frame._dcs_.get(childName);
		var p = this.frame._dcs_.get(parentName);
		var ctx = new e4e.dc.DcContext({
			childDc : c,
			parentDc : p,
			relation : relation
		});
		p.addChild(c);
		c.setDcContext(ctx);
		return this;
	},

	/**
	 * Add a data-control edit form view.
	 */
	addDcFormView : function(dc, config) {
		config.__dcViewType__ = "form";
		config.__dcName__ = dc;
		this.addDcView(dc, config);
		return this;
	},

	/**
	 * Add a data-control filter form view.
	 */
	addDcFilterFormView : function(dc, config) {
		config.__dcViewType__ = "filter-form";
		config.__dcName__ = dc;
		this.addDcView(dc, config);
		return this;
	},

	/**
	 * Add a data-control grid view.
	 */
	addDcGridView : function(dc, config) {
		config.__dcViewType__ = "grid";
		config.__dcName__ = dc;
		config.stateId = this.frame.$className + "-" + config.name;
		if (getApplication().getSession().rememberViewState) {
			config.stateful = true;
		}
		return this.addDcView(dc, config);
	},

	/**
	 * Add a data-control edit grid view.
	 */
	addDcEditGridView : function(dc, config) {
		return this.addDcGridView(dc, config);
	},

	/**
	 * Add a free-hand data-control view.
	 */
	addDcView : function(dc, config) {
		Ext.apply(config, {
			_controller_ : this.frame._dcs_.get(dc)
		});
		if (config._hasTitle_ === true) {
			config.title = this.frame.translate(config.name + "__ttl");
		}
		this.applyViewSharedConfig(config);
		return this;
	},

	/**
	 * Add a presentation panel
	 */
	addPanel : function(config) {
		config.listeners = config.listeners || {};
		if (config.onActivateDoLayoutFor) {
			var onActivateDoLayoutFor = config.onActivateDoLayoutFor;
			delete config.onActivateDoLayoutFor;
			var activate = {
				scope : this.frame,
				fn : function() {
					for (var i = 0; i < onActivateDoLayoutFor.length; i++) {
						var e = this._getElement_(onActivateDoLayoutFor[i]);
						e.doLayout();
					}
				}
			}
			if (!config.listeners.activate) {
				config.listeners.activate = activate;
			}
		}
		if( config.collapsible === true && config.xtype === "panel" ){
			Ext.applyIf(config, { cls : "sone-panel-collapsible" } );
		}
		Ext.applyIf(config, {
			id : Ext.id()
		});

		if( config._parentCssCls_ && !config.listeners.afterrender ) {
			var afterrender = {
				scope : this.frame,
				single : true,
				fn : function(e) {
					if( e._parentCssCls_ ){
						var te = e.getTargetEl();
						if( te ){
							te.addCls(e._parentCssCls_);
						}
					}
				}
			};
			config.listeners.afterrender = afterrender;
		}
		
		if (config._hasTitle_ === true) {
			config.title = this.frame.translate(config.name + "__ttl");
		}
		this.applyViewSharedConfig(config);
		return this;
	},

	/**
	 * Add child elements to the given container
	 */
	addChildrenTo : function(c, list, regions, tabPos) {
		var cnt = this.frame._getElementConfig_(c);
		var isWrapped = cnt._wrapped_;
		var items = ((isWrapped) ? cnt["items"]["items"] : cnt["items"]) || [];
		if( Ext.isNumber(tabPos) ){
			if( tabPos<0 ){
				tabPos=0;
			} else {
				if( tabPos>items.length ){
					tabPos = items.length;
				}
			}
		} else {
			tabPos = items.length;
		}
		for (var i = 0, len = list.length; i < len; i++) {
			var cmp = this.frame._elems_.get(list[i]);
			items.splice(tabPos, 0, cmp);
			tabPos++;
			if (regions) {
				cmp.region = regions[i];
			}
		}
		if (isWrapped) {
			cnt["items"]["items"] = items;
		} else {
			cnt["items"] = items;
		}
		return this;
	},
	
	addChildrenTo2 : function(c, list, regions, tabPos) {
		var cnt = this.frame._getElement_(c);
		if( !cnt ){
			// component is not on the first canvas/tab => is not yet created
			return this.addChildrenTo(c, list, regions, tabPos);
		}
		var isWrapped = cnt._wrapped_;
		var items = ((isWrapped) ? cnt["items"]["items"] : cnt["items"]) || [];
		if( Ext.isNumber(tabPos) ){
			if( tabPos<0 ){
				tabPos=0;
			} else {
				if( tabPos>items.length ){
					tabPos = items.length;
				}
			}
		} else {
			tabPos = items.length;
		}
		for (var i = 0, len = list.length; i < len; i++) {
			var cmp = this.frame._getElementConfig_(list[i]);
			if( cnt.xtype === "tabpanel" ){
				cnt.insert(tabPos,cmp);
			} else {
				Ext.applyIf(cmp,{xtype:"panel"});
				items.add(list[i], Ext.create(cmp));
			}
			tabPos++;
			if (regions) {
				cmp.region = regions[i];
			}
		}
		return this;
	},

	/**
	 * Link a toolbar to the given component.
	 */
	addToolbarTo : function(c, tlb) {
		this.frame._linkToolbar_(tlb, c);
		return this;
	},

	/**
	 * Start a toolbar creation process. The returned ActionBuilder provides
	 * methods to add elements with a fluid API
	 */
	beginToolbar : function(name, config) {
		return new e4e.ui.ActionBuilder({
			name : name,
			frame : this.frame,
			dc : config.dc,
			dcInst : this.frame._getDc_(config.dc)
		});
	},

	/**
	 * Start the custom action-manager definitions. The returned ActionBuilder provides
	 * methods to add elements with a fluid API
	 */
	beginStateFnDef : function(name, config) {
		return new e4e.ui.ActionBuilder({
			name : name,
			frame : this.frame,
			dc : config.dc,
			dcInst : this.frame._getDc_(config.dc)
		});
	},

	/**
	 * Add a table-of-contents elements.Used to include several independent root
	 * components , each on its own canvas with a ToC like navigation.
	 */
	addToc : function(canvases) {
		var data = [];
		for (var i = 0; i < canvases.length; i++) {
			var c = canvases[i].split(":");
			data[i] = {
				"name" : c[0],
				"dc" : c[1],
				"title" : this.frame.translate(c[0] + "__ttl")
			};
		}
		var store = Ext.create('Ext.data.Store', {
			model : 'e4e.ui.FrameBuilder$TocModel',
			data : data
		});

		var config = {
			name : "_toc_",
			collapsible : true,
			layout : "fit",
			id : Ext.id(),
			region : "west",
			title : 'Navigation',
			width : 200,
			frame : false,
			items : [ {
				name : "_toc_items_",
				xtype : 'gridpanel',
				id : Ext.id(),
				hideHeaders : true,
				autoScroll : true,
				viewConfig : {
					stripeRows : false
				},
				singleSelect : true,
				forceFit : true,
				store : store,
				columns : [ {
					header : 'title',
					dataIndex : 'title'
				} ],
				selModel : {
					mode : "SINGLE",
					listeners : {
						"selectionchange" : {
							scope : this.frame,
							fn : function(sm, selected) { // additional parameters: options
								if (this._getElement_("main").rendered) {
									var _d = selected[0].data;
									this._showStackedViewElement_("main", _d.name);
									this._rootDc_ = _d.dc;
								}
							}
						}
					}
				},
				listeners : {
					scope : this.frame,

					afterrender : function() {
						this._showTocElement_(0);
					}
				}
			} ]

		}
		this.frame._elems_.add(config.name, config);
		return this;
	},

	/**
	 * Create a button
	 * 
	 * @param {}
	 *            config
	 * @return {}
	 */
	addButton : function(config) {
		Ext.applyIf(config, {
			id : Ext.id(),
			xtype : "button"
		});

		var lblKey = config.name + "__lbl";
		var tlpKey = config.name + "__tlp";

		Ext.apply(config, {
			text : this.frame.translate(lblKey),
			tooltip : this.frame.translate(tlpKey)
		});

		if (!Main.viewConfig.USE_BUTTON_ICONS) {
			config.iconCls = null;
		}

		this.frame._elems_.add(config.name, config);

		if (config.stateManager) {
			for(var i=0;i<config.stateManager.length;i++){
				var options = {
					and : config.stateManager[i].and,
					visibleRule : config.stateManager[i].visibleRule
				};
				if (options.and) {
					var s = (options.visibleRule===true)?"_vr":"";
					this.frame._buttonStateRules_[config.name+s] = options.and;
				}
				e4e.ui.FrameButtonStateManager.register(config.name,
						config.stateManager[i].name, config.stateManager[i].dc,
						this.frame, options);
			}
		}
		return this;
	},

	/**
	 * Add assignment component.
	 */
	addAsgn : function(config) {
		Ext.applyIf(config, {
			id : Ext.id(),
			objectIdField : "id"
		});
		this.frame._elems_.add(config.name, config);
		return this;
	},

	addAsgnPanel : function(config) {
		Ext.applyIf(config, {
			id : Ext.id(),
			objectIdField : "id"
		});
		if (config._hasTitle_ === true) {
			config.title = this.frame.translate(config.name + "__ttl");
		}
		this.frame._elems_.add(config.name, config);
		return this;
	},
	
	/**
	 * Add a window to the elements list.
	 */
	addWindow : function(config) {
		Ext.applyIf(config, {
			id : Ext.id(),
			_window_ : true
		});
		if (!config.listeners) {
			config.listeners = {}
		}
		if (!config.listeners.show) {
			config.listeners.show = {
				fn : function() {
					var _f = this.down(' textfield');
					if (_f) {
						_f.focus();
					}
				}
			}
		}
		config.title = this.frame.translate(config.name + "__ttl");
		this.frame._elems_.add(config.name, config);
		return this;
	},
	
	addAsgnWindow : function(config) {
		Ext.applyIf(config, {
			_frame_ : this.frame,
			_asgnWindow_ : true
		});
		
		this.addWindow(config);
		delete config._window_;
		
		return this;
	},
	
	addWizardWindow : function(config) {
		Ext.applyIf(config, {
			_frame_ : this.frame,
			_wizardWindow_ : true
		});
		
		this.addWindow(config);
		delete config._window_;
		
		return this;
	},

	/**
	 * Add a generic free-hand component.
	 */
	add : function(config) {
		Ext.applyIf(config, {
			id : Ext.id()
		});
		this.frame._elems_.add(config.name, config);
		return this;
	},

	/**
	 * Merge (add missing, leave existing unchanged) existing configuration of
	 * the specified element with the new configuration.
	 * 
	 */
	merge : function(name, config) {
		Ext.applyIf(this.frame._elems_.get(name), config);
		return this;
	},

	/**
	 * Change (overwrite) existing configuration of the specified element with
	 * the new configuration.
	 */
	change : function(name, config) {
		Ext.apply(this.frame._elems_.get(name), config);
		return this;
	},

	/**
	 * Remove the specified element from the list.
	 */
	remove : function(name) {
		this.frame._elems_.remove(name);
		return this;
	},

	
	addTabLoadRestriction : function(dcName,tabPanelName,relatedPanels){
		var dc = this.frame._getDc_(dcName);
		dc.addTabLoadRestriction(tabPanelName, relatedPanels);
		return this;
	},
	
	addContentLoadRestriction : function(dcName,containerPanelName){
		var dc = this.frame._getDc_(dcName);
		dc.addContentLoadRestriction(containerPanelName);
		return this;
	},
	
	addTabPanelForUserFields : function(config){
		// config should contain: tabPanelName,containerPanelName,dcName
		this.frame._userFieldsCfg_.add(config.tabPanelName, config);
		return this;
	},
	
	// private

	applyViewSharedConfig : function(config) {
		Ext.applyIf(config, {
			id : Ext.id(),
			itemId : config.name
		});
		this.frame._elems_.add(config.name, config);
	}
});