/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.dc.view.AbstractDcvGrid", {
	extend : "e4e.dc.view.AbstractDc_Grid",
	cls: "dnet-grid-panel",

	// **************** Properties *****************

	/**
	 * Component builder
	 */
	_builder_ : null,

	/**
	 * Helper property to identify this dc-view type as read-only grid
	 */
	_dcViewType_ : "grid",
	
	_menuContext_ : new Ext.menu.Menu({
	    items: [{
	        id: 'do-something',
	        text: 'Do something'
	    }],
	    listeners: {
	        itemclick: function() {
	            
	        }
	    }
	}),

	// **************** Public API *****************

	/**
	 * Returns the builder associated with this type of component. Each
	 * predefined data-control view type has its own builder. If it doesn't
	 * exist yet attempts to create it.
	 */
	_getBuilder_ : function() {
		if (this._builder_ == null) {
			this._builder_ = new e4e.dc.view.DcvGridBuilder({
				dcv : this
			});
		}
		return this._builder_;
	},

	// **************** Private methods *****************

	initComponent : function(config) {

		this._initDcGrid_();
		var cfg = this._createDefaultGridConfig_();
		
		Ext.apply(cfg, {
			
			// Dan: switching to spreadsheet model
			
			selModel : { 
				mode : "MULTI",
				listeners : {
					"select" : {
						scope : this,
						fn : function(sm, record) { // additional parameters: index, eOpts
							var ctrl = this._controller_;
							ctrl.selectRecord(record, {
								fromGrid : true,
								grid : this
							});
						}
					},

					"deselect" : {
						scope : this,
						fn : function(sm, record) { // additional parameters: index, eOpts
							var ctrl = this._controller_;
							ctrl.deSelectRecord(record, {
								fromGrid : true,
								grid : this
							});
						}
					},

					"beforedeselect" : {
						scope : this,
						fn : function(sm, record) { // additional parameters: index, eOpts
							if (record === this._controller_.record && 
									(!this._controller_.dcState.isRecordChangeAllowed()) ) {
								return false;
							}
						}
					}
				}
			},
			
			listeners : {
				"itemdblclick" : {
					scope : this,
					fn : function() {
						// possible parameters: view, model, item, idx, evnt, evntOpts
						if( this._controller_._frame_._initPhase2Done_ ){
							if( Ext.isFunction(this._controller_.dblClickActionHandler) ){
								this._controller_.dblClickActionHandler.call(this._controller_.dblClickActionScope); 
							} else {
								this._controller_.doEditIn();
							}
						}
					}
				}
				,"viewready" : function (grid) {
		            Ext.Array.each(grid.columns, function(column) {
		                if (column.renderer){
		                    column.renderer = Ext.Function.createSequence(column.renderer,function(val,meta){
		                        if(val && meta){
		                        	meta.tdAttr += 'data-qtip="' + Ext.String.htmlEncode(val) + '"';
		                        }
		                        return val;
		                    });
		                } else {
		                    column.renderer = function (val,meta){
		                        if(val && meta){
		                        	meta.tdAttr += 'data-qtip="' + Ext.String.htmlEncode(val) + '"'; 
		                        }
		                        return val;
		                    }
		                }
		            });
		            grid.getView().refresh();
		        }
			}
		});
		Ext.apply(this, cfg);
		this.callParent(arguments);
		this._registerListeners_();
		this._registerKeyBindings_();
		this._addExportButton_();
		this._addExternalReportsButton_();
		this._afterInitComponent_();
		Ext.defer(this._onController_selectionChange, 200, this, [{dc:this._controller_}]);
	},
	
	_filterByEmptyNotEmpty_ : function(menu, operation) {
		var col = menu.activeHeader;
        var grid = menu.up('grid');
        var field = col.dataIndex;
        var dc = grid._controller_;
        var af = [];
        var o = {
			"id":null,
			"fieldName":field,
			"operation":operation,
			"value1": ""
		};
		af.push(o);
		dc.advancedFilter = af;
		dc.doQuery();
	},
	
	/**
	 * Register event listeners
	 */
	_registerListeners_ : function() {
		var ctrl = this._controller_;
		var store = ctrl.store;

		// When edit-out requested, focus this grid view.
		// It's very likely that coming from an editor, the user wants to get to
		// the list.
		this.mon(ctrl, "onEditOut", this._gotoFirstNavigationItem_, this);
		this.mon(ctrl, "afterDoQuerySuccess", function(dc, ajaxResult) {
			var o = ajaxResult.options;
			if (!o || o.initiator !== "dcContext") {
				this._gotoFirstNavigationItem_();
			}
		}, this);

		this.mon(ctrl, "afterDoDeleteSuccess", function() {
			this._gotoFirstNavigationItem_();
		}, this);

		this.mon(ctrl, "selectionChange", this._onController_selectionChange, this);
		this.mon(store, "load", this._onStore_load_, this);
		this.mon(store, "write", function(){
			this._onStore_write_(store); // Dan: SONE-2098: Export feature from Tools is not working when the 1st record is added into a list
			var view = this.getView();
			if (!Ext.isEmpty(view.getTargetEl())) {
				view.refresh(); // SONE-1591 : in ExtJs 5.1.1.451 the grid view is not refreshed after save; extra refresh needed
			}
			
		}, this);
	},

	_registerKeyBindings_ : function() {
		return new Ext.util.KeyMap({
			target : this.view,
			eventName : 'itemkeydown',
			processEvent : function(view, record, node, index, event) {
				return event;
			},
			binding : [ Ext.apply(KeyBindings.values.dc.doEnterQuery, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doEnterQuery();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doClearQuery, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doClearQuery();
					this._controller_.doEnterQuery();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doQuery, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doQuery();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doNew, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doNew();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doCancel, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doCancel();
					this.view.focus();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doSave, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doSave();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doDelete, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doDelete();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doCopy, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doCopy();
					this._controller_.doEditIn();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.doEditIn, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doEditIn();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.nextPage, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doNextPage();
				},
				scope : this
			}), Ext.apply(KeyBindings.values.dc.prevPage, {
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doPrevPage();
				},
				scope : this
			}), { // In the context of a grid, allow to switch to editor with
				// a plain ENTER key also, not just the doEditIn key binding
				key : Ext.event.Event.ENTER,
				ctrl : false,
				shift : false,
				alt : false,
				fn : function(keyCode, e) {
					e.stopEvent();
					this._controller_.doEditIn();
				},
				scope : this
			} ]
		});
	}
});
