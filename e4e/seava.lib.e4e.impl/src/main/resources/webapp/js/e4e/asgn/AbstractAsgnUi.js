/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.asgn.AbstractAsgnUi", {
	extend : "Ext.window.Window",

	mixins : {
		elemBuilder : "e4e.base.Abstract_View"
	},

	// **************** Properties *****************

	/**
	 * Assignment controller
	 */
	_controller_ : null,

	/**
	 * Component builder
	 */
	_builder_ : null,

	/**
	 * Left grid (available records) id
	 */
	_leftGridId_ : null,

	/**
	 * Right grid (selected records) id
	 */
	_rightGridId_ : null,

	/**
	 * Configuration object for the window
	 */
	_windowConfig_ : null,

	_filterFields_ : null,
	_defaultFilterField_ : null,

	/**
	 * Flag to switch on/off auto-close mode of the assignment window upon
	 * succesful save.
	 */
	_autoCloseAfterSave_ : true,
	_autoCloseAfterCancel_ : true,

	/**
	 * Cancel button id.
	 */
	_btnCancelId_ : null,

	/**
	 * Save button id.
	 */
	_btnSaveId_ : null,

	/**
	 * Move left selection button id.
	 */
	_btnMoveLeftId_ : null,

	/**
	 * Move left all button id.
	 */
	_btnMoveLeftAllId_ : null,

	/**
	 * Move right selection button id.
	 */
	_btnMoveRightId_ : null,

	/**
	 * Move right all button id.
	 */
	_btnMoveRightAllId_ : null,
	
	// **************** Public API *****************

	/**
	 * Returns the builder.
	 */
	_getBuilder_ : function() {
		if (this._builder_ == null) {
			this._builder_ = new e4e.asgn.AsgnUiBuilder({
				asgnUi : this
			});
		}
		return this._builder_;
	},

	/**
	 * Execute query for the left list which shows the available elements.
	 */
	_doQueryLeft_ : function() {
		this._doQueryLeftImpl_();
	},

	_doQueryRight_ : function() {
		this._doQueryRightImpl_();
	},

	/**
	 * Get left grid instance
	 * 
	 * @return {e4e.asgn.AbstractAsgnGrid}
	 */
	_getLeftGrid_ : function() {
		return Ext.getCmp(this._leftGridId_);
	},

	/**
	 * Get right grid instance
	 * 
	 * @return {e4e.asgn.AbstractAsgnGrid}
	 */
	_getRightGrid_ : function() {
		return Ext.getCmp(this._rightGridId_);
	},

	/**
	 * Get move left button
	 * 
	 * @return {Ext.button.Button}
	 */
	_getBtnMoveLeft_ : function() {
		return Ext.getCmp(this._btnMoveLeftId_);
	},

	/**
	 * Get move left all button
	 * 
	 * @return {Ext.button.Button}
	 */
	_getBtnMoveLeftAll_ : function() {
		return Ext.getCmp(this._btnMoveLeftAllId_);
	},

	/**
	 * Get move right button
	 * 
	 * @return {Ext.button.Button}
	 */
	_getBtnMoveRight_ : function() {
		return Ext.getCmp(this._btnMoveRightId_);
	},

	/**
	 * Get move right all button
	 * 
	 * @return {Ext.button.Button}
	 */
	_getBtnMoveRightAll_ : function() {
		return Ext.getCmp(this._btnMoveRightAllId_);
	},

	/**
	 * Get save button
	 * 
	 * @return {Ext.button.Button}
	 */
	_getBtnSave_ : function() {
		return Ext.getCmp(this._btnSaveId_);
	},

	/**
	 * Get cancel button
	 * 
	 * @return {Ext.button.Button}
	 */
	_getBtnCancel_ : function() {
		return Ext.getCmp(this._btnCancelId_);
	},

	/**
	 * Apply state rules to enable/disable or show/hide components.
	 * 
	 */
	_applyStates_ : function() {
		if (this._beforeApplyStates_() !== false) {
			this._onApplyStates_();
		}
		this._afterApplyStates_();
	},

	/**
	 * Template method checked before applying states.
	 * 
	 * @return {Boolean}
	 */
	_beforeApplyStates_ : function() {
		return true;
	},

	/**
	 * Template method invoked after the state rules are applied.
	 */
	_afterApplyStates_ : function() {
	},

	/**
	 * Implement the state control logic in subclasses.
	 * 
	 */
	_onApplyStates_ : function() {

		var l = this._getLeftGrid_().getSelectionModel().getSelection().length;
		if (l === 0) {
			this._getBtnMoveRight_().disable();
		} else {
			this._getBtnMoveRight_().enable();
		}

		l = this._getRightGrid_().getSelectionModel().getSelection().length;
		if (l === 0) {
			this._getBtnMoveLeft_().disable();
		} else {
			this._getBtnMoveLeft_().enable();
		}
		
		var btn = this._getBtnSave_();
		if( btn ){
			btn.setDisabled(!this._controller_.isDirty());
		}
	},

	// **************** Defaults and overrides *****************

	layout : {
		type : "hbox",
		align : "stretch"
	},
	closable : true,
	closeAction : "hide",
	modal : true,
	constrain: true,
	
	initComponent : function() {

		if (this._controller_ == null) {
			this._controller_ = this.$className.replace("$Ui", "");
		}
		if (!Ext.ClassManager.isCreated(this._controller_)) {
			Ext.define(this._controller_, {
				extend : "e4e.asgn.AbstractAsgn"
			});
		}

		this._elems_ = new Ext.util.MixedCollection();
		this._tlbs_ = new Ext.util.MixedCollection();
		this._tlbitms_ = new Ext.util.MixedCollection();
		this._controller_ = Ext.create(this._controller_, {});
		this._leftGridId_ = Ext.id()
		this._rightGridId_ = Ext.id()

		if (Ext.isArray(this._filterFields_)) {
			for ( var i = 0, l = this._filterFields_.length; i < l; i++) {
				var e = this._filterFields_[i];
				e[1] = Main.translateModelField(null, e[0]);
			}
		}

		this._startDefine_();
		this._defineDefaultElements_();

		if (this._beforeDefineElements_() !== false) {
			this._defineElements_();
		}
		this._afterDefineElements_();

		if (this._beforeLinkElements_() !== false) {
			this._linkElements_();
		}
		this._afterLinkElements_();

		this._endDefine_();

		this.items = [
				{
					flex : 10,
					xtype : "container",
					padding : 6,
					layout : {
						type : "vbox",
						align : "stretch"
					},
					items : [ this._elems_.get("leftFilter"),
							this._elems_.get("leftList") ]
				}, {
					width : 40,
					xtype : "container",
					layout : {
						type : "vbox",
						align : "stretch",
						pack : "center"
					},
					items : this._buildToolbarItems_()
				}, {
					flex : 10,
					xtype : "container",
					padding : 6,
					layout : {
						type : "vbox",
						align : "stretch"
					},
					items : [ this._elems_.get("rightFilter"),
							this._elems_.get("rightList") ]
				} 
		];

		this.dockedItems = [{
			xtype : "toolbar", ui:"footer", dock:"bottom", weight:-1,
			items : [ {
				xtype : "button",
				text : Main.translate("asgn", "save__lbl"),
				tooltip : Main.translate("asgn", "save__tlp"),
				id : this._btnSaveId_,
				glyph : "xf00c@FontAwesome",
				iconCls : "glyph-green",
				handler : this._onBtnAssignSave_,
				scope : this
			}, {
				xtype : "button",
				text : Main.translate("asgn", "cancel__lbl"),
				tooltip : Main.translate("asgn", "cancel__tlp"),
				id : this._btnCancelId_,
				glyph : "xf05e@FontAwesome",
				iconCls : "glyph-red",
				handler : this._onBtnAssignCancel_,
				scope : this
			} ]
		}];

		this.callParent(arguments);
		this._registerListeners_();
	},

	_onBtnAssignSave_ : function(){
		this._controller_.doSave();
	},
	
	_onBtnAssignCancel_ : function(){
		if(this._autoCloseAfterCancel_){
			this.close();
		} else {
			this._controller_.doReset();
		}
	},
	
	/**
	 * Register event listeners
	 */
	_registerListeners_ : function() {
		this.mon(this._controller_, "dirtyChanged", this._applyStates_, this);
		if (this._autoCloseAfterSave_ === true) {
			this._controller_.on("afterDoSaveSuccess", function() {
				this.close();
			}, this);
		}
		var _lg = this._getLeftGrid_();
		if (_lg) {
			this.mon(_lg, "itemdblclick", this._onLeftGrid_dblclick_, this);
			this.mon(_lg.getSelectionModel(), "selectionchange", this._applyStates_, this);
		}
		var _rg = this._getRightGrid_();
		if (_rg) {
			this.mon(_rg, "itemdblclick", this._onRightGrid_dblclick_, this);
			this.mon(_rg.getSelectionModel(), "selectionchange", this._applyStates_, this);
		}
		this.mon(this._controller_.storeLeft, "load", function(store) {
			if (store.totalCount === 0) {
				this._getBtnMoveRightAll_().disable();
			} else {
				this._getBtnMoveRightAll_().enable();
			}
		}, this);
		this.mon(this._controller_.storeRight, "load", function(store) {
			if (store.totalCount === 0) {
				this._getBtnMoveLeftAll_().disable();
			} else {
				this._getBtnMoveLeftAll_().enable();
			}
		}, this);
		this.on("close", function() {
			this._controller_.doCleanup();
		}, this);
		this.on("beforeclose", function(){ 
			return this._onWindowBeforeHide_(); 
		},this);
		this.on("beforehide", function(){ 
			return this._onWindowBeforeHide_(); 
		},this);
	},

	// **************** Private API *****************

	/**
	 * When double-click a row automatically move it.
	 */
	_onLeftGrid_dblclick_ : function() {
		var btn = this._getBtnMoveRight_();
		if (btn && !btn.disabled) {
			btn.handler.call(btn.scope);
		}
	},

	/**
	 * When double-click a row automatically move it.
	 */
	_onRightGrid_dblclick_ : function() {
		var btn = this._getBtnMoveLeft_();
		if (btn && !btn.disabled) {
			btn.handler.call(btn.scope);
		}
	},
	
	
	_getAdvFilter_ : function(val, grid){
		var af = [];
        if (val && val !== "*") {
			for(var i=0;i<grid.columns.length;i++){
				if (grid.columns[i].hidden === false) {
					var fName = grid.columns[i].dataIndex;
					for(var j=0;j<this._filterFields_.length;j++){
						if( this._filterFields_[j][0] === fName ){
							var operation ="like";
							var value1 = "%"+val+"%";
							var o = {
								"id":null,
								"fieldName":fName,
								"operation":operation,
								"value1": value1,
								"groupOp":"OR1"
							};
							af.push(o);
							break;
						}
					}
				}
			}
        }
        return af;
	},
	
	_doFilterLeft_ : function(val){
		this._doFilterLeftImpl_(val);
	},
	
	_doFilterLeftImpl_ : function(val){
        var grid = this._getLeftGrid_();
        this._controller_.leftAdvUIFilter = this._getAdvFilter_(val, grid);
		this._controller_.doQueryLeft();
	},
	
	_doFilterRight_ : function(val){
		this._doFilterRightImpl_(val);
	},
	
	_doFilterRightImpl_ : function(val){
        var grid = this._getRightGrid_();
        this._controller_.rightAdvUIFilter = this._getAdvFilter_(val, grid);
		this._controller_.doQueryRight();
	},
	
	/**
	 * Execute query for the left list which shows the available elements.
	 * Private implementation code.
	 */
	_doQueryLeftImpl_ : function() {
		this._controller_.doQueryLeft();
	},

	/**
	 * Execute query for the right list which shows the selected elements.
	 * Private implementation code.
	 */
	_doQueryRightImpl_ : function() {
		this._controller_.doQueryRight();
	},

	/**
	 * Define the default filter elements for the assignment window.
	 */
	_defineDefaultElements_ : function() {

		this._elems_.add("leftNewFilterField", {
			xtype : "textfield",
			flex : 100,
			emptyText : Main.translate("asgn", "filter__lbl") + "...",
			id : Ext.id(),
			cls : "sone-refresh",
			listeners : {
				change : {
					scope : this,
					buffer : 1000,
					fn : function(field, newVal) {
						this._doFilterLeft_(newVal);
					}
				}
			}
		});
		this._elems_.add("rightNewFilterField", {
			xtype : "textfield",
			flex : 100,
			emptyText : Main.translate("asgn", "filter__lbl") + "...",
			id : Ext.id(),
			cls : "sone-refresh",
			listeners : {
				change : {
					scope : this,
					buffer : 1000,
					fn : function(field, newVal) {
						this._doFilterRight_(newVal);
					}
				}
			}
		});

		this._elems_.add("leftFilter", {
			xtype : "fieldcontainer",
			layout : 'hbox',
			preventMark : true,
			items : [ this._elems_.get("leftNewFilterField") ]
		});

		this._elems_.add("rightFilter", {
			xtype : "fieldcontainer",
			layout : 'hbox',
			preventMark : true,
			items : [ this._elems_.get("rightNewFilterField") ]
		});
	},

	/**
	 * Define the buttons for the assignment windows. Selection buttons, save
	 * and initialize.
	 */
	_buildToolbarItems_ : function() {
		this._btnCancelId_ = Ext.id();
		this._btnSaveId_ = Ext.id();
		this._btnMoveLeftId_ = Ext.id();
		this._btnMoveLeftAllId_ = Ext.id();
		this._btnMoveRightId_ = Ext.id();
		this._btnMoveRightAllId_ = Ext.id();
		return [
				{
					xtype : "tbspacer",
					height : 25
				},
				{
					xtype : "button",
					glyph: "xf105@FontAwesome",
					cls: "sone-flat-button",
					tooltip : Main.translate("asgn", "move_right__tlp"),					
					id : this._btnMoveRightId_,
					disabled : true,
					scope : this,
					handler : function() {
						this._controller_.doMoveRight(Ext
								.getCmp(this._leftGridId_), Ext
								.getCmp(this._rightGridId_));
					}
				},
				{
					xtype : "tbspacer",
					height : 5
				},
				{
					xtype : "button",
					glyph: "xf104@FontAwesome",
					cls: "sone-flat-button",
					tooltip : Main.translate("asgn", "move_left__tlp"),
					id : this._btnMoveLeftId_,
					disabled : true,
					scope : this,
					handler : function() {
						this._controller_.doMoveLeft(Ext
								.getCmp(this._leftGridId_), Ext
								.getCmp(this._rightGridId_));
					}
				}, {
					xtype : "tbspacer",
					height : 25
				}, {
					xtype : "button",
					glyph: "xf101@FontAwesome",
					cls: "sone-flat-button",
					tooltip : Main.translate("asgn", "move_right_all__tlp"),
					id : this._btnMoveRightAllId_,
					disabled : true,
					scope : this,
					handler : function() {
						this._getBtnMoveRightAll_().disable();
						this._controller_.doMoveRightAll();
					}
				}, {
					xtype : "tbspacer",
					height : 5
				}, {
					xtype : "button",
					glyph: "xf100@FontAwesome",
					cls: "sone-flat-button",
					tooltip : Main.translate("asgn", "move_left_all__tlp"),
					id : this._btnMoveLeftAllId_,
					disabled : true,
					scope : this,
					handler : function() {
						this._getBtnMoveLeftAll_().disable();
						this._controller_.doMoveLeftAll();
					}
				}, {
					xtype : "tbspacer",
					height : 25
				} 
		];
	},
	
	
	_onWindowBeforeHide_ : function(){
		if( this._controller_.isDirty() ){
			Ext.Msg.show({
				title : Main.translate("asgn", "unsaved_title"), 
				msg : Main.translate("asgn", "unsaved_msg"), 
				buttons : Ext.MessageBox.YES + Ext.MessageBox.NO + Ext.MessageBox.CANCEL,
				buttonText : {
					yes : Main.translate("asgn", "save__lbl"), 
					no : Main.translate("asgn", "dontsave__lbl") 
				},
				scope : this,
				fn : function(btnId) {
					if (btnId === "yes") {
						var btn = this._getBtnSave_();
						if (btn && !btn.disabled) {
							btn.handler.call(btn.scope);
						}
					} else {
						if (btnId === "no") {
							this._controller_.cleanDirty();
							this.hide();
						}
					}
				},
			    icon: Ext.MessageBox.WARNING
			});
			return false;
		}
		return true;
	}

});
