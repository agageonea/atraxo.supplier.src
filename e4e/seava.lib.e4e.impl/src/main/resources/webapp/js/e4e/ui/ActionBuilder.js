/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.ui.ActionBuilder", {

	/**
	 * Frame instance
	 */
	frame : null,

	/**
	 * Toolbar name
	 */
	name : null,

	/**
	 * Default bound data-control.
	 */
	dc : null,
	dcInst : null,

	/**
	 * Index to count the separators.
	 */
	sepIdx : null,

	constructor : function(config) {
		config = config || {};
		Ext.apply(this, config);
		this.callParent(arguments);
	},

	setup : function() {
	},

	addSeparator : function(config) {
		if (this.sepIdx == null) {
			this.sepIdx = 0;
		}
		var cfg = config || {};
		// Dan: removed the toolbar separator
		/*
		//		this.frame._tlbitms_.add(this.name + "___S" + (this.sepIdx++) + "_", "-");
		*/
		if( cfg._btnContainerName_ ){
			cfg.xtype = "menuseparator";
			this._addInContainer_(cfg);
		}
		return this;
	},

	/**
	 * Add button to a container
	 */
	_addInContainer_ : function(config, action){
		var itemConfig = this.frame._getToolbarItemConfig_(this.name+"__"+config._btnContainerName_);
		if( itemConfig ){
			if( config._isDefaultHandler_+""==="true" ){
				if(action){
					action.initialConfig._isDefaultHandler_ = true;
					action.initialConfig._isAction_ = true;
					action.initialConfig.text = "<b>" + action.initialConfig.text + "</b>"; 
				} else {
					config.text = "<b>" + config.text + "</b>";
				}
			}
			if(action){
				itemConfig.menu.add(action);
			} else {
				itemConfig.menu.add(config);
			}
		}
	},
	
	/**
	 * Add a generic button.
	 */
	addButton : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			id : Ext.id(),
			xtype : "button"
		});
		if( cfg._btnContainerName_ ){
			delete cfg.xtype;
			this._addInContainer_(cfg);
		} else {
			if( ""+cfg.isBtnContainer === "true" ){
				cfg.handler = function(obj,evnt) {
					if( Ext.isFunction(obj._checkMenuHandler_) ){
						obj._checkMenuHandler_(this,obj,evnt,true);
					}
		        };
				cfg.menu = new Ext.menu.Menu({
					items: []
				});
			}
			this.frame._tlbitms_.add(this.name + "__" + cfg.name, cfg);
		}
		return this;
	},

	/**
	 * Add generic buttons.
	 */
	addButtons : function(btns) {
		if (Ext.isArray(btns)) {
			for (var i = 0; i < btns.length; i++) {
				this.addButton(btns[i]);
			}
		}
		return this;
	},

	/**
	 * Add a label element
	 */
	addLabel : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc,
			xtype : "label",
			cls : "dnet-toolbar-label"
		});
		this.frame._tlbitms_.add(this.name + "__" + cfg.name, cfg);
		return this;
	},

	/**
	 * Add title for toolbar.
	 */
	addTitle : function(config) {
		var cfg = config || {};
		var ttlKey = this.name + "__ttl";
		Ext.applyIf(cfg, {
			dc : this.dc,
			xtype : "label",
			name : "title",
			height : 20,
			text : this.frame.translate(ttlKey),
			cls : "dnet-toolbar-title"
		});
		this.frame._tlbitms_.add(this.name + "__" + cfg.name, cfg);
		if (this.frame._tlbtitles_ == null) {
			this.frame._tlbtitles_ = new Ext.util.MixedCollection();
		}
		this.frame._tlbtitles_.add(this.name, cfg.text);
		return this;
	},

	/**
	 * Add do-query action.
	 */
	addQuery : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc
		});
		var a = this.frame._getDc_(cfg.dc).actions.doQuery;
		this._applyClassConfig_(a,cfg);
		this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		return this;
	},

	/**
	 * Add clear-query action.
	 */
	addClearQuery : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc
		});
		var a = this.frame._getDc_(cfg.dc).actions.doClearQuery;
		this._applyClassConfig_(a,cfg);
		this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		return this;
	},

	/**
	 * Add clear-query action.
	 */
	addEnterQuery : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc
		});
		var a = this.frame._getDc_(cfg.dc).actions.doEnterQuery;
		this._applyClassConfig_(a,cfg);
		this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		return this;
	},

	/**
	 * Add reload-page action.
	 */
	addReloadPage : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc
		});
		var a = this.frame._getDc_(cfg.dc).actions.doReloadPage;
		this._applyClassConfig_(a,cfg);
		this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		return this;
	},

	/**
	 * Add reload-page action.
	 */
	addReloadRec : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc
		});
		var a = this.frame._getDc_(cfg.dc).actions.doReloadRec;
		this._applyClassConfig_(a,cfg);
		this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		return this;
	},

	/**
	 * Add next record action
	 */
	addNextRec : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc
		});
		var a = this.frame._getDc_(cfg.dc).actions.doNextRec;
		this._applyClassConfig_(a,cfg);
		this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a); // new
		return this;
	},

	/**
	 * Add previous record action
	 */
	addPrevRec : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc
		});
		var a = this.frame._getDc_(cfg.dc).actions.doPrevRec;
		this._applyClassConfig_(a,cfg);
		this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a); // new
		return this;
	},

	/**
	 * Add cancel changes action
	 */
	addCancel : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc,
			tlb : this.name,
			autoBack : true
		});
		var a = this.frame._getDc_(cfg.dc).actions.doCancel;
		this._applyClassConfig_(a,cfg);
		if( cfg._btnContainerName_ ){
			this._addInContainer_(cfg, a);
		} else {
			this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		}
		if (cfg.autoBack) {
			this.frame.mon(this.frame._getDc_(cfg.dc), 'recordChanged',
					function(event) {
						if (event.record == null) {
							this._invokeTlbItem_("doEditOut", cfg.tlb);
						}
					}, this.frame);
		}
		return this;
	},

	/**
	 * Add cancel changes on selected records action
	 */
	addCancelSelected : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc,
			tlb : this.name,
			autoBack : true
		});
		var a = this.frame._getDc_(cfg.dc).actions.doCancelSelected;
		this._applyClassConfig_(a,cfg);
		if( cfg._btnContainerName_ ){
			this._addInContainer_(cfg, a);
		} else {
			this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		}
		if (cfg.autoBack) {
			this.frame.mon(this.frame._getDc_(cfg.dc), 'recordChanged',
					function(event) {
						if (event.record == null) {
							this._invokeTlbItem_("doEditOut", cfg.tlb);
						}
					}, this.frame);
		}
		return this;
	},
	
	/**
	 * Add delete selected records action.
	 */
	addDelete : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc
		});
		var a = this.frame._getDc_(cfg.dc).actions.doDelete;
		this._applyClassConfig_(a,cfg);
		if( cfg._btnContainerName_ ){
			this._addInContainer_(cfg, a);
		} else {
			this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		}
		return this;
	},

	/**
	 * Add save action.
	 * 
	 * @param {}
	 *            config
	 * @return {}
	 */
	addSave : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc
		});
		var a = this.frame._getDc_(cfg.dc).actions.doSave;
		this._applyClassConfig_(a,cfg);
		if( cfg._btnContainerName_ ){
			this._addInContainer_(cfg, a);
		} else {
			this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		}
		return this;
	},

	/**
	 * Add save action.
	 * 
	 * @param {}
	 *            config
	 * @return {}
	 */
	addSaveInChild : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc
		});
		var a = this.frame._getDc_(cfg.dc).actions.doSaveInChild;
		this._applyClassConfig_(a,cfg);
		if( cfg._btnContainerName_ ){
			this._addInContainer_(cfg, a);
		} else {
			this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		}
		return this;
	},
	
	/**
	 * Default dbl-click handler set - for buttons
	 */
	addDefaultDblClickBtnAction : function(btnName){
		var btnCfg = this.frame._elems_.get(btnName);
		if( btnCfg ){
			btnCfg.defaultDblClickAction = true;
		}
		return this;
	},
	
	/**
	 * Default dbl-click handler set - for framework actions
	 */
	addDefaultDblClickAction : function(actName){
		if( this.dcInst ){
			this.dcInst.dblClickActionHandler = this.dcInst[actName];
			this.dcInst.dblClickActionScope = this.dcInst;
		}	
		return this;
	},
	
	/**
	 * End toolbar creation.
	 * 
	 * @return {}
	 */
	end : function() {
		var n = this.name, t = this.frame._tlbitms_.filterBy(function(o, k) {
			return (k.indexOf(n + "__") !== -1);
		}), tarray = t.getRange();

		// defaultDblClickAction - set
		var e;
		var i, cnt=tarray.length;
		for(i=0;i<cnt;i++){
			if( tarray[i].defaultDblClickAction === true ){
				if( this.dcInst ){
					e = tarray[i];
					this.dcInst.dblClickActionHandler = e.handler;
					this.dcInst.dblClickActionScope = e.scope || e;
				}
				break;
			}
		}
		// create a collector menu for buttons if the buttons number is too high
		var maxNo = Main.MAX_BUTTONS_ON_TOOLBARS;
		if( cnt > maxNo ){
			// count only the visible buttons
			var k = 0, idx = 0;
			while( idx<cnt && k<maxNo ){
				if( tarray[idx].visible !== false ){
					k++;
				}
				idx++;
			}
			if( idx<cnt ){
				var menuBtnId = Ext.id();
				var btn = {
					 name : "btnMenu" // do not change the name, because it's reffered it from the _addExportButton_ function from AbstractDcvGrid
					//,iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-menu-btn-more" : null  
					,glyph : 'xf141@FontAwesome'
					,arrowCls: ''
					,disabled : false
					,isBtnContainer : true
					,scope : this
					,handler : function(obj,evnt) {
			            obj._checkMenuHandler_(this,obj,evnt,true);
			        }
					,menu : new Ext.menu.Menu({
						items: []
					})
					,id : menuBtnId
				};
				for(i=idx;i<tarray.length;i++){
					e = tarray[i];
					delete e.xtype;
					if( e.visible !== false ){
						btn.menu.add(e);
						e._menuBtnId_ = menuBtnId;
					}
				}
				tarray.splice(idx,tarray.length-idx);
				tarray.push(btn);
			}
		}
		var tbar = {
			_actions_ : tarray,
			_view_ : null
		};
		this.frame._tlbs_.add(this.name, tbar);
		return this.frame._getBuilder_();
	},

	/**
	 * Add edit action
	 * 
	 * @param {}
	 *            config
	 * @return {}
	 */
	addEdit : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc,
			tlb : this.name,
			scope : this.frame
		});
		if (!cfg.fn) {
			cfg.fn = function() {
				var ct = (cfg.inContainer) ? this._getElement_(cfg.inContainer)	: this._getElement_("main");
				var layout = ct.getLayout();
				var activate = null;
				if (cfg.showView) {
					activate = this._get_(cfg.showView);
					if (!activate) {
						activate = this._getElementConfig_(cfg.showView).id;
					}
				}
				if( layout && !Ext.isEmpty(activate) && Ext.isFunction(layout.setActiveItem) ){
					layout.setActiveItem(activate);
				}
			};
		}

		var a = this.frame._getDc_(cfg.dc).actions.doEditIn;
		this._applyClassConfig_(a,cfg);
		if( cfg._btnContainerName_ ){
			this._addInContainer_(cfg, a);
		} else {
			this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		}
		this.frame.mon(this.frame._getDc_(cfg.dc), "onEditIn", cfg.fn, cfg.scope);
		return this;
	},

	/**
	 * Add back button. Move to a different canvas/stacked view.
	 */
	addBack : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc,
			tlb : this.name,
			scope : this.frame
		});

		if (!cfg.fn) {
			cfg.fn = function() {
				var idx;
				var ct = (cfg.inContainer) ? this._getElement_(cfg.inContainer)	: this._getElement_("main");
				if (cfg.showView) {
					idx = cfg.showView;
					var cmp = this._get_(cfg.showView);
					if (cmp) {
						ct.getLayout().setActiveItem(cmp);
					} else {
						cmp = this._getElementConfig_(cfg.showView).id;
						ct.getLayout().setActiveItem(cmp);
					}
				} else {
					idx = 0;
					ct.getLayout().setActiveItem(0);
				}
				this._afterShowStackedViewElement_(ct.name, idx);
			}
		}

		var a = this.frame._getDc_(cfg.dc).actions.doEditOut;
		this._applyClassConfig_(a,cfg);
		if( cfg._btnContainerName_ ){
			this._addInContainer_(cfg, a);
		} else {
			this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		}
		this.frame.mon(this.frame._getDc_(cfg.dc), "onEditOut", cfg.fn,	cfg.scope);
		return this;
	},
	
	/**
	 * Add close button. Move to a different canvas/stacked view.
	 * Same function with back, but no caption displayed, only the close icon: X
	 */
	addClose : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc,
			tlb : this.name,
			scope : this.frame
		});

		if (!cfg.fn) {
			cfg.fn = function() {
				var idx;
				var ct = (cfg.inContainer) ? this._getElement_(cfg.inContainer)	: this._getElement_("main");
				if (cfg.showView) {
					idx = cfg.showView;
					var cmp = this._get_(cfg.showView);
					if (cmp) {
						ct.getLayout().setActiveItem(cmp);
					} else {
						cmp = this._getElementConfig_(cfg.showView).id;
						ct.getLayout().setActiveItem(cmp);
					}
				} else {
					idx = 0;
					ct.getLayout().setActiveItem(0);
				}
				this._afterShowStackedViewElement_(ct.name, idx);
			}
		}

		var a = this.frame._getDc_(cfg.dc).actions.doClose;
		this._applyClassConfig_(a,cfg);
		if( cfg._btnContainerName_ ){
			this._addInContainer_(cfg, a);
		} else {
			this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		}
		this.frame.mon(this.frame._getDc_(cfg.dc), "onEditOut", cfg.fn,	cfg.scope);
		return this;
	},
	
	/**
	 * Add a item to the toolbar for align to right the next items. 
	 */
	addRightAlign : function(){
		this.frame._tlbitms_.add(this.name + "__rightAlign_" + Ext.id(), { xtype: 'tbfill' });
		return this;
	},

	/**
	 * Add action for auto-load. When toggled, the child data is automatically
	 * loaded on parent change.
	 */
	addAutoLoad : function() {
		var dc = this.frame._getDc_(this.dc);
		
		var cfg = {
			name : "autoLoad",
			disabled : false,
			enableToggle : true,
			text : Main.translate("tlbitem", "autoload__lbl"),
			tooltip : Main.translate("tlbitem", "autoload__tlp"),
			scope : dc,
			handler : function(btn) {
				dc.dcContext.relation.fetchMode = (btn.pressed) ? "auto" : "manual";
			},
			dc : dc
		};

		if (dc.dcContext.relation.fetchMode === "auto"
				|| dc.dcContext.relation.fetchMode === undefined || dc.dcContext.relation.fetchMode === null) {
			cfg.pressed = true;
		}

		var a = new Ext.Action(cfg);
		this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		return this;
	},

	/**
	 * Add reports contributed by the extension providers.
	 */
	addReports : function() {
		if (this.frame._reports_ != null) {
			var r = [], rc = this.frame._reports_;
			for (var i = 0, l = rc.length; i < l; i++) {
				if (rc[i].toolbar === this.name) {					
					var rcfg = Ext.apply({
						scope : this.frame,
						text : rc[i].title,
						handler : this.frame.runReport
					}, rc[i]);

					r.push(rcfg);
				}
			}
			if (r.length > 0) {
				this.addSeparator();
				this.frame._tlbitms_.add(this.name + "___REPS_", {
					text : "Reports",
					menu : r
				});

			}
		}
		return this;
	},

	/**
	 * Add create new record button
	 * 
	 * @param {}
	 *            config
	 * @return {}
	 */
	addNew : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc,
			tlb : this.name,
			autoEdit : true
		});

		var a = this.frame._getDc_(cfg.dc).actions.doNew;
		this._applyClassConfig_(a,cfg);
		
		if( cfg._btnContainerName_ ){
			this._addInContainer_(cfg, a);
		} else {
			this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		}

		if (cfg.autoEdit+"" !== "false") {
			this.frame.mon(this.frame._getDc_(cfg.dc), "afterDoNew", function() {
				this._invokeTlbItem_("doEditIn", cfg.tlb);
			}, this.frame);

		} else {
			if (!Ext.isEmpty(cfg.showView)) {
				var fn = this.createHandler_New(cfg);
				this.frame.mon(this.frame._getDc_(cfg.dc), "afterDoNew", fn, this.frame);
			}
		}
		return this;
	},

	/**
	 * Create the handler function for the New action
	 */
	createHandler_New : function(cfg) {
		return function() {

			var ct;

			if (cfg.inContainer) {
				ct = this._getElement_(cfg.inContainer);
			} else {
				ct = this._getElement_("main");
			}

			if (cfg.showView) {
				var cmp = this._get_(cfg.showView);
				if (cmp) {
					ct.getLayout().setActiveItem(cmp);
				} else {
					var _id = this._getElementConfig_(cfg.showView).id;
					ct.getLayout().setActiveItem(_id);
				}
			} else {
				ct.getLayout().setActiveItem(1);
			}

		};
	},

	/**
	 * Add create new record button - for child
	 * 
	 * @param {}
	 *            config
	 * @return {}
	 */
	addNewInChild : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc,
			tlb : this.name,
			autoEdit : true
		});

		var a = this.frame._getDc_(cfg.dc).actions.doNewInChild;
		this._applyClassConfig_(a,cfg);
		
		if( cfg._btnContainerName_ ){
			this._addInContainer_(cfg, a);
		} else {
			this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		}

		if (cfg.autoEdit+"" !== "false") {
			this.frame.mon(this.frame._getDc_(cfg.dc), "afterDoNewInChild",
					function() {
						this._invokeTlbItem_("doEditIn", cfg.tlb);
					}, this.frame);
		} else {
			if (!Ext.isEmpty(cfg.showView)) {
				var fn = this.createHandler_NewInChild(cfg);
				this.frame.mon(this.frame._getDc_(cfg.dc), "afterDoNewInChild", fn, this.frame);
			}
		}
		return this;
	},

	/**
	 * Create the handler function for the NewInChild action
	 */
	createHandler_NewInChild : function(cfg) {
		return function() {

			var ct;

			if (cfg.inContainer) {
				ct = this._getElement_(cfg.inContainer);
			} else {
				ct = this._getElement_("main");
			}

			if (cfg.showView) {
				var cmp = this._get_(cfg.showView);
				if (cmp) {
					ct.getLayout().setActiveItem(cmp);
				} else {
					var _id = this._getElementConfig_(cfg.showView).id;
					ct.getLayout().setActiveItem(_id);
				}
			} else {
				ct.getLayout().setActiveItem(1);
			}

		};
	},
	
	/**
	 * Add copy current record button
	 * 
	 * @param {}
	 *            config
	 * @return {}
	 */
	addCopy : function(config) {
		var cfg = config || {};
		Ext.applyIf(cfg, {
			dc : this.dc,
			tlb : this.name,
			autoEdit : true
		});
		var a = this.frame._getDc_(cfg.dc).actions.doCopy;
		this._applyClassConfig_(a,cfg);
		if( cfg._btnContainerName_ ){
			this._addInContainer_(cfg, a);
		} else {
			this.frame._tlbitms_.add(this.name + "__" + a.initialConfig.name, a);
		}
		if (cfg.autoEdit !== "false") {
			this.frame.mon(this.frame._getDc_(cfg.dc), "afterDoNew",
					function() {
						this._invokeTlbItem_("doEditIn", cfg.tlb);
					}, this.frame);
		} else {
			if (!Ext.isEmpty(cfg.showView)) {
				var fn = this.createHandler_Copy(cfg);
				this.frame.mon(this.frame._getDc_(cfg.dc), "afterDoNew", fn,
						this.frame);
			}
		}
		return this;
	},

	/**
	 * Create the handler function for the Copy action
	 */
	createHandler_Copy : function(cfg) {
		return function() {
			var ct = (cfg.inContainer) ? this._getElement_(cfg.inContainer)
					: this._getElement_("main");
			if (cfg.showView) {
				var cmp = this._get_(cfg.showView);
				if (cmp) {
					ct.getLayout().setActiveItem(cmp);
				} else {
					ct.getLayout().setActiveItem(
							this._getElementConfig_(cfg.showView).id);
				}
			} else {
				ct.getLayout().setActiveItem(1);
			}
		};
	},


	// custom function state control (customStateManager)
	
	setStateFn_Query : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoQuery = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_ClearQuery : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoClearQuery = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_EnterQuery : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoEnterQuery = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_New : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoNew = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_NewInChild : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoNewInChild = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_Copy : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoCopy = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_Save : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoSave = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_SaveInChild : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoSaveInChild = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_Delete : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoDelete = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_Cancel : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoCancel = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_CancelOnFields : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoCancelOnFields = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_CancelSelected : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoCancelSelected = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_Edit : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoEditIn = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_Back : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoEditOut = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_Close : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoEditOut = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_PrevRec : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoPrevRec = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_NextRec : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoNextRec = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_ReloadRec : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoReloadRec = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	setStateFn_ReloadPage : function(fn){
		if( this.dcInst ){
			var fnState = fn;
			this.dcInst.canDoReloadPage = function(dc){
				return fnState(dc);
			}
		}
		return this;
	},
	endStateFnSet : function(){
		return this.frame._getBuilder_();
	},

	// helper functions

	_applyClassConfig_ : function(action,cfg) {
		if( cfg.glyph ){
			action.initialConfig.glyph = cfg.glyph;
			action.initialConfig.iconCls = null; // if glyph is added, do not use default icon
		}
		if( cfg.iconCls ){
			action.initialConfig.iconCls = cfg.iconCls;
		}
	}
	
});
