/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.dc.AbstractDc", {

	mixins : {
		observable : 'Ext.util.Observable'
	},

	dsName : "",
	
	/**
	 * In which frame is used. (Instance of AbstractUi.)
	 */
	_frame_: null,
	
	/**
	 * The instance name in the frame.
	 */
	_instanceKey_: null,

	/**
	 * Collection of flags which describe the dc-state.
	 */
	dcState : null,

	/**
	 * List with action names implemented
	 */
	actionNames : null,

	/**
	 * Action instances. Used to create control widgets in UI to be triggered by
	 * user, mainly toolbar items.
	 */
	actions : null,

	/**
	 * Executable functions. Implements workers for API methods of a
	 * data-control.
	 */
	commands : null,

	/**
	 * Allow to edit more than one record at once.
	 */
	multiEdit : false,

	trackEditMode : false,

	isEditMode : false,
	
	_editTriggered_ : null,

	/**
	 * Filter model instance.
	 * 
	 * @type Ext.data.Model
	 */
	filter : null,

	advancedFilter : null,

	/**
	 * Data model instance.
	 * 
	 * @type Ext.data.Model
	 */
	record : null,

	/**
	 * Parameters model instance
	 */
	params : null,
	
	/**
	 * Parameter values can be override by user (saved values in view)
	 * In this case, need to keep the preferred values.
	 */
	forcedParams : null,

	/**
	 * Data model signature - record constructor.
	 * 
	 * @type Ext.data.Model
	 */
	recordModel : null,

	/**
	 * Filter model signature - filter constructor. Defaults to recordModel if
	 * not specified.
	 * 
	 * @type Ext.data.Model
	 */
	filterModel : null,

	/**
	 * Parameters model signature - record constructor.
	 */
	paramModel : null,

	/**
	 * Keep track of the selected records
	 */
	selectedRecords : null,

	/**
	 * Various runtime configuration properties.
	 */
	tuning : {
		/**
		 * Timeout of Ajax call in store's proxy Default 15 minutes
		 */
		storeTimeout : Main.ajaxTimeout,

		/**
		 * Timeout of Ajax call in RPC commands Default 15 minutes
		 */
		rpcTimeout : Main.ajaxTimeout,

		/**
		 * Page-size for a query
		 */
		fetchSize : Main.viewConfig.FETCH_SIZE
	},

	/**
	 * Children data-controls, similar to a `HasMany` association.
	 */
	children : null,

	/**
	 * Parent data-control, similar to a `BelongsTo` association.
	 * 
	 * @type e4e.dc.AbstractDc
	 */
	parent : null,

	/**
	 * Should apply a default selection on store load ?
	 */
	afterStoreLoadDoDefaultSelection : true,

	/**
	 * Local reference to the data-source store.
	 * 
	 * @type Ext.data.Store
	 */
	store : null,
	_storeIsLoading_ : false,

	/**
	 * 
	 * @type e4e.dc.DcContext
	 */
	dcContext : null,

	/**
	 * Execution flow related context. Stores filter/params/record contextual
	 * values.
	 */
	flowContext : null,

	readOnly : false,

	/**
	 * disabled: similar functionality with readOnly, but used by state-manager functions
	 */
	disabled : false, 

	/**
	 * disabled_rules: object which contain the info about which operation is disabled in the current user context
	 * 	N : new
	 *  E : edit
	 *  S : save
	 *  D : delete
	 */
	disabledRules : null,
	
	/**
	 * ignore parent state in a parent-child relation 
	 */
	parentCheckDisabled : false,
	
	_trl_ : null,

	/**
	 * last error message object created by _isValid_ function call
	 */
	_lastMessage_ : null,
	
	_infiniteScroll_ : null,
	
	lastStateManagerFlags : null,

	noReset : null,

	_delayedQueryTask_ : null,
	_queryTaskTimeout_ : 100,

	_doEmptyCtxFilter_ : null,
	
	_tabPanel_ : null,
	_tabs_ : null,
	_tabPanelName_ : null, // need to keep also name, because tab panel / container panel can be created delayed
	_relatedPanels_ : null,
	_containerPanel_ : null,
	_containerPanelName_ : null,
	_execQueryOnVisibilityChg_ : null,
	_execQueryParam_ : null,
	_isQuerying_ : null,
	_disableAutoSelection_ : null,

	// some commands cannot be executed without any delay between two execution
	// need to manage somehow this rule
	_executedCommands_ : null,
	// time (ms) to wait until the command will be available again
	_cmdExecutionTimeout_ : 3000,
	
	_ajaxRequests_ : null,
	
	constructor : function(config) {
		this._ajaxRequests_ = new Ext.util.MixedCollection();
		this._executedCommands_ = {};
		this.disabledRules = {};
		config = config || {};
		Ext.apply(this, config);
		this.dcState = new e4e.dc.DcState();
		this.children = [];
		this.selectedRecords = [];
		this.forcedParams = {};

		var _rm = this.recordModel;

		if (Ext.isFunction(this.recordModel)) {
			_rm = this.recordModel.$className;
		}

		this.dsName = this.recordModel.ALIAS;

		this.recordModelFqn = _rm;

		if (this.store == null) {
			this.store = this.createStore();
		}

		this.mixins.observable.constructor.call(this);

        this._delayedQueryTask_ = new Ext.util.DelayedTask(this.doQuery, this);

		this._setup_();
	},
	
	getFrame : function() {
		if( this._frame_ ){
			return this._frame_;
		}
		return getActiveDialog();
	},

	createStore : function() {
		var alias = "Ext.data.Store";
		if( this._infiniteScroll_ ){
			alias = "Ext.data.BufferedStore";
		}
		return Ext.create(alias, {
			_controller_ : this,
			model : this.recordModel,
			remoteSort : true,
			leadingBufferZone : 2*this.tuning.fetchSize,
			trailingBufferZone : this.tuning.fetchSize,
			autoLoad : false,
			autoSync : false,
			clearOnPageLoad : true,
			pageSize : this.tuning.fetchSize,
			proxy : {
				type : 'ajax',
				timeout : this.tuning.storeTimeout,
				api : Main.dsAPI(this.dsName, "json"),
				// api : Main.dsAPI(this.recordModel.ALIAS, "json"),
				model : this.recordModel,
				extraParams : {
					params : {}
				},
				actionMethods : {
					create : 'POST',
					read : 'POST',
					update : 'POST',
					destroy : 'POST'
				},
				reader : {
					type : 'json',
					rootProperty : 'data',
					idProperty : 'id',
					totalProperty : 'totalCount',
					messageProperty : 'message'
				},
				writer : {
					type : 'json',
					encode : true,
					rootProperty : "data",
					allowSingle : false,
					writeAllFields : true,
					clientIdProperty : "__clientRecordId__"
				},
				listeners : {
					"exception" : {
						fn : this.proxyException,
						scope : this
					}
				},
				startParam : Main.requestParam.START,
				limitParam : Main.requestParam.SIZE,
				sortParam : Main.requestParam.ORDERBY
			}
		});
	},
	
	doEmptyCtxFilter : function() {
		this._doEmptyCtxFilter_ = true;
	},
	
	doRestoreCtxFilter : function() {
		this._doEmptyCtxFilter_ = null;
	},

	_setup_ : function() {
		var _trlFqn = this.recordModelFqn.replace(".ui.extjs.", ".i18n.");
		try {
			if ( _trlFqn !== this.recordModelFqn && Ext.ClassManager.get(_trlFqn) ){
				this._trl_ = Ext.create(_trlFqn);
			}
		} catch (e) {
			// nothing to do
		}

		if (Ext.isEmpty(this.filterModel)) {
			this.filterModel = Main.createFilterModelFromRecordModel({
				recordModelFqn : this.recordModelFqn
			});
		}
		this.setFilter(this.newFilterInstance());

		if (!Ext.isEmpty(this.paramModel)) {
			var _p = Ext.create(this.paramModel, {});
			if (_p.initParam) {
				_p.beginEdit();
				_p.initParam();
				_p.endEdit();
			}
			this.setParams(_p);
		}

		this.actionNames = e4e.dc.DcActionsFactory.actionNames();
		this.commandNames = e4e.dc.DcCommandFactory.commandNames();

		this.actions = e4e.dc.DcActionsFactory.createActions(this, this.actionNames);
		this.commands = e4e.dc.DcCommandFactory.createCommands(this, this.commandNames.concat(this.actionNames));
		this._registerListeners_();
	},

	_registerListeners_ : function() {

		this.mon(this.store, "beforeload", this.onStore_beforeload, this);
		this.mon(this.store, "beforesync", this.onStore_beforesync, this);
		this.mon(this.store, "update", this.onStore_update, this);
		this.mon(this.store, "load", this.onStore_load, this);

		this.mon(this.store, "write", function() {
			this.updateDcState();
		}, this);
		
		this.mon(this.store, "changes_rejected", function() {
			this.updateDcState();
		}, this);

		this.mon(this, "dcstatechange", function() {
			e4e.dc.DcActionsStateManager.applyStates(this);
		}, this, {
			buffer : 50
		});
	},

	/**
	 * Default initial selection
	 */
	restoreSelection : function() {
		if (this._disableAutoSelection_ !== true) {
			var newSel = null, oldSel;
			var s = this.store;
			// hack: split code with function to not get error from SonarQube
			var fSelectFromList = function(){
				var l = oldSel.length;
				for (var i = 0; i < l; i++) {
					var r = s.getById(oldSel[i].get(oldSel[i].idProperty));
					if (r != null) {
						var idx = s.indexOf(r);
						if (idx >= 0){
							newSel[newSel.length] = r;
						}
					}
				}
			};
			
			if (s.getCount() > 0) {
				newSel = [];
				oldSel = this.getSelectedRecords();
				if (oldSel.length > 0) {
					fSelectFromList();
				}

				if (newSel.length === 0 && this.afterStoreLoadDoDefaultSelection) {
					newSel = [ s.getAt(0) ];
				}
			}

			if (newSel == null) {
				this.setRecord(null);
				this.setSelectedRecords([]);
			} else {
				this.setSelectedRecords(newSel);
			}
		}
	},

	onStore_beforesync : function() {
		this._setStoreIsLoading_(true);
	},
	
	onStore_beforeload : function() {
		if (e4e.dc.DcActionsStateManager.isQueryDisabled(this)) {
			return false;
		}
		this._setStoreIsLoading_(true);
		this._isQuerying_ = true;
		this.updateDcState();
		e4e.dc.DcActionsStateManager.applyStates(this);
	},

	onStore_update : function() {
		this.fireEvent("statusChange", { dc : this });
		this.updateDcState();
	},

	onStore_load : function() {
		this._setStoreIsLoading_(false);
		this._isQuerying_ = false;
		this.fireEvent("storeLoad", {dc : this});
		this.restoreSelection();
		this.updateDcState();
	},
	
	_setStoreIsLoading_ : function(value){
		if( this._storeIsLoading_ !== value ){
			this._storeIsLoading_ = value;
			this.fireEvent("storeIsLoadingChanged", this, this._storeIsLoading_);
		}
	},
	
	/* ***************************************************** */
	/* ***************** Public API ************************ */
	/* ***************************************************** */

	doDefaultSelection : function() {
		if (this.store.count() > 0) {
			this.setSelectedRecords([ this.store.getAt(0) ]);
		}
	},
	
	/**
	 * Clear the DC filters
	 */
	
	doClearFilters : function() {
		this.advancedFilter = null;
		this.doClearQuery();
	},
	
	/**
	 * Empty filter object
	 */
	
	_isCtrlInSomeContext_: function() {

        var isInSomeContext = false;

        if (!Ext.isEmpty(this.dcContext)) {
            isInSomeContext = true;
        }

        return isInSomeContext;
    },

	
	doEmptyFilters : function() {
		var dc = this;

		var fcf = {};
		var fcp = {};

		if (dc.flowContext) {
			fcf = dc.flowContext.filter;
			fcp = dc.flowContext.params;
		}

		var k;
		// reset fields
		for ( k in dc.filter.data) {
			if (fcf[k] === undefined) {
				dc.setFilterValue(k, null, false);
			} else {
				dc.setFilterValue(k, fcf[k], false);
			}
		}

		// reset params used for filter
		var p = dc.params;
		if (p) {
			for ( k in p.data) {
				if (fcp[k] === undefined) {
					dc.setParamValue(k, null, false, "clearQuery");
				} else {
					dc.setParamValue(k, fcp[k], false, "clearQuery");
				}
			}
		}

		// apply context filter
		if (dc.dcContext) {
			dc.dcContext._updateChildFilter_();
		}
	},
	
	doClearAllFilters : function() {
		this.advancedFilter = [];
		this.doEmptyFilters();
	},

	// tab load restrictions are added even if tabs are created later
	// but only for a single tab panel and container panel
	addTabLoadRestriction : function(tabPanelName,relatedPanels){
		this._tabPanel_ = this._frame_._getElement_(tabPanelName);
		if( this._tabPanel_ ){
			var rp = [];
			for(var i=0;i<relatedPanels.length;i++){
				rp.push(this._frame_._getElement_(relatedPanels[i]));
			}
			this._tabs_ = rp;
			this._tabPanel_.on("tabchange",this._onContainerVisibilityChange_,this);
		} else {
			// try it later
			this._tabPanelName_ = tabPanelName;
			this._relatedPanels_ = relatedPanels;
		}
	},
	
	addContentLoadRestriction : function(containerPanelName){
		this._containerPanel_ = this._frame_._getElement_(containerPanelName);
		if( this._containerPanel_ ){
			this._containerPanel_.on("expand",this._onContainerVisibilityChange_,this);
		} else {
			// try it later
			this._containerPanelName_ = containerPanelName;
		}
	},
	
	_isQueryDisabledByContainer_ : function(){
		// restrictions are extended to the collapsible panels too
		var res = false;
		if( !this._containerPanel_ && this._containerPanelName_ ){
			this.addContentLoadRestriction(this._containerPanelName_);
		} else {
			if( !this._tabPanel_ && this._tabPanelName_ ){
				this.addTabLoadRestriction(this._tabPanelName_, this._relatedPanels_);
			}
		}
		if( this._containerPanel_ ){
			res = ( this._containerPanel_.getCollapsed() !== false );
		} else {
			if( this._tabPanel_ && this._tabs_ ){
				var atId = this._tabPanel_.getActiveTab().id;
				res = true;
				for(var i=0;i<this._tabs_.length;i++){
					if( atId === this._tabs_[i].id ){
						res = false;
						break;
					}
				}
			}
		}
		return res;
	},
	
	_onContainerVisibilityChange_ : function() { 
		// possible parameters: tabPanel, newCard, oldCard, eOpts 
		if( this._execQueryOnVisibilityChg_ === true ){
			this.doQuery(this._execQueryParam_);
		}
	},

	/**
	 * Execute query to fetch data.
	 */
	doQuery : function(options) {
		if(this._storeIsLoading_ && this._queryTaskTimeout_>0){
			this.delayedQuery(options);
		} else {
			if( this._isQueryDisabledByContainer_() ){
				this._execQueryParam_ = options;
				this._execQueryOnVisibilityChg_ = true;
			} else {
				this._execQueryOnVisibilityChg_ = false;
				this.commands.doQuery.execute(options);
			}
		}		
	},
	
    delayedQuery : function(obj){
    	if( this._queryTaskTimeout_>0 ){
    		this._delayedQueryTask_.delay(this._queryTaskTimeout_, this.doQuery, this, [obj]);
    	} else {
    		this.doQuery(obj);
    	}
    },

	/**
	 * Clear query criteria -> reset filter to its initial state
	 */
	doClearQuery : function(options) {
		this.commands.doClearQuery.execute(options);
	},

	/**
	 * Enter query. Fire the onEnterQuery event which should be listened by
	 * filter forms to acquire focus.
	 * 
	 */
	doEnterQuery : function(options) {
		this.commands.doEnterQuery.execute(options);
	},

	/**
	 * Create a new record.
	 */
	doNew : function(options) {
		this.commands.doNew.execute(options);
	},
	
	/**
	 * Create a new record in a child DC even when parent record is new (not yet saved in db)
	 */
	doNewInChild : function(options) {
		this.commands.doNewInChild.execute(options);
	},

	/**
	 * Copy the current record reset its ID and make it current record ready to
	 * be edited.
	 */
	doCopy : function(options) {
		this.commands.doCopy.execute(options);
	},

	/**
	 * Save changes.
	 */
	doSave : function(options) {
		this.commands.doSave.execute(options);
	},
	
	/**
	 * Save changes from current DC and child dcs
	 */
	doSaveInChild : function(options) {
		return this.commands.doSaveInChild.execute(options);
	},
	
	/**
	 * Discard changes to the last clean state.
	 */
	doCancel : function(options) {
		this.commands.doCancel.execute(options);
	},

	/**
	 * Discard changes for a list of fields.
	 * 
	 * options : an object with a mandatory field, called fields, which is an array with
	 * field names which need to be changed back to the original value.
	 * 
	 * Experimental function, need to create command for final implementation.
	 */
	doCancelOnFields : function(options) {
		this.commands.doCancelOnFields.execute(options);
	},
	
	/**
	 * Discard changes on the selected records.
	 */
	doCancelSelected : function(options) {
		this.commands.doCancelSelected.execute(options);
	},
	
	/**
	 * Delete selected records.
	 */
	doDelete : function(options) {
		this.commands.doDelete.execute(options);
	},

	/**
	 * Start edit process. Fires onEditIn event which Edit forms should listen
	 * to in order to acquire focus. Used for data-controls which are not multi
	 * edit and work with a grid/form pairs.
	 */
	doEditIn : function(options) {
		this.commands.doEditIn.execute(options);
	},

	/**
	 * Finish edit process. Fires onEditOut event which grid lists should listen
	 * to in order to acquire focus. Used for data-controls which are not multi
	 * edit and work with a grid/form pairs.
	 */
	doEditOut : function(options) {
		this.commands.doEditOut.execute(options);
	},

	/**
	 * Reload the current record data from server
	 */
	doReloadRecord : function(options) {
		this.commands.doReloadRec.execute(options);
	},

	/**
	 * Reload the current data-set (page) from server
	 */
	doReloadPage : function(options) {
		this.commands.doReloadPage.execute(options);
	},

	/**
	 * Call a server side RPC with the filter instance
	 */
	doRpcFilter : function(options) {
		this.commands.doRpcFilter.execute(options);
	},

	/**
	 * Call a server side RPC with the model instance
	 */
	doRpcData : function(options) {
		this.commands.doRpcData.execute(options);
	},

	/**
	 * Call a server side RPC with the model instance list
	 */
	doRpcDataList : function(options) {
		this.commands.doRpcDataList.execute(options);
	},

	/**
	 * Call a server side RPC with a list of id's
	 */
	doRpcIdList : function(options) {
		this.commands.doRpcIdList.execute(options);
	},

	/**
	 * Set the previous available record as current record.
	 */
	doPrevRec : function(options) {
		this.commands.doPrevRec.execute(options);
	},

	/**
	 * Set the next available record as current record.
	 */
	doNextRec : function(options) {
		this.commands.doNextRec.execute(options);
	},

	/**
	 * Load the previous page of records
	 */
	doPrevPage : function(options) {
		var s = this.store;
		if (s.loading) {
			return;
		}
		if (e4e.dc.DcActionsStateManager.isQueryDisabled(this)) {
			this.info(Main.msg.DC_QUERY_NOT_ALLOWED, "msg");
			return false;
		}
		if (s.currentPage > 1) {
			s.loadPage(s.currentPage - 1, options);
		} else {
			this.info(Main.msg.AT_FIRST_PAGE, "msg");
		}
	},

	/**
	 * Load the next page of records
	 */
	doNextPage : function(options) {
		var s = this.store;
		if (s.loading) {
			return;
		}
		if (e4e.dc.DcActionsStateManager.isQueryDisabled(this)) {
			this.info(Main.msg.DC_QUERY_NOT_ALLOWED, "msg");
			return false;
		}
		if (s.totalCount) {
			if (s.currentPage * s.pageSize < s.totalCount) {
				s.loadPage(s.currentPage + 1, options);
			} else {
				this.info(Main.msg.AT_LAST_PAGE, "msg");
			}
		} else {
			s.loadPage(s.currentPage + 1, options);
		}

	},

	// --------------- other ---------------

	/**
	 * Filter validator. It should get interceptor functions chain injected by
	 * the filter forms.
	 */
	isFilterValid : function() {
		return true;
	},

	/**
	 * Record validator. It should get interceptor functions chain injected by
	 * the editor forms.
	 */
	isRecordValid : function() {
		return this.record.isValid();
	},

	/**
	 * Get current record state: dirty/clean
	 */
	getRecordState : function() {
		if (this.record)
			return (this.isCurrentRecordDirty()) ? 'dirty' : 'clean';
		else
			return null;
	},

	/**
	 * Get current record status: insert/update
	 */
	getRecordStatus : function() {
		if (this.record)
			return (this.record.phantom) ? 'insert' : 'update';
		else
			return null;
	},

	/**
	 * Update the enabled/disabled states of the actions. Delegate the work to
	 * the states manager.
	 */
	updateDcState : function() {
		if (this.dcState.run(this)) {
			this.fireEvent("dcstatechange", {
				dc : this
			});
		}
	},

	/**
	 * Returns true if any of the child data-controls is dirty
	 */
	isAnyChildDirty : function() {
		var dirty = false, l = this.children.length;
		for (var i = 0; i < l; i++) {
			if (this.children[i].isDirty()) {
				dirty = true;
				break;
			}
		}
		return dirty;
	},

	/**
	 * Returns true if the current record instance is dirty
	 */
	isCurrentRecordDirty : function() {
		if (this.record != null && (this.record.dirty || this.record.phantom)) {
			return true;
		}
		return false;
	},

	/**
	 * Returns true if the store is dirty. Is relevant only if
	 * <code>multiEdit=true</code>
	 */
	isStoreDirty : function() {
		return this.store.getRemovedRecords().length > 0
				|| this.store.getUpdatedRecords().length > 0
				|| this.store.getAllNewRecords().length > 0;
	},
	
	isStoreEmpty : function() {
		return (this.store.getCount() <= 0);
	},

	/**
	 * Returns true if the data-control is dirty i.e either some the own records
	 * or any child
	 */
	isDirty : function() {
		return this.isCurrentRecordDirty() || this.isStoreDirty()
				|| this.isAnyChildDirty();
	},

	/**
	 * Helper function which packs the necessary information for a query. It is
	 * used in queryCommand, and export and print windows.
	 * 
	 * @return {}
	 */
	buildRequestParamsForQuery : function() {
		var _fd = this.filter.data;
		for ( var key in _fd) {
			if (_fd[key] === "") {
				_fd[key] = null;
			}
		}
		var _p = {};
		_p[Main.requestParam.FILTER] = Ext.encode(this.filter.data);
		_p[Main.requestParam.ADVANCED_FILTER] = Ext.encode(this.advancedFilter || []);
		if( Ext.isFunction(this.filter.getFilterAttributes) ){
			_p[Main.requestParam.FILTER_ATTRIBUTES] = Ext.encode(this.filter.getFilterAttributes());
		}
		if (this.params != null) {
			_p[Main.requestParam.PARAMS] = Ext.encode(this.params.data);
		}
		return _p;
	},

	// messages

	/**
	 * Publish a message to notify subscribers. Used by a frame to write
	 * published messages in its notifications area ( status bar )
	 */
	message : function(type, message, trlGroup, params) {
		this._lastMessage_ = {
			type : type,
			message : message,
			trlGroup : trlGroup,
			params : params
		};
		this.fireEvent("message", this._lastMessage_);
	},

	/**
	 * Publish an info message
	 */
	info : function(message, trlGroup, params) {
		this.message(Ext.Msg.INFO, message, trlGroup, params);
	},

	/**
	 * Publish an info message
	 */
	warning : function(message, trlGroup, params) {
		this.message(Ext.Msg.WARNING, message, trlGroup, params);
	},

	/**
	 * Publish an error message
	 */
	error : function(message, trlGroup, params) {
		this.message(Ext.Msg.ERROR, message, trlGroup, params);

	},

	// --------------------- getters / setters ----------------------

	/**
	 * Returns the filter instance
	 */
	getFilter : function() {
		return this.filter;
	},

	/**
	 * Set the filter instance
	 */
	setFilter : function(v) {
		var of = this.filter;
		this.filter = v;
		this.fireEvent("filterChanged", {
			dc : this,
			newFilter : this.filter,
			oldFilter : of
		});
	},

	/**
	 * Get filter property value
	 */
	getFilterValue : function(n) {
		return this.filter.get(n);
	},

	/**
	 * Set filter property value. As currently the filter model is not part of a
	 * store, changes to properties must be fired so that filter views bound to
	 * the filter model can be notified to refresh their fields values.
	 * 
	 * In some future release it is likely to be enhanced so that filters can be
	 * saved and reused.
	 * 
	 * @param {String}
	 *            property filter-model property to change
	 * @param {Object}
	 *            newValue The new value
	 * @param {boolean}
	 *            silent Do not fire the event.
	 */
	setFilterValue : function(property, newValue, silent, operation, callbackFn) {
		var oldValue = this.filter.get(property);
		if (oldValue !== newValue) {
			this.filter.set(property, newValue);
			
			if (silent !== true) {
				this.fireEvent("filterValueChanged", this, property, oldValue,newValue, operation);
			}
			else {
				this.noReset = true;
			}
		}
		if (callbackFn) {
			callbackFn();
		}
	},

	/**
	 * Get the parameters instance
	 */
	getParams : function() {
		return this.params;
	},

	/**
	 * Set the parametr instance
	 */
	setParams : function(v) {
		this.params = v;
		this.forcedParams = {};
	},

	/**
	 * Get parameter property value
	 */
	getParamValue : function(n) {
		return this.params.get(n);
	},

	/**
	 * Set parameter property value
	 */
	setParamValue : function(property, newValue, silent, op, forced) {
		if( this.params && (this.forcedParams[property] === undefined || forced === true) ){	
			var ov = this.params.get(property);
			if (ov !== newValue) {
				this.params.set(property, newValue);
				if (silent !== true) {
					this.fireEvent("parameterValueChanged", this, property, ov, newValue, op);
				}
			}
			
			if( forced === true ){
				this.forcedParams[property] = newValue;
			}
		}
	},

	/**
	 * Returns the current record
	 * 
	 * @return {Ext.data.Model} Current record or null
	 */
	getRecord : function() {
		return this.record;
	},

	/**
	 * remove all data from DC
	 */
	clearData : function(){
		this.setRecord(null);
		this.setSelectedRecords([]);
		this.store.loadData([], false);
	},
	
	/**
	 * Template method to override with instance specific logic in case is
	 * necessary
	 */
	beforeSetRecord : function() {
		return true;
	},

	
	_isSetRecordAllowed_ : function(){
		if (!this.multiEdit && this.isDirty()) {
			this.error(Main.msg.DIRTY_DATA_FOUND, Ext.Msg.ERROR);
			return false;
		}

		if (this.beforeSetRecord() === false) {
			return false;
		}
		
		return true;
	},
	
	
	__setRecordCheckChanged__ : function(selectIt,changed,rec,oldrec,eOpts){
		if (changed) {
			this.updateDcState();
	
			if (selectIt === true) {
				if (rec != null) {
					this.setSelectedRecords([ rec ], eOpts);
				} else {
					this.setSelectedRecords([], eOpts);
				}
			}
	
			this.fireEvent('recordChange', {
				dc : this,
				newRecord : rec,
				oldRecord : oldrec,
				status : this.getRecordStatus(),
				eOpts : eOpts
			} );
		}
	},
	
	/**
	 * Set the given record as the current working record. If the selectIt
	 * parameter is true make it the selection.
	 * 
	 * @param {Ext.data.Model}
	 *            p Record to be set as current record
	 * @param {Boolean}
	 *            selectIt Update the selected records
	 */
	setRecord : function(p, selectIt, eOpts) {

		if( !this._isSetRecordAllowed_() ){
			return false;
		}
		
		p = (p !== undefined) ? p : null;
		
		var rec;
		var changed = false;
		var oldrec = null;
		
		if (p != null) {
			if (Ext.isNumber(p)) {
				rec = this.store.getAt(p);
				if (rec && (this.record !== rec)) {
					oldrec = this.record;
					this.record = rec;
					changed = true;
				}
			} else {
				rec = p;
				if (rec && (this.record !== rec)) {
					oldrec = this.record;
					this.record = rec;
					changed = true;
				}
			}
		} else {
			rec = p;
			oldrec = this.record;
			this.record = rec;
			changed = (oldrec != null);
		}

		this.__setRecordCheckChanged__(selectIt,changed,rec,oldrec,eOpts);
	},
	
	/**
	 * Returns the selected records
	 */
	getSelectedRecords : function() {
		return this.selectedRecords;
	},

	selectRecord : function(r, eOpts) {
		if( Ext.Array.indexOf(this.selectedRecords,r)<0 ){
			Ext.Array.insert(this.selectedRecords, this.selectedRecords.length,	[ r ]);
			this.updateDcState();
			this.setRecordFromSelection();
			this.fireEvent('selectionChange', {
				dc : this,
				eOpts : eOpts
			});
		}
	},

	deSelectRecord : function(r, eOpts) {
		Ext.Array.remove(this.selectedRecords, r);
		this.updateDcState();
		this.setRecordFromSelection();
		this.fireEvent('selectionChange', {
			dc : this,
			eOpts : eOpts
		});
	},

	setSelectedRecords : function(recArray, eOpts) {
		var s = this.selectedRecords;
		var recs = recArray || [];

		var Arr = Ext.Array;
		var c = Arr.intersect(s, recs);

		if (c.length !== s.length || c.length !== recs.length) {
			Arr.erase(s, 0, s.length);
			Arr.insert(s, 0, recs);
			this.setRecordFromSelection();
			this.fireEvent('selectionChange', {
				dc : this,
				eOpts : eOpts
			});
		}
	},

	setRecordFromSelection : function() {
		var s = this.selectedRecords;
		var r = this.record;
		var Arr = Ext.Array;
		
		if (r != null) {
			if (Arr.indexOf(s, r) === -1) {
				this.setRecord(s[0], false);
			} else {
				this.updateDcState();
			}
		} else {
			this.setRecord(s[0], false);
		}
	},

	/**
	 * Get flow context
	 */
	getFlowContext : function() {
		if (this.flowContext == null) {
			this.flowContext = Ext.create(e4e.dc.FlowContext, {});
			this.flowContext.dc = this;
		}
		return this.flowContext;
	},

	/**
	 * Set data-control context.
	 */
	setDcContext : function(ctx) {
		this.dcContext = ctx;
	},

	getDcContextFields : function() {
		var cf = [];
		if( this.dcContext && this.dcContext.relation.fields ){
			var f = this.dcContext.relation.fields;
			var i, l = f.length;
			for(i=0;i<l;i++){
				cf.push(f[i].childField);
			}
		}
		return cf;
	},
	
	/**
	 * Return parent data-control
	 */
	getParent : function() {
		return (this.dcContext) ? this.dcContext.parentDc : null;
	},

	/**
	 * Return children data-controls list
	 */
	getChildren : function() {
		return this.children;
	},

	/**
	 * Register a child data-control
	 */
	addChild : function(dc) {
		this.children[this.children.length] = dc;
	},

	isReadOnly : function() {
		return this.readOnly;
	},

	setReadOnly : function(v, silent) {
		this.readOnly = v;
		if (silent !== true) {
			this.updateDcState();
			this.fireEvent("readOnlyChanged", this, v);
		}
	},
	
	isDisabled : function() {
		return this.disabled;
	},

	setDisabled : function(v, silent) {
		this.disabled = v;
		if (silent !== true) {
			this.updateDcState();
			this.fireEvent("disabledChanged", this, v);
		}
	},
	
	isParentCheckDisabled : function(){
		return this.parentCheckDisabled;
	},
	
	setParentCheckDisabled : function(v){
		if( this.parentCheckDisabled !== v ){
			this.parentCheckDisabled = v;
			this.updateDcState();
		}
	},

	/**
	 * Creates a new record instance and initialize it
	 */
	newRecordInstance : function() {
		var r = Ext.create(this.recordModel);
		if (r.initRecord) {
			r.beginEdit();
			r.initRecord();
			r.endEdit();
		}
		return r;
	},

	/**
	 * Creates a new filter instance and initialize it
	 */
	newFilterInstance : function() {
		var f = Ext.create(this.filterModel);
		if (f.initFilter) {
			f.beginEdit();
			f.initFilter();
			f.endEdit();
		}
		return f;
	},

	/**
	 * Get the translation for the specified model field. Delegate to the
	 * translator in Main
	 */
	translateModelField : function(f) {
		return Main.translateModelField(this._trl_, f);
	},

	/* ********************************************************** */
	/* ******************** Internal API ************************ */
	/* ********************************************************** */

	/**
	 * Default proxy-exception handler
	 */
	proxyException : function(proxy, response, operation) {
		this._setStoreIsLoading_(false);
		if (operation.error) {
			operation.error.responseText = Ext.getResponseErrorText(response);
		}
		if (operation && operation.action === "destroy" && !this.multiEdit) {
			this.store.rejectChanges();
		}
	},

	/* *********** SERVICE DATA ********************* */

	/**
	 * Default AJAX request failure handler.
	 */
	onAjaxRequestSuccess : function(response, options) {
		try {
			Ext.MessageBox.hide();
		} catch (e) {
			// nothing to do
		}
		var o = options.options || {};
		if (o.action) {
			if (o.action === "doQuery") {
				this.afterDoQuerySuccess();				
			}
			if (o.action === "doSave") {
				this.afterDoSaveSuccess();
			}
			if (o.action === "doService") {
				this.afterDoServiceSuccess(response, o.serviceName, o.specs);
			}
			if (o.action === "doServiceFilter") {
				this.afterDoServiceFilterSuccess(response, o.serviceName, o.specs);
			}
		}
	},

	/**
	 * Default AJAX request failure handler.
	 */
	onAjaxRequestFailure : function(response, options) {
		try {
			Ext.MessageBox.hide();
		} catch (e) {
			// nothing to do
		}
		var o = options.options || {};
		if (o.action) {
			if (o.action === "doQuery") {
				this.afterDoQueryFailure();
			}
			if (o.action === "doSave") {
				this.afterDoSaveFailure();
			}
			if (o.action === "doService") {
				this.afterDoServiceFailure(response, o.serviceName, o.specs);
			}
			if (o.action === "doServiceFilter") {
				this.afterDoServiceFilterFailure(response, o.serviceName, o.specs);
			}
		}
	},

	onCleanDc : function() {
		this.fireEvent('cleanDc', {
			dc : this
		});
		if (this.dcContext != null) {
			this.dcContext._onChildCleaned_();
		}
	},

	onChildCleaned : function() {
		if (!this.isStoreDirty() && !this.isAnyChildDirty()) {
			this.onCleanDc();
		}
	},

	destroy : function() {
		var p;
		for (p in this.actions) {
			delete this.actions[p].dc;
		}
		for (p in this.commands) {
			delete this.commands[p].dc;
		}

		this.store.clearListeners();
		this.store.destroy();

		if (this.dcContext) {
			this.dcContext.destroy();
		}
		delete this.children;
		delete this.parent;
		delete this.dcContext;

		if (this.flowContext) {
			this.flowContext.dc = null;
			delete this.flowContext;
		}

	},

	/**
	 * When edit and new are hacked (in pop-up window and not standard actions are used)
	 * the edit mode need to be handled by the programmer manually
	 */
	setEditMode : function(){
		if (this.trackEditMode) {
			this.isEditMode = true;
		}
	},
	clearEditMode : function(){
		if (this.trackEditMode) {
			this.isEditMode = false;
		}
	},
	
	doExternalReport : function(reportId, format){
		var dc = this;
		var p = {
			data : Ext.encode(dc.record.data)
		}
		p[Main.requestParam.DATA_FORMAT] = format;
		if (dc.params != null) {
			p["params"] = Ext.encode(dc.params.data);
		}
		Ext.Ajax.request({
			url : Main.externalReportsAPI(reportId).run,
			method : "POST",
			params : p,
			success : function(){
				Main.alert(Ext.Msg.INFO, "report_execution_scheduled");
			},
			scope : this,
			timeout : Main.ajaxTimeout
		});
	},
	
	isExternalReportExists : function(clbkFn,clbkScope){
		Ext.Ajax.request({
			url : Main.externalReportsAPI(this.dsName).exists,
			method : "POST",
			params : {},
			success : function(response, options){
				if( options.options && options.options.clbkFn ){
					options.options.clbkFn.call(options.options.clbkScope || this, response, options);
				}
			},
			failure : function(response){
				this.commands.doSave.showAjaxErrors(response);
			},
			scope : this,
			timeout : Main.ajaxTimeout,
			options : {
				clbkFn : clbkFn,
				clbkScope : clbkScope
			}
		});
	},
	
	cancelActiveAjax : function(key){
		var req = this._ajaxRequests_.removeAtKey(key);
		if( req ){
			req.cancelActiveRun = true;
			Ext.Ajax.abort(req);
		}
	},

	addActiveAjax : function(key, req){
		this._ajaxRequests_.add(key, req); 
	}

});
