/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

/**
 * Standard assign window component. Grid with values are provided separately and the source/destination
 * of selected values also. The selected values are separated by comma.
 */
Ext.define("e4e.asgn.AsgnWindow", {
	extend : "Ext.window.Window",
	alias: 'widget.asgnwindow',
	
	closeAction : "hide", 
	resizable : true, 
	layout : "fit", 
	modal : true,
	
	_frame_ : null,
	
	_srcDcName_ : null,
	_srcDc_ : null,
	_srcField_ : null,
	_srcIdField_ : null,

	_gridName_ : null,
	_grid_ : null,
	_gridField_ : null,
	_gridIdField_ : "id",
	
	initComponent: function(config){
		
		this.items = [this._frame_._getElementConfig_(this._gridName_)];
		
		this.dockedItems = [{
			xtype : "toolbar", ui:"footer", dock:"bottom", weight:-1,
			items : [ {
				xtype : "button",
				text : Main.translate("asgn", "btn_asgn__lbl"),
				tooltip : Main.translate("asgn", "btn_asgn__tpl"),
				glyph : "xf00c@FontAwesome",
				iconCls : "glyph-green",
				handler : this._onBtnAssign_,
				scope : this
			}, {
				xtype : "button",
				text : Main.translate("asgn", "btn_asgn_cancel__lbl"),
				tooltip : Main.translate("asgn", "btn_asgn_cancel__tpl"),
				glyph : "xf05e@FontAwesome",
				iconCls : "glyph-red",
				handler : this._onBtnAssignCancel_,
				scope : this
			} ]
		}];
		
		this.callParent(arguments);
		
		this.on("show", function(){
			this._grid_._getController_().doReloadPage();
		}, this);
		this.on("hide", function(){
			this._grid_.getSelectionModel().deselectAll();
		}, this);

		this._srcDc_ = this._frame_._getDc_(this._srcDcName_);
		
		this._grid_ = this._frame_._getElement_(this._gridName_);
		this._grid_.store.on("load", this._selectRecordsInGrid_, this);
	},
	
	_onBtnAssign_ : function(){
		var srcRec = this._srcDc_.getRecord();
		if( srcRec ){
			
			var s = this._grid_.getSelectionModel().getSelection();
			var selectedValues = [];
			var selectedIds = [];
			
			Ext.each(s, function (item) {
				selectedValues.push(item.data[this._gridField_]);
				selectedIds.push(item.data[this._gridIdField_]);
			}, this);
			
			var strValues = selectedValues.join(",");
			var strIds = selectedIds.join(",");
			srcRec.beginEdit();
			if( this._srcIdField_ ){
				srcRec.set(this._srcIdField_, strIds);
			}
			srcRec.set(this._srcField_, strValues);
			srcRec.endEdit();
			
			this.fireEvent("selectionChanged", {wnd: this, selectedIds: strIds, selectedValues: strValues});
		}
		this.hide();
	},
	
	_onBtnAssignCancel_ : function(){
		this.hide();
	},
	
	_selectRecordsInGrid_ : function(store){
		if( this.isVisible() ){
			
			this._grid_.getSelectionModel().deselectAll();
			
			var srcRec = this._srcDc_.getRecord();
			if( srcRec ){
				
				var v = srcRec.get(this._srcField_);
				if( v ){
					
					var va = v.split(",");
					if (store) {
						store.each(function(r){
							if( va.indexOf(r.get(this._gridField_)) >= 0 ){
								this._grid_.getSelectionModel().select(store.indexOf(r), true);
							}
						}, this);
					}
					
				}
			}
		}
	}
	
});
