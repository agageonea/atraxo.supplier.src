/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
/**
 * Factory class used to create the Ext.Action instances for the registered
 * action names. To create other actions register them here, create a command
 * executor and implement the state management rules in DcActionStateManager.
 */
e4e.dc.DcActionsFactory = {

	RUN_QUERY : "Query",
	CLEAR_QUERY : "ClearQuery",
	ENTER_QUERY : "EnterQuery",
	CREATE : "New",
	CREATE_IN_CHILD : "NewInChild",
	COPY : "Copy",
	SAVE : "Save",
	SAVE_IN_CHILD : "SaveInChild",
	DELETE : "Delete",
	CANCEL : "Cancel",
	CANCEL_ON_FIELDS : "CancelOnFields",
	CANCEL_SELECTED : "CancelSelected",
	EDIT_IN : "EditIn",
	EDIT_OUT : "EditOut",
	CLOSE : "Close",
	PREV_REC : "PrevRec",
	NEXT_REC : "NextRec",
	RELOAD_REC : "ReloadRec",
	RELOAD_PAGE : "ReloadPage",

	BUTTON_UI : "default",

	/**
	 * List with the names of the registered actions. See the appropriate create
	 * method for the meaning of each of the actions.
	 */
	actionNames : function() {
		return [ this.RUN_QUERY, this.CLEAR_QUERY, this.ENTER_QUERY,
		         this.CREATE, this.CREATE_IN_CHILD, this.COPY, this.SAVE, this.SAVE_IN_CHILD, this.DELETE, 
		         this.CANCEL, this.CANCEL_ON_FIELDS, this.CANCEL_SELECTED,	this.EDIT_IN, this.EDIT_OUT, 
		         this.CLOSE, this.PREV_REC, this.NEXT_REC, this.RELOAD_REC, this.RELOAD_PAGE ];
	},

	/**
	 * Create an object of actions for the given data-control and list of action
	 * names.
	 */
	createActions : function(dc, names) {
		var result = {};
		for (var i = 0, l = names.length; i < l; i++) {
			var n = names[i];
			result["do" + n] = this["create" + n + "Action"](dc);
		}
		return result;
	},

	/**
	 * Create the action to execute a query.
	 */
	createQueryAction : function(dc) {
		return new Ext.Action({
			name : "doQuery",
			ui : this.BUTTON_UI,
			iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-fetch"
					: null,
			disabled : false,
			text : Main.translate("tlbitem", "load__lbl"),
			tooltip : Main.translate("tlbitem", "load__tlp"),
			scope : dc,
			handler : dc.doQuery
		});
	},

	/**
	 * Create the action to clear the filter values.
	 */
	createClearQueryAction : function(dc) {
		return new Ext.Action({
			name : "doClearQuery",
			ui : this.BUTTON_UI,
			iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-fetch"
					: null,
			disabled : false,
			text : Main.translate("tlbitem", "clear_query__lbl"),
			tooltip : Main.translate("tlbitem", "clear_query__tlp"),
			scope : dc,
			handler : dc.doClearQuery
		});
	},

	/**
	 * Create the action to enter/modify the filter values.
	 */
	createEnterQueryAction : function(dc) {
		return new Ext.Action({
			name : "doEnterQuery",
			ui : this.BUTTON_UI,
			iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-fetch"
					: null,
			disabled : false,
			text : Main.translate("tlbitem", "enter_query__lbl"),
			tooltip : Main.translate("tlbitem", "enter_query__tlp"),
			scope : dc,
			handler : dc.doEnterQuery
		});
	},

	/**
	 * Create the action to create a new record.
	 */
	createNewAction : function(dc) {
		return new Ext.Action({
			name : "doNew",
			ui : this.BUTTON_UI,
			iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-new" : null,
			disabled : true,
			text : Main.translate("tlbitem", "new__lbl"),
			tooltip : Main.translate("tlbitem", "new__tlp"),
			scope : dc,
			handler : dc.doNew
		});
	},

	createNewInChildAction : function(dc) {
		return new Ext.Action({
			name : "doNewInChild",
			ui : this.BUTTON_UI,
			iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-new" : null,
			disabled : false,
			text : Main.translate("tlbitem", "new__lbl"),
			tooltip : Main.translate("tlbitem", "new__tlp"),
			scope : dc,
			handler : dc.doNewInChild
		});
	},
	
	/**
	 * Create the action to copy a record in client.
	 */
	createCopyAction : function(dc) {
		return new Ext.Action({
			name : "doCopy",
			ui : this.BUTTON_UI,
			iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-copy"
					: null,
			disabled : true,
			text : Main.translate("tlbitem", "copy__lbl"),
			tooltip : Main.translate("tlbitem", "copy__tlp"),
			scope : dc,
			handler : dc.doCopy
		});
	},

	/**
	 * Create the action to save changes.
	 */
	createSaveAction : function(dc) {
		return new Ext.Action({
			name : "doSave",
			ui : this.BUTTON_UI,
			iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-save" : null,
			disabled : true,
			text : Main.translate("tlbitem", "save__lbl"),
			tooltip : Main.translate("tlbitem", "save__tlp"),
			scope : dc,
			handler : dc.doSave
		});
	},

	createSaveInChildAction : function(dc) {
		return new Ext.Action({
			name : "doSaveInChild",
			ui : this.BUTTON_UI,
			iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-save" : null,
			disabled : true,
			text : Main.translate("tlbitem", "save__lbl"),
			tooltip : Main.translate("tlbitem", "save__tlp"),
			scope : dc,
			handler : dc.doSaveInChild
		});
	},
		
	/**
	 * Create the action to delete records.
	 */
	createDeleteAction : function(dc) {
		return new Ext.Action(
				{
					name : "doDelete",
					ui : this.BUTTON_UI,
					iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-delete"
							: null,
					disabled : true,
					text : Main.translate("tlbitem", "delete__lbl"),
					tooltip : Main.translate("tlbitem", "delete__tlp"),
					scope : dc,
					handler : dc.doDelete
				});
	},

	/**
	 * Create the action to cancel changes made to the data-model.
	 */
	createCancelAction : function(dc) {
		return new Ext.Action(
				{
					name : "doCancel",
					ui : this.BUTTON_UI,
					iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-rollback"
							: null,
					disabled : true,
					text : Main.translate("tlbitem", "cancel__lbl"),
					tooltip : Main.translate("tlbitem", "cancel__tlp"),
					scope : dc,
					handler : dc.doCancel
				});
	},
	
	/**
	 * Create the action to cancel changes made to the list of fields in data-model.
	 */
	createCancelOnFieldsAction : function(dc) {
		return new Ext.Action(
				{
					name : "doCancelOnFields",
					ui : this.BUTTON_UI,
					iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-rollback-selected"
							: null,
					disabled : true,
					text : Main.translate("tlbitem", "cancel_on_fields__lbl"),
					tooltip : Main.translate("tlbitem", "cancel_on_fields__tlp"),
					scope : dc,
					handler : dc.doCancelOnFields
				});
	},

	/**
	 * Create the action to cancel changes made to the selected record in data-model.
	 */
	createCancelSelectedAction : function(dc) {
		return new Ext.Action(
				{
					name : "doCancelSelected",
					ui : this.BUTTON_UI,
					iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-rollback-selected"
							: null,
					disabled : true,
					text : Main.translate("tlbitem", "cancel_selected__lbl"),
					tooltip : Main.translate("tlbitem", "cancel_selected__tlp"),
					scope : dc,
					handler : dc.doCancelSelected
				});
	},

	/**
	 * Create the action to enter edit-mode.
	 */
	createEditInAction : function(dc) {
		return new Ext.Action({
			name : "doEditIn",
			ui : this.BUTTON_UI,
			iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-edit"
					: null,
			disabled : true,
			text : Main.translate("tlbitem", "edit__lbl"),
			tooltip : Main.translate("tlbitem", "edit__tlp"),
			scope : dc,
			handler : dc.doEditIn
		});
	},

	/**
	 * Create the action to exit edit-mode.
	 */
	createEditOutAction : function(dc) {
		return new Ext.Action({
			name : "doEditOut",
			ui : this.BUTTON_UI,
			iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-back"
					: null,
			disabled : false,
			text : Main.translate("tlbitem", "back__lbl"),
			tooltip : Main.translate("tlbitem", "back__tlp"),
			scope : dc,
			handler : dc.doEditOut
		});
	},

	/**
	 * Create the action to exit edit-mode / close panel.
	 */
	createCloseAction : function(dc) {
		return new Ext.Action({
			name : "doClose",
			ui : this.BUTTON_UI,
			iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-close"
					: null,
			disabled : false,
			text : "",
			tooltip : Main.translate("tlbitem", "back__tlp"),
			scope : dc,
			handler : dc.doEditOut
		});
	},

	/**
	 * Create the action to load the previous available record in store as
	 * current record.
	 */
	createPrevRecAction : function(dc) {
		return new Ext.Action(
				{
					name : "doPrevRec",
					ui : this.BUTTON_UI,
					iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-previous" : null,
					disabled : false,
					text : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "" : "<",
					tooltip : Main.translate("tlbitem", "prev_rec__tlp"),
					scope : dc,
					handler : dc.doPrevRec
				});
	},

	/**
	 * Create the action to load the next available record in store as current
	 * record.
	 */
	createNextRecAction : function(dc) {
		return new Ext.Action({
			name : "doNextRec",
			ui : this.BUTTON_UI,
			iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-next" : null,
			disabled : false,
			text : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "" : ">",
			tooltip : Main.translate("tlbitem", "next_rec__tlp"),
			scope : dc,
			handler : dc.doNextRec
		});
	},

	/**
	 * Create the action to reload the current record from server.
	 */
	createReloadRecAction : function(dc) {
		return new Ext.Action(
				{
					name : "doReloadRec",
					ui : this.BUTTON_UI,
					iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-refresh"
							: null,
					disabled : false,
					text : Main.translate("tlbitem", "reload_rec__lbl"),
					tooltip : Main.translate("tlbitem", "reload_rec__tlp"),
					scope : dc,
					handler : dc.doReloadRecord
				});
	},

	/**
	 * Create the action to reload the current record from server.
	 */
	createReloadPageAction : function(dc) {
		return new Ext.Action(
				{
					name : "doReloadPage",
					ui : this.BUTTON_UI,
					iconCls : (Main.viewConfig.USE_TOOLBAR_ICONS) ? "icon-action-refresh"
							: null,
					disabled : false,
					text : Main.translate("tlbitem", "reload_page__lbl"),
					tooltip : Main.translate("tlbitem", "reload_page__tlp"),
					scope : dc,
					handler : dc.doReloadPage
				});
	}

};