/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
function getApplication() {
	if (!__application__ && !window.parent) {
		alert("Cannot find application.");
		return;
	}
	return (__application__) ? __application__ : window.parent.__application__;
}
function getActiveDialog(){
	var app = getApplication();
	if( app ) {
		return app.getActiveFrameInstance();
	}
	return null;
}
Ext.ns("e4e.base");
e4e.base.Application = {

	/**
	 * Session object.
	 * 
	 * @type e4e.base.Session
	 */
	session : e4e.base.Session,

	/**
	 * Main application view.
	 * 
	 * @type
	 */
	view : null,

	/**
	 * Main application menu.
	 * 
	 * @type
	 */
	menu : null,

	/**
	 * Frame navigator instance
	 * 
	 * @type e4e.base.FrameNavigatorWithIframe
	 */
	navigator : null,

	/**
	 * Map which stores the registered callbacks used by the inter-frame
	 * communication.
	 */
	frameCallbacks : new Ext.util.MixedCollection(),
	
	/**
	 * Map which stores the registered parameters used by the inter-frame
	 * communication. These parameters are retrieved by frame before frame
	 * initialization are finished and frameCallback function are called.
	 */
	frameOpenParameters : new Ext.util.MixedCollection(),

	/**
	 * Login window object.
	 * 
	 * @type e4e.base.LoginWindow
	 */
	loginWindow : null,

	/**
	 * Getter for the session object.
	 * 
	 * @return {}
	 */
	getSession : function() {
		return this.session;
	},

	/**
	 * Getter for the main view object.
	 * 
	 * @return {}
	 */
	getView : function() {
		return this.view;
	},

	/**
	 * Getter for the application home view.
	 * 
	 * @return {}
	 */
	getViewHome : function() {
		return Ext.getCmp(Constants.CMP_ID.APP_VIEW_HOME);
	},

	/**
	 * Getter for application header view.
	 * 
	 * @return {}
	 */
	getViewHeader : function() {
		return Ext.getCmp(Constants.CMP_ID.APP_VIEW_HEADER);
	},

	/**
	 * Getter for the application footer view.
	 * 
	 * @return {}
	 */
	getViewFooter : function() {
		return Ext.getCmp(Constants.CMP_ID.APP_VIEW_FOOTER);
	},

	/**
	 * Getter for the application body view.
	 * 
	 * @return {}
	 */
	getViewBody : function() {
		return Ext.getCmp(Constants.CMP_ID.APP_VIEW_BODY);
	},

	/**
	 * Show the specified frame
	 * 
	 * @param {}
	 *            name
	 * @param {}
	 *            bundle
	 */
	showFrameByName : function(bundle, name) {
		this.showFrame(name, {
			url : Main.buildUiPath(bundle, name, false)
		});
	},

	/**
	 * Open the specified frame.
	 * 
	 * @param options:
	 *            callback: the callback function to be executed after the frame
	 *            is rendered. params: the arguments used in callback call
	 */
	showFrame : function(frame, options) {
		var theFrame = this.navigator.getFrameInstance(frame);
		var applyCallbackNow = !Ext.isEmpty(theFrame);
		if(options.frameOpenParams){
			this.addFrameOpenParameters(frame, options.frameOpenParams);
		}
		this.navigator.showFrame(frame, options);
		this.registerFrameCallback(frame, options);
		if (applyCallbackNow) {
			this.applyFrameCallback(frame, theFrame);
		}
		var f = function(){
			var m = getApplication().menu;
			if( m ) {
				m._appIsLoading_ = false;
			}
		};
		Ext.defer(f,1000,this);
	},
	
	notifyFrameOpen : function(frame) { 
		var items = this.getViewBody().items.items;
		for(var i=0;i<items.length;i++){
			var fId = items[i].n21_iframeID;
			if( fId ){
				var fr = this.getFrameInstance(items[i].fqn);
				if( fr && frame !== fr.$className && Ext.isFunction(fr.onFrameOpened) ){
					fr.onFrameOpened(frame);
				}
			}
		}
	},

	gotoNextTab : function() {
		var vb = this.getViewBody();
		var crt = vb.getActiveTab();
		var next = crt.nextSibling();
		if (next) {
			vb.setActiveTab(next);
		} else {
			vb.setActiveTab(0);
		}
	},

	/**
	 * Returns the active tab.
	 */
	getActiveTab : function() {
		return this.getViewBody().getActiveTab();
	},

	/**
	 * Get the application frame instance for the active tab.
	 * 
	 * @return {}
	 */
	getActiveFrameInstance : function() {
		if (this.getActiveTab()) {
			return this.navigator.getFrameInstance(this.getActiveTab().initialConfig.fqn);
		}
		return null;
	},
	
	
	/**
	 * Get the application frame instance for the frameFqn.
	 * 
	 * frameFqn is something like: atraxo.fmbas.ui.extjs.frame.Customers_Ui
	 * 
	 * @return {}
	 */
	getFrameInstance : function(frameFqn) {
		return this.navigator.getFrameInstance(frameFqn);
	},
	
	/**
	 * Get the application frame instance for the frame.
	 * 
	 * frame is the end part of the frameFqn, something which is easy to remember, like: Customers_Ui
	 * 
	 * @return {}
	 */
	searchForFrameInstance : function(frame) {
		var items = this.getViewBody().items.items;
		frame = (""+frame).toUpperCase();
		
		for(var i=0;i<items.length;i++){
			if( (""+items[i].fqn).toUpperCase().endsWith(frame) ){
				return this.getFrameInstance(items[i].fqn);
			}
		}
		return null;
	},

	/**
	 * Register a callback function to be executed in a certain frame. The frame
	 * may not be open at the time the callback execution is requested. It is
	 * placed in the queue and executed when the frame is rendered.
	 * 
	 * @param {}
	 *            frame
	 * @param {}
	 *            options
	 */
	registerFrameCallback : function(frame, options) {
		if (options && !Ext.isEmpty(options.tocElement)
				&& Ext.isEmpty(options.callback)) {
			options.callback = function(params) {
				this._showTocElement_(params.tocElement);
			}
			options.params = {
				tocElement : options.tocElement
			}
		}
		if (options && options.callback) {
			this.frameCallbacks.add(frame, options);
		}
	},

	/**
	 * Execute the registered callback
	 * 
	 * @param {}
	 *            frame
	 * @param {}
	 *            theFrameObject
	 */
	applyFrameCallback : function(frame, theFrameObject) {
		if (this.frameCallbacks.containsKey(frame)) {
			var opt = this.frameCallbacks.get(frame);
			Ext.defer(opt.callback, 500, theFrameObject, [opt.params]);
			this.frameCallbacks.removeAtKey(frame);
		}
	},
	
	addFrameOpenParameters : function(frame, params){
		if (this.frameOpenParameters.containsKey(frame)) {
			this.frameOpenParameters.removeAtKey(frame);
		}
		this.frameOpenParameters.add(frame, params);
	},
	
	getFrameOpenParameters : function(frame) {
		var params = {};
		if (this.frameOpenParameters.containsKey(frame)) {
			params = this.frameOpenParameters.get(frame);
			this.frameOpenParameters.removeAtKey(frame);
		}
		return params;
	},
	
	/**
	 * Check for notifications
	 */
	notificationTaskRunner : null,
	readNotificationsTask : null,
	checkNotificationsTask : null,
	checkNotificationsCallIsRunning : null,
	
	startCheckNotificationTask : function(restart){
		if( !this.readNotificationsTask ){
			// at first run, read the stored notifications
			this.readNotificationsTask = new Ext.util.DelayedTask(this.readNotifications, this);
			this.readNotificationsTask.delay(10);
		}
		if( !this.checkNotificationsTask ){
			this.checkNotificationsTask = new Ext.util.DelayedTask(this.checkNotifications, this);
		}
    	if( !this.checkNotificationsCallIsRunning && (restart || !this.checkNotificationsTask.id) ){
   			this.checkNotificationsTask.delay(100);
    	}
    	
    	if( !this.notificationTaskRunner ){
    		this.notificationTaskRunner = new Ext.util.DelayedTask(this.startCheckNotificationTask, this);
    	}
    	this.notificationTaskRunner.delay(60*1000);
	},
	
	checkNotifications: function() {		
		if (typeof(_SYSTEMPARAMETERS_) !== "undefined" && typeof(_SYSTEMPARAMETERS_) !== "null" 
				&& _SYSTEMPARAMETERS_.live_notifications === "false") {
			return;
		}

		this.checkNotificationsCallIsRunning = true;
		try{
			Ext.Ajax.request({
	            url: Main.dsAPI("fmbas_Notification_Ds", "json").notify,
	            method: "POST",
	            scope: this,
	            timeout : Main.ajaxTimeout,
	            success: function(response) {
	            	this.checkNotificationsCallIsRunning = false;
	            	
	            	var data = Ext.getResponseDataInJSON(response);
	            	if( data.id !== -1 ){
		            	var uid = Math.floor(Math.random()*90000) + 10000;
		            	
		            	Main.pushNotification(data.category, data.title, data.description, data.eventDate, data.id, "notification-"+uid, data.methodName, data.methodParam);
		            	
		            	var notificationCounter = document.getElementById(this.menu._idCmpNotificationCounter_);
		        		if (Main.countNotifications() > 0) {
		        			notificationCounter.style.display="block";
		        		}
		        		notificationCounter.innerHTML = Main.countNotifications();
		        		Main.notify();
	            	}
	            	if( this.readNotificationsTask.id ) {
	            		// exists a schedule, execute now
	            		this.readNotificationsTask.delay(10);
	            	}
	            	this.checkNotificationsTask.delay(50);
	            },
	            failure: function(response) {
	            	this.checkNotificationsCallIsRunning = false;
	            	var longTimeout = true;
	            	if (response.status === 0 && response.timedout === true) {
	            		// If the request is cancelled
                    	this.checkNotificationsTask.delay(100);
                    	longTimeout = false;
	            	}
	            	if( longTimeout ) {
	            		var checkTimeOut = 1000 * 60 * 5; // 5 minutes
                		// If the request is failed make another request after 5 minutes
                    	this.readNotificationsTask.delay(checkTimeOut);
                    	this.checkNotificationsTask.delay(checkTimeOut+1000);
            		}
	            }
			});
		}catch(e){
			this.checkNotificationsCallIsRunning = false;
			Main.error(Main.translate("msg", "error_occured")+e);
		}
	},
	
	readNotifications : function(){
		if (typeof(_SYSTEMPARAMETERS_) !== "undefined" && typeof(_SYSTEMPARAMETERS_) !== "null" 
			&& _SYSTEMPARAMETERS_.live_notifications === "false") {
			return;
		}
		
		Ext.Ajax.request({
            url: Main.dsAPI("fmbas_Notification_Ds", "json").read,
            method: "POST",
        	params : {
				data : Ext.encode({
					userCode : getApplication().getSession().getUser().code
				})
			},
            scope: this,
            success: function(response) {
            	var data = Ext.getResponseDataInJSON(response).data; 
            	var len = data.length;
            	
            	if (len > 0) {
            		Ext.each(data, function(n) {
            			var uid = Math.floor(Math.random()*90000) + 10000;
                		Main.pushNotification(n.category, n.title, n.descripion, n.eventDate, n.id, "notification-"+uid, n.methodName, n.methodParam);
                	}, this);
                	
                	var notificationCounter = document.getElementById(this.menu._idCmpNotificationCounter_);
            		if (Main.countNotifications() > 0) {
            			notificationCounter.style.display="block";
            		}
            		notificationCounter.innerHTML = Main.countNotifications();
            		Main.notify();
            	}
            },
            failure: function() {
        		// If the request is failed make another request after 5 minutes
            	this.readNotificationsTask.delay(1000*60*5);
            }
    	});
	},

	/**
	 * Entry point function to start-up an application.
	 */
	run : function() {

		this.navigator = e4e.base.FrameNavigatorWithIframe;
		this.navigator.maxOpenTabs = Constants.MAX_OPEN_TABS;

		if (this.loginWindow == null) {
			this.loginWindow = new e4e.base.LoginWindow({});
		}

		if (!this.session.isAuthenticated()) {
			this.loginWindow.show();
			this.loginWindow.clearInvalid();
		} else if (this.session.locked) {
			this.loginWindow.applyState_LockSession();
			this.showLoginView();
			this.loginWindow.getPasswordField().focus(false, 200);
		} else {
			this.menu.setUserText(this.session.user.name);
			this.menu.setClientText(this.session.client.code);
			this.menu.setCompanyText(this.session.company.code);
			if (this.session.user.systemUser) {
				this.menu.addSystemMenu();
			} else {
				this.menu.removeSystemMenu();
			}
		}
		
		this.startCheckNotificationTask(true);
	},

	/**
	 * On login success hide the login window and show the application canvas.
	 * Add whatever else is necessary.
	 */
	onLoginSuccess : function(accountChange) {
		if (accountChange) {
			this.getViewBody().plugins[0].onCloseAll(); // .removeAll(true);
		}
		Ext.Msg.hide();
		this.showMainApplicationView();
		this.loginWindow.hide();

		this.menu.setUserText(this.session.user.name);
		this.menu.setClientText(this.session.client.code);

		if (this.session.user.systemUser) {
			this.menu.addSystemMenu();
		} else {
			this.menu.removeSystemMenu();
		}
		if (this._reloadFrame_) {
			var frame = this._reloadFrame_;
			this._reloadFrame_ = null;
			var iFrameInst = this.navigator.getIFrameInstance(frame); 
			if (iFrameInst) {
				try{
					iFrameInst.contentWindow.location.reload();
				}catch(e){
					// nothing to do
				}
			}
		}
		
		this.startCheckNotificationTask(true);
	},

	/**
	 * When default company has been changed, update the text in the application
	 * header
	 */
	onCompanyChange : function() {
		var c = this.session.company;
		this.menu.setCompanyText(c.code);
		Ext.util.Cookies.set("dnet-compId", c.id);
		Ext.util.Cookies.set("dnet-compCode", c.code);
	},

	/**
	 * Logout from application
	 */
	doLogout : function() {
		Ext.Ajax.request({
			method : "POST",
			scope : this,
			url : Main.sessionAPI("json").logout,
			success: function(response) {
				confirm_leave = false;
				var url = Ext.getResponseDataInText(response);
				if(!Ext.isEmpty(url)){
					url = url.trim();
					if( url.indexOf("?") > 0 ){
						url += "&";
					} else {
						url += "?";
					}
					url += this._getVersion_();
				}
				if (url && url.length > 0) {
					window.location.href = url;
				} else {
					window.location.href = Main.urlSession + "/login?" + this._getVersion_();
				}
			}
		});
	},

	doReloadPageOrLockSession : function(ajaxResp){
		var res = true;
		
		if(ajaxResp){
			var respStatus = ""+ajaxResp.status;
			if( respStatus !== "401" && respStatus !== "403" ){
				res = false;
			}
		}
		
		if( res ){
			Ext.Msg.hide();

			if( (""+e4e.base.Session.auth).toLowerCase() === "saml" ){
				Ext.Msg.show({
					msg : Main.translate("login", "msgSaml"),
					icon : Ext.MessageBox.INFO,
					buttons : Ext.Msg.OK,
					_version_ : this._getVersion_(),
					cls: "sone-on-top",
					fn: function(btnId, x, opt) {
						if( btnId === "ok" ){
							confirm_leave = false;
							window.location.href = Main.urlUiExtjs + "?" + opt._version_;
						}
					}
				});
			} else {
				this.doLockSession(true);
			}
		}
		return res;
	},
	
	/**
	 * Lock active session.
	 */
	doLockSession : function(onlyShowDialog) {
		if( !onlyShowDialog ){
			Ext.Ajax.request({
				method : "GET",
				scope : this,
				url : Main.sessionAPI("json").lock,
				timeout : Main.ajaxTimeout
			});
		}
		this.loginWindow.applyState_LockSession();
		this.showLoginView();
		this.loginWindow.getPasswordField().focus(false, 200);
	},

	/**
	 * Activate the empty canvas and show the login window.
	 */
	showLoginView : function() {
		// in Firefox when events occurs on an invisible layout the layout can lose some of it's content
		// SONE-1234
		/*
		//this.view.getLayout().setActiveItem(0);
		//Ext.getBody().mask();
		*/
		this.loginWindow.show();
	},

	/**
	 * Activate the application view canvas.
	 */
	showMainApplicationView : function() {
		/*
		//this.view.getLayout().setActiveItem(1);
		//Ext.getBody().unmask();
		*/
	},

	_getVersion_ : function(){
		return "version=" + Math.floor(Date.now() / 1000);
	},
	
	reloadAppWithNewParameter : function(p){
		window.location.href = Main.urlUiExtjs + "?" + this._getVersion_() + "&" + p;
	},
	
	/**
	 * Change the current theme with the provided one.
	 * 
	 * @param {}
	 *            t selected theme
	 */
	changeCurrentTheme : function(t) {
		this.reloadAppWithNewParameter(Ext.urlEncode({ theme : t }));
	},

	/**
	 * Change current language with the provided one.
	 * 
	 * @param {}
	 *            l selected language
	 */
	changeCurrentLanguage : function(l) {
		this.reloadAppWithNewParameter(Ext.urlEncode({ lang : l }));
	},

	/**
	 * Set the tab title according to the nested frame title.
	 * 
	 * @param {}
	 *            frame
	 * @param {}
	 *            title
	 */
	setFrameTabTitle : function(frame, title) {
		var n = Main.getFrameTabId(frame);
		var c = Ext.getCmp(n);
		if (c) {
			c.setTitle(title);
		}
	},
	
	/**
	 * Display the authentication dialog and after authenticated, reload the current frame.
	 * Is used when authentication failed on a frame open.
	 */
	authenticateAndReloadFrame : function() {
		try{
			this._reloadFrame_ = this.getActiveTab().fqn;
			getApplication().doReloadPageOrLockSession();
		}catch(e){
			this._reloadFrame_ = null;
		}
	},

	/**
	 * Convert units taking in consideration system density
	 */
	_sysdens_ : null,
	_convUnitError_ : null,
	_getConvObj_ : function(cd){
		var i, l=_CONV_UNITS_.length;
		var res = null;
		for(i=0;i<l;i++){
			if( _CONV_UNITS_[i].code === cd ){
				res = _CONV_UNITS_[i];
				break;
			}
		}
		return res;
	},
	convertUnit : function(frCd,toCd,value,density) {
		var res = null;
		this._convUnitError_ = null;
		if( frCd !== toCd ){
			if( !this._sysdens_ ){
				this._sysdens_ = _SYSTEMPARAMETERS_.sysdens;
				if( !this._sysdens_ ){
					this._sysdens_ = 0.8;
				}
			}
			density = density || this._sysdens_;
			var objFr = this._getConvObj_(frCd);
			var objTo = this._getConvObj_(toCd);
			if( objFr && objTo ){
				try{
					res = value * objFr.factor / objTo.factor;
					if( objFr.utc !== objTo.utc ){
						if( objFr.utc === "V" && objTo.utc === "M" ){
							res = res * density;
						} else
						if( objFr.utc === "M" && objTo.utc === "V" ){
							res = res / density;
						} else {
							res = null;
							this._convUnitError_ = "Incompatible unit types (from: " + objFr.utc + " to: " + objTo.utc + ") given for conversion!";
						}
					}
				}catch(e){
					res = null;
					this._convUnitError_ = "Error occured during conversion! " + e;
				}
			} else {
				if( !objFr ){
					this._convUnitError_ = "From code (" + frCd + ") not found in conversion table! ";
				} else {
					this._convUnitError_ = "";
				}
				if( !objTo ){
					this._convUnitError_ = this._convUnitError_ + "To code (" + toCd + ") not found in conversion table!";
				}
			}
		} else {
			res = value;
		}
		return res;
	},
	getLastConversionError : function(){
		return this._convUnitError_;
	}
	
};
