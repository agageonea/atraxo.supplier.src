/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
/**
 * Collection of flags to describe the state of a data-control.
 * 
 */
Ext.define("e4e.dc.DcState", {

	mixins : {
		observable : 'Ext.util.Observable'
	},

	isReadOnly : false,

	isDisabled : false,

	isStoreDirty : false,

	isDirty : false,

	isAnyChildDirty : false,

	recordIsNull : true,

	isCurrentRecordDirty : false,

	multiEdit : false,

	selectedRecordsLength : 0,

	storeCount : 0,

	hasParent : false,

	parentIsNull : false,

	parentIsNew : false,

	parentIsReadOnly : false,
	
	parentIsDisabled : false,

	parentIsDirty : false,
	
	parentCheckDisabled : null,
	
	canDo : {},
	
	disabledRules : {},
	
	isQuerying : null,

	constructor : function() {
		this.mixins.observable.constructor.call(this);
	},

	set : function(flag, nv, silent) {
		var me = this;
		var ov = me[flag];
		if (ov === undefined) {
			alert("Invalid flag name `" + flag + "` provided to DcState! ");
		}
		
		var changed;
		if( Ext.isObject(nv) ){
			changed = (Ext.JSON.encodeValue(ov) !== Ext.JSON.encodeValue(nv));
		} else {
			changed = (ov !== nv);
		}
		if (changed) {
			me[flag] = nv;
			if (silent !== true) {
				me.fireEvent("flagchanged", {
					flag : flag,
					ov : ov,
					nv : nv
				});
			}
			return true;
		}
		return false;
	},

	/**
	 * Recalculate state flags and returns true if state change has been
	 * detected.
	 */
	run : function(dc) {
		var changed = false;
		var _canDo = {};

		var names = dc.actionNames;
		for (var i = 0, l = names.length; i < l; i++) {
			var n = names[i];
			var an = "canDo" + n;
			
			var m = dc[an];
			if (m !== undefined && m !== null) {
				if (Ext.isFunction(m)) {
					_canDo[an] = m.call(dc,dc);
				} else {
					_canDo[an] = m;
				}
			} 
		}

		var flags = {
			isReadOnly : dc.isReadOnly(),
			isDisabled : dc.isDisabled(),
			isStoreDirty : dc.isStoreDirty(),
			isDirty : dc.isDirty(),
			isAnyChildDirty : dc.isAnyChildDirty(),
			recordIsNull : Ext.isEmpty(dc.record),
			isCurrentRecordDirty : dc.isCurrentRecordDirty(),
			multiEdit : dc.multiEdit,
			selectedRecordsLength : dc.getSelectedRecords().length,
			storeCount : dc.store.getCount(),
			disabledRules : dc.disabledRules,
			canDo: _canDo,
			isQuerying : dc._isQuerying_,
			parentCheckDisabled : dc.isParentCheckDisabled()
		};

		flags.hasParent = (dc.getParent() != null);

		if (flags.hasParent) {

			var _parent = dc.getParent();
			
			flags.parentIsDisabled = _parent.isDisabled();

			if (!flags.parentIsNull && _parent.isReadOnly()) {
				flags.parentIsReadOnly = true;
			} else {
				flags.parentIsReadOnly = false;
			}

			if (!_parent.getRecord()) {
				flags.parentIsNull = true;
			} else {
				flags.parentIsNull = false;

				if (_parent.record.phantom) {
					flags.parentIsNew = true;
				} else {
					flags.parentIsNew = false;

					if (_parent.isCurrentRecordDirty()) {
						flags.parentIsDirty = true;
					} else {
						flags.parentIsDirty = false;
					}
				}
			}
		}

		for ( var flag in flags) {
			if (this.set(flag, flags[flag], true) === true) {
				changed = true;
			}
		}

		return changed;
	},

	isRecordChangeAllowed : function() {
		if (this.multiEdit) {
			return !this.isAnyChildDirty;
		} else {
			return !(this.isCurrentRecordDirty || this.isAnyChildDirty);
		}
	}

});
