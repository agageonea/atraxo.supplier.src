/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.lov.AbstractCombo", {

	extend : "Ext.form.field.ComboBox",
	alias : "widget.xcombo",

	// Properties
	
	cls: "",

	/**
	 * 
	 * Link to the dc-view fields to filter the records in this combo.
	 * 
	 * Example: ,filterFieldMapping: [{lovField:"...lovFieldName", dsField:"...dsFieldName"} ]
	 * 
	 * Or: ,filterFieldMapping: [{lovField:"...lovFieldName", value: "...static value"} ]
	 */
	filterFieldMapping : null,
	
	advFilter : null,

	/**
	 * Specify what values should this combo return to the dc-record.
	 * 
	 * @type Array
	 */
	retFieldMapping : null,

	_dataProviderFields_ : null,
	_dataProviderName_ : null,
	_dummyValue_ : null,
	_editFrame_ : null,
	openFrame : null,
	_isLov_ : true, // used in framework
	_targetRecord_ : null,

	/**
	 * Data-control view type this field belongs to. Injected by the
	 * corresponding view builder.
	 */
	_dcView_ : null,

	/**
	 * Data model signature - record constructor.
	 * 
	 * @type Ext.data.Model
	 */
	recordModel : null,

	recordModelFqn : null,
	/**
	 * Parameters model signature - record constructor.
	 * 
	 * @type Ext.data.Model
	 */
	paramModel : null,
	/**
	 * Parameters model instance
	 */
	params : null,

	// defaults
	triggerAction : "all",
	pageSize : Main.viewConfig.FETCH_SIZE_LOV,
	autoSelect : false,
	minChars : 0,
	queryMode : "remote",
	queryCaching : false, // for auto-complete behavior
	storeAutoLoad : false, 
	typeAhead: true,
	//trigger1Cls : Ext.baseCSSPrefix + 'x-form-trigger',
	defaultListConfig : {
		minWidth : 250,
		width : 250,
		shadow : "sides",
		autoScroll : true
	},
	autoScroll : true,

	_localCache_ : null,
	_noPaginator_ : true,
	_initialQueryRunning_ : null,
	_initialQueryStarting_ : null,
	_columns_ : null,  
    _myIsLoading_ : null,
    _clearMyIsLoadingTask_ : null,
    _valueValidated_ : 0, // -1 = not in db; 0 = force selection is not set, val is not in db; 1 = value in db
	_onFocusValueValidated_ : 0,
	_oldValueValidated_ : 0,

	initComponent : function() {
		
		this.__storeAndFieldsInitialization__();
		this.__eventHandlersInitialization__();
		
		this.callParent(arguments);

		this._clearMyIsLoadingTask_ = new Ext.util.DelayedTask(this._clearMyIsLoading_, this);
		
		if( this._localCache_ ){

			Ext.defer(function(){
				this._initialQueryStarting_ = true;
				if( this.doQuery(null, true, true) ){
					this._initialQueryRunning_ = true;
					var fAfterLoad = function(){
						this._initialQueryRunning_ = false;
						this.queryMode = "local";
						this.queryDelay = 100;
						this.minChars = 0;
					};
					this.store.on("load", fAfterLoad, this, {single:true});
					this.store.proxy.on("exception", function(){this._initialQueryRunning_ = false;}, this, {single:true});
				}
				this._initialQueryStarting_ = false;
			}, 100, this, []);
		} else {
			var fAfterLoad = function(){
				this._highlightRecord_(this._searchForRecord_());
			};
			this.store.on("load", fAfterLoad, this);
		}
		if( this._noPaginator_ && !this._localCache_ ){
			// display only a limited set of records; the rest of records will be available using filtering mechanism
			this.pageSize = Main.viewConfig.FETCH_SIZE_LOV_NO_PAGINATOR;
			this.store.pageSize = this.pageSize;
		}
		
        this.store.on("beforeload", function() {
        	this._storeIsLoading_ = true;
        }, this);
		this.store.on('load', function(){
        	this._storeIsLoading_ = false;
		}, this );
		this.store.on('exception', function(){
        	this._storeIsLoading_ = false;
		}, this );

	},
	
	_keyPressed_ : null,
    fireKey: function(e){
		if( !e.isNavKeyPress() ){
			var k = e.keyCode;
			if( !( (k >= 16 && k <= 20) || // Shift, Ctrl, Alt, Pause, Caps Lock
		        (k >= 44 && k <= 45) )   // Print Screen, Insert
		    ) {
				this._keyPressed_ = true;
			}
		}
    	this.callParent(arguments);
    },
    
	__storeAndFieldsInitialization__: function(){
		if( this._localCache_ ){
			this.pageSize = Main.viewConfig.FETCH_SIZE_UNLIMITED;
			this.typeAhead = false;
		}
		
		this._createStore_();
		if (!Ext.isEmpty(this.paramModel)) {
			this.params = Ext.create(this.paramModel, {});
		}
		if (this.retFieldMapping == null) {
			this.retFieldMapping = [];
		}
		if (this.dataIndex) {
			this.retFieldMapping[this.retFieldMapping.length] = {
				lovField : this.displayField,
				dsField : this.dataIndex
			};
		} else if (this.paramIndex) {
			this.retFieldMapping[this.retFieldMapping.length] = {
				lovField : this.displayField,
				dsParam : this.paramIndex
			};
		}
		
		if( !this._columns_ ){
			// if columns info is not defined, add the displayField into the array
			this._columns_ = [this.displayField];
		}
	},
	
	__eventHandlersInitialization__: function(){
		this.on("select", this.assertValueMy, this);
		this.on("blur", function(){
			this._autoSelectRecord_();
		}, this);
		this.on("specialkey", function(field, e){
			var key = e.getKey();
			if (key === e.ESC) {
				this.setValue(this._onFocusVal_);
            } else
            if( (key === e.TAB || key === e.ENTER) && (!this._myIsLoading_) ){
        		// in some situations the blur event is not received when user left the list with TAB key-press
        		// need to map values also in these situations - item SONE-3455
           		this._autoSelectRecord_();
            }
		}, this);
	},
	
	_searchForRecord_: function(){
		var record = null;
		if(this.store.getCount()>0){
			record = this.store.findRecord(this.displayField, this.getRawValue());
			if( !record ){
				record = this.store.getAt(0);
			}
		}
		return record;
	},
	_highlightRecord_: function(record){
		if(record){
			var boundList = this.getPicker();
			var navModel = boundList.getNavigationModel();
			boundList.highlightItem(boundList.getNode(record));
			var idx = boundList.indexOf(boundList.highlightedItem);
			navModel.setPosition(idx);
		}
	},
	
    onTypeAhead : function() {
    	this._highlightRecord_(this._searchForRecord_());
	},
    
	_beforeExpandVal_: null,
	expand : function(){
		if( this._initialQueryRunning_ === true || this._initialQueryStarting_ === true ){
			return;
		}
		this.callParent(arguments);
		this._beforeExpandVal_ = this.getValue();
	},
	
	collapse : function(){
		var check = this.isExpanded;
		this.callParent(arguments);
		if( check ){
			var crtVal = this.getValue();
			if( String(crtVal) !== String(this._beforeExpandVal_) ){
				this.fireEvent('fpchange', this, crtVal, this._beforeExpandVal_);
			}
		}
	},

	onTrigger2Click : function() {
		if (this._editFrame_ == null) {
			alert("No destination frame specified.");
			return;
		} else {
			if( this._editFrame_.custom === undefined || this._editFrame_.custom === null ){
				this._editFrame_.custom = false;
			}
		}
		getApplication().showFrame(this._editFrame_.name, {
			url : Main.buildUiPath(this._editFrame_.name),
			tocElement : this._editFrame_.tocElement
		});
	},

	_createStore_ : function() {
		if (this._dataProviderName_ == null) {
			if (Ext.isFunction(this.recordModel)) {
				this.recordModelFqn = this.recordModel.$className;
			} else {
				this.recordModel = Ext.ClassManager.get(this.recordModel)
				this.recordModelFqn = this.recordModel.$className;
			}
			this._dataProviderName_ = this.recordModel.ALIAS;
		}
		var alias = "Ext.data.Store";
		if( this._noPaginator_ ){
			this.pageSize = 0;
		}
		this.store = Ext.create(alias, {
			model : this.recordModel,
			remoteSort : true,
			autoLoad : this.storeAutoLoad,
			autoSync : false,
			clearOnPageLoad : true,
			pageSize : this.pageSize,
			proxy : {
				type : 'ajax',
				api : Main.dsAPI(this._dataProviderName_, "json"),
				model : this.recordModel,
				timeout : Main.ajaxTimeout,
				extraParams : {
					params : {}
				},
				actionMethods : {
					create : 'POST',
					read : 'POST',
					update : 'POST',
					destroy : 'POST'
				},
				reader : {
					type : 'json',
					rootProperty : 'data',
					idProperty : 'id',
					totalProperty : 'totalCount',
					messageProperty : 'message'
				},
				listeners : {
					"exception" : {
						fn : this.proxyException,
						scope : this
					}
				},
				startParam : Main.requestParam.START,
				limitParam : Main.requestParam.SIZE,
				sortParam : Main.requestParam.SORT,
				directionParam : Main.requestParam.SENSE
			}
		});
	},

	__inEditor__ : null,
	inEditor : function(){
		if(this.__inEditor__ === null){
			var dcv = this._dcView_;
			if( dcv ){
				var vt = dcv._dcViewType_;
				this.__inEditor__ = (vt === "edit-grid" || vt === "filter-propgrid" || vt === "edit-propgrid" || vt === "bulk-edit-field");
			} else {
				return false;
			}				
		}
		return this.__inEditor__;
	},
	
	_getTargetRecord_ : function() {
		var drec = null;
		var dcv = this._dcView_;
		var vt = dcv._dcViewType_;
		var ctrl = dcv._controller_;

		if (this.inEditor() && vt !== "edit-propgrid" && vt !== "filter-propgrid") {
			if (dcv && vt === "bulk-edit-field") {
				//is a bulk editor for one ds field in a property grid
				drec = dcv.getSource();
			} else {
				drec = this._targetRecord_;
			}
		} else {
			if (vt === "edit-form" || vt === "edit-propgrid") {
				drec = ctrl.getRecord();
			} else if (vt === "filter-form" || vt === "filter-propgrid") {
				drec = ctrl.getFilter();
			}
		}
		return drec;
	},

	/**
	 * Map fields from the combo-record received as argument (crec) to the
	 * target record according to fieldMappings
	 * 
	 * @param {}
	 *            crec Combo selected record
	 */
	_mapReturnFields_ : function(crec) {
		var dcv = this._dcView_;
		if (dcv) {
			var vt = dcv._dcViewType_;
			var ctrl = dcv._controller_;
			var drec = null;
			var prec = ctrl.getParams();
			var targetIsFilter = false;

			if (this.inEditor() && vt !== "edit-propgrid" && vt !== "filter-propgrid") {
				if (vt === "bulk-edit-field") {
					// is a bulk editor for one ds field in a property grid
					drec = dcv.getSource();
					this._mapReturnFieldsExecuteBulkEdit_(crec, drec);
				} else {
					drec = this._targetRecord_;
					this._mapReturnFieldsExecute_(crec, drec, prec);
				}
			} else {
				if (vt === "edit-form" || vt === "edit-propgrid") {
					drec = ctrl.getRecord();
				}
				if (vt === "filter-form" || vt === "filter-propgrid") {
					drec = ctrl.getFilter();
					targetIsFilter = true;
				}
				this._mapReturnFieldsExecute_(crec, drec, prec, targetIsFilter);
			}
		}
		
	},

	__mapReturnFieldsProcessItem__ : function(i, rawv, crec, drec, prec, targetIsFilter) {
		
		var nv, ov, isParam, retDataIndex;
		
		isParam = !Ext.isEmpty(this.retFieldMapping[i]["dsParam"]);
		if (isParam) {
			retDataIndex = this.retFieldMapping[i]["dsParam"];
			ov = (prec?prec.get(retDataIndex):null);
		} else {
			retDataIndex = this.retFieldMapping[i]["dsField"];
			if (targetIsFilter) {
				ov = this._dcView_._controller_.getFilterValue(retDataIndex);
			} else  if( drec ){
                ov = drec.get(retDataIndex);
            } else {
                ov = null;
            }
		}
		
		if (crec && crec.data) {
			nv = crec.data[this.retFieldMapping[i]["lovField"]];
			if (nv != ov) {
				if (isParam) {
					this._dcView_._controller_.setParamValue(retDataIndex, nv);
				} else {
					if (targetIsFilter) {
						this._dcView_._controller_.setFilterValue(retDataIndex, nv);
					} else if( drec ){
						drec.set(retDataIndex, nv);
					}
				}
			}
		} else {
			if (retDataIndex === this.dataIndex) {
				if (this._validateListValue_ && rawv != ov) {
					rawv = null;
					this.setRawValue(rawv);
				}
				if (rawv != ov) {
					if (isParam) {
						this._dcView_._controller_.setParamValue(retDataIndex, rawv);
					} else {
						if (targetIsFilter) {
							this._dcView_._controller_.setFilterValue(retDataIndex, rawv);
						} else if( drec ){
							drec.set(retDataIndex, rawv);
						}
					}
				}
			} else {
				if ((ov != null && ov !== "")) {
					if (isParam) {
						this._dcView_._controller_.setParamValue(retDataIndex, null);
					} else {
						if (targetIsFilter) {
							this._dcView_._controller_.setFilterValue(retDataIndex, null);
						} else if( drec ){
							drec.set(retDataIndex, null);
						}
					}
				}
			}
		}
	},
	
	/**
	 * Params:<br>
	 * crec: Combo selected record <br>
	 * drec - Controller data-record. <br>
	 * The current record or current filter based on the view-type context <br>
	 * prec - Controller params record
	 */
	_mapReturnFieldsExecute_ : function(crec, drec, prec, targetIsFilter) {
		if (!drec && !this.retFieldMapping) {
			return;
		}
		var i, rawv = this.getRawValue(), len = this.retFieldMapping.length - 1;
		for (i = len; i >= 0; i--) {
			this.__mapReturnFieldsProcessItem__(i, rawv, crec, drec, prec, targetIsFilter);
		}
	},

	_mapReturnFieldsExecuteBulkEdit_ : function(crec, recdata) {
		if (!recdata) {
			return;
		}
		if (this.retFieldMapping != null) {
			for (var i = this.retFieldMapping.length - 1; i >= 0; i--) {
				var retDataIndex;
				var nv;
				var isParam = !Ext.isEmpty(this.retFieldMapping[i]["dsParam"]);
				if (isParam) {
					retDataIndex = this.retFieldMapping[i]["dsParam"];
				} else {
					retDataIndex = this.retFieldMapping[i]["dsField"];
				}
				if (crec && crec.data) {
					nv = crec.data[this.retFieldMapping[i]["lovField"]];
					recdata[retDataIndex] = nv;
				}
			}
		}
	},

	_mapFilterFields_ : function(bp) {

		var drec = null;
		var dcv = this._dcView_;
		var vt = dcv._dcViewType_;
		var prec = dcv._controller_.getParams();

		if (this.inEditor()) {
			drec = this._targetRecord_;
			return this._mapFilterFieldsExecute_(bp, drec, prec);
		} else {
			if (vt === "edit-form") {
				drec = dcv._controller_.getRecord();
			} else {
				if (vt === "filter-form") {
					drec = dcv._controller_.getFilter();
				}
			}
			return this._mapFilterFieldsExecute_(bp, drec, prec);
		}
	},

	__mapFFExecute__ : function(_ffm, bp, drec, prec){
		var _lp = _ffm["lovParam"];
		var _lf = _ffm["lovField"];

		var _dsp = _ffm["dsParam"];
		var _dsf = _ffm["dsField"];

		var _v = _ffm["value"];
		var _nn = _ffm["notNull"];

		var isLovMemberParam = !Ext.isEmpty(_lp);
		var _val = null;

		if (_v !== undefined && _v != null) {
			_val = _v;
		} else {
			if (!Ext.isEmpty(_dsp)) {
				if( prec ){
					_val = prec.get(_dsp)
				}
			} else {
				if( drec ){
					_val = drec.get(_dsf)
				}
			}
		}

		if (Ext.isEmpty(_val)) {
			_val = "";
			if (_nn === true) {
				if (this._dcView_) {
					this._dcView_._controller_.info("Select the context value for this list of values field.");
					return false;
				} else {
					alert("Select the context value for this list of values field.");
					return false;
				}
			}
		}

		if (isLovMemberParam) {
			this.params.set(_lp, _val);
		} else {
			bp[_lf] = _val;
		}
		
		return true;
	},
	
	/**
	 * Parameters: bp: base params for the store
	 * 
	 * drec - Controller data-record. The current record or current filter based on the view-type context 
	 * prec - Controller params record
	 * 
	 * Note: this function was splitted because of complexity
	 */
	_mapFilterFieldsExecute_ : function(bp, drec, prec) {
		if (!drec && !this.filterFieldMapping) {
			return false;
		}
		var len = this.filterFieldMapping.length;
		for (var i = 0; i < len; i++) {

			if( !this.__mapFFExecute__(this.filterFieldMapping[i], bp, drec, prec) ){
				return false;
			}
		}
		return true;
	},

	/**
	 * Default proxy-exception handler
	 */
	proxyException : function(proxy, response, operation, eOpts) {
		this.showAjaxErrors(response, eOpts);
	},

	/**
	 * Show errors to user.
	 */
	showAjaxErrors : function(response) {
		Main.serverMessage(null,response);
	},

	// **************************************************
	// *********************** OVERRIDES ****************
	// **************************************************
    doLocalQuery: function(queryPlan) {
		var me = this,
	    	queryString = queryPlan.query,
	        store = me.getStore(),
            filter = me.queryFilter;

        me.queryFilter = null;

        // Must set changingFilters flag for this.checkValueOnChange.
        // the suppressEvents flag does not affect the filterchange event
        me.changingFilters = true;
        if (filter) {
            store.removeFilter(filter, true);
        }

        // Querying by a string...
        if (queryString) {
        	var _FC_ = this._columns_;
        	var _QS_ = (""+queryString).toUpperCase();
            filter = me.queryFilter = new Ext.util.Filter({
                filterFn: function(item) {
                	for(var i=0;i<_FC_.length;i++){
                		var v = ""+item.get(_FC_[i]);
                		if( v.toUpperCase().indexOf(_QS_) >= 0 ){
                			return true;
                		}
                	}
                	return false;
            	}
            });
            store.addFilter(filter, true);
        }
		me.changingFilters = false;

		// Expand after adjusting the filter if there are records or if emptyText is configured.
		if (me.store.getCount() || me.getPicker().emptyText) {
			// The filter changing was done with events suppressed, so
			// refresh the picker DOM while hidden and it will layout on show.
			me.getPicker().refresh(); 
			me.expand();
			this._highlightRecord_(this._searchForRecord_());
        } else {
			me.collapse();
		}

		me.afterQuery(queryPlan);
    },
    
	_getAdvFilter_ : function(queryPlan) {

		var af = [];
		var i, o, r;
		
		var _populateAF_ = function(advFilter){
			for(i=0;i<advFilter.length;i++){
				var f = advFilter[i];
				o = {
					"id":null,
					"fieldName":f.lovFieldName,
					"operation":f.operation,
					"value1": !Ext.isEmpty(f.value1)?f.value1:r.get(f.fieldName1),
					"groupOp":"AND"+i
				};
				af.push(o);
			}
		};
		
		if( this.advFilter ){
			// preapre advanced filter set at LOV usage
			var dc = this._dcView_._controller_;
			if( dc ){
				r = dc.getRecord();
				if( r ){
					_populateAF_(this.advFilter);
				}
			}
		}
		
		// prepare advanced filter generated at LOV definition
		for(i=0;i<this._columns_.length;i++){
			o = {
				"id":null,
				"fieldName":this._columns_[i],
				"operation":"like",
				"value1": "%"+queryPlan.query+"%",
				"groupOp":"OR1"
			};
			af.push(o);
		}
		return af;
	},
	
	__remoteQueryPrepareParams__ : function(queryPlan, bp, extraParams){
		if( this._columns_.length <= 0 ){
			bp[this.displayField] = queryPlan.query + "*";
		}
		if (this.filterFieldMapping != null) {
			if (this._mapFilterFields_(bp) === false) {
				return false;
			}
			this.queryCaching = false;
		}
		extraParams[Main.requestParam.ADVANCED_FILTER] = Ext.encode(this._getAdvFilter_(queryPlan));
		extraParams[Main.requestParam.FILTER] = Ext.encode(bp);
		if (this.params != null) {
			extraParams[Main.requestParam.PARAMS] = Ext.encode(this.params.data);
		}
	},
	
	doRemoteQuery : function(queryPlan) { 
		if( this._initialQueryRunning_ === true ){
			return;
		}
		var bp = {};
		var extraParams = this.store.proxy.extraParams;
		
		this.__remoteQueryPrepareParams__(queryPlan, bp, extraParams);

		// do the parent function job, but expand the list only if the focus is still in the component
        var me = this,
            loadCallback = function() {
                if (!me.destroyed) {
                    me.afterQuery(queryPlan);
                }
            };
        if( this.hasFocus ){
        	// expand before loading so LoadMask can position itself correctly
        	me.expand();
        }
        
        // In queryMode: 'remote', we assume Store filters are added by the developer as remote filters,
        // and these are automatically passed as params with every load call, so we do *not* call clearFilter.
        if (me.pageSize) {
            // if we're paging, we've changed the query so start at page 1.
            me.loadPage(1, {
                rawQuery: queryPlan.rawQuery,
                callback: loadCallback
            });
        } else {
            me.store.load({
                params: me.getParams(queryPlan.query),
                rawQuery: queryPlan.rawQuery,
                callback: loadCallback
            });
        }
	},

    afterQuery: function(queryPlan) {
    	this.callParent(arguments); 
        this._myIsLoading_ = false;
        if( !this.hasFocus ){
        	this._autoSelectRecord_();
        }
    },

    __autoSelectRecordCheckMatchForOneCheckField__ : function(rec,vu,fieldName){
    	var res = false;
		if( (""+rec.get(fieldName)).toUpperCase().indexOf(vu)===0 ){
			this.setValue(rec.get(this.displayField));
			res = true;
		}
		return res;
    },
    
    __autoSelectRecordCheckMatchForOne__ : function(rec,vu){
		var c = this.store.getCount();
		if( c === 1 && vu !== "" ){
			rec = this.store.getAt(0);
			var res = this.__autoSelectRecordCheckMatchForOneCheckField__(rec,vu,this.displayField);
			var i=0;
			var len = this._columns_.length;
			while( i<len && !res ){
				if( this._columns_[i] !== this.displayField ){
					res = this.__autoSelectRecordCheckMatchForOneCheckField__(rec,vu,this._columns_[i]);
				}
				i++;
			}
			if( !res ){
				rec = null;
			}
		}
    	return rec;
    },
    
    __autoSelectRecordCheckMatchInListCheckFieldExact__ : function(rec,vu,fieldName){
    	var res = false;
		if( (""+rec.get(fieldName)).toUpperCase() === vu ){
			this.setValue(rec.get(this.displayField));
			res = true;
		}
		return res;
    },
    
    __autoSelectRecordCheckMatchInListCheckFieldPartial__ : function(rec,vu,fieldName){
    	var res = false;
		if( (""+rec.get(fieldName)).toUpperCase().indexOf(vu) === 0 ){
			this.setValue(rec.get(this.displayField));
			res = true;
		}
		return res;
    },
    
    __autoSelectRecordCheckMatchInList__ : function(rec,vu){
		var i, r, res;
		var c = this.store.getCount();
		var colLen = this._columns_.length;
		var colIdx;
		
		for(i=0;i<c;i++){
			r = this.store.getAt(i);
			res = this.__autoSelectRecordCheckMatchInListCheckFieldExact__(r, vu, this.displayField);
			if( res ){
				rec = r;
				break;
			}
		}
		if(!rec){
			for(i=0;i<c;i++){
				r = this.store.getAt(i);
				colIdx = 0;
				while( !res && colIdx<colLen ){
					if( this._columns_[colIdx] !== this.displayField ){
						res = this.__autoSelectRecordCheckMatchInListCheckFieldExact__(r, vu, this._columns_[colIdx]);
					}
					colIdx++;
				}
				if( res ){
					rec = r;
					break;
				}
			}
		}
		if(!rec){
			for(i=0;i<c;i++){
				r = this.store.getAt(i);
				res = this.__autoSelectRecordCheckMatchInListCheckFieldPartial__(r, vu, this.displayField);
				colIdx = 0;
				while( !res && colIdx<colLen ){
					if( this._columns_[colIdx] !== this.displayField ){
						res = this.__autoSelectRecordCheckMatchInListCheckFieldPartial__(r, vu, this._columns_[colIdx]);
					}
					colIdx++;
				}
				if( res ){
					rec = r;
					break;
				}
			}
		}
    	return rec;
    },
    
	_autoSelectRecord_ : function() {
		var rw = this.getRawValue();
		var vu = (""+rw).toUpperCase();
		var rec = this.__autoSelectRecordCheckMatchForOne__(rec,vu);
		if( !rec && vu !== "" ){
			rec = this.__autoSelectRecordCheckMatchInList__(rec,vu);
		}
		if( rec ){
			this.assertValueMy();
			this._checkAndFireValueChange_();
		} else {
			if( this.forceSelection === true ){
				if( rw !== this._onFocusVal_ && (this.inEditor() || (!this._storeIsLoading_ && this._keyPressed_)) ){
					this.clearValue();
				} else {
					if( this._storeIsLoading_ ){
						Ext.defer(this._autoSelectRecord_, 100, this);
						return false;
					}
				}
				this.assertValueMy();
				this._checkAndFireValueChange_();
			} else {
				this._checkAndApplyEmptyVal_(this.getRawValue());
				this._valueValidated_ = 0;
				this.isValid();
			}
		}
		this._keyPressed_ = false;
		return true;
	},

	clearUiFlags: function(){
		this._oldValueValidated_ = 0;
		this._valueValidated_ = 0;
		this.isValid();
	},
    
	_setUiFlags_: function(isV){
		if( this.inEditor() ){
			return;
		}
		if( isV>=0 || isV===true ){
			this.removeCls("combo-box-check-fail");
			if( this._valueValidated_ > 0 ){
				this.addCls("combo-box-check-ok");
			} else {
				this.removeCls("combo-box-check-ok");
			}
		} else {
			this.removeCls("combo-box-check-ok");
			this.addCls("combo-box-check-fail");
		}
	},

    isValid: function() {
    	var result;
    	if( this._isDisabled_() ){
    		this._valueValidated_ = 0;
    		result = this.callParent(arguments);
    	} else {
	    	var isV = this._valueValidated_;
	    	if( isV >= 0 ){
	    		isV = this.callParent(arguments);
	    	}
	    	this._setUiFlags_(isV);
	    	if(isV === false || isV === true){
	    		result = isV;
	    	} else {
	    		result = (isV>=0)?true:(isV<0)?false:isV;
	    	}
    	}
		if( this._valueValidated_ !== this._oldValueValidated_ ){
			this.fireEvent('fpvalidchange', this, this._valueValidated_, this._oldValueValidated_);
		}
		this._oldValueValidated_ = this._valueValidated_;
    	return result;
    },

    _clearMyIsLoading_: function(){
		this._myIsLoading_ = false;
    },
    
    onFieldMutation: function(e) {
    	this.callParent(arguments);
    	this._myIsLoading_ = true;
    	this._clearMyIsLoadingTask_.delay(500);
    },
    
    assertValue : function() {
		if( this.store.hasPendingLoad() || this._myIsLoading_ ){
			return;
		}
    	this.callParent(arguments);
    },

    _checkAndApplyEmptyVal_ : function(val) {
		if (Ext.isEmpty(val)) {
			this._valueValidated_ = 0;
			this._mapReturnFields_(null);
			this._checkAndFireValueChange_();
		}
	},

	onFocus: function() {
		this.callParent(arguments);
		this._onFocusValueValidated_ = this._valueValidated_;
	},

	__assertValueMyPhase1__: function(val){
		if( this.queryMode !== 'local' && (this.store.hasPendingLoad() /*|| this._myIsLoading_*/) ){
			if( Ext.isEmpty(val) ){
				this._checkAndApplyEmptyVal_(val);
			}
			this._valueValidated_ = this._onFocusValueValidated_;
			return false;
		}
		return true;
	},
	
	__assertValueMyPhase2__: function(rec, val){
		rec = this.findRecord(this.displayField, val);
		if( rec && rec._fake_ === true ){
			rec = null;
		}
		if(!rec){
			rec = this.valueCollection.find(this.displayField, val);
			if( rec && rec._fake_ === true ){
				rec = null;
			}
			if(rec && val === rec.get(this.displayField)) {
				this._valueValidated_ = 1;
				return false;
			}
		}
		return rec;
	},

	
	__assertValueMyPhase3__: function(rec, val){
		if (rec) {
			this._valueValidated_ = 1;
			this._mapReturnFields_(rec);
			if(! (val === rec.get(this.displayField) && this.value === rec.get(this.valueField)) ){
				val = rec.get(this.valueField || this.displayField);
	
				if (this.getRawValue() !== val) {
					this.setValue(val);
				}
			}
		} else {
			if (this.forceSelection) {
				if (val !== this._onFocusVal_){
					this._valueValidated_ = -1;
					this.setRawValue(val);
					this._mapReturnFields_(null);
				}
				this._checkAndApplyEmptyVal_(val);
			}
		}
		// do not change icon on lovs if value was not changed
		// without this check, it's happening on local cached lovs to put OK icon when the user left the field, 
		// because the record list is filled and the value (get from DB) is found in the list 
		if (val === this._onFocusVal_){
			this._valueValidated_ = this._onFocusValueValidated_;
		}
	},
	
	assertValueMy : function() {
		// because of the complexity, this function was splited in more 
		if( !this._isDisabled_() ){
			this._valueValidated_ = 0;
			var val, rec;
			
			val = this.getRawValue();
			
			if( this.__assertValueMyPhase1__(val) ){
				rec = this.__assertValueMyPhase2__(rec,val);
				if(rec){
					this.__assertValueMyPhase3__(rec,val);
				}
			}
		}
		this.isValid();
	},

	setValue: function(value) {
		this.callParent(arguments);
		
		if(Ext.isEmpty(this.getValue())){
			this.clearUiFlags();
		}
	},
	
	getValue: function() {
		// If the user has not changed the raw field value since a value was selected from the list,
		// then return the structured value from the selection. If the raw field value is different
		// than what would be displayed due to selection, return that raw value.
		var me = this, picker = me.picker, rawValue = me.getRawValue(), // current value of text field
		value = me.value; // stored value from last selection or setValue() call

		if (me.getDisplayValue() !== rawValue) {
			me.displayTplData = undefined;
			if (picker) {
				// We do not need to hear about this clearing out of the value collection, so suspend events.
				me.valueCollection.suspendEvents();
				picker.getSelectionModel().deselectAll();
				me.valueCollection.resumeEvents();
			}
			// If the raw input value gets out of sync in a multiple ComboBox, then we have to give up.
			// Multiple is not designed for typing *and* displaying the comma separated result of selection.
			// Same in the case of forceSelection.
			if (me.multiSelect || me.forceSelection) {
				value = me.value = undefined;
			} else {
				value = me.value = rawValue;
			}
		}

		// Return null if value is undefined/null, not falsy.
		me.value = value == null ? null : value;
		return me.value;
	}
});
