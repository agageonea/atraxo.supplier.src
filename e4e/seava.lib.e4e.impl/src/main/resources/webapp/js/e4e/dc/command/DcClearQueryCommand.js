/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.dc.command.DcClearQueryCommand", {
	extend : "e4e.dc.command.AbstractDcSyncCommand",

	dcApiMethod : e4e.dc.DcActionsFactory.CLEAR_QUERY,

	onExecute : function() { // parameters: options

		var dc = this.dc;
		dc.advancedFilter = [];

		var fcf = {};
		var fcp = {};

		if (dc.flowContext) {
			fcf = dc.flowContext.filter;
			fcp = dc.flowContext.params;
		}

		var k;
		// reset fields
		for ( k in dc.filter.data) {
			if (fcf[k] === undefined) {
				dc.setFilterValue(k, null, false, "clearQuery");
			} else {
				dc.setFilterValue(k, fcf[k], false, "clearQuery");
			}
		}

		// reset params used for filter
		var p = dc.params;
		if (p) {
			for ( k in p.data) {
				if (fcp[k] === undefined) {
					dc.setParamValue(k, null, false, "clearQuery");
				} else {
					dc.setParamValue(k, fcp[k], false, "clearQuery");
				}
			}
		}

		// apply context filter
		if (dc.dcContext) {
			dc.dcContext._updateChildFilter_();
		}
		
		dc.store.loadData([], false); 
	},

	isActionAllowed : function() {
		if (e4e.dc.DcActionsStateManager.isClearQueryDisabled(this.dc)) {
			this.dc.warning(Main.msg.DIRTY_DATA_FOUND, "msg");
			return false;
		}
	}

});