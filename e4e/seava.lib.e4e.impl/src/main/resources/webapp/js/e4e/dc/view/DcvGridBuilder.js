/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
/**
 * Builder for read-only grid views.
 */
Ext.define("e4e.dc.view.DcvGridBuilder", {
	extend : "Ext.util.Observable",

	/**
	 * Target grid
	 */
	dcv : null,

	addTextColumn : function(config) {
		config.xtype = "gridcolumn";
		config.field = {
			xtype : 'textfield',
			readOnly : true
		};
		this.applySharedConfig(config);
		return this;
	},

	addBooleanColumn : function(config) {
		config.xtype = "booleancolumn";
		Ext.apply(config, {
			trueText : Main.translate("msg", "bool_true"),
			falseText : Main.translate("msg", "bool_false")
		});
		Ext.applyIf(config, {
			width : Main.viewConfig.BOOLEAN_COL_WIDTH
		});
		this.applySharedConfig(config);
		return this;
	},

	addDateColumn : function(config) {
		config.xtype = "datecolumn";
		Ext.applyIf(config, {
			_mask_ : Masks.DATE
		});
		config.format = Main[config._mask_];
		this.applySharedConfig(config);
		return this;
	},
	
	addTimeColumn : function(config) {
		config.xtype = "datecolumn";
		Ext.applyIf(config, {
			_mask_ : Masks.TIME
		});
		config.format = Main[config._mask_];
		this.applySharedConfig(config);
		return this;
	},

	addNumberColumn : function(config) {
		if (typeof(_SYSTEMPARAMETERS_) !== "undefined" && typeof(_SYSTEMPARAMETERS_) !== "null" && config.sysDec && !config.decimals ){
			var d = _SYSTEMPARAMETERS_[config.sysDec];
			if( Ext.isNumeric(d) ){
				config.decimals = 1*d;
			}
		}
		config.xtype = "numbercolumn";
		Ext.applyIf(config, {
			align : "right",
			format : Main.getNumberFormat(config.decimals || 0)
		});
		this.applySharedConfig(config);
		return this;
	},
	
	addButtonColumn : function(config) {
		Ext.applyIf(config, {
            xtype: "widgetcolumn",
			noSort : true,
			noFilter : true,
            widget: {
                xtype: "button",
                glyph: config.glyph,
                handler: config.handler,
                listeners : config.listeners,
                disabled : config.disabled,
                _enableFn_ : config._enableFn_,
                _getController_ : function(){
                	var ctr = null;
                	try {
                		ctr = this.getWidgetColumn().getView().grid._controller_;
                	}catch(ex){
                		// nothing to do
                	}
                	return ctr;
                }
            },
            onWidgetAttach : function(col,item,record){
        		if( item && Ext.isFunction(item._enableFn_) ){
        			if( item._enableFn_(record,item) ){
        				item.enable();
        			} else {
        				item.disable();
        			}
        		}
            }
		});
		this.applySharedConfig(config);
		return this;
	},

	shouldCreateField : function(r, cols, name) {
        return (r.fieldsMap[name] && !cols.containsKey(name));
    },
    
	addDefaults : function() {
		var r = Ext.create(this.dcv._controller_.recordModel, {});
		var cols = this.dcv._columns_;

		if (this.shouldCreateField(r, cols, "notes")) {
			this.addTextColumn({
				name : "notes",
				dataIndex : "notes",
				hidden : true,
				width : 150,
				alwaysHidden : true,
				noSort : true,
				noFilter : true
			});
		}

		if (this.shouldCreateField(r, cols, "createdAt")) {
			this.addDateColumn({
				name : "createdAt",
				dataIndex : "createdAt",
				hidden : true,
				width : 140,
				_mask_ : Masks.DATETIMESEC,
				//alwaysHidden : true,
				noSort : false,
//				noFilter : true
			});
		}

		if (this.shouldCreateField(r, cols, "modifiedAt")) {
			this.addDateColumn({
				name : "modifiedAt",
				dataIndex : "modifiedAt",
				hidden : true,
				width : 140,
				_mask_ : Masks.DATETIMESEC,
				//alwaysHidden : true,
				noSort : false,
				//noFilter : true
			});
		}

		if (this.shouldCreateField(r, cols, "createdBy")) {
			this.addTextColumn({
				name : "createdBy",
				dataIndex : "createdBy",
				hidden : true,
				width : 100,
				//alwaysHidden : true,
				noSort : false,
				//noFilter : true
			});
		}

		if (this.shouldCreateField(r, cols, "modifiedBy")) {
			this.addTextColumn({
				name : "modifiedBy",
				dataIndex : "modifiedBy",
				hidden : true,
				width : 100,
				//alwaysHidden : true,
				noSort : false,
				//noFilter : true
			});
		}

		if (this.shouldCreateField(r, cols, "id")) {
			this.addTextColumn({
				name : "id",
				dataIndex : "id",
				hidden : true,
				width : 100,
				alwaysHidden : true,
				noSort : true,
				noFilter : true
			});
		}

		if (this.shouldCreateField(r, cols, "refid")) {
			this.addTextColumn({
				name : "refid",
				dataIndex : "refid",
				hidden : true,
				width : 100,
				//alwaysHidden : true,
				noSort : false,
				//noFilter : true
			});
		}
		return this;
	},

	add : function(config) {
		this.applySharedConfig(config);
		return this;
	},

	merge : function(name, config) {
		Ext.applyIf(this.dcv._columns_.get(name), config);
		return this;
	},

	change : function(name, config) {
		Ext.apply(this.dcv._columns_.get(name), config);
		return this;
	},

	remove : function(name) {
		this.dcv._columns_.remove(name);
		return this;
	},

	// private

	applySharedConfig : function(config) {
		// read the settings from recordModel
		var rm = this.dcv._controller_.recordModel;
		var rf = rm.fieldsMap[config.dataIndex] || {};
		var rcfg = {
			noSort : rf.noSort,
			noFilter : rf.noFilter,
			alwaysHidden : rf.alwaysHidden
		};
		// but keep the settings from given config if they exists
		Ext.applyIf(config,rcfg)
		
		Ext.applyIf(config, {
			id : Ext.id(),
			sortable : (config.noSort===true) ? false : true,
			hidden : (config.alwaysHidden===true) ? true : false
		});
		this.dcv._columns_.add(config.name, config);
	},

	__isSpecialColumn__ : function(name){
		var sc = ["id","createdAt","createdBy","version","clientId"];
		var l = sc.length, i;
		for(i=0;i<l;i++){
			if(sc[i] === name){
				return true;
			}
		}
		return false;
	},
	
	addAllFromDataSource : function() {

		var f = this.dcv._controller_.ds.recordFields;
		var i, len = f.length;
		for (i=0; i<len; i++) {
			var name = f[i]["name"];
			var type = f[i]["type"];
			
			var cfg = {
				name : name,
				dataIndex : name
			};
			
			// try to guess something
			if (this.__isSpecialColumn__(name)) {
				cfg.hidden = true;
				cfg.noSort = true;
			}

			if (type === "string") {
				this.addTextColumn(cfg);
			} else 
			if (type === "date") {
				this.addDateColumn(cfg);
			} else
			if (type === "boolean") {
				this.addBooleanColumn(cfg);
			} else
			if (type === "int") {
				this.addNumberColumn(cfg);
			}
		}
	},


	// view and filter toolbar related functions
	
	addDisplayFieldText : function(config) {
		config.xtype = "displayfieldtext";
		Ext.applyIf(config, {
			//anchor : "-20",
			cls : "sone-toolbar-displayfield"
		});
		this.tbiApplySharedConfig(config);
		return this;
	},

	addDisplayFieldNumber : function(config) {
		config.xtype = "displayfieldnumber";
		Ext.applyIf(config, {
			//anchor : "-20",
			format : Main.getNumberFormat(config.decimals || 0),
			cls : "sone-toolbar-displayfieldnumber"
		});
		this.tbiApplySharedConfig(config);
		return this;
	},

	addDisplayFieldDate : function(config) {
		config.xtype = "displayfielddate";
		Ext.applyIf(config, {
			//anchor : "-20",
			cls : "sone-toolbar-displayfield"
		});
		this.tbiApplySharedConfig(config);
		return this;
	},

	addDisplayFieldBoolean : function(config) {
		config.xtype = "displayfieldboolean";
		Ext.applyIf(config, {
			// anchor:"-20" ,
			cls : "sone-toolbar-displayfield"
		});
		this.tbiApplySharedConfig(config);
		return this;
	},
	
	tbiApplySharedConfig : function(config) {
		Ext.applyIf(config, {
			id : Ext.id(),
			_onViewToolbar_ : true,
			_enableAutoLabelWidth_ : true,
			_dcView_ : this.dcv,
			labelSeparator : Main.viewConfig.FORM_LABEL_SEPARATOR,
			labelAlign : "right",
			align : "left"
		});
		
		this.dcv._elems_.add(config.name, config);
	}
	
});
