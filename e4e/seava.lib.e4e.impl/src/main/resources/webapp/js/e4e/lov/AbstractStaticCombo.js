/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.lov.AbstractStaticCombo", {

	extend : "Ext.form.field.ComboBox",
	alias : "widget.xstaticcombo",

	// Properties

	cls : "",
	pageSize : 0,

	/**
	 * Data-control view type this field belongs to. Injected by the corresponding view builder.
	 */
	_dcView_ : null,

	_valueValidated_ : 0, // -1 = not in db; 0 = force selection is not set, val is not in db; 1 = value in db
	_onFocusValueValidated_ : 0,

	initComponent : function() {
		this.callParent(arguments);
	},

	assertValue : function() {
		this.callParent(arguments);
		if( !this._isDisabled_() ){
			var val = this.getRawValue();
			if (val === this._onFocusVal_) {
				this._valueValidated_ = this._onFocusValueValidated_;
			} else {
				if (Ext.isEmpty(val)) {
					this._valueValidated_ = 0;
				} else {
					var displayValue = this.getDisplayValue();
					var rec = this.findRecordByDisplay(displayValue);
					if( rec ){
						this._valueValidated_ = 1;
					} else {
						this._valueValidated_ = -1;
					}
				}
			}
		}
		this.isValid();
	},

	clearUiFlags : function() {
		this._valueValidated_ = 0;
		this.isValid();
	},

	_setUiFlags_ : function(isV) {
		if (this.inEditor()) {
			return;
		}
		if (isV >= 0 || isV === true) {
			this.removeCls("combo-box-check-fail");
			if (this._valueValidated_ > 0) {
				this.addCls("combo-box-check-ok");
			} else {
				this.removeCls("combo-box-check-ok");
			}
		} else {
			this.removeCls("combo-box-check-ok");
			this.addCls("combo-box-check-fail");
		}
	},

	isValid : function() {
    	if( this._isDisabled_() ){
    		this._valueValidated_ = 0;
    		return this.callParent(arguments);
    	}
		var isV = this._valueValidated_;
		if (isV >= 0) {
			isV = this.callParent(arguments);
		}
		this._setUiFlags_(isV);
		return (isV >= 0) ? true : (isV < 0) ? false : isV;
	},

	onFocus : function() {
		this.callParent(arguments);
		this._onFocusValueValidated_ = this._valueValidated_;
	},

	__inEditor__ : null,
	inEditor : function() {
		if (this.__inEditor__ === null) {
			var dcv = this._dcView_;
			if (dcv) {
				var vt = dcv._dcViewType_;
				this.__inEditor__ = (vt === "edit-grid" || vt === "filter-propgrid" || vt === "edit-propgrid" || vt === "bulk-edit-field");
			} else {
				return false;
			}
		}
		return this.__inEditor__;
	}
});
