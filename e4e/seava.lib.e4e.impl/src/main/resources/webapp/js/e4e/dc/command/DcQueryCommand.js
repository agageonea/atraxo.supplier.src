/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.dc.command.DcQueryCommand", {
	extend : "e4e.dc.command.AbstractDcAsyncCommand",

	dcApiMethod : e4e.dc.DcActionsFactory.RUN_QUERY,

	errorTpl : Ext.create('Ext.XTemplate', [
	                            			'<ul style="list-style-type: none;padding:0; margin:0;">',
	                            			'<tpl for=".">', '<li style="list-style-type: none;">',
	                            			'<span class="field-name">', Main.translate("ds", "fld"),
	                            			' `{fieldTitle}` </span>', '<span class="error">{message}</span>',
	                            			'</li></tpl></ul>' ]),
	
	onExecute : function(options) {
		var dc = this.dc;
		var _p = dc.buildRequestParamsForQuery();
		Ext.apply(dc.store.proxy.extraParams, _p);
		dc.store.load({
			callback : function(records, operation, success) {
				
				this.onAjaxResult({
					records : records,
					response : operation._response, // Tibi: E5
					operation : operation,
					options : options,
					success : success,
				});

				if (options.viewFiltersReset === true) {
					var resetOptions = {};
					if (options.resetOptions) {
						resetOptions = options.resetOptions;
					}
					dc.fireEvent("afterViewFiltersReset", this, resetOptions);
				}
				
			},
			scope : this,
			options : options,
			page : 1
		});
		// the paging doesn't get refreshed as the store doesn't update its
		// currentPage. Force it here, hopefully will be fixed
		this.dc.store.currentPage = 1;
	},

	isActionAllowed : function() {
		if (e4e.dc.DcActionsStateManager.isQueryDisabled(this.dc)) {
			this.dc.warning(Main.msg.DC_QUERY_NOT_ALLOWED, "msg");
			return false;
		}
		var res;
		var dc = this.dc;
		// new style validation: check if valid based on filter model
		res = this.isValid(dc.getFilter(), Main.translate("msg","invalid_filter"));
		if( res ){
			res = this.isValid(dc.getParams(), Main.translate("msg","invalid_param"));
		}
		// old style validation: check if valid based on the UI items - filter panel
		if (res && !dc.filter.isValid()) {
			this.dc.error(Main.msg.INVALID_FILTER, "msg");
			res = false;
		}
		if(!res){
			this.dc.store.lastOptions = null;
		}
		return res;
	},
	
	addFieldNameToError : function(item) {
		var v = Main.translateModelField(this.dc._trl_, item.field);
		item["fieldTitle"] = v;
	},
	
	isValid : function(obj, title) {
		if(obj){
			var errors = obj.validate();
			if (!errors.isValid()) {
				errors.each(this.addFieldNameToError, this);
				Ext.Msg.show({
					title : title,
					msg : this.errorTpl.apply(errors.getRange()),
					icon : Ext.MessageBox.ERROR,
					buttons : Ext.MessageBox.OK
				});
				return false;
			}
		}
		return true;
	}

});
