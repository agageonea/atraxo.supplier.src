/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("e4e.base.WfAbstractFormWindowExtjs", {
	extend : "Ext.Window",

	border : true,
	resizable : true,
	closable : true,
	constrain : true,
	layout : "fit",
	modal : true,
	buttonAlign : "center",

	_formProperties_ : null,

	_buildFormFields_ : function(fields) {

		for ( var i = 0, l = this._formProperties_.length; i < l; i++) {
			var p = this._formProperties_[i];
			var f = {
				name : p.id,
				fieldLabel : p.name,
				value : p.value
			}
			if (p.isRequired) {
				f.allowBlank = false;
				f.labelSeparator = Main.viewConfig.FORM_LABEL_SEPARATOR_FOR_MANDATORY;
			}
			if (!p.isWritable) {
				f.readOnly = true;
			}
			if (p.type.name === "string") {
				f.xtype = "textfield";
			}
			if (p.type.name === "long") {
				f.xtype = "numberfield";
			}
			if (p.type.name === "date") {
				f.xtype = "datefield";
				f.format = "Y-m-d";

			}
			if (p.type.name === "enum") {
				f.xtype = "combobox";
				f.store = p.type.values;
			}
			fields[fields.length] = f;
		}
	}
});