/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
 /**
  * Builder for filter property grid views.
  */
 Ext.define("e4e.dc.view.DcvFilterPropGridBuilder", {
     extend : "Ext.util.Observable",

     dcv : null,

     addTextField : function(config) {
         if (!config.editor) {
             config.editor = {};
         }
         var e = config.editor;
         config._type_ = 'string';
         if (e.maxLength) {
             e.enforceMaxLength = true;
         }
         if (e.caseRestriction) {
             e.fieldStyle += "text-transform:" + e.caseRestriction + ";";
         }
         config.editorInstance = Ext.create('Ext.form.field.Text', e);

         config._default_ = "";
         this.applySharedConfig(config);
         return this;
     },

     addDateField : function(config) {

         if (!config.editor) {
             config.editor = {};
         }
         config._type_ = 'date';

         var e = config.editor;
         config.editorInstance = Ext.create('Ext.form.field.Date', Ext.applyIf(e, {
             format : Main.DATE_FORMAT,
             _mask_ : Masks.DATE,
 			 altFormats : Main.ALT_FORMATS,
             selectOnFocus : true
         }));
         config._default_ = "";
         config.renderer = config.renderer || Ext.util.Format.dateRenderer(Main.DATE_FORMAT);
         this.applySharedConfig(config);
         return this;
     },

     addDateTimeField : function(config) {

         if (!config.editor) {
             config.editor = {};
         }
         config._type_ = 'date';

         var e = config.editor;
         config.editorInstance = Ext.create('Ext.form.field.Date', Ext.applyIf(e, {
             format : Main.DATETIME_FORMAT,
             _mask_ : Masks.DATETIME,
 			 altFormats : Main.ALT_FORMATS,
             selectOnFocus : true
         }));
         config._default_ = "";
         config.renderer = config.renderer || Ext.util.Format.dateRenderer(Main.DATETIME_FORMAT);
         this.applySharedConfig(config);
         return this;
     },
     
     addNumberField : function(config) {
         if (!config.editor) {
             config.editor = {};
         }
         config._type_ = 'number';
         var e = config.editor;
         config.editorInstance = Ext.create('Ext.form.field.Number', Ext
                 .applyIf(e, {
                     fieldStyle : "text-align:right;",
                     selectOnFocus : true
                 }));
         this.applySharedConfig(config);
         return this;
     },

     addLov : function(config) {
         config._type_ = 'string';
         if (config.editor) {
             try {
                 config.editorInstance = Ext.create(config.editor.xtype, config.editor);
                 config._default_ = "";
             } catch (e) {
                 alert('Cannot create LOV editor from xtype `' + config.editor.xtype + '`');
             }
         }
         this.applySharedConfig(config);
         return this;
     },

     addCombo : function(config) {
         config._type_ = 'string';
         if (!config.editor) {
             config.editor = {}; 
             config.editor.pageSize = 0;
             config.editor.store = config.store;
             if (config.selectOnFocus === false) {
                 config.editor.selectOnFocus = false;
             } else {
                 config.editor.selectOnFocus = true;
             }
             if (config.listeners) {
            	 config.editor.listeners = Ext.apply({}, config.listeners);
             }
         }
         var e = config.editor;
         
         Ext.applyIf(e, {
             forceSelection : true
         });
         if (e.maxLength) {
             e.enforceMaxLength = true;
         }
         if (e.caseRestriction) {
             e.fieldStyle += "text-transform:" + e.caseRestriction + ";";
         }
         config.editorInstance = Ext.create('Ext.form.field.ComboBox', e);
         config._default_ = "";
         this.applySharedConfig(config);
         return this;
     },

     addBooleanField : function(config) {
         config._type_ = 'boolean';
         config.editor = Ext.applyIf(config.editor || {}, {
             forceSelection : false,
             pageSize : 0
         });
         Ext.applyIf(config, {
             _default_ : null
         });
         var yesNoStore = Main.createBooleanStore();
         config.editorInstance = Ext.create('Ext.form.field.ComboBox', Ext
                 .apply(config.editor, {
                     queryMode : "local",
                     valueField : "bv",
                     displayField : "tv",
                     triggerAction : "all",
                     store : yesNoStore
                 }));

         config.renderer = function(v) {
             if (v == null) {
                 return "";
             }
             return Main.translate("msg", "bool_" + ((!!v)));
         }
         this.applySharedConfig(config);
         return this;
     },

 	addToggleField : function(config) {
		return this.addBooleanField(config);
	},

     // ==============================================

     applySharedConfig : function(config) {
         Ext.applyIf(config, {
             id : Ext.id()
         });
         if (config.editorInstance) {
             config.editorInstance._dcView_ = this.dcv;
             config.editorInstance.selectOnFocus = true;
             config.editorInstance.enableKeyEvents = true;
             config.editorInstance.addManagedListener(config.editorInstance, "keydown", function(f, e) {
                 if (KeyBindings.keyEventBelongsToKeyBindings(e)) {
                     e.stopEvent();
                 }
             });
         }
         if (config.allowBlank === false) {
             config.labelSeparator = Main.viewConfig.FORM_LABEL_SEPARATOR_FOR_MANDATORY;
         }
         // check the model validation presence
         var msg, i, field, 
         	fields = this.dcv._controller_.filter.fields;
         var len = fields.length;
         for (i = 0; i < len; i++) {
             field = fields[i];
             if( field.name === config.dataIndex ) {
	             if (field.validate && !field.validate.$nullFn) {
	                 msg = field.validate(null, null, null);
	                 if (msg !== true) {
	                	 config.labelSeparator = Main.viewConfig.FORM_LABEL_SEPARATOR_FOR_MANDATORY;
	                	 config.allowBlank = false;
	                 }
	             }
	             break;
             }
         }

         this.dcv._elems_.add(config.name, config);
     }

 });
