/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
/**
 */
Ext.define("e4e.dc.view.DcvEditFormBuilder", {
	extend : "Ext.util.Observable",

	dcv : null,

	addLabel : function(config) {
		config.xtype = "label";
		this.applySharedConfig(config);
		return this;
	},

	addTextField : function(config) {
		config.xtype = "textfield";
		this.applySharedConfig(config);
		return this;
	},
	
	addUploadField: function(config) {
		config.xtype = "fileuploadfield";
        if (config._hasLabelForButton_ === true) {
            config.buttonText = this.dcv.translate(config.name + "__btn_lbl");
        }
		this.applySharedConfig(config);
		return this;
	},

	addHtmlEditor : function(config) {
		config.xtype = "htmleditor";
		Ext.applyIf(config, {
			selectOnFocus : false
		});
		this.applySharedConfig(config);
		return this;
	},
	
	addTextArea : function(config) {
		config.xtype = "textarea";
		Ext.applyIf(config, {
			selectOnFocus : false
		});
		this.applySharedConfig(config);
		return this;
	},

	addPopoverTextField : function(config) {
		config.xtype = "fppopovertextfield";
		Ext.applyIf(config, {
			noEdit : true
		});
		this.applySharedConfig(config);
		return this;
	},
	
	addPopoverTextArea : function(config) {
		config.xtype = "fppopovertextarea";
		Ext.applyIf(config, {
			selectOnFocus : false,
			noEdit : true
		});
		this.applySharedConfig(config);
		return this;
	},
	
	addBooleanField : function(config) {
		return this.addCheckbox(config);
	},

	addCheckbox : function(config) {
		config.xtype = "checkbox";
		config.value = false;
		config.checked = false;
		config.uncheckedValue = false;
		this.applySharedConfig(config);
		return this;
	},
	
	addToggleField : function(config) {
		config.xtype = "togglebutton";
		this.applySharedConfig(config);
		return this;
	},

	addDateField : function(config) {
		config.xtype = "datefield";
		Ext.applyIf(config, {
			_mask_ : Masks.DATE,
			altFormats : Main.ALT_FORMATS
		});
		config.format = Main[config._mask_];
		this.applySharedConfig(config);
		return this;
	},

	addDateTimeField : function(config) {
		config.xtype = "datefield";
		Ext.applyIf(config, {
			_mask_ : Masks.DATETIME,
			altFormats : Main.ALT_FORMATS
		});
		config.format = Main[config._mask_];
		this.applySharedConfig(config);
		return this;
	},

	addTimeField : function(config) {
		config.xtype = "timefield";
		Ext.applyIf(config, {
			_mask_ : Masks.TIME,
			growToLongestValue : false,
			enforceMaxLength : true,
			maxLength : 11,
			increment : 1,
			forceSelection : true
		});
		config.format = Main[config._mask_];
		this.applySharedConfig(config);
		return this;
	},
	
	addNumberField : function(config) {
		if (typeof(_SYSTEMPARAMETERS_) !== "undefined" && typeof(_SYSTEMPARAMETERS_) !== "null" 
				&& config.sysDec && !config.decimals ){
			var d = _SYSTEMPARAMETERS_[config.sysDec];
			if( Ext.isNumeric(d) ){
				config.decimals = 1*d;
			}
		}
		
		if( !Ext.isEmpty(config.maxValue) && !Ext.isEmpty(config.minValue) ){
			Ext.applyIf(config, { xtype : "numberfield" });
		} else {
			Ext.applyIf(config, { xtype : "fpnumberfield" });
			if (!this.dcv._numberfields_) {
				this.dcv._numberfields_ = [];
			}
			this.dcv._numberfields_[this.dcv._numberfields_.length] = config.name;
		}

		Ext.applyIf(config, {
			format : Main.getNumberFormat(config.decimals || 0),
			hideTrigger : true,
			keyNavEnabled : false,
			mouseWheelEnabled : false,
			maxLength : 19
		});
		// from database we receive the data with at least 6 decimals; don't truncate to less 
		if (config.decimals) {
			config.decimalPrecision = (config.decimals<6)?6:config.decimals;
			config.maxLength = config.maxLength + 2;
		} else {
			config.maxLength = config.maxLength + 1;
			config.decimalPrecision = 6;
		}
		config.fieldStyle = (config.fieldStyle) ? config.fieldStyle	+ ";text-align:right;" : "text-align:right;";
		this.applySharedConfig(config);
		return this;
	},

	addLov : function(config) {
		Ext.applyIf(config, {
			forceSelection : true
		});
		this.applySharedConfig(config);
		return this;
	},
	
	addCombo : function(config) {
		if( config.multiSelect === true ) {
			Ext.apply(config, {
				xtype : "tagfield",
			});
			Ext.applyIf(config, {
				autoSelect: false,
				growMax: 1
			});
		} else {
			Ext.apply(config, {
				xtype : "xstaticcombo"
			});
			Ext.applyIf(config, {
				forceSelection : true
			});
		}
		this.applySharedConfig(config);
		return this;
	},
	
	addLovAsgn : function(config) {
		Ext.applyIf(config, {
			forceSelection : true,
		});
		this.applySharedConfig(config);
		return this;
	},
	
	processRadioFieldElements : function(config){
		// config - radio field config
		var l = config._elements_.length;
		var items = [];
		for(var i=0; i<l; i++){
			var e = config._elements_[i];
			var x = {};
			Ext.apply(x,e);
			x.name = config.name;
			x.boxLabel = x.inputValue;			
			x.listeners = {
				change: {
					scope: this,
					fn: function(el,n,o) {
						if (n === true) {
							var view = el._groupObj_._dcView_;
							var ctrl = view._controller_;
							var rec = ctrl.getRecord();
							if (rec) {
								el._groupObj_.setValue(el.boxLabel);
								if (el._groupObj_.dataIndex) {
									var oldV = rec.get(el._groupObj_.dataIndex);
									rec.set(el._groupObj_.dataIndex, el.boxLabel);
									el._groupObj_.fireEvent('change', el._groupObj_, el.boxLabel, oldV);
								}
							}							
						}
					}
				}
			},
			items.push(x);
		}
		return items;
	},
	
	addRadioField : function(config){
		this.dcv._onBeforeAddRadioField_(this,config);
		config.xtype = "fpradiogroup";
		config.items = this.processRadioFieldElements(config);		
		this.applySharedConfig(config);		
		return this;
	},

	addHiddenField : function(config) {
		config.xtype = "hiddenfield";
		Ext.applyIf(config, {
			maxLength : 20
		});
		this.applySharedConfig(config);
		return this;
	},

	addImage : function(config) {
		config.xtype = "image";
		Ext.applyIf(config, {
			border : true,
			style : {
				backgroundColor : "#fff",
				border : "1px solid #777"
			}
		});
		if (this.dcv._images_ == null) {
			this.dcv._images_ = [];
		}
		this.dcv._images_[this.dcv._images_.length] = config.name;
		this.applySharedConfig(config);
		return this;
	},

	addDisplayFieldText : function(config) {
		config.xtype = "displayfieldtext";
		Ext.applyIf(config, {
			anchor : "-20",
			fieldCls : "displayfield"

		});
		this.applySharedConfig(config);
		return this;
	},

	addDisplayFieldNumber : function(config) {
		config.xtype = "displayfieldnumber";
		Ext.applyIf(config, {
			anchor : "-20",
			format : Main.getNumberFormat(config.decimals || 0),
			fieldCls : "displayfieldnumber"
		});
		this.applySharedConfig(config);
		return this;
	},

	addDisplayFieldDate : function(config) {
		config.xtype = "displayfielddate";
		Ext.applyIf(config, {
			anchor : "-20",
			fieldCls : "displayfield"
		});
		this.applySharedConfig(config);
		return this;
	},

	addDisplayFieldBoolean : function(config) {
		config.xtype = "displayfieldboolean";
		Ext.applyIf(config, {
			// anchor:"-20" ,
			fieldCls : "displayfield"
		});
		this.applySharedConfig(config);
		return this;
	},

	addPanel : function(config) {
		Ext.applyIf(config, this.dcv.defaults);
		config.listeners = config.listeners || {};
		var d = config.defaults || {};
		var cd = config.childrenDefaults || {};
		Ext.apply(d, cd)
		Ext.applyIf(config, {
			defaults : d,
			xtype : "container",
			id : Ext.id()
		});
		if (config._hasTitle_ === true) {
			config.title = this.dcv.translate(config.name + "__ttl");
		}
		
		if( config._parentCssCls_ && !config.listeners.afterrender ) {
			var afterrender = {
				scope : this.frame,
				single : true,
				fn : function(e) {
					if( e._parentCssCls_ ){
						var te = e.getTargetEl();
						if( te ){
							te.addCls(e._parentCssCls_);
						}
					}
				}
			};
			config.listeners.afterrender = afterrender;
		}
		
		this.dcv._elems_.add(config.name, config);
		return this;
	},

	addFieldContainer : function(config) {
		Ext.applyIf(config, {
			xtype : 'fieldcontainer',
			layout : 'hbox',
			combineErrors : true,
			defaults : {
				flex : 1,
				hideLabel : true
			}
		});
		this.dcv._elems_.add(config.name, config);
		return this;
	},

	addButton : function(config) {
		Ext.applyIf(config, {
			id : Ext.id(),
			xtype : "button"
		});
		if( ""+config.forClpsAndExp === "true" ){
			config.menu = new Ext.menu.Menu({
				items: []
			});
		}
		this.dcv._elems_.add(config.name, config);
		return this;
	},

	addChildrenTo : function(c, list) {
		var items = this.dcv._elems_.get(c)["items"] || [];
		for (var i = 0, len = list.length; i < len; i++) {
			items[items.length] = this.dcv._elems_.get(list[i]);
		}
		this.dcv._elems_.get(c)["items"] = items;
		return this;
	},

	add : function(config) {
		this.applySharedConfig(config);
		return this;
	},

	merge : function(name, config) {
		Ext.applyIf(this.dcv._elems_.get(name), config);
		return this;
	},

	change : function(name, config) {
		Ext.apply(this.dcv._elems_.get(name), config);
		return this;
	},

	remove : function(name) {
		this.dcv._elems_.remove(name);
		return this;
	},
	
	// for wizard form
	
	addWizardLabels : function(c, list){
		var dcv = this.dcv;
		var createWizardLabels = function() {
			var res = [];
			var lnMargin = 100;
			if (dcv._wizardLineMargin_) {
				lnMargin = dcv._wizardLineMargin_;
			} else {
				if( !Ext.isEmpty(dcv._windowWidth_) ){
					if( dcv._windowWidth_ <= 900 ){
						lnMargin = Math.round((dcv._windowWidth_-20*list.length) / (list.length+2));
					} else {
						lnMargin = Math.round((dcv._windowWidth_-50*list.length) / (list.length+2));
					}
				}
			}
			res.push("<div class='sone-wizard-line' style='left:"+lnMargin+"px; right: "+lnMargin+"px'>",
				"</div>",
				"<div class='sone-wizard-wrapper'>");
			for (var i = 0, len = list.length; i < len; i++) {
				var lbl = dcv._elems_.get(list[i]);
				lbl["_dcView_"] = dcv;
				Main.translateField(dcv._trl_, dcv._controller_._trl_, lbl);
				res.push("<div class='sone-wizard-step step-"+(i+1)+" step-pending'>",
					"<div class='sone-step-body'>",
						"<div class='sone-step-box'>",
							"<div class='sone-step-indicator'>",
								"<i class='fa fa-2x fa-times'></i>",
							"</div>",
							"<div class='sone-step-label'>",
							lbl.fieldLabel,
							"</div>",
						"</div>",
					"</div>",
				"</div>");
			}
		    res.push("</div>");
			return res;
		};
		var topPnlId = Ext.id();
		var centerPnlId = Ext.id();
		var form = dcv._elems_.get(c);
		var oldItems = dcv._elems_.get(c)["items"] || [];
		dcv._topPnlId_ = topPnlId;
		dcv._centerPnlId_ = centerPnlId;
		dcv._wizardMaxSteps_ = list.length;
		var cfg = {
			layout: "border",
			scrollable: false,
			items: [{
				region: "north",
				height: 110,
				id : topPnlId,
				html: createWizardLabels()
			}, {
				region: "center",
				id: centerPnlId,
				items: oldItems
			}]
		};
		Ext.apply(form, cfg);
		return this;
	},

	// private

	applySharedConfig : function(config) {
		Ext.applyIf(config, {
			id : Ext.id(),
			//itemId : config.name,
			selectOnFocus : true,
			_dcView_ : this.dcv,
			shrinkWrap : true  
		});
		
		if (config._visibleFn_ !== undefined && config._visibleFn_ !== null) {
			this.dcv._hasVisibilityRules_ = true;
		}
		if (config.maxLength) {
			config.enforceMaxLength = true;
		}
		if (config.caseRestriction) {
			config.fieldStyle = "text-transform:" + config.caseRestriction + ";";
		}
		if (config.allowBlank === false && config.noLabel !== true) {
			config.labelSeparator = Main.viewConfig.FORM_LABEL_SEPARATOR_FOR_MANDATORY;
		}
		this.dcv._elems_.add(config.name, config);
	}
});
