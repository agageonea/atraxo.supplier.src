/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
/**
 * Utility to display information about an application frame.
 */

Ext
		.define(
				"e4e.base.FrameInspector",
				{
					extend : "Ext.Window",

					title : Main
							.translate("appmenuitem", "frameInspector__lbl"),
					border : true,
					height : 500,
					width : 800,
					layout : 'fit',
					resizable : true,

					bodyPadding : 20,
					closable : true,
					constrain : true,
					modal : true,
					autoScroll : true,

					initComponent : function() {

						var _t = [
								'<h1 style="padding-top: 10px;padding-bottom: 10px;">Frame: {frameName}</h1>',
								'<p style=" padding-bottom: 5px;"> Class: {frameFqn} </p>',
								'<h3 style="padding-top: 10px;padding-bottom: 10px;"><u>Views</u></h3>',
								'<tpl for="view">',
								'<p style=" padding-bottom: 5px;">{#}. <i>Name:</i> {name} <br>',
								'<i>Class:</i> {className}<br><i>Fields:</i><br> <textarea style="width:100%; height:80px; border:1px solid #333333">{fields}</textarea>',
								'<br><i>View state:</i><br> <textarea style="width:100%; height:80px; border:1px solid #333333">{viewState}</textarea></p></tpl>',
								'<h3 style="padding-top: 10px;padding-bottom: 10px;"><u>Data-controls</u></h3>',
								'<tpl for="dc">',
								'<p style=" padding-bottom: 5px;">{#}. <i>Key:</i> {alias} <br> <i>Class:</i> {name} <br>',
								'<i>Data-source: </i> ',
								'<a href="#" onclick="javascript:var w=window.open( Main.dsAPI(\'{dsAlias}\',\'html\').info',
								',\'DataSourceInfo\',\'width=600,height=500,scrollbars=yes\');w.focus();" >{ds}</a>',
								'</p></tpl>',
								'<h3 style="padding-top: 10px;padding-bottom: 10px;"><u>Toolbars</u></h3> ',
								'<tpl for="tlb">',
								'<p style=" padding-bottom: 5px;">{#}.  <i>Key:</i>  {alias}, <i>Title:</i> `{title}`</p></tpl>',
								'<h3 style="padding-top: 10px;padding-bottom: 10px;"><u>Assignments</u></h3> ',
								'<p style=" padding-bottom: 5px;">Assignment components are visible only after they have been opened in the frame.</p>',
								'<tpl for="asgn">',
								'<p style=" padding-bottom: 5px;">{#}. <i>Title:</i> `{title}` <br> <i>Class:</i>  {className}</p></tpl>' ];

						this.tpl = new Ext.XTemplate(_t);
						var frame = getApplication().getActiveFrameInstance();
						
						if (frame) {
							var dcs = [], tlbs = [], asgns = [], views = [];
							
							if (frame._elems_ != null) {
								var elems = frame._elems_;
								var items = elems.items, l = items.length, i = 0;
								
								for (i; i < l; i++) {
									if (items[i].__dcViewType__ == "grid") {
										var dc = items[i]._controller_;
										var s = dc.store;
										var fields = s.getModel().getFields();
										var fieldsObj = {};
										var gridView = frame._get_(items[i].name);
										
										var viewState;
										if (!Ext.isEmpty(gridView)) {
											viewState = gridView._getViewState_()
										}
										
										Ext.each(fields, function(field) {
											fieldsObj[field.name] = null;
										}, this);
										
										
										var viewItem = {
											name: items[i].name,
											className: items[i].stateId,
											fields : JSON.stringify(fieldsObj),
											viewState: JSON.stringify(viewState)
										}
										views.push(viewItem);
									}
								}								
							}	
							
							if (frame._dcs_ != null) {
								frame._dcs_.each(function(item) {
									var dc = {
										name : item.$className,
										alias : item._instanceKey_,
										ds : item.recordModelFqn,
										dsAlias : item.dsName
									};
									dcs[dcs.length] = dc;
								}, this);
							}

							if (frame._tlbs_ != null) {
								frame._tlbs_.eachKey(
									function(key) {
										var tlb = {
											alias : key,
											title : ((frame._tlbtitles_) ? frame._tlbtitles_.get(key) || "-" : "-")
										};
										tlbs[tlbs.length] = tlb;
									}, this);
							}

							if (frame._asgns_ != null) {
								frame._asgns_.eachKey(function(key, item) {
									var asgn = {
										className : item.className,
										title : item.title
									}
									asgns[asgns.length] = asgn;
								}, this);
							}

							this.data = {
								frameFqn : frame.$className,
								frameName : frame.$className.substring(
										frame.$className.lastIndexOf('.') + 1,
										frame.$className.length),
								dc : dcs,
								view : views,
								tlb : tlbs,
								asgn : asgns
							};
						} else {
							this.html = "<br><br>Frame inspector works only for "
									+ " application frames not for the home panel !";
						}
						this.callParent(arguments);
					}

				});