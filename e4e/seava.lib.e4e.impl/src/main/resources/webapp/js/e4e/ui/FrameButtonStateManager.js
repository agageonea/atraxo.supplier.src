/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

/**
 * Defines states to be assigned to buttons/actions and register button/event
 * listeners.
 */
e4e.ui.FrameButtonStateManager = {

	register : function(btnName, state, dcName, frame, options) {
		this[state](btnName, state, dcName, frame, options);
		return;
	},

	/**
	 * Register dc-event to be listened by the specified button
	 */
	registerForDcEvent : function(btnName, frame, dc, eventName, options) {
		if( options.dcMon ){
			dc.mon(dc, eventName, function() {
				this._applyStateDc_(options);
			}, frame);
		} else {
			dc.mon(dc, eventName, function() {
				this._applyStateButton_(btnName,options.visibleRule);
			}, frame);
		}
	},

	/**
	 * Register store-event to be listened by the specified button
	 */
	registerForDcStoreEvent : function(btnName, frame, dc, eventName, options) {
		if( options.dcMon ){
			dc.mon(dc.store, eventName, function() {
				this._applyStateDc_(options);
			}, frame);
		} else {
			dc.mon(dc.store, eventName, function() {
				this._applyStateButton_(btnName,options.visibleRule);
			}, frame);
		}
	},

	/**
	 * Register frame to be listened by the specified button
	 * Now only is used to initialize the button state after frame rendered.
	 * In the future this functionality can be extended.
	 */
	registerForFrameEvent : function(btnName, frame, dc, eventName, options) {
		if( options.dcMon ){
			dc.mon(frame, eventName, function() {
				this._applyStateDc_(options);
			}, frame, {single: true});
		} else {
			dc.mon(frame, eventName, function() {
				this._applyStateButton_(btnName,options.visibleRule);
			}, frame, {single: true});
		}
	},

	// 
	rule_param : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForDcEvent(btnName, frame, theDc, "parameterValueChanged", options);
	},

	rule_filter : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForDcEvent(btnName, frame, theDc, "filterValueChanged", options);
	},

	rule_param_and_filter : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForDcEvent(btnName, frame, theDc, "parameterValueChanged", options);
		this.registerForDcEvent(btnName, frame, theDc, "filterValueChanged", options);
	},

	// record state based

	record_is_clean : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForDcEvent(btnName, frame, theDc, "statusChange", options);
		this.registerForDcEvent(btnName, frame, theDc, "recordChange", options);
		this.registerForDcStoreEvent(btnName, frame, theDc, "update", options);
	},

	record_is_dirty : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForDcEvent(btnName, frame, theDc, "statusChange", options);
		this.registerForDcEvent(btnName, frame, theDc, "recordChange", options);
		this.registerForDcStoreEvent(btnName, frame, theDc, "update", options);
	},
	
	no_record_or_record_is_clean : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForDcEvent(btnName, frame, theDc, "statusChange", options);
		this.registerForDcEvent(btnName, frame, theDc, "recordChange", options);
		this.registerForDcStoreEvent(btnName, frame, theDc, "update", options);
		this.registerForDcStoreEvent(btnName, frame, theDc, "datachanged", options);
	},
	
	dc_in_any_state : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForFrameEvent(btnName, frame, theDc, "afterrender", options);
		this.registerForDcEvent(btnName, frame, theDc, "recordChange", options);
		this.registerForDcStoreEvent(btnName, frame, theDc, "update", options);
		this.registerForDcStoreEvent(btnName, frame, theDc, "datachanged", options);
	},

	// record status based

	record_status_is_new : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForDcEvent(btnName, frame, theDc, "recordChange", options);
	},

	record_status_is_edit : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForDcEvent(btnName, frame, theDc, "recordChange", options);
	},

	// selection based

	selected_zero : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForDcEvent(btnName, frame, theDc, "selectionChange", options);
	},

	selected_one : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForDcEvent(btnName, frame, theDc, "selectionChange", options);
		this.registerForDcStoreEvent(btnName, frame, theDc, "update", options);
	},

	selected_one_clean : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForDcEvent(btnName, frame, theDc, "selectionChange", options);
		this.registerForDcStoreEvent(btnName, frame, theDc, "update", options);
	},

	selected_one_dirty : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForDcEvent(btnName, frame, theDc, "selectionChange", options);
		this.registerForDcStoreEvent(btnName, frame, theDc, "update", options);

	},

	selected_many : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForDcEvent(btnName, frame, theDc, "selectionChange", options);
	},

	selected_not_zero : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForDcEvent(btnName, frame, theDc, "selectionChange", options);
		// also monitor the update event for selected_not_zero, because this rule generally is used together with a custom function,
		// which function result many times depends on the state of the current selected record (see item: SONE-2122)
		this.registerForDcStoreEvent(btnName, frame, theDc, "update", options);
	},
	
	list_not_empty : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForDcStoreEvent(btnName, frame, theDc, "load", options);
		this.registerForDcStoreEvent(btnName, frame, theDc, "clear", options);
		this.registerForDcStoreEvent(btnName, frame, theDc, "update", options);
	},
	
	list_is_empty : function(btnName, state, dcName, frame, options) {
		var theDc = frame._getDc_(dcName);
		this.registerForDcStoreEvent(btnName, frame, theDc, "load", options);
		this.registerForDcStoreEvent(btnName, frame, theDc, "clear", options);
		this.registerForDcStoreEvent(btnName, frame, theDc, "update", options);
	},
	
	// helper functions

	is_rule_param : function() {
		return true;
	},
	is_rule_filter : function() {
		return true;
	},
	is_rule_param_and_filter : function() {
		return true;
	},

	is_record_is_clean : function(dc) {
		return dc.getRecord() && !dc.isCurrentRecordDirty();
	},
	
	is_no_record_or_record_is_clean : function(dc) {
		var rec = dc.getRecord();
		return (dc.isStoreEmpty()) || (rec && !dc.isCurrentRecordDirty());
	},
	
	is_dc_in_any_state : function() {
		return true;
	},

	is_record_is_dirty : function(dc) {
		return dc.getRecord() && dc.isCurrentRecordDirty();
	},

	is_record_status_is_new : function(dc) {
		return dc.getRecordStatus() === 'insert'
	},

	is_record_status_is_edit : function(dc) {
		return dc.getRecordStatus() === 'update'
	},

	is_selected_zero : function(dc) {
		return dc.selectedRecords.length === 0;
	},

	is_selected_one : function(dc) {
		return dc.selectedRecords.length === 1;
	},

	is_selected_one_clean : function(dc) {
		return dc.selectedRecords.length === 1 && !dc.isCurrentRecordDirty();
	},

	is_selected_one_dirty : function(dc) {
		return dc.selectedRecords.length === 1 && dc.isCurrentRecordDirty();
	},

	is_selected_many : function(dc) {
		return dc.selectedRecords.length > 1;
	},

	is_selected_not_zero : function(dc) {
		return dc.selectedRecords.length > 0;
	},
	
	is_list_not_empty : function(dc) {
		return !dc.isStoreEmpty();
	},
	
	is_list_is_empty : function(dc) {
		return dc.isStoreEmpty();
	}

};
