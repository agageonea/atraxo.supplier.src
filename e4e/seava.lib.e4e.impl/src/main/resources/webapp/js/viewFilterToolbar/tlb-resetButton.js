Ext.define("Reset.Button", {
    extend: 'Ext.button.Button',

    _grid_: null,
    _standardFilterData_: null,
    _advancedFilterData_: null,
    _filterCombo_: null,
    _viewCombo_: null,
    _isSet_: null,
    _currentAppliedFilter_: null,
    _isSilent_: null,
    _doActivate_ : null,
    _filtersCleared_ : null,

    // Function to add the dc context parameter values to the standard filter if the DC is in context of another DC

    _setContextFilter_: function(silent) {

        if (this._filterCombo_._isCtrlInSomeContext_() === true) {

            var ctrl = this._grid_._ctrl_;
            var currentFilter = ctrl.filter.data;
            var ctxData = ctrl.dcContext.ctxData;
            var contextFieldName;
            var contextFieldvalue;
            
            if (silent === true) {
                ctrl.noReset = true;
            }
            
            for (var i = 0; i < ctxData.length; i++) {
	        	
	        	contextFieldName = ctxData[i].name;
	        	contextFieldvalue = ctxData[i].value;
	        	
	        	if( currentFilter.hasOwnProperty(contextFieldName) ){
	        		currentFilter[contextFieldName] = contextFieldvalue;
	        	}
/*	        	
//	        	for (var key in currentFilter) {
//	                if (key === contextFieldName) {
//	                    currentFilter[key] = contextFieldvalue;
//	                }
//	            }
*/	            
	        }
        }
    },

    _clearCtxFilter_: function(target) {
        // Do not save the value of the field used to link the current DC to the parent DC if the current DC is in some context
    	var ctrl = this._grid_._ctrl_;
    	var cf = ctrl.filter.data;
        if (target) {
        	cf = target;
        }        
        var currentFilter = Ext.apply({},cf);        
        if (this._filterCombo_._isCtrlInSomeContext_() === true) {        	
        	var ctxData = ctrl.getDcContextFields();        	
        	Ext.each(ctxData, function(item) {
	        	if( currentFilter.hasOwnProperty(item) ){
	        		currentFilter[item] = null;
	        	}
        	}, this);
        }
        return currentFilter;
        // End Dan
    },

    _applyDefaultFilter_: function(options) {

        var filterCombo = this._filterCombo_;
        var viewCombo = this._viewCombo_;
        var viewValue = viewCombo.getValue();
        var viewRecord = viewCombo.findRecord(viewCombo.valueField || viewCombo.displayField, viewValue);
        var filterStore = filterCombo.getStore();
        var ctrl = this._grid_._ctrl_;

        this._filtersCleared_ = false;
        
        filterStore.load({
            scope: this,
            callback: function(records, operation, success) {
                if( success === true ) {
                	if (viewRecord) {
	                	var selId = viewRecord.get("advancedFilterId");
	                    if (selId !== 0) {
	                        filterCombo.setValue(viewRecord.get("advancedFilterName"));
	                        filterCombo._selectedId_ = viewRecord.get("advancedFilterId");
	
	                        // If the controller has default filters previously set with a silent flag
	                        var rec = null;
	                        for(var i=0; i<records.length; i++){
	                        	if(records[i].data.id === selId){
	                        		rec = records[i];
	                        		break;
	                        	}
	                        }
	
	                        if(rec){
	                        	
		                        var standardFilterValue = Ext.decode(rec.get("standardFilterValue"));
		                        
		                        viewRecord.set("standardFilterValue",rec.get("standardFilterValue"));
		                        ctrl.noReset =  false;
		                        
		                        for (var key in standardFilterValue) {
		                            ctrl.setFilterValue(key, standardFilterValue[key]);
		                        }
		
		                        ctrl.advancedFilter = Ext.decode(rec.get("advancedFilterValue"));
	                        }
	                        viewCombo._clearFilterValue_ = null;
	                    }
                	}
                    this._setContextFilter_(true);
                    ctrl.delayedQuery({viewFiltersReset: true, resetOptions: options});                   
                }
            }
        
        });
    },

    _resetGrid_: function(options) {

        var filterCombo = this._filterCombo_;
        var filterButton = Ext.getCmp(this._grid_._filterWindowButtonId_);

        // Clear the filter

        this._grid_._ctrl_.doClearAllFilters();

        filterCombo.setReadOnly(false);
        filterButton.setDisabled(false);
        this.addCls("reset-normal");
        filterCombo.setValue("");

        this._applyDefaultFilter_(options);
        this._isSet_ = null;
        this.setDisabled(true);
        this._currentAppliedFilter_ = this._grid_._ctrl_.getFilter();
        
    },
    
    _loadViewStore_ : function(store, params) {
    	var action = store.load({
			customParams: params,
			scope: this,
			callback: function(records, operation, success) {
				if( success ) {
					var cp = operation.customParams;
					cp.button._doConfig_(cp);
				}
            }
        });
    	return action;
    },
    
    _filtersAreSame_: function(a, b) {
    	// check only the properties from 'a' to exists in 'b' with the same value
        for (var k in a) {
            // If values of same property are not equal,
            // objects are not equivalent
            if (a[k] !== b[k] && typeof(b[k]) !== "undefined") {
                return false;
            }
        }
        // If we made it this far, objects
        // are considered equivalent
        return true;
    },
    
    _isEmptyObject_ : function(obj) {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }

        return true;
    },

    _doConfigWhenViewRecordExists_ : function(cp){
    	var ctxData, z, i;

        if (cp.viewRecord.get("advancedFilterId") !== 0) {
        	
            // If the current view has a default filter than compare it to the current applied filter
            // Stringify the current applied filter and replace all empty quotes with null
        	
        	var currentJSONFilter;
        	
            if (!Ext.isEmpty(cp.stdFilter)) {
            	currentJSONFilter = this._clearCtxFilter_(cp.stdFilter);
            } else {
            	currentJSONFilter = this._clearCtxFilter_(cp.ctrl.getFilter().data);
            }
            
            for (var key in currentJSONFilter) {
            	if (!Ext.isEmpty(currentJSONFilter[key]) && (currentJSONFilter[key] instanceof Date)) {
            		currentJSONFilter[key] = Ext.Date.format(currentJSONFilter[key], "Y-m-d H:i:s");
            	}
            }
           
            var defaultJSONFilter = {};
            
            if (!Ext.isEmpty(cp.viewRecord.get("standardFilterValue")) && cp.viewRecord.get("standardFilterValue") !== "") {
            	defaultJSONFilter = this._clearCtxFilter_(JSON.parse(cp.viewRecord.get("standardFilterValue")));
            }
            
            var filterDifferences = this._filtersAreSame_(currentJSONFilter,defaultJSONFilter);
            
            // Compare the current applied filter with the standard filter value
            
        	if (this._filtersCleared_ === true && cp.ctrl.noReset !== true) {
                this._activate_();
        	}
            
            if (filterDifferences === false) {
            	if (cp.ctrl.noReset !== true) {
                    this._activate_();
                    this._configFilterCombo_(cp.stdFilter);
                }	                
            } else {
            	
                if (cp.viewRecord.get("advancedFilterId") === cp.filterCombo._selectedId_) {
                	
                	// Start compare advanced filters
                	
                	var advancedFilterValue = JSON.parse(cp.viewRecord.get("advancedFilterValue"));
                    var appliedAdvancedFilter = cp.ctrl.advancedFilter;
                    var equal = true;
                    
                    if (advancedFilterValue.length !== appliedAdvancedFilter.length) {
                    	equal = false;
                    }                       
                    else {
                    	for (i = 0; i < appliedAdvancedFilter.length; i++) {
                    		if (!this._filtersAreSame_(appliedAdvancedFilter[i],advancedFilterValue[i])) {
                    			equal = false;
                    			break;
                    		}
                        }
                    }
                    
                    // End compare advanced filters
                	
                    if (cp.ctrl.noReset !== true) {
                    	if (equal) {
                    		this._deActivate_();
                    	}
                    	else {
                    		this._activate_();
                    	}
                    }
                } else {
                    if (cp.ctrl.noReset !== true) {
                        this._activate_();
                        this._configFilterCombo_(cp.stdFilter);
                    }
                }
            }
        } else {
            var currentAppliedFilter = cp.ctrl.getFilter().data;
            var currentAdvancedFilter = cp.ctrl.advancedFilter;
            
            for (i in currentAppliedFilter) {
                if (!Ext.isEmpty(currentAppliedFilter[i])) {

                    // Dan: Bug fix when clearing filter data (check if the DC is in contect of another DC)

                    var isInSomeContext = false;

                    if (!Ext.isEmpty(cp.ctrl.dcContext)) {
                        ctxData = cp.ctrl.dcContext.ctxData;
                        for (z = 0; z < ctxData.length; z++) {
                            if (ctxData[z].name === i) {
                                isInSomeContext = true;
                                break;
                            }
                        }
                    }
                    
                    if (isInSomeContext === false) {
                        if (cp.ctrl.noReset !== true) {
                            this._activate_();
                            this._configFilterCombo_(cp.stdFilter);
                        }
                    } else {
                        if (this._countOtherThanNull_(currentAppliedFilter) > ctxData.length && cp.ctrl.noReset !== true) {
                            this._activate_();
                            this._configFilterCombo_(cp.stdFilter);
                        }
                    }

                    // End Dan: Bug fix when clearing filter data (check if the DC is in context of another DC)
                    break;
                }
            }
            
            // Do the same as above for the advanced filter
            
            if (this._countOtherThanNull_(currentAdvancedFilter) > 0) {
            	this._activate_();
            }
        }
    },

    _doConfigWhenViewRecordIsEmpty_: function(cp){
    	var ctxData, z, i, y, k;
    	
        // If the cp.viewRecord is empty
    	
        var previousValue = cp.filterCombo.lastValue;
        var currentValue = cp.filterCombo.getValue();

        if (previousValue !== currentValue && !Ext.isEmpty(currentValue) && cp.ctrl.noReset !== true) {
            this._activate_();
        }
        
        if (Ext.isEmpty(cp.filterCombo._selectedId_)) {
            var currentFilter = cp.ctrl.getFilter().data;
            var advancedFilter = cp.ctrl.advancedFilter;
            
            var otherThanNullFields = this._getOtherThanNull_(currentFilter);
            var ctxDataFields = [];
            
            if (!Ext.isEmpty(cp.ctrl.dcContext)) {
            	ctxData = cp.ctrl.dcContext.ctxData;
                for (z = 0; z < ctxData.length; z++) {
                    
                    // Exclude the param keys
                	if (ctxData[z].type !== "param") {
                		ctxDataFields.push(ctxData[z].name);
                	}
                }
            } else {
            	var notNullFilters = this._countOtherThanNull_(currentFilter);
            	if (notNullFilters > 0 && cp.ctrl.noReset !== true) {
            		this._activate_();
            	}
            }
            
            // Dan: check if one or more of otherThanNullFields are contained in the ctxDataFields
            
            var keysFound = [];
            var otherThanNullData = [];
            
            for (i = 0; i < ctxDataFields.length; i++) {
                if (otherThanNullFields.indexOf(ctxDataFields[i]) > -1) {
                    keysFound.push(ctxDataFields[i]);
                }
            }
            
            // If one or more keys are found than check if the values from the otherThanNullFields are different or equal to ctxDataFields values

            if (keysFound.length > 0) {
                for (i = 0; i < keysFound.length; i++) {
                    var obj ={
                    	name: keysFound[i],
                    	value: currentFilter[keysFound[i]]
                    }
                    otherThanNullData.push(obj);
                }
            }
            
            for (y = 0; y<otherThanNullData.length; y++) {
            	
            	var fName = otherThanNullData[y].name;
            	var fValue = otherThanNullData[y].value;
            	
            	for (k =0; k<ctxData.length; k++) {
            		if (fName === ctxData[k].name && fValue !== ctxData[k].value) {
            			this._doActivate_ = true;
            		}
            	}
            }
            
            // Check if otherThanNullFields contain fields which aren't in ctxDataFields
            for (i = 0; i < otherThanNullFields.length; i++) {
                if (ctxDataFields.length > 0 && ctxDataFields.indexOf(otherThanNullFields[i]) === -1) {
                	this._doActivate_ = true;
                    break;
                }
            }
           
            // Dan: if the fields from the standard filter which contain values other than null are the same as the ones from the dc context
            // than do not trigger the reset button
            if (this._countOtherThanNull_(currentFilter) > 0 && this._doActivate_ === true && cp.ctrl.noReset !== true) {
                this._activate_();
            }
            if (!Ext.isEmpty(advancedFilter) && advancedFilter.length > 0 && cp.ctrl.noReset !== true) {
                this._activate_();
            }
        }
    },
    
    _doConfig_ : function(cp) {
    	cp.viewRecord = cp.viewCombo.findRecord(cp.viewCombo.valueField || cp.viewCombo.displayField, cp.viewValue);
	    if (cp.viewRecord) {
	    	this._doConfigWhenViewRecordExists_(cp);
	    } else {
	    	this._doConfigWhenViewRecordIsEmpty_(cp);
	    }
    },

	_setUp_: function() {

		var filterCombo = this._filterCombo_;
		var viewCombo = this._viewCombo_;
		
		if (viewCombo || filterCombo) {
			
			var viewValue = viewCombo.getValue();
			var viewRecord = "";
			var viewStore = viewCombo.getStore();
			var grid = this._grid_;
			var ctrl = this._grid_._ctrl_;
			var button = this;
			var stdFilter = ctrl.filter.data;
			
			// Get the default filter for the current view
			
			var customParams = {
				viewCombo: viewCombo,
				filterCombo : filterCombo,
				ctrl: ctrl,
				grid: grid,
				stdFilter: stdFilter,
				viewValue: viewValue,
				viewRecord: viewRecord,
				button: button
			};
			
			if (viewStore.isLoaded() === false) {
				this._loadViewStore_(viewStore, customParams);
			} else {
				this._doConfig_(customParams);
			}
        }
    },  
    

    _activate_: function() {
        this.setDisabled(false);
        this.addCls("reset-red");
    },

    _deActivate_: function() {
        this.setDisabled(true);
        this.addCls("reset-normal");
        this._doActivate_ = null;
    },

    _configFilterCombo_: function(stdFilter) {

        var filterCombo = this._filterCombo_;
        var ctrl = this._grid_._ctrl_;

        if (Ext.isEmpty(filterCombo._doNotClearValue_)) {

            filterCombo.setValue("");
            filterCombo._selectedId_ = null;
            ctrl.advancedFilter = null;
            for (var key in stdFilter) {
                ctrl.setFilterValue(key, stdFilter[key]);
            }
        }
    },

    _countOtherThanNull_: function(target) {
        var counter = 0;
        if (typeof target === "string") {
        	target = JSON.parse(target);
        }
        for (var member in target) {
            if (!Ext.isEmpty(target[member])) {
            	counter++;
            }
               
        }
        return counter;
    },
    
    _getOtherThanNull_: function(target) {
        var fields = [];
        if (typeof target === "string") {
        	target = JSON.parse(target);
        }
        for (var member in target) {
            if (!Ext.isEmpty(target[member])) {
            	fields.push(member);
            }
               
        }
        return fields;
    },

	_delayedSetupTask_ : null,
	_setupTaskTimeout_ : 150,
 
    initComponent: function() {
        var cfg = {
            text: Main.translate("viewAndFilters", "resetFilter__lbl"),
            disabled: true,
            handler: function() {
                this.removeCls("reset-red");
                this._resetGrid_()
            }
        };
        Ext.apply(this, cfg);
        this.callParent(arguments);

        this._delayedSetupTask_ = new Ext.util.DelayedTask(this.doQuery, this);

        this._grid_._ctrl_.store.on("load", function() {
        	this.doSetup();
        }, this);
    },
    
    doSetup : function() {
    	if( this._setupTaskTimeout_>0 ){
    		this._delayedSetupTask_.delay(this._setupTaskTimeout_, this._setUp_, this);
    	} else {
    		this._setUp_();
    	}
    }

});

