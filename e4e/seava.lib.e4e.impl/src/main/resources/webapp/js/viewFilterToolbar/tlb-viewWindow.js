Ext.define("Toolbar.View.Window", {

    extend: 'Ext.window.Window',
    name: 'viewWindow',
    width: 345,
    layout: 'fit',
    bodyStyle: 'border:0px',
    resizable: false,

    // =================== Custom properties ===================

    _showCurrency_: true,
    _showUnit_: true,
    _showTotal_: true,
    _viewRecord_: null,
    _viewStore_: null,
    _viewState_: null,
    _gridName_: null,
    _viewStateAlias_: null,
    _defaultViewStateAlias_ : null,
    _viewStateModel_: null,
    _selectedId_: null,
    _defaultPageSize_: null,


    _idCmpIsDefaultView_: null,
    _idCmpIsSystemWide_: null,
    _idCmpPageSize_: null,
    _idCmpTotalsCombo_: null,
    _idCmpViewName_: null,
    _idCmpCurrenciesCombo_: null,
    _idCmpUnitsCombo_: null,
    _idCmpSaveBtn_: null,
    _idCmpDeleteBtn_: null,
    _idCmpSaveAsBtn_: null,
    _idCmpDefaultFilterCombo_: null,
    _idCmpIsShared_ : null,


    _idCmpViewComboMain_: null,
    _idCmpFilterComboMain_: null,
    _idCmpTotalsComboMain_: null,
    _idCmpUnitsComboMain_: null,
    _idCmpCurrenciesComboMain_: null,

    _cmpViewComboMain_: null,
    _cmpFilterComboMain_: null,
    _cmpTotalsComboMain_: null,
    _cmpUnitsComboMain_: null,
    _cmpCurrenciesComboMain_: null,
    _toolBarId_ : null,

    // =================== End custom properties ===================

    initComponent: function() {
        this.callParent();

        // get 'master' combo-s
        this._cmpViewComboMain_ = this._getComponentById_(this._idCmpViewComboMain_);
        this._cmpFilterComboMain_ = this._getComponentById_(this._idCmpFilterComboMain_);
        this._cmpTotalsComboMain_ = this._getComponentById_(this._idCmpTotalsComboMain_);
        this._cmpUnitsComboMain_ = this._getComponentById_(this._idCmpUnitsComboMain_);
        this._cmpCurrenciesComboMain_ = this._getComponentById_(this._idCmpCurrenciesComboMain_);

        // generate id-s
        this._idCmpDefaultFilterCombo_ = Ext.id();
        this._idCmpCurrenciesCombo_ = Ext.id();
        this._idCmpUnitsCombo_ = Ext.id();
        this._idCmpIsDefaultView_ = Ext.id();
        this._idCmpIsSystemWide_ = Ext.id();
        this._idCmpPageSize_ = Ext.id();
        this._idCmpTotalsCombo_ = Ext.id();
        this._idCmpViewName_ = Ext.id();
        this._idCmpSaveBtn_ = Ext.id();
        this._idCmpDeleteBtn_ = Ext.id();
        this._idCmpSaveAsBtn_ = Ext.id();
        this._idCmpIsShared_ = Ext.id();

        this.title = this._viewRecord_.getValue() == null ? Main.translate("viewAndFilters", "viewWindow__lbl") : Main.translate("viewAndFilters", "currentView__lbl") + this._viewRecord_.getValue();
        this._buildPanel_();
    },

    _getComponentById_: function(id) {
        var component = Ext.ComponentQuery.query('[id=' + id + ']')[0];
        return component;
    },

    _findViewComboRecord_: function(prop, value, store) {
        var record;
        if (store.getCount() > 0) {
            store.each(function(r) {
                if (r.data[prop] == value) {
                    record = r;
                    return false;
                }
            });
        }
        return record;
    },

    _isOwner_: function() {

        var store = this._viewStore_;
        var id = this._cmpViewComboMain_._selectedId_;
        var record = store.getById(id);
        var hasPriviledges;

        if (!record) {
            return;
        }

        var createdBy = record.get("createdBy");
        var currentUser = getApplication().getSession().getUser().code;


        if (createdBy === currentUser || currentUser === __SUPERUSER__) {
            hasPriviledges = true;
        } else {
            hasPriviledges = false;
        }
        return hasPriviledges;

    },

    // Dan
    // SONE-1174: Enhance filter management 

    _setSystemWideFilter_: function(checkbox) {
        var filterCombo = this._cmpFilterComboMain_;
        var filterComboValue = filterCombo.getRawValue();
        var availableSystemwide;

        if (Ext.isEmpty(checkbox)) {
            availableSystemwide = this._getComponentById_(this._idCmpIsSystemWide_).getValue();
        } else {
            availableSystemwide = checkbox;
        }


        if (!Ext.isEmpty(filterComboValue)) {

            var filterStore = filterCombo.getStore();
            var filterRecord = filterStore.getById(filterCombo._selectedId_);

            if (!filterRecord) {
                return;
            }

            // Check to see if we havce sufficient rights to modify the filter's system wide flag

            var createdBy = filterRecord.get("createdBy");
            var currentUser = getApplication().getSession().getUser().code;

            if (createdBy === currentUser || currentUser === __SUPERUSER__) {

                // If we have sufficient rights continue to check if the filter is assigned to a view

                var viewStore = this._viewStore_;
                var storeItems = viewStore.data.items;
                var existsFalg = false,
                    i = 0,
                    l = storeItems.length;
                var canContinue = false;
                var counter = 0;

                for (i; i < l; i++) {

                    var filterId = storeItems[i].data.advancedFilterId;
                    var viewId = storeItems[i].data.id;

                    if (filterCombo._selectedId_ === filterId && this._cmpViewComboMain_._selectedId_ !== viewId) {
                        counter++;
                        existsFalg = true;
                        break;
                    }

                }

                var newFilterRecord = filterStore.getById(filterId);

                if (!newFilterRecord) {
                    return;
                }

                var isSystemWide = newFilterRecord.get("availableSystemwide");

                if( (isSystemWide === false) || (existsFalg === true && availableSystemwide === true) ) {
                    // if the filter is already assigned to a view check if its also available system wide

                    // If it isn't available system wide and we check the system wide checkbox than 
                    // we can continue to make the filter system wide
                	
                    // If the filter isn't assigned to any other view than we can continue to change the system wide flag
                    canContinue = true;
                }
            }

            if (canContinue === true) {
                filterRecord.beginEdit();
                filterRecord.set("availableSystemwide", availableSystemwide);
                filterRecord.endEdit();
                filterStore.commitChanges();
            }
        }
    },

    _doSaveAsView_: function() {
    	
        var name = this._getComponentById_(this._idCmpViewName_).getValue();

        var cmp = this._gridName_.stateId;
        var value = this._viewState_;
        var active = 1;
        var defaultView = this._getComponentById_(this._idCmpIsDefaultView_).getValue();
        var availableSystemwide = this._getComponentById_(this._idCmpIsSystemWide_).getValue();
        var pageSize = this._getComponentById_(this._idCmpPageSize_).getValue();
        var viewCombo = this._cmpViewComboMain_;
        var totals = this._getComponentById_(this._idCmpTotalsCombo_).getValue();
        var unitCode = this._getComponentById_(this._idCmpUnitsCombo_).getValue();
        var currencyCode = this._getComponentById_(this._idCmpCurrenciesCombo_).getValue();
        var defaultFilterCombo = this._getComponentById_(this._idCmpDefaultFilterCombo_).getValue();
        var filterCombo = this._cmpFilterComboMain_;
        var filterStore = filterCombo.getStore();

        if (Ext.isEmpty(pageSize)) {
            Main.error(Main.translate("viewAndFilters", "pageSizeNotEmpty__lbl"));
            return false;
        }       
        

        if (!Ext.isEmpty(filterCombo._selectedId_) && !Ext.isEmpty(defaultFilterCombo)) {

            filterStore.load({
                callback: function(records, operation, success) {
                	if( success === true ) {
                        Ext.Array.each(records, function(record) {
                            if (record.get("id") === filterCombo._selectedId_) {
                                var filterRecord = filterStore.getById(record.get("id"));

                                filterRecord.beginEdit();
                                filterRecord.set("defaultFilter", 1);
                                filterRecord.endEdit();

                                filterStore.commitChanges();
                            }
                        });
                    }
                }
            });
        } else {
            filterCombo.setValue("");
            filterCombo._selectedId_ = null;
            this._gridName_._controller_.store.removeFilter();
            this._gridName_._controller_.doClearAllFilters();
        }
        
        // Strip special characters
        
        var sanitizedName = name.replace(/"|&|'|<|>|\//g,'');

        Ext.Ajax.request({
            url: Main.dsAPI(this._viewStateAlias_, "json").create,
            method: "POST",
            params: {
                data: Ext.JSON.encode({
                    name: sanitizedName,
                    value: Ext.JSON.encode(value),
                    cmp: cmp,
                    advancedFilterId: filterCombo._selectedId_,
                    active: active,
                    totals: totals,
                    pageSize: pageSize,
                    unitCode: unitCode,
                    currencyCode: currencyCode,
                    //defaultView: defaultView,
                    availableSystemwide: availableSystemwide,
                    cmpType: "frame-dcgrid"
                })
            },
            success: function(response) {
            	
                var viewStore = viewCombo.getStore();

                // ================================================================================
                // Dan: insert the newly created view state into the database if it's a default one
                // ================================================================================
                
                var data = Ext.getResponseDataInJSON(response).data[0];
                var createdId = data.id;
                var currentUser = getApplication().getSession().getUser().code;
                
                
                if (defaultView === true) {
                	
                	// Check to see if we already have a default view state saved for the current view
                	// and the current logged in user
                	
	                Ext.Ajax.request({
	                  url: Main.dsAPI(this._defaultViewStateAlias_, "json").read,
	                  method: "POST",
	                  scope: this,
	                  params: {
                        data: Ext.JSON.encode({
                            cmp : this._gridName_.stateId,
                            userCode : currentUser
                        })
	                  },
	                  success: function(response) {
	                	  
	                	  // Now we evaluate the response to check if we have any data
	                	  // If any data is present it means that we already have a default view state saved for the current
	                	  // view and user
	                	  
	                	  var checkData = Ext.getResponseDataInJSON(response).data;
	                	  var length = checkData.length;
	                	  var strData = JSON.stringify(checkData);
	                	  
	                	  if (length > 0) {
	                		  
	                		  // Found a default view state for the current view, DELETE IT and than CREATE THE NEW ONE
	                		  
	                		  Ext.Ajax.request({
	                              url: Main.dsAPI(this._defaultViewStateAlias_, "json").destroy,
	                              method: "POST",
	                              scope: this,
	                              params: {
	                                  data: strData
	          	                  },
	                              success: function() {
	                            	  // If the default view has been successfully deleted than create the new one
	                            	  
	                            	  Ext.Ajax.request({
	    	                              url: Main.dsAPI(this._defaultViewStateAlias_, "json").create,
	    	                              method: "POST",
	    	                              params: {
	    	                                  data: Ext.JSON.encode({
	    	                                      viewId : createdId,
	    	                                      userCode : currentUser
	    	                                  })
	    	                              },
	    	                              scope: this,
	    	                              failure: function(response) {
	    	                                  Main.error(Ext.getResponseErrorText(response));
	    	                              }
	    	                		  });
	                              },
	                              failure: function(response) {
	                                  Main.error(Ext.getResponseErrorText(response));
		                          }
	                		  });               		  

	                	  }
	                	  else {
	                		  
	                		  // The current view doesn't have a default view state, CREATE A NEW ONE
	                		  
	                		  Ext.Ajax.request({
	                              url: Main.dsAPI(this._defaultViewStateAlias_, "json").create,
	                              method: "POST",
	                              params: {
	                                  data: Ext.JSON.encode({
	                                      viewId : createdId,
	                                      userCode : currentUser
	                                  })
	                              },
	                              scope: this,
	                              failure: function(response) {
	                                  Main.error(Ext.getResponseErrorText(response));
	                              }
	                      	  });	
	                	  }
	                	  
	                  },
	                  failure: function(response) {
	                      Main.error(Ext.getResponseErrorText(response));
	                  }
	                });
                }

                
                // ========
                // Dan: End
                // ========

                // Dan: E5 bug fixes
                
                viewStore.load({
                    callback: function() {

                        viewCombo.setValue(sanitizedName);
                        var record = this._findViewComboRecord_("name", sanitizedName, viewStore);
                        var id = record.data.id;
                        this._viewRecord_._selectedId_ = id;

                        // Set the selected filter to be available system wide if the "Available systemwide" checkbox is checked

                        this._setSystemWideFilter_(availableSystemwide);
                        
                        if (defaultView === true) {
                        	this._gridName_._defaultViewId_ = this._viewRecord_._selectedId_;
                        }
                        else {
                        	this._gridName_._defaultViewId_ = null;
                        }

                        this.close();
                    },
                    scope: this
                }, this);
            },
            failure: function(response) {
                Main.error(Ext.getResponseErrorText(response));
            },
            scope: this
        });

        // Apply the new page size
        this._gridName_._setPageSize_(pageSize);
        viewCombo._pageSize_ = pageSize;
        this._refreshData_();
    },

    _doSaveView_: function() {

        var record = this._viewStore_.getById(this._viewRecord_._selectedId_);
        
        if (!record) {
            return;
        }

            var pageSize = this._getComponentById_(this._idCmpPageSize_).getValue();
            var viewCombo = this._cmpViewComboMain_;
            var totals = this._getComponentById_(this._idCmpTotalsCombo_).getValue();
            var unitCode = this._getComponentById_(this._idCmpUnitsCombo_).getValue();
            var currencyCode = this._getComponentById_(this._idCmpCurrenciesCombo_).getValue();
            var filterCombo = this._cmpFilterComboMain_;

            if (Ext.isEmpty(pageSize)) {
                Main.error(Main.translate("viewAndFilters", "pageSizeNotEmpty__lbl"));
                return false;
            }

            var cmpWindowFilterComboValue = Ext.getCmp(this._idCmpDefaultFilterCombo_).getRawValue();
            var cmpToolbarFilterComboValue = Ext.getCmp(this._idCmpFilterComboMain_).getRawValue();

            var s = this._viewStore_;
            var r = s.getById(this._viewRecord_._selectedId_);

            // Set the selected filter to be available system wide if the "Available systemwide" checkbox is checked
            this._setSystemWideFilter_();

            r.beginEdit();

            // Strip special characters
            
            var newName = this._getComponentById_(this._idCmpViewName_).getValue();
            var sanitizedName = newName.replace(/"|&|'|<|>|\//g,'');
            
            r.set("name", sanitizedName);
            r.set("value", Ext.JSON.encode(this._viewState_));
            r.set("pageSize", pageSize);

            // Update the default view filter ID

            if (!Ext.isEmpty(filterCombo._selectedId_)) {
                if (cmpWindowFilterComboValue === cmpToolbarFilterComboValue) {
                    r.set("advancedFilterId", filterCombo._selectedId_);
                } else {
                    if (Ext.isEmpty(cmpWindowFilterComboValue)) {
                        r.set("advancedFilterId", null);
                        this._cmpViewComboMain_._clearFilter_();
                    }
                }
            } else {
                r.set("advancedFilterId", null);
                this._cmpViewComboMain_._clearFilter_();
            }

            r.set("totals", totals);
            r.set("unitCode", unitCode);
            r.set("currencyCode", currencyCode);
            r.set("availableSystemwide", this._getComponentById_(this._idCmpIsSystemWide_).getValue());
            r.endEdit();
            
            s.commitChanges();
            
            this._setViewAsDefault_();
            
            var mainViewCombo = Ext.getCmp(this._idCmpViewComboMain_);
            mainViewCombo.setValue(sanitizedName);

            // Apply the new page size
            this._gridName_._setPageSize_(pageSize);
            viewCombo._pageSize_ = pageSize;
            this._refreshData_();
            
            this.close();
    },
    
    

    _doDeleteView_: function() {

        var record = this._viewStore_.getById(this._viewRecord_._selectedId_);

        if (!record) {
            return;
        }
        
        // Dan: before deleting a view state check to see if it is already used by other users 
        // as a default view state
        
        Ext.Ajax.request({
            url: Main.dsAPI(this._defaultViewStateAlias_, "json").read,
            method: "POST",
            scope: this,
            params: {
              data: Ext.JSON.encode({
                  cmp : this._gridName_.stateId,
                  name : this._getComponentById_(this._idCmpViewName_).getValue()
              })
            },
            success: function(response) {
            	
            	var resultData = Ext.getResponseDataInJSON(response).data;
            	var users = [];
            	for (var i = 0; i<resultData.length; i++) {
            		var userCode = resultData[i].userCode;
            		var currentUser = getApplication().getSession().getUser().code;
            		if (userCode !== currentUser) {
            			users.push(userCode);
            		}
            	}
            	
            	if (users.length > 0) {
            		
            		// If there are other users who are using the view state as a default view than
            		// throw an error message and do not delete the current view
            		
            		Main.error(Main.translate("viewAndFilters", "viewUsedByOthers__lbl"));
            		return;
            	}
            	else {
            		
            		// Delete the current view
            		
            		Ext.MessageBox.confirm(Main.translate("viewAndFilters", "delete__lbl"), Main.translate("viewAndFilters", "askToDeleteView1__lbl") + record.get("name") + Main.translate("viewAndFilters", "askToDeleteView2__lbl"), function(btn) {
                        if (btn === 'yes') {
                            var createdBy = record.get("createdBy");
                            var currentUser = getApplication().getSession().getUser().code;

                            if (createdBy === currentUser || currentUser === __SUPERUSER__) {
                                this._viewStore_.remove(record);
                                var viewCombo = this._cmpViewComboMain_;
                                viewCombo.setValue("");
                                this._viewRecord_._selectedId_ = "";
                                this._gridName_._applyViewState_(viewCombo._originalViewState_);
                                this._gridName_._setPageSize_(this._defaultPageSize_);
                                this._gridName_.getStore().load();
                                this.close();
                            } else {
                                Main.error(Main.translate("viewAndFilters", "notEditable__lbl"));
                            }
                        } else {
                            return false;
                        }
                    }, this);
            	}
            	
            },
            failure: function(response) {
                Main.error(Ext.getResponseErrorText(response));
            }
        });

        
    },

    // =============== build form ========================

    _buildPanel_: function() {
        var panel = Ext.create("Ext.panel.Panel", {
            xtype: "panel",
            frame: true,
            bodyPadding: 12,
            buttonAlign: 'center',
            layout: 'anchor',
            bodyStyle: "background:transparent",
            defaultType: 'textfield',
            items: this._buildItems_(),
            // Reset and Submit buttons
            buttons: this._buildButtons_()
        });

        this.add(panel);
    },

    _filterComboBySystemWide_: function(store) {

        var availableSystemWide = this._getComponentById_(this._idCmpIsSystemWide_).getValue();
        store.clearFilter();
        if (availableSystemWide === true) {
            store.filter('availableSystemwide', true);
        }
    },

    _buildDefaultFilterCombo_: function() {
        var alias = "fmbas_AdvancedFilterRt_Ds";
        var ds = "Advanced.Filter.Model";
        var grid = this._gridName_;
        var filterCombo = this._cmpFilterComboMain_;
        var viewCombo = Ext.getCmp(grid._viewComboId_);

        var combo = Ext.create("Filter.Combo", {
            id: this._idCmpDefaultFilterCombo_,
            _label_: Main.translate("viewAndFilters", "usingFilter__lbl"),
            _anchor_: "100%",
            _advancedFilterAlias_: alias,
            _advancedFilterDs_: ds,
            _ctrl_: grid._controller_,
            _grid_: grid
        });

        var comboCfg = {
            labelWidth: 100,
            labelAlign: "right"
        };

        // Start Dan
        // Dan - the default filter combo value has to be the name of the filter assigned to the current view

        var s = this._viewStore_;
        var r = s.getById(this._viewRecord_._selectedId_);

        var filterStore = combo.getStore();

        filterStore.on("beforeload", function(s) {
            this._filterComboBySystemWide_(s);
        }, this);

        combo.on("afterrender", function() {
            if (!Ext.isEmpty(r)) {
                var advancedFilterId = r.get("advancedFilterId");
                if (advancedFilterId !== 0 && Ext.isEmpty(viewCombo._clearFilterValue_)) {
                    this.setRawValue(r.get("advancedFilterName"));
                }
            }
        });

        // End Dan
        
        combo.on('blur', function(combo) {
            combo.store.clearFilter();
            // force the reload on trigger
            combo.lastQuery = null;
        });

        combo.on('select', function(combo, record) {
            filterCombo._doNotClearValue_ = true;
            filterCombo.setValue(this.getValue());
            var rec = record;
            if (Ext.isArray(rec)) {
                rec = record[0];
            }

            filterCombo._selectedId_ = rec.data.id;
            combo.store.rejectChanges();
        });

        Ext.apply(combo, comboCfg);

        return combo;
    },
    
    _setViewAsDefault_ : function() {
    	
    	// ================================================================================
        // Dan: insert the newly created view state into the database if it's a default one
        // ================================================================================
        
    	var viewCombo = Ext.getCmp(this._idCmpViewComboMain_);
        var updatedId = viewCombo._selectedId_;
        var currentUser = getApplication().getSession().getUser().code;
        var toolbar = Ext.getCmp(this._gridName_._viewFilterToolbarId_);
        var assignBtn = Ext.getCmp(toolbar._assignBtnId_);
        var defaultView = this._getComponentById_(this._idCmpIsDefaultView_).getValue();
        
        if (defaultView === true) {
        	
        	// Check to see if we already have a default view state saved for the current view
        	// and the current logged in user
        	
            Ext.Ajax.request({
              url: Main.dsAPI(this._defaultViewStateAlias_, "json").read,
              method: "POST",
              scope: this,
              params: {
                data: Ext.JSON.encode({
                    cmp : this._gridName_.stateId,
                    userCode : currentUser
                })
              },
              success: function(response) {
            	  
            	  // Now we evaluate the response to check if we have any data
            	  // If any data is present it means that we already have a default view state saved for the current
            	  // view and user
            	  
            	  var checkData = Ext.getResponseDataInJSON(response).data;
            	  var length = checkData.length;
            	  var strData = JSON.stringify(checkData);
            	  
            	  if (length > 0) {
            		  
            		  // Found a default view state for the current view, DELETE IT and than CREATE THE NEW ONE
            		  
            		  Ext.Ajax.request({
                          url: Main.dsAPI(this._defaultViewStateAlias_, "json").destroy,
                          method: "POST",
                          scope: this,
                          params: {
                              data: strData
      	                  },
                          success: function() {
                        	  
                        	  // If the default view has been successfully deleted than create the new one
                        	  
                        	  Ext.Ajax.request({
	                              url: Main.dsAPI(this._defaultViewStateAlias_, "json").create,
	                              method: "POST",
	                              params: {
	                                  data: Ext.JSON.encode({
	                                      viewId : updatedId,
	                                      userCode : currentUser
	                                  })
	                              },
	                              scope: this,
	                              failure: function(response) {
	                                  Main.error(Ext.getResponseErrorText(response));
	                              }
	                		  });
                        	  
                          },
                          failure: function(response) {
                              Main.error(Ext.getResponseErrorText(response));
                          }
            		  });               		  

            	  }
            	  else {
            		  
            		  // The current view doesn't have a default view state, CREATE A NEW ONE
            		  
            		  Ext.Ajax.request({
                          url: Main.dsAPI(this._defaultViewStateAlias_, "json").create,
                          method: "POST",
                          params: {
                              data: Ext.JSON.encode({
                                  viewId : updatedId,
                                  userCode : currentUser
                              })
                          },
                          scope: this,
                          failure: function(response) {
                              Main.error(Ext.getResponseErrorText(response));
                          }
                  	  });	
            	  }
            	  
              },
              failure: function(response) {
                  Main.error(Ext.getResponseErrorText(response));
              }
            });
            
            this._gridName_._defaultViewId_ = this._viewRecord_._selectedId_;
        }
        else {

        	// delete the current view if it is a default one
        	
        	if (this._isDefaultViewState_() === true) {
        		this._gridName_._defaultViewId_ = null;
        		
        		Ext.Ajax.request({
                    url: Main.dsAPI(this._defaultViewStateAlias_, "json").read,
                    method: "POST",
                    scope: this,
                    params: {
                      data: Ext.JSON.encode({
                          cmp : this._gridName_.stateId,
                          userCode : currentUser
                      })
                    },
                    success: function(response) {
                  	  
                  	  // Now we evaluate the response to check if we have any data
                  	  // If any data is present it means that we already have a default view state saved for the current
                  	  // view and user
                  	  
                  	  var checkData = Ext.getResponseDataInJSON(response).data;
                  	  var length = checkData.length;
                  	  var strData = JSON.stringify(checkData);
                  	  
                  	  if (length > 0) {
                  		  
                  		  // Found a default view state for the current view, DELETE IT and than CREATE THE NEW ONE
                  		  
                  		  Ext.Ajax.request({
                                url: Main.dsAPI(this._defaultViewStateAlias_, "json").destroy,
                                method: "POST",
                                scope: this,
                                params: {
                                    data: strData
            	                },
                                failure: function(response) {
                                    Main.error(Ext.getResponseErrorText(response));
                                }
                  		  });
                  	  }
                   }
        		});

        	}
        	
        }

        assignBtn.setDisabled(true);
        
        // ========
        // Dan: End
        // ========
    },
    
    _isDefaultViewState_: function() {

        var isDefaultView = false;
        
        var record = this._viewStore_.getById(this._viewRecord_._selectedId_);
        if (!record) {
            return;
        }
        
        var selectedId = record.get("id");
        var defaultViewId = this._gridName_._defaultViewId_;
        
        if (selectedId === defaultViewId) {
        	isDefaultView = true;
        }
        
        return isDefaultView;
    },

    _isSystemWideState_: function() {

        var isSystemWide = "";

        if (!Ext.isEmpty(this._viewRecord_._selectedId_)) {
            var s = this._viewStore_;
            var r = s.getById(this._viewRecord_._selectedId_);
            if (r) {
                isSystemWide = r.get("availableSystemwide");
            }
        }
        return isSystemWide;
    },

    _getValueFromRecord_: function(field) {
        var value = "";
        if (!Ext.isEmpty(this._viewRecord_._selectedId_)) {
            var s = this._viewStore_;
            var r = s.getById(this._viewRecord_._selectedId_);
            if (r) {
                value = r.get(field);
            }
        }
        return value;
    },
    _getTotals_: function() {
        return this._getValueFromRecord_("totals");
    },
    _getUnit_: function() {
        return this._getValueFromRecord_("unitCode");
    },
    _getCurrency_: function() {
        return this._getValueFromRecord_("currencyCode");
    },

    _applyValues_: function(obj) {
        var grid = this._gridName_;
        var dc = grid._controller_;

        if (this._cmpTotalsComboMain_) {
            this._cmpTotalsComboMain_.setValue(obj.totals);
        }
        if (this._cmpCurrenciesComboMain_) {
            this._cmpCurrenciesComboMain_.setValue(obj.currencyCode);
        }
        if (this._cmpUnitsComboMain_) {
            this._cmpUnitsComboMain_.setValue(obj.unitCode);
        }

        if (grid._currencyParamName_) {
            dc.setParamValue(grid._currencyParamName_, obj.currencyCode, null, null, true);
        }
        if (grid._unitParamName_) {
            dc.setParamValue(grid._unitParamName_, obj.unitCode, null, null, true);
        }
    },

    _refreshData_: function() {
        var grid = this._gridName_;
        grid._controller_.delayedQuery();
    },

    _buildItems_: function() {
        return [{
                xtype: "textfield",
                anchor: '100%',
                fieldLabel: Main.translate("viewAndFilters", "viewName__lbl"),
                labelWidth: 100,
                labelAlign: "right",
                labelSeparator: Main.viewConfig.FORM_LABEL_SEPARATOR,
                id: this._idCmpViewName_,
                allowBlank: false,
                value: this._viewRecord_.getValue() == null ? "" : this._viewRecord_.getValue(),
                listeners: {
                    change: {
                        scope: this,
                        fn: function(el) {

                            var btnSaveAs = this._getComponentById_(this._idCmpSaveAsBtn_);
                            var btnSave = this._getComponentById_(this._idCmpSaveBtn_);
                            var btnDelete = this._getComponentById_(this._idCmpDeleteBtn_);
                            var disabled = true, record;
                            var saveDeleteDisabled = false;

                            if (el.getValue().trim() !== this._viewRecord_.getValue() && el.getValue().trim() !== "") {
                                disabled = false;
                                saveDeleteDisabled = true;
                            }
                            btnSaveAs.setDisabled(disabled);
                            
                            // save is enabled only if record exists
                            record = this._viewStore_.getById(this._viewRecord_._selectedId_);
                            if (!record || el.getValue().trim() === "") {
                                saveDeleteDisabled = true;
                            } else {
                            	var viewCreatedBy = record.get("createdBy");
                                var currentUser = getApplication().getSession().getUser().code;
                                if (viewCreatedBy !== currentUser) {
                                	btnSave.setDisabled(true);
                                }
                            }
                            if (this._isOwner_() === true) {
                                btnDelete.setDisabled(saveDeleteDisabled);
                            }
                        }
                    }
                }
            },
            this._buildCurrenciesCombo_(),
            this._buildUnitsCombo_(),
            this._buildTotalsCombo_(), {
                xtype: "numberfield",
                decimalPrecision: 0,
                fieldLabel: Main.translate("viewAndFilters", "pageSize__lbl"),
                labelWidth: 100,
                labelAlign: "right",
                labelSeparator: Main.viewConfig.FORM_LABEL_SEPARATOR,
                id: this._idCmpPageSize_,
                name: 'pageSize',
                anchor: '55%',
                value: !Ext.isEmpty(this._cmpViewComboMain_._pageSize_) ? this._cmpViewComboMain_._pageSize_ : this._defaultPageSize_,
                maxValue: 1000,
                minValue: 0,
                step: 5
            },
            this._buildDefaultFilterCombo_(), {
                xtype: "fieldcontainer",
                layout: "column",
                margin: "10 0 0 0",
                items: [{
                    xtype: 'checkbox',
                    id: this._idCmpIsDefaultView_,
                    checked: this._isDefaultViewState_() === true ? true : false
                            ,listeners: {
                            	change: {
                            		scope: this,
                            		fn: function() {
                            			this._activateSaveBtn_();
                            		}
                            	}
                            }
                }, {
                    xtype: "displayfield",
                    fieldCls: "x-form-item-label",
                    value: Main.translate("viewAndFilters", "defaultViewForDialog__lbl")

                }]
            }, {
                xtype: "fieldcontainer",
                layout: "column",
                items: [{
                    xtype: 'checkbox',
                    id: this._idCmpIsSystemWide_,
                    checked: this._isSystemWideState_() === true ? true : false
                }, {
                    xtype: "displayfield",
                    fieldCls: "x-form-item-label",
                    value: Main.translate("viewAndFilters", "shareView__lbl")
                }]
            }
        ]
    },
    
    _activateSaveBtn_ : function() {
    	
    	var viewCombo = Ext.getCmp(this._idCmpViewComboMain_);
	    var viewStore = viewCombo.store;
	    var viewRecord = viewStore.getById(viewCombo._selectedId_);
	    var viewCreatedBy = "";
	    if (viewRecord) {
	    	viewCreatedBy = viewRecord.get("createdBy");
	    }
	    var currentUser = getApplication().getSession().getUser().code;
	    var saveBtn = this._getComponentById_(this._idCmpSaveBtn_);
    	var viewName = Ext.getCmp(this._idCmpViewName_);
    	var pageSize = Ext.getCmp(this._idCmpPageSize_);
    	var filterCombo = Ext.getCmp(this._idCmpDefaultFilterCombo_);
    	var isShared = Ext.getCmp(this._idCmpIsSystemWide_);
    	var resetButton = Ext.getCmp(this._gridName_._resetButtonId_);
    	
    	var totalsCombo = Ext.getCmp(this._idCmpTotalsCombo_);
    	var totalsComboMain = Ext.getCmp(this._idCmpTotalsComboMain_);
    	
    	var currencyCombo = Ext.getCmp(this._idCmpCurrenciesCombo_);
    	var currencyComboMain = Ext.getCmp(this._idCmpCurrenciesComboMain_);   	
    	
    	var unitCombo = Ext.getCmp(this._idCmpUnitsCombo_);
    	var unitComboMain = Ext.getCmp(this._idCmpUnitsComboMain_); 
    	
	    if (viewCreatedBy !== currentUser && viewCombo.getValue() === viewName.getValue()) {

	    	viewName.reset();
	    	pageSize.reset();
	    	isShared.reset();
	    	
	    	viewName.setReadOnly(true);
	    	pageSize.setReadOnly(true);
	    	isShared.setReadOnly(true);
	    	
	    	filterCombo.setReadOnly(true);
	    	
	    	if (currencyCombo && currencyComboMain) {
	    		currencyCombo.setReadOnly(true);
	    		currencyCombo.reset();
	    		currencyComboMain.reset();
	    	}
	    	
	    	if (unitCombo && unitComboMain) {
	    		unitCombo.setReadOnly(true);
	    		unitCombo.reset();
	    		unitComboMain.reset();
	    	}
	    	
	    	if (totalsCombo && totalsComboMain) {
	    		totalsCombo.reset();
	    		totalsCombo.setReadOnly(true);
	    		totalsComboMain.reset();
	    	}
	    	
	    	
	    	var setComboLastValue = function(comboBox, value) {
	    		var store = comboBox.store;
	    	    var valueField = comboBox.valueField;
	    	    var displayField = comboBox.displayField;

	    	    var recordNumber = store.findExact(valueField, value, 0);

	    	    if (recordNumber === -1)
	    	        return -1;

	    	    var displayValue = store.getAt(recordNumber).data[displayField];
	    	    comboBox.setValue(value);
	    	    comboBox.setRawValue(displayValue);
	    	    comboBox.selectedIndex = recordNumber;
	    	}
	    	
	    	var s = this._viewStore_;
	        var r = s.getById(this._viewRecord_._selectedId_);
	        var advancedFilterName = r.get("advancedFilterName");
	        
	        if (!r) {
	        	return;
	        }
	        else {
	        	setComboLastValue(filterCombo, advancedFilterName);	        	
	        	resetButton._resetGrid_();
	        }
	        
	        saveBtn.setDisabled(false);
	    }

    },

    _currenciesChanged_: function(value) {

        var grid = this._gridName_;
        var dc = grid._controller_;
        var cmpToolbarFilterCombo = Ext.getCmp(this._idCmpFilterComboMain_);

        if (dc.getParamValue(grid._currencyParamName_) !== value) {
            dc.setParamValue(grid._currencyParamName_, value, null, null, true);
            cmpToolbarFilterCombo._setContextFilter_(); //SONE-2758
            dc.delayedQuery();
        }
    },

    _unitsChanged_: function(value) {

        var grid = this._gridName_;
        var dc = grid._controller_;
        var cmpToolbarFilterCombo = Ext.getCmp(this._idCmpFilterComboMain_);

        if (dc.getParamValue(grid._unitParamName_) !== value) {
            dc.setParamValue(grid._unitParamName_, value, null, null, true);
            cmpToolbarFilterCombo._setContextFilter_(); //SONE-2758
            dc.delayedQuery();
        }
    },

    _buildCurrenciesCombo_: function() {
        var store = getCurrenciesComboStore();
        var combo = Ext.create('Ext.form.ComboBox', {
            id: this._idCmpCurrenciesCombo_,
            fieldLabel: Main.translate("viewAndFilters", "defaultCurrency__lbl"),
            xtype: 'combo',
            hidden: this._showCurrency_ !== true ? true : false,
            labelWidth: 100,
            labelAlign: "right",
            labelSeparator: Main.viewConfig.FORM_LABEL_SEPARATOR,
            name: 'default-currency',
            selectOnFocus: true,
            forceSelection: false,
            autoSelect: false,
            allowBlank: true,
            allQuery: "%",
            triggerAction: "all",
            minChars: 0,
            displayField: "code",
            queryMode: "remote",
            remoteSort: true,
            remoteFilter: true,
            queryDelay: 100,
            width: 220,
            pageSize: store.pageSize,
            listConfig: {
                getInnerTpl: function() {
                    return "<span>{code}, {name}</span>";
                },
                minWidth: 280
            },
            listeners: {
                select: {
                    scope: this,
                    fn: function(cb) {
                        var v = cb.getValue();
                        this._currenciesChanged_(v);
                    }
                },
                blur: {
                    scope: this,
                    fn: function(cb) {
                        var v = cb.getValue();
                        this._currenciesChanged_(v);
                    }
                }
            },
            store: store,
            value: this._setDefaultCurrencyValue_()
        });
        return combo;
    },
    
    _setDefaultCurrencyValue_ : function() {
    	if (!Ext.isEmpty(this._cmpCurrenciesComboMain_)) { 
    		return this._cmpCurrenciesComboMain_.getValue() 
    	}
    },

    _buildUnitsCombo_: function() {
        var store = getUnitsComboStore();
        var combo = Ext.create('Ext.form.ComboBox', {
            id: this._idCmpUnitsCombo_,
            fieldLabel: Main.translate("viewAndFilters", "defaultUnit__lbl"),
            hidden: this._showUnit_ !== true ? true : false,
            labelWidth: 100,
            labelAlign: "right",
            labelSeparator: Main.viewConfig.FORM_LABEL_SEPARATOR,
            width: 220,
            name: 'default-unit',
            selectOnFocus: true,
            forceSelection: false,
            autoSelect: false,
            allowBlank: true,
            allQuery: "%",
            triggerAction: "all",
            minChars: 0,
            displayField: "code",
            queryMode: "remote",
            remoteSort: true,
            remoteFilter: true,
            queryDelay: 100,
            pageSize: store.pageSize,
            listConfig: {
                getInnerTpl: function() {
                    return "<span>{code}, {name}</span>";
                },
                minWidth: 280
            },
            listeners: {
                select: {
                    scope: this,
                    fn: function(cb) {
                        var v = cb.getValue();
                        this._unitsChanged_(v);
                    }
                },
                blur: {
                	scope: this,
                    fn: function(cb) {
                        var v = cb.getValue();
                        this._unitsChanged_(v);
                    }
                }
            },
            store: store,
            value: this._setUnitValue_()
        });

        return combo;
    },
    
    _setUnitValue_ : function() {
    	if (!Ext.isEmpty(this._cmpUnitsComboMain_)) { 
    		return this._cmpUnitsComboMain_.getValue() 
    	}
    },
    
    _setTotalsValue_ : function() {
    	if (!Ext.isEmpty(this._cmpTotalsComboMain_)) { 
    		return this._cmpTotalsComboMain_.getValue()
    	}
    },

    _buildTotalsCombo_: function() {
        var combo = Ext.create('Ext.form.ComboBox', {
            id: this._idCmpTotalsCombo_,
            fieldLabel: Main.translate("viewAndFilters", "totals__lbl"),
            hidden: (this._showTotal_ !== true && Ext.isEmpty(this._gridName_._totalPanelId_)) ? true : false,
            value: this._setTotalsValue_(),
            labelWidth: 100,
            labelAlign: "right",
            labelSeparator: Main.viewConfig.FORM_LABEL_SEPARATOR,
            width: 220,
            name: 'totals',
            store: getTotalsComboStore(),
            queryMode: 'local',
            valueField: 'value',
            displayField: 'total',
            tpl: Ext.create('Ext.XTemplate', '<tpl for=".">', '<div class="x-boundlist-item">{total}</div>', '</tpl>'),
            pageSize: 0,
            listeners: {
                select: {
                    scope: this,
                    fn: function(cb) {
                        var val = cb.getValue();
                        
                    	// Dan: on select update the value of the totals combo from the toolbar
                        
                        this._cmpTotalsComboMain_.setValue(val);

                        // Enable or disable the totals

                        if (cb._onFocusVal_ !== val && !(Ext.isEmpty(val) && Ext.isEmpty(cb._onFocusVal_))) {
                            this._enableSummary_(val === 1);
                        }
                    }
                }
            }
        });
        return combo;
    },

    _getGrid_: function() {
        var grid = this._gridName_;
        return grid;
    },

    _enableSummary_: function(enable) {
        var grid = this._getGrid_();
        grid._showSummary_(enable);
    },

    _buildButtons_: function() {

        var buttons = [{
            text: Main.translate("viewAndFilters", "update__lbl"),
            glyph: "xf05d@FontAwesome",
            disabled: this._isOwner_() === false || Ext.isEmpty(this._viewRecord_.getValue()) ? true : false,
            //disabled: true,
            name: 'saveBtn',
            id: this._idCmpSaveBtn_,
            listeners: {
                click: {
                    scope: this,
                    fn: function() {
                        this._doSaveView_();
                    }
                }
            }
        }, {
            text: Main.translate("viewAndFilters", "createNew__lbl"),
            name: 'saveAsBtn',
            glyph: "xf0c7@FontAwesome",
            id: this._idCmpSaveAsBtn_,
            disabled: true,
            listeners: {
                click: {
                    scope: this,
                    fn: function() {
                        this._doSaveAsView_();
                    }
                }
            }
        }, {
            text: Main.translate("viewAndFilters", "remove__lbl"),
            disabled: this._isOwner_() === false || Ext.isEmpty(this._viewRecord_.getValue()) ? true : false,
            glyph: "xf05e@FontAwesome",
            name: 'deleteBtn',
            id: this._idCmpDeleteBtn_,
            listeners: {
                click: {
                    scope: this,
                    fn: function() {
                        this._doDeleteView_();
                    }
                }
            }
        }];
        return buttons;
    }
});