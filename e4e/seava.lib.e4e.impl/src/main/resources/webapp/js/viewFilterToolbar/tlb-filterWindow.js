Ext.define("Toolbar.Filter.Window", {

    extend: 'Ext.window.Window',
    name: 'filterWindow',
    layout: 'fit',
    bodyStyle: 'border:0px',
    resizable: false,
    cls: 'sone-filter-window',

    // =================== Custom properties ===================

    _grid_: null,
    _filterGrid_: null,
    _filterStore_: null,
    _fieldStore_: null,
    _filterCombo_: null,
    _standardFilterCfg_: null,
    _advancedFilterAlias_: null,
    _advancedFilterDs_: null,
    _doNotSetFilterName_: null,

    _idCmpAdvancedFilterPanel_: null,
    _idCmpFilterName_: null,
    _idCmpDefaultFilter_: null,
    _idCmpAvailableSystemWideFilter_: null,

    _idCmpApplyBtn_: null,
    _idCmpCancelBtn_: null,
    _idCmpSaveBtn_: null,
    _idCmpDeleteBtn_: null,
    _idCmpSaveAsBtn_: null,

    _idCmpViewComboMain_: null,
    _idCmpFilterComboMain_: null,

    _cmpViewComboMain_: null,
    _cmpFilterComboMain_: null,
    _cmpDefaultFilter_: null,


    // =================== End custom properties ===================

    initComponent: function(config) {

        // get 'master' combo-s
        this._cmpViewComboMain_ = this._getComponentById_(this._idCmpViewComboMain_);
        this._cmpFilterComboMain_ = this._getComponentById_(this._idCmpFilterComboMain_);
        this._cmpDefaultFilter_ = this._getComponentById_(this._idCmpDefaultFilter_);

        this._idCmpAdvancedFilterPanel_ = Ext.id();
        this._idCmpFilterName_ = Ext.id();
        this._idCmpDefaultFilter_ = Ext.id();
        this._idCmpAvailableSystemWideFilter_ = Ext.id();
        this._idCmpApplyBtn_ = Ext.id();
        this._idCmpCancelBtn_ = Ext.id();
        this._idCmpSaveBtn_ = Ext.id();
        this._idCmpDeleteBtn_ = Ext.id();
        this._idCmpSaveAsBtn_ = Ext.id();

        this._buildElements_();
        var cfg = {
            title: this._filterCombo_.getValue() == null ? Main.translate("viewAndFilters", "filterWindow__lbl") : Main.translate("viewAndFilters", "currentFilter__lbl") + this._filterCombo_.getValue(),
            width: 475,
            height: 500,
            layout: 'border',
            buttonAlign: 'center',
            modal: true,
            items: this._buildItems_(),
            buttons: this._buildButtons_(),
            listeners: {
                show: {
                    scope: this,
                    fn: function() {
                        if (this._filterStore_.data.items.length > 0) {
                            var pnlAdvFilter = this._getComponentById_(this._idCmpAdvancedFilterPanel_);
                            pnlAdvFilter.expand();
                        }
                    }
                }
            }
        };

        Ext.apply(this, cfg, config);
        this.callParent(arguments);
    },

    _getComponentById_: function(id) {
        var component = Ext.ComponentQuery.query('[id=' + id + ']')[0];
        return component;
    },
    
    // Function to add the dc context parameter values to the standard filter if the DC is in context of another DC

    _setContextFilter_: function(silent) {

        if (this._isCtrlInSomeContext_() === true) {

            var ctrl = this._grid_._controller_;
            var currentFilter = ctrl.filter.data;
            var ctxData = ctrl.dcContext.ctxData;
            var contextFieldName;
            var contextFieldvalue;
            
            if (silent === true) {
                ctrl.noReset = true;
            }
            
            for (var i = 0; i < ctxData.length; i++) {
	        	
	        	contextFieldName = ctxData[i].name;
	        	contextFieldvalue = ctxData[i].value;
	        	
	        	for (var key in currentFilter) {
                    if (key === contextFieldName) {
                        currentFilter[key] = contextFieldvalue;
                    }
                }
	        }
        }
    },

    _onApply_: function() {
        var ctrl = this._grid_._controller_;
        var advancedFilterValue = this._getAdvancedFilterEncodedValue_();
        var resetButton = Ext.getCmp(this._grid_._resetButtonId_);

        this._setContextFilter_();
        ctrl.advancedFilter = Ext.decode(advancedFilterValue);
        this._filterStore_.commitChanges();

        this._filterCombo_._doNotClearValue_ = true;
        resetButton.doSetup();
        ctrl.noReset = false;
        ctrl.delayedQuery();
    },
    
    _getAdvancedFilterEncodedValue_: function(){
    	this._doConvertDateFormatForServer_();
    	var res = Ext.encode(Ext.pluck(this._filterStore_.data.items, 'data'));
    	this._doConvertDateFormatForDisplay_();
    	return res;
    },
    
    _doConvertDateFormatForDisplay_: function(){
        var items = this._filterStore_.data.items;

        Ext.each(items, function(item) {

            var value1 = item.data.value1;
            var value2 = item.data.value2;
            
            var newValue1 = this._getDateFromString_(value1);
            if( newValue1 ){
                item.data["value1"] = Ext.util.Format.date(newValue1,Main.DATE_FORMAT);
            }

            var newValue2 = this._getDateFromString_(value2);
            if( newValue2 ){
                item.data["value2"] = Ext.util.Format.date(newValue2,Main.DATE_FORMAT);
            }
        }, this);
    },

    _doConvertDateFormatForServer_: function() {
        // Check the values and convert the dates back to date format
        var items = this._filterStore_.data.items;

        Ext.each(items, function(item) {

            var value1 = item.data.value1;
            var value2 = item.data.value2;
            var newValue1 = this._getDateFromString_(value1);
            var newValue2 = this._getDateFromString_(value2);
            
            if( newValue1 ){
            	if( Ext.isDate(newValue1) ){
            		var op = item.data.operation;
            		switch(op) {
	            	    case "<":
	            	    	break;
	            	    case "<=":
	                		// add a day -1 second
	            	    	newValue1.setHours(23,59,59);
	            	        break;
	            	    case ">":
	            	    	newValue1.setHours(23,59,59);
	            	        break;
	            	    case ">=":
	            	    	break;
	            	    case "between":
	                		// clear time
	            	    	newValue1.setHours(0,0,0,0);
	            	        break;
	            	    default:
	            	    	break;
	            	}
            	}
            	item.data["value1"] = Ext.util.Format.date(newValue1,Main.MODEL_DATE_FORMAT);
            }

            if( newValue2 ){
            	if( Ext.isDate(newValue2) ){
            		// add a day -1 second
            		newValue2.setHours(23,59,59);
            	}
                item.data["value2"] = Ext.util.Format.date(newValue2,Main.MODEL_DATE_FORMAT);
            }
        }, this);
    },

    _comboSetter_: function(comboBox, value) {
        var store = comboBox.store;
        var valueField = comboBox.valueField;
        var displayField = comboBox.displayField;

        var recordNumber = store.findExact(valueField, value, 0);

        if (recordNumber === -1)
            return -1;

        var displayValue = store.getAt(recordNumber).data[displayField];
        comboBox.setValue(value);
        comboBox.setRawValue(displayValue);
        comboBox.selectedIndex = recordNumber;
        return recordNumber;
    },

    _findFilterComboRecord_: function(prop, value, store) {
        var record;
        if (store.getCount() > 0) {
            store.each(function(r) {
                if (r.data[prop] == value) {
                    record = r;
                    return false;
                }
            });
        }
        return record;
    },

    _updateDefaultViewFilter_: function(filterId, checked) {

        var viewCombo = Ext.getCmp(this._idCmpViewComboMain_);
        var s = viewCombo.getStore();
        var r = s.getById(viewCombo._selectedId_);

        if (checked === true && !Ext.isEmpty(r)) {
            r.beginEdit();
            r.set("advancedFilterId", filterId);
            r.endEdit();
            s.commitChanges();
        }
    },

    _isCtrlInSomeContext_: function() {

        var isInSomeContext = false;
        var ctrl = this._grid_._controller_;

        if (!Ext.isEmpty(ctrl.dcContext)) {
            isInSomeContext = true;
        }
        return isInSomeContext;
    },

    _clearCtxFilter_: function() {

        // Do not save the value of the field used to link the current DC to the parent DC if the current DC is in some context

    	if (this._isCtrlInSomeContext_() === true) {
    		
	        var ctrl = this._grid_._controller_;
	        var currentFilter = ctrl.filter.data;
	        var ctxData = ctrl.dcContext.ctxData;
	        var contextFieldName;
	        
	        for (var i = 0; i < ctxData.length; i++) {
	        	
	        	contextFieldName = ctxData[i].name;
	        	
	        	for (var key in currentFilter) {
	                if (key === contextFieldName) {
	                    currentFilter[key] = null;
	                }
	            }
	        }
        }
        return currentFilter;
        // End Dan
    },

    _doSaveFilter_: function() {
        var store = this._filterCombo_.getStore();
        var id = this._filterCombo_._selectedId_;
        this._grid_._controller_.noReset = true;
        var record = store.getById(id);
        if (!record) {
            return;
        }
        
        // ===============================================================================================
        // Dan: Start checking if the current user has sufficient privilefges to update the current filter
        // ===============================================================================================

        var createdBy = record.get("createdBy");
        var currentUser = getApplication().getSession().getUser().code;

        if (createdBy === currentUser || currentUser === __SUPERUSER__) {

            //----------------- Update the default view filter id -----------------

            var checkbox = Ext.getCmp(this._idCmpDefaultFilter_);
            var checkBoxValue = checkbox.getValue();
            var viewCombo = Ext.getCmp(this._idCmpViewComboMain_);
            var viewStore = viewCombo.getStore();
            var viewRecord = viewStore.getById(viewCombo._selectedId_);
            var filterId = record.get("id");
            var name = this._getComponentById_(this._idCmpFilterName_).getValue();
            var defaultFilter = this._getComponentById_(this._idCmpDefaultFilter_);
            var filterValue, standardFilterValue;
            var availableSystemwide = this._getComponentById_(this._idCmpAvailableSystemWideFilter_).getValue();
            var advancedFilterValue = this._getAdvancedFilterEncodedValue_();
            var appliedStandardFilter = this._grid_._controller_.filter.data;
            var resetButton = Ext.getCmp(this._grid_._resetButtonId_);
            
            appliedStandardFilter = resetButton._clearCtxFilter_(appliedStandardFilter);
            
            standardFilterValue = Ext.encode(appliedStandardFilter);
            standardFilterValue = standardFilterValue.replace(/""/g, null);
            
            if (viewRecord) {
            	viewRecord.beginEdit();
                if (checkBoxValue === true) {
                    viewRecord.set("advancedFilterId", filterId);
                }
                viewRecord.set("standardFilterValue",standardFilterValue);
                viewRecord.endEdit();
                viewStore.commitChanges();
            }
            
            //----------------- New values -----------------

            // Dan: Disabled fields cannot be saved

            if (defaultFilter.disabled === true) {
                filterValue = false;
            } else {
                filterValue = defaultFilter.getValue();
            }

            // http://stackoverflow.com/questions/16262081/how-to-set-multiple-values-at-one-time-to-ext-data-model
            
            record.beginEdit();
            
            var sanitizedName = name.replace(/"|&|'|<|>|\//g,'');

            record.set("name", sanitizedName);
            record.set("standardFilterValue", standardFilterValue);
            record.set("advancedFilterValue", advancedFilterValue);
            record.set("defaultFilter", filterValue);
            record.set("availableSystemwide", availableSystemwide);

            record.endEdit();

            //----------------- End new values -----------------

            store.commitChanges();
            this._updateViewFilterId_(checkbox);
            
            // Dan: Deactivate the reset button
            Ext.getCmp(this._grid_._resetButtonId_)._deActivate_();
            
            if (Ext.isEmpty(this._doNotSetFilterName_)) {
                this._filterCombo_.setValue(sanitizedName);
            }

            this._grid_._controller_.advancedFilter = Ext.decode(advancedFilterValue);
            this._setContextFilter_();
            this._grid_._controller_.delayedQuery();
            this._filterCombo_.getStore().load();
            viewCombo.getStore().load();
            this.close();
            // ===============================================================================================
            // Dan: End checking if the current user has sufficient privilefges to update the current filter
            // ===============================================================================================

        } else {
            Main.error(Main.translate("viewAndFilters", "notEditable__lbl"));
        }
    },

    _doSaveFilterAs_: function() {

        var advancedFilterValue = this._getAdvancedFilterEncodedValue_();
        
        var currentFilter;
        if (this._isCtrlInSomeContext_() === true) {
        	currentFilter = Ext.encode(this._clearCtxFilter_());
        } else {
        	currentFilter = Ext.encode(this._grid_._controller_.filter.data);
        	currentFilter = currentFilter.replace(/""/g, null); 
        }
        var standardFilterValue = currentFilter;
        var cmp = this._grid_.stateId;
        var cmpType = "frame-dcgrid";
        var name = this._getComponentById_(this._idCmpFilterName_).getValue();
        var isCurrentFilter = this._getComponentById_(this._idCmpDefaultFilter_).getValue();
        var dsName = this._advancedFilterAlias_;
        var availableSystemwide = this._getComponentById_(this._idCmpAvailableSystemWideFilter_).getValue();
        
        var sanitizedName = name.replace(/"|&|'|<|>|\//g,'');

        Ext.Ajax.request({
            url: Main.dsAPI(dsName, "json").create,
            method: "POST",
            params: {
                data: Ext.JSON.encode({
                    name: sanitizedName,
                    standardFilterValue: standardFilterValue,
                    advancedFilterValue: advancedFilterValue,
                    cmp: cmp,
                    cmpType: cmpType,
                    defaultFilter: isCurrentFilter,
                    availableSystemwide: availableSystemwide,
                    active: true
                })
            },
            success: function() { // parameters: response, opts

            	this._setContextFilter_();
                this._filterCombo_._doNotClearValue_ = true;
                this._grid_._controller_.advancedFilter = Ext.decode(advancedFilterValue);
                this._grid_._controller_.delayedQuery();
                var filterStore = this._filterCombo_.getStore();

                // Dan: E5 bug fixes

                filterStore.load({
                    callback: function() {
                    	if (!Ext.isEmpty(this._filterCombo_)) {
                    		this._filterCombo_.setValue(sanitizedName);
                            var record = this._findFilterComboRecord_("name", sanitizedName, filterStore);
                            var id = record.data.id;
                            this._filterCombo_._selectedId_ = id;

                            // Dan - update the default view filter ID

                            this._updateDefaultViewFilter_(id, isCurrentFilter);
                            this.close();
                    	}
                    },
                    scope: this
                });
            },
            failure: function(response) {
                Main.error(Ext.getResponseErrorText(response));
            },
            scope: this
        });
    },

    _updateViewFilterId_: function(checkbox) {
    	
        var original = checkbox.originalValue;
        var raw = checkbox.rawValue;

        if (original === true && raw === false) {

            var viewCombo = Ext.getCmp(this._idCmpViewComboMain_);
            var filterCombo = Ext.getCmp(this._idCmpFilterComboMain_);
            var viewStore = viewCombo.getStore();
            var viewRecord = viewStore.getById(viewCombo._selectedId_);

            viewRecord.beginEdit();
            viewRecord.set("advancedFilterId", null);
            viewRecord.endEdit();

            viewStore.commitChanges();
            this._resetFilterValues_();
        }
    },

    _isEmptyObject_: function(obj) {

        // null and undefined are "empty"
        if (obj === null || obj === undefined){
        	return true;
        }

        // Assume if it has a length property with a non-zero value
        // that that property is correct.
        if (obj.length > 0){
        	return false;
        }
        if (obj.length === 0){
        	return true;
        }

        // Otherwise, does it have any properties of its own?
        // Note that this doesn't handle
        // toString and valueOf enumeration bugs in IE < 9
        for (var key in obj) {
            if (hasOwnProperty.call(obj, key)){
            	return false;
            }
        }

        return true;
    },

    _resetFilterValues_: function() {

        var viewCombo = Ext.getCmp(this._idCmpViewComboMain_);
        var filterComboMain = Ext.getCmp(this._idCmpFilterComboMain_);
        viewCombo._clearFilterValue_ = true;

        // Dan: E5
        
        var filterCombo = this._filterCombo_;        
        filterComboMain.setValue("");        
        
        filterCombo.getStore().load({
            callback: function() {            	
                filterCombo.setValue("");                
            }
        }, this);

        this._grid_._controller_.advancedFilter = null;

        // Clear grid filter
        this._grid_._controller_.doClearQuery();
        this._grid_._controller_.store.removeFilter();

        // Clear the default elements
        if (this._isEmptyObject_() === false) {
            [0].filter.id = null;
        }

        this._filterCombo_._selectedId_ = null;
        this._doNotSetFilterName_ = true;
    },

    _isOwner_: function() {
        var store = this._filterCombo_.getStore();
        var id = this._filterCombo_._selectedId_;
        var record = store.getById(id);
        var hasPriviledges;

        if (!record) {
            return;
        }

        var createdBy = record.get("createdBy");
        var currentUser = getApplication().getSession().getUser().code;

        if (createdBy === currentUser || currentUser === __SUPERUSER__) {
            hasPriviledges = true;
        } else {
            hasPriviledges = false;
        }

        return hasPriviledges;
    },

    _onClear_: function() {

    	 // Dan
        // SONE-1132: Filter: the "Clear" function should apply for "Advanced Filter" section, also 

        var resetButton = Ext.getCmp(this._grid_._resetButtonId_);
        var _ctrl = this._grid_._controller_;
        
        this._filterStore_.removeAll();
        resetButton.doSetup();
        
        // End Dan
        _ctrl.doClearAllFilters();
        resetButton._deActivate_();
        resetButton._filtersCleared_ = true;
        _ctrl.delayedQuery();
    },

    _onRemove_: function() {
        this._filterStore_.remove(this._filterGrid_.getSelectionModel().getSelection());
        var r = this._filterStore_.first();
        this._filterGrid_.getSelectionModel().select(r);
    },

    _onAdd_: function() {
        this._filterStore_.add({});
        var r = this._filterStore_.last();
        // SONE-4350 -- Begin
        r.set("operation","=");
        if(this._fieldStore_.data.items.length > 0) {
	        r.set("fieldName",this._fieldStore_.data.items[0].data.name);
	        r.set("title",this._fieldStore_.data.items[0].data.title);
        }
        // SONE-4350 -- End
        this._filterGrid_.getSelectionModel().select(r);

    },

    _onCopy_: function() {
        var s = this._filterGrid_.getSelectionModel().getSelection()[0].data;
        this._filterStore_.add({
            field: s.field,
            title: s.title,
            operation: s.operation,
            value1: s.value1,
            value2: s.value2
        });
        var r = this._filterStore_.last();
        this._filterGrid_.getSelectionModel().select(r);
    },

    _onDelete_: function() {
        var store = this._filterCombo_.getStore();
        var id = this._filterCombo_._selectedId_;
        var resetButton = Ext.getCmp(this._grid_._resetButtonId_);
        var record = store.getById(id);
        if (!record) {
            return;
        }
        var recordId = record.get("id");
        var dsName = this._advancedFilterAlias_;

        // ===============================================================================================
        // Dan: Start checking if the current user has sufficient privilefges to delete the current filter
        // ===============================================================================================

        var createdBy = record.get("createdBy");
        var currentUser = getApplication().getSession().getUser().code;

        if (createdBy === currentUser || currentUser === __SUPERUSER__) {

            Ext.MessageBox.confirm(Main.translate("viewAndFilters", "delete__lbl"), Main.translate("viewAndFilters", "askToDeleteFilter1__lbl") + record.get("name") + Main.translate("viewAndFilters", "askToDeleteFilter2__lbl"), function(btn) {
                if (btn === "yes") {
                    Ext.Ajax.request({
                        url: Main.dsAPI(dsName, "json").destroy,
                        method: "POST",
                        params: {
                            data: Ext.JSON.encode({
                                id: recordId
                            })
                        },
                        success: function() {
                        	
                            var filterCombo = this._filterCombo_;

                            filterCombo.getStore().load({
                                callback: function() {
                                    filterCombo.setValue("");
                                }
                            }, this);

                            this._grid_._controller_.advancedFilter = null;
                            resetButton._deActivate_();
                            // Clear grid filter
                            this._grid_._controller_.doClearAllFilters();

                            // Clear the filter_id refference
                            var viewCombo = Ext.getCmp(this._idCmpViewComboMain_);
                            var viewStore = viewCombo.getStore();
                            var viewRecord = viewStore.getById(viewCombo._selectedId_);

                            if (!Ext.isEmpty(viewRecord)) {
                                var viewFilterId = viewRecord.get("advancedFilterId");
                                var currentFilterId = this._filterCombo_._selectedId_;

                                if (viewFilterId === currentFilterId) {
                                    viewRecord.beginEdit();
                                    viewRecord.set("advancedFilterId", null);
                                    viewRecord.endEdit();

                                    viewStore.commitChanges();
                                }
                            }

                            // End clear the filter_id refference

                            this._grid_._controller_.delayedQuery();

                            this.close();
                        },
                        failure: function(response) {
                            Main.error(Ext.getResponseErrorText(response));
                        },
                        scope: this
                    });
                } else {
                    return false;
                }
            }, this);

            // ===============================================================================================
            // Dan: End checking if the current user has sufficient privilefges to delete the current filter
            // ===============================================================================================

        } else {
            Main.error(Main.translate("viewAndFilters", "notEditable__lbl"));
        }
    },

    _buildNorthToolbar_: function() {

        var tbar = Ext.create('Ext.toolbar.Toolbar', {
            items: ["->", {
                id: this._idCmpApplyBtn_,
                text: Main.translate("viewAndFilters", "apply__lbl"),
                glyph: "xf00c@FontAwesome",
                listeners: {
                    click: {
                        scope: this,
                        fn: function() {
                            this._onApply_();
                        }
                    },
                    render: function(c) {
                        return new Ext.ToolTip({
                            target: c.getEl(),
                            html: Main.translate("viewAndFilters", "applyTmpFilter__tlp"),
                            trackMouse:true,
                            anchorOffset: 5
                        });
                    }
                }
            }, {
                id: this._idCmpCancelBtn_,
                text: Main.translate("viewAndFilters", "clear__lbl"),
                glyph: "xf00d@FontAwesome",
                listeners: {
                    click: {
                        scope: this,
                        fn: function() {
                            this._onClear_();
                        }
                    },
                    render: function(c) {
                        return new Ext.ToolTip({
                            target: c.getEl(),
                            html: Main.translate("viewAndFilters", "removeFilter__tlp"),
                            trackMouse:true,
                            anchorOffset: 5
                        });
                    }
                }
            }]
        });

        return tbar;

    },

    _buildNorthPanel_: function() {
        var panel = {
            region: 'center',
            xtype: 'panel',
            layout: 'fit',
            split: true,
            margins: 0,
            bodyPadding: 0,
            bodyStyle: 'font-size:11px; border:0px; background:transparent',
            items: this._buildStandardFilterGrid_()
            // ,dockedItems: this._buildNorthToolbar_()
        };
        return panel;
    },

    //============== Temporary solution =================

    _buildStandardFilterGrid_: function() {

        var grid = this._standardFilterCfg_;
        delete grid["tbar"];
        return grid;
    },

    //============== End Temporary solution =================

    _buildCenterPanel_: function() {
        var panel = {
            id: this._idCmpAdvancedFilterPanel_,
            //region: 'center',
            cls: 'sone-advanced-filter-panel',
            region: 'south',
            xtype: 'panel',
            layout: 'fit',
            collapsible: true,
            collapsed: true,
            floatable: false,
            height: 180,
            title: Main.translate("viewAndFilters", "advancedFilter__lbl"),
            split: true,
            margins: 0,
            bodyPadding: 0,
            bodyStyle: 'font-size:11px; border:0px; background:transparent',
            items: this._filterGrid_
        };
        return panel;
    },

    _buildSouthPanel_: function() {
        return {
            region: 'south',
            xtype: 'panel',
            //split: true,
            bodyPadding: '6px 12px 6px 12px',
            layout: 'anchor',
            frame: true,
            bodyStyle: 'font-size:11px',
            items: [{
                xtype: 'textfield',
                fieldLabel: Main.translate("viewAndFilters", "filterName__lbl"),
                labelSeparator: Main.viewConfig.FORM_LABEL_SEPARATOR,
                id: this._idCmpFilterName_,
                name: 'filterName',
                labelWidth: 70,
                width: 260,

                // Dan: E5

                value: this._filterCombo_.getRawValue(),
                listeners: {
                    change: {
                        scope: this,
                        fn: function(el) {

                        	var btnSave = this._getComponentById_(this._idCmpSaveBtn_);
                            var btnSaveAs = this._getComponentById_(this._idCmpSaveAsBtn_);
                            var btnDelete = this._getComponentById_(this._idCmpDeleteBtn_);
                            var defaultFilterCombo = this._getComponentById_(this._idCmpDefaultFilter_);
                            var disabled = true;
                            var saveDeleteDisabled = false;
                            var record;
                            
                            var viewCombo = this._cmpViewComboMain_;
                            var viewStore = viewCombo.store;
                            var viewRecord = viewStore.getById(viewCombo._selectedId_);
                            var viewCreatedBy = "";
                            if (viewRecord) {
                            	viewCreatedBy = viewRecord.get("createdBy");
                            }                           
                            
                            var currentUser = getApplication().getSession().getUser().code;

                            // Dan: E5
                            if (el.getValue().trim() !== this._filterCombo_.getRawValue() && el.getValue().trim() !== "") {
                                disabled = false;
                                saveDeleteDisabled = true;
                            }

                            btnSaveAs.setDisabled(disabled);
                            btnSave.setDisabled(!disabled);
                            // save is enabled only if record exists
                            var store = this._filterCombo_.getStore();
                            var id = this._filterCombo_._selectedId_;
                            record = store.getById(id);
                            if (!record || el.getValue().trim() === "") {
                                saveDeleteDisabled = true;
                            }
                            if (this._isOwner_() === true) {
                                btnDelete.setDisabled(saveDeleteDisabled);
                            }
                            if (viewCreatedBy !== currentUser) {
                            	defaultFilterCombo.setValue(false);
                            	defaultFilterCombo.setDisabled(true);
                            }
                        }
                    }
                }
            }, {
                xtype: "fieldcontainer",
                layout: "column",
                margin: "5 0 0 0",
                items: [
                    this._buildDefaultFilterCheckBox_(), {
                        xtype: "displayfield",
                        fieldCls: "x-form-item-label",
                        value: Main.translate("viewAndFilters", "applyFilterForView__lbl")
                    }
                ]
            }, {
                xtype: "fieldcontainer",
                layout: "column",
                margin: "5 0 0 0",
                items: [
                    this._buildAvailableSystemWideCheckBox_(), {
                        xtype: "displayfield",
                        fieldCls: "x-form-item-label",
                        value: Main.translate("viewAndFilters", "share__lbl")
                    }
                ]
            }]
        };
    },

    // Dan : set the checkbox disabled state based on having or not having an already selected view 

    _checkViewExistence_: function() {
        var viewCombo = this._cmpViewComboMain_;
        var i = viewCombo._selectedId_;
        var r = true;
        if (!Ext.isEmpty(i)) {
            r = false;
        }
        return r;
    },

    _checkViewAvailableSystemWide_: function() {

        var disabled = true;

        if (this._checkViewExistence_() === false) {

            // If we have a selected view check if it is available system wide

            var viewCombo = this._cmpViewComboMain_;
            var viewStore = viewCombo.getStore();
            var viewRecord = viewStore.getById(viewCombo._selectedId_);

            var filterCombo = this._filterCombo_;
            var filterId = filterCombo._selectedId_;
            var filterStore = filterCombo.getStore();
            var filterRecord = filterStore.getById(filterId);

            if (!viewRecord || !filterRecord) {
                return;
            }

            var viewAvailableSystemwide = viewRecord.get("availableSystemwide");
            var filterAvailableSystemWide = filterRecord.get("availableSystemwide");

            // If the view is available system wide

            if (viewAvailableSystemwide === true) {
                // Check if we have a filter selected
                if (Ext.isEmpty(filterId)) {
                    disabled = true;
                } else {
                    // check if the selected filter is systemwide
                    if (filterAvailableSystemWide === true) {
                        disabled = false;
                    } else {
                        disabled = true;
                    }
                }
            } else {
                disabled = false;
            }
        }
        return disabled;
    },

    _checkDefaultFilter_: function() {

        var checked = false;

        var filterCombo = this._filterCombo_;
        var filterId = filterCombo._selectedId_;

        var viewCombo = this._cmpViewComboMain_;
        var viewId = viewCombo._selectedId_;
        var viewStore = viewCombo.getStore();
        var viewRecord = viewStore.getById(viewId);

        if (!Ext.isEmpty(viewRecord)) {
            var viewFilterId = viewRecord.get("advancedFilterId");

            if (filterId === viewFilterId) {
                checked = true;
            }
        }
        return checked;
    },

    _checkFilterAvailableSystemWide_: function() {

        var checked = false;
        var filterCombo = this._filterCombo_;

        var s = filterCombo.getStore();
        var r = s.getById(filterCombo._selectedId_);

        if (!r) {
            return;
        }

        if (r.get("availableSystemwide") === true) {
            checked = true;
        }
        return checked;
    },

    _buildDefaultFilterCheckBox_: function() {

        var combo = {
            id: this._idCmpDefaultFilter_,
            xtype: 'checkbox',
            name: 'isCurrentFilter',
            disabled: this._checkViewAvailableSystemWide_(),
            checked: this._checkDefaultFilter_(),
            listeners: {
                afterrender: {
                    scope: this,
                    fn: function(el) {
                        if (el.getValue() === true) {
                            el.setDisabled(false);
                        }
                    }
                }
            }
        };

        return combo;
    },

    _buildAvailableSystemWideCheckBox_: function() {

        var combo = {
            id: this._idCmpAvailableSystemWideFilter_,
            xtype: 'checkbox',
            name: 'isCurrentFilter',
            // cls: 'sone-no-label-after',
            checked: this._checkFilterAvailableSystemWide_(),
            listeners: {
                change: {
                    scope: this,
                    fn: function(el) {
                        var store = this._cmpViewComboMain_.getStore();
                        var id = this._cmpViewComboMain_._selectedId_;
                        var record = store.getById(id);

                        if (!record) {
                            return;
                        }

                        var viewAvailableSystemWide = record.get("availableSystemwide");
                        var defaultFilter = Ext.getCmp(this._idCmpDefaultFilter_);
                        var viewCombo = this._cmpViewComboMain_;
                        var viewStore = viewCombo.store;
                        var viewRecord = viewStore.getById(viewCombo._selectedId_);
                        var viewCreatedBy = viewRecord.get("createdBy");
                        var currentUser = getApplication().getSession().getUser().code;

                        var value = el.getValue();

                        if (value === false) {
                            if (viewAvailableSystemWide === true) {
                                defaultFilter.setValue("");
                                defaultFilter.setDisabled(true);
                            } else {
                            	if (viewCreatedBy === currentUser) {
                            		defaultFilter.setDisabled(false);
                            	}
                            }
                        } else {
                            if (viewAvailableSystemWide === true && viewCreatedBy === currentUser) {
                                defaultFilter.setDisabled(false);
                            }
                        }
                    }
                }
            }
        };

        return combo;
    },

    // Tibi: rearange panels
    _buildFilterPanel_: function() {
        var panel = {
            region: 'center',
            xtype: 'panel',
            layout: 'border',
            margins: 0,
            bodyPadding: 0,
            bodyStyle: 'font-size:11px; border:0px; background:transparent',
            items: [this._buildNorthPanel_(), this._buildCenterPanel_()]
        };
        return panel;
    },

    _buildItems_: function() {
        var items = [
            this._buildFilterPanel_(),
            this._buildSouthPanel_()
        ];
        return items;
    },

    _buildElements_: function() {

        var af = this._grid_._controller_.advancedFilter;
        var dc = this._grid_._controller_;

        var _items = [];

        if (af != null && Ext.isArray(af)) {
            for (var i = 0, len = af.length; i < len; i++) {
                var title = dc.translateModelField(af[i].fieldName);
                var r = {
                    fieldName: af[i].fieldName,
                    title: title,
                    operation: af[i].operation,
                    value1: this._formatDate_(af[i].value1),
                    value2: this._formatDate_(af[i].value2)
                }
                _items[_items.length] = r;
            }
        }

        this._filterStore_ = Ext.create("Ext.data.Store", {
            fields: ["fieldName", "title", "operation", "value1", "value2"],
            data: {
                "items": _items
            },
            proxy: {
                type: "memory",
                reader: {
                    type: "json",
                    rootProperty: "items"
                }
            }
        });

        this._filterGrid_ = Ext.create("Ext.grid.Panel", {
            height: 200,
            plugins: [Ext.create('Ext.grid.plugin.CellEditing', {
                clicksToEdit: 1
            })],
            store: this._filterStore_,
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: this._buildFilterGridActions_()
            }],
            columns: this._buildFilterGridColumns_(),
            listeners: {
            	beforeedit: {
            		scope: this,
            		fn: function() {
                        this._setupCellEditor_(["value1", "value2"]);
            		}
            	}
            }
        });


    },

    /**
     * Create window level buttons.
     */
    _buildButtons_: function() {
        return [{
            id: this._idCmpApplyBtn_,
            text: Main.translate("viewAndFilters", "apply__lbl"),
            glyph: "xf00c@FontAwesome",
            listeners: {
                click: {
                    scope: this,
                    fn: function() {
                        this._onApply_();
                    }
                },
                render: function(c) {
                    return new Ext.ToolTip({
                        target: c.getEl(),
                        html: Main.translate("viewAndFilters", "applyTmpFilter__tlp"),
                        trackMouse:true,
                        anchorOffset: 5
                    });
                }
            }
        }, {
            id: this._idCmpCancelBtn_,
            text: Main.translate("viewAndFilters", "clear__lbl"),
            glyph: "xf00d@FontAwesome",
            listeners: {
                click: {
                    scope: this,
                    fn: function() {
                        this._onClear_();
                    }
                },
                render: function(c) {
                    return new Ext.ToolTip({
                        target: c.getEl(),
                        html: Main.translate("viewAndFilters", "removeFilter__tlp"),
                        trackMouse:true,
                        anchorOffset: 5
                    });
                }
            }
        }, {
            id: this._idCmpSaveBtn_,
            text: Main.translate("viewAndFilters", "update__lbl"),
            tooltip: {
            	text: Main.translate("viewAndFilters", "saveFilter__tlp"),
            	trackMouse:true,
                anchorOffset: 5
            },
            glyph: "xf0c7@FontAwesome",
            disabled: this._isOwner_() === false || Ext.isEmpty(this._filterCombo_.getValue()) ? true : false,
            scope: this,
            listeners: {
                click: {
                    scope: this,
                    fn: function() {
                        this._doSaveFilter_();
                    }
                }
            }
        }, {
            id: this._idCmpSaveAsBtn_,
            text: Main.translate("viewAndFilters", "createNew__lbl"),
            glyph: "xf0c7@FontAwesome",
            tooltip: {
            	text: Main.translate("viewAndFilters", "createNew__tlp"),
            	trackMouse:true,
                anchorOffset: 5
            },
            name: "saveFilterAs",
            disabled: true,
            listeners: {
                click: {
                    scope: this,
                    fn: function() {
                        this._doSaveFilterAs_();
                    }
                }
            }
        }, {
            id: this._idCmpDeleteBtn_,
            text: Main.translate("viewAndFilters", "deleteFilter__lbl"),
            glyph: "xf05e@FontAwesome",
            tooltip: {
            	text: Main.translate("viewAndFilters", "deleteFilter__tlp"),
            	trackMouse:true,
                anchorOffset: 5
            },
            disabled: this._isOwner_() === false || Ext.isEmpty(this._filterCombo_.getValue()) ? true : false,
            scope: this,
            handler: this._onDelete_
        }];
    },

    /**
     * Build toolbar actions.
     */
    _buildFilterGridActions_: function() {
        return ["->", {
            text: Main.translate("dcFilter", "new__lbl"),
            tooltip: {
            	text: Main.translate("dcFilter", "new__tlp"),
            	trackMouse:true,
                anchorOffset: 5
            },
            glyph: "xf067@FontAwesome",
            scope: this,
            handler: this._onAdd_
        }, {
            text: Main.translate("dcFilter", "delete__lbl"),
            tooltip: {
            	text: Main.translate("dcFilter", "delete__tlp"),
            	trackMouse:true,
                anchorOffset: 5
            },
            glyph: "xf068@FontAwesome",
            scope: this,
            handler: this._onRemove_
        }, {
            text: Main.translate("dcFilter", "copy__lbl"),
            tooltip: {
            	text: Main.translate("dcFilter", "copy__tlp"),
            	trackMouse:true,
                anchorOffset: 5
            },
            scope: this,
            glyph: "xf0c5@FontAwesome",
            handler: this._onCopy_
        }]
    },

    _dateRenderer_: function(value){
    	var res = value;
    	if( Ext.isDate(value) ){
    		res = Ext.util.Format.date(value,Main.DATE_FORMAT);
    	}
    	return res;
    },
    
    _setupCellEditor_: function(param) {

        var columns = this._filterGrid_.columnManager.getColumns();
        var selectedModel = this._filterGrid_.getSelectionModel().getSelection()[0];
        var selectedFieldName = selectedModel.get('fieldName');

        var textEditor = {
            xtype: "textfield",
            selectOnFocus: true
        }
        var dateEditor = {
            xtype: "datefield",
            format: Main.DATE_FORMAT,
            altFormats: Main.ALT_FORMATS,
            submitFormat: Main.DATE_FORMAT,
            submitValue: true,
            selectOnFocus: true
        }
        var comboEditor = {
            xtype: "combo",
            selectOnFocus: true,
            queryMode: "local",
            store: ["True", "False"]
        }

        if (!Ext.isEmpty(selectedFieldName)) {

        	// Dan: SONE-1965
        	var rec = this._fieldStore_.findRecord('name', selectedFieldName);
        	if( rec ){
	            var selectedFieldType = rec.data.type;
	            
	            Ext.each(columns, function(column) {
	                if (param instanceof Array) {
	                    if (param.indexOf(column.dataIndex) !== -1) {
	                        if (selectedFieldType === "bool" || selectedFieldType === "boolean") {
	                            column.setEditor(comboEditor);
	                            column.renderer = null;
	                        } else if (selectedFieldType === "date") {
	                            column.setEditor(dateEditor);
	                            column.renderer = this._dateRenderer_;
	                        } else {
	                            column.setEditor(textEditor);
	                            column.renderer = null;
	                        }
	                    }
	                } else {
	                    if (column.dataIndex === param) {
	                        if (selectedFieldType === "bool" || selectedFieldType === "boolean") {
	                            column.setEditor(comboEditor);
	                            column.renderer = null;
	                        } else if (selectedFieldType === "date") {
	                            column.setEditor(dateEditor);
	                            column.renderer = this._dateRenderer_;
	                        } else {
	                            column.setEditor(textEditor);
	                            column.renderer = null;
	                        }
	                    }
	                }
	            }, this);
        	}
        }
    },

	_getDateFromString_ : function(value) {
		var res = null;
		if (Ext.isDate(value)){
			res = value;
		} else {
			if (!Ext.isEmpty(value)) {
				value = ("" + value).substring(0, 10);
				var v = Ext.Date.parse(value, Main.DATE_FORMAT);
				if (v instanceof Date) {
					res = v;
				} else {
					v = Ext.Date.parse(value, Main.MODEL_DATE_FORMAT.substring(0,5));
					if (v instanceof Date) {
						res = v;
					} else {
						v = Ext.Date.parse(value, "Y-m-d");
						if (v instanceof Date) {
							res = v;
						}
					}
				}
			}
		}
		return res;
	},
    
    _formatDate_: function(value) {
        // format the value to be in Main.DATE_FORMAT
        var newValue = value;
        var v = this._getDateFromString_(value);
        if( v ){
        	newValue = Ext.util.Format.date(v,Main.DATE_FORMAT);
        }
        return newValue;
    },

    _filterOperators_: function(combo) { // additional parameters: recs, eOpts

        // Filter the operations store based on the data type of the selected field 

        var operationsStore = combo.getStore();
        operationsStore.clearFilter();

        var selectedModel = this._filterGrid_.getSelectionModel().getSelection()[0];
        var selectedFieldName = selectedModel.get('fieldName');

        if (!Ext.isEmpty(selectedFieldName)) {
        	var rec = this._fieldStore_.findRecord('name', selectedFieldName);
        	if( rec ){
	            var selectedFieldType = rec.data.type;
	
	            if (selectedFieldType === "bool" || selectedFieldType === "boolean") {
	                operationsStore.filter('field1', '=');
	            }
        	}
        }
    },
    
    _isInArray_ : function(value, array) {
    	  return array.indexOf(value) > -1;
    },

    /**
     * Build the filter-grid columns.
     */
    _buildFilterGridColumns_: function() {
        var _data = [];
        var dc = this._grid_._controller_;
        var _trl = dc._trl_;
        var _modelFields = dc.filterModel.getFields();
        var cf = dc.getDcContextFields();
        
        // Dan: get the column dataIndexes from the current grid
        var gridDataIndexes = [];
        var gridColumns = this._grid_.headerCt.getGridColumns();
        for (var z = 0; z < gridColumns.length; z++) { 
        	gridDataIndexes.push(gridColumns[z].dataIndex);
        }

        for (var i = 0; i < dc.filter.fields.length; i++) {
            var item = dc.filter.fields[i];
            if (!item.name.endsWith("_From") && !item.name.endsWith("_To")) {
            	
                var _n = item.name;
                var _t = Main.translateModelField(_trl, _n);
                var _mf = this._getModelField_(_modelFields, _n);
                
                // Dan: If the columns from the grid match field names from the DS, display the fields in the advanced filter grid field list
                
                if (this._isInArray_(item.name, gridDataIndexes) === true) {
                	
                	// Dan: Exclude the ID's from the list of items and the context fields
                	if (_t && !_t.endsWith("(ID)") && _mf && _mf.noFilter !== true && cf.indexOf(_n)<0) { 
	                    _data[_data.length] = {
	                        name: _n,
	                        title: _t,
	                        type: item.type
	                    }
					}
                }
            }
        }

        this._fieldStore_ = Ext.create("Ext.data.Store", {
            fields: ["name", "title", "type"],
            data: _data
        });

        return [{
            text: Main.translate("dcFilter", "title"),
            dataIndex: "title",
            width: 150,
            editor: {
                xtype: "combo",
                selectOnFocus: true,
                forceSelection: true,
                allowBlank: false,
                typeAhead: true,
                queryMode: "local",
                valueField: "title",
                displayField: "title",
                store: this._fieldStore_,
                listeners: {
                    blur: {
                        scope: this,
                        fn: function(combo) {
                        	var r = combo.getSelectedRecord();
                            if (Ext.isArray(r)) {
                                r = r[0];
                            }
                            if (!Ext.isEmpty(r)) {
                                var name = r.data.name;
                                var selectedModel = this._filterGrid_.getSelectionModel().getSelection()[0];
                                selectedModel.set('fieldName', name);
                                this._setupCellEditor_(["value1", "value2"]);
                            }
                        }
                    }
                }
            }
        }, {
            text: Main.translate("dcFilter", "field"),
            dataIndex: "fieldName",
            hidden: true
        }, {
            text: Main.translate("dcFilter", "op"),
            dataIndex: "operation",
            value: "=",
            editor: {
                xtype: "combo",
                selectOnFocus: true,
                forceSelection: true,
                typeAhead: true,
                queryMode: "local",
                store: ["=", "<>", "<", "<=", ">", ">=", "like",
                    "not like", "in", "not in", "between"
                ],
                listeners: {
                    focus: {
                        scope: this,
                        fn: this._filterOperators_
                    }
                }
            }
        }, {
            text: Main.translate("dcFilter", "val1"),
            dataIndex: "value1" ,
            editor: {
            	xtype: "textfield",
                selectOnFocus: true,
                listeners: {
                    change: {
                        scope: this,
                        fn: function(field) {
                            // Reset previous renderers
                            field.column.renderer = null;
                        }
                    }
                }
            }
        }, {
            text: Main.translate("dcFilter", "val2"),
            dataIndex: "value2" ,
            editor: {
            	xtype: "textfield",
                selectOnFocus: true,
                listeners: {
                    change: {
                        scope: this,
                        fn: function(field) {
                            // Reset previous renderers
                            field.column.renderer = null;
                        }
                    }
                }
            }
        }];
    },

    /**
     * Return a field object from the model instance based on the name.
     * If not found null is returned.
     */
    _getModelField_: function(model, fieldName) {
        var i = 0;
        while (i < model.length) {
            if (model[i].name === fieldName) {
                return model[i];
            }
            i++;
        }
        return null;
    }
});
