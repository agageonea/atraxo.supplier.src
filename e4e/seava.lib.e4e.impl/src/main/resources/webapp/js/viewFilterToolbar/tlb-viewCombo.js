Ext.define("View.Combo", {
    extend: "Ext.form.ComboBox",

    labelSeparator: Main.viewConfig.FORM_LABEL_SEPARATOR,
    
    // =================== Custom properties ===================

    _label_: null,
    _fieldWidth_: null,
    _anchor_: null,
    _viewStateAlias_: null,
    _viewStateDs_: null,
    _originalViewState_: null,
    _ctrl_: null,
    _selectedId_: null,
    _grid_: null,
    _gridName_: null,
    _selectedIndex_: null,
    _pageSize_: null,
    _clearFilterValue_: null,
    _advancedFilterId_: null,
    _filter_: null,
    _viewWindowId_ : null,

    // =================== End custom properties ===================

    onBeforeLoadStates: function(store) { // additional parameters: op, eOpts

        if (Ext.isEmpty(this._grid_)) {
            this._grid_ = Ext.ComponentQuery.query('[name=' + this._gridName_ + ']')[0];
        }

        var p = {};
        p.data = Ext.encode({
            cmp: this._grid_.stateId
        });

        store.proxy.extraParams.data = p.data;
    },

    _doApplyDefaultViewFilter_: function(standardFilterValue, advancedFilterValue, filterName, filterId) {

        var ctrl = this._ctrl_;
        var resetButton = Ext.getCmp(this._grid_._resetButtonId_);
        resetButton._currentAppliedFilter_ = null;

        // Dan - get the filter combo

        var filterCombo = Ext.getCmp(this._grid_._filterComboId_);
        // aici
        
        ctrl.advancedFilter = null;
        ctrl.setFilter(Ext.create(ctrl.filterModel, Ext.decode(standardFilterValue)));
        ctrl.advancedFilter = Ext.decode(advancedFilterValue);

        // Dan - Set the filter combo value
        // Dan: E5

        filterCombo.getStore().load({
        	customParams: {
        		filterName: filterName,
        		filterId: filterId
        	},
            callback: function(records, operation) { // additional parameters: success
            	if( operation && operation.customParams ){
            		filterCombo.setValue(operation.customParams.filterName);
            		filterCombo._selectedId_ = operation.customParams.filterId;
            	}
            }
        }, this)
    },

    _clearFilter_: function() {
    	
        var ctrl = this._ctrl_;
        var filterCombo = Ext.ComponentQuery.query('[id=' + this._grid_._filterComboId_ + ']')[0];
        
        ctrl.doClearAllFilters();

        filterCombo.setRawValue("");
        filterCombo.setValue("");
        filterCombo._selectedId_ = null;
    },

    _applyValues_: function(obj) {
    	
        var grid = this._grid_;
        var dc = grid._controller_;
        var _cmpCurrenciesComboMain_ = Ext.getCmp(grid._currencyComboId_);
        var _cmpUnitsComboMain_ = Ext.getCmp(grid._unitComboId_);

        if (_cmpCurrenciesComboMain_) {
            _cmpCurrenciesComboMain_.setValue(obj.currencyCode);
        }
        if (_cmpUnitsComboMain_) {
            _cmpUnitsComboMain_.setValue(obj.unitCode);
        }

        if (grid._currencyParamName_) {
            dc.params.set(grid._currencyParamName_, obj.currencyCode);
        }
        if (grid._unitParamName_) {
            dc.params.set(grid._unitParamName_, obj.unitCode);
        }
    },

    _doApply_: function(record) { // additional parameters: gridName, message

        if (!Ext.isEmpty(record)) {
            var _value = Ext.JSON.decode(record.data.value);
            var _standardFilterValue = record.data.standardFilterValue;
            var _advancedFilterValue = record.data.advancedFilterValue;
            var _filterName = record.data.advancedFilterName;
            var _filterId = record.data.advancedFilterId;
            var _totals = record.data.totals;
            var _currencyCode = record.data.currencyCode;
            var _unitCode = record.data.unitCode;
            var _grid_ = this._grid_;
            var _toolbarTotalsCombo = Ext.getCmp(_grid_._totalsComboId_);
            var _filterCombo = Ext.getCmp(this._grid_._filterComboId_);

            this._applyValues_({
                totals: _totals,
                currencyCode: _currencyCode,
                unitCode: _unitCode
            });

            if (_totals) {
                _grid_._showSummary_(true);
                _toolbarTotalsCombo.setValue(1);
            } else {
                _grid_._showSummary_(false);
                if (_toolbarTotalsCombo) {
                    _toolbarTotalsCombo.setValue(0);
                }
            }

            this._pageSize_ = record.data.pageSize;
            _grid_._setPageSize_(this._pageSize_);

            if (!Ext.isEmpty(_standardFilterValue) && !Ext.isEmpty(_advancedFilterValue)) {
                this._doApplyDefaultViewFilter_(_standardFilterValue, _advancedFilterValue, _filterName, _filterId);
                _filterCombo._setContextFilter_(true);
            } else {
            	
                this._clearFilter_();
                _filterCombo._setContextFilter_(true);
            }
            _grid_._applyViewState_(_value);
            _grid_._controller_.delayedQuery();
        }
    },

    _sync_: function(record) {
        this._grid_._setPageSize_(record.data.pageSize);
    },

    initComponent: function() {
        var cfg = {
            name: "viewCombo",
            xtype: "combo",
            anchor: this._anchor_,
            width: this._fieldWidth_,
            labelWidth: this._labelWidth_,
            fieldLabel: this._label_,
            selectOnFocus: true,
            forceSelection: false, 
            autoSelect: false,
            allowBlank: true,
            allQuery: "%",
            triggerAction: "all",
            minChars: 0,
            pageSize: 0,
            displayField: "name",
            queryMode: "remote",
            remoteSort: true,
            remoteFilter: true,
            typeAhead: true,
            queryDelay: 100,
            listeners: {
                select: {
                    scope: this,
                    fn: function(combo, recs) {
                        var r = recs;
                        if (Ext.isArray(recs)) {
                            r = recs[0];
                        }
                        if (!Ext.isEmpty(r)) {
                            this._doApply_(r, this._gridName_, Main.translate("viewAndFilters", "applyingFilter__lbl"));
                            this._selectedId_ = r.data.id;
                            this._selectedIndex_ = this.store.indexOf(r);
                        }
                        
                        this.store.clearFilter();
                        var toolbar = Ext.getCmp(this._grid_._viewFilterToolbarId_);
                        var assignBtn = Ext.getCmp(toolbar._assignBtnId_);
                        assignBtn.setDisabled(false);
                    }
                },
                afterRender: {
                    scope: this,
                    fn: function() {

                        if (Ext.isEmpty(this._grid_)) {
                            this._grid_ = Ext.ComponentQuery.query('[name=' + this._gridName_ + ']')[0];
                        }

                        this._originalViewState_ = this._grid_._getViewState_();
                    }
                },
                render: function(c) {
                    return new Ext.ToolTip({
                        target: c.getEl(),
                        html: Main.translate("viewAndFilters", "selectView__tlp"),
                        trackMouse:true,
                        anchorOffset: 5
                    });
                },
                focus: {
                	scope: this,
                    fn: function() { // parameter: combo
                        this.store.clearFilter();
                    }
                },
                change: function() { // parameters: combo, newValue, oldValue
                    var store = this.store;
                    var rawValue = this.getRawValue();
                    store.filter("name",rawValue);
                }
            },
            tpl: Ext.create('Ext.XTemplate',
                	'<tpl for=".">',
                        '<div class="x-boundlist-item">{[this.returnName(values.name, values.id)]}</div>',
                    '</tpl>',
                    {
                        scope: this,
                        returnName: function(val, id) {
                        	var result;
                            if (this.scope._grid_._defaultViewId_ === id) {
                            	result = "<b>"+val+"</b>";
                            }
                            else {
                            	result = val;
                            }
                            return result;
                        }
                    }
            ),
            listConfig: {
                width: 180
            },
            store: Ext.create('Ext.data.Store', {
                model: this._viewStateDs_,
                autoSync: true,
                listeners: {
                    beforeLoad: {
                        scope: this,
                        fn: this.onBeforeLoadStates
                    }
                },
                proxy: {
                    type: 'ajax',
                    api: Main.dsAPI(this._viewStateAlias_, "json"),
                    actionMethods: {
                        create: 'POST',
                        read: 'POST',
                        update: 'POST',
                        destroy: 'POST'
                    },
                    reader: {
                        type: 'json',
                        rootProperty: 'data',
                        idProperty: 'id',
                        totalProperty: 'totalCount'
                    },
                    writer: {
                        type: 'json',
                        encode: true,
                        rootProperty: "data",
                        allowSingle: false,
                        writeAllFields: true,
                        clientIdProperty: "__clientRecordId__" // Tibi: E5
                    },
                    listeners: {
                        "exception": {
                            fn: this.proxyException,
                            scope: this
                        }
                    },
                    startParam: Main.requestParam.START,
                    limitParam: Main.requestParam.SIZE,
                    sortParam: Main.requestParam.SORT,
                    directionParam: Main.requestParam.SENSE
                }
            })
        };

        Ext.apply(this, cfg);
        this.callParent(arguments);
    }
});
