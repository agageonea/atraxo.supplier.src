//========================================================================================
//                  GRID FILTER AND VIEW IMPLEMENTATION BEGIN
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


// define totals, units and currencies stores

function getTotalsComboStore() {
    var totalsStore = Ext.create('Ext.data.Store', {
        fields: ['total', 'value'],
        data: [{
            "total": Main.translate("viewAndFilters", "on__lbl"),
            "value": 1
        }, {
            "total": Main.translate("viewAndFilters", "off__lbl"),
            "value": 0
        }]
    });
    return totalsStore;
};

function getCurrenciesComboStore() {
    var model = "Currencies.Model";
    var alias = "fmbas_CurrenciesLov_Ds";

    var currenctiesStore = Ext.create("Ext.data.Store", {
        model: model,
        remoteSort: true,
        autoLoad: false,
        autoSync: false,
        clearOnPageLoad: true,
        pageSize: 0,
        listeners: {
            beforeload: {
                fn: function(store, op) {
                    store.proxy.extraParams.data = Ext.encode({
                        code: op._params.query + "%"
                    });
                }
            }
        },
        proxy: {
            type: 'ajax',
            api: Main.dsAPI(alias, "json"),
            model: model,
            extraParams: {
                params: {}
            },
            actionMethods: {
                create: 'POST',
                read: 'POST',
                update: 'POST',
                destroy: 'POST'
            },
            reader: {
                type: 'json',
                rootProperty: 'data',
                idProperty: 'id',
                totalProperty: 'totalCount',
                messageProperty: 'message'
            },
            listeners: {
                "exception": {
                    fn: this.proxyException,
                    scope: this
                }

            },
            startParam: Main.requestParam.START,
            limitParam: Main.requestParam.SIZE,
            sortParam: Main.requestParam.SORT,
            directionParam: Main.requestParam.SENSE
        }
    });
    return currenctiesStore;
};

function getUnitsComboStore() {
    var model = "Units.Model";
    var alias = "fmbas_MeasurmentUnitsLov_Ds";

    var unitsStore = Ext.create("Ext.data.Store", {
        model: model,
        remoteSort: true,
        autoLoad: false,
        autoSync: false,
        pageSize: 0,
        clearOnPageLoad: true,
        proxy: {
            type: 'ajax',
            api: Main.dsAPI(alias, "json"),
            model: model,
            extraParams: {
                params: {}
            },
            actionMethods: {
                create: 'POST',
                read: 'POST',
                update: 'POST',
                destroy: 'POST'
            },
            reader: {
                type: 'json',
                rootProperty: 'data',
                idProperty: 'id',
                totalProperty: 'totalCount',
                messageProperty: 'message'
            },
            listeners: {
                "exception": {
                    fn: this.proxyException,
                    scope: this
                }
            },
            startParam: Main.requestParam.START,
            limitParam: Main.requestParam.SIZE,
            sortParam: Main.requestParam.SORT,
            directionParam: Main.requestParam.SENSE
        }
    });

    return unitsStore;
};

// this toolbar will be placed above the grids and contains: view, filter and unit, currency, total combos 

Ext.define("ViewFilter.Toolbar", {

    extend: 'Ext.toolbar.Toolbar',

    _filterComboId_: null,
    _viewComboId_: null,
    _unitComboId_: null,
    _currencyComboId_: null,
    _datePickerId_: null,
    _totalsComboId_: null,
    _resetButtonId_: null,
    _filterWindowButtonId_: null,
    _filterWindow_: null,
    _ctrl_: null,
    _filterCfg_: null,
    _gridCfg_: null,
    _gridName_: null,
    _gridObj_: null,
    _viewFilterToolbarId_ : null,
    _isTotalsPanel_ : null,
    _assignBtnId_ : null,

    initComponent: function() {

        this._filterComboId_ = Ext.id();
        this._viewComboId_ = Ext.id();
        this._unitComboId_ = Ext.id();
        this._currencyComboId_ = Ext.id();
        this._datePickerId_ = Ext.id();
        this._totalsComboId_ = Ext.id();
        this._resetButtonId_ = Ext.id();
        this._filterWindowButtonId_ = Ext.id();
        this._viewFilterToolbarId_ = Ext.id();
        this._assignBtnId_ = Ext.id();

    	var items2 = this._buildFieldsFromDc_(); //items2 should be generated before items1 (it is used to calc the space before unit btns)

        var items1 = [
                     this._buildAssignButton_(),this._buildViewCombo_(), {
                         icon: Main.urlStaticCore + "/images/list.png",
                         xtype: "button",
                         tooltip: {
                             text: Main.translate("viewAndFilters", "editOrCreateView__tlp"),
                             trackMouse:true,
                             anchorOffset: 5
                         },
                         listeners: {
                             click: {
                                 scope: this,
                                 fn: function() {
                                     this._checkSuperUser_(this._viewComboId_);
                                 }
                             }
                         },
                     },{
                         glyph: "xf021@FontAwesome",
                         cls: "sone-view-filter-toolbar-btn",
                         xtype: "button",
                         hidden: true,
                         listeners: {
                         	render: {
                         		scope: this,
                         		fn: function(b) {
                         			if (!Ext.isEmpty(this._getGrid_()._noPaginator_) && this._getGrid_()._noPaginator_ === true || this._getGrid_()._controller_._infiniteScroll_) {
                         				b.setHidden(false);
                         			}
                         		}
                         	},
                         	click: {
                         		scope: this,
                         		fn: function() {
                         			var ctrl = this._getGrid_()._controller_;
                                 	ctrl.doReloadPage();
                         		}
                         	}
                         }
                     },{
                         xtype: "tbspacer",
                         width: items2.length>1 ? 30 : 100
                     }
            ].concat(this._buildToolbarCurrencyCombo_(),this._buildToolbarUnitCombo_(),
            		this._buildToolbarTotalsCombo_(),this._buildToolbarDatePicker_()),

            items3 = [
                     this._buildResetButton_(), this._buildFilterCombo_(), {
                         icon: Main.urlStaticCore + "/images/list.png",
                         id: this._filterWindowButtonId_,
                         tooltip: {
                             text: Main.translate("viewAndFilters", "editOrCreateFilter__tlp"),
                             trackMouse:true,
                             anchorOffset: 5
                         },
                         listeners: {
                             click: {
                                 scope: this,
                                 fn: function() {
                                     this._checkSuperUser_(this._filterComboId_);
                                 }
                             }
                         }
                     }
        ];
        
        var cfg = {
            cls: "view-filter-toolbar",
            id: this._viewFilterToolbarId_,
            xtype: "toolbar",
            hidden: this._getGrid_()._showViewFilterToolbar_ === false ? true : false,
            style: "padding:5px",
            items: items1.concat(items2,items3)
        };
        Ext.apply(this, cfg);
        this.callParent(arguments);
        
    },
    
    
    
    _buildAssignButton_ : function() {
    	var btn = {
    		glyph: "xf05d@FontAwesome",
            cls: "sone-view-filter-toolbar-btn",
    		id : this._assignBtnId_,
            xtype: "button",
            disabled: true,
            listeners: {
            	click: {
            		scope: this,
            		fn: function() {
            			var viewWindow = this._createViewWindow_();
            			var defaultViewCheckBox = Ext.getCmp(viewWindow._idCmpIsDefaultView_);
            			defaultViewCheckBox.setValue(true);
            			viewWindow._setViewAsDefault_();
            			viewWindow._refreshData_();
            		}
            	}
            }
    	}
    	return btn;
    },
    
    _createViewWindow_ : function() {
    	var vr = Ext.ComponentQuery.query('[id=' + this._viewComboId_ + ']')[0];
        var grid = this._getGrid_();
        var vs = vr.store;
        var vState = grid._getViewState_();
        var vm = "View.State.Model";
        var va = "fmbas_ViewStateRt_Ds";
        var da = "fmbas_DefaultViewState_Ds";
    	var w = Ext.create('Toolbar.View.Window', {
            modal: true,
            _showCurrency_: this._isCurrencyParamExist_(),
            _showUnit_: this._isUnitParamExist_(),
            _showTotal_: this._isTotalParamExist_(),
            _viewRecord_: vr,
            _viewStore_: vs,
            _frame_ : grid._controller_.getFrame(),
            _viewState_: vState,
            _gridName_: grid,
            _viewStateModel_: vm,
            _viewStateAlias_: va,
            _defaultViewStateAlias_ : da,
            _selectedId_: this._selectedId_,
            _defaultPageSize_: 0,
            _idCmpViewComboMain_: this._viewComboId_,
            _idCmpFilterComboMain_: this._filterComboId_,
            _idCmpTotalsComboMain_: this._totalsComboId_,
            _idCmpUnitsComboMain_: this._unitComboId_,
            _idCmpCurrenciesComboMain_: this._currencyComboId_,
            _toolBarId_ : this._viewFilterToolbarId_
        });
    	return w;
    },

    _openViewWindow_: function() {
    	var ctx = this;
        this._createViewWindow_().show(undefined, function() {
        	var viewCombo = Ext.getCmp(ctx._viewComboId_);
        	viewCombo._viewWindowId_ = this.getId();
        });
    },

    _openFilterWindow_: function() {

        var fr = Ext.ComponentQuery.query('[id=' + this._filterComboId_ + ']')[0];
        var grid = this._getGrid_();
        var stdFilterCfg = this._filterCfg_;

        Ext.create('Toolbar.Filter.Window', {
            modal: true,
            _filterCombo_: fr,
            _grid_: grid,
            _standardFilterCfg_: stdFilterCfg,
            _advancedFilterAlias_: "fmbas_AdvancedFilterRt_Ds",
            _advancedFilterDs_: "Advanced.Filter.Model",
            _idCmpViewComboMain_: this._viewComboId_,
            _idCmpFilterComboMain_: this._filterComboId_
        }).show();
    },

    _checkSuperUser_: function(comboId) {
        var combo = Ext.ComponentQuery.query('[id=' + comboId + ']')[0];
        var store = combo.getStore();
        var record = store.getById(combo._selectedId_);
        var currentUser = getApplication().getSession().getUser().code;

        var standard = (record ? record.get("isStandard") : false);

        if (comboId === this._filterComboId_) {
            if (standard === false) {
                this._openFilterWindow_();
            } else {
                if (currentUser !== __SUPERUSER__) {
                    Main.error(Main.translate("viewAndFilters", "noPriviledges__lbl"));
                } else {
                    this._openFilterWindow_();
                }
            }
        } else if (comboId === this._viewComboId_) {
            if (standard === false) {
                this._openViewWindow_();
            } else {
                if (currentUser !== __SUPERUSER__) {
                    Main.error(Main.translate("viewAndFilters", "noPriviledges__lbl"));
                } else {
                    this._openViewWindow_();
                }
            }
        }
    },

    _buildResetButton_: function() {

        var button = Ext.create("Reset.Button", {
            id: this._resetButtonId_,
            _grid_: this,
            _filterCombo_: this._filterCombo_,
            _viewCombo_: this._viewCombo_,
            glyph: "xf0b0@FontAwesome",
            cls: "sone-view-filter-toolbar-btn"
        });
        return button;
    },

    _buildFilterCombo_: function() {

        var alias = "fmbas_AdvancedFilterRt_Ds";
        var ds = "Advanced.Filter.Model";

        var combo = Ext.create("Filter.Combo", {
            id: this._filterComboId_,
            _fieldWidth_: 240,
            _advancedFilterAlias_: alias,
            _advancedFilterDs_: ds,
            _ctrl_: this._ctrl_,
            _gridName_: this._gridName_
        });

        var comboCfg = {
            labelWidth: 60
        };

        Ext.apply(combo, comboCfg);
        return combo;
    },

    _buildViewCombo_: function() {

        var alias = "fmbas_ViewStateRt_Ds";
        var ds = "View.State.Model";

        var combo = Ext.create("View.Combo", {
            id: this._viewComboId_,
            _fieldWidth_: 210,
            _viewStateAlias_: alias,
            _viewStateDs_: ds,
            _ctrl_: this._ctrl_,
            _gridName_: this._gridName_
        });
        var comboCfg = {
            labelWidth: 30
        };

        Ext.apply(combo, comboCfg);
        return combo;
    },
    
    _hasView_ : function() {
    	var result = true;
    	var viewCombo = Ext.getCmp(this._viewComboId_);
    	var selectedId = viewCombo._selectedId_;
    	if (Ext.isEmpty(selectedId)) {
    		result = false;
    	}
    	return result;
    },
    
    _buildToolbarCurrencyCombo_: function() {
    	var cfg = [];
        if (this._isCurrencyParamExist_()) {
	        var label = Main.translate("viewAndFilters", "currUnit__lbl");
	        var labelWidth = 85;
	        if (!this._isUnitParamExist_()) {
	            label = Main.translate("viewAndFilters", "curr__lbl");
	            labelWidth = 25;
	        }
	        var store = getCurrenciesComboStore();
	        cfg.push({
	            xtype: "combo",
	            id: this._currencyComboId_,
	            fieldLabel: label,
	            labelSeparator: Main.viewConfig.FORM_LABEL_SEPARATOR,
	            labelWidth: labelWidth,
	            labelAlign: "right",
	            labelStyle: 'white-space: nowrap;',
	            width: 80 + labelWidth,
	            selectOnFocus: true,
	            forceSelection: false,
	            autoSelect: false,
	            allowBlank: true,
	            allQuery: "%",
	            autoLoad: true,
	            triggerAction: "all",
	            minChars: 0,
	            displayField: "code",
	            queryMode: "remote",
	            remoteSort: true,
	            remoteFilter: true,
	            queryDelay: 100,
	            pageSize: store.pageSize,
	            //aici
	            value: this._getCurrencyValue_(),
	            listConfig: {
	                getInnerTpl: function() {
	                    return "<span>{code}, {name}</span>";
	                },
	                minWidth: 280
	            },
	            store: store,
	            listeners: {
	            	afterrender: {
	            		scope: this,
	            		fn: function(cmp) { 
	            			if (this._hasView_() === false) {
	            				this._currenciesChanged_(cmp.getValue());
	            			}
	            		}
	            	},
	                select: {
	                    scope: this,
	                    fn: function(cb) { // additional parameters: recs, eOpts
	                        var val = cb.getValue();
	                        this._currenciesChanged_(val);
	                    }
	                },
	                blur: {
	                    scope: this,
	                    fn: function(cb) { // additional parameters: recs, eOpts
	                        var val = cb.getValue();
	                        this._currenciesChanged_(val);
	                    }
	                }
	            }
	        });
        }
        return cfg;
    },

    _buildToolbarUnitCombo_: function() {
    	var cfg = [];
        if (this._isUnitParamExist_()) {
	        var label = "";
	        var labelWidth = 0;
	        if (!this._isCurrencyParamExist_()) {
	            label = Main.translate("viewAndFilters", "unit__lbl");
	            labelWidth = 25;
	        }
	        var store = getUnitsComboStore();
	        cfg.push({
	            xtype: "combo",
	            id: this._unitComboId_,
	            labelWidth: labelWidth,
	            width: 80 + labelWidth,
	            fieldLabel: label,
	            labelSeparator: Main.viewConfig.FORM_LABEL_SEPARATOR,
	            selectOnFocus: true,
	            forceSelection: false,
	            autoSelect: false,
	            allowBlank: true,
	            allQuery: "%",
	            triggerAction: "all",
	            minChars: 0,
	            displayField: "code",
	            queryMode: "remote",
	            remoteSort: true,
	            remoteFilter: true,
	            queryDelay: 100,
	            pageSize: store.pageSize,
	            listConfig: {
	                getInnerTpl: function() {
	                    return "<span>{code}, {name}</span>";
	                },
	                minWidth: 280
	            },
	            store: store,
	            value: this._getUnitValue_(),
	            listeners: {
	            	afterrender: {
	            		scope: this,
	            		fn: function(cmp) { 
	            			if (this._hasView_() === false) {
	            				this._unitsChanged_(cmp.getValue());
	            			}
	            		}
	            	},
	                select: {
	                    scope: this,
	                    fn: function(cb) { // additional parameters: evnt, eOpts
	                        var val = cb.getValue();
	                        this._unitsChanged_(val);
	                    }
	                },
	                blur: {
	                    scope: this,
	                    fn: function(cb) { // additional parameters: evnt, eOpts
	                        var val = cb.getValue();
	                        this._unitsChanged_(val);
	                    }
	                },
	                change: function() { // parameters: combo, newValue, oldValue
	                    var store = this.store;
	                    var rawValue = this.getRawValue();

	                    store.filter("code",rawValue);
	                },
	                expand: function() { // parameters: combo
	                    var store = this.store;
	                    store.clearFilter();
	                }
	            }
	        });
        }
        if(this._isCurrencyParamExist_() || this._isUnitParamExist_()){
        	cfg.push({xtype: "tbspacer", width: 15});
        }
        return cfg;
    },
    
    _buildToolbarDatePicker_: function(){
    	var cfg = [];
        if (this._isDateParamExist_()) {
        	cfg.push({
				xtype: 'datefield',
	            id: this._datePickerId_,
				fieldLabel: Main.translate("viewAndFilters", "date__lbl"),
				labelWidth: 30,
				labelSeparator: Main.viewConfig.FORM_LABEL_SEPARATOR,
				allowBlank: true,
				width: 140,
				altFormats: Main.ALT_FORMATS,
				format: Main[Masks.DATE],
	            value: this._getDateValue_(),
	            listeners: {
	                blur: {
	                    scope: this,
	                    fn: function(field) { // additional parameters: event, eOpts
	                    	var value = field.getValue();
	                        this._dateChanged_(value);
	                    }
	                }
	            }
	        });
        	cfg.push({
                xtype: "tbspacer",
                width: 15
            });
        }
        return cfg;
    },
    
    _buildFieldsFromDc_: function(){
    	var grid = this._getGrid_(),
    		cfg = [];

		var _add_fn = function(item) {
			if( item._onViewToolbar_ === true ){
				cfg.push(item);
			}
		}
		grid._elems_.each(_add_fn, this);

		if( cfg.length>0 ){
			grid.setViewModel(Ext.create(Ext.app.ViewModel, {}));
			var ctrl = grid._controller_;
	        grid.viewModel.setData({
	            d : ctrl.record,
	            p : ctrl.params
	        });
			this.mon(ctrl, "recordChange", function(evnt) {
	    		var newRecord = evnt.newRecord;
	    		this.viewModel.setData({
	    			d : newRecord,
	    			p : this._controller_.params
	    		});
	    	}, grid);

			var cfg2 = Ext.create('Ext.form.Panel',{
				layout: 'hbox',
				border: true,
				items: cfg
			});
			cfg = [cfg2];
		}
		
		return cfg.concat([{ xtype: 'tbfill' }]);
    },

    _buildToolbarTotalsCombo_: function() {
        var cfg = [];
	        cfg.push({
	            id: this._totalsComboId_,
	            xtype: 'combobox',
	            pageSize: 0,
	            fieldLabel: Main.translate("viewAndFilters", "totals__lbl"),
	            labelSeparator: Main.viewConfig.FORM_LABEL_SEPARATOR,
	            hidden: Ext.isEmpty(this._gridCfg_._summaryDefined_) ? true : false,
	            width: 115,
	            value: this._hasView_() === false ? 1 : 0,
	            labelWidth: 35,
	            name: 'totals',
	            store: getTotalsComboStore(),
	            queryMode: 'local',
	            valueField: 'value',
	            displayField: 'total',
	            tpl: Ext.create('Ext.XTemplate', '<tpl for=".">', '<div class="x-boundlist-item">{total}</div>', '</tpl>'),
	            listeners: {
	                select: {
	                    scope: this,
	                    fn: function(cb) { // additional parameters: evnt, eOpts
	                        var val = cb.getValue();                    
	                        if (cb._onFocusVal_ !== val && !(Ext.isEmpty(val) && Ext.isEmpty(cb._onFocusVal_))) {
	                            this._enableSummary_(val === 1);
	                        }
	                    }
	                }
	            }
	        });
	        cfg.push({ xtype: "tbspacer", width: 15 });
        return cfg;
    },


    _getCurrencyValue_ : function() {
		var grid = this._getGrid_();
		var v = grid._controller_.getParamValue(grid._currencyParamName_);
		if (typeof(_SYSTEMPARAMETERS_) !== "undefined" && typeof(_SYSTEMPARAMETERS_) !== "null" && Ext.isEmpty(v) ){
			v = _SYSTEMPARAMETERS_.syscrncy;
		}
		return v;
    },
    _getUnitValue_ : function() {
		var grid = this._getGrid_();
		var v = grid._controller_.getParamValue(grid._unitParamName_);
		if (typeof(_SYSTEMPARAMETERS_) !== "undefined" && typeof(_SYSTEMPARAMETERS_) !== "null" && Ext.isEmpty(v) ){
			v = _SYSTEMPARAMETERS_.sysvol;
		}
		return v;
    },
    _getDateValue_ : function() {
		var grid = this._getGrid_();
		var v = grid._controller_.getParamValue(grid._dateParamName_);
		return v;
    },
    
    _enableSummary_: function(enable) {
        var grid = this._getGrid_();
        grid._showSummary_(enable);
    },
    _currenciesChanged_: function(value) {
        var grid = this._getGrid_();
        var cmpToolbarFilterCombo = Ext.getCmp(this._filterComboId_);
        
        if (grid._controller_.getParamValue(grid._currencyParamName_) !== value) {
            grid._controller_.setParamValue(grid._currencyParamName_, value, null, null, true);
            cmpToolbarFilterCombo._setContextFilter_(); //SONE-2758
            grid._controller_.delayedQuery();
        }
    },
    _unitsChanged_: function(value) {
        var grid = this._getGrid_();
        var cmpToolbarFilterCombo = Ext.getCmp(this._filterComboId_);
        
        if (grid._controller_.getParamValue(grid._unitParamName_) !== value) {
            grid._controller_.setParamValue(grid._unitParamName_, value, null, null, true);
            cmpToolbarFilterCombo._setContextFilter_(); //SONE-2758
            grid._controller_.delayedQuery();
        }
    },
    _dateChanged_: function(value) {
        var grid = this._getGrid_();
        var oldValue = grid._controller_.getParamValue(grid._dateParamName_),
        	different;
        if( Ext.isDate(value) && Ext.isDate(oldValue) ){
        	different = (value.getTime() - oldValue.getTime());
        } else {
        	different = (String(value) !== String(oldValue));
        }
        if (different) {
            grid._controller_.setParamValue(grid._dateParamName_, value, null, null, true);
            grid._controller_.delayedQuery();
        }
    },
    
    _getGrid_: function() {
    	if( !this._gridObj_ ){
    		this._gridObj_ = Ext.ComponentQuery.query('[name=' + this._gridName_ + ']')[0];
    	}
        return this._gridObj_;
    },

    _isCurrencyParamExist_: function() {
    	try{
    		return !!(this._getGrid_()._currencyParamName_);
    	}catch(e){
    		return false;
    	}
    },
    _isUnitParamExist_: function() {
    	try{
    		return !!(this._getGrid_()._unitParamName_);
    	}catch(e){
    		return false;
    	}
    },
    _isDateParamExist_: function() {
    	try{
    		return !!(this._getGrid_()._dateParamName_);
    	}catch(e){
    		return false;
    	}
    },
    _isTotalParamExist_: function() { 
    	try{
    		return !!(this._getGrid_()._summaryDefined_);
    	}catch(e){
    		return false;
    	}
    }
});

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//                  GRID FILTER AND VIEW IMPLEMENTATION END
//========================================================================================
