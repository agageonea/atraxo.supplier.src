Ext.define("Filter.Combo", {

    extend: 'Ext.form.ComboBox',

    labelSeparator: Main.viewConfig.FORM_LABEL_SEPARATOR,
  
    // =================== Custom properties ===================

    _label_: null,
    _fieldWidth_: null,
    _anchor_: null,
    _advancedFilterAlias_: null,
    _advancedFilterDs_: null,
    _ctrl_: null,
    _record_: null,
    _selectedId_: null,
    _selectedIndex_: null,
    _grid_: null,
    _gridName_: null,
    _doNotClearValue_: null,
    _previousValue_: null,

    // =================== End custom properties ===================

    _doApplyFilter_: function(standardFilterValue, advancedFilterValue) {

        var ctrl = this._ctrl_;
        this._doNotClearValue_ = true;
        ctrl.advancedFilter = null;

        var resetButton = Ext.getCmp(this._grid_._resetButtonId_);
        resetButton._currentAppliedFilter_ = null;

        ctrl.setFilter(Ext.create(ctrl.filterModel, Ext.decode(standardFilterValue)));
        ctrl.noReset = false;
        ctrl.advancedFilter = Ext.decode(advancedFilterValue);

        this._setContextFilter_();
        ctrl.delayedQuery();

    },

    _isCtrlInSomeContext_: function() {

        var isInSomeContext = false;
        var ctrl = this._ctrl_;

        if (!Ext.isEmpty(ctrl.dcContext)) {
            isInSomeContext = true;
        }

        return isInSomeContext;
    },
    
    // Function to add the dc context parameter values to the standard filter if the DC is in context of another DC

    _setContextFilter_: function(silent) {

        if (this._isCtrlInSomeContext_() === true) {

            var ctrl = this._ctrl_;
            var currentFilter = ctrl.filter.data;
            var ctxData = ctrl.dcContext.ctxData;
            var contextFieldName;
            var contextFieldvalue;

            if (silent === true) {
                ctrl.noReset = true;
            }
            
            for (var i = 0; i < ctxData.length; i++) {
	        	
	        	contextFieldName = ctxData[i].name;
	        	contextFieldvalue = ctxData[i].value;
	        	
	        	for (var key in currentFilter) {
	                if (key === contextFieldName) {
	                    currentFilter[key] = contextFieldvalue;
	                }
	            }
	        }
        }
    },

    onBeforeLoadStates: function(store) { // additional parameters: op, eOpts

        if (Ext.isEmpty(this._grid_)) {
            this._grid_ = Ext.ComponentQuery.query('[name=' + this._gridName_ + ']')[0];
        }

        var p = {};

        p.data = Ext.encode({
            cmp: this._grid_.stateId
        });

        store.proxy.extraParams.data = p.data;
    },

    initComponent: function() {
   	
        var cfg = {
        	customLogic: function() { return true; },
            xtype: 'combo',
            name: "filterCombo",
            anchor: this._anchor_,
            width: this._fieldWidth_,
            labelWidth: this._labelWidth_,
            fieldLabel: this._label_,
            
            triggerAction: "all",
            minChars: 0,
            pageSize: 0,
            displayField: "name",
            valueField: "name",
            queryMode: "remote",
            queryCaching : false, // for auto-complete behavior
            queryDelay: 100,
            
            listeners: {
                select: {
                    scope: this,
                    fn: function(combo, recs) {
                        var r = recs;
                        if (Ext.isArray(recs)) {
                            r = recs[0];
                        }
                        if (!Ext.isEmpty(r)) {
                            this._doApplyFilter_(r.data.standardFilterValue, r.data.advancedFilterValue);
                            combo._selectedId_ = r.data.id;
                            this._selectedIndex_ = this.store.indexOf(r);
                        }
                        this.store.clearFilter();
                    }
                },
                focus: {
                	scope: this,
                    fn: function() { // parameter: combo
                        this.store.clearFilter();
                    }
                },
                render: function(c) {
                    return new Ext.ToolTip({
                        target: c.getEl(),
                        html: Main.translate("viewAndFilters", "selectFilter__tlp"),
                        trackMouse:true,
                        anchorOffset: 5
                    });
                },
                change: function(combo, newValue, oldValue) {
                    this._previousValue_ = oldValue;
                    var store = this.store;
                    var rawValue = this.getRawValue();
                    store.filter("name",rawValue);
                }
            },
            
            // Dan: nu stergeti comment-ul, va rog
            // Nu e necesar acum insa e bine de stiut
            
            tpl: Ext.create('Ext.XTemplate',
            	'<tpl for=".">',
                    '<div class="x-boundlist-item">{[this.returnName(values.name, values.defaultFilter)]}</div>',
                '</tpl>',
                {
                    scope: this,
                    returnName: function(val, isDefault) {
                    	var result;
                        if (isDefault === true) {
                        	result = "<b>"+val+"</b>";
                        } else {
                        	result = val;
                        }
                        return result;
                    }
                }
            ),
            listConfig: {
                width: 180,
                getInnerTpl: function() {
                	return "{name}";
                }
            },
            store: Ext.create('Ext.data.Store', {
                model: this._advancedFilterDs_,
                autoSync: true,
                listeners: {
                    beforeLoad: {
                        scope: this,
                        fn: this.onBeforeLoadStates
                    }
                },
                proxy: {
                    type: 'ajax',
                    api: Main.dsAPI(this._advancedFilterAlias_, "json"),
                    actionMethods: {
                        create: 'POST',
                        read: 'POST',
                        update: 'POST',
                        destroy: 'POST'
                    },
                    reader: {
                        type: 'json',
                        rootProperty: 'data',
                        idProperty: 'id',
                        totalProperty: 'totalCount'
                    },
                    writer: {
                        type: 'json',
                        encode: true,
                        rootProperty: "data",
                        allowSingle: false,
                        writeAllFields: true,
                        clientIdProperty: "__clientRecordId__" // Tibi: E5
                    },
                    listeners: {
                        "exception": {
                            fn: this.proxyException,
                            scope: this
                        }
                    },
                    startParam: Main.requestParam.START,
                    limitParam: Main.requestParam.SIZE,
                    sortParam: Main.requestParam.SORT,
                    directionParam: Main.requestParam.SENSE
                }

            })
        };

        Ext.apply(this, cfg);
        this.callParent(arguments);
    }
});
