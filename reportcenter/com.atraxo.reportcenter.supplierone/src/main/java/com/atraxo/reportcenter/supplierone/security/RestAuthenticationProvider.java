package com.atraxo.reportcenter.supplierone.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class RestAuthenticationProvider implements AuthenticationProvider {

    private String validUser;

    private String validPass;

    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
	RestToken restToken = (RestToken) authentication;
	String key = restToken.getKey();
	String credentials = restToken.getCredentials();
	if (!key.equals(validUser) || !credentials.equals(validPass)) {
	    throw new BadCredentialsException("Invalid username and	/or password");
	}
	return getAuthenticatedUser(key, credentials);
    }

    private Authentication getAuthenticatedUser(String key, String credentials) {
	List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
	authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
	return new RestToken(key, credentials, authorities);
    }

    /*
     * Determines if this class can support the token provided by the filter.
     */
    public boolean supports(Class<?> authentication) {
	return RestToken.class.equals(authentication);
    }

    public String getValidUser() {
        return validUser;
    }

    public void setValidUser(String validUser) {
        this.validUser = validUser;
    }

    public String getValidPass() {
        return validPass;
    }

    public void setValidPass(String validPass) {
        this.validPass = validPass;
    }

}