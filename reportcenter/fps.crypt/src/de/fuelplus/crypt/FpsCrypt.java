package de.fuelplus.crypt;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.crypto.SecretKey;

import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;

public class FpsCrypt {

	private static SecretKey key = null;
	private static FpEncrypter des = null;
	private static String algorithm = "AES"; // Blowfish, AES, DES, DESede
//	private static String KEY_PATH = "/de/fuelplus/crypt/";
	private static String KEY_FILE = "crypt.key";

	public static String encryptToken(String token) {
		try {
			init();
		} catch (FpsCryptException e) {
			e.printStackTrace();
			return "";
		}
		String result = des.encrypt(token);
		return result;
	}

	public static String decryptToken(String token) {
		try {
			init();
		} catch (FpsCryptException e) {
			e.printStackTrace();
			return "";
		}
		String result = des.decrypt(token);
		return result;
	}

	private static void init() throws FpsCryptException {
		if (key == null) {
			try {
				key = getKeyFromFile();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				throw new FpsCryptException("Encrypt/Decrypt error", e);
			} catch (IOException e) {
				e.printStackTrace();
				throw new FpsCryptException("Encrypt/Decrypt error", e);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				throw new FpsCryptException("Encrypt/Decrypt error", e);
			}
		}

		if (des == null) {
			des = new FpEncrypter(key, algorithm);
		}
	}

	@SuppressWarnings("unused")
	private static void saveKeyToFile(SecretKey key, String path)
			throws FileNotFoundException, IOException {
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(
				path + KEY_FILE));
		oos.writeObject(key);
		oos.close();
	}

	private static SecretKey getKeyFromFile()
			throws FileNotFoundException, IOException, ClassNotFoundException {
		SecretKey akey = null;

		ObjectInputStream ois = new ObjectInputStream(FpsCrypt.class.getResourceAsStream(KEY_FILE));
		akey = (SecretKey) ois.readObject();
		ois.close();
		return akey;
	}

	@SuppressWarnings("unused")
	private static boolean keyFileExists(String path) {
		File file = new File(path + KEY_FILE);
		if (file.exists())
			return true;
		return false;
	}

	public static String passMax(String passMax) throws Base64DecodingException{
		//String result = passMax.substring(0, passMax.length()-1);
		String result="";
		byte[] bytearray = Base64.decode(passMax.getBytes());
		for (int i=0; i<bytearray.length; i++){
			 result += new Character((char)bytearray[i]).toString();
		}
		return result;
	}

	public static String passMin(String passMin) {
		String result ="";
		result = Base64.encode(passMin.getBytes());
		return result;
	}
}
