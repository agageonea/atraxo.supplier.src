package de.fuelplus.crypt;

public class CryptTest {

	/**
	 * @param args
	 * @throws FpsCryptException
	 */
	public static void main(String[] args) throws FpsCryptException {
		String[] pass = { 
				"fismgr", 
				"fismgr1", 
				"qwerty", 
				"qwerty1",
				"UndeMergemAzi",
				"#$$%^&***",
				"Al^&(()_=]][}|}{:",
				"FismgrfISMGRKnewHope*"};

		for (int i = 0; i < pass.length; i++) {
			String en = FpsCrypt.encryptToken(pass[i]);
			String de = FpsCrypt.decryptToken(en);
			System.out.print(String.format("%30s", pass[i]) + " : ");
			if (pass[i].equals(de)) 
				System.out.print("SUCESS");
			else
				System.out.print("ERROR");
			System.out.println();

		}
		System.out.println("Terminated");
	}

}
