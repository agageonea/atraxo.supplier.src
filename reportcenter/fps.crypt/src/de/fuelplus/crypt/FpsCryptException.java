package de.fuelplus.crypt;

public class FpsCryptException extends Exception {

	private static final long serialVersionUID = 8967932238373797286L;

	public FpsCryptException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FpsCryptException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public FpsCryptException(String message) {
		super(message);

	}

	public FpsCryptException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
