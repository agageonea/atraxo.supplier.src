package com.atraxo.reportcenter.birt.emitter.csv;

/*******************************************************************************
 * Copyright (c) 2004 Actuate Corporation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Actuate Corporation  - initial API and implementation
 *******************************************************************************/

/**
 * 
 * define all CSV tags used in  CSV emitter 
 */
public class CSVTags
{
  
    public static final String TAG_COMMA = "," ; //$NON-NLS-1$
    public static final String TAG_CR = "\n" ; //$NON-NLS-1$
    
}
