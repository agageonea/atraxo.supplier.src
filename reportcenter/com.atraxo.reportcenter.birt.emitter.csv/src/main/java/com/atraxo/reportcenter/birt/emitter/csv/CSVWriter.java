
/*******************************************************************************
 * Copyright (c) 2004 Actuate Corporation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Actuate Corporation  - initial API and implementation
 *******************************************************************************/

package com.atraxo.reportcenter.birt.emitter.csv;

/**
 * @author
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.birt.report.engine.emitter.XMLWriter;

/**
 * <code>HTMLWriter</code> is a concrete subclass of <code>XMLWriter</code>
 * that outputs the HTML content.
 *
 * @version $Revision: 76834 $ $Date: 2015-04-03 19:32:01 +0300 (Fri, 03 Apr 2015) $
 */
public class CSVWriter
{
	/** logger */
	protected static Logger log = Logger.getLogger( XMLWriter.class.getName( ) );

	/** the print writer for outputting */
	protected PrintWriter printWriter;

	/** character encoding */
	protected String encoding = "UTF-8"; //$NON-NLS-1$

	/** whether or not we have text before the end tag */
	protected boolean bText = false;

	private String quoteString = "\"";

	/**
	 * Creates a CSVWriter using this constructor.
	 */
	public CSVWriter( )
	{
	}

	/**
	 * Outputs java script code.
	 *
	 * @param code
	 *            a line of code
	 */
	public void writeCode( String code )
	{
	}

	// Overrides
	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.birt.report.engine.emitter.XMLWriter#startWriter()
	 */
	public void startWriter( )
	{

	}
	public void open( OutputStream outputStream, String encoding )
	{
		assert ( encoding != null );
		assert ( outputStream != null );

		this.encoding = encoding;
		try
		{
			this.printWriter = new PrintWriter( new OutputStreamWriter(
					outputStream, encoding ), false );
		}
		catch ( UnsupportedEncodingException e )
		{
			log.log( Level.SEVERE,
					"the character encoding {0} unsupported !", encoding ); //$NON-NLS-1$
		}
	}

	public void open( OutputStream outputStream )
	{
		open( outputStream, "UTF-8" ); //$NON-NLS-1$
	}

	public void close( )
	{
		printWriter.close( );
	}
	/**
	 * the event of endding the writer
	 */
	public void endWriter( )
	{
		printWriter.flush( );
	}

	/**
	 * Close the tag
	 *
	 * @param tagName
	 *            tag name
	 */
	public void closeTag( String tagName )
	{
		printWriter.print( tagName );

	}

	/**
	 * Close the tag whose end tag is forbidden say, "br".
	 *
	 * @param tagName
	 *            tag name
	 */
	public void closeNoEndTag( )
	{

	}
	/**
	 * Output the encoded content
	 *
	 * @param value
	 *            the content
	 */
	public void text( String value )
	{
		if (value != null) {
			value = value.replaceAll(String.valueOf((char)13), " ");
			value = value.replaceAll(String.valueOf((char)10), " ");
		}
		else
			value = "";

		printWriter.print( quoteString + value + quoteString);
	}

	public String getQuoteString() {
		return quoteString;
	}

	public void setQuoteString(String quoteString) {
		this.quoteString = quoteString;
	}


}