package com.atraxo.reportcenter.birt.emitter.csv;

import org.eclipse.birt.report.engine.api.RenderOption;

public class CSVRenderOption extends RenderOption {

	public static final String CSV = "CSV";
	public static final String FIELD_SEPARATOR = "CSV_FLD_SEP";
	public static final String QUOTE = "CSV_QUOT";
	public static final String DATE_FMT = "CSV_FMT_DATE";
	public static final String DEC_SEP = "CSV_DEC_SEP";
	public static final String TH_SEP = "CSV_TH_SEP";

	private String fieldSeparator = FIELD_SEPARATOR;
	private String quoteString = QUOTE;
	private String dateFormat = DATE_FMT;
	private String decimalSeparator = DEC_SEP;
	private String thousandSeparator = TH_SEP;

	/**
	 * constructor
	 *
	 */
	public CSVRenderOption() {
		this.setOption(CSVRenderOption.FIELD_SEPARATOR, ";");
		this.setOption(CSVRenderOption.QUOTE, "\"");
		this.setOption(CSVRenderOption.DATE_FMT, "dd.MM.yyyy");
		this.setOption(CSVRenderOption.DEC_SEP, ".");
		this.setOption(CSVRenderOption.TH_SEP, "");
	}

//	public String getFieldSeparator() {
//		return fieldSeparator;
//	}
//
//	public void setFieldSeparator(String fieldSeparator) {
//		this.fieldSeparator = fieldSeparator;
//		this.setOption(FIELD_SEPARATOR, fieldSeparator);
//	}
//
//	public String getQuoteString() {
//		return quoteString;
//	}
//
//	public void setQuoteString(String quoteString) {
//		this.quoteString = quoteString;
//		this.setOption(QUOTE, quoteString);
//	}
//
//	public String getDateFormat() {
//		return dateFormat;
//	}
//
//	public void setDateFormat(String dateFormat) {
//		this.dateFormat = dateFormat;
//		this.setOption(DATE_FMT, dateFormat);
//	}
//
//	public String getDecimalSeparator() {
//		return decimalSeparator;
//	}
//
//	public void setDecimalSeparator(String decimalSeparator) {
//		this.decimalSeparator = decimalSeparator;
//		this.setOption(DEC_SEP, decimalSeparator);
//	}
//
//	public String getThousandSeparator() {
//		return thousandSeparator;
//	}
//
//	public void setThousandSeparator(String thousandSeparator) {
//		this.thousandSeparator = thousandSeparator;
//		this.setOption(TH_SEP, thousandSeparator);
//	}

}