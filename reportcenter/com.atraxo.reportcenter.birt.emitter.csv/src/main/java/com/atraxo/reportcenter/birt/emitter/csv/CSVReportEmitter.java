/*******************************************************************************
 * Copyright (c) 2004 Actuate Corporation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * Contributors:
 *  Actuate Corporation  - initial API and implementation
 *******************************************************************************/

/**
 * $Revision: 76834 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.birt.emitter.csv/src/main/java/com/atraxo/reportcenter/birt/emitter/csv/CSVReportEmitter.java $
 */

package com.atraxo.reportcenter.birt.emitter.csv;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.birt.core.exception.BirtException;
import org.eclipse.birt.report.engine.api.IRenderOption;
import org.eclipse.birt.report.engine.api.RenderOption;
import org.eclipse.birt.report.engine.content.IAutoTextContent;
import org.eclipse.birt.report.engine.content.IBandContent;
import org.eclipse.birt.report.engine.content.ICellContent;
import org.eclipse.birt.report.engine.content.IContainerContent;
import org.eclipse.birt.report.engine.content.IContent;
import org.eclipse.birt.report.engine.content.IDataContent;
import org.eclipse.birt.report.engine.content.IElement;
import org.eclipse.birt.report.engine.content.IForeignContent;
import org.eclipse.birt.report.engine.content.IGroupContent;
import org.eclipse.birt.report.engine.content.IImageContent;
import org.eclipse.birt.report.engine.content.ILabelContent;
import org.eclipse.birt.report.engine.content.IListBandContent;
import org.eclipse.birt.report.engine.content.IListContent;
import org.eclipse.birt.report.engine.content.IListGroupContent;
import org.eclipse.birt.report.engine.content.IPageContent;
import org.eclipse.birt.report.engine.content.IReportContent;
import org.eclipse.birt.report.engine.content.IRowContent;
import org.eclipse.birt.report.engine.content.ITableBandContent;
import org.eclipse.birt.report.engine.content.ITableContent;
import org.eclipse.birt.report.engine.content.ITableGroupContent;
import org.eclipse.birt.report.engine.content.ITextContent;
import org.eclipse.birt.report.engine.emitter.ContentEmitterAdapter;
import org.eclipse.birt.report.engine.emitter.IEmitterServices;
import org.eclipse.birt.report.engine.presentation.ContentEmitterVisitor;

/**
 * <code>CSVReportEmitter</code> is a subclass of
 * <code>ContentEmitterAdapter</code> that implements IContentEmitter interface
 * to output IARD Report objects to CSV file.
 */
public class CSVReportEmitter extends ContentEmitterAdapter {
	// new
	public final static String APPCONTEXT_CSV_RENDER_CONTEXT = "CSV_RENDER_CONTEXT"; //$NON-NLS-1$

	/**
	 * the output format
	 */
	public static final String OUTPUT_FORMAT_CSV = "CSV"; //$NON-NLS-1$

	/**
	 * the default target report file name
	 */
	public static final String REPORT_FILE = "report.csv"; //$NON-NLS-1$

	/**
	 * output stream
	 */
	protected OutputStream out = null;

	/**
	 * the report content
	 */
	protected IReportContent report;

	/**
	 * the render options
	 */
	protected IRenderOption renderOption;

	/**
	 * should output the page header & footer
	 */
	protected boolean outputMasterPageContent = true;

	/**
	 * the <code>CSVWriter<code> object that is used to output CSV content
	 */
	protected CSVWriter writer;

	/**
	 * An Log object that <code>CSVReportEmitter</code> use to log the error,
	 * debug, information messages.
	 */
	protected static Logger logger = Logger.getLogger(CSVReportEmitter.class
			.getName());

	/**
	 * emitter services
	 */
	protected IEmitterServices services;

	/**
	 * content visitor that is used to handle page header/footer
	 */
	protected ContentEmitterVisitor contentVisitor;

	/**
	 * The plug-in exports only data/label elements in the table body if the
	 * element belongs to the table header, footer, or is an element of type
	 * Image it does not get exported. The flag is used to indicate if the
	 * current processed element is exportable.
	 *
	 */
	protected boolean exportElement = true;

	protected boolean skipCell = false;

	protected boolean rowEmpty = false;

	/**
	 * Used to indicate nested tables. It increases with each startTable() and
	 * decreases with endTable()
	 */
	protected int getTableDepth() {
		return tablesStack.size();
	}

	private class TableInfo {
		private int columnCount = 0;
		private int currentColumn = 0;
	}

	protected Stack<TableInfo> tablesStack = new Stack<TableInfo>();

	private DecimalFormatSymbols decFmtSymb;

	/**
	 * the constructor
	 */
	public CSVReportEmitter() {
		contentVisitor = new ContentEmitterVisitor(this);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#initialize(org
	 * .eclipse.birt.report.engine.emitter.IEmitterServices)
	 */
	public void initialize(IEmitterServices services) {
		this.services = services;
		this.renderOption = services.getRenderOption();

		Object fd = services.getOption(RenderOption.OUTPUT_FILE_NAME);
		File file = null;
		try {
			if (fd != null) {
				file = new File(fd.toString());
				File parent = file.getParentFile();
				if (parent != null && !parent.exists()) {
					parent.mkdirs();
				}
				out = new BufferedOutputStream(new FileOutputStream(file));
			}
		} catch (FileNotFoundException e) {
			logger.log(Level.WARNING, e.getMessage(), e);
		}

		if (out == null) {
			Object value = services.getOption(RenderOption.OUTPUT_STREAM);
			if (value != null && value instanceof OutputStream) {
				out = (OutputStream) value;
			} else {
				try {
					file = new File(REPORT_FILE);
					out = new BufferedOutputStream(new FileOutputStream(file));
				} catch (FileNotFoundException e) {
					logger.log(Level.SEVERE, e.getMessage(), e);
				}
			}
		}
		writer = new CSVWriter();

		Locale locale = Locale.getDefault();
		decFmtSymb = new DecimalFormatSymbols(locale);
		if ((CSVRenderOption.DEC_SEP != null) && (CSVRenderOption.DEC_SEP.length() > 0)) {
			decFmtSymb.setDecimalSeparator(renderOption.getOption(CSVRenderOption.DEC_SEP).toString().charAt(0));
		}
		if ((CSVRenderOption.TH_SEP != null) && (CSVRenderOption.TH_SEP.length() > 0)) {
			decFmtSymb.setGroupingSeparator(renderOption.getOption(CSVRenderOption.TH_SEP).toString().charAt(0));
		}
	}

	/**
	 * @return the <code>Report</code> object.
	 */
	public IReportContent getReport() {
		return report;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#getOutputFormat()
	 */
	public String getOutputFormat() {
		return OUTPUT_FORMAT_CSV;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#start(org.eclipse
	 * .birt.report.engine.content.IReportContent)
	 */
	public void start(IReportContent report) {
		logger.log(Level.FINE, "[CSVReportEmitter] Start emitter."); //$NON-NLS-1$

		this.report = report;

		this.tablesStack.clear();

		writer.setQuoteString((String) renderOption
				.getOption(CSVRenderOption.QUOTE));

		writer.open(out, "UTF-8"); //$NON-NLS-1$

		writer.startWriter();

		Object repCode = report.getReportContext().getParameterValue(
				"P_Report_Code");
		if (repCode != null) {
			writer.text(repCode.toString());
//			writer.closeTag((String) renderOption.getOption(CSVRenderOption.FIELD_SEPARATOR));
			writer.closeTag(CSVTags.TAG_CR);
			writer.closeTag(CSVTags.TAG_CR);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#end(org.eclipse
	 * .birt.report.engine.content.IReportContent)
	 */
	public void end(IReportContent report) {
		logger.log(Level.FINE, "[CSVReportEmitter] End report."); //$NON-NLS-1$
		writer.endWriter();
		writer.close();
		if (out != null) {
			try {
				out.close();
			} catch (IOException e) {
				logger.log(Level.WARNING, e.getMessage(), e);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#startPage(org.
	 * eclipse.birt.report.engine.content.IPageContent)
	 */
	public void startPage(IPageContent page) {

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#endPage(org.eclipse
	 * .birt.report.engine.content.IPageContent)
	 */
	public void endPage(IPageContent page) {

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#startTable(org
	 * .eclipse.birt.report.engine.content.ITableContent)
	 */
	public void startTable(ITableContent table) {
		logger.log(Level.FINE, "[CSVTableEmitter] Start table ID :"
				+ table.getInstanceID().getComponentID());

		TableInfo currentTable = new TableInfo();
		currentTable.columnCount = table.getColumnCount();
		tablesStack.push(currentTable);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#endTable(org.eclipse
	 * .birt.report.engine.content.ITableContent)
	 */
	public void endTable(ITableContent table) {
		tablesStack.pop();
	}

	public void startRow(IRowContent row) {
		assert row != null;
		TableInfo tableInfo = tablesStack.peek();
		tableInfo.currentColumn = 0;

		// if ( isRowInFooterBand( row ) )
		// exportElement = false;
	}

	boolean isRowInFooterBand(IRowContent row) {
		IElement parent = row.getParent();
		if (!(parent instanceof IBandContent)) {
			return false;
		}
		IBandContent band = (IBandContent) parent;
		if (band.getBandType() == IBandContent.BAND_FOOTER) {
			return true;
		}
		return false;
	}

	boolean isRowInHeaderBand(IRowContent row) {
		IElement parent = row.getParent();
		if (!(parent instanceof IBandContent)) {
			return false;
		}
		IBandContent band = (IBandContent) parent;
		if (band.getBandType() == IBandContent.BAND_HEADER) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#endRow(org.eclipse
	 * .birt.report.engine.content.IRowContent)
	 */
	public void endRow(IRowContent row) {
		if (exportElement && (getTableDepth() <= 1)) {
			writer.closeTag(CSVTags.TAG_CR);
		}

		exportElement = true;

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#startCell(org.
	 * eclipse.birt.report.engine.content.ICellContent)
	 */
	public void startCell(ICellContent cell) {
		TableInfo tableInfo = tablesStack.peek();
		tableInfo.currentColumn = tableInfo.currentColumn + 1;

		if ((cell != null) && (cell.getWidth() != null)) {
			if (cell.getWidth().getMeasure() == 0)
				skipCell = true;
		}
		if ((cell != null) && (cell.getHeight() != null)) {
			if (cell.getHeight().getMeasure() == 0)
				skipCell = true;
		}

		logger.log(Level.FINE, "[CSVTableEmitter] Start cell."); //$NON-NLS-1$

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#endCell(org.eclipse
	 * .birt.report.engine.content.ICellContent)
	 */
	public void endCell(ICellContent cell) {
		logger.log(Level.FINE, "[CSVReportEmitter] End cell."); //$NON-NLS-1$

		TableInfo tableInfo = tablesStack.peek();
		logger.log(Level.FINE, "[CSVReportEmitter] Cell " + tableInfo.currentColumn + " / " + tableInfo.columnCount); //$NON-NLS-1$

		if (! skipCell) {
			if ((getTableDepth() <= 1) && (tableInfo.currentColumn < tableInfo.columnCount)) {
				writer.closeTag((String) renderOption.getOption("CSV_FLD_SEP"));
			}
//			else if (tableInfo.currentColumn < (tableInfo.columnCount - 1) ) {
//				writer.closeTag((String) renderOption.getOption("CSV_FLD_SEP"));
//			}
		}

		skipCell = false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#startContainer
	 * (org.eclipse.birt.report.engine.content.IContainerContent)
	 */
	public void startContainer(IContainerContent container) {
		logger.log(Level.FINE, "[CSVReportEmitter] Start container"); //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#endContainer(org
	 * .eclipse.birt.report.engine.content.IContainerContent)
	 */
	public void endContainer(IContainerContent container) {
		logger.log(Level.FINE, "[CSVContainerEmitter] End container"); //$NON-NLS-1$

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#startText(org.
	 * eclipse.birt.report.engine.content.ITextContent)
	 */
	public void startText(ITextContent text) {
		logger.log(Level.FINE, "[CSVReportEmitter] Start text"); //$NON-NLS-1$

		String textValue = text.getText();
		if (exportElement) {
			writer.text(textValue);
			logger.log(Level.FINE, "[CSVReportEmitter] Text :" + textValue);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#startForeign(org
	 * .eclipse.birt.report.engine.content.IForeignContent)
	 */
	public void startForeign(IForeignContent foreign) {
		logger.log(Level.FINE, "[CSVReportEmitter] Start foreign");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#startLabel(org
	 * .eclipse.birt.report.engine.content.ILabelContent)
	 */
	public void startLabel(ILabelContent label) {
		logger.log(Level.FINE, "[CSVReportEmitter] Start label");
		startText(label);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#startData(org.
	 * eclipse.birt.report.engine.content.IDataContent)
	 */
	public void startData(IDataContent data) {
		logger.log(Level.FINE, "[CSVReportEmitter] Start data");

		if (exportElement) {
			Object objValue = data.getValue();
			String textValue = data.getText();

			if ((objValue instanceof Date) || (objValue instanceof java.util.Date)){
				if (textValue != null) {
					writer.text(textValue.toUpperCase());
					logger.log(Level.FINE, "[CSVReportEmitter] Data :" + textValue);
				}
				else {
					writer.text("");
					logger.log(Level.FINE, "[CSVReportEmitter] Data :" + "");
				}
			}
			else if (objValue instanceof BigDecimal) {
				BigDecimal v = (BigDecimal) objValue;
				String fmt = "#.0";
				if (data.getStyle() != null) {
					if (data.getStyle().getNumberFormat() != null) {
						fmt = data.getStyle().getNumberFormat();
						if (fmt != null) {
						    if (fmt.contains("{Rounding")) {
							int idx = fmt.indexOf("{Rounding");
							if (idx >= 0) fmt = fmt.substring(0, idx);
						    }
						}
					}
				}
				DecimalFormat df = new DecimalFormat(fmt, decFmtSymb);
				writer.text(df.format(v));

			}
			else {
				if (objValue != null) {
					logger.log(Level.FINE, "[CSVReportEmitter] Data :" + textValue + "( type: " + objValue.getClass().getName() + ")");
				}

				startText(data);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.birt.report.engine.emitter.IContentEmitter#startImage(org
	 * .eclipse.birt.report.engine.content.IImageContent)
	 */
	public void startImage(IImageContent image) {
		logger.log(Level.FINE, "[CSVReportEmitter] Start image");
		exportElement = false;
	}

	/**
	 * handle style image
	 *
	 * @param uri
	 *            uri in style image
	 * @return
	 */
	public String handleStyleImage(String uri) {
		String id = null;
		return id;
	}

	@Override
	public void startAutoText(IAutoTextContent autoText) throws BirtException {
		logger.log(Level.FINE, "[CSVReportEmitter] Start autoText");
		startText(autoText);
		//super.startAutoText(autoText);
	}

	@Override
	public void startContent(IContent content) throws BirtException {
		logger.log(Level.FINE, "[CSVReportEmitter] Start content");
		super.startContent(content);
	}

	@Override
	public void startGroup(IGroupContent group) throws BirtException {
		logger.log(Level.FINE, "[CSVReportEmitter] Start group");
		super.startGroup(group);
	}

	@Override
	public void startList(IListContent list) throws BirtException {
		logger.log(Level.FINE, "[CSVReportEmitter] Start list");
		super.startList(list);
	}

	@Override
	public void startListBand(IListBandContent listBand) throws BirtException {
		logger.log(Level.FINE, "[CSVReportEmitter] Start listBand");
		super.startListBand(listBand);
	}

	@Override
	public void startListGroup(IListGroupContent group) throws BirtException {
		// TODO Auto-generated method stub
		super.startListGroup(group);
	}

	@Override
	public void startTableBand(ITableBandContent band) throws BirtException {
		// TODO Auto-generated method stub
		super.startTableBand(band);
	}

	@Override
	public void startTableGroup(ITableGroupContent group) throws BirtException {
		// TODO Auto-generated method stub
		super.startTableGroup(group);
	}

}