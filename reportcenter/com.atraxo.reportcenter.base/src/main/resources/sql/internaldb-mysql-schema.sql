DROP TABLE run_reports IF EXISTS;
CREATE TABLE run_reports
(
  run_rpt_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  rpt_name   varchar(25) not null,
  rpt_ttl    varchar(200),
  usr        varchar(15),
  stat       varchar(1),
  url        varchar(2000),
  err_msg    varchar(4000),
  start_dt   datetime,
  end_dt     datetime,
  filename   varchar(255)
);