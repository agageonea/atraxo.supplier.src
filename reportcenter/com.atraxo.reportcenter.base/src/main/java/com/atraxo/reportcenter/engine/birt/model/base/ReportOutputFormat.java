/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 77121 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/base/ReportOutputFormat.java $
 */
package com.atraxo.reportcenter.engine.birt.model.base;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum ReportOutputFormat {

    //@formatter:off
    @XmlEnumValue("PDF")
    PDF("PDF"), 
    @XmlEnumValue("HTML")
    HTML("HTML"), 
    @XmlEnumValue("DOC")
    DOC("doc"), 
    @XmlEnumValue("DOCX")
    DOCX("docx"), 
    @XmlEnumValue("XLS")
    XLS("XLS"), 
    @XmlEnumValue("XLSX")
    XLSX("XLSX"), 
    @XmlEnumValue("CSV")
    CSV("CSV"), 
    @XmlEnumValue("XML")
    // Generic XML exporter is not currently implemented
    XML("XML"), // Possible implementation can be done using ICustomReporter
    @XmlEnumValue("FIXUP")
    // Generic FixLen Upload exporter is not currently implemented
    FLU("FIXUP") // Possible implementation can be done using ICustomReporter
    //@formatter:on
    ;

    private final String value;

    ReportOutputFormat(String v) {
	this.value = v;
    }

    public static ReportOutputFormat fromValue(String v) {
	for (ReportOutputFormat rt : ReportOutputFormat.values()) {
	    if (rt.value.equals(v)) {
		return rt;
	    }
	}
	throw new IllegalArgumentException(v);
    }
}
