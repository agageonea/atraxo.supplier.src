/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 76832 $
 * $Date: 11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL:
 * http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dialog/DialogReport.java
 * $
 */
package com.atraxo.reportcenter.engine.birt.model.dialog;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.birt.report.engine.api.IRenderTask;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atraxo.reportcenter.engine.base.ReportCenterException;
import com.atraxo.reportcenter.engine.birt.BirtParamNames;
import com.atraxo.reportcenter.engine.birt.model.base.BaseReportWithParams;
import com.atraxo.reportcenter.engine.birt.model.base.ReportParam;

@XmlRootElement(name = "dialog-report")
public class DialogReport extends BaseReportWithParams {

	private static final Logger logger = LoggerFactory.getLogger(DialogReport.class);

	@Override
	public String getReportFileName() {
		return "/DialogReports/" + getCode() + ".rptdesign";
	}

	@Override
	public void assignParametersToReport(IReportEngine engine, IReportRunnable design, IRenderTask task)
	        throws ReportCenterException {
		super.assignParametersToReport(engine, design, task);

		task.setParameterValue(BirtParamNames.P_JDBC_DB_USER, getJdbcUsername());
		task.setParameterValue(BirtParamNames.P_JDBC_DB_PASS, getJdbcPassword());
		task.setParameterValue(BirtParamNames.P_JDBC_DB_URL, getJdbcDbUrl());
		task.setParameterValue(BirtParamNames.P_JDBC_DB_DRV_CLASS, getReportCenterConfig().getDbDriver());

		logger.debug("Username : " + getJdbcUsername());
		logger.debug("Password : " + getJdbcPassword());

		logger.debug("Report parameters ");
		for (ReportParam param : getParams()) {
			Object obj = param.getConvValue(this);

			if (obj == null) {
				logger.debug("Param. " + param.getName() + ": null");
			} else {
				logger.debug("Param. " + param.getName() + ": " + param.getValue());
			}

			task.setParameterValue(param.getName(), obj);
		}
	}

	@Override
	public void assignParametersToReport(IReportEngine engine, IReportRunnable design, IRunTask task)
	        throws ReportCenterException {
		super.assignParametersToReport(engine, design, task);

		task.setParameterValue(BirtParamNames.P_JDBC_DB_USER, getJdbcUsername());
		task.setParameterValue(BirtParamNames.P_JDBC_DB_PASS, getJdbcPassword());
		task.setParameterValue(BirtParamNames.P_JDBC_DB_URL, getJdbcDbUrl());
		task.setParameterValue(BirtParamNames.P_JDBC_DB_DRV_CLASS, getReportCenterConfig().getDbDriver());

		logger.debug("Username : " + getJdbcUsername());
		logger.debug("Password : " + getJdbcPassword());

		logger.debug("Report parameters ");
		for (ReportParam param : getParams()) {
			Object obj = param.getConvValue(this);

			if (obj == null) {
				logger.debug("Param. " + param.getName() + ": null");
			} else {
				logger.debug("Param. " + param.getName() + ": " + param.getValue());
			}

			task.setParameterValue(param.getName(), obj);
		}
	}
	
}
