package com.atraxo.reportcenter.reportexec;

import java.io.File;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

public class ReportExecutionImpl implements IReportExecutionObject {

    private Long execId; // NUMERIC(10) not null,
    private String name; // VARCHAR(25) not null,
    private String title; // VARCHAR(200),
    private String user; // VARCHAR(15),
    private String stat; // VARCHAR(1),
    private String url; // VARCHAR(2000),
    private String errMsg; // VARCHAR(4000),
    private Date startDt; // DATE,
    private Date endDt; // DATE,
    private String filename; // VARCHAR(255)

    private String localResultFile;

    @XmlElement(name="id")
    public Long getExecId() {
	return execId;
    }

    public void setExecId(Long execId) {
	this.execId = execId;
    }

    @XmlElement(name="code")
    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    @XmlElement(name="title")
    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    @XmlElement(name="user")
    public String getUser() {
	return user;
    }

    public void setUser(String user) {
	this.user = user;
    }

    @XmlElement(name="status")
    public String getStatus() {
	return stat;
    }

    public void setStatus(String stat) {
	this.stat = stat;
    }

    @XmlTransient
    public String getUrl() {
	return url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    @XmlElement(name="error")
    public String getErrMsg() {
	return errMsg;
    }

    public void setErrMsg(String errMsg) {
	this.errMsg = errMsg;
    }

    @XmlElement(name="startAt")
    public Date getStartDt() {
	return startDt;
    }

    public void setStartDt(Date startDt) {
	this.startDt = startDt;
    }

    @XmlElement(name="endAt")
    public Date getEndDt() {
	return endDt;
    }

    public void setEndDt(Date endDt) {
	this.endDt = endDt;
    }

    @XmlTransient
    public String getFilename() {
	return filename;
    }

    public void setFilename(String filename) {
	this.filename = filename;
    }

    @Override
    public boolean isSuccess() {
	return ReportExecutionConstants.REPORT_STATUS_SUCCESS.equals(stat);
    }

    @Override
    @XmlTransient
    public String getLocalResultFile() {
	return this.localResultFile;
    }

    @Override
    public void setLocalResultFile(String fileName) {
	this.localResultFile = fileName;
	if (fileName != null) {
	    File file = new File(fileName);
	    this.filename = file.getName();
	}
    }

}
