/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 77121 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/DynamicReportTotals.java $
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;
import com.atraxo.reportcenter.engine.birt.model.dynamic.parser.DynamicReportXMLParser;
import com.atraxo.reportcenter.engine.birt.model.dynamic.parser.INodeHandler;

public class DynamicReportTotals {

    private static final Logger LOG = LoggerFactory.getLogger(DynamicReportTotals.class);

    private List<List<IDynamicReportTotal>> totals = new ArrayList<List<IDynamicReportTotal>>();
    private DynamicReportXMLParser xmlParser;

    private BaseReport report;

    public DynamicReportTotals(BaseReport report) {
	this.report = report;
    }

    public List<List<IDynamicReportTotal>> getTotals() {
	return totals;
    }

    public void readTotals() {
	xmlParser.cleanUp();

	xmlParser.setStartPath("/reportData/records/record");
	xmlParser.setStartPathAttributeName("type");
	xmlParser.setStartPathAttributeValue("total");
	xmlParser.setEndPath("/reportData/records");

	INodeHandler handler = new INodeHandler() {

	    private List<IDynamicReportTotal> row;

	    public void onChildNode(String name, String value, Hashtable<String, String> attributes) {
		if (row == null)
		    return;

		DynamicReportTotal total = new DynamicReportTotal(DynamicReportTotals.this.getReport());

		String fmtAttr = attributes.get("fmt");
		if (fmtAttr != null)
		    total.setFormat(fmtAttr);

		String lblAttr = attributes.get("lbl");
		if (lblAttr != null)
		    total.setLabel(lblAttr);

		total.setCode(name);
		total.setValue(value);

		row.add(total);
	    }

	    public void onNodeStart(String name, Hashtable<String, String> attributes) {
		row = new ArrayList<IDynamicReportTotal>();
	    }

	    public void onNodeEnd(String name, Hashtable<String, String> attributes) {
		totals.add(row);
	    }
	};

	xmlParser.addNodeHandler(handler);

	try {
	    xmlParser.process();
	} catch (ParserConfigurationException e) {
	    LOG.error(e.getMessage(), e);
	} catch (SAXException e) {
	    LOG.error(e.getMessage(), e);
	} catch (IOException e) {
	    LOG.error(e.getMessage(), e);
	}

	xmlParser.removeNodeHandler(handler);

    }

    public void printTotals() {
	LOG.debug("Totals");
	for (int j = 0; j < totals.size(); j++) {
	    List<IDynamicReportTotal> row = totals.get(j);

	    LOG.debug("Total row " + j);
	    for (int i = 0; i < row.size(); i++) {
		DynamicReportTotal col = (DynamicReportTotal) row.get(i);
		LOG.debug(col.getCode() + "; lineNo=" + j + "; label=" + col.getLabel() + "; value=" + col.getValue() + " format=" + col.getFormat());
	    }
	}
    }

    public int size() {
	return totals.size();
    }

    public void setXmlParser(DynamicReportXMLParser xmlPArser) {
	this.xmlParser = xmlPArser;
    }

    public void clear() {
	totals.clear();
    }

    public List<IDynamicReportTotal> getTotal(int i) {
	return totals.get(i);
    }

    public IDynamicReportTotal getTotalForColumn(int rowNo, IDynamicReportColumn column) {
	List<IDynamicReportTotal> row = getTotal(rowNo);
	for (int j = 0; j < row.size(); j++) {
	    IDynamicReportColumn col = (IDynamicReportColumn) row.get(j);
	    if (col.getCode().equals(column.getCode()))
		return row.get(j);
	}
	return null;
    }

    public BaseReport getReport() {
        return report;
    }
}
