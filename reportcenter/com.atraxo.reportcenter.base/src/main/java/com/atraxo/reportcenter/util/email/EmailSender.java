package com.atraxo.reportcenter.util.email;

import java.util.List;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmailSender {

	private static final Logger logger = LoggerFactory.getLogger(EmailSender.class);

	private String to; // "abcd@gmail.com";
	private String from; // "web@gmail.com";
	private String subject; // "This is the Subject Line!";
	private String body; // "This is message body";
	private List<FpEmailAttachment> attachments = null;
	private String smtpHost;
	private String smtpUser;
	private String smtpPass;
	private int smtpPort = 25;
	private boolean useSSLOnConnect = false;
	private boolean startTlsEnabled = false;

	public EmailSender() {
		super();
	}

	public void send() throws EmailException {
		logger.info("Prepare to send email message ");
		logger.debug(" - Host    : " + smtpHost);
		logger.debug(" - Port    : " + smtpPort);
		logger.debug(" - SSL     : " + useSSLOnConnect);
		logger.debug(" - TLS     : " + startTlsEnabled);
		logger.debug(" - User    : " + smtpUser);
		logger.debug(" - Pass    : " + smtpPass);
		logger.info (" - From     : " + from);
		logger.info (" - To       : " + to);
		logger.info (" - Subject  : " + subject);
		logger.info (" - Body     : " + body);
		logger.info (" - Attachments: ");
		for (FpEmailAttachment af : attachments) {
		    logger.info("    - " + af.getEmailFile());
		}


		// Create the email message
		MultiPartEmail email = new MultiPartEmail();
		email.setHostName(smtpHost);

		email.setSmtpPort(smtpPort);
		if (smtpUser != null) {
			email.setAuthenticator(new DefaultAuthenticator(smtpUser, smtpPass));
		}
		email.setSSLOnConnect(useSSLOnConnect);
		email.setStartTLSEnabled(startTlsEnabled);

		String tos[] = to.split(";");
		if (tos.length > 0) {
			for (String t : tos)
				if ((t != null) && (! "".equals(t))) email.addTo(t);
		} else {
			email.addTo(to);
		}
		email.setFrom(from);
		email.setSubject(subject);
		email.setMsg(body);

		// Create the attachment
		for (FpEmailAttachment af : attachments) {
			EmailAttachment attachment = new EmailAttachment();
			attachment.setPath(af.getLocalFile());
			attachment.setDisposition(EmailAttachment.ATTACHMENT);
			attachment.setDescription(af.getEmailFile());
			attachment.setName(af.getEmailFile());

			// add the attachment
			email.attach(attachment);
		}

		// send the email
		email.send();
		logger.debug("Email sucessfully sent");
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public List<FpEmailAttachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<FpEmailAttachment> attachments) {
		this.attachments = attachments;
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public String getSmtpUser() {
		return smtpUser;
	}

	public void setSmtpUser(String smtpUser) {
		this.smtpUser = smtpUser;
	}

	public String getSmtpPass() {
		return smtpPass;
	}

	public void setSmtpPass(String smtpPass) {
		this.smtpPass = smtpPass;
	}

	public int getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(int smtpPort) {
		this.smtpPort = smtpPort;
	}

	public boolean isUseSSLOnConnect() {
		return useSSLOnConnect;
	}

	public void setUseSSLOnConnect(boolean useSSLOnConnect) {
		this.useSSLOnConnect = useSSLOnConnect;
	}

	public boolean isStartTlsEnabled() {
		return startTlsEnabled;
	}

	public void setStartTlsEnabled(boolean startTlsEnabled) {
		this.startTlsEnabled = startTlsEnabled;
	}
}
