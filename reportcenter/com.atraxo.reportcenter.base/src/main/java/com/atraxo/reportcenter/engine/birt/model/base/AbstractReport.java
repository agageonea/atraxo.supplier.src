package com.atraxo.reportcenter.engine.birt.model.base;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.xml.bind.annotation.XmlTransient;

import org.springframework.context.ApplicationContext;

import com.atraxo.reportcenter.config.ReportCenterConfig;
import com.atraxo.reportcenter.engine.base.ReportCenterException;
import com.atraxo.reportcenter.util.DateUtils;
import com.atraxo.reportcenter.util.FileUtils;
import com.atraxo.reportcenter.util.IReportUtils;
import com.atraxo.reportcenter.util.ListUtils;

public abstract class AbstractReport {

    private ApplicationContext context;

    private DateUtils dateUtils;

    private FileUtils fileUtils;

    private ReportCenterConfig reportCenterConfig;

    private ListUtils listUtils;

    private IReportUtils reportUtils;

    @XmlTransient
    public ApplicationContext getContext() {
	return context;
    }

    public void setContext(ApplicationContext context) {
	this.context = context;
    }

    public void init() throws ReportCenterException {
	dateUtils = context.getBean(DateUtils.class);
	fileUtils = context.getBean(FileUtils.class);
	listUtils = context.getBean(ListUtils.class);
	reportUtils = context.getBean(IReportUtils.class);

	reportCenterConfig = context.getBean(ReportCenterConfig.class);
    }

    public abstract Connection getConnection() throws NamingException, SQLException;

    public abstract String getJdbcUsername();

    public abstract String getCode();

    public abstract String getFormatAsString();

    public abstract String getTitle();

    public abstract String getOutputFileName();

    public abstract String getFileExtension();

    public DateUtils getDateUtils() {
	return dateUtils;
    }

    public FileUtils getFileUtils() {
	return fileUtils;
    }

    public ReportCenterConfig getReportCenterConfig() {
	return reportCenterConfig;
    }

    public ListUtils getListUtils() {
	return listUtils;
    }

    public IReportUtils getReportUtils() {
	return reportUtils;
    }

}
