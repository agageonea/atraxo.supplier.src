package com.atraxo.reportcenter.util.xml;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class XMLDateAdapter extends XmlAdapter<String, Date> {

    private SimpleDateFormat fmt = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");

    @Override
    public Date unmarshal(String value) throws Exception {
	return fmt.parse(value);
    }

    @Override
    public String marshal(Date value) throws Exception {
	return fmt.format(value);
    }

}
