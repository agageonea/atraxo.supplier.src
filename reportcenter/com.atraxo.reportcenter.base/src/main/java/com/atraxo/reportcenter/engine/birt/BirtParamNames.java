/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 76832 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/BirtParamNames.java $
 */
package com.atraxo.reportcenter.engine.birt;

public interface BirtParamNames {

    public static final String P_DB_USER_PASS = "_rid";

    public static final String P_JDBC_DB_URL = "jdbcDbUrl";
    public static final String P_JDBC_DB_USER = "jdbcUsername";
    public static final String P_JDBC_DB_PASS = "jdbcPassword";
    public static final String P_JDBC_DB_DRV_CLASS = "jdbcDriverClass";
    public static final String P_JDBC_DEV_MODE = "jdbcDevMode";

    // public static final String P_REPORT_CODE = "rcode";
    // public static final String P_REPORT_FORMAT = "rfmt";
    // public static final String P_REPORT_TYPE = "rtype";

    public static final String P_REPORT_ORIENTATION = "rpgor";
    public static final String P_REPORT_PAGE = "rpage";

    public static final String P_REPORT_CODE = "P_Report_Code";
    public static final String P_REPORT_DYN_TITLE = "P_Report_Title";
    public static final String P_REPORT_DYN_ORIENTATION = "P_Report_Orientation";
    public static final String P_REPORT_FORMAT = "P_Report_Format";
    public static final String P_TIMESTAMP_DIFF = "P_Timestamp_diff";
    public static final String P_REPORT_TIMESTAMP = "P_Report_Timestamp";

    public static final String P_SHOW_FOOTER = "P_Show_Footer";
    public static final String P_SHOW_HEADER = "P_Show_Header";
    public static final String P_SHOW_PARAMS = "P_Show_Params";

    // BIRT named expression
    public static final String FP_REPORT_OBJ = "FP_REPORT_OBJ";  // report object
    public static final String FP_CONTEXT = "FP_CONTEXT";  // Spring context
}
