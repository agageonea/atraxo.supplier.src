/**
 * Copyright (C) FuelPlus GmbH 
 * $Revision: 76832 $  
 * $Date:   11/16/2005 11:35:09 AM$ 
 * $Author: fc $  
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/IXmlQueryHandler.java $  
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic;

import org.w3c.dom.Node;

public interface IXmlQueryHandler {

    public void onNodeStart(Node node, String name, String value);

    public void onChildNode(Node node, String name, String value);

    public void onNodeEnd(Node node);
}
