package com.atraxo.reportcenter.engine.base;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

public interface IReportRenderer {
//    String XLS = "xls";
//    String PDF = "pdf";
//    String DOC = "doc";
//    String ODP = "odp";
//    String HTML = "html";
//    String ODS = "ods";
//    String PPT = "ppt";
//    String ODT = "odt";

//    IRunAndRenderTask getRunAndRenderTask(String designName) throws EngineException, DesignFileException, SemanticException, IOException, ReportCenterException;
//
//    int render(IRunAndRenderTask task, String type, OutputStream outputStream, List<Exception> runErrors) throws EngineException, DesignFileException, SemanticException,
//	    IOException;
//
//    int render(IRunAndRenderTask task, String type, OutputStream outputStream, Map<String, Object> map, List<Exception> runErrors) throws EngineException, DesignFileException,
//	    SemanticException, IOException;
//
//    EmitterInfo[] getEmitterInfos();

    String getContentType(String type);

//    String getExtension(String type);


    int render(String type, OutputStream outputStream, Map<String, Object> map, List<Exception> runErrors);

}