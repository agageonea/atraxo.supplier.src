/**
 * Copyright (C) FuelPlus GmbH 
 * $Revision: 76832 $  
 * $Date:   11/16/2005 11:35:09 AM$ 
 * $Author: fc $  
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/parser/EndPathEncounteredException.java $  
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic.parser;

public class EndPathEncounteredException extends Exception {

    /**
	 *
	 */
    private static final long serialVersionUID = -6367342708954028999L;

}
