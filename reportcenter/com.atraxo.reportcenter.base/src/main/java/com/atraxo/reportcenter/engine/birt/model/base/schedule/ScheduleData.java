package com.atraxo.reportcenter.engine.birt.model.base.schedule;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.NONE)
public class ScheduleData {

    private Long itemExecId;
    private String destType;
    private String destName;

    @XmlElement(name = "id", required = true)
    public Long getItemExecId() {
	return itemExecId;
    }

    public void setItemExecId(Long itemExecId) {
	this.itemExecId = itemExecId;
    }

    @XmlElement(name = "dt", required = false)
    public String getDestType() {
	return destType;
    }

    public void setDestType(String destType) {
	this.destType = destType;
    }

    @XmlElement(name = "dn", required = false)
    public String getDestName() {
	return destName;
    }

    public void setDestName(String destName) {
	this.destName = destName;
    }

}
