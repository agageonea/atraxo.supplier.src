package com.atraxo.reportcenter.util.email;


public class FpEmailAttachment {

    private String localFile;
    private String emailFile;

    public FpEmailAttachment(String localFile, String emailFile) {
	super();
	this.localFile = localFile;
	this.emailFile = emailFile;
    }

    public String getLocalFile() {
	return localFile;
    }

    public String getEmailFile() {
	return emailFile;
    }

}
