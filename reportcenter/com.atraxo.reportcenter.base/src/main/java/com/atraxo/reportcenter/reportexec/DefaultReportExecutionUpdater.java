package com.atraxo.reportcenter.reportexec;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.atraxo.reportcenter.engine.base.ReportCenterException;
import com.atraxo.reportcenter.engine.birt.model.base.AbstractReport;
import com.atraxo.reportcenter.service.IReportExecution;

public class DefaultReportExecutionUpdater implements IReportExecution<Long> {

    @Autowired
    IReportExecutionDao<Long> rptExecDao;

    @Transactional
    public Long reportStart(AbstractReport report) throws ReportCenterException {
	IReportExecutionObject rptExec = new ReportExecutionImpl();
	rptExec.setName(report.getCode());
	rptExec.setTitle(report.getTitle());
	rptExec.setStartDt(new Date());
	rptExec.setEndDt(null);
	rptExec.setUser(report.getJdbcUsername());
	rptExec.setStatus(ReportExecutionConstants.REPORT_STATUS_RUNNING);

	try {
	Long id = rptExecDao.insert(rptExec);
	return id;
	}
	catch (Exception e) {

	    throw new ReportCenterException(e);
	}
    }

    @Transactional
    public void reportSuccess(Long id, String url, String fileName) throws ReportCenterException {

	IReportExecutionObject rptExec = rptExecDao.getById(id);

	rptExec.setUrl(url);
	rptExec.setFilename(fileName);
	rptExec.setEndDt(new Date());
	rptExec.setStatus(ReportExecutionConstants.REPORT_STATUS_SUCCESS);

	rptExecDao.update(rptExec);
    }

    @Transactional
    public void reportFailed(Long id, String errMsg) throws ReportCenterException {
	IReportExecutionObject rptExec = rptExecDao.getById(id);

	rptExec.setErrMsg(errMsg);
	rptExec.setEndDt(new Date());
	rptExec.setStatus(ReportExecutionConstants.REPORT_STATUS_FAILED);

	rptExecDao.update(rptExec);

    }


    @Override
    public List<IReportExecutionObject> listAll() {
	return rptExecDao.getAll();
    }

    @Override
    public IReportExecutionObject getById(Long id) {
	return rptExecDao.getById(id);
    }


}
