/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 76832 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/base/ReportRunMode.java $
 */
package com.atraxo.reportcenter.engine.birt.model.base;

import javax.xml.bind.annotation.XmlAttribute;

public class ReportRunMode {

    private boolean later;
    private boolean showWin = true;

    public ReportRunMode() {
    }

    @XmlAttribute(name = "later", required = false)
    public boolean getLater() {
	return later;
    }

    public void setLater(boolean later) {
	this.later = later;
    }

    @XmlAttribute(name = "show", required = false)
    public boolean getShowWin() {
	return showWin;
    }

    public void setShowWin(boolean showWin) {
	this.showWin = showWin;
    }

}
