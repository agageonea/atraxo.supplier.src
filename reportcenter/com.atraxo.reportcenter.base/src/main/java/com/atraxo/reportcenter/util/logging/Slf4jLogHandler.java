package com.atraxo.reportcenter.util.logging;

import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Slf4jLogHandler extends Handler {
    private Formatter julFormatter = new SimpleFormatter();

    @Override
    public void publish(LogRecord record) {
	if (record == null) {
	    return;
	}
	ClassLoader hanlderClassLoader = this.getClass().getClassLoader();
	ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
	if (hanlderClassLoader != contextClassLoader) {
	    // do not log in foreign contexts
	    /*
	     * This check is necessary if several web applications with
	     * "JUL to SLF4J" bridge are deployed in the same Servlet container.
	     * Each application has its own instance of SLF4J logger, but they
	     * all are mapped to the same JUL logger, because the JUL logger is
	     * loaded by the root classloader. Whereas SLF4J loggers are loaded
	     * by their respective webapp classloaders. Thus comparing
	     * classloaders is the only known way to find out whom the JUL log
	     * record belongs to.
	     */
	    return;
	}
	String loggerName = record.getLoggerName();
	if (loggerName == null) {
	    loggerName = "unknown";
	}
	Logger slf4jLogger = LoggerFactory.getLogger(loggerName);
	/*
	 * JUL levels in descending order are: <ul> <li>SEVERE (highest value)
	 * <li>WARNING <li>INFO <li>CONFIG <li>FINE <li>FINER <li>FINEST (lowest
	 * value) </ul>
	 */
	if (record.getLevel().intValue() <= Level.FINEST.intValue()) {
	    if (slf4jLogger.isTraceEnabled()) {
		slf4jLogger.trace(julFormatter.format(record), record.getThrown());
	    }
	} else if (record.getLevel().intValue() <= Level.FINE.intValue()) {
	    if (slf4jLogger.isDebugEnabled()) {
		slf4jLogger.debug(julFormatter.format(record), record.getThrown());
	    }
	} else if (record.getLevel().intValue() <= Level.INFO.intValue()) {
	    if (slf4jLogger.isInfoEnabled()) {
		slf4jLogger.info(julFormatter.format(record), record.getThrown());
	    }
	} else if (record.getLevel().intValue() <= Level.WARNING.intValue()) {
	    if (slf4jLogger.isWarnEnabled()) {
		slf4jLogger.warn(julFormatter.format(record), record.getThrown());
	    }
	} else if (record.getLevel().intValue() <= Level.SEVERE.intValue()) {
	    if (slf4jLogger.isErrorEnabled()) {
		slf4jLogger.error(julFormatter.format(record), record.getThrown());
	    }
	} else if (record.getLevel().intValue() == Level.OFF.intValue()) {
	    // logger is switched off
	} else {
	    slf4jLogger.warn("Unexpected log level {}.", record.getLevel().intValue());
	    if (slf4jLogger.isErrorEnabled()) {
		slf4jLogger.error(julFormatter.format(record), record.getThrown());
	    }
	}
    }

    @Override
    public void flush() {
	// noop
    }

    @Override
    public void close() throws SecurityException {
	// noop
    }
}