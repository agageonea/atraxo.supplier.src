/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 77121 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/ReportSectionUrl.java $
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;

public class ReportSectionUrl {

    private ReportSectionUrlType urlType = ReportSectionUrlType.REMOTE;

    private String content;
    private String localXmlFile;

    @XmlAttribute(name = "type", required = false)
    public ReportSectionUrlType getUrlType() {
	return urlType;
    }

    @XmlValue
    public String getContent() {
	return content;
    }

    @XmlTransient
    public String getLocalXmlFile() {
	return localXmlFile;
    }

    public void setLocalXmlFile(String localXmlFile) {
	this.localXmlFile = localXmlFile;
    }

    public void setContent(String content) {
	this.content = content;
    }

    public void setUrlType(ReportSectionUrlType urlType) {
	this.urlType = urlType;
    }

}
