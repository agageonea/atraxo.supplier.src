/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 76832 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/util/DateUtils.java $
 */
package com.atraxo.reportcenter.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    public String dateAsString(Date date, String pattern) {
	SimpleDateFormat fmt = new SimpleDateFormat(pattern);
	return fmt.format(date);
    }
}
