package com.atraxo.reportcenter.reportexec;

import java.util.Date;

public interface IReportExecutionObject {

    public Date getEndDt();
    public String getErrMsg();
    public Long getExecId();
    public String getFilename();
    public String getName();
    public Date getStartDt();
    public String getStatus();
    public String getTitle();
    public String getUrl();
    public String getUser();

    public void setEndDt(Date value);
    public void setErrMsg(String value);
    public void setExecId(Long value);
    public void setFilename(String value);
    public void setName(String value);
    public void setStartDt(Date value);
    public void setStatus(String value);
    public void setTitle(String value);
    public void setUrl(String value);
    public void setUser(String value);

    public boolean isSuccess();

    public String getLocalResultFile();
    public void setLocalResultFile(String fileName);
}
