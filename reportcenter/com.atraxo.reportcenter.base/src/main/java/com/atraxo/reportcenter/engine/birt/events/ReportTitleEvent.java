package com.atraxo.reportcenter.engine.birt.events;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.element.IDynamicText;
import org.eclipse.birt.report.engine.api.script.eventadapter.DynamicTextEventAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.atraxo.reportcenter.engine.birt.BirtParamNames;
import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;
import com.atraxo.reportcenter.util.IReportUtils;

public class ReportTitleEvent extends DynamicTextEventAdapter {

    private static final Logger logger = LoggerFactory.getLogger(ReportTitleEvent.class);

    @Override
    public void onPrepare(IDynamicText textData, IReportContext reportContext) throws ScriptException {

	ApplicationContext context = (ApplicationContext) reportContext.getAppContext().get(BirtParamNames.FP_CONTEXT);
	BaseReport report = (BaseReport) reportContext.getAppContext().get(BirtParamNames.FP_REPORT_OBJ);

	IReportUtils rptUtils = null;
	try {
	    rptUtils = context.getBean(IReportUtils.class);
	} catch (Exception e) {
	}

	String expr = "'No title';";
	if (rptUtils != null) {
	    String result = rptUtils.getReportTitle(report, reportContext);

	    if (result == null) {
		expr = expr + "var customTitle = " + textData.getReport().getNamedExpression("CUSTOM_REPORT_TITLE") + ";\n";
		expr = expr + "var fpTitle     = 'No title';\n";
		expr = expr + "var result = (customTitle != null) ? customTitle : fpTitle;\n";
		expr = expr + "result;";
	    }
	    else {
		expr = expr + "var customTitle = " + textData.getReport().getNamedExpression("CUSTOM_REPORT_TITLE") + ";\n";
		expr = expr + "var fpTitle     = '" + result + "';\n";
		expr = expr + "var result = (customTitle != null) ? customTitle : fpTitle;\n";
		expr = expr + "result;";
	    }
	}

	textData.setValueExpr(expr);

    }
}
