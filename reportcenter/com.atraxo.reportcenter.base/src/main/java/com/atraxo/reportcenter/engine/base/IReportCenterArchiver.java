package com.atraxo.reportcenter.engine.base;


public interface IReportCenterArchiver {

    public double addFile(String fileName) throws ReportCenterException;
}
