/**
 * Copyright (C) FuelPlus GmbH 
 * $Revision: 76832 $  
 * $Date:   11/16/2005 11:35:09 AM$ 
 * $Author: fc $  
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/IDynamicReportData.java $  
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic;

public interface IDynamicReportData {

    public String getDataType();

    public String getStyleName();

    public String getFormat();

    public void setDataType(String dataType);

    public void setStyleName(String styleName);

    public void setFormat(String format);

    public String getBirtDataType();

    public String getBirtValue();

}
