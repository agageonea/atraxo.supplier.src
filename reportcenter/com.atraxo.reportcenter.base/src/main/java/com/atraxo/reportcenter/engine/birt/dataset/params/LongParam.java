package com.atraxo.reportcenter.engine.birt.dataset.params;

import java.sql.SQLException;
import java.sql.Types;

import com.atraxo.reportcenter.engine.birt.dataset.NamedParameterStatement;


public class LongParam extends AbstractReportParam<Long> {

    public LongParam(String paramName, Long paramValue) {
	super(paramName, paramValue);
    }

    @Override
    public void setStatementParam(NamedParameterStatement statement) throws SQLException {
	if (getValue() == null)
	    statement.setNull(getName(), Types.BIGINT);
	else
	    statement.setLong(getName(), getValue());
    }
}
