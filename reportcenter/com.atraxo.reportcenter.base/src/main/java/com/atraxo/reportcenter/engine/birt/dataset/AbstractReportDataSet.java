package com.atraxo.reportcenter.engine.birt.dataset;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.atraxo.reportcenter.engine.birt.dataset.params.AbstractReportParam;

@SuppressWarnings("rawtypes")
public abstract class AbstractReportDataSet<T extends IReportDataSetRow> implements IReportDataSet {

    private NamedParameterStatement statement;
    private ResultSet resultSet;
    private Logger logger;
    private org.apache.log4j.Logger log4jLogger = org.apache.log4j.Logger.getLogger(AbstractReportDataSet.class);
    private HashMap<String, Object> params = new HashMap<String, Object>();
    private List<AbstractReportParam> paramList = new ArrayList<AbstractReportParam>();
    private Connection conn;

    @Override
    public void open(Connection conn) throws SQLException {
	this.conn = conn;
	logDebug("Execute SQL");
	logDebug(getSQL());
	statement = new NamedParameterStatement(conn, getSQL());
	if (params != null) {
	    Iterator<AbstractReportParam> it = paramList.iterator();
	    while (it.hasNext()) {
		AbstractReportParam p = it.next();
		logDebug("Param " + p.getName() + " = " + p.getValue());
		p.setStatementParam(statement);
	    }

	}
	resultSet = statement.executeQuery();
    }

    @SuppressWarnings("unchecked")
    public T next() throws SQLException {
	if (resultSet != null) {
	    if (resultSet.next()) {
		T obj = (T) resultSetToObject(resultSet);
		logDebug(obj.asLogString());
		return obj;
	    }
	}

	return null;
    }

    @Override
    public void close() throws SQLException {
    	this.close(false);
    }

    public void close(boolean keepDbConOpen) throws SQLException {
	if (resultSet != null) {
	    logDebug("Close resultSet");
	    resultSet.close();
	    resultSet = null;
	}

	if (statement != null) {
	    logDebug("Close statement");
	    statement.close();
	    statement = null;
	}

	if ( (!keepDbConOpen) && (conn != null) && (! conn.isClosed())) {
	    logDebug("Close connection");
	    conn.close();
	    conn = null;
	}
    }

    public void setLogger(Logger logger) {
	this.logger = logger;
    }

    public void setLog4jLogger(org.apache.log4j.Logger logger) {
	this.log4jLogger = logger;
    }

    private void logDebug(String msg) {
	if (logger != null)
	    logger.log(Level.FINEST, msg);
	if (log4jLogger != null)
	    log4jLogger.debug(msg);
    }

    @Override
    public Collection<AbstractReportParam> getParamList() {
	return this.paramList;
    }

    @Override
    public void addParam(AbstractReportParam param) {
	paramList.add(param);
    }

    @Override
    public void removeParam(AbstractReportParam param) {
	paramList.remove(param);
    }

    @Override
    public void removeParam(String paramName) {
	if (paramName == null)
	    return;
	if ("".equals(paramName.trim()))
	    return;
	int idx = getParamIdx(paramName);
	if (idx >= 0) {
	    paramList.remove(idx);
	}
    }

    @Override
    public AbstractReportParam getParam(String paramName) {
	int idx = getParamIdx(paramName);
	if (idx >= 0)
	    return paramList.get(idx);
	return null;
    }

    private int getParamIdx(String paramName) {
	for (int i=0; i < paramList.size(); i++) {
	    if (paramName.equals(paramList.get(i).getName()))
		return i;
	}

	return -1;
    }
}
