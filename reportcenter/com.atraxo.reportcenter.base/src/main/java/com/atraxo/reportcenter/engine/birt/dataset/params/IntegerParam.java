package com.atraxo.reportcenter.engine.birt.dataset.params;

import java.sql.SQLException;
import java.sql.Types;

import com.atraxo.reportcenter.engine.birt.dataset.NamedParameterStatement;

public class IntegerParam extends AbstractReportParam<Integer> {

    public IntegerParam(String paramName, Integer paramValue) {
	super(paramName, paramValue);
    }

    @Override
    public void setStatementParam(NamedParameterStatement statement) throws SQLException {
	if (getValue() == null)
	    statement.setNull(getName(), Types.INTEGER);
	else
	    statement.setInt(getName(), getValue());
    }

}
