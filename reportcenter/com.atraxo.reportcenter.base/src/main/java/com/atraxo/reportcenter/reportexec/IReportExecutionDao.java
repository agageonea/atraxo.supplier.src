package com.atraxo.reportcenter.reportexec;

import java.util.List;

public interface IReportExecutionDao<T> {


    public List<IReportExecutionObject> getAll();
    public IReportExecutionObject getById(T id);
    public T insert(IReportExecutionObject rptExec) throws Exception;
    public int update(IReportExecutionObject rptExec);
    public int delete(T id);
}
