/**
 * Copyright (C) FuelPlus GmbH 
 * $Revision: 76832 $  
 * $Date:   11/16/2005 11:35:09 AM$ 
 * $Author: fc $  
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/base/ReportParamType.java $  
 */
package com.atraxo.reportcenter.engine.birt.model.base;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum ReportParamType {

    @XmlEnumValue("string")
    STRING("string"), @XmlEnumValue("date")
    DATE("date"), @XmlEnumValue("integer")
    INTEGER("integer"), @XmlEnumValue("number")
    NUMBER("number"),

    @XmlEnumValue("sqlalphanum")
    SQL_ALPHANUM("sqlalphanum"), @XmlEnumValue("sqldate")
    SQL_DATE("sqldate"), @XmlEnumValue("sqlboolean")
    SQL_BOOLEAN("sqlboolean"), @XmlEnumValue("sqlnumeric")
    SQL_NUMERIC("sqlnumeric")

    ;

    private final String value;

    ReportParamType(String v) {
	this.value = v;
    }

    public static ReportParamType fromValue(String v) {
	for (ReportParamType rt : ReportParamType.values()) {
	    if (rt.value.equals(v)) {
		return rt;
	    }
	}
	throw new IllegalArgumentException(v);
    }
}
