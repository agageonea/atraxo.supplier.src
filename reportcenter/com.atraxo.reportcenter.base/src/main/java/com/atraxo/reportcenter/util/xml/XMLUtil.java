/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 77121 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/util/xml/XMLUtil.java $
 */
package com.atraxo.reportcenter.util.xml;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atraxo.reportcenter.engine.birt.model.dynamic.DynamicReportData;

public class XMLUtil {
    
    private static final Logger LOG = LoggerFactory.getLogger(XMLUtil.class);
    
    private DecimalFormat df;
    private SimpleDateFormat sdf;

    private XMLUtil() {
	df = new DecimalFormat("##################.#########");
	sdf = new SimpleDateFormat("yyyy-MM-dd");
    }

    private static class LazyHolder {
	private static final XMLUtil INSTANCE = new XMLUtil();
    }

    public static XMLUtil getInstance() {
	return LazyHolder.INSTANCE;
    }

    public String format(String value) {
	if (value == null)
	    return "";
	return value;
    }

    public String format(Double value) {
	if (value == null)
	    return "";
	return df.format(value);
    }

    public String format(java.sql.Date value) {
	if (value == null)
	    return "";
	return sdf.format(value);
    }

    public String format(BigDecimal value) {
	return df.format(value);
    }

    public void writeReportDataXml(DynamicReportData rptData, String xmlFileName) {
	JAXBContext context;
	try {
	    // RecordTypeAdapter ad = new
	    // RecordTypeAdapter(rptData.getColumns());
	    BufferedWriter writer = new BufferedWriter(new FileWriter(xmlFileName));
	    context = JAXBContext.newInstance(DynamicReportData.class);
	    Marshaller m = context.createMarshaller();
	    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
	    m.marshal(rptData, writer);
	    writer.flush();
	    writer.close();
	} catch (JAXBException e) {
	    LOG.error(e.getMessage(), e);
	} catch (IOException e) {
	    LOG.error(e.getMessage(), e);
	}
    }

    public String replaceHtmlTags(String value) {
	value = value.replace("<", "&lt;");
	value = value.replace(">", "&gt;");
	value = value.replace("&", "&amp;");

	return value;
    }
}
