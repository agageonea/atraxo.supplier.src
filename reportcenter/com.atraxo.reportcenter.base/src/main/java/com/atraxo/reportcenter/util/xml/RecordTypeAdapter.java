package com.atraxo.reportcenter.util.xml;

import java.util.List;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.atraxo.reportcenter.engine.birt.model.dynamic.DynamicReportRecord;

public class RecordTypeAdapter extends
		XmlAdapter<Object, List<DynamicReportRecord>> {

	public RecordTypeAdapter() {
		super();
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unused")
	@Override
	public List<DynamicReportRecord> unmarshal(Object v) throws Exception {
		Element allRec = (Element) v;
		NodeList recs = allRec.getChildNodes();
		//List<DynamicReportRecord> result = new ArrayList<DynamicReportRecord>();
		return null;
	}

	@Override
	public Object marshal(List<DynamicReportRecord> v) throws Exception {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();

		Element allRec = doc.createElement("records");
		for (DynamicReportRecord r : v) {
			Element recEl = doc.createElement("record");
			if (r.getType() != null) {
				recEl.setAttribute("type", r.getType());
			}
			for (String colName : r.getData().keySet()) {
				String val = r.getColumn(colName);
				Element colEl = doc.createElement(colName);
				if (val != null) {
					colEl.appendChild(doc.createTextNode(r.getColumn(colName)
							.toString()));

					String colFmt = r.getColumnFormat(colName);
					if (colFmt != null) {
						colEl.setAttribute("fmt", colFmt);
					}
				}
				recEl.appendChild(colEl);
			}
			allRec.appendChild(recEl);
		}

		return allRec;
	}

}
