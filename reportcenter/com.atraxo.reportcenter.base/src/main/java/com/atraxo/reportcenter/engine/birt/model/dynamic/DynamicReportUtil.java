package com.atraxo.reportcenter.engine.birt.model.dynamic;

import java.util.List;

/**
 * Copyright (C) FuelPlus GmbH $Revision: 76832 $ $Date: 11/16/2005 11:35:09 AM$
 * $Author: fc $ $URL:
 * http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportServer
 * /src/de/fuelplus/reportserver/service/model/dynamic/DynamicReportUtil.java $
 */
public class DynamicReportUtil {

    public static final String DATA_TYPE_STRING = "string";
    public static final String DATA_TYPE_NUMBER = "number";
    public static final String DATA_TYPE_DATE = "date";
    public static final String DATA_TYPE_DATETIME = "datetime";

    public static final String ALIGN_LEFT = "left";
    public static final String ALIGN_RIGHT = "right";
    public static final String ALIGN_CENTER = "center";

    private DynamicReportUtil() {

    }

    private static class LazyHolder {
	private static final DynamicReportUtil INSTANCE = new DynamicReportUtil();
    }

    public static DynamicReportUtil getInstance() {
	return LazyHolder.INSTANCE;
    }

    public String[] columnListAsArray(List<DynamicReportColumn> list) {
	if (list == null)
	    return null;

	String[] result = new String[list.size()];

	for (int i = 0; i < list.size(); i++) {
	    result[i] = list.get(i).getCode();
	}

	return result;
    }

    public String fixHTMLChars(String value) {
	if (value != null) {
	    value = value.replace("&lt;", "<");
	    value = value.replace("&gt;", ">");
	    value = value.replace("&amp;", "&");
	}

	return value;
    }

    public String fixNewLine(String value) {
	if (value != null) {
	    value = value.replaceAll("</{0,1}\\s*br\\s*/{0,1}>", "\n");
	}
	return value;
    }
}
