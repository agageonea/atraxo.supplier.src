package com.atraxo.reportcenter.config;

public class ReportCenterConstants {

    public static final String CONFIG_DEV_MODE = "devMode";

    public static final String CONFIG_SMTP_HOST = "mail.smtp.host";
    public static final String CONFIG_SMTP_PORT = "mail.smtp.port";
    public static final String CONFIG_SMTP_FROM = "mail.from";
    public static final String CONFIG_SMTP_USER = "mail.user";
    public static final String CONFIG_SMTP_PASS = "mail.pwd";
    public static final String CONFIG_SMTP_SSL = "mail.SSL"; // useSSLOnConnect
    public static final String CONFIG_SMTP_TLS = "mail.TLS"; // startTlsEnabled

    public static final String CONFIG_FTP_HOST = "ftp.server.host";
    public static final String CONFIG_FTP_USER = "ftp.user";
    public static final String CONFIG_FTP_PASS = "ftp.pwd";
    public static final String CONFIG_FTP_PORT = "ftp.port";
    public static final String CONFIG_FTP_PROTOCOL = "ftp.protocol";
    public static final String CONFIG_FTP_REMOTE_FOLDER = "ftp.remote.folder";

    public static final String CONFIG_REPORTS_PATH = "path.reports";
    public static final String CONFIG_REPORTS_LIBRARY_PATH = "path.reports.libraries";
    public static final String CONFIG_REPORTS_IMAGES_PATH = "path.reports.images";
    public static final String CONFIG_REPORTS_USER_PATH = "path.reports.user";
    public static final String CONFIG_TEMP_PATH = "path.temp";

    public static final String CONFIG_GMT_TIMESTAMP_DIF = "gmtTimestampDif";

    public static final String CONFIG_DB_URL = "db.url";
    public static final String CONFIG_DB_DRV = "db.driver";
    public static final String CONFIG_DB_USER = "db.user";
    public static final String CONFIG_DB_PASS = "db.pass";
}
