/**
 * Copyright (C) FuelPlus GmbH 
 * $Revision: 76832 $  
 * $Date:   11/16/2005 11:35:09 AM$ 
 * $Author: fc $  
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/ReportSectionLayoutType.java $  
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum ReportSectionLayoutType {

    @XmlEnumValue("OnRows")
    ON_ROWS("OnRows"), @XmlEnumValue("OnColumns")
    ON_COLUMNS("OnColumns");

    private final String value;

    ReportSectionLayoutType(String v) {
	this.value = v;
    }

    public static ReportSectionLayoutType fromValue(String v) {
	for (ReportSectionLayoutType rt : ReportSectionLayoutType.values()) {
	    if (rt.value.equals(v)) {
		return rt;
	    }
	}
	throw new IllegalArgumentException(v);
    }
}
