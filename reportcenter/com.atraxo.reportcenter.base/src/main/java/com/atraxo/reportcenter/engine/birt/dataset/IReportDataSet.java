package com.atraxo.reportcenter.engine.birt.dataset;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import com.atraxo.reportcenter.engine.birt.dataset.params.AbstractReportParam;

@SuppressWarnings("rawtypes")
public interface IReportDataSet<T extends IReportDataSetRow> {

	public String getSQL();
	public void setSQL(String sql);
	public void open(Connection conn) throws SQLException;
	public void close() throws SQLException;
	public T resultSetToObject(ResultSet rs) throws SQLException;
	public T next() throws SQLException;

	public Collection<AbstractReportParam> getParamList();
	public void addParam(AbstractReportParam param);
	public void removeParam(AbstractReportParam param);
	public void removeParam(String paramName);
	public AbstractReportParam getParam(String paramName);
}
