package com.atraxo.reportcenter.engine.birt.model.dynamic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.atraxo.reportcenter.engine.birt.model.base.ReportPageOrientation;
import com.atraxo.reportcenter.util.xml.RecordTypeAdapter;
import com.atraxo.reportcenter.util.xml.XMLDateAdapter;

@XmlRootElement(name = "reportData")
public class DynamicReportData {

    private String code;
    private ReportPageOrientation layout;
    private String title;
    private String user;
    private Date generatedOn;
    private List<DynamicReportQueryParam> params = new ArrayList<DynamicReportQueryParam>();
    private List<DynamicReportColumn> columns = new ArrayList<DynamicReportColumn>();
    private List<DynamicReportRecord> records = new ArrayList<DynamicReportRecord>();

    @XmlAttribute(name = "code")
    public String getCode() {
	return code;
    }

    public void setCode(String code) {
	this.code = code;
    }

    @XmlAttribute(name = "layout")
    public ReportPageOrientation getLayout() {
	return layout;
    }

    public void setLayout(ReportPageOrientation layout) {
	this.layout = layout;
    }

    @XmlAttribute(name = "title")
    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    @XmlAttribute(name = "by")
    public String getUser() {
	return user;
    }

    public void setUser(String user) {
	this.user = user;
    }

    @XmlAttribute(name = "on")
    @XmlJavaTypeAdapter(XMLDateAdapter.class)
    public Date getGeneratedOn() {
	return generatedOn;
    }

    public void setGeneratedOn(Date generatedOn) {
	this.generatedOn = generatedOn;
    }

    @XmlElementWrapper(name = "queryParams")
    @XmlElement(name = "queryParam")
    public List<DynamicReportQueryParam> getParams() {
	return params;
    }

    public void setParams(List<DynamicReportQueryParam> params) {
	this.params = params;
    }

    @XmlElementWrapper(name = "columnDef", required = true)
    @XmlElement(name = "column")
    public List<DynamicReportColumn> getColumns() {
	return columns;
    }

    public void setColumns(List<DynamicReportColumn> columns) {
	this.columns = columns;
    }

    @XmlElement(name = "records")
    @XmlJavaTypeAdapter(RecordTypeAdapter.class)
    public List<DynamicReportRecord> getRecords() {
	return records;
    }

    public void setRecords(List<DynamicReportRecord> records) {
	this.records = records;
    }

}
