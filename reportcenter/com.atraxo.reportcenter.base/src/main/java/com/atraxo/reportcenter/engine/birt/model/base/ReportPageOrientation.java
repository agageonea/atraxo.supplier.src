/**
 * Copyright (C) FuelPlus GmbH 
 * $Revision: 76832 $  
 * $Date:   11/16/2005 11:35:09 AM$ 
 * $Author: fc $  
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/base/ReportPageOrientation.java $  
 */
package com.atraxo.reportcenter.engine.birt.model.base;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum ReportPageOrientation {

    @XmlEnumValue("portrait")
    PORTRAIT("portrait"), @XmlEnumValue("landscape")
    LANDSCAPE("landscape");

    private final String value;

    ReportPageOrientation(String v) {
	this.value = v;
    }

    public static ReportPageOrientation fromValue(String v) {
	for (ReportPageOrientation rt : ReportPageOrientation.values()) {
	    if (rt.value.equals(v.toLowerCase())) {
		return rt;
	    }
	}
	throw new IllegalArgumentException(v);
    }
}
