/**
 * Copyright (C) FuelPlus GmbH $Revision: 77157 $ $Date: 2015-04-30 15:29:59 +0300 (Thu, 30 Apr 2015) $Author: fc $ $URL:
 * http://brafs01.haj.lan/svn/fps-devel/trunk/product5
 * /src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/util/FileUtils.java $
 */
package com.atraxo.reportcenter.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.atraxo.reportcenter.config.ReportCenterConfig;
import com.atraxo.reportcenter.engine.base.ReportCenterException;

public class FileUtils {

	private static final Logger LOG = LoggerFactory.getLogger(FileUtils.class);

	@Autowired
	private ReportCenterConfig config;

	public void downloadXml(String remoteFile, String localFile, String serverId) throws ReportCenterException {
		// if remote file is on local machine on temp path
		if ((!remoteFile.toLowerCase().startsWith("http")) && (!remoteFile.toLowerCase().startsWith("file:"))) {
			String tempPath = this.config.getTempPath();
			File rFile = new File(tempPath + "/" + remoteFile);
			if (!rFile.exists()) {
				throw new ReportCenterException(new Exception("Invalid temporary path configuration! Please contact your administrator!"));
			}

			File tmpFile = new File(localFile + ".tmp");
			if (rFile.renameTo(tmpFile)) {
				File lFile = new File(localFile);
				tmpFile.renameTo(lFile);
			}

		} else {
			BufferedWriter os = null;
			BufferedReader is = null;

			try {
				File outFile = new File(localFile);
				if (outFile.exists()) {
					outFile.delete();
				}
				outFile.createNewFile();

				os = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "UTF-8"));
				// os.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>".getBytes());

				URL url = new URL(remoteFile);

				if (remoteFile.toLowerCase().startsWith("https:")) {
					HttpsURLConnection httpsConn = (HttpsURLConnection) url.openConnection();
					if (serverId != null) {
						httpsConn.setRequestProperty("Cookie", "SERVERID=" + serverId);
					}

					if (this.config.isDevMode()) {
						httpsConn.setHostnameVerifier(new HostnameVerifier() {

							@Override
							public boolean verify(String hostname, SSLSession session) {
								return true;
							}
						});
					}

					is = new BufferedReader(new InputStreamReader(httpsConn.getInputStream(), "UTF-8"));
				} else {
					URLConnection httpConn = url.openConnection();
					if (serverId != null) {
						httpConn.setRequestProperty("Cookie", "SERVERID=" + serverId);
					}

					is = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), "UTF-8"));
				}

				int c = 0;
				while ((c = is.read()) != -1) {
					os.write(c);
				}

			} catch (MalformedURLException e) {
				LOG.debug(e.getMessage(), e);
				throw new ReportCenterException(e);
			} catch (IOException e) {
				LOG.debug(e.getMessage(), e);
				throw new ReportCenterException(e);
			} finally {
				try {
					if (os != null) {
						os.flush();
						os.close();
					}
					if (is != null) {
						is.close();
					}
				} catch (IOException e) {
					LOG.debug(e.getMessage(), e);
					throw new ReportCenterException(e);
				}
			}
		}
	}

	public void removeFolderRecursive(File file) throws IOException {
		if (file == null) {
			return;
		}

		if (file.isDirectory()) {

			// directory is empty, then delete it
			String[] files = file.list();
			if (files != null) {
				if (files.length == 0) {
					file.delete();
				} else {
					// list all the directory contents
					files = file.list();

					if (files != null) {
						for (String temp : files) {
							// construct the file structure
							File fileDelete = new File(file, temp);

							// recursive delete
							this.removeFolderRecursive(fileDelete);
						}

						// check the directory again, if empty then delete it
						if (files.length == 0) {
							file.delete();
						}
					}

				}
			}
		} else {
			// if file, then delete it
			file.delete();
		}
	}

	public List<File> listFolder(String path) {
		return this.listFolder(path, null);
	}

	public List<File> listFolder(String path, final String fileNameFilter) {
		File[] tmpFiles = null;

		List<File> result = new ArrayList<File>();
		File folder = new File(path);
		if (folder.exists() && folder.isDirectory()) {
			if (fileNameFilter != null) {
				FilenameFilter filter = new FilenameFilter() {

					@Override
					public boolean accept(File dir, String name) {
						if (name.contains(fileNameFilter)) {
							return true;
						}
						return false;
					}
				};
				tmpFiles = folder.listFiles(filter);
				if (tmpFiles != null) {
					result.addAll(Arrays.asList(tmpFiles));
				}
			} else {
				tmpFiles = folder.listFiles();
				if (tmpFiles != null) {
					result.addAll(Arrays.asList(tmpFiles));
				}
			}

		}
		return result;
	}

	public List<File> listFilesBeforeDate(String path, Date aDt) {
		List<File> result = new ArrayList<File>();

		File folder = new File(path);
		File[] listOfFiles = folder.listFiles(new FileFilter() {

			@Override
			public boolean accept(File file) {
				return (file.isFile());
			}
		});

		if (listOfFiles != null) {
			for (File f : listOfFiles) {
				Date dt = new Date(f.lastModified());
				if (dt.before(aDt)) {
					result.add(f);
				}
			}
		}

		return result;
	}

	public void deleteFiles(List<File> filesToDel) {
		for (File f : filesToDel) {
			if (f.exists() && f.isFile()) {
				LOG.debug("Delete file : " + f.getName());
				f.delete();
			}
		}
	}

	public void createXmlFile(String fileName, String content) {

		try {
			Files.write(Paths.get(fileName), content.getBytes(), StandardOpenOption.CREATE);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}
	}
}
