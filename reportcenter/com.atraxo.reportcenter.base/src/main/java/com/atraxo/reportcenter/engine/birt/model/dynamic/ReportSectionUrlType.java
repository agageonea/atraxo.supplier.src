package com.atraxo.reportcenter.engine.birt.model.dynamic;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum ReportSectionUrlType {

    //@formatter:off
    @XmlEnumValue("local")
    LOCAL("local"), 
    @XmlEnumValue("remote")
    REMOTE("remote"),
    @XmlEnumValue("inline")
    INLINE("inline")    
    //@formatter:on
    ;

    private final String value;

    ReportSectionUrlType(String v) {
	this.value = v;
    }

    public static ReportSectionUrlType fromValue(String v) {
	for (ReportSectionUrlType rt : ReportSectionUrlType.values()) {
	    if (rt.value.equals(v)) {
		return rt;
	    }
	}
	throw new IllegalArgumentException(v);
    }
}
