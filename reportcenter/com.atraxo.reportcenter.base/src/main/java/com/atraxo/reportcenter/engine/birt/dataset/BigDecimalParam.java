package com.atraxo.reportcenter.engine.birt.dataset;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Types;

import com.atraxo.reportcenter.engine.birt.dataset.params.AbstractReportParam;

public class BigDecimalParam extends AbstractReportParam<BigDecimal> {

    public BigDecimalParam(String paramName, BigDecimal paramValue) {
	super(paramName, paramValue);
    }

    @Override
    public void setStatementParam(NamedParameterStatement statement) throws SQLException {
	if (getValue() == null)
	    statement.setNull(getName(), Types.NUMERIC);
	else
	    statement.setBigDecimal(getName(), getValue());
    }

}
