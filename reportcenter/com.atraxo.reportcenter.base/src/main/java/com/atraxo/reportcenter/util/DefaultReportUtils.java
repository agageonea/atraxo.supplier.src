package com.atraxo.reportcenter.util;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.model.api.elements.DesignChoiceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.atraxo.reportcenter.config.ReportCenterConfig;
import com.atraxo.reportcenter.engine.base.ReportCenterException;
import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;
import com.atraxo.reportcenter.engine.birt.model.dynamic.XMLDataType;

import de.fuelplus.crypt.FpsCrypt;

public class DefaultReportUtils implements IReportUtils {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultReportUtils.class);
    
    @Autowired
    private ReportCenterConfig config;

    @Autowired
    private DataSource dataSource;

    public String[] splitUserPass(String userAndPass) throws ReportCenterException {
	if (userAndPass == null)
	    return null;
	String[] userWithPass = userAndPass.split(":");
	return userWithPass;
    }

    public String getDatabaseURL() {
	return config.getDbUrl();
    }

    public String decodePassword(String encodedPassword) {
	String result = FpsCrypt.decryptToken(encodedPassword);
	if ((result == null) || ("".equals(result))) {
	    throw new ReportCenterException("Invalid username and/or password");
	}
	return result;
    }

    public Connection getConnection(String jdbcDbUrl, String jdbcUsername, String jdbcPassword) throws ReportCenterException {
	try {
	    return dataSource.getConnection();
	}
	catch (Exception e) {
	    throw new ReportCenterException(e);
	}
    }

    public String internalDataType2BirtDataType(String dataType) {
	if (XMLDataType.NUMBER.equals(dataType))
	    return DesignChoiceConstants.COLUMN_DATA_TYPE_DECIMAL;
	else if (XMLDataType.INTEGER.equals(dataType))
	    return DesignChoiceConstants.COLUMN_DATA_TYPE_INTEGER;
	else if (XMLDataType.DATE.equals(dataType))
	    return DesignChoiceConstants.COLUMN_DATA_TYPE_DATETIME;
	else if (XMLDataType.DATETIME.equals(dataType))
	    return DesignChoiceConstants.COLUMN_DATA_TYPE_DATETIME;
	else
	    return DesignChoiceConstants.COLUMN_DATA_TYPE_STRING;
    }

    public boolean IsValueDataType(String tmpDataType, String value) {
	if (XMLDataType.NUMBER.equals(tmpDataType))
	    try {
		Double.valueOf(value);
		return true;
	    } catch (Exception e) {
		return false;
	    }
	else if (XMLDataType.INTEGER.equals(tmpDataType))
	    try {
		Integer.valueOf(value);
		return true;
	    } catch (Exception e) {
		return false;
	    }
	else if (XMLDataType.DATE.equals(tmpDataType))
	    try {
		sdfIn.parse(value);
		return true;
	    } catch (Exception e) {
		return false;
	    }
	else if (XMLDataType.DATETIME.equals(tmpDataType)) {
	    try {
		sdfIn.parse(value);
		return true;
	    } catch (Exception e) {
		return false;
	    }
	} else if (XMLDataType.STRING.equals(tmpDataType)) {
	    return true;
	}

	return false;
    }

    private SimpleDateFormat sdfIn = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat sdfOut = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    public String getBirtValue(String internalDataType, String stringValue) {
	if (XMLDataType.NUMBER.equals(internalDataType))
	    return stringValue;
	else if (XMLDataType.INTEGER.equals(internalDataType))
	    return stringValue;
	else if (XMLDataType.DATE.equals(internalDataType))
	    return "'" + stringValue + "'";
	else if (XMLDataType.DATETIME.equals(internalDataType)) {
	    java.util.Date inDt;
	    try {
		inDt = sdfIn.parse(stringValue);
		String outVal = sdfOut.format(inDt);
		return "'" + outVal + "'";
	    } catch (ParseException e) {
		LOG.debug(e.getMessage(), e);
	    }
	    return null;
	} else
	    return "'" + stringValue + "'";
    }

    @Override
    public String getReportTitle(BaseReport report, IReportContext reportContext) {
	return report.getTitle();
    }

}
