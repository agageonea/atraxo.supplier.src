/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 77121 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/base/ReportParam.java $
 */
package com.atraxo.reportcenter.engine.birt.model.base;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.naming.NamingException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atraxo.reportcenter.engine.base.ReportCenterException;

@XmlAccessorType(XmlAccessType.NONE)
public class ReportParam {
    
    private static final Logger LOG = LoggerFactory.getLogger(ReportParam.class);
    
    private String name;
    private String value;
    private String format;
    private boolean optional = true;
    private ReportParamType type = ReportParamType.STRING;
    private Locale locale;

    @XmlAttribute(name = "name", required = true)
    public String getName() {
	return name;
    }

    @XmlValue
    public String getValue() {
	if ("".equals(value))
	    return null;
	return value;
    }

    @XmlAttribute(name = "format", required = false)
    public String getFormat() {
	return format;
    }

    @XmlAttribute(name = "type", required = false)
    public ReportParamType getType() {
	return type;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setValue(String value) {
	this.value = value;
    }

    public void setFormat(String format) {
	this.format = format;
    }

    public void setType(ReportParamType type) {
	this.type = type;
    }

    public Object getConvValue(BaseReport report) throws ReportCenterException {
	if (getValue() == null) {
	    return null;
	}

	switch (type) {
	case STRING:
	    return getValue();
	case NUMBER:
	    return getNumberValue();
	case INTEGER:
	    return getIntegerValue();
	case DATE:
	    return getDateValue();

	case SQL_ALPHANUM:
	case SQL_BOOLEAN:
	case SQL_DATE:
	case SQL_NUMERIC:
	    return getSqlValueConv(report);
	}
	return null;
    }

    public Object getConvValue(Connection conn) throws ReportCenterException {
	if (getValue() == null) {
	    return null;
	}

	switch (type) {
	case STRING:
	    return getValue();
	case NUMBER:
	    return getNumberValue();
	case INTEGER:
	    return getIntegerValue();
	case DATE:
	    return getDateValue();

	case SQL_ALPHANUM:
	case SQL_BOOLEAN:
	case SQL_DATE:
	case SQL_NUMERIC:
	    return getSqlValueConv(conn);
	}
	return null;
    }

    public String getSqlExpression() {
	switch (type) {
	case SQL_BOOLEAN:
	case SQL_ALPHANUM:
	    if (getValue() == "")
		return "null";
	    else
		return String.format("'%s'", getValue());
	case SQL_NUMERIC:
	    return getValue();
	case SQL_DATE:
	    if ("".equals(getValue()))
		return null;
	    return String.format("to_date('%s', '%s')", getValue(), getFormat());
	}
	return "";
    }

    public Object getSqlValueConv(BaseReport report) throws ReportCenterException {
	Object result = null;
	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
	    conn = report.getConnection();
	    String sql = "select " + getSqlExpression() + " from dual";
	    ps = conn.prepareStatement(sql);
	    rs = ps.executeQuery();
	    while (rs.next()) {
		result = rs.getObject(1);
	    }
	} catch (SQLException e) {
	    throw new ReportCenterException(e);
	} catch (NamingException e) {
	    throw new ReportCenterException(e);
	} finally {
	    if (rs != null)
		try {
		    rs.close();
		} catch (SQLException e) {
		    LOG.error(e.getMessage(), e);
		}
	    if (ps != null)
		try {
		    ps.close();
		} catch (SQLException e) {
		    LOG.error(e.getMessage(), e);
		}
	    if (conn != null)
		try {
		    conn.close();
		} catch (SQLException e) {
		    LOG.error(e.getMessage(), e);
		}
	}
	return result;
    }

    public Object getSqlValueConv(Connection conn) throws ReportCenterException {
	Object result = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
	    String sql = "select " + getSqlExpression() + " from dual";
	    ps = conn.prepareStatement(sql);
	    rs = ps.executeQuery();
	    while (rs.next()) {
		result = rs.getObject(1);
		if (result instanceof Timestamp)
		    result = new Date(((Timestamp) result).getTime());
	    }
	} catch (SQLException e) {
	    throw new ReportCenterException(e);
	} finally {
	    if (rs != null)
		try {
		    rs.close();
		} catch (SQLException e) {
		    LOG.debug(e.getMessage(), e);
		}
	    if (ps != null)
		try {
		    ps.close();
		} catch (SQLException e) {
		    LOG.debug(e.getMessage(), e);
		}
	}
	return result;
    }

    private Object getDateValue() throws ReportCenterException {
	try {
	    if ("".equals(getValue()))
		return null;

	    // SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy",
	    // this.locale);
	    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
	    if (getFormat() != null) {
		// sdf = new SimpleDateFormat(getFormat(), this.locale);
		sdf = new SimpleDateFormat(getFormat());
	    }

	    java.util.Date dt = sdf.parse(getValue());
	    java.sql.Date result = new java.sql.Date(dt.getTime());
	    return result;
	} catch (Exception e) {
	    throw new ReportCenterException(e);
	}
    }

    private Object getIntegerValue() throws ReportCenterException {
	try {
	    if ("".equals(getValue()))
		return null;
	    return Integer.valueOf(getValue());
	} catch (Exception e) {
	    throw new ReportCenterException(e);
	}
    }

    private Object getNumberValue() {
	try {
	    if ("".equals(getValue()))
		return null;
	    return Double.valueOf(getValue());
	} catch (Exception e) {
	    throw new ReportCenterException(e);
	}
    }

    public boolean isOptional() {
	return optional;
    }

    public void setOptional(boolean optional) {
	this.optional = optional;
    }

    public void setLocale(Locale locale) {
	this.locale = locale;
    }

    public Locale getLocale() {
	return locale;
    }

}
