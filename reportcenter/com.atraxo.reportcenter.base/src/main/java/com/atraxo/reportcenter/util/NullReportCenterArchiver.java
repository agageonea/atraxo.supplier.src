package com.atraxo.reportcenter.util;

import com.atraxo.reportcenter.engine.base.IReportCenterArchiver;
import com.atraxo.reportcenter.engine.base.ReportCenterException;

public class NullReportCenterArchiver implements IReportCenterArchiver {

    public double addFile(String fileName) throws ReportCenterException {
	return 0;
    }

}
