/**
 * Copyright (C) FuelPlus GmbH 
 * $Revision: 76832 $  
 * $Date:   11/16/2005 11:35:09 AM$ 
 * $Author: fc $  
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/layout/DynamicDesignRowTableData.java $  
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic.layout;

import java.util.ArrayList;
import java.util.List;

public class DynamicDesignRowTableData {

    private List<DynamicDesignCellTableData> cells = new ArrayList<DynamicDesignCellTableData>();

    public List<DynamicDesignCellTableData> getCells() {
	return cells;
    }

}
