/**
 * Copyright (C) FuelPlus GmbH 
 * $Revision: 76832 $  
 * $Date:   11/16/2005 11:35:09 AM$ 
 * $Author: fc $  
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/base/ReportFormatOptions.java $  
 */
package com.atraxo.reportcenter.engine.birt.model.base;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class ReportFormatOptions {

    private ReportOutputFormat format;
    private boolean showHeader = true;
    private boolean showFooter = true;
    private boolean showPageNumber = true;
    private boolean showParams = true;

    @XmlAttribute(name = "fmt", required = false)
    public ReportOutputFormat getFormat() {
	return format;
    }

    @XmlElement(name = "sHeader", required = false)
    public boolean isShowHeader() {
	return showHeader;
    }

    @XmlElement(name = "sFooter", required = false)
    public boolean isShowFooter() {
	return showFooter;
    }

    @XmlElement(name = "sPgNum", required = false)
    public boolean isShowPageNumber() {
	return showPageNumber;
    }

    @XmlElement(name = "sParams", required = false)
    public boolean isShowParams() {
	return showParams;
    }

    public void setFormat(ReportOutputFormat format) {
	this.format = format;
    }

    public void setShowHeader(boolean showHeader) {
	this.showHeader = showHeader;
    }

    public void setShowFooter(boolean showFooter) {
	this.showFooter = showFooter;
    }

    public void setShowPageNumber(boolean showPageNumber) {
	this.showPageNumber = showPageNumber;
    }

    public void setShowParams(boolean showParams) {
	this.showParams = showParams;
    }

}
