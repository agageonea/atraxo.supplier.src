package com.atraxo.reportcenter.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.atraxo.reportcenter.config.ReportCenterConfig;
import com.atraxo.reportcenter.engine.RunReportResult;
import com.atraxo.reportcenter.engine.base.ReportCenterException;
import com.atraxo.reportcenter.reportexec.IReportExecutionObject;
import com.atraxo.reportcenter.service.IReportExecution;
import com.atraxo.reportcenter.service.ReportCenterService;

@Controller
@RequestMapping(value = "/reports")
public class ReportCenterController {

	private static final Logger LOG = LoggerFactory.getLogger(ReportCenterController.class);

	@Autowired
	private ReportCenterService repCenterService;

	@Autowired
	private IReportExecution<Long> execUpdater;

	@Autowired
	private ReportCenterConfig config;

	@RequestMapping(value = "/run", method = RequestMethod.POST)
	@ResponseBody
	public void runAndDownload(HttpServletResponse response, @RequestParam(value = "_rid", required = false) String rid,
			@RequestParam(value = "_devMode", required = false) boolean devMode, @RequestParam("data") String data) {
		IReportExecutionObject rptExec;
		try {
			rptExec = this.repCenterService.run(rid, "en", data, response.getOutputStream(), false, devMode);
			this.getFileForDownload(response, rptExec);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new ReportCenterException(e);
		}

	}

	/*
	 * Will be implemented for scheduled reports
	 */
	@RequestMapping(value = "/sch", method = RequestMethod.POST, produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public RunReportResult run(HttpServletResponse response, @RequestParam(value = "_rid", required = false) String rid,
			@RequestParam(value = "_devMode", required = false) boolean devMode, @RequestParam("data") String data) {
		RunReportResult result = new RunReportResult();
		try {
			IReportExecutionObject rptExec = this.repCenterService.run(rid, "en", data, response.getOutputStream(), false, devMode);
			result.getReports().add(rptExec);
			result.setSuccess(true);
		} catch (Exception e) {
			result.setSuccess(false);
		}
		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public RunReportResult list() {
		RunReportResult result = new RunReportResult();
		try {
			List<IReportExecutionObject> rptExec = this.repCenterService.listAll();
			result.getReports().addAll(rptExec);
			result.setSuccess(true);
		} catch (Exception e) {
			result.setSuccess(false);
		}
		return result;
	}

	@RequestMapping(value = "/dl/{runId}", method = RequestMethod.GET)
	@ResponseBody
	public void downloadReport(HttpServletResponse response, @PathVariable("runId") Long runId) {
		IReportExecutionObject rptExec = this.execUpdater.getById(runId);
		this.getFileForDownload(response, rptExec);
	}

	private void getFileForDownload(HttpServletResponse response, IReportExecutionObject rptExec) throws ReportCenterException {
		String tmpPath = this.config.getTempPath();

		if (rptExec.getFilename() == null) {
			return;
		}

		try {
			String tmpFileNamePath = tmpPath + "/" + rptExec.getFilename();
			File file = new File(tmpFileNamePath);

			response.setHeader("Cache-Control", "private");
			response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
			response.setHeader("Content-Length", String.valueOf(file.length()));

			OutputStream out = response.getOutputStream();
			FileInputStream in = new FileInputStream(file);

			byte[] buf = new byte[1024];
			int bytesRead;
			while ((bytesRead = in.read(buf)) > 0) {
				out.write(buf, 0, bytesRead);
			}
			out.flush();
			out.close();
			in.close();
		} catch (Exception e) {
			throw new ReportCenterException(e);
		}
	}
}
