package com.atraxo.reportcenter.engine.base;

import java.io.File;

import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;

public interface ICustomReportEngine {

    public void buildReport(BaseReport report, File file) throws ReportCenterException;

    public boolean shouldBeUsed(BaseReport report);

    public void afterExecuteReport(BaseReport report);

    public String getCustomResponse();
}