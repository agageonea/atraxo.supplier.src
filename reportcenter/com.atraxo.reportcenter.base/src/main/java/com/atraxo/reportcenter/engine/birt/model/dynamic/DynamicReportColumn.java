/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 76832 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/DynamicReportColumn.java $
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;

public class DynamicReportColumn implements IDynamicReportColumn, IDynamicReportData {

    private String code;
    private String title;
    private int width;
    private String align;
    private boolean pageBreak = false;
    private String styleName = null;
    private String dataType = null;
    private String format = null;
    private double witdthPerc = 0;
    private BaseReport report;

    public DynamicReportColumn(BaseReport report) {
	this.report = report;
    }

    @XmlElement(name = "name")
    public String getCode() {
	return code;
    }

    public void setCode(String code) {
	this.code = code;
    }

    @XmlElement(name = "label")
    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    @XmlElement(name = "width")
    public int getWidth() {
	return width;
    }

    public void setWidth(int width) {
	this.width = width;
    }

    @XmlTransient
    public double getWitdthPerc() {
	return witdthPerc;
    }

    public void setWitdthPerc(double witdthPerc) {
	this.witdthPerc = witdthPerc;
    }

    @XmlElement(name = "align")
    public String getAlign() {
	return align;
    }

    public void setAlign(String align) {
	this.align = align;
    }

    @XmlElement(name = "pageBreak")
    public boolean isPageBreak() {
	return pageBreak;
    }

    public void setPageBreak(boolean pageBreak) {
	this.pageBreak = pageBreak;
    }

    public String getStyleName() {
	return styleName;
    }

    public void setStyleName(String styleName) {
	this.styleName = styleName;
    }

    @XmlElement(name = "type")
    public String getDataType() {
	return dataType;
    }

    public void setDataType(String dataType) {
	dataType = (dataType != null) ? dataType.toUpperCase() : "STRING";

	this.dataType = dataType;
    }

    @XmlElement(name = "fmt")
    public String getFormat() {
	return format;
    }

    public void setFormat(String format) {
	this.format = format;
    }

    public String getBirtDataType() {
	return report.getReportUtils().internalDataType2BirtDataType(this.getDataType());
    }

    public String getBirtValue() {
	return null;
    }

}
