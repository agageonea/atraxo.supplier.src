/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 76832 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/parser/DynamicReportErrorHandler.java $
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class DynamicReportErrorHandler implements ErrorHandler {

    private static final Logger LOG = LoggerFactory.getLogger(DynamicReportErrorHandler.class);

    public DynamicReportErrorHandler() {
	super();
    }

    public void warning(SAXParseException exception) throws SAXException {
	LOG.warn(exception.getMessage());
    }

    public void error(SAXParseException exception) throws SAXException {
	LOG.error(exception.getMessage());
    }

    public void fatalError(SAXParseException exception) throws SAXException {
	LOG.error(exception.getMessage());
    }

}
