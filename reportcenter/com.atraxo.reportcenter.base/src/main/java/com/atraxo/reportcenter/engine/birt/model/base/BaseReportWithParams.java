/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 76832 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/base/BaseReportWithParams.java $
 */
package com.atraxo.reportcenter.engine.birt.model.base;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import org.eclipse.birt.report.engine.api.IRenderTask;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;

import com.atraxo.reportcenter.engine.base.ReportCenterException;
import com.atraxo.reportcenter.engine.birt.BirtParamNames;

public abstract class BaseReportWithParams extends BaseReport {

    private List<ReportParam> params = new ArrayList<ReportParam>();

    @XmlElementWrapper(name = "params")
    @XmlElement(name = "param")
    public List<ReportParam> getParams() {
	return params;
    }

    public void setParams(List<ReportParam> params) {
	this.params = params;
    }

    @Override
    public String getReportFileName() {
	// TODO Auto-generated method stub
	return null;
    }

    public ReportParam getParameterByName(String paramName) {
	for (ReportParam param : getParams()) {
	    if (paramName.toUpperCase().equals(param.getName().toUpperCase())) {
		return param;
	    }
	}
	return null;
    }

    @Override
    public void assignParametersToReport(IReportEngine engine, IReportRunnable design, IRenderTask task) throws ReportCenterException {
	super.assignParametersToReport(engine, design, task);

	task.setParameterValue(BirtParamNames.P_REPORT_CODE, getCode());
    }

    @Override
    public String getOutputFileName() {
	ReportParam param = getParameterByName("P_Output_FileName");

	if (param != null) {
	    String value = param.getValue();
	    if ((value != null) && (!"".equals(value.trim()))) {
		return value;
	    }
	}

	return super.getOutputFileName();
    }

}
