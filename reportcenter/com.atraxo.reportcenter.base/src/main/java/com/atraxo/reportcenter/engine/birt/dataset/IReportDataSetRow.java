package com.atraxo.reportcenter.engine.birt.dataset;

public interface IReportDataSetRow {

	public Object getRowId();
	public void setRowId(Object rowId);
	public String asLogString();
}
