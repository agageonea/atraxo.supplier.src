package com.atraxo.reportcenter.service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.atraxo.reportcenter.engine.ReportExecutor;
import com.atraxo.reportcenter.engine.base.IReportCenterCleaner;
import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;
import com.atraxo.reportcenter.engine.birt.model.base.InitReport;
import com.atraxo.reportcenter.engine.birt.model.dialog.DialogReport;
import com.atraxo.reportcenter.engine.birt.model.dynamic.DynamicReport;
import com.atraxo.reportcenter.reportexec.IReportExecutionObject;

@Service
public class ReportCenterService {

    @Autowired
    private IReportCenterCleaner cleaner;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private IReportExecution<Long> execUpdater;

    private static final Logger LOG = LoggerFactory.getLogger(ReportCenterService.class);

    public IReportExecutionObject run(String rid, String localeCode, String data, OutputStream output, boolean initMode, Boolean isDevMode) {
	BaseReport currentReport;
	IReportExecutionObject currReportExecObj = null;
	Long runId;

	try {
	    LOG.info("Start report generation ");

	    LOG.debug("Report data : \n" + data);

	    JAXBContext jcContext = JAXBContext.newInstance(
	    // SpecialReport.class,
		    DialogReport.class, DynamicReport.class, InitReport.class);
	    Unmarshaller u = jcContext.createUnmarshaller();
	    InputStream is = new ByteArrayInputStream(data.getBytes("UTF-8"));

	    currentReport = (BaseReport) u.unmarshal(is);

	    currentReport.validate();
	    
	    currentReport.setContext(context);
	    currentReport.init();

	    if (!initMode) {
		currentReport.setUserAndPass(rid);
		currentReport.setIsPassEncoded(!isDevMode);
		currentReport.getDbConnectionParams();

		runId = execUpdater.reportStart(currentReport);
		currReportExecObj = execUpdater.getById(runId);
	    }

	    try {
		if (!initMode)
		    cleaner.execute(currentReport, rid, isDevMode);
	    } catch (Exception e) {
		LOG.debug(e.getMessage(), e);
	    }

	    LOG.info("Generate report : " + currentReport.getCode());

	    ReportExecutor reportExecutor = new ReportExecutor(context, rid, isDevMode, currentReport, localeCode, initMode, currReportExecObj);

	    reportExecutor.run();

	    currReportExecObj = reportExecutor.getReportExecObj();

//	    if (currentRunResult.isSuccess()) {
		execUpdater.reportSuccess(currReportExecObj.getExecId(), currReportExecObj.getUrl(), currReportExecObj.getFilename());
//	    } else {
//		throw new Exception(currentRunResult.getErrMsg());
//	    }

	} catch (Exception e) {
	    if (currReportExecObj != null) {
		currReportExecObj.setErrMsg(e.getMessage());
		execUpdater.reportFailed(currReportExecObj.getExecId(), currReportExecObj.getErrMsg());
	    }
	    
	    LOG.error(e.getMessage(), e);
	} finally {
	    if (currReportExecObj != null) {
		currReportExecObj = execUpdater.getById(currReportExecObj.getExecId());
	    }
	}

	return currReportExecObj;
    }

    public IReportCenterCleaner getCleaner() {
	return cleaner;
    }

    public List<IReportExecutionObject> listAll() {
	List<IReportExecutionObject> rptExecs = execUpdater.listAll();
	return rptExecs;
    }
}
