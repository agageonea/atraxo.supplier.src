package com.atraxo.reportcenter.util.exception;

public class ExceptionUtil {

    public static String getExeceptionMsgAndCause(Exception e) {
	String result = "";
	if (e.getMessage() != null) {
	    result = e.getMessage();
	}
	if (e.getCause() != null) {
	    result = result + "\nCaused by: " + e.getCause().getMessage();
	}

	return result;
    }
}
