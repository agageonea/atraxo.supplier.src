/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 77121 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/DynamicReportQueryParams.java $
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;
import com.atraxo.reportcenter.engine.birt.model.dynamic.parser.DynamicReportXMLParser;
import com.atraxo.reportcenter.engine.birt.model.dynamic.parser.INodeHandler;

public class DynamicReportQueryParams {

    private static final Logger LOG = LoggerFactory.getLogger(DynamicReportQueryParams.class);

    private static final String NODE_NAME = "name";
    private static final String NODE_VALUE = "value";
    private static final String NODE_TYPE = "type";
    private static final String NODE_STYLE = "style";
    private static final String NODE_FMT = "fmt";

    private List<DynamicReportQueryParam> params = new ArrayList<DynamicReportQueryParam>();
    private DynamicReportXMLParser xmlParser;

    private BaseReport report;

    public DynamicReportQueryParams(BaseReport report) {
	this.report = report;
    }

    public int size() {
	return params.size();
    }

    public DynamicReportQueryParam getParam(int index) {
	return params.get(index);
    }

    public void readParams() {
	xmlParser.cleanUp();

	xmlParser.setStartPath("/reportData/queryParams/queryParam");
	xmlParser.setEndPath("/reportData/queryParams");

	INodeHandler handler = new INodeHandler() {

	    private DynamicReportQueryParam param;

	    public void onChildNode(String name, String value, Hashtable<String, String> attributes) {
		if (NODE_NAME.equals(name))
		    param.setName(value);
		else if (NODE_VALUE.equals(name))
		    param.setValue(value);
		else if (NODE_TYPE.equals(name))
		    param.setDataType(value);
		else if (NODE_STYLE.equals(name))
		    param.setStyleName(value);
		else if (NODE_FMT.equals(name))
		    param.setFormat(value);
	    }

	    public void onNodeStart(String name, Hashtable<String, String> attributes) {
		param = new DynamicReportQueryParam();
	    }

	    public void onNodeEnd(String name, Hashtable<String, String> attributes) {
		params.add(param);
	    }
	};

	xmlParser.addNodeHandler(handler);
	try {
	    xmlParser.process();
	} catch (ParserConfigurationException e) {
	    LOG.debug(e.getMessage(), e);
	} catch (SAXException e) {
	    LOG.debug(e.getMessage(), e);
	} catch (IOException e) {
	    LOG.debug(e.getMessage(), e);
	}
	xmlParser.removeNodeHandler(handler);

    }

    public void printParams() {
	DynamicReportUtil rptUtl = DynamicReportUtil.getInstance();
	LOG.debug("Report parameters");
	for (int i = 0; i < params.size(); i++) {
	    DynamicReportQueryParam col = params.get(i);
	    LOG.debug(col.getName() + " = " + rptUtl.fixHTMLChars(col.getValue()));
	}
    }

    public void setXmlParser(DynamicReportXMLParser xmlParser) {
	this.xmlParser = xmlParser;
    }

    public void clear() {
	params.clear();
    }

    public BaseReport getReport() {
        return report;
    }

}
