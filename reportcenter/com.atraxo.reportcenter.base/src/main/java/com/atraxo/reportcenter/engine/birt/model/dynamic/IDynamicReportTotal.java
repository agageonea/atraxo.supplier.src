/**
 * Copyright (C) FuelPlus GmbH 
 * $Revision: 76832 $  
 * $Date:   11/16/2005 11:35:09 AM$ 
 * $Author: fc $  
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/IDynamicReportTotal.java $  
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic;

public interface IDynamicReportTotal {

    public String getValue();

    public void setValue(String value);

    public String getLabel();

    public void setLabel(String label);

    public String getFormat();

    public void setFormat(String format);

}
