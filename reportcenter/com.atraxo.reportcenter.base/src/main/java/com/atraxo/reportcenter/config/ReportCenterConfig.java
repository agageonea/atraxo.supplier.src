package com.atraxo.reportcenter.config;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

public class ReportCenterConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportCenterConfig.class);

    @Value("${" + ReportCenterConstants.CONFIG_DEV_MODE + ":false}")
    private String devMode = "false";

    @Value("${" + ReportCenterConstants.CONFIG_SMTP_HOST + ":@null}")
    private String smtpHost;

    @Value("${" + ReportCenterConstants.CONFIG_SMTP_PORT + ":25}")
    private String smtpPortStr;

    @Value("${" + ReportCenterConstants.CONFIG_SMTP_FROM + ":@null}")
    private String smtpFrom;

    @Value("${" + ReportCenterConstants.CONFIG_SMTP_USER + ":@null}")
    private String smtpUser;

    @Value("${" + ReportCenterConstants.CONFIG_SMTP_PASS + ":@null}")
    private String smtpPass;

    @Value("${" + ReportCenterConstants.CONFIG_SMTP_SSL + ":@null}")
    private String smtpSsl;

    @Value("${" + ReportCenterConstants.CONFIG_SMTP_TLS + ":@null}")
    private String smtpTls;

    @Value("${" + ReportCenterConstants.CONFIG_REPORTS_USER_PATH + ":@null}")
    private String userReportsPath;

    @Value("${" + ReportCenterConstants.CONFIG_TEMP_PATH + "}")
    private String tempPath;

    @Value("${" + ReportCenterConstants.CONFIG_REPORTS_PATH + ":@null}")
    private String reportsPath;

    @Value("${" + ReportCenterConstants.CONFIG_REPORTS_LIBRARY_PATH + ":@null}")
    private String reportsLibrariesPath;

    @Value("${" + ReportCenterConstants.CONFIG_REPORTS_IMAGES_PATH + ":@null}")
    private String reportsImagesPath;

    @Value("${" + ReportCenterConstants.CONFIG_GMT_TIMESTAMP_DIF + ":@null}")
    private String gmtTimestampDif;

    @Value("${" + ReportCenterConstants.CONFIG_DB_URL + ":@null}")
    private String dbUrl;

    @Value("${" + ReportCenterConstants.CONFIG_DB_DRV + ":@null}")
    private String dbDriver;

    @Value("${" + ReportCenterConstants.CONFIG_DB_USER + ":@null}")
    private String dbUser;

    @Value("${" + ReportCenterConstants.CONFIG_DB_PASS + ":@null}")
    private String dbPass;

    public boolean isDevMode() {
	if (devMode != null) {
	    return ("1".equals(devMode) || "true".equalsIgnoreCase(devMode));
	}
	return false;
    }

    protected String getDevMode() {
	return devMode;
    }

    public void setDevMode(String devMode) {
	this.devMode = devMode;
    }

    public String getSmtpHost() {
	return smtpHost;
    }

    public void setSmtpHost(String smtpHost) {
	this.smtpHost = smtpHost;
    }

    public Integer getSmtpPort() {
	Integer result = new Integer(25);
	try {
	    result = Integer.valueOf(smtpPortStr);
	} catch (Exception e) {

	}
	return result;
    }

    public String getSmtpPortStr() {
	return smtpPortStr;
    }

    public void setSmtpPortStr(String smtpPortStr) {
	this.smtpPortStr = smtpPortStr;
    }

    public String getSmtpFrom() {
	return smtpFrom;
    }

    public void setSmtpFrom(String smtpFrom) {
	this.smtpFrom = smtpFrom;
    }

    public String getSmtpUser() {
	return smtpUser;
    }

    public void setSmtpUser(String smtpUser) {
	this.smtpUser = smtpUser;
    }

    public String getSmtpPass() {
	return smtpPass;
    }

    public void setSmtpPass(String smtpPass) {
	this.smtpPass = smtpPass;
    }

    public String getSmtpSsl() {
	return smtpSsl;
    }

    public void setSmtpSsl(String smtpSsl) {
	this.smtpSsl = smtpSsl;
    }

    public String getSmtpTls() {
	return smtpTls;
    }

    public void setSmtpTls(String smtpTls) {
	this.smtpTls = smtpTls;
    }

    public String getUserReportsPath() {
	return userReportsPath;
    }

    public void setUserReportsPath(String userReportsPath) {
	this.userReportsPath = userReportsPath;
    }

    public String getTempPath() {
	if (tempPath != null) {
		if (! tempPath.endsWith("/")) tempPath = tempPath + "/";
	}
    	return tempPath;
    }

    public void setTempPath(String tempPath) {
	this.tempPath = tempPath;
    }

    public String getReportsPath() {
	return reportsPath;
    }

    public void setReportsPath(String reportsPath) {
	this.reportsPath = reportsPath;
    }

    public String getReportsLibrariesPath() {
	return reportsLibrariesPath;
    }

    public void setReportsLibrariesPath(String reportsLibrariesPath) {
	this.reportsLibrariesPath = reportsLibrariesPath;
    }

    public String getReportsImagesPath() {
	return reportsImagesPath;
    }

    public void setReportsImagesPath(String reportsImagesPath) {
	this.reportsImagesPath = reportsImagesPath;
    }

    public String getDbUrl() {
	return dbUrl;
    }

    public void setDbUrl(String dbUrl) {
	this.dbUrl = dbUrl;
    }

    public String getDbDriver() {
	return dbDriver;
    }

    public void setDbDriver(String dbDriver) {
	this.dbDriver = dbDriver;
    }

    public String getDbUser() {
	return dbUser;
    }

    public void setDbUser(String dbUser) {
	this.dbUser = dbUser;
    }

    public String getDbPass() {
	return dbPass;
    }

    public void setDbPass(String dbPass) {
	this.dbPass = dbPass;
    }

    public synchronized int getUTCTimestampDiff() {
	if ("@null".equalsIgnoreCase(gmtTimestampDif))
	    return 0;
	try {
	    Pattern pattern = Pattern.compile("([+,-]){0,1}([0-9]{1,2})(:([0-9]{1,2})){0,1}");
	    Matcher matcher = pattern.matcher(gmtTimestampDif);

	    if (matcher.find()) {

		String sign = (matcher.group(1) == null) ? "+" : matcher.group(1);
		int hour = Integer.valueOf(matcher.group(2));
		int min = Integer.valueOf((matcher.group(4) == null) ? "0" : matcher.group(4));

		if (hour > 23)
		    throw new Exception("Invalid hour provided ( greater than 23) : " + hour);
		if (min > 60)
		    throw new Exception("Invalid minutes provided ( greater than 60) : " + min);

		int retVal = hour * 60 + min;
		if ("-".equals(sign))
		    retVal = (-1) * retVal;
		return retVal;
	    } else {
		throw new Exception("Invalid value provided.");
	    }
	} catch (Exception e) {
	    LOGGER.error("ERROR setting gmtTimeStampDiff parameter. " + e.getMessage());
	    return 0;
	}
    }

    public Boolean isUserReportsPathIsSet() {
	return (!"@null".equalsIgnoreCase(getUserReportsPath()));
    }

    public boolean isReportsPathIsSet() {
	return (!"@null".equalsIgnoreCase(getReportsPath()));
    }

    public boolean isReportsLibrariesPathIsSet() {
	return (!"@null".equalsIgnoreCase(getReportsLibrariesPath()));
    }

    public boolean isReportsImagesPathIsSet() {
	return (!"@null".equalsIgnoreCase(getReportsImagesPath()));
    }

}
