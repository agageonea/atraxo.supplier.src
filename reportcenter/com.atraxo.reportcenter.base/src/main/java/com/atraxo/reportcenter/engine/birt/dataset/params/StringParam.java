package com.atraxo.reportcenter.engine.birt.dataset.params;

import java.sql.SQLException;
import java.sql.Types;

import com.atraxo.reportcenter.engine.birt.dataset.NamedParameterStatement;

public class StringParam extends AbstractReportParam<String> {

    public StringParam(String paramName, String paramValue) {
	super(paramName, paramValue);
    }

    @Override
    public void setStatementParam(NamedParameterStatement statement) throws SQLException {
	if (getValue() == null)
	    statement.setNull(getName(), Types.VARCHAR);
	else
	    statement.setString(getName(), getValue());
    }

}
