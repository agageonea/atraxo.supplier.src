/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 76832 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/DynamicReportColumns.java $
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;
import com.atraxo.reportcenter.engine.birt.model.dynamic.parser.DynamicReportXMLParser;
import com.atraxo.reportcenter.engine.birt.model.dynamic.parser.INodeHandler;

public class DynamicReportColumns {

    private static final Logger LOG = LoggerFactory.getLogger(DynamicReportColumns.class);

    private static final String NODE_NAME = "name";
    private static final String NODE_LABEL = "label";
    private static final String NODE_WIDTH = "width";
    private static final String NODE_ALIGN = "align";
    private static final String NODE_BREAK = "break";
    private static final String NODE_STYLE_NAME = "styleName";
    private static final String NODE_TYPE = "type";
    private static final String NODE_FMT = "fmt";

    private List<DynamicReportColumn> columns = new ArrayList<DynamicReportColumn>();
    private DynamicReportXMLParser xmlParser;

    private BaseReport report;

    public DynamicReportColumns(BaseReport report) {
	this.report = report;
    }

    public List<DynamicReportColumn> getColumns() {
	return columns;
    }

    public void readColumns() {

	xmlParser.cleanUp();
	xmlParser.setStartPath("/reportData/columnDef/column");
	xmlParser.setEndPath("/reportData/columnDef");

	INodeHandler handler = new INodeHandler() {

	    private DynamicReportColumn column;

	    public void onChildNode(String name, String value, Hashtable<String, String> attributes) {
		if (NODE_NAME.equals(name))
		    column.setCode(value);
		else if (NODE_LABEL.equals(name))
		    column.setTitle(value);
		else if (NODE_WIDTH.equals(name))
		    column.setWidth(Integer.valueOf(value));
		else if (NODE_ALIGN.equals(name))
		    column.setAlign(value);
		else if (NODE_BREAK.equals(name))
		    column.setPageBreak(Boolean.valueOf(value));
		else if (NODE_STYLE_NAME.equals(name))
		    column.setStyleName(value);
		else if (NODE_TYPE.equals(name))
		    column.setDataType(value);
		else if (NODE_FMT.equals(name))
		    column.setFormat(value);
	    }

	    public void onNodeStart(String name, Hashtable<String, String> attributes) {
		column = new DynamicReportColumn(DynamicReportColumns.this.getReport());
	    }

	    public void onNodeEnd(String nodeName, Hashtable<String, String> attributes) {
		columns.add(column);
	    }

	};

	xmlParser.addNodeHandler(handler);

	try {
	    xmlParser.process();
	} catch (ParserConfigurationException e) {
	    LOG.debug(e.getMessage(), e);
	} catch (SAXException e) {
	    LOG.debug(e.getMessage(), e);
	} catch (IOException e) {
	    LOG.debug(e.getMessage(), e);
	}

	xmlParser.removeNodeHandler(handler);

	calculateWidthPerc();
    }

    private void calculateWidthPerc() {
	int totalWidth = 0;
	for (int i = 0; i < columns.size(); i++) {
	    totalWidth = totalWidth + columns.get(i).getWidth();
	}

	if (totalWidth > 0) {
	    double totalPerc = 0;
	    for (int i = 0; i < columns.size(); i++) {
		DynamicReportColumn col = columns.get(i);

		BigDecimal v1 = new BigDecimal(col.getWidth());
		BigDecimal v2 = new BigDecimal(totalWidth);

		BigDecimal bd = new BigDecimal(v1.doubleValue() / v2.doubleValue() * 100.00);
		bd = bd.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		col.setWitdthPerc(bd.doubleValue());

		totalPerc = totalPerc + bd.doubleValue();
	    }

	    BigDecimal bd = new BigDecimal(totalPerc);
	    bd = bd.setScale(2, BigDecimal.ROUND_HALF_EVEN);

	    if (bd.doubleValue() < 100.00) {
		DynamicReportColumn col = columns.get(columns.size() - 1);
		BigDecimal v = new BigDecimal(col.getWitdthPerc() + (100.00 - bd.doubleValue()));
		v = v.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		col.setWitdthPerc(v.doubleValue());
	    } else if (bd.doubleValue() > 100.00) {
		DynamicReportColumn col = columns.get(columns.size() - 1);
		BigDecimal v = new BigDecimal(col.getWitdthPerc() - (bd.doubleValue() - 100.00));
		v = v.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		col.setWitdthPerc(v.doubleValue());
	    }
	}

    }

    public void printColumns() {
	DynamicReportUtil rptUtl = DynamicReportUtil.getInstance();

	LOG.debug("Report columns");
	for (int i = 0; i < columns.size(); i++) {
	    DynamicReportColumn col = columns.get(i);
	    LOG.debug(col.getCode() + " = " + rptUtl.fixHTMLChars(col.getTitle()) + "; width=" + col.getWitdthPerc() + "% ; align=" + col.getAlign()
		    + " ; dataType/BIRTdataType=" + col.getDataType() + "/" + col.getBirtDataType() + " format=" + col.getFormat());
	}
    }

    public int size() {
	return columns.size();
    }

    public DynamicReportColumn getColumn(int index) {
	return columns.get(index);
    }

    public void setXmlParser(DynamicReportXMLParser xmlParser) {
	this.xmlParser = xmlParser;
    }

    public void clear() {
	columns.clear();
    }

    public IDynamicReportColumn getColumnByCode(String colCode) {
	for (int i = 0; i < columns.size(); i++) {
	    IDynamicReportColumn col = getColumn(i);
	    if (col.getCode().equals(colCode))
		return col;
	}
	return null;
    }

    public BaseReport getReport() {
        return report;
    }
}
