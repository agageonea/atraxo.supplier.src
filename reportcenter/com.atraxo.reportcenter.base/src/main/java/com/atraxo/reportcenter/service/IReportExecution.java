package com.atraxo.reportcenter.service;

import java.util.List;

import com.atraxo.reportcenter.engine.base.ReportCenterException;
import com.atraxo.reportcenter.engine.birt.model.base.AbstractReport;
import com.atraxo.reportcenter.reportexec.IReportExecutionObject;


public interface IReportExecution<T> {

    public T reportStart(AbstractReport report) throws ReportCenterException;
    public void reportSuccess(T id, String url, String fileName) throws ReportCenterException;
    public void reportFailed(T id, String errMsg) throws ReportCenterException;

    public List<IReportExecutionObject> listAll();
    public IReportExecutionObject getById(T id);
}
