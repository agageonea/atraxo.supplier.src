/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 76832 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/parser/DynamicReportXMLHandler.java $
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic.parser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class DynamicReportXMLHandler extends DefaultHandler {

    private String startPath = null;
    private String startPathAttributeName = null;
    private String startPathAttributeValue = null;
    private String endPath = null;

    private String currentPath = "";
    private Stack<Hashtable<String, String>> currentAttributesStack = new Stack<Hashtable<String, String>>();
    private Stack<String> currentPathStack = new Stack<String>();
    private Hashtable<String, String> currentAttributes;

    private List<INodeHandler> handlers = new ArrayList<INodeHandler>();
    private String currentValue;

    private boolean shouldCallNodeStartEnd() {
	if (startPath.equals(currentPath)) {
	    if ((startPathAttributeName != null) && (startPathAttributeValue != null)) {
		if (currentAttributes != null) {
		    for (String attrName : Collections.list(currentAttributes.keys())) {
			if (startPathAttributeName.equals(attrName)) {
			    if (startPathAttributeValue.equals(currentAttributes.get(attrName)))
				return true;
			}
		    }
		    return false;
		} else
		    return false;
	    } else
		return true;
	}

	return false;
    }

    private boolean shouldCallChildNode() {
	return (currentPath.startsWith(startPath) && (!startPath.equals(currentPath)));
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
	currentPath = currentPath + "/" + qName;
	currentAttributes = extractAttibutes(attributes);
	currentValue = "";

	boolean callNodeStart = shouldCallNodeStartEnd();

	if (callNodeStart) {
	    Iterator<INodeHandler> itCh = handlers.iterator();
	    while (itCh.hasNext()) {
		INodeHandler h = itCh.next();
		h.onNodeStart(qName, currentAttributes);
	    }
	}

	currentPathStack.push(currentPath);
	currentAttributesStack.push(currentAttributes);
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {

	String text = new String(ch, start, length);
	// if (text != null) text = text.trim();
	currentValue = currentValue + text;
    }

    private Hashtable<String, String> extractAttibutes(Attributes aAttributes) {
	Hashtable<String, String> attributes = new Hashtable<String, String>();
	for (int i = 0; i < aAttributes.getLength(); i++) {
	    String name = aAttributes.getQName(i);
	    String value = aAttributes.getValue(i);
	    attributes.put(name, value);
	}
	return attributes;
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
	currentAttributes = currentAttributesStack.pop();
	currentPath = currentPathStack.pop();

	boolean callNodeEnd = shouldCallNodeStartEnd();
	boolean isChildNode = shouldCallChildNode();

	if (isChildNode) {
	    Iterator<INodeHandler> itCh = handlers.iterator();
	    while (itCh.hasNext()) {
		INodeHandler h = itCh.next();
		h.onChildNode(qName, currentValue, currentAttributes);
	    }
	}
	if (callNodeEnd) {
	    Iterator<INodeHandler> itCh = handlers.iterator();
	    while (itCh.hasNext()) {
		INodeHandler h = itCh.next();
		h.onNodeEnd(qName, currentAttributes);
	    }
	}

	if (endPath.equals(currentPath)) {
	    throw new SAXException(new EndPathEncounteredException());
	}

	int idx = currentPath.lastIndexOf("/" + qName);
	currentPath = currentPath.substring(0, idx);

    }

    public void setStartPath(String startPath) {
	this.startPath = startPath;
    }

    public void setStartPathAttributeName(String startPathAttributeName) {
	this.startPathAttributeName = startPathAttributeName;
    }

    public void setStartPathAttributeValue(String startPathAttributeValue) {
	this.startPathAttributeValue = startPathAttributeValue;
    }

    public void setEndPath(String endPath) {
	this.endPath = endPath;
    }

    public void addNodeHandler(INodeHandler handler) {
	if (!handlers.contains(handler)) {
	    handlers.add(handler);
	}
    }

    public void removeNodeHandler(INodeHandler handler) {
	if (handlers.contains(handler)) {
	    handlers.remove(handler);
	}
    }
}
