package com.atraxo.reportcenter.engine.birt;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import org.eclipse.birt.report.model.api.IResourceLocator;
import org.eclipse.birt.report.model.api.ModuleHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import com.atraxo.reportcenter.config.ReportCenterConfig;

public class BirtResourceLocator implements IResourceLocator {
    private static final Logger LOGGER = LoggerFactory.getLogger(BirtResourceLocator.class);
    private Resource defaultReportsDir;
    private Resource defaultLibraryDir;
    private Resource defaultLocaleDir;
    private Resource defaultImagesDir;
    private Resource defaultUserReportsDir;

    @Autowired
    private ReportCenterConfig config;

    @Autowired
    private ApplicationContext context;

    public URL findResource(ModuleHandle moduleHandle, String fileName, int type) {

	Resource defaultRes = context.getResource(fileName);
	if (defaultRes.exists()) {
	    try {
		return defaultRes.getURL();
	    }
	    catch (Exception e) {
		LOGGER.error(e.getMessage(), e);
		return null;
	    }
	}

	defaultRes = getDefaultReportsDir();
	switch (type) {
	case IResourceLocator.LIBRARY:
	    defaultRes = getDefaultLibraryDir();
	    break;
	case IResourceLocator.OTHERS:
	    defaultRes = defaultLocaleDir;
	    break;
	case IResourceLocator.IMAGE:
	    defaultRes = getDefaultImagesDir();
	    break;
	default:
	    LOGGER.warn("This type is not managed : {}", type);
	    break;
	}
	return findResource(fileName, defaultRes);
    }

    @SuppressWarnings("rawtypes")
    public URL findResource(ModuleHandle moduleHandle, String fileName, int type, Map map) {
	return findResource(moduleHandle, fileName, type);
    }

    private URL findResource(final String fileName, final Resource res) {
	try {
	    Resource r = res.createRelative(fileName);
	    return r.getURL();
	} catch (IOException e) {
	    LOGGER.error(e.getMessage(), e);
	    return null;
	}
    }

    /* GETTER AND SETTER */
    public void setDefaultReportsDir(Resource defaultReportsDir) {
	this.defaultReportsDir = defaultReportsDir;
    }

    public void setDefaultUserReportsDir(Resource defaultUserReportsDir) {
	this.defaultUserReportsDir = defaultUserReportsDir;
    }

    public void setDefaultLibraryDir(Resource defaultLibraryDir) {
	this.defaultLibraryDir = defaultLibraryDir;
    }

    public void setDefaultImagesDir(Resource defaultImagesDir) {
	this.defaultImagesDir = defaultImagesDir;
    }

    public void setDefaultLocaleDir(Resource defaultLocaleDir) {
	this.defaultLocaleDir = defaultLocaleDir;
    }

    public Resource getDefaultReportsDir() {
	if (config.isReportsPathIsSet()) {
	    this.defaultReportsDir = new FileSystemResource(config.getReportsPath());
	}
	return this.defaultReportsDir;
    }

    public Resource getDefaultUserReportsDir() {
	if (config.isUserReportsPathIsSet()) {
	    this.defaultUserReportsDir = new FileSystemResource(config.getUserReportsPath());
	}
	return this.defaultUserReportsDir;
    }

    public Resource getDefaultLibraryDir() {
	if (config.isReportsLibrariesPathIsSet()) {
	    this.defaultLibraryDir = new FileSystemResource(config.getReportsLibrariesPath());
	}
	return this.defaultLibraryDir;
    }

    public Resource getDefaultImagesDir() {
	if (config.isReportsImagesPathIsSet()) {
	    this.defaultImagesDir = new FileSystemResource(config.getReportsImagesPath());
	}
	return this.defaultImagesDir;
    }

}