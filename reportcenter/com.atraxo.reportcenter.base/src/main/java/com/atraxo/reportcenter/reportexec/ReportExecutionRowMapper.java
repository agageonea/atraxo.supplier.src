package com.atraxo.reportcenter.reportexec;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ReportExecutionRowMapper implements RowMapper<IReportExecutionObject>{

    @Override
    public IReportExecutionObject mapRow(ResultSet rs, int rowNum) throws SQLException {
	ReportExecutionImpl result = new ReportExecutionImpl();

	result.setExecId(rs.getLong("RUN_RPT_ID"));
	result.setName(rs.getString("RPT_NAME"));
	result.setTitle(rs.getString("RPT_TTL"));
	result.setUser(rs.getString("USR"));
	result.setStatus(rs.getString("STAT"));
	result.setUrl(rs.getString("URL"));
	result.setErrMsg(rs.getString("ERR_MSG"));
	result.setStartDt(rs.getTimestamp("START_DT"));
	result.setEndDt(rs.getTimestamp("END_DT"));
	result.setFilename(rs.getString("FILENAME"));

	return result;
    }

}
