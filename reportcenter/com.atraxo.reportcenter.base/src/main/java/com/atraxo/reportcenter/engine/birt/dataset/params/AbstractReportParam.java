package com.atraxo.reportcenter.engine.birt.dataset.params;

import java.sql.SQLException;

import com.atraxo.reportcenter.engine.birt.dataset.NamedParameterStatement;

public abstract class AbstractReportParam<T> {

    private String paramName;
    private T paramValue;

    public AbstractReportParam(String paramName, T paramValue) {
	this.paramName = paramName;
	this.paramValue = paramValue;
    }

    public T getValue() {
	return paramValue;
    }

    public String getName() {
	return paramName;
    }

    public abstract void setStatementParam(NamedParameterStatement statement) throws SQLException;
}
