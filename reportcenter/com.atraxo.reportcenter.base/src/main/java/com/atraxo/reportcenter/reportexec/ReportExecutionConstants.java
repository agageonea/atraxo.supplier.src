package com.atraxo.reportcenter.reportexec;

public class ReportExecutionConstants {

    public static final String COL_RUN_RPT_ID = "run_rpt_id";
    public static final String COL_RPT_NAME   = "rpt_name";
    public static final String COL_RPT_TTL    = "rpt_ttl";
    public static final String COL_USR        = "usr";
    public static final String COL_STAT       = "stat";
    public static final String COL_URL        = "url";
    public static final String COL_ERR_MSG    = "err_msg";
    public static final String COL_START_DT   = "start_dt";
    public static final String COL_END_DT     = "end_dt";
    public static final String COL_FILENAME   = "filename";

    public static final String REPORT_STATUS_RUNNING = "R";
    public static final String REPORT_STATUS_FAILED = "F";
    public static final String REPORT_STATUS_SUCCESS = "S";
    public static final String REPORT_STATUS_UNKNOWN = "U";


    public static String SQL_LIST_ALL =//@formatter:off
		"SELECT " +
		" run_rpt_id," +
		" rpt_name," +
		" rpt_ttl," +
		" usr," +
		" stat," +
		" url," +
		" err_msg," +
		" start_dt," +
		" end_dt," +
		" filename " +
		"FROM run_reports " +
		"order by run_rpt_id desc";
    		//@formatter:on
    public static  String SQL_GET_BY_ID =//@formatter:off
        	"SELECT " +
        	" run_rpt_id," +
        	" rpt_name," +
        	" rpt_ttl," +
        	" usr," +
        	" stat," +
        	" url," +
        	" err_msg," +
        	" start_dt," +
        	" end_dt," +
        	" filename " +
        	"FROM run_reports " +
        	"WHERE run_rpt_id = ?";
        	//@formatter:on
    public static String SQL_DELETE_BY_ID =//@formatter:off
		"DELETE FROM run_reports WHERE run_rpt_id = ?";
		//@formatter:on

    public static String SQL_INSERT =//@formatter:off
        	"INSERT INTO run_reports (" +
        	" rpt_name," +
        	" rpt_ttl," +
        	" usr," +
        	" stat," +
        	" url," +
        	" err_msg," +
        	" start_dt," +
        	" end_dt," +
        	" filename " +
        	") VALUES (" +
        	" ?," + 	// rpt_name
        	" ?," + 	// rpt_ttl
        	" ?," + 	// usr
        	" ?," + 	// stat
        	" ?," + 	// url
        	" ?," + 	// err_msg
        	" ?," + 	// start_dt
        	" ?," + 	// end_dt
        	" ?" +  	// filename
        	")";
        	//@formatter:on

    public static final String SQL_UPDATE_BY_ID =//@formatter:off
        	"UPDATE run_reports SET " +
        	" rpt_name = ?," +
        	" rpt_ttl = ?," +
        	" usr = ?," +
        	" stat = ?," +
        	" url = ?," +
        	" err_msg = ?," +
        	" start_dt = ?," +
        	" end_dt = ?," +
        	" filename = ? " +
        	"WHERE run_rpt_id = ?";
        	//@formatter:on

}
