package com.atraxo.reportcenter.engine;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.naming.NamingException;

import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.engine.api.IEngineTask;
import org.eclipse.birt.report.engine.api.IRenderTask;
import org.eclipse.birt.report.engine.api.IReportDocument;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.IRunTask;
import org.eclipse.birt.report.engine.api.RenderOption;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.birt.report.model.api.activity.SemanticException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;

import com.atraxo.reportcenter.config.ReportCenterConfig;
import com.atraxo.reportcenter.engine.base.ICustomReportEngine;
import com.atraxo.reportcenter.engine.base.IReportCenterArchiver;
import com.atraxo.reportcenter.engine.base.ReportCenterException;
import com.atraxo.reportcenter.engine.birt.BirtParamNames;
import com.atraxo.reportcenter.engine.birt.BirtResourceLocator;
import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;
import com.atraxo.reportcenter.engine.birt.model.base.IReportDesignModifier;
import com.atraxo.reportcenter.engine.birt.model.base.IReportModifier;
import com.atraxo.reportcenter.engine.birt.model.base.IReportOutputModifier;
import com.atraxo.reportcenter.engine.birt.model.base.InitReport;
import com.atraxo.reportcenter.engine.birt.model.base.ReportFormatOptions;
import com.atraxo.reportcenter.engine.birt.model.base.ReportOutputFormat;
import com.atraxo.reportcenter.engine.birt.model.base.schedule.ScheduleData;
import com.atraxo.reportcenter.reportexec.IReportExecutionObject;
import com.atraxo.reportcenter.service.IReportExecution;
import com.atraxo.reportcenter.util.DateUtils;
import com.atraxo.reportcenter.util.email.EmailSender;
import com.atraxo.reportcenter.util.email.FpEmailAttachment;
import com.atraxo.reportcenter.util.exception.ExceptionUtil;
import com.ibm.icu.text.DecimalFormatSymbols;
import com.ibm.icu.util.ULocale;

public class ReportExecutor {

	private static final Logger LOG = LoggerFactory.getLogger(ReportExecutor.class);

	private BaseReport report;
	private String localeCode;
	private boolean initMode;
	private IReportExecutionObject runResult;
	private boolean devMode;
	private String rid;

	private IReportExecution<Long> execUpdater;

	private ReportCenterConfig config;

	private IReportCenterArchiver reportArchiver;

	private IReportEngine reportEngine;

	private BirtResourceLocator birtResourceLocator;

	private DateUtils dateUtils;

	public ReportExecutor(ApplicationContext context, String rid, boolean devMode, BaseReport report, String localeCode, boolean initMode,
			IReportExecutionObject runResult) {
		this.devMode = devMode;
		this.report = report;
		this.localeCode = localeCode;
		this.initMode = initMode;
		this.runResult = runResult;
		this.rid = rid;

		this.birtResourceLocator = context.getBean(BirtResourceLocator.class);
		this.reportEngine = context.getBean(IReportEngine.class);
		this.reportArchiver = context.getBean(IReportCenterArchiver.class);
		this.config = context.getBean(ReportCenterConfig.class);
		this.execUpdater = context.getBean(IReportExecution.class);
		this.dateUtils = context.getBean(DateUtils.class);
	}

	public void run() throws Exception {
		String reportOutUrl = "";
		// String reportOutName = "";

		try {
			if (this.report.isConnectionNull() && (!this.initMode)) {
				this.report.getDbConnectionParams();
			}

			ICustomReportEngine customEngine = this.getCustomEngine(this.report);

			File outFile = null;
			if (customEngine == null) {
				outFile = this.genReportWithBIRT(this.localeCode, this.report);
			} else {
				outFile = this.genReportWithCustomEngine(customEngine, this.report);
			}

			if (this.initMode) {
				outFile.delete();
			} else {
				this.runResult.setLocalResultFile(outFile.getAbsolutePath());

				reportOutUrl =  // @formatter:off
				// uriInfo.getBaseUri().toString()
				// + uriInfo.getPath()
				((this.rid != null) ? ("?_rid=" + java.net.URLEncoder.encode(this.rid, "UTF-8") + "&") : "?") + "repFileName="
						+ java.net.URLEncoder.encode(outFile.getCanonicalPath(), "UTF-8") + "&contentType="
						+ java.net.URLEncoder.encode(this.report.getContentType(), "UTF-8") + "&contentDisposition="
						+ java.net.URLEncoder.encode(this.report.getHeaderContentDisposition(), "UTF-8") + (this.devMode ? "&devMode=true" : "");
				// @formatter:on
				reportOutUrl = reportOutUrl.replace("/run/sch?", "/run?");

				this.runResult.setLocalResultFile(outFile.getAbsolutePath());

				this.execUpdater.reportSuccess(this.runResult.getExecId(), reportOutUrl, this.runResult.getFilename());
			}

		} catch (Exception e) {
			String err = e.getMessage();
			if (err == null) {
				err = e.getClass().getCanonicalName();
			}

			if (!(err.startsWith("Can't get parameter type"))) {
				this.runResult.setErrMsg(e.getMessage());
				LOG.error(e.getMessage(), e);
				throw e;
			}
		} finally {

		}

		// if (!runResult.isSuccess()) {
		// try {
		// if ((!initMode) && ((rid != null))) {
		// execUpdater.reportFailed(runId, runResult.getErrorMessage());
		// }
		// } catch (ReportCenterException e) {
		// runResult.setError(e);
		// LOG.debug(e.getMessage(), e);
		// }
		// }

		if (this.report.getScheduleData() != null) {
			String emlErr = null;

			ScheduleData sd = this.report.getScheduleData();

			// TODO Check scheduled reports result
			// String batchSrvUrl = BirtEngine.getBatchSrvUrl();
			// String batchSrvUser = BirtEngine.getBatchSrvUser();
			// String batchSrvPass = BirtEngine.getBatchSrvPass();

			String destType = (this.report.getScheduleData().getDestType() != null) ? this.report.getScheduleData().getDestType() : null;
			String destName = (this.report.getScheduleData().getDestName() != null) ? this.report.getScheduleData().getDestName() : null;

			if (this.runResult.isSuccess()) {
				if (this.config.getSmtpHost() == null) {
					emlErr = "Email server not set!!! Please check your configuration !!!";
					LOG.error(emlErr);
				} else {
					try {
						if ("EML".equals(destType) && (destName != null)) {
							EmailSender eml = new EmailSender();
							eml.setSmtpHost(this.config.getSmtpHost());
							eml.setSmtpPort(this.config.getSmtpPort());
							if ((this.config.getSmtpUser() != null) && (!"".equals(this.config.getSmtpUser()))) {
								eml.setSmtpUser(this.config.getSmtpUser());
								eml.setSmtpPass(this.config.getSmtpPass());
							}
							eml.setFrom(this.config.getSmtpFrom());
							eml.setTo(destName);

							eml.setSubject("Report " + this.report.getOutputFileName() + this.report.getFileExtension());
							eml.setBody("Report " + this.report.getOutputFileName() + this.report.getFileExtension());
							ArrayList<FpEmailAttachment> files = new ArrayList<FpEmailAttachment>();
							files.add(new FpEmailAttachment(this.runResult.getLocalResultFile(),
									this.report.getOutputFileName() + this.report.getFileExtension()));

							eml.setAttachments(files);

							eml.send();
						}
					} catch (Exception e) {
						emlErr = "Error when sending email. \nError details:\n" + ExceptionUtil.getExeceptionMsgAndCause(e);
						LOG.error(emlErr);
					}
				}
			}

			Double extDocId = null;
			String saveErr = null;

			if (this.reportArchiver != null) {
				try {
					extDocId = this.reportArchiver.addFile(this.runResult.getLocalResultFile());
				} catch (Exception e) {
					saveErr = "Error when saving file into archive. \nError details:\n" + ExceptionUtil.getExeceptionMsgAndCause(e);
					LOG.error(saveErr, e);
				}
			}

			String xmlStr = "";
			xmlStr = xmlStr + "<ieo>";
			xmlStr = xmlStr + "<id>" + sd.getItemExecId() + "</id>";
			if (this.runResult.isSuccess() && (emlErr == null) && (saveErr == null)) {
				xmlStr = xmlStr + "<st>S</st>";
				xmlStr = xmlStr + "<u><![CDATA[" + reportOutUrl + "]]></u>";
				xmlStr = xmlStr + "<fn>" + this.report.getOutputFileName() + this.report.getFileExtension() + "</fn>";
				if (extDocId != null) {
					xmlStr = xmlStr + "<did>" + extDocId.longValue() + "</did>";
				}

			} else {
				xmlStr = xmlStr + "<st>F</st>";
				String errMsg = null;
				if (this.runResult.getErrMsg() != null) {
					errMsg = "Errors when generating report.\nError details:\n" + this.runResult.getErrMsg();
				}
				if (emlErr != null) {
					errMsg = (errMsg != null) ? (errMsg + "\n" + emlErr) : emlErr;
				}
				if (saveErr != null) {
					errMsg = (errMsg != null) ? (errMsg + "\n" + saveErr) : saveErr;
				}

				xmlStr = xmlStr + "<m><![CDATA[" + errMsg + "]]></m>";
			}
			xmlStr = xmlStr + "</ieo>";

			// TODO Check scheduled reports result
			// Client client = Client.create();
			// client.addFilter(new HTTPBasicAuthFilter(batchSrvUser,
			// batchSrvPass));
			// WebResource webResource = client.resource(batchSrvUrl +
			// "/rest/itex/add.xml");
			//
			// webResource.type(MediaType.APPLICATION_XML_TYPE).post(ClientResponse.class,
			// xmlStr);

		}

	}

	@SuppressWarnings("unchecked")
	private File genReportWithBIRT(String localeCode, BaseReport report)
			throws IOException, EngineException, FileNotFoundException, SemanticException, ReportCenterException, NamingException, SQLException {
		IReportRunnable design;

		Date dtStart = new Date();

//		IRunAndRenderTask task = null;

		try {

			// If parameter locale is set then this will be used
			// else if report locale is set then this will be uesd
			// otherwise English locale will be used
			// TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
			String lc = "en";
			if (localeCode != null) {
				lc = localeCode;
			} else if (report.getLocaleCode() != null) {
				lc = report.getLocaleCode();
			}

			IReportModifier repMod = this.getReportModifier(report);
			if (repMod != null) {
				report = repMod.modifyReport(report);
			}

			// report.init();

			Resource resFolder = this.birtResourceLocator.getDefaultReportsDir();
			org.springframework.core.io.Resource designResource;
			// TODO replace isFuelPlusReport with a more generic function name
			if (report.isFuelPlusReport()) {
				String filePath = resFolder.getFilename() + "/" + report.getReportFileName();
				designResource = resFolder.createRelative(filePath);
				LOG.debug("Load report from " + filePath);
			} else {
				resFolder = this.birtResourceLocator.getDefaultUserReportsDir();
				String filePath = resFolder.getFilename() + "/" + report.getReportFileName();
				designResource = resFolder.createRelative(filePath);	
				LOG.debug("Load report from " + filePath);
			}
		
			
			// Open report design
			design = this.reportEngine.openReportDesign(designResource.getInputStream());

			ReportDesignHandle reportDesignHandle = (ReportDesignHandle) design.getDesignHandle();

			report.modifyReport(reportDesignHandle);

			List<IReportDesignModifier> dsgnModList = this.getReportDesignModifier(report);
			if ((dsgnModList != null) && (dsgnModList.size() > 0)) {
				for (IReportDesignModifier rptMod : dsgnModList) {
					rptMod.modifyReportDesign(reportDesignHandle);
				}
			}

			String fileName = this.getTempFileName(report);
			String localFullFilePath = this.config.getTempPath() + fileName;
			String localRptDocFilePath = this.config.getTempPath() + fileName + ".rptdocument";
			
			// create task to run report 
			IRunTask taskRun = this.reportEngine.createRunTask(design);
			
			if (!(report instanceof InitReport)) {
				taskRun.getAppContext().put(BirtParamNames.FP_REPORT_OBJ, report);
				taskRun.getAppContext().put(BirtParamNames.FP_CONTEXT, report.getContext());
				taskRun.getAppContext().put("OdaJDBCDriverPassInConnection", report.getConnection());
			}
			
			report.assignParametersToReport(this.reportEngine, design, taskRun);
			
			taskRun.run(localRptDocFilePath);
			
			int runStatus = taskRun.getStatus();
			List<Exception> runErrors = taskRun.getErrors();

			taskRun.close();

			if (runStatus == IEngineTask.STATUS_FAILED) {
				String msg = "Error executing report \n";
				for (Exception ex : runErrors) {
					msg = msg + ex.getMessage() + "\n";
				}
				throw new EngineException(msg);
			}
			LOG.info("Report document " + report.getCode() + " generated in " + localRptDocFilePath);
			
			
			// create task to render report
			IReportDocument iRptDocument = this.reportEngine.openReportDocument(localRptDocFilePath);
			IRenderTask taskRender = this.reportEngine.createRenderTask(iRptDocument);
			
			// task.setTimeZone(com.ibm.icu.util.TimeZone.getTimeZone("GMT"));

			File outFile = new File(localFullFilePath);
			if (outFile.exists()) {
				outFile.delete();
			}
			outFile.createNewFile();

			OutputStream output = new FileOutputStream(outFile);

			RenderOption options = report.getRenderOptions(output);

			// Try to create locale using provided code
			// on error fallback to English
			ULocale locale = null;
			try {
				locale = new ULocale(lc);
			} catch (Exception e) {
				locale = new ULocale("en", "US");
			}

			DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(taskRender.getLocale());
			if (report.getFormat() == ReportOutputFormat.CSV) {
				char decSep = report.getCsvDecimalSep().charAt(0);
				char thSep = report.getCsvThousandSep().charAt(0);
				dfs.setDecimalSeparator(decSep);
				dfs.setGroupingSeparator(thSep);
			}
			taskRender.setLocale(locale);

			report.assignParametersToReport(this.reportEngine, design, taskRender);

			IReportOutputModifier outMod = this.getReportOutputModifier(report);
			if (outMod != null) {
				outMod.modify(report, options);
				outMod.modifyRunAndRenderTask(taskRender);
			}

			taskRender.setRenderOption(options);

			// For debug purposes in BirtConfig.properties, set property
			// devMode to true in order to have .rptdesign file saved to temp
			// folder
			boolean devSaveRptDsgn = report.getReportCenterConfig().isDevMode();
			if (devSaveRptDsgn) {
				String devSavePath = this.config.getTempPath();
				reportDesignHandle.saveAs(devSavePath + "/" + fileName + "_" + report.getCode() + "_"
						+ this.dateUtils.dateAsString(new Date(), "yyyyMMddHHmmssSSSS") + ".rptdesign");
			}

			setReportFormatOptions(report, taskRender);
			// render report

			if (!(report instanceof InitReport)) {
				taskRender.getAppContext().put(BirtParamNames.FP_REPORT_OBJ, report);
				taskRender.getAppContext().put(BirtParamNames.FP_CONTEXT, report.getContext());
				taskRender.getAppContext().put("OdaJDBCDriverPassInConnection", report.getConnection());
			}

			taskRender.render();

			int renderStatus = taskRender.getStatus();
			List<Exception> renderErrors = taskRender.getErrors();

			taskRender.close();

			report.finish();

			output.flush();
			output.close();

			if (renderStatus == IEngineTask.STATUS_FAILED) {
				String msg = "Error executing report \n";
				for (Exception ex : renderErrors) {
					msg = msg + ex.getMessage() + "\n";
				}
//				LOG.error(msg);
				throw new EngineException(msg);
			}
			LOG.info("Report " + report.getCode() + " generated in " + localFullFilePath);

			return outFile;
		} finally {
			Date dtEnd = new Date();
			long dtDif = dtEnd.getTime() - dtStart.getTime();

			long genMin = TimeUnit.MILLISECONDS.toMinutes(dtDif);
			long genSec = TimeUnit.MILLISECONDS.toSeconds(dtDif);
			long genMs = TimeUnit.MILLISECONDS.toMillis(dtDif);

			LOG.info("Generation time for report " + report.getCode() + " : " + genMin + "m " + genSec + "s " + genMs + "ms");
		}
	}

	private File genReportWithCustomEngine(ICustomReportEngine repEngine, BaseReport report) throws IOException {
		// report.init();

		String fileName = this.getTempFileName(report);
		String localFullFilePath = this.config.getTempPath() + fileName;

		File outFile = new File(localFullFilePath);
		if (outFile.exists()) {
			outFile.delete();
		}
		outFile.createNewFile();

		repEngine.buildReport(report, outFile);

		repEngine.afterExecuteReport(report);

		return outFile;
	}

	private String getTempFileName(BaseReport report) {
		return report.generateTempFileName() + report.getFileExtension();
	}

	public BaseReport getReport() {
		return this.report;
	}

	private static void setReportFormatOptions(BaseReport report, IRenderTask task) {

		ReportFormatOptions fmtOpt = report.getFormatOptions();

		switch (report.getFormat()) {
		case XLS:
		case XLSX:
			task.setParameterValue(BirtParamNames.P_SHOW_FOOTER, "N");

			if ((fmtOpt != null) && (ReportOutputFormat.XLS.equals(fmtOpt.getFormat()))) {
				if (fmtOpt.isShowParams()) {
					task.setParameterValue(BirtParamNames.P_SHOW_PARAMS, "Y");
				} else {
					task.setParameterValue(BirtParamNames.P_SHOW_PARAMS, "N");
				}
				if (fmtOpt.isShowHeader()) {
					task.setParameterValue(BirtParamNames.P_SHOW_HEADER, "Y");
				} else {
					task.setParameterValue(BirtParamNames.P_SHOW_HEADER, "N");
				}
			} else {
				task.setParameterValue(BirtParamNames.P_SHOW_PARAMS, "Y");
				task.setParameterValue(BirtParamNames.P_SHOW_HEADER, "Y");
			}
			break;
		case HTML:
			task.setParameterValue(BirtParamNames.P_SHOW_FOOTER, "N");
			task.setParameterValue(BirtParamNames.P_SHOW_HEADER, "Y");
			break;
		default:
			if (fmtOpt != null) {
				if (fmtOpt.isShowParams()) {
					task.setParameterValue(BirtParamNames.P_SHOW_PARAMS, "Y");
				} else {
					task.setParameterValue(BirtParamNames.P_SHOW_PARAMS, "N");
				}
				if (fmtOpt.isShowHeader()) {
					task.setParameterValue(BirtParamNames.P_SHOW_HEADER, "Y");
				} else {
					task.setParameterValue(BirtParamNames.P_SHOW_HEADER, "N");
				}
				if (fmtOpt.isShowHeader()) {
					task.setParameterValue(BirtParamNames.P_SHOW_FOOTER, "Y");
				} else {
					task.setParameterValue(BirtParamNames.P_SHOW_FOOTER, "N");
				}
			} else {
				task.setParameterValue(BirtParamNames.P_SHOW_PARAMS, "Y");
				task.setParameterValue(BirtParamNames.P_SHOW_HEADER, "Y");
				task.setParameterValue(BirtParamNames.P_SHOW_FOOTER, "Y");
			}
		}

	}

	public ICustomReportEngine getCustomEngine(BaseReport report) {
		// for (int i = 0; i < customRepEngines.length; i++) {
		// if (customRepEngines[i].shouldBeUsed(report))
		// return customRepEngines[i];
		// }
		return null;
	}

	public IReportModifier getReportModifier(BaseReport report) {
		// for (int i = 0; i < reportModifiers.length; i++) {
		// if (reportModifiers[i].shouldBeUsed(report))
		// return reportModifiers[i];
		// }
		return null;
	}

	public List<IReportDesignModifier> getReportDesignModifier(BaseReport report) {
		List<IReportDesignModifier> result = new ArrayList<IReportDesignModifier>();
		// for (int i = 0; i < reportDesignModifiers.length; i++) {
		// if (reportDesignModifiers[i].shouldBeUsed(report))
		// result.add(reportDesignModifiers[i]);
		// }
		return result;
	}

	private IReportOutputModifier getReportOutputModifier(BaseReport report) {
		// for (int i = 0; i < reportOutputModifiers.length; i++) {
		// if (reportOutputModifiers[i].shouldBeUsed(report))
		// return reportOutputModifiers[i];
		// }
		return null;
	}

	public IReportExecutionObject getReportExecObj() {
		return this.runResult;
	}
}