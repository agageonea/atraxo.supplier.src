package com.atraxo.reportcenter.engine.base;

@SuppressWarnings("serial")
public class ReportCenterException extends RuntimeException {
    public ReportCenterException() {
	super();
    }

    public ReportCenterException(String message) {
	super(message);
    }

    public ReportCenterException(String message, Throwable cause) {
	super(message, cause);
    }

    public ReportCenterException(Throwable cause) {
	super(cause);

    }
}