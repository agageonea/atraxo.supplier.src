/**
 * Copyright (C) FuelPlus GmbH 
 * $Revision: 76832 $  
 * $Date:   11/16/2005 11:35:09 AM$ 
 * $Author: fc $  
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/base/IReportOutputModifier.java $  
 */
package com.atraxo.reportcenter.engine.birt.model.base;

import org.eclipse.birt.report.engine.api.IRenderTask;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.RenderOption;

public interface IReportOutputModifier {

    public boolean shouldBeUsed(BaseReport report);

    public void modify(BaseReport report, RenderOption options);

    public void modifyRunAndRenderTask(IRenderTask task);

}
