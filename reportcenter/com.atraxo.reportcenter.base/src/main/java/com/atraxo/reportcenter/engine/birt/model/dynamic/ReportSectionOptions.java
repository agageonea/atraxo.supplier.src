package com.atraxo.reportcenter.engine.birt.model.dynamic;

import javax.xml.bind.annotation.XmlElement;

public class ReportSectionOptions {

    private boolean headersVisible = true;
    private boolean simpleTable = false;

    @XmlElement(name = "visibleHeader", defaultValue = "true")
    public boolean isHeadersVisible() {
	return headersVisible;
    }

    public void setHeadersVisible(boolean headersVisible) {
	this.headersVisible = headersVisible;
    }

    @XmlElement(name = "simpleTable", defaultValue = "false")
    public boolean isSimpleTable() {
	return simpleTable;
    }

    public void setSimpleTable(boolean simpleTable) {
	this.simpleTable = simpleTable;
    }

}
