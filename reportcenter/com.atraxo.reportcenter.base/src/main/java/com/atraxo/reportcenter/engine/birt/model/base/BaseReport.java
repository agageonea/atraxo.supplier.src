/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 77121 $
 * $Date: 11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL:
 * http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/base/BaseReport.java
 * $
 */
package com.atraxo.reportcenter.engine.birt.model.base;

import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.naming.NamingException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.birt.report.engine.api.DocxRenderOption;
import org.eclipse.birt.report.engine.api.EXCELRenderOption;
import org.eclipse.birt.report.engine.api.HTMLRenderOption;
import org.eclipse.birt.report.engine.api.IGetParameterDefinitionTask;
import org.eclipse.birt.report.engine.api.IParameterDefnBase;
import org.eclipse.birt.report.engine.api.IRenderOption;
import org.eclipse.birt.report.engine.api.IRenderTask;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.IRunTask;
import org.eclipse.birt.report.engine.api.IScalarParameterDefn;
import org.eclipse.birt.report.engine.api.PDFRenderOption;
import org.eclipse.birt.report.engine.api.RenderOption;
import org.eclipse.birt.report.model.api.OdaDataSourceHandle;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.birt.report.model.api.activity.SemanticException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.spudsoft.birt.emitters.excel.ExcelEmitter;

import com.atraxo.reportcenter.birt.emitter.csv.CSVRenderOption;
import com.atraxo.reportcenter.engine.base.ReportCenterException;
import com.atraxo.reportcenter.engine.birt.BirtParamNames;
import com.atraxo.reportcenter.engine.birt.model.base.schedule.ScheduleData;
import com.atraxo.reportcenter.util.CsvUtil;

public abstract class BaseReport extends AbstractReport {

	public static final String NO_CODE = "no_code";

	private static final Logger LOG = LoggerFactory.getLogger(BaseReport.class);

	private String					code			= NO_CODE;
	private String					UserAndPass		= null;
	private Boolean					isPassEncoded	= false;
	private String					title			= null;								// "No title";
	private ReportPageOrientation	orientation		= ReportPageOrientation.LANDSCAPE;
	private ReportOutputFormat		format			= ReportOutputFormat.PDF;
	private String					localeCode		= "en";
	private List<XmlDataSource>		datasources		= new ArrayList<XmlDataSource>();
	// private HttpServletRequest request;
	private String				clientTime			= null;
	private ReportFormatOptions	formatOptions;
	private String				csvFieldSeparator	= CsvUtil.DEFAULT_FIELD_SEPARATOR;
	private String				csvQuoteChar		= CsvUtil.DEFAULT_QUOTE_CHAR;
	private String				csvDateFmt			= CsvUtil.DEFAULT_DATE_FMT;
	private String				csvDecimalSep		= CsvUtil.DEFAULT_DEC_SEP;
	private String				csvThousandSep		= CsvUtil.DEFAULT_TH_SEP;
	private ReportRunMode		runMode				= new ReportRunMode();

	private String	jdbcDbUrl;
	private String	jdbcUsername;
	private String	jdbcPassword;
	private String	requestUrl;

	private boolean cleanBeforeRun = false;

	private ScheduleData scheduleData;

	private Connection jdbcConnection;

	// private String dateFormat;

	@XmlElement(name = "fmtOpt", required = false)
	public ReportFormatOptions getFormatOptions() {
		return formatOptions;
	}

	public void setFormatOptions(ReportFormatOptions formatOptions) {
		this.formatOptions = formatOptions;
	}

	@XmlElement(name = "code", required = true)
	public String getCode() {
		return code;
	}

	@XmlElement(name = "orientation", required = false)
	public ReportPageOrientation getOrientation() {
		return orientation;
	}

	public void setOrientation(ReportPageOrientation orientation) {
		this.orientation = orientation;
	}

	public void setCleanBeforeRun(boolean value) {
		this.cleanBeforeRun = value;
	}

	@XmlElement(name = "clean", required = false)
	public boolean getCleanBeforeRun() {
		return this.cleanBeforeRun;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@XmlElement(name = "title", required = false)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@XmlElement(name = "format", required = true)
	public ReportOutputFormat getFormat() {
		return format;
	}

	public void setFormat(ReportOutputFormat format) {
		this.format = format;
	}

	@XmlElement(name = "P_CSV_FIELD_SEP", required = false)
	public String getCsvFieldSeparator() {
		if ((this.csvFieldSeparator == null) || "".equals(this.csvFieldSeparator)) {
			this.csvFieldSeparator = CsvUtil.DEFAULT_FIELD_SEPARATOR;
		}
		return this.csvFieldSeparator;
	}

	public void setCsvFieldSeparator(String csvFieldSeparator) {
		this.csvFieldSeparator = csvFieldSeparator;
	}

	@XmlElement(name = "P_CSV_QUOTE_STR")
	public String getCsvQuoteChar() {
		return this.csvQuoteChar;
	}

	public void setCsvQuoteChar(String csvQuoteChar) {
		this.csvQuoteChar = csvQuoteChar;
	}

	@XmlElement(name = "P_CSV_DATE_FMT")
	public String getCsvDateFmt() {
		return this.csvDateFmt;
	}

	public void setCsvDateFmt(String csvDateFmt) {
		this.csvDateFmt = csvDateFmt;
	}

	@XmlElement(name = "P_CSV_DEC_SEP")
	public String getCsvDecimalSep() {
		return this.csvDecimalSep;
	}

	public void setCsvDecimalSep(String csvDecSep) {
		this.csvDecimalSep = csvDecSep;
	}

	@XmlElement(name = "P_CSV_TH_SEP")
	public String getCsvThousandSep() {
		return this.csvThousandSep;
	}

	public void setCsvThousandSep(String csvThousandSep) {
		this.csvThousandSep = csvThousandSep;
	}

	@XmlElement(name = "run", required = false)
	public ReportRunMode getRunMode() {
		return runMode;
	}

	public void setRunMode(ReportRunMode runMode) {
		this.runMode = runMode;
	}

	public boolean shouldRunLater() {
		if (runMode != null)
			return runMode.getLater();
		return false;
	}

	@XmlElementWrapper(name = "datasources", required = false)
	@XmlElement(name = "datasource", required = true)
	public List<XmlDataSource> getDatasources() {
		return this.datasources;
	}

	public void setDatasources(List<XmlDataSource> datasources) {
		this.datasources = datasources;
	}

	@XmlElement(name = "locale", required = false, defaultValue = "en")
	public String getLocaleCode() {
		return localeCode;
	}

	public void setLocaleCode(String localeCode) {
		this.localeCode = localeCode;
	}

	@XmlElement(name = "clientTime", required = false)
	public String getClientTime() {
		return clientTime;
	}

	public void setClientTime(String clientTime) {
		this.clientTime = clientTime;
	}

	public Date getClientTimestamp() {
		if (getClientTime() == null)
			return null;

		clientTime = getClientTime().trim();
		if ("".equals(clientTime))
			return null;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			return sdf.parse(clientTime);
		} catch (Exception e) {
			return null;
		}
	}

	public final void getDbConnectionParams() throws Exception {
		jdbcDbUrl = getReportUtils().getDatabaseURL();
		jdbcUsername = null;
		jdbcPassword = null;

		String[] userAndPass = getReportUtils().splitUserPass(getUserAndPass());

		if (userAndPass != null) {
			jdbcUsername = userAndPass[0];
			jdbcPassword = userAndPass[1];
			if (getIsPassEncoded()) {
				jdbcPassword = getReportUtils().decodePassword(jdbcPassword);
				if ((jdbcPassword == null) || ("".equals(jdbcPassword))) {
					throw new ReportCenterException(new Exception("Invalid username and/or password"));
				}
			}
		}

		this.jdbcConnection = getReportUtils().getConnection(jdbcDbUrl, jdbcUsername, jdbcPassword);
	}

	public void init() throws ReportCenterException {
		super.init();
		LOG.debug("Init report");

		if (this.getDatasources().size() > 0) {
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSS");
			LOG.debug("XML datasources found");
			LOG.debug("Start downloading files");
			for (int i = 0; i < getDatasources().size(); i++) {
				XmlDataSource ds = getDatasources().get(i);
				ds.setLocalXmlFile(getReportCenterConfig().getTempPath() + df.format(new Date()) + i + ".xml");
				LOG.debug("Download XML file from " + ds.getUrl() + " to local path " + ds.getLocalXmlFile());
				getFileUtils().downloadXml(ds.getUrl(), ds.getLocalXmlFile(), null);
				LOG.debug("File download terminated");
			}
			LOG.debug("Finish downloading files");
		}

	}

	public void finish() {
		LOG.debug("Finish report");
	}

	public abstract String getReportFileName();

	public RenderOption getRenderOptions(OutputStream outputStream) {
		if (getFormat() == null)
			setFormat(ReportOutputFormat.PDF);

		switch (getFormat()) {
			case PDF:
				RenderOption optionsPDF = new PDFRenderOption();
				optionsPDF.setOutputFormat(HTMLRenderOption.OUTPUT_FORMAT_PDF);
				optionsPDF.setOutputStream(outputStream);
				return optionsPDF;
			case CSV:
				CSVRenderOption optionsCSV = new CSVRenderOption();
				optionsCSV.setOutputFormat("csv");

				optionsCSV.setOption(CSVRenderOption.FIELD_SEPARATOR, getCsvFieldSeparator());
				optionsCSV.setOption(CSVRenderOption.QUOTE, getCsvQuoteChar());
				optionsCSV.setOption(CSVRenderOption.DATE_FMT, getCsvDateFmt());
				optionsCSV.setOption(CSVRenderOption.DEC_SEP, getCsvDecimalSep());
				optionsCSV.setOption(CSVRenderOption.TH_SEP, getCsvThousandSep());

				optionsCSV.setOutputStream(outputStream);
				return optionsCSV;
			case HTML:
				HTMLRenderOption optionsHTML = new HTMLRenderOption();
				optionsHTML.setOutputFormat(HTMLRenderOption.OUTPUT_FORMAT_HTML);
				// if (getRequestUrl() != null) {
				// String url = getRequestUrl().replace("/run", "");
				// optionsHTML.setBaseImageURL(url + "/temp");
				// }

				// optionsHTML.setImageHandler(new HTMLServerImageHandler());
				// optionsHTML.setImageDirectory(getReportCenterConfig().getTempPath());

				// optionsHTML.setEnableMetadata(true);
				optionsHTML.setEnableMetadata(true);
				optionsHTML.setEnableInlineStyle(true);
				optionsHTML.setEnableAgentStyleEngine(true);
				optionsHTML.setEmbeddable(false);
				optionsHTML.setHtmlPagination(true);
				optionsHTML.setWrapTemplateTable(false);
				optionsHTML.setHtmlRtLFlag(false);
				optionsHTML.setMasterPageContent(true);

				optionsHTML.setOutputStream(outputStream);
				return optionsHTML;
			case XLSX:
				EXCELRenderOption optionsXLSX = new EXCELRenderOption();

				optionsXLSX.setOption(IRenderOption.EMITTER_ID, "uk.co.spudsoft.birt.emitters.excel.XlsxEmitter");
				optionsXLSX.setOption(IRenderOption.OUTPUT_FORMAT, "xlsx");
				optionsXLSX.setOption(ExcelEmitter.FORCEAUTOCOLWIDTHS_PROP, true);
				optionsXLSX.setOption(ExcelEmitter.SINGLE_SHEET, true);
				optionsXLSX.setOption(ExcelEmitter.SINGLE_SHEET_PAGE_BREAKS, false);
				optionsXLSX.setOption(ExcelEmitter.PRINT_BREAK_AFTER, false);
				optionsXLSX.setOption(ExcelEmitter.DISABLE_GROUPING, true);
				optionsXLSX.setOption(ExcelEmitter.REMOVE_BLANK_ROWS, true);
				optionsXLSX.setOption(ExcelEmitter.AUTO_FILTER, false);

				// optionsXLS.setImageHandler(new HTMLServerImageHandler());

				optionsXLSX.setOutputStream(outputStream);

				return optionsXLSX;
			case XLS:
				EXCELRenderOption optionsXLS = new EXCELRenderOption();
				// optionsXLS.setImageHandler(new HTMLServerImageHandler());

				optionsXLS.setOption(IRenderOption.EMITTER_ID, "uk.co.spudsoft.birt.emitters.excel.XlsEmitter");
				optionsXLS.setOutputFormat("xls_spudsoft");
				optionsXLS.setOption(ExcelEmitter.FORCEAUTOCOLWIDTHS_PROP, true);
				optionsXLS.setOption(ExcelEmitter.SINGLE_SHEET, true);
				optionsXLS.setOption(ExcelEmitter.SINGLE_SHEET_PAGE_BREAKS, false);
				optionsXLS.setOption(ExcelEmitter.PRINT_BREAK_AFTER, false);
				optionsXLS.setOption(ExcelEmitter.DISABLE_GROUPING, true);
				optionsXLS.setOption(ExcelEmitter.REMOVE_BLANK_ROWS, true);
				optionsXLS.setOption(ExcelEmitter.AUTO_FILTER, false);

				optionsXLS.setOutputStream(outputStream);

				return optionsXLS;
			case DOC:
				RenderOption optionsDOC = new RenderOption();
				optionsDOC.setOutputFormat("doc");
				optionsDOC.setOutputStream(outputStream);
				return optionsDOC;
			case DOCX:
				DocxRenderOption optionsDOCX = new DocxRenderOption();
				optionsDOCX.setOutputFormat("docx");
				optionsDOCX.setOutputStream(outputStream);
				return optionsDOCX;
			case XML:
				break;
			default:
				break;
		}

		return null;
	}

	public String getRequestUrl() {
		return this.requestUrl;
	}

	public void setRequestUrl(String url) {
		this.requestUrl = url;
	}

	public String getHeaderContentDisposition() {
		if (getFormat() == null)
			setFormat(ReportOutputFormat.PDF);

		String tmpFileName = getOutputFileName();

		switch (getFormat()) {
			case PDF:
				return "attachment; filename=\"" + tmpFileName + ".pdf\"";
			case CSV:
				return "attachment; filename=\"" + tmpFileName + ".csv\"";
			case HTML:
				return "inline; filename=\"" + tmpFileName + ".html\"";
			case XLS:
				return "attachment; filename=\"" + tmpFileName + ".xls\"";
			case XLSX:
				return "attachment; filename=\"" + tmpFileName + ".xlsx\"";
			case DOC:
				return "attachment; filename=\"" + tmpFileName + ".doc\"";
			case DOCX:
				return "attachment; filename=\"" + tmpFileName + ".docx\"";
			case XML:
				return "attachment; filename=\"" + tmpFileName + ".xml\"";
			case FLU:
				return "attachment; filename=\"" + tmpFileName + ".csv\"";
		}

		return null;
	}

	public String getFileExtension() {
		if (getFormat() == null)
			setFormat(ReportOutputFormat.PDF);
		switch (getFormat()) {
			case PDF:
				return ".pdf";
			case CSV:
				return ".csv";
			case HTML:
				return ".html";
			case XLS:
				return ".xls";
			case XLSX:
				return ".xlsx";
			case DOC:
				return ".doc";
			case DOCX:
				return ".docx";
			case XML:
				return ".xml";
			case FLU:
				return ".csv";
		}

		return null;
	}

	public String getContentType() {
		if (getFormat() == null)
			setFormat(ReportOutputFormat.PDF);

		switch (getFormat()) {
			case PDF:
				return "application/pdf";
			case CSV:
				return "text/csv";
			case HTML:
				return "text/html";
			case XLS:
				return "application/vnd.ms-excel";
			case XLSX:
				return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
			case DOC:
				return "application/msword";
			case DOCX:
				return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
			case XML:
				return "text/xml";
			case FLU:
				return "text/plaintext";
		}

		return null;
	}

	public String getOutputFileName() {
		return getCode();
	}

	public void modifyReport(ReportDesignHandle reportDesignHandle) throws ReportCenterException {
		if (getDatasources().size() > 0) {
			LOG.debug("Report data is taken from XML files");
			LOG.debug("Assign XML files to data sources in the report");
			for (int i = 0; i < getDatasources().size(); i++) {
				XmlDataSource ds = getDatasources().get(i);
				LOG.debug("Find if data source with name " + ds.getName() + " is defined in the report");

				boolean found = false;
				Iterator<?> it = reportDesignHandle.getDataSources().iterator();
				while (it.hasNext()) {
					Object o = it.next();
					if (o instanceof OdaDataSourceHandle) {
						OdaDataSourceHandle dsHandle = (OdaDataSourceHandle) o;
						if ("org.eclipse.datatools.enablement.oda.xml".equals(dsHandle.getExtensionID())) {
							if (dsHandle.getName().equals(ds.getName())) {
								try {
									dsHandle.setProperty("FILELIST", ds.getLocalXmlFile());
									found = true;
									LOG.debug("Data source with name " + ds.getName()
									        + " associated with local XML file " + ds.getLocalXmlFile());
									break;
								} catch (SemanticException e) {
									LOG.debug(
									        "Error when assigning XML file to the report data source " + ds.getName());
								}
							}
						}
					}
				}

				if (!found) {
					LOG.debug("Data source " + ds.getName() + " could not be found in the report !!!");
					throw new ReportCenterException(new Exception("Invalid report call. Data source with name "
					        + ds.getName() + " could not be found in the report definition."));
				}
			}
		}
	}

	public void assignParametersToReport(IReportEngine engine, IReportRunnable design, IRenderTask task)
	        throws ReportCenterException {
		task.setParameterValue(BirtParamNames.P_REPORT_DYN_TITLE, getTitle());
		task.setParameterValue(BirtParamNames.P_REPORT_ORIENTATION, getOrientation());
		task.setParameterValue(BirtParamNames.P_JDBC_DEV_MODE, false);
		task.setParameterValue(BirtParamNames.P_REPORT_FORMAT, getFormat().toString().toUpperCase());
		task.setParameterValue(BirtParamNames.P_TIMESTAMP_DIFF, getReportCenterConfig().getUTCTimestampDiff());

		Date ts = getClientTimestamp();
		if (ts != null) {
			task.setParameterValue(BirtParamNames.P_REPORT_TIMESTAMP, ts);
		}

	}

	public void assignParametersToReport(IReportEngine engine, IReportRunnable design, IRunTask task)
	        throws ReportCenterException {
		task.setParameterValue(BirtParamNames.P_REPORT_DYN_TITLE, getTitle());
		task.setParameterValue(BirtParamNames.P_REPORT_ORIENTATION, getOrientation());
		task.setParameterValue(BirtParamNames.P_JDBC_DEV_MODE, false);
		task.setParameterValue(BirtParamNames.P_REPORT_FORMAT, getFormat().toString().toUpperCase());
		task.setParameterValue(BirtParamNames.P_TIMESTAMP_DIFF, getReportCenterConfig().getUTCTimestampDiff());

		Date ts = getClientTimestamp();
		if (ts != null) {
			task.setParameterValue(BirtParamNames.P_REPORT_TIMESTAMP, ts);
		}

	}
	
	public String generateTempFileName() {
		return "tmp" + getDateUtils().dateAsString(new Date(), "yyyyMMddHHmmssSS");
	}

	public String getUserAndPass() {
		return UserAndPass;
	}

	public void setUserAndPass(String userAndPass) {
		UserAndPass = userAndPass;
	}

	public void setParameter(IReportEngine engine, IReportRunnable design, IRunAndRenderTask task, String paramName,
	        Object value) throws ReportCenterException {
		try {
			IGetParameterDefinitionTask paramTask = engine.createGetParameterDefinitionTask(design);

			@SuppressWarnings("unchecked")
			Collection<IParameterDefnBase> params = paramTask.getParameterDefns(true);
			Iterator<IParameterDefnBase> iterator = params.iterator();

			while (iterator.hasNext()) {
				IParameterDefnBase param = iterator.next();

				if (param instanceof IScalarParameterDefn) {
					IScalarParameterDefn scalar = (IScalarParameterDefn) param;
					if (scalar.getName().toUpperCase().equals(paramName.toUpperCase())) {
						task.setParameterValue(scalar.getName(), value);
						paramTask.close();
						return;
					}
				}
			}
			paramTask.close();

			throw new ReportCenterException(
			        new Exception("Parameter " + paramName + " not defined for report " + getCode()));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new ReportCenterException(e);
		}
	}

	public Boolean getIsPassEncoded() {
		return isPassEncoded;
	}

	public void setIsPassEncoded(Boolean isPassEncoded) {
		this.isPassEncoded = isPassEncoded;
	}

	public boolean isFuelPlusReport() {
		return true;
	}

	public String getJdbcDbUrl() {
		return jdbcDbUrl;
	}

	@XmlTransient
	public String getJdbcUsername() {
		return jdbcUsername;
	}

	public void setJdbcUsername(String jdbcUsername) {
		this.jdbcUsername = jdbcUsername;
	}

	public String getJdbcPassword() {
		return jdbcPassword;
	}

	@XmlElement(name = "sd", required = false)
	public ScheduleData getScheduleData() {
		return scheduleData;
	}

	public void setScheduleData(ScheduleData scheduleData) {
		this.scheduleData = scheduleData;
	}

	public boolean isConnectionNull() {
		return (this.jdbcConnection == null);
	}

	public Connection getConnection() throws NamingException, SQLException {
		return this.jdbcConnection;
	}

	@Override
	public String getFormatAsString() {
		String result = "???";
		if (getFileExtension() != null) {
			result = getFileExtension();
			if (result == null)
				result = getFormat().toString();
		}
		if (result == null)
			result = "???";
		return result;
	}

	public void validate() throws ReportCenterException {
		if (getFormat() == null)
			throw new ReportCenterException("Invalid format provided");
	}

}
