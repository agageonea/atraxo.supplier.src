package com.atraxo.reportcenter.engine.base;

import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;

public interface IReportCenterCleaner {

    public void execute(BaseReport report, String rid, boolean devMode) throws ReportCenterException;
}
