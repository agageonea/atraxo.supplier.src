/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 77121 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/layout/DynamicDesignCellTableData.java $
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic.layout;

import java.text.ParseException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;
import com.atraxo.reportcenter.engine.birt.model.dynamic.IDynamicReportData;
import com.ibm.icu.text.SimpleDateFormat;

public class DynamicDesignCellTableData implements IDynamicReportData {
    
    private static final Logger LOG = LoggerFactory.getLogger(DynamicDesignCellTableData.class);
    
    private String text;
    private String dataType = null;
    private String styleName = null;
    private boolean label = false;
    private String fmt = null;
    private BaseReport report;

    public DynamicDesignCellTableData(BaseReport report) {
	this.report = report;
    }

    public String getText() {
	return text;
    }

    public void setText(String text) {
	this.text = text;
    }

    public boolean isLabel() {
	return label;
    }

    public void setLabel(boolean label) {
	this.label = label;
    }

    public String getDataType() {
	dataType = (dataType != null) ? dataType.toUpperCase() : "STRING";

	return dataType;
    }

    public void setDataType(String dataType) {
	this.dataType = dataType;
    }

    public String getStyleName() {
	return styleName;
    }

    public void setStyleName(String styleName) {
	this.styleName = styleName;
    }

    public String getFormat() {
	return fmt;
    }

    public void setFormat(String fmt) {
	this.fmt = fmt;
    }

    public String getBirtDataType() {
	return report.getReportUtils().internalDataType2BirtDataType(this.getDataType());
    }

    private static SimpleDateFormat sdfIn = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static SimpleDateFormat sdfOut = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    public String getBirtValue() {
	String aText = getText();

	if ((aText == null) || ("".equals(aText)))
	    return "null";

	String dtyp = getDataType();

	if ("NUMBER".equals(dtyp)) {
	    return aText;
	} else if ("INTEGER".equals(dtyp)) {
	    return aText;
	} else if ("DATE".equals(dtyp))
	    return "'" + aText + "'";
	else if ("DATETIME".equals(dtyp)) {
	    Date inDt;
	    try {
		inDt = sdfIn.parse(aText);
		String outVal = sdfOut.format(inDt);
		return "'" + outVal + "'";
	    } catch (ParseException e) {
		LOG.debug(e.getMessage(), e);
	    }
	    return null;
	} else {
	    aText = aText.replace("\n", "\\n");
	    aText = aText.replace("\r", "\\n");
	    return "'" + aText + "'";
	}
    }

}
