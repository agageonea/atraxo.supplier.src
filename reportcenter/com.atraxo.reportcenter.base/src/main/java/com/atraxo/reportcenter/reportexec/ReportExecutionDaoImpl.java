package com.atraxo.reportcenter.reportexec;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

public class ReportExecutionDaoImpl extends JdbcDaoSupport implements IReportExecutionDao<Long> {

    private static final Logger LOG = LoggerFactory.getLogger(ReportExecutionDaoImpl.class);

    @Override
    public List<IReportExecutionObject> getAll() {
	List<IReportExecutionObject> result = getJdbcTemplate().query(ReportExecutionConstants.SQL_LIST_ALL, new ReportExecutionRowMapper());
	return result;
    }

    @Override
    public IReportExecutionObject getById(Long id) {
	IReportExecutionObject result = getJdbcTemplate().queryForObject(ReportExecutionConstants.SQL_GET_BY_ID, new Object[] { id }, new ReportExecutionRowMapper());
	return result;
    }

    @Override
    public Long insert(final IReportExecutionObject rptExec) throws Exception {
	KeyHolder keyHolder = new GeneratedKeyHolder();
	try {
	    getJdbcTemplate().update(new PreparedStatementCreator() {

		@Override
		public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
		    PreparedStatement ps = con.prepareStatement(ReportExecutionConstants.SQL_INSERT, Statement.RETURN_GENERATED_KEYS);

//		    java.sql.Timestamp startDt = null;
//		    if (rptExec.getStartDt() != null)
//			startDt = new Timestamp(rptExec.getStartDt().getTime());
//		    java.sql.Timestamp endDt = null;
//		    if (rptExec.getEndDt() != null)
//			endDt = new Timestamp(rptExec.getEndDt().getTime());

		    ps.setString(1, rptExec.getName()); // rpt_name
		    ps.setString(2, rptExec.getTitle()); // rpt_ttl
		    ps.setString(3, rptExec.getUser()); // usr
		    ps.setString(4, rptExec.getStatus()); // stat
		    ps.setString(5, rptExec.getUrl()); // url
		    ps.setString(6, rptExec.getErrMsg()); // err_msg
		    ps.setTimestamp(7, null); // start_dt
		    ps.setTimestamp(8, null); // end_dt
		    ps.setString(9, rptExec.getFilename()); // filename

		    return ps;
		}
	    }, keyHolder);
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    throw e;
	}

	final long result = keyHolder.getKey().longValue();

	// Send another update as timestamps saved with 00:00 for time allways
	getJdbcTemplate().update(
		    ReportExecutionConstants.SQL_UPDATE_BY_ID, //@formatter:off
		    	new Object[] {
			    rptExec.getName(),
			    rptExec.getTitle(),
			    rptExec.getUser(),
			    rptExec.getStatus(),
			    rptExec.getUrl(),
			    rptExec.getErrMsg(),
			    rptExec.getStartDt(),
			    rptExec.getEndDt(),
			    rptExec.getFilename(),
			    result}
		    	//@formatter:on
		);
	return result;
    }

    @Override
    public int update(IReportExecutionObject rptExec) {
	int result = getJdbcTemplate().update(
		    ReportExecutionConstants.SQL_UPDATE_BY_ID, //@formatter:off
		    	new Object[] {
			    rptExec.getName(),
			    rptExec.getTitle(),
			    rptExec.getUser(),
			    rptExec.getStatus(),
			    rptExec.getUrl(),
			    rptExec.getErrMsg(),
			    rptExec.getStartDt(),
			    rptExec.getEndDt(),
			    rptExec.getFilename(),
			    rptExec.getExecId()}
		    	//@formatter:on
		);
	return result;
    }

    @Override
    public int delete(Long id) {
	int result = getJdbcTemplate().update(ReportExecutionConstants.SQL_DELETE_BY_ID, id);
	return result;
    }

}
