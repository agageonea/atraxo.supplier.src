/**
 * Copyright (C) FuelPlus GmbH 
 * $Revision: 77121 $  
 * $Date:   11/16/2005 11:35:09 AM$ 
 * $Author: fc $  
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/XmlQuery.java $  
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XmlQuery {

    private String xmlFile;
    private String query;

    private List<IXmlQueryHandler> handlers = new ArrayList<IXmlQueryHandler>();

    public XmlQuery() {
	super();
    }

    public String getXmlFile() {
	return xmlFile;
    }

    public String getQuery() {
	return query;
    }

    public void process() throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
	// DocumentBuilderFactory domFactory = DocumentBuilderFactory
	// .newInstance();
	// domFactory.setNamespaceAware(true);
	// DocumentBuilder builder = domFactory.newDocumentBuilder();
	// Document doc = builder.parse(xmlFile);
	XPathFactory factory = XPathFactory.newInstance();
	XPath xpath = factory.newXPath();

	// XPathExpression expr =
	// xpath.compile("//reportData/columnDef/column");
	XPathExpression expr = xpath.compile(query);
	XPathExpression expr1 = xpath.compile("./*");

	InputSource inputSrc = new InputSource(xmlFile);
	Object result = expr.evaluate(inputSrc, XPathConstants.NODESET);

	NodeList nodes = (NodeList) result;

	for (int i = 0; i < nodes.getLength(); i++) {
	    Node node = nodes.item(i);

	    // Call start node handlers
	    Iterator<IXmlQueryHandler> itSta = handlers.iterator();
	    while (itSta.hasNext()) {
		IXmlQueryHandler h = itSta.next();
		h.onNodeStart(node, node.getNodeName(), node.getNodeValue());
	    }

	    Object result1 = expr1.evaluate(node, XPathConstants.NODESET);
	    NodeList nodes1 = (NodeList) result1;
	    for (int j = 0; j < nodes1.getLength(); j++) {
		Node node1 = nodes1.item(j);
		// Call child node handlers
		Iterator<IXmlQueryHandler> itCh = handlers.iterator();
		while (itCh.hasNext()) {
		    IXmlQueryHandler h = itCh.next();
		    h.onChildNode(node1, node1.getNodeName(), node1.getNodeValue());
		}
	    }

	    // Call end node handlers
	    Iterator<IXmlQueryHandler> itEnd = handlers.iterator();
	    while (itEnd.hasNext()) {
		IXmlQueryHandler h = itEnd.next();
		h.onNodeEnd(node);
	    }

	}

    }

    public void setXmlFile(String xmlFile) {
	this.xmlFile = xmlFile;
    }

    public void setQuery(String query) {
	this.query = query;
    }

    public void addXmlQueryHandler(IXmlQueryHandler handler) {
	if (!handlers.contains(handler)) {
	    handlers.add(handler);
	}
    }

    public void removeXmlQueryHandler(IXmlQueryHandler handler) {
	if (handlers.contains(handler)) {
	    handlers.remove(handler);
	}
    }

}
