/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 77121 $
 * $Date: 11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL:
 * http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/DynamicReport.java
 * $
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.birt.report.engine.api.IRenderTask;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunTask;
import org.eclipse.birt.report.model.api.CellHandle;
import org.eclipse.birt.report.model.api.ColumnHandle;
import org.eclipse.birt.report.model.api.DataItemHandle;
import org.eclipse.birt.report.model.api.DesignElementHandle;
import org.eclipse.birt.report.model.api.ElementFactory;
import org.eclipse.birt.report.model.api.GridHandle;
import org.eclipse.birt.report.model.api.LabelHandle;
import org.eclipse.birt.report.model.api.MasterPageHandle;
import org.eclipse.birt.report.model.api.OdaDataSetHandle;
import org.eclipse.birt.report.model.api.OdaDataSourceHandle;
import org.eclipse.birt.report.model.api.PropertyHandle;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.birt.report.model.api.RowHandle;
import org.eclipse.birt.report.model.api.SlotHandle;
import org.eclipse.birt.report.model.api.StructureFactory;
import org.eclipse.birt.report.model.api.StyleHandle;
import org.eclipse.birt.report.model.api.TableHandle;
import org.eclipse.birt.report.model.api.TextItemHandle;
import org.eclipse.birt.report.model.api.activity.SemanticException;
import org.eclipse.birt.report.model.api.elements.DesignChoiceConstants;
import org.eclipse.birt.report.model.api.elements.structures.ComputedColumn;
import org.eclipse.birt.report.model.api.elements.structures.DateFormatValue;
import org.eclipse.birt.report.model.api.elements.structures.FilterCondition;
import org.eclipse.birt.report.model.api.elements.structures.FormatValue;
import org.eclipse.birt.report.model.api.elements.structures.HideRule;
import org.eclipse.birt.report.model.api.elements.structures.NumberFormatValue;
import org.eclipse.birt.report.model.api.elements.structures.OdaResultSetColumn;
import org.eclipse.birt.report.model.elements.ReportItem;
import org.eclipse.birt.report.model.elements.Style;
import org.eclipse.birt.report.model.elements.interfaces.IDataSetModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atraxo.reportcenter.engine.base.ReportCenterException;
import com.atraxo.reportcenter.engine.birt.BirtParamNames;
import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;
import com.atraxo.reportcenter.engine.birt.model.dynamic.layout.DynamicDesignCellTableData;
import com.atraxo.reportcenter.engine.birt.model.dynamic.layout.DynamicDesignRowTableData;
import com.atraxo.reportcenter.engine.birt.model.dynamic.layout.DynamicDesignTableData;
import com.atraxo.reportcenter.engine.birt.model.dynamic.parser.DynamicReportXMLParser;

@XmlRootElement(name = "dynamic-report")
public class DynamicReport extends BaseReport {

	private static final Logger LOG = LoggerFactory.getLogger(DynamicReport.class);

	private List<ReportSection> sections = new ArrayList<ReportSection>();

	private String userName = null;

	private DynamicReportXMLParser xmlParser;

	private HashMap<ReportSection, DynamicReportColumns>		xmlDataColumns		= new HashMap<ReportSection, DynamicReportColumns>();
	private HashMap<ReportSection, DynamicReportTotals>			xmlDataTotals		= new HashMap<ReportSection, DynamicReportTotals>();
	private HashMap<ReportSection, DynamicReportQueryParams>	xmlDataQryParams	= new HashMap<ReportSection, DynamicReportQueryParams>();

	private DynamicReportUser	xmlDataUser;
	private DynamicReportCode	xmlDataReportCode;
	private DynamicReportTitle	xmlDataReportTitle;

	@XmlElementWrapper(name = "sections")
	@XmlElement(name = "section", required = true)
	public List<ReportSection> getSections() {
		return sections;
	}

	public void setSections(List<ReportSection> sections) {
		this.sections = sections;
	}

	@Override
	public String getReportFileName() {
		return "/DynamicReports/dyna_ql.rptdesign";
	}

	@XmlElement(name = "title", required = true)
	@Override
	public String getTitle() {
		return super.getTitle();
	}

	@Override
	public void init() throws ReportCenterException {
		super.init();

		// Get remote xml file url
		Collections.sort(getSections(), new Comparator<ReportSection>() {

			public int compare(ReportSection f1, ReportSection f2) {
				if (f1.getOrder() < f2.getOrder())
					return -1;
				else if (f1.getOrder() > f2.getOrder())
					return 1;
				return 0;
			}
		});

		// for (int i = 0; i < getSections().size(); i++) {
		// remoteXmlUrl.add(getSections().get(i).getUrl());
		// }

		LOG.debug("Generate dynamic report using XML file(s) ");

		// Construct local xml path
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSS");
		for (int i = 0; i < sections.size(); i++) {
			ReportSection section = sections.get(i);
			if (section.getUrl().getLocalXmlFile() == null) {

				String localXmlFile = getReportCenterConfig().getTempPath() + df.format(new Date()) + i + ".xml";

				section.getUrl().setLocalXmlFile(localXmlFile);

				if (!section.getUrl().getUrlType().equals(ReportSectionUrlType.INLINE)) {
					LOG.debug("Download XML file from " + section.getUrl() + " to local path "
					        + section.getUrl().getLocalXmlFile());
					String remoteFile = section.getUrl().getContent().trim();
					getFileUtils().downloadXml(remoteFile, section.getUrl().getLocalXmlFile(), section.getServerId());
					LOG.debug("File download terminated");
				} else {
					LOG.debug("Creating file : " + section.getUrl().getLocalXmlFile());
					getFileUtils().createXmlFile(section.getUrl().getLocalXmlFile(), section.getUrl().getContent());
					LOG.debug("File created");
				}

			}
		}

		xmlParser = new DynamicReportXMLParser();
		xmlDataUser = new DynamicReportUser(this);
		xmlDataReportCode = new DynamicReportCode(this);
		xmlDataReportTitle = new DynamicReportTitle(this);

		for (int i = 0; i < getSections().size(); i++) {
			ReportSection section = getSections().get(i);

			DynamicReportColumns sectColumns = new DynamicReportColumns(this);
			DynamicReportTotals sectTotals = new DynamicReportTotals(this);
			DynamicReportQueryParams sectQryParams = new DynamicReportQueryParams(this);

			if (i == 0) {
				xmlParser.setXmlFile(section.getUrl().getLocalXmlFile());
				xmlDataUser.setXmlParser(xmlParser);
				xmlDataUser.readUser();
				userName = xmlDataUser.getUser();

				if (getJdbcUsername() == null) {
					setUserAndPass(userName + ":");
				}

				xmlDataReportCode.setXmlParser(xmlParser);
				xmlDataReportCode.readReportCode();

				if (BaseReport.NO_CODE.equals(getCode())) {
					setCode(xmlDataReportCode.getReportCode());
				}

				xmlDataReportTitle.setXmlParser(xmlParser);
				xmlDataReportTitle.readReportTitle();

				if (getTitle() == null) {
					setTitle(xmlDataReportTitle.getReportTitle());
				}
			}

			sectColumns.clear();
			sectTotals.clear();
			sectQryParams.clear();

			xmlParser.setXmlFile(section.getUrl().getLocalXmlFile());
			// Set report title and orientation
			// logger.log(Level.FINEST, "Read report title");
			// internalSetTitle(xmlQuery);

			// logger.log(Level.FINEST, "Read page orientation");
			// internalOrientation(xmlQuery);

			LOG.debug("Read report columns");
			sectColumns.setXmlParser(xmlParser);
			sectColumns.readColumns();
			sectColumns.printColumns();

			LOG.debug("Read report totals");
			sectTotals.setXmlParser(xmlParser);
			sectTotals.readTotals();
			sectTotals.printTotals();

			LOG.debug("Read query parameters");
			sectQryParams.setXmlParser(xmlParser);
			sectQryParams.readParams();
			sectQryParams.printParams();

			xmlDataColumns.put(section, sectColumns);
			xmlDataTotals.put(section, sectTotals);
			xmlDataQryParams.put(section, sectQryParams);

		}

		LOG.debug("Read report code");
		if (getCode() == null) {
			setCode("dyna_ql");
		}

	}

	@Override
	public void modifyReport(ReportDesignHandle reportDesignHandle) throws ReportCenterException {
		super.modifyReport(reportDesignHandle);

		for (int j = 0; j < sections.size(); j++) {
			ReportSection section = sections.get(j);

			try {
				ElementFactory designFactory = reportDesignHandle.getElementFactory();

				String XML_DATA_SOURCE = "XMLDataSource" + j;
				String XML_DATA_SET = "XMLDataSet" + j;

				buildXMLDataSource(designFactory, reportDesignHandle, XML_DATA_SOURCE,
				        section.getUrl().getLocalXmlFile());
				buildXMLDataSet(xmlDataColumns.get(section), designFactory, reportDesignHandle, XML_DATA_SET,
				        XML_DATA_SOURCE);

				// Get first master page
				String masterPageName = "MasterPage";
				SlotHandle mps = reportDesignHandle.getMasterPages();
				if (mps.getCount() > 0) {
					MasterPageHandle mp = (MasterPageHandle) mps.get(0);
					masterPageName = mp.getName();
					mp.setOrientation(getOrientation().toString());
				}

				// Query parameters
				if ((section.getTitle() != null)
				        && ((section.getForceShowTitle()) || (xmlDataQryParams.get(section).size() > 0))) {
					GridHandle gridPar = designFactory.newGridItem("gridQueryPar" + j, 1, 1);

					LabelHandle lblQryParams = designFactory.newLabel("lblQryParams" + j);
					lblQryParams.setText(section.getTitle());
					// lblQryParams.setStyleName("qlQueryParams");
					lblQryParams.setProperty("marginBottom", "5pt");
					lblQryParams.setProperty("marginTop", "5pt");

					CellHandle cell = gridPar.getCell(0, 0);
					cell.getContent().add(lblQryParams);
					if (!section.getOptions().isSimpleTable()) {
						cell.setProperty("backgroundColor", "#000080");
						cell.setProperty("color", "#FFFFFF");
					}
					cell.setProperty("fontWeight", "bold");

					// lblQryParams.setProperty("fontWeight", "bold");
					// lblQryParams.setProperty("marginTop", "10pt");
					//
					// lblQryParams.setProperty("borderBottomColor", "#000000");
					// lblQryParams.setProperty("borderBottomStyle", "solid");
					// lblQryParams.setProperty("borderBottomWidth", "thin");

					reportDesignHandle.getBody().add(gridPar);
				}
				if (xmlDataQryParams.get(section).size() > 0) {
					DynamicDesignTableData gridParData = getGridParamsData(section, xmlDataQryParams.get(section));

					GridHandle grid = designFactory.newGridItem("gridQuery" + j, gridParData.getColsCount() * 2,
					        gridParData.getRowsCount());

					// DesignElementHandle column2 = grid.getColumns().get(1);
					// column2.setProperty("width", "30px");
					// column2.setProperty("textAlign", "center");

					for (int ri = 0; ri < gridParData.getRowsCount(); ri++) {
						DynamicDesignRowTableData rowData = gridParData.getRows().get(ri);
						for (int ci = 0; ci < gridParData.getColsCount() * 2; ci++) {
							DynamicDesignCellTableData cellData = rowData.getCells().get(ci);

							if (cellData.getDataType() == null) {
								LabelHandle lbl = designFactory.newLabel("lbl_" + ri + "_" + ci);
								lbl.setText(cellData.getText());
								if (cellData.isLabel())
									lbl.setProperty("fontWeight", "bold");
								// lbl.setProperty("textAlign", "right");

								grid.getCell(ri + 1, ci + 1).getContent().add(lbl);
							} else {
								DataItemHandle di = designFactory.newDataItem("txt_" + ri + "_" + ci);
								di.setResultSetColumn("column_" + ri + "_" + ci);
								PropertyHandle col = di.getColumnBindings();

								ComputedColumn cc = StructureFactory.createComputedColumn();
								cc.setName("column_" + ri + "_" + ci);
								cc.setDataType(cellData.getBirtDataType());
								cc.setExpression(cellData.getBirtValue());

								di.setStyleName(cellData.getStyleName());
								di.setProperty("textAlign", "left");
								if (cellData.isLabel())
									di.setProperty("fontWeight", "bold");

								setCellFormat(di, cellData);

								col.addItem(cc);

								grid.getCell(ri + 1, ci + 1).getContent().add(di);
							}
						}
					}

					grid.setProperty("marginBottom", "10pt");
					reportDesignHandle.getBody().add(grid);
				}

				if (xmlDataColumns.get(section).size() > 0) {
					// Table data
					TableHandle table = designFactory.newTableItem("table" + j, xmlDataColumns.get(section).size());
					switch (getFormat()) {
						case DOC:
						case DOCX:
							table.setPageBreakInterval(0);
							break;
						case HTML:
							table.setPageBreakInterval(22);
							break;
					}

					table.setDataSet(reportDesignHandle.findDataSet(XML_DATA_SET));

					// table.getHeader().get(0).setStyleName("listHeader");

					// table.getDetail().get(0).setStyleName("listDetail");

					// create computed columns for dataset
					PropertyHandle computedSet = table.getColumnBindings();
					ComputedColumn cs1 = null;

					for (int i = 0; i < xmlDataColumns.get(section).size(); i++) {
						DynamicReportColumn col = xmlDataColumns.get(section).getColumn(i);

						cs1 = StructureFactory.createComputedColumn();
						cs1.setName(col.getCode() + "_" + i);
						cs1.setDisplayName(col.getCode() + "_" + i);
						// cs1.setExpression("dataSetRow[\"" + col.getCode() +
						// "\"]");

						cs1.setDataType(col.getBirtDataType());
						if ("DATETIME".equals(col.getDataType()) || "DATE".equals(col.getDataType())) {
							// cs1.setDataType(DesignChoiceConstants.COLUMN_DATA_TYPE_STRING);
							// ComputedColumn cs11 =
							// createAdditionalDateTimeColumn(i, col);
							// computedSet.addItem(cs11);
							cs1.setExpression("dataSetRow[\"computedCol_" + col.getCode() + "\"]");
						} else {
							// cs1.setDataType(col.getBirtDataType());
							cs1.setExpression("dataSetRow[\"" + col.getCode() + "\"]");
						}
						computedSet.addItem(cs1);
					}

					cs1 = StructureFactory.createComputedColumn();
					cs1.setName("recType");
					cs1.setDisplayName("recType");
					cs1.setExpression("dataSetRow[\"recType\"]");
					cs1.setDataType(DesignChoiceConstants.COLUMN_DATA_TYPE_STRING);
					computedSet.addItem(cs1);

					// table header
					RowHandle tableHeaderRow = (RowHandle) table.getHeader().get(0);

					DynamicReportUtil rptUtl = DynamicReportUtil.getInstance();

					for (int i = 0; i < xmlDataColumns.get(section).size(); i++) {
						DynamicReportColumn col = xmlDataColumns.get(section).getColumn(i);
						LabelHandle lblCellTitle = designFactory.newLabel(col.getCode() + "_" + i);
						lblCellTitle.setText(rptUtl.fixNewLine(rptUtl.fixHTMLChars(col.getTitle())));

						// lblCellTitle.setProperty("fontWeight", "bold");
						// lblCellTitle.setProperty("textAlign", "left");

						CellHandle cell = (CellHandle) tableHeaderRow.getCells().get(i);

						if (col.isPageBreak()) {
							ColumnHandle tblCol = (ColumnHandle) table.getColumns().get(i);
							tblCol.setProperty(DesignChoiceConstants.CHOICE_PAGE_BREAK_AFTER,
							        DesignChoiceConstants.PAGE_BREAK_AFTER_ALWAYS);

						}

						cell.getContent().add(lblCellTitle);
					}
					if (!section.getOptions().isHeadersVisible()) {
						HideRule hideRule = StructureFactory.createHideRule();
						hideRule.setFormat("all");
						hideRule.setExpression("true");
						PropertyHandle ph = tableHeaderRow.getPropertyHandle(ReportItem.VISIBILITY_PROP);
						ph.addItem(hideRule);
					}

					// table detail
					RowHandle tableDetailRow = (RowHandle) table.getDetail().get(0);

					if (!section.getOptions().isSimpleTable())
						tableDetailRow.setStyleName("table-detail-alternate-color");

					for (int i = 0; i < xmlDataColumns.get(section).size(); i++) {
						DynamicReportColumn col = xmlDataColumns.get(section).getColumn(i);
						CellHandle cell = (CellHandle) tableDetailRow.getCells().get(i);
						DataItemHandle data = designFactory.newDataItem("data_" + col.getCode() + "_" + i);

						data.setResultSetColumn(col.getCode() + "_" + i);

						// if ("DATETIME".equals(col.getDataType()) ||
						// "DATE".equals(col.getDataType())) {
						// data.setResultSetColumn(col.getCode() + "_" + i +
						// "_dt");
						// }
						// else {
						// data.setResultSetColumn(col.getCode() + "_" + i);
						// }

						if (col.getStyleName() != null) {
							data.setStyleName(col.getStyleName());
						}

						setCellFormat(data, col);

						// data.setProperty("textAlign", col.getAlign());
						cell.getContent().add(data);

					}

					// table footer
					for (int rowNo = 0; rowNo < xmlDataTotals.get(section).size(); rowNo++) {
						RowHandle tableFooterRow = (RowHandle) table.getFooter().get(rowNo);
						if (tableFooterRow == null) {
							tableFooterRow = designFactory.newTableRow();
							for (int colNo = 0; colNo < xmlDataColumns.get(section).size(); colNo++) {
								CellHandle cell = designFactory.newCell();
								tableFooterRow.getCells().add(cell);
							}
							table.getFooter().add(tableFooterRow);
						}

						for (int colNo = 0; colNo < xmlDataColumns.get(section).size(); colNo++) {
							IDynamicReportColumn column = xmlDataColumns.get(section).getColumn(colNo);
							IDynamicReportTotal total = xmlDataTotals.get(section).getTotalForColumn(rowNo, column);
							CellHandle cell = (CellHandle) tableFooterRow.getCells().get(colNo);

							if (total != null) {
								((IDynamicReportData) total).setDataType(((DynamicReportColumn) column).getDataType());

								if (total.getLabel() != null) {
									TextItemHandle txt = designFactory
									        .newTextItem(column.getCode() + "_total_" + rowNo + "_" + colNo);
									txt.setContentType(DesignChoiceConstants.TEXT_CONTENT_TYPE_HTML);
									String content = "";

									content = content + total.getLabel();

									if (total.getValue() != null) {
										if (total.getFormat() != null) {
											content = content + "<VALUE-OF";
											content = content + " format='" + total.getFormat() + "'";
											content = content + ">";
											content = content + total.getValue();
											content = content + "</VALUE-OF>";
										} else {
											content = content + total.getValue();
										}
									}
									txt.setContent(content);
									cell.getContent().add(txt);
								} else {
									if (total.getValue() != null) {
										DataItemHandle di = designFactory
										        .newDataItem(column.getCode() + "_total_" + rowNo + "_" + colNo);
										di.setResultSetColumn(
										        "computed_" + column.getCode() + "_total_" + rowNo + "_" + colNo);
										PropertyHandle colProp = di.getColumnBindings();

										ComputedColumn cc = StructureFactory.createComputedColumn();
										cc.setName("computed_" + column.getCode() + "_total_" + rowNo + "_" + colNo);
										cc.setDataType(((DynamicReportTotal) total).getBirtDataType());
										cc.setExpression(((DynamicReportTotal) total).getBirtValue());
										setCellFormat(di, (IDynamicReportData) total);

										colProp.addItem(cc);

										cell.getContent().add(di);
									}
								}

							}
						}
					}

					// Add highlight rule for totals
					// HighlightRule hr =
					// StructureFactory.createHighlightRule();
					// hr.setOperator(DesignChoiceConstants.MAP_OPERATOR_EQ);
					// hr.setTestExpression("row[\"recType\"]");
					// hr.setValue1("\"total\"");
					// hr.setProperty(HighlightRule.FONT_WEIGHT_MEMBER, "bold");
					//
					// PropertyHandle ph =
					// tabledetail.getPropertyHandle(StyleHandle.HIGHLIGHT_RULES_PROP);
					// ph.addItem(hr);

					for (int i = 0; i < xmlDataColumns.get(section).size(); i++) {
						DynamicReportColumn col = xmlDataColumns.get(section).getColumn(i);
						DesignElementHandle tblCol = table.getColumns().get(i);
						tblCol.setProperty(DesignChoiceConstants.CHOICE_TEXT_ALIGN, col.getAlign());
						if (col.getWitdthPerc() > 0) {
							tblCol.setProperty(ReportItem.WIDTH_PROP, col.getWitdthPerc() + "%");
						}
					}

					table.setProperty(Style.MARGIN_BOTTOM_PROP, "10pt");
					table.setProperty("masterPage", masterPageName);
					// add table to report
					reportDesignHandle.getBody().add(table);
				}

				reportDesignHandle.setLayoutPreference("Fixed Layout");

			} catch (SemanticException se) {
				throw new ReportCenterException(se);
			}
		}

	}

	/*
	 * private ComputedColumn createAdditionalDateTimeColumn(int idx,
	 * DynamicReportColumn col) { ComputedColumn result = null; result =
	 * StructureFactory.createComputedColumn(); result.setName(col.getCode() +
	 * "_" + idx + "_dt"); result.setDisplayName(col.getCode() + "_" + idx +
	 * "_dt"); result.setDataType("Date Time");
	 * 
	 * String regExp =
	 * "/(\\d{4})-(\\d{2})-(\\d{2}) (\\d{2}):(\\d{2}):(\\d{2})/"; String exp =
	 * "var cellVal = row[\"" + col.getCode() + "_" + idx + "\"];\n" +
	 * "if ((cellVal == \"\") || (cellVal == null)) {\n" + "  null;\n" + "}\n" +
	 * "else {\n" + "  var dtStr =  BirtStr.trim(cellVal);\n" +
	 * "  dtStr = (dtStr.length == 10) ? (dtStr + \" 00:00:00\") : dtStr;\n" +
	 * "  var regExp = " + regExp + ";\n" +
	 * "  var dtArr = regExp.exec(dtStr);\n" + "  aDt = new Date(\n" +
	 * "    dtArr[1],\n" + "    dtArr[2]-1,\n" + "    dtArr[3],\n" +
	 * "    dtArr[4],\n" + "    dtArr[5],\n" + "    dtArr[6]\n" + "  );\n" +
	 * "  aDt;\n" + "}" + "\n"; result.setExpression(exp);
	 * 
	 * 
	 * return result; }
	 */
	private void setCellFormat(DataItemHandle di, IDynamicReportData cellData) throws SemanticException {

		String cellDataType = cellData.getDataType();

		if ("NUMBER".equals(cellDataType) || "INTEGER".equals(cellDataType)) {
			FormatValue formatValueToSet = new NumberFormatValue();
			formatValueToSet.setPattern(cellData.getFormat());
			formatValueToSet.setCategory("Custom");
			di.setProperty(StyleHandle.NUMBER_FORMAT_PROP, formatValueToSet);
		} else if ("DATE".equals(cellData.getDataType()) || "DATETIME".equals(cellDataType)) {
			FormatValue formatValueToSet = new DateFormatValue();
			formatValueToSet.setPattern(cellData.getFormat());
			formatValueToSet.setCategory("Custom");
			di.setProperty(StyleHandle.DATE_TIME_FORMAT_PROP, formatValueToSet);
		}
	}

	private DynamicDesignTableData getGridParamsData(ReportSection section, DynamicReportQueryParams qryParams) {
		return new DynamicDesignTableData(this, section, qryParams);
	}

	private void buildXMLDataSet(DynamicReportColumns columns, ElementFactory designFactory,
	        ReportDesignHandle reportDesignHandle, String dataSetName, String dataSourceName) throws SemanticException {
		OdaDataSetHandle dataSetHandle = designFactory.newOdaDataSet(dataSetName,
		        "org.eclipse.datatools.enablement.oda.xml.dataSet");
		dataSetHandle.setDataSource(dataSourceName);
		List<String> cols = new ArrayList<String>();

		String qText = "table0#-TNAME-#table0#:#[/reportData/records/record]#:#";
		for (int i = 0; i < columns.size(); i++) {
			DynamicReportColumn col = columns.getColumn(i);

			if (!getListUtils().valueIsInList(cols, col.getCode())) {
				cols.add(col.getCode());
				String colType = getXmlDataType(col.getDataType());

				qText = qText + String.format("{%s;%s;/%s}", col.getCode(), colType, col.getCode());
				// qText = qText + String.format("{%s;%s;/%s}", col.getCode(),
				// "STRING", col.getCode());
				qText = qText + ",";
			}
		}

		qText = qText + "{recType;STRING;/@type}";

		dataSetHandle.setProperty("queryText", qText);

		// CachedMetaData cacheMD = StructureFactory.createCachedMetaData();
		// dataSetHandle.setCachedMetaData(cacheMD);

		PropertyHandle rs = dataSetHandle.getPropertyHandle(IDataSetModel.RESULT_SET_PROP);

		cols.clear();

		int pos = 0;
		for (int i = 0; i < columns.size(); i++) {
			DynamicReportColumn col = columns.getColumn(i);

			if (!getListUtils().valueIsInList(cols, col.getCode())) {
				pos = pos + 1;
				cols.add(col.getCode());

				OdaResultSetColumn rc = StructureFactory.createOdaResultSetColumn();
				rc.setColumnName(col.getCode());
				rc.setPosition(new Integer(pos));

				if ("DATE".equals(col.getDataType()) || ("DATETIME".equals(col.getDataType()))) {
					rc.setDataType(DesignChoiceConstants.COLUMN_DATA_TYPE_STRING);

					ComputedColumn compCol = StructureFactory.createComputedColumn();
					compCol.setDataType(DesignChoiceConstants.COLUMN_DATA_TYPE_DATETIME);
					compCol.setName("computedCol_" + col.getCode());
					compCol.setDisplayName("computedCol_" + col.getCode());

					String regExp = "/(\\d{4})-(\\d{2})-(\\d{2}) (\\d{2}):(\\d{2}):(\\d{2})/";
					String exp = //@formatter:off
			  "if ( row[\"recType\"] == \"total\") null;  \n"
			+ "else {  \n" + "var cellVal = row[\""
			+ col.getCode() + "\"];\n"
			+ "if ((cellVal == \"\") || (cellVal == null)) {\n"
			+ "  null;\n"
			+ "}\n" + "else {\n"
			+ "  var dtStr =  BirtStr.trim(cellVal);\n"
			+ "  dtStr = (dtStr.length == 10) ? (dtStr + \" 00:00:00\") : dtStr;\n"
			+ "  var regExp = " + regExp + ";\n"
			+ "  var dtArr = regExp.exec(dtStr);\n"
			+ "  var aDt = new Date(\n"
			+ "    dtArr[1],\n"
			+ "    dtArr[2]-1,\n"
			+ "    dtArr[3],\n"
			+ "    dtArr[4],\n"
			+ "    dtArr[5],\n"
			+ "    dtArr[6]\n"
			+ "  );\n"
			+ "  aDt;\n" + "}" + "}" + "\n";
		    //@formatter:on
					compCol.setExpression(exp);
					PropertyHandle compCols = dataSetHandle.getPropertyHandle(IDataSetModel.COMPUTED_COLUMNS_PROP);
					compCols.addItem(compCol);
				} else {
					rc.setDataType(col.getBirtDataType());
				}

				rs.addItem(rc);
			}
		}

		// Add total filter
		FilterCondition fc = new FilterCondition();
		fc.setExpr("row[\"recType\"]");
		fc.setOperator(DesignChoiceConstants.FILTER_OPERATOR_NULL);
		dataSetHandle.addFilter(fc);

		reportDesignHandle.getDataSets().add(dataSetHandle);

	}

	private static String getXmlDataType(String dataType) {
		if ("NUMBER".equals(dataType))
			return "BIGDECIMAL";
		else if ("INTEGER".equals(dataType))
			return "INTEGER";
		// else if ("DATE".equals(dataType))
	    // return "DATETIME";
	    // else if ("DATETIME".equals(dataType))
	    // return "DATETIME";
		else
			return "STRING";
	}

	private void buildXMLDataSource(ElementFactory designFactory, ReportDesignHandle reportDesignHandle,
	        String dataSourceName, String fileName) throws SemanticException {
		OdaDataSourceHandle dsHandle = designFactory.newOdaDataSource(dataSourceName,
		        "org.eclipse.birt.report.data.oda.xml");
		dsHandle.setProperty("FILELIST", fileName);

		reportDesignHandle.getDataSources().add(dsHandle);

	}

	@Override
	public void finish() {
		// File outFile = new File(localXmlFileName);
		// if (outFile.exists()) outFile.delete();
	}

	@Override
	public void assignParametersToReport(IReportEngine engine, IReportRunnable design, IRenderTask task)
		    throws ReportCenterException {
		super.assignParametersToReport(engine, design, task);

		task.setParameterValue(BirtParamNames.P_REPORT_CODE, getCode()); // "dyna_ql"
		task.setParameterValue(BirtParamNames.P_REPORT_DYN_TITLE, getTitle());
		task.setParameterValue(BirtParamNames.P_REPORT_DYN_ORIENTATION, getOrientation().toString());

		task.setParameterValue(BirtParamNames.P_JDBC_DB_USER, userName);

	}

	@Override
	public void assignParametersToReport(IReportEngine engine, IReportRunnable design, IRunTask task)
		    throws ReportCenterException {
		super.assignParametersToReport(engine, design, task);

		task.setParameterValue(BirtParamNames.P_REPORT_CODE, getCode()); // "dyna_ql"
		task.setParameterValue(BirtParamNames.P_REPORT_DYN_TITLE, getTitle());
		task.setParameterValue(BirtParamNames.P_REPORT_DYN_ORIENTATION, getOrientation().toString());

		task.setParameterValue(BirtParamNames.P_JDBC_DB_USER, userName);

	}

}
