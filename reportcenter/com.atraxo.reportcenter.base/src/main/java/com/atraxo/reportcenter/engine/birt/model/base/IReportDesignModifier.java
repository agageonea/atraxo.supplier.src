/**
 * Copyright (C) FuelPlus GmbH 
 * $Revision: 76832 $  
 * $Date:   11/16/2005 11:35:09 AM$ 
 * $Author: fc $  
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/base/IReportDesignModifier.java $  
 */
package com.atraxo.reportcenter.engine.birt.model.base;

import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.birt.report.model.api.activity.SemanticException;

public interface IReportDesignModifier {

    public boolean shouldBeUsed(BaseReport report);

    public void modifyReportDesign(ReportDesignHandle reportDesignHandle) throws SemanticException;
}
