package com.atraxo.reportcenter.util;

import java.sql.Connection;

import org.eclipse.birt.report.engine.api.script.IReportContext;

import com.atraxo.reportcenter.engine.base.ReportCenterException;
import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;

public interface IReportUtils {

    public String[] splitUserPass(String userAndPass);
    public String getDatabaseURL();
    public String decodePassword(String encodedPassword) throws ReportCenterException;
    public Connection getConnection(String jdbcDbUrl, String jdbcUsername, String jdbcPassword) throws ReportCenterException;

    public String getReportTitle(BaseReport report, IReportContext reportContext);

    public String internalDataType2BirtDataType(String dataType);
    public boolean IsValueDataType(String tmpDataType, String value);
    public String getBirtValue(String internalDataType, String stringValue);
}
