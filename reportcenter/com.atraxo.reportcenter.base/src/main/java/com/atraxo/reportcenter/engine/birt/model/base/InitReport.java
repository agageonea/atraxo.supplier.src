/**
 * Copyright (C) FuelPlus GmbH 
 * $Revision: 76832 $  
 * $Date:   11/16/2005 11:35:09 AM$ 
 * $Author: fc $  
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/base/InitReport.java $  
 */
package com.atraxo.reportcenter.engine.birt.model.base;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "init-report")
public class InitReport extends BaseReport {

    @Override
    public String getReportFileName() {
	return "/resources/init_report.rptdesign";
    }

    public InitReport() {
	super();
	// InitReport should always wait for answer
	this.getRunMode().setLater(false);
    }

}
