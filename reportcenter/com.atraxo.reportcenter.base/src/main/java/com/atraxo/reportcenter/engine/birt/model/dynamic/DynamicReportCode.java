/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 77121 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/DynamicReportCode.java $
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic;

import java.io.IOException;
import java.util.Hashtable;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;
import com.atraxo.reportcenter.engine.birt.model.dynamic.parser.DynamicReportXMLParser;
import com.atraxo.reportcenter.engine.birt.model.dynamic.parser.INodeHandler;

public class DynamicReportCode {

    private static final Logger LOG = LoggerFactory.getLogger(DynamicReportCode.class);

    private String reportCode = null;
    private DynamicReportXMLParser xmlParser;

    private BaseReport report;

    public DynamicReportCode(BaseReport report) {
	this.report = report;
    }

    public String getReportCode() {
	return reportCode;
    }

    public void readReportCode() {
	xmlParser.cleanUp();

	xmlParser.setStartPath("/reportData");
	xmlParser.setStartPathAttributeName("code");
	xmlParser.setEndPath("/reportData/numberFormat");

	INodeHandler handler = new INodeHandler() {

	    public void onChildNode(String name, String value, Hashtable<String, String> attributes) {

	    }

	    public void onNodeStart(String name, Hashtable<String, String> attributes) {
		reportCode = attributes.get("code");
	    }

	    public void onNodeEnd(String name, Hashtable<String, String> attributes) {
	    }
	};

	xmlParser.addNodeHandler(handler);

	try {
	    xmlParser.process();
	} catch (ParserConfigurationException e) {
	    LOG.debug(e.getMessage(), e);
	} catch (SAXException e) {
	    LOG.debug(e.getMessage(), e);
	} catch (IOException e) {
	    LOG.debug(e.getMessage(), e);
	}
	xmlParser.removeNodeHandler(handler);
    }

    public void setXmlParser(DynamicReportXMLParser xmlParser) {
	this.xmlParser = xmlParser;
    }

    public void printReportCode() {
	LOG.debug("User : " + getReportCode());
    }

    public BaseReport getReport() {
        return report;
    }
}
