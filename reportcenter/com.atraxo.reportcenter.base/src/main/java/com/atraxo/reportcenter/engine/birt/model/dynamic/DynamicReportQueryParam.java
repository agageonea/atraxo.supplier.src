/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 76832 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/DynamicReportQueryParam.java $
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic;

import javax.xml.bind.annotation.XmlElement;

public class DynamicReportQueryParam {

    private String name;
    private String value;
    private String dataType;
    private String styleName;
    private String format;

    @XmlElement(name = "name")
    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    @XmlElement(name = "value")
    public String getValue() {
	return value;
    }

    public void setValue(String value) {
	this.value = value;
    }

    @XmlElement(name = "type")
    public String getDataType() {
	return dataType;
    }

    public void setDataType(String dataType) {
	this.dataType = dataType;
    }

    public String getStyleName() {
	return styleName;
    }

    public void setStyleName(String styleName) {
	this.styleName = styleName;
    }

    @XmlElement(name = "fmt")
    public String getFormat() {
	return format;
    }

    public void setFormat(String format) {
	this.format = format;
    }

}
