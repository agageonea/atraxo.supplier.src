/**
 * Copyright (C) FuelPlus GmbH 
 * $Revision: 76832 $  
 * $Date:   11/16/2005 11:35:09 AM$ 
 * $Author: fc $  
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/parser/INodeHandler.java $  
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic.parser;

import java.util.Hashtable;

public interface INodeHandler {

    public void onChildNode(String childNodeName, String value, Hashtable<String, String> attributes);

    public void onNodeStart(String nodeName, Hashtable<String, String> attributes);

    public void onNodeEnd(String nodeName, Hashtable<String, String> attributes);
}
