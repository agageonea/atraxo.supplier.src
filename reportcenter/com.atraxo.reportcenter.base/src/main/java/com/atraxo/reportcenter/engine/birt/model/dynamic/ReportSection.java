/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 77121 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/ReportSection.java $
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.NONE)
public class ReportSection {

    private String title;
    private boolean forceShowTitle;
    private ReportSectionUrl url;
    private int order;
    private int maxColumns = 0;
    private ReportSectionLayoutType layoutType = ReportSectionLayoutType.ON_COLUMNS;
    private String serverId;
    private ReportSectionOptions options = new ReportSectionOptions();

    @XmlElement(name = "serverid", required = false)
    public String getServerId() {
	return serverId;
    }

    public void setServerId(String value) {
	this.serverId = value;
    }

    @XmlElement(name = "forceTitle", required = false)
    public boolean getForceShowTitle() {
	return forceShowTitle;
    }

    @XmlElement(name = "title", required = false)
    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    @XmlElement(name = "url", required = true)
    public ReportSectionUrl getUrl() {
	return url;
    }

    public void setUrl(ReportSectionUrl url) {
	this.url = url;
    }

    @XmlAttribute(name = "order", required = true)
    public int getOrder() {
	return order;
    }

    public void setOrder(int order) {
	this.order = order;
    }

    public void setForceShowTitle(boolean forceShowTitle) {
	this.forceShowTitle = forceShowTitle;
    }

    @XmlAttribute(name = "columns", required = false)
    public int getMaxColumns() {
	return maxColumns;
    }

    public void setMaxColumns(int maxColumns) {
	this.maxColumns = maxColumns;
    }

    @XmlAttribute(name = "layoutType", required = false)
    public ReportSectionLayoutType getLayoutType() {
	return layoutType;
    }

    public void setLayoutType(ReportSectionLayoutType layoutType) {
	this.layoutType = layoutType;
    }

    @XmlElement(name = "options")
    public ReportSectionOptions getOptions() {
	return options;
    }

    public void setOptions(ReportSectionOptions options) {
	this.options = options;
    }

}
