/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 76832 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/parser/DynamicReportXMLParser.java $
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class DynamicReportXMLParser {

    private String startPath;
    private String startPathAttributeName;
    private String startPathAttributeValue;
    private String endPath;

    private String xmlFile;
    private List<INodeHandler> handlers = new ArrayList<INodeHandler>();

    public void setXmlFile(String xmlFile) {
	this.xmlFile = xmlFile;
    }

    public void setStartPath(String startPath) {
	this.startPath = startPath;
    }

    public void setStartPathAttributeName(String startPathAttributeName) {
	this.startPathAttributeName = startPathAttributeName;
    }

    public void setStartPathAttributeValue(String startPathAttributeValue) {
	this.startPathAttributeValue = startPathAttributeValue;
    }

    public void setEndPath(String endPath) {
	this.endPath = endPath;
    }

    public void addNodeHandler(INodeHandler handler) {
	if (!handlers.contains(handler)) {
	    handlers.add(handler);
	}
    }

    public void removeNodeHandler(INodeHandler handler) {
	if (handlers.contains(handler)) {
	    handlers.remove(handler);
	}
    }

    public void process() throws ParserConfigurationException, SAXException, IOException {
	SAXParserFactory factory = SAXParserFactory.newInstance();
	SAXParser saxParser = factory.newSAXParser();
	XMLReader xmlReader = saxParser.getXMLReader();

	DynamicReportErrorHandler errHandler = new DynamicReportErrorHandler();
	xmlReader.setErrorHandler(errHandler);

	DynamicReportXMLHandler contentHandler = new DynamicReportXMLHandler();

	contentHandler.setStartPath(startPath);
	contentHandler.setStartPathAttributeName(startPathAttributeName);
	contentHandler.setStartPathAttributeValue(startPathAttributeValue);
	contentHandler.setEndPath(endPath);

	xmlReader.setContentHandler(contentHandler);

	File inpFile = new File(xmlFile);
	InputStream inputStream = new FileInputStream(inpFile);
	Reader fileReader = new InputStreamReader(inputStream);

	for (INodeHandler h : handlers) {
	    contentHandler.addNodeHandler(h);
	}

	InputSource inputSrc = new InputSource(fileReader);
	try {
	    xmlReader.parse(inputSrc);
	} catch (SAXException e) {
	    if (e.getException() instanceof EndPathEncounteredException)
		return;
	    else
		throw e;
	} finally {
	    for (INodeHandler h : handlers) {
		contentHandler.removeNodeHandler(h);
	    }
	}
    }

    public void cleanUp() {
	this.startPath = null;
	this.startPathAttributeName = null;
	this.startPathAttributeValue = null;
	this.endPath = null;
    }
}
