/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 76832 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/layout/DynamicDesignTableData.java $
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic.layout;

import java.util.ArrayList;
import java.util.List;

import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;
import com.atraxo.reportcenter.engine.birt.model.dynamic.DynamicReport;
import com.atraxo.reportcenter.engine.birt.model.dynamic.DynamicReportQueryParam;
import com.atraxo.reportcenter.engine.birt.model.dynamic.DynamicReportQueryParams;
import com.atraxo.reportcenter.engine.birt.model.dynamic.DynamicReportUtil;
import com.atraxo.reportcenter.engine.birt.model.dynamic.ReportSection;
import com.atraxo.reportcenter.engine.birt.model.dynamic.ReportSectionLayoutType;

public class DynamicDesignTableData {

    private int rowsCount = 0;
    private int colsCount = 0;
    // private ReportPageOrientation orientation;

    private List<DynamicDesignRowTableData> rows = new ArrayList<DynamicDesignRowTableData>();
    private BaseReport report;

    public DynamicDesignTableData(DynamicReport report, ReportSection section, DynamicReportQueryParams qryParams) {
	this.report = report;
	int maxCols = section.getMaxColumns();
	if (maxCols <= 0) {
	    switch (report.getOrientation()) {
	    case LANDSCAPE:
		maxCols = 3;
		break;
	    case PORTRAIT:
		maxCols = 2;
		break;
	    }
	}

	ReportSectionLayoutType lt = section.getLayoutType();
	switch (lt) {
	case ON_ROWS:
	    computeLayoutData(qryParams, true, maxCols);
	    break;
	case ON_COLUMNS:
	    computeLayoutData(qryParams, false, maxCols);
	    break;
	}
    }

    /**
     *
     * @param qryParams
     * @param byRow
     *            If value is true layout will be : row/col | 0 | 1 | 2
     *            -------------------------------- 0 | p1 v1 | p2 v2 | p3 v3 1 |
     *            p4 v4 | p5 v5 | If value is false layout will be: row/col | 0
     *            | 1 | 2 -------------------------------- 0 | p1 v1 | p3 v3 |
     *            p5 v5 1 | p2 v2 | p4 v4 |
     * @param maxCols
     * @return
     */
    private DynamicDesignTableData computeLayoutData(DynamicReportQueryParams qryParams, boolean byRow, int maxCols) {
	int n = qryParams.size();
	int maxRows = n / maxCols + 1;

	rowsCount = maxRows;
	colsCount = maxCols;

	if (byRow) {
	    int k = 0;
	    DynamicReportUtil rptUtl = DynamicReportUtil.getInstance();

	    for (int i = 0; i < rowsCount; i++) {
		DynamicDesignRowTableData rowData = new DynamicDesignRowTableData();
		for (int j = 0; j < colsCount; j++) {
		    if (k < qryParams.size()) {
			DynamicReportQueryParam param = qryParams.getParam(k);
			DynamicDesignCellTableData cellData1 = new DynamicDesignCellTableData(this.report);
			cellData1.setLabel(true);
			if ((param.getName() == null) || (param.getName().isEmpty()))
			    cellData1.setText("");
			else
			    cellData1.setText(param.getName() + ":");
			rowData.getCells().add(cellData1);

			DynamicDesignCellTableData cellData2 = new DynamicDesignCellTableData(this.report);
			cellData2.setText(rptUtl.fixHTMLChars(param.getValue()));
			cellData2.setDataType(param.getDataType());
			cellData2.setStyleName(param.getStyleName());
			cellData2.setFormat(param.getFormat());

			rowData.getCells().add(cellData2);

			k++;
		    } else {
			DynamicDesignCellTableData cellData1 = new DynamicDesignCellTableData(this.report);
			cellData1.setText("");
			cellData1.setLabel(true);
			rowData.getCells().add(cellData1);
			DynamicDesignCellTableData cellData2 = new DynamicDesignCellTableData(this.report);
			cellData2.setText("");
			rowData.getCells().add(cellData2);
		    }
		}
		rows.add(rowData);
	    }
	} else {
	    int k = 0;
	    for (int j = 0; j < colsCount * 2; j++) {
		for (int i = 0; i < rowsCount; i++) {
		    DynamicDesignRowTableData rowData;
		    if (j == 0) {
			rowData = new DynamicDesignRowTableData();
			rows.add(rowData);
		    } else
			rowData = rows.get(i);

		    if (k < qryParams.size()) {
			DynamicReportQueryParam param = qryParams.getParam(k);
			DynamicDesignCellTableData cellData1 = new DynamicDesignCellTableData(this.report);
			cellData1.setLabel(true);
			if ((param.getName() == null) || (param.getName().isEmpty()))
			    cellData1.setText("");
			else
			    cellData1.setText(param.getName() + ":");
			rowData.getCells().add(cellData1);

			DynamicDesignCellTableData cellData2 = new DynamicDesignCellTableData(this.report);
			cellData2.setText(param.getValue());
			cellData2.setDataType(param.getDataType());
			cellData2.setStyleName(param.getStyleName());
			cellData2.setFormat(param.getFormat());
			rowData.getCells().add(cellData2);

			k++;
		    } else {
			DynamicDesignCellTableData cellData1 = new DynamicDesignCellTableData(this.report);
			cellData1.setText("");
			cellData1.setLabel(true);
			rowData.getCells().add(cellData1);
			DynamicDesignCellTableData cellData2 = new DynamicDesignCellTableData(this.report);
			cellData2.setText("");
			rowData.getCells().add(cellData2);
		    }
		}
	    }
	}

	return this;
    }

    public int getRowsCount() {
	return rowsCount;
    }

    public void setRowsCount(int rowsCount) {
	this.rowsCount = rowsCount;
    }

    public int getColsCount() {
	return colsCount;
    }

    public void setColsCount(int colsCount) {
	this.colsCount = colsCount;
    }

    public List<DynamicDesignRowTableData> getRows() {
	return rows;
    }

}
