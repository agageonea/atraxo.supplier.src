/**
 * Copyright (C) FuelPlus GmbH $Revision: 76832 $ $Date: 11/16/2005 11:35:09 AM$ $Author: fc $ $URL:
 * http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/util/
 * ListUtils.java $
 */
package com.atraxo.reportcenter.util;

import java.util.Iterator;
import java.util.List;

public class ListUtils {

	public boolean valueIsInList(List<?> list, Object value) {
		Iterator<?> it = list.iterator();
		while (it.hasNext()) {
			Object obj = it.next();
			if (obj != null && obj.equals(value)) {
				return true;
			}
		}

		return false;
	}

	public Object[] concat(Object[]... list) {
		int n = 0;
		for (Object[] l : list) {
			n = n + l.length;
		}
		Object[] result = new Object[n];
		int p = 0;
		for (Object[] l : list) {
			int ln = l.length;
			System.arraycopy(l, 0, result, p, ln);
			p = p + ln;
		}
		return result;
	}

	public String arrayAsString(Object[] o, String sep) {
		String result = "";
		for (int i = 0; i < o.length; i++) {
			result = result + ((i == 0) ? o[i] : (sep + o[i]));
		}

		return result;
	}
}
