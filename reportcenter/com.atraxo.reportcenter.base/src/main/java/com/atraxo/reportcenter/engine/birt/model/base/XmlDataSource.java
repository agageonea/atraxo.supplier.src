/**
 * Copyright (C) FuelPlus GmbH 
 * $Revision: 76832 $  
 * $Date:   11/16/2005 11:35:09 AM$ 
 * $Author: fc $  
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/base/XmlDataSource.java $  
 */
package com.atraxo.reportcenter.engine.birt.model.base;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class XmlDataSource {

    private String url;
    private String name;
    private String localXmlFile;

    @XmlAttribute(name = "name", required = true)
    public String getName() {
	return this.name;
    }

    public void setName(String name) {
	this.name = name;
    }

    @XmlElement(name = "url", required = true)
    public String getUrl() {
	return url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    public void setLocalXmlFile(String xmlFile) {
	this.localXmlFile = xmlFile;
    }

    public String getLocalXmlFile() {
	return this.localXmlFile;
    }
}
