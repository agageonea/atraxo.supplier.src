package com.atraxo.reportcenter.engine.birt.dataset.params;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Types;

import com.atraxo.reportcenter.engine.birt.dataset.NamedParameterStatement;

public class DateParam extends AbstractReportParam<Date> {

    public DateParam(String paramName, Date paramValue) {
	super(paramName, paramValue);
    }

    @Override
    public void setStatementParam(NamedParameterStatement statement) throws SQLException {
	if (getValue() == null)
	    statement.setNull(getName(), Types.DATE);
	else
	    statement.setDate(getName(), (java.sql.Date) getValue());
    }

}
