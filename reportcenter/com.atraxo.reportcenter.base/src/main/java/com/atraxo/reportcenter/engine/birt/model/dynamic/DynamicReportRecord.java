package com.atraxo.reportcenter.engine.birt.model.dynamic;

import java.util.HashMap;
import java.util.Map;

public class DynamicReportRecord {

    private Map<String, String> data = new HashMap<String, String>();
    private Map<String, String> columnFormat = new HashMap<String, String>();
    private String type = null;

    public Map<String, String> getData() {
	return data;
    }

    public void setData(Map<String, String> data) {
	this.data = data;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public Map<String, String> getColumnFormat() {
	return columnFormat;
    }

    public void setColumnFormat(Map<String, String> columnFormat) {
	this.columnFormat = columnFormat;
    }

    public void setColumn(String colName, String value) {
	data.put(colName, value);
    }

    public String getColumn(String colName) {
	return data.get(colName);
    }

    public void setColumnFormat(String colName, String value) {
	columnFormat.put(colName, value);
    }

    public String getColumnFormat(String colName) {
	return columnFormat.get(colName);
    }

}
