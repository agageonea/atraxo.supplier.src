package com.atraxo.reportcenter.engine;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.atraxo.reportcenter.reportexec.IReportExecutionObject;
import com.atraxo.reportcenter.reportexec.ReportExecutionImpl;

@XmlRootElement(name="run-result")
public class RunReportResult {

    private boolean success = true;
//    private String localResultFile;
//    private String resultFileName;
//    private String reportUrl;

//    private String errorMessage;
//    private Long runId;

    private List<IReportExecutionObject> reports = new ArrayList<IReportExecutionObject>();

    @XmlElement(name="success", required=true)
    public boolean isSuccess() {
	return success;
    }

//    @XmlElement(name="error", required=false)
//    public String getErrorMessage() {
//	return errorMessage;
//    }

//    public void setError(Exception e) {
//	if (e != null) {
//	    success = false;
//	    String err = e.getMessage();
//
//	    if (err == null) {
//		err = e.getClass().getCanonicalName();
//	    }
//
//	    this.errorMessage = err;
//	}
//	else {
//	    success = true;
//	    errorMessage = null;
//	}
//    }

//    @XmlElement(name="file", required=false)
//    public String getResultFile() {
//	return resultFileName;
//    }
//
//    @XmlTransient
//    public String getLocalResultFile() {
//	return localResultFile;
//    }
//
//    public void setLocalResultFile(String resultFile) {
//	File file = new File(resultFile);
//	this.resultFileName = file.getName();
//	this.localResultFile = resultFile;
//    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

//    public void setRunId(Long runId) {
//	this.runId = runId;
//    }
//
//    @XmlElement(name="id", required = true)
//    public Long getRunId() {
//        return runId;
//    }

//    @XmlElement(name="url")
//    public String getReportUrl() {
//        return reportUrl;
//    }
//
//    public void setReportUrl(String reportUrl) {
//        this.reportUrl = reportUrl;
//    }

    @XmlElementWrapper(name="reports", required=false)
    @XmlElement(name="report", type=ReportExecutionImpl.class)
    public List<IReportExecutionObject> getReports() {
        return reports;
    }

    public void setReports(List<IReportExecutionObject> reports) {
        this.reports = reports;
    }

}
