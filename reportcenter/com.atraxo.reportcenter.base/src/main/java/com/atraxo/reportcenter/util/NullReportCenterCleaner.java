package com.atraxo.reportcenter.util;

import com.atraxo.reportcenter.engine.base.IReportCenterCleaner;
import com.atraxo.reportcenter.engine.base.ReportCenterException;
import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;

public class NullReportCenterCleaner implements IReportCenterCleaner {

    public void execute(BaseReport report, String rid, boolean devMode) throws ReportCenterException {

    }

}
