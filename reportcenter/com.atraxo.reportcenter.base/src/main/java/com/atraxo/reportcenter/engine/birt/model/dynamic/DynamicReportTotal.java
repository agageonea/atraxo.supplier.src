/**
 * Copyright (C) FuelPlus GmbH
 * $Revision: 76832 $
 * $Date:   11/16/2005 11:35:09 AM$
 * $Author: fc $
 * $URL: http://brafs01.haj.lan/svn/fps-devel/trunk/product5/src/ReportCenter/com.atraxo.reportcenter.base/src/main/java/com/atraxo/reportcenter/engine/birt/model/dynamic/DynamicReportTotal.java $
 */
package com.atraxo.reportcenter.engine.birt.model.dynamic;

import com.atraxo.reportcenter.engine.birt.model.base.BaseReport;

public class DynamicReportTotal implements IDynamicReportColumn, IDynamicReportTotal, IDynamicReportData {

    private String code;
    private String value;
    private String label;
    private String format = null;
    private String dataType;
    private BaseReport report;

    public DynamicReportTotal(BaseReport report) {
	this.report = report;
    }

    public String getCode() {
	return code;
    }

    public void setCode(String code) {
	this.code = code;
    }

    public String getLabel() {
	return label;
    }

    public void setLabel(String label) {
	this.label = label;
    }

    public String getValue() {
	return value;
    }

    public void setValue(String value) {
	this.value = value;
    }

    public String getFormat() {
	return format;
    }

    public void setFormat(String format) {
	this.format = format;
    }

    public String getDataType() {
	dataType = (dataType != null) ? dataType.toUpperCase() : "STRING";

	boolean isDataTypeAsColumn = report.getReportUtils().IsValueDataType(dataType, getValue());
	if (!isDataTypeAsColumn)
	    dataType = "STRING";

	return dataType;
    }

    public void setDataType(String dataType) {
	this.dataType = dataType;
    }

    public String getStyleName() {
	return "";
    }

    public void setStyleName(String styleName) {
	// TODO Auto-generated method stub

    }

    public String getBirtDataType() {
	return report.getReportUtils().internalDataType2BirtDataType(this.getDataType());
    }

    public String getBirtValue() {
	return report.getReportUtils().getBirtValue(getDataType(), getValue());
    }

}
