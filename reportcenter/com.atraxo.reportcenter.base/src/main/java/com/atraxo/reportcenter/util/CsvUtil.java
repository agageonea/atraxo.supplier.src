package com.atraxo.reportcenter.util;

public class CsvUtil {

    public static final String DEFAULT_FIELD_SEPARATOR = ";";
    public static final String DEFAULT_QUOTE_CHAR = "\"";
    public static final String DEFAULT_DATE_FMT = "yyyyMMdd";
    public static final String DEFAULT_DEC_SEP = ".";
    public static final String DEFAULT_TH_SEP = ",";

}
