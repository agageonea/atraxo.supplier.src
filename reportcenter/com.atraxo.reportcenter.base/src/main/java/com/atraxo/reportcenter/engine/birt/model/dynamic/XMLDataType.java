package com.atraxo.reportcenter.engine.birt.model.dynamic;

public interface XMLDataType {
    public static final String STRING = "STRING";
    public static final String DATE = "DATE";
    public static final String DATETIME = "DATETIME";
    public static final String INTEGER = "INTEGER";
    public static final String NUMBER = "NUMBER";
}
