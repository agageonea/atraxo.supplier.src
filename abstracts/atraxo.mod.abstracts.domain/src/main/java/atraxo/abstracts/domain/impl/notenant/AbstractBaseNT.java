/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.abstracts.domain.impl.notenant;

import atraxo.abstracts.domain.impl.base.AbstractEntity;
import java.io.Serializable;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * Entity class for {@link AbstractBaseNT} domain entity.
 * Generated code. Do not modify in this file.
 */
@MappedSuperclass
public abstract class AbstractBaseNT extends AbstractEntity
		implements
			Serializable {

	private static final long serialVersionUID = -8865917134914502125L;

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
