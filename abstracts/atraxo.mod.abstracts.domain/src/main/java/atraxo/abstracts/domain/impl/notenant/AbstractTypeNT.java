/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.abstracts.domain.impl.notenant;

import atraxo.abstracts.domain.impl.notenant.AbstractAuditableNT;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link AbstractTypeNT} domain entity.
 * Generated code. Do not modify in this file.
 */
@MappedSuperclass
public abstract class AbstractTypeNT extends AbstractAuditableNT
		implements
			Serializable {

	private static final long serialVersionUID = -8865917134914502125L;

	@NotBlank
	@Column(name = "name", nullable = false, length = 255)
	private String name;

	@Column(name = "description", length = 1000)
	private String description;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
