/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.abstracts.domain.impl.tenant;

import atraxo.abstracts.domain.impl.base.AbstractEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seava.j4e.api.model.IModelWithClientId;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.Session;

/**
 * Entity class for {@link AbstractBase} domain entity.
 * Generated code. Do not modify in this file.
 */
@MappedSuperclass
public abstract class AbstractBase extends AbstractEntity
		implements
			Serializable,
			IModelWithClientId {

	private static final long serialVersionUID = -8865917134914502125L;

	private static final Logger LOG = LoggerFactory
			.getLogger(AbstractBase.class);

	/**
	 * Default constructor: used to set clientId
	 */
	public AbstractBase() {
		try {
			IUser u = Session.user.get();
			if (u != null) {
				this.clientId = u.getClientId();
			}
		} catch (Exception e) {
			LOG.debug("Cannot set client id on entity.", e);
		}
	}

	@NotBlank
	@Column(name = "clientid", nullable = false, length = 64)
	private String clientId;

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		this.clientId = Session.user.get().getClient().getId();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
		this.__validate_client_context__(this.clientId);
	}

	protected void __validate_client_context__(String clientId) {
		if (clientId != null && "SYS".equals(Session.user.get().getCode())) {
			return;
		}
		if (clientId != null
				&& !Session.user.get().getClient().getId().equals(clientId)) {
			throw new RuntimeException(
					"Client conflict detected. You are trying to work with an entity which belongs to client with id=`"
							+ clientId
							+ "` but the current session is connected to client with id=`"
							+ Session.user.get().getClient().getId() + "` ");
		}
	}

}
