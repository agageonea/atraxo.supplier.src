/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.abstracts.domain.impl.tenant;

import atraxo.abstracts.domain.impl.tenant.AbstractBase;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;
import seava.j4e.api.session.Session;

@MappedSuperclass
public abstract class AbstractAuditableAvatar extends AbstractBase
		implements
			Serializable {

	private static final long serialVersionUID = -8865917134914502125L;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", nullable = false)
	private Date createdAt;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_at", nullable = false)
	private Date modifiedAt;

	@NotBlank
	@Column(name = "created_by", nullable = false, length = 32)
	private String createdBy;

	@NotBlank
	@Column(name = "modified_by", nullable = false, length = 32)
	private String modifiedBy;

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return this.modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@PrePersist
	public void prePersist() {
		super.prePersist();
		this.createdAt = new Date();
		this.modifiedAt = new Date();
		this.createdBy = Session.user.get().getCode();
		this.modifiedBy = Session.user.get().getCode();
	}

	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
		this.modifiedAt = new Date();
		this.modifiedBy = Session.user.get().getCode();
	}

}
