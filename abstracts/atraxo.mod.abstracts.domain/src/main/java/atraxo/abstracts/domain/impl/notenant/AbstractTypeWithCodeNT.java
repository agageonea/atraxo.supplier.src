/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.abstracts.domain.impl.notenant;

import atraxo.abstracts.domain.impl.notenant.AbstractTypeNT;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link AbstractTypeWithCodeNT} domain entity.
 * Generated code. Do not modify in this file.
 */
@MappedSuperclass
public abstract class AbstractTypeWithCodeNT extends AbstractTypeNT
		implements
			Serializable {

	private static final long serialVersionUID = -8865917134914502125L;

	@NotBlank
	@Column(name = "code", nullable = false, length = 64)
	private String code;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
