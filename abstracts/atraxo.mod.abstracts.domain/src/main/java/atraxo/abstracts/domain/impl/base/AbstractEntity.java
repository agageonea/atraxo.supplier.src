/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.abstracts.domain.impl.base;

import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import javax.persistence.Version;
import org.hibernate.validator.constraints.NotBlank;
import seava.j4e.api.Constants;
import seava.j4e.api.model.IModelWithId;

/**
 * Entity class for {@link AbstractEntity} domain entity.
 * Generated code. Do not modify in this file.
 */
@MappedSuperclass
public abstract class AbstractEntity
		implements
			Serializable,
			IModelWithId<String> {

	private static final long serialVersionUID = -8865917134914502125L;

	@Id
	@GeneratedValue(generator = Constants.UUID_GENERATOR_NAME)
	@NotBlank
	@Column(name = "id", nullable = false, length = 64)
	private String id;

	@Version
	@Column(name = "version", precision = 10)
	private Long version;

	@NotBlank
	@Column(name = "refid", nullable = false, length = 64)
	private String refid;

	@Transient
	private String entityAlias;

	@Transient
	private String entityFqn;

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getRefid() {
		return this.refid;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}

	public String getEntityAlias() {
		return this.getClass().getSimpleName();
	}

	public void setEntityAlias(String entityAlias) {
		this.entityAlias = entityAlias;
	}

	public String getEntityFqn() {
		return this.getClass().getCanonicalName();
	}

	public void setEntityFqn(String entityFqn) {
		this.entityFqn = entityFqn;
	}

	/**
	 * called before persist
	 */
	@PrePersist
	public void prePersist() {
		if (this.refid == null || "".equals(this.refid)) {
			this.refid = UUID.randomUUID().toString().toUpperCase();
		}
	}

	/**
	 * called before update
	 */
	@PreUpdate
	public void preUpdate() {
	}

}
