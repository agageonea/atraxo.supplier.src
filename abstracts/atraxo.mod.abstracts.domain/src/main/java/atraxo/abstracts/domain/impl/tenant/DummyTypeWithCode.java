/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.abstracts.domain.impl.tenant;

import atraxo.abstracts.domain.impl.tenant.AbstractTypeWithCode;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

/**
 * Entity class for {@link DummyTypeWithCode} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = DummyTypeWithCode.TABLE_NAME)
public class DummyTypeWithCode extends AbstractTypeWithCode
		implements
			Serializable {

	public static final String TABLE_NAME = "DUMMY";

	private static final long serialVersionUID = -8865917134914502125L;

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
