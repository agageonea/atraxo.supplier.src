/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.abstracts.domain.impl.tenant;

import atraxo.abstracts.domain.impl.tenant.AbstractBase;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;
import seava.j4e.api.session.Session;

/**
 * Entity class for {@link AbstractAuditable} domain entity.
 * Generated code. Do not modify in this file.
 */
@MappedSuperclass
public abstract class AbstractAuditable extends AbstractBase
		implements
			Serializable {

	private static final long serialVersionUID = -8865917134914502125L;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdat", nullable = false)
	private Date createdAt;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modifiedat", nullable = false)
	private Date modifiedAt;

	@NotBlank
	@Column(name = "createdby", nullable = false, length = 32)
	private String createdBy;

	@NotBlank
	@Column(name = "modifiedby", nullable = false, length = 32)
	private String modifiedBy;

	@Column(name = "notes", length = 4000)
	private String notes;

	@NotNull
	@Column(name = "active", nullable = false)
	private Boolean active;

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return this.modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.createdAt == null) {
			this.createdAt = new Date();
		}
		this.modifiedAt = new Date();
		if (this.createdBy == null || "".equals(this.createdBy)) {
			this.createdBy = Session.user.get().getCode();
		}
		this.modifiedBy = Session.user.get().getCode();
		if (this.active == null) {
			this.active = new Boolean(false);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
		this.modifiedAt = new Date();
		this.modifiedBy = Session.user.get().getCode();
	}

}
