/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.abstracts.presenter.impl.notenant.model;

import java.util.Date;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.model.IModelWithId;
import seava.j4e.presenter.model.AbstractDsModel;

/**
 * Generated code. Do not modify in this file.
 */
public class AbstractTypeNT_Ds<E> extends AbstractDsModel<E>
		implements
			IModelWithId<String> {

	public static final String ALIAS = "abstr_AbstractTypeNT_Ds";

	public static final String F_ID = "id";
	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_NOTES = "notes";
	public static final String F_ACTIVE = "active";
	public static final String F_CREATEDAT = "createdAt";
	public static final String F_MODIFIEDAT = "modifiedAt";
	public static final String F_CREATEDBY = "createdBy";
	public static final String F_MODIFIEDBY = "modifiedBy";
	public static final String F_VERSION = "version";
	public static final String F_REFID = "refid";
	public static final String F_ENTITYALIAS = "entityAlias";
	public static final String F_ENTITYFQN = "entityFqn";

	@DsField(noUpdate = true)
	private String id;

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private String notes;

	@DsField
	private Boolean active;

	@DsField(noUpdate = true)
	private Date createdAt;

	@DsField(noUpdate = true)
	private Date modifiedAt;

	@DsField(noUpdate = true)
	private String createdBy;

	@DsField(noUpdate = true)
	private String modifiedBy;

	@DsField
	private Long version;

	@DsField
	private String refid;

	@DsField(noUpdate = true, fetch = false)
	private String entityAlias;

	@DsField(noUpdate = true, fetch = false)
	private String entityFqn;

	/**
	 * Default constructor
	 */
	public AbstractTypeNT_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AbstractTypeNT_Ds(E e) {
		super(e);
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return this.modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getRefid() {
		return this.refid;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}

	public String getEntityAlias() {
		return this.entityAlias;
	}

	public void setEntityAlias(String entityAlias) {
		this.entityAlias = entityAlias;
	}

	public String getEntityFqn() {
		return this.entityFqn;
	}

	public void setEntityFqn(String entityFqn) {
		this.entityFqn = entityFqn;
	}
}
