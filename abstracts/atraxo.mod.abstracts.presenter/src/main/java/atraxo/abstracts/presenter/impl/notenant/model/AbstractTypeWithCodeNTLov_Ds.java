/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.abstracts.presenter.impl.notenant.model;

import seava.j4e.api.annotation.DsField;
import seava.j4e.api.model.IModelWithId;
import seava.j4e.presenter.model.AbstractDsModel;

/**
 * Generated code. Do not modify in this file.
 */
public class AbstractTypeWithCodeNTLov_Ds<E> extends AbstractDsModel<E>
		implements
			IModelWithId<String> {

	public static final String ALIAS = "abstr_AbstractTypeWithCodeNTLov_Ds";

	public static final String F_ID = "id";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_ACTIVE = "active";
	public static final String F_REFID = "refid";

	@DsField
	private String id;

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private Boolean active;

	@DsField
	private String refid;

	/**
	 * Default constructor
	 */
	public AbstractTypeWithCodeNTLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AbstractTypeWithCodeNTLov_Ds(E e) {
		super(e);
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getRefid() {
		return this.refid;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}
}
