/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.abstracts.presenter.impl.tenant.model;

import java.util.Date;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.model.IModelWithClientId;
import seava.j4e.api.model.IModelWithId;
import seava.j4e.presenter.model.AbstractDsModel;

public class AbstractAuditableAvatars_Ds<E> extends AbstractDsModel<E>
		implements
			IModelWithId<String>,
			IModelWithClientId {

	public static final String ALIAS = "abstr_AbstractAuditableAvatars_Ds";

	public static final String f_id = "id";
	public static final String f_clientId = "clientId";
	public static final String f_createdAt = "createdAt";
	public static final String f_modifiedAt = "modifiedAt";
	public static final String f_createdBy = "createdBy";
	public static final String f_modifiedBy = "modifiedBy";
	public static final String f_notes = "notes";
	public static final String f_active = "active";
	public static final String f_version = "version";
	public static final String f_refid = "refid";
	public static final String f_entityAlias = "entityAlias";
	public static final String f_entityFqn = "entityFqn";

	@DsField(noUpdate = true)
	private String id;

	@DsField(noUpdate = true)
	private String clientId;

	@DsField(noUpdate = true)
	private Date createdAt;

	@DsField(noUpdate = true)
	private Date modifiedAt;

	@DsField(noUpdate = true)
	private String createdBy;

	@DsField(noUpdate = true)
	private String modifiedBy;

	@DsField
	private String notes;

	@DsField
	private Boolean active;

	@DsField
	private Long version;

	@DsField
	private String refid;

	@DsField(noUpdate = true, fetch = false)
	private String entityAlias;

	@DsField(noUpdate = true, fetch = false)
	private String entityFqn;

	public AbstractAuditableAvatars_Ds() {
		super();
	}

	public AbstractAuditableAvatars_Ds(E e) {
		super(e);
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return this.modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getRefid() {
		return this.refid;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}

	public String getEntityAlias() {
		return this.entityAlias;
	}

	public void setEntityAlias(String entityAlias) {
		this.entityAlias = entityAlias;
	}

	public String getEntityFqn() {
		return this.entityFqn;
	}

	public void setEntityFqn(String entityFqn) {
		this.entityFqn = entityFqn;
	}
}
