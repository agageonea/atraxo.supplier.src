/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.abstracts.presenter.impl.tenant.model;

import seava.j4e.api.annotation.DsField;
import seava.j4e.api.model.IModelWithClientId;
import seava.j4e.api.model.IModelWithId;
import seava.j4e.presenter.model.AbstractDsModel;

/**
 * Generated code. Do not modify in this file.
 */
public class AbstractAuditableLov_Ds<E> extends AbstractDsModel<E>
		implements
			IModelWithId<String>,
			IModelWithClientId {

	public static final String ALIAS = "abstr_AbstractAuditableLov_Ds";

	public static final String F_ID = "id";
	public static final String F_CLIENTID = "clientId";
	public static final String F_REFID = "refid";

	@DsField
	private String id;

	@DsField
	private String clientId;

	@DsField
	private String refid;

	/**
	 * Default constructor
	 */
	public AbstractAuditableLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AbstractAuditableLov_Ds(E e) {
		super(e);
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getRefid() {
		return this.refid;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}
}
