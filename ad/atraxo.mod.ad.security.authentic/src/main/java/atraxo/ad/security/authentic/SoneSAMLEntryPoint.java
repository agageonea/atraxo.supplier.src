package atraxo.ad.security.authentic;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.opensaml.common.SAMLException;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.opensaml.ws.message.encoder.MessageEncodingException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.saml.SAMLEntryPoint;
import org.springframework.security.saml.context.SAMLMessageContext;

/**
 * @author tlukacs
 */
public class SoneSAMLEntryPoint extends SAMLEntryPoint {

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
		try {

			SAMLMessageContext context = this.contextProvider.getLocalAndPeerEntity(request, response);

			if (this.isECP(context)) {
				this.initializeECP(context, e);
			} else if (this.isDiscovery(context)) {
				this.initializeDiscovery(context);
			} else if (this.isAjax(request)) {
				response.sendError(HttpStatus.FORBIDDEN.value());
			} else {
				this.initializeSSO(context, e);
			}

		} catch (SAMLException | MetadataProviderException | MessageEncodingException e1) {
			logger.debug("Error initializing entry point", e1);
			throw new ServletException(e1);
		}
	}

	private boolean isAjax(HttpServletRequest request) {
		return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
	}
}
