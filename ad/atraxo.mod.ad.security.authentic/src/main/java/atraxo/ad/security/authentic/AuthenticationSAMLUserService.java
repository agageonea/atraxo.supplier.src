package atraxo.ad.security.authentic;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.security.saml.userdetails.SAMLUserDetailsService;
import org.springframework.util.CollectionUtils;

import atraxo.ad.business.api.system.IClientService;
import atraxo.ad.domain.impl.security.User;
import atraxo.ad.domain.impl.system.Client;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.InvalidConfiguration;
import seava.j4e.api.session.ISessionUser;
import seava.j4e.commons.security.LoginParams;

public class AuthenticationSAMLUserService extends AbstractSecurity implements SAMLUserDetailsService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationSAMLUserService.class);

	@Override
	public Object loadUserBySAML(SAMLCredential cred) {
		ISessionUser su = null;
		try {
			su = this.createSessionUser(cred);
		} catch (BusinessException e) {
			LOGGER.error("Error:could not create session user !", e);
		}
		return su;
	}

	private ISessionUser createSessionUser(SAMLCredential cred) throws BusinessException {
		String clientCode = cred.getAttributeAsString("Customer");
		String userName = cred.getAttributeAsString("username");
		EntityManager em = this.getEntityManager();
		IClientService clientService = (IClientService) this.getServiceLocator().findEntityService(Client.class);
		Client c = null;
		User u;
		try {
			c = em.createQuery("select e from " + Client.class.getSimpleName() + " e where e.code = :code", Client.class)
					.setParameter("code", clientCode.toUpperCase()).getSingleResult();
		} catch (NoResultException | NonUniqueResultException e) {
			LOGGER.warn("Warning:could not retrieve Client from query due to invalid credentials, will throw UsernameNotFoundException !", e);
			throw new UsernameNotFoundException("Invalid credentials");
		}

		if (!c.getActive()) {
			LOGGER.warn("Warning:the client is not active, will throw UsernameNotFoundException !");
			throw new UsernameNotFoundException("Inactive client");
		}

		try {
			u = em.createQuery("select e from " + User.class.getSimpleName() + " e where e.loginName = :loginName and e.clientId = :clientId",
					User.class).setParameter("loginName", userName).setParameter("clientId", c.getId()).getSingleResult();
		} catch (NoResultException | NonUniqueResultException e) {
			LOGGER.warn("Warning:could not retrieve User from query due to invalid credentials, will add a new user account !", e);
			u = clientService.doAddUserAccount(c, userName, userName, userName);
		}

		if (!u.getActive()) {
			LOGGER.warn("Warning:the user is not active, will throw UsernameNotFoundException !");
			throw new UsernameNotFoundException("Inactive user");
		}

		if (CollectionUtils.isEmpty(u.getRoles())) {
			LOGGER.warn("Warning:the user has no roles, will throw UsernameNotFoundException !");
			throw new UsernameNotFoundException("User is not allowed to connect to application.");
		}

		ISessionUser su;
		try {
			su = this.buildSessionUser(c, u, new LoginParams());
		} catch (InvalidConfiguration e) {
			LOGGER.warn("Warning:could not build session user due to invalid configuration, will throw UsernameNotFoundException !", e);
			throw new UsernameNotFoundException(e.getMessage());
		}
		return su;
	}

}
