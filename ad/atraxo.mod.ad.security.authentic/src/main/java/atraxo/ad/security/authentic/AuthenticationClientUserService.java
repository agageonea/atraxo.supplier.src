package atraxo.ad.security.authentic;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import atraxo.ad.domain.impl.security.User;
import atraxo.ad.domain.impl.system.Client;
import seava.j4e.api.exceptions.InvalidConfiguration;
import seava.j4e.api.security.IAuthenticationClientUserService;
import seava.j4e.api.security.ILoginParams;
import seava.j4e.api.security.LoginParamsHolder;
import seava.j4e.api.session.ISessionUser;

/**
 * Authenticates login credentials against users defined in DNet-AD module.
 *
 * @author amathe
 */
public class AuthenticationClientUserService extends AbstractSecurity implements IAuthenticationClientUserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationClientUserService.class);

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		EntityManager em = this.getEntityManager();

		ILoginParams lp = LoginParamsHolder.params.get();
		Client c = null;
		User u;
		try {
			c = em.createQuery("select e from " + Client.class.getSimpleName() + " e where e.code = :code", Client.class)
					.setParameter("code", lp.getClientCode()).getSingleResult();
		} catch (NoResultException | NonUniqueResultException e) {
			LOGGER.warn("Warning:could not retrieve Client from query due to invalid credentials, will throw UsernameNotFoundException !", e);
			throw new UsernameNotFoundException("Invalid credentials");
		}

		if (!c.getActive()) {
			LOGGER.warn("Warning:the client is not active, will throw UsernameNotFoundException !");
			throw new UsernameNotFoundException("Inactive client");
		}

		try {
			u = em.createQuery("select e from " + User.class.getSimpleName() + " e where e.loginName = :loginName and e.clientId = :clientId",
					User.class).setParameter("loginName", username).setParameter("clientId", c.getId()).getSingleResult();
		} catch (NonUniqueResultException | NoResultException e) {
			LOGGER.warn("Warning:could not retrieve User from query due to invalid credentials, will throw UsernameNotFoundException !", e);
			throw new UsernameNotFoundException("Invalid credentials");
		}

		if (!u.getActive()) {
			LOGGER.warn("Warning:the user is not active, will throw UsernameNotFoundException !");
			throw new UsernameNotFoundException("Inactive user");
		}

		if (u.getRoles().size() == 0) {
			throw new UsernameNotFoundException("User is not allowed to connect to application.");
		}

		ISessionUser su;
		try {
			su = this.buildSessionUser(c, u, lp);
		} catch (InvalidConfiguration e) {
			LOGGER.warn("Warning:could not build session user due to invalid configuration, will throw UsernameNotFoundException !", e);
			throw new UsernameNotFoundException(e.getMessage());
		}
		return su;
	}

}
