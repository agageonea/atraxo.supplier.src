/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.security.authentic;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import atraxo.ad.business.ext.exceptions.AdErrorCode;
import atraxo.ad.domain.impl.security.User;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.security.IChangePasswordService;
import seava.j4e.api.security.IPasswordValidator;

public class ChangePasswordService extends AbstractSecurity implements IChangePasswordService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChangePasswordService.class);

	@Override
	@Transactional
	public void doChangePassword(String userCode, String newPassword, String oldPassword, String clientId, String clientCode) throws Exception {

		EntityManager em = this.getEntityManager();
		User u;

		if (newPassword != null && !"".equals(newPassword) && newPassword.equals(oldPassword)) {
			throw new Exception("New password is the same as the old password.");
		}
		try {
			u = em.createQuery("select e from " + User.class.getSimpleName() + " e where e.code = :code and e.clientId = :clientId", User.class)
					.setParameter("code", userCode).setParameter("clientId", clientId).getSingleResult();
		} catch (NoResultException | NonUniqueResultException e) {
			LOGGER.warn("Warning:could not retrieve User from query due to invalid credentials, will throw new invalid user Exception !", e);
			throw new ApplicationException(AdErrorCode.INVALID_USER_NAME, String.format(AdErrorCode.INVALID_USER_NAME.getErrMsg(), userCode));
		}

		String hashedOldPassword = this.encryptPassword(oldPassword);

		if (!u.getPassword().equals(hashedOldPassword)) {
			throw new Exception("Incorrect password.");
		}

		this.getApplicationContext().getBean(IPasswordValidator.class).validate(newPassword);

		String hashedNewPassword = this.encryptPassword(newPassword);
		u.setPassword(hashedNewPassword);
		em.merge(u);

	}

	protected String encryptPassword(String thePassword) throws NoSuchAlgorithmException {

		MessageDigest messageDigest = MessageDigest.getInstance("MD5");
		messageDigest.update(thePassword.getBytes(), 0, thePassword.length());
		String hashedPass = new BigInteger(1, messageDigest.digest()).toString(16);
		if (hashedPass.length() < 32) {
			hashedPass = "0" + hashedPass;
		}
		return hashedPass;
	}

}
