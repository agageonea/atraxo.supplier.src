package atraxo.ad.security.authentic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.support.MessageBuilder;

import atraxo.ad.domain.impl.ad.LanguageCodes;
import atraxo.ad.domain.impl.security.Role;
import atraxo.ad.domain.impl.security.User;
import atraxo.ad.domain.impl.system.Client;
import atraxo.ad.domain.impl.system.DateFormatMask;
import seava.j4e.api.Constants;
import seava.j4e.api.ISettings;
import seava.j4e.api.exceptions.InvalidConfiguration;
import seava.j4e.api.security.ILoginParams;
import seava.j4e.api.service.IServiceLocatorBusiness;
import seava.j4e.api.session.ISessionUser;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.IUserProfile;
import seava.j4e.api.session.IWorkspace;
import seava.j4e.api.session.Session;
import seava.j4e.commons.security.AppClient;
import seava.j4e.commons.security.AppUser;
import seava.j4e.commons.security.AppUserProfile;
import seava.j4e.commons.security.AppUserSettings;
import seava.j4e.commons.security.AppWorkspace;
import seava.j4e.commons.security.SessionUser;

public abstract class AbstractSecurity implements ApplicationContextAware {

	@PersistenceContext
	@Autowired
	private EntityManager entityManager;
	private ApplicationContext applicationContext;
	private ISettings settings;
	private IServiceLocatorBusiness serviceLocator;

	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	public EntityManager getEntityManager() {
		return this.entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public ISettings getSettings() {
		if (this.settings == null) {
			this.settings = this.applicationContext.getBean(ISettings.class);
		}
		return this.settings;
	}

	public void setSettings(ISettings settings) {
		this.settings = settings;
	}

	public IServiceLocatorBusiness getServiceLocator() {
		if (this.serviceLocator == null) {
			this.serviceLocator = this.getApplicationContext().getBean(IServiceLocatorBusiness.class);
		}
		return this.serviceLocator;
	}

	public ISessionUser buildSessionUser(Client c, User u, ILoginParams lp) throws InvalidConfiguration {

		boolean isAdmin = false;
		List<String> roles = new ArrayList<>();

		for (Role role : u.getRoles()) {
			roles.add(role.getCode());

			if (role.getCode().equals(Constants.ROLE_USER_CODE)) {
				// Add the prefix required by Spring Security
				roles.add("ROLE_" + role.getCode());
			}

			if (role.getCode().equals(c.getAdminRole())) {
				isAdmin = true;
			}
		}

		AppClient client = new AppClient(c.getId(), c.getCode(), c.getName());

		IWorkspace ws = new AppWorkspace(c.getWorkspacePath(), c.getImportPath(), c.getExportPath(), c.getTempPath());

		AppUserSettings userSettings = AppUserSettings.newInstance(this.getSettings());

		if (u.getThousandSeparator() != null && !"".equals(u.getThousandSeparator().getName()) && u.getDecimalSeparator() != null
				&& !"".equals(u.getDecimalSeparator().getName())) {
			String numberFormat = "0" + u.getThousandSeparator() + "000" + u.getDecimalSeparator() + "00";
			userSettings.setNumberFormat(numberFormat);
		}

		if (u.getDateFormat() != null) {
			for (DateFormatMask mask : u.getDateFormat().getMasks()) {
				userSettings.setDateFormatMask(mask.getMask(), mask.getValue());
			}
		}
		if (u.getUserLanguage() != null && !u.getUserLanguage().equals(LanguageCodes._EMPTY_)) {
			userSettings.setLanguage(u.getUserLanguage().getCode());
		}

		if (lp.getLanguage() != null && !"".equals(lp.getLanguage())) {
			userSettings.setLanguage(lp.getLanguage());
		}

		IUserProfile profile = new AppUserProfile(isAdmin, roles, false, false, u.getLocked());
		IUser user = new AppUser(u.getCode(), u.getName(), u.getLoginName(), u.getPassword(), null, null, client, userSettings, profile, ws, false);
		ISessionUser su = new SessionUser(user, lp.getUserAgent(), new Date(), lp.getRemoteHost(), lp.getRemoteIp());
		Session.user.set(user);
		// TODO add organizations to session user profile
		this.sendMessage("updateSessionChannel", su);
		return su;
	}

	protected void sendMessage(String to, Object content) {
		Message<Object> message = MessageBuilder.withPayload(content).build();
		this.getApplicationContext().getBean(to, MessageChannel.class).send(message);
	}

}
