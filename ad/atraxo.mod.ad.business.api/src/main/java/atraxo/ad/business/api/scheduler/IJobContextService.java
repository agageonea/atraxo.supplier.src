/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.scheduler;

import atraxo.ad.domain.impl.scheduler.JobContext;
import atraxo.ad.domain.impl.scheduler.JobContextParam;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link JobContext} domain entity.
 */
public interface IJobContextService extends IEntityService<JobContext> {

	/**
	 * Find by unique key
	 *
	 * @return JobContext
	 */
	public JobContext findByName(String name);

	/**
	 * Find by reference: params
	 *
	 * @param params
	 * @return List<JobContext>
	 */
	public List<JobContext> findByParams(JobContextParam params);

	/**
	 * Find by ID of reference: params.id
	 *
	 * @param paramsId
	 * @return List<JobContext>
	 */
	public List<JobContext> findByParamsId(String paramsId);
}
