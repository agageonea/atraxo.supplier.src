/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.externalReport;

import atraxo.ad.domain.impl.ad.ReportStatus;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.businessArea.BusinessArea;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.ExternalReportParameter;
import atraxo.ad.domain.impl.externalReport.ReportLog;
import atraxo.ad.domain.impl.externalReport.SourceUploadHistory;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ExternalReport} domain entity.
 */
public interface IExternalReportService extends IEntityService<ExternalReport> {

	/**
	 * Custom service runReport
	 *
	 * @return void
	 */
	public void runReport(String externalReportId, String format,
			String fileName) throws BusinessException;

	/**
	 * Custom service generateSaleInvoiceReport
	 *
	 * @return void
	 */
	public void generateSaleInvoiceReport(String invoiceId, String reportId,
			String format, String entityAlias, String fileName)
			throws BusinessException;

	/**
	 * Custom service generateSaleInvoiceReportSync
	 *
	 * @return Attachment
	 */
	public Attachment generateSaleInvoiceReportSync(String invoiceId,
			String reportId, String format, String entityAlias, String fileName)
			throws BusinessException;

	/**
	 * Custom service buildFileName
	 *
	 * @return String
	 */
	public String buildFileName(String reportName, String ext)
			throws BusinessException;

	/**
	 * Custom service runReport
	 *
	 * @return void
	 */
	public void runReport(Object ds, String reportName, String format,
			String fileName) throws BusinessException;

	/**
	 * Custom service parseAndSaveParameters
	 *
	 * @return void
	 */
	public void parseAndSaveParameters(ExternalReport report, byte[] file,
			String fileName) throws BusinessException;

	/**
	 * Custom service onReady
	 *
	 * @return void
	 */
	public void onReady(Integer generatedReportId, ReportStatus status)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return ExternalReport
	 */
	public ExternalReport findByName(String name);

	/**
	 * Find by reference: businessArea
	 *
	 * @param businessArea
	 * @return List<ExternalReport>
	 */
	public List<ExternalReport> findByBusinessArea(BusinessArea businessArea);

	/**
	 * Find by ID of reference: businessArea.id
	 *
	 * @param businessAreaId
	 * @return List<ExternalReport>
	 */
	public List<ExternalReport> findByBusinessAreaId(String businessAreaId);

	/**
	 * Find by reference: params
	 *
	 * @param params
	 * @return List<ExternalReport>
	 */
	public List<ExternalReport> findByParams(ExternalReportParameter params);

	/**
	 * Find by ID of reference: params.id
	 *
	 * @param paramsId
	 * @return List<ExternalReport>
	 */
	public List<ExternalReport> findByParamsId(String paramsId);

	/**
	 * Find by reference: reportLogs
	 *
	 * @param reportLogs
	 * @return List<ExternalReport>
	 */
	public List<ExternalReport> findByReportLogs(ReportLog reportLogs);

	/**
	 * Find by ID of reference: reportLogs.id
	 *
	 * @param reportLogsId
	 * @return List<ExternalReport>
	 */
	public List<ExternalReport> findByReportLogsId(String reportLogsId);

	/**
	 * Find by reference: sourceUploadHistories
	 *
	 * @param sourceUploadHistories
	 * @return List<ExternalReport>
	 */
	public List<ExternalReport> findBySourceUploadHistories(
			SourceUploadHistory sourceUploadHistories);

	/**
	 * Find by ID of reference: sourceUploadHistories.id
	 *
	 * @param sourceUploadHistoriesId
	 * @return List<ExternalReport>
	 */
	public List<ExternalReport> findBySourceUploadHistoriesId(
			String sourceUploadHistoriesId);
}
