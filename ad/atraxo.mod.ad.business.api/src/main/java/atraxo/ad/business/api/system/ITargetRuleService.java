/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.system;

import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.ad.domain.impl.system.TargetRule;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link TargetRule} domain entity.
 */
public interface ITargetRuleService extends IEntityService<TargetRule> {

	/**
	 * Find by unique key
	 *
	 * @return TargetRule
	 */
	public TargetRule findByTargetRule(AttachmentType sourceRefId,
			String targetAlias);

	/**
	 * Find by unique key
	 *
	 * @return TargetRule
	 */
	public TargetRule findByTargetRule(Long sourceRefIdId, String targetAlias);

	/**
	 * Find by reference: sourceRefId
	 *
	 * @param sourceRefId
	 * @return List<TargetRule>
	 */
	public List<TargetRule> findBySourceRefId(AttachmentType sourceRefId);

	/**
	 * Find by ID of reference: sourceRefId.id
	 *
	 * @param sourceRefIdId
	 * @return List<TargetRule>
	 */
	public List<TargetRule> findBySourceRefIdId(String sourceRefIdId);
}
