/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.scheduler;

import atraxo.ad.domain.impl.scheduler.JobContext;
import atraxo.ad.domain.impl.scheduler.JobContextParam;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link JobContextParam} domain entity.
 */
public interface IJobContextParamService
		extends
			IEntityService<JobContextParam> {

	/**
	 * Find by unique key
	 *
	 * @return JobContextParam
	 */
	public JobContextParam findByName(JobContext jobContext, String paramName);

	/**
	 * Find by unique key
	 *
	 * @return JobContextParam
	 */
	public JobContextParam findByName(Long jobContextId, String paramName);

	/**
	 * Find by reference: jobContext
	 *
	 * @param jobContext
	 * @return List<JobContextParam>
	 */
	public List<JobContextParam> findByJobContext(JobContext jobContext);

	/**
	 * Find by ID of reference: jobContext.id
	 *
	 * @param jobContextId
	 * @return List<JobContextParam>
	 */
	public List<JobContextParam> findByJobContextId(String jobContextId);
}
