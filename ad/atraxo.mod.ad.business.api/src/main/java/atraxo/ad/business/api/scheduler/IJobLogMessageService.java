/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.scheduler;

import atraxo.ad.domain.impl.scheduler.JobLog;
import atraxo.ad.domain.impl.scheduler.JobLogMessage;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link JobLogMessage} domain entity.
 */
public interface IJobLogMessageService extends IEntityService<JobLogMessage> {

	/**
	 * Find by reference: jobLog
	 *
	 * @param jobLog
	 * @return List<JobLogMessage>
	 */
	public List<JobLogMessage> findByJobLog(JobLog jobLog);

	/**
	 * Find by ID of reference: jobLog.id
	 *
	 * @param jobLogId
	 * @return List<JobLogMessage>
	 */
	public List<JobLogMessage> findByJobLogId(String jobLogId);
}
