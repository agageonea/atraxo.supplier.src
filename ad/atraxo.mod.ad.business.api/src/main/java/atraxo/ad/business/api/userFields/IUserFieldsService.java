/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.userFields;

import atraxo.ad.domain.impl.businessArea.BusinessArea;
import atraxo.ad.domain.impl.userFields.UserFields;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link UserFields} domain entity.
 */
public interface IUserFieldsService extends IEntityService<UserFields> {

	/**
	 * Find by unique key
	 *
	 * @return UserFields
	 */
	public UserFields findByName(String name, BusinessArea businessArea);

	/**
	 * Find by unique key
	 *
	 * @return UserFields
	 */
	public UserFields findByName(String name, Long businessAreaId);

	/**
	 * Find by reference: businessArea
	 *
	 * @param businessArea
	 * @return List<UserFields>
	 */
	public List<UserFields> findByBusinessArea(BusinessArea businessArea);

	/**
	 * Find by ID of reference: businessArea.id
	 *
	 * @param businessAreaId
	 * @return List<UserFields>
	 */
	public List<UserFields> findByBusinessAreaId(String businessAreaId);
}
