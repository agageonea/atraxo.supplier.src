/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.scheduler;

import atraxo.ad.domain.impl.scheduler.StepExecutionSeq;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link StepExecutionSeq} domain entity.
 */
public interface IStepExecutionSeqService
		extends
			IEntityService<StepExecutionSeq> {

	/**
	 * Find by unique key
	 *
	 * @return StepExecutionSeq
	 */
	public StepExecutionSeq findByUn(String uniqueKey);
}
