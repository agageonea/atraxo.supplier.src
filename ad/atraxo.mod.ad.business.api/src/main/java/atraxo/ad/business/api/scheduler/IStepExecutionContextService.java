/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.scheduler;

import atraxo.ad.domain.impl.scheduler.StepExecution;
import atraxo.ad.domain.impl.scheduler.StepExecutionContext;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link StepExecutionContext} domain entity.
 */
public interface IStepExecutionContextService
		extends
			IEntityService<StepExecutionContext> {

	/**
	 * Find by reference: stepExecution
	 *
	 * @param stepExecution
	 * @return List<StepExecutionContext>
	 */
	public List<StepExecutionContext> findByStepExecution(
			StepExecution stepExecution);

	/**
	 * Find by ID of reference: stepExecution.id
	 *
	 * @param stepExecutionId
	 * @return List<StepExecutionContext>
	 */
	public List<StepExecutionContext> findByStepExecutionId(Long stepExecutionId);
}
