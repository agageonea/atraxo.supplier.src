/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.system;

import atraxo.ad.domain.impl.system.FrameExtension;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FrameExtension} domain entity.
 */
public interface IFrameExtensionService extends IEntityService<FrameExtension> {

	/**
	 * Find by unique key
	 *
	 * @return FrameExtension
	 */
	public FrameExtension findByName(String frame, String fileLocation);
}
