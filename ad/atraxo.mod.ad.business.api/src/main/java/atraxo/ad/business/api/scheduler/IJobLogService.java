/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.scheduler;

import atraxo.ad.domain.impl.scheduler.JobContext;
import atraxo.ad.domain.impl.scheduler.JobLog;
import atraxo.ad.domain.impl.scheduler.JobLogMessage;
import atraxo.ad.domain.impl.scheduler.JobTimer;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link JobLog} domain entity.
 */
public interface IJobLogService extends IEntityService<JobLog> {

	/**
	 * Find by reference: jobContext
	 *
	 * @param jobContext
	 * @return List<JobLog>
	 */
	public List<JobLog> findByJobContext(JobContext jobContext);

	/**
	 * Find by ID of reference: jobContext.id
	 *
	 * @param jobContextId
	 * @return List<JobLog>
	 */
	public List<JobLog> findByJobContextId(String jobContextId);

	/**
	 * Find by reference: jobTimer
	 *
	 * @param jobTimer
	 * @return List<JobLog>
	 */
	public List<JobLog> findByJobTimer(JobTimer jobTimer);

	/**
	 * Find by ID of reference: jobTimer.id
	 *
	 * @param jobTimerId
	 * @return List<JobLog>
	 */
	public List<JobLog> findByJobTimerId(String jobTimerId);

	/**
	 * Find by reference: messages
	 *
	 * @param messages
	 * @return List<JobLog>
	 */
	public List<JobLog> findByMessages(JobLogMessage messages);

	/**
	 * Find by ID of reference: messages.id
	 *
	 * @param messagesId
	 * @return List<JobLog>
	 */
	public List<JobLog> findByMessagesId(String messagesId);
}
