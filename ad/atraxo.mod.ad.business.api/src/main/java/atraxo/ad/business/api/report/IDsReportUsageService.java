/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.report;

import atraxo.ad.domain.impl.report.DsReport;
import atraxo.ad.domain.impl.report.DsReportUsage;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link DsReportUsage} domain entity.
 */
public interface IDsReportUsageService extends IEntityService<DsReportUsage> {

	/**
	 * Find by unique key
	 *
	 * @return DsReportUsage
	 */
	public DsReportUsage findByReportUsage_ds(DsReport dsReport);

	/**
	 * Find by unique key
	 *
	 * @return DsReportUsage
	 */
	public DsReportUsage findByReportUsage_ds(Long dsReportId);

	/**
	 * Find by reference: dsReport
	 *
	 * @param dsReport
	 * @return List<DsReportUsage>
	 */
	public List<DsReportUsage> findByDsReport(DsReport dsReport);

	/**
	 * Find by ID of reference: dsReport.id
	 *
	 * @param dsReportId
	 * @return List<DsReportUsage>
	 */
	public List<DsReportUsage> findByDsReportId(String dsReportId);
}
