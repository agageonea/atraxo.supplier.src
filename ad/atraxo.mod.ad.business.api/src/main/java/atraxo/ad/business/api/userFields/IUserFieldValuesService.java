/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.userFields;

import atraxo.ad.domain.impl.userFields.UserFieldValues;
import atraxo.ad.domain.impl.userFields.UserFields;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link UserFieldValues} domain entity.
 */
public interface IUserFieldValuesService
		extends
			IEntityService<UserFieldValues> {

	/**
	 * Find by reference: userField
	 *
	 * @param userField
	 * @return List<UserFieldValues>
	 */
	public List<UserFieldValues> findByUserField(UserFields userField);

	/**
	 * Find by ID of reference: userField.id
	 *
	 * @param userFieldId
	 * @return List<UserFieldValues>
	 */
	public List<UserFieldValues> findByUserFieldId(String userFieldId);
}
