package atraxo.ad.business.api.ext.report;

import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author apetho
 */
public interface IReportDownloaderService {

	/**
	 * Check the report status. If is ready send notification and call onReady service.
	 *
	 * @param id
	 * @param er
	 * @param userCode
	 * @param clientCode
	 * @throws BusinessException
	 */
	void startDownloader(Integer id, ExternalReport er, String userCode, String clientCode, String fileName) throws BusinessException;

	/**
	 * Check the report status. If is ready send attachment and call onReady service.
	 *
	 * @param id
	 * @param er
	 * @param userCode
	 * @param clientCode
	 * @param entityName
	 * @param parentId
	 * @param fileExtension
	 * @throws BusinessException
	 */
	void downloadCreateAttach(Integer id, ExternalReport er, String userCode, String clientCode, String entityName, String parentId,
			String fileExtension, String fileName) throws BusinessException;

	/**
	 * @param id
	 * @param er
	 * @param userCode
	 * @param clientCode
	 * @param entityName
	 * @param parentId
	 * @param fileExtension
	 * @return
	 * @throws BusinessException
	 */
	Attachment downloadCreateAttachSync(Integer id, ExternalReport er, String userCode, String clientCode, String entityName, String parentId,
			String fileExtension, String fileName) throws BusinessException;
}
