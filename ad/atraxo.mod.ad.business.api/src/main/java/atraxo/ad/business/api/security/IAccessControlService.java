/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.security;

import atraxo.ad.domain.impl.security.AccessControl;
import atraxo.ad.domain.impl.security.AccessControlDs;
import atraxo.ad.domain.impl.security.Role;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link AccessControl} domain entity.
 */
public interface IAccessControlService extends IEntityService<AccessControl> {

	/**
	 * Find by unique key
	 *
	 * @return AccessControl
	 */
	public AccessControl findByName(String name);

	/**
	 * Find by reference: dsRules
	 *
	 * @param dsRules
	 * @return List<AccessControl>
	 */
	public List<AccessControl> findByDsRules(AccessControlDs dsRules);

	/**
	 * Find by ID of reference: dsRules.id
	 *
	 * @param dsRulesId
	 * @return List<AccessControl>
	 */
	public List<AccessControl> findByDsRulesId(String dsRulesId);

	/**
	 * Find by reference: roles
	 *
	 * @param roles
	 * @return List<AccessControl>
	 */
	public List<AccessControl> findByRoles(Role roles);

	/**
	 * Find by ID of reference: roles.id
	 *
	 * @param rolesId
	 * @return List<AccessControl>
	 */
	public List<AccessControl> findByRolesId(String rolesId);
}
