/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.system;

import atraxo.ad.domain.impl.system.Job;
import atraxo.ad.domain.impl.system.JobParam;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Job} domain entity.
 */
public interface IJobService extends IEntityService<Job> {

	/**
	 * Find by unique key
	 *
	 * @return Job
	 */
	public Job findByName(String name);

	/**
	 * Find by unique key
	 *
	 * @return Job
	 */
	public Job findByJclass(String javaClass);

	/**
	 * Find by reference: params
	 *
	 * @param params
	 * @return List<Job>
	 */
	public List<Job> findByParams(JobParam params);

	/**
	 * Find by ID of reference: params.id
	 *
	 * @param paramsId
	 * @return List<Job>
	 */
	public List<Job> findByParamsId(String paramsId);
}
