/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.system;

import atraxo.ad.domain.impl.system.ParamValue;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ParamValue} domain entity.
 */
public interface IParamValueService extends IEntityService<ParamValue> {
}
