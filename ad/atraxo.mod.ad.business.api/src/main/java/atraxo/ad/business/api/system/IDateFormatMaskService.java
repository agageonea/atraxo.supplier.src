/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.system;

import atraxo.ad.domain.impl.system.DateFormat;
import atraxo.ad.domain.impl.system.DateFormatMask;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link DateFormatMask} domain entity.
 */
public interface IDateFormatMaskService extends IEntityService<DateFormatMask> {

	/**
	 * Find by unique key
	 *
	 * @return DateFormatMask
	 */
	public DateFormatMask findByName(DateFormat dateFormat, String mask);

	/**
	 * Find by unique key
	 *
	 * @return DateFormatMask
	 */
	public DateFormatMask findByName(Long dateFormatId, String mask);

	/**
	 * Find by reference: dateFormat
	 *
	 * @param dateFormat
	 * @return List<DateFormatMask>
	 */
	public List<DateFormatMask> findByDateFormat(DateFormat dateFormat);

	/**
	 * Find by ID of reference: dateFormat.id
	 *
	 * @param dateFormatId
	 * @return List<DateFormatMask>
	 */
	public List<DateFormatMask> findByDateFormatId(String dateFormatId);
}
