/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.security;

import atraxo.ad.domain.impl.security.AccessControl;
import atraxo.ad.domain.impl.security.AccessControlAsgn;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link AccessControlAsgn} domain entity.
 */
public interface IAccessControlAsgnService
		extends
			IEntityService<AccessControlAsgn> {

	/**
	 * Find by unique key
	 *
	 * @return AccessControlAsgn
	 */
	public AccessControlAsgn findByUnique(AccessControl accessControl,
			String asgnName);

	/**
	 * Find by unique key
	 *
	 * @return AccessControlAsgn
	 */
	public AccessControlAsgn findByUnique(Long accessControlId, String asgnName);

	/**
	 * Find by reference: accessControl
	 *
	 * @param accessControl
	 * @return List<AccessControlAsgn>
	 */
	public List<AccessControlAsgn> findByAccessControl(
			AccessControl accessControl);

	/**
	 * Find by ID of reference: accessControl.id
	 *
	 * @param accessControlId
	 * @return List<AccessControlAsgn>
	 */
	public List<AccessControlAsgn> findByAccessControlId(String accessControlId);
}
