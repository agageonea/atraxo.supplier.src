/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.system;

import atraxo.ad.domain.impl.system.DateFormat;
import atraxo.ad.domain.impl.system.DateFormatMask;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link DateFormat} domain entity.
 */
public interface IDateFormatService extends IEntityService<DateFormat> {

	/**
	 * Find by unique key
	 *
	 * @return DateFormat
	 */
	public DateFormat findByName(String name);

	/**
	 * Find by reference: masks
	 *
	 * @param masks
	 * @return List<DateFormat>
	 */
	public List<DateFormat> findByMasks(DateFormatMask masks);

	/**
	 * Find by ID of reference: masks.id
	 *
	 * @param masksId
	 * @return List<DateFormat>
	 */
	public List<DateFormat> findByMasksId(String masksId);
}
