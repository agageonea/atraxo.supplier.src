/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.attachment;

import atraxo.ad.domain.impl.ad.TAttachmentType;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import java.io.InputStream;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Attachment} domain entity.
 */
public interface IAttachmentService extends IEntityService<Attachment> {

	/**
	 * Custom service download
	 *
	 * @return InputStream
	 */
	public InputStream download(String refId) throws BusinessException;

	/**
	 * Custom service deleteFromS3
	 *
	 * @return void
	 */
	public void deleteFromS3(String id) throws BusinessException;

	/**
	 * Custom service insertWithoutUpload
	 *
	 * @return void
	 */
	public void insertWithoutUpload(Attachment attachment)
			throws BusinessException;

	/**
	 * Custom service getDocumentsByType
	 *
	 * @return List<Attachment>
	 */
	public List<Attachment> getDocumentsByType(Integer targetRefid,
			String targetAlias, TAttachmentType docType)
			throws BusinessException;

	/**
	 * Find by reference: type
	 *
	 * @param type
	 * @return List<Attachment>
	 */
	public List<Attachment> findByType(AttachmentType type);

	/**
	 * Find by ID of reference: type.id
	 *
	 * @param typeId
	 * @return List<Attachment>
	 */
	public List<Attachment> findByTypeId(String typeId);
}
