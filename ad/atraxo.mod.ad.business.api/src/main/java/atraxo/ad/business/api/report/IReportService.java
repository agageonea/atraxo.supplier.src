/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.report;

import atraxo.ad.domain.impl.report.Report;
import atraxo.ad.domain.impl.report.ReportParam;
import atraxo.ad.domain.impl.report.ReportServer;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Report} domain entity.
 */
public interface IReportService extends IEntityService<Report> {

	/**
	 * Find by unique key
	 *
	 * @return Report
	 */
	public Report findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return Report
	 */
	public Report findByName(String name);

	/**
	 * Find by reference: reportServer
	 *
	 * @param reportServer
	 * @return List<Report>
	 */
	public List<Report> findByReportServer(ReportServer reportServer);

	/**
	 * Find by ID of reference: reportServer.id
	 *
	 * @param reportServerId
	 * @return List<Report>
	 */
	public List<Report> findByReportServerId(String reportServerId);

	/**
	 * Find by reference: reportParameters
	 *
	 * @param reportParameters
	 * @return List<Report>
	 */
	public List<Report> findByReportParameters(ReportParam reportParameters);

	/**
	 * Find by ID of reference: reportParameters.id
	 *
	 * @param reportParametersId
	 * @return List<Report>
	 */
	public List<Report> findByReportParametersId(String reportParametersId);
}
