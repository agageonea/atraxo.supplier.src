/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.security;

import atraxo.ad.domain.impl.security.AccessControl;
import atraxo.ad.domain.impl.security.AccessControlDs;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link AccessControlDs} domain entity.
 */
public interface IAccessControlDsService
		extends
			IEntityService<AccessControlDs> {

	/**
	 * Find by unique key
	 *
	 * @return AccessControlDs
	 */
	public AccessControlDs findByUnique(AccessControl accessControl,
			String dsName);

	/**
	 * Find by unique key
	 *
	 * @return AccessControlDs
	 */
	public AccessControlDs findByUnique(Long accessControlId, String dsName);

	/**
	 * Find by reference: accessControl
	 *
	 * @param accessControl
	 * @return List<AccessControlDs>
	 */
	public List<AccessControlDs> findByAccessControl(AccessControl accessControl);

	/**
	 * Find by ID of reference: accessControl.id
	 *
	 * @param accessControlId
	 * @return List<AccessControlDs>
	 */
	public List<AccessControlDs> findByAccessControlId(String accessControlId);
}
