/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.externalReport;

import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.ExternalReportParameter;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ExternalReportParameter} domain entity.
 */
public interface IExternalReportParameterService
		extends
			IEntityService<ExternalReportParameter> {

	/**
	 * Find by unique key
	 *
	 * @return ExternalReportParameter
	 */
	public ExternalReportParameter findByKey(ExternalReport externalReport,
			String name);

	/**
	 * Find by unique key
	 *
	 * @return ExternalReportParameter
	 */
	public ExternalReportParameter findByKey(Long externalReportId, String name);

	/**
	 * Find by reference: externalReport
	 *
	 * @param externalReport
	 * @return List<ExternalReportParameter>
	 */
	public List<ExternalReportParameter> findByExternalReport(
			ExternalReport externalReport);

	/**
	 * Find by ID of reference: externalReport.id
	 *
	 * @param externalReportId
	 * @return List<ExternalReportParameter>
	 */
	public List<ExternalReportParameter> findByExternalReportId(
			String externalReportId);
}
