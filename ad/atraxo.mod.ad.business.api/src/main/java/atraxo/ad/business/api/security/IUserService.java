/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.security;

import atraxo.ad.domain.impl.security.Role;
import atraxo.ad.domain.impl.security.User;
import atraxo.ad.domain.impl.security.UserGroup;
import atraxo.ad.domain.impl.system.DateFormat;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link User} domain entity.
 */
public interface IUserService extends IEntityService<User> {

	/**
	 * Custom service doChangePassword
	 *
	 * @return void
	 */
	public void doChangePassword(String userId, String newPassword)
			throws BusinessException;

	/**
	 * Custom service findAll
	 *
	 * @return List<User>
	 */
	public List<User> findAll() throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return User
	 */
	public User findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return User
	 */
	public User findByLogin(String loginName);

	/**
	 * Find by reference: dateFormat
	 *
	 * @param dateFormat
	 * @return List<User>
	 */
	public List<User> findByDateFormat(DateFormat dateFormat);

	/**
	 * Find by ID of reference: dateFormat.id
	 *
	 * @param dateFormatId
	 * @return List<User>
	 */
	public List<User> findByDateFormatId(String dateFormatId);

	/**
	 * Find by reference: roles
	 *
	 * @param roles
	 * @return List<User>
	 */
	public List<User> findByRoles(Role roles);

	/**
	 * Find by ID of reference: roles.id
	 *
	 * @param rolesId
	 * @return List<User>
	 */
	public List<User> findByRolesId(String rolesId);

	/**
	 * Find by reference: groups
	 *
	 * @param groups
	 * @return List<User>
	 */
	public List<User> findByGroups(UserGroup groups);

	/**
	 * Find by ID of reference: groups.id
	 *
	 * @param groupsId
	 * @return List<User>
	 */
	public List<User> findByGroupsId(String groupsId);
}
