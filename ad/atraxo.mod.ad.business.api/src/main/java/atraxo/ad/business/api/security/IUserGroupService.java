/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.security;

import atraxo.ad.domain.impl.security.User;
import atraxo.ad.domain.impl.security.UserGroup;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link UserGroup} domain entity.
 */
public interface IUserGroupService extends IEntityService<UserGroup> {

	/**
	 * Find by unique key
	 *
	 * @return UserGroup
	 */
	public UserGroup findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return UserGroup
	 */
	public UserGroup findByName(String name);

	/**
	 * Find by reference: users
	 *
	 * @param users
	 * @return List<UserGroup>
	 */
	public List<UserGroup> findByUsers(User users);

	/**
	 * Find by ID of reference: users.id
	 *
	 * @param usersId
	 * @return List<UserGroup>
	 */
	public List<UserGroup> findByUsersId(String usersId);
}
