/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.security;

import atraxo.ad.domain.impl.security.Menu;
import atraxo.ad.domain.impl.security.Role;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Menu} domain entity.
 */
public interface IMenuService extends IEntityService<Menu> {

	/**
	 * Find by unique key
	 *
	 * @return Menu
	 */
	public Menu findByName(String name);

	/**
	 * Find by reference: roles
	 *
	 * @param roles
	 * @return List<Menu>
	 */
	public List<Menu> findByRoles(Role roles);

	/**
	 * Find by ID of reference: roles.id
	 *
	 * @param rolesId
	 * @return List<Menu>
	 */
	public List<Menu> findByRolesId(String rolesId);
}
