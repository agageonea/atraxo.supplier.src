/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.scheduler;

import atraxo.ad.domain.impl.scheduler.JobContext;
import atraxo.ad.domain.impl.scheduler.JobTimer;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link JobTimer} domain entity.
 */
public interface IJobTimerService extends IEntityService<JobTimer> {

	/**
	 * Find by unique key
	 *
	 * @return JobTimer
	 */
	public JobTimer findByName(JobContext jobContext, String name);

	/**
	 * Find by unique key
	 *
	 * @return JobTimer
	 */
	public JobTimer findByName(Long jobContextId, String name);

	/**
	 * Find by reference: jobContext
	 *
	 * @param jobContext
	 * @return List<JobTimer>
	 */
	public List<JobTimer> findByJobContext(JobContext jobContext);

	/**
	 * Find by ID of reference: jobContext.id
	 *
	 * @param jobContextId
	 * @return List<JobTimer>
	 */
	public List<JobTimer> findByJobContextId(String jobContextId);
}
