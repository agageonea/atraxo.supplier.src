/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.report;

import atraxo.ad.domain.impl.report.DsReport;
import atraxo.ad.domain.impl.report.DsReportParam;
import atraxo.ad.domain.impl.report.Report;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link DsReport} domain entity.
 */
public interface IDsReportService extends IEntityService<DsReport> {

	/**
	 * Find by unique key
	 *
	 * @return DsReport
	 */
	public DsReport findByRep_ds(Report report, String dataSource);

	/**
	 * Find by unique key
	 *
	 * @return DsReport
	 */
	public DsReport findByRep_ds(Long reportId, String dataSource);

	/**
	 * Find by reference: report
	 *
	 * @param report
	 * @return List<DsReport>
	 */
	public List<DsReport> findByReport(Report report);

	/**
	 * Find by ID of reference: report.id
	 *
	 * @param reportId
	 * @return List<DsReport>
	 */
	public List<DsReport> findByReportId(String reportId);

	/**
	 * Find by reference: dsReportParams
	 *
	 * @param dsReportParams
	 * @return List<DsReport>
	 */
	public List<DsReport> findByDsReportParams(DsReportParam dsReportParams);

	/**
	 * Find by ID of reference: dsReportParams.id
	 *
	 * @param dsReportParamsId
	 * @return List<DsReport>
	 */
	public List<DsReport> findByDsReportParamsId(String dsReportParamsId);
}
