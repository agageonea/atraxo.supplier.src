/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.security;

import atraxo.ad.domain.impl.security.AccessControl;
import atraxo.ad.domain.impl.security.AccessControlDsRpc;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link AccessControlDsRpc} domain entity.
 */
public interface IAccessControlDsRpcService
		extends
			IEntityService<AccessControlDsRpc> {

	/**
	 * Find by unique key
	 *
	 * @return AccessControlDsRpc
	 */
	public AccessControlDsRpc findByUnique(AccessControl accessControl,
			String dsName, String serviceMethod);

	/**
	 * Find by unique key
	 *
	 * @return AccessControlDsRpc
	 */
	public AccessControlDsRpc findByUnique(Long accessControlId, String dsName,
			String serviceMethod);

	/**
	 * Find by reference: accessControl
	 *
	 * @param accessControl
	 * @return List<AccessControlDsRpc>
	 */
	public List<AccessControlDsRpc> findByAccessControl(
			AccessControl accessControl);

	/**
	 * Find by ID of reference: accessControl.id
	 *
	 * @param accessControlId
	 * @return List<AccessControlDsRpc>
	 */
	public List<AccessControlDsRpc> findByAccessControlId(String accessControlId);
}
