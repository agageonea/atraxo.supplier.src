/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.externalReport;

import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.SourceUploadHistory;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link SourceUploadHistory} domain entity.
 */
public interface ISourceUploadHistoryService
		extends
			IEntityService<SourceUploadHistory> {

	/**
	 * Custom service empty
	 *
	 * @return void
	 */
	public void empty(String externalReportId) throws BusinessException;

	/**
	 * Find by reference: externalReport
	 *
	 * @param externalReport
	 * @return List<SourceUploadHistory>
	 */
	public List<SourceUploadHistory> findByExternalReport(
			ExternalReport externalReport);

	/**
	 * Find by ID of reference: externalReport.id
	 *
	 * @param externalReportId
	 * @return List<SourceUploadHistory>
	 */
	public List<SourceUploadHistory> findByExternalReportId(
			String externalReportId);
}
