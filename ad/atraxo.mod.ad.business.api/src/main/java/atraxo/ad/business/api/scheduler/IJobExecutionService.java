/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.scheduler;

import atraxo.ad.domain.impl.scheduler.JobExecution;
import atraxo.ad.domain.impl.scheduler.JobInstance;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link JobExecution} domain entity.
 */
public interface IJobExecutionService extends IEntityService<JobExecution> {

	/**
	 * Custom service deleteJobExecution
	 *
	 * @return void
	 */
	public void deleteJobExecution(List ids) throws BusinessException;

	/**
	 * Custom service emptyHistory
	 *
	 * @return void
	 */
	public void emptyHistory(Integer id) throws BusinessException;

	/**
	 * Custom service findByJobExecutionParams
	 *
	 * @return List
	 */
	public List findByJobExecutionParams(Integer id) throws BusinessException;

	/**
	 * Find by reference: jobInstance
	 *
	 * @param jobInstance
	 * @return List<JobExecution>
	 */
	public List<JobExecution> findByJobInstance(JobInstance jobInstance);

	/**
	 * Find by ID of reference: jobInstance.id
	 *
	 * @param jobInstanceId
	 * @return List<JobExecution>
	 */
	public List<JobExecution> findByJobInstanceId(Long jobInstanceId);
}
