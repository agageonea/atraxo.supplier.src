/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.system;

import atraxo.ad.domain.impl.system.Param;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Param} domain entity.
 */
public interface IParamService extends IEntityService<Param> {

	/**
	 * Custom service doSynchronizeCatalog
	 *
	 * @return void
	 */
	public void doSynchronizeCatalog() throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Param
	 */
	public Param findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return Param
	 */
	public Param findByName(String name);
}
