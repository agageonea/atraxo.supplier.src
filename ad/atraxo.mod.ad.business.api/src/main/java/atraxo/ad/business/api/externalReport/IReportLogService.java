/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.externalReport;

import atraxo.ad.domain.impl.ad.ReportStatus;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.ReportLog;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ReportLog} domain entity.
 */
public interface IReportLogService extends IEntityService<ReportLog> {

	/**
	 * Custom service empty
	 *
	 * @return void
	 */
	public void empty(String externalReportId) throws BusinessException;

	/**
	 * Custom service getReport
	 *
	 * @return byte[]
	 */
	public byte[] getReport(String logId) throws BusinessException;

	/**
	 * Custom service updateLog
	 *
	 * @return void
	 */
	public void updateLog(ExternalReport externalReport, Integer reportId,
			ReportStatus reportStatus, String description)
			throws BusinessException;

	/**
	 * Custom service findLastEntry
	 *
	 * @return ReportLog
	 */
	public ReportLog findLastEntry(String externalReportId)
			throws BusinessException;

	/**
	 * Find by reference: externalReport
	 *
	 * @param externalReport
	 * @return List<ReportLog>
	 */
	public List<ReportLog> findByExternalReport(ExternalReport externalReport);

	/**
	 * Find by ID of reference: externalReport.id
	 *
	 * @param externalReportId
	 * @return List<ReportLog>
	 */
	public List<ReportLog> findByExternalReportId(String externalReportId);
}
