/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.system;

import atraxo.ad.domain.impl.system.DataSource;
import atraxo.ad.domain.impl.system.DataSourceRpc;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link DataSourceRpc} domain entity.
 */
public interface IDataSourceRpcService extends IEntityService<DataSourceRpc> {

	/**
	 * Find by unique key
	 *
	 * @return DataSourceRpc
	 */
	public DataSourceRpc findByName(DataSource dataSource, String name);

	/**
	 * Find by unique key
	 *
	 * @return DataSourceRpc
	 */
	public DataSourceRpc findByName(Long dataSourceId, String name);

	/**
	 * Find by reference: dataSource
	 *
	 * @param dataSource
	 * @return List<DataSourceRpc>
	 */
	public List<DataSourceRpc> findByDataSource(DataSource dataSource);

	/**
	 * Find by ID of reference: dataSource.id
	 *
	 * @param dataSourceId
	 * @return List<DataSourceRpc>
	 */
	public List<DataSourceRpc> findByDataSourceId(String dataSourceId);
}
