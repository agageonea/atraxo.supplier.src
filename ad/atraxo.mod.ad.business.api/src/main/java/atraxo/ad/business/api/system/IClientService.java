/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.system;

import atraxo.ad.domain.impl.security.User;
import atraxo.ad.domain.impl.system.Client;
import seava.j4e.api.action.impex.IImportDataPackage;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Client} domain entity.
 */
public interface IClientService extends IEntityService<Client> {

	/**
	 * Custom service doInsertWithUserAccount
	 *
	 * @return void
	 */
	public void doInsertWithUserAccount(Client client, String userCode,
			String userName, String loginName, String password)
			throws BusinessException;

	/**
	 * Custom service doInsertWithUserAccountAndSetup
	 *
	 * @return void
	 */
	public void doInsertWithUserAccountAndSetup(Client client, String userCode,
			String userName, String loginName, String password,
			IImportDataPackage dataPackage) throws BusinessException;

	/**
	 * Custom service doAddUserAccount
	 *
	 * @return User
	 */
	public User doAddUserAccount(Client client, String userCode,
			String userName, String loginName) throws BusinessException;

	/**
	 * Custom service doUpgrade
	 *
	 * @return void
	 */
	public void doUpgrade(Client client, IImportDataPackage dataPackage)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Client
	 */
	public Client findByCode(String code);
}
