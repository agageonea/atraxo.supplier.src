/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.scheduler;

import atraxo.ad.domain.impl.scheduler.QuartzCalendar;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link QuartzCalendar} domain entity.
 */
public interface IQuartzCalendarService extends IEntityService<QuartzCalendar> {
}
