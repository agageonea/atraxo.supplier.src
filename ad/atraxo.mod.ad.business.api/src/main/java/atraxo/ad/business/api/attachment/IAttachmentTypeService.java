/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.attachment;

import atraxo.ad.domain.impl.attachment.AttachmentType;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link AttachmentType} domain entity.
 */
public interface IAttachmentTypeService extends IEntityService<AttachmentType> {

	/**
	 * Custom service findExternalLink
	 *
	 * @return AttachmentType
	 */
	public AttachmentType findExternalLink() throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return AttachmentType
	 */
	public AttachmentType findByName(String name);
}
