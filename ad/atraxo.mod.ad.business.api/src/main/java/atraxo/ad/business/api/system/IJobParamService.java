/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.system;

import atraxo.ad.domain.impl.system.Job;
import atraxo.ad.domain.impl.system.JobParam;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link JobParam} domain entity.
 */
public interface IJobParamService extends IEntityService<JobParam> {

	/**
	 * Find by unique key
	 *
	 * @return JobParam
	 */
	public JobParam findByName(Job job, String name);

	/**
	 * Find by unique key
	 *
	 * @return JobParam
	 */
	public JobParam findByName(Long jobId, String name);

	/**
	 * Find by reference: job
	 *
	 * @param job
	 * @return List<JobParam>
	 */
	public List<JobParam> findByJob(Job job);

	/**
	 * Find by ID of reference: job.id
	 *
	 * @param jobId
	 * @return List<JobParam>
	 */
	public List<JobParam> findByJobId(String jobId);
}
