/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.businessArea;

import atraxo.ad.domain.impl.businessArea.BusinessArea;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link BusinessArea} domain entity.
 */
public interface IBusinessAreaService extends IEntityService<BusinessArea> {

	/**
	 * Find by unique key
	 *
	 * @return BusinessArea
	 */
	public BusinessArea findByName(String name);
}
