/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.security;

import atraxo.ad.domain.impl.security.AccessControl;
import atraxo.ad.domain.impl.security.Menu;
import atraxo.ad.domain.impl.security.MenuItem;
import atraxo.ad.domain.impl.security.Role;
import atraxo.ad.domain.impl.security.User;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Role} domain entity.
 */
public interface IRoleService extends IEntityService<Role> {

	/**
	 * Find by unique key
	 *
	 * @return Role
	 */
	public Role findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return Role
	 */
	public Role findByName(String name);

	/**
	 * Find by reference: users
	 *
	 * @param users
	 * @return List<Role>
	 */
	public List<Role> findByUsers(User users);

	/**
	 * Find by ID of reference: users.id
	 *
	 * @param usersId
	 * @return List<Role>
	 */
	public List<Role> findByUsersId(String usersId);

	/**
	 * Find by reference: accessControls
	 *
	 * @param accessControls
	 * @return List<Role>
	 */
	public List<Role> findByAccessControls(AccessControl accessControls);

	/**
	 * Find by ID of reference: accessControls.id
	 *
	 * @param accessControlsId
	 * @return List<Role>
	 */
	public List<Role> findByAccessControlsId(String accessControlsId);

	/**
	 * Find by reference: menus
	 *
	 * @param menus
	 * @return List<Role>
	 */
	public List<Role> findByMenus(Menu menus);

	/**
	 * Find by ID of reference: menus.id
	 *
	 * @param menusId
	 * @return List<Role>
	 */
	public List<Role> findByMenusId(String menusId);

	/**
	 * Find by reference: menuItems
	 *
	 * @param menuItems
	 * @return List<Role>
	 */
	public List<Role> findByMenuItems(MenuItem menuItems);

	/**
	 * Find by ID of reference: menuItems.id
	 *
	 * @param menuItemsId
	 * @return List<Role>
	 */
	public List<Role> findByMenuItemsId(String menuItemsId);
}
