/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.scheduler;

import atraxo.ad.domain.impl.scheduler.JobExecution;
import atraxo.ad.domain.impl.scheduler.StepExecution;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link StepExecution} domain entity.
 */
public interface IStepExecutionService extends IEntityService<StepExecution> {

	/**
	 * Find by reference: jobExecStep
	 *
	 * @param jobExecStep
	 * @return List<StepExecution>
	 */
	public List<StepExecution> findByJobExecStep(JobExecution jobExecStep);

	/**
	 * Find by ID of reference: jobExecStep.id
	 *
	 * @param jobExecStepId
	 * @return List<StepExecution>
	 */
	public List<StepExecution> findByJobExecStepId(Long jobExecStepId);
}
