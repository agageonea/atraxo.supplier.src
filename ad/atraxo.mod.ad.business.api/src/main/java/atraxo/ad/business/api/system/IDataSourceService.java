/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.system;

import atraxo.ad.domain.impl.system.DataSource;
import atraxo.ad.domain.impl.system.DataSourceField;
import atraxo.ad.domain.impl.system.DataSourceRpc;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link DataSource} domain entity.
 */
public interface IDataSourceService extends IEntityService<DataSource> {

	/**
	 * Find by unique key
	 *
	 * @return DataSource
	 */
	public DataSource findByName(String name);

	/**
	 * Find by unique key
	 *
	 * @return DataSource
	 */
	public DataSource findByModel(String model);

	/**
	 * Find by reference: fields
	 *
	 * @param fields
	 * @return List<DataSource>
	 */
	public List<DataSource> findByFields(DataSourceField fields);

	/**
	 * Find by ID of reference: fields.id
	 *
	 * @param fieldsId
	 * @return List<DataSource>
	 */
	public List<DataSource> findByFieldsId(String fieldsId);

	/**
	 * Find by reference: serviceMethods
	 *
	 * @param serviceMethods
	 * @return List<DataSource>
	 */
	public List<DataSource> findByServiceMethods(DataSourceRpc serviceMethods);

	/**
	 * Find by ID of reference: serviceMethods.id
	 *
	 * @param serviceMethodsId
	 * @return List<DataSource>
	 */
	public List<DataSource> findByServiceMethodsId(String serviceMethodsId);
}
