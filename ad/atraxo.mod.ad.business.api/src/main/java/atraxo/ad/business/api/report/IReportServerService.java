/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.report;

import atraxo.ad.domain.impl.report.ReportServer;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ReportServer} domain entity.
 */
public interface IReportServerService extends IEntityService<ReportServer> {

	/**
	 * Find by unique key
	 *
	 * @return ReportServer
	 */
	public ReportServer findByName(String name);
}
