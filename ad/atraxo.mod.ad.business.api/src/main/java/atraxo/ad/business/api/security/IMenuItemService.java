/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.security;

import atraxo.ad.domain.impl.security.Menu;
import atraxo.ad.domain.impl.security.MenuItem;
import atraxo.ad.domain.impl.security.Role;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link MenuItem} domain entity.
 */
public interface IMenuItemService extends IEntityService<MenuItem> {

	/**
	 * Find by unique key
	 *
	 * @return MenuItem
	 */
	public MenuItem findByName(String name);

	/**
	 * Find by unique key
	 *
	 * @return MenuItem
	 */
	public MenuItem findByFrame(String frame);

	/**
	 * Find by unique key
	 *
	 * @return MenuItem
	 */
	public MenuItem findByTitle(String title);

	/**
	 * Find by reference: menuItem
	 *
	 * @param menuItem
	 * @return List<MenuItem>
	 */
	public List<MenuItem> findByMenuItem(MenuItem menuItem);

	/**
	 * Find by ID of reference: menuItem.id
	 *
	 * @param menuItemId
	 * @return List<MenuItem>
	 */
	public List<MenuItem> findByMenuItemId(String menuItemId);

	/**
	 * Find by reference: menu
	 *
	 * @param menu
	 * @return List<MenuItem>
	 */
	public List<MenuItem> findByMenu(Menu menu);

	/**
	 * Find by ID of reference: menu.id
	 *
	 * @param menuId
	 * @return List<MenuItem>
	 */
	public List<MenuItem> findByMenuId(String menuId);

	/**
	 * Find by reference: roles
	 *
	 * @param roles
	 * @return List<MenuItem>
	 */
	public List<MenuItem> findByRoles(Role roles);

	/**
	 * Find by ID of reference: roles.id
	 *
	 * @param rolesId
	 * @return List<MenuItem>
	 */
	public List<MenuItem> findByRolesId(String rolesId);
}
