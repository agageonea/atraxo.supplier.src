/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.report;

import atraxo.ad.domain.impl.report.Report;
import atraxo.ad.domain.impl.report.ReportParam;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ReportParam} domain entity.
 */
public interface IReportParamService extends IEntityService<ReportParam> {

	/**
	 * Find by unique key
	 *
	 * @return ReportParam
	 */
	public ReportParam findByName(Report report, String name);

	/**
	 * Find by unique key
	 *
	 * @return ReportParam
	 */
	public ReportParam findByName(Long reportId, String name);

	/**
	 * Find by reference: report
	 *
	 * @param report
	 * @return List<ReportParam>
	 */
	public List<ReportParam> findByReport(Report report);

	/**
	 * Find by ID of reference: report.id
	 *
	 * @param reportId
	 * @return List<ReportParam>
	 */
	public List<ReportParam> findByReportId(String reportId);
}
