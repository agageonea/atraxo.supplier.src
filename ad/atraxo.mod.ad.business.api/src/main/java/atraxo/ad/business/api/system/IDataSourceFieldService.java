/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.system;

import atraxo.ad.domain.impl.system.DataSource;
import atraxo.ad.domain.impl.system.DataSourceField;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link DataSourceField} domain entity.
 */
public interface IDataSourceFieldService
		extends
			IEntityService<DataSourceField> {

	/**
	 * Find by unique key
	 *
	 * @return DataSourceField
	 */
	public DataSourceField findByName(DataSource dataSource, String name);

	/**
	 * Find by unique key
	 *
	 * @return DataSourceField
	 */
	public DataSourceField findByName(Long dataSourceId, String name);

	/**
	 * Find by reference: dataSource
	 *
	 * @param dataSource
	 * @return List<DataSourceField>
	 */
	public List<DataSourceField> findByDataSource(DataSource dataSource);

	/**
	 * Find by ID of reference: dataSource.id
	 *
	 * @param dataSourceId
	 * @return List<DataSourceField>
	 */
	public List<DataSourceField> findByDataSourceId(String dataSourceId);
}
