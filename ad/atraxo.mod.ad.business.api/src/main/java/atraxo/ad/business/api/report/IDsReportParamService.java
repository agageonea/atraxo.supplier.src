/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.report;

import atraxo.ad.domain.impl.report.DsReport;
import atraxo.ad.domain.impl.report.DsReportParam;
import atraxo.ad.domain.impl.report.ReportParam;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link DsReportParam} domain entity.
 */
public interface IDsReportParamService extends IEntityService<DsReportParam> {

	/**
	 * Find by unique key
	 *
	 * @return DsReportParam
	 */
	public DsReportParam findByRepParam_ds(DsReport dsReport,
			ReportParam reportParam);

	/**
	 * Find by unique key
	 *
	 * @return DsReportParam
	 */
	public DsReportParam findByRepParam_ds(Long dsReportId, Long reportParamId);

	/**
	 * Find by reference: dsReport
	 *
	 * @param dsReport
	 * @return List<DsReportParam>
	 */
	public List<DsReportParam> findByDsReport(DsReport dsReport);

	/**
	 * Find by ID of reference: dsReport.id
	 *
	 * @param dsReportId
	 * @return List<DsReportParam>
	 */
	public List<DsReportParam> findByDsReportId(String dsReportId);

	/**
	 * Find by reference: reportParam
	 *
	 * @param reportParam
	 * @return List<DsReportParam>
	 */
	public List<DsReportParam> findByReportParam(ReportParam reportParam);

	/**
	 * Find by ID of reference: reportParam.id
	 *
	 * @param reportParamId
	 * @return List<DsReportParam>
	 */
	public List<DsReportParam> findByReportParamId(String reportParamId);
}
