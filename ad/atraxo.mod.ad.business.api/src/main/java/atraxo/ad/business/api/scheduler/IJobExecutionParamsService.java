/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.api.scheduler;

import atraxo.ad.domain.impl.scheduler.JobExecution;
import atraxo.ad.domain.impl.scheduler.JobExecutionParams;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link JobExecutionParams} domain entity.
 */
public interface IJobExecutionParamsService
		extends
			IEntityService<JobExecutionParams> {

	/**
	 * Find by reference: jobExecution
	 *
	 * @param jobExecution
	 * @return List<JobExecutionParams>
	 */
	public List<JobExecutionParams> findByJobExecution(JobExecution jobExecution);

	/**
	 * Find by ID of reference: jobExecution.id
	 *
	 * @param jobExecutionId
	 * @return List<JobExecutionParams>
	 */
	public List<JobExecutionParams> findByJobExecutionId(Long jobExecutionId);
}
