Ext.define("atraxo.ad.i18n.frame.ExternalReport_Ui", {
	/* view */
	externalReportParameterList__ttl: ".Parameters",
	historySourceUploadList__ttl: ".Upload History",
	reportLogList__ttl: ".Reports log",
	wdwLog__ttl: ".Log",
	wdwNew__ttl: ".Upload new report design",
	wdwReportFormat__ttl: ".Format",
	/* menu */
	/* button */
	btnCancel__lbl: ".Cancel",
	btnCancel__tlp: ".Cancel",
	btnCancelRunReport__lbl: ".Cancel",
	btnCancelRunReport__tlp: ".Cancel",
	btnCloseLog__lbl: ".Close",
	btnCloseLog__tlp: ".Close",
	btnDeleteHistory__lbl: ".Delete",
	btnDeleteHistory__tlp: ".Delete",
	btnEmptyHistory__lbl: ".Empty",
	btnEmptyHistory__tlp: ".Empty",
	btnEmptyLog__lbl: ".Empty",
	btnEmptyLog__tlp: ".Empty",
	btnHelp__lbl: ".Help",
	btnHelp__tlp: ".Help",
	btnRunReport__lbl: ".Run",
	btnRunReport__tlp: ".Run",
	btnRunReportTemplate__lbl: ".Run report",
	btnRunReportTemplate__tlp: ".Run report",
	btnSelectFile__lbl: ".Select file",
	btnSelectFile__tlp: ".Select file",
	btnUploadReportTemplate__lbl: ".Upload report",
	btnUploadReportTemplate__tlp: ".Upload report",
	btnViewLog__lbl: ".View log",
	btnViewLog__tlp: ".View log",
	
	title: ".Reports"
});
