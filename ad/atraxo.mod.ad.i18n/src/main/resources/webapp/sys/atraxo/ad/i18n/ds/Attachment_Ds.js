Ext.define("atraxo.ad.i18n.ds.Attachment_Ds", {
	baseUrl__lbl: ".Base Url",
	categType__lbl: ".Category",
	category__lbl: ".Category",
	contentType__lbl: ".Content Type",
	fileName__lbl: ".File Name",
	filePath__lbl: "",
	location__lbl: ".Location",
	name__lbl: ".Name",
	targetAlias__lbl: ".Target Alias",
	targetRefid__lbl: ".Target Refid",
	targetType__lbl: ".Target Type",
	typeId__lbl: ".Type(ID)",
	type__lbl: ".Type",
	url__lbl: ".Url"
});
