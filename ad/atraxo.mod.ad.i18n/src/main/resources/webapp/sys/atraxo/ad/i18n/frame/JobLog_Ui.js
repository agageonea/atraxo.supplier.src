Ext.define("atraxo.ad.i18n.frame.JobLog_Ui", {
	/* view */
	logList__ttl: ".Job logs",
	msgList__ttl: ".Messages",
	/* menu */
	/* button */
	btnShowJob__lbl: ".Show job",
	btnShowJob__tlp: "",
	btnShowJobContext__lbl: ".Show job context",
	btnShowJobContext__tlp: "",
	btnShowJobTimer__lbl: ".Show job timer",
	btnShowJobTimer__tlp: "",
	
	title: ".Job logs"
});
