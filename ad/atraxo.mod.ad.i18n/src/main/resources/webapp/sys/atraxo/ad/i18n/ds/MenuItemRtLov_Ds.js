Ext.define("atraxo.ad.i18n.ds.MenuItemRtLov_Ds", {
	bundle__lbl: ".Bundle",
	frame__lbl: ".Frame",
	glyph__lbl: ".Glyph",
	iconUrl__lbl: ".Icon Url",
	leaf__lbl: ".Leaf Node",
	menuId__lbl: ".Menu(ID)",
	menuItemId__lbl: ".Menu Item(ID)",
	menuItem__lbl: ".Menu Item",
	menu__lbl: ".Menu",
	separatorAfter__lbl: ".Separator After",
	separatorBefore__lbl: ".Separator Before",
	sequenceNo__lbl: ".Sequence No",
	text__lbl: ".Title",
	title__lbl: ".Title"
});
