Ext.define("atraxo.ad.i18n.ds.Client_Ds", {
	adminPasswordRe__lbl: ".Confirm password",
	adminPassword__lbl: ".Password",
	adminRole__lbl: ".Admin Role",
	adminUserCode__lbl: ".Code",
	adminUserLogin__lbl: ".Login name",
	adminUserName__lbl: ".Name",
	exportPath__lbl: ".Export Path",
	importPath__lbl: ".Import Path",
	initFileLocation__lbl: ".Init File Location",
	product__lbl: ".Product",
	tempPath__lbl: ".Temp Path",
	workspacePath__lbl: ".Workspace Path"
});
