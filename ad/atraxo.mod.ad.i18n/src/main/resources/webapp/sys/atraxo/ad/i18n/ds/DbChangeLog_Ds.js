Ext.define("atraxo.ad.i18n.ds.DbChangeLog_Ds", {
	author__lbl: ".Author",
	comments__lbl: ".Comments",
	dateExecuted_From__lbl: ".Date Executed(From)",
	dateExecuted_To__lbl: ".Date Executed(To)",
	dateExecuted__lbl: ".Date Executed",
	description__lbl: ".Description",
	filename__lbl: ".Filename",
	id__lbl: ".Id",
	liquibase__lbl: ".Liquibase",
	md5sum__lbl: ".Md5sum",
	orderExecuted_From__lbl: ".Order Executed(From)",
	orderExecuted_To__lbl: ".Order Executed(To)",
	orderExecuted__lbl: ".Order Executed",
	tag__lbl: ".Tag",
	txid__lbl: ".Txid"
});
