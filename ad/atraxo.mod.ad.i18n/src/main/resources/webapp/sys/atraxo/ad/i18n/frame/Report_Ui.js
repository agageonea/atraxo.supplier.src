Ext.define("atraxo.ad.i18n.frame.Report_Ui", {
	/* view */
	dsRep__ttl: ".DS links",
	dsparamList__ttl: ".Parameter mapping",
	paramEditList__ttl: ".Parameters",
	usageList__ttl: ".Usage",
	wdwTestReport__ttl: ".Test report",
	/* menu */
	/* button */
	btnCancelReport__lbl: ".Cancel",
	btnCancelReport__tlp: ".Cancel",
	btnRunReport__lbl: ".Run",
	btnRunReport__tlp: ".Run",
	btnTestReport__lbl: ".Test report",
	btnTestReport__tlp: ".Test report",
	
	title: ".Reports"
});
