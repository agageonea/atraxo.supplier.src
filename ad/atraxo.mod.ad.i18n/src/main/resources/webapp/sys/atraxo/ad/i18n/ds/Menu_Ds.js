Ext.define("atraxo.ad.i18n.ds.Menu_Ds", {
	glyph__lbl: ".Glyph",
	sequenceNo__lbl: ".Sequence No",
	tag__lbl: ".Tag",
	title__lbl: ".Title",
	withRoleId__lbl: ".With Role Id",
	withRole__lbl: ".With Role"
});
