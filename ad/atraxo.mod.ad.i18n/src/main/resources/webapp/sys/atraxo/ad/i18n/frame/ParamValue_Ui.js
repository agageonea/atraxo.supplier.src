Ext.define("atraxo.ad.i18n.frame.ParamValue_Ui", {
	/* view */
	/* menu */
	/* button */
	btnPublish__lbl: ".Publish changes",
	btnPublish__tlp: ".Publish the changes",
	btnShowParamsFrame__lbl: ".Open parameters frame",
	btnShowParamsFrame__tlp: ".Show the parameters frame",
	
	title: ".Parameter values"
});
