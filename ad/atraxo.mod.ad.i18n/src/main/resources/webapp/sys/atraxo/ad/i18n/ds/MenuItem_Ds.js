Ext.define("atraxo.ad.i18n.ds.MenuItem_Ds", {
	bundle__lbl: ".Bundle",
	foldersOnly__lbl: ".Folders Only",
	frame__lbl: ".Frame",
	framesOnly__lbl: ".Frames Only",
	glyph__lbl: ".Glyph",
	iconUrl__lbl: ".Icon Url",
	menuId__lbl: ".Menu(ID)",
	menuItemId__lbl: ".Menu Item(ID)",
	menuItem__lbl: ".Menu Item",
	menu__lbl: ".Menu",
	separatorAfter__lbl: ".Separator After",
	separatorBefore__lbl: ".Separator Before",
	sequenceNo__lbl: ".Sequence No",
	title__lbl: ".Title",
	withRoleId__lbl: ".With Role Id",
	withRole__lbl: ".With Role"
});
