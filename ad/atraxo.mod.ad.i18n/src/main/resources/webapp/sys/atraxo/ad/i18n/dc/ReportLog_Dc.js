
Ext.define("atraxo.ad.i18n.dc.ReportLog_Dc$List", {
	format__lbl: ".Format",
	reportName__lbl: ".Report name",
	runBy__lbl: ".Run by",
	runOn__lbl: ".Run on",
	status__lbl: ".Status"
});

Ext.define("atraxo.ad.i18n.dc.ReportLog_Dc$LogForm", {
	log__lbl: "."
});
