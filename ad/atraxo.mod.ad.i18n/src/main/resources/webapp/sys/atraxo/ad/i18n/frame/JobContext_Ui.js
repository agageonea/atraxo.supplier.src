Ext.define("atraxo.ad.i18n.frame.JobContext_Ui", {
	/* view */
	logList__ttl: ".Execution logs",
	paramsEditList__ttl: ".Parameter values",
	scheduleEditList__ttl: ".Execution schedule",
	/* menu */
	/* button */
	btnShowLog__lbl: ".Show log details",
	btnShowLog__tlp: "",
	btnSyncParams__lbl: ".Synchronize parameters",
	btnSyncParams__tlp: ".Synchronize context parameters with job definition parameters.",
	
	title: ".Job context"
});
