Ext.define("atraxo.ad.i18n.ds.JobTimer_Ds", {
	cronExpression__lbl: ".Cron Expression",
	endTime_From__lbl: ".End Time(From)",
	endTime_To__lbl: ".End Time(To)",
	endTime__lbl: ".End Time",
	jobContextId__lbl: ".Job Context(ID)",
	jobContext__lbl: ".Job Context",
	jobName__lbl: ".Job Name",
	repeatCount__lbl: ".Repeat Count",
	repeatIntervalType__lbl: ".Repeat Interval Type",
	repeatInterval__lbl: ".Repeat Interval",
	startTime_From__lbl: ".Start Time(From)",
	startTime_To__lbl: ".Start Time(To)",
	startTime__lbl: ".Start Time",
	type__lbl: ".Type"
});
