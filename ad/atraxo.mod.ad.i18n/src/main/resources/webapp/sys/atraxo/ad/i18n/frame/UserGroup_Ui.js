Ext.define("atraxo.ad.i18n.frame.UserGroup_Ui", {
	/* view */
	usrList__ttl: ".Users",
	/* menu */
	/* button */
	btnAsgnUsers__lbl: ".Assign-Users",
	btnAsgnUsers__tlp: ".Add users to selected group",
	btnDetailsUser__lbl: ".User details",
	btnDetailsUser__tlp: ".Open frame to show more details for the selected user",
	
	title: ".User groups"
});
