Ext.define("atraxo.ad.i18n.ds.JobLog_Ds", {
	endTime_From__lbl: ".End Time(From)",
	endTime_To__lbl: ".End Time(To)",
	endTime__lbl: ".End Time",
	jobContextId__lbl: ".Job Context(ID)",
	jobContext__lbl: ".Job Context",
	jobName__lbl: ".Job Name",
	jobTimerId__lbl: ".Job Timer(ID)",
	jobTimer__lbl: ".Job Timer",
	startTime_From__lbl: ".Start Time(From)",
	startTime_To__lbl: ".Start Time(To)",
	startTime__lbl: ".Start Time"
});
