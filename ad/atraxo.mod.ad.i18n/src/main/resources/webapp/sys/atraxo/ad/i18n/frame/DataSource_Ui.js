Ext.define("atraxo.ad.i18n.frame.DataSource_Ui", {
	/* view */
	fieldsList__ttl: ".Fields",
	rpcsList__ttl: ".Services",
	/* menu */
	/* button */
	btnInfo__lbl: ".Show info",
	btnInfo__tlp: ".Show more information.",
	btnSynchronize__lbl: ".Synchronize",
	btnSynchronize__tlp: ".Scan classpath and synchronize catalog with deployed instances.",
	
	title: ".Data-sources"
});
