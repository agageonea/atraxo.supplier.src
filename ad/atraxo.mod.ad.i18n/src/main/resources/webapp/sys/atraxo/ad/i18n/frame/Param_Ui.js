Ext.define("atraxo.ad.i18n.frame.Param_Ui", {
	/* view */
	/* menu */
	/* button */
	btnSynchronize__lbl: ".Synchronize",
	btnSynchronize__tlp: ".Scan classpath and synchronize catalog with parameters declared by modules.",
	
	title: ".System parameters"
});
