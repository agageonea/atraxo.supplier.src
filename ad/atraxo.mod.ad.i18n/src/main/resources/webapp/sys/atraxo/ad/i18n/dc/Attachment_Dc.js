
Ext.define("atraxo.ad.i18n.dc.Attachment_Dc$New", {
	btnCancel__lbl: ".Cancel",
	btnSaveLink__lbl: ".Save",
	btnSelectFile__lbl: ".Select file",
	name__lbl: ".Display name",
	type__lbl: ".Document type"
});

Ext.define("atraxo.ad.i18n.dc.Attachment_Dc$NewWithName", {
	btnCancel__lbl: ".Cancel",
	btnSelectFile__lbl: ".Select file",
	name__lbl: ".Display name",
	type__lbl: ".Document type"
});
