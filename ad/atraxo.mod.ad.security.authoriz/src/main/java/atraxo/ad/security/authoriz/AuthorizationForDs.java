/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.security.authoriz;

import java.util.List;

import javax.persistence.TypedQuery;

import seava.j4e.api.Constants;
import seava.j4e.api.exceptions.NotAuthorizedRequestException;
import seava.j4e.api.security.IAuthorization;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.Session;
import atraxo.ad.domain.impl.security.AccessControl;
import atraxo.ad.domain.impl.security.AccessControlDs;
import atraxo.ad.domain.impl.security.AccessControlDsRpc;

public class AuthorizationForDs extends AbstractSecurity implements IAuthorization {

	@Override
	public void authorize(String dsName, String action, String rpcName) throws Exception {

		IUser user = Session.user.get();

		if (user.isSystemUser() || user.getProfile().isAdministrator()) {
			return;
		}

		String filter = null;
		String eql = null;

		if (action.equals(Constants.DS_ACTION_RPC)) {
			filter = " and e.serviceMethod = :serviceMethod ";
			eql = "select 1 from " + AccessControlDsRpc.class.getSimpleName() + " e where e.clientId = :clientId " + " and e.dsName = :dsName "
					+ filter + " and e.accessControl.id in ( select p.id from " + AccessControl.class.getSimpleName()
					+ " p, IN (p.roles) c where c.code in :roles ) ";
		} else {
			filter = this.actionFilter(dsName, action);
			eql = "select 1 from " + AccessControlDs.class.getSimpleName() + " e where e.clientId = :clientId " + " and e.dsName = :dsName " + filter
					+ " and e.accessControl.id in ( select p.id from " + AccessControl.class.getSimpleName()
					+ " p, IN (p.roles) c where c.code in :roles ) ";
		}

		TypedQuery<Integer> query = this.getEntityManager().createQuery(eql, Integer.class).setParameter("dsName", dsName)
				.setParameter("clientId", Session.user.get().getClientId()).setParameter("roles", user.getProfile().getRoles());

		if (action.equals(Constants.DS_ACTION_RPC)) {
			query.setParameter("serviceMethod", rpcName);
		}

		List<Integer> result = query.getResultList();
		if (result.isEmpty()) {
			String actionMessage = null;
			switch(action) {
			case("find") :
			{
				actionMessage = "see this.";
				break;
			}
			case("delete") :
			{
				actionMessage = "delete this entity.";
				break;
			}
			case("update") :
			{
				actionMessage = "update this entity.";
				break;
			}
			case("insert") :
			{
				actionMessage = "create this entity.";
				break;
			}
			default:
				actionMessage = "perform this action.";
			}		
			
			throw new NotAuthorizedRequestException("You don't have enough privileges to " + actionMessage);
		}
	}

	private String actionFilter(String dsName, String action) {

		if (action.equals(Constants.DS_ACTION_QUERY)) {
			return " and e.queryAllowed = true ";
		} else if (action.equals(Constants.DS_ACTION_EXPORT)) {
			return " and e.exportAllowed = true ";
		} else if (action.equals(Constants.DS_ACTION_IMPORT)) {
			return " and e.importAllowed = true ";
		} else if (action.equals(Constants.DS_ACTION_INSERT)) {
			return " and e.insertAllowed = true ";
		} else if (action.equals(Constants.DS_ACTION_UPDATE)) {
			return " and e.updateAllowed = true ";
		} else if (action.equals(Constants.DS_ACTION_DELETE)) {
			return " and e.deleteAllowed = true ";
		}
		return "";
	}

}
