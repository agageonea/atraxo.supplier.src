/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.security.authoriz;

import seava.j4e.api.security.IAuthorization;

public class AuthorizationForJob extends AbstractSecurity implements IAuthorization {

	@Override
	public void authorize(String resourceName, String action, String rpcName) throws Exception {
		// If it doesn't throw exception is authorized
	}

}
