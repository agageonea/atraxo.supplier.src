Ext.override(atraxo.ad.i18n.ds.DsReportUsageRt_Ds, {
	dcKey__lbl: "Data controler key",
	dsReportId__lbl: "DS report (ID)",
	frameName__lbl: "Frame name",
	queryBuilderClass__lbl: "Query builder class",
	reportCode__lbl: "Report",
	reportContextPath__lbl: "Context path",
	reportId__lbl: "Report (ID)",
	reportTitle__lbl: "Report name",
	sequenceNo__lbl: "Sequence #",
	serverUrl__lbl: "Url",
	toolbarKey__lbl: "Toolbar key"
});
