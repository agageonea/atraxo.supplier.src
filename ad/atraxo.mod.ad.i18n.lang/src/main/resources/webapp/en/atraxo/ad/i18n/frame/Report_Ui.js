Ext.override(atraxo.ad.i18n.frame.Report_Ui, {
	/* view */
	dsRep__ttl: "Data Store Links",
	dsparamList__ttl: "Parameter Mapping",
	paramEditList__ttl: "Parameters",
	usageList__ttl: "Usage",
	wdwTestReport__ttl: "Test Report",
	/* menu */
	/* button */
	btnCancelReport__lbl: "Cancel",
	btnCancelReport__tlp: "Cancel",
	btnRunReport__lbl: "Run",
	btnRunReport__tlp: "Run",
	btnTestReport__lbl: "Test report",
	btnTestReport__tlp: "Test report",
	
	title: "Reports"
});
