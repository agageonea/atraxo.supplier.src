Ext.override(atraxo.ad.i18n.ds.Role_Ds, {
	withPrivilegeId__lbl: "With privilege ID",
	withPrivilege__lbl: "With privilege",
	withUserId__lbl: "With user ID",
	withUser__lbl: "With user"
});
