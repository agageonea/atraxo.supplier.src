Ext.override(atraxo.ad.i18n.ds.Menu_Ds, {
	glyph__lbl: "Glyph",
	sequenceNo__lbl: "Sequence #",
	tag__lbl: "Tag",
	titleAr__lbl: "Title Ar",
	titleDe__lbl: "Title De",
	titleEn__lbl: "Title En",
	titleEs__lbl: "Title Es",
	titleFr__lbl: "Title Fr",
	titleJp__lbl: "Title Jp",
	title__lbl: "Title",
	withRoleId__lbl: "With role ID",
	withRole__lbl: "With role"
});
