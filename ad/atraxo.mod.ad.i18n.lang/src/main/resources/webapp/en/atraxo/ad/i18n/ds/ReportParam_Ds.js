Ext.override(atraxo.ad.i18n.ds.ReportParam_Ds, {
	dataType__lbl: "Data type",
	defaultValue__lbl: "Default value",
	listOfValues__lbl: "List of values",
	mandatory__lbl: "Mandatory",
	noEdit__lbl: "No edit",
	reportCode__lbl: "Report",
	reportId__lbl: "Report (ID)",
	sequenceNo__lbl: "Sequence #",
	title__lbl: "Title"
});
