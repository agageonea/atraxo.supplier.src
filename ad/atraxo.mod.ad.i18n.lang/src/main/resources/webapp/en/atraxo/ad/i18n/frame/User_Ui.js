Ext.override(atraxo.ad.i18n.frame.User_Ui, {
	/* view */
	grpList__ttl: "Groups",
	rolList__ttl: "Roles",
	wdwChangePassword__ttl: "Change Password",
	/* menu */
	/* button */
	btnAsgnGroups__lbl: "Assign groups",
	btnAsgnGroups__tlp: "Add to user-groups",
	btnAsgnRoles__lbl: "Assign roles",
	btnAsgnRoles__tlp: "Assign roles",
	btnChangePassword__lbl: "Change password",
	btnChangePassword__tlp: "Change the user`s password",
	btnDetailsGroup__lbl: "Group details",
	btnDetailsGroup__tlp: "Open frame to show more details for the selected group",
	btnDetailsRole__lbl: "Role details",
	btnDetailsRole__tlp: "Open frame to show more details for the selected role",
	btnSavePassword__lbl: "Save",
	btnSavePassword__tlp: "Save new password",
	
	title: "Users"
});
