Ext.override(atraxo.ad.i18n.frame.AccessControl_Ui, {
	/* view */
	asgnAccessCtxEditList__ttl: "Access Rules - Assign",
	dsAccessCtxEditList__ttl: "Access Rules - Data Store",
	dsMtdAccessCtxEditList__ttl: "Access Rules - Services",
	rolList__ttl: "Roles",
	wdwCopyRules__ttl: "Select Source",
	/* menu */
	/* button */
	btnAsgnRoles__lbl: "Assign roles",
	btnAsgnRoles__tlp: "Assign selected privilege to roles",
	btnCopyRules__lbl: "Copy rules",
	btnCopyRules__tlp: "Copy rules from another privilege",
	btnCopyRulesExec__lbl: "OK",
	btnCopyRulesExec__tlp: "Copy rules from selected privilege",
	btnDetailsRole__lbl: "Role details",
	btnDetailsRole__tlp: "Open frame to show more details for the selected role",
	btnShowUiAsgnRules__lbl: "Assign rules",
	btnShowUiAsgnRules__tlp: "Open assignment rules frame",
	btnShowUiDsRules__lbl: "Data store rules",
	btnShowUiDsRules__tlp: "Open data-source rules frame",
	btnShowUiServiceRules__lbl: "Service rules",
	btnShowUiServiceRules__tlp: "Open data-source service rules frame",
	
	title: "Privileges"
});
