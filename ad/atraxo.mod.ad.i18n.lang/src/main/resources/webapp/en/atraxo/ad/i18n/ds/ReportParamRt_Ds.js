Ext.override(atraxo.ad.i18n.ds.ReportParamRt_Ds, {
	dataType__lbl: "Data type",
	listOfValues__lbl: "List of values",
	mandatory__lbl: "Mandatory",
	noEdit__lbl: "No edit",
	reportId__lbl: "Report (ID)",
	report__lbl: "Report",
	sequenceNo__lbl: "Sequence #",
	title__lbl: "Parameter",
	value__lbl: "Value"
});
