Ext.override(atraxo.ad.i18n.ds.DsReportParamRt_Ds, {
	dataSource__lbl: "Data source",
	dsField__lbl: "Data store field",
	dsReportId__lbl: "DS report (ID)",
	paramDataType__lbl: "Data type",
	paramDefaultValue__lbl: "Default value",
	paramId__lbl: "Report param (ID)",
	paramListOfValues__lbl: "List of values",
	paramMandatory__lbl: "Mandatory",
	paramNoEdit__lbl: "No edit",
	param__lbl: "Report Parameter",
	reportCode__lbl: "Report",
	reportId__lbl: "Report (ID)",
	staticValue__lbl: "Static value"
});
