Ext.override(atraxo.ad.i18n.ds.DbChangeLog_Ds, {
	author__lbl: "Author",
	comments__lbl: "Comments",
	dateExecuted_From__lbl: "Date executed (from)",
	dateExecuted_To__lbl: "Date executed (to)",
	dateExecuted__lbl: "Date executed",
	description__lbl: "Description",
	filename__lbl: "File name",
	id__lbl: "ID",
	liquibase__lbl: "Liquibase",
	md5sum__lbl: "Md5sum",
	orderExecuted_From__lbl: "Order executed (from)",
	orderExecuted_To__lbl: "Order executed (to)",
	orderExecuted__lbl: "Order executed",
	tag__lbl: "Tag",
	txid__lbl: "Txid"
});
