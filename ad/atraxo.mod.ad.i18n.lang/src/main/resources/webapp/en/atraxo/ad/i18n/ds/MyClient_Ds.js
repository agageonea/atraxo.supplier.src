Ext.override(atraxo.ad.i18n.ds.MyClient_Ds, {
	adminRole__lbl: "Admin role",
	exportPath__lbl: "Export path",
	importPath__lbl: "Import path",
	tempPath__lbl: "Temporary path",
	workspacePath__lbl: "Workspace path"
});
