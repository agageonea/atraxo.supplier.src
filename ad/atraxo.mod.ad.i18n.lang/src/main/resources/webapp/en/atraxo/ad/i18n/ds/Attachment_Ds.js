Ext.override(atraxo.ad.i18n.ds.Attachment_Ds, {
	baseUrl__lbl: "Base Url",
	categType__lbl: "Category",
	category__lbl: "Category",
	contentType__lbl: "Content type",
	fileName__lbl: "File name",
	filePath__lbl: "",
	location__lbl: "Location",
	name__lbl: "Name",
	targetAlias__lbl: "Target alias",
	targetRefid__lbl: "Target ref ID",
	targetType__lbl: "Target type",
	typeId__lbl: "Type (ID)",
	type__lbl: "Type",
	url__lbl: "Url"
});
