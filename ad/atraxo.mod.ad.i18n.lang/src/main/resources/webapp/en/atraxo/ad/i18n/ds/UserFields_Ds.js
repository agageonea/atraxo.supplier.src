Ext.override(atraxo.ad.i18n.ds.UserFields_Ds, {
	businessAreaId__lbl: "Business Area (ID)",
	businessArea__lbl: "Business object",
	businessArea__tlp: "Name",
	defaultValue__lbl: "Default value",
	defaultValue__tlp: "User field default value",
	description__lbl: "Description",
	description__tlp: "User field description",
	dsAlias__lbl: "Data source alias",
	dsAlias__tlp: "Data source alias",
	dsModel__lbl: "Data source model",
	dsModel__tlp: "Data source model",
	dsName__lbl: "Data source name",
	dsName__tlp: "Data source name",
	name__lbl: "Name",
	name__tlp: "User field name",
	type__lbl: "Type",
	type__tlp: "User field type"
});
