Ext.override(atraxo.ad.i18n.ds.User_Ds, {
	code__lbl: "Code",
	confirmPassword__lbl: "Confirm password",
	dateFormatId__lbl: "Date format (ID)",
	dateFormat__lbl: "Date format",
	decimalSeparator__lbl: "Decimal separator",
	email__lbl: "Email",
	inGroupId__lbl: "In group ID",
	inGroup__lbl: "In group",
	locked__lbl: "Locked",
	loginName__lbl: "Login name",
	newPassword__lbl: "New password",
	thousandSeparator__lbl: "Thousand separator",
	withRoleId__lbl: "With role ID",
	withRole__lbl: "With role"
});
