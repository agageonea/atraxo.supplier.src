Ext.override(atraxo.ad.i18n.ds.MenuItemLov_Ds, {
	foldersOnly__lbl: "Folders only",
	framesOnly__lbl: "Frames only",
	titleAr__lbl: "Title Ar",
	titleDe__lbl: "Title De",
	titleEn__lbl: "Title En",
	titleEs__lbl: "Title Es",
	titleFr__lbl: "Title Fr",
	titleJp__lbl: "Title Jp",
	title__lbl: "Title"
});
