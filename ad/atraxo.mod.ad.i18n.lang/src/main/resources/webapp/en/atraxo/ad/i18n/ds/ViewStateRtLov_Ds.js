Ext.override(atraxo.ad.i18n.ds.ViewStateRtLov_Ds, {
	cmpType__lbl: "Cmp type",
	cmp__lbl: "Cmp",
	hideMine__lbl: "Hide mine",
	hideOthers__lbl: "Hide others",
	owner__lbl: "Created by",
	value__lbl: "Value"
});
