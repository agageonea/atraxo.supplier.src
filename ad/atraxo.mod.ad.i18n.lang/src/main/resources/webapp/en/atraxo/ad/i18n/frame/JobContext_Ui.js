Ext.override(atraxo.ad.i18n.frame.JobContext_Ui, {
	/* view */
	logList__ttl: "Execution Logs",
	paramsEditList__ttl: "Parameter Values",
	scheduleEditList__ttl: "Execution Schedule",
	/* menu */
	/* button */
	btnShowLog__lbl: "Show log details",
	btnShowLog__tlp: "",
	btnSyncParams__lbl: "Synchronize parameters",
	btnSyncParams__tlp: "Synchronize context parameters with job definition parameters.",
	
	title: "Job context"
});
