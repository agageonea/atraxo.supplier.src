Ext.override(atraxo.ad.i18n.frame.ReportRt_Ui, {
	/* view */
	wdwParams__ttl: "Parameters",
	/* menu */
	/* button */
	btnCancelReport__lbl: "Cancel",
	btnCancelReport__tlp: "Cancel",
	btnRunReport__lbl: "Run report",
	btnRunReport__tlp: "Run report",
	btnShowParamsWdw__lbl: "Show parameters",
	btnShowParamsWdw__tlp: "Show parameter form to run report",
	
	title: "Runtime center"
});
