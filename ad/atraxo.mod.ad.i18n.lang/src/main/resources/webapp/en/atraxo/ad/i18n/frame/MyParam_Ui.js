Ext.override(atraxo.ad.i18n.frame.MyParam_Ui, {
	/* view */
	wdwInitVal__ttl: "Set Validity",
	/* menu */
	/* button */
	btnInitVal__lbl: "Initialize values",
	btnInitVal__tlp: "Open window to create initial values for the selected parameters (Ignores those already initialized).",
	btnOk__lbl: "OK",
	btnOk__tlp: "OK",
	btnPublish__lbl: "Publish changes",
	btnPublish__tlp: "Publish changes",
	btnShowValuesFrame__lbl: "Open values frame",
	btnShowValuesFrame__tlp: "Show the values frame",
	
	title: "Parameters"
});
