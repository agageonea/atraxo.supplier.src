Ext.override(atraxo.ad.i18n.ds.MenuRtLov_Ds, {
	glyph__lbl: "Glyph",
	sequenceNo__lbl: "Sequence #",
	tag__lbl: "Tag",
	titleAr__lbl: "Title Ar",
	titleDe__lbl: "Title De",
	titleEn__lbl: "Title En",
	titleEs__lbl: "Title Es",
	titleFr__lbl: "Title Fr",
	titleJp__lbl: "Title Jp",
	title__lbl: "Title"
});
