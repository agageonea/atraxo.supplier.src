Ext.override(atraxo.ad.i18n.ds.ExternalReportFWLov_Ds, {
	businessAreaId__lbl: "Business Area (ID)",
	businessArea__lbl: "Business area",
	businessArea__tlp: "Name",
	clientId__lbl: "Client Id",
	description__lbl: "Description",
	designName__lbl: "Design name",
	dsName__lbl: "Data source name",
	dsName__tlp: "Data source name",
	entityAlias__lbl: "Entity alias",
	entityFqn__lbl: "Entity Fqn",
	id__lbl: "ID",
	name__lbl: "Report name",
	reportCode__lbl: "Report code",
	system__lbl: "System",
	system__tlp: "Is this a system report?"
});
