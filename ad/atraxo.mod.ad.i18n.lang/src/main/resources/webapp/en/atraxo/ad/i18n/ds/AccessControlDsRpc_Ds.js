Ext.override(atraxo.ad.i18n.ds.AccessControlDsRpc_Ds, {
	accessControlId__lbl: "Access control (ID)",
	accessControl__lbl: "Access control",
	dsName__lbl: "Data store name",
	serviceMethod__lbl: "Allow method"
});
