Ext.override(atraxo.ad.i18n.frame.Menu_Ui, {
	/* view */
	canvasItem__ttl: "Menu Items",
	canvasMenu__ttl: "Menus",
	/* menu */
	/* button */
	btnAsgnItemRoles__lbl: "Assign roles",
	btnAsgnItemRoles__tlp: "Assign roles to the selected menu item",
	btnAsgnMenuRoles__lbl: "Assign roles",
	btnAsgnMenuRoles__tlp: "Assign roles to the selected menu",
	
	title: "Menus"
});
