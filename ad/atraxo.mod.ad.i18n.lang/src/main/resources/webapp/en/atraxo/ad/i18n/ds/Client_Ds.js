Ext.override(atraxo.ad.i18n.ds.Client_Ds, {
	adminPasswordRe__lbl: "Confirm password",
	adminPassword__lbl: "Password",
	adminRole__lbl: "Admin role",
	adminUserCode__lbl: "Code",
	adminUserLogin__lbl: "Login name",
	adminUserName__lbl: "Name",
	exportPath__lbl: "Export path",
	importPath__lbl: "Import path",
	initFileLocation__lbl: "Init file location",
	product__lbl: "Product",
	tempPath__lbl: "Temp path",
	workspacePath__lbl: "Workspace path"
});
