Ext.override(atraxo.ad.i18n.ds.JobTimer_Ds, {
	cronExpression__lbl: "Cron Expression",
	endTime_From__lbl: "End time (From)",
	endTime_To__lbl: "End time (To)",
	endTime__lbl: "End time",
	jobContextId__lbl: "Job Context (ID)",
	jobContext__lbl: "Job context",
	jobName__lbl: "Job name",
	repeatCount__lbl: "Repeat count",
	repeatIntervalType__lbl: "Repeat interval type",
	repeatInterval__lbl: "Repeat interval",
	startTime_From__lbl: "Start time (From)",
	startTime_To__lbl: "Start time (To)",
	startTime__lbl: "Start Time",
	type__lbl: "Type"
});
