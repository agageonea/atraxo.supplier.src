Ext.override(atraxo.ad.i18n.ds.ReportLog_Ds, {
	externalReportId__lbl: "External report (ID)",
	externalReportName__lbl: "External report",
	format__lbl: "Format",
	format__tlp: "Format",
	log__lbl: "Log",
	log__tlp: "Log",
	name__lbl: "Name",
	name__tlp: "Name",
	reportFile__lbl: "Report file",
	runBy__lbl: "Created by",
	runDate__lbl: "Run on",
	runDate__tlp: "Run on",
	status__lbl: "Status",
	status__tlp: "Status"
});
