Ext.override(atraxo.ad.i18n.ds.JobLog_Ds, {
	endTime_From__lbl: "End time (From)",
	endTime_To__lbl: "End time (To)",
	endTime__lbl: "End time",
	jobContextId__lbl: "Job Context (ID)",
	jobContext__lbl: "Job context",
	jobName__lbl: "Job name",
	jobTimerId__lbl: "Job Timer (ID)",
	jobTimer__lbl: "Job timer",
	startTime_From__lbl: "Start time (From)",
	startTime_To__lbl: "Start time (To)",
	startTime__lbl: "Start Time"
});
