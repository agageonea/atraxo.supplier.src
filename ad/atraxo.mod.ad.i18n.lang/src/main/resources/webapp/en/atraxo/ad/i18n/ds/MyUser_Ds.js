Ext.override(atraxo.ad.i18n.ds.MyUser_Ds, {
	dateFormatId__lbl: "Date format (ID)",
	dateFormat__lbl: "Date format",
	decimalSeparator__lbl: "Decimal separator",
	email__lbl: "Email",
	loginName__lbl: "Login name",
	thousandSeparator__lbl: "Thousand separator"
});
