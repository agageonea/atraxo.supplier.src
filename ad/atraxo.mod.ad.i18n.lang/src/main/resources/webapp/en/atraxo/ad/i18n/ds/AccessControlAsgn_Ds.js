Ext.override(atraxo.ad.i18n.ds.AccessControlAsgn_Ds, {
	accessControlId__lbl: "Access control (ID)",
	accessControl__lbl: "Access control",
	asgnName__lbl: "Assign name",
	exportAllowed__lbl: "Export",
	importAllowed__lbl: "Import",
	queryAllowed__lbl: "Read",
	updateAllowed__lbl: "Write"
});
