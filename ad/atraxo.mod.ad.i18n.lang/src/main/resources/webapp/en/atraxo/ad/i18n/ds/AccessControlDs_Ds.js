Ext.override(atraxo.ad.i18n.ds.AccessControlDs_Ds, {
	accessControlId__lbl: "Access control (ID)",
	accessControl__lbl: "Access control",
	deleteAllowed__lbl: "Delete",
	dsName__lbl: "Data store name",
	exportAllowed__lbl: "Export",
	importAllowed__lbl: "Import",
	insertAllowed__lbl: "Insert",
	queryAllowed__lbl: "Query",
	updateAllowed__lbl: "Update",
	withRoleId__lbl: "Role ID",
	withRole__lbl: "Role"
});
