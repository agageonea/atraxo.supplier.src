Ext.override(atraxo.ad.i18n.ds.BusinessArea_Ds, {
	dsAlias__lbl: "Data source alias",
	dsAlias__tlp: "Data source alias",
	dsModel__lbl: "Data source model",
	dsModel__tlp: "Data source model",
	dsName__lbl: "Data source name",
	dsName__tlp: "Data source name",
	name__lbl: "Name",
	name__tlp: "Name"
});
