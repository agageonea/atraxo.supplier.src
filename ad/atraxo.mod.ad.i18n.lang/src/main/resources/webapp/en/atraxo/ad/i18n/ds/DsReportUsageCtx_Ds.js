Ext.override(atraxo.ad.i18n.ds.DsReportUsageCtx_Ds, {
	dcKey__lbl: "Data controller key",
	dsReportId__lbl: "DS report (ID)",
	frameName__lbl: "Frame name",
	reportId__lbl: "Report (ID)",
	report__lbl: "Report",
	sequenceNo__lbl: "Sequence #",
	toolbarKey__lbl: "Toolbar key"
});
