Ext.override(atraxo.ad.i18n.ds.AccessControl_Ds, {
	copyFromId__lbl: "Copy From ID",
	copyFrom__lbl: "Copy from",
	resetRules__lbl: "Reset rules",
	skipAsgn__lbl: "Skip assign",
	skipDs__lbl: "Skip data store",
	withRoleId__lbl: "With role ID",
	withRole__lbl: "With role"
});
