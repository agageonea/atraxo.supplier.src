Ext.override(atraxo.ad.i18n.ds.DsReportParam_Ds, {
	dataSource__lbl: "Data source",
	dsField__lbl: "Data store field",
	dsReportId__lbl: "DS report (ID)",
	paramId__lbl: "Report param (ID)",
	paramTitle__lbl: "Title",
	param__lbl: "Report Parameter",
	reportCode__lbl: "Report",
	reportId__lbl: "Report (ID)",
	staticValue__lbl: "Static value"
});
