Ext.override(atraxo.ad.i18n.ds.SourceUploadHistory_Ds, {
	externalReportId__lbl: "External report (ID)",
	externalReportName__lbl: "External report",
	fileName__lbl: "File name",
	fileName__tlp: "File name",
	file__lbl: "",
	name__lbl: "Name",
	name__tlp: "Name",
	notes__lbl: "Notes",
	reportId__lbl: "Report ID",
	reportName__lbl: "Report Name",
	uploadDate__lbl: "Date of upload",
	uploadDate__tlp: "Date of upload",
	uploader__lbl: "User name",
	uploader__tlp: "User name"
});
