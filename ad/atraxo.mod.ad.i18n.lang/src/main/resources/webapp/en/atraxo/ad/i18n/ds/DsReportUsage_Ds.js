Ext.override(atraxo.ad.i18n.ds.DsReportUsage_Ds, {
	dataSource__lbl: "Data source",
	dcKey__lbl: "Data controler key",
	dsReportId__lbl: "DS report (ID)",
	frameName__lbl: "Frame name",
	reportId__lbl: "Report (ID)",
	reportName__lbl: "Report name",
	report__lbl: "Report",
	sequenceNo__lbl: "Sequence #",
	toolbarKey__lbl: "Toolbar key"
});
