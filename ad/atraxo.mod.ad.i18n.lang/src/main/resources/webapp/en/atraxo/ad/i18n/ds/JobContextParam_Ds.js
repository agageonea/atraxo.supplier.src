Ext.override(atraxo.ad.i18n.ds.JobContextParam_Ds, {
	dataType__lbl: "Data type",
	jobContextId__lbl: "Job Context (ID)",
	jobContext__lbl: "Job context",
	jobName__lbl: "Job name",
	paramName__lbl: "Parameter name",
	value__lbl: "Value"
});
