Ext.override(atraxo.ad.i18n.ds.ParamValue_Ds, {
	sysParam__lbl: "System parameter",
	validAt__lbl: "Valid at",
	validFrom__lbl: "Valid from",
	validTo__lbl: "Valid to",
	value__lbl: "Value"
});
