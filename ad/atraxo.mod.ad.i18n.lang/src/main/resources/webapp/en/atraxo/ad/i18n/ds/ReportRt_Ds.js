Ext.override(atraxo.ad.i18n.ds.ReportRt_Ds, {
	contextPath__lbl: "Context path",
	queryBuilderClass__lbl: "Query builder class",
	reportServerId__lbl: "Report server (ID)",
	reportServer__lbl: "Report server",
	serverUrl__lbl: "Url"
});
