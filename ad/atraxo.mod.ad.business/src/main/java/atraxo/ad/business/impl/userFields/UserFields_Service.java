/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.userFields;

import atraxo.ad.business.api.userFields.IUserFieldsService;
import atraxo.ad.domain.impl.businessArea.BusinessArea;
import atraxo.ad.domain.impl.userFields.UserFields;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link UserFields} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class UserFields_Service extends AbstractEntityService<UserFields>
		implements
			IUserFieldsService {

	/**
	 * Public constructor for UserFields_Service
	 */
	public UserFields_Service() {
		super();
	}

	/**
	 * Public constructor for UserFields_Service
	 * 
	 * @param em
	 */
	public UserFields_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<UserFields> getEntityClass() {
		return UserFields.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return UserFields
	 */
	public UserFields findByName(String name, BusinessArea businessArea) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(UserFields.NQ_FIND_BY_NAME,
							UserFields.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name)
					.setParameter("businessArea", businessArea)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"UserFields", "name, businessArea"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"UserFields", "name, businessArea"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return UserFields
	 */
	public UserFields findByName(String name, Long businessAreaId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(UserFields.NQ_FIND_BY_NAME_PRIMITIVE,
							UserFields.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name)
					.setParameter("businessAreaId", businessAreaId)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"UserFields", "name, businessAreaId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"UserFields", "name, businessAreaId"), nure);
		}
	}

	/**
	 * Find by reference: businessArea
	 *
	 * @param businessArea
	 * @return List<UserFields>
	 */
	public List<UserFields> findByBusinessArea(BusinessArea businessArea) {
		return this.findByBusinessAreaId(businessArea.getId());
	}
	/**
	 * Find by ID of reference: businessArea.id
	 *
	 * @param businessAreaId
	 * @return List<UserFields>
	 */
	public List<UserFields> findByBusinessAreaId(String businessAreaId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from UserFields e where e.clientId = :clientId and e.businessArea.id = :businessAreaId",
						UserFields.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("businessAreaId", businessAreaId).getResultList();
	}
}
