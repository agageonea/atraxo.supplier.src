/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IQuartzLockService;
import atraxo.ad.domain.impl.scheduler.QuartzLock;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link QuartzLock} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class QuartzLock_Service extends AbstractEntityService<QuartzLock>
		implements
			IQuartzLockService {

	/**
	 * Public constructor for QuartzLock_Service
	 */
	public QuartzLock_Service() {
		super();
	}

	/**
	 * Public constructor for QuartzLock_Service
	 * 
	 * @param em
	 */
	public QuartzLock_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<QuartzLock> getEntityClass() {
		return QuartzLock.class;
	}

}
