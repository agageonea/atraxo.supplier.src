/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.ext.externalReport.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathExpressionException;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import atraxo.ad.business.api.attachment.IAttachmentService;
import atraxo.ad.business.api.ext.report.IReportDownloaderService;
import atraxo.ad.business.api.externalReport.IExternalReportParameterService;
import atraxo.ad.business.api.externalReport.IExternalReportService;
import atraxo.ad.business.api.externalReport.IReportLogService;
import atraxo.ad.business.api.externalReport.ISourceUploadHistoryService;
import atraxo.ad.business.ext.exceptions.AdErrorCode;
import atraxo.ad.business.ext.exceptions.ReportCenterException;
import atraxo.ad.business.ext.externalReport.util.ReportParamsExtractor;
import atraxo.ad.business.ext.externalReport.xml.DialogReport;
import atraxo.ad.business.ext.externalReport.xml.Parameter;
import atraxo.ad.business.ext.externalReport.xml.ReportBuilder;
import atraxo.ad.domain.impl.ad.ReportFormat;
import atraxo.ad.domain.impl.ad.ReportParamType;
import atraxo.ad.domain.impl.ad.ReportStatus;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.ExternalReportParameter;
import atraxo.ad.domain.impl.externalReport.ReportLog;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IExtReportService;
import seava.j4e.api.service.report.IReportService;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link ExternalReport} domain entity.
 *
 * @author apetho
 */
public class ExternalReport_Service extends atraxo.ad.business.impl.externalReport.ExternalReport_Service
		implements IExternalReportService, IExtReportService {

	private static final String REPORT_GENERATION_ERROR = "Report generation error";

	private static final String GET = "get";

	private static final Logger LOGGER = LoggerFactory.getLogger(ExternalReport_Service.class);

	private static final String ATTACHMENT_IDS = "attachmentIds";
	private static final String CDATA_ELEMENT = "url";

	@Autowired
	private IAttachmentService attachmentService;
	@Autowired
	private IReportLogService reportLogService;
	@Autowired
	private IExternalReportParameterService externalReportParameterService;
	@Autowired
	private ISourceUploadHistoryService sourceUploadHistoryService;
	@Autowired
	private IReportService reportService;
	@Autowired
	private IReportDownloaderService reportDownloaderService;

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		List<Object> params = new ArrayList<>();
		List<Object> sourceUploadHistory = new ArrayList<>();
		List<Object> reportLogs = new ArrayList<>();
		List<Object> attachmentIds = new ArrayList<>();
		for (Object reportId : ids) {
			ExternalReport er = this.findById(reportId);
			params.addAll(this.collectIds(new ArrayList<>(er.getParams())));
			sourceUploadHistory.addAll(this.collectIds(new ArrayList<>(er.getSourceUploadHistories())));
			reportLogs.addAll(this.collectIds(new ArrayList<>(er.getReportLogs())));
		}
		context.put(ATTACHMENT_IDS, attachmentIds);
		this.externalReportParameterService.deleteByIds(params);
		this.sourceUploadHistoryService.deleteByIds(sourceUploadHistory);
		this.reportLogService.deleteByIds(reportLogs);
	}

	@Override
	protected void postDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.postDeleteByIds(ids, context);
		if (context.containsKey(ATTACHMENT_IDS)) {
			@SuppressWarnings("unchecked")
			List<Object> attachmentIds = (List<Object>) context.get(ATTACHMENT_IDS);
			if (!CollectionUtils.isEmpty(attachmentIds)) {
				this.attachmentService.deleteByIds(attachmentIds);
			}
		}
	}

	@Override
	@Transactional
	public void runReport(String externalReportId, String format, String fileName) throws BusinessException {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Starting report.");
		}
		ExternalReport externalReport = this.findById(externalReportId);
		this.runReport(format, externalReport, new HashMap<>(), fileName);
	}

	/**
	 * Run the report.
	 *
	 * @param format - {@link String} The file extension.
	 * @param externalReport - {@link ExternalReport} The used report.
	 * @param paramMap - The parameters to the report.
	 * @throws BusinessException
	 */
	private void runReport(String format, ExternalReport externalReport, Map<String, Object> paramMap, String fileName) throws BusinessException {
		try {
			if (StringUtils.isEmpty(externalReport.getReportCode())) {
				throw new BusinessException(AdErrorCode.REPORT_CODE_MUST_BE_PRESENT_IN_REPORT,
						AdErrorCode.REPORT_CODE_MUST_BE_PRESENT_IN_REPORT.getErrMsg());
			}
			String reportData = ReportBuilder.buildSpecialReport(format, externalReport, paramMap);
			Integer reportId = this.reportService.startReport(reportData);
			ReportLog reportLog = this.buildReportLog(format, externalReport, reportId);
			this.reportLogService.insert(reportLog);
			String userIdentificator = Session.user.get().getLoginName() == null ? Session.user.get().getCode() : Session.user.get().getLoginName();
			this.reportDownloaderService.startDownloader(reportId, externalReport, userIdentificator, Session.user.get().getClientCode(), fileName);
		} catch (UnsupportedEncodingException | JAXBException | ParserConfigurationException | TransformerFactoryConfigurationError
				| TransformerException e) {
			LOGGER.warn(REPORT_GENERATION_ERROR, e);
		}
	}

	/**
	 * Run the report.
	 *
	 * @param format - {@link String} The file extension.
	 * @param externalReport - {@link ExternalReport} The used report.
	 * @param paramMap - The parameters to the report.
	 * @throws BusinessException
	 */
	private void runReportSaveAttachemenet(String format, ExternalReport externalReport, Map<String, Object> paramMap, String entityName,
			String parentId, String fileName) throws BusinessException {
		try {
			if (StringUtils.isEmpty(externalReport.getReportCode())) {
				throw new BusinessException(AdErrorCode.REPORT_CODE_MUST_BE_PRESENT_IN_REPORT,
						AdErrorCode.REPORT_CODE_MUST_BE_PRESENT_IN_REPORT.getErrMsg());
			}
			String reportData = ReportBuilder.buildSpecialReport(format, externalReport, paramMap);
			Integer reportId = this.reportService.startReport(reportData);
			ReportLog reportLog = this.buildReportLog(format, externalReport, reportId);
			this.reportLogService.insert(reportLog);
			String userIdentificator = Session.user.get().getLoginName() == null ? Session.user.get().getCode() : Session.user.get().getLoginName();
			this.reportDownloaderService.downloadCreateAttach(reportId, externalReport, userIdentificator, Session.user.get().getClientCode(),
					entityName, parentId, format, fileName);
		} catch (UnsupportedEncodingException | JAXBException | ParserConfigurationException | TransformerFactoryConfigurationError
				| TransformerException e) {
			LOGGER.warn(REPORT_GENERATION_ERROR, e);
		}
	}

	/**
	 * Build a report log.
	 *
	 * @param format - {@link String} The file extension.
	 * @param externalReport - {@link ExternalReport} The used report.
	 * @param reportId - {@link Integer} - The generated report id and not the external report id.
	 * @return - {@link ReportLog} the generated reportLog.
	 */
	private ReportLog buildReportLog(String format, ExternalReport externalReport, Integer reportId) {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Build the report log.");
		}
		ReportLog reportLog = new ReportLog();
		reportLog.setActive(true);
		reportLog.setExternalReport(externalReport);
		reportLog.setFormat(ReportFormat.getByName(format));
		reportLog.setStatus(ReportStatus._IN_PROGRESS_);
		reportLog.setRunDate(new Date());
		reportLog.setReportId(reportId);
		reportLog.setName(externalReport.getName());
		return reportLog;
	}

	@Override
	public void generateSaleInvoiceReport(String invoiceId, String reportId, String format, String entityAlias, String fileName)
			throws ReportCenterException {
		try {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Generate sales invoice report.");
			}
			ExternalReport externalReport = this.findById(reportId);
			Map<String, Object> params = this.buildInvoiceReportParameters(externalReport, invoiceId);
			this.runReportSaveAttachemenet(format, externalReport, params, entityAlias, invoiceId, fileName);
		} catch (BusinessException e1) {
			LOGGER.warn(REPORT_GENERATION_ERROR, e1);
		}
	}

	// check from where you call creates a new transaction
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Attachment generateSaleInvoiceReportSync(String invoiceId, String reportId, String format, String entityAlias, String fileName)
			throws BusinessException {
		try {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Generate sales invoice report syncron.");
			}
			ExternalReport externalReport = this.findById(reportId);
			Map<String, Object> params = this.buildInvoiceReportParameters(externalReport, invoiceId);

			if (StringUtils.isEmpty(externalReport.getReportCode())) {
				throw new BusinessException(AdErrorCode.REPORT_CODE_MUST_BE_PRESENT_IN_REPORT,
						AdErrorCode.REPORT_CODE_MUST_BE_PRESENT_IN_REPORT.getErrMsg());
			}
			String reportData = ReportBuilder.buildSpecialReport(format, externalReport, params);
			Integer generatedReportId = this.reportService.startReport(reportData);
			ReportLog reportLog = this.buildReportLog(format, externalReport, generatedReportId);
			this.reportLogService.insert(reportLog);
			String userIdentificator = Session.user.get().getLoginName() == null ? Session.user.get().getCode() : Session.user.get().getLoginName();
			return this.reportDownloaderService.downloadCreateAttachSync(generatedReportId, externalReport, userIdentificator,
					Session.user.get().getClientCode(), entityAlias, invoiceId, format, fileName);

		} catch (BusinessException | UnsupportedEncodingException | JAXBException | ParserConfigurationException
				| TransformerFactoryConfigurationError | TransformerException e1) {
			LOGGER.warn(REPORT_GENERATION_ERROR, e1);
		}
		return null;
	}

	private Map<String, Object> buildInvoiceReportParameters(ExternalReport externalReport, String invoiceId) {
		Map<String, Object> retList = new HashMap<>();
		for (ExternalReportParameter erp : externalReport.getParams()) {
			if (!StringUtils.isEmpty(erp.getDsFieldName()) && "id".equalsIgnoreCase(erp.getDsFieldName())) {
				retList.put(erp.getDsFieldName(), invoiceId);
			}
		}
		return retList;
	}

	public byte[] getBytesFromInputStream(InputStream is) throws IOException {
		try (ByteArrayOutputStream os = new ByteArrayOutputStream();) {
			byte[] buffer = new byte[0xFFFF];
			for (int len; (len = is.read(buffer)) != -1;) {
				os.write(buffer, 0, len);
			}
			os.flush();
			return os.toByteArray();
		}
	}

	@SuppressWarnings("resource")
	public String generateReport(String reportCenterUrl, DialogReport dr) throws ReportCenterException {
		try {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Start dialog report.");
			}
			String boddy = this.buildXml(dr);
			List<NameValuePair> httpParams = new ArrayList<>();
			httpParams.add(new BasicNameValuePair("data", boddy));
			HttpPost post = new HttpPost(reportCenterUrl);
			post.setEntity(new UrlEncodedFormEntity(httpParams, "UTF-8"));
			HttpResponse reportResponse = this.getHttpClient().execute(post);
			InputStream is = reportResponse.getEntity().getContent();
			java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
			String response = s.hasNext() ? s.next() : "";
			return this.getId(response);
		} catch (IOException | JAXBException | ParserConfigurationException | TransformerFactoryConfigurationError | TransformerException e) {
			LOGGER.error("Could not generate invoice report !", e);
			throw new ReportCenterException(AdErrorCode.REPORT_GENERATOR_PROBLEM,
					String.format(AdErrorCode.REPORT_GENERATOR_PROBLEM.getErrMsg(), "Could not generate invoice report:" + e.getLocalizedMessage()));
		}
	}

	private String getId(String response) throws ReportCenterException {
		try {
			JSONObject json = new JSONObject(response);
			if (json.has("reports")) {
				JSONArray reportsJsonArray = json.getJSONArray("reports");
				if (reportsJsonArray != null && reportsJsonArray.length() > 0) {
					JSONObject reportJson = reportsJsonArray.optJSONObject(0);
					if (reportJson != null && reportJson.has("success")) {
						if (reportJson.optBoolean("success")) {
							return reportJson.optString("execId");
						} else {
							throw new ReportCenterException(AdErrorCode.REPORT_GENERATOR_PROBLEM,
									String.format(AdErrorCode.REPORT_GENERATOR_PROBLEM.getErrMsg(), reportJson.optString("errMsg")));
						}
					}
				}
			}
		} catch (JSONException e) {
			LOGGER.error("Invalid JSON response from report center.", e);
			throw new ReportCenterException(AdErrorCode.REPORT_GENERATOR_PROBLEM,
					String.format(AdErrorCode.REPORT_GENERATOR_PROBLEM.getErrMsg(), "Invalid JSON response from report center."));
		}
		return null;
	}

	private String buildXml(DialogReport dr) throws JAXBException, ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, UnsupportedEncodingException {
		JAXBContext jaxbContext = JAXBContext.newInstance(DialogReport.class, Parameter.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		Document document = docBuilderFactory.newDocumentBuilder().newDocument();

		jaxbMarshaller.marshal(dr, document);

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer nullTransformer = transformerFactory.newTransformer();
		nullTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
		nullTransformer.setOutputProperty(OutputKeys.CDATA_SECTION_ELEMENTS, CDATA_ELEMENT);
		nullTransformer.transform(new DOMSource(document), new StreamResult(os));

		return os.toString(StandardCharsets.UTF_8.name());
	}

	public DialogReport buildReport(String format, ExternalReport externalReport) {
		String reportName = externalReport.getDesignName().split("\\.")[0];
		DialogReport dr = new DialogReport();
		dr.setCode(reportName);
		dr.setFormat(format);
		dr.setTitle(reportName);
		dr.setParamList(this.buildParamterList(externalReport));
		return dr;
	}

	private List<Parameter> buildParamterList(ExternalReport externalReport) {
		List<Parameter> retList = new ArrayList<>();
		for (ExternalReportParameter erp : externalReport.getParams()) {
			Parameter param = new Parameter();
			param.setName(erp.getName());
			param.setType(erp.getType().getName());
			param.setValue(erp.getParamValue() == null || erp.getParamValue().isEmpty() ? erp.getDefaultValue() : erp.getParamValue());
			retList.add(param);
		}
		return retList;
	}

	private HttpClient getHttpClient() {
		return HttpClientBuilder.create().useSystemProperties().build();
	}

	@Override
	public String buildFileName(String reportName, String ext) {
		char separator = '_';
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
		String date = formatter.format(new Date());
		int idx = reportName.lastIndexOf('.');
		String fileName = reportName;
		if (idx != -1) {
			fileName = reportName.substring(0, idx).replaceAll("\\s", "");
		}
		return new StringBuilder(fileName).append(separator).append(date).append(".").append(ext).toString();
	}

	@Override
	@Transactional
	public void runReport(Object ds, String reportName, String format, String fileName) throws BusinessException {
		ExternalReport er = this.findByName(reportName);
		Map<String, Object> paramMap = this.buildParamMap(ds, er);
		this.runReport(format, er, paramMap, fileName);
	}

	private Map<String, Object> buildParamMap(Object ds, ExternalReport er) {
		Collection<ExternalReportParameter> params = er.getParams();
		Map<String, Object> paramMap = new HashMap<>();
		for (ExternalReportParameter erp : params) {
			if (StringUtils.isEmpty(erp.getDsFieldName())) {
				continue;
			}
			try {
				String dsFieldName = erp.getDsFieldName();
				char c = dsFieldName.toUpperCase().charAt(0);
				dsFieldName = GET + c + dsFieldName.substring(1);
				if (this.isMethod(ds.getClass(), dsFieldName)) {
					Method method = ds.getClass().getMethod(dsFieldName);
					Object retVal = method.invoke(ds, new Object[0]);
					paramMap.put(erp.getDsFieldName(), retVal);
				}
			} catch (IllegalArgumentException | SecurityException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
				LOGGER.warn("Can not return the external report parameter!", e);
			}
		}
		return paramMap;
	}

	private boolean isMethod(@SuppressWarnings("rawtypes") Class clazz, String methodName) {
		for (Method m : clazz.getMethods()) {
			if (methodName.equalsIgnoreCase(m.getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param report
	 * @param lastAttachment
	 * @throws BusinessException
	 */
	@Override
	@Transactional
	public void parseAndSaveParameters(ExternalReport report, byte[] file, String fileName) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START parseAndSaveParameters()");
		}
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Parsing parameters");
		}
		try {
			InputStream is = new ByteArrayInputStream(file);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;

			dBuilder = dbFactory.newDocumentBuilder();

			Document doc = dBuilder.parse(is);

			doc.getDocumentElement().normalize();

			List<ExternalReportParameter> externalReportParamList = ReportParamsExtractor.getParams(report, doc);
			String reportCode = ReportParamsExtractor.getReportCode(doc);
			if (!StringUtils.isEmpty(reportCode)) {
				report.setReportCode(reportCode);
			} else {
				throw new BusinessException(AdErrorCode.REPORT_CODE_MUST_BE_PRESENT_IN_REPORT,
						AdErrorCode.REPORT_CODE_MUST_BE_PRESENT_IN_REPORT.getErrMsg());
			}
			// close the Stream
			is.close();
			report.getParams().clear();
			report.setDesignName(fileName);
			this.update(report);
			this.getEntityManager().flush();
			if (!externalReportParamList.isEmpty()) {
				ExternalReport er = this.findById(report.getId());
				for (ExternalReportParameter rep : externalReportParamList) {
					if (null == rep.getType()) {
						rep.setType(ReportParamType._EMPTY_);
					}
					rep.setExternalReport(er);
					er.addToParams(rep);
				}
				this.update(er);
			}
		} catch (ParserConfigurationException | XPathExpressionException | IOException | SAXException e) {
			LOGGER.warn("Parse parameters problem.", e);
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END parseAndSaveParameters()");
		}
	}

	@Override
	public void onReady(Integer generatedReportId, ReportStatus status) throws BusinessException {

	}

	/**
	 * @param externalReportId
	 * @param format
	 * @param paramMap
	 * @throws BusinessException
	 */
	@Override
	@Transactional
	public void runExternalReport(String externalReportId, String format, Map<String, Object> paramMap, String fileName) throws BusinessException {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Run external report report.");
		}
		ExternalReport er = this.findById(externalReportId);
		this.runReport(format, er, paramMap, fileName);
	}

	/**
	 * @param externalReportId
	 * @throws BusinessException
	 */
	@Override
	public boolean isExternalReportExists(String dsName) throws BusinessException {
		String hql = "select e from " + ExternalReport.class.getSimpleName()
				+ " e where e.clientId=:clientId and e.reportCode != '' and e.businessArea.dsName =:dsName";
		List<ExternalReport> reports = this.getEntityManager().createQuery(hql, ExternalReport.class)
				.setParameter("clientId", Session.user.get().getClientId()).setParameter("dsName", dsName).getResultList();
		return !CollectionUtils.isEmpty(reports);
	}

}
