/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.system;

import atraxo.ad.business.api.system.IDateFormatMaskService;
import atraxo.ad.domain.impl.system.DateFormat;
import atraxo.ad.domain.impl.system.DateFormatMask;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link DateFormatMask} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class DateFormatMask_Service
		extends
			AbstractEntityService<DateFormatMask>
		implements
			IDateFormatMaskService {

	/**
	 * Public constructor for DateFormatMask_Service
	 */
	public DateFormatMask_Service() {
		super();
	}

	/**
	 * Public constructor for DateFormatMask_Service
	 * 
	 * @param em
	 */
	public DateFormatMask_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<DateFormatMask> getEntityClass() {
		return DateFormatMask.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return DateFormatMask
	 */
	public DateFormatMask findByName(DateFormat dateFormat, String mask) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DateFormatMask.NQ_FIND_BY_NAME,
							DateFormatMask.class)
					.setParameter("dateFormat", dateFormat)
					.setParameter("mask", mask).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DateFormatMask", "dateFormat, mask"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DateFormatMask", "dateFormat, mask"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return DateFormatMask
	 */
	public DateFormatMask findByName(Long dateFormatId, String mask) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DateFormatMask.NQ_FIND_BY_NAME_PRIMITIVE,
							DateFormatMask.class)
					.setParameter("dateFormatId", dateFormatId)
					.setParameter("mask", mask).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DateFormatMask", "dateFormatId, mask"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DateFormatMask", "dateFormatId, mask"), nure);
		}
	}

	/**
	 * Find by reference: dateFormat
	 *
	 * @param dateFormat
	 * @return List<DateFormatMask>
	 */
	public List<DateFormatMask> findByDateFormat(DateFormat dateFormat) {
		return this.findByDateFormatId(dateFormat.getId());
	}
	/**
	 * Find by ID of reference: dateFormat.id
	 *
	 * @param dateFormatId
	 * @return List<DateFormatMask>
	 */
	public List<DateFormatMask> findByDateFormatId(String dateFormatId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DateFormatMask e where  e.dateFormat.id = :dateFormatId",
						DateFormatMask.class)
				.setParameter("dateFormatId", dateFormatId).getResultList();
	}
}
