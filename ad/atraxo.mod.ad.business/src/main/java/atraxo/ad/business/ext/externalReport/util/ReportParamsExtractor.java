package atraxo.ad.business.ext.externalReport.util;

import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import atraxo.ad.domain.impl.ad.ReportParamType;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.ExternalReportParameter;

public class ReportParamsExtractor {

	private static final String NAME = "name";

	private static final String PROPERTY = "property";

	public static List<ExternalReportParameter> getParams(ExternalReport report, Document doc) throws XPathExpressionException {
		List<ExternalReportParameter> externalReportParamList = new ArrayList<>();
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodes = (NodeList) xPath.evaluate("/report/parameters/scalar-parameter", doc.getDocumentElement(), XPathConstants.NODESET);
		for (int i = 0; i < nodes.getLength(); ++i) {
			Element scalarParamEl = (Element) nodes.item(i);

			ExternalReportParameter repParam = new ExternalReportParameter();
			repParam.setExternalReport(report);
			externalReportParamList.add(repParam);
			repParam.setMandatory(Boolean.FALSE);
			repParam.setName(scalarParamEl.getAttribute(NAME));

			// set mandatory and data type properties
			NodeList propertyNodeList = scalarParamEl.getElementsByTagName(PROPERTY);
			for (int j = 0; j < propertyNodeList.getLength(); j++) {
				Element propertyEl = (Element) propertyNodeList.item(j);
				String nameAttribute = propertyEl.getAttribute(NAME);
				if (!StringUtils.isEmpty(nameAttribute.isEmpty())) {
					if ("dataType".equals(nameAttribute)) {
						String dataTypeValue = propertyEl.getTextContent();
						if (dataTypeValue != null) {
							if ("integer".equals(dataTypeValue)) {
								repParam.setType(ReportParamType._INTEGER_);
							} else if ("boolean".equals(dataTypeValue)) {
								repParam.setType(ReportParamType._BOOLEAN_);
							} else if ("string".equals(dataTypeValue)) {
								repParam.setType(ReportParamType._STRING_);
							} else if ("dateTime".equals(dataTypeValue)) {
								repParam.setType(ReportParamType._DATE_);
							}
						}
					} else if ("isRequired".equals(nameAttribute)) {
						String isRequiredValue = propertyEl.getTextContent();
						repParam.setMandatory(Boolean.parseBoolean(isRequiredValue));
					}
				}
			}

			// set description
			NodeList textPropertyNodeList = scalarParamEl.getElementsByTagName("text-property");
			for (int j = 0; j < textPropertyNodeList.getLength(); j++) {
				Element textPropertyEl = (Element) textPropertyNodeList.item(j);
				if ("promptText".equals(textPropertyEl.getAttribute(NAME))) {
					repParam.setDescription(textPropertyEl.getTextContent());
				} else if ("helpText".equals(textPropertyEl.getAttribute(NAME)) && StringUtils.isEmpty(repParam.getDescription())) {
					repParam.setDescription(textPropertyEl.getTextContent());
				}
			}

			// set default value property
			if (repParam.getType() != null) {
				NodeList simplePropsNodeList = scalarParamEl.getElementsByTagName("simple-property-list");
				if (simplePropsNodeList != null && simplePropsNodeList.getLength() == 1) {
					Element firstSimplePropListEl = (Element) simplePropsNodeList.item(0);
					if (firstSimplePropListEl != null && "defaultValue".equals(firstSimplePropListEl.getAttribute(NAME))) {
						NodeList valueNodeList = firstSimplePropListEl.getElementsByTagName("value");
						for (int k = 0; k < valueNodeList.getLength(); k++) {
							Element valueEl = (Element) valueNodeList.item(k);
							if ("constant".equals(valueEl.getAttribute("type"))) {
								repParam.setDefaultValue(valueEl.getTextContent());
							}
						}
					}
				}
			}
		}
		return externalReportParamList;
	}

	public static String getReportCode(Document doc) throws XPathExpressionException {
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodes = (NodeList) xPath.evaluate("/report", doc.getDocumentElement(), XPathConstants.NODESET);
		for (int i = 0; i < nodes.getLength(); ++i) {
			Element scalarParamEl = (Element) nodes.item(i);
			NodeList properties = scalarParamEl.getElementsByTagName(PROPERTY);
			for (int j = 0; j < properties.getLength(); j++) {
				Element propertyEl = (Element) properties.item(j);
				String nameAttribute = propertyEl.getAttribute(NAME);
				if (!StringUtils.isEmpty(nameAttribute.isEmpty()) && "REPORT_CODE".equalsIgnoreCase(nameAttribute)) {
					return propertyEl.getTextContent();
				}
			}
		}
		return "";
	}
}
