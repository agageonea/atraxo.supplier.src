package atraxo.ad.business.ext.downloader.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;

import atraxo.ad.business.api.attachment.IAttachmentService;
import atraxo.ad.business.api.attachment.IAttachmentTypeService;
import atraxo.ad.business.api.ext.report.IReportDownloaderService;
import atraxo.ad.business.api.externalReport.IReportLogService;
import atraxo.ad.business.api.system.IClientService;
import atraxo.ad.domain.impl.ad.ReportStatus;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.system.Client;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.exceptions.InvalidConfiguration;
import seava.j4e.api.service.IClientInfoProvider;
import seava.j4e.api.service.report.IReportService;
import seava.j4e.api.session.IClient;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.IUserProfile;
import seava.j4e.api.session.IUserSettings;
import seava.j4e.api.session.IWorkspace;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.AbstractBusinessBaseService;
import seava.j4e.business.service.storage.StorageService;
import seava.j4e.commons.security.AppClient;
import seava.j4e.commons.security.AppUser;
import seava.j4e.commons.security.AppUserProfile;
import seava.j4e.commons.security.AppUserSettings;

public class ReportDownloaderService extends AbstractBusinessBaseService implements IReportDownloaderService {

	private static final String TIMED_OUT = "The report generation has timed out!";

	private static final int ITERATE_NUMBER = 50;
	private static final String SUCCESS = "SUCCESS";
	private static final String REPORT_DOWNLOADER_PROBLEM = "Report downloader problem.";
	private static final String RUNNING = "RUNNING";
	private static final String SALE_INVOICE_REPORT_ATTACHMENT_TYPE = "report";

	private static final Logger LOGGER = LoggerFactory.getLogger(ReportDownloaderService.class);

	@Autowired
	IReportService reportService;
	@Autowired
	private IReportLogService reportLogService;
	@Autowired
	private IClientService clientSrv;
	@Autowired
	private StorageService storageService;
	@Autowired
	private IAttachmentTypeService attachmentTypeService;
	@Autowired
	private IAttachmentService attachmentService;

	@Async
	@Override
	public void startDownloader(Integer id, ExternalReport er, String userCode, String clientCode, String fileName) throws BusinessException {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Starting downloader");
		}
		int counter = 0;
		this.createSessionUser(userCode, clientCode);
		try {
			String reportStatus = this.reportService.getReportStatus(id);
			boolean isRunning = RUNNING.equalsIgnoreCase(reportStatus);
			while (isRunning && counter++ < ITERATE_NUMBER) {
				LOGGER.info("Sleeping time is :" + (long) Math.pow(2, counter) + " ms");
				Thread.sleep((long) Math.pow(2, counter));
				LOGGER.info("Verify the report status. Verify count:" + counter + 1);
				reportStatus = this.reportService.getReportStatus(id);
				isRunning = RUNNING.equalsIgnoreCase(reportStatus);
			}
			if (!isRunning) {
				if (SUCCESS.equalsIgnoreCase(reportStatus)) {
					this.reportService.sendNotification(id, fileName);
					this.reportLogService.updateLog(er, id, ReportStatus._SUCCESS_, null);
				} else {
					this.reportService.sendNotification(id, fileName);
					this.reportLogService.updateLog(er, id, ReportStatus._FAILED_, ReportStatus._FAILED_.getName());
				}
			} else {
				this.reportService.sendNotification(id, fileName);
				this.reportLogService.updateLog(er, id, ReportStatus._FAILED_, TIMED_OUT);
			}
		} catch (InterruptedException e) {
			LOGGER.warn(REPORT_DOWNLOADER_PROBLEM, e);
			this.reportLogService.updateLog(er, id, ReportStatus._FAILED_, e.getLocalizedMessage());
			Thread.currentThread().interrupt();
		}
	}

	@Async
	@Override
	public void downloadCreateAttach(Integer id, ExternalReport er, String userCode, String clientCode, String entityName, String parentId,
			String fileExtension, String fileName) throws BusinessException {
		int counter = 0;
		this.createSessionUser(userCode, clientCode);
		try {
			String reportStatus = this.reportService.getReportStatus(id);
			boolean isRunning = RUNNING.equalsIgnoreCase(reportStatus);
			while (isRunning && counter++ < ITERATE_NUMBER) {
				LOGGER.info("Sleeping time is :" + (long) Math.pow(2, counter) + " ms");
				Thread.sleep((long) Math.pow(2, counter));
				LOGGER.info("Verify the report status. Verify count:" + counter + 1);
				reportStatus = this.reportService.getReportStatus(id);
				isRunning = RUNNING.equalsIgnoreCase(reportStatus);
			}
			if (!isRunning) {
				if (SUCCESS.equalsIgnoreCase(reportStatus)) {
					this.createAttachement(id, er, entityName, parentId, fileExtension, fileName);
					this.reportLogService.updateLog(er, id, ReportStatus._SUCCESS_, null);
				} else {
					this.reportLogService.updateLog(er, id, ReportStatus._FAILED_, ReportStatus._FAILED_.getName());
				}
			} else {

				this.reportLogService.updateLog(er, id, ReportStatus._FAILED_, TIMED_OUT);
			}
		} catch (InterruptedException e) {
			LOGGER.warn(REPORT_DOWNLOADER_PROBLEM, e);
			this.reportLogService.updateLog(er, id, ReportStatus._FAILED_, e.getLocalizedMessage());
			Thread.currentThread().interrupt();
		}
	}

	@Override
	public Attachment downloadCreateAttachSync(Integer id, ExternalReport er, String userCode, String clientCode, String entityName, String parentId,
			String fileExtension, String fileName) throws BusinessException {
		int counter = 0;
		try {
			String reportStatus = this.reportService.getReportStatus(id);
			boolean isRunning = RUNNING.equalsIgnoreCase(reportStatus);
			while (isRunning && counter++ < ITERATE_NUMBER) {
				LOGGER.info("Sleeping time is :" + 10l * 1000 + " ms");
				Thread.sleep(10l * 1000); // 10 second
				LOGGER.info("Verify the report status. Verify count:" + counter + 1);
				reportStatus = this.reportService.getReportStatus(id);
				isRunning = RUNNING.equalsIgnoreCase(reportStatus);
			}
			if (!isRunning) {
				if (SUCCESS.equalsIgnoreCase(reportStatus)) {
					this.reportLogService.updateLog(er, id, ReportStatus._SUCCESS_, null);
					return this.createAttachement(id, er, entityName, parentId, fileExtension, fileName);
				} else {
					this.reportLogService.updateLog(er, id, ReportStatus._FAILED_, ReportStatus._FAILED_.getName());
				}
			} else {
				this.reportLogService.updateLog(er, id, ReportStatus._FAILED_, TIMED_OUT);
			}
		} catch (InterruptedException e) {
			LOGGER.warn(REPORT_DOWNLOADER_PROBLEM, e);
			this.reportLogService.updateLog(er, id, ReportStatus._FAILED_, e.getLocalizedMessage());
			Thread.currentThread().interrupt();
		}
		return null;
	}

	private Attachment createAttachement(Integer id, ExternalReport er, String entityName, String parentId, String fileExtension, String fileName)
			throws BusinessException {
		AttachmentType attachmentType = this.attachmentTypeService.findByName(SALE_INVOICE_REPORT_ATTACHMENT_TYPE);
		byte[] generatedReport = this.reportService.downloadReport(id);
		Attachment doc = this.createAttachment(parentId, fileName, attachmentType, entityName, fileExtension, true);
		this.storageService.uploadFile(generatedReport, doc.getRefid(), fileName, Attachment.class.getSimpleName());
		return doc;
	}

	private Attachment createAttachment(String parentId, String name, AttachmentType type, String targetAlias, String extension, Boolean active)
			throws BusinessException {
		Attachment doc = new Attachment();
		doc.setTargetAlias(targetAlias);
		doc.setTargetRefid(parentId);
		doc.setFileName(name);
		doc.setName(name);
		doc.setType(type);
		doc.setActive(active);
		doc.setNotes("Sale invoice report document generated by the system");
		doc.setContentType(extension);
		this.attachmentService.insert(doc);
		return doc;
	}

	private void createSessionUser(String userCode, String clientCode) throws BusinessException {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Build a new session for the downloder thread.");
		}
		try {
			Client c = this.clientSrv.findByCode(clientCode);
			IClient client = new AppClient(c.getId(), clientCode, "");
			IUserSettings settings;
			settings = AppUserSettings.newInstance(this.getSettings());
			IUserProfile profile = new AppUserProfile(true, null, false, false, false);

			// create an incomplete user first
			IUser user = new AppUser(userCode, userCode, null, userCode, null, null, client, settings, profile, null, true);
			Session.user.set(user);

			// get the client workspace info
			IWorkspace ws = this.getApplicationContext().getBean(IClientInfoProvider.class).getClientWorkspace();
			user = new AppUser(userCode, userCode, userCode, userCode, null, null, client, settings, profile, ws, true);
			Session.user.set(user);
		} catch (InvalidConfiguration e) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}
	}
}