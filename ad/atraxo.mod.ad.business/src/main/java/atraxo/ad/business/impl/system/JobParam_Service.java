/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.system;

import atraxo.ad.business.api.system.IJobParamService;
import atraxo.ad.domain.impl.system.Job;
import atraxo.ad.domain.impl.system.JobParam;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link JobParam} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class JobParam_Service extends AbstractEntityService<JobParam>
		implements
			IJobParamService {

	/**
	 * Public constructor for JobParam_Service
	 */
	public JobParam_Service() {
		super();
	}

	/**
	 * Public constructor for JobParam_Service
	 * 
	 * @param em
	 */
	public JobParam_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<JobParam> getEntityClass() {
		return JobParam.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return JobParam
	 */
	public JobParam findByName(Job job, String name) {
		try {
			return this.getEntityManager()
					.createNamedQuery(JobParam.NQ_FIND_BY_NAME, JobParam.class)
					.setParameter("job", job).setParameter("name", name)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"JobParam", "job, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"JobParam", "job, name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return JobParam
	 */
	public JobParam findByName(Long jobId, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(JobParam.NQ_FIND_BY_NAME_PRIMITIVE,
							JobParam.class).setParameter("jobId", jobId)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"JobParam", "jobId, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"JobParam", "jobId, name"), nure);
		}
	}

	/**
	 * Find by reference: job
	 *
	 * @param job
	 * @return List<JobParam>
	 */
	public List<JobParam> findByJob(Job job) {
		return this.findByJobId(job.getId());
	}
	/**
	 * Find by ID of reference: job.id
	 *
	 * @param jobId
	 * @return List<JobParam>
	 */
	public List<JobParam> findByJobId(String jobId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from JobParam e where  e.job.id = :jobId",
						JobParam.class).setParameter("jobId", jobId)
				.getResultList();
	}
}
