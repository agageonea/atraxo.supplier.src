/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.security;

import atraxo.ad.domain.impl.security.Role;
import atraxo.ad.domain.impl.security.User;
import atraxo.ad.domain.impl.security.UserGroup;
import atraxo.ad.domain.impl.system.DateFormat;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link User} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class User_Service extends AbstractEntityService<User> {

	/**
	 * Public constructor for User_Service
	 */
	public User_Service() {
		super();
	}

	/**
	 * Public constructor for User_Service
	 * 
	 * @param em
	 */
	public User_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<User> getEntityClass() {
		return User.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return User
	 */
	public User findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(User.NQ_FIND_BY_CODE, User.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"User", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"User", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return User
	 */
	public User findByLogin(String loginName) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(User.NQ_FIND_BY_LOGIN, User.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("loginName", loginName).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"User", "loginName"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"User", "loginName"), nure);
		}
	}

	/**
	 * Find by reference: dateFormat
	 *
	 * @param dateFormat
	 * @return List<User>
	 */
	public List<User> findByDateFormat(DateFormat dateFormat) {
		return this.findByDateFormatId(dateFormat.getId());
	}
	/**
	 * Find by ID of reference: dateFormat.id
	 *
	 * @param dateFormatId
	 * @return List<User>
	 */
	public List<User> findByDateFormatId(String dateFormatId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from User e where e.clientId = :clientId and e.dateFormat.id = :dateFormatId",
						User.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("dateFormatId", dateFormatId).getResultList();
	}
	/**
	 * Find by reference: roles
	 *
	 * @param roles
	 * @return List<User>
	 */
	public List<User> findByRoles(Role roles) {
		return this.findByRolesId(roles.getId());
	}
	/**
	 * Find by ID of reference: roles.id
	 *
	 * @param rolesId
	 * @return List<User>
	 */
	public List<User> findByRolesId(String rolesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from User e, IN (e.roles) c where e.clientId = :clientId and c.id = :rolesId",
						User.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("rolesId", rolesId).getResultList();
	}
	/**
	 * Find by reference: groups
	 *
	 * @param groups
	 * @return List<User>
	 */
	public List<User> findByGroups(UserGroup groups) {
		return this.findByGroupsId(groups.getId());
	}
	/**
	 * Find by ID of reference: groups.id
	 *
	 * @param groupsId
	 * @return List<User>
	 */
	public List<User> findByGroupsId(String groupsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from User e, IN (e.groups) c where e.clientId = :clientId and c.id = :groupsId",
						User.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("groupsId", groupsId).getResultList();
	}
}
