/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.report;

import atraxo.ad.business.api.report.IDsReportParamService;
import atraxo.ad.domain.impl.report.DsReport;
import atraxo.ad.domain.impl.report.DsReportParam;
import atraxo.ad.domain.impl.report.ReportParam;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link DsReportParam} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class DsReportParam_Service extends AbstractEntityService<DsReportParam>
		implements
			IDsReportParamService {

	/**
	 * Public constructor for DsReportParam_Service
	 */
	public DsReportParam_Service() {
		super();
	}

	/**
	 * Public constructor for DsReportParam_Service
	 * 
	 * @param em
	 */
	public DsReportParam_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<DsReportParam> getEntityClass() {
		return DsReportParam.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return DsReportParam
	 */
	public DsReportParam findByRepParam_ds(DsReport dsReport,
			ReportParam reportParam) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DsReportParam.NQ_FIND_BY_REPPARAM_DS,
							DsReportParam.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("dsReport", dsReport)
					.setParameter("reportParam", reportParam).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DsReportParam", "dsReport, reportParam"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DsReportParam", "dsReport, reportParam"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return DsReportParam
	 */
	public DsReportParam findByRepParam_ds(Long dsReportId, Long reportParamId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							DsReportParam.NQ_FIND_BY_REPPARAM_DS_PRIMITIVE,
							DsReportParam.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("dsReportId", dsReportId)
					.setParameter("reportParamId", reportParamId)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DsReportParam", "dsReportId, reportParamId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DsReportParam", "dsReportId, reportParamId"), nure);
		}
	}

	/**
	 * Find by reference: dsReport
	 *
	 * @param dsReport
	 * @return List<DsReportParam>
	 */
	public List<DsReportParam> findByDsReport(DsReport dsReport) {
		return this.findByDsReportId(dsReport.getId());
	}
	/**
	 * Find by ID of reference: dsReport.id
	 *
	 * @param dsReportId
	 * @return List<DsReportParam>
	 */
	public List<DsReportParam> findByDsReportId(String dsReportId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DsReportParam e where e.clientId = :clientId and e.dsReport.id = :dsReportId",
						DsReportParam.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("dsReportId", dsReportId).getResultList();
	}
	/**
	 * Find by reference: reportParam
	 *
	 * @param reportParam
	 * @return List<DsReportParam>
	 */
	public List<DsReportParam> findByReportParam(ReportParam reportParam) {
		return this.findByReportParamId(reportParam.getId());
	}
	/**
	 * Find by ID of reference: reportParam.id
	 *
	 * @param reportParamId
	 * @return List<DsReportParam>
	 */
	public List<DsReportParam> findByReportParamId(String reportParamId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DsReportParam e where e.clientId = :clientId and e.reportParam.id = :reportParamId",
						DsReportParam.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("reportParamId", reportParamId).getResultList();
	}
}
