/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.ext.scheduler.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import atraxo.ad.business.api.scheduler.IJobExecutionService;
import atraxo.ad.domain.impl.scheduler.JobExecution;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.domain.batchjob.JobParameter;

/**
 * Business extensions specific for {@link JobExecution} domain entity.
 */
public class JobExecution_Service extends atraxo.ad.business.impl.scheduler.JobExecution_Service implements IJobExecutionService {

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public void deleteJobExecution(List ids) throws BusinessException {
		String idsList = ids.toString().replace("[", "(").replace("]", ")");

		List<Long> stepExecutionIds = this.getEntityManager()
				.createNativeQuery("select e.step_execution_id from BATCH_STEP_EXECUTION e where e.job_execution_id in " + idsList).getResultList();
		String stepExecutionIdsList = stepExecutionIds.toString().replace("[", "(").replace("]", ")");
		this.getEntityManager().createNativeQuery("delete from BATCH_STEP_EXECUTION_CONTEXT  where step_execution_id in " + stepExecutionIdsList)
				.executeUpdate();
		this.getEntityManager().createNativeQuery("delete from BATCH_STEP_EXECUTION  where step_execution_id in " + stepExecutionIdsList)
				.executeUpdate();

		this.getEntityManager().createNativeQuery("delete from BATCH_JOB_EXECUTION_CONTEXT where job_execution_id in " + idsList).executeUpdate();
		this.getEntityManager().createNativeQuery("delete from BATCH_JOB_EXECUTION_PARAMS  where job_execution_id in " + idsList).executeUpdate();

		List<Long> jobInstanceIds = this.getEntityManager()
				.createNativeQuery("select e.job_instance_id from BATCH_JOB_EXECUTION e where e.job_execution_id in " + idsList).getResultList();
		String jobInstanceIdsList = jobInstanceIds.toString().replace("[", "(").replace("]", ")");
		this.getEntityManager().createNativeQuery("delete from BATCH_JOB_EXECUTION where job_execution_id in " + idsList).executeUpdate();
		this.getEntityManager().createNativeQuery("delete from BATCH_JOB_INSTANCE where job_instance_id in " + jobInstanceIdsList).executeUpdate();
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public void emptyHistory(Integer id) throws BusinessException {
		List<Long> jobExecutionIds = this.getEntityManager()
				.createNativeQuery("select e.job_execution_id from BATCH_JOB_EXECUTION_PARAMS e where e.key_name = '" + JobParameter.JOBCHAIN.name()
						+ "' and e.long_val = " + id)
				.getResultList();
		this.deleteJobExecution(jobExecutionIds);
	}

	@Override
	public List findByJobExecutionParams(Integer id) throws BusinessException {
		return this.getEntityManager()
				.createNativeQuery("select job_execution_id from BATCH_JOB_EXECUTION_PARAMS where key_name = 'JOBCHAIN' and long_val = " + id)
				.getResultList();
	}
}