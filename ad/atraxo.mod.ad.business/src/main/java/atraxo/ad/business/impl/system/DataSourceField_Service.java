/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.system;

import atraxo.ad.business.api.system.IDataSourceFieldService;
import atraxo.ad.domain.impl.system.DataSource;
import atraxo.ad.domain.impl.system.DataSourceField;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link DataSourceField} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class DataSourceField_Service
		extends
			AbstractEntityService<DataSourceField>
		implements
			IDataSourceFieldService {

	/**
	 * Public constructor for DataSourceField_Service
	 */
	public DataSourceField_Service() {
		super();
	}

	/**
	 * Public constructor for DataSourceField_Service
	 * 
	 * @param em
	 */
	public DataSourceField_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<DataSourceField> getEntityClass() {
		return DataSourceField.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return DataSourceField
	 */
	public DataSourceField findByName(DataSource dataSource, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DataSourceField.NQ_FIND_BY_NAME,
							DataSourceField.class)
					.setParameter("dataSource", dataSource)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DataSourceField", "dataSource, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DataSourceField", "dataSource, name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return DataSourceField
	 */
	public DataSourceField findByName(Long dataSourceId, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							DataSourceField.NQ_FIND_BY_NAME_PRIMITIVE,
							DataSourceField.class)
					.setParameter("dataSourceId", dataSourceId)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DataSourceField", "dataSourceId, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DataSourceField", "dataSourceId, name"), nure);
		}
	}

	/**
	 * Find by reference: dataSource
	 *
	 * @param dataSource
	 * @return List<DataSourceField>
	 */
	public List<DataSourceField> findByDataSource(DataSource dataSource) {
		return this.findByDataSourceId(dataSource.getId());
	}
	/**
	 * Find by ID of reference: dataSource.id
	 *
	 * @param dataSourceId
	 * @return List<DataSourceField>
	 */
	public List<DataSourceField> findByDataSourceId(String dataSourceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DataSourceField e where  e.dataSource.id = :dataSourceId",
						DataSourceField.class)
				.setParameter("dataSourceId", dataSourceId).getResultList();
	}
}
