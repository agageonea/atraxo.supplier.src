/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.ext.system.service;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import atraxo.ad.business.api.system.IClientService;
import atraxo.ad.business.ext.system.delegate.Client_Bd;
import atraxo.ad.domain.impl.security.User;
import atraxo.ad.domain.impl.system.Client;
import seava.j4e.api.action.impex.IImportDataPackage;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.exceptions.J4eErrorCode;

/**
 * Business extensions specific for {@link Client} domain entity.
 */
public class Client_Service extends atraxo.ad.business.impl.system.Client_Service implements IClientService {

	private static final Logger LOGGER = LoggerFactory.getLogger(Client_Service.class);

	@Override
	@Transactional
	public void doInsertWithUserAccount(Client client, String userCode, String userName, String loginName, String password) throws BusinessException {
		this.preSave(client);
		Client_Bd delegate = this.getBusinessDelegate(Client_Bd.class);
		delegate.createClientWithAdminUser(client, userCode, userName, loginName, password);
	}

	@Override
	@Transactional
	public User doAddUserAccount(Client client, String userCode, String userName, String loginName) throws BusinessException {
		Client_Bd delegate = this.getBusinessDelegate(Client_Bd.class);
		return delegate.addInitialUser(client, userCode, userName, loginName);
	}

	@Override
	@Transactional
	public void doInsertWithUserAccountAndSetup(Client client, String userCode, String userName, String loginName, String password,
			IImportDataPackage dataPackage) throws BusinessException {
		this.preSave(client);
		Client_Bd delegate = this.getBusinessDelegate(Client_Bd.class);
		delegate.createClientWithAdminUserAndSetup(client, userCode, userName, loginName, password, dataPackage);
	}

	@Override
	public void doUpgrade(Client client, IImportDataPackage dataPackage) throws BusinessException {
		Client_Bd delegate = this.getBusinessDelegate(Client_Bd.class);
		delegate.upgradeOldClient(client, dataPackage);
	}

	@Override
	protected void preInsert(Client e) throws BusinessException {
		this.preSave(e);
	}

	@Override
	protected void preUpdate(Client e) throws BusinessException {
		this.preSave(e);
	}

	protected void preSave(Client e) throws BusinessException {

		this.validatePath(e.getWorkspacePath(), "Workspace");
		this.validatePath(e.getImportPath(), "Import");
		this.validatePath(e.getExportPath(), "Export");
		this.validatePath(e.getTempPath(), "Temporary");
		this.validateCode(e);
	}

	private void validateCode(Client e) throws BusinessException {
		if (e.getCode() == null || e.getCode().isEmpty()) {
			throw new BusinessException(ErrorCode.G_NULL_FIELD_CODE, "Client code cannot be empty.");
		}
		try {
			Client oldClient = this.findByCode(e.getCode());
			if (e.getId() == null || !e.getId().equals(oldClient.getId())) {
				throw new BusinessException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
						"Client with code=\"" + e.getCode() + "\" already exists in the system.");
			}
		} catch (ApplicationException apEx) {
			if (!J4eErrorCode.DB_NO_RESULT.equals(apEx.getErrorCode())) {
				throw apEx;
			} else {
				// do nothing, client with the same code isn't exists in the system therefore it can be inserted.
				LOGGER.warn(
						"Warning:could not find client! Will do nothing, client with the same code doesn't exist in the system therefore it can be inserted.",
						apEx);
			}
		}
	}

	protected void validatePath(String path, String name) throws BusinessException {
		if (path == null) {
			throw new BusinessException(ErrorCode.G_FILE_INVALID_LOCATION, name + " path cannot be empty.");
		}
		File f = new File(path);

		if (!f.exists()) {
			if (!f.mkdirs()) {
				throw new BusinessException(ErrorCode.G_FILE_NOT_CREATABLE, name + " path `" + path + "` structure cannot be created.");
			}
		} else {
			if (!f.canRead()) {
				throw new BusinessException(ErrorCode.G_FILE_NOT_READABLE, name + " path `" + path + "` is not read enabled.");
			}
			if (!f.canWrite()) {
				throw new BusinessException(ErrorCode.G_FILE_NOT_WRITABLE, name + " path `" + path + "` is not write enabled.");
			}
		}
	}

}
