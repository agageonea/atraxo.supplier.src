package atraxo.ad.business.ext.bpm;

import org.activiti.engine.impl.interceptor.Session;
import org.activiti.engine.impl.interceptor.SessionFactory;
import org.activiti.engine.impl.persistence.entity.UserEntityManager;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.ad.business.api.security.IUserService;

public class ActivitiUserManagerFactory implements SessionFactory {

	@Autowired
	private IUserService userService;

	@Override
	public Class<?> getSessionType() {
		return UserEntityManager.class;
	}

	@Override
	public Session openSession() {
		return new ActivitiUserManager(this.userService);
	}

}
