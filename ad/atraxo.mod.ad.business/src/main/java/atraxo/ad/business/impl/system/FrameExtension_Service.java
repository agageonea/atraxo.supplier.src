/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.system;

import atraxo.ad.business.api.system.IFrameExtensionService;
import atraxo.ad.domain.impl.system.FrameExtension;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FrameExtension} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FrameExtension_Service
		extends
			AbstractEntityService<FrameExtension>
		implements
			IFrameExtensionService {

	/**
	 * Public constructor for FrameExtension_Service
	 */
	public FrameExtension_Service() {
		super();
	}

	/**
	 * Public constructor for FrameExtension_Service
	 * 
	 * @param em
	 */
	public FrameExtension_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FrameExtension> getEntityClass() {
		return FrameExtension.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FrameExtension
	 */
	public FrameExtension findByName(String frame, String fileLocation) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FrameExtension.NQ_FIND_BY_NAME,
							FrameExtension.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("frame", frame)
					.setParameter("fileLocation", fileLocation)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FrameExtension", "frame, fileLocation"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FrameExtension", "frame, fileLocation"), nure);
		}
	}

}
