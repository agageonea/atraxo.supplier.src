/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IJobExecutionParamsService;
import atraxo.ad.domain.impl.scheduler.JobExecution;
import atraxo.ad.domain.impl.scheduler.JobExecutionParams;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link JobExecutionParams} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class JobExecutionParams_Service
		extends
			AbstractEntityService<JobExecutionParams>
		implements
			IJobExecutionParamsService {

	/**
	 * Public constructor for JobExecutionParams_Service
	 */
	public JobExecutionParams_Service() {
		super();
	}

	/**
	 * Public constructor for JobExecutionParams_Service
	 * 
	 * @param em
	 */
	public JobExecutionParams_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<JobExecutionParams> getEntityClass() {
		return JobExecutionParams.class;
	}

	/**
	 * Find by reference: jobExecution
	 *
	 * @param jobExecution
	 * @return List<JobExecutionParams>
	 */
	public List<JobExecutionParams> findByJobExecution(JobExecution jobExecution) {
		return this.findByJobExecutionId(jobExecution.getId());
	}
	/**
	 * Find by ID of reference: jobExecution.id
	 *
	 * @param jobExecutionId
	 * @return List<JobExecutionParams>
	 */
	public List<JobExecutionParams> findByJobExecutionId(Long jobExecutionId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from JobExecutionParams e where  e.jobExecution.id = :jobExecutionId",
						JobExecutionParams.class)
				.setParameter("jobExecutionId", jobExecutionId).getResultList();
	}
}
