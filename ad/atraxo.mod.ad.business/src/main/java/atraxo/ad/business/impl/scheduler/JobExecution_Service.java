/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.domain.impl.scheduler.JobExecution;
import atraxo.ad.domain.impl.scheduler.JobInstance;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link JobExecution} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class JobExecution_Service extends AbstractEntityService<JobExecution> {

	/**
	 * Public constructor for JobExecution_Service
	 */
	public JobExecution_Service() {
		super();
	}

	/**
	 * Public constructor for JobExecution_Service
	 * 
	 * @param em
	 */
	public JobExecution_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<JobExecution> getEntityClass() {
		return JobExecution.class;
	}

	/**
	 * Find by reference: jobInstance
	 *
	 * @param jobInstance
	 * @return List<JobExecution>
	 */
	public List<JobExecution> findByJobInstance(JobInstance jobInstance) {
		return this.findByJobInstanceId(jobInstance.getId());
	}
	/**
	 * Find by ID of reference: jobInstance.id
	 *
	 * @param jobInstanceId
	 * @return List<JobExecution>
	 */
	public List<JobExecution> findByJobInstanceId(Long jobInstanceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from JobExecution e where  e.jobInstance.id = :jobInstanceId",
						JobExecution.class)
				.setParameter("jobInstanceId", jobInstanceId).getResultList();
	}
}
