/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.security;

import atraxo.ad.business.api.security.IAccessControlService;
import atraxo.ad.domain.impl.security.AccessControl;
import atraxo.ad.domain.impl.security.AccessControlDs;
import atraxo.ad.domain.impl.security.Role;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link AccessControl} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class AccessControl_Service extends AbstractEntityService<AccessControl>
		implements
			IAccessControlService {

	/**
	 * Public constructor for AccessControl_Service
	 */
	public AccessControl_Service() {
		super();
	}

	/**
	 * Public constructor for AccessControl_Service
	 * 
	 * @param em
	 */
	public AccessControl_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<AccessControl> getEntityClass() {
		return AccessControl.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return AccessControl
	 */
	public AccessControl findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(AccessControl.NQ_FIND_BY_NAME,
							AccessControl.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"AccessControl", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"AccessControl", "name"), nure);
		}
	}

	/**
	 * Find by reference: dsRules
	 *
	 * @param dsRules
	 * @return List<AccessControl>
	 */
	public List<AccessControl> findByDsRules(AccessControlDs dsRules) {
		return this.findByDsRulesId(dsRules.getId());
	}
	/**
	 * Find by ID of reference: dsRules.id
	 *
	 * @param dsRulesId
	 * @return List<AccessControl>
	 */
	public List<AccessControl> findByDsRulesId(String dsRulesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from AccessControl e, IN (e.dsRules) c where e.clientId = :clientId and c.id = :dsRulesId",
						AccessControl.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("dsRulesId", dsRulesId).getResultList();
	}
	/**
	 * Find by reference: roles
	 *
	 * @param roles
	 * @return List<AccessControl>
	 */
	public List<AccessControl> findByRoles(Role roles) {
		return this.findByRolesId(roles.getId());
	}
	/**
	 * Find by ID of reference: roles.id
	 *
	 * @param rolesId
	 * @return List<AccessControl>
	 */
	public List<AccessControl> findByRolesId(String rolesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from AccessControl e, IN (e.roles) c where e.clientId = :clientId and c.id = :rolesId",
						AccessControl.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("rolesId", rolesId).getResultList();
	}
}
