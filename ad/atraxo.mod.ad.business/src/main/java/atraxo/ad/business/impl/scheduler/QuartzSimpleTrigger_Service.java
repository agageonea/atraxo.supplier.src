/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IQuartzSimpleTriggerService;
import atraxo.ad.domain.impl.scheduler.QuartzSimpleTrigger;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link QuartzSimpleTrigger} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class QuartzSimpleTrigger_Service
		extends
			AbstractEntityService<QuartzSimpleTrigger>
		implements
			IQuartzSimpleTriggerService {

	/**
	 * Public constructor for QuartzSimpleTrigger_Service
	 */
	public QuartzSimpleTrigger_Service() {
		super();
	}

	/**
	 * Public constructor for QuartzSimpleTrigger_Service
	 * 
	 * @param em
	 */
	public QuartzSimpleTrigger_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<QuartzSimpleTrigger> getEntityClass() {
		return QuartzSimpleTrigger.class;
	}

}
