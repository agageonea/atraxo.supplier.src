/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.ext.scheduler.service;

import java.util.Date;
import java.util.List;

import seava.j4e.api.exceptions.BusinessException;
import atraxo.ad.business.api.scheduler.IJobTimerService;
import atraxo.ad.business.ext.scheduler.delegate.JobScheduler_Bd;
import atraxo.ad.domain.impl.scheduler.JobTimer;

/**
 * Business extensions specific for {@link JobTimer} domain entity.
 */
public class JobTimer_Service extends atraxo.ad.business.impl.scheduler.JobTimer_Service implements IJobTimerService {

	@Override
	protected void preInsert(JobTimer e) throws BusinessException {
		if (e.getStartTime() == null) {
			e.setStartTime(new Date());
		}
	}

	@Override
	protected void preUpdate(JobTimer e) throws BusinessException {
		if (e.getStartTime() == null) {
			e.setStartTime(new Date());
		}
	}

	/**
	 * Register timers as Quartz triggers.
	 */
	@Override
	protected void postInsert(List<JobTimer> list) throws BusinessException {
		this.getBusinessDelegate(JobScheduler_Bd.class).createQuartzTrigger(list, false);
	}

	/**
	 * Update the associated Quartz triggers
	 */
	@Override
	protected void postUpdate(List<JobTimer> list) throws BusinessException {
		this.getBusinessDelegate(JobScheduler_Bd.class).createQuartzTrigger(list, true);
	}

}
