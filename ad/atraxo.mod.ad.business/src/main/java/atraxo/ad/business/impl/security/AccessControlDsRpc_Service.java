/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.security;

import atraxo.ad.business.api.security.IAccessControlDsRpcService;
import atraxo.ad.domain.impl.security.AccessControl;
import atraxo.ad.domain.impl.security.AccessControlDsRpc;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link AccessControlDsRpc} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class AccessControlDsRpc_Service
		extends
			AbstractEntityService<AccessControlDsRpc>
		implements
			IAccessControlDsRpcService {

	/**
	 * Public constructor for AccessControlDsRpc_Service
	 */
	public AccessControlDsRpc_Service() {
		super();
	}

	/**
	 * Public constructor for AccessControlDsRpc_Service
	 * 
	 * @param em
	 */
	public AccessControlDsRpc_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<AccessControlDsRpc> getEntityClass() {
		return AccessControlDsRpc.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return AccessControlDsRpc
	 */
	public AccessControlDsRpc findByUnique(AccessControl accessControl,
			String dsName, String serviceMethod) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(AccessControlDsRpc.NQ_FIND_BY_UNIQUE,
							AccessControlDsRpc.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("accessControl", accessControl)
					.setParameter("dsName", dsName)
					.setParameter("serviceMethod", serviceMethod)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"AccessControlDsRpc",
							"accessControl, dsName, serviceMethod"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"AccessControlDsRpc",
							"accessControl, dsName, serviceMethod"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return AccessControlDsRpc
	 */
	public AccessControlDsRpc findByUnique(Long accessControlId, String dsName,
			String serviceMethod) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							AccessControlDsRpc.NQ_FIND_BY_UNIQUE_PRIMITIVE,
							AccessControlDsRpc.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("accessControlId", accessControlId)
					.setParameter("dsName", dsName)
					.setParameter("serviceMethod", serviceMethod)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"AccessControlDsRpc",
							"accessControlId, dsName, serviceMethod"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"AccessControlDsRpc",
							"accessControlId, dsName, serviceMethod"), nure);
		}
	}

	/**
	 * Find by reference: accessControl
	 *
	 * @param accessControl
	 * @return List<AccessControlDsRpc>
	 */
	public List<AccessControlDsRpc> findByAccessControl(
			AccessControl accessControl) {
		return this.findByAccessControlId(accessControl.getId());
	}
	/**
	 * Find by ID of reference: accessControl.id
	 *
	 * @param accessControlId
	 * @return List<AccessControlDsRpc>
	 */
	public List<AccessControlDsRpc> findByAccessControlId(String accessControlId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from AccessControlDsRpc e where e.clientId = :clientId and e.accessControl.id = :accessControlId",
						AccessControlDsRpc.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("accessControlId", accessControlId)
				.getResultList();
	}
}
