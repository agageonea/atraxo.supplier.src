/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.attachment;

import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Attachment} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Attachment_Service extends AbstractEntityService<Attachment> {

	/**
	 * Public constructor for Attachment_Service
	 */
	public Attachment_Service() {
		super();
	}

	/**
	 * Public constructor for Attachment_Service
	 * 
	 * @param em
	 */
	public Attachment_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Attachment> getEntityClass() {
		return Attachment.class;
	}

	/**
	 * Find by reference: type
	 *
	 * @param type
	 * @return List<Attachment>
	 */
	public List<Attachment> findByType(AttachmentType type) {
		return this.findByTypeId(type.getId());
	}
	/**
	 * Find by ID of reference: type.id
	 *
	 * @param typeId
	 * @return List<Attachment>
	 */
	public List<Attachment> findByTypeId(String typeId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Attachment e where e.clientId = :clientId and e.type.id = :typeId",
						Attachment.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("typeId", typeId).getResultList();
	}
}
