/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.security;

import atraxo.ad.business.api.security.IMenuService;
import atraxo.ad.domain.impl.security.Menu;
import atraxo.ad.domain.impl.security.Role;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Menu} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Menu_Service extends AbstractEntityService<Menu>
		implements
			IMenuService {

	/**
	 * Public constructor for Menu_Service
	 */
	public Menu_Service() {
		super();
	}

	/**
	 * Public constructor for Menu_Service
	 * 
	 * @param em
	 */
	public Menu_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Menu> getEntityClass() {
		return Menu.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Menu
	 */
	public Menu findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Menu.NQ_FIND_BY_NAME, Menu.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Menu", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Menu", "name"), nure);
		}
	}

	/**
	 * Find by reference: roles
	 *
	 * @param roles
	 * @return List<Menu>
	 */
	public List<Menu> findByRoles(Role roles) {
		return this.findByRolesId(roles.getId());
	}
	/**
	 * Find by ID of reference: roles.id
	 *
	 * @param rolesId
	 * @return List<Menu>
	 */
	public List<Menu> findByRolesId(String rolesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Menu e, IN (e.roles) c where e.clientId = :clientId and c.id = :rolesId",
						Menu.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("rolesId", rolesId).getResultList();
	}
}
