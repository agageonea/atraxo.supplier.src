/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.system;

import atraxo.ad.business.api.system.IDateFormatService;
import atraxo.ad.domain.impl.system.DateFormat;
import atraxo.ad.domain.impl.system.DateFormatMask;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link DateFormat} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class DateFormat_Service extends AbstractEntityService<DateFormat>
		implements
			IDateFormatService {

	/**
	 * Public constructor for DateFormat_Service
	 */
	public DateFormat_Service() {
		super();
	}

	/**
	 * Public constructor for DateFormat_Service
	 * 
	 * @param em
	 */
	public DateFormat_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<DateFormat> getEntityClass() {
		return DateFormat.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return DateFormat
	 */
	public DateFormat findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DateFormat.NQ_FIND_BY_NAME,
							DateFormat.class).setParameter("name", name)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DateFormat", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DateFormat", "name"), nure);
		}
	}

	/**
	 * Find by reference: masks
	 *
	 * @param masks
	 * @return List<DateFormat>
	 */
	public List<DateFormat> findByMasks(DateFormatMask masks) {
		return this.findByMasksId(masks.getId());
	}
	/**
	 * Find by ID of reference: masks.id
	 *
	 * @param masksId
	 * @return List<DateFormat>
	 */
	public List<DateFormat> findByMasksId(String masksId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from DateFormat e, IN (e.masks) c where  c.id = :masksId",
						DateFormat.class).setParameter("masksId", masksId)
				.getResultList();
	}
}
