/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.ext.system.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import atraxo.ad.business.api.system.IParamService;
import atraxo.ad.domain.impl.system.Param;
import seava.j4e.api.descriptor.ISysParamDefinition;
import seava.j4e.api.descriptor.ISysParamDefinitions;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link Param} domain entity.
 */
public class Param_Service extends atraxo.ad.business.impl.system.Param_Service implements IParamService {

	@Override
	@Transactional
	public void doSynchronizeCatalog() throws BusinessException {

		List<ISysParamDefinitions> defs = this.getSettings().getParamDefinitions();
		List<Param> entities = new ArrayList<>();
		for (ISysParamDefinitions def : defs) {
			for (ISysParamDefinition d : def.getSysParamDefinitions()) {
				Param e = new Param();
				e.setActive(true);
				e.setCode(d.getName());
				e.setName(d.getTitle());
				e.setDefaultValue(d.getDefaultValue());
				e.setListOfValues(d.getListOfValues());
				e.setDescription(d.getDescription());
				entities.add(e);
			}
		}

		this.update("delete from " + Param.class.getSimpleName(), null);
		this.insert(entities);
	}

}
