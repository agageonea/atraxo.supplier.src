/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.attachment;

import atraxo.ad.domain.impl.attachment.AttachmentType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link AttachmentType} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class AttachmentType_Service
		extends
			AbstractEntityService<AttachmentType> {

	/**
	 * Public constructor for AttachmentType_Service
	 */
	public AttachmentType_Service() {
		super();
	}

	/**
	 * Public constructor for AttachmentType_Service
	 * 
	 * @param em
	 */
	public AttachmentType_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<AttachmentType> getEntityClass() {
		return AttachmentType.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return AttachmentType
	 */
	public AttachmentType findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(AttachmentType.NQ_FIND_BY_NAME,
							AttachmentType.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"AttachmentType", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"AttachmentType", "name"), nure);
		}
	}

}
