package atraxo.ad.business.ext.bpm;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.GroupQuery;
import org.activiti.engine.impl.persistence.entity.GroupEntityManager;

import atraxo.ad.business.api.security.IRoleService;
import atraxo.ad.business.api.security.IUserService;
import atraxo.ad.domain.impl.security.Role;

/**
 * @author vhojda
 */
public class ActivitiGroupManager extends GroupEntityManager {

	private IUserService userService;

	private IRoleService roleService;

	/**
	 * @param userService
	 * @param userGroupService
	 */
	public ActivitiGroupManager() {
		super();
	}

	/**
	 * @param userService
	 * @param userGroupService
	 */
	public ActivitiGroupManager(IUserService userService, IRoleService roleService) {
		super();
		this.userService = userService;
		this.roleService = roleService;
	}

	@Override
	public Group createNewGroup(String groupId) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void insertGroup(Group group) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void updateGroup(Group updatedGroup) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteGroup(String groupId) {
		throw new UnsupportedOperationException();
	}

	@Override
	public GroupQuery createNewGroupQuery() {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Group> findGroupsByUser(String username) {
		atraxo.ad.domain.impl.security.User soneUser = this.userService.findByLogin(username);
		List<Group> groups = new ArrayList<Group>();
		for (Role role : soneUser.getRoles()) {
			groups.add(ActivitiUserHelper.soneRoleToActivitiGroupEntity(role));
		}
		return groups;
	}

	public IUserService getUserService() {
		return this.userService;
	}

	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	public IRoleService getRoleService() {
		return this.roleService;
	}

	public void setRoleService(IRoleService roleService) {
		this.roleService = roleService;
	}

}
