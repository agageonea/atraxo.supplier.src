/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IQuartzCronTriggerService;
import atraxo.ad.domain.impl.scheduler.QuartzCronTrigger;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link QuartzCronTrigger} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class QuartzCronTrigger_Service
		extends
			AbstractEntityService<QuartzCronTrigger>
		implements
			IQuartzCronTriggerService {

	/**
	 * Public constructor for QuartzCronTrigger_Service
	 */
	public QuartzCronTrigger_Service() {
		super();
	}

	/**
	 * Public constructor for QuartzCronTrigger_Service
	 * 
	 * @param em
	 */
	public QuartzCronTrigger_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<QuartzCronTrigger> getEntityClass() {
		return QuartzCronTrigger.class;
	}

}
