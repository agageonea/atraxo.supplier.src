package atraxo.ad.business.ext.bpm;

import org.activiti.engine.impl.persistence.entity.GroupEntity;
import org.activiti.engine.impl.persistence.entity.UserEntity;

import atraxo.ad.domain.impl.security.Role;

/**
 * @author vhojda
 */
public class ActivitiUserHelper {

	/**
	 * @param usuario
	 * @return
	 */
	public static UserEntity soneUserToActivitiUserEntity(atraxo.ad.domain.impl.security.User usuario) {
		UserEntity activitiUser = new UserEntity();
		activitiUser.setId(usuario.getId().toString());
		activitiUser.setPassword(usuario.getPassword());
		activitiUser.setFirstName(usuario.getName());
		activitiUser.setLastName(usuario.getName());
		activitiUser.setEmail(usuario.getEmail());
		return activitiUser;
	}

	/**
	 * @param userRole
	 * @return
	 */
	public static GroupEntity soneRoleToActivitiGroupEntity(Role userRole) {
		GroupEntity groupEntity = new GroupEntity();
		groupEntity.setId(userRole.getId());
		groupEntity.setName(userRole.getName());
		groupEntity.setType(userRole.getCode());
		return groupEntity;
	}

}
