/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IStepExecutionContextService;
import atraxo.ad.domain.impl.scheduler.StepExecution;
import atraxo.ad.domain.impl.scheduler.StepExecutionContext;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link StepExecutionContext} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class StepExecutionContext_Service
		extends
			AbstractEntityService<StepExecutionContext>
		implements
			IStepExecutionContextService {

	/**
	 * Public constructor for StepExecutionContext_Service
	 */
	public StepExecutionContext_Service() {
		super();
	}

	/**
	 * Public constructor for StepExecutionContext_Service
	 * 
	 * @param em
	 */
	public StepExecutionContext_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<StepExecutionContext> getEntityClass() {
		return StepExecutionContext.class;
	}

	/**
	 * Find by reference: stepExecution
	 *
	 * @param stepExecution
	 * @return List<StepExecutionContext>
	 */
	public List<StepExecutionContext> findByStepExecution(
			StepExecution stepExecution) {
		return this.findByStepExecutionId(stepExecution.getId());
	}
	/**
	 * Find by ID of reference: stepExecution.id
	 *
	 * @param stepExecutionId
	 * @return List<StepExecutionContext>
	 */
	public List<StepExecutionContext> findByStepExecutionId(Long stepExecutionId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from StepExecutionContext e where  e.stepExecution.id = :stepExecutionId",
						StepExecutionContext.class)
				.setParameter("stepExecutionId", stepExecutionId)
				.getResultList();
	}
}
