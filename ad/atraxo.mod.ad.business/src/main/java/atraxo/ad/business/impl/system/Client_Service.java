/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.system;

import atraxo.ad.domain.impl.system.Client;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Client} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Client_Service extends AbstractEntityService<Client> {

	/**
	 * Public constructor for Client_Service
	 */
	public Client_Service() {
		super();
	}

	/**
	 * Public constructor for Client_Service
	 * 
	 * @param em
	 */
	public Client_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Client> getEntityClass() {
		return Client.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Client
	 */
	public Client findByCode(String code) {
		try {
			return this.getEntityManager()
					.createNamedQuery(Client.NQ_FIND_BY_CODE, Client.class)
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Client", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Client", "code"), nure);
		}
	}

}
