/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IStepExecutionSeqService;
import atraxo.ad.domain.impl.scheduler.StepExecutionSeq;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link StepExecutionSeq} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class StepExecutionSeq_Service
		extends
			AbstractEntityService<StepExecutionSeq>
		implements
			IStepExecutionSeqService {

	/**
	 * Public constructor for StepExecutionSeq_Service
	 */
	public StepExecutionSeq_Service() {
		super();
	}

	/**
	 * Public constructor for StepExecutionSeq_Service
	 * 
	 * @param em
	 */
	public StepExecutionSeq_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<StepExecutionSeq> getEntityClass() {
		return StepExecutionSeq.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return StepExecutionSeq
	 */
	public StepExecutionSeq findByUn(String uniqueKey) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(StepExecutionSeq.NQ_FIND_BY_UN,
							StepExecutionSeq.class)
					.setParameter("uniqueKey", uniqueKey).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"StepExecutionSeq", "uniqueKey"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"StepExecutionSeq", "uniqueKey"), nure);
		}
	}

}
