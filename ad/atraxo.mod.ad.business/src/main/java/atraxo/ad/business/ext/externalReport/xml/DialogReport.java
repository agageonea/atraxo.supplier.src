package atraxo.ad.business.ext.externalReport.xml;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement(name = "dialog-report")
public class DialogReport {

	private String code;
	private String format;
	private String title;
	private List<Parameter> paramList;

	@XmlElement
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@XmlElement
	public String getFormat() {
		return this.format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	@XmlElement
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@XmlElement(name = "params")
	public List<Parameter> getParamList() {
		return this.paramList;
	}

	@XmlJavaTypeAdapter(ParameterListAdapter.class)
	public void setParamList(List<Parameter> paramList) {
		this.paramList = paramList;
	}

}
