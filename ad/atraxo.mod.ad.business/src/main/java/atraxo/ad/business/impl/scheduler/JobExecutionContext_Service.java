/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IJobExecutionContextService;
import atraxo.ad.domain.impl.scheduler.JobExecution;
import atraxo.ad.domain.impl.scheduler.JobExecutionContext;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link JobExecutionContext} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class JobExecutionContext_Service
		extends
			AbstractEntityService<JobExecutionContext>
		implements
			IJobExecutionContextService {

	/**
	 * Public constructor for JobExecutionContext_Service
	 */
	public JobExecutionContext_Service() {
		super();
	}

	/**
	 * Public constructor for JobExecutionContext_Service
	 * 
	 * @param em
	 */
	public JobExecutionContext_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<JobExecutionContext> getEntityClass() {
		return JobExecutionContext.class;
	}

	/**
	 * Find by reference: jobExecution
	 *
	 * @param jobExecution
	 * @return List<JobExecutionContext>
	 */
	public List<JobExecutionContext> findByJobExecution(
			JobExecution jobExecution) {
		return this.findByJobExecutionId(jobExecution.getId());
	}
	/**
	 * Find by ID of reference: jobExecution.id
	 *
	 * @param jobExecutionId
	 * @return List<JobExecutionContext>
	 */
	public List<JobExecutionContext> findByJobExecutionId(Long jobExecutionId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from JobExecutionContext e where  e.jobExecution.id = :jobExecutionId",
						JobExecutionContext.class)
				.setParameter("jobExecutionId", jobExecutionId).getResultList();
	}
}
