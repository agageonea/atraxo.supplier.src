/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.report;

import atraxo.ad.business.api.report.IReportService;
import atraxo.ad.domain.impl.report.Report;
import atraxo.ad.domain.impl.report.ReportParam;
import atraxo.ad.domain.impl.report.ReportServer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Report} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Report_Service extends AbstractEntityService<Report>
		implements
			IReportService {

	/**
	 * Public constructor for Report_Service
	 */
	public Report_Service() {
		super();
	}

	/**
	 * Public constructor for Report_Service
	 * 
	 * @param em
	 */
	public Report_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Report> getEntityClass() {
		return Report.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Report
	 */
	public Report findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Report.NQ_FIND_BY_CODE, Report.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Report", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Report", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Report
	 */
	public Report findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Report.NQ_FIND_BY_NAME, Report.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Report", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Report", "name"), nure);
		}
	}

	/**
	 * Find by reference: reportServer
	 *
	 * @param reportServer
	 * @return List<Report>
	 */
	public List<Report> findByReportServer(ReportServer reportServer) {
		return this.findByReportServerId(reportServer.getId());
	}
	/**
	 * Find by ID of reference: reportServer.id
	 *
	 * @param reportServerId
	 * @return List<Report>
	 */
	public List<Report> findByReportServerId(String reportServerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Report e where e.clientId = :clientId and e.reportServer.id = :reportServerId",
						Report.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("reportServerId", reportServerId).getResultList();
	}
	/**
	 * Find by reference: reportParameters
	 *
	 * @param reportParameters
	 * @return List<Report>
	 */
	public List<Report> findByReportParameters(ReportParam reportParameters) {
		return this.findByReportParametersId(reportParameters.getId());
	}
	/**
	 * Find by ID of reference: reportParameters.id
	 *
	 * @param reportParametersId
	 * @return List<Report>
	 */
	public List<Report> findByReportParametersId(String reportParametersId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Report e, IN (e.reportParameters) c where e.clientId = :clientId and c.id = :reportParametersId",
						Report.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("reportParametersId", reportParametersId)
				.getResultList();
	}
}
