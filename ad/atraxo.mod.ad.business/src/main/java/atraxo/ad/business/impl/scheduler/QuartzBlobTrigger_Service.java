/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IQuartzBlobTriggerService;
import atraxo.ad.domain.impl.scheduler.QuartzBlobTrigger;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link QuartzBlobTrigger} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class QuartzBlobTrigger_Service
		extends
			AbstractEntityService<QuartzBlobTrigger>
		implements
			IQuartzBlobTriggerService {

	/**
	 * Public constructor for QuartzBlobTrigger_Service
	 */
	public QuartzBlobTrigger_Service() {
		super();
	}

	/**
	 * Public constructor for QuartzBlobTrigger_Service
	 * 
	 * @param em
	 */
	public QuartzBlobTrigger_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<QuartzBlobTrigger> getEntityClass() {
		return QuartzBlobTrigger.class;
	}

}
