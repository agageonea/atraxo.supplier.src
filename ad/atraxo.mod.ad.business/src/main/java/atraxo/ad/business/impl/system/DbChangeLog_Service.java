/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.system;

import atraxo.ad.business.api.system.IDbChangeLogService;
import atraxo.ad.domain.impl.system.DbChangeLog;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link DbChangeLog} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class DbChangeLog_Service extends AbstractEntityService<DbChangeLog>
		implements
			IDbChangeLogService {

	/**
	 * Public constructor for DbChangeLog_Service
	 */
	public DbChangeLog_Service() {
		super();
	}

	/**
	 * Public constructor for DbChangeLog_Service
	 * 
	 * @param em
	 */
	public DbChangeLog_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<DbChangeLog> getEntityClass() {
		return DbChangeLog.class;
	}

}
