package atraxo.ad.business.ext.externalReport.xml;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

class ReportXml {
	protected Marshaller jaxbMarshaller;
	protected DialogReport report;

	public ReportXml() throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(DialogReport.class, Parameter.class);
		this.jaxbMarshaller = jaxbContext.createMarshaller();
		this.jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		this.report = new DialogReport();
	}
}
