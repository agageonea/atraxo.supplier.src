/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.security;

import atraxo.ad.business.api.security.IAccessControlDsService;
import atraxo.ad.domain.impl.security.AccessControl;
import atraxo.ad.domain.impl.security.AccessControlDs;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link AccessControlDs} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class AccessControlDs_Service
		extends
			AbstractEntityService<AccessControlDs>
		implements
			IAccessControlDsService {

	/**
	 * Public constructor for AccessControlDs_Service
	 */
	public AccessControlDs_Service() {
		super();
	}

	/**
	 * Public constructor for AccessControlDs_Service
	 * 
	 * @param em
	 */
	public AccessControlDs_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<AccessControlDs> getEntityClass() {
		return AccessControlDs.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return AccessControlDs
	 */
	public AccessControlDs findByUnique(AccessControl accessControl,
			String dsName) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(AccessControlDs.NQ_FIND_BY_UNIQUE,
							AccessControlDs.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("accessControl", accessControl)
					.setParameter("dsName", dsName).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"AccessControlDs", "accessControl, dsName"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"AccessControlDs", "accessControl, dsName"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return AccessControlDs
	 */
	public AccessControlDs findByUnique(Long accessControlId, String dsName) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							AccessControlDs.NQ_FIND_BY_UNIQUE_PRIMITIVE,
							AccessControlDs.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("accessControlId", accessControlId)
					.setParameter("dsName", dsName).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"AccessControlDs", "accessControlId, dsName"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"AccessControlDs", "accessControlId, dsName"), nure);
		}
	}

	/**
	 * Find by reference: accessControl
	 *
	 * @param accessControl
	 * @return List<AccessControlDs>
	 */
	public List<AccessControlDs> findByAccessControl(AccessControl accessControl) {
		return this.findByAccessControlId(accessControl.getId());
	}
	/**
	 * Find by ID of reference: accessControl.id
	 *
	 * @param accessControlId
	 * @return List<AccessControlDs>
	 */
	public List<AccessControlDs> findByAccessControlId(String accessControlId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from AccessControlDs e where e.clientId = :clientId and e.accessControl.id = :accessControlId",
						AccessControlDs.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("accessControlId", accessControlId)
				.getResultList();
	}
}
