/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.ext.externalReport.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import atraxo.ad.business.api.externalReport.ISourceUploadHistoryService;
import atraxo.ad.domain.impl.externalReport.SourceUploadHistory;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link SourceUploadHistory} domain entity.
 */
public class SourceUploadHistory_Service extends atraxo.ad.business.impl.externalReport.SourceUploadHistory_Service
		implements ISourceUploadHistoryService {

	@Override
	@Transactional
	public void empty(String externalReportId) throws BusinessException {
		List<SourceUploadHistory> sourceUploadHistoryList = this.findByExternalReportId(externalReportId);
		List<Object> ids = new ArrayList<>();
		for (SourceUploadHistory sourceUploadHistory : sourceUploadHistoryList) {
			ids.add(sourceUploadHistory.getId());
		}
		this.deleteByIds(ids);
	}

}
