/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.externalReport;

import atraxo.ad.domain.impl.businessArea.BusinessArea;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.ExternalReportParameter;
import atraxo.ad.domain.impl.externalReport.ReportLog;
import atraxo.ad.domain.impl.externalReport.SourceUploadHistory;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ExternalReport} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ExternalReport_Service
		extends
			AbstractEntityService<ExternalReport> {

	/**
	 * Public constructor for ExternalReport_Service
	 */
	public ExternalReport_Service() {
		super();
	}

	/**
	 * Public constructor for ExternalReport_Service
	 * 
	 * @param em
	 */
	public ExternalReport_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ExternalReport> getEntityClass() {
		return ExternalReport.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ExternalReport
	 */
	public ExternalReport findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ExternalReport.NQ_FIND_BY_NAME,
							ExternalReport.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ExternalReport", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ExternalReport", "name"), nure);
		}
	}

	/**
	 * Find by reference: businessArea
	 *
	 * @param businessArea
	 * @return List<ExternalReport>
	 */
	public List<ExternalReport> findByBusinessArea(BusinessArea businessArea) {
		return this.findByBusinessAreaId(businessArea.getId());
	}
	/**
	 * Find by ID of reference: businessArea.id
	 *
	 * @param businessAreaId
	 * @return List<ExternalReport>
	 */
	public List<ExternalReport> findByBusinessAreaId(String businessAreaId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ExternalReport e where e.clientId = :clientId and e.businessArea.id = :businessAreaId",
						ExternalReport.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("businessAreaId", businessAreaId).getResultList();
	}
	/**
	 * Find by reference: params
	 *
	 * @param params
	 * @return List<ExternalReport>
	 */
	public List<ExternalReport> findByParams(ExternalReportParameter params) {
		return this.findByParamsId(params.getId());
	}
	/**
	 * Find by ID of reference: params.id
	 *
	 * @param paramsId
	 * @return List<ExternalReport>
	 */
	public List<ExternalReport> findByParamsId(String paramsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from ExternalReport e, IN (e.params) c where e.clientId = :clientId and c.id = :paramsId",
						ExternalReport.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("paramsId", paramsId).getResultList();
	}
	/**
	 * Find by reference: reportLogs
	 *
	 * @param reportLogs
	 * @return List<ExternalReport>
	 */
	public List<ExternalReport> findByReportLogs(ReportLog reportLogs) {
		return this.findByReportLogsId(reportLogs.getId());
	}
	/**
	 * Find by ID of reference: reportLogs.id
	 *
	 * @param reportLogsId
	 * @return List<ExternalReport>
	 */
	public List<ExternalReport> findByReportLogsId(String reportLogsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from ExternalReport e, IN (e.reportLogs) c where e.clientId = :clientId and c.id = :reportLogsId",
						ExternalReport.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("reportLogsId", reportLogsId).getResultList();
	}
	/**
	 * Find by reference: sourceUploadHistories
	 *
	 * @param sourceUploadHistories
	 * @return List<ExternalReport>
	 */
	public List<ExternalReport> findBySourceUploadHistories(
			SourceUploadHistory sourceUploadHistories) {
		return this
				.findBySourceUploadHistoriesId(sourceUploadHistories.getId());
	}
	/**
	 * Find by ID of reference: sourceUploadHistories.id
	 *
	 * @param sourceUploadHistoriesId
	 * @return List<ExternalReport>
	 */
	public List<ExternalReport> findBySourceUploadHistoriesId(
			String sourceUploadHistoriesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from ExternalReport e, IN (e.sourceUploadHistories) c where e.clientId = :clientId and c.id = :sourceUploadHistoriesId",
						ExternalReport.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("sourceUploadHistoriesId",
						sourceUploadHistoriesId).getResultList();
	}
}
