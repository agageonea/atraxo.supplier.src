/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.report;

import atraxo.ad.business.api.report.IDsReportUsageService;
import atraxo.ad.domain.impl.report.DsReport;
import atraxo.ad.domain.impl.report.DsReportUsage;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link DsReportUsage} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class DsReportUsage_Service extends AbstractEntityService<DsReportUsage>
		implements
			IDsReportUsageService {

	/**
	 * Public constructor for DsReportUsage_Service
	 */
	public DsReportUsage_Service() {
		super();
	}

	/**
	 * Public constructor for DsReportUsage_Service
	 * 
	 * @param em
	 */
	public DsReportUsage_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<DsReportUsage> getEntityClass() {
		return DsReportUsage.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return DsReportUsage
	 */
	public DsReportUsage findByReportUsage_ds(DsReport dsReport) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DsReportUsage.NQ_FIND_BY_REPORTUSAGE_DS,
							DsReportUsage.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("dsReport", dsReport).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DsReportUsage", "dsReport"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DsReportUsage", "dsReport"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return DsReportUsage
	 */
	public DsReportUsage findByReportUsage_ds(Long dsReportId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							DsReportUsage.NQ_FIND_BY_REPORTUSAGE_DS_PRIMITIVE,
							DsReportUsage.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("dsReportId", dsReportId).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DsReportUsage", "dsReportId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DsReportUsage", "dsReportId"), nure);
		}
	}

	/**
	 * Find by reference: dsReport
	 *
	 * @param dsReport
	 * @return List<DsReportUsage>
	 */
	public List<DsReportUsage> findByDsReport(DsReport dsReport) {
		return this.findByDsReportId(dsReport.getId());
	}
	/**
	 * Find by ID of reference: dsReport.id
	 *
	 * @param dsReportId
	 * @return List<DsReportUsage>
	 */
	public List<DsReportUsage> findByDsReportId(String dsReportId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DsReportUsage e where e.clientId = :clientId and e.dsReport.id = :dsReportId",
						DsReportUsage.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("dsReportId", dsReportId).getResultList();
	}
}
