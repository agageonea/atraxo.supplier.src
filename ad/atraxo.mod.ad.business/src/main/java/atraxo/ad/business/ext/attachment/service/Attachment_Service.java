/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.ext.attachment.service;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.ad.business.api.attachment.IAttachmentService;
import atraxo.ad.business.api.attachment.IAttachmentTypeService;
import atraxo.ad.domain.impl.ad.TAttachmentType;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.business.service.storage.StorageService;

/**
 * Business extensions specific for {@link Attachment} domain entity.
 */
public class Attachment_Service extends atraxo.ad.business.impl.attachment.Attachment_Service implements IAttachmentService {

	@Autowired
	private StorageService storageService;

	@Autowired
	private IAttachmentTypeService attachmentTypeService;

	@Override
	protected void postInsert(Attachment e) throws BusinessException {
		super.postInsert(e);

		// Monitor documents for contract blueprint
		if ("Contract".equals(e.getTargetAlias())) {
			this.sendMessage("contractAttachmentChannel", e);
		}
	}

	@Override
	protected void preInsert(Attachment e) throws BusinessException {
		String name = e.getName();

		if (name == null || "".equals(name)) {
			AttachmentType type = e.getType();
			if (type == null) {
				throw new BusinessException(ErrorCode.G_NULL_FIELD_TYPE, "No type specified for attachment.");
			}
			if (TAttachmentType._LINK_.equals(type.getCategory())) {
				e.setName(e.getLocation());
			} else {
				e.setName(e.getFileName());
			}
		}
		if (e.getFileName().contains(" ")) {
			e.setFileName(e.getFileName().replace(" ", "_"));
		}
		super.preInsert(e);
	}

	@Override
	public InputStream download(String refId) throws BusinessException {
		return this.storageService.downloadFile(refId);
	}

	@Override
	public void deleteFromS3(String id) throws BusinessException {
		Attachment attachment = this.findById(id);
		if (attachment != null) {
			this.storageService.deleteFile(attachment.getRefid());
		}
	}

	@Override
	public void insertWithoutUpload(Attachment attachment) throws BusinessException {
		this.onInsert(attachment);
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);

		// Monitor documents for contract blueprint
		for (Attachment attachment : this.findByIds(ids)) {
			if ("Contract".equals(attachment.getTargetAlias())) {
				this.sendMessage("contractAttachmentChannel", attachment);
			}
		}
	}

	@Override
	public List<Attachment> getDocumentsByType(Integer targetRefid, String targetAlias, TAttachmentType docType) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("targetRefid", targetRefid);
		params.put("targetAlias", targetAlias);
		params.put("type", this.getAttachmentTypes(docType));
		return this.findEntitiesByAttributes(params);
	}

	public List<AttachmentType> getAttachmentTypes(TAttachmentType docType) {
		Map<String, Object> params2 = new HashMap<>();
		params2.put("category", docType);
		return this.attachmentTypeService.findEntitiesByAttributes(params2);
	}
}
