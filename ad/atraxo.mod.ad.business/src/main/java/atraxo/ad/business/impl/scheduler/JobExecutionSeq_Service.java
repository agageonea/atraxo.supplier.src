/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IJobExecutionSeqService;
import atraxo.ad.domain.impl.scheduler.JobExecutionSeq;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link JobExecutionSeq} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class JobExecutionSeq_Service
		extends
			AbstractEntityService<JobExecutionSeq>
		implements
			IJobExecutionSeqService {

	/**
	 * Public constructor for JobExecutionSeq_Service
	 */
	public JobExecutionSeq_Service() {
		super();
	}

	/**
	 * Public constructor for JobExecutionSeq_Service
	 * 
	 * @param em
	 */
	public JobExecutionSeq_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<JobExecutionSeq> getEntityClass() {
		return JobExecutionSeq.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return JobExecutionSeq
	 */
	public JobExecutionSeq findByUn(String uniqueKey) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(JobExecutionSeq.NQ_FIND_BY_UN,
							JobExecutionSeq.class)
					.setParameter("uniqueKey", uniqueKey).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"JobExecutionSeq", "uniqueKey"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"JobExecutionSeq", "uniqueKey"), nure);
		}
	}

}
