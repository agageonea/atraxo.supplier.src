package atraxo.ad.business.ext.bpm;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;
import org.activiti.engine.impl.persistence.entity.IdentityInfoEntity;
import org.activiti.engine.impl.persistence.entity.UserEntity;
import org.activiti.engine.impl.persistence.entity.UserEntityManager;

import atraxo.ad.business.api.security.IUserService;
import atraxo.ad.domain.impl.security.Role;

/**
 * @author vhojda
 */
public class ActivitiUserManager extends UserEntityManager {

	private IUserService userService;

	/**
	 *
	 */
	public ActivitiUserManager() {
		super();
	}

	/**
	 * @param userService
	 */
	public ActivitiUserManager(IUserService userService) {
		this();
		this.userService = userService;
	}

	@Override
	public UserEntity findUserById(String username) {
		atraxo.ad.domain.impl.security.User userSone = this.userService.findByLogin(username);
		return ActivitiUserHelper.soneUserToActivitiUserEntity(userSone);
	}

	@Override
	public List<Group> findGroupsByUser(String username) {
		atraxo.ad.domain.impl.security.User soneUser = this.userService.findByLogin(username);
		List<Group> groups = new ArrayList<Group>();
		for (Role role : soneUser.getRoles()) {
			groups.add(ActivitiUserHelper.soneRoleToActivitiGroupEntity(role));
		}
		return groups;
	}

	@Override
	public User createNewUser(String userId) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void insertUser(User user) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void updateUser(User updatedUser) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteUser(String userId) {
		throw new UnsupportedOperationException();
	}

	@Override
	public UserQuery createNewUserQuery() {
		return super.createNewUserQuery();
	}

	@Override
	public IdentityInfoEntity findUserInfoByUserIdAndKey(String userId, String key) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<String> findUserInfoKeysByUserIdAndType(String userId, String type) {
		throw new UnsupportedOperationException();
	}

	public IUserService getUserService() {
		return this.userService;
	}

	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

}
