package atraxo.ad.business.ext.externalReport.xml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAnyElement;

public class ListWrapper<T> {

	@XmlAnyElement
	public List<T> properties = new ArrayList<>();
}
