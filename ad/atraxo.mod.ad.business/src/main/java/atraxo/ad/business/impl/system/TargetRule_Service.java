/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.system;

import atraxo.ad.business.api.system.ITargetRuleService;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.ad.domain.impl.system.TargetRule;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link TargetRule} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class TargetRule_Service extends AbstractEntityService<TargetRule>
		implements
			ITargetRuleService {

	/**
	 * Public constructor for TargetRule_Service
	 */
	public TargetRule_Service() {
		super();
	}

	/**
	 * Public constructor for TargetRule_Service
	 * 
	 * @param em
	 */
	public TargetRule_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<TargetRule> getEntityClass() {
		return TargetRule.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return TargetRule
	 */
	public TargetRule findByTargetRule(AttachmentType sourceRefId,
			String targetAlias) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(TargetRule.NQ_FIND_BY_TARGETRULE,
							TargetRule.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("sourceRefId", sourceRefId)
					.setParameter("targetAlias", targetAlias).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TargetRule", "sourceRefId, targetAlias"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TargetRule", "sourceRefId, targetAlias"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return TargetRule
	 */
	public TargetRule findByTargetRule(Long sourceRefIdId, String targetAlias) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							TargetRule.NQ_FIND_BY_TARGETRULE_PRIMITIVE,
							TargetRule.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("sourceRefIdId", sourceRefIdId)
					.setParameter("targetAlias", targetAlias).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TargetRule", "sourceRefIdId, targetAlias"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TargetRule", "sourceRefIdId, targetAlias"), nure);
		}
	}

	/**
	 * Find by reference: sourceRefId
	 *
	 * @param sourceRefId
	 * @return List<TargetRule>
	 */
	public List<TargetRule> findBySourceRefId(AttachmentType sourceRefId) {
		return this.findBySourceRefIdId(sourceRefId.getId());
	}
	/**
	 * Find by ID of reference: sourceRefId.id
	 *
	 * @param sourceRefIdId
	 * @return List<TargetRule>
	 */
	public List<TargetRule> findBySourceRefIdId(String sourceRefIdId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TargetRule e where e.clientId = :clientId and e.sourceRefId.id = :sourceRefIdId",
						TargetRule.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("sourceRefIdId", sourceRefIdId).getResultList();
	}
}
