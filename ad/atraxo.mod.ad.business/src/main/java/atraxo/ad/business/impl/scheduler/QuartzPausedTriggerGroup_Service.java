/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IQuartzPausedTriggerGroupService;
import atraxo.ad.domain.impl.scheduler.QuartzPausedTriggerGroup;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link QuartzPausedTriggerGroup} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class QuartzPausedTriggerGroup_Service
		extends
			AbstractEntityService<QuartzPausedTriggerGroup>
		implements
			IQuartzPausedTriggerGroupService {

	/**
	 * Public constructor for QuartzPausedTriggerGroup_Service
	 */
	public QuartzPausedTriggerGroup_Service() {
		super();
	}

	/**
	 * Public constructor for QuartzPausedTriggerGroup_Service
	 * 
	 * @param em
	 */
	public QuartzPausedTriggerGroup_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<QuartzPausedTriggerGroup> getEntityClass() {
		return QuartzPausedTriggerGroup.class;
	}

}
