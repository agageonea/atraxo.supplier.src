/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.externalReport;

import atraxo.ad.business.api.externalReport.IExternalReportParameterService;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.ExternalReportParameter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ExternalReportParameter} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ExternalReportParameter_Service
		extends
			AbstractEntityService<ExternalReportParameter>
		implements
			IExternalReportParameterService {

	/**
	 * Public constructor for ExternalReportParameter_Service
	 */
	public ExternalReportParameter_Service() {
		super();
	}

	/**
	 * Public constructor for ExternalReportParameter_Service
	 * 
	 * @param em
	 */
	public ExternalReportParameter_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ExternalReportParameter> getEntityClass() {
		return ExternalReportParameter.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ExternalReportParameter
	 */
	public ExternalReportParameter findByKey(ExternalReport externalReport,
			String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ExternalReportParameter.NQ_FIND_BY_KEY,
							ExternalReportParameter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("externalReport", externalReport)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ExternalReportParameter", "externalReport, name"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ExternalReportParameter", "externalReport, name"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return ExternalReportParameter
	 */
	public ExternalReportParameter findByKey(Long externalReportId, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							ExternalReportParameter.NQ_FIND_BY_KEY_PRIMITIVE,
							ExternalReportParameter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("externalReportId", externalReportId)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(
					J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ExternalReportParameter", "externalReportId, name"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(
					J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ExternalReportParameter", "externalReportId, name"),
					nure);
		}
	}

	/**
	 * Find by reference: externalReport
	 *
	 * @param externalReport
	 * @return List<ExternalReportParameter>
	 */
	public List<ExternalReportParameter> findByExternalReport(
			ExternalReport externalReport) {
		return this.findByExternalReportId(externalReport.getId());
	}
	/**
	 * Find by ID of reference: externalReport.id
	 *
	 * @param externalReportId
	 * @return List<ExternalReportParameter>
	 */
	public List<ExternalReportParameter> findByExternalReportId(
			String externalReportId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ExternalReportParameter e where e.clientId = :clientId and e.externalReport.id = :externalReportId",
						ExternalReportParameter.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("externalReportId", externalReportId)
				.getResultList();
	}
}
