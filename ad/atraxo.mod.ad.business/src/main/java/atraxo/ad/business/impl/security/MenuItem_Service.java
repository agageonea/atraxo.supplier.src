/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.security;

import atraxo.ad.business.api.security.IMenuItemService;
import atraxo.ad.domain.impl.security.Menu;
import atraxo.ad.domain.impl.security.MenuItem;
import atraxo.ad.domain.impl.security.Role;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link MenuItem} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class MenuItem_Service extends AbstractEntityService<MenuItem>
		implements
			IMenuItemService {

	/**
	 * Public constructor for MenuItem_Service
	 */
	public MenuItem_Service() {
		super();
	}

	/**
	 * Public constructor for MenuItem_Service
	 * 
	 * @param em
	 */
	public MenuItem_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<MenuItem> getEntityClass() {
		return MenuItem.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return MenuItem
	 */
	public MenuItem findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(MenuItem.NQ_FIND_BY_NAME, MenuItem.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"MenuItem", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"MenuItem", "name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return MenuItem
	 */
	public MenuItem findByFrame(String frame) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(MenuItem.NQ_FIND_BY_FRAME, MenuItem.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("frame", frame).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"MenuItem", "frame"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"MenuItem", "frame"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return MenuItem
	 */
	public MenuItem findByTitle(String title) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(MenuItem.NQ_FIND_BY_TITLE, MenuItem.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("title", title).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"MenuItem", "title"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"MenuItem", "title"), nure);
		}
	}

	/**
	 * Find by reference: menuItem
	 *
	 * @param menuItem
	 * @return List<MenuItem>
	 */
	public List<MenuItem> findByMenuItem(MenuItem menuItem) {
		return this.findByMenuItemId(menuItem.getId());
	}
	/**
	 * Find by ID of reference: menuItem.id
	 *
	 * @param menuItemId
	 * @return List<MenuItem>
	 */
	public List<MenuItem> findByMenuItemId(String menuItemId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from MenuItem e where e.clientId = :clientId and e.menuItem.id = :menuItemId",
						MenuItem.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("menuItemId", menuItemId).getResultList();
	}
	/**
	 * Find by reference: menu
	 *
	 * @param menu
	 * @return List<MenuItem>
	 */
	public List<MenuItem> findByMenu(Menu menu) {
		return this.findByMenuId(menu.getId());
	}
	/**
	 * Find by ID of reference: menu.id
	 *
	 * @param menuId
	 * @return List<MenuItem>
	 */
	public List<MenuItem> findByMenuId(String menuId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from MenuItem e where e.clientId = :clientId and e.menu.id = :menuId",
						MenuItem.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("menuId", menuId).getResultList();
	}
	/**
	 * Find by reference: roles
	 *
	 * @param roles
	 * @return List<MenuItem>
	 */
	public List<MenuItem> findByRoles(Role roles) {
		return this.findByRolesId(roles.getId());
	}
	/**
	 * Find by ID of reference: roles.id
	 *
	 * @param rolesId
	 * @return List<MenuItem>
	 */
	public List<MenuItem> findByRolesId(String rolesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from MenuItem e, IN (e.roles) c where e.clientId = :clientId and c.id = :rolesId",
						MenuItem.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("rolesId", rolesId).getResultList();
	}
}
