/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.domain.impl.scheduler.JobContext;
import atraxo.ad.domain.impl.scheduler.JobContextParam;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link JobContext} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class JobContext_Service extends AbstractEntityService<JobContext> {

	/**
	 * Public constructor for JobContext_Service
	 */
	public JobContext_Service() {
		super();
	}

	/**
	 * Public constructor for JobContext_Service
	 * 
	 * @param em
	 */
	public JobContext_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<JobContext> getEntityClass() {
		return JobContext.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return JobContext
	 */
	public JobContext findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(JobContext.NQ_FIND_BY_NAME,
							JobContext.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"JobContext", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"JobContext", "name"), nure);
		}
	}

	/**
	 * Find by reference: params
	 *
	 * @param params
	 * @return List<JobContext>
	 */
	public List<JobContext> findByParams(JobContextParam params) {
		return this.findByParamsId(params.getId());
	}
	/**
	 * Find by ID of reference: params.id
	 *
	 * @param paramsId
	 * @return List<JobContext>
	 */
	public List<JobContext> findByParamsId(String paramsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from JobContext e, IN (e.params) c where e.clientId = :clientId and c.id = :paramsId",
						JobContext.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("paramsId", paramsId).getResultList();
	}
}
