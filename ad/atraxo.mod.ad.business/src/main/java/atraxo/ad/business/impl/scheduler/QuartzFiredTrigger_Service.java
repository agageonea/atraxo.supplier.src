/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IQuartzFiredTriggerService;
import atraxo.ad.domain.impl.scheduler.QuartzFiredTrigger;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link QuartzFiredTrigger} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class QuartzFiredTrigger_Service
		extends
			AbstractEntityService<QuartzFiredTrigger>
		implements
			IQuartzFiredTriggerService {

	/**
	 * Public constructor for QuartzFiredTrigger_Service
	 */
	public QuartzFiredTrigger_Service() {
		super();
	}

	/**
	 * Public constructor for QuartzFiredTrigger_Service
	 * 
	 * @param em
	 */
	public QuartzFiredTrigger_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<QuartzFiredTrigger> getEntityClass() {
		return QuartzFiredTrigger.class;
	}

}
