package atraxo.ad.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.IErrorCode;

/**
 * Use when any error occurred with birt report generation
 *
 * @author zspeter
 */
public class ReportCenterException extends BusinessException {

	/**
	 *
	 */
	private static final long serialVersionUID = -7146784106754145131L;

	public ReportCenterException(IErrorCode errorCode, String errorDetails, Throwable exception) {
		super(errorCode, errorDetails, exception);
	}

	public ReportCenterException(IErrorCode errorCode, String errorDetails) {
		super(errorCode, errorDetails);
	}

	public ReportCenterException(IErrorCode errorCode, Throwable exception) {
		super(errorCode, exception);
	}

}
