/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.report;

import atraxo.ad.business.api.report.IReportServerService;
import atraxo.ad.domain.impl.report.ReportServer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ReportServer} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ReportServer_Service extends AbstractEntityService<ReportServer>
		implements
			IReportServerService {

	/**
	 * Public constructor for ReportServer_Service
	 */
	public ReportServer_Service() {
		super();
	}

	/**
	 * Public constructor for ReportServer_Service
	 * 
	 * @param em
	 */
	public ReportServer_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ReportServer> getEntityClass() {
		return ReportServer.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ReportServer
	 */
	public ReportServer findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ReportServer.NQ_FIND_BY_NAME,
							ReportServer.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ReportServer", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ReportServer", "name"), nure);
		}
	}

}
