/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.system;

import atraxo.ad.domain.impl.system.Param;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Param} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Param_Service extends AbstractEntityService<Param> {

	/**
	 * Public constructor for Param_Service
	 */
	public Param_Service() {
		super();
	}

	/**
	 * Public constructor for Param_Service
	 * 
	 * @param em
	 */
	public Param_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Param> getEntityClass() {
		return Param.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Param
	 */
	public Param findByCode(String code) {
		try {
			return this.getEntityManager()
					.createNamedQuery(Param.NQ_FIND_BY_CODE, Param.class)
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Param", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Param", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Param
	 */
	public Param findByName(String name) {
		try {
			return this.getEntityManager()
					.createNamedQuery(Param.NQ_FIND_BY_NAME, Param.class)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Param", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Param", "name"), nure);
		}
	}

}
