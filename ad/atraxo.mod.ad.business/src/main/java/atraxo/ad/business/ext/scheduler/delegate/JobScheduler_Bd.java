package atraxo.ad.business.ext.scheduler.delegate;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;

import atraxo.ad.domain.impl.scheduler.JobContext;
import atraxo.ad.domain.impl.scheduler.JobContextParam;
import atraxo.ad.domain.impl.scheduler.JobTimer;
import seava.j4e.api.Constants;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.service.job.IScheduler;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.AbstractBusinessDelegate;
import seava.j4e.scheduler.JobDetailBase;

public class JobScheduler_Bd extends AbstractBusinessDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(JobScheduler_Bd.class);

	/**
	 * Delete Quartz jobs associated with the {@link JobContext} entities identified by the list of ids.
	 *
	 * @param jobContextIds
	 * @throws BusinessException
	 */
	public void removeQuartzJob(List<Object> jobContextIds) throws BusinessException {
		List<JobKey> jobKeys = new ArrayList<>();
		String clientId = Session.user.get().getClientId();
		for (Object id : jobContextIds) {
			jobKeys.add(new JobKey((String) id, clientId));
		}
		Scheduler quartzScheduler = this.getQuartzScheduler();
		try {
			quartzScheduler.deleteJobs(jobKeys);
		} catch (SchedulerException exc) {
			LOGGER.error("Error:can not delete jobs, will raise Business Exception !", exc);
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, "Cannot delete quartz jobs", exc);
		}
	}

	/**
	 * Create Quartz jobs for a list of {@link JobContext} entities.
	 *
	 * @param list
	 * @throws BusinessException
	 */
	public void createQuartzJob(List<JobContext> list) throws BusinessException {

		Scheduler quartzScheduler = this.getQuartzScheduler();
		for (JobContext e : list) {
			JobDetail jobDetail = newJob(JobDetailBase.class).withIdentity(e.getId(), e.getClientId()).storeDurably().build();
			jobDetail.getJobDataMap().put(Constants.QUARTZ_JOB_NAME, e.getJobName());

			List<SimpleDateFormat> dateFormats = new ArrayList<>();
			String dateFormatMasks = Constants.get_server_alt_formats();
			String[] t = dateFormatMasks.split(";");
			for (int i = 0, len = t.length; i < len; i++) {
				dateFormats.add(new SimpleDateFormat(t[i]));
			}

			for (JobContextParam jcp : e.getParams()) {
				String name = jcp.getParamName();
				String value = jcp.getValue();
				String dataType = jcp.getDataType();

				Object v = value;
				if (value != null && !"".equals(value) && "java.util.Date".equals(dataType)) {
					boolean ok = false;
					for (SimpleDateFormat df : dateFormats) {
						try {
							v = df.parse(value);
							ok = true;
							break;
						} catch (Exception exc) {
							// ignore and try the next one
							LOGGER.info("Caught an exception when parsing date, will ignore and try the next one !", exc);
						}
					}
					if (!ok) {
						throw new IllegalArgumentException(
								"Date value [" + value + "] doesn't match any of the expected formats:  [" + dateFormatMasks + "]");
					}
				}

				jobDetail.getJobDataMap().put(name, v);
			}

			try {
				quartzScheduler.addJob(jobDetail, true);
			} catch (Exception exc) {
				throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, "Cannot add job to quartz scheduler.", exc);
			}
		}
	}

	/**
	 * Delete Quartz triggers associated with the {@link JobTimer} entities identified by the list of ids.
	 *
	 * @param jobTimerIds
	 * @throws BusinessException
	 */
	public void removeQuartzTrigger(List<Object> jobTimerIds) throws BusinessException {
		List<TriggerKey> triggerKeys = new ArrayList<>();
		String clientId = Session.user.get().getClientId();
		for (Object id : jobTimerIds) {
			triggerKeys.add(new TriggerKey((String) id, clientId));
		}
		Scheduler quartzScheduler = this.getQuartzScheduler();
		try {
			quartzScheduler.unscheduleJobs(triggerKeys);
		} catch (SchedulerException exc) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, "Cannot delete quartz triggers.", exc);
		}
	}

	/**
	 * Create Quartz triggers for a list of {@link JobTimer} entities.
	 *
	 * @param isUpdateContext
	 * @param list
	 * @throws BusinessException
	 */
	public void createQuartzTrigger(List<JobTimer> list, boolean isUpdateContext) throws BusinessException {

		Scheduler quartzScheduler = this.getQuartzScheduler();

		for (JobTimer e : list) {
			try {

				JobDetail jobDetail = quartzScheduler.getJobDetail(JobKey.jobKey(e.getJobContext().getId(), e.getJobContext().getClientId()));

				Trigger newTrigger = this.createQuartzTrigger(e, jobDetail);

				if (isUpdateContext) {
					Trigger oldTrigger = quartzScheduler.getTrigger(TriggerKey.triggerKey(e.getId(), e.getClientId()));

					if (oldTrigger == null) {
						quartzScheduler.scheduleJob(newTrigger);
					} else {
						quartzScheduler.rescheduleJob(TriggerKey.triggerKey(e.getId(), e.getClientId()), newTrigger);
					}
				} else {
					quartzScheduler.scheduleJob(newTrigger);
				}

			} catch (Exception ex) {
				throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, "Cannot create timer ", ex);
			}
		}
	}

	/**
	 * Helper method to create a Quartz Trigger.
	 *
	 * @param e
	 * @param jobDetail
	 * @return
	 */
	protected Trigger createQuartzTrigger(JobTimer e, JobDetail jobDetail) {
		Trigger newTrigger = null;
		if (e.getType() != null && "simple".equals(e.getType().getName())) {

			SimpleScheduleBuilder sb = simpleSchedule().withRepeatCount(e.getRepeatCount());

			if ("seconds".equals(e.getRepeatIntervalType().getName())) {
				sb = sb.withIntervalInSeconds(e.getRepeatInterval());
			}
			if ("minutes".equals(e.getRepeatIntervalType().getName())) {
				sb = sb.withIntervalInMinutes(e.getRepeatInterval());
			}
			if ("hours".equals(e.getRepeatIntervalType().getName())) {
				sb = sb.withIntervalInHours(e.getRepeatInterval());
			}

			newTrigger = newTrigger().withIdentity(e.getId(), e.getClientId()).startAt(e.getStartTime()).endAt(e.getEndTime()).withSchedule(sb)
					.forJob(jobDetail).build();

		} else if ("cron".equals(e.getType().getName())) {
			newTrigger = newTrigger().withIdentity(e.getId(), e.getClientId()).startAt(e.getStartTime()).endAt(e.getEndTime())
					.withSchedule(cronSchedule(e.getCronExpression())).forJob(jobDetail).build();
		}
		return newTrigger;
	}

	/**
	 * Get the Quartz scheduler
	 *
	 * @return
	 * @throws BusinessException
	 */
	protected Scheduler getQuartzScheduler() throws BusinessException {
		try {
			return (Scheduler) this.getApplicationContext().getBean(IScheduler.class).getDelegate();
		} catch (BeansException e) {
			throw e;
		} catch (Exception e) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, "Cannot get Quartz scheduler", e);
		}
	}
}
