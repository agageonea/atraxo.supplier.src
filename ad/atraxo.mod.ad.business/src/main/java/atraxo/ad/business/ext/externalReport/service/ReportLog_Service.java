/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.ext.externalReport.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import atraxo.ad.business.api.externalReport.IReportLogService;
import atraxo.ad.domain.impl.ad.ReportStatus;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.ReportLog;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.report.IReportService;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link ReportLog} domain entity.
 */
public class ReportLog_Service extends atraxo.ad.business.impl.externalReport.ReportLog_Service implements IReportLogService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReportLog_Service.class);

	@Autowired
	private IReportService reportService;

	@Override
	@Transactional
	public void empty(String externalReportId) throws BusinessException {
		List<ReportLog> reportList = this.findByExternalReportId(externalReportId);
		List<Object> ids = this.collectIds(reportList);
		this.deleteByIds(ids);
	}

	@Override
	public byte[] getReport(String logId) throws BusinessException {
		ReportLog log = this.findById(logId);
		return this.reportService.downloadReport(log.getReportId());
	}

	public byte[] getBytesFromInputStream(InputStream is) throws IOException {
		try (ByteArrayOutputStream os = new ByteArrayOutputStream();) {
			byte[] buffer = new byte[0xFFFF];
			for (int len; (len = is.read(buffer)) != -1;) {
				os.write(buffer, 0, len);
			}
			os.flush();
			return os.toByteArray();
		}
	}

	@Override
	@Transactional
	public void updateLog(ExternalReport externalReport, Integer reportId, ReportStatus reportStatus, String description) throws BusinessException {
		LOGGER.info("Report generation was " + reportStatus.name().toLowerCase());
		Map<String, Object> params = new HashMap<>();
		params.put("externalReport", externalReport);
		params.put("reportId", reportId);
		List<ReportLog> logs = this.findEntitiesByAttributes(params);
		if (CollectionUtils.isEmpty(logs)) {
			return;
		}
		for (ReportLog log : logs) {
			log.setStatus(reportStatus);
			if (!StringUtils.isEmpty(description)) {
				log.setLog(description);
			}
		}
		this.update(logs);
	}

	@Override
	public ReportLog findLastEntry(String externalReportId) throws BusinessException {
		try {
			return this.getEntityManager()
					.createQuery(
							"select e from ReportLog e where e.clientId = :clientId and e.externalReport.id = :externalReportId ORDER BY e.runDate DESC",
							ReportLog.class)
					.setParameter("clientId", Session.user.get().getClient().getId()).setParameter("externalReportId", externalReportId)
					.setMaxResults(1).getSingleResult();
		} catch (Exception e) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(e.getMessage(), e);
			}
			return null;
		}
	}
}
