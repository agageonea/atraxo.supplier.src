/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.security;

import atraxo.ad.business.api.security.IRoleService;
import atraxo.ad.domain.impl.security.AccessControl;
import atraxo.ad.domain.impl.security.Menu;
import atraxo.ad.domain.impl.security.MenuItem;
import atraxo.ad.domain.impl.security.Role;
import atraxo.ad.domain.impl.security.User;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Role} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Role_Service extends AbstractEntityService<Role>
		implements
			IRoleService {

	/**
	 * Public constructor for Role_Service
	 */
	public Role_Service() {
		super();
	}

	/**
	 * Public constructor for Role_Service
	 * 
	 * @param em
	 */
	public Role_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Role> getEntityClass() {
		return Role.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Role
	 */
	public Role findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Role.NQ_FIND_BY_CODE, Role.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Role", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Role", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Role
	 */
	public Role findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Role.NQ_FIND_BY_NAME, Role.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Role", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Role", "name"), nure);
		}
	}

	/**
	 * Find by reference: users
	 *
	 * @param users
	 * @return List<Role>
	 */
	public List<Role> findByUsers(User users) {
		return this.findByUsersId(users.getId());
	}
	/**
	 * Find by ID of reference: users.id
	 *
	 * @param usersId
	 * @return List<Role>
	 */
	public List<Role> findByUsersId(String usersId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Role e, IN (e.users) c where e.clientId = :clientId and c.id = :usersId",
						Role.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("usersId", usersId).getResultList();
	}
	/**
	 * Find by reference: accessControls
	 *
	 * @param accessControls
	 * @return List<Role>
	 */
	public List<Role> findByAccessControls(AccessControl accessControls) {
		return this.findByAccessControlsId(accessControls.getId());
	}
	/**
	 * Find by ID of reference: accessControls.id
	 *
	 * @param accessControlsId
	 * @return List<Role>
	 */
	public List<Role> findByAccessControlsId(String accessControlsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Role e, IN (e.accessControls) c where e.clientId = :clientId and c.id = :accessControlsId",
						Role.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("accessControlsId", accessControlsId)
				.getResultList();
	}
	/**
	 * Find by reference: menus
	 *
	 * @param menus
	 * @return List<Role>
	 */
	public List<Role> findByMenus(Menu menus) {
		return this.findByMenusId(menus.getId());
	}
	/**
	 * Find by ID of reference: menus.id
	 *
	 * @param menusId
	 * @return List<Role>
	 */
	public List<Role> findByMenusId(String menusId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Role e, IN (e.menus) c where e.clientId = :clientId and c.id = :menusId",
						Role.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("menusId", menusId).getResultList();
	}
	/**
	 * Find by reference: menuItems
	 *
	 * @param menuItems
	 * @return List<Role>
	 */
	public List<Role> findByMenuItems(MenuItem menuItems) {
		return this.findByMenuItemsId(menuItems.getId());
	}
	/**
	 * Find by ID of reference: menuItems.id
	 *
	 * @param menuItemsId
	 * @return List<Role>
	 */
	public List<Role> findByMenuItemsId(String menuItemsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Role e, IN (e.menuItems) c where e.clientId = :clientId and c.id = :menuItemsId",
						Role.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("menuItemsId", menuItemsId).getResultList();
	}
}
