/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.system;

import atraxo.ad.business.api.system.IJobService;
import atraxo.ad.domain.impl.system.Job;
import atraxo.ad.domain.impl.system.JobParam;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Job} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Job_Service extends AbstractEntityService<Job>
		implements
			IJobService {

	/**
	 * Public constructor for Job_Service
	 */
	public Job_Service() {
		super();
	}

	/**
	 * Public constructor for Job_Service
	 * 
	 * @param em
	 */
	public Job_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Job> getEntityClass() {
		return Job.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Job
	 */
	public Job findByName(String name) {
		try {
			return this.getEntityManager()
					.createNamedQuery(Job.NQ_FIND_BY_NAME, Job.class)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(), "Job",
							"name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Job", "name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Job
	 */
	public Job findByJclass(String javaClass) {
		try {
			return this.getEntityManager()
					.createNamedQuery(Job.NQ_FIND_BY_JCLASS, Job.class)
					.setParameter("javaClass", javaClass).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(), "Job",
							"javaClass"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Job", "javaClass"), nure);
		}
	}

	/**
	 * Find by reference: params
	 *
	 * @param params
	 * @return List<Job>
	 */
	public List<Job> findByParams(JobParam params) {
		return this.findByParamsId(params.getId());
	}
	/**
	 * Find by ID of reference: params.id
	 *
	 * @param paramsId
	 * @return List<Job>
	 */
	public List<Job> findByParamsId(String paramsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Job e, IN (e.params) c where  c.id = :paramsId",
						Job.class).setParameter("paramsId", paramsId)
				.getResultList();
	}
}
