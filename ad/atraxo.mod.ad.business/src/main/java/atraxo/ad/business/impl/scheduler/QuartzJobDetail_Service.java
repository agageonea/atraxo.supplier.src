/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IQuartzJobDetailService;
import atraxo.ad.domain.impl.scheduler.QuartzJobDetail;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link QuartzJobDetail} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class QuartzJobDetail_Service
		extends
			AbstractEntityService<QuartzJobDetail>
		implements
			IQuartzJobDetailService {

	/**
	 * Public constructor for QuartzJobDetail_Service
	 */
	public QuartzJobDetail_Service() {
		super();
	}

	/**
	 * Public constructor for QuartzJobDetail_Service
	 * 
	 * @param em
	 */
	public QuartzJobDetail_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<QuartzJobDetail> getEntityClass() {
		return QuartzJobDetail.class;
	}

}
