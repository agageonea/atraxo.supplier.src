/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.domain.impl.scheduler.JobContext;
import atraxo.ad.domain.impl.scheduler.JobTimer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link JobTimer} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class JobTimer_Service extends AbstractEntityService<JobTimer> {

	/**
	 * Public constructor for JobTimer_Service
	 */
	public JobTimer_Service() {
		super();
	}

	/**
	 * Public constructor for JobTimer_Service
	 * 
	 * @param em
	 */
	public JobTimer_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<JobTimer> getEntityClass() {
		return JobTimer.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return JobTimer
	 */
	public JobTimer findByName(JobContext jobContext, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(JobTimer.NQ_FIND_BY_NAME, JobTimer.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("jobContext", jobContext)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"JobTimer", "jobContext, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"JobTimer", "jobContext, name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return JobTimer
	 */
	public JobTimer findByName(Long jobContextId, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(JobTimer.NQ_FIND_BY_NAME_PRIMITIVE,
							JobTimer.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("jobContextId", jobContextId)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"JobTimer", "jobContextId, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"JobTimer", "jobContextId, name"), nure);
		}
	}

	/**
	 * Find by reference: jobContext
	 *
	 * @param jobContext
	 * @return List<JobTimer>
	 */
	public List<JobTimer> findByJobContext(JobContext jobContext) {
		return this.findByJobContextId(jobContext.getId());
	}
	/**
	 * Find by ID of reference: jobContext.id
	 *
	 * @param jobContextId
	 * @return List<JobTimer>
	 */
	public List<JobTimer> findByJobContextId(String jobContextId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from JobTimer e where e.clientId = :clientId and e.jobContext.id = :jobContextId",
						JobTimer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("jobContextId", jobContextId).getResultList();
	}
}
