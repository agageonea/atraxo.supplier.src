/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.security;

import atraxo.ad.business.api.security.IUserGroupService;
import atraxo.ad.domain.impl.security.User;
import atraxo.ad.domain.impl.security.UserGroup;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link UserGroup} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class UserGroup_Service extends AbstractEntityService<UserGroup>
		implements
			IUserGroupService {

	/**
	 * Public constructor for UserGroup_Service
	 */
	public UserGroup_Service() {
		super();
	}

	/**
	 * Public constructor for UserGroup_Service
	 * 
	 * @param em
	 */
	public UserGroup_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<UserGroup> getEntityClass() {
		return UserGroup.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return UserGroup
	 */
	public UserGroup findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(UserGroup.NQ_FIND_BY_CODE,
							UserGroup.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"UserGroup", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"UserGroup", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return UserGroup
	 */
	public UserGroup findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(UserGroup.NQ_FIND_BY_NAME,
							UserGroup.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"UserGroup", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"UserGroup", "name"), nure);
		}
	}

	/**
	 * Find by reference: users
	 *
	 * @param users
	 * @return List<UserGroup>
	 */
	public List<UserGroup> findByUsers(User users) {
		return this.findByUsersId(users.getId());
	}
	/**
	 * Find by ID of reference: users.id
	 *
	 * @param usersId
	 * @return List<UserGroup>
	 */
	public List<UserGroup> findByUsersId(String usersId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from UserGroup e, IN (e.users) c where e.clientId = :clientId and c.id = :usersId",
						UserGroup.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("usersId", usersId).getResultList();
	}
}
