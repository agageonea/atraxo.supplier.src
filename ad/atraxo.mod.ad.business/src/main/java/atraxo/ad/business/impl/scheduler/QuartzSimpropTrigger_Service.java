/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IQuartzSimpropTriggerService;
import atraxo.ad.domain.impl.scheduler.QuartzSimpropTrigger;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link QuartzSimpropTrigger} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class QuartzSimpropTrigger_Service
		extends
			AbstractEntityService<QuartzSimpropTrigger>
		implements
			IQuartzSimpropTriggerService {

	/**
	 * Public constructor for QuartzSimpropTrigger_Service
	 */
	public QuartzSimpropTrigger_Service() {
		super();
	}

	/**
	 * Public constructor for QuartzSimpropTrigger_Service
	 * 
	 * @param em
	 */
	public QuartzSimpropTrigger_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<QuartzSimpropTrigger> getEntityClass() {
		return QuartzSimpropTrigger.class;
	}

}
