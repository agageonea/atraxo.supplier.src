/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.system;

import atraxo.ad.business.api.system.IDataSourceRpcService;
import atraxo.ad.domain.impl.system.DataSource;
import atraxo.ad.domain.impl.system.DataSourceRpc;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link DataSourceRpc} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class DataSourceRpc_Service extends AbstractEntityService<DataSourceRpc>
		implements
			IDataSourceRpcService {

	/**
	 * Public constructor for DataSourceRpc_Service
	 */
	public DataSourceRpc_Service() {
		super();
	}

	/**
	 * Public constructor for DataSourceRpc_Service
	 * 
	 * @param em
	 */
	public DataSourceRpc_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<DataSourceRpc> getEntityClass() {
		return DataSourceRpc.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return DataSourceRpc
	 */
	public DataSourceRpc findByName(DataSource dataSource, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DataSourceRpc.NQ_FIND_BY_NAME,
							DataSourceRpc.class)
					.setParameter("dataSource", dataSource)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DataSourceRpc", "dataSource, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DataSourceRpc", "dataSource, name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return DataSourceRpc
	 */
	public DataSourceRpc findByName(Long dataSourceId, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DataSourceRpc.NQ_FIND_BY_NAME_PRIMITIVE,
							DataSourceRpc.class)
					.setParameter("dataSourceId", dataSourceId)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DataSourceRpc", "dataSourceId, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DataSourceRpc", "dataSourceId, name"), nure);
		}
	}

	/**
	 * Find by reference: dataSource
	 *
	 * @param dataSource
	 * @return List<DataSourceRpc>
	 */
	public List<DataSourceRpc> findByDataSource(DataSource dataSource) {
		return this.findByDataSourceId(dataSource.getId());
	}
	/**
	 * Find by ID of reference: dataSource.id
	 *
	 * @param dataSourceId
	 * @return List<DataSourceRpc>
	 */
	public List<DataSourceRpc> findByDataSourceId(String dataSourceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DataSourceRpc e where  e.dataSource.id = :dataSourceId",
						DataSourceRpc.class)
				.setParameter("dataSourceId", dataSourceId).getResultList();
	}
}
