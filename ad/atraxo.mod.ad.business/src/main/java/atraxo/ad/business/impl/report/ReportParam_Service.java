/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.report;

import atraxo.ad.business.api.report.IReportParamService;
import atraxo.ad.domain.impl.report.Report;
import atraxo.ad.domain.impl.report.ReportParam;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ReportParam} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ReportParam_Service extends AbstractEntityService<ReportParam>
		implements
			IReportParamService {

	/**
	 * Public constructor for ReportParam_Service
	 */
	public ReportParam_Service() {
		super();
	}

	/**
	 * Public constructor for ReportParam_Service
	 * 
	 * @param em
	 */
	public ReportParam_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ReportParam> getEntityClass() {
		return ReportParam.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ReportParam
	 */
	public ReportParam findByName(Report report, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ReportParam.NQ_FIND_BY_NAME,
							ReportParam.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("report", report).setParameter("name", name)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ReportParam", "report, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ReportParam", "report, name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return ReportParam
	 */
	public ReportParam findByName(Long reportId, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ReportParam.NQ_FIND_BY_NAME_PRIMITIVE,
							ReportParam.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("reportId", reportId)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ReportParam", "reportId, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ReportParam", "reportId, name"), nure);
		}
	}

	/**
	 * Find by reference: report
	 *
	 * @param report
	 * @return List<ReportParam>
	 */
	public List<ReportParam> findByReport(Report report) {
		return this.findByReportId(report.getId());
	}
	/**
	 * Find by ID of reference: report.id
	 *
	 * @param reportId
	 * @return List<ReportParam>
	 */
	public List<ReportParam> findByReportId(String reportId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ReportParam e where e.clientId = :clientId and e.report.id = :reportId",
						ReportParam.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("reportId", reportId).getResultList();
	}
}
