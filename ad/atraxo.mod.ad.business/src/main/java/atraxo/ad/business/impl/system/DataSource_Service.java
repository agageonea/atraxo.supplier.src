/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.system;

import atraxo.ad.business.api.system.IDataSourceService;
import atraxo.ad.domain.impl.system.DataSource;
import atraxo.ad.domain.impl.system.DataSourceField;
import atraxo.ad.domain.impl.system.DataSourceRpc;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link DataSource} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class DataSource_Service extends AbstractEntityService<DataSource>
		implements
			IDataSourceService {

	/**
	 * Public constructor for DataSource_Service
	 */
	public DataSource_Service() {
		super();
	}

	/**
	 * Public constructor for DataSource_Service
	 * 
	 * @param em
	 */
	public DataSource_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<DataSource> getEntityClass() {
		return DataSource.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return DataSource
	 */
	public DataSource findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DataSource.NQ_FIND_BY_NAME,
							DataSource.class).setParameter("name", name)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DataSource", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DataSource", "name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return DataSource
	 */
	public DataSource findByModel(String model) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DataSource.NQ_FIND_BY_MODEL,
							DataSource.class).setParameter("model", model)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DataSource", "model"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DataSource", "model"), nure);
		}
	}

	/**
	 * Find by reference: fields
	 *
	 * @param fields
	 * @return List<DataSource>
	 */
	public List<DataSource> findByFields(DataSourceField fields) {
		return this.findByFieldsId(fields.getId());
	}
	/**
	 * Find by ID of reference: fields.id
	 *
	 * @param fieldsId
	 * @return List<DataSource>
	 */
	public List<DataSource> findByFieldsId(String fieldsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from DataSource e, IN (e.fields) c where  c.id = :fieldsId",
						DataSource.class).setParameter("fieldsId", fieldsId)
				.getResultList();
	}
	/**
	 * Find by reference: serviceMethods
	 *
	 * @param serviceMethods
	 * @return List<DataSource>
	 */
	public List<DataSource> findByServiceMethods(DataSourceRpc serviceMethods) {
		return this.findByServiceMethodsId(serviceMethods.getId());
	}
	/**
	 * Find by ID of reference: serviceMethods.id
	 *
	 * @param serviceMethodsId
	 * @return List<DataSource>
	 */
	public List<DataSource> findByServiceMethodsId(String serviceMethodsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from DataSource e, IN (e.serviceMethods) c where  c.id = :serviceMethodsId",
						DataSource.class)
				.setParameter("serviceMethodsId", serviceMethodsId)
				.getResultList();
	}
}
