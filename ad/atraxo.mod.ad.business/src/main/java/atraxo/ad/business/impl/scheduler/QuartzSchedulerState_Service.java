/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IQuartzSchedulerStateService;
import atraxo.ad.domain.impl.scheduler.QuartzSchedulerState;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link QuartzSchedulerState} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class QuartzSchedulerState_Service
		extends
			AbstractEntityService<QuartzSchedulerState>
		implements
			IQuartzSchedulerStateService {

	/**
	 * Public constructor for QuartzSchedulerState_Service
	 */
	public QuartzSchedulerState_Service() {
		super();
	}

	/**
	 * Public constructor for QuartzSchedulerState_Service
	 * 
	 * @param em
	 */
	public QuartzSchedulerState_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<QuartzSchedulerState> getEntityClass() {
		return QuartzSchedulerState.class;
	}

}
