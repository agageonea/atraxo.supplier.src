/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IQuartzTriggerService;
import atraxo.ad.domain.impl.scheduler.QuartzTrigger;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link QuartzTrigger} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class QuartzTrigger_Service extends AbstractEntityService<QuartzTrigger>
		implements
			IQuartzTriggerService {

	/**
	 * Public constructor for QuartzTrigger_Service
	 */
	public QuartzTrigger_Service() {
		super();
	}

	/**
	 * Public constructor for QuartzTrigger_Service
	 * 
	 * @param em
	 */
	public QuartzTrigger_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<QuartzTrigger> getEntityClass() {
		return QuartzTrigger.class;
	}

}
