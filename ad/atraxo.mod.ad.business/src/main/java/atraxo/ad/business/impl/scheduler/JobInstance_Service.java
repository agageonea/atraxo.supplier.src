/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IJobInstanceService;
import atraxo.ad.domain.impl.scheduler.JobInstance;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link JobInstance} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class JobInstance_Service extends AbstractEntityService<JobInstance>
		implements
			IJobInstanceService {

	/**
	 * Public constructor for JobInstance_Service
	 */
	public JobInstance_Service() {
		super();
	}

	/**
	 * Public constructor for JobInstance_Service
	 * 
	 * @param em
	 */
	public JobInstance_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<JobInstance> getEntityClass() {
		return JobInstance.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return JobInstance
	 */
	public JobInstance findByUn(String jobName, String jobKey) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(JobInstance.NQ_FIND_BY_UN,
							JobInstance.class).setParameter("jobName", jobName)
					.setParameter("jobKey", jobKey).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"JobInstance", "jobName, jobKey"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"JobInstance", "jobName, jobKey"), nure);
		}
	}

}
