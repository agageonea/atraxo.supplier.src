/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.report;

import atraxo.ad.business.api.report.IDsReportService;
import atraxo.ad.domain.impl.report.DsReport;
import atraxo.ad.domain.impl.report.DsReportParam;
import atraxo.ad.domain.impl.report.Report;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link DsReport} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class DsReport_Service extends AbstractEntityService<DsReport>
		implements
			IDsReportService {

	/**
	 * Public constructor for DsReport_Service
	 */
	public DsReport_Service() {
		super();
	}

	/**
	 * Public constructor for DsReport_Service
	 * 
	 * @param em
	 */
	public DsReport_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<DsReport> getEntityClass() {
		return DsReport.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return DsReport
	 */
	public DsReport findByRep_ds(Report report, String dataSource) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DsReport.NQ_FIND_BY_REP_DS,
							DsReport.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("report", report)
					.setParameter("dataSource", dataSource).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DsReport", "report, dataSource"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DsReport", "report, dataSource"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return DsReport
	 */
	public DsReport findByRep_ds(Long reportId, String dataSource) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DsReport.NQ_FIND_BY_REP_DS_PRIMITIVE,
							DsReport.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("reportId", reportId)
					.setParameter("dataSource", dataSource).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DsReport", "reportId, dataSource"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DsReport", "reportId, dataSource"), nure);
		}
	}

	/**
	 * Find by reference: report
	 *
	 * @param report
	 * @return List<DsReport>
	 */
	public List<DsReport> findByReport(Report report) {
		return this.findByReportId(report.getId());
	}
	/**
	 * Find by ID of reference: report.id
	 *
	 * @param reportId
	 * @return List<DsReport>
	 */
	public List<DsReport> findByReportId(String reportId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DsReport e where e.clientId = :clientId and e.report.id = :reportId",
						DsReport.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("reportId", reportId).getResultList();
	}
	/**
	 * Find by reference: dsReportParams
	 *
	 * @param dsReportParams
	 * @return List<DsReport>
	 */
	public List<DsReport> findByDsReportParams(DsReportParam dsReportParams) {
		return this.findByDsReportParamsId(dsReportParams.getId());
	}
	/**
	 * Find by ID of reference: dsReportParams.id
	 *
	 * @param dsReportParamsId
	 * @return List<DsReport>
	 */
	public List<DsReport> findByDsReportParamsId(String dsReportParamsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from DsReport e, IN (e.dsReportParams) c where e.clientId = :clientId and c.id = :dsReportParamsId",
						DsReport.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("dsReportParamsId", dsReportParamsId)
				.getResultList();
	}
}
