/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IStepExecutionService;
import atraxo.ad.domain.impl.scheduler.JobExecution;
import atraxo.ad.domain.impl.scheduler.StepExecution;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link StepExecution} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class StepExecution_Service extends AbstractEntityService<StepExecution>
		implements
			IStepExecutionService {

	/**
	 * Public constructor for StepExecution_Service
	 */
	public StepExecution_Service() {
		super();
	}

	/**
	 * Public constructor for StepExecution_Service
	 * 
	 * @param em
	 */
	public StepExecution_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<StepExecution> getEntityClass() {
		return StepExecution.class;
	}

	/**
	 * Find by reference: jobExecStep
	 *
	 * @param jobExecStep
	 * @return List<StepExecution>
	 */
	public List<StepExecution> findByJobExecStep(JobExecution jobExecStep) {
		return this.findByJobExecStepId(jobExecStep.getId());
	}
	/**
	 * Find by ID of reference: jobExecStep.id
	 *
	 * @param jobExecStepId
	 * @return List<StepExecution>
	 */
	public List<StepExecution> findByJobExecStepId(Long jobExecStepId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from StepExecution e where  e.jobExecStep.id = :jobExecStepId",
						StepExecution.class)
				.setParameter("jobExecStepId", jobExecStepId).getResultList();
	}
}
