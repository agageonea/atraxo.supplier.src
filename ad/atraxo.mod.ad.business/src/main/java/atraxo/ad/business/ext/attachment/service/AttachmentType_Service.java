/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.ext.attachment.service;

import java.util.HashMap;
import java.util.Map;

import atraxo.ad.business.api.attachment.IAttachmentTypeService;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link AttachmentType} domain entity.
 */
public class AttachmentType_Service extends atraxo.ad.business.impl.attachment.AttachmentType_Service implements IAttachmentTypeService {
	private static final String EXTERNAL_LINK = "link";

	@Override
	public AttachmentType findExternalLink() throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("name", EXTERNAL_LINK);
		return this.findByUk(AttachmentType.NQ_FIND_BY_NAME, params);
	}
}
