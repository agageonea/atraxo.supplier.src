package atraxo.ad.business.ext.bpm;

import org.activiti.engine.impl.interceptor.Session;
import org.activiti.engine.impl.interceptor.SessionFactory;
import org.activiti.engine.impl.persistence.entity.GroupEntityManager;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.ad.business.api.security.IRoleService;
import atraxo.ad.business.api.security.IUserService;

public class ActivitiGroupManagerFactory implements SessionFactory {

	@Autowired
	private IUserService userService;

	@Autowired
	private IRoleService roleService;

	@Override
	public Class<?> getSessionType() {
		return GroupEntityManager.class;
	}

	@Override
	public Session openSession() {
		return new ActivitiGroupManager(this.userService, this.roleService);
	}

}
