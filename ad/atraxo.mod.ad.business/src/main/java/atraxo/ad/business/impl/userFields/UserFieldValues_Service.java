/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.userFields;

import atraxo.ad.business.api.userFields.IUserFieldValuesService;
import atraxo.ad.domain.impl.userFields.UserFieldValues;
import atraxo.ad.domain.impl.userFields.UserFields;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link UserFieldValues} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class UserFieldValues_Service
		extends
			AbstractEntityService<UserFieldValues>
		implements
			IUserFieldValuesService {

	/**
	 * Public constructor for UserFieldValues_Service
	 */
	public UserFieldValues_Service() {
		super();
	}

	/**
	 * Public constructor for UserFieldValues_Service
	 * 
	 * @param em
	 */
	public UserFieldValues_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<UserFieldValues> getEntityClass() {
		return UserFieldValues.class;
	}

	/**
	 * Find by reference: userField
	 *
	 * @param userField
	 * @return List<UserFieldValues>
	 */
	public List<UserFieldValues> findByUserField(UserFields userField) {
		return this.findByUserFieldId(userField.getId());
	}
	/**
	 * Find by ID of reference: userField.id
	 *
	 * @param userFieldId
	 * @return List<UserFieldValues>
	 */
	public List<UserFieldValues> findByUserFieldId(String userFieldId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from UserFieldValues e where e.clientId = :clientId and e.userField.id = :userFieldId",
						UserFieldValues.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("userFieldId", userFieldId).getResultList();
	}
}
