package atraxo.ad.business.ext.externalReport.xml;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.util.StringUtils;
import org.w3c.dom.Document;

import seava.j4e.api.exceptions.BusinessException;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.ExternalReportParameter;

/**
 * @author apetho
 */
public class ReportBuilder {

	private static final String CDATA_ELEMENT = "url";

	/**
	 * @param format - {@link String} The report format.
	 * @param externalReport - {@link ExternalReport} The used report.
	 * @param paramMap - The parameters to the report.
	 * @return - {@link String} The generated xml.
	 * @throws BusinessException
	 * @throws UnsupportedEncodingException
	 * @throws JAXBException
	 * @throws ParserConfigurationException
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerException
	 */
	public static String buildSpecialReport(String format, ExternalReport externalReport, Map<String, Object> paramMap) throws BusinessException,
			UnsupportedEncodingException, JAXBException, ParserConfigurationException, TransformerFactoryConfigurationError, TransformerException {
		SpecialReport specialReport = getSpecialReport(format, externalReport, paramMap);
		return buildSpecialReportXml(specialReport);
	}

	private static String buildSpecialReportXml(SpecialReport specialReport) throws JAXBException, ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException, UnsupportedEncodingException {
		JAXBContext jaxbContext = JAXBContext.newInstance(SpecialReport.class, Parameter.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		Document document = docBuilderFactory.newDocumentBuilder().newDocument();

		jaxbMarshaller.marshal(specialReport, document);

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer nullTransformer = transformerFactory.newTransformer();
		nullTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
		nullTransformer.setOutputProperty(OutputKeys.CDATA_SECTION_ELEMENTS, CDATA_ELEMENT);
		nullTransformer.transform(new DOMSource(document), new StreamResult(os));

		return os.toString(StandardCharsets.UTF_8.name());
	}

	public static DialogReport buildDialogReport(String format, ExternalReport externalReport, Map<String, Object> paramMap) throws BusinessException {
		String reportName = externalReport.getDesignName().split("\\.")[0];
		DialogReport dr = new DialogReport();
		dr.setCode(reportName);
		dr.setFormat(format);
		dr.setTitle(reportName);
		dr.setParamList(buildParamterList(externalReport, paramMap));
		return dr;
	}

	private static SpecialReport getSpecialReport(String format, ExternalReport externalReport, Map<String, Object> paramMap)
			throws BusinessException {
		String reportName = externalReport.getDesignName().split("\\.")[0];
		SpecialReport specialReport = new SpecialReport();
		specialReport.setCode(externalReport.getReportCode());
		specialReport.setFormat(format);
		specialReport.setTitle(reportName);
		specialReport.setParamList(buildParamterList(externalReport, paramMap));
		return specialReport;
	}

	private static List<Parameter> buildParamterList(ExternalReport externalReport, Map<String, Object> paramMap) {
		List<Parameter> retList = new ArrayList<>();
		for (ExternalReportParameter erp : externalReport.getParams()) {
			Parameter param = new Parameter();
			param.setName(erp.getName());
			param.setType(erp.getType().getName());
			Object pv = null;
			if (paramMap != null && !StringUtils.isEmpty(erp.getDsFieldName()) && paramMap.containsKey(erp.getDsFieldName())) {
				pv = paramMap.get(erp.getDsFieldName());
			}
			if (null != pv) {
				param.setValue(pv.toString());
			} else {
				param.setValue(erp.getParamValue() == null || erp.getParamValue().isEmpty() ? erp.getDefaultValue() : erp.getParamValue());
			}
			retList.add(param);
		}
		return retList;
	}
}
