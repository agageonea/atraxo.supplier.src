/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IQuartzCalendarService;
import atraxo.ad.domain.impl.scheduler.QuartzCalendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link QuartzCalendar} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class QuartzCalendar_Service
		extends
			AbstractEntityService<QuartzCalendar>
		implements
			IQuartzCalendarService {

	/**
	 * Public constructor for QuartzCalendar_Service
	 */
	public QuartzCalendar_Service() {
		super();
	}

	/**
	 * Public constructor for QuartzCalendar_Service
	 * 
	 * @param em
	 */
	public QuartzCalendar_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<QuartzCalendar> getEntityClass() {
		return QuartzCalendar.class;
	}

}
