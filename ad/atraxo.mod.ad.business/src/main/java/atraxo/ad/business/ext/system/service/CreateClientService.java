package atraxo.ad.business.ext.system.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import atraxo.ad.business.api.system.IClientService;
import atraxo.ad.domain.impl.system.Client;
import seava.j4e.api.Constants;
import seava.j4e.api.ISettings;
import seava.j4e.api.action.impex.ICreateClient;
import seava.j4e.api.action.impex.IImportDataPackage;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.exceptions.InvalidConfiguration;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.IUserProfile;
import seava.j4e.api.session.IWorkspace;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.AbstractBusinessBaseService;
import seava.j4e.commons.action.impex.DataPackage;
import seava.j4e.commons.security.AppClient;
import seava.j4e.commons.security.AppUser;
import seava.j4e.commons.security.AppUserProfile;
import seava.j4e.commons.security.AppUserSettings;
import seava.j4e.commons.security.AppWorkspace;

/**
 * @author apetho
 */
public class CreateClientService extends AbstractBusinessBaseService implements ICreateClient {

	private static final String DESCRIPTION = "description";

	private static final String TIME = "time";

	private static final String DATE_FORMAT = "yyyy.MM.dd hh.mm.ss";

	private static final Logger LOGGER = LoggerFactory.getLogger(CreateClientService.class);

	private static final String SYS = "SYS";
	private static final String CONNECT = "CONNECT";
	private static final String ADMIN = "ADMIN";
	private static final String WORKSPACE_PATH = "../../app/storage";
	private static final String IMPORT_PATH = "/import";
	private static final String EXPORT_PATH = "/export";
	private static final String TEMP_PATH = "/temp";

	@Autowired
	private IClientService clientService;
	@Autowired
	private ISettings settings;

	@Override
	@Transactional
	@Async
	public void newClient(String clientName, String clientCode, String adminUserCode, String adminUserName, String adminLoginName,
			String adminLoginPasword, String callbackUrl) throws Exception {

		Client client = new Client();
		client.setActive(false);
		client.setCode(clientCode);
		client.setName(clientName);
		client.setExportPath(WORKSPACE_PATH + EXPORT_PATH);
		client.setWorkspacePath(WORKSPACE_PATH);
		client.setImportPath(WORKSPACE_PATH + IMPORT_PATH);
		client.setTempPath(WORKSPACE_PATH + TEMP_PATH);
		client.setVersion((long) 1);
		client.setCreatedAt(new Date());
		client.setCreatedBy(SYS);
		client.setModifiedAt(new Date());
		client.setModifiedBy(SYS);

		this.buildSession();

		IImportDataPackage dataPackage = DataPackage.forIndexFile(this.settings.getInitFileLocation());

		try {
			this.clientService.doInsertWithUserAccountAndSetup(client, adminUserCode, adminUserName, adminLoginName, adminLoginPasword, dataPackage);
			this.respondToClient(callbackUrl, true, "", clientCode, adminUserName);
		} catch (BusinessException be) {
			LOGGER.warn("Create client error:" + clientCode, be);
			this.respondToClient(callbackUrl, false, be.getMessage(), clientCode, adminUserName);
		}
	}

	private void respondToClient(String callbackUrl, boolean success, String errorMessage, String clientCode, String adminUserName)
			throws BusinessException {
		if (StringUtils.isEmpty(callbackUrl)) {
			return;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		JSONObject callJson = new JSONObject();
		callJson.put(TIME, sdf.format(new Date()));
		callJson.put(Constants.STATUS, success ? "SUCCESS" : "FAILED");
		callJson.put(DESCRIPTION, errorMessage);
		callJson.put(Constants.REQUEST_PARAM_CLIENT_CODE, clientCode);
		callJson.put(Constants.REQUEST_PARAM_ADMIN_USER_NAME, adminUserName);

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(callbackUrl);

		StringEntity requestEntity = new StringEntity(callJson.toString(), ContentType.APPLICATION_JSON);
		post.setEntity(requestEntity);

		try {
			HttpResponse response = client.execute(post);
			if (response.getStatusLine().getStatusCode() == 200) {

			} else {
				throw new BusinessException(ErrorCode.COMMUNICATION_ERROR, ErrorCode.COMMUNICATION_ERROR.getErrMsg());
			}
		} catch (IOException e) {
			LOGGER.warn("Communication error", e);
			throw new BusinessException(ErrorCode.COMMUNICATION_ERROR, ErrorCode.COMMUNICATION_ERROR.getErrMsg());
		}
	}

	private void buildSession() throws InvalidConfiguration {
		IWorkspace ws = new AppWorkspace(WORKSPACE_PATH, IMPORT_PATH, EXPORT_PATH, TEMP_PATH);

		AppUserSettings settings = AppUserSettings.newInstance(this.getApplicationContext().getBean(ISettings.class));
		AppClient clientSYS = new AppClient(null, null, null);

		IUserProfile profile = new AppUserProfile(true, Arrays.asList(ADMIN, CONNECT), false, false, false);
		IUser user = new AppUser(SYS, SYS, SYS, SYS, null, null, clientSYS, settings, profile, ws, false);

		Session.user.set(user);
	}

}
