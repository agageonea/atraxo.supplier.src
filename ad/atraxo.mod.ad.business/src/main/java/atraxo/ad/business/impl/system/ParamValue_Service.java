/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.system;

import atraxo.ad.business.api.system.IParamValueService;
import atraxo.ad.domain.impl.system.ParamValue;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ParamValue} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ParamValue_Service extends AbstractEntityService<ParamValue>
		implements
			IParamValueService {

	/**
	 * Public constructor for ParamValue_Service
	 */
	public ParamValue_Service() {
		super();
	}

	/**
	 * Public constructor for ParamValue_Service
	 * 
	 * @param em
	 */
	public ParamValue_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ParamValue> getEntityClass() {
		return ParamValue.class;
	}

}
