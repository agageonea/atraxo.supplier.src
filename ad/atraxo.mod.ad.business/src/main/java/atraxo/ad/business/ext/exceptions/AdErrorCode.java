package atraxo.ad.business.ext.exceptions;

import java.util.Locale;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import seava.j4e.api.exceptions.IErrorCode;
import seava.j4e.api.session.Session;

public enum AdErrorCode implements IErrorCode {

	REPORT_GENERATOR_PROBLEM(7000, "%s", "Error"),
	INTERFACE_NOTIFICATION_SUCCESS(7001, "%s was completed successfully.", "Error"),
	INTERFACE_NOTIFICATION_FAILURE(7002, "%s failed. Check report result for details.", "Error"),
	THE_FILE_IS_INVALID(7003, "The uploaded file is invalid.", "Error"),
	REPORT_NOT_FOUND(7004, "Report with code %s, not found!", "Error"),
	INVALID_REPORT_PARAMETER(7005, "Invalid report parameters: %s", "Error"),
	GENERAL_REPORT_CENTER_ERROR(7006, "Error occured during report generation. Please contact application administrator.", "Error"),
	REPORT_CODE_MUST_BE_PRESENT_IN_REPORT(7007, "The report code is missing.", "Error"),

	INVALID_DATASOURCE(7010, "Invalid datasource with name %s. Please contact system administrator.", "Error"),

	INVALID_USER_NAME(7030, "User name: %s, not found", "Error"),

	FIELD_ALREADY_EXISTS(7020, "Field with name: %s already exists for business object %s.", "Error");

	private final Logger LOGGER = LoggerFactory.getLogger(AdErrorCode.class);

	private ResourceBundle boundle;

	private final int errNo;
	private final String errMsg;
	private final String errGroup;

	private AdErrorCode(int errNo, String errMsg, String errGroup) {
		this.errNo = errNo;
		this.errMsg = errMsg;
		this.errGroup = errGroup;
	}

	@Override
	public String getErrGroup() {
		return this.errGroup;
	}

	@Override
	public int getErrNo() {
		return this.errNo;
	}

	@Override
	public String getErrMsg() {
		try {
			String language = Session.user.get().getSettings().getLanguage();
			if (language.equals("sys")) {
				this.boundle = ResourceBundle.getBundle("locale.sone.ad.error.messages");
			} else {
				this.boundle = ResourceBundle.getBundle("locale.sone.ad.error.messages", new Locale(language));
			}
		} catch (NullPointerException e) {
			this.LOGGER.trace("No session while testing", e);
			this.boundle = ResourceBundle.getBundle("locale.sone.ad.error.messages");
		}
		return this.boundle.containsKey(Integer.toString(this.errNo)) ? this.boundle.getString(Integer.toString(this.errNo)) : this.errMsg;
	}

}
