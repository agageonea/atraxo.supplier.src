/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IJobSeqService;
import atraxo.ad.domain.impl.scheduler.JobSeq;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link JobSeq} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class JobSeq_Service extends AbstractEntityService<JobSeq>
		implements
			IJobSeqService {

	/**
	 * Public constructor for JobSeq_Service
	 */
	public JobSeq_Service() {
		super();
	}

	/**
	 * Public constructor for JobSeq_Service
	 * 
	 * @param em
	 */
	public JobSeq_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<JobSeq> getEntityClass() {
		return JobSeq.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return JobSeq
	 */
	public JobSeq findByUn(String uniqueKey) {
		try {
			return this.getEntityManager()
					.createNamedQuery(JobSeq.NQ_FIND_BY_UN, JobSeq.class)
					.setParameter("uniqueKey", uniqueKey).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"JobSeq", "uniqueKey"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"JobSeq", "uniqueKey"), nure);
		}
	}

}
