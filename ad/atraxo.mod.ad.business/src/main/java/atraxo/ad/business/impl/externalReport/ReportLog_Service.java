/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.externalReport;

import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.ReportLog;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ReportLog} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ReportLog_Service extends AbstractEntityService<ReportLog> {

	/**
	 * Public constructor for ReportLog_Service
	 */
	public ReportLog_Service() {
		super();
	}

	/**
	 * Public constructor for ReportLog_Service
	 * 
	 * @param em
	 */
	public ReportLog_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ReportLog> getEntityClass() {
		return ReportLog.class;
	}

	/**
	 * Find by reference: externalReport
	 *
	 * @param externalReport
	 * @return List<ReportLog>
	 */
	public List<ReportLog> findByExternalReport(ExternalReport externalReport) {
		return this.findByExternalReportId(externalReport.getId());
	}
	/**
	 * Find by ID of reference: externalReport.id
	 *
	 * @param externalReportId
	 * @return List<ReportLog>
	 */
	public List<ReportLog> findByExternalReportId(String externalReportId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ReportLog e where e.clientId = :clientId and e.externalReport.id = :externalReportId",
						ReportLog.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("externalReportId", externalReportId)
				.getResultList();
	}
}
