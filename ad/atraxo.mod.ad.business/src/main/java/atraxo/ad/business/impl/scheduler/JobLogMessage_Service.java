/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IJobLogMessageService;
import atraxo.ad.domain.impl.scheduler.JobLog;
import atraxo.ad.domain.impl.scheduler.JobLogMessage;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link JobLogMessage} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class JobLogMessage_Service extends AbstractEntityService<JobLogMessage>
		implements
			IJobLogMessageService {

	/**
	 * Public constructor for JobLogMessage_Service
	 */
	public JobLogMessage_Service() {
		super();
	}

	/**
	 * Public constructor for JobLogMessage_Service
	 * 
	 * @param em
	 */
	public JobLogMessage_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<JobLogMessage> getEntityClass() {
		return JobLogMessage.class;
	}

	/**
	 * Find by reference: jobLog
	 *
	 * @param jobLog
	 * @return List<JobLogMessage>
	 */
	public List<JobLogMessage> findByJobLog(JobLog jobLog) {
		return this.findByJobLogId(jobLog.getId());
	}
	/**
	 * Find by ID of reference: jobLog.id
	 *
	 * @param jobLogId
	 * @return List<JobLogMessage>
	 */
	public List<JobLogMessage> findByJobLogId(String jobLogId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from JobLogMessage e where e.clientId = :clientId and e.jobLog.id = :jobLogId",
						JobLogMessage.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("jobLogId", jobLogId).getResultList();
	}
}
