/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.ext.businessArea.service;

import java.lang.annotation.Annotation;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.ad.business.api.businessArea.IBusinessAreaService;
import atraxo.ad.business.api.system.IDataSourceService;
import atraxo.ad.business.ext.exceptions.AdErrorCode;
import atraxo.ad.domain.impl.businessArea.BusinessArea;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link BusinessArea} domain entity.
 */
public class BusinessArea_Service extends atraxo.ad.business.impl.businessArea.BusinessArea_Service implements IBusinessAreaService {

	@Autowired
	private IDataSourceService dsSrv;

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.business.service.entity.AbstractEntityWriteService#preInsert(java.lang.Object)
	 */
	@Override
	protected void preInsert(BusinessArea e) throws BusinessException {
		super.preInsert(e);
		this.stDsAlias(e);
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.business.service.entity.AbstractEntityWriteService#preUpdate(java.lang.Object)
	 */
	@Override
	protected void preUpdate(BusinessArea e) throws BusinessException {
		super.preUpdate(e);
		this.stDsAlias(e);
	}

	private void stDsAlias(BusinessArea e) throws BusinessException {
		String model = this.dsSrv.findByName(e.getDsName()).getModel();
		try {
			Annotation[] annotations = Class.forName(model).getAnnotations();
			for (Annotation annotation : annotations) {
				if (annotation instanceof Ds) {
					Ds dsAnnotation = (Ds) annotation;
					e.setDsAlias(dsAnnotation.entity().getSimpleName());
				}
			}
		} catch (ClassNotFoundException cnfe) {
			throw new BusinessException(AdErrorCode.INVALID_DATASOURCE, AdErrorCode.INVALID_DATASOURCE.getErrMsg(), cnfe);
		}
	}
}
