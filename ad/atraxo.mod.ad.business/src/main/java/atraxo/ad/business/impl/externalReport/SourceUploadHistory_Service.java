/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.externalReport;

import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.SourceUploadHistory;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link SourceUploadHistory} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class SourceUploadHistory_Service
		extends
			AbstractEntityService<SourceUploadHistory> {

	/**
	 * Public constructor for SourceUploadHistory_Service
	 */
	public SourceUploadHistory_Service() {
		super();
	}

	/**
	 * Public constructor for SourceUploadHistory_Service
	 * 
	 * @param em
	 */
	public SourceUploadHistory_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<SourceUploadHistory> getEntityClass() {
		return SourceUploadHistory.class;
	}

	/**
	 * Find by reference: externalReport
	 *
	 * @param externalReport
	 * @return List<SourceUploadHistory>
	 */
	public List<SourceUploadHistory> findByExternalReport(
			ExternalReport externalReport) {
		return this.findByExternalReportId(externalReport.getId());
	}
	/**
	 * Find by ID of reference: externalReport.id
	 *
	 * @param externalReportId
	 * @return List<SourceUploadHistory>
	 */
	public List<SourceUploadHistory> findByExternalReportId(
			String externalReportId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from SourceUploadHistory e where e.clientId = :clientId and e.externalReport.id = :externalReportId",
						SourceUploadHistory.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("externalReportId", externalReportId)
				.getResultList();
	}
}
