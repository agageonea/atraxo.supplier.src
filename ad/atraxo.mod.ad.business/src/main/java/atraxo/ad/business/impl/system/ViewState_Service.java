/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.system;

import atraxo.ad.business.api.system.IViewStateService;
import atraxo.ad.domain.impl.system.ViewState;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ViewState} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ViewState_Service extends AbstractEntityService<ViewState>
		implements
			IViewStateService {

	/**
	 * Public constructor for ViewState_Service
	 */
	public ViewState_Service() {
		super();
	}

	/**
	 * Public constructor for ViewState_Service
	 * 
	 * @param em
	 */
	public ViewState_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ViewState> getEntityClass() {
		return ViewState.class;
	}

}
