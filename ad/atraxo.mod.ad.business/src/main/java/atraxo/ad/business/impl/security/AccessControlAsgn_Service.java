/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.security;

import atraxo.ad.business.api.security.IAccessControlAsgnService;
import atraxo.ad.domain.impl.security.AccessControl;
import atraxo.ad.domain.impl.security.AccessControlAsgn;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link AccessControlAsgn} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class AccessControlAsgn_Service
		extends
			AbstractEntityService<AccessControlAsgn>
		implements
			IAccessControlAsgnService {

	/**
	 * Public constructor for AccessControlAsgn_Service
	 */
	public AccessControlAsgn_Service() {
		super();
	}

	/**
	 * Public constructor for AccessControlAsgn_Service
	 * 
	 * @param em
	 */
	public AccessControlAsgn_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<AccessControlAsgn> getEntityClass() {
		return AccessControlAsgn.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return AccessControlAsgn
	 */
	public AccessControlAsgn findByUnique(AccessControl accessControl,
			String asgnName) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(AccessControlAsgn.NQ_FIND_BY_UNIQUE,
							AccessControlAsgn.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("accessControl", accessControl)
					.setParameter("asgnName", asgnName).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"AccessControlAsgn", "accessControl, asgnName"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"AccessControlAsgn", "accessControl, asgnName"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return AccessControlAsgn
	 */
	public AccessControlAsgn findByUnique(Long accessControlId, String asgnName) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							AccessControlAsgn.NQ_FIND_BY_UNIQUE_PRIMITIVE,
							AccessControlAsgn.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("accessControlId", accessControlId)
					.setParameter("asgnName", asgnName).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"AccessControlAsgn", "accessControlId, asgnName"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"AccessControlAsgn", "accessControlId, asgnName"),
					nure);
		}
	}

	/**
	 * Find by reference: accessControl
	 *
	 * @param accessControl
	 * @return List<AccessControlAsgn>
	 */
	public List<AccessControlAsgn> findByAccessControl(
			AccessControl accessControl) {
		return this.findByAccessControlId(accessControl.getId());
	}
	/**
	 * Find by ID of reference: accessControl.id
	 *
	 * @param accessControlId
	 * @return List<AccessControlAsgn>
	 */
	public List<AccessControlAsgn> findByAccessControlId(String accessControlId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from AccessControlAsgn e where e.clientId = :clientId and e.accessControl.id = :accessControlId",
						AccessControlAsgn.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("accessControlId", accessControlId)
				.getResultList();
	}
}
