/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.business.api.scheduler.IJobLogService;
import atraxo.ad.domain.impl.scheduler.JobContext;
import atraxo.ad.domain.impl.scheduler.JobLog;
import atraxo.ad.domain.impl.scheduler.JobLogMessage;
import atraxo.ad.domain.impl.scheduler.JobTimer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link JobLog} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class JobLog_Service extends AbstractEntityService<JobLog>
		implements
			IJobLogService {

	/**
	 * Public constructor for JobLog_Service
	 */
	public JobLog_Service() {
		super();
	}

	/**
	 * Public constructor for JobLog_Service
	 * 
	 * @param em
	 */
	public JobLog_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<JobLog> getEntityClass() {
		return JobLog.class;
	}

	/**
	 * Find by reference: jobContext
	 *
	 * @param jobContext
	 * @return List<JobLog>
	 */
	public List<JobLog> findByJobContext(JobContext jobContext) {
		return this.findByJobContextId(jobContext.getId());
	}
	/**
	 * Find by ID of reference: jobContext.id
	 *
	 * @param jobContextId
	 * @return List<JobLog>
	 */
	public List<JobLog> findByJobContextId(String jobContextId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from JobLog e where e.clientId = :clientId and e.jobContext.id = :jobContextId",
						JobLog.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("jobContextId", jobContextId).getResultList();
	}
	/**
	 * Find by reference: jobTimer
	 *
	 * @param jobTimer
	 * @return List<JobLog>
	 */
	public List<JobLog> findByJobTimer(JobTimer jobTimer) {
		return this.findByJobTimerId(jobTimer.getId());
	}
	/**
	 * Find by ID of reference: jobTimer.id
	 *
	 * @param jobTimerId
	 * @return List<JobLog>
	 */
	public List<JobLog> findByJobTimerId(String jobTimerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from JobLog e where e.clientId = :clientId and e.jobTimer.id = :jobTimerId",
						JobLog.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("jobTimerId", jobTimerId).getResultList();
	}
	/**
	 * Find by reference: messages
	 *
	 * @param messages
	 * @return List<JobLog>
	 */
	public List<JobLog> findByMessages(JobLogMessage messages) {
		return this.findByMessagesId(messages.getId());
	}
	/**
	 * Find by ID of reference: messages.id
	 *
	 * @param messagesId
	 * @return List<JobLog>
	 */
	public List<JobLog> findByMessagesId(String messagesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from JobLog e, IN (e.messages) c where e.clientId = :clientId and c.id = :messagesId",
						JobLog.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("messagesId", messagesId).getResultList();
	}
}
