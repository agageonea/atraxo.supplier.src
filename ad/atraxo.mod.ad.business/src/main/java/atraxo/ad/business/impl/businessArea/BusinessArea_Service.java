/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.businessArea;

import atraxo.ad.domain.impl.businessArea.BusinessArea;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link BusinessArea} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class BusinessArea_Service extends AbstractEntityService<BusinessArea> {

	/**
	 * Public constructor for BusinessArea_Service
	 */
	public BusinessArea_Service() {
		super();
	}

	/**
	 * Public constructor for BusinessArea_Service
	 * 
	 * @param em
	 */
	public BusinessArea_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<BusinessArea> getEntityClass() {
		return BusinessArea.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return BusinessArea
	 */
	public BusinessArea findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(BusinessArea.NQ_FIND_BY_NAME,
							BusinessArea.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"BusinessArea", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"BusinessArea", "name"), nure);
		}
	}

}
