/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.ext.scheduler.service;

import java.util.ArrayList;
import java.util.List;

import atraxo.ad.business.api.scheduler.IJobContextParamService;
import atraxo.ad.business.ext.scheduler.delegate.JobScheduler_Bd;
import atraxo.ad.domain.impl.scheduler.JobContext;
import atraxo.ad.domain.impl.scheduler.JobContextParam;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link JobContextParam} domain entity.
 */
public class JobContextParam_Service extends atraxo.ad.business.impl.scheduler.JobContextParam_Service implements IJobContextParamService {

	@Override
	protected void postUpdate(List<JobContextParam> list) throws BusinessException {
		List<JobContext> jobContexts = new ArrayList<>();
		for (JobContextParam o : list) {
			if (!jobContexts.contains(o.getJobContext())) {
				jobContexts.add(o.getJobContext());
			}
		}
		this.getBusinessDelegate(JobScheduler_Bd.class).createQuartzJob(jobContexts);
	}

	@Override
	protected void postInsert(List<JobContextParam> list) throws BusinessException {
		List<JobContext> jobContexts = new ArrayList<>();
		for (JobContextParam o : list) {
			if (!jobContexts.contains(o.getJobContext())) {
				jobContexts.add(o.getJobContext());
			}
		}
		this.getBusinessDelegate(JobScheduler_Bd.class).createQuartzJob(jobContexts);
	}

}
