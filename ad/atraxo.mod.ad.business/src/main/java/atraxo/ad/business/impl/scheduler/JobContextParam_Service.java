/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.business.impl.scheduler;

import atraxo.ad.domain.impl.scheduler.JobContext;
import atraxo.ad.domain.impl.scheduler.JobContextParam;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link JobContextParam} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class JobContextParam_Service
		extends
			AbstractEntityService<JobContextParam> {

	/**
	 * Public constructor for JobContextParam_Service
	 */
	public JobContextParam_Service() {
		super();
	}

	/**
	 * Public constructor for JobContextParam_Service
	 * 
	 * @param em
	 */
	public JobContextParam_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<JobContextParam> getEntityClass() {
		return JobContextParam.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return JobContextParam
	 */
	public JobContextParam findByName(JobContext jobContext, String paramName) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(JobContextParam.NQ_FIND_BY_NAME,
							JobContextParam.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("jobContext", jobContext)
					.setParameter("paramName", paramName).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"JobContextParam", "jobContext, paramName"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"JobContextParam", "jobContext, paramName"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return JobContextParam
	 */
	public JobContextParam findByName(Long jobContextId, String paramName) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							JobContextParam.NQ_FIND_BY_NAME_PRIMITIVE,
							JobContextParam.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("jobContextId", jobContextId)
					.setParameter("paramName", paramName).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"JobContextParam", "jobContextId, paramName"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"JobContextParam", "jobContextId, paramName"), nure);
		}
	}

	/**
	 * Find by reference: jobContext
	 *
	 * @param jobContext
	 * @return List<JobContextParam>
	 */
	public List<JobContextParam> findByJobContext(JobContext jobContext) {
		return this.findByJobContextId(jobContext.getId());
	}
	/**
	 * Find by ID of reference: jobContext.id
	 *
	 * @param jobContextId
	 * @return List<JobContextParam>
	 */
	public List<JobContextParam> findByJobContextId(String jobContextId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from JobContextParam e where e.clientId = :clientId and e.jobContext.id = :jobContextId",
						JobContextParam.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("jobContextId", jobContextId).getResultList();
	}
}
