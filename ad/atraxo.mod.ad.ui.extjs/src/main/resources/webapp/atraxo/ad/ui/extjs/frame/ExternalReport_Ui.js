/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.ExternalReport_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.ExternalReport_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("externalReport", Ext.create(atraxo.ad.ui.extjs.dc.ExternalReport_Dc,{multiEdit: true}))
		.addDc("externalReportParameters", Ext.create(atraxo.ad.ui.extjs.dc.ExternalReportParameters_Dc,{multiEdit: true}))
		.addDc("externalReportSourceUploadHistory", Ext.create(atraxo.ad.ui.extjs.dc.SourceUploadHistory_Dc,{}))
		.addDc("attachment", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("reportLog", Ext.create(atraxo.ad.ui.extjs.dc.ReportLog_Dc,{}))
		.linkDc("externalReportParameters", "externalReport",{fetchMode:"auto",fields:[
					{childField:"externalReportId", parentField:"id"}]})
				.linkDc("externalReportSourceUploadHistory", "externalReport",{fetchMode:"auto",fields:[
					{childField:"externalReportId", parentField:"id"}]})
				.linkDc("attachment", "externalReport",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}]})
				.linkDc("reportLog", "externalReport",{fetchMode:"auto",fields:[
					{childField:"externalReportId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnUploadReportTemplate",glyph:fp_asc.upload_glyph.glyph, disabled:true, handler: this.onBtnUploadReportTemplate,stateManager:[{ name:"selected_one_clean", dc:"externalReport", and: function(dc) {return (this.canUploadReportTemplate(dc));} }], scope:this})
		.addButton({name:"btnRunReportTemplate",glyph:fp_asc.flash_glyph.glyph,iconCls: fp_asc.flash_glyph.css, disabled:true, handler: this.onBtnRunReportTemplate,stateManager:[{ name:"selected_one_clean", dc:"externalReport", and: function(dc) {return (this.canRunReport(dc));} }], scope:this})
		.addButton({name:"btnDeleteHistory",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteHistory,stateManager:[{ name:"selected_one_clean", dc:"externalReportSourceUploadHistory"}], scope:this})
		.addButton({name:"btnEmptyHistory",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnEmptyHistory,stateManager:[{ name:"list_not_empty", dc:"externalReportSourceUploadHistory"}], scope:this})
		.addButton({name:"btnEmptyLog",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnEmptyLog, scope:this})
		.addButton({name:"btnRunReport",glyph:fp_asc.flash_glyph.glyph,iconCls: fp_asc.flash_glyph.css, disabled:false, handler: this.onBtnRunReport, scope:this})
		.addButton({name:"btnCancelRunReport",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRunReport, scope:this})
		.addButton({name:"btnViewLog",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnViewLog, scope:this})
		.addButton({name:"btnCloseLog",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseLog, scope:this})
		.addButton({name:"btnSelectFile",glyph:fp_asc.export_glyph.glyph, disabled:false, handler: this.onBtnSelectFile, scope:this})
		.addButton({name:"btnCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancel, scope:this})
		.addButton({name:"btnHelp",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onBtnHelp, scope:this})
		.addDcEditGridView("externalReport", {name:"externalReportList", height:300, xtype:"ad_ExternalReport_Dc$List", frame:true})
		.addDcFilterFormView("externalReport", {name:"externalReportFilter", xtype:"ad_ExternalReport_Dc$Filter"})
		.addDcEditGridView("externalReportParameters", {name:"externalReportParameterList", _hasTitle_:true, height:350, xtype:"ad_ExternalReportParameters_Dc$List", frame:true})
		.addDcFilterFormView("externalReportSourceUploadHistory", {name:"historySourceUploadFilter", xtype:"ad_SourceUploadHistory_Dc$Filter"})
		.addDcGridView("externalReportSourceUploadHistory", {name:"historySourceUploadList", _hasTitle_:true, height:350, xtype:"ad_SourceUploadHistory_Dc$List"})
		.addDcFormView("externalReportSourceUploadHistory", {name:"historySourceNew", xtype:"ad_SourceUploadHistory_Dc$New"})
		.addDcGridView("reportLog", {name:"reportLogList", _hasTitle_:true, height:350, xtype:"ad_ReportLog_Dc$List"})
		.addDcFormView("externalReport", {name:"externalReportFormat", xtype:"ad_ExternalReport_Dc$FormatForm"})
		.addDcFormView("reportLog", {name:"externalReportRunLog", xtype:"ad_ReportLog_Dc$LogForm"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", height:300, xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addWindow({name:"wdwReportFormat", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("externalReportFormat")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnRunReport"), this._elems_.get("btnCancelRunReport")]}]})
		.addWindow({name:"wdwLog", _hasTitle_:true, width:725, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("externalReportRunLog")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCloseLog")]}]})
		.addWindow({name:"wdwNew", _hasTitle_:true, width:370, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("historySourceNew")],  listeners:{ close:{fn:this.doCancelNewRptDesign, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSelectFile"), this._elems_.get("btnCancel")]}]})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas1", dcName:"externalReportParameters"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["externalReportList", "detailsTab"], ["center", "south"])
		.addChildrenTo("detailsTab", ["externalReportParameterList"])
		.addToolbarTo("externalReportList", "tlbExternalReportList")
		.addToolbarTo("externalReportParameterList", "tlbExternalParamsReportList")
		.addToolbarTo("historySourceUploadList", "tlbHistoryList")
		.addToolbarTo("reportLogList", "tlbReportLog");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("detailsTab", ["historySourceUploadList", "reportLogList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbExternalReportList", {dc: "externalReport"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnUploadReportTemplate"),this._elems_.get("btnRunReportTemplate"),this._elems_.get("btnHelp")])
			.addReports()
		.end()
		.beginToolbar("tlbExternalParamsReportList", {dc: "externalReportParameters"})
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbHistoryList", {dc: "externalReportSourceUploadHistory"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnDeleteHistory"),this._elems_.get("btnEmptyHistory")])
			.addReports()
		.end()
		.beginToolbar("tlbReportLog", {dc: "reportLog"})
			.addButtons([])
			.addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnViewLog"),this._elems_.get("btnEmptyLog")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnUploadReportTemplate
	 */
	,onBtnUploadReportTemplate: function() {
		this._getWindow_("wdwNew").show();
		this._getDc_("externalReportSourceUploadHistory").doNew();
			var externalReportId = this._getDc_("externalReport").getRecord().get("id");
							var dc = this._getDc_("externalReportSourceUploadHistory");
							dc.setParamValue("reportId",externalReportId);
	}
	
	/**
	 * On-Click handler for button btnRunReportTemplate
	 */
	,onBtnRunReportTemplate: function() {
		this._getWindow_("wdwReportFormat").show();
	}
	
	/**
	 * On-Click handler for button btnDeleteHistory
	 */
	,onBtnDeleteHistory: function() {
		this._getDc_("externalReportSourceUploadHistory").doDelete();
	}
	
	/**
	 * On-Click handler for button btnEmptyHistory
	 */
	,onBtnEmptyHistory: function() {
		var successFn = function() {
			this._getDc_("externalReportSourceUploadHistory").doReloadPage();
		};
		var o={
			name:"emptyHistory",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("externalReport").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnEmptyLog
	 */
	,onBtnEmptyLog: function() {
		var successFn = function() {
			this._getDc_("reportLog").doReloadPage();
		};
		var o={
			name:"empty",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("reportLog").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnRunReport
	 */
	,onBtnRunReport: function() {
		var successFn = function() {
			this._getDc_("reportLog").doReloadPage();
			this._getWindow_("wdwReportFormat").close();
		};
		var o={
			name:"runReportTemplate",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("externalReport").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCancelRunReport
	 */
	,onBtnCancelRunReport: function() {
		this._getWindow_("wdwReportFormat").close();
	}
	
	/**
	 * On-Click handler for button btnViewLog
	 */
	,onBtnViewLog: function() {
		this._viewResult_();
	}
	
	/**
	 * On-Click handler for button btnCloseLog
	 */
	,onBtnCloseLog: function() {
		this._getWindow_("wdwLog").close();
	}
	
	/**
	 * On-Click handler for button btnSelectFile
	 */
	,onBtnSelectFile: function() {
		this.doSaveNewRPTSource();
	}
	
	/**
	 * On-Click handler for button btnCancel
	 */
	,onBtnCancel: function() {
		this.doCancelNewRptDesign();
		this._getWindow_("wdwNew").close();
	}
	
	/**
	 * On-Click handler for button btnHelp
	 */
	,onBtnHelp: function() {
		this.openHelp();
	}
	
	,openResult: function() {	
		var successFn = function(dc) {
			
							dc = this._getDc_("reportLog");
			                var fn = dc.params.get("reportFile");
			                var r = window.open(Main.urlDownload + "/" + fn,"Report");
			                r.focus();   
		};
		var o={
			name:"viewResult",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("reportLog").doRpcData(o);
	}
	
	,openLog: function() {	
		this._getWindow_("wdwLog").show();
	}
	
	,doCancelNewRptDesign: function() {	
		this._getDc_("externalReportSourceUploadHistory").doCancel();;
		this._getDc_("externalReport").doReloadPage();
	}
	
	,canUploadReportTemplate: function(dc) {
		 
						var selectedRecord = dc.selectedRecords[0];
						var system =  selectedRecord.get("system");
						return !(system===true);
	}
	
	,canRunReport: function(dc) {
		 
						var selectedRecord = dc.selectedRecords[0];
						var url =  selectedRecord.get("url");
						if (url){
							return true;
						}
						return false;
	}
	
	,_afterDefineDcs_: function() {
		
						var externalReport = this._getDc_("externalReport");				
						externalReport.on("afterDoSaveSuccess", function () { 
							this._applyStateAllButtons_();
						},this);
	}
	
	,_postDoNewFn_: function(dc) {
		
						var successFn = function() {
							//refresh first controller (external report controller)
							if (this.postRefreshCtrl1){
								this.postRefreshCtrl1.doReloadPage();
							}
							//refresh second controller (history upload controller)
							if (this.postRefreshCtrl2){
								this.postRefreshCtrl2.doReloadPage();
							}
						}
						var o={
							name:"uploadReportTemplate",
							callbacks:{
								successFn: successFn,
								successScope: this
							},
							modal:true
						};
						dc.doRpcData(o); 
	}
	
	,_viewResult_: function() {
		
						var reportLogDc = this._getDc_("reportLog");
						var selectedRecord = reportLogDc.selectedRecords[0];
						if(selectedRecord){
							var status = selectedRecord.get("status");
							if(status === __AD__.ReportStatus._SUCCESS_){
								this.openResult();
							}else if(status === __AD__.ReportStatus._FAILED_){
								this.openLog();
							}
						}
	}
	
	,_afterOnReady_: function() {
		
						var g = this._get_("externalReportList");
						if( g ){
							g.on("beforeedit", function(editor, context){
								if(!context.record.data.system){
									return true;
								}
								return false;
							},this);
						}
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/ReportCenter.html";
					window.open( url, "SONE_Help");
	}
	
	,doSaveNewRPTSource: function() {
		
					var historySourceNew = this._get_("historySourceNew");
					var externalReportId = this._getDc_("externalReport").getRecord().get("id");
					
					var dc = historySourceNew._controller_;
					var r = dc.getRecord();
		
					if (r) {
						var name = r.get("name");
						var notes = r.get("notes");
						var file = historySourceNew._get_("file").getValue();
						var fileRec = r.get("file");
						dc.setParamValue("reportId",externalReportId);				
						if (!Ext.isEmpty(name) && !Ext.isEmpty(notes) && !Ext.isEmpty(file) && !Ext.isEmpty(fileRec)) {
							historySourceNew._doUploadFile_();
						}
						else {
							historySourceNew._selectFile_();
						}
		
					} 
					
	}
});
