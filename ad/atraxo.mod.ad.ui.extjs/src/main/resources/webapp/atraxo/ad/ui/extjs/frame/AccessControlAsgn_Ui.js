/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.AccessControlAsgn_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.AccessControlAsgn_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("asgnAccess", Ext.create(atraxo.ad.ui.extjs.dc.AccessControlAsgn_Dc,{multiEdit: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addDcFilterFormView("asgnAccess", {name:"asgnAccessFilter", xtype:"ad_AccessControlAsgn_Dc$FilterPG"})
		.addDcEditGridView("asgnAccess", {name:"asgnAccessEditList", xtype:"ad_AccessControlAsgn_Dc$EditList", frame:true})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["asgnAccessEditList"], ["center"])
		.addToolbarTo("main", "tlbAsgnAccessEditList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbAsgnAccessEditList", {dc: "asgnAccess"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end();
	}
	

});
