/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.DbChangeLog_Dc", {
	extend: "e4e.dc.AbstractDc",
	filterModel: atraxo.ad.ui.extjs.ds.DbChangeLog_DsFilter,
	recordModel: atraxo.ad.ui.extjs.ds.DbChangeLog_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.DbChangeLog_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_DbChangeLog_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"author", bind:"{d.author}", dataIndex:"author", maxLength:255})
		.addTextField({ name:"liquibase", bind:"{d.liquibase}", dataIndex:"liquibase", maxLength:255})
		.addTextField({ name:"filename", bind:"{d.filename}", dataIndex:"filename", maxLength:255})
		.addTextField({ name:"description", bind:"{d.description}", dataIndex:"description", maxLength:1000})
		.addTextField({ name:"txid", bind:"{d.txid}", dataIndex:"txid", width:100, maxLength:255})
		.addDateField({name:"dateExecuted_From", dataIndex:"dateExecuted_From", emptyText:"From" })
		.addDateField({name:"dateExecuted_To", dataIndex:"dateExecuted_To", emptyText:"To" })
		.addFieldContainer({name: "dateExecuted"})
			.addChildrenTo("dateExecuted",["dateExecuted_From", "dateExecuted_To"])
		.addNumberField({name:"orderExecuted_From", dataIndex:"orderExecuted_From", emptyText:"From" })
		.addNumberField({name:"orderExecuted_To", dataIndex:"orderExecuted_To", emptyText:"To" })
		.addFieldContainer({name: "orderExecuted"})
			.addChildrenTo("orderExecuted",["orderExecuted_From", "orderExecuted_To"])
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"})
		.addPanel({ name:"col2", width:300, layout:"anchor"})
		.addPanel({ name:"col3", width:290, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3"])
		.addChildrenTo("col1", ["liquibase", "txid", "author"])
		.addChildrenTo("col2", ["filename", "description"])
		.addChildrenTo("col3", ["dateExecuted", "orderExecuted"]);
	}
});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.DbChangeLog_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_DbChangeLog_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"liquibase", dataIndex:"liquibase", width:80})
		.addTextColumn({ name:"txid", dataIndex:"txid", width:300})
		.addTextColumn({ name:"author", dataIndex:"author", width:80})
		.addTextColumn({ name:"filename", dataIndex:"filename", width:400})
		.addTextColumn({ name:"description", dataIndex:"description", width:200})
		.addDateColumn({ name:"dateExecuted", dataIndex:"dateExecuted", width:120, _mask_: Masks.DATETIME})
		.addNumberColumn({ name:"orderExecuted", dataIndex:"orderExecuted", width:120})
		.addTextColumn({ name:"tag", dataIndex:"tag", hidden:true, width:100})
		.addTextColumn({ name:"md5sum", dataIndex:"md5sum", hidden:true, width:100})
		.addTextColumn({ name:"comments", dataIndex:"comments", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
