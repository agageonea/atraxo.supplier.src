/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.UserGroup_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.UserGroup_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("group", Ext.create(atraxo.ad.ui.extjs.dc.UserGroup_Dc,{multiEdit: true}))
		.addDc("usr", Ext.create(atraxo.ad.ui.extjs.dc.User_Dc,{}))
		.linkDc("usr", "group",{fetchMode:"auto",fields:[
					{childParam:"inGroupId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnAsgnUsers", disabled:true, handler: this.onBtnAsgnUsers,stateManager:[{ name:"selected_one_clean", dc:"group"}], scope:this})
		.addButton({name:"btnDetailsUser", disabled:true, handler: this.onBtnDetailsUser,stateManager:[{ name:"selected_one", dc:"usr"}], scope:this})
		.addDcFilterFormView("group", {name:"filterGroup", xtype:"ad_UserGroup_Dc$FilterPG"})
		.addDcEditGridView("group", {name:"listGroup", xtype:"ad_UserGroup_Dc$EditList", frame:true})
		.addDcGridView("usr", {name:"usrList", _hasTitle_:true, height:"50%", xtype:"ad_User_Dc$ListShort"})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["listGroup", "usrList"], ["center", "south"])
		.addToolbarTo("main", "tlbGroupEditList")
		.addToolbarTo("usrList", "tlbUsrList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbGroupEditList", {dc: "group"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end()
		.beginToolbar("tlbUsrList", {dc: "usr"})
			.addButtons([])
			.addQuery()
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnAsgnUsers"),this._elems_.get("btnDetailsUser")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnAsgnUsers
	 */
	,onBtnAsgnUsers: function() {
		this._showAsgnWindow_("atraxo.ad.ui.extjs.asgn.UserGroup_User_Asgn$Ui" ,{dc: "group", objectIdField: "id"
		,listeners: {close: {scope: this, fn: function() { this._getDc_("usr").doReloadPage();
		}} }});
	}
	
	/**
	 * On-Click handler for button btnDetailsUser
	 */
	,onBtnDetailsUser: function() {
		var bundle = "atraxo.mod.ad";
		var frame = "atraxo.ad.ui.extjs.frame.User_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				id: this._getDc_("usr").getRecord().get("id"),
				code: this._getDc_("usr").getRecord().get("code")
			},
			callback: function (params) {
				this._when_called_for_details(params);
			}
		});
	}
	
	,_when_called_for_details: function(params) {
		
						var dc = this._getDc_("group");
						dc.setFilterValue("id", params.id);
						dc.setFilterValue("code", params.code);
						dc.doQuery();
	}
});
