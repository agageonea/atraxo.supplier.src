/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.BusinessAreas_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.BusinessAreas_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("businessArea", Ext.create(atraxo.ad.ui.extjs.dc.BusinessArea_Dc,{multiEdit: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addDcEditGridView("businessArea", {name:"businessAreaList", xtype:"ad_BusinessArea_Dc$List", frame:true})
		.addDcFilterFormView("businessArea", {name:"businessAreasFilter", xtype:"ad_BusinessArea_Dc$Filter"})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["businessAreaList"], ["center"])
		.addToolbarTo("businessAreaList", "tlbAdvancedFilterList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbAdvancedFilterList", {dc: "businessArea"})
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addCopy({iconCls:fp_asc.copy_glyph.css,glyph:fp_asc.copy_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addReports()
		.end();
	}
	

});
