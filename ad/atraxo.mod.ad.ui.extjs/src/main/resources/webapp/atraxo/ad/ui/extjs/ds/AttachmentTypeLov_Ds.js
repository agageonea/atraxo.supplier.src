/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.AttachmentTypeLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_AttachmentTypeLov_Ds"
	},
	
	
	fields: [
		{name:"category", type:"string"},
		{name:"targetAlias", type:"string", noSort:true},
		{name:"targetType", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"active", type:"boolean"},
		{name:"refid", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.AttachmentTypeLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"category", type:"string"},
		{name:"targetAlias", type:"string", noSort:true},
		{name:"targetType", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"refid", type:"string"}
	]
});
