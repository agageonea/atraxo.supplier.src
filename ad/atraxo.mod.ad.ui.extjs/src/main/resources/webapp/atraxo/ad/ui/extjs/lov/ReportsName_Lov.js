/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.lov.ReportsName_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.ad_ReportsName_Lov",
	displayField: "name", 
	_columns_: ["id", "name", "code"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{name}, {code}</span>';
		},
		width:250, maxHeight:350
	},
	_editFrame_: {
		name: "atraxo.ad.ui.extjs.frame.Report_Ui"
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.ad.ui.extjs.ds.ReportLov_Ds
});
