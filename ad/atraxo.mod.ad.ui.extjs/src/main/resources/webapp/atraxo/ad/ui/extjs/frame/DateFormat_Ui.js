/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.DateFormat_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.DateFormat_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("fmt", Ext.create(atraxo.ad.ui.extjs.dc.DateFormat_Dc,{multiEdit: true}))
		.addDc("mask", Ext.create(atraxo.ad.ui.extjs.dc.DateFormatMask_Dc,{multiEdit: true}))
		.linkDc("mask", "fmt",{fetchMode:"auto",fields:[
					{childField:"dateFormatId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnSynchronize", disabled:true, handler: this.onBtnSynchronize,stateManager:[{ name:"selected_one_clean", dc:"fmt"}], scope:this})
		.addDcFilterFormView("fmt", {name:"fmtFilter", xtype:"ad_DateFormat_Dc$Filter"})
		.addDcEditGridView("fmt", {name:"fmtEditList", xtype:"ad_DateFormat_Dc$EditList", frame:true})
		.addDcEditGridView("mask", {name:"maskEditList", height:280, xtype:"ad_DateFormatMask_Dc$CtxEditList", frame:true})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["fmtEditList", "maskEditList"], ["north", "center"])
		.addToolbarTo("main", "tlbFmtList")
		.addToolbarTo("maskEditList", "tlbMaskList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbFmtList", {dc: "fmt"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end()
		.beginToolbar("tlbMaskList", {dc: "mask"})
			.addQuery().addSave().addCancel()
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnSynchronize
	 */
	,onBtnSynchronize: function() {
		var successFn = function() {
			this._getDc_("mask").doQuery();
		};
		var o={
			name:"synchronizeMasks",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("fmt").doRpcData(o);
	}
});
