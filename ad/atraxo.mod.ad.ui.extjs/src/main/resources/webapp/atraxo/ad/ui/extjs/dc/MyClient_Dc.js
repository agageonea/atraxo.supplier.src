/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.MyClient_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.MyClient_Ds
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.ad.ui.extjs.dc.MyClient_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ad_MyClient_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", noEdit:true , maxLength:255})
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", noEdit:true , maxLength:64, caseRestriction:"uppercase"})
		.addTextField({ name:"workspacePath", bind:"{d.workspacePath}", dataIndex:"workspacePath", noEdit:true , maxLength:255})
		.addTextField({ name:"importPath", bind:"{d.importPath}", dataIndex:"importPath", noEdit:true , maxLength:255})
		.addTextField({ name:"exportPath", bind:"{d.exportPath}", dataIndex:"exportPath", noEdit:true , maxLength:255})
		.addTextField({ name:"tempPath", bind:"{d.tempPath}", dataIndex:"tempPath", noEdit:true , maxLength:255})
		.addTextField({ name:"adminRole", bind:"{d.adminRole}", dataIndex:"adminRole", noEdit:true , maxLength:32})
		.addDateField({name:"createdAt", bind:"{d.createdAt}", dataIndex:"createdAt", noEdit:true })
		.addDateField({name:"modifiedAt", bind:"{d.modifiedAt}", dataIndex:"modifiedAt", noEdit:true })
		.addTextField({ name:"createdBy", bind:"{d.createdBy}", dataIndex:"createdBy", noEdit:true , maxLength:32})
		.addTextField({ name:"modifiedBy", bind:"{d.modifiedBy}", dataIndex:"modifiedBy", noEdit:true , maxLength:32})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:350, layout:"anchor"})
		.addPanel({ name:"col3", width:220, layout:"anchor"})
		.addPanel({ name:"col2", _hasTitle_: true, width:500,  xtype:"fieldset", collapsible:true, border:true, defaults: { labelWidth:"120"}, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3"])
		.addChildrenTo("col1", ["name", "code", "adminRole"])
		.addChildrenTo("col3", ["createdAt", "modifiedAt", "createdBy", "modifiedBy"])
		.addChildrenTo("col2", ["workspacePath", "importPath", "exportPath", "tempPath"]);
	}
});
