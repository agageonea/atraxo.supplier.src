/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.Report_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Report_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("rep", Ext.create(atraxo.ad.ui.extjs.dc.Report_Dc,{}))
		.addDc("params", Ext.create(atraxo.ad.ui.extjs.dc.ReportParam_Dc,{multiEdit: true}))
		.addDc("paramsRt", Ext.create(atraxo.ad.ui.extjs.dc.ReportParamRt_Dc,{multiEdit: true}))
		.addDc("dsrep", Ext.create(atraxo.ad.ui.extjs.dc.DsReport_Dc,{multiEdit: true}))
		.addDc("dsparam", Ext.create(atraxo.ad.ui.extjs.dc.DsReportParam_Dc,{multiEdit: true}))
		.addDc("usage", Ext.create(atraxo.ad.ui.extjs.dc.DsReportUsageCtx_Dc,{multiEdit: true}))
		.linkDc("params", "rep",{fetchMode:"auto",fields:[
					{childField:"reportId", parentField:"id"}]})
				.linkDc("paramsRt", "rep",{fetchMode:"auto",fields:[
					{childField:"reportId", parentField:"id"}]})
				.linkDc("dsrep", "rep",{fetchMode:"auto",fields:[
					{childField:"reportId", parentField:"id"}]})
				.linkDc("dsparam", "dsrep",{fetchMode:"auto",fields:[
					{childField:"dsReportId", parentField:"id"}, {childField:"reportId", parentField:"reportId"}, {childField:"dataSource", parentField:"dataSource"}]})
				.linkDc("usage", "dsrep",{fetchMode:"auto",fields:[
					{childField:"dsReportId", parentField:"id"}, {childField:"reportId", parentField:"reportId"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnTestReport",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnTestReport, scope:this})
		.addButton({name:"btnRunReport",glyph:fp_asc.gear_glyph.glyph, disabled:false, handler: this.onBtnRunReport, scope:this})
		.addButton({name:"btnCancelReport",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false, handler: this.onBtnCancelReport, scope:this})
		.addDcFilterFormView("rep", {name:"repFilter", xtype:"ad_Report_Dc$FilterPG"})
		.addDcGridView("rep", {name:"repList", xtype:"ad_Report_Dc$List"})
		.addDcFormView("rep", {name:"repEdit", xtype:"ad_Report_Dc$Edit"})
		.addDcEditGridView("params", {name:"paramEditList", _hasTitle_:true, xtype:"ad_ReportParam_Dc$CtxEditList", frame:true})
		.addDcEditGridView("paramsRt", {name:"paramTest", xtype:"ad_ReportParamRt_Dc$List", frame:true})
		.addDcEditGridView("dsrep", {name:"dsrepList", width:600, xtype:"ad_DsReport_Dc$ReportCtxList", frame:true})
		.addDcEditGridView("dsparam", {name:"dsparamList", _hasTitle_:true, xtype:"ad_DsReportParam_Dc$CtxEditList", frame:true})
		.addDcEditGridView("usage", {name:"usageList", _hasTitle_:true, xtype:"ad_DsReportUsageCtx_Dc$EditList", frame:true})
		.addWindow({name:"wdwTestReport", _hasTitle_:true, width:450, height:350, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("paramTest")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnRunReport"), this._elems_.get("btnCancelReport")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"repDetailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"dsRep", _hasTitle_:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"dsrepDetailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"repDetailsTab", containerPanelName:"canvas2", dcName:"rep"})
		
		.addTabPanelForUserFields({tabPanelName:"dsrepDetailsTab", containerPanelName:"dsRep", dcName:"dsrep"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["repList"], ["center"])
		.addChildrenTo("canvas2", ["repEdit", "repDetailsTab"], ["north", "center"])
		.addChildrenTo("repDetailsTab", ["paramEditList"])
		.addChildrenTo("dsRep", ["dsrepList", "dsrepDetailsTab"], ["west", "center"])
		.addChildrenTo("dsrepDetailsTab", ["dsparamList"])
		.addToolbarTo("canvas1", "tlbRepList")
		.addToolbarTo("canvas2", "tlbRepEdit")
		.addToolbarTo("paramEditList", "tlbParamList")
		.addToolbarTo("dsrepList", "tlbDsRepList")
		.addToolbarTo("dsparamList", "tlbDsParamList")
		.addToolbarTo("usageList", "tlbUsageList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2"])
		.addChildrenTo2("repDetailsTab", ["dsRep"])
		.addChildrenTo2("dsrepDetailsTab", ["usageList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbRepList", {dc: "rep"})
			.addNew({inContainer:"main",showView:"canvas2",autoEdit:"true"}).addEdit({inContainer:"main",showView:"canvas2"}).addCopy().addDelete()
			.addReports()
		.end()
		.beginToolbar("tlbRepEdit", {dc: "rep"})
			.addBack({inContainer:"main",showView:"canvas1"}).addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addPrevRec().addNextRec()
			.addReports()
		.end()
		.beginToolbar("tlbParamList", {dc: "params"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end()
		.beginToolbar("tlbDsRepList", {dc: "dsrep"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end()
		.beginToolbar("tlbDsParamList", {dc: "dsparam"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end()
		.beginToolbar("tlbUsageList", {dc: "usage"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnTestReport
	 */
	,onBtnTestReport: function() {
		this._getWindow_("wdwTestReport").show();
		this._getDc_("paramsRt").doQuery();
	}
	
	/**
	 * On-Click handler for button btnRunReport
	 */
	,onBtnRunReport: function() {
		this._runReport_();
	}
	
	/**
	 * On-Click handler for button btnCancelReport
	 */
	,onBtnCancelReport: function() {
		this._getDc_("paramsRt").doCancel();
		this._getWindow_("wdwTestReport").close();
	}
	
	,_runReport_: function() {
		
					var paramsDc = this._getDc_("paramsRt");
					var serverUrl = this._getDc_("rep").record.data.serverUrl;
					var qs = "";
			
					paramsDc.store.data.each(function(item) {
						if(qs !== "") {
							qs += "&";
						}
						qs += item.get("code") + "=" + item.get("value");
					});
					window.open(serverUrl + "?" + qs,"Test-report","").focus();
	}
});
