/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.lov.MenuItemKpiLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.ad_MenuItemKpiLov_Lov",
	displayField: "title", 
	_columns_: ["title", "id", "name"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{title}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.ad.ui.extjs.ds.MenuItemRtLov_Ds
});
