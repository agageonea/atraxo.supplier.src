/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.JobTimer_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.JobTimer_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("timer", Ext.create(atraxo.ad.ui.extjs.dc.JobTimer_Dc,{}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addDcFilterFormView("timer", {name:"timerFilter", xtype:"ad_JobTimer_Dc$Filter"})
		.addDcGridView("timer", {name:"timerList", _hasTitle_:true, xtype:"ad_JobTimer_Dc$List"})
		.addDcFormView("timer", {name:"timerEdit", _hasTitle_:true, xtype:"ad_JobTimer_Dc$Edit"})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["timerFilter", "timerList", "timerEdit"], ["north", "center", "east"])
		.addToolbarTo("main", "tlbTimer")
		.addToolbarTo("timerList", "tlbTimerList")
		.addToolbarTo("timerEdit", "tlbTimerEdit");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbTimer", {dc: "timer"})
			.addQuery()
			.addReports()
		.end()
		.beginToolbar("tlbTimerList", {dc: "timer"})
			.addQuery().addNew({inContainer:"main",showView:"canvas2",autoEdit:"true"}).addCopy().addDelete()
			.addReports()
		.end()
		.beginToolbar("tlbTimerEdit", {dc: "timer"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy()
			.addReports()
		.end();
	}
	

	
	,_when_called_to_view_: function(params) {
		
						var timer = this._getDc_("timer");
						if (timer.isDirty()) {
							this._alert_dirty_();
							return;
						}
						timer.doClearQuery();	 
						
						timer.setFilterValue("id", params.id );
						timer.setFilterValue("name", params.name );
						timer.setFilterValue("jobContextId", params.jobContextId );
						timer.setFilterValue("jobContext", params.jobContext );
						timer.doQuery();
	}
});
