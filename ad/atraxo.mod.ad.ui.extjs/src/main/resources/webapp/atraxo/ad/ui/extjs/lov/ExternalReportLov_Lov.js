/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.lov.ExternalReportLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.ad_ExternalReportLov_Lov",
	displayField: "name", 
	_columns_: ["name", "dsName", "designName", "businessArea"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{name}, {designName}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.ad.ui.extjs.ds.ExternalReportLov_Ds
});
