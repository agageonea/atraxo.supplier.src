/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.Menu_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_Menu_Ds"
	},
	
	
	validators: {
		sequenceNo: [{type: 'presence'}],
		name: [{type: 'presence'}],
		title: [{type: 'presence'}]
	},
	
	fields: [
		{name:"sequenceNo", type:"int", allowNull:true},
		{name:"title", type:"string"},
		{name:"tag", type:"string"},
		{name:"glyph", type:"string"},
		{name:"id", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.Menu_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"sequenceNo", type:"int", allowNull:true},
		{name:"title", type:"string"},
		{name:"tag", type:"string"},
		{name:"glyph", type:"string"},
		{name:"id", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});

Ext.define("atraxo.ad.ui.extjs.ds.Menu_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"withRole", type:"string", forFilter:true},
		{name:"withRoleId", type:"string"}
	]
});
