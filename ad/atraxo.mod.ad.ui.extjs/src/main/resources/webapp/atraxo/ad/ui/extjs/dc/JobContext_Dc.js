/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.JobContext_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.JobContext_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.JobContext_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_JobContext_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"id", bind:"{d.id}", dataIndex:"id", maxLength:64})
		.addLov({name:"jobName", bind:"{d.jobName}", dataIndex:"jobName", xtype:"ad_Jobs_Lov", maxLength:255})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", maxLength:255})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"})
		.addPanel({ name:"col2", width:250, layout:"anchor"})
		.addPanel({ name:"col3", width:250, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3"])
		.addChildrenTo("col1", ["jobName"])
		.addChildrenTo("col2", ["name"])
		.addChildrenTo("col3", ["id"]);
	}
});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.JobContext_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_JobContext_Dc$EditList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"name", dataIndex:"name", width:200, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"description", dataIndex:"description", width:200, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addLov({name:"jobName", dataIndex:"jobName", width:200, noUpdate: true, xtype:"gridcolumn", 
			editor:{xtype:"ad_Jobs_Lov", noUpdate:true, maxLength:255}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
