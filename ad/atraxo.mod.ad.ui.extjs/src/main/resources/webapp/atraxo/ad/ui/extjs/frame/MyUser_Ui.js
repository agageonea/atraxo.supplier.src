/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.MyUser_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.MyUser_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("usr", Ext.create(atraxo.ad.ui.extjs.dc.MyUser_Dc,{}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addDcFormView("usr", {name:"usrEdit", xtype:"ad_MyUser_Dc$Edit"})
		.addPanel({name:"main", layout:"fit"}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["usrEdit"])
		.addToolbarTo("main", "tlbUsrEdit");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbUsrEdit", {dc: "usr"})
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addReports()
		.end();
	}
	

	
	,loadCurrentUser: function() {
		
		  				this._getDc_("usr").doQuery();
	}
	,_afterDefineDcs_: function() {
		this.loadCurrentUser();
	}
});
