/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.lov.MenuItems_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.ad_MenuItems_Lov",
	displayField: "name", 
	_columns_: ["id", "name", "description", "title", "description"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{name}, {description}, {title}</span>';
		},
		width:250, maxHeight:350
	},
	_editFrame_: {
		name: "atraxo.ad.ui.extjs.frame.Menu_Ui"
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	paramModel: atraxo.ad.ui.extjs.ds.MenuItemLov_DsParam,
	recordModel: atraxo.ad.ui.extjs.ds.MenuItemLov_Ds
});
