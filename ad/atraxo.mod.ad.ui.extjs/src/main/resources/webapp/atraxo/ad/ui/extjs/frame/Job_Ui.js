/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.Job_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Job_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("job", Ext.create(atraxo.ad.ui.extjs.dc.Job_Dc,{}))
		.addDc("params", Ext.create(atraxo.ad.ui.extjs.dc.JobParam_Dc,{}))
		.linkDc("params", "job",{fetchMode:"auto",fields:[
					{childField:"jobId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnSynchronize",glyph:fp_asc.gear_glyph.glyph, disabled:false, handler: this.onBtnSynchronize, scope:this})
		.addDcFilterFormView("job", {name:"jobFilter", xtype:"ad_Job_Dc$FilterPG"})
		.addDcGridView("job", {name:"jobList", xtype:"ad_Job_Dc$List"})
		.addDcGridView("params", {name:"paramsCtxList", width:500, xtype:"ad_JobParam_Dc$CtxList"})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["jobList", "paramsCtxList"], ["center", "east"])
		.addToolbarTo("main", "tlbJobList")
		.addToolbarTo("paramsCtxList", "tlbParamList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbJobList", {dc: "job"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnSynchronize")])
			.addReports()
		.end()
		.beginToolbar("tlbParamList", {dc: "params"})
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnSynchronize
	 */
	,onBtnSynchronize: function() {
		var successFn = function() {
			this._getDc_("job").doQuery();
		};
		var o={
			name:"synchronizeCatalog",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("job").doRpcFilter(o);
	}
	
	,_when_called_to_view_: function(params) {
		
						var job = this._getDc_("job");
						if (job.isDirty()) {
							this._alert_dirty_();
							return;
						}
						job.doClearQuery();
							 
						job.setFilterValue("jobName", params.jobName );
						job.doQuery();
	}
	
	,_afterDefineElements_: function() {
		
						if (!getApplication().getSession().user.systemUser) {
						         this._getBuilder_()
						    .change("btnSynchronize", {disabled: true});
						}
	}
});
