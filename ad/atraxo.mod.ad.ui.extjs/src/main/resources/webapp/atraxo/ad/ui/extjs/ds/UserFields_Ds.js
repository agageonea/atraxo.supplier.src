/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.UserFields_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_UserFields_Ds"
	},
	
	
	validators: {
		businessArea: [{type: 'presence'}],
		name: [{type: 'presence'}],
		description: [{type: 'presence'}],
		defaultValue: [{type: 'presence'}],
		type: [{type: 'presence'}]
	},
	
	fields: [
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"defaultValue", type:"string"},
		{name:"type", type:"string"},
		{name:"businessAreaId", type:"string"},
		{name:"businessArea", type:"string"},
		{name:"dsModel", type:"string"},
		{name:"dsName", type:"string"},
		{name:"dsAlias", type:"string"},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.UserFields_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"defaultValue", type:"string"},
		{name:"type", type:"string"},
		{name:"businessAreaId", type:"string"},
		{name:"businessArea", type:"string"},
		{name:"dsModel", type:"string"},
		{name:"dsName", type:"string"},
		{name:"dsAlias", type:"string"},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
