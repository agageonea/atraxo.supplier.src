/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.Report_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.Report_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Report_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_Report_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"code", bind:"{d.code}", dataIndex:"code", xtype:"ad_Reports_Lov", maxLength:64, caseRestriction:"uppercase",
			retFieldMapping: [{lovField:"id", dsField: "id"} ]})
		.addLov({name:"name", bind:"{d.name}", dataIndex:"name", xtype:"ad_ReportsName_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "id"} ]})
		.addLov({name:"reportServer", bind:"{d.reportServer}", dataIndex:"reportServer", xtype:"ad_ReportServers_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "reportServerId"} ]})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active"})
		.addTextField({ name:"contextPath", bind:"{d.contextPath}", dataIndex:"contextPath", maxLength:255})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:250, layout:"anchor"})
		.addPanel({ name:"col2", width:250, layout:"anchor"})
		.addPanel({ name:"col3", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3"])
		.addChildrenTo("col1", ["code", "name"])
		.addChildrenTo("col2", ["reportServer", "contextPath"])
		.addChildrenTo("col3", ["active"]);
	}
});

/* ================= FILTER: FilterPG ================= */


Ext.define("atraxo.ad.ui.extjs.dc.Report_Dc$FilterPG", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_Report_Dc$FilterPG",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"code", dataIndex:"code", maxLength:64, caseRestriction:"uppercase",
				editor:{xtype:"atraxo.ad.ui.extjs.lov.Reports_Lov", selectOnFocus:true, maxLength:64, caseRestriction:"uppercase",
					retFieldMapping: [{lovField:"id", dsField: "id"} ]}})
			.addLov({name:"name", dataIndex:"name", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.ReportsName_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "id"} ]}})
			.addLov({name:"reportServer", dataIndex:"reportServer", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.ReportServers_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "reportServerId"} ]}})
			.addBooleanField({ name:"active", dataIndex:"active"})
			.addTextField({ name:"contextPath", dataIndex:"contextPath", maxLength:255})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Report_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_Report_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:200})
		.addTextColumn({ name:"code", dataIndex:"code", width:120})
		.addTextColumn({ name:"notes", dataIndex:"notes", width:200})
		.addTextColumn({ name:"reportServer", dataIndex:"reportServer", width:200})
		.addTextColumn({ name:"contextPath", dataIndex:"contextPath", width:200})
		.addBooleanColumn({ name:"active", dataIndex:"active"})
		.addTextColumn({ name:"reportServerId", dataIndex:"reportServerId", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Report_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ad_Report_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, maxLength:255})
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", allowBlank:false, maxLength:64, caseRestriction:"uppercase"})
		.addTextField({ name:"contextPath", bind:"{d.contextPath}", dataIndex:"contextPath", maxLength:255})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active"})
		.addTextArea({ name:"notes", bind:"{d.notes}", dataIndex:"notes", height:60})
		.addLov({name:"reportServer", bind:"{d.reportServer}", dataIndex:"reportServer", allowBlank:false, xtype:"ad_ReportServers_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "reportServerId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"})
		.addPanel({ name:"col2", width:500, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["name", "code", "reportServer", "active"])
		.addChildrenTo("col2", ["contextPath", "notes"]);
	}
});
