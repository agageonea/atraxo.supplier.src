/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.lov.Params_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.ad_Params_Lov",
	displayField: "code", 
	_columns_: ["id", "code", "name"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{code}, {name}</span>';
		},
		width:350, maxHeight:200
	},
	_editFrame_: {
		name: "atraxo.ad.ui.extjs.frame.MyParam_Ui"
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.ad.ui.extjs.ds.ParamLov_Ds
});
