/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.AttachmentType_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.AttachmentType_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("attchType", Ext.create(atraxo.ad.ui.extjs.dc.AttachmentType_Dc,{multiEdit: true}))
		.addDc("targetRule", Ext.create(atraxo.ad.ui.extjs.dc.TargetRule_Dc,{multiEdit: true}))
		.linkDc("targetRule", "attchType",{fetchMode:"auto",fields:[
					{childField:"sourceRefId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addDcFilterFormView("attchType", {name:"attchTypeFilter", xtype:"ad_AttachmentType_Dc$FilterPG"})
		.addDcEditGridView("attchType", {name:"attchTypeEditList", xtype:"ad_AttachmentType_Dc$EditList", frame:true})
		.addDcEditGridView("targetRule", {name:"targetRuleEditList", height:"50%", xtype:"ad_TargetRule_Dc$EditList", frame:true})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["attchTypeEditList", "targetRuleEditList"], ["center", "south"])
		.addToolbarTo("main", "tlbAttchTypeEditList")
		.addToolbarTo("targetRuleEditList", "tlbTargetRuleEditList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbAttchTypeEditList", {dc: "attchType"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end()
		.beginToolbar("tlbTargetRuleEditList", {dc: "targetRule"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end();
	}
	

});
