/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.Menu_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ad.ui.extjs.ds.Menu_DsParam,
	recordModel: atraxo.ad.ui.extjs.ds.Menu_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ad.ui.extjs.dc.Menu_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_Menu_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"name", dataIndex:"name", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.Menus_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "id"} ]}})
			.addTextField({ name:"title", dataIndex:"title", maxLength:255})
			.addCombo({ xtype:"combo", name:"tag", dataIndex:"tag", store:[ __AD__.TMenuTag._TOP_, __AD__.TMenuTag._LEFT_]})
			.addBooleanField({ name:"active", dataIndex:"active"})
			.addTextField({ name:"glyph", dataIndex:"glyph", maxLength:64})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Menu_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_Menu_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addNumberColumn({ name:"sequenceNo", dataIndex:"sequenceNo", width:70, format:"0"})
		.addTextColumn({ name:"name", dataIndex:"name", width:120})
		.addTextColumn({ name:"title", dataIndex:"title", width:200})
		.addTextColumn({ name:"tag", dataIndex:"tag", width:80})
		.addTextColumn({ name:"glyph", dataIndex:"glyph", width:120})
		.addBooleanColumn({ name:"active", dataIndex:"active"})
		.addTextColumn({ name:"description", dataIndex:"description", hidden:true, width:200})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Menu_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_Menu_Dc$EditList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addNumberColumn({name:"sequenceNo", dataIndex:"sequenceNo", width:70, maxLength:4, align:"right", format:"0" })
		.addTextColumn({name:"name", dataIndex:"name", width:120, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"title", dataIndex:"title", width:120, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addComboColumn({name:"tag", dataIndex:"tag", width:80, 
			editor:{xtype:"combo", mode: 'local', triggerAction:'all', forceSelection:true, store:[ __AD__.TMenuTag._TOP_, __AD__.TMenuTag._LEFT_]}})
		.addTextColumn({name:"glyph", dataIndex:"glyph", width:120, maxLength:64, 
			editor: { xtype:"textfield", maxLength:64}})
		.addBooleanColumn({name:"active", dataIndex:"active"})
		.addTextColumn({name:"description", dataIndex:"description", hidden:true, width:200, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
