/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.DbChangeLog_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.DbChangeLog_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("dblog", Ext.create(atraxo.ad.ui.extjs.dc.DbChangeLog_Dc,{}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addDcFilterFormView("dblog", {name:"dblogFilter", xtype:"ad_DbChangeLog_Dc$Filter"})
		.addDcGridView("dblog", {name:"dblogList", xtype:"ad_DbChangeLog_Dc$List"})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["dblogList"], ["center"])
		;
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_();
	}
	

});
