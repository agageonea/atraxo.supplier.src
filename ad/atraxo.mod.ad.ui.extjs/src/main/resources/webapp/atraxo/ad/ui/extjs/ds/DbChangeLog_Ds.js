/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.DbChangeLog_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_DbChangeLog_Ds"
	},
	
	
	fields: [
		{name:"id", type:"string"},
		{name:"txid", type:"string"},
		{name:"author", type:"string"},
		{name:"filename", type:"string"},
		{name:"dateExecuted", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"orderExecuted", type:"int", allowNull:true},
		{name:"md5sum", type:"string"},
		{name:"description", type:"string"},
		{name:"comments", type:"string"},
		{name:"tag", type:"string"},
		{name:"liquibase", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.DbChangeLog_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"id", type:"string"},
		{name:"txid", type:"string"},
		{name:"author", type:"string"},
		{name:"filename", type:"string"},
		{name:"dateExecuted", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"orderExecuted", type:"int", allowNull:true},
		{name:"md5sum", type:"string"},
		{name:"description", type:"string"},
		{name:"comments", type:"string"},
		{name:"tag", type:"string"},
		{name:"liquibase", type:"string"}
		,
		{name:"dateExecuted_From",type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"dateExecuted_To",type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"orderExecuted_From",type:"int", allowNull:true},
		{name:"orderExecuted_To",type:"int", allowNull:true}
	]
});
