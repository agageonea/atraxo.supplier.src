/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.User_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.User_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("usr", Ext.create(atraxo.ad.ui.extjs.dc.User_Dc,{trackEditMode: true}))
		.addDc("rol", Ext.create(atraxo.ad.ui.extjs.dc.Role_Dc,{}))
		.addDc("grp", Ext.create(atraxo.ad.ui.extjs.dc.UserGroup_Dc,{}))
		.linkDc("rol", "usr",{fetchMode:"auto",fields:[
					{childParam:"withUserId", parentField:"id"}]})
				.linkDc("grp", "usr",{fetchMode:"auto",fields:[
					{childParam:"withUserId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnChangePassword", disabled:false, handler: this.onBtnChangePassword,stateManager:[{ name:"record_is_clean", dc:"usr"}], scope:this})
		.addButton({name:"btnSavePassword", disabled:false, handler: this.onBtnSavePassword, scope:this})
		.addButton({name:"btnAsgnRoles", disabled:false, handler: this.onBtnAsgnRoles,stateManager:[{ name:"record_is_clean", dc:"usr"}], scope:this})
		.addButton({name:"btnAsgnGroups", disabled:false, handler: this.onBtnAsgnGroups,stateManager:[{ name:"record_is_clean", dc:"usr"}], scope:this})
		.addButton({name:"btnDetailsRole", disabled:true, handler: this.onBtnDetailsRole,stateManager:[{ name:"selected_one", dc:"rol"}], scope:this})
		.addButton({name:"btnDetailsGroup", disabled:true, handler: this.onBtnDetailsGroup,stateManager:[{ name:"selected_one", dc:"grp"}], scope:this})
		.addDcFilterFormView("usr", {name:"usrFilter", xtype:"ad_User_Dc$FilterPG"})
		.addDcGridView("usr", {name:"usrList", xtype:"ad_User_Dc$List"})
		.addDcFormView("usr", {name:"usrEdit", xtype:"ad_User_Dc$Edit"})
		.addDcFormView("usr", {name:"canvasPassword", preventHeader:true, xtype:"ad_User_Dc$ChangePasswordForm"})
		.addDcGridView("rol", {name:"rolList", _hasTitle_:true, xtype:"ad_Role_Dc$List"})
		.addDcGridView("grp", {name:"grpList", _hasTitle_:true, xtype:"ad_UserGroup_Dc$List"})
		.addWindow({name:"wdwChangePassword", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("canvasPassword")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSavePassword")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas2", dcName:"usr"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["usrList"], ["center"])
		.addChildrenTo("canvas2", ["usrEdit", "detailsTab"], ["north", "center"])
		.addChildrenTo("detailsTab", ["rolList"])
		.addToolbarTo("canvas1", "tlbUsrList")
		.addToolbarTo("canvas2", "tlbUsrEdit")
		.addToolbarTo("rolList", "tlbRolList")
		.addToolbarTo("grpList", "tlbGrpList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2"])
		.addChildrenTo2("detailsTab", ["grpList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbUsrList", {dc: "usr"})
			.addNew({inContainer:"main",showView:"canvas2",autoEdit:"true"}).addEdit({inContainer:"main",showView:"canvas2"}).addCopy().addDelete()
			.addReports()
		.end()
		.beginToolbar("tlbUsrEdit", {dc: "usr"})
			.addButtons([])
			.addBack({inContainer:"main",showView:"canvas1"}).addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addPrevRec().addNextRec()
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnChangePassword")])
			.addReports()
		.end()
		.beginToolbar("tlbRolList", {dc: "rol"})
			.addButtons([])
			.addQuery()
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnAsgnRoles"),this._elems_.get("btnDetailsRole")])
			.addReports()
		.end()
		.beginToolbar("tlbGrpList", {dc: "grp"})
			.addButtons([])
			.addQuery()
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnAsgnGroups"),this._elems_.get("btnDetailsGroup")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnChangePassword
	 */
	,onBtnChangePassword: function() {
		this._getWindow_("wdwChangePassword").show();
	}
	
	/**
	 * On-Click handler for button btnSavePassword
	 */
	,onBtnSavePassword: function() {
		var successFn = function() {
			this._getWindow_("wdwChangePassword").close();
		};
		var o={
			name:"changePassword",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("usr").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnAsgnRoles
	 */
	,onBtnAsgnRoles: function() {
		this._showAsgnWindow_("atraxo.ad.ui.extjs.asgn.User_Role_Asgn$Ui" ,{dc: "usr", objectIdField: "id"
		,listeners: {close: {scope: this, fn: function() { this._getDc_("rol").doQuery();
		}} }});
	}
	
	/**
	 * On-Click handler for button btnAsgnGroups
	 */
	,onBtnAsgnGroups: function() {
		this._showAsgnWindow_("atraxo.ad.ui.extjs.asgn.User_UserGroup_Asgn$Ui" ,{dc: "usr", objectIdField: "id"
		,listeners: {close: {scope: this, fn: function() { this._getDc_("grp").doQuery();
		}} }});
	}
	
	/**
	 * On-Click handler for button btnDetailsRole
	 */
	,onBtnDetailsRole: function() {
		var bundle = "atraxo.mod.ad";
		var frame = "atraxo.ad.ui.extjs.frame.Role_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				id: this._getDc_("rol").getRecord().get("id"),
				code: this._getDc_("rol").getRecord().get("code")
			},
			callback: function (params) {
				this._when_called_for_details(params);
			}
		});
	}
	
	/**
	 * On-Click handler for button btnDetailsGroup
	 */
	,onBtnDetailsGroup: function() {
		var bundle = "atraxo.mod.ad";
		var frame = "atraxo.ad.ui.extjs.frame.UserGroup_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				id: this._getDc_("grp").getRecord().get("id"),
				code: this._getDc_("grp").getRecord().get("code")
			},
			callback: function (params) {
				this._when_called_for_details(params);
			}
		});
	}
	
	,_when_called_for_details: function(params) {
		
						var dc = this._getDc_("usr");
						dc.setFilterValue("id", params.id);
						dc.setFilterValue("code", params.code);
						dc.doQuery();
						dc.doEditIn();
	}
	
	,_afterDefineDcs_: function() {
		
						var usr = this._getDc_("usr");
						usr.on("afterDoSaveSuccess", function (dc) {
				            this._applyStateAllButtons_();
				        }, this);
	}
});
