/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.ReportUsage_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.ReportUsage_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("ru", Ext.create(atraxo.ad.ui.extjs.dc.DsReportUsage_Dc,{multiEdit: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addDcFilterFormView("ru", {name:"ruFilter", xtype:"ad_DsReportUsage_Dc$FilterPG"})
		.addDcEditGridView("ru", {name:"ruList", xtype:"ad_DsReportUsage_Dc$List", frame:true})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["ruList"], ["center"])
		.addToolbarTo("main", "tlbRuList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbRuList", {dc: "ru"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end();
	}
	

});
