/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.ViewState_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.ViewState_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.ViewState_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_ViewState_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", maxLength:255})
		.addTextField({ name:"cmp", bind:"{d.cmp}", dataIndex:"cmp", maxLength:255})
		.addCombo({ xtype:"combo", name:"cmpType", bind:"{d.cmpType}", dataIndex:"cmpType", store:[ __AD__.TViewStateCmpType._FRAME_DCGRID_, __AD__.TViewStateCmpType._FRAME_DCEGRID_]})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:250, layout:"anchor"})
		.addPanel({ name:"col2", width:250, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["name"])
		.addChildrenTo("col2", ["cmpType", "cmp"]);
	}
});

/* ================= FILTER: FilterPG ================= */


Ext.define("atraxo.ad.ui.extjs.dc.ViewState_Dc$FilterPG", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_ViewState_Dc$FilterPG",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:255})
			.addTextField({ name:"cmp", dataIndex:"cmp", maxLength:255})
			.addCombo({ xtype:"combo", name:"cmpType", dataIndex:"cmpType", store:[ __AD__.TViewStateCmpType._FRAME_DCGRID_, __AD__.TViewStateCmpType._FRAME_DCEGRID_]})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.ViewState_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_ViewState_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:150})
		.addTextColumn({ name:"cmpType", dataIndex:"cmpType", width:120})
		.addTextColumn({ name:"cmp", dataIndex:"cmp", width:400})
		.addTextColumn({ name:"value", dataIndex:"value", hidden:true, width:200})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Value ================= */

Ext.define("atraxo.ad.ui.extjs.dc.ViewState_Dc$Value", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ad_ViewState_Dc$Value",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"value", bind:"{d.value}", dataIndex:"value", maxLength:4000})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, defaults: { labelAlign:"top"}, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["value"]);
	}
});
