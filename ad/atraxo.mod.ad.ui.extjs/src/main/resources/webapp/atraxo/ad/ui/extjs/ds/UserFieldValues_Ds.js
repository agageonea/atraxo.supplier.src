/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.UserFieldValues_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_UserFieldValues_Ds"
	},
	
	
	validators: {
		value: [{type: 'presence'}]
	},
	
	fields: [
		{name:"value", type:"string"},
		{name:"targetRefid", type:"int", allowNull:true},
		{name:"targetAlias", type:"string"},
		{name:"userFieldId", type:"string"},
		{name:"userFieldName", type:"string"},
		{name:"userFieldDescription", type:"string"},
		{name:"userFieldDefaultValue", type:"string"},
		{name:"userFieldType", type:"string"},
		{name:"businessAreaId", type:"string"},
		{name:"businessArea", type:"string"},
		{name:"dsModel", type:"string"},
		{name:"dsName", type:"string"},
		{name:"dsAlias", type:"string"},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.UserFieldValues_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"value", type:"string"},
		{name:"targetRefid", type:"int", allowNull:true},
		{name:"targetAlias", type:"string"},
		{name:"userFieldId", type:"string"},
		{name:"userFieldName", type:"string"},
		{name:"userFieldDescription", type:"string"},
		{name:"userFieldDefaultValue", type:"string"},
		{name:"userFieldType", type:"string"},
		{name:"businessAreaId", type:"string"},
		{name:"businessArea", type:"string"},
		{name:"dsModel", type:"string"},
		{name:"dsName", type:"string"},
		{name:"dsAlias", type:"string"},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
