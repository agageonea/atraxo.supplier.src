/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.JobLog_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_JobLog_Ds"
	},
	
	
	fields: [
		{name:"startTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"endTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"jobContextId", type:"string"},
		{name:"jobContext", type:"string"},
		{name:"jobName", type:"string"},
		{name:"jobTimerId", type:"string"},
		{name:"jobTimer", type:"string"},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.JobLog_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"startTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"endTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"jobContextId", type:"string"},
		{name:"jobContext", type:"string"},
		{name:"jobName", type:"string"},
		{name:"jobTimerId", type:"string"},
		{name:"jobTimer", type:"string"},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
		,
		{name:"startTime_From",type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"startTime_To",type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"endTime_From",type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"endTime_To",type:"date", dateFormat:Main.MODEL_DATE_FORMAT}
	]
});
