/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.ParamValue_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.ParamValue_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("val", Ext.create(atraxo.ad.ui.extjs.dc.ParamValue_Dc,{multiEdit: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnPublish", disabled:false, handler: this.onBtnPublish, scope:this})
		.addButton({name:"btnShowParamsFrame", disabled:false, handler: this.onBtnShowParamsFrame, scope:this})
		.addDcFilterFormView("val", {name:"valFilter", xtype:"ad_ParamValue_Dc$FilterPG"})
		.addDcEditGridView("val", {name:"valList", xtype:"ad_ParamValue_Dc$EditList", frame:true})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["valList"], ["center"])
		.addToolbarTo("main", "tlbValList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbValList", {dc: "val"})
			.addButtons([])
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnPublish"),this._elems_.get("btnShowParamsFrame")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnPublish
	 */
	,onBtnPublish: function() {
		var o={
			name:"publishValues",
			modal:true
		};
		this._getDc_("val").doRpcFilter(o);
	}
	
	/**
	 * On-Click handler for button btnShowParamsFrame
	 */
	,onBtnShowParamsFrame: function() {
		var bundle = "atraxo.mod.ad";
		var frame = "atraxo.ad.ui.extjs.frame.MyParam_Ui";
		getApplication().showFrameByName(bundle, frame);
	}
});
