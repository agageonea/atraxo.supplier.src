/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.FrameExtension_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_FrameExtension_Ds"
	},
	
	
	validators: {
		frame: [{type: 'presence'}],
		sequenceNo: [{type: 'presence'}],
		fileLocation: [{type: 'presence'}]
	},
	
	fields: [
		{name:"frame", type:"string"},
		{name:"sequenceNo", type:"int", allowNull:true},
		{name:"fileLocation", type:"string"},
		{name:"relativePath", type:"boolean"},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.FrameExtension_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"frame", type:"string"},
		{name:"sequenceNo", type:"int", allowNull:true},
		{name:"fileLocation", type:"string"},
		{name:"relativePath", type:"boolean", allowNull:true},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
