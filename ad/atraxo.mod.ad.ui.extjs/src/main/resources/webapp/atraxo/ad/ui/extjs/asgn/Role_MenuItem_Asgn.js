/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

/* model */
Ext.define("atraxo.ad.ui.extjs.asgn.Role_MenuItem_Asgn$Model", {
	extend: 'Ext.data.Model',
	statics: {
		ALIAS: "ad_Role_MenuItem_Asgn"
	},
	fields:  [
		{name:"id",type:"string"},
		{name:"name",type:"string"},
		{name:"title",type:"string"}
	]
});

/* lists */
Ext.define( "atraxo.ad.ui.extjs.asgn.Role_MenuItem_Asgn$List", {
	extend: "e4e.asgn.AbstractAsgnGrid",
	alias:[ "widget.ad_Role_MenuItem_Asgn$Left","widget.ad_Role_MenuItem_Asgn$Right" ],
	_defineColumns_: function () {
		this._getBuilder_()
		.addTextColumn({name:"id", dataIndex:"id", hidden:true})
		.addTextColumn({name:"name", dataIndex:"name"})
		.addTextColumn({name:"title", dataIndex:"title"})
	} 
});
	
/* ui-window */
Ext.define("atraxo.ad.ui.extjs.asgn.Role_MenuItem_Asgn$Ui", {
	extend: "e4e.asgn.AbstractAsgnUi",
	width:800,
	height:400,
	title:"Assign menu-items to role",
	_filterFields_: [
		["id"],
		["name"],
		["title"]
	],
	_defaultFilterField_ : "name",

	_defineElements_: function () {
		this._getBuilder_()
			.addLeftGrid({ xtype:"ad_Role_MenuItem_Asgn$Left"})
			.addRightGrid({ xtype:"ad_Role_MenuItem_Asgn$Right"})
	}
});

/* ui-panel */
Ext.define("atraxo.ad.ui.extjs.asgn.Role_MenuItem_Asgn$AsgnPanel", {
	extend: "e4e.asgn.AbstractAsgnPanel",
	alias: "widget.ad_Role_MenuItem_Asgn$AsgnPanel",
	width:800,
	height:400,
	title:"Assign menu-items to role",
	_filterFields_: [
		["id"],
		["name"],
		["title"]
	],
	_defaultFilterField_ : "name",

	_defineElements_: function () {
		this._getBuilder_()
			.addLeftGrid({ xtype:"ad_Role_MenuItem_Asgn$Left"})
			.addRightGrid({ xtype:"ad_Role_MenuItem_Asgn$Right"})
	}
});
