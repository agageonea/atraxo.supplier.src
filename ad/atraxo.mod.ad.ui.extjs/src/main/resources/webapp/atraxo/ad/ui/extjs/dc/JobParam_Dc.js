/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.JobParam_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.JobParam_Ds
});

/* ================= GRID: CtxList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.JobParam_Dc$CtxList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_JobParam_Dc$CtxList",
	_noImport_: true,
	_noExport_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:200})
		.addTextColumn({ name:"dataType", dataIndex:"dataType", width:150})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
