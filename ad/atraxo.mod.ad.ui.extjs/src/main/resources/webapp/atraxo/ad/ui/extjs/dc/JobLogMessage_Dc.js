/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.JobLogMessage_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.JobLogMessage_Ds
});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.JobLogMessage_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_JobLogMessage_Dc$List",
	_noImport_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"messageType", dataIndex:"messageType", hidden:true, width:80})
		.addTextColumn({ name:"message", dataIndex:"message", width:400})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
