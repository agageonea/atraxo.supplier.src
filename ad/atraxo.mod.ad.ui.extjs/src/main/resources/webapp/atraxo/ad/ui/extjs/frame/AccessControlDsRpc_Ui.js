/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.AccessControlDsRpc_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.AccessControlDsRpc_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("dsAccess", Ext.create(atraxo.ad.ui.extjs.dc.AccessControlDsRpc_Dc,{multiEdit: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addDcFilterFormView("dsAccess", {name:"dsAccessFilter", xtype:"ad_AccessControlDsRpc_Dc$FilterPG"})
		.addDcEditGridView("dsAccess", {name:"dsAccessEditList", xtype:"ad_AccessControlDsRpc_Dc$EditList", frame:true})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["dsAccessEditList"], ["center"])
		.addToolbarTo("main", "tlbDsAccessEditList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbDsAccessEditList", {dc: "dsAccess"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end();
	}
	

});
