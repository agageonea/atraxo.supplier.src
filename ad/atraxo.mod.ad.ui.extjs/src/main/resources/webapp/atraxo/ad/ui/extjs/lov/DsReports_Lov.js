/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.lov.DsReports_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.ad_DsReports_Lov",
	displayField: "dataSource", 
	_columns_: ["id", "id", "reportId", "dataSource"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{dataSource}</span>';
		},
		width:250, maxHeight:350
	},
	_editFrame_: {
		name: "atraxo.ad.ui.extjs.frame.Report_Ui"
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.ad.ui.extjs.ds.DsReportLov_Ds
});
