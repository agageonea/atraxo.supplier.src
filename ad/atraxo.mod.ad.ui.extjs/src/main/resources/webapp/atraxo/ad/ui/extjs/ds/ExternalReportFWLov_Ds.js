/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.ExternalReportFWLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_ExternalReportFWLov_Ds"
	},
	
	
	fields: [
		{name:"id", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"name", type:"string"},
		{name:"reportCode", type:"string"},
		{name:"description", type:"string"},
		{name:"system", type:"boolean"},
		{name:"businessAreaId", type:"string"},
		{name:"businessArea", type:"string"},
		{name:"designName", type:"string"},
		{name:"dsName", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.ExternalReportFWLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"id", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"name", type:"string"},
		{name:"reportCode", type:"string"},
		{name:"description", type:"string"},
		{name:"system", type:"boolean", allowNull:true},
		{name:"businessAreaId", type:"string"},
		{name:"businessArea", type:"string"},
		{name:"designName", type:"string"},
		{name:"dsName", type:"string"}
	]
});
