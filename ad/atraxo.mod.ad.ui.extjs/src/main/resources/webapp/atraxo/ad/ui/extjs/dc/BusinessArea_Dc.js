/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.BusinessArea_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.BusinessArea_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ad.ui.extjs.dc.BusinessArea_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_BusinessArea_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:255})
			.addLov({name:"dsName", dataIndex:"dsName", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.DataSources_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"name", dsField: "dsName"} ]}})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.BusinessArea_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_BusinessArea_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"dsName", dataIndex:"dsName", width:120, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"ad_DataSources_Lov", allowBlank:false, maxLength:255,
				retFieldMapping: [{lovField:"name", dsField: "dsName"} ,{lovField:"model", dsField: "dsModel"} ]},  flex:1})
		.addTextColumn({name:"dsModel", dataIndex:"dsModel", width:120, noEdit: true, allowBlank: false, maxLength:255,  flex:1})
		.addTextColumn({name:"dsAlias", dataIndex:"dsAlias", width:120, noEdit: true, allowBlank: false, maxLength:255,  flex:1})
		.addTextColumn({name:"name", dataIndex:"name", width:200, allowBlank: false, maxLength:255,  flex:1, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:255}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
