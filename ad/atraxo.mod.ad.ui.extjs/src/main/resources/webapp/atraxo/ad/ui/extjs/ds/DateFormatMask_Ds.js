/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.DateFormatMask_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_DateFormatMask_Ds"
	},
	
	
	fields: [
		{name:"mask", type:"string"},
		{name:"value", type:"string"},
		{name:"dateFormatId", type:"string"},
		{name:"dateFormat", type:"string"},
		{name:"id", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.DateFormatMask_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"mask", type:"string"},
		{name:"value", type:"string"},
		{name:"dateFormatId", type:"string"},
		{name:"dateFormat", type:"string"},
		{name:"id", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
