/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.DataSource_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.DataSource_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.DataSource_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_DataSource_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"name", bind:"{d.name}", dataIndex:"name", xtype:"ad_DataSources_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "id"} ]})
		.addTextField({ name:"model", bind:"{d.model}", dataIndex:"model", maxLength:255})
		.addBooleanField({ name:"isAsgn", bind:"{d.isAsgn}", dataIndex:"isAsgn"})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:250, layout:"anchor"})
		.addPanel({ name:"col2", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["name", "model"])
		.addChildrenTo("col2", ["isAsgn"]);
	}
});

/* ================= FILTER: FilterPG ================= */


Ext.define("atraxo.ad.ui.extjs.dc.DataSource_Dc$FilterPG", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_DataSource_Dc$FilterPG",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"name", dataIndex:"name", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.DataSources_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "id"} ]}})
			.addTextField({ name:"model", dataIndex:"model", maxLength:255})
			.addBooleanField({ name:"isAsgn", dataIndex:"isAsgn"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.DataSource_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_DataSource_Dc$List",
	_noImport_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:250})
		.addTextColumn({ name:"model", dataIndex:"model", width:500})
		.addBooleanColumn({ name:"isAsgn", dataIndex:"isAsgn", width:100})
		.addTextColumn({ name:"description", dataIndex:"description", hidden:true, width:200})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
