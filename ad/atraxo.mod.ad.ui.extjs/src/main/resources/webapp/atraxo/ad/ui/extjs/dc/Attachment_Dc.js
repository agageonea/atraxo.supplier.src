/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.Attachment_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.Attachment_Ds,
		
			/* ================ Business functions ================ */
	
	doNew: function(options) {
		
					this._passedOptions_ = options;
		
					var wdwName = (options && options.wdwName) ? options.wdwName : "ad_Attachment_Dc$New";
					var wdwHeight = (options && options.wdwHeight) ? options.wdwHeight : 275 ;
					var wdwTitle = (options && options.wdwTitle) ? options.wdwTitle : Main.translate("applicationMsg", "addNewDocumnet__lbl") ;
		
					if (this._doNewWdw_ == null ) {
						this._doNewWdw_ = Ext.create("Ext.window.Window", {
							width:390, 
							height:wdwHeight,
							title:wdwTitle,
							closable: false,
							closeAction: "hide",
							resizable:true, 
							layout:"fit", 
							modal:true,
							items: {
								_controller_:this,
								xtype: wdwName
							},
							listeners: {
								close: {
									scope: this,
									fn: function() {
										this.fireEvent("afterAttachmentWindowClose");
									}
								}
							},
							tools: [{
				                type : "close",
								listeners: {
									click: {
										scope: this,
										fn: function() {
											this._doNewWdw_.close();
											this.doCancel();
										}
									}
								}
				            }]
						});
					}
					this._doNewWdw_.show();
					this.commands.doNew.execute();
	}

});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Attachment_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_Attachment_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", maxLength:255})
		.addLov({name:"type", bind:"{d.type}", dataIndex:"type", xtype:"ad_AttachmentTypes_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "typeId"} ]})
		.addTextField({ name:"targetType", bind:"{d.targetType}", dataIndex:"targetType", maxLength:255})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"})
		.addPanel({ name:"col2", width:250, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["name", "type"])
		.addChildrenTo("col2", ["targetType"]);
	}
});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Attachment_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_Attachment_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"type", dataIndex:"type", width:200})
		.addTextColumn({ name:"name", dataIndex:"name", width:200,  renderer:function(val, meta, record, rowIndex) { meta.style="cursor:pointer"; return '<a style="color: #0179B5; text-decoration:underline" href="'+record.data.url+'" target="_blank">'+record.data.name+'</a>'; }})
		.addTextColumn({ name:"notes", dataIndex:"notes", width:130,  flex:1})
		.addTextColumn({ name:"url", dataIndex:"url", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= GRID: attachmentAssignList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Attachment_Dc$attachmentAssignList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_Attachment_Dc$attachmentAssignList",
	_noExport_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"type", dataIndex:"type", width:200})
		.addTextColumn({ name:"name", dataIndex:"name", width:200,  renderer:function(val, meta, record, rowIndex) { meta.style="cursor:pointer"; return '<a style="color: #0179B5; text-decoration:underline" href="'+record.data.url+'" target="_blank">'+record.data.name+'</a>'; }})
		.addTextColumn({ name:"notes", dataIndex:"notes", width:130,  flex:1})
		.addTextColumn({ name:"url", dataIndex:"url", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: New ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Attachment_Dc$New", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ad_Attachment_Dc$New",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btnSelectFile", scope: this, handler: this._selectFile_, text: "Select file"})
		.addButton({name:"btnSaveLink", scope: this, handler: this._saveLink_, text: "Save", _enableFn_: function(dc, rec) { return this._canAddLocation_(rec); } })
		.addButton({name:"btnCancel", scope: this, handler: this._cancel_, text: "Cancel"})
		.addLov({name:"type", bind:"{d.type}", dataIndex:"type", allowBlank:false, width:215, xtype:"ad_AttachmentTypes_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "typeId"} ,{lovField:"category", dsField: "category"} ],
			filterFieldMapping: [{lovField:"targetAlias", dsField: "targetAlias"}, {lovField:"targetType", dsField: "targetType"} ],listeners:{
			fpchange:{scope:this, fn:this._enableLocation_},
			change:{scope:this, fn:this._save_}
		}})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, width:325, maxLength:255})
		.addTextArea({ name:"notes", bind:"{d.notes}", dataIndex:"notes", width:325})
		.addTextField({ name:"location", bind:"{d.location}", dataIndex:"location", _enableFn_: function(dc, rec) { return this._canAddLocation_(rec); } , width:325, maxLength:1000})
		.addUploadField({ name:"file", bind:"{d.filePath}", dataIndex:"filePath", style:"display:none", listeners:{change: {scope: this,fn:function(e) {this._save_()}}}})
		.addHiddenField({ name:"typeId", bind:"{d.typeId}", dataIndex:"typeId"})
		.addHiddenField({ name:"targetRefid", bind:"{d.targetRefid}", dataIndex:"targetRefid"})
		.addHiddenField({ name:"targetType", bind:"{d.targetType}", dataIndex:"targetType"})
		.addHiddenField({ name:"targetAlias", bind:"{d.targetAlias}", dataIndex:"targetAlias"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, buttons: [this._getConfig_("btnSelectFile"),this._getConfig_("btnSaveLink"),this._getConfig_("btnCancel")],  xtype:"panel", buttonAlign:"left"})
		.addPanel({ name:"col", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col"])
		.addChildrenTo("col", ["name", "type", "location", "file", "notes", "targetRefid", "targetType", "targetAlias", "typeId"]);
	},
	/* ==================== Business functions ==================== */
	
	_selectFile_: function() {
		
						var fileUploadField = this._get_("file");
						var elemId = fileUploadField.button.fileInputEl.dom.getAttribute("id");
						var elem = document.getElementById(elemId);
		
					   if(elem && document.createEvent) {
					      var evt = document.createEvent("MouseEvents");
					      evt.initEvent("click", true, false);
					      elem.dispatchEvent(evt);
					   }
	},
	
	_save_: function() {
		
						var me = this;
						var fileUploadField = this._get_("file");
						var uploadFieldValue = fileUploadField.getValue();
		
						if (me.isValid() && !Ext.isEmpty(uploadFieldValue)) {
							this._doUploadFile_();
						}
	},
	
	_saveLink_: function() {
		
						if(this.isValid()){
							this._controller_.doSave();
							this.up("window").hide();
						}
	},
	
	_cancel_: function() {
		
						this.up("window").hide();
						this._controller_.doCancel();
	},
	
	_canAddLocation_: function(record) {
		
						return record != null && record.data.category == "link"
	},
	
	_enableLocation_: function(el) {
		
						if (el._fpRawValue_ != el._onFocusVal_) {
							// get ui elements
							var location = this._get_("location");
							var selectFileBtn = this._get_("btnSelectFile");
							var btnSaveLink = this._get_("btnSaveLink");
		
							// get record
							var dc = this._controller_;
							var record = dc.getRecord();
		
							// togle location based on the attachment type category
							if(this._canAddLocation_(record)){
							   // enable element
							   location.setReadOnly(false);
		
							   selectFileBtn._disable_();
							   btnSaveLink._enable_();
							}else{
							   // reset ui element
							   location._safeSetValue_("");
		
							   // reset record data
							   record.set("location", null);
		
							   // disable element
							   location.setReadOnly(true);
		
							   selectFileBtn._enable_();
							   btnSaveLink._disable_();
							}
						}
	},
	
	_doUploadFile_: function() {
		
						
						var window = this._controller_._doNewWdw_;
						var form = window.items.get(0);
						var passedOptions = this._controller_._passedOptions_;
						var cfg = {
				                _handler_ : "uploadAttachment",
				                _succesCallbackScope_ : this._controller_,
								_succesCallbackFn_ : function() {
									
									this.store.commitChanges();
									this.doReloadPage();
									window.close();
								}
				        };
						Ext.apply(window,cfg);
						var formCfg = {
							fileUpload : true
						}
						Ext.apply(form,formCfg);
						if (form.getForm().isValid()) {
							form.getForm().submit(
									{
										url : Main.urlUpload + "/" + window._handler_,
										waitMsg : Main.translate("msg", "uploading"),
										scope : window,
										success : function() {
											if (passedOptions && !Ext.isEmpty(passedOptions.postDoNewFn)) {
												passedOptions.postDoNewFn(passedOptions.postNewCtrl,passedOptions.postRefreshCtrl1,passedOptions.postRefreshCtrl2);
											}
		
											try {
												Ext.Msg.hide();
											} catch (e) {
												// nothing to do
											}
											if (this._succesCallbackFn_ != null) {
												this._succesCallbackFn_.call(this._succesCallbackScope_ || this);
											}
										},
										failure : function(form, action) {
											try {
												Ext.Msg.hide();
											} catch (e) {
												// nothing to do
											}
											if( action.failureType === Ext.form.Action.CLIENT_INVALID ){
												Main.error("INVALID_FORM");
											} else {
												Main.serverMessage(null, action.response);
											}
										}
									});
						} else {
							Main.error("INVALID_FORM");
						}
		
	}
});

/* ================= EDIT FORM: NewWithName ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Attachment_Dc$NewWithName", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ad_Attachment_Dc$NewWithName",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btnSelectFile", scope: this, handler: this._selectFile_, text: "Select file"})
		.addButton({name:"btnCancel", scope: this, handler: this._cancel_, text: "Cancel"})
		.addLov({name:"type", bind:"{d.type}", dataIndex:"type", allowBlank:false, width:215, xtype:"ad_AttachmentTypes_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "typeId"} ,{lovField:"category", dsField: "category"} ],
			filterFieldMapping: [{lovField:"targetAlias", dsField: "targetAlias"}, {lovField:"targetType", dsField: "targetType"} ]})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, width:325, maxLength:255})
		.addUploadField({ name:"file", bind:"{d.filePath}", dataIndex:"filePath", style:"display:none", listeners:{change: {scope: this,fn:function(e) {this._save_()}}}})
		.addHiddenField({ name:"typeId", bind:"{d.typeId}", dataIndex:"typeId"})
		.addHiddenField({ name:"targetRefid", bind:"{d.targetRefid}", dataIndex:"targetRefid"})
		.addHiddenField({ name:"targetType", bind:"{d.targetType}", dataIndex:"targetType"})
		.addHiddenField({ name:"targetAlias", bind:"{d.targetAlias}", dataIndex:"targetAlias"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, buttons: [this._getConfig_("btnSelectFile"),this._getConfig_("btnCancel")],  xtype:"panel", buttonAlign:"left"})
		.addPanel({ name:"col", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col"])
		.addChildrenTo("col", ["name", "type", "file", "targetRefid", "targetType", "targetAlias", "typeId"]);
	},
	/* ==================== Business functions ==================== */
	
	_selectFile_: function() {
		
						var fileUploadField = this._get_("file");
						var ctrl = this._controller_;
						var frame = ctrl.getFrame();
						var importBidDc = frame._getDc_("importBid");
		
						var elemId = fileUploadField.button.fileInputEl.dom.getAttribute("id");
						var elem = document.getElementById(elemId);
		
						if (importBidDc._uploadAcceptedFileTypes_) {
							elem.setAttribute("accept",importBidDc._uploadAcceptedFileTypes_);
						}
					   if(elem && document.createEvent) {
					      var evt = document.createEvent("MouseEvents");
					      evt.initEvent("click", true, false);
					      elem.dispatchEvent(evt);
					   }
	},
	
	_save_: function() {
		 
						var me = this;
						var fileUploadField = this._get_("file");
						var uploadFieldValue = fileUploadField.getValue();
						var win
						if (me.isValid() && !Ext.isEmpty(uploadFieldValue)) {
							this._doUploadFile_();
						}
	},
	
	_cancel_: function() {
		
						this.up("window").hide();
						this._controller_.doCancel();
	},
	
	_doUploadFile_: function() {
		
						
						var window = this._controller_._doNewWdw_;
						var form = window.items.get(0);
						var passedOptions = this._controller_._passedOptions_;
						var cfg = {
				                _handler_ : "uploadAttachment",
				                _succesCallbackScope_ : this._controller_,
								_succesCallbackFn_ : function() {
									
									this.store.commitChanges();
									this.doReloadPage();
									window.close();
								}
				        };
						Ext.apply(window,cfg);
						var formCfg = {
							fileUpload : true
						}
						Ext.apply(form,formCfg);
						if (form.getForm().isValid()) {
							form.getForm().submit(
									{
										url : Main.urlUpload + "/" + window._handler_,
										waitMsg : Main.translate("msg", "uploading"),
										scope : window,
										success : function() {
											if (!Ext.isEmpty(passedOptions.postDoNewFn)) {
												passedOptions.postDoNewFn(passedOptions.postNewCtrl,passedOptions.postRefreshCtrl1,passedOptions.postRefreshCtrl2);
											}
		
											try {
												Ext.Msg.hide();
											} catch (e) {
												// nothing to do
											}
											if (this._succesCallbackFn_ != null) {
												this._succesCallbackFn_.call(this._succesCallbackScope_ || this);
											}
										},
										failure : function(form, action) {
											try {
												Ext.Msg.hide();
											} catch (e) {
												// nothing to do
											}
											if( action.failureType === Ext.form.Action.CLIENT_INVALID ){
												Main.error("INVALID_FORM");
											} else {
												Main.serverMessage(null, action.response);
											}
										}
									});
						} else {
							Main.error("INVALID_FORM");
						}
		
	}
});
