/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.DsReportParam_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.DsReportParam_Ds
});

/* ================= EDIT-GRID: CtxEditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.DsReportParam_Dc$CtxEditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_DsReportParam_Dc$CtxEditList",
	_noImport_: true,
	_noExport_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"param", dataIndex:"param", width:200, xtype:"gridcolumn", 
			editor:{xtype:"ad_ReportParams_Lov", maxLength:255,
				retFieldMapping: [{lovField:"id", dsField: "paramId"} ,{lovField:"title", dsField: "paramTitle"} ],
				filterFieldMapping: [{lovField:"reportId", dsField: "reportId"} ]}})
		.addTextColumn({name:"paramTitle", dataIndex:"paramTitle", width:200, noEdit: true, maxLength:255})
		.addLov({name:"dsField", dataIndex:"dsField", width:200, xtype:"gridcolumn", 
			editor:{xtype:"ad_DataSourceFields_Lov", maxLength:255,
				filterFieldMapping: [{lovField:"dataSourceName", dsField: "dataSource"} ]}})
		.addTextColumn({name:"staticValue", dataIndex:"staticValue", width:200, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
