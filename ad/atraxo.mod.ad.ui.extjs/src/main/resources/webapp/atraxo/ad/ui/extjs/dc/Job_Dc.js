/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.Job_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.Job_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Job_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_Job_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"name", bind:"{d.name}", dataIndex:"name", xtype:"ad_Jobs_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "id"} ]})
		.addTextField({ name:"javaClass", bind:"{d.javaClass}", dataIndex:"javaClass", maxLength:255})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"})
		.addPanel({ name:"col2", width:250, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["name"])
		.addChildrenTo("col2", ["javaClass"]);
	}
});

/* ================= FILTER: FilterPG ================= */


Ext.define("atraxo.ad.ui.extjs.dc.Job_Dc$FilterPG", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_Job_Dc$FilterPG",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"name", dataIndex:"name", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.Jobs_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "id"} ]}})
			.addTextField({ name:"javaClass", dataIndex:"javaClass", maxLength:255})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Job_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_Job_Dc$List",
	_noImport_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:200})
		.addTextColumn({ name:"javaClass", dataIndex:"javaClass", width:450})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
