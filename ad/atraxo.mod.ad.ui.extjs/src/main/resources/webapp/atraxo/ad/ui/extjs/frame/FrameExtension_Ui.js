/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.FrameExtension_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.FrameExtension_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("extensions", Ext.create(atraxo.ad.ui.extjs.dc.FrameExtension_Dc,{multiEdit: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addDcFilterFormView("extensions", {name:"extensionsFilter", xtype:"ad_FrameExtension_Dc$FilterPG"})
		.addDcEditGridView("extensions", {name:"extensionsEditList", xtype:"ad_FrameExtension_Dc$EditList", frame:true})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["extensionsEditList"], ["center"])
		.addToolbarTo("main", "tlbExtensionsEditList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbExtensionsEditList", {dc: "extensions"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end();
	}
	

});
