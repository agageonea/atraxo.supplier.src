/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.DsReportLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_DsReportLov_Ds"
	},
	
	
	fields: [
		{name:"reportId", type:"string"},
		{name:"reportCode", type:"string"},
		{name:"dataSource", type:"string"},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"refid", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.DsReportLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"reportId", type:"string"},
		{name:"reportCode", type:"string"},
		{name:"dataSource", type:"string"},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"refid", type:"string"}
	]
});
