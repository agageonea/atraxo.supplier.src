/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.AccessControlDsRpc_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.AccessControlDsRpc_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.AccessControlDsRpc_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_AccessControlDsRpc_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"dsName", bind:"{d.dsName}", dataIndex:"dsName", xtype:"ad_DataSourcesDs_Lov", maxLength:255})
		.addLov({name:"accessControl", bind:"{d.accessControl}", dataIndex:"accessControl", xtype:"ad_AccessControls_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "accessControlId"} ]})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:250, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["accessControl", "dsName"]);
	}
});

/* ================= FILTER: FilterPG ================= */


Ext.define("atraxo.ad.ui.extjs.dc.AccessControlDsRpc_Dc$FilterPG", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_AccessControlDsRpc_Dc$FilterPG",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"dsName", dataIndex:"dsName", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.DataSourcesDs_Lov", selectOnFocus:true, maxLength:255}})
			.addLov({name:"accessControl", dataIndex:"accessControl", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.AccessControls_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "accessControlId"} ]}})
		;
	}

});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.AccessControlDsRpc_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_AccessControlDsRpc_Dc$EditList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"accessControl", dataIndex:"accessControl", width:200, xtype:"gridcolumn", 
			editor:{xtype:"ad_AccessControls_Lov", maxLength:255,
				retFieldMapping: [{lovField:"id", dsField: "accessControlId"} ]}})
		.addLov({name:"dsName", dataIndex:"dsName", width:200, xtype:"gridcolumn", 
			editor:{xtype:"ad_DataSourcesDs_Lov", maxLength:255}})
		.addLov({name:"serviceMethod", dataIndex:"serviceMethod", width:200, xtype:"gridcolumn", 
			editor:{xtype:"ad_DataSourceRpcs_Lov", maxLength:255,
				filterFieldMapping: [{lovField:"dataSourceName", dsField: "dsName"} ]}})
		.addTextColumn({name:"accessControlId", dataIndex:"accessControlId", hidden:true, width:100, noEdit: true, maxLength:64})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT-GRID: CtxEditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.AccessControlDsRpc_Dc$CtxEditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_AccessControlDsRpc_Dc$CtxEditList",
	_noImport_: true,
	_noExport_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"dsName", dataIndex:"dsName", width:200, xtype:"gridcolumn", 
			editor:{xtype:"ad_DataSourcesDs_Lov", maxLength:255}})
		.addLov({name:"serviceMethod", dataIndex:"serviceMethod", width:200, xtype:"gridcolumn", 
			editor:{xtype:"ad_DataSourceRpcs_Lov", maxLength:255,
				filterFieldMapping: [{lovField:"dataSourceName", dsField: "dsName"} ]}})
		.addTextColumn({name:"accessControlId", dataIndex:"accessControlId", hidden:true, width:200, noEdit: true, maxLength:64})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
