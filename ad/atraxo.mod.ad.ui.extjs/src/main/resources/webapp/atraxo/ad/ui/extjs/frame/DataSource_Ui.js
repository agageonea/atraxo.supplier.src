/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.DataSource_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.DataSource_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("ds", Ext.create(atraxo.ad.ui.extjs.dc.DataSource_Dc,{}))
		.addDc("fields", Ext.create(atraxo.ad.ui.extjs.dc.DataSourceField_Dc,{}))
		.addDc("rpcs", Ext.create(atraxo.ad.ui.extjs.dc.DataSourceRpc_Dc,{}))
		.linkDc("fields", "ds",{fetchMode:"auto",fields:[
					{childField:"dataSourceId", parentField:"id"}]})
				.linkDc("rpcs", "ds",{fetchMode:"auto",fields:[
					{childField:"dataSourceId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnSynchronize",glyph:fp_asc.gear_glyph.glyph, disabled:false, handler: this.onBtnSynchronize, scope:this})
		.addButton({name:"btnInfo",glyph:fp_asc.list_glyph.glyph,iconCls: fp_asc.list_glyph.css, disabled:true, handler: this.showDsInfo,stateManager:[{ name:"selected_one", dc:"ds", and: function(dc) {return ( ! dc.getRecord().get('isAsgn') );} }], scope:this})
		.addDcFilterFormView("ds", {name:"dsFilter", xtype:"ad_DataSource_Dc$FilterPG"})
		.addDcGridView("ds", {name:"dsList", xtype:"ad_DataSource_Dc$List"})
		.addDcGridView("fields", {name:"fieldsList", _hasTitle_:true, xtype:"ad_DataSourceField_Dc$CtxList"})
		.addDcGridView("rpcs", {name:"rpcsList", _hasTitle_:true, xtype:"ad_DataSourceRpc_Dc$CtxList"})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"panelDetails", width:500, xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"panelDetails", containerPanelName:"main", dcName:"fields"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["dsList", "panelDetails"], ["center", "east"])
		.addChildrenTo("panelDetails", ["fieldsList"])
		.addToolbarTo("main", "tlbDsList")
		.addToolbarTo("fieldsList", "tlbFieldsList")
		.addToolbarTo("rpcsList", "tlbRpcsList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("panelDetails", ["rpcsList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbDsList", {dc: "ds"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnSynchronize"),this._elems_.get("btnInfo")])
			.addReports()
		.end()
		.beginToolbar("tlbFieldsList", {dc: "fields"})
			.addReports()
		.end()
		.beginToolbar("tlbRpcsList", {dc: "rpcs"})
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnSynchronize
	 */
	,onBtnSynchronize: function() {
		var successFn = function() {
			this._getDc_("ds").doQuery();
		};
		var o={
			name:"synchronizeCatalog",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("ds").doRpcFilter(o);
	}
	
	,_afterDefineElements_: function() {
		
						if (!getApplication().getSession().user.systemUser) {
						         this._getBuilder_()
						    .change("btnSynchronize", {disabled: true});
						}
	}
	
	,showDsInfo: function() {
		
						var rd = this._getDc_("ds").getRecord(); 
						var w=window.open( Main.dsAPI(rd.get("name"),"html").info ,"DataSourceInfo","width=600,height=500,scrollbars=yes");
						w.focus();
	}
});
