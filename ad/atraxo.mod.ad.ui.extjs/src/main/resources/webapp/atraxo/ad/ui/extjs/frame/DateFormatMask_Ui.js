/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.DateFormatMask_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.DateFormatMask_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("mask", Ext.create(atraxo.ad.ui.extjs.dc.DateFormatMask_Dc,{multiEdit: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addDcFilterFormView("mask", {name:"maskFilter", xtype:"ad_DateFormatMask_Dc$Filter"})
		.addDcEditGridView("mask", {name:"maskEditList", xtype:"ad_DateFormatMask_Dc$EditList", frame:true})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["maskEditList"], ["center"])
		.addToolbarTo("main", "tlbMaskList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbMaskList", {dc: "mask"})
			.addQuery().addSave().addCancel()
			.addReports()
		.end();
	}
	

});
