/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.AccessControl_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.AccessControl_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("ctrl", Ext.create(atraxo.ad.ui.extjs.dc.AccessControl_Dc,{multiEdit: true}))
		.addDc("dsAccess", Ext.create(atraxo.ad.ui.extjs.dc.AccessControlDs_Dc,{multiEdit: true}))
		.addDc("asgnAccess", Ext.create(atraxo.ad.ui.extjs.dc.AccessControlAsgn_Dc,{multiEdit: true}))
		.addDc("dsMtdAccess", Ext.create(atraxo.ad.ui.extjs.dc.AccessControlDsRpc_Dc,{multiEdit: true}))
		.addDc("rol", Ext.create(atraxo.ad.ui.extjs.dc.Role_Dc,{}))
		.linkDc("dsAccess", "ctrl",{fetchMode:"auto",fields:[
					{childField:"accessControlId", parentField:"id"}]})
				.linkDc("asgnAccess", "ctrl",{fetchMode:"auto",fields:[
					{childField:"accessControlId", parentField:"id"}]})
				.linkDc("dsMtdAccess", "ctrl",{fetchMode:"auto",fields:[
					{childField:"accessControlId", parentField:"id"}]})
				.linkDc("rol", "ctrl",{fetchMode:"auto",fields:[
					{childParam:"withPrivilegeId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnCopyRules",glyph:fp_asc.copy_glyph.glyph,iconCls: fp_asc.copy_glyph.css, disabled:true, handler: this.onBtnCopyRules,stateManager:[{ name:"selected_one_clean", dc:"ctrl"}], scope:this})
		.addButton({name:"btnCopyRulesExec",glyph:fp_asc.copy_glyph.glyph,iconCls: fp_asc.copy_glyph.css, disabled:false, handler: this.onBtnCopyRulesExec, scope:this})
		.addButton({name:"btnAsgnRoles",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnAsgnRoles,stateManager:[{ name:"selected_one_clean", dc:"ctrl"}], scope:this})
		.addButton({name:"btnShowUiAsgnRules",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnShowUiAsgnRules, scope:this})
		.addButton({name:"btnShowUiDsRules",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnShowUiDsRules, scope:this})
		.addButton({name:"btnShowUiServiceRules",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnShowUiServiceRules, scope:this})
		.addButton({name:"btnDetailsRole",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnDetailsRole,stateManager:[{ name:"selected_one", dc:"rol"}], scope:this})
		.addDcFilterFormView("ctrl", {name:"privilegeFilter", xtype:"ad_AccessControl_Dc$FilterPG"})
		.addDcEditGridView("ctrl", {name:"privilegeEditList", xtype:"ad_AccessControl_Dc$EditList", frame:true})
		.addDcFormView("ctrl", {name:"privilegeCopyRules", xtype:"ad_AccessControl_Dc$CopyRulesFromSource"})
		.addDcEditGridView("dsAccess", {name:"dsAccessCtxEditList", _hasTitle_:true, xtype:"ad_AccessControlDs_Dc$CtxEditList", frame:true})
		.addDcEditGridView("asgnAccess", {name:"asgnAccessCtxEditList", _hasTitle_:true, xtype:"ad_AccessControlAsgn_Dc$CtxEditList", frame:true})
		.addDcEditGridView("dsMtdAccess", {name:"dsMtdAccessCtxEditList", _hasTitle_:true, xtype:"ad_AccessControlDsRpc_Dc$CtxEditList", frame:true})
		.addDcGridView("rol", {name:"rolList", _hasTitle_:true, xtype:"ad_Role_Dc$List"})
		.addWindow({name:"wdwCopyRules", _hasTitle_:true, width:400, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("privilegeCopyRules")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCopyRulesExec")]}]})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailTabs", height:"50%", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"detailTabs", containerPanelName:"main", dcName:"dsAccess"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["privilegeEditList", "detailTabs"], ["center", "south"])
		.addChildrenTo("detailTabs", ["dsAccessCtxEditList"])
		.addToolbarTo("main", "tlbCtrlEditList")
		.addToolbarTo("dsAccessCtxEditList", "tlbDsAccessCtxEditList")
		.addToolbarTo("asgnAccessCtxEditList", "tlbAsgnAccessCtxEditList")
		.addToolbarTo("dsMtdAccessCtxEditList", "tlbDsMtdAccessCtxEditList")
		.addToolbarTo("rolList", "tlbRolList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("detailTabs", ["asgnAccessCtxEditList", "dsMtdAccessCtxEditList", "rolList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbCtrlEditList", {dc: "ctrl"})
			.addButtons([])
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCopyRules"),this._elems_.get("btnShowUiDsRules"),this._elems_.get("btnShowUiAsgnRules"),this._elems_.get("btnShowUiServiceRules")])
			.addReports()
		.end()
		.beginToolbar("tlbDsAccessCtxEditList", {dc: "dsAccess"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end()
		.beginToolbar("tlbAsgnAccessCtxEditList", {dc: "asgnAccess"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end()
		.beginToolbar("tlbDsMtdAccessCtxEditList", {dc: "dsMtdAccess"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end()
		.beginToolbar("tlbRolList", {dc: "rol"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnAsgnRoles"),this._elems_.get("btnDetailsRole")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnCopyRules
	 */
	,onBtnCopyRules: function() {
		this._getWindow_("wdwCopyRules").show();
	}
	
	/**
	 * On-Click handler for button btnCopyRulesExec
	 */
	,onBtnCopyRulesExec: function() {
		var successFn = function() {
			this._getWindow_("wdwCopyRules").close();
			this._getDc_("dsAccess").doQuery();
			this._getDc_("asgnAccess").doQuery();
		};
		var o={
			name:"copyRulesFromAccessControl",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("ctrl").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnAsgnRoles
	 */
	,onBtnAsgnRoles: function() {
		this._showAsgnWindow_("atraxo.ad.ui.extjs.asgn.AccessControl_Role_Asgn$Ui" ,{dc: "ctrl", objectIdField: "id"
		,listeners: {close: {scope: this, fn: function() { this._getDc_("rol").doReloadPage();
		}} }});
	}
	
	/**
	 * On-Click handler for button btnShowUiAsgnRules
	 */
	,onBtnShowUiAsgnRules: function() {
		var bundle = "atraxo.mod.ad";
		var frame = "atraxo.ad.ui.extjs.frame.AccessControlAsgn_Ui";
		getApplication().showFrameByName(bundle, frame);
	}
	
	/**
	 * On-Click handler for button btnShowUiDsRules
	 */
	,onBtnShowUiDsRules: function() {
		var bundle = "atraxo.mod.ad";
		var frame = "atraxo.ad.ui.extjs.frame.AccessControlDs_Ui";
		getApplication().showFrameByName(bundle, frame);
	}
	
	/**
	 * On-Click handler for button btnShowUiServiceRules
	 */
	,onBtnShowUiServiceRules: function() {
		var bundle = "atraxo.mod.ad";
		var frame = "atraxo.ad.ui.extjs.frame.AccessControlDsRpc_Ui";
		getApplication().showFrameByName(bundle, frame);
	}
	
	/**
	 * On-Click handler for button btnDetailsRole
	 */
	,onBtnDetailsRole: function() {
		var bundle = "atraxo.mod.ad";
		var frame = "atraxo.ad.ui.extjs.frame.Role_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				id: this._getDc_("rol").getRecord().get("id"),
				code: this._getDc_("rol").getRecord().get("code")
			},
			callback: function (params) {
				this._when_called_for_details(params);
			}
		});
	}
	
	,_when_called_for_details: function(params) {
		
						var dc = this._getDc_("ctrl");
						dc.setFilterValue("id", params.id);
						dc.setFilterValue("name", params.name);
						dc.doQuery();
	}
});
