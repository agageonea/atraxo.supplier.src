/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.ReportParam_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.ReportParam_Ds
});

/* ================= EDIT-GRID: CtxEditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.ReportParam_Dc$CtxEditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_ReportParam_Dc$CtxEditList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"reportCode", dataIndex:"reportCode", hidden:true, width:60, maxLength:64, caseRestriction:"uppercase", 
			editor: { xtype:"textfield", maxLength:64, caseRestriction:"uppercase"}})
		.addNumberColumn({name:"sequenceNo", dataIndex:"sequenceNo", maxLength:4, align:"right" })
		.addTextColumn({name:"name", dataIndex:"name", width:200, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"title", dataIndex:"title", width:200, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addComboColumn({name:"dataType", dataIndex:"dataType", width:100, 
			editor:{xtype:"combo", mode: 'local', triggerAction:'all', forceSelection:true, store:[ __AD__.TDataType._STRING_, __AD__.TDataType._TEXT_, __AD__.TDataType._INTEGER_, __AD__.TDataType._DECIMAL_, __AD__.TDataType._BOOLEAN_, __AD__.TDataType._DATE_]}})
		.addTextColumn({name:"listOfValues", dataIndex:"listOfValues", width:100, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addTextColumn({name:"defaultValue", dataIndex:"defaultValue", width:100, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addBooleanColumn({name:"mandatory", dataIndex:"mandatory", width:60})
		.addBooleanColumn({name:"noEdit", dataIndex:"noEdit", width:60})
		.addBooleanColumn({name:"active", dataIndex:"active", width:60})
		.addTextColumn({name:"notes", dataIndex:"notes", hidden:true, width:200, maxLength:4000, 
			editor: { xtype:"textfield", maxLength:4000}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
