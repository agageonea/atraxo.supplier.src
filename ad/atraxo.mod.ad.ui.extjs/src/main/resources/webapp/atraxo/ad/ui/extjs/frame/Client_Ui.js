/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.Client_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Client_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("client", Ext.create(atraxo.ad.ui.extjs.dc.Client_Dc,{}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"upgrade",glyph:fp_asc.level_up.glyph, disabled:true, handler: this.onUpgrade,stateManager:[{ name:"selected_not_zero", dc:"client"}], scope:this})
		.addButton({name:"upgradeList",glyph:fp_asc.level_up.glyph, disabled:true, handler: this.onUpgradeList,stateManager:[{ name:"selected_not_zero", dc:"client"}], scope:this})
		.addDcFilterFormView("client", {name:"clientFilter", xtype:"ad_Client_Dc$Filter"})
		.addDcGridView("client", {name:"clientList", xtype:"ad_Client_Dc$List"})
		.addDcFormView("client", {name:"clientEdit", xtype:"ad_Client_Dc$Edit"})
		.addDcFormView("client", {name:"clientCreate", xtype:"ad_Client_Dc$Create"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["clientList"], ["center"])
		.addChildrenTo("canvas2", ["clientEdit"], ["center"])
		.addChildrenTo("canvas3", ["clientCreate"], ["center"])
		.addToolbarTo("canvas1", "tlbClientList")
		.addToolbarTo("canvas2", "tlbClientEdit")
		.addToolbarTo("canvas3", "tlbClientCreate");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2", "canvas3"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbClientList", {dc: "client"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas3",autoEdit:"false"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("upgradeList")])
			.addReports()
		.end()
		.beginToolbar("tlbClientEdit", {dc: "client"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("upgrade")])
			.addReports()
		.end()
		.beginToolbar("tlbClientCreate", {dc: "client"})
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button upgrade
	 */
	,onUpgrade: function() {
		var o={
			name:"upgrade",
			modal:true
		};
		this._getDc_("client").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button upgradeList
	 */
	,onUpgradeList: function() {
		var o={
			name:"upgradeList",
			modal:true
		};
		this._getDc_("client").doRpcDataList(o);
	}
});
