/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.ExternalReport_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ad.ui.extjs.ds.ExternalReport_DsParam,
	recordModel: atraxo.ad.ui.extjs.ds.ExternalReport_Ds,
		
			/* ================ Business functions ================ */
	
	canDoDelete: function() {
		
					var selRecords = this.selectedRecords;
		
					for (var i = 0; i < selRecords.length; i++) {
		               if (selRecords[i].data.system == true) {
						  return false;
					   }
					}
					return  true;
	}

});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ad.ui.extjs.dc.ExternalReport_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_ExternalReport_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:255})
			.addTextField({ name:"description", dataIndex:"description", maxLength:1000})
			.addBooleanField({ name:"system", dataIndex:"system"})
			.addDateTimeField({name:"lastExecution", dataIndex:"lastExecution"})
			.addCombo({ xtype:"combo", name:"lastExecutionStatus", dataIndex:"lastExecutionStatus", store:[]})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.ExternalReport_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_ExternalReport_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"name", dataIndex:"name", width:200, allowBlank: false, maxLength:255,  flex:1, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:255}})
		.addTextColumn({name:"description", dataIndex:"description", width:350, allowBlank: false, maxLength:1000, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:1000}})
		.addTextColumn({name:"designName", dataIndex:"designName", width:120, noEdit: true, maxLength:200,  renderer:function(val, meta, record, rowIndex) { 
						meta.style="cursor:pointer";return '<a style="color: #0179B5; text-decoration:underline" href="'+record.data.url+'" target="_blank">'+record.data.designName+'</a>'; 
					}})
		.addLov({name:"businessArea", dataIndex:"businessArea", width:200, xtype:"gridcolumn", 
			editor:{xtype:"ad_BusinessAreaLov_Lov", maxLength:255,
				retFieldMapping: [{lovField:"id", dsField: "businessAreaId"} ]}})
		.addBooleanColumn({name:"system", dataIndex:"system", noEdit: true,  flex:1, renderer:function(val, meta, record, rowIndex) { return this._adjustSystem_(val, meta, record, rowIndex); }})
		.addDateColumn({name:"lastExecution", dataIndex:"lastExecution", noEdit: true, _mask_: Masks.DATETIME,  flex:1 })
		.addTextColumn({name:"lastExecutionStatus", dataIndex:"lastExecutionStatus", width:50, noEdit: true, maxLength:25,  flex:1, renderer:function(val, meta, record, rowIndex) { return this._adjustRequestStatus_(val, meta, record, rowIndex); }})
		.addTextColumn({name:"reportCode", dataIndex:"reportCode", width:120, noEdit: true, maxLength:200})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_adjustSystem_: function(val,meta,record,rowIndex) {
		
						var enabled = record.get("system");
						var newStatus;
						if (enabled == true) {
							newStatus = "System";
						}
						else {
							newStatus = "User defined";
						}
						return newStatus;
	},
	
	_adjustRequestStatus_: function(val,meta,record,rowIndex) {
		
						var lastRequestStatus = record.get("lastExecutionStatus");
						var glyphColor = "#FFFFFF";
						if (lastRequestStatus == __AD__.LastExecutionStatus._SUCCESS_) {
							glyphColor = "#52AA39";
						}
						else if (lastRequestStatus == __AD__.LastExecutionStatus._FAILURE_) {
							glyphColor = "#F94F52";
						}
						var htmlWrapper = "<div style='display:table; table-layout: fixed'><div style='display:table-cell; width:100px; white-space: nowrap; vertical-align:middle'>"+lastRequestStatus+"</div><div style='display:table-cell; text-align: center; vertical-align:middle; padding-left:15px; '><i class='fa fa-circle' style='font-size:16px; line-height: 16px; color: "+glyphColor+"'></i></div></div>";
						return htmlWrapper;
	}
});

/* ================= EDIT FORM: FormatForm ================= */

Ext.define("atraxo.ad.ui.extjs.dc.ExternalReport_Dc$FormatForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ad_ExternalReport_Dc$FormatForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addCombo({ xtype:"combo", name:"format", bind:"{p.format}", paramIndex:"format", store:[ __AD__.ReportFormat._PDF_, __AD__.ReportFormat._CSV_, __AD__.ReportFormat._XLSX_, __AD__.ReportFormat._DOC_]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  layout:"fit"})
		.addPanel({ name:"remarkCol1", layout:"fit"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["remarkCol1"])
		.addChildrenTo("remarkCol1", ["format"]);
	}
});
