/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.UserFields_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.UserFields_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("userFields", Ext.create(atraxo.ad.ui.extjs.dc.UserFields_Dc,{multiEdit: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addDcEditGridView("userFields", {name:"userFieldsList", xtype:"ad_UserFields_Dc$List", frame:true,  listeners:{ beforeedit: { scope: this, fn: function(editor, ctx) { this._setEditor_(editor, ctx, this._get_('userFieldsList'))}}}})
		.addDcFilterFormView("userFields", {name:"userFieldsFilter", xtype:"ad_UserFields_Dc$Filter"})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["userFieldsList"], ["center"])
		.addToolbarTo("userFieldsList", "tlbList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbList", {dc: "userFields"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/UserDefinedFields.html";
					window.open( url, "SONE_Help");
	}
	
	,_setEditor_: function(editor,ctx,view) {
		
		
					var fieldIndex = ctx.field;
					var col = ctx.column;
					var cfg = {};
					var textEditor = {xtype:"textfield", maxLength:32};	
					var numberEditor = {xtype:"numberfield", maxLength:32};	
					var dateEditor = {
						xtype:"datefield",
						_initValue_ : this.value,
						format: Main.DATETIME_FORMAT,
						altFormats: Main.ALT_FORMATS,
						_mask_ : Main.DATETIME_FORMAT,
						selectOnFocus: true,
						listeners: {						
							boxready: {
								scope: this,
								fn: function(el) {
									var dc = view._controller_, value;
									var r = dc.getRecord();
									if (r) {
										if (fieldIndex === "value") {
											value = r.get("value");
										}
										else if (fieldIndex === "defaultValue") {
											value = r.get("defaultValue");
										}
										if (!Ext.isEmpty(value)) {
											el.setValue(new Date(value));
										}									
									}							
								}
							}
						}					
					};	
					var storeData = [];
		
					if (fieldIndex === "defaultValue") {
						var r = ctx.record;
						if (r) {
							var type = r.get("type");
							if (!Ext.isEmpty(type)) {
								if (type === __AD__.UserFieldType._DATETIME_) {
									cfg = dateEditor;						
								}
								else if (type === __AD__.UserFieldType._NUMERIC_ ) {
									cfg = numberEditor;
								}
								else {
									cfg = textEditor;
								}
								col.setEditor(cfg);			
							}
						}
					}
		
					// Clear the value on restriction type change
		
					if (fieldIndex === "type") {
						var ed = col.getEditor();
						ed.on("change", function() {
							var record = view._controller_.getRecord();
							record.set("value", null);
							record.set("defaultValue", null);
						}, this);
					}
		
	}
});
