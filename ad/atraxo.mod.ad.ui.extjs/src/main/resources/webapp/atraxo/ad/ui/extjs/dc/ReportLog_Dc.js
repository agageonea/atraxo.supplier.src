/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.ReportLog_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ad.ui.extjs.ds.ReportLog_DsParam,
	recordModel: atraxo.ad.ui.extjs.ds.ReportLog_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ad.ui.extjs.dc.ReportLog_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_ReportLog_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateField({name:"runDate", dataIndex:"runDate"})
			.addTextField({ name:"name", dataIndex:"name", maxLength:255})
			.addTextField({ name:"runBy", dataIndex:"runBy", maxLength:32})
			.addCombo({ xtype:"combo", name:"format", dataIndex:"format", store:[ __AD__.ReportFormat._PDF_, __AD__.ReportFormat._CSV_, __AD__.ReportFormat._XLSX_, __AD__.ReportFormat._DOC_]})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __AD__.ReportStatus._SUCCESS_, __AD__.ReportStatus._IN_PROGRESS_, __AD__.ReportStatus._FAILED_]})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.ReportLog_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_ReportLog_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"runOn", dataIndex:"runDate", width:300, _mask_: Masks.DATETIME})
		.addTextColumn({ name:"reportName", dataIndex:"name", width:300})
		.addTextColumn({ name:"runBy", dataIndex:"runBy", width:300})
		.addTextColumn({ name:"format", dataIndex:"format", width:300})
		.addTextColumn({ name:"status", dataIndex:"status", width:300})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: LogForm ================= */

Ext.define("atraxo.ad.ui.extjs.dc.ReportLog_Dc$LogForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ad_ReportLog_Dc$LogForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"log", bind:"{d.log}", dataIndex:"log", noEdit:true , width:700, noLabel: true, height:400})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"remarkCol1", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["remarkCol1"])
		.addChildrenTo("remarkCol1", ["log"]);
	}
});
