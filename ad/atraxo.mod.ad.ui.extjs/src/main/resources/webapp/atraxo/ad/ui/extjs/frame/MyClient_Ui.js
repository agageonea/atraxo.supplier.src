/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.MyClient_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.MyClient_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("client", Ext.create(atraxo.ad.ui.extjs.dc.MyClient_Dc,{}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addDcFormView("client", {name:"clientEdit", xtype:"ad_MyClient_Dc$Edit"})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["clientEdit"], ["center"])
		.addToolbarTo("main", "clientEditTlb");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("clientEditTlb", {dc: "client"})
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addReports()
		.end();
	}
	

	
	,loadCurrentClient: function() {	
		this._getDc_("client").doQuery();
	}
	,_afterDefineDcs_: function() {
		this.loadCurrentClient();
	}
});
