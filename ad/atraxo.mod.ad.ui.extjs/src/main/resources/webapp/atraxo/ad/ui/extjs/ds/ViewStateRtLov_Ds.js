/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.ViewStateRtLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_ViewStateRtLov_Ds"
	},
	
	
	fields: [
		{name:"cmp", type:"string"},
		{name:"cmpType", type:"string"},
		{name:"value", type:"string"},
		{name:"owner", type:"string"},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"active", type:"boolean"},
		{name:"refid", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.ViewStateRtLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"cmp", type:"string"},
		{name:"cmpType", type:"string"},
		{name:"value", type:"string"},
		{name:"owner", type:"string"},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"refid", type:"string"}
	]
});

Ext.define("atraxo.ad.ui.extjs.ds.ViewStateRtLov_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"hideMine", type:"boolean"},
		{name:"hideOthers", type:"boolean"}
	]
});
