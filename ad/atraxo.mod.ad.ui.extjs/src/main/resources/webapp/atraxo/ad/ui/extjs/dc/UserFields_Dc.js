/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.UserFields_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.UserFields_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ad.ui.extjs.dc.UserFields_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_UserFields_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:64})
			.addCombo({ xtype:"combo", name:"type", dataIndex:"type", store:[ __AD__.UserFieldType._STRING_, __AD__.UserFieldType._NUMERIC_, __AD__.UserFieldType._DATETIME_]})
			.addTextField({ name:"defaultValue", dataIndex:"defaultValue", maxLength:64})
			.addLov({name:"businessArea", dataIndex:"businessArea", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.BusinessAreaLov_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "businessAreaId"} ]}})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.UserFields_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_UserFields_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"businessArea", dataIndex:"businessArea", width:200, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"ad_BusinessAreaLov_Lov", allowBlank:false, maxLength:255,
				retFieldMapping: [{lovField:"id", dsField: "businessAreaId"} ]},  flex:1})
		.addTextColumn({name:"name", dataIndex:"name", width:120, allowBlank: false, maxLength:64,  flex:1, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:64}})
		.addTextColumn({name:"description", dataIndex:"description", width:200, allowBlank: false, maxLength:1000,  flex:1, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:1000}})
		.addComboColumn({name:"type", dataIndex:"type", width:100, allowBlank: false, 
			editor:{xtype:"combo", mode: 'local', allowBlank:false, triggerAction:'all', forceSelection:true, store:[ __AD__.UserFieldType._STRING_, __AD__.UserFieldType._NUMERIC_, __AD__.UserFieldType._DATETIME_]},  flex:1})
		.addTextColumn({name:"defaultValue", dataIndex:"defaultValue", width:120, allowBlank: false, maxLength:64,  flex:1, renderer:function(v,m,r) {return this._setRenderer_(v,m,r)}, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:64}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_setRenderer_: function(v,m,r) {
		
						var type = r.get("type");
						var formatedDate = v;
						if (type === __AD__.UserFieldType._DATETIME_) {
							formatedDate = Ext.util.Format.date(v, Main.DATETIME_FORMAT)
						}
						return formatedDate;
	}
});

/* ================= EDIT-GRID: DialogList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.UserFields_Dc$DialogList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_UserFields_Dc$DialogList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"businessArea", dataIndex:"businessArea", width:200, noEdit: true, maxLength:255,  flex:1})
		.addTextColumn({name:"name", dataIndex:"name", width:120, allowBlank: false, maxLength:64,  flex:1, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:64}})
		.addTextColumn({name:"description", dataIndex:"description", width:200, allowBlank: false, maxLength:1000,  flex:1, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:1000}})
		.addComboColumn({name:"type", dataIndex:"type", width:100, noEdit: true,  flex:1})
		.addTextColumn({name:"defaultValue", dataIndex:"defaultValue", width:120, noEdit: true, maxLength:64,  flex:1, renderer:function(v,m,r) {return this._setRenderer_(v,m,r)}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_setRenderer_: function(v,m,r) {
		
						var type = r.get("type");
						var formatedDate = v;
						if (type === __AD__.UserFieldType._DATETIME_) {
							formatedDate = Ext.util.Format.date(v, Main.DATETIME_FORMAT)
						}
						return formatedDate;
	}
});
