/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.JobLog_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.JobLog_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("log", Ext.create(atraxo.ad.ui.extjs.dc.JobLog_Dc,{}))
		.addDc("msg", Ext.create(atraxo.ad.ui.extjs.dc.JobLogMessage_Dc,{}))
		.linkDc("msg", "log",{fetchMode:"auto",fields:[
					{childField:"jobLogId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnShowJob", disabled:true, handler: this.onBtnShowJob,stateManager:[{ name:"selected_one", dc:"log"}], scope:this})
		.addButton({name:"btnShowJobContext", disabled:true, handler: this.onBtnShowJobContext,stateManager:[{ name:"selected_one", dc:"log"}], scope:this})
		.addButton({name:"btnShowJobTimer", disabled:true, handler: this.onBtnShowJobTimer,stateManager:[{ name:"selected_one", dc:"log"}], scope:this})
		.addDcFilterFormView("log", {name:"logFilter", xtype:"ad_JobLog_Dc$Filter"})
		.addDcGridView("log", {name:"logList", _hasTitle_:true, xtype:"ad_JobLog_Dc$List"})
		.addDcGridView("msg", {name:"msgList", _hasTitle_:true, xtype:"ad_JobLogMessage_Dc$List"})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["logFilter", "logList", "msgList"], ["north", "center", "east"])
		.addToolbarTo("main", "tlbLogList")
		.addToolbarTo("msgList", "tlbMsgList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbLogList", {dc: "log"})
			.addButtons([])
			.addQuery()
			.addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnShowJob"),this._elems_.get("btnShowJobContext"),this._elems_.get("btnShowJobTimer")])
			.addReports()
		.end()
		.beginToolbar("tlbMsgList", {dc: "msg"})
			.addQuery()
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnShowJob
	 */
	,onBtnShowJob: function() {
		var bundle = "atraxo.mod.ad";
		var frame = "atraxo.ad.ui.extjs.frame.Job_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				jobName: this._getDc_("log").getRecord().get("jobName")
			},
			callback: function (params) {
				this._when_called_to_view_(params);
			}
		});
	}
	
	/**
	 * On-Click handler for button btnShowJobContext
	 */
	,onBtnShowJobContext: function() {
		var bundle = "atraxo.mod.ad";
		var frame = "atraxo.ad.ui.extjs.frame.JobContext_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				id: this._getDc_("log").getRecord().get("jobContextId")
			},
			callback: function (params) {
				this._when_called_to_view_(params);
			}
		});
	}
	
	/**
	 * On-Click handler for button btnShowJobTimer
	 */
	,onBtnShowJobTimer: function() {
		var bundle = "atraxo.mod.ad";
		var frame = "atraxo.ad.ui.extjs.frame.JobTimer_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				id: this._getDc_("log").getRecord().get("jobTimerId")
			},
			callback: function (params) {
				this._when_called_to_view_(params);
			}
		});
	}
	
	,_when_called_to_view_: function(params) {
		
						var log = this._getDc_("log");
						if (log.isDirty()) {
							this._alert_dirty_();
							return;
						}
						log.doClearQuery();	 
						log.setFilterValue("id", params.id );
						log.doQuery();
	}
});
