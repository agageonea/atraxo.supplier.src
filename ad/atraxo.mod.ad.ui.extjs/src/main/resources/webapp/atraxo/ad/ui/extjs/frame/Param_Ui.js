/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.Param_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Param_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("sysparam", Ext.create(atraxo.ad.ui.extjs.dc.Param_Dc,{}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnSynchronize",glyph:fp_asc.gear_glyph.glyph, disabled:false, handler: this.onBtnSynchronize, scope:this})
		.addDcFilterFormView("sysparam", {name:"sysparamFilter", xtype:"ad_Param_Dc$FilterPG"})
		.addDcGridView("sysparam", {name:"sysparamList", xtype:"ad_Param_Dc$List"})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["sysparamList"], ["center"])
		.addToolbarTo("main", "tlbSysparamList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbSysparamList", {dc: "sysparam"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnSynchronize")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnSynchronize
	 */
	,onBtnSynchronize: function() {
		var successFn = function() {
			this._getDc_("sysparam").doQuery();
		};
		var o={
			name:"synchronizeCatalog",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("sysparam").doRpcFilter(o);
	}
	
	,_afterDefineElements_: function() {
		
						if (!getApplication().getSession().user.systemUser) {
						         this._getBuilder_()
						    .change("btnSynchronize", {disabled: true});
						}
	}
});
