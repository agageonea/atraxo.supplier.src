/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.AttachmentType_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_AttachmentType_Ds"
	},
	
	
	validators: {
			name: [{type: 'presence'}]
	},
	
	fields: [
		{name:"category", type:"string"},
		{name:"uploadPath", type:"string"},
		{name:"baseUrl", type:"string"},
		{name:"id", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.AttachmentType_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"category", type:"string"},
		{name:"uploadPath", type:"string"},
		{name:"baseUrl", type:"string"},
		{name:"id", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
