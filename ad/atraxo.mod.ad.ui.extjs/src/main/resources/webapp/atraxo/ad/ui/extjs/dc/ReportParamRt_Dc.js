/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.ReportParamRt_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.ReportParamRt_Ds
});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.ReportParamRt_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_ReportParamRt_Dc$List",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addNumberColumn({name:"sequenceNo", dataIndex:"sequenceNo", hidden:true, noEdit: true, maxLength:4, align:"right" })
		.addTextColumn({name:"name", dataIndex:"name", hidden:true, width:200, noEdit: true, maxLength:255})
		.addTextColumn({name:"title", dataIndex:"title", width:200, noEdit: true, maxLength:255})
		.addTextColumn({name:"value", dataIndex:"value", width:200, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_getCustomCellEditor_: function(record) {
		
						var ed;
				 		
						if ( record.data.noEdit ) {			 
					    	ed = new  Ext.form.field.Text({ 
								readOnly:true 
							});
					    }
						else if (!Ext.isEmpty(record.data.listOfValues)) {
					    	ed = new Ext.form.field.ComboBox({
								store:record.data.listOfValues.split(","),
								allowBlank : ((record.data.mandatory === true) ? true : false)
							});
					    }
						else if (record.data.dataType === "integer" || record.data.dataType === "decimal") {
					    	ed = new Ext.form.field.Number({ 
								allowBlank : ((record.data.mandatory === true) ? true : false),
								fieldStyle : "text-align:right;",
								hideTrigger : true,
								keyNavEnabled : false,
								mouseWheelEnabled : false 
							});
					    }
					    else if (record.data.dataType === "date") {
					    	ed = new Ext.form.field.Date({
								format: Main.MODEL_DATE_FORMAT
							});
					    }
					    else if (record.data.dataType === "boolean") {
					    	ed = new Ext.form.field.ComboBox({
								store:["true","false"]
							});
					    } else {
					    	ed = new  Ext.form.field.Text({
								allowBlank : ((record.data.mandatory === true) ? true : false),
							});
						}
					    if(ed){
						    ed._dcView_ =  this;
					    }
					    return ed;
	}
});
