/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.DsReport_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.DsReport_Ds
});

/* ================= EDIT-GRID: ReportCtxList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.DsReport_Dc$ReportCtxList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_DsReport_Dc$ReportCtxList",
	_noImport_: true,
	_noExport_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"reportId", dataIndex:"reportId", hidden:true, width:100, maxLength:64, 
			editor: { xtype:"textfield", maxLength:64}})
		.addLov({name:"dataSource", dataIndex:"dataSource", width:200, noUpdate: true, xtype:"gridcolumn", 
			editor:{xtype:"ad_DataSources_Lov", noUpdate:true, maxLength:255}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
