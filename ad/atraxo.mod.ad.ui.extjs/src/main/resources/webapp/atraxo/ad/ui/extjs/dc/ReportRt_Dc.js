/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.ReportRt_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.ReportRt_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.ReportRt_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_ReportRt_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"code", bind:"{d.code}", dataIndex:"code", xtype:"ad_Reports_Lov", maxLength:64, caseRestriction:"uppercase",
			retFieldMapping: [{lovField:"id", dsField: "id"} ]})
		.addLov({name:"name", bind:"{d.name}", dataIndex:"name", xtype:"ad_ReportsName_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "id"} ]})
		.addLov({name:"reportServer", bind:"{d.reportServer}", dataIndex:"reportServer", xtype:"ad_ReportServers_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "reportServerId"} ]})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:250, layout:"anchor"})
		.addPanel({ name:"col2", width:250, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["code", "name"])
		.addChildrenTo("col2", ["reportServer"]);
	}
});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.ReportRt_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_ReportRt_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:300})
		.addTextColumn({ name:"code", dataIndex:"code", width:150})
		.addTextColumn({ name:"reportServer", dataIndex:"reportServer", hidden:true, width:100})
		.addTextColumn({ name:"reportServerId", dataIndex:"reportServerId", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
