/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.AccessControlDs_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ad.ui.extjs.ds.AccessControlDs_DsParam,
	recordModel: atraxo.ad.ui.extjs.ds.AccessControlDs_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.AccessControlDs_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_AccessControlDs_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"dsName", bind:"{d.dsName}", dataIndex:"dsName", xtype:"ad_DataSourcesDs_Lov", maxLength:255})
		.addLov({name:"accessControl", bind:"{d.accessControl}", dataIndex:"accessControl", xtype:"ad_AccessControls_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "accessControlId"} ]})
		.addLov({name:"withRole", bind:"{p.withRole}", paramIndex:"withRole", xtype:"ad_Roles_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsParam: "withRoleId"} ]})
		.addBooleanField({ name:"queryAllowed", bind:"{d.queryAllowed}", dataIndex:"queryAllowed"})
		.addBooleanField({ name:"insertAllowed", bind:"{d.insertAllowed}", dataIndex:"insertAllowed"})
		.addBooleanField({ name:"updateAllowed", bind:"{d.updateAllowed}", dataIndex:"updateAllowed"})
		.addBooleanField({ name:"deleteAllowed", bind:"{d.deleteAllowed}", dataIndex:"deleteAllowed"})
		.addBooleanField({ name:"importAllowed", bind:"{d.importAllowed}", dataIndex:"importAllowed"})
		.addBooleanField({ name:"exportAllowed", bind:"{d.exportAllowed}", dataIndex:"exportAllowed"})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:250, layout:"anchor"})
		.addPanel({ name:"col2", width:180, layout:"anchor"})
		.addPanel({ name:"col3", width:180, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3"])
		.addChildrenTo("col1", ["accessControl", "dsName", "withRole"])
		.addChildrenTo("col2", ["queryAllowed", "importAllowed", "exportAllowed"])
		.addChildrenTo("col3", ["insertAllowed", "updateAllowed", "deleteAllowed"]);
	}
});

/* ================= FILTER: FilterPG ================= */


Ext.define("atraxo.ad.ui.extjs.dc.AccessControlDs_Dc$FilterPG", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_AccessControlDs_Dc$FilterPG",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"dsName", dataIndex:"dsName", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.DataSourcesDs_Lov", selectOnFocus:true, maxLength:255}})
			.addLov({name:"accessControl", dataIndex:"accessControl", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.AccessControls_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "accessControlId"} ]}})
			.addLov({name:"withRole", paramIndex:"withRole", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.Roles_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsParam: "withRoleId"} ]}})
			.addBooleanField({ name:"queryAllowed", dataIndex:"queryAllowed"})
			.addBooleanField({ name:"insertAllowed", dataIndex:"insertAllowed"})
			.addBooleanField({ name:"updateAllowed", dataIndex:"updateAllowed"})
			.addBooleanField({ name:"deleteAllowed", dataIndex:"deleteAllowed"})
			.addBooleanField({ name:"importAllowed", dataIndex:"importAllowed"})
			.addBooleanField({ name:"exportAllowed", dataIndex:"exportAllowed"})
		;
	}

});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.AccessControlDs_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_AccessControlDs_Dc$EditList",
	_bulkEditFields_: ["queryAllowed","insertAllowed","updateAllowed","deleteAllowed","importAllowed","exportAllowed"],

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"accessControl", dataIndex:"accessControl", width:200, xtype:"gridcolumn", 
			editor:{xtype:"ad_AccessControls_Lov", maxLength:255,
				retFieldMapping: [{lovField:"id", dsField: "accessControlId"} ]}})
		.addLov({name:"dsName", dataIndex:"dsName", width:200, xtype:"gridcolumn", 
			editor:{xtype:"ad_DataSourcesDs_Lov", maxLength:255}})
		.addBooleanColumn({name:"queryAllowed", dataIndex:"queryAllowed"})
		.addBooleanColumn({name:"insertAllowed", dataIndex:"insertAllowed"})
		.addBooleanColumn({name:"updateAllowed", dataIndex:"updateAllowed"})
		.addBooleanColumn({name:"deleteAllowed", dataIndex:"deleteAllowed"})
		.addBooleanColumn({name:"importAllowed", dataIndex:"importAllowed"})
		.addBooleanColumn({name:"exportAllowed", dataIndex:"exportAllowed"})
		.addTextColumn({name:"accessControlId", dataIndex:"accessControlId", hidden:true, width:100, noEdit: true, maxLength:64})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT-GRID: CtxEditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.AccessControlDs_Dc$CtxEditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_AccessControlDs_Dc$CtxEditList",
	_noImport_: true,
	_noExport_: true,
	_bulkEditFields_: ["queryAllowed","insertAllowed","updateAllowed","deleteAllowed","importAllowed","exportAllowed"],

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"dsName", dataIndex:"dsName", width:250, xtype:"gridcolumn", 
			editor:{xtype:"ad_DataSourcesDs_Lov", maxLength:255}})
		.addBooleanColumn({name:"queryAllowed", dataIndex:"queryAllowed"})
		.addBooleanColumn({name:"insertAllowed", dataIndex:"insertAllowed"})
		.addBooleanColumn({name:"updateAllowed", dataIndex:"updateAllowed"})
		.addBooleanColumn({name:"deleteAllowed", dataIndex:"deleteAllowed"})
		.addBooleanColumn({name:"importAllowed", dataIndex:"importAllowed"})
		.addBooleanColumn({name:"exportAllowed", dataIndex:"exportAllowed"})
		.addTextColumn({name:"accessControlId", dataIndex:"accessControlId", hidden:true, width:100, noEdit: true, maxLength:64})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
