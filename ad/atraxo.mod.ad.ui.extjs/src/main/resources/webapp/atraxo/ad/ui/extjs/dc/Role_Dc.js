/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.Role_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ad.ui.extjs.ds.Role_DsParam,
	recordModel: atraxo.ad.ui.extjs.ds.Role_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Role_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_Role_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"code", bind:"{d.code}", dataIndex:"code", xtype:"ad_Roles_Lov", maxLength:64, caseRestriction:"uppercase",
			retFieldMapping: [{lovField:"id", dsField: "id"} ]})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", maxLength:255})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active"})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:280, layout:"anchor"})
		.addPanel({ name:"col2", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["code", "name"])
		.addChildrenTo("col2", ["active"]);
	}
});

/* ================= FILTER: FilterPG ================= */


Ext.define("atraxo.ad.ui.extjs.dc.Role_Dc$FilterPG", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_Role_Dc$FilterPG",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"code", dataIndex:"code", maxLength:64, caseRestriction:"uppercase",
				editor:{xtype:"atraxo.ad.ui.extjs.lov.Roles_Lov", selectOnFocus:true, maxLength:64, caseRestriction:"uppercase",
					retFieldMapping: [{lovField:"id", dsField: "id"} ]}})
			.addTextField({ name:"name", dataIndex:"name", maxLength:255})
			.addBooleanField({ name:"active", dataIndex:"active"})
			.addLov({name:"withUser", paramIndex:"withUser", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.Users_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsParam: "withUserId"} ]}})
			.addLov({name:"withPrivilege", paramIndex:"withPrivilege", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.AccessControls_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsParam: "withPrivilegeId"} ]}})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Role_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_Role_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:200})
		.addTextColumn({ name:"name", dataIndex:"name", width:150})
		.addTextColumn({ name:"description", dataIndex:"description", width:400})
		.addBooleanColumn({ name:"active", dataIndex:"active"})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Role_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_Role_Dc$EditList",
	_bulkEditFields_: ["active"],

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"code", dataIndex:"code", width:120, maxLength:64, caseRestriction:"uppercase", 
			editor: { xtype:"textfield", maxLength:64, caseRestriction:"uppercase"}})
		.addTextColumn({name:"name", dataIndex:"name", width:200, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"description", dataIndex:"description", width:200, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addBooleanColumn({name:"active", dataIndex:"active"})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
