/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.ReportRt_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_ReportRt_Ds"
	},
	
	
	validators: {
			name: [{type: 'presence'}]
	},
	
	fields: [
		{name:"reportServerId", type:"string"},
		{name:"reportServer", type:"string"},
		{name:"serverUrl", type:"string"},
		{name:"queryBuilderClass", type:"string"},
		{name:"contextPath", type:"string"},
		{name:"id", type:"string", noFilter:true},
		{name:"clientId", type:"string", noFilter:true},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string", noFilter:true},
		{name:"notes", type:"string", noFilter:true},
		{name:"active", type:"boolean"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true},
		{name:"createdBy", type:"string", noFilter:true},
		{name:"modifiedBy", type:"string", noFilter:true},
		{name:"version", type:"int", allowNull:true, noFilter:true},
		{name:"refid", type:"string", noFilter:true},
		{name:"entityAlias", type:"string", noFilter:true},
		{name:"entityFqn", type:"string", noFilter:true}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.ReportRt_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"reportServerId", type:"string"},
		{name:"reportServer", type:"string"},
		{name:"serverUrl", type:"string"},
		{name:"queryBuilderClass", type:"string"},
		{name:"contextPath", type:"string"},
		{name:"id", type:"string", noFilter:true},
		{name:"clientId", type:"string", noFilter:true},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string", noFilter:true},
		{name:"notes", type:"string", noFilter:true},
		{name:"active", type:"boolean", allowNull:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true},
		{name:"createdBy", type:"string", noFilter:true},
		{name:"modifiedBy", type:"string", noFilter:true},
		{name:"version", type:"int", allowNull:true, noFilter:true},
		{name:"refid", type:"string", noFilter:true},
		{name:"entityAlias", type:"string", noFilter:true},
		{name:"entityFqn", type:"string", noFilter:true}
	]
});
