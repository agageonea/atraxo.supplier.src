/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.FrameExtension_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.FrameExtension_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.FrameExtension_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_FrameExtension_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"frame", bind:"{d.frame}", dataIndex:"frame", maxLength:255})
		.addTextField({ name:"fileLocation", bind:"{d.fileLocation}", dataIndex:"fileLocation", maxLength:255})
		.addBooleanField({ name:"relativePath", bind:"{d.relativePath}", dataIndex:"relativePath"})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active"})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"})
		.addPanel({ name:"col2", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["frame", "fileLocation"])
		.addChildrenTo("col2", ["relativePath", "active"]);
	}
});

/* ================= FILTER: FilterPG ================= */


Ext.define("atraxo.ad.ui.extjs.dc.FrameExtension_Dc$FilterPG", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_FrameExtension_Dc$FilterPG",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"frame", dataIndex:"frame", maxLength:255})
			.addTextField({ name:"fileLocation", dataIndex:"fileLocation", maxLength:255})
			.addBooleanField({ name:"relativePath", dataIndex:"relativePath"})
			.addBooleanField({ name:"active", dataIndex:"active"})
		;
	}

});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.FrameExtension_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_FrameExtension_Dc$EditList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"frame", dataIndex:"frame", width:200, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addNumberColumn({name:"sequenceNo", dataIndex:"sequenceNo", maxLength:4, align:"right" })
		.addTextColumn({name:"fileLocation", dataIndex:"fileLocation", width:200, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addBooleanColumn({name:"relativePath", dataIndex:"relativePath"})
		.addBooleanColumn({name:"active", dataIndex:"active"})
		.addTextColumn({name:"notes", dataIndex:"notes", width:200, maxLength:4000, 
			editor: { xtype:"textfield", maxLength:4000}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
