/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.JobContext_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.JobContext_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("job", Ext.create(atraxo.ad.ui.extjs.dc.JobContext_Dc,{multiEdit: true}))
		.addDc("params", Ext.create(atraxo.ad.ui.extjs.dc.JobContextParam_Dc,{multiEdit: true}))
		.addDc("schedule", Ext.create(atraxo.ad.ui.extjs.dc.JobTimer_Dc,{}))
		.addDc("log", Ext.create(atraxo.ad.ui.extjs.dc.JobLog_Dc,{}))
		.linkDc("params", "job",{fields:[
					{childField:"jobContextId", parentField:"id"}]})
				.linkDc("schedule", "job",{fields:[
					{childField:"jobContextId", parentField:"id"}]})
				.linkDc("log", "job",{fields:[
					{childField:"jobContextId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnShowLog", disabled:true, handler: this.onBtnShowLog,stateManager:[{ name:"selected_not_zero", dc:"log"}], scope:this})
		.addButton({name:"btnSyncParams", disabled:true, handler: this.onBtnSyncParams,stateManager:[{ name:"selected_not_zero", dc:"job"}], scope:this})
		.addDcFilterFormView("job", {name:"jobFilter", xtype:"ad_JobContext_Dc$Filter"})
		.addDcEditGridView("job", {name:"jobEditList", xtype:"ad_JobContext_Dc$EditList", frame:true})
		.addDcEditGridView("params", {name:"paramsEditList", _hasTitle_:true, xtype:"ad_JobContextParam_Dc$CtxEditList", frame:true})
		.addDcGridView("schedule", {name:"scheduleEditList", _hasTitle_:true, xtype:"ad_JobTimer_Dc$CtxListJobContext"})
		.addDcGridView("log", {name:"logList", _hasTitle_:true, xtype:"ad_JobLog_Dc$CtxListJobContext"})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"theDetails", width:600, xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"theDetails", containerPanelName:"main", dcName:"params"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["jobEditList", "theDetails"], ["center", "east"])
		.addChildrenTo("theDetails", ["paramsEditList"])
		.addToolbarTo("main", "tlbJobEditList")
		.addToolbarTo("paramsEditList", "tlbParamsEditList")
		.addToolbarTo("scheduleEditList", "tlbScheduleEditList")
		.addToolbarTo("logList", "tlbLogList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("theDetails", ["scheduleEditList", "logList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbJobEditList", {dc: "job"})
			.addButtons([])
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnSyncParams")])
			.addReports()
		.end()
		.beginToolbar("tlbParamsEditList", {dc: "params"})
			.addQuery().addSave().addCancel()
			.addReports()
		.end()
		.beginToolbar("tlbScheduleEditList", {dc: "schedule"})
			.addQuery()
			.addReports()
		.end()
		.beginToolbar("tlbLogList", {dc: "log"})
			.addButtons([])
			.addQuery()
			.addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnShowLog")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnShowLog
	 */
	,onBtnShowLog: function() {
		var bundle = "atraxo.mod.ad";
		var frame = "atraxo.ad.ui.extjs.frame.JobLog_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				id: this._getDc_("log").getRecord().get("id")
			},
			callback: function (params) {
				this._when_called_to_view_(params);
			}
		});
	}
	
	/**
	 * On-Click handler for button btnSyncParams
	 */
	,onBtnSyncParams: function() {
		var successFn = function() {
			this._getDc_("job").doQuery();
		};
		var o={
			name:"synchronizeParameters",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("job").doRpcDataList(o);
	}
	
	,_when_called_to_view_: function(params) {
		
						var job = this._getDc_("job");
						if (job.isDirty()) {
							this._alert_dirty_();
							return;
						}
						job.doClearQuery();	 
						
						job.setFilterValue("id", params.id );
						job.setFilterValue("name", params.name );
						job.setFilterValue("jobName", params.jobName );
						
						job.doQuery();
	}
});
