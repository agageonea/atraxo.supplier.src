/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.User_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ad.ui.extjs.ds.User_DsParam,
	recordModel: atraxo.ad.ui.extjs.ds.User_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.User_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_User_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", maxLength:255})
		.addTextField({ name:"loginName", bind:"{d.loginName}", dataIndex:"loginName", maxLength:255})
		.addLov({name:"code", bind:"{d.code}", dataIndex:"code", xtype:"ad_Users_Lov", maxLength:64, caseRestriction:"uppercase",
			retFieldMapping: [{lovField:"id", dsField: "id"} ]})
		.addLov({name:"withRole", bind:"{p.withRole}", paramIndex:"withRole", xtype:"ad_Roles_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsParam: "withRoleId"} ]})
		.addLov({name:"inGroup", bind:"{p.inGroup}", paramIndex:"inGroup", xtype:"ad_UserGroups_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsParam: "inGroupId"} ]})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active"})
		.addBooleanField({ name:"locked", bind:"{d.locked}", dataIndex:"locked"})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:280, layout:"anchor"})
		.addPanel({ name:"col2", width:250, layout:"anchor"})
		.addPanel({ name:"col3", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3"])
		.addChildrenTo("col1", ["code", "name", "loginName"])
		.addChildrenTo("col2", ["withRole", "inGroup"])
		.addChildrenTo("col3", ["active", "locked"]);
	}
});

/* ================= FILTER: FilterPG ================= */


Ext.define("atraxo.ad.ui.extjs.dc.User_Dc$FilterPG", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_User_Dc$FilterPG",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:255})
			.addTextField({ name:"loginName", dataIndex:"loginName", maxLength:255})
			.addLov({name:"code", dataIndex:"code", maxLength:64, caseRestriction:"uppercase",
				editor:{xtype:"atraxo.ad.ui.extjs.lov.Users_Lov", selectOnFocus:true, maxLength:64, caseRestriction:"uppercase",
					retFieldMapping: [{lovField:"id", dsField: "id"} ]}})
			.addLov({name:"withRole", paramIndex:"withRole", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.Roles_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsParam: "withRoleId"} ]}})
			.addLov({name:"inGroup", paramIndex:"inGroup", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.UserGroups_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsParam: "inGroupId"} ]}})
			.addBooleanField({ name:"active", dataIndex:"active"})
			.addBooleanField({ name:"locked", dataIndex:"locked"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.User_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_User_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:120})
		.addTextColumn({ name:"name", dataIndex:"name", width:200})
		.addTextColumn({ name:"loginName", dataIndex:"loginName", width:200})
		.addTextColumn({ name:"dateFormat", dataIndex:"dateFormat", width:200})
		.addBooleanColumn({ name:"active", dataIndex:"active"})
		.addBooleanColumn({ name:"locked", dataIndex:"locked"})
		.addTextColumn({ name:"decimalSeparator", dataIndex:"decimalSeparator", hidden:true, width:50})
		.addTextColumn({ name:"thousandSeparator", dataIndex:"thousandSeparator", hidden:true, width:50})
		.addTextColumn({ name:"notes", dataIndex:"notes", hidden:true, width:200})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= GRID: ListShort ================= */

Ext.define("atraxo.ad.ui.extjs.dc.User_Dc$ListShort", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_User_Dc$ListShort",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:120})
		.addTextColumn({ name:"name", dataIndex:"name", width:200})
		.addTextColumn({ name:"loginName", dataIndex:"loginName", width:200})
		.addBooleanColumn({ name:"active", dataIndex:"active"})
		.addBooleanColumn({ name:"locked", dataIndex:"locked"})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.ad.ui.extjs.dc.User_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ad_User_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", noUpdate:true, allowBlank:false, maxLength:64, caseRestriction:"uppercase"})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, maxLength:255})
		.addTextField({ name:"loginName", bind:"{d.loginName}", dataIndex:"loginName", allowBlank:false, maxLength:255})
		.addTextField({ name:"email", bind:"{d.email}", dataIndex:"email", maxLength:128})
		.addTextArea({ name:"notes", bind:"{d.notes}", dataIndex:"notes", height:60})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active"})
		.addBooleanField({ name:"locked", bind:"{d.locked}", dataIndex:"locked"})
		.addCombo({ xtype:"combo", name:"decimalSeparator", bind:"{d.decimalSeparator}", dataIndex:"decimalSeparator", store:[ __AD__.TNumberSeparator._POINT_, __AD__.TNumberSeparator._COMMA_]})
		.addCombo({ xtype:"combo", name:"thousandSeparator", bind:"{d.thousandSeparator}", dataIndex:"thousandSeparator", store:[ __AD__.TNumberSeparator._POINT_, __AD__.TNumberSeparator._COMMA_]})
		.addLov({name:"dateFormat", bind:"{d.dateFormat}", dataIndex:"dateFormat", xtype:"ad_DateFormats_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "dateFormatId"} ]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:350, layout:"anchor"})
		.addPanel({ name:"col2", width:350, layout:"anchor"})
		.addPanel({ name:"col3", _hasTitle_: true, width:300,  xtype:"fieldset", collapsible:true, border:true, defaults: { labelWidth:"120"}, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3"])
		.addChildrenTo("col1", ["name", "code", "loginName", "email"])
		.addChildrenTo("col2", ["notes", "active", "locked"])
		.addChildrenTo("col3", ["dateFormat", "decimalSeparator", "thousandSeparator"]);
	}
});

/* ================= EDIT FORM: ChangePasswordForm ================= */

Ext.define("atraxo.ad.ui.extjs.dc.User_Dc$ChangePasswordForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ad_User_Dc$ChangePasswordForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"newPassword", bind:"{p.newPassword}", paramIndex:"newPassword", allowBlank:false, maxLength:255, inputType:"password"})
		.addTextField({ name:"confirmPassword", bind:"{p.confirmPassword}", paramIndex:"confirmPassword", allowBlank:false, maxLength:255, inputType:"password"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:350, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["newPassword", "confirmPassword"]);
	}
});
