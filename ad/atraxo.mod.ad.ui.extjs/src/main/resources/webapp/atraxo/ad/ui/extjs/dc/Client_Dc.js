/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.Client_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ad.ui.extjs.ds.Client_DsParam,
	recordModel: atraxo.ad.ui.extjs.ds.Client_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ad.ui.extjs.dc.Client_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_Client_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"code", dataIndex:"code", maxLength:64, caseRestriction:"uppercase",
				editor:{xtype:"atraxo.ad.ui.extjs.lov.Clients_Lov", selectOnFocus:true, maxLength:64, caseRestriction:"uppercase",
					retFieldMapping: [{lovField:"id", dsField: "id"} ]}})
			.addTextField({ name:"name", dataIndex:"name", maxLength:255})
			.addBooleanField({ name:"active", dataIndex:"active"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Client_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_Client_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:120})
		.addTextColumn({ name:"name", dataIndex:"name", width:200})
		.addTextColumn({ name:"notes", dataIndex:"notes", hidden:true, width:200})
		.addTextColumn({ name:"adminRole", dataIndex:"adminRole", hidden:true, width:50})
		.addBooleanColumn({ name:"active", dataIndex:"active"})
		.addTextColumn({ name:"workspacePath", dataIndex:"workspacePath", width:200})
		.addTextColumn({ name:"importPath", dataIndex:"importPath", width:200})
		.addTextColumn({ name:"exportPath", dataIndex:"exportPath", width:200})
		.addTextColumn({ name:"tempPath", dataIndex:"tempPath", width:200})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Client_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ad_Client_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, maxLength:255})
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", noEdit:true , allowBlank:false, maxLength:64, caseRestriction:"uppercase"})
		.addTextArea({ name:"notes", bind:"{d.notes}", dataIndex:"notes", height:60})
		.addTextField({ name:"adminRole", bind:"{d.adminRole}", dataIndex:"adminRole", noEdit:true , maxLength:32})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active"})
		.addTextField({ name:"workspacePath", bind:"{d.workspacePath}", dataIndex:"workspacePath", allowBlank:false, width:400, maxLength:255})
		.addTextField({ name:"importPath", bind:"{d.importPath}", dataIndex:"importPath", allowBlank:false, width:400, maxLength:255})
		.addTextField({ name:"exportPath", bind:"{d.exportPath}", dataIndex:"exportPath", allowBlank:false, width:400, maxLength:255})
		.addTextField({ name:"tempPath", bind:"{d.tempPath}", dataIndex:"tempPath", allowBlank:false, width:400, maxLength:255})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:350, layout:"anchor"})
		.addPanel({ name:"col2", _hasTitle_: true, width:550,  xtype:"fieldset", collapsible:true, border:false, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["name", "code", "notes", "adminRole", "active"])
		.addChildrenTo("col2", ["workspacePath", "importPath", "exportPath", "tempPath"]);
	}
});

/* ================= EDIT FORM: Create ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Client_Dc$Create", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ad_Client_Dc$Create",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, maxLength:255})
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", allowBlank:false, maxLength:64, caseRestriction:"uppercase"})
		.addTextArea({ name:"notes", bind:"{d.notes}", dataIndex:"notes", height:60})
		.addTextField({ name:"adminRole", bind:"{d.adminRole}", dataIndex:"adminRole", noEdit:true , maxLength:32})
		.addTextField({ name:"adminUserCode", bind:"{p.adminUserCode}", paramIndex:"adminUserCode", allowBlank:false, maxLength:64, caseRestriction:"uppercase"})
		.addTextField({ name:"adminUserName", bind:"{p.adminUserName}", paramIndex:"adminUserName", allowBlank:false, maxLength:255,listeners:{
			change:{scope:this, fn:this._resetPwdFields_},
			blur:{scope:this, fn:this._onBlurName_},
			focus:{scope:this, fn:this._onFocusName_}
		}})
		.addTextField({ name:"adminUserLogin", bind:"{p.adminUserLogin}", paramIndex:"adminUserLogin", allowBlank:false, maxLength:255,listeners:{
			change:{scope:this, fn:this._resetPwdFields_},
			blur:{scope:this, fn:this._onBlurName_},
			focus:{scope:this, fn:this._onFocusName_}
		}})
		.addTextField({ name:"adminPassword", bind:"{p.adminPassword}", paramIndex:"adminPassword", allowBlank:false, maxLength:255, readOnly:true, inputType:"password",listeners:{
			change:{scope:this, fn:this._onPwdChange_},
			blur:{scope:this, fn:this._onBlurPwd_},
			focus:{scope:this, fn:this._onFocusPwd_}
		}})
		.addTextField({ name:"adminPasswordRe", bind:"{p.adminPasswordRe}", paramIndex:"adminPasswordRe", allowBlank:false, maxLength:255, readOnly:true, inputType:"password",listeners:{
			change:{scope:this, fn:this._onPwdChange_},
			blur:{scope:this, fn:this._onBlurPwd_},
			focus:{scope:this, fn:this._onFocusPwd_}
		}})
		.addTextField({ name:"initFileLocation", bind:"{p.initFileLocation}", paramIndex:"initFileLocation", allowBlank:false, width:400, maxLength:255})
		.addTextField({ name:"workspacePath", bind:"{d.workspacePath}", dataIndex:"workspacePath", allowBlank:false, width:400, maxLength:255,listeners:{
			blur:{scope:this, fn:this.createPathsFromWorkspace}
		}})
		.addTextField({ name:"importPath", bind:"{d.importPath}", dataIndex:"importPath", allowBlank:false, width:400, maxLength:255})
		.addTextField({ name:"exportPath", bind:"{d.exportPath}", dataIndex:"exportPath", allowBlank:false, width:400, maxLength:255})
		.addTextField({ name:"tempPath", bind:"{d.tempPath}", dataIndex:"tempPath", allowBlank:false, width:400, maxLength:255})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"})
		.addPanel({ name:"col2", _hasTitle_: true, width:350,  xtype:"fieldset", collapsible:true, layout:"anchor"})
		.addPanel({ name:"col3", _hasTitle_: true, width:500,  xtype:"fieldset", collapsible:true, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3"])
		.addChildrenTo("col1", ["name", "code", "notes", "adminRole"])
		.addChildrenTo("col2", ["adminUserCode", "adminUserName", "adminUserLogin", "adminPassword", "adminPasswordRe"])
		.addChildrenTo("col3", ["initFileLocation", "workspacePath", "importPath", "exportPath", "tempPath"]);
	},
	/* ==================== Business functions ==================== */
	
	_shouldValidate_: function() {
		return this._controller_.record.phantom;
	},
	
	createPathsFromWorkspace: function() {
		
					var r = this._controller_.record;
					r.set("importPath",r.get("workspacePath") + "/import");
					r.set("exportPath",r.get("workspacePath") + "/export");
					r.set("tempPath",r.get("workspacePath") + "/temp");
	},
	
	_afterDefineElements_: function() {
		
					var dc = this._controller_;
					dc.canDoSave = function(_dc){
						var pwd = _dc.getParamValue("adminPassword");
						var pwdRe = _dc.getParamValue("adminPasswordRe");
						return (pwd === pwdRe) && !Ext.isEmpty(pwd);
					};
					dc.on("afterDoNew", function(_dc){
						var f = function(){
							this.setParamValue("adminPassword", "");
							this.setParamValue("adminPasswordRe", "");
						};
						Ext.defer(f, 500, _dc);
					}, this);
		
					this.pwdStatusTask = new Ext.util.DelayedTask(this.setPwdFieldsStatus, this);
					this.nameStatusTask = new Ext.util.DelayedTask(this.setNameFieldsStatus, this);
	},
	
	_onPwdChange_: function() {
		
					var dc = this._controller_;
					Ext.defer(dc.updateDcState, 100, dc);
	},
	
	_resetPwdFields_: function() {
		
					var f = function(){
					 	var dc = this._controller_;
						dc.setParamValue("adminPassword", "");
						dc.setParamValue("adminPasswordRe", "");
					};
					Ext.defer(f, 50, this);
	},
	
	setPwdFieldsStatus: function(f1,f2,status) {
		
					f1.setReadOnly(status);
					f2.setReadOnly(status);
	},
	
	setNameFieldsStatus: function(f1,f2,status) {
		
					f1.setReadOnly(status);
					f2.setReadOnly(status);
	},
	
	_onBlurName_: function() {
		
					var f1 = this._getElement_("adminPassword");
					var f2 = this._getElement_("adminPasswordRe");
					this.pwdStatusTask.delay(50, this.setPwdFieldsStatus, this, [f1,f2,false]);
	},
	
	_onFocusName_: function() {
		
					var f1 = this._getElement_("adminPassword");
					var f2 = this._getElement_("adminPasswordRe");
					f1.setReadOnly(true);
					f2.setReadOnly(true);
					this.pwdStatusTask.delay(50, this.setPwdFieldsStatus, this, [f1,f2,true]);
	},
	
	_onBlurPwd_: function() {
		
					var f1 = this._getElement_("adminUserName");
					var f2 = this._getElement_("adminUserLogin");
					this.nameStatusTask.delay(50, this.setNameFieldsStatus, this, [f1,f2,false]);
	},
	
	_onFocusPwd_: function() {
		
					var f1 = this._getElement_("adminUserName");
					var f2 = this._getElement_("adminUserLogin");
					f1.setReadOnly(true);
					f2.setReadOnly(true);
					this.nameStatusTask.delay(50, this.setNameFieldsStatus, this, [f1,f2,true]);
	}
});
