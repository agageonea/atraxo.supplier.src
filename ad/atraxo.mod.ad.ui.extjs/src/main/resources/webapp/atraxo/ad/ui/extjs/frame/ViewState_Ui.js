/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.ViewState_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.ViewState_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("viewstate", Ext.create(atraxo.ad.ui.extjs.dc.ViewState_Dc,{}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addDcFilterFormView("viewstate", {name:"viewstateFilter", xtype:"ad_ViewState_Dc$FilterPG"})
		.addDcGridView("viewstate", {name:"viewstateList", xtype:"ad_ViewState_Dc$List"})
		.addDcFormView("viewstate", {name:"viewstateViewState", height:150, xtype:"ad_ViewState_Dc$Value"})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["viewstateList", "viewstateViewState"], ["center", "south"])
		.addToolbarTo("main", "tlbViewstateList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbViewstateList", {dc: "viewstate"})
			.addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end();
	}
	

});
