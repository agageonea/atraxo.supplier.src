/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.ReportServer_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.ReportServer_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("m", Ext.create(atraxo.ad.ui.extjs.dc.ReportServer_Dc,{multiEdit: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addDcFilterFormView("m", {name:"mFilter", xtype:"ad_ReportServer_Dc$FilterPG"})
		.addDcEditGridView("m", {name:"mEditList", xtype:"ad_ReportServer_Dc$EditList", frame:true})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["mEditList"], ["center"])
		.addToolbarTo("canvas1", "tlbMEditList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbMEditList", {dc: "m"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end();
	}
	

});
