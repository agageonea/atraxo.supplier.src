/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
__AD__ = {
	TDataType : {
		_STRING_ : "string" , 
		_TEXT_ : "text" , 
		_INTEGER_ : "integer" , 
		_DECIMAL_ : "decimal" , 
		_BOOLEAN_ : "boolean" , 
		_DATE_ : "date" 
	},
	TAttachmentType : {
		_LINK_ : "link" , 
		_UPLOAD_ : "upload" 
	},
	TViewStateCmpType : {
		_FRAME_DCGRID_ : "frame-dcgrid" , 
		_FRAME_DCEGRID_ : "frame-dcegrid" 
	},
	TNumberSeparator : {
		_POINT_ : "." , 
		_COMMA_ : "," 
	},
	TMenuTag : {
		_TOP_ : "top" , 
		_LEFT_ : "left" 
	},
	TTimerType : {
		_SIMPLE_ : "simple" , 
		_CRON_ : "cron" 
	},
	TTimerIntervalType : {
		_SECONDS_ : "seconds" , 
		_MINUTES_ : "minutes" , 
		_HOURS_ : "hours" 
	},
	TJobLogMsgType : {
		_INFO_ : "info" , 
		_ERROR_ : "error" , 
		_WARNING_ : "warning" 
	},
	Project : {
		_SUPPLIERONE_ : "SupplierOne" , 
		_MARKETPLACE_ : "MarketPlace" 
	},
	LastExecutionStatus : {
		_UNKNOWN_ : "Unknown" , 
		_SUCCESS_ : "Success" , 
		_FAILURE_ : "Failure" 
	},
	BusinessArea : {
		_ACCOUNT_MANAGEMENT_ : "Account management" , 
		_SUPPLY_MANAGEMENT_ : "Supply management" , 
		_SALES_CONTRACTS_ : "Sales contracts" , 
		_PURCHASE_CONTRACTS_ : "Purchase contracts" , 
		_FUEL_TICKETS_ : "Fuel tickets" , 
		_FUEL_EVENTS_ : "Fuel events" , 
		_SALES_INVOICES_ : "Sales invoices" , 
		_PURCHASE_INVOICES_ : "Purchase invoices" 
	},
	ReportParamType : {
		_STRING_ : "string" , 
		_INTEGER_ : "integer" , 
		_BOOLEAN_ : "boolean" , 
		_DATE_ : "date" , 
		_ENUMERATION_ : "enumeration" , 
		_LOV_ : "lov" , 
		_MASKED_ : "masked" 
	},
	ReportFormat : {
		_PDF_ : "PDF" , 
		_CSV_ : "CSV" , 
		_XLSX_ : "XLSX" , 
		_DOC_ : "DOC" 
	},
	ReportStatus : {
		_SUCCESS_ : "Success" , 
		_IN_PROGRESS_ : "In progress" , 
		_FAILED_ : "Failed" 
	},
	NotificationAction : {
		_NOACTION_ : "NoAction" , 
		_GOTO_ : "GoTo" 
	},
	Category : {
		_ACTIVITY_ : "Activity" , 
		_JOB_ : "Job" , 
		_ERROR_ : "Error" , 
		_PROCESS_ : "Process" 
	},
	UserFieldType : {
		_STRING_ : "String" , 
		_NUMERIC_ : "Numeric" , 
		_DATETIME_ : "DateTime" 
	},
	Priority : {
		_MAX_ : "Max" , 
		_HIGH_ : "High" , 
		_DEFAULT_ : "Default" 
	},
	LanguageCodes : {
		_ENGLISH_ : "English" , 
		_GERMAN_ : "German" , 
		_FRENCH_ : "French" , 
		_SPANISH_ : "Spanish" , 
		_PORTUGUESE_ : "Portuguese" , 
		_CHINESE_ : "Chinese" 
	}
}
