/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.UserFieldValues_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.UserFieldValues_Ds
});

/* ================= EDIT-GRID: DialogList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.UserFieldValues_Dc$DialogList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_UserFieldValues_Dc$DialogList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"userFieldName", dataIndex:"userFieldName", width:120, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"ad_UserFieldsLov_Lov", allowBlank:false, maxLength:64,
				retFieldMapping: [{lovField:"id", dsField: "userFieldId"} ,{lovField:"description", dsField: "userFieldDescription"} ,{lovField:"type", dsField: "userFieldType"} ,{lovField:"defaultValue", dsField: "value"} ],
				filterFieldMapping: [{lovField:"dsAlias", dsField: "targetAlias"} ]},  flex:1})
		.addTextColumn({name:"userFieldDescription", dataIndex:"userFieldDescription", width:200, noEdit: true, allowBlank: false, maxLength:1000,  flex:1})
		.addComboColumn({name:"userFieldType", dataIndex:"userFieldType", width:100, noEdit: true,  flex:1})
		.addTextColumn({name:"value", dataIndex:"value", width:200, allowBlank: false, maxLength:1000,  flex:1, renderer:function(v,m,r) {return this._setRenderer_(v,m,r)}, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:1000}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_setRenderer_: function(v,m,r) {
		
						var type = r.get("userFieldType");
						var formatedDate = v;
						if (type === __AD__.UserFieldType._DATETIME_) {
							formatedDate = Ext.util.Format.date(v, Main.DATETIME_FORMAT)
						}
						return formatedDate;
	}
});
