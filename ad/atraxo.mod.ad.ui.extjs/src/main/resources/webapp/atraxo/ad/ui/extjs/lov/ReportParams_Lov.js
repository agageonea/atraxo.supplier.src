/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.lov.ReportParams_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.ad_ReportParams_Lov",
	displayField: "name", 
	_columns_: ["id", "reportId", "name", "title"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{name}, {title}</span>';
		},
		width:250, maxHeight:350
	},
	_editFrame_: {
		name: "atraxo.ad.ui.extjs.frame.Report_Ui"
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.ad.ui.extjs.ds.ReportParamLov_Ds
});
