/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.ClientLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_ClientLov_Ds"
	},
	
	
	fields: [
		{name:"id", type:"string"},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"active", type:"boolean"},
		{name:"refid", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.ClientLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"id", type:"string"},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"refid", type:"string"}
	]
});
