/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.Param_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.Param_Ds
});

/* ================= FILTER: FilterPG ================= */


Ext.define("atraxo.ad.ui.extjs.dc.Param_Dc$FilterPG", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_Param_Dc$FilterPG",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"code", dataIndex:"code", maxLength:64, caseRestriction:"uppercase",
				editor:{xtype:"atraxo.ad.ui.extjs.lov.Params_Lov", selectOnFocus:true, maxLength:64, caseRestriction:"uppercase"}})
			.addTextField({ name:"name", dataIndex:"name", maxLength:255})
			.addTextField({ name:"defaultValue", dataIndex:"defaultValue", maxLength:1000})
			.addTextField({ name:"description", dataIndex:"description", maxLength:1000})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.Param_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_Param_Dc$List",
	_noImport_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:200})
		.addTextColumn({ name:"name", dataIndex:"name", width:200})
		.addTextColumn({ name:"description", dataIndex:"description", width:300})
		.addTextColumn({ name:"defaultValue", dataIndex:"defaultValue", width:200})
		.addTextColumn({ name:"listOfValues", dataIndex:"listOfValues", width:200})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
