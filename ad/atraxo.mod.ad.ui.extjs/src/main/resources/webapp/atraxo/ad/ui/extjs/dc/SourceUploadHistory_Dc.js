/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.SourceUploadHistory_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ad.ui.extjs.ds.SourceUploadHistory_DsParam,
	recordModel: atraxo.ad.ui.extjs.ds.SourceUploadHistory_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ad.ui.extjs.dc.SourceUploadHistory_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_SourceUploadHistory_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateTimeField({name:"uploadDate", dataIndex:"uploadDate"})
			.addTextField({ name:"name", dataIndex:"name", maxLength:255})
			.addTextField({ name:"uploader", dataIndex:"uploader", maxLength:1000})
			.addTextField({ name:"fileName", dataIndex:"fileName", maxLength:255})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.SourceUploadHistory_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_SourceUploadHistory_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:200,  flex:1})
		.addDateColumn({ name:"uploadDate", dataIndex:"uploadDate", _mask_: Masks.DATETIME,  flex:1})
		.addTextColumn({ name:"uploader", dataIndex:"uploader", width:200,  flex:1})
		.addTextColumn({ name:"notes", dataIndex:"notes", width:200,  flex:1})
		.addTextColumn({ name:"fileName", dataIndex:"fileName", width:200,  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: New ================= */

Ext.define("atraxo.ad.ui.extjs.dc.SourceUploadHistory_Dc$New", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ad_SourceUploadHistory_Dc$New",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, maxLength:255})
		.addTextArea({ name:"notes", bind:"{d.notes}", dataIndex:"notes"})
		.addHiddenField({ name:"reportId", bind:"{p.reportId}", paramIndex:"reportId"})
		.addUploadField({ name:"file", bind:"{d.file}", dataIndex:"file", style:"display:none", listeners:{change: {scope: this,fn:function(e) { this._save_() }}}})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col"])
		.addChildrenTo("col", ["name", "notes", "file", "reportId"]);
	},
	/* ==================== Business functions ==================== */
	
	_selectFile_: function() {
		
						var fileUploadField = this._get_("file");
						var uploadButton = fileUploadField.button;
						var elemId = fileUploadField.button.fileInputEl.dom.getAttribute("id");
						var elem = document.getElementById(elemId);
		
						elem.setAttribute("accept", ".rptdesign");
		
					   if(elem && document.createEvent) {
					      var evt = document.createEvent("MouseEvents");
					      evt.initEvent("click", true, false);
					      elem.dispatchEvent(evt);
					   }
	},
	
	_save_: function() {
		
						var me = this;
						var fileUploadField = this._get_("file");
						var uploadFieldValue = fileUploadField.getValue();
						var dc = this._controller_;
						var r = dc.getRecord();			
		
						if (me.isValid() && !Ext.isEmpty(uploadFieldValue)) {
							this._doUploadFile_();
						}
	},
	
	_cancel_: function() {
		
						this.up("window").hide();
						this._controller_.doCancel();
	},
	
	_doUploadFile_: function() {
		
						var frame = this._controller_.getFrame();
						var window = frame._getWindow_("wdwNew");
						var form = window.items.get(0);
						var rd = this._controller_.record.data;
						var cfg = {
				                _handler_ : "uploadRptDesign",
				                _succesCallbackScope_ : this._controller_,
								_succesCallbackFn_ : function() {
									this.store.commitChanges();
									this.doReloadPage();
									window.close();
								}
				        };
						Ext.apply(window,cfg);
						var formCfg = {
							fileUpload : true
						}
						Ext.apply(form,formCfg);
						if (form.getForm().isValid()) {
							Main.working();
							form.getForm().submit(
									{
										url : Main.urlUpload + "/" + window._handler_,
										scope : window,
										success : function(form, action) {
											try {
												Main.workingEnd();
											} catch (e) {
				
											}
											if (this._succesCallbackFn_ != null) {
												this._succesCallbackFn_.call(this._succesCallbackScope_ || this);
											}
										},
										failure : function(form, action) {
		
											try {
												Main.workingEnd();
											} catch (e) {
												
											}
											switch (action.failureType) {
											case Ext.form.Action.CLIENT_INVALID:
												Main.error("INVALID_FORM");
												break;
											case Ext.form.Action.CONNECT_FAILURE:
												Main.serverMessage(null, action.response);
												break;
											case Ext.form.Action.SERVER_INVALID:
												Main.serverMessage(null, action.response);
											}
										}
									});
						} else {
							Main.error("INVALID_FORM");
						}
		
	}
});
