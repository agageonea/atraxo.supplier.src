/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.MyUser_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.MyUser_Ds
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.ad.ui.extjs.dc.MyUser_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ad_MyUser_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", noEdit:true , maxLength:64, caseRestriction:"uppercase"})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", noEdit:true , maxLength:255})
		.addTextField({ name:"email", bind:"{d.email}", dataIndex:"email", maxLength:128})
		.addTextField({ name:"loginName", bind:"{d.loginName}", dataIndex:"loginName", maxLength:255})
		.addCombo({ xtype:"combo", name:"decimalSeparator", bind:"{d.decimalSeparator}", dataIndex:"decimalSeparator", store:[ __AD__.TNumberSeparator._POINT_, __AD__.TNumberSeparator._COMMA_]})
		.addCombo({ xtype:"combo", name:"thousandSeparator", bind:"{d.thousandSeparator}", dataIndex:"thousandSeparator", store:[ __AD__.TNumberSeparator._POINT_, __AD__.TNumberSeparator._COMMA_]})
		.addLov({name:"dateFormat", bind:"{d.dateFormat}", dataIndex:"dateFormat", xtype:"ad_DateFormats_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "dateFormatId"} ]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:350, layout:"anchor"})
		.addPanel({ name:"col2", _hasTitle_: true, width:280,  xtype:"fieldset", collapsible:false, border:true, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["name", "code", "loginName", "email"])
		.addChildrenTo("col2", ["dateFormat", "decimalSeparator", "thousandSeparator"]);
	}
});
