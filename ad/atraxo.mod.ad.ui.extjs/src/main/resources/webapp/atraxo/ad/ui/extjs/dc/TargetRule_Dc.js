/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.TargetRule_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.TargetRule_Ds
});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.TargetRule_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_TargetRule_Dc$EditList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"targetAlias", dataIndex:"targetAlias", width:200, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"targetType", dataIndex:"targetType", width:200, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"sourceRefId", dataIndex:"sourceRefId", hidden:true, width:100, noEdit: true, maxLength:64})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
