/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.AccessControl_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ad.ui.extjs.ds.AccessControl_DsParam,
	recordModel: atraxo.ad.ui.extjs.ds.AccessControl_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.AccessControl_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_AccessControl_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"name", bind:"{d.name}", dataIndex:"name", xtype:"ad_AccessControls_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "id"} ]})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active"})
		.addLov({name:"withRole", bind:"{p.withRole}", paramIndex:"withRole", xtype:"ad_Roles_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsParam: "withRoleId"} ]})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:250, layout:"anchor"})
		.addPanel({ name:"col2", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["name", "withRole"])
		.addChildrenTo("col2", ["active"]);
	}
});

/* ================= FILTER: FilterPG ================= */


Ext.define("atraxo.ad.ui.extjs.dc.AccessControl_Dc$FilterPG", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_AccessControl_Dc$FilterPG",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"name", dataIndex:"name", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.AccessControls_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "id"} ]}})
			.addBooleanField({ name:"active", dataIndex:"active"})
			.addLov({name:"withRole", paramIndex:"withRole", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.Roles_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsParam: "withRoleId"} ]}})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.AccessControl_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_AccessControl_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:200})
		.addTextColumn({ name:"description", dataIndex:"description", width:300})
		.addBooleanColumn({ name:"active", dataIndex:"active"})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.AccessControl_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_AccessControl_Dc$EditList",
	_bulkEditFields_: ["active","description"],

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"name", dataIndex:"name", width:200, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"description", dataIndex:"description", width:300, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addBooleanColumn({name:"active", dataIndex:"active"})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: CopyRulesFromSource ================= */

Ext.define("atraxo.ad.ui.extjs.dc.AccessControl_Dc$CopyRulesFromSource", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ad_AccessControl_Dc$CopyRulesFromSource",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"copyFrom", bind:"{p.copyFrom}", paramIndex:"copyFrom", xtype:"ad_AccessControls_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsParam: "copyFromId"} ]})
		.addBooleanField({ name:"skipDs", bind:"{p.skipDs}", paramIndex:"skipDs"})
		.addBooleanField({ name:"skipAsgn", bind:"{p.skipAsgn}", paramIndex:"skipAsgn"})
		.addBooleanField({ name:"resetRules", bind:"{p.resetRules}", paramIndex:"resetRules"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["copyFrom", "skipDs", "skipAsgn", "resetRules"]);
	}
});
