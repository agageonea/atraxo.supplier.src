/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.MyParam_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.MyParam_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("param", Ext.create(atraxo.ad.ui.extjs.dc.MyParam_Dc,{}))
		.addDc("val", Ext.create(atraxo.ad.ui.extjs.dc.ParamValue_Dc,{multiEdit: true}))
		.linkDc("val", "param",{fetchMode:"auto",fields:[
					{childField:"sysParam", parentField:"code"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnPublish", disabled:false, handler: this.onBtnPublish, scope:this})
		.addButton({name:"btnShowValuesFrame", disabled:false, handler: this.onBtnShowValuesFrame, scope:this})
		.addButton({name:"btnInitVal", disabled:true, handler: this.onBtnInitVal,stateManager:[{ name:"selected_not_zero", dc:"param"}], scope:this})
		.addButton({name:"btnOk", disabled:false, handler: this.onBtnOk, scope:this})
		.addDcFilterFormView("param", {name:"paramFilter", xtype:"ad_MyParam_Dc$FilterPG"})
		.addDcGridView("param", {name:"paramList", xtype:"ad_MyParam_Dc$List"})
		.addDcFormView("param", {name:"initvalForm", xtype:"ad_MyParam_Dc$InitValuesDateRange"})
		.addDcEditGridView("val", {name:"valList", height:250, xtype:"ad_ParamValue_Dc$CtxEditList", frame:true})
		.addWindow({name:"wdwInitVal", _hasTitle_:true, width:350, height:150, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("initvalForm")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnOk")]}]})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["paramList", "valList"], ["center", "south"])
		.addToolbarTo("main", "tlbParamList")
		.addToolbarTo("valList", "tlbValList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbParamList", {dc: "param"})
			.addButtons([])
			.addQuery()
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnPublish"),this._elems_.get("btnInitVal"),this._elems_.get("btnShowValuesFrame")])
			.addReports()
		.end()
		.beginToolbar("tlbValList", {dc: "val"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addSeparator().addAutoLoad()
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnPublish
	 */
	,onBtnPublish: function() {
		var o={
			name:"publishValues",
			modal:true
		};
		this._getDc_("val").doRpcFilter(o);
	}
	
	/**
	 * On-Click handler for button btnShowValuesFrame
	 */
	,onBtnShowValuesFrame: function() {
		var bundle = "atraxo.mod.ad";
		var frame = "atraxo.ad.ui.extjs.frame.ParamValue_Ui";
		getApplication().showFrameByName(bundle, frame);
	}
	
	/**
	 * On-Click handler for button btnInitVal
	 */
	,onBtnInitVal: function() {
		this._getWindow_("wdwInitVal").show();
	}
	
	/**
	 * On-Click handler for button btnOk
	 */
	,onBtnOk: function() {
		var successFn = function() {
			this._getWindow_("wdwInitVal").close();
			this._getDc_("val").doQuery();
		};
		var o={
			name:"createInitialValues",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("param").doRpcDataList(o);
	}
});
