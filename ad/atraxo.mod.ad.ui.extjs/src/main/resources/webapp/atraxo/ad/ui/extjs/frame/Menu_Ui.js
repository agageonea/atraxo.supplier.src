/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.Menu_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Menu_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("menuitem", Ext.create(atraxo.ad.ui.extjs.dc.MenuItem_Dc,{multiEdit: true}))
		.addDc("menu", Ext.create(atraxo.ad.ui.extjs.dc.Menu_Dc,{multiEdit: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnAsgnItemRoles",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnAsgnItemRoles,stateManager:[{ name:"selected_one_clean", dc:"menuitem"}], scope:this})
		.addButton({name:"btnAsgnMenuRoles",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnAsgnMenuRoles,stateManager:[{ name:"selected_one_clean", dc:"menu"}], scope:this})
		.addDcFilterFormView("menu", {name:"menuFilter", xtype:"ad_Menu_Dc$Filter"})
		.addDcEditGridView("menu", {name:"menuEditList", xtype:"ad_Menu_Dc$EditList", frame:true})
		.addDcFilterFormView("menuitem", {name:"menuitemFilter", xtype:"ad_MenuItem_Dc$Filter"})
		.addDcEditGridView("menuitem", {name:"menuitemList", xtype:"ad_MenuItem_Dc$EditList", frame:true})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"menuTable", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"canvasMenu", _hasTitle_:true, preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvasItem", _hasTitle_:true, preventHeader:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["menuTable"])
		.addChildrenTo("menuTable", ["canvasMenu"])
		.addChildrenTo("canvasMenu", ["menuEditList"], ["center"])
		.addChildrenTo("canvasItem", ["menuitemList"], ["center"])
		.addToolbarTo("canvasMenu", "tlbMenuEditList")
		.addToolbarTo("canvasItem", "tlbMenuitemList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("menuTable", ["canvasItem"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbMenuEditList", {dc: "menu"})
			.addButtons([])
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnAsgnMenuRoles")])
			.addReports()
		.end()
		.beginToolbar("tlbMenuitemList", {dc: "menuitem"})
			.addButtons([])
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnAsgnItemRoles")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnAsgnItemRoles
	 */
	,onBtnAsgnItemRoles: function() {
		this._showAsgnWindow_("atraxo.ad.ui.extjs.asgn.MenuItem_Role_Asgn$Ui" ,{dc: "menuitem", objectIdField: "id"});
	}
	
	/**
	 * On-Click handler for button btnAsgnMenuRoles
	 */
	,onBtnAsgnMenuRoles: function() {
		this._showAsgnWindow_("atraxo.ad.ui.extjs.asgn.Menu_Role_Asgn$Ui" ,{dc: "menu", objectIdField: "id"});
	}
});
