/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.ReportParamRt_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_ReportParamRt_Ds"
	},
	
	
	validators: {
			name: [{type: 'presence'}]
	},
	
	fields: [
		{name:"reportId", type:"string"},
		{name:"report", type:"string"},
		{name:"value", type:"string"},
		{name:"dataType", type:"string"},
		{name:"title", type:"string"},
		{name:"listOfValues", type:"string"},
		{name:"noEdit", type:"boolean"},
		{name:"mandatory", type:"boolean"},
		{name:"sequenceNo", type:"int", allowNull:true},
		{name:"id", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.ReportParamRt_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"reportId", type:"string"},
		{name:"report", type:"string"},
		{name:"value", type:"string"},
		{name:"dataType", type:"string"},
		{name:"title", type:"string"},
		{name:"listOfValues", type:"string"},
		{name:"noEdit", type:"boolean", allowNull:true},
		{name:"mandatory", type:"boolean", allowNull:true},
		{name:"sequenceNo", type:"int", allowNull:true},
		{name:"id", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
