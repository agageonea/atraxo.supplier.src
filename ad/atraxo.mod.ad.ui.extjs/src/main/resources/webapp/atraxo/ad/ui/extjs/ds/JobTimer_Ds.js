/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.JobTimer_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_JobTimer_Ds"
	},
	
	
	validators: {
			name: [{type: 'presence'}]
	},
	
	fields: [
		{name:"startTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"endTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"type", type:"string"},
		{name:"cronExpression", type:"string"},
		{name:"repeatCount", type:"int", allowNull:true},
		{name:"repeatInterval", type:"int", allowNull:true},
		{name:"repeatIntervalType", type:"string"},
		{name:"jobContextId", type:"string"},
		{name:"jobContext", type:"string"},
		{name:"jobName", type:"string"},
		{name:"id", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.JobTimer_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"startTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"endTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"type", type:"string"},
		{name:"cronExpression", type:"string"},
		{name:"repeatCount", type:"int", allowNull:true},
		{name:"repeatInterval", type:"int", allowNull:true},
		{name:"repeatIntervalType", type:"string"},
		{name:"jobContextId", type:"string"},
		{name:"jobContext", type:"string"},
		{name:"jobName", type:"string"},
		{name:"id", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
		,
		{name:"startTime_From",type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"startTime_To",type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"endTime_From",type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"endTime_To",type:"date", dateFormat:Main.MODEL_DATE_FORMAT}
	]
});
