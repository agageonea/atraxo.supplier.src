/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.DsReportUsage_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.DsReportUsage_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.DsReportUsage_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_DsReportUsage_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"report", bind:"{d.report}", dataIndex:"report", xtype:"ad_Reports_Lov", maxLength:64, caseRestriction:"uppercase",
			retFieldMapping: [{lovField:"id", dsField: "reportId"} ]})
		.addLov({name:"reportName", bind:"{d.reportName}", dataIndex:"reportName", xtype:"ad_ReportsName_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "reportId"} ]})
		.addLov({name:"dataSource", bind:"{d.dataSource}", dataIndex:"dataSource", xtype:"ad_DataSources_Lov", maxLength:255})
		.addTextField({ name:"frameName", bind:"{d.frameName}", dataIndex:"frameName", maxLength:255})
		.addTextField({ name:"toolbarKey", bind:"{d.toolbarKey}", dataIndex:"toolbarKey", maxLength:1000})
		.addTextField({ name:"dcKey", bind:"{d.dcKey}", dataIndex:"dcKey", maxLength:1000})
		.addNumberField({name:"sequenceNo", bind:"{d.sequenceNo}", dataIndex:"sequenceNo", maxLength:4})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:250, layout:"anchor"})
		.addPanel({ name:"col2", width:250, layout:"anchor"})
		.addPanel({ name:"col3", width:250, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3"])
		.addChildrenTo("col1", ["report", "reportName"])
		.addChildrenTo("col2", ["dataSource", "frameName"])
		.addChildrenTo("col3", ["toolbarKey", "dcKey"]);
	}
});

/* ================= FILTER: FilterPG ================= */


Ext.define("atraxo.ad.ui.extjs.dc.DsReportUsage_Dc$FilterPG", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_DsReportUsage_Dc$FilterPG",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"report", dataIndex:"report", maxLength:64, caseRestriction:"uppercase",
				editor:{xtype:"atraxo.ad.ui.extjs.lov.Reports_Lov", selectOnFocus:true, maxLength:64, caseRestriction:"uppercase",
					retFieldMapping: [{lovField:"id", dsField: "reportId"} ]}})
			.addLov({name:"reportName", dataIndex:"reportName", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.ReportsName_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "reportId"} ]}})
			.addLov({name:"dataSource", dataIndex:"dataSource", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.DataSources_Lov", selectOnFocus:true, maxLength:255}})
			.addTextField({ name:"frameName", dataIndex:"frameName", maxLength:255})
			.addTextField({ name:"toolbarKey", dataIndex:"toolbarKey", maxLength:1000})
			.addTextField({ name:"dcKey", dataIndex:"dcKey", maxLength:1000})
			.addNumberField({name:"sequenceNo", dataIndex:"sequenceNo", maxLength:4})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.DsReportUsage_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_DsReportUsage_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"report", dataIndex:"report", width:120, xtype:"gridcolumn", 
			editor:{xtype:"ad_Reports_Lov", maxLength:64, caseRestriction:"uppercase",
				retFieldMapping: [{lovField:"id", dsField: "reportId"} ,{lovField:"name", dsField: "reportName"} ]}})
		.addTextColumn({name:"reportName", dataIndex:"reportName", width:200, noEdit: true, maxLength:255})
		.addLov({name:"dataSource", dataIndex:"dataSource", width:150, xtype:"gridcolumn", 
			editor:{xtype:"ad_DsReports_Lov", maxLength:255,
				retFieldMapping: [{lovField:"id", dsField: "dsReportId"} ],
				filterFieldMapping: [{lovField:"reportId", dsField: "reportId"} ]}})
		.addTextColumn({name:"frameName", dataIndex:"frameName", width:200, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"toolbarKey", dataIndex:"toolbarKey", width:120, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addTextColumn({name:"dcKey", dataIndex:"dcKey", width:120, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addNumberColumn({name:"sequenceNo", dataIndex:"sequenceNo", width:80, maxLength:4, align:"right" })
		.addTextColumn({name:"dsReportId", dataIndex:"dsReportId", hidden:true, width:100, noEdit: true, maxLength:64})
		.addTextColumn({name:"reportId", dataIndex:"reportId", hidden:true, width:100, noEdit: true, maxLength:64})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
