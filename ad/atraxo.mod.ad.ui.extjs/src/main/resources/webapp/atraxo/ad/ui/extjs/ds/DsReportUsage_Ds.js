/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.DsReportUsage_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_DsReportUsage_Ds"
	},
	
	
	fields: [
		{name:"dsReportId", type:"string"},
		{name:"dataSource", type:"string"},
		{name:"reportId", type:"string"},
		{name:"report", type:"string"},
		{name:"reportName", type:"string"},
		{name:"frameName", type:"string"},
		{name:"toolbarKey", type:"string"},
		{name:"dcKey", type:"string"},
		{name:"sequenceNo", type:"int", allowNull:true},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.DsReportUsage_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"dsReportId", type:"string"},
		{name:"dataSource", type:"string"},
		{name:"reportId", type:"string"},
		{name:"report", type:"string"},
		{name:"reportName", type:"string"},
		{name:"frameName", type:"string"},
		{name:"toolbarKey", type:"string"},
		{name:"dcKey", type:"string"},
		{name:"sequenceNo", type:"int", allowNull:true},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
