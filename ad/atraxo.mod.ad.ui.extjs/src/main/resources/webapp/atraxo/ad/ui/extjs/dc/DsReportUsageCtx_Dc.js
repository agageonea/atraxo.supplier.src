/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.DsReportUsageCtx_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.DsReportUsageCtx_Ds
});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.DsReportUsageCtx_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_DsReportUsageCtx_Dc$EditList",
	_noImport_: true,
	_noExport_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"frameName", dataIndex:"frameName", width:250, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"dcKey", dataIndex:"dcKey", width:80, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addTextColumn({name:"toolbarKey", dataIndex:"toolbarKey", width:80, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addNumberColumn({name:"sequenceNo", dataIndex:"sequenceNo", width:80, maxLength:4, align:"right" })
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
