/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.Attachment_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_Attachment_Ds"
	},
	
	
	validators: {
		type: [{type: 'presence'}]
	},
	
	fields: [
		{name:"targetRefid", type:"string"},
		{name:"targetAlias", type:"string"},
		{name:"targetType", type:"string"},
		{name:"categType", type:"string"},
		{name:"name", type:"string"},
		{name:"location", type:"string"},
		{name:"contentType", type:"string"},
		{name:"url", type:"string"},
		{name:"fileName", type:"string"},
		{name:"typeId", type:"string"},
		{name:"type", type:"string"},
		{name:"category", type:"string"},
		{name:"baseUrl", type:"string"},
		{name:"filePath", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.Attachment_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"targetRefid", type:"string"},
		{name:"targetAlias", type:"string"},
		{name:"targetType", type:"string"},
		{name:"categType", type:"string"},
		{name:"name", type:"string"},
		{name:"location", type:"string"},
		{name:"contentType", type:"string"},
		{name:"url", type:"string"},
		{name:"fileName", type:"string"},
		{name:"typeId", type:"string"},
		{name:"type", type:"string"},
		{name:"category", type:"string"},
		{name:"baseUrl", type:"string"},
		{name:"filePath", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
