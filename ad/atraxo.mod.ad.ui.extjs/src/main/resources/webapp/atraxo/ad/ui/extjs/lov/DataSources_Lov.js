/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.lov.DataSources_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.ad_DataSources_Lov",
	displayField: "name", 
	_columns_: ["id", "name", "description"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{name}, {description}</span>';
		},
		width:250, maxHeight:350
	},
	_editFrame_: {
		name: "atraxo.ad.ui.extjs.frame.DataSource_Ui"
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.ad.ui.extjs.ds.DataSourceLov_Ds
});
