/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.MenuItemRtLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_MenuItemRtLov_Ds"
	},
	
	
	validators: {
			name: [{type: 'presence'}]
	},
	
	fields: [
		{name:"title", type:"string"},
		{name:"sequenceNo", type:"int", allowNull:true},
		{name:"text", type:"string"},
		{name:"frame", type:"string"},
		{name:"bundle", type:"string"},
		{name:"leaf", type:"boolean"},
		{name:"iconUrl", type:"string"},
		{name:"separatorBefore", type:"boolean"},
		{name:"separatorAfter", type:"boolean"},
		{name:"glyph", type:"string"},
		{name:"menuItemId", type:"string"},
		{name:"menuItem", type:"string"},
		{name:"menuId", type:"string"},
		{name:"menu", type:"string"},
		{name:"id", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.MenuItemRtLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"title", type:"string"},
		{name:"sequenceNo", type:"int", allowNull:true},
		{name:"text", type:"string"},
		{name:"frame", type:"string"},
		{name:"bundle", type:"string"},
		{name:"leaf", type:"boolean", allowNull:true},
		{name:"iconUrl", type:"string"},
		{name:"separatorBefore", type:"boolean", allowNull:true},
		{name:"separatorAfter", type:"boolean", allowNull:true},
		{name:"glyph", type:"string"},
		{name:"menuItemId", type:"string"},
		{name:"menuItem", type:"string"},
		{name:"menuId", type:"string"},
		{name:"menu", type:"string"},
		{name:"id", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
