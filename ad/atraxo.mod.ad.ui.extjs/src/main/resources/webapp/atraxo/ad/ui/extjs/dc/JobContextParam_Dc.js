/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.JobContextParam_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.JobContextParam_Ds
});

/* ================= EDIT-GRID: CtxEditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.JobContextParam_Dc$CtxEditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_JobContextParam_Dc$CtxEditList",
	_noImport_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"paramName", dataIndex:"paramName", width:200, noEdit: true, maxLength:255})
		.addTextColumn({name:"dataType", dataIndex:"dataType", width:150, noEdit: true, maxLength:255})
		.addTextColumn({name:"value", dataIndex:"value", width:200, allowBlank: false, maxLength:1000, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:1000}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
