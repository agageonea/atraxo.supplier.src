/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.frame.Role_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Role_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("rol", Ext.create(atraxo.ad.ui.extjs.dc.Role_Dc,{multiEdit: true}))
		.addDc("usr", Ext.create(atraxo.ad.ui.extjs.dc.User_Dc,{}))
		.addDc("acl", Ext.create(atraxo.ad.ui.extjs.dc.AccessControl_Dc,{}))
		.addDc("menu", Ext.create(atraxo.ad.ui.extjs.dc.Menu_Dc,{}))
		.addDc("mi", Ext.create(atraxo.ad.ui.extjs.dc.MenuItem_Dc,{}))
		.linkDc("usr", "rol",{fetchMode:"auto",fields:[
					{childParam:"withRoleId", parentField:"id"}]})
				.linkDc("acl", "rol",{fetchMode:"auto",fields:[
					{childParam:"withRoleId", parentField:"id"}]})
				.linkDc("menu", "rol",{fetchMode:"auto",fields:[
					{childParam:"withRoleId", parentField:"id"}]})
				.linkDc("mi", "rol",{fetchMode:"auto",fields:[
					{childParam:"withRoleId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnAsgnRoleToUsers", disabled:true, handler: this.onBtnAsgnRoleToUsers,stateManager:[{ name:"selected_one_clean", dc:"rol"}], scope:this})
		.addButton({name:"btnAsgnRoleToAccessCtrl", disabled:true, handler: this.onBtnAsgnRoleToAccessCtrl,stateManager:[{ name:"selected_one_clean", dc:"rol"}], scope:this})
		.addButton({name:"btnAsgnRoleToMenuItem", disabled:true, handler: this.onBtnAsgnRoleToMenuItem,stateManager:[{ name:"selected_one_clean", dc:"rol"}], scope:this})
		.addButton({name:"btnAsgnRoleToMenu", disabled:true, handler: this.onBtnAsgnRoleToMenu,stateManager:[{ name:"selected_one_clean", dc:"rol"}], scope:this})
		.addButton({name:"btnDetailsUser", disabled:true, handler: this.onBtnDetailsUser,stateManager:[{ name:"selected_one", dc:"usr"}], scope:this})
		.addButton({name:"btnDetailsPrivilege", disabled:true, handler: this.onBtnDetailsPrivilege,stateManager:[{ name:"selected_one", dc:"acl"}], scope:this})
		.addDcFilterFormView("rol", {name:"rolFilter", _hasTitle_:true, xtype:"ad_Role_Dc$FilterPG"})
		.addDcEditGridView("rol", {name:"rolList", xtype:"ad_Role_Dc$EditList", frame:true})
		.addDcGridView("usr", {name:"usrList", _hasTitle_:true, xtype:"ad_User_Dc$ListShort"})
		.addDcGridView("acl", {name:"aclList", _hasTitle_:true, xtype:"ad_AccessControl_Dc$List"})
		.addDcGridView("menu", {name:"menuList", _hasTitle_:true, xtype:"ad_Menu_Dc$List"})
		.addDcGridView("mi", {name:"miList", _hasTitle_:true, xtype:"ad_MenuItem_Dc$List"})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", height:300, xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"main", dcName:"usr"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["rolList", "detailsTab"], ["center", "south"])
		.addChildrenTo("detailsTab", ["usrList"])
		.addToolbarTo("main", "tlbRolList")
		.addToolbarTo("usrList", "tlbUsrList")
		.addToolbarTo("aclList", "tlbAclList")
		.addToolbarTo("menuList", "tlbMenuList")
		.addToolbarTo("miList", "tlbMiList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("detailsTab", ["aclList", "menuList", "miList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbRolList", {dc: "rol"})
			.addNew({inContainer:"main",showView:"canvas2"}).addSave().addCancel().addCopy().addDelete()
			.addReports()
		.end()
		.beginToolbar("tlbUsrList", {dc: "usr"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnAsgnRoleToUsers"),this._elems_.get("btnDetailsUser")])
			.addReports()
		.end()
		.beginToolbar("tlbAclList", {dc: "acl"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnAsgnRoleToAccessCtrl"),this._elems_.get("btnDetailsPrivilege")])
			.addReports()
		.end()
		.beginToolbar("tlbMenuList", {dc: "menu"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnAsgnRoleToMenu")])
			.addReports()
		.end()
		.beginToolbar("tlbMiList", {dc: "mi"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnAsgnRoleToMenuItem")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnAsgnRoleToUsers
	 */
	,onBtnAsgnRoleToUsers: function() {
		this._showAsgnWindow_("atraxo.ad.ui.extjs.asgn.Role_User_Asgn$Ui" ,{dc: "rol", objectIdField: "id"
		,listeners: {close: {scope: this, fn: function() { this._getDc_("usr").doQuery();
		}} }});
	}
	
	/**
	 * On-Click handler for button btnAsgnRoleToAccessCtrl
	 */
	,onBtnAsgnRoleToAccessCtrl: function() {
		this._showAsgnWindow_("atraxo.ad.ui.extjs.asgn.Role_AccessControl_Asgn$Ui" ,{dc: "rol", objectIdField: "id"
		,listeners: {close: {scope: this, fn: function() { this._getDc_("acl").doQuery();
		}} }});
	}
	
	/**
	 * On-Click handler for button btnAsgnRoleToMenuItem
	 */
	,onBtnAsgnRoleToMenuItem: function() {
		this._showAsgnWindow_("atraxo.ad.ui.extjs.asgn.Role_MenuItem_Asgn$Ui" ,{dc: "rol", objectIdField: "id"
		,listeners: {close: {scope: this, fn: function() { this._getDc_("mi").doQuery();
		}} }});
	}
	
	/**
	 * On-Click handler for button btnAsgnRoleToMenu
	 */
	,onBtnAsgnRoleToMenu: function() {
		this._showAsgnWindow_("atraxo.ad.ui.extjs.asgn.Role_Menu_Asgn$Ui" ,{dc: "rol", objectIdField: "id"
		,listeners: {close: {scope: this, fn: function() { this._getDc_("menu").doQuery();
		}} }});
	}
	
	/**
	 * On-Click handler for button btnDetailsUser
	 */
	,onBtnDetailsUser: function() {
		var bundle = "atraxo.mod.ad";
		var frame = "atraxo.ad.ui.extjs.frame.User_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				id: this._getDc_("usr").getRecord().get("id"),
				code: this._getDc_("usr").getRecord().get("code")
			},
			callback: function (params) {
				this._when_called_for_details(params);
			}
		});
	}
	
	/**
	 * On-Click handler for button btnDetailsPrivilege
	 */
	,onBtnDetailsPrivilege: function() {
		var bundle = "atraxo.mod.ad";
		var frame = "atraxo.ad.ui.extjs.frame.AccessControl_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				id: this._getDc_("acl").getRecord().get("id"),
				name: this._getDc_("acl").getRecord().get("name")
			},
			callback: function (params) {
				this._when_called_for_details(params);
			}
		});
	}
	
	,_when_called_for_details: function(params) {
		
						var dc = this._getDc_("rol");
						dc.setFilterValue("id", params.id);
						dc.setFilterValue("code", params.code);
						dc.doQuery();
	}
});
