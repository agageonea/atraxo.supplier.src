/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.ParamValue_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ad.ui.extjs.ds.ParamValue_DsParam,
	recordModel: atraxo.ad.ui.extjs.ds.ParamValue_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.ParamValue_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_ParamValue_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"sysparam", bind:"{d.sysParam}", dataIndex:"sysParam", xtype:"ad_Params_Lov", maxLength:64, caseRestriction:"uppercase"})
		.addDateField({name:"validAt", bind:"{p.validAt}", paramIndex:"validAt"})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"})
		.addPanel({ name:"col2", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["sysparam"])
		.addChildrenTo("col2", ["validAt"]);
	}
});

/* ================= FILTER: FilterPG ================= */


Ext.define("atraxo.ad.ui.extjs.dc.ParamValue_Dc$FilterPG", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_ParamValue_Dc$FilterPG",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"sysparam", dataIndex:"sysParam", maxLength:64, caseRestriction:"uppercase",
				editor:{xtype:"atraxo.ad.ui.extjs.lov.Params_Lov", selectOnFocus:true, maxLength:64, caseRestriction:"uppercase"}})
			.addDateField({name:"validAt", paramIndex:"validAt"})
		;
	}

});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.ParamValue_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_ParamValue_Dc$EditList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"sysparam", dataIndex:"sysParam", width:250, xtype:"gridcolumn", 
			editor:{xtype:"ad_Params_Lov", maxLength:64, caseRestriction:"uppercase"}})
		.addTextColumn({name:"value", dataIndex:"value", width:350, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addDateColumn({name:"validFrom", dataIndex:"validFrom", _mask_: Masks.DATE })
		.addDateColumn({name:"validTo", dataIndex:"validTo", _mask_: Masks.DATE })
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT-GRID: CtxEditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.ParamValue_Dc$CtxEditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_ParamValue_Dc$CtxEditList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"value", dataIndex:"value", width:350, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addDateColumn({name:"validFrom", dataIndex:"validFrom", _mask_: Masks.DATE })
		.addDateColumn({name:"validTo", dataIndex:"validTo", _mask_: Masks.DATE })
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
