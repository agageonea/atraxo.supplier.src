/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.Client_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_Client_Ds"
	},
	
	
	validators: {
			name: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("workspacePath", "../../app/storage");
		this.set("importPath", "../../app/storage/import");
		this.set("exportPath", "../../app/storage/export");
		this.set("tempPath", "../../app/storage/temp");
	},
	
	fields: [
		{name:"workspacePath", type:"string"},
		{name:"importPath", type:"string"},
		{name:"exportPath", type:"string"},
		{name:"tempPath", type:"string"},
		{name:"adminRole", type:"string"},
		{name:"product", type:"string"},
		{name:"id", type:"string"},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.Client_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"workspacePath", type:"string"},
		{name:"importPath", type:"string"},
		{name:"exportPath", type:"string"},
		{name:"tempPath", type:"string"},
		{name:"adminRole", type:"string"},
		{name:"product", type:"string"},
		{name:"id", type:"string"},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});

Ext.define("atraxo.ad.ui.extjs.ds.Client_DsParam", {
	extend: 'Ext.data.Model',



	initParam: function() {
		this.set("adminUserCode", "ADMIN");
		this.set("adminUserName", "Administrator");
		this.set("adminUserLogin", "admin");
		this.set("initFileLocation", "../../app/storage/idl/client/index.xml");
	},

	fields: [
		{name:"adminPassword", type:"string"},
		{name:"adminPasswordRe", type:"string"},
		{name:"adminUserCode", type:"string"},
		{name:"adminUserLogin", type:"string"},
		{name:"adminUserName", type:"string"},
		{name:"initFileLocation", type:"string"}
	]
});
