/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.ExternalReport_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_ExternalReport_Ds"
	},
	
	
	validators: {
		name: [{type: 'presence'}],
		description: [{type: 'presence'}],
		businessArea: [{type: 'presence'}]
	},
	
	fields: [
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"system", type:"boolean"},
		{name:"designName", type:"string"},
		{name:"reportCode", type:"string"},
		{name:"reportId", type:"string"},
		{name:"businessAreaId", type:"string"},
		{name:"businessArea", type:"string"},
		{name:"dsName", type:"string"},
		{name:"dsModel", type:"string"},
		{name:"dsAlias", type:"string"},
		{name:"url", type:"string", noFilter:true, noSort:true},
		{name:"lastExecution", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"lastExecutionStatus", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean"},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.ExternalReport_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"system", type:"boolean", allowNull:true},
		{name:"designName", type:"string"},
		{name:"reportCode", type:"string"},
		{name:"reportId", type:"string"},
		{name:"businessAreaId", type:"string"},
		{name:"businessArea", type:"string"},
		{name:"dsName", type:"string"},
		{name:"dsModel", type:"string"},
		{name:"dsAlias", type:"string"},
		{name:"url", type:"string", noFilter:true, noSort:true},
		{name:"lastExecution", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"lastExecutionStatus", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"notes", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"clientId", type:"string"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"createdBy", type:"string"},
		{name:"modifiedBy", type:"string"},
		{name:"version", type:"int", allowNull:true},
		{name:"refid", type:"string"},
		{name:"entityAlias", type:"string"},
		{name:"entityFqn", type:"string"}
	]
});

Ext.define("atraxo.ad.ui.extjs.ds.ExternalReport_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"format", type:"string"}
	]
});
