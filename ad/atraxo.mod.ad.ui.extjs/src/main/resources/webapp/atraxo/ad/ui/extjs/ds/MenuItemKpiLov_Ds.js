/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ad.ui.extjs.ds.MenuItemKpiLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ad_MenuItemKpiLov_Ds"
	},
	
	
	fields: [
		{name:"title", type:"string"},
		{name:"sequenceNo", type:"int", allowNull:true},
		{name:"menuId", type:"string"},
		{name:"menu", type:"string"},
		{name:"frame", type:"string"},
		{name:"bundle", type:"string"},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"active", type:"boolean"},
		{name:"refid", type:"string"}
	]
});
	
Ext.define("atraxo.ad.ui.extjs.ds.MenuItemKpiLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"title", type:"string"},
		{name:"sequenceNo", type:"int", allowNull:true},
		{name:"menuId", type:"string"},
		{name:"menu", type:"string"},
		{name:"frame", type:"string"},
		{name:"bundle", type:"string"},
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"refid", type:"string"}
	]
});
