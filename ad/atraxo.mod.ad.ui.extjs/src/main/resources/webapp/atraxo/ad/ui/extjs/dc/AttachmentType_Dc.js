/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.AttachmentType_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ad.ui.extjs.ds.AttachmentType_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.AttachmentType_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_AttachmentType_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", maxLength:255})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active"})
		.addCombo({ xtype:"combo", name:"category", bind:"{d.category}", dataIndex:"category", store:[ __AD__.TAttachmentType._LINK_, __AD__.TAttachmentType._UPLOAD_]})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"})
		.addPanel({ name:"col2", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["name", "category"])
		.addChildrenTo("col2", ["active"]);
	}
});

/* ================= FILTER: FilterPG ================= */


Ext.define("atraxo.ad.ui.extjs.dc.AttachmentType_Dc$FilterPG", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_AttachmentType_Dc$FilterPG",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:255})
			.addBooleanField({ name:"active", dataIndex:"active"})
			.addCombo({ xtype:"combo", name:"category", dataIndex:"category", store:[ __AD__.TAttachmentType._LINK_, __AD__.TAttachmentType._UPLOAD_]})
		;
	}

});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.AttachmentType_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_AttachmentType_Dc$EditList",
	_bulkEditFields_: ["active","description","category","uploadPath","baseUrl"],

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"name", dataIndex:"name", width:200, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"description", dataIndex:"description", width:200, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addComboColumn({name:"category", dataIndex:"category", width:80, 
			editor:{xtype:"combo", mode: 'local', triggerAction:'all', forceSelection:true, store:[ __AD__.TAttachmentType._LINK_, __AD__.TAttachmentType._UPLOAD_]}})
		.addTextColumn({name:"uploadPath", dataIndex:"uploadPath", width:200, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addTextColumn({name:"baseUrl", dataIndex:"baseUrl", width:100, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addBooleanColumn({name:"active", dataIndex:"active"})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
