/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.MenuItem_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ad.ui.extjs.ds.MenuItem_DsParam,
	recordModel: atraxo.ad.ui.extjs.ds.MenuItem_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ad.ui.extjs.dc.MenuItem_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ad_MenuItem_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"title", dataIndex:"title", maxLength:255})
			.addLov({name:"name", dataIndex:"name", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.MenuItems_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "id"} ]}})
			.addTextField({ name:"frame", dataIndex:"frame", maxLength:255})
			.addTextField({ name:"bundle", dataIndex:"bundle", maxLength:255})
			.addTextField({ name:"iconUrl", dataIndex:"iconUrl", maxLength:255})
			.addBooleanField({ name:"active", dataIndex:"active"})
			.addBooleanField({ name:"separatorBefore", dataIndex:"separatorBefore"})
			.addBooleanField({ name:"separatorAfter", dataIndex:"separatorAfter"})
			.addBooleanField({ name:"foldersOnly", paramIndex:"foldersOnly"})
			.addBooleanField({ name:"framesOnly", paramIndex:"framesOnly"})
			.addTextField({ name:"glyph", dataIndex:"glyph", maxLength:64})
			.addLov({name:"menuItem", dataIndex:"menuItem", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.MenuItems_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "menuItemId"} ],
					filterFieldMapping: [{lovParam:"foldersOnly", value: "true"} ]}})
			.addLov({name:"menu", dataIndex:"menu", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.Menus_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "menuId"} ]}})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.MenuItem_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_MenuItem_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addNumberColumn({ name:"sequenceNo", dataIndex:"sequenceNo", width:70, format:"0"})
		.addTextColumn({ name:"name", dataIndex:"name", width:150})
		.addTextColumn({ name:"title", dataIndex:"title", width:150})
		.addTextColumn({ name:"menu", dataIndex:"menu", width:150})
		.addTextColumn({ name:"menuItem", dataIndex:"menuItem", width:150})
		.addTextColumn({ name:"frame", dataIndex:"frame", width:300})
		.addTextColumn({ name:"bundle", dataIndex:"bundle", width:150})
		.addTextColumn({ name:"glyph", dataIndex:"glyph", width:150})
		.addBooleanColumn({ name:"active", dataIndex:"active"})
		.addTextColumn({ name:"description", dataIndex:"description", hidden:true, width:200})
		.addBooleanColumn({ name:"separatorBefore", dataIndex:"separatorBefore", hidden:true})
		.addBooleanColumn({ name:"separatorAfter", dataIndex:"separatorAfter", hidden:true})
		.addTextColumn({ name:"iconUrl", dataIndex:"iconUrl", hidden:true, width:100})
		.addTextColumn({ name:"menuId", dataIndex:"menuId", hidden:true, width:100})
		.addTextColumn({ name:"menuItemId", dataIndex:"menuItemId", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.ad.ui.extjs.dc.MenuItem_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ad_MenuItem_Dc$EditList",
	_bulkEditFields_: ["menu","menuItem","active","bundle","frame","sequenceNo"],

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addNumberColumn({name:"sequenceNo", dataIndex:"sequenceNo", width:70, allowBlank: false, maxLength:4, align:"right", format:"0" })
		.addTextColumn({name:"name", dataIndex:"name", width:150, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"title", dataIndex:"title", width:150, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addLov({name:"menu", dataIndex:"menu", width:150, xtype:"gridcolumn", 
			editor:{xtype:"ad_Menus_Lov", maxLength:255,
				retFieldMapping: [{lovField:"id", dsField: "menuId"} ]}})
		.addLov({name:"menuItem", dataIndex:"menuItem", width:150, xtype:"gridcolumn", 
			editor:{xtype:"ad_MenuItems_Lov", maxLength:255,
				retFieldMapping: [{lovField:"id", dsField: "menuItemId"} ],
				filterFieldMapping: [{lovParam:"foldersOnly", value: "true"} ]}})
		.addTextColumn({name:"frame", dataIndex:"frame", width:300, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"bundle", dataIndex:"bundle", width:150, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"glyph", dataIndex:"glyph", width:150, maxLength:64, 
			editor: { xtype:"textfield", maxLength:64}})
		.addBooleanColumn({name:"active", dataIndex:"active", width:70})
		.addTextColumn({name:"description", dataIndex:"description", hidden:true, width:200, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addBooleanColumn({name:"separatorBefore", dataIndex:"separatorBefore", hidden:true})
		.addBooleanColumn({name:"separatorAfter", dataIndex:"separatorAfter", hidden:true})
		.addTextColumn({name:"iconUrl", dataIndex:"iconUrl", hidden:true, width:100, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"menuId", dataIndex:"menuId", hidden:true, width:100, maxLength:64, 
			editor: { xtype:"textfield", maxLength:64}})
		.addTextColumn({name:"menuItemId", dataIndex:"menuItemId", hidden:true, width:100, maxLength:64, 
			editor: { xtype:"textfield", maxLength:64}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.ad.ui.extjs.dc.MenuItem_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ad_MenuItem_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", maxLength:255})
		.addTextField({ name:"title", bind:"{d.title}", dataIndex:"title", maxLength:255})
		.addTextField({ name:"frame", bind:"{d.frame}", dataIndex:"frame", maxLength:255})
		.addTextField({ name:"bundle", bind:"{d.bundle}", dataIndex:"bundle", maxLength:255})
		.addTextField({ name:"iconUrl", bind:"{d.iconUrl}", dataIndex:"iconUrl", maxLength:255})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active"})
		.addBooleanField({ name:"separatorBefore", bind:"{d.separatorBefore}", dataIndex:"separatorBefore"})
		.addBooleanField({ name:"separatorAfter", bind:"{d.separatorAfter}", dataIndex:"separatorAfter"})
		.addTextField({ name:"glyph", bind:"{d.glyph}", dataIndex:"glyph", maxLength:64})
		.addLov({name:"menuItem", bind:"{d.menuItem}", dataIndex:"menuItem", xtype:"ad_MenuItems_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "menuItemId"} ]})
		.addLov({name:"menu", bind:"{d.menu}", dataIndex:"menu", xtype:"ad_Menus_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "menuId"} ]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col0", width:300, layout:"anchor"})
		.addPanel({ name:"col1", width:300, layout:"anchor"})
		.addPanel({ name:"col2", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col0", "col1", "col2"])
		.addChildrenTo("col0", ["title"])
		.addChildrenTo("col1", ["name", "separatorBefore", "separatorAfter", "iconUrl", "active"])
		.addChildrenTo("col2", ["menu", "menuItem", "frame", "bundle", "glyph"]);
	}
});
