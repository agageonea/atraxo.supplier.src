/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ad.ui.extjs.dc.JobLog_Dc", {
	extend: "e4e.dc.AbstractDc",
	filterModel: atraxo.ad.ui.extjs.ds.JobLog_DsFilter,
	recordModel: atraxo.ad.ui.extjs.ds.JobLog_Ds
});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.ad.ui.extjs.dc.JobLog_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.ad_JobLog_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"id", bind:"{d.id}", dataIndex:"id", maxLength:64})
		.addLov({name:"jobName", bind:"{d.jobName}", dataIndex:"jobName", xtype:"ad_Jobs_Lov", maxLength:255})
		.addLov({name:"jobContext", bind:"{d.jobContext}", dataIndex:"jobContext", xtype:"ad_JobContexts_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "jobContextId"} ],
			filterFieldMapping: [{lovField:"jobName", dsField: "jobName"} ]})
		.addLov({name:"jobTimer", bind:"{d.jobTimer}", dataIndex:"jobTimer", xtype:"ad_JobTimers_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "jobTimerId"} ],
			filterFieldMapping: [{lovField:"jobContextId", dsField: "jobContextId"} ]})
		.addDateField({name:"startTime_From", dataIndex:"startTime_From", emptyText:"From" })
		.addDateField({name:"startTime_To", dataIndex:"startTime_To", emptyText:"To" })
		.addFieldContainer({name: "startTime"})
			.addChildrenTo("startTime",["startTime_From", "startTime_To"])
		.addDateField({name:"endTime_From", dataIndex:"endTime_From", emptyText:"From" })
		.addDateField({name:"endTime_To", dataIndex:"endTime_To", emptyText:"To" })
		.addFieldContainer({name: "endTime"})
			.addChildrenTo("endTime",["endTime_From", "endTime_To"])
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:280, layout:"anchor"})
		.addPanel({ name:"col2", width:250, layout:"anchor"})
		.addPanel({ name:"col3", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3"])
		.addChildrenTo("col1", ["jobName", "jobContext"])
		.addChildrenTo("col2", ["jobTimer", "id"])
		.addChildrenTo("col3", ["startTime", "endTime"]);
	}
});

/* ================= GRID: List ================= */

Ext.define("atraxo.ad.ui.extjs.dc.JobLog_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_JobLog_Dc$List",
	_noImport_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"jobName", dataIndex:"jobName", width:200})
		.addTextColumn({ name:"jobContext", dataIndex:"jobContext", width:200})
		.addTextColumn({ name:"jobTimer", dataIndex:"jobTimer", width:200})
		.addDateColumn({ name:"startTime", dataIndex:"startTime", _mask_: Masks.DATETIME})
		.addDateColumn({ name:"endTime", dataIndex:"endTime", _mask_: Masks.DATETIME})
		.addTextColumn({ name:"jobContextId", dataIndex:"jobContextId", hidden:true, width:100})
		.addTextColumn({ name:"jobTimerId", dataIndex:"jobTimerId", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= GRID: CtxListJob ================= */

Ext.define("atraxo.ad.ui.extjs.dc.JobLog_Dc$CtxListJob", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_JobLog_Dc$CtxListJob",
	_noImport_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"jobContext", dataIndex:"jobContext", width:200})
		.addDateColumn({ name:"startTime", dataIndex:"startTime", _mask_: Masks.DATETIME})
		.addDateColumn({ name:"endTime", dataIndex:"endTime", _mask_: Masks.DATETIME})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= GRID: CtxListJobContext ================= */

Ext.define("atraxo.ad.ui.extjs.dc.JobLog_Dc$CtxListJobContext", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ad_JobLog_Dc$CtxListJobContext",
	_noImport_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"startTime", dataIndex:"startTime", width:150, _mask_: Masks.DATETIME})
		.addDateColumn({ name:"endTime", dataIndex:"endTime", width:150, _mask_: Masks.DATETIME})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
