/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.security.AccessControl;
import atraxo.ad.domain.impl.security.AccessControlAsgn;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = AccessControlAsgn.class, sort = {@SortField(field = AccessControlAsgn_Ds.F_ASGNNAME)})
@RefLookups({@RefLookup(refId = AccessControlAsgn_Ds.F_ACCESSCONTROLID, namedQuery = AccessControl.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = AccessControlAsgn_Ds.F_ACCESSCONTROL)})})
public class AccessControlAsgn_Ds
		extends
			AbstractAuditable_Ds<AccessControlAsgn> {

	public static final String ALIAS = "ad_AccessControlAsgn_Ds";

	public static final String F_ASGNNAME = "asgnName";
	public static final String F_QUERYALLOWED = "queryAllowed";
	public static final String F_UPDATEALLOWED = "updateAllowed";
	public static final String F_IMPORTALLOWED = "importAllowed";
	public static final String F_EXPORTALLOWED = "exportAllowed";
	public static final String F_ACCESSCONTROLID = "accessControlId";
	public static final String F_ACCESSCONTROL = "accessControl";

	@DsField
	private String asgnName;

	@DsField
	private Boolean queryAllowed;

	@DsField
	private Boolean updateAllowed;

	@DsField
	private Boolean importAllowed;

	@DsField
	private Boolean exportAllowed;

	@DsField(join = "left", path = "accessControl.id")
	private String accessControlId;

	@DsField(join = "left", path = "accessControl.name")
	private String accessControl;

	/**
	 * Default constructor
	 */
	public AccessControlAsgn_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AccessControlAsgn_Ds(AccessControlAsgn e) {
		super(e);
	}

	public String getAsgnName() {
		return this.asgnName;
	}

	public void setAsgnName(String asgnName) {
		this.asgnName = asgnName;
	}

	public Boolean getQueryAllowed() {
		return this.queryAllowed;
	}

	public void setQueryAllowed(Boolean queryAllowed) {
		this.queryAllowed = queryAllowed;
	}

	public Boolean getUpdateAllowed() {
		return this.updateAllowed;
	}

	public void setUpdateAllowed(Boolean updateAllowed) {
		this.updateAllowed = updateAllowed;
	}

	public Boolean getImportAllowed() {
		return this.importAllowed;
	}

	public void setImportAllowed(Boolean importAllowed) {
		this.importAllowed = importAllowed;
	}

	public Boolean getExportAllowed() {
		return this.exportAllowed;
	}

	public void setExportAllowed(Boolean exportAllowed) {
		this.exportAllowed = exportAllowed;
	}

	public String getAccessControlId() {
		return this.accessControlId;
	}

	public void setAccessControlId(String accessControlId) {
		this.accessControlId = accessControlId;
	}

	public String getAccessControl() {
		return this.accessControl;
	}

	public void setAccessControl(String accessControl) {
		this.accessControl = accessControl;
	}
}
