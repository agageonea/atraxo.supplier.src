/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.attachment.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeLov_Ds;
import atraxo.ad.domain.impl.ad.TAttachmentType;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = AttachmentType.class, sort = {@SortField(field = AttachmentTypeLov_Ds.F_NAME)})
public class AttachmentTypeLov_Ds extends AbstractTypeLov_Ds<AttachmentType> {

	public static final String ALIAS = "ad_AttachmentTypeLov_Ds";

	public static final String F_CATEGORY = "category";
	public static final String F_TARGETALIAS = "targetAlias";
	public static final String F_TARGETTYPE = "targetType";

	@DsField
	private TAttachmentType category;

	@DsField(fetch = false, jpqlFilter = "  e.id in (select t.sourceRefId.id from TargetRule t where  t.targetAlias = :targetAlias and  t.targetType = :targetType and t.clientId = :clientId ) ")
	private String targetAlias;

	@DsField(fetch = false)
	private String targetType;

	/**
	 * Default constructor
	 */
	public AttachmentTypeLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AttachmentTypeLov_Ds(AttachmentType e) {
		super(e);
	}

	public TAttachmentType getCategory() {
		return this.category;
	}

	public void setCategory(TAttachmentType category) {
		this.category = category;
	}

	public String getTargetAlias() {
		return this.targetAlias;
	}

	public void setTargetAlias(String targetAlias) {
		this.targetAlias = targetAlias;
	}

	public String getTargetType() {
		return this.targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}
}
