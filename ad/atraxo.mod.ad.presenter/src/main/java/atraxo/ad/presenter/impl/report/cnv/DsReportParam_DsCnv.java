/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.report.cnv;

import atraxo.ad.business.api.report.IReportService;
import atraxo.ad.domain.impl.report.DsReportParam;
import atraxo.ad.domain.impl.report.Report;
import atraxo.ad.presenter.impl.report.model.DsReportParam_Ds;
import javax.persistence.EntityManager;
import seava.j4e.api.action.result.IDsConverter;
import seava.j4e.presenter.converter.AbstractDsConverter;

/**
 * Converter DsReportParam_DsCnv
 *
 * Generated code. Do not modify in this file.
 */
public class DsReportParam_DsCnv
		extends
			AbstractDsConverter<DsReportParam_Ds, DsReportParam>
		implements
			IDsConverter<DsReportParam_Ds, DsReportParam> {

	protected void modelToEntityReferences(DsReportParam_Ds ds,
			DsReportParam e, boolean isInsert, EntityManager em)
			throws Exception {

		if (ds.getReportId() == null) {
			Report x = ((IReportService) findEntityService(Report.class))
					.findByCode(ds.getReportCode());
			ds.setReportId(x.getId());
		}
		super.modelToEntityReferences(ds, e, isInsert, em);
	}
}
