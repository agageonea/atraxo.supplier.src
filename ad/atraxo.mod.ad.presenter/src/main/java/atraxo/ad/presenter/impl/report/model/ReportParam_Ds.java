/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.report.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractType_Ds;
import atraxo.ad.domain.impl.ad.TDataType;
import atraxo.ad.domain.impl.report.Report;
import atraxo.ad.domain.impl.report.ReportParam;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ReportParam.class, sort = {@SortField(field = ReportParam_Ds.F_NAME)})
@RefLookups({@RefLookup(refId = ReportParam_Ds.F_REPORTID, namedQuery = Report.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = ReportParam_Ds.F_REPORTCODE)})})
public class ReportParam_Ds extends AbstractType_Ds<ReportParam> {

	public static final String ALIAS = "ad_ReportParam_Ds";

	public static final String F_REPORTID = "reportId";
	public static final String F_REPORTCODE = "reportCode";
	public static final String F_TITLE = "title";
	public static final String F_SEQUENCENO = "sequenceNo";
	public static final String F_DEFAULTVALUE = "defaultValue";
	public static final String F_DATATYPE = "dataType";
	public static final String F_LISTOFVALUES = "listOfValues";
	public static final String F_NOEDIT = "noEdit";
	public static final String F_MANDATORY = "mandatory";

	@DsField(join = "left", path = "report.id")
	private String reportId;

	@DsField(join = "left", path = "report.code")
	private String reportCode;

	@DsField
	private String title;

	@DsField
	private Integer sequenceNo;

	@DsField
	private String defaultValue;

	@DsField
	private TDataType dataType;

	@DsField
	private String listOfValues;

	@DsField
	private Boolean noEdit;

	@DsField
	private Boolean mandatory;

	/**
	 * Default constructor
	 */
	public ReportParam_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ReportParam_Ds(ReportParam e) {
		super(e);
	}

	public String getReportId() {
		return this.reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getReportCode() {
		return this.reportCode;
	}

	public void setReportCode(String reportCode) {
		this.reportCode = reportCode;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getSequenceNo() {
		return this.sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public TDataType getDataType() {
		return this.dataType;
	}

	public void setDataType(TDataType dataType) {
		this.dataType = dataType;
	}

	public String getListOfValues() {
		return this.listOfValues;
	}

	public void setListOfValues(String listOfValues) {
		this.listOfValues = listOfValues;
	}

	public Boolean getNoEdit() {
		return this.noEdit;
	}

	public void setNoEdit(Boolean noEdit) {
		this.noEdit = noEdit;
	}

	public Boolean getMandatory() {
		return this.mandatory;
	}

	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}
}
