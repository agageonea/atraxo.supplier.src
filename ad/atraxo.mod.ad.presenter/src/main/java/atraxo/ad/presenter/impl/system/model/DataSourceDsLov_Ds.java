/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractTypeNTLov_Ds;
import atraxo.ad.domain.impl.system.DataSource;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DataSource.class, jpqlWhere = " e.isAsgn = false ", sort = {@SortField(field = DataSourceDsLov_Ds.F_NAME)})
public class DataSourceDsLov_Ds extends AbstractTypeNTLov_Ds<DataSource> {

	public static final String ALIAS = "ad_DataSourceDsLov_Ds";

	/**
	 * Default constructor
	 */
	public DataSourceDsLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DataSourceDsLov_Ds(DataSource e) {
		super(e);
	}
}
