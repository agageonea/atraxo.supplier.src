/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.report.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractType_Ds;
import atraxo.ad.domain.impl.report.ReportServer;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ReportServer.class, sort = {@SortField(field = ReportServer_Ds.F_NAME)})
public class ReportServer_Ds extends AbstractType_Ds<ReportServer> {

	public static final String ALIAS = "ad_ReportServer_Ds";

	public static final String F_URL = "url";
	public static final String F_QUERYBUILDERCLASS = "queryBuilderClass";

	@DsField
	private String url;

	@DsField
	private String queryBuilderClass;

	/**
	 * Default constructor
	 */
	public ReportServer_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ReportServer_Ds(ReportServer e) {
		super(e);
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getQueryBuilderClass() {
		return this.queryBuilderClass;
	}

	public void setQueryBuilderClass(String queryBuilderClass) {
		this.queryBuilderClass = queryBuilderClass;
	}
}
