/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.qb;

import atraxo.ad.presenter.impl.security.model.MenuItemLov_Ds;
import atraxo.ad.presenter.impl.security.model.MenuItemLov_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class MenuItemLov_DsQb
		extends
			QueryBuilderWithJpql<MenuItemLov_Ds, MenuItemLov_Ds, MenuItemLov_DsParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getFoldersOnly() != null
				&& !"".equals(this.params.getFoldersOnly())) {
			addFilterCondition("  (  (:foldersOnly = false  ) or (e.frame is null ) ) ");
			addCustomFilterItem("foldersOnly", this.params.getFoldersOnly());
		}
		if (this.params != null && this.params.getFramesOnly() != null
				&& !"".equals(this.params.getFramesOnly())) {
			addFilterCondition("  (  (:framesOnly = false  ) or (e.frame is not  null ) ) ");
			addCustomFilterItem("framesOnly", this.params.getFramesOnly());
		}
	}
}
