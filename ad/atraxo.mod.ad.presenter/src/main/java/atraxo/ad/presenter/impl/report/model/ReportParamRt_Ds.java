/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.report.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractType_Ds;
import atraxo.ad.domain.impl.ad.TDataType;
import atraxo.ad.domain.impl.report.ReportParam;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ReportParam.class, jpqlWhere = "  e.active = true  ", sort = {
		@SortField(field = ReportParamRt_Ds.F_SEQUENCENO),
		@SortField(field = ReportParamRt_Ds.F_NAME)})
public class ReportParamRt_Ds extends AbstractType_Ds<ReportParam> {

	public static final String ALIAS = "ad_ReportParamRt_Ds";

	public static final String F_REPORTID = "reportId";
	public static final String F_REPORT = "report";
	public static final String F_VALUE = "value";
	public static final String F_DATATYPE = "dataType";
	public static final String F_TITLE = "title";
	public static final String F_LISTOFVALUES = "listOfValues";
	public static final String F_NOEDIT = "noEdit";
	public static final String F_MANDATORY = "mandatory";
	public static final String F_SEQUENCENO = "sequenceNo";

	@DsField(join = "left", path = "report.id")
	private String reportId;

	@DsField(join = "left", path = "report.code")
	private String report;

	@DsField(path = "defaultValue")
	private String value;

	@DsField
	private TDataType dataType;

	@DsField
	private String title;

	@DsField
	private String listOfValues;

	@DsField
	private Boolean noEdit;

	@DsField
	private Boolean mandatory;

	@DsField
	private Integer sequenceNo;

	/**
	 * Default constructor
	 */
	public ReportParamRt_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ReportParamRt_Ds(ReportParam e) {
		super(e);
	}

	public String getReportId() {
		return this.reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getReport() {
		return this.report;
	}

	public void setReport(String report) {
		this.report = report;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public TDataType getDataType() {
		return this.dataType;
	}

	public void setDataType(TDataType dataType) {
		this.dataType = dataType;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getListOfValues() {
		return this.listOfValues;
	}

	public void setListOfValues(String listOfValues) {
		this.listOfValues = listOfValues;
	}

	public Boolean getNoEdit() {
		return this.noEdit;
	}

	public void setNoEdit(Boolean noEdit) {
		this.noEdit = noEdit;
	}

	public Boolean getMandatory() {
		return this.mandatory;
	}

	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}

	public Integer getSequenceNo() {
		return this.sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
}
