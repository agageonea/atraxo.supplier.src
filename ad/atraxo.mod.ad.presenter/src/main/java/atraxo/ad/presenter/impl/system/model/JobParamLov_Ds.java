/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractTypeNT_Ds;
import atraxo.ad.domain.impl.system.JobParam;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = JobParam.class, sort = {@SortField(field = JobParamLov_Ds.F_NAME)})
public class JobParamLov_Ds extends AbstractTypeNT_Ds<JobParam> {

	public static final String ALIAS = "ad_JobParamLov_Ds";

	/**
	 * Default constructor
	 */
	public JobParamLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public JobParamLov_Ds(JobParam e) {
		super(e);
	}
}
