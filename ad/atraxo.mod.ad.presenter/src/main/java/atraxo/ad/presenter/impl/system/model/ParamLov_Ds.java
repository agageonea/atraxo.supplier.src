/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractTypeWithCodeNTLov_Ds;
import atraxo.ad.domain.impl.system.Param;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Param.class, sort = {@SortField(field = ParamLov_Ds.F_CODE)})
public class ParamLov_Ds extends AbstractTypeWithCodeNTLov_Ds<Param> {

	public static final String ALIAS = "ad_ParamLov_Ds";

	/**
	 * Default constructor
	 */
	public ParamLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ParamLov_Ds(Param e) {
		super(e);
	}
}
