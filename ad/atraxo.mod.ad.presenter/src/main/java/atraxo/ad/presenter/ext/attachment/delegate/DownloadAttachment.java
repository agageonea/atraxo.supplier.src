package atraxo.ad.presenter.ext.attachment.delegate;

import java.io.InputStream;

import atraxo.ad.business.api.attachment.IAttachmentService;
import atraxo.ad.domain.impl.attachment.Attachment;
import seava.j4e.api.service.IFileDownloadService;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class DownloadAttachment extends AbstractPresenterDelegate implements IFileDownloadService {

	@Override
	public InputStream execute(String refid, String name) throws Exception {
		IAttachmentService srv = (IAttachmentService) this.findEntityService(Attachment.class);
		return srv.download(refid);
	}
}
