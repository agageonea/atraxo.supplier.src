/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractTypeNT_Ds;
import atraxo.ad.domain.impl.system.DataSource;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DataSource.class, sort = {@SortField(field = DataSource_Ds.F_NAME)})
public class DataSource_Ds extends AbstractTypeNT_Ds<DataSource> {

	public static final String ALIAS = "ad_DataSource_Ds";

	public static final String F_MODEL = "model";
	public static final String F_ISASGN = "isAsgn";

	@DsField
	private String model;

	@DsField
	private Boolean isAsgn;

	/**
	 * Default constructor
	 */
	public DataSource_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DataSource_Ds(DataSource e) {
		super(e);
	}

	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Boolean getIsAsgn() {
		return this.isAsgn;
	}

	public void setIsAsgn(Boolean isAsgn) {
		this.isAsgn = isAsgn;
	}
}
