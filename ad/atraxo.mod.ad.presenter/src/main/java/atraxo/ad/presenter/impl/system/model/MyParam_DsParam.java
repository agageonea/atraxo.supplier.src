/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import java.util.Date;
/**
 * Generated code. Do not modify in this file.
 */
public class MyParam_DsParam {

	public static final String f_validFrom = "validFrom";
	public static final String f_validTo = "validTo";

	private Date validFrom;

	private Date validTo;

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
}
