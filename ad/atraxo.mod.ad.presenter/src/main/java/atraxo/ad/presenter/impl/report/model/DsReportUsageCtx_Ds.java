/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.report.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.report.DsReportUsage;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DsReportUsage.class)
@RefLookups({@RefLookup(refId = DsReportUsageCtx_Ds.F_DSREPORTID)})
public class DsReportUsageCtx_Ds extends AbstractAuditable_Ds<DsReportUsage> {

	public static final String ALIAS = "ad_DsReportUsageCtx_Ds";

	public static final String F_DSREPORTID = "dsReportId";
	public static final String F_REPORTID = "reportId";
	public static final String F_REPORT = "report";
	public static final String F_FRAMENAME = "frameName";
	public static final String F_TOOLBARKEY = "toolbarKey";
	public static final String F_DCKEY = "dcKey";
	public static final String F_SEQUENCENO = "sequenceNo";

	@DsField(join = "left", path = "dsReport.id")
	private String dsReportId;

	@DsField(join = "left", path = "dsReport.report.id")
	private String reportId;

	@DsField(join = "left", path = "dsReport.report.code")
	private String report;

	@DsField
	private String frameName;

	@DsField
	private String toolbarKey;

	@DsField
	private String dcKey;

	@DsField
	private Integer sequenceNo;

	/**
	 * Default constructor
	 */
	public DsReportUsageCtx_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DsReportUsageCtx_Ds(DsReportUsage e) {
		super(e);
	}

	public String getDsReportId() {
		return this.dsReportId;
	}

	public void setDsReportId(String dsReportId) {
		this.dsReportId = dsReportId;
	}

	public String getReportId() {
		return this.reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getReport() {
		return this.report;
	}

	public void setReport(String report) {
		this.report = report;
	}

	public String getFrameName() {
		return this.frameName;
	}

	public void setFrameName(String frameName) {
		this.frameName = frameName;
	}

	public String getToolbarKey() {
		return this.toolbarKey;
	}

	public void setToolbarKey(String toolbarKey) {
		this.toolbarKey = toolbarKey;
	}

	public String getDcKey() {
		return this.dcKey;
	}

	public void setDcKey(String dcKey) {
		this.dcKey = dcKey;
	}

	public Integer getSequenceNo() {
		return this.sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
}
