/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.qb;

import atraxo.ad.presenter.impl.system.model.ParamValue_Ds;
import atraxo.ad.presenter.impl.system.model.ParamValue_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class ParamValue_DsQb
		extends
			QueryBuilderWithJpql<ParamValue_Ds, ParamValue_Ds, ParamValue_DsParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getValidAt() != null
				&& !"".equals(this.params.getValidAt())) {
			addFilterCondition("  :validAt between e.validFrom and e.validTo ");
			addCustomFilterItem("validAt", this.params.getValidAt());
		}
	}
}
