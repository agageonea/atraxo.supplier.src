/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractTypeNT_Ds;
import atraxo.ad.domain.impl.system.DateFormat;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DateFormat.class, sort = {@SortField(field = DateFormat_Ds.F_NAME)})
public class DateFormat_Ds extends AbstractTypeNT_Ds<DateFormat> {

	public static final String ALIAS = "ad_DateFormat_Ds";

	/**
	 * Default constructor
	 */
	public DateFormat_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DateFormat_Ds(DateFormat e) {
		super(e);
	}
}
