/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

/**
 * Generated code. Do not modify in this file.
 */
public class User_DsParam {

	public static final String f_newPassword = "newPassword";
	public static final String f_confirmPassword = "confirmPassword";
	public static final String f_withRole = "withRole";
	public static final String f_withRoleId = "withRoleId";
	public static final String f_inGroup = "inGroup";
	public static final String f_inGroupId = "inGroupId";

	private String newPassword;

	private String confirmPassword;

	private String withRole;

	private String withRoleId;

	private String inGroup;

	private String inGroupId;

	public String getNewPassword() {
		return this.newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return this.confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getWithRole() {
		return this.withRole;
	}

	public void setWithRole(String withRole) {
		this.withRole = withRole;
	}

	public String getWithRoleId() {
		return this.withRoleId;
	}

	public void setWithRoleId(String withRoleId) {
		this.withRoleId = withRoleId;
	}

	public String getInGroup() {
		return this.inGroup;
	}

	public void setInGroup(String inGroup) {
		this.inGroup = inGroup;
	}

	public String getInGroupId() {
		return this.inGroupId;
	}

	public void setInGroupId(String inGroupId) {
		this.inGroupId = inGroupId;
	}
}
