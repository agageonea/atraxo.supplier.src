/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeWithCode_Ds;
import atraxo.ad.domain.impl.ad.TNumberSeparator;
import atraxo.ad.domain.impl.security.User;
import atraxo.ad.domain.impl.system.DateFormat;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = User.class, sort = {@SortField(field = User_Ds.F_NAME)})
@RefLookups({@RefLookup(refId = User_Ds.F_DATEFORMATID, namedQuery = DateFormat.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = User_Ds.F_DATEFORMAT)})})
public class User_Ds extends AbstractTypeWithCode_Ds<User> {

	public static final String ALIAS = "ad_User_Ds";

	public static final String F_LOGINNAME = "loginName";
	public static final String F_CODE = "code";
	public static final String F_LOCKED = "locked";
	public static final String F_EMAIL = "email";
	public static final String F_DECIMALSEPARATOR = "decimalSeparator";
	public static final String F_THOUSANDSEPARATOR = "thousandSeparator";
	public static final String F_DATEFORMATID = "dateFormatId";
	public static final String F_DATEFORMAT = "dateFormat";

	@DsField
	private String loginName;

	@DsField(noUpdate = true)
	private String code;

	@DsField
	private Boolean locked;

	@DsField
	private String email;

	@DsField
	private TNumberSeparator decimalSeparator;

	@DsField
	private TNumberSeparator thousandSeparator;

	@DsField(join = "left", path = "dateFormat.id")
	private String dateFormatId;

	@DsField(join = "left", path = "dateFormat.name")
	private String dateFormat;

	/**
	 * Default constructor
	 */
	public User_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public User_Ds(User e) {
		super(e);
	}

	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getLocked() {
		return this.locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public TNumberSeparator getDecimalSeparator() {
		return this.decimalSeparator;
	}

	public void setDecimalSeparator(TNumberSeparator decimalSeparator) {
		this.decimalSeparator = decimalSeparator;
	}

	public TNumberSeparator getThousandSeparator() {
		return this.thousandSeparator;
	}

	public void setThousandSeparator(TNumberSeparator thousandSeparator) {
		this.thousandSeparator = thousandSeparator;
	}

	public String getDateFormatId() {
		return this.dateFormatId;
	}

	public void setDateFormatId(String dateFormatId) {
		this.dateFormatId = dateFormatId;
	}

	public String getDateFormat() {
		return this.dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
}
