/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.ad.domain.impl.system.TargetRule;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = TargetRule.class)
@RefLookups({@RefLookup(refId = TargetRule_Ds.F_SOURCEREFID, namedQuery = AttachmentType.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = TargetRule_Ds.F_SOURCEREFNAME)})})
public class TargetRule_Ds extends AbstractAuditable_Ds<TargetRule> {

	public static final String ALIAS = "ad_TargetRule_Ds";

	public static final String F_SOURCEREFID = "sourceRefId";
	public static final String F_SOURCEREFNAME = "sourceRefName";
	public static final String F_TARGETALIAS = "targetAlias";
	public static final String F_TARGETTYPE = "targetType";

	@DsField(join = "left", path = "sourceRefId.id")
	private String sourceRefId;

	@DsField(join = "left", path = "sourceRefId.name")
	private String sourceRefName;

	@DsField
	private String targetAlias;

	@DsField
	private String targetType;

	/**
	 * Default constructor
	 */
	public TargetRule_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public TargetRule_Ds(TargetRule e) {
		super(e);
	}

	public String getSourceRefId() {
		return this.sourceRefId;
	}

	public void setSourceRefId(String sourceRefId) {
		this.sourceRefId = sourceRefId;
	}

	public String getSourceRefName() {
		return this.sourceRefName;
	}

	public void setSourceRefName(String sourceRefName) {
		this.sourceRefName = sourceRefName;
	}

	public String getTargetAlias() {
		return this.targetAlias;
	}

	public void setTargetAlias(String targetAlias) {
		this.targetAlias = targetAlias;
	}

	public String getTargetType() {
		return this.targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}
}
