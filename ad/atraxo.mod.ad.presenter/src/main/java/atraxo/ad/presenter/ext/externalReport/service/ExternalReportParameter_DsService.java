/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.ext.externalReport.service;

import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;
import atraxo.ad.domain.impl.externalReport.ExternalReportParameter;
import atraxo.ad.presenter.impl.externalReport.model.ExternalReportParameter_Ds;

/**
 * Custom Service ExternalReportParameter_DsService
 */
public class ExternalReportParameter_DsService extends
AbstractEntityDsService<ExternalReportParameter_Ds, ExternalReportParameter_Ds, Object, ExternalReportParameter> implements
IDsService<ExternalReportParameter_Ds, ExternalReportParameter_Ds, Object> {

	// @Override
	// protected void postFind(IQueryBuilder<ExternalReportParameter_Ds, ExternalReportParameter_Ds, Object> builder,
	// List<ExternalReportParameter_Ds> result) throws Exception {
	// super.postFind(builder, result);
	//
	// for (ExternalReportParameter_Ds ds : result) {
	// ds.setDsName(ds.getDsName());
	// }
	// }

}
