/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.report.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.report.DsReportUsage;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DsReportUsage.class, jpqlWhere = " e.dsReport.report.active = true ", sort = {
		@SortField(field = DsReportUsageRt_Ds.F_SEQUENCENO),
		@SortField(field = DsReportUsageRt_Ds.F_REPORTTITLE)})
public class DsReportUsageRt_Ds extends AbstractAuditable_Ds<DsReportUsage> {

	public static final String ALIAS = "ad_DsReportUsageRt_Ds";

	public static final String F_DSREPORTID = "dsReportId";
	public static final String F_REPORTID = "reportId";
	public static final String F_REPORTCODE = "reportCode";
	public static final String F_REPORTTITLE = "reportTitle";
	public static final String F_REPORTCONTEXTPATH = "reportContextPath";
	public static final String F_SERVERURL = "serverUrl";
	public static final String F_QUERYBUILDERCLASS = "queryBuilderClass";
	public static final String F_FRAMENAME = "frameName";
	public static final String F_TOOLBARKEY = "toolbarKey";
	public static final String F_DCKEY = "dcKey";
	public static final String F_SEQUENCENO = "sequenceNo";

	@DsField(join = "left", path = "dsReport.id")
	private String dsReportId;

	@DsField(join = "left", path = "dsReport.report.id")
	private String reportId;

	@DsField(join = "left", path = "dsReport.report.code")
	private String reportCode;

	@DsField(join = "left", path = "dsReport.report.name")
	private String reportTitle;

	@DsField(join = "left", path = "dsReport.report.contextPath")
	private String reportContextPath;

	@DsField(join = "left", path = "dsReport.report.reportServer.url")
	private String serverUrl;

	@DsField(join = "left", path = "dsReport.report.reportServer.queryBuilderClass")
	private String queryBuilderClass;

	@DsField
	private String frameName;

	@DsField
	private String toolbarKey;

	@DsField
	private String dcKey;

	@DsField
	private Integer sequenceNo;

	/**
	 * Default constructor
	 */
	public DsReportUsageRt_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DsReportUsageRt_Ds(DsReportUsage e) {
		super(e);
	}

	public String getDsReportId() {
		return this.dsReportId;
	}

	public void setDsReportId(String dsReportId) {
		this.dsReportId = dsReportId;
	}

	public String getReportId() {
		return this.reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getReportCode() {
		return this.reportCode;
	}

	public void setReportCode(String reportCode) {
		this.reportCode = reportCode;
	}

	public String getReportTitle() {
		return this.reportTitle;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}

	public String getReportContextPath() {
		return this.reportContextPath;
	}

	public void setReportContextPath(String reportContextPath) {
		this.reportContextPath = reportContextPath;
	}

	public String getServerUrl() {
		return this.serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	public String getQueryBuilderClass() {
		return this.queryBuilderClass;
	}

	public void setQueryBuilderClass(String queryBuilderClass) {
		this.queryBuilderClass = queryBuilderClass;
	}

	public String getFrameName() {
		return this.frameName;
	}

	public void setFrameName(String frameName) {
		this.frameName = frameName;
	}

	public String getToolbarKey() {
		return this.toolbarKey;
	}

	public void setToolbarKey(String toolbarKey) {
		this.toolbarKey = toolbarKey;
	}

	public String getDcKey() {
		return this.dcKey;
	}

	public void setDcKey(String dcKey) {
		this.dcKey = dcKey;
	}

	public Integer getSequenceNo() {
		return this.sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
}
