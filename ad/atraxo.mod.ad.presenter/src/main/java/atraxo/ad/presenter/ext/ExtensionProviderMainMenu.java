package atraxo.ad.presenter.ext;

import java.util.List;

import seava.j4e.api.extensions.IExtensionContentProvider;
import seava.j4e.api.extensions.IExtensions;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterBaseService;
import atraxo.ad.domain.impl.ad.LanguageCodes;
import atraxo.ad.domain.impl.ad.TMenuTag;
import atraxo.ad.presenter.impl.security.model.MenuRtLov_Ds;

/**
 * @author dtoma
 */
public class ExtensionProviderMainMenu extends AbstractPresenterBaseService implements IExtensionContentProvider {

	@Override
	public synchronized String getContent(String targetName) throws Exception {
		if (IExtensions.UI_EXTJS_MAIN.equals(targetName)) {
			StringBuilder sb = new StringBuilder();
			if (Session.user.get().isSystemUser()) {
				this.addSystemMenus(sb);
			} else {
				this.addNavigationTreeMenus(sb);
				this.addNavigationTopMenus(sb);
			}
			// inject supported languages code
			this.addSupportedLanguages(sb);

			return sb.toString();
		}
		return null;
	}

	protected void addSupportedLanguages(StringBuilder sb) {
		sb.append("supLangCodes=[");
		boolean addComma = false;
		for (LanguageCodes lc : LanguageCodes.values()) {
			if (!"".equals(lc.getCode())) {
				if (addComma) {
					sb.append(",");
				} else {
					addComma = true;
				}
				sb.append("{_langId_:'" + lc.getCode() + "', text:'" + lc.getName() + "'}");
			}
		}
		sb.append("];");
	}

	protected void addSystemMenus(StringBuilder sb) {

		sb.append("Main.systemMenus = [");
		sb.append("{\"bundle\":\"atraxo.mod.ad\", \"frame\":\"atraxo.ad.ui.extjs.frame.Client_Ui\", \"labelKey\":\"appmenuitem/clientmgmt__lbl\" },");
		sb.append("{\"bundle\":\"atraxo.mod.ad\", \"frame\":\"atraxo.ad.ui.extjs.frame.DataSource_Ui\", \"labelKey\":\"appmenuitem/sysds__lbl\" },");
		sb.append("{\"bundle\":\"atraxo.mod.ad\", \"frame\":\"atraxo.ad.ui.extjs.frame.Job_Ui\", \"labelKey\":\"appmenuitem/sysjob__lbl\" },");
		sb.append("{\"bundle\":\"atraxo.mod.ad\", \"frame\":\"atraxo.ad.ui.extjs.frame.Param_Ui\", \"labelKey\":\"appmenuitem/sysparam__lbl\" },");
		sb.append("{\"bundle\":\"atraxo.mod.ad\", \"frame\":\"atraxo.ad.ui.extjs.frame.DateFormat_Ui\", \"labelKey\":\"appmenuitem/dateformat__lbl\" },");
		sb.append("{\"bundle\":\"atraxo.mod.ad\", \"frame\":\"atraxo.ad.ui.extjs.frame.DateFormatMask_Ui\", \"labelKey\":\"appmenuitem/dateformatmask__lbl\" },");
		sb.append("{\"bundle\":\"atraxo.mod.ad\", \"frame\":\"atraxo.ad.ui.extjs.frame.DbChangeLog_Ui\", \"labelKey\":\"appmenuitem/dbchangelog__lbl\" }");
		sb.append("];");
	}

	protected void addNavigationTreeMenus(StringBuilder sb) throws Exception {
		IDsService<MenuRtLov_Ds, MenuRtLov_Ds, Object> srv = this.findDsService(MenuRtLov_Ds.class);
		MenuRtLov_Ds filter = new MenuRtLov_Ds();
		filter.setActive(true);
		filter.setTag(TMenuTag._LEFT_);
		List<MenuRtLov_Ds> menus = srv.find(filter);
		int i = 0;

		String glyph;
		String description;

		sb.append("Main.navigationTreeMenus = [");
		for (MenuRtLov_Ds menu : menus) {
			if (i > 0) {
				sb.append(",");
			}
			glyph = menu.getGlyph();
			description = menu.getDescription();

			sb.append("{name:\"" + menu.getName() + "\", title:\"" + menu.getTitle() + "\"");
			if (glyph != null) {
				sb.append(", glyph:\"" + menu.getGlyph() + "\"");
			}
			if (description != null) {
				sb.append(", description:\"" + menu.getDescription() + "\"");
			}
			sb.append("}");
			i++;
		}
		sb.append("];");
	}

	protected void addNavigationTopMenus(StringBuilder sb) throws Exception {
		IDsService<MenuRtLov_Ds, MenuRtLov_Ds, Object> srv = this.findDsService(MenuRtLov_Ds.class);
		MenuRtLov_Ds filter = new MenuRtLov_Ds();
		filter.setActive(true);
		filter.setTag(TMenuTag._TOP_);
		List<MenuRtLov_Ds> menus = srv.find(filter);
		int i = 0;

		String glyph;
		String description;

		sb.append("Main.navigationTopMenus = [");
		for (MenuRtLov_Ds menu : menus) {
			if (i > 0) {
				sb.append(",");
			}
			glyph = menu.getGlyph();
			description = menu.getDescription();

			sb.append("{name:\"" + menu.getName() + "\", title:\"" + menu.getTitle() + "\"");
			if (glyph != null) {
				sb.append(", glyph:\"" + menu.getGlyph() + "\"");
			}
			if (description != null) {
				sb.append(", description:\"" + menu.getDescription() + "\"");
			}
			sb.append("}");
			i++;
		}
		sb.append("];");
	}

}
