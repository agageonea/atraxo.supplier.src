/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.externalReport.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.ad.ReportParamType;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.ExternalReportParameter;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ExternalReportParameter.class)
@RefLookups({@RefLookup(refId = ExternalReportParameter_Ds.F_EXTERNALREPORTID, namedQuery = ExternalReport.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = ExternalReportParameter_Ds.F_EXTERNALREPORTNAME)})})
public class ExternalReportParameter_Ds
		extends
			AbstractAuditable_Ds<ExternalReportParameter> {

	public static final String ALIAS = "ad_ExternalReportParameter_Ds";

	public static final String F_EXTERNALREPORTID = "externalReportId";
	public static final String F_EXTERNALREPORTNAME = "externalReportName";
	public static final String F_BUSINESSAREA = "businessArea";
	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_PARAMVALUE = "paramValue";
	public static final String F_DEFAULTVALUE = "defaultValue";
	public static final String F_TYPE = "type";
	public static final String F_MANDATORY = "mandatory";
	public static final String F_DSFIELDNAME = "dsFieldName";
	public static final String F_DSNAME = "dsName";
	public static final String F_DSMODEL = "dsModel";
	public static final String F_DSALIAS = "dsAlias";

	@DsField(join = "left", path = "externalReport.id")
	private String externalReportId;

	@DsField(join = "left", path = "externalReport.name")
	private String externalReportName;

	@DsField(join = "left", path = "externalReport.businessArea.name")
	private String businessArea;

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private String paramValue;

	@DsField
	private String defaultValue;

	@DsField
	private ReportParamType type;

	@DsField
	private Boolean mandatory;

	@DsField
	private String dsFieldName;

	@DsField(join = "left", path = "externalReport.businessArea.dsName")
	private String dsName;

	@DsField(join = "left", path = "externalReport.businessArea.dsModel")
	private String dsModel;

	@DsField(join = "left", path = "externalReport.businessArea.dsAlias")
	private String dsAlias;

	/**
	 * Default constructor
	 */
	public ExternalReportParameter_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ExternalReportParameter_Ds(ExternalReportParameter e) {
		super(e);
	}

	public String getExternalReportId() {
		return this.externalReportId;
	}

	public void setExternalReportId(String externalReportId) {
		this.externalReportId = externalReportId;
	}

	public String getExternalReportName() {
		return this.externalReportName;
	}

	public void setExternalReportName(String externalReportName) {
		this.externalReportName = externalReportName;
	}

	public String getBusinessArea() {
		return this.businessArea;
	}

	public void setBusinessArea(String businessArea) {
		this.businessArea = businessArea;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getParamValue() {
		return this.paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public ReportParamType getType() {
		return this.type;
	}

	public void setType(ReportParamType type) {
		this.type = type;
	}

	public Boolean getMandatory() {
		return this.mandatory;
	}

	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}

	public String getDsFieldName() {
		return this.dsFieldName;
	}

	public void setDsFieldName(String dsFieldName) {
		this.dsFieldName = dsFieldName;
	}

	public String getDsName() {
		return this.dsName;
	}

	public void setDsName(String dsName) {
		this.dsName = dsName;
	}

	public String getDsModel() {
		return this.dsModel;
	}

	public void setDsModel(String dsModel) {
		this.dsModel = dsModel;
	}

	public String getDsAlias() {
		return this.dsAlias;
	}

	public void setDsAlias(String dsAlias) {
		this.dsAlias = dsAlias;
	}
}
