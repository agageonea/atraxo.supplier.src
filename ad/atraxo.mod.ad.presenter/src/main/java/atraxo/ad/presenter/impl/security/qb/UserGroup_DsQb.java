/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.qb;

import atraxo.ad.presenter.impl.security.model.UserGroup_Ds;
import atraxo.ad.presenter.impl.security.model.UserGroup_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class UserGroup_DsQb
		extends
			QueryBuilderWithJpql<UserGroup_Ds, UserGroup_Ds, UserGroup_DsParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getWithUserId() != null
				&& !"".equals(this.params.getWithUserId())) {
			addFilterCondition("  e.id in ( select p.id from UserGroup p, IN (p.users) c where c.id = :withUserId )  ");
			addCustomFilterItem("withUserId", this.params.getWithUserId());
		}
	}
}
