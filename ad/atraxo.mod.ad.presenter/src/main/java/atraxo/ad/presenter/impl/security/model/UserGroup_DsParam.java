/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

/**
 * Generated code. Do not modify in this file.
 */
public class UserGroup_DsParam {

	public static final String f_withUser = "withUser";
	public static final String f_withUserId = "withUserId";

	private String withUser;

	private String withUserId;

	public String getWithUser() {
		return this.withUser;
	}

	public void setWithUser(String withUser) {
		this.withUser = withUser;
	}

	public String getWithUserId() {
		return this.withUserId;
	}

	public void setWithUserId(String withUserId) {
		this.withUserId = withUserId;
	}
}
