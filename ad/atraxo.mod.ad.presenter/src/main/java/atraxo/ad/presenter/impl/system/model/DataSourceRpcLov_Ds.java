/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractTypeNTLov_Ds;
import atraxo.ad.domain.impl.system.DataSourceRpc;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DataSourceRpc.class, sort = {@SortField(field = DataSourceRpcLov_Ds.F_NAME)})
public class DataSourceRpcLov_Ds extends AbstractTypeNTLov_Ds<DataSourceRpc> {

	public static final String ALIAS = "ad_DataSourceRpcLov_Ds";

	public static final String F_DATASOURCEID = "dataSourceId";
	public static final String F_DATASOURCENAME = "dataSourceName";

	@DsField(join = "left", path = "dataSource.id")
	private String dataSourceId;

	@DsField(join = "left", path = "dataSource.name")
	private String dataSourceName;

	/**
	 * Default constructor
	 */
	public DataSourceRpcLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DataSourceRpcLov_Ds(DataSourceRpc e) {
		super(e);
	}

	public String getDataSourceId() {
		return this.dataSourceId;
	}

	public void setDataSourceId(String dataSourceId) {
		this.dataSourceId = dataSourceId;
	}

	public String getDataSourceName() {
		return this.dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}
}
