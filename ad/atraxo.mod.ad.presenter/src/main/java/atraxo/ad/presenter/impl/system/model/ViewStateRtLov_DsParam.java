/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

/**
 * Generated code. Do not modify in this file.
 */
public class ViewStateRtLov_DsParam {

	public static final String f_hideMine = "hideMine";
	public static final String f_hideOthers = "hideOthers";

	private Boolean hideMine;

	private Boolean hideOthers;

	public Boolean getHideMine() {
		return this.hideMine;
	}

	public void setHideMine(Boolean hideMine) {
		this.hideMine = hideMine;
	}

	public Boolean getHideOthers() {
		return this.hideOthers;
	}

	public void setHideOthers(Boolean hideOthers) {
		this.hideOthers = hideOthers;
	}
}
