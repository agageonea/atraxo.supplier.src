/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.scheduler.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.scheduler.JobContext;
import atraxo.ad.domain.impl.scheduler.JobContextParam;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = JobContextParam.class)
@RefLookups({@RefLookup(refId = JobContextParam_Ds.F_JOBCONTEXTID, namedQuery = JobContext.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = JobContextParam_Ds.F_JOBCONTEXT)})})
public class JobContextParam_Ds extends AbstractAuditable_Ds<JobContextParam> {

	public static final String ALIAS = "ad_JobContextParam_Ds";

	public static final String F_PARAMNAME = "paramName";
	public static final String F_DATATYPE = "dataType";
	public static final String F_VALUE = "value";
	public static final String F_JOBCONTEXTID = "jobContextId";
	public static final String F_JOBCONTEXT = "jobContext";
	public static final String F_JOBNAME = "jobName";

	@DsField
	private String paramName;

	@DsField
	private String dataType;

	@DsField
	private String value;

	@DsField(join = "left", path = "jobContext.id")
	private String jobContextId;

	@DsField(join = "left", path = "jobContext.name")
	private String jobContext;

	@DsField(join = "left", path = "jobContext.jobName")
	private String jobName;

	/**
	 * Default constructor
	 */
	public JobContextParam_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public JobContextParam_Ds(JobContextParam e) {
		super(e);
	}

	public String getParamName() {
		return this.paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getDataType() {
		return this.dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getJobContextId() {
		return this.jobContextId;
	}

	public void setJobContextId(String jobContextId) {
		this.jobContextId = jobContextId;
	}

	public String getJobContext() {
		return this.jobContext;
	}

	public void setJobContext(String jobContext) {
		this.jobContext = jobContext;
	}

	public String getJobName() {
		return this.jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
}
