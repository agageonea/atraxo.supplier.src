/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.userFields.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.ad.UserFieldType;
import atraxo.ad.domain.impl.businessArea.BusinessArea;
import atraxo.ad.domain.impl.userFields.UserFieldValues;
import atraxo.ad.domain.impl.userFields.UserFields;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = UserFieldValues.class)
@RefLookups({
		@RefLookup(refId = UserFieldValues_Ds.F_BUSINESSAREAID, namedQuery = BusinessArea.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = UserFieldValues_Ds.F_BUSINESSAREA)}),
		@RefLookup(refId = UserFieldValues_Ds.F_USERFIELDID, namedQuery = UserFields.NQ_FIND_BY_NAME_PRIMITIVE, params = {@Param(name = "name", field = UserFieldValues_Ds.F_USERFIELDNAME)})})
public class UserFieldValues_Ds extends AbstractAuditable_Ds<UserFieldValues> {

	public static final String ALIAS = "ad_UserFieldValues_Ds";

	public static final String F_VALUE = "value";
	public static final String F_TARGETREFID = "targetRefid";
	public static final String F_TARGETALIAS = "targetAlias";
	public static final String F_USERFIELDID = "userFieldId";
	public static final String F_USERFIELDNAME = "userFieldName";
	public static final String F_USERFIELDDESCRIPTION = "userFieldDescription";
	public static final String F_USERFIELDDEFAULTVALUE = "userFieldDefaultValue";
	public static final String F_USERFIELDTYPE = "userFieldType";
	public static final String F_BUSINESSAREAID = "businessAreaId";
	public static final String F_BUSINESSAREA = "businessArea";
	public static final String F_DSMODEL = "dsModel";
	public static final String F_DSNAME = "dsName";
	public static final String F_DSALIAS = "dsAlias";

	@DsField
	private String value;

	@DsField
	private Integer targetRefid;

	@DsField
	private String targetAlias;

	@DsField(join = "left", path = "userField.id")
	private String userFieldId;

	@DsField(join = "left", path = "userField.name")
	private String userFieldName;

	@DsField(join = "left", path = "userField.description")
	private String userFieldDescription;

	@DsField(join = "left", path = "userField.defaultValue")
	private String userFieldDefaultValue;

	@DsField(join = "left", path = "userField.type")
	private UserFieldType userFieldType;

	@DsField(join = "left", path = "userField.businessArea.id")
	private String businessAreaId;

	@DsField(join = "left", path = "userField.businessArea.name")
	private String businessArea;

	@DsField(join = "left", path = "userField.businessArea.dsModel")
	private String dsModel;

	@DsField(join = "left", path = "userField.businessArea.dsName")
	private String dsName;

	@DsField(join = "left", path = "userField.businessArea.dsAlias")
	private String dsAlias;

	/**
	 * Default constructor
	 */
	public UserFieldValues_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public UserFieldValues_Ds(UserFieldValues e) {
		super(e);
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getTargetRefid() {
		return this.targetRefid;
	}

	public void setTargetRefid(Integer targetRefid) {
		this.targetRefid = targetRefid;
	}

	public String getTargetAlias() {
		return this.targetAlias;
	}

	public void setTargetAlias(String targetAlias) {
		this.targetAlias = targetAlias;
	}

	public String getUserFieldId() {
		return this.userFieldId;
	}

	public void setUserFieldId(String userFieldId) {
		this.userFieldId = userFieldId;
	}

	public String getUserFieldName() {
		return this.userFieldName;
	}

	public void setUserFieldName(String userFieldName) {
		this.userFieldName = userFieldName;
	}

	public String getUserFieldDescription() {
		return this.userFieldDescription;
	}

	public void setUserFieldDescription(String userFieldDescription) {
		this.userFieldDescription = userFieldDescription;
	}

	public String getUserFieldDefaultValue() {
		return this.userFieldDefaultValue;
	}

	public void setUserFieldDefaultValue(String userFieldDefaultValue) {
		this.userFieldDefaultValue = userFieldDefaultValue;
	}

	public UserFieldType getUserFieldType() {
		return this.userFieldType;
	}

	public void setUserFieldType(UserFieldType userFieldType) {
		this.userFieldType = userFieldType;
	}

	public String getBusinessAreaId() {
		return this.businessAreaId;
	}

	public void setBusinessAreaId(String businessAreaId) {
		this.businessAreaId = businessAreaId;
	}

	public String getBusinessArea() {
		return this.businessArea;
	}

	public void setBusinessArea(String businessArea) {
		this.businessArea = businessArea;
	}

	public String getDsModel() {
		return this.dsModel;
	}

	public void setDsModel(String dsModel) {
		this.dsModel = dsModel;
	}

	public String getDsName() {
		return this.dsName;
	}

	public void setDsName(String dsName) {
		this.dsName = dsName;
	}

	public String getDsAlias() {
		return this.dsAlias;
	}

	public void setDsAlias(String dsAlias) {
		this.dsAlias = dsAlias;
	}
}
