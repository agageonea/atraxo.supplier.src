/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.qb;

import atraxo.ad.presenter.impl.security.model.MyUser_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class MyUser_DsQb
		extends
			QueryBuilderWithJpql<MyUser_Ds, MyUser_Ds, Object> {

	@Override
	public void setFilter(MyUser_Ds filter) {
		filter.setCode(Session.user.get().getCode());
		super.setFilter(filter);
	}
}
