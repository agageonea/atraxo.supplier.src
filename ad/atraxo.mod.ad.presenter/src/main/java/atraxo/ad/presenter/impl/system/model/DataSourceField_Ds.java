/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractTypeNT_Ds;
import atraxo.ad.domain.impl.system.DataSourceField;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DataSourceField.class, sort = {@SortField(field = DataSourceField_Ds.F_NAME)})
public class DataSourceField_Ds extends AbstractTypeNT_Ds<DataSourceField> {

	public static final String ALIAS = "ad_DataSourceField_Ds";

	public static final String F_DATATYPE = "dataType";
	public static final String F_DATASOURCEID = "dataSourceId";
	public static final String F_DATASOURCE = "dataSource";

	@DsField
	private String dataType;

	@DsField(join = "left", path = "dataSource.id")
	private String dataSourceId;

	@DsField(join = "left", path = "dataSource.name")
	private String dataSource;

	/**
	 * Default constructor
	 */
	public DataSourceField_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DataSourceField_Ds(DataSourceField e) {
		super(e);
	}

	public String getDataType() {
		return this.dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getDataSourceId() {
		return this.dataSourceId;
	}

	public void setDataSourceId(String dataSourceId) {
		this.dataSourceId = dataSourceId;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
}
