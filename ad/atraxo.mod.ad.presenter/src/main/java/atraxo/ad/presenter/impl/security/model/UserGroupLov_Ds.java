/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeWithCodeLov_Ds;
import atraxo.ad.domain.impl.security.UserGroup;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = UserGroup.class, sort = {@SortField(field = UserGroupLov_Ds.F_CODE)})
public class UserGroupLov_Ds extends AbstractTypeWithCodeLov_Ds<UserGroup> {

	public static final String ALIAS = "ad_UserGroupLov_Ds";

	/**
	 * Default constructor
	 */
	public UserGroupLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public UserGroupLov_Ds(UserGroup e) {
		super(e);
	}
}
