/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractAuditableNT_Ds;
import atraxo.ad.domain.impl.system.DateFormat;
import atraxo.ad.domain.impl.system.DateFormatMask;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DateFormatMask.class, sort = {
		@SortField(field = DateFormatMask_Ds.F_DATEFORMAT),
		@SortField(field = DateFormatMask_Ds.F_MASK)})
@RefLookups({@RefLookup(refId = DateFormatMask_Ds.F_DATEFORMATID, namedQuery = DateFormat.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = DateFormatMask_Ds.F_DATEFORMAT)})})
public class DateFormatMask_Ds extends AbstractAuditableNT_Ds<DateFormatMask> {

	public static final String ALIAS = "ad_DateFormatMask_Ds";

	public static final String F_MASK = "mask";
	public static final String F_VALUE = "value";
	public static final String F_DATEFORMATID = "dateFormatId";
	public static final String F_DATEFORMAT = "dateFormat";

	@DsField
	private String mask;

	@DsField
	private String value;

	@DsField(join = "left", path = "dateFormat.id")
	private String dateFormatId;

	@DsField(join = "left", path = "dateFormat.name")
	private String dateFormat;

	/**
	 * Default constructor
	 */
	public DateFormatMask_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DateFormatMask_Ds(DateFormatMask e) {
		super(e);
	}

	public String getMask() {
		return this.mask;
	}

	public void setMask(String mask) {
		this.mask = mask;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDateFormatId() {
		return this.dateFormatId;
	}

	public void setDateFormatId(String dateFormatId) {
		this.dateFormatId = dateFormatId;
	}

	public String getDateFormat() {
		return this.dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
}
