/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.externalReport.model;

import atraxo.ad.domain.impl.externalReport.ExternalReport;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.model.IModelWithClientId;
import seava.j4e.api.model.IModelWithId;
import seava.j4e.presenter.model.AbstractDsModel;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ExternalReport.class, jpqlWhere = "e.reportCode != ''")
public class ExternalReportFWLov_Ds extends AbstractDsModel<ExternalReport>
		implements
			IModelWithId<String>,
			IModelWithClientId {

	public static final String ALIAS = "ad_ExternalReportFWLov_Ds";

	public static final String F_ID = "id";
	public static final String F_CLIENTID = "clientId";
	public static final String F_ENTITYALIAS = "entityAlias";
	public static final String F_ENTITYFQN = "entityFqn";
	public static final String F_NAME = "name";
	public static final String F_REPORTCODE = "reportCode";
	public static final String F_DESCRIPTION = "description";
	public static final String F_SYSTEM = "system";
	public static final String F_BUSINESSAREAID = "businessAreaId";
	public static final String F_BUSINESSAREA = "businessArea";
	public static final String F_DESIGNNAME = "designName";
	public static final String F_DSNAME = "dsName";

	@DsField
	private String id;

	@DsField
	private String clientId;

	@DsField(fetch = false)
	private String entityAlias;

	@DsField(fetch = false)
	private String entityFqn;

	@DsField
	private String name;

	@DsField
	private String reportCode;

	@DsField
	private String description;

	@DsField
	private Boolean system;

	@DsField(join = "left", path = "businessArea.id")
	private String businessAreaId;

	@DsField(join = "left", path = "businessArea.name")
	private String businessArea;

	@DsField
	private String designName;

	@DsField(join = "left", path = "businessArea.dsName")
	private String dsName;

	/**
	 * Default constructor
	 */
	public ExternalReportFWLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ExternalReportFWLov_Ds(ExternalReport e) {
		super(e);
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getEntityAlias() {
		return this.entityAlias;
	}

	public void setEntityAlias(String entityAlias) {
		this.entityAlias = entityAlias;
	}

	public String getEntityFqn() {
		return this.entityFqn;
	}

	public void setEntityFqn(String entityFqn) {
		this.entityFqn = entityFqn;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReportCode() {
		return this.reportCode;
	}

	public void setReportCode(String reportCode) {
		this.reportCode = reportCode;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getSystem() {
		return this.system;
	}

	public void setSystem(Boolean system) {
		this.system = system;
	}

	public String getBusinessAreaId() {
		return this.businessAreaId;
	}

	public void setBusinessAreaId(String businessAreaId) {
		this.businessAreaId = businessAreaId;
	}

	public String getBusinessArea() {
		return this.businessArea;
	}

	public void setBusinessArea(String businessArea) {
		this.businessArea = businessArea;
	}

	public String getDesignName() {
		return this.designName;
	}

	public void setDesignName(String designName) {
		this.designName = designName;
	}

	public String getDsName() {
		return this.dsName;
	}

	public void setDsName(String dsName) {
		this.dsName = dsName;
	}
}
