/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeWithCode_Ds;
import atraxo.ad.domain.impl.security.UserGroup;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = UserGroup.class, sort = {@SortField(field = UserGroup_Ds.F_CODE)})
public class UserGroup_Ds extends AbstractTypeWithCode_Ds<UserGroup> {

	public static final String ALIAS = "ad_UserGroup_Ds";

	/**
	 * Default constructor
	 */
	public UserGroup_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public UserGroup_Ds(UserGroup e) {
		super(e);
	}
}
