/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.externalReport.model;

import atraxo.ad.domain.impl.ad.ReportFormat;
/**
 * Generated code. Do not modify in this file.
 */
public class ExternalReport_DsParam {

	public static final String f_format = "format";

	private ReportFormat format;

	public ReportFormat getFormat() {
		return this.format;
	}

	public void setFormat(ReportFormat format) {
		this.format = format;
	}
}
