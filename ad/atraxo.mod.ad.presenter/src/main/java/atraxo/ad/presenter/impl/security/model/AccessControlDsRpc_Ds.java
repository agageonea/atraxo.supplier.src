/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.security.AccessControl;
import atraxo.ad.domain.impl.security.AccessControlDsRpc;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = AccessControlDsRpc.class, sort = {
		@SortField(field = AccessControlDsRpc_Ds.F_DSNAME),
		@SortField(field = AccessControlDsRpc_Ds.F_SERVICEMETHOD)})
@RefLookups({@RefLookup(refId = AccessControlDsRpc_Ds.F_ACCESSCONTROLID, namedQuery = AccessControl.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = AccessControlDsRpc_Ds.F_ACCESSCONTROL)})})
public class AccessControlDsRpc_Ds
		extends
			AbstractAuditable_Ds<AccessControlDsRpc> {

	public static final String ALIAS = "ad_AccessControlDsRpc_Ds";

	public static final String F_DSNAME = "dsName";
	public static final String F_SERVICEMETHOD = "serviceMethod";
	public static final String F_ACCESSCONTROLID = "accessControlId";
	public static final String F_ACCESSCONTROL = "accessControl";

	@DsField
	private String dsName;

	@DsField
	private String serviceMethod;

	@DsField(join = "left", path = "accessControl.id")
	private String accessControlId;

	@DsField(join = "left", path = "accessControl.name")
	private String accessControl;

	/**
	 * Default constructor
	 */
	public AccessControlDsRpc_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AccessControlDsRpc_Ds(AccessControlDsRpc e) {
		super(e);
	}

	public String getDsName() {
		return this.dsName;
	}

	public void setDsName(String dsName) {
		this.dsName = dsName;
	}

	public String getServiceMethod() {
		return this.serviceMethod;
	}

	public void setServiceMethod(String serviceMethod) {
		this.serviceMethod = serviceMethod;
	}

	public String getAccessControlId() {
		return this.accessControlId;
	}

	public void setAccessControlId(String accessControlId) {
		this.accessControlId = accessControlId;
	}

	public String getAccessControl() {
		return this.accessControl;
	}

	public void setAccessControl(String accessControl) {
		this.accessControl = accessControl;
	}
}
