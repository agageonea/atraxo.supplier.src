package atraxo.ad.presenter.ext.scheduler.delegate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.ad.business.api.scheduler.IJobContextService;
import atraxo.ad.domain.impl.scheduler.JobContext;
import atraxo.ad.domain.impl.scheduler.JobContextParam;
import atraxo.ad.domain.impl.system.JobParam;
import atraxo.ad.presenter.impl.scheduler.model.JobContext_Ds;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class JobContext_Pd extends AbstractPresenterDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(JobContext_Pd.class);

	public void synchronizeParameters(List<JobContext_Ds> list) throws Exception {

		List<Object> ids = this.collectIds(list);

		IJobContextService srv = (IJobContextService) this.findEntityService(JobContext.class);
		EntityManager em = srv.getEntityManager();

		// delete the obsolete job-context parameters

		// make the delete in two steps, Eclipselink builds a sh*t of delete
		// statement otherwise

		List<String> idsToDelete = em
				.createQuery(
						" select e.id from " + JobContextParam.class.getSimpleName() + " e  where e.clientId = :clientId "
								+ "		and e.jobContext.id in :ids" + "     and not exists ( " + "			select 1 from JobParam p "
								+ "			where p.name = e.paramName " + "			  and p.job.name =  e.jobContext.jobName)" + "   ",
						String.class)
				.setParameter("clientId", Session.user.get().getClientId()).setParameter("ids", ids).getResultList();

		if (!idsToDelete.isEmpty()) {
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("pIds", idsToDelete);

			int x = srv.update("delete from JobContextParam  where id in :pIds", parameters);

			if (LOG.isDebugEnabled()) {
				LOG.debug("Deleted " + x + " obsolete job-context parameters. ");
			}
		} else {
			if (LOG.isDebugEnabled()) {
				LOG.debug("No obsolete job-context parameters to delete. ");
			}
		}

		// create the missing ones

		List<JobContext> jobContexts = srv.findByIds(ids);
		for (JobContext jc : jobContexts) {
			List<JobParam> paramsToCreate = em
					.createQuery("select p from " + JobParam.class.getSimpleName() + " p where p.job.name = :jobName" + "     and p.name not in ("
							+ " select x.paramName " + "   from JobContextParam x " + "  where x.clientId = :clientId"
							+ "    and x.jobContext.id = :id ) ", JobParam.class)
					.setParameter("clientId", Session.user.get().getClientId()).setParameter("id", jc.getId())
					.setParameter("jobName", jc.getJobName()).getResultList();

			for (JobParam jp : paramsToCreate) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Creating job-context parameters: job={} paramName={}.", new Object[] { jc.getJobName(), jp.getName() });
				}
				JobContextParam cp = new JobContextParam();
				cp.setJobContext(jc);
				cp.setParamName(jp.getName());
				cp.setDataType(jp.getDataType());
				jc.addToParams(cp);
			}

		}
		srv.update(jobContexts);
	}
}
