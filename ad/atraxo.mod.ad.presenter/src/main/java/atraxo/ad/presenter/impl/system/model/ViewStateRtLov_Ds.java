/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeLov_Ds;
import atraxo.ad.domain.impl.ad.TViewStateCmpType;
import atraxo.ad.domain.impl.system.ViewState;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ViewState.class, sort = {@SortField(field = ViewStateRtLov_Ds.F_NAME)})
public class ViewStateRtLov_Ds extends AbstractTypeLov_Ds<ViewState> {

	public static final String ALIAS = "ad_ViewStateRtLov_Ds";

	public static final String F_CMP = "cmp";
	public static final String F_CMPTYPE = "cmpType";
	public static final String F_VALUE = "value";
	public static final String F_OWNER = "owner";

	@DsField
	private String cmp;

	@DsField
	private TViewStateCmpType cmpType;

	@DsField
	private String value;

	@DsField(path = "createdBy")
	private String owner;

	/**
	 * Default constructor
	 */
	public ViewStateRtLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ViewStateRtLov_Ds(ViewState e) {
		super(e);
	}

	public String getCmp() {
		return this.cmp;
	}

	public void setCmp(String cmp) {
		this.cmp = cmp;
	}

	public TViewStateCmpType getCmpType() {
		return this.cmpType;
	}

	public void setCmpType(TViewStateCmpType cmpType) {
		this.cmpType = cmpType;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}
}
