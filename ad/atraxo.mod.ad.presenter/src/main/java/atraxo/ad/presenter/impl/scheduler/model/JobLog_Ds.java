/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.scheduler.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.scheduler.JobLog;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = JobLog.class, sort = {@SortField(field = JobLog_Ds.F_STARTTIME, desc = true)})
public class JobLog_Ds extends AbstractAuditable_Ds<JobLog> {

	public static final String ALIAS = "ad_JobLog_Ds";

	public static final String F_STARTTIME = "startTime";
	public static final String F_ENDTIME = "endTime";
	public static final String F_JOBCONTEXTID = "jobContextId";
	public static final String F_JOBCONTEXT = "jobContext";
	public static final String F_JOBNAME = "jobName";
	public static final String F_JOBTIMERID = "jobTimerId";
	public static final String F_JOBTIMER = "jobTimer";

	@DsField
	private Date startTime;

	@DsField
	private Date endTime;

	@DsField(join = "left", path = "jobContext.id")
	private String jobContextId;

	@DsField(join = "left", path = "jobContext.name")
	private String jobContext;

	@DsField(join = "left", path = "jobContext.jobName")
	private String jobName;

	@DsField(join = "left", path = "jobTimer.id")
	private String jobTimerId;

	@DsField(join = "left", path = "jobTimer.name")
	private String jobTimer;

	/**
	 * Default constructor
	 */
	public JobLog_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public JobLog_Ds(JobLog e) {
		super(e);
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getJobContextId() {
		return this.jobContextId;
	}

	public void setJobContextId(String jobContextId) {
		this.jobContextId = jobContextId;
	}

	public String getJobContext() {
		return this.jobContext;
	}

	public void setJobContext(String jobContext) {
		this.jobContext = jobContext;
	}

	public String getJobName() {
		return this.jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobTimerId() {
		return this.jobTimerId;
	}

	public void setJobTimerId(String jobTimerId) {
		this.jobTimerId = jobTimerId;
	}

	public String getJobTimer() {
		return this.jobTimer;
	}

	public void setJobTimer(String jobTimer) {
		this.jobTimer = jobTimer;
	}
}
