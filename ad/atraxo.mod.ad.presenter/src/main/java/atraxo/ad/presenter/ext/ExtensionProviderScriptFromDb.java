package atraxo.ad.presenter.ext;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import atraxo.ad.domain.impl.system.FrameExtension;
import seava.j4e.api.extensions.IExtensionFile;
import seava.j4e.api.extensions.IExtensionProvider;
import seava.j4e.api.extensions.IExtensions;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.extensions.ExtensionFile;
import seava.j4e.presenter.service.AbstractPresenterBaseService;

/**
 * Return extension files defined in database for the given target
 *
 * @author amathe
 */
public class ExtensionProviderScriptFromDb extends AbstractPresenterBaseService implements IExtensionProvider {

	private final static Logger LOGGER = LoggerFactory.getLogger(ExtensionProviderScriptFromDb.class);

	@Override
	public List<IExtensionFile> getFiles(String targetFrame) throws Exception {

		List<IExtensionFile> files = new ArrayList<>();

		try {
			DataSource dataSource = this.getApplicationContext().getBean(DataSource.class);
			JdbcTemplate tpl = new JdbcTemplate(dataSource);
			tpl.queryForList("select 1 from " + FrameExtension.TABLE_NAME + "  where 1=0");

		} catch (Exception e) {
			LOGGER.warn("Warning:could not execute query, will return empty array of files!", e);
			return files;
		}

		List<String> targets = new ArrayList<>();
		targets.add(targetFrame);
		if (!targetFrame.matches(IExtensions.UI_EXTJS_MAIN) && !targetFrame.matches(IExtensions.UI_EXTJS_DASHBOARD)) {
			targets.add(IExtensions.UI_EXTJS_FRAME);
		}

		List<FrameExtension> result = this.findEntityService(FrameExtension.class).getEntityManager()
				.createQuery(" select e from " + FrameExtension.class.getSimpleName() + " e where "
						+ " e.clientId = :clientId and e.frame in :frame and e.active = true order by e.sequenceNo ", FrameExtension.class)
				.setParameter("frame", targets).setParameter("clientId", Session.user.get().getClientId()).getResultList();

		for (FrameExtension e : result) {
			files.add(new ExtensionFile(e.getFileLocation(), e.getRelativePath()));
		}
		return files;
	}

}
