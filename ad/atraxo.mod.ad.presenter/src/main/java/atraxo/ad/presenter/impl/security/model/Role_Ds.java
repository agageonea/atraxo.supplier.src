/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeWithCode_Ds;
import atraxo.ad.domain.impl.security.Role;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Role.class, sort = {@SortField(field = Role_Ds.F_CODE)})
public class Role_Ds extends AbstractTypeWithCode_Ds<Role> {

	public static final String ALIAS = "ad_Role_Ds";

	/**
	 * Default constructor
	 */
	public Role_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Role_Ds(Role e) {
		super(e);
	}
}
