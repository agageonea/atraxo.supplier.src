/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.attachment.qb;

import atraxo.ad.presenter.impl.attachment.model.AttachmentTypeLov_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class AttachmentTypeLov_DsQb
		extends
			QueryBuilderWithJpql<AttachmentTypeLov_Ds, AttachmentTypeLov_Ds, Object> {

	@Override
	public void setFilter(AttachmentTypeLov_Ds filter) {
		if (filter.getTargetType() == null || filter.getTargetType().equals("")) {
			filter.setTargetType("N/A");
		}
		super.setFilter(filter);
	}
}
