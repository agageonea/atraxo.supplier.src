package atraxo.ad.presenter.ext.externalReport.delegate;

import java.io.File;

import org.apache.commons.io.FileUtils;

import atraxo.ad.business.api.externalReport.IReportLogService;
import atraxo.ad.domain.impl.externalReport.ReportLog;
import atraxo.ad.presenter.impl.externalReport.model.ReportLog_Ds;
import atraxo.ad.presenter.impl.externalReport.model.ReportLog_DsParam;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class ReportsLog_Pd extends AbstractPresenterDelegate {

	public void empty(ReportLog_Ds ds) throws Exception {
		IReportLogService reportLogService = (IReportLogService) this.findEntityService(ReportLog.class);
		reportLogService.empty(ds.getExternalReportId());
	}

	public void viewLog(ReportLog_Ds ds, ReportLog_DsParam param) throws Exception {
		IReportLogService reportLogService = (IReportLogService) this.findEntityService(ReportLog.class);
		byte[] reportByte = reportLogService.getReport(ds.getId());
		String path = Session.user.get().getWorkspace().getTempPath();
		String fileName = ds.getName() + "." + ds.getFormat();
		File file = new File(path + File.separator + fileName);
		FileUtils.writeByteArrayToFile(file.getAbsoluteFile(), reportByte);
		param.setReportFile(fileName);
	}
}
