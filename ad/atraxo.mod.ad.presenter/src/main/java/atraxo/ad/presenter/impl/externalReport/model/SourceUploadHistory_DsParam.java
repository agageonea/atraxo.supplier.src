/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.externalReport.model;

/**
 * Generated code. Do not modify in this file.
 */
public class SourceUploadHistory_DsParam {

	public static final String f_reportId = "reportId";
	public static final String f_reportName = "reportName";

	private String reportId;

	private String reportName;

	public String getReportId() {
		return this.reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getReportName() {
		return this.reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
}
