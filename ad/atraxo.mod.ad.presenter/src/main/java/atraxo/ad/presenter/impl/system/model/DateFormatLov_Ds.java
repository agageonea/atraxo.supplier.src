/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractTypeNTLov_Ds;
import atraxo.ad.domain.impl.system.DateFormat;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DateFormat.class, sort = {@SortField(field = DateFormatLov_Ds.F_NAME)})
public class DateFormatLov_Ds extends AbstractTypeNTLov_Ds<DateFormat> {

	public static final String ALIAS = "ad_DateFormatLov_Ds";

	/**
	 * Default constructor
	 */
	public DateFormatLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DateFormatLov_Ds(DateFormat e) {
		super(e);
	}
}
