/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.externalReport.model;

/**
 * Generated code. Do not modify in this file.
 */
public class ReportLog_DsParam {

	public static final String f_reportFile = "reportFile";

	private String reportFile;

	public String getReportFile() {
		return this.reportFile;
	}

	public void setReportFile(String reportFile) {
		this.reportFile = reportFile;
	}
}
