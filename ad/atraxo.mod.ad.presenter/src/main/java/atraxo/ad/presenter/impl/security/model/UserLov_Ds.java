/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeWithCodeLov_Ds;
import atraxo.ad.domain.impl.security.User;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = User.class, sort = {@SortField(field = UserLov_Ds.F_CODE)})
public class UserLov_Ds extends AbstractTypeWithCodeLov_Ds<User> {

	public static final String ALIAS = "ad_UserLov_Ds";

	/**
	 * Default constructor
	 */
	public UserLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public UserLov_Ds(User e) {
		super(e);
	}
}
