/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.ext.system.qb;

import atraxo.ad.presenter.impl.system.model.ViewStateRtLov_Ds;
import atraxo.ad.presenter.impl.system.model.ViewStateRtLov_DsParam;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class ViewStateRtLov_DsQb extends QueryBuilderWithJpql<ViewStateRtLov_Ds, ViewStateRtLov_Ds, ViewStateRtLov_DsParam> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		String userCode = Session.user.get().getCode();

		if (this.params.getHideMine() != null && this.params.getHideMine()) {
			this.addFilterCondition("  e.createdBy <> :userCode ");
			this.addCustomFilterItem("userCode", userCode);
		}

		if (this.params.getHideOthers() != null && this.params.getHideOthers()) {
			this.addFilterCondition("  e.createdBy = :userCode ");
			this.addCustomFilterItem("userCode", userCode);
		}
	}

}
