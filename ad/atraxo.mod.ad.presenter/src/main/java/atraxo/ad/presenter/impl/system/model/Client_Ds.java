/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractTypeWithCodeNT_Ds;
import atraxo.ad.domain.impl.ad.Project;
import atraxo.ad.domain.impl.system.Client;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Client.class, sort = {@SortField(field = Client_Ds.F_CODE)})
public class Client_Ds extends AbstractTypeWithCodeNT_Ds<Client> {

	public static final String ALIAS = "ad_Client_Ds";

	public static final String F_WORKSPACEPATH = "workspacePath";
	public static final String F_IMPORTPATH = "importPath";
	public static final String F_EXPORTPATH = "exportPath";
	public static final String F_TEMPPATH = "tempPath";
	public static final String F_ADMINROLE = "adminRole";
	public static final String F_PRODUCT = "product";

	@DsField
	private String workspacePath;

	@DsField
	private String importPath;

	@DsField
	private String exportPath;

	@DsField
	private String tempPath;

	@DsField
	private String adminRole;

	@DsField(fetch = false)
	private Project product;

	/**
	 * Default constructor
	 */
	public Client_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Client_Ds(Client e) {
		super(e);
	}

	public String getWorkspacePath() {
		return this.workspacePath;
	}

	public void setWorkspacePath(String workspacePath) {
		this.workspacePath = workspacePath;
	}

	public String getImportPath() {
		return this.importPath;
	}

	public void setImportPath(String importPath) {
		this.importPath = importPath;
	}

	public String getExportPath() {
		return this.exportPath;
	}

	public void setExportPath(String exportPath) {
		this.exportPath = exportPath;
	}

	public String getTempPath() {
		return this.tempPath;
	}

	public void setTempPath(String tempPath) {
		this.tempPath = tempPath;
	}

	public String getAdminRole() {
		return this.adminRole;
	}

	public void setAdminRole(String adminRole) {
		this.adminRole = adminRole;
	}

	public Project getProduct() {
		return this.product;
	}

	public void setProduct(Project product) {
		this.product = product;
	}
}
