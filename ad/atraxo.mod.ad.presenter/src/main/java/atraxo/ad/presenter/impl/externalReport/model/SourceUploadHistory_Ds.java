/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.externalReport.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.externalReport.SourceUploadHistory;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = SourceUploadHistory.class, sort = {@SortField(field = SourceUploadHistory_Ds.F_UPLOADDATE, desc = true)})
@RefLookups({@RefLookup(refId = SourceUploadHistory_Ds.F_EXTERNALREPORTID)})
public class SourceUploadHistory_Ds
		extends
			AbstractAuditable_Ds<SourceUploadHistory> {

	public static final String ALIAS = "ad_SourceUploadHistory_Ds";

	public static final String F_EXTERNALREPORTID = "externalReportId";
	public static final String F_EXTERNALREPORTNAME = "externalReportName";
	public static final String F_NAME = "name";
	public static final String F_UPLOADDATE = "uploadDate";
	public static final String F_UPLOADER = "uploader";
	public static final String F_NOTES = "notes";
	public static final String F_FILENAME = "fileName";
	public static final String F_FILE = "file";

	@DsField(join = "left", path = "externalReport.id")
	private String externalReportId;

	@DsField(join = "left", path = "externalReport.name")
	private String externalReportName;

	@DsField
	private String name;

	@DsField
	private Date uploadDate;

	@DsField
	private String uploader;

	@DsField
	private String notes;

	@DsField
	private String fileName;

	@DsField(fetch = false)
	private String file;

	/**
	 * Default constructor
	 */
	public SourceUploadHistory_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public SourceUploadHistory_Ds(SourceUploadHistory e) {
		super(e);
	}

	public String getExternalReportId() {
		return this.externalReportId;
	}

	public void setExternalReportId(String externalReportId) {
		this.externalReportId = externalReportId;
	}

	public String getExternalReportName() {
		return this.externalReportName;
	}

	public void setExternalReportName(String externalReportName) {
		this.externalReportName = externalReportName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getUploadDate() {
		return this.uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getUploader() {
		return this.uploader;
	}

	public void setUploader(String uploader) {
		this.uploader = uploader;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFile() {
		return this.file;
	}

	public void setFile(String file) {
		this.file = file;
	}
}
