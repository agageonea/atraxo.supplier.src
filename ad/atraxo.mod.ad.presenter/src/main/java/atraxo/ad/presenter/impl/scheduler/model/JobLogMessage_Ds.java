/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.scheduler.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.ad.TJobLogMsgType;
import atraxo.ad.domain.impl.scheduler.JobLogMessage;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = JobLogMessage.class)
public class JobLogMessage_Ds extends AbstractAuditable_Ds<JobLogMessage> {

	public static final String ALIAS = "ad_JobLogMessage_Ds";

	public static final String F_MESSAGETYPE = "messageType";
	public static final String F_MESSAGE = "message";
	public static final String F_JOBLOGID = "jobLogId";

	@DsField
	private TJobLogMsgType messageType;

	@DsField
	private String message;

	@DsField(join = "left", path = "jobLog.id")
	private String jobLogId;

	/**
	 * Default constructor
	 */
	public JobLogMessage_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public JobLogMessage_Ds(JobLogMessage e) {
		super(e);
	}

	public TJobLogMsgType getMessageType() {
		return this.messageType;
	}

	public void setMessageType(TJobLogMsgType messageType) {
		this.messageType = messageType;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getJobLogId() {
		return this.jobLogId;
	}

	public void setJobLogId(String jobLogId) {
		this.jobLogId = jobLogId;
	}
}
