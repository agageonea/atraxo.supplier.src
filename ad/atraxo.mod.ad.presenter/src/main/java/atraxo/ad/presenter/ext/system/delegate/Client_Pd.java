package atraxo.ad.presenter.ext.system.delegate;

import java.util.List;

import atraxo.ad.business.api.system.IClientService;
import atraxo.ad.domain.impl.system.Client;
import atraxo.ad.presenter.impl.system.model.Client_Ds;
import seava.j4e.api.action.impex.IImportDataPackage;
import seava.j4e.commons.action.impex.DataPackage;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class Client_Pd extends AbstractPresenterDelegate {

	private static final String CLIENT_IDL = "/idl/client/index.xml";

	public void upgrade(Client_Ds ds) throws Exception {
		IClientService srv = (IClientService) this.findEntityService(Client.class);
		Client c = srv.findByCode(ds.getCode());

		IImportDataPackage dp = DataPackage.forIndexFile(c.getWorkspacePath() + CLIENT_IDL);
		srv.doUpgrade(c, dp);
	}

	public void upgrade(List<Client_Ds> list) throws Exception {
		for (Client_Ds ds : list) {
			this.upgrade(ds);
		}
	}
}
