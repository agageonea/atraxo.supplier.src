/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.report.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeLov_Ds;
import atraxo.ad.domain.impl.report.ReportParam;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ReportParam.class, sort = {@SortField(field = ReportParamLov_Ds.F_NAME)})
public class ReportParamLov_Ds extends AbstractTypeLov_Ds<ReportParam> {

	public static final String ALIAS = "ad_ReportParamLov_Ds";

	public static final String F_REPORTID = "reportId";
	public static final String F_TITLE = "title";

	@DsField(join = "left", path = "report.id")
	private String reportId;

	@DsField
	private String title;

	/**
	 * Default constructor
	 */
	public ReportParamLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ReportParamLov_Ds(ReportParam e) {
		super(e);
	}

	public String getReportId() {
		return this.reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
