/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.report.cnv;

import atraxo.ad.business.api.report.IReportService;
import atraxo.ad.domain.impl.report.DsReportUsage;
import atraxo.ad.domain.impl.report.Report;
import atraxo.ad.presenter.impl.report.model.DsReportUsage_Ds;
import javax.persistence.EntityManager;
import seava.j4e.api.action.result.IDsConverter;
import seava.j4e.presenter.converter.AbstractDsConverter;

/**
 * Converter DsReportUsage_DsCnv
 *
 * Generated code. Do not modify in this file.
 */
public class DsReportUsage_DsCnv
		extends
			AbstractDsConverter<DsReportUsage_Ds, DsReportUsage>
		implements
			IDsConverter<DsReportUsage_Ds, DsReportUsage> {

	protected void modelToEntityReferences(DsReportUsage_Ds ds,
			DsReportUsage e, boolean isInsert, EntityManager em)
			throws Exception {

		if (ds.getReportId() == null) {
			Report x = ((IReportService) findEntityService(Report.class))
					.findByCode(ds.getReport());
			ds.setReportId(x.getId());
		}
		super.modelToEntityReferences(ds, e, isInsert, em);
	}
}
