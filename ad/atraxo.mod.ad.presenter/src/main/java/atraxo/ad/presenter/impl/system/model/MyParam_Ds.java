/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractTypeWithCodeNT_Ds;
import atraxo.ad.domain.impl.system.Param;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Param.class, sort = {@SortField(field = MyParam_Ds.F_CODE)})
public class MyParam_Ds extends AbstractTypeWithCodeNT_Ds<Param> {

	public static final String ALIAS = "ad_MyParam_Ds";

	public static final String F_DEFAULTVALUE = "defaultValue";
	public static final String F_LISTOFVALUES = "listOfValues";

	@DsField
	private String defaultValue;

	@DsField
	private String listOfValues;

	/**
	 * Default constructor
	 */
	public MyParam_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public MyParam_Ds(Param e) {
		super(e);
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getListOfValues() {
		return this.listOfValues;
	}

	public void setListOfValues(String listOfValues) {
		this.listOfValues = listOfValues;
	}
}
