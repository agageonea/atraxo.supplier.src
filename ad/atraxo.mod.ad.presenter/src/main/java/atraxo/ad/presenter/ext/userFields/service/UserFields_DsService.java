/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.ext.userFields.service;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.ad.business.api.system.IDataSourceService;
import atraxo.ad.business.ext.exceptions.AdErrorCode;
import atraxo.ad.domain.impl.system.DataSource;
import atraxo.ad.domain.impl.system.DataSourceField;
import atraxo.ad.domain.impl.userFields.UserFields;
import atraxo.ad.presenter.impl.userFields.model.UserFields_Ds;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service UserFields_DsService
 */
public class UserFields_DsService extends AbstractEntityDsService<UserFields_Ds, UserFields_Ds, Object, UserFields>
		implements IDsService<UserFields_Ds, UserFields_Ds, Object> {

	@Autowired
	private IDataSourceService srv;

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.presenter.service.ds.AbstractEntityDsWriteService#preInsert(seava.j4e.presenter.model.AbstractDsModel, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	protected void preInsert(UserFields_Ds ds, UserFields e, Object params) throws Exception {
		super.preInsert(ds, e, params);
		this.validateFieldName(e);
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.presenter.service.ds.AbstractEntityDsWriteService#preUpdateAfterEntity(seava.j4e.presenter.model.AbstractDsModel,
	 * java.lang.Object, java.lang.Object)
	 */
	@Override
	protected void preUpdateAfterEntity(UserFields_Ds ds, UserFields e, Object params) throws Exception {
		super.preUpdateAfterEntity(ds, e, params);
		this.validateFieldName(e);
	}

	private void validateFieldName(UserFields e) throws BusinessException {
		DataSource dataSource = this.srv.findByModel(e.getBusinessArea().getDsModel());
		for (DataSourceField dsField : dataSource.getFields()) {
			if (dsField.getName().equalsIgnoreCase(e.getName())) {
				throw new BusinessException(AdErrorCode.FIELD_ALREADY_EXISTS,
						String.format(AdErrorCode.FIELD_ALREADY_EXISTS.getErrMsg(), e.getName(), e.getBusinessArea().getName()));
			}
		}
	}

}
