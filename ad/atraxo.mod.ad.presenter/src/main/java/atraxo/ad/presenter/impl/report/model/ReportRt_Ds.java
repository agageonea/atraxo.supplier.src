/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.report.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeWithCode_Ds;
import atraxo.ad.domain.impl.report.Report;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
/**
 * Read-only report info to be used for runtime report execution center
 */
@Ds(entity = Report.class, sort = {@SortField(field = ReportRt_Ds.F_CODE)})
public class ReportRt_Ds extends AbstractTypeWithCode_Ds<Report> {

	public static final String ALIAS = "ad_ReportRt_Ds";

	public static final String F_REPORTSERVERID = "reportServerId";
	public static final String F_REPORTSERVER = "reportServer";
	public static final String F_SERVERURL = "serverUrl";
	public static final String F_QUERYBUILDERCLASS = "queryBuilderClass";
	public static final String F_CONTEXTPATH = "contextPath";

	@DsField(join = "left", path = "reportServer.id")
	private String reportServerId;

	@DsField(join = "left", path = "reportServer.name")
	private String reportServer;

	@DsField(join = "left", path = "reportServer.url")
	private String serverUrl;

	@DsField(join = "left", path = "reportServer.queryBuilderClass")
	private String queryBuilderClass;

	@DsField
	private String contextPath;

	/**
	 * Default constructor
	 */
	public ReportRt_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ReportRt_Ds(Report e) {
		super(e);
	}

	public String getReportServerId() {
		return this.reportServerId;
	}

	public void setReportServerId(String reportServerId) {
		this.reportServerId = reportServerId;
	}

	public String getReportServer() {
		return this.reportServer;
	}

	public void setReportServer(String reportServer) {
		this.reportServer = reportServer;
	}

	public String getServerUrl() {
		return this.serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	public String getQueryBuilderClass() {
		return this.queryBuilderClass;
	}

	public void setQueryBuilderClass(String queryBuilderClass) {
		this.queryBuilderClass = queryBuilderClass;
	}

	public String getContextPath() {
		return this.contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}
}
