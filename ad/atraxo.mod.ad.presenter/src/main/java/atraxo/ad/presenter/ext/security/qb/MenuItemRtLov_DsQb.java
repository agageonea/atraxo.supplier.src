/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.ext.security.qb;

import seava.j4e.api.session.Session;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import atraxo.ad.presenter.impl.security.model.MenuItemRtLov_Ds;

public class MenuItemRtLov_DsQb extends QueryBuilderWithJpql<MenuItemRtLov_Ds, MenuItemRtLov_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {

		if (!Session.user.get().getProfile().isAdministrator()) {

			this.addFilterCondition("  e.id in ( select p.id from  MenuItem p, IN (p.roles) c where c.code in :pRoles )  ");
			this.addCustomFilterItem("pRoles", Session.user.get().getProfile().getRoles());

		}
	}

}
