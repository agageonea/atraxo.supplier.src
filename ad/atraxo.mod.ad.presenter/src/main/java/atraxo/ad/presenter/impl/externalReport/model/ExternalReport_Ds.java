/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.externalReport.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractType_Ds;
import atraxo.ad.domain.impl.businessArea.BusinessArea;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ExternalReport.class, sort = {@SortField(field = ExternalReport_Ds.F_NAME)})
@RefLookups({@RefLookup(refId = ExternalReport_Ds.F_BUSINESSAREAID, namedQuery = BusinessArea.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = ExternalReport_Ds.F_BUSINESSAREA)})})
public class ExternalReport_Ds extends AbstractType_Ds<ExternalReport> {

	public static final String ALIAS = "ad_ExternalReport_Ds";

	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_SYSTEM = "system";
	public static final String F_DESIGNNAME = "designName";
	public static final String F_REPORTCODE = "reportCode";
	public static final String F_REPORTID = "reportId";
	public static final String F_BUSINESSAREAID = "businessAreaId";
	public static final String F_BUSINESSAREA = "businessArea";
	public static final String F_DSNAME = "dsName";
	public static final String F_DSMODEL = "dsModel";
	public static final String F_DSALIAS = "dsAlias";
	public static final String F_URL = "url";
	public static final String F_LASTEXECUTION = "lastExecution";
	public static final String F_LASTEXECUTIONSTATUS = "lastExecutionStatus";

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private Boolean system;

	@DsField
	private String designName;

	@DsField
	private String reportCode;

	@DsField(path = "id")
	private String reportId;

	@DsField(join = "left", path = "businessArea.id")
	private String businessAreaId;

	@DsField(join = "left", path = "businessArea.name")
	private String businessArea;

	@DsField(join = "left", path = "businessArea.dsName")
	private String dsName;

	@DsField(join = "left", path = "businessArea.dsModel")
	private String dsModel;

	@DsField(join = "left", path = "businessArea.dsAlias")
	private String dsAlias;

	@DsField(fetch = false)
	private String url;

	@DsField(fetch = false)
	private Date lastExecution;

	@DsField(fetch = false)
	private String lastExecutionStatus;

	/**
	 * Default constructor
	 */
	public ExternalReport_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ExternalReport_Ds(ExternalReport e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getSystem() {
		return this.system;
	}

	public void setSystem(Boolean system) {
		this.system = system;
	}

	public String getDesignName() {
		return this.designName;
	}

	public void setDesignName(String designName) {
		this.designName = designName;
	}

	public String getReportCode() {
		return this.reportCode;
	}

	public void setReportCode(String reportCode) {
		this.reportCode = reportCode;
	}

	public String getReportId() {
		return this.reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getBusinessAreaId() {
		return this.businessAreaId;
	}

	public void setBusinessAreaId(String businessAreaId) {
		this.businessAreaId = businessAreaId;
	}

	public String getBusinessArea() {
		return this.businessArea;
	}

	public void setBusinessArea(String businessArea) {
		this.businessArea = businessArea;
	}

	public String getDsName() {
		return this.dsName;
	}

	public void setDsName(String dsName) {
		this.dsName = dsName;
	}

	public String getDsModel() {
		return this.dsModel;
	}

	public void setDsModel(String dsModel) {
		this.dsModel = dsModel;
	}

	public String getDsAlias() {
		return this.dsAlias;
	}

	public void setDsAlias(String dsAlias) {
		this.dsAlias = dsAlias;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getLastExecution() {
		return this.lastExecution;
	}

	public void setLastExecution(Date lastExecution) {
		this.lastExecution = lastExecution;
	}

	public String getLastExecutionStatus() {
		return this.lastExecutionStatus;
	}

	public void setLastExecutionStatus(String lastExecutionStatus) {
		this.lastExecutionStatus = lastExecutionStatus;
	}
}
