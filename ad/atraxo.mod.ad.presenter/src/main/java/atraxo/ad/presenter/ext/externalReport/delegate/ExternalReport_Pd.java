package atraxo.ad.presenter.ext.externalReport.delegate;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.ad.business.api.externalReport.IExternalReportService;
import atraxo.ad.business.api.externalReport.ISourceUploadHistoryService;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.SourceUploadHistory;
import atraxo.ad.presenter.impl.externalReport.model.ExternalReport_Ds;
import atraxo.ad.presenter.impl.externalReport.model.ExternalReport_DsParam;
import seava.j4e.api.Constants;
import seava.j4e.api.ISettings;
import seava.j4e.api.service.report.IReportService;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author vhojda
 */
public class ExternalReport_Pd extends AbstractPresenterDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExternalReport_Pd.class);

	/**
	 * @param ds
	 */
	public void uploadReportTemplate(ExternalReport_Ds ds) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START uploadReportTemplate()");
		}

		IReportService reportService = this.getApplicationContext().getBean("reportService", IReportService.class);

		Path path = Paths.get(ds.getDesignName());
		byte[] data = Files.readAllBytes(path);
		reportService.uploadReport(data, ds.getDesignName());

		ISettings settings = this.getApplicationContext().getBean(ISettings.class);

		String externalScheme = settings.get(Constants.PROP_SYS_EXTERNAL_SCHEME);
		String externalServerName = settings.get(Constants.PROP_SYS_EXTERNAL_SERVER_NAME);
		String externalServerPort = settings.get(Constants.PROP_SYS_EXTERNAL_SERVER_PORT);
		String externalContextPath = settings.get(Constants.PROP_SYS_EXTERNAL_CONTEXT_PATH);
		String url = new StringBuilder(externalScheme).append("://").append(externalServerName).append(":").append(externalServerPort)
				.append(externalContextPath).toString();

		ds.setUrl(url + Constants.SERVLETPATH_DATA + Constants.CTXPATH_DS + "/" + ExternalReport_Ds.ALIAS + "." + ds.getReportCode() + "?"
				+ Constants.REQUEST_PARAM_ACTION + "=" + Constants.DS_ACTION_DOWNLOAD_RPT_DESIGN);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END uploadReportTemplate()");
		}
	}

	/**
	 * @param ds
	 * @throws Exception
	 */
	public void runReportTemplate(ExternalReport_Ds ds, ExternalReport_DsParam params) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START runReportTemplate()");
		}
		IExternalReportService reportService = (IExternalReportService) this.findEntityService(ExternalReport.class);
		reportService.runReport(ds.getId(), params.getFormat().getName(), null);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END runReportTemplate()");
		}
	}

	/**
	 * @param report
	 * @param attachment
	 * @throws Exception
	 */
	private void saveUploadHistory(ExternalReport report, Attachment attachment) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START saveUploadHistory()");
		}

		SourceUploadHistory hist = new SourceUploadHistory();
		hist.setName(attachment.getName());
		hist.setNotes(attachment.getNotes());
		hist.setActive(Boolean.TRUE);
		hist.setExternalReport(report);
		hist.setUploadDate(new Date());
		hist.setUploader(Session.user.get().getLoginName());
		hist.setFileName(report.getDesignName());

		ISourceUploadHistoryService sourceUploadReportservice = (ISourceUploadHistoryService) this.findEntityService(SourceUploadHistory.class);
		sourceUploadReportservice.insert(hist);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END saveUploadHistory()");
		}
	}

}
