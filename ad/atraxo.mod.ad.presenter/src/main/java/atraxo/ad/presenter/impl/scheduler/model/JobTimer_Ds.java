/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.scheduler.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractType_Ds;
import atraxo.ad.domain.impl.ad.TTimerIntervalType;
import atraxo.ad.domain.impl.ad.TTimerType;
import atraxo.ad.domain.impl.scheduler.JobContext;
import atraxo.ad.domain.impl.scheduler.JobTimer;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = JobTimer.class, sort = {@SortField(field = JobTimer_Ds.F_NAME)})
@RefLookups({@RefLookup(refId = JobTimer_Ds.F_JOBCONTEXTID, namedQuery = JobContext.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = JobTimer_Ds.F_JOBCONTEXT)})})
public class JobTimer_Ds extends AbstractType_Ds<JobTimer> {

	public static final String ALIAS = "ad_JobTimer_Ds";

	public static final String F_STARTTIME = "startTime";
	public static final String F_ENDTIME = "endTime";
	public static final String F_TYPE = "type";
	public static final String F_CRONEXPRESSION = "cronExpression";
	public static final String F_REPEATCOUNT = "repeatCount";
	public static final String F_REPEATINTERVAL = "repeatInterval";
	public static final String F_REPEATINTERVALTYPE = "repeatIntervalType";
	public static final String F_JOBCONTEXTID = "jobContextId";
	public static final String F_JOBCONTEXT = "jobContext";
	public static final String F_JOBNAME = "jobName";

	@DsField
	private Date startTime;

	@DsField
	private Date endTime;

	@DsField
	private TTimerType type;

	@DsField
	private String cronExpression;

	@DsField
	private Integer repeatCount;

	@DsField
	private Integer repeatInterval;

	@DsField
	private TTimerIntervalType repeatIntervalType;

	@DsField(join = "left", path = "jobContext.id")
	private String jobContextId;

	@DsField(join = "left", path = "jobContext.name")
	private String jobContext;

	@DsField(join = "left", path = "jobContext.jobName")
	private String jobName;

	/**
	 * Default constructor
	 */
	public JobTimer_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public JobTimer_Ds(JobTimer e) {
		super(e);
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public TTimerType getType() {
		return this.type;
	}

	public void setType(TTimerType type) {
		this.type = type;
	}

	public String getCronExpression() {
		return this.cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public Integer getRepeatCount() {
		return this.repeatCount;
	}

	public void setRepeatCount(Integer repeatCount) {
		this.repeatCount = repeatCount;
	}

	public Integer getRepeatInterval() {
		return this.repeatInterval;
	}

	public void setRepeatInterval(Integer repeatInterval) {
		this.repeatInterval = repeatInterval;
	}

	public TTimerIntervalType getRepeatIntervalType() {
		return this.repeatIntervalType;
	}

	public void setRepeatIntervalType(TTimerIntervalType repeatIntervalType) {
		this.repeatIntervalType = repeatIntervalType;
	}

	public String getJobContextId() {
		return this.jobContextId;
	}

	public void setJobContextId(String jobContextId) {
		this.jobContextId = jobContextId;
	}

	public String getJobContext() {
		return this.jobContext;
	}

	public void setJobContext(String jobContext) {
		this.jobContext = jobContext;
	}

	public String getJobName() {
		return this.jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
}
