/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.report.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeLov_Ds;
import atraxo.ad.domain.impl.report.ReportServer;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ReportServer.class, sort = {@SortField(field = ReportServerLov_Ds.F_NAME)})
public class ReportServerLov_Ds extends AbstractTypeLov_Ds<ReportServer> {

	public static final String ALIAS = "ad_ReportServerLov_Ds";

	/**
	 * Default constructor
	 */
	public ReportServerLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ReportServerLov_Ds(ReportServer e) {
		super(e);
	}
}
