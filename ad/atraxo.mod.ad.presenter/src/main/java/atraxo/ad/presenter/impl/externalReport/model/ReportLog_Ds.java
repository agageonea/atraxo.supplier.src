/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.externalReport.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.ad.ReportFormat;
import atraxo.ad.domain.impl.ad.ReportStatus;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.ReportLog;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ReportLog.class, sort = {@SortField(field = ReportLog_Ds.F_RUNDATE, desc = true)})
@RefLookups({@RefLookup(refId = ReportLog_Ds.F_EXTERNALREPORTID, namedQuery = ExternalReport.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = ReportLog_Ds.F_EXTERNALREPORTNAME)})})
public class ReportLog_Ds extends AbstractAuditable_Ds<ReportLog> {

	public static final String ALIAS = "ad_ReportLog_Ds";

	public static final String F_EXTERNALREPORTID = "externalReportId";
	public static final String F_EXTERNALREPORTNAME = "externalReportName";
	public static final String F_RUNBY = "runBy";
	public static final String F_NAME = "name";
	public static final String F_FORMAT = "format";
	public static final String F_RUNDATE = "runDate";
	public static final String F_STATUS = "status";
	public static final String F_LOG = "log";

	@DsField(join = "left", path = "externalReport.id")
	private String externalReportId;

	@DsField(join = "left", path = "externalReport.name")
	private String externalReportName;

	@DsField(path = "createdBy")
	private String runBy;

	@DsField
	private String name;

	@DsField
	private ReportFormat format;

	@DsField
	private Date runDate;

	@DsField
	private ReportStatus status;

	@DsField
	private String log;

	/**
	 * Default constructor
	 */
	public ReportLog_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ReportLog_Ds(ReportLog e) {
		super(e);
	}

	public String getExternalReportId() {
		return this.externalReportId;
	}

	public void setExternalReportId(String externalReportId) {
		this.externalReportId = externalReportId;
	}

	public String getExternalReportName() {
		return this.externalReportName;
	}

	public void setExternalReportName(String externalReportName) {
		this.externalReportName = externalReportName;
	}

	public String getRunBy() {
		return this.runBy;
	}

	public void setRunBy(String runBy) {
		this.runBy = runBy;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ReportFormat getFormat() {
		return this.format;
	}

	public void setFormat(ReportFormat format) {
		this.format = format;
	}

	public Date getRunDate() {
		return this.runDate;
	}

	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}

	public ReportStatus getStatus() {
		return this.status;
	}

	public void setStatus(ReportStatus status) {
		this.status = status;
	}

	public String getLog() {
		return this.log;
	}

	public void setLog(String log) {
		this.log = log;
	}
}
