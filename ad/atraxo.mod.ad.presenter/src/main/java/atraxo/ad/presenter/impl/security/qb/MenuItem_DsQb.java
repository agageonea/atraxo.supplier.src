/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.qb;

import atraxo.ad.presenter.impl.security.model.MenuItem_Ds;
import atraxo.ad.presenter.impl.security.model.MenuItem_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class MenuItem_DsQb
		extends
			QueryBuilderWithJpql<MenuItem_Ds, MenuItem_Ds, MenuItem_DsParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getFoldersOnly() != null
				&& !"".equals(this.params.getFoldersOnly())) {
			addFilterCondition("  (  (:foldersOnly = false  ) or (e.frame is null or e.frame = '' ) ) ");
			addCustomFilterItem("foldersOnly", this.params.getFoldersOnly());
		}
		if (this.params != null && this.params.getFramesOnly() != null
				&& !"".equals(this.params.getFramesOnly())) {
			addFilterCondition("  (  (:framesOnly = false  ) or (e.frame is not null and e.frame <> '' ) ) ");
			addCustomFilterItem("framesOnly", this.params.getFramesOnly());
		}
		if (this.params != null && this.params.getWithRoleId() != null
				&& !"".equals(this.params.getWithRoleId())) {
			addFilterCondition("  e.id in ( select p.id from  MenuItem p, IN (p.roles) c where c.id = :withRoleId )  ");
			addCustomFilterItem("withRoleId", this.params.getWithRoleId());
		}
	}
}
