package atraxo.ad.presenter.ext.system.delegate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atraxo.ad.business.api.system.IDateFormatMaskService;
import atraxo.ad.domain.impl.system.DateFormat;
import atraxo.ad.domain.impl.system.DateFormatMask;
import atraxo.ad.presenter.impl.system.model.DateFormat_Ds;
import seava.j4e.api.enums.DateFormatAttribute;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class DateFormat_Pd extends AbstractPresenterDelegate {

	/**
	 * Remove obsolete masks and add the missing ones to the specified date-format.
	 *
	 * @param ds
	 * @throws Exception
	 */
	public void synchronizeMasks(DateFormat_Ds ds) throws Exception {

		IDateFormatMaskService srv = (IDateFormatMaskService) this.findEntityService(DateFormatMask.class);
		DateFormatAttribute[] dfas = DateFormatAttribute.values();
		List<DateFormatMask> masks = srv.findByDateFormatId(ds.getId());

		// delete obsolete

		List<String> namesToDelete = new ArrayList<>();
		for (DateFormatMask mask : masks) {
			String name = mask.getMask();
			if (!this.existsMask(dfas, name)) {
				namesToDelete.add(name);
			}
		}

		Map<String, Object> params = new HashMap<>();
		params.put("masks", namesToDelete);
		params.put("dateFormatId", ds.getId());

		srv.update("delete from " + DateFormatMask.class.getSimpleName() + " where mask in :masks and dateFormat.id = :dateFormatId", params);

		List<DateFormatMask> result = new ArrayList<>();
		DateFormat df = srv.findById(ds.getId(), DateFormat.class);
		for (DateFormatAttribute a : dfas) {
			if (!this.existsMask(masks, a.name())) {
				DateFormatMask attr = new DateFormatMask();
				attr.setMask(a.name());
				attr.setDateFormat(df);
				attr.setValue(this.getSettings().get(a.getPropertyFileKey()));
				result.add(attr);
			}
		}
		srv.insert(result);
	}

	private boolean existsMask(DateFormatAttribute[] dfas, String mask) {
		int l = dfas.length;
		for (int i = 0; i < l; i++) {
			if (dfas[i].name().equals(mask)) {
				return true;
			}
		}
		return false;
	}

	private boolean existsMask(List<DateFormatMask> list, String mask) {
		for (DateFormatMask m : list) {
			if (m.getMask().equals(mask)) {
				return true;
			}
		}
		return false;
	}

}
