/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.service;

import atraxo.ad.domain.impl.system.ViewState;
import atraxo.ad.presenter.impl.system.model.ViewState_Ds;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;
/**
 * Services ViewState_DsService
 *
 * Generated code. Do not modify in this file.
 */
public class ViewState_DsService
		extends
			AbstractEntityDsService<ViewState_Ds, ViewState_Ds, Object, ViewState>
		implements
			IDsService<ViewState_Ds, ViewState_Ds, Object> {

	@Override
	public void preInsert(ViewState_Ds ds, Object params) {
		ds.setActive(true);
	}

	@Override
	public void preUpdate(ViewState_Ds ds, Object params) {
		ds.setActive(true);
	}
}
