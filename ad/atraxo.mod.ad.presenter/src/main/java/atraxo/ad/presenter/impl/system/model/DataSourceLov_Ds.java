/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractTypeNTLov_Ds;
import atraxo.ad.domain.impl.system.DataSource;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DataSource.class, sort = {@SortField(field = DataSourceLov_Ds.F_NAME)})
public class DataSourceLov_Ds extends AbstractTypeNTLov_Ds<DataSource> {

	public static final String ALIAS = "ad_DataSourceLov_Ds";

	public static final String F_MODEL = "model";

	@DsField
	private String model;

	/**
	 * Default constructor
	 */
	public DataSourceLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DataSourceLov_Ds(DataSource e) {
		super(e);
	}

	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}
}
