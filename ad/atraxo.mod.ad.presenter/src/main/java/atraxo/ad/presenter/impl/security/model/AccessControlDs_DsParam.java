/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

/**
 * Generated code. Do not modify in this file.
 */
public class AccessControlDs_DsParam {

	public static final String f_withRoleId = "withRoleId";
	public static final String f_withRole = "withRole";

	private String withRoleId;

	private String withRole;

	public String getWithRoleId() {
		return this.withRoleId;
	}

	public void setWithRoleId(String withRoleId) {
		this.withRoleId = withRoleId;
	}

	public String getWithRole() {
		return this.withRole;
	}

	public void setWithRole(String withRole) {
		this.withRole = withRole;
	}
}
