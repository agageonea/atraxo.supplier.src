/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import java.util.Date;
/**
 * Generated code. Do not modify in this file.
 */
public class ParamValue_DsParam {

	public static final String f_validAt = "validAt";

	private Date validAt;

	public Date getValidAt() {
		return this.validAt;
	}

	public void setValidAt(Date validAt) {
		this.validAt = validAt;
	}
}
