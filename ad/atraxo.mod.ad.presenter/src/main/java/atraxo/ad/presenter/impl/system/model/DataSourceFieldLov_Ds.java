/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractTypeNTLov_Ds;
import atraxo.ad.domain.impl.system.DataSourceField;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DataSourceField.class, sort = {@SortField(field = DataSourceFieldLov_Ds.F_NAME)})
public class DataSourceFieldLov_Ds
		extends
			AbstractTypeNTLov_Ds<DataSourceField> {

	public static final String ALIAS = "ad_DataSourceFieldLov_Ds";

	public static final String F_DATATYPE = "dataType";
	public static final String F_DATASOURCEID = "dataSourceId";
	public static final String F_DATASOURCENAME = "dataSourceName";

	@DsField
	private String dataType;

	@DsField(join = "left", path = "dataSource.id")
	private String dataSourceId;

	@DsField(join = "left", path = "dataSource.name")
	private String dataSourceName;

	/**
	 * Default constructor
	 */
	public DataSourceFieldLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DataSourceFieldLov_Ds(DataSourceField e) {
		super(e);
	}

	public String getDataType() {
		return this.dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getDataSourceId() {
		return this.dataSourceId;
	}

	public void setDataSourceId(String dataSourceId) {
		this.dataSourceId = dataSourceId;
	}

	public String getDataSourceName() {
		return this.dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}
}
