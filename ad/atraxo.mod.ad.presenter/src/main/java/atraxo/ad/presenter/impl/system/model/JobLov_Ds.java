/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractTypeNTLov_Ds;
import atraxo.ad.domain.impl.system.Job;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Job.class, sort = {@SortField(field = JobLov_Ds.F_NAME)})
public class JobLov_Ds extends AbstractTypeNTLov_Ds<Job> {

	public static final String ALIAS = "ad_JobLov_Ds";

	/**
	 * Default constructor
	 */
	public JobLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public JobLov_Ds(Job e) {
		super(e);
	}
}
