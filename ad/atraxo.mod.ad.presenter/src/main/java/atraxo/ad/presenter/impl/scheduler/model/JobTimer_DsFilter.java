/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.scheduler.model;
import atraxo.ad.presenter.impl.scheduler.model.JobTimer_Ds;
import java.util.Date;

/**
 * Helper filter object to run query by example with range values.
 *
 * Generated code. Do not modify in this file.
 */
public class JobTimer_DsFilter extends JobTimer_Ds {
	private Date startTime_From;
	private Date startTime_To;
	private Date endTime_From;
	private Date endTime_To;

	public Date getStartTime_From() {
		return this.startTime_From;
	}
	public Date getStartTime_To() {
		return this.startTime_To;
	}

	public void setStartTime_From(Date startTime_From) {
		this.startTime_From = startTime_From;
	}
	public void setStartTime_To(Date startTime_To) {
		this.startTime_To = startTime_To;
	}

	public Date getEndTime_From() {
		return this.endTime_From;
	}
	public Date getEndTime_To() {
		return this.endTime_To;
	}

	public void setEndTime_From(Date endTime_From) {
		this.endTime_From = endTime_From;
	}
	public void setEndTime_To(Date endTime_To) {
		this.endTime_To = endTime_To;
	}
}
