/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractType_Ds;
import atraxo.ad.domain.impl.security.AccessControl;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = AccessControl.class, sort = {@SortField(field = AccessControl_Ds.F_NAME)})
public class AccessControl_Ds extends AbstractType_Ds<AccessControl> {

	public static final String ALIAS = "ad_AccessControl_Ds";

	/**
	 * Default constructor
	 */
	public AccessControl_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AccessControl_Ds(AccessControl e) {
		super(e);
	}
}
