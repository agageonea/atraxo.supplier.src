/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeWithCodeLov_Ds;
import atraxo.ad.domain.impl.security.Role;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Role.class, sort = {@SortField(field = RoleLov_Ds.F_CODE)})
public class RoleLov_Ds extends AbstractTypeWithCodeLov_Ds<Role> {

	public static final String ALIAS = "ad_RoleLov_Ds";

	/**
	 * Default constructor
	 */
	public RoleLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public RoleLov_Ds(Role e) {
		super(e);
	}
}
