package atraxo.ad.presenter.ext.externalReport.delegate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.ad.business.api.externalReport.ISourceUploadHistoryService;
import atraxo.ad.domain.impl.externalReport.SourceUploadHistory;
import atraxo.ad.presenter.impl.externalReport.model.ExternalReport_Ds;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author vhojda
 */
public class SourceUploadHistory_Pd extends AbstractPresenterDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(SourceUploadHistory_Pd.class);

	/**
	 * @param ds
	 * @throws Exception
	 */
	public void empty(ExternalReport_Ds ds) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START empty()");
		}
		ISourceUploadHistoryService service = (ISourceUploadHistoryService) this.findEntityService(SourceUploadHistory.class);
		service.empty(ds.getId());

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END empty()");
		}
	}

}
