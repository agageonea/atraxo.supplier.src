package atraxo.ad.presenter.ext;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.ad.domain.impl.security.Role;
import seava.j4e.api.annotation.JobParam;
import seava.j4e.presenter.service.job.AbstractPresenterJob;

public class DummyPresenterJob extends AbstractPresenterJob {

	private static final Logger LOG = LoggerFactory.getLogger(DummyPresenterJob.class);

	@JobParam
	private String stringParam;

	@JobParam
	private int intParam;

	@JobParam
	private Integer integerParam;

	@JobParam
	private long longParam;

	@JobParam
	private Long pLongParam;

	@JobParam
	private boolean booleanParam;

	@JobParam
	private Boolean pBooleanParam;

	@JobParam
	private Date dateParam;

	@Override
	protected void onExecute() throws Exception {

		LOG.info("======================= DummyPresenterJob.onExecute ================================");

		Map<String, Object> params = new HashMap<>();
		params.put("code", "ROLE_DNET_USER");

		String s = this.findEntityService(Role.class).findByUk(Role.NQ_FIND_BY_CODE, params).getName();
		LOG.info(s);

		this.getPersistableLog().info("This is an `info` message. Executing DummyPresenterJob.");
		this.getPersistableLog().warning("This is a `warning` message. First role found is " + s + ". ");
	}

	public String getStringParam() {
		return this.stringParam;
	}

	public void setStringParam(String stringParam) {
		this.stringParam = stringParam;
	}

	public int getIntParam() {
		return this.intParam;
	}

	public void setIntParam(int intParam) {
		this.intParam = intParam;
	}

	public Integer getIntegerParam() {
		return this.integerParam;
	}

	public void setIntegerParam(Integer integerParam) {
		this.integerParam = integerParam;
	}

	public long getLongParam() {
		return this.longParam;
	}

	public void setLongParam(long longParam) {
		this.longParam = longParam;
	}

	public Long getpLongParam() {
		return this.pLongParam;
	}

	public void setpLongParam(Long pLongParam) {
		this.pLongParam = pLongParam;
	}

	public boolean isBooleanParam() {
		return this.booleanParam;
	}

	public void setBooleanParam(boolean booleanParam) {
		this.booleanParam = booleanParam;
	}

	public Boolean getpBooleanParam() {
		return this.pBooleanParam;
	}

	public void setpBooleanParam(Boolean pBooleanParam) {
		this.pBooleanParam = pBooleanParam;
	}

	public Date getDateParam() {
		return this.dateParam;
	}

	public void setDateParam(Date dateParam) {
		this.dateParam = dateParam;
	}

}