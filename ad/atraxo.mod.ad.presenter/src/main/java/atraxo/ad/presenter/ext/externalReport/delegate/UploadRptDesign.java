package atraxo.ad.presenter.ext.externalReport.delegate;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.ad.business.api.externalReport.IExternalReportService;
import atraxo.ad.business.api.externalReport.ISourceUploadHistoryService;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.SourceUploadHistory;
import seava.j4e.api.descriptor.IUploadedFileDescriptor;
import seava.j4e.api.service.IFileUploadService;
import seava.j4e.api.service.report.IReportService;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class UploadRptDesign extends AbstractPresenterDelegate implements IFileUploadService {

	private static final String NAME = "name";

	private List<String> paramNames;

	private static final Logger LOGGER = LoggerFactory.getLogger(UploadRptDesign.class);

	@Override
	public List<String> getParamNames() {
		this.paramNames = new ArrayList<>();
		this.paramNames.add("reportId");
		this.paramNames.add(NAME);
		this.paramNames.add("notes");
		return this.paramNames;
	}

	@Override
	public Map<String, Object> execute(IUploadedFileDescriptor fileDescriptor, InputStream inputStream, Map<String, String> params) throws Exception {
		LOGGER.info("Start upload file");
		byte[] file = IOUtils.toByteArray(inputStream);
		Map<String, Object> result = new HashMap<>();
		IReportService reportService = this.getApplicationContext().getBean("reportService", IReportService.class);
		ISourceUploadHistoryService sourceService = (ISourceUploadHistoryService) this.findEntityService(SourceUploadHistory.class);
		IExternalReportService externalReportService = (IExternalReportService) this.findEntityService(ExternalReport.class);
		String fileName = fileDescriptor.getOriginalName();
		if (reportService.uploadReport(file, fileName)) {
			SourceUploadHistory sourceHistory = new SourceUploadHistory();
			sourceHistory.setActive(true);
			sourceHistory.setFileName(fileName);
			sourceHistory.setName(params.get(NAME));
			ExternalReport er = externalReportService.findById(params.get("reportId"));
			sourceHistory.setExternalReport(er);
			sourceHistory.setUploadDate(new Date());
			sourceHistory.setNotes(params.get("notes"));
			sourceHistory.setUploader(Session.user.get().getCode());
			sourceHistory.setCreatedBy(Session.user.get().getCode());
			sourceService.insert(sourceHistory);
			externalReportService.parseAndSaveParameters(er, file, fileName);
		}
		return result;
	}

}
