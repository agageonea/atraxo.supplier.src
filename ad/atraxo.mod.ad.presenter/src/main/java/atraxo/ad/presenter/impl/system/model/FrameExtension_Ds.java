/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.system.FrameExtension;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FrameExtension.class)
public class FrameExtension_Ds extends AbstractAuditable_Ds<FrameExtension> {

	public static final String ALIAS = "ad_FrameExtension_Ds";

	public static final String F_FRAME = "frame";
	public static final String F_SEQUENCENO = "sequenceNo";
	public static final String F_FILELOCATION = "fileLocation";
	public static final String F_RELATIVEPATH = "relativePath";

	@DsField
	private String frame;

	@DsField
	private Integer sequenceNo;

	@DsField
	private String fileLocation;

	@DsField
	private Boolean relativePath;

	/**
	 * Default constructor
	 */
	public FrameExtension_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FrameExtension_Ds(FrameExtension e) {
		super(e);
	}

	public String getFrame() {
		return this.frame;
	}

	public void setFrame(String frame) {
		this.frame = frame;
	}

	public Integer getSequenceNo() {
		return this.sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getFileLocation() {
		return this.fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public Boolean getRelativePath() {
		return this.relativePath;
	}

	public void setRelativePath(Boolean relativePath) {
		this.relativePath = relativePath;
	}
}
