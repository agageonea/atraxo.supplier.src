package atraxo.ad.presenter.ext.attachment.service;

import java.util.List;

import atraxo.ad.business.api.attachment.IAttachmentService;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.presenter.impl.attachment.model.Attachment_Ds;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class Attachment_DsService extends AbstractEntityDsService<Attachment_Ds, Attachment_Ds, Object, Attachment>
		implements IDsService<Attachment_Ds, Attachment_Ds, Object> {

	@Override
	protected void preDelete(List<Object> ids) throws Exception {
		super.preDelete(ids);
		IAttachmentService srv = (IAttachmentService) this.getEntityService();
		for (Object id : ids) {
			srv.deleteFromS3((String) id);
		}
	}
}
