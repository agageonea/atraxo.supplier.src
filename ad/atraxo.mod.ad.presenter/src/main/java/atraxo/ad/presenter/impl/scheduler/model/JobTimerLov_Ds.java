/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.scheduler.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeLov_Ds;
import atraxo.ad.domain.impl.scheduler.JobTimer;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = JobTimer.class, sort = {@SortField(field = JobTimerLov_Ds.F_NAME)})
public class JobTimerLov_Ds extends AbstractTypeLov_Ds<JobTimer> {

	public static final String ALIAS = "ad_JobTimerLov_Ds";

	public static final String F_JOBCONTEXTID = "jobContextId";
	public static final String F_JOBCONTEXT = "jobContext";
	public static final String F_JOBNAME = "jobName";

	@DsField(join = "left", path = "jobContext.id")
	private String jobContextId;

	@DsField(join = "left", path = "jobContext.name")
	private String jobContext;

	@DsField(join = "left", path = "jobContext.jobName")
	private String jobName;

	/**
	 * Default constructor
	 */
	public JobTimerLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public JobTimerLov_Ds(JobTimer e) {
		super(e);
	}

	public String getJobContextId() {
		return this.jobContextId;
	}

	public void setJobContextId(String jobContextId) {
		this.jobContextId = jobContextId;
	}

	public String getJobContext() {
		return this.jobContext;
	}

	public void setJobContext(String jobContext) {
		this.jobContext = jobContext;
	}

	public String getJobName() {
		return this.jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
}
