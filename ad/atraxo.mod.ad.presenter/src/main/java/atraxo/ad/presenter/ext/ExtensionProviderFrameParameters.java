package atraxo.ad.presenter.ext;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import seava.j4e.api.extensions.IExtensionContentProvider;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterBaseService;
import atraxo.ad.business.api.businessArea.IBusinessAreaService;
import atraxo.ad.business.api.security.IMenuItemService;
import atraxo.ad.domain.impl.businessArea.BusinessArea;
import atraxo.ad.domain.impl.security.MenuItem;
import atraxo.ad.domain.impl.userFields.UserFields;

/**
 * ExtensionProviderFrameParameters
 */
public class ExtensionProviderFrameParameters extends AbstractPresenterBaseService implements IExtensionContentProvider {

	protected static final Logger logger = LoggerFactory.getLogger(ExtensionProviderFrameParameters.class);

	@Override
	public String getContent(String targetName) throws Exception {
		StringBuilder sb = new StringBuilder();
		List<MenuItem> menuItems = this.listMenuItems(targetName);
		this.addFrameParameters(sb, menuItems);
		this.addDsForUserDefinedFields(sb);
		return sb.toString();
	}

	private void addFrameParameters(StringBuilder sb, List<MenuItem> menuItems) {
		sb.append("__FRAMEPARAMETERS__ = ");
		try {
			JSONArray jsonArray = new JSONArray();

			for (MenuItem mi : menuItems) {
				JSONObject json = new JSONObject();
				json.put("glyph", mi.getGlyph());
				json.put("title", mi.getTitle());
				json.put("name", mi.getName());
				jsonArray.put(json);
			}

			sb.append(jsonArray.toString());
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Error occured during __FRAMEPARAMETERS__ JSON construction.", e);
			}
			sb.append("[]");
		}
		sb.append(";");
	}

	private List<MenuItem> listMenuItems(String targetFrame) {
		try {
			IMenuItemService srv = (IMenuItemService) this.findEntityService(MenuItem.class);
			EntityManager em = srv.getEntityManager();

			String getMenuItems = "SELECT e FROM " + MenuItem.class.getSimpleName() + " e WHERE e.frame = :frame AND e.clientId=:clientId";
			TypedQuery<MenuItem> menuItemQuery = em.createQuery(getMenuItems, MenuItem.class);

			menuItemQuery.setParameter("clientId", Session.user.get().getClientId());
			menuItemQuery.setParameter("frame", targetFrame);

			return menuItemQuery.getResultList();
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Error occured during __FRAMEPARAMETERS__ MenuItemList construction.", e);
			}
		}
		return new ArrayList<>();
	}

	private void addDsForUserDefinedFields(StringBuilder sb) {
		sb.append("__DS_FOR_USER_DEFINED_FIELDS__ = ");
		try {
			JSONArray jsonArray = new JSONArray();

			/*
			 * SELECT ba.ds_name FROM `auto_fuelticket`.`ad_business_areas` AS ba WHERE ba.clientid = '06A246A436EE449296623587746AB650' AND EXISTS
			 * (SELECT NAME FROM `auto_fuelticket`.`ad_user_fields` AS uf WHERE uf.business_area_id = ba.id)
			 */
			StringBuilder hql = new StringBuilder();
			hql.append("select ba from ").append(BusinessArea.class.getSimpleName()).append(" ba");
			hql.append(" where ba.clientId=:clientId and exists").append("(").append("select uf.name from ").append(UserFields.class.getSimpleName())
					.append(" uf ").append(" where uf.businessArea.id=ba.id").append(")");

			IBusinessAreaService businessAreaService = (IBusinessAreaService) this.findEntityService(BusinessArea.class);
			List<BusinessArea> list = businessAreaService.getEntityManager().createQuery(hql.toString(), BusinessArea.class)
					.setParameter("clientId", Session.user.get().getClientId()).getResultList();

			for (BusinessArea ba : list) {
				JSONObject json = new JSONObject();
				json.put("dsName", ba.getDsName());
				jsonArray.put(json);
			}

			sb.append(jsonArray.toString());
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Error occured during __DS_FOR_USER_DEFINED_FIELDS__ construction. User defined fields tab will not be created.", e);
			}
			sb.append("[]");
		}
		sb.append(";");
	}
}
