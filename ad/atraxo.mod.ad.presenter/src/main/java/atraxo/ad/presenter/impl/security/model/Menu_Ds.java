/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractType_Ds;
import atraxo.ad.domain.impl.ad.TMenuTag;
import atraxo.ad.domain.impl.security.Menu;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Menu.class, sort = {@SortField(field = Menu_Ds.F_SEQUENCENO)})
public class Menu_Ds extends AbstractType_Ds<Menu> {

	public static final String ALIAS = "ad_Menu_Ds";

	public static final String F_SEQUENCENO = "sequenceNo";
	public static final String F_TITLE = "title";
	public static final String F_TAG = "tag";
	public static final String F_GLYPH = "glyph";

	@DsField
	private Integer sequenceNo;

	@DsField
	private String title;

	@DsField
	private TMenuTag tag;

	@DsField
	private String glyph;

	/**
	 * Default constructor
	 */
	public Menu_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Menu_Ds(Menu e) {
		super(e);
	}

	public Integer getSequenceNo() {
		return this.sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public TMenuTag getTag() {
		return this.tag;
	}

	public void setTag(TMenuTag tag) {
		this.tag = tag;
	}

	public String getGlyph() {
		return this.glyph;
	}

	public void setGlyph(String glyph) {
		this.glyph = glyph;
	}
}
