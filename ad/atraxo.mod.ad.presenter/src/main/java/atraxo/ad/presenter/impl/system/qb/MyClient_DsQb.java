/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.qb;

import atraxo.ad.presenter.impl.system.model.MyClient_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class MyClient_DsQb
		extends
			QueryBuilderWithJpql<MyClient_Ds, MyClient_Ds, Object> {

	@Override
	public void setFilter(MyClient_Ds filter) {
		filter.setId(Session.user.get().getClientId());
		super.setFilter(filter);
	}
}
