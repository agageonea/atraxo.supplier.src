/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.ext.externalReport.cnv;

import javax.persistence.EntityManager;

import atraxo.ad.domain.impl.ad.LastExecutionStatus;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.presenter.impl.externalReport.model.ExternalReport_Ds;
import seava.j4e.api.action.result.IDsConverter;
import seava.j4e.presenter.converter.AbstractDsConverter;

/**
 * Custom converter ExternalReport_DsCnv
 */
public class ExternalReport_DsCnv extends AbstractDsConverter<ExternalReport_Ds, ExternalReport>
		implements IDsConverter<ExternalReport_Ds, ExternalReport> {

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.presenter.converter.AbstractDsConverter#modelToEntity(java.lang.Object, java.lang.Object, boolean,
	 * javax.persistence.EntityManager)
	 */
	@Override
	public void modelToEntity(ExternalReport_Ds m, ExternalReport e, boolean isInsert, EntityManager em) throws Exception {
		super.modelToEntity(m, e, isInsert, em);
		if (e.getLastExecutionStatus() == null) {
			e.setLastExecutionStatus(LastExecutionStatus._EMPTY_);
		}
	}
}
