/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractTypeNT_Ds;
import atraxo.ad.domain.impl.system.JobParam;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = JobParam.class, sort = {@SortField(field = JobParam_Ds.F_NAME)})
public class JobParam_Ds extends AbstractTypeNT_Ds<JobParam> {

	public static final String ALIAS = "ad_JobParam_Ds";

	public static final String F_DATATYPE = "dataType";
	public static final String F_JOBID = "jobId";
	public static final String F_JOB = "job";

	@DsField
	private String dataType;

	@DsField(join = "left", path = "job.id")
	private String jobId;

	@DsField(join = "left", path = "job.name")
	private String job;

	/**
	 * Default constructor
	 */
	public JobParam_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public JobParam_Ds(JobParam e) {
		super(e);
	}

	public String getDataType() {
		return this.dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getJobId() {
		return this.jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getJob() {
		return this.job;
	}

	public void setJob(String job) {
		this.job = job;
	}
}
