/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeLov_Ds;
import atraxo.ad.domain.impl.security.Menu;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Menu.class, sort = {@SortField(field = MenuLov_Ds.F_NAME)})
public class MenuLov_Ds extends AbstractTypeLov_Ds<Menu> {

	public static final String ALIAS = "ad_MenuLov_Ds";

	public static final String F_TITLE = "title";

	@DsField
	private String title;

	/**
	 * Default constructor
	 */
	public MenuLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public MenuLov_Ds(Menu e) {
		super(e);
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
