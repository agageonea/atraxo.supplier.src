/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractType_Ds;
import atraxo.ad.domain.impl.security.MenuItem;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = MenuItem.class, sort = {@SortField(field = MenuItemRtLov_Ds.F_SEQUENCENO)})
public class MenuItemRtLov_Ds extends AbstractType_Ds<MenuItem> {

	public static final String ALIAS = "ad_MenuItemRtLov_Ds";

	public static final String F_TITLE = "title";
	public static final String F_SEQUENCENO = "sequenceNo";
	public static final String F_TEXT = "text";
	public static final String F_FRAME = "frame";
	public static final String F_BUNDLE = "bundle";
	public static final String F_LEAF = "leaf";
	public static final String F_ICONURL = "iconUrl";
	public static final String F_SEPARATORBEFORE = "separatorBefore";
	public static final String F_SEPARATORAFTER = "separatorAfter";
	public static final String F_GLYPH = "glyph";
	public static final String F_MENUITEMID = "menuItemId";
	public static final String F_MENUITEM = "menuItem";
	public static final String F_MENUID = "menuId";
	public static final String F_MENU = "menu";

	@DsField
	private String title;

	@DsField
	private Integer sequenceNo;

	@DsField(path = "title")
	private String text;

	@DsField
	private String frame;

	@DsField
	private String bundle;

	@DsField(fetch = false, path = "leafNode")
	private Boolean leaf;

	@DsField
	private String iconUrl;

	@DsField
	private Boolean separatorBefore;

	@DsField
	private Boolean separatorAfter;

	@DsField
	private String glyph;

	@DsField(join = "left", path = "menuItem.id")
	private String menuItemId;

	@DsField(join = "left", path = "menuItem.name")
	private String menuItem;

	@DsField(join = "left", path = "menu.id")
	private String menuId;

	@DsField(join = "left", path = "menu.name")
	private String menu;

	/**
	 * Default constructor
	 */
	public MenuItemRtLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public MenuItemRtLov_Ds(MenuItem e) {
		super(e);
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getSequenceNo() {
		return this.sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getFrame() {
		return this.frame;
	}

	public void setFrame(String frame) {
		this.frame = frame;
	}

	public String getBundle() {
		return this.bundle;
	}

	public void setBundle(String bundle) {
		this.bundle = bundle;
	}

	public Boolean getLeaf() {
		return this.leaf;
	}

	public void setLeaf(Boolean leaf) {
		this.leaf = leaf;
	}

	public String getIconUrl() {
		return this.iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public Boolean getSeparatorBefore() {
		return this.separatorBefore;
	}

	public void setSeparatorBefore(Boolean separatorBefore) {
		this.separatorBefore = separatorBefore;
	}

	public Boolean getSeparatorAfter() {
		return this.separatorAfter;
	}

	public void setSeparatorAfter(Boolean separatorAfter) {
		this.separatorAfter = separatorAfter;
	}

	public String getGlyph() {
		return this.glyph;
	}

	public void setGlyph(String glyph) {
		this.glyph = glyph;
	}

	public String getMenuItemId() {
		return this.menuItemId;
	}

	public void setMenuItemId(String menuItemId) {
		this.menuItemId = menuItemId;
	}

	public String getMenuItem() {
		return this.menuItem;
	}

	public void setMenuItem(String menuItem) {
		this.menuItem = menuItem;
	}

	public String getMenuId() {
		return this.menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getMenu() {
		return this.menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}
}
