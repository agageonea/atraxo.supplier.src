package atraxo.ad.presenter.ext.security.delegate;

import atraxo.ad.business.api.security.IUserService;
import atraxo.ad.domain.impl.security.User;
import atraxo.ad.presenter.impl.security.model.User_Ds;
import atraxo.ad.presenter.impl.security.model.User_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class User_Pd extends AbstractPresenterDelegate {

	public void changePassword(User_Ds ds, User_DsParam params) throws Exception {

		String newPassword = params.getNewPassword();
		String confPassword = params.getConfirmPassword();

		if (newPassword == null || "".equals(newPassword)) {
			throw new Exception("Password must not be empty!");
		}

		if (!newPassword.equals(confPassword)) {
			throw new Exception("New password is not confirmed correctly !");
		}

		((IUserService) this.findEntityService(User.class)).doChangePassword(ds.getId(), newPassword);
	}

}
