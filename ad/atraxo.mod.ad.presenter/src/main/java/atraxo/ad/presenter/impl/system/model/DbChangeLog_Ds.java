/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.ad.domain.impl.system.DbChangeLog;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;
import seava.j4e.api.model.IModelWithId;
import seava.j4e.presenter.model.AbstractDsModel;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DbChangeLog.class, sort = {@SortField(field = DbChangeLog_Ds.F_ORDEREXECUTED, desc = true)})
public class DbChangeLog_Ds extends AbstractDsModel<DbChangeLog>
		implements
			IModelWithId<String> {

	public static final String ALIAS = "ad_DbChangeLog_Ds";

	public static final String F_ID = "id";
	public static final String F_TXID = "txid";
	public static final String F_AUTHOR = "author";
	public static final String F_FILENAME = "filename";
	public static final String F_DATEEXECUTED = "dateExecuted";
	public static final String F_ORDEREXECUTED = "orderExecuted";
	public static final String F_MD5SUM = "md5sum";
	public static final String F_DESCRIPTION = "description";
	public static final String F_COMMENTS = "comments";
	public static final String F_TAG = "tag";
	public static final String F_LIQUIBASE = "liquibase";

	@DsField(fetch = false)
	private String id;

	@DsField
	private String txid;

	@DsField
	private String author;

	@DsField
	private String filename;

	@DsField
	private Date dateExecuted;

	@DsField
	private Integer orderExecuted;

	@DsField
	private String md5sum;

	@DsField
	private String description;

	@DsField
	private String comments;

	@DsField
	private String tag;

	@DsField
	private String liquibase;

	/**
	 * Default constructor
	 */
	public DbChangeLog_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DbChangeLog_Ds(DbChangeLog e) {
		super(e);
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTxid() {
		return this.txid;
	}

	public void setTxid(String txid) {
		this.txid = txid;
	}

	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getFilename() {
		return this.filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Date getDateExecuted() {
		return this.dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public Integer getOrderExecuted() {
		return this.orderExecuted;
	}

	public void setOrderExecuted(Integer orderExecuted) {
		this.orderExecuted = orderExecuted;
	}

	public String getMd5sum() {
		return this.md5sum;
	}

	public void setMd5sum(String md5sum) {
		this.md5sum = md5sum;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getTag() {
		return this.tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getLiquibase() {
		return this.liquibase;
	}

	public void setLiquibase(String liquibase) {
		this.liquibase = liquibase;
	}
}
