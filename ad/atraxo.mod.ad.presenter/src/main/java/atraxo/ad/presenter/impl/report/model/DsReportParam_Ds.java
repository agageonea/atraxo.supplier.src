/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.report.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.report.DsReport;
import atraxo.ad.domain.impl.report.DsReportParam;
import atraxo.ad.domain.impl.report.ReportParam;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DsReportParam.class)
@RefLookups({
		@RefLookup(refId = DsReportParam_Ds.F_PARAMID, namedQuery = ReportParam.NQ_FIND_BY_NAME_PRIMITIVE, params = {
				@Param(name = "reportId", field = DsReportParam_Ds.F_REPORTID),
				@Param(name = "name", field = DsReportParam_Ds.F_PARAM)}),
		@RefLookup(refId = DsReportParam_Ds.F_DSREPORTID, namedQuery = DsReport.NQ_FIND_BY_REP_DS_PRIMITIVE, params = {
				@Param(name = "reportId", field = DsReportParam_Ds.F_REPORTID),
				@Param(name = "dataSource", field = DsReportParam_Ds.F_DATASOURCE)})})
public class DsReportParam_Ds extends AbstractAuditable_Ds<DsReportParam> {

	public static final String ALIAS = "ad_DsReportParam_Ds";

	public static final String F_DSREPORTID = "dsReportId";
	public static final String F_DATASOURCE = "dataSource";
	public static final String F_REPORTID = "reportId";
	public static final String F_REPORTCODE = "reportCode";
	public static final String F_PARAMID = "paramId";
	public static final String F_PARAM = "param";
	public static final String F_PARAMTITLE = "paramTitle";
	public static final String F_DSFIELD = "dsField";
	public static final String F_STATICVALUE = "staticValue";

	@DsField(join = "left", path = "dsReport.id")
	private String dsReportId;

	@DsField(join = "left", path = "dsReport.dataSource")
	private String dataSource;

	@DsField(join = "left", path = "dsReport.report.id")
	private String reportId;

	@DsField(join = "left", path = "dsReport.report.code")
	private String reportCode;

	@DsField(join = "left", path = "reportParam.id")
	private String paramId;

	@DsField(join = "left", path = "reportParam.name")
	private String param;

	@DsField(join = "left", path = "reportParam.title")
	private String paramTitle;

	@DsField
	private String dsField;

	@DsField
	private String staticValue;

	/**
	 * Default constructor
	 */
	public DsReportParam_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DsReportParam_Ds(DsReportParam e) {
		super(e);
	}

	public String getDsReportId() {
		return this.dsReportId;
	}

	public void setDsReportId(String dsReportId) {
		this.dsReportId = dsReportId;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getReportId() {
		return this.reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getReportCode() {
		return this.reportCode;
	}

	public void setReportCode(String reportCode) {
		this.reportCode = reportCode;
	}

	public String getParamId() {
		return this.paramId;
	}

	public void setParamId(String paramId) {
		this.paramId = paramId;
	}

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getParamTitle() {
		return this.paramTitle;
	}

	public void setParamTitle(String paramTitle) {
		this.paramTitle = paramTitle;
	}

	public String getDsField() {
		return this.dsField;
	}

	public void setDsField(String dsField) {
		this.dsField = dsField;
	}

	public String getStaticValue() {
		return this.staticValue;
	}

	public void setStaticValue(String staticValue) {
		this.staticValue = staticValue;
	}
}
