package atraxo.ad.presenter.ext.attachment.delegate;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import atraxo.ad.business.api.attachment.IAttachmentService;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import seava.j4e.api.descriptor.IUploadedFileDescriptor;
import seava.j4e.api.service.IFileUploadService;
import seava.j4e.business.service.storage.StorageService;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class UploadAttachment extends AbstractPresenterDelegate implements IFileUploadService {

	public static final String KEY_ID = "id";
	public static final String KEY_NAME = "name";
	public static final String KEY_NOTES = "notes";
	public static final String KEY_TYPEID = "typeId";
	public static final String KEY_CATEGORY = "category";
	public static final String KEY_TARGETALIAS = "targetAlias";
	public static final String KEY_TARGETREFID = "targetRefid";
	public static final String KEY_TARGETTYPE = "targetType";

	private List<String> paramNames;

	public UploadAttachment() {
		super();
		this.paramNames = new ArrayList<>();
		this.paramNames.add(KEY_ID);
		this.paramNames.add(KEY_NAME);
		this.paramNames.add(KEY_NOTES);
		this.paramNames.add(KEY_TYPEID);
		this.paramNames.add(KEY_CATEGORY);

		this.paramNames.add(KEY_TARGETALIAS);
		this.paramNames.add(KEY_TARGETREFID);
		this.paramNames.add(KEY_TARGETTYPE);

	}

	@Override
	public List<String> getParamNames() {
		return this.paramNames;
	}

	@Override
	public Map<String, Object> execute(IUploadedFileDescriptor fileDescriptor, InputStream inputStream, Map<String, String> params) throws Exception {

		String id = params.get(KEY_ID);
		String name = params.get(KEY_NAME);
		String notes = params.get(KEY_NOTES);
		String typeId = params.get(KEY_TYPEID);

		String targetAlias = params.get(KEY_TARGETALIAS);
		String targetRefid = params.get(KEY_TARGETREFID);
		String targetType = params.get(KEY_TARGETTYPE);

		Map<String, Object> result = new HashMap<>();

		StorageService storageService = (StorageService) this.getApplicationContext().getBean("storageService");
		IAttachmentService srv = (IAttachmentService) this.findEntityService(Attachment.class);
		AttachmentType attchType = srv.findById(typeId, AttachmentType.class);
		String extension = fileDescriptor.getOriginalName().substring(fileDescriptor.getOriginalName().lastIndexOf('.') + 1);

		if (id == null || "".equals(id)) {
			String refId = UUID.randomUUID().toString().toUpperCase();
			storageService.uploadFile(inputStream, refId, fileDescriptor.getOriginalName(), Attachment.class.getSimpleName());

			Attachment a = new Attachment();
			a.setType(attchType);
			a.setFileName(fileDescriptor.getOriginalName());
			a.setName(name);
			a.setNotes(notes);
			a.setActive(true);
			a.setTargetAlias(targetAlias);
			a.setTargetRefid(targetRefid);
			a.setTargetType(targetType);
			a.setContentType(extension);
			a.setRefid(refId);
			srv.insert(a);
		} else {
			Attachment a = srv.findById(id);
			storageService.uploadFile(inputStream, a.getRefid(), fileDescriptor.getOriginalName(), Attachment.class.getSimpleName());
		}
		return result;
	}
}
