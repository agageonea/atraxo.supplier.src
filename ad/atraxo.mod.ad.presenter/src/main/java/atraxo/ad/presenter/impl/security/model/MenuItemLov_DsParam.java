/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

/**
 * Generated code. Do not modify in this file.
 */
public class MenuItemLov_DsParam {

	public static final String f_foldersOnly = "foldersOnly";
	public static final String f_framesOnly = "framesOnly";

	private Boolean foldersOnly;

	private Boolean framesOnly;

	public Boolean getFoldersOnly() {
		return this.foldersOnly;
	}

	public void setFoldersOnly(Boolean foldersOnly) {
		this.foldersOnly = foldersOnly;
	}

	public Boolean getFramesOnly() {
		return this.framesOnly;
	}

	public void setFramesOnly(Boolean framesOnly) {
		this.framesOnly = framesOnly;
	}
}
