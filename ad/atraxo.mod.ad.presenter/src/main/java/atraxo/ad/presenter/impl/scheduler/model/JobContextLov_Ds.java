/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.scheduler.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeLov_Ds;
import atraxo.ad.domain.impl.scheduler.JobContext;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = JobContext.class, sort = {@SortField(field = JobContextLov_Ds.F_NAME)})
public class JobContextLov_Ds extends AbstractTypeLov_Ds<JobContext> {

	public static final String ALIAS = "ad_JobContextLov_Ds";

	public static final String F_JOBNAME = "jobName";

	@DsField
	private String jobName;

	/**
	 * Default constructor
	 */
	public JobContextLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public JobContextLov_Ds(JobContext e) {
		super(e);
	}

	public String getJobName() {
		return this.jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
}
