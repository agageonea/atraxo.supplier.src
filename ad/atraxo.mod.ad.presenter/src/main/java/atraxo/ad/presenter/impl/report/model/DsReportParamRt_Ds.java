/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.report.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.ad.TDataType;
import atraxo.ad.domain.impl.report.DsReportParam;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DsReportParam.class)
public class DsReportParamRt_Ds extends AbstractAuditable_Ds<DsReportParam> {

	public static final String ALIAS = "ad_DsReportParamRt_Ds";

	public static final String F_DSREPORTID = "dsReportId";
	public static final String F_DATASOURCE = "dataSource";
	public static final String F_REPORTID = "reportId";
	public static final String F_REPORTCODE = "reportCode";
	public static final String F_PARAMID = "paramId";
	public static final String F_PARAM = "param";
	public static final String F_PARAMDATATYPE = "paramDataType";
	public static final String F_PARAMMANDATORY = "paramMandatory";
	public static final String F_PARAMDEFAULTVALUE = "paramDefaultValue";
	public static final String F_PARAMLISTOFVALUES = "paramListOfValues";
	public static final String F_PARAMNOEDIT = "paramNoEdit";
	public static final String F_DSFIELD = "dsField";
	public static final String F_STATICVALUE = "staticValue";

	@DsField(join = "left", path = "dsReport.id")
	private String dsReportId;

	@DsField(join = "left", path = "dsReport.dataSource")
	private String dataSource;

	@DsField(join = "left", path = "dsReport.report.id")
	private String reportId;

	@DsField(join = "left", path = "dsReport.report.code")
	private String reportCode;

	@DsField(join = "left", path = "reportParam.id")
	private String paramId;

	@DsField(join = "left", path = "reportParam.name")
	private String param;

	@DsField(join = "left", path = "reportParam.dataType")
	private TDataType paramDataType;

	@DsField(join = "left", path = "reportParam.mandatory")
	private Boolean paramMandatory;

	@DsField(join = "left", path = "reportParam.defaultValue")
	private String paramDefaultValue;

	@DsField(join = "left", path = "reportParam.listOfValues")
	private String paramListOfValues;

	@DsField(join = "left", path = "reportParam.noEdit")
	private Boolean paramNoEdit;

	@DsField
	private String dsField;

	@DsField
	private String staticValue;

	/**
	 * Default constructor
	 */
	public DsReportParamRt_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DsReportParamRt_Ds(DsReportParam e) {
		super(e);
	}

	public String getDsReportId() {
		return this.dsReportId;
	}

	public void setDsReportId(String dsReportId) {
		this.dsReportId = dsReportId;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getReportId() {
		return this.reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getReportCode() {
		return this.reportCode;
	}

	public void setReportCode(String reportCode) {
		this.reportCode = reportCode;
	}

	public String getParamId() {
		return this.paramId;
	}

	public void setParamId(String paramId) {
		this.paramId = paramId;
	}

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public TDataType getParamDataType() {
		return this.paramDataType;
	}

	public void setParamDataType(TDataType paramDataType) {
		this.paramDataType = paramDataType;
	}

	public Boolean getParamMandatory() {
		return this.paramMandatory;
	}

	public void setParamMandatory(Boolean paramMandatory) {
		this.paramMandatory = paramMandatory;
	}

	public String getParamDefaultValue() {
		return this.paramDefaultValue;
	}

	public void setParamDefaultValue(String paramDefaultValue) {
		this.paramDefaultValue = paramDefaultValue;
	}

	public String getParamListOfValues() {
		return this.paramListOfValues;
	}

	public void setParamListOfValues(String paramListOfValues) {
		this.paramListOfValues = paramListOfValues;
	}

	public Boolean getParamNoEdit() {
		return this.paramNoEdit;
	}

	public void setParamNoEdit(Boolean paramNoEdit) {
		this.paramNoEdit = paramNoEdit;
	}

	public String getDsField() {
		return this.dsField;
	}

	public void setDsField(String dsField) {
		this.dsField = dsField;
	}

	public String getStaticValue() {
		return this.staticValue;
	}

	public void setStaticValue(String staticValue) {
		this.staticValue = staticValue;
	}
}
