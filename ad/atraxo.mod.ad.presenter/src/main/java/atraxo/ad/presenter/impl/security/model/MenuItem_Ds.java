/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractType_Ds;
import atraxo.ad.domain.impl.security.Menu;
import atraxo.ad.domain.impl.security.MenuItem;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = MenuItem.class, sort = {@SortField(field = MenuItem_Ds.F_SEQUENCENO)})
@RefLookups({
		@RefLookup(refId = MenuItem_Ds.F_MENUITEMID, namedQuery = MenuItem.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = MenuItem_Ds.F_MENUITEM)}),
		@RefLookup(refId = MenuItem_Ds.F_MENUID, namedQuery = Menu.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = MenuItem_Ds.F_MENU)})})
public class MenuItem_Ds extends AbstractType_Ds<MenuItem> {

	public static final String ALIAS = "ad_MenuItem_Ds";

	public static final String F_SEQUENCENO = "sequenceNo";
	public static final String F_TITLE = "title";
	public static final String F_FRAME = "frame";
	public static final String F_BUNDLE = "bundle";
	public static final String F_ICONURL = "iconUrl";
	public static final String F_SEPARATORBEFORE = "separatorBefore";
	public static final String F_SEPARATORAFTER = "separatorAfter";
	public static final String F_GLYPH = "glyph";
	public static final String F_MENUID = "menuId";
	public static final String F_MENU = "menu";
	public static final String F_MENUITEMID = "menuItemId";
	public static final String F_MENUITEM = "menuItem";

	@DsField
	private Integer sequenceNo;

	@DsField
	private String title;

	@DsField
	private String frame;

	@DsField
	private String bundle;

	@DsField
	private String iconUrl;

	@DsField
	private Boolean separatorBefore;

	@DsField
	private Boolean separatorAfter;

	@DsField
	private String glyph;

	@DsField(join = "left", path = "menu.id")
	private String menuId;

	@DsField(join = "left", path = "menu.name")
	private String menu;

	@DsField(join = "left", path = "menuItem.id")
	private String menuItemId;

	@DsField(join = "left", path = "menuItem.name")
	private String menuItem;

	/**
	 * Default constructor
	 */
	public MenuItem_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public MenuItem_Ds(MenuItem e) {
		super(e);
	}

	public Integer getSequenceNo() {
		return this.sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFrame() {
		return this.frame;
	}

	public void setFrame(String frame) {
		this.frame = frame;
	}

	public String getBundle() {
		return this.bundle;
	}

	public void setBundle(String bundle) {
		this.bundle = bundle;
	}

	public String getIconUrl() {
		return this.iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public Boolean getSeparatorBefore() {
		return this.separatorBefore;
	}

	public void setSeparatorBefore(Boolean separatorBefore) {
		this.separatorBefore = separatorBefore;
	}

	public Boolean getSeparatorAfter() {
		return this.separatorAfter;
	}

	public void setSeparatorAfter(Boolean separatorAfter) {
		this.separatorAfter = separatorAfter;
	}

	public String getGlyph() {
		return this.glyph;
	}

	public void setGlyph(String glyph) {
		this.glyph = glyph;
	}

	public String getMenuId() {
		return this.menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getMenu() {
		return this.menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	public String getMenuItemId() {
		return this.menuItemId;
	}

	public void setMenuItemId(String menuItemId) {
		this.menuItemId = menuItemId;
	}

	public String getMenuItem() {
		return this.menuItem;
	}

	public void setMenuItem(String menuItem) {
		this.menuItem = menuItem;
	}
}
