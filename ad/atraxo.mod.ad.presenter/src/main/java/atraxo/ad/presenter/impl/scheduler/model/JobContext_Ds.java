/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.scheduler.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractType_Ds;
import atraxo.ad.domain.impl.scheduler.JobContext;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = JobContext.class, sort = {@SortField(field = JobContext_Ds.F_NAME)})
public class JobContext_Ds extends AbstractType_Ds<JobContext> {

	public static final String ALIAS = "ad_JobContext_Ds";

	public static final String F_JOBNAME = "jobName";

	@DsField(noUpdate = true)
	private String jobName;

	/**
	 * Default constructor
	 */
	public JobContext_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public JobContext_Ds(JobContext e) {
		super(e);
	}

	public String getJobName() {
		return this.jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
}
