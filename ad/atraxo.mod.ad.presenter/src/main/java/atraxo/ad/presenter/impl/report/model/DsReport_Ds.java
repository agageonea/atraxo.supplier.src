/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.report.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.report.DsReport;
import atraxo.ad.domain.impl.report.Report;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DsReport.class)
@RefLookups({@RefLookup(refId = DsReport_Ds.F_REPORTID, namedQuery = Report.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = DsReport_Ds.F_REPORTCODE)})})
public class DsReport_Ds extends AbstractAuditable_Ds<DsReport> {

	public static final String ALIAS = "ad_DsReport_Ds";

	public static final String F_REPORTID = "reportId";
	public static final String F_REPORTCODE = "reportCode";
	public static final String F_DATASOURCE = "dataSource";

	@DsField(join = "left", path = "report.id")
	private String reportId;

	@DsField(join = "left", path = "report.code")
	private String reportCode;

	@DsField
	private String dataSource;

	/**
	 * Default constructor
	 */
	public DsReport_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DsReport_Ds(DsReport e) {
		super(e);
	}

	public String getReportId() {
		return this.reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getReportCode() {
		return this.reportCode;
	}

	public void setReportCode(String reportCode) {
		this.reportCode = reportCode;
	}

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
}
