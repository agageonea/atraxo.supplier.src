/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeLov_Ds;
import atraxo.ad.domain.impl.security.AccessControl;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = AccessControl.class, sort = {@SortField(field = AccessControlLov_Ds.F_NAME)})
public class AccessControlLov_Ds extends AbstractTypeLov_Ds<AccessControl> {

	public static final String ALIAS = "ad_AccessControlLov_Ds";

	/**
	 * Default constructor
	 */
	public AccessControlLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AccessControlLov_Ds(AccessControl e) {
		super(e);
	}
}
