/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

/**
 * Generated code. Do not modify in this file.
 */
public class MenuItem_DsParam {

	public static final String f_foldersOnly = "foldersOnly";
	public static final String f_framesOnly = "framesOnly";
	public static final String f_withRole = "withRole";
	public static final String f_withRoleId = "withRoleId";

	private Boolean foldersOnly;

	private Boolean framesOnly;

	private String withRole;

	private String withRoleId;

	public Boolean getFoldersOnly() {
		return this.foldersOnly;
	}

	public void setFoldersOnly(Boolean foldersOnly) {
		this.foldersOnly = foldersOnly;
	}

	public Boolean getFramesOnly() {
		return this.framesOnly;
	}

	public void setFramesOnly(Boolean framesOnly) {
		this.framesOnly = framesOnly;
	}

	public String getWithRole() {
		return this.withRole;
	}

	public void setWithRole(String withRole) {
		this.withRole = withRole;
	}

	public String getWithRoleId() {
		return this.withRoleId;
	}

	public void setWithRoleId(String withRoleId) {
		this.withRoleId = withRoleId;
	}
}
