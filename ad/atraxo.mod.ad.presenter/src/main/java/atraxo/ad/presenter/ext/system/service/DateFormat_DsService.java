/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.ext.system.service;

import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;
import atraxo.ad.domain.impl.system.DateFormat;
import atraxo.ad.presenter.impl.system.model.DateFormat_Ds;

public class DateFormat_DsService extends AbstractEntityDsService<DateFormat_Ds, DateFormat_Ds, Object, DateFormat> implements
		IDsService<DateFormat_Ds, DateFormat_Ds, Object> {

	@Override
	protected boolean canInsert() {
		return this.canChange();
	}

	@Override
	protected boolean canUpdate() {
		return this.canChange();
	}

	@Override
	protected boolean canDelete() {
		return this.canChange();
	}

	private boolean canChange() {
		return Session.user.get().isSystemUser();
	}

}
