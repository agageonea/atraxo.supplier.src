/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.security.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeLov_Ds;
import atraxo.ad.domain.impl.security.MenuItem;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = MenuItem.class, jpqlWhere = " e.active = true ", sort = {@SortField(field = MenuItemKpiLov_Ds.F_SEQUENCENO)})
public class MenuItemKpiLov_Ds extends AbstractTypeLov_Ds<MenuItem> {

	public static final String ALIAS = "ad_MenuItemKpiLov_Ds";

	public static final String F_TITLE = "title";
	public static final String F_SEQUENCENO = "sequenceNo";
	public static final String F_MENUID = "menuId";
	public static final String F_MENU = "menu";
	public static final String F_FRAME = "frame";
	public static final String F_BUNDLE = "bundle";

	@DsField
	private String title;

	@DsField
	private Integer sequenceNo;

	@DsField(join = "left", path = "menu.id")
	private String menuId;

	@DsField(join = "left", path = "menu.name")
	private String menu;

	@DsField
	private String frame;

	@DsField
	private String bundle;

	/**
	 * Default constructor
	 */
	public MenuItemKpiLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public MenuItemKpiLov_Ds(MenuItem e) {
		super(e);
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getSequenceNo() {
		return this.sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getMenuId() {
		return this.menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getMenu() {
		return this.menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	public String getFrame() {
		return this.frame;
	}

	public void setFrame(String frame) {
		this.frame = frame;
	}

	public String getBundle() {
		return this.bundle;
	}

	public void setBundle(String bundle) {
		this.bundle = bundle;
	}
}
