/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractTypeWithCodeNTLov_Ds;
import atraxo.ad.domain.impl.system.Client;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Client.class, sort = {@SortField(field = ClientLov_Ds.F_CODE)})
public class ClientLov_Ds extends AbstractTypeWithCodeNTLov_Ds<Client> {

	public static final String ALIAS = "ad_ClientLov_Ds";

	/**
	 * Default constructor
	 */
	public ClientLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ClientLov_Ds(Client e) {
		super(e);
	}
}
