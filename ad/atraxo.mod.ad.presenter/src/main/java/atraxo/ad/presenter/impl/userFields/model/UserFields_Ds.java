/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.userFields.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.ad.UserFieldType;
import atraxo.ad.domain.impl.businessArea.BusinessArea;
import atraxo.ad.domain.impl.userFields.UserFields;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = UserFields.class)
@RefLookups({@RefLookup(refId = UserFields_Ds.F_BUSINESSAREAID, namedQuery = BusinessArea.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = UserFields_Ds.F_BUSINESSAREA)})})
public class UserFields_Ds extends AbstractAuditable_Ds<UserFields> {

	public static final String ALIAS = "ad_UserFields_Ds";

	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_DEFAULTVALUE = "defaultValue";
	public static final String F_TYPE = "type";
	public static final String F_BUSINESSAREAID = "businessAreaId";
	public static final String F_BUSINESSAREA = "businessArea";
	public static final String F_DSMODEL = "dsModel";
	public static final String F_DSNAME = "dsName";
	public static final String F_DSALIAS = "dsAlias";

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private String defaultValue;

	@DsField
	private UserFieldType type;

	@DsField(join = "left", path = "businessArea.id")
	private String businessAreaId;

	@DsField(join = "left", path = "businessArea.name")
	private String businessArea;

	@DsField(join = "left", path = "businessArea.dsModel")
	private String dsModel;

	@DsField(join = "left", path = "businessArea.dsName")
	private String dsName;

	@DsField(join = "left", path = "businessArea.dsAlias")
	private String dsAlias;

	/**
	 * Default constructor
	 */
	public UserFields_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public UserFields_Ds(UserFields e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public UserFieldType getType() {
		return this.type;
	}

	public void setType(UserFieldType type) {
		this.type = type;
	}

	public String getBusinessAreaId() {
		return this.businessAreaId;
	}

	public void setBusinessAreaId(String businessAreaId) {
		this.businessAreaId = businessAreaId;
	}

	public String getBusinessArea() {
		return this.businessArea;
	}

	public void setBusinessArea(String businessArea) {
		this.businessArea = businessArea;
	}

	public String getDsModel() {
		return this.dsModel;
	}

	public void setDsModel(String dsModel) {
		this.dsModel = dsModel;
	}

	public String getDsName() {
		return this.dsName;
	}

	public void setDsName(String dsName) {
		this.dsName = dsName;
	}

	public String getDsAlias() {
		return this.dsAlias;
	}

	public void setDsAlias(String dsAlias) {
		this.dsAlias = dsAlias;
	}
}
