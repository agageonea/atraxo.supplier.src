/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.system.ParamValue;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ParamValue.class)
public class ParamValue_Ds extends AbstractAuditable_Ds<ParamValue> {

	public static final String ALIAS = "ad_ParamValue_Ds";

	public static final String F_SYSPARAM = "sysParam";
	public static final String F_VALUE = "value";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";

	@DsField
	private String sysParam;

	@DsField
	private String value;

	@DsField
	private Date validFrom;

	@DsField
	private Date validTo;

	/**
	 * Default constructor
	 */
	public ParamValue_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ParamValue_Ds(ParamValue e) {
		super(e);
	}

	public String getSysParam() {
		return this.sysParam;
	}

	public void setSysParam(String sysParam) {
		this.sysParam = sysParam;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
}
