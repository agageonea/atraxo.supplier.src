/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.attachment.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractType_Ds;
import atraxo.ad.domain.impl.ad.TAttachmentType;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = AttachmentType.class, sort = {@SortField(field = AttachmentType_Ds.F_NAME)})
public class AttachmentType_Ds extends AbstractType_Ds<AttachmentType> {

	public static final String ALIAS = "ad_AttachmentType_Ds";

	public static final String F_CATEGORY = "category";
	public static final String F_UPLOADPATH = "uploadPath";
	public static final String F_BASEURL = "baseUrl";

	@DsField
	private TAttachmentType category;

	@DsField
	private String uploadPath;

	@DsField
	private String baseUrl;

	/**
	 * Default constructor
	 */
	public AttachmentType_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AttachmentType_Ds(AttachmentType e) {
		super(e);
	}

	public TAttachmentType getCategory() {
		return this.category;
	}

	public void setCategory(TAttachmentType category) {
		this.category = category;
	}

	public String getUploadPath() {
		return this.uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	public String getBaseUrl() {
		return this.baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
}
