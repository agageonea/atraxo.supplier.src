/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.ext.externalReport.service;

import java.io.File;
import java.nio.file.Files;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import atraxo.ad.business.api.externalReport.IExternalReportService;
import atraxo.ad.business.api.externalReport.IReportLogService;
import atraxo.ad.business.api.externalReport.ISourceUploadHistoryService;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.externalReport.ReportLog;
import atraxo.ad.domain.impl.externalReport.SourceUploadHistory;
import atraxo.ad.presenter.impl.externalReport.model.ExternalReport_Ds;
import atraxo.ad.presenter.impl.externalReport.model.ExternalReport_DsParam;
import seava.j4e.api.Constants;
import seava.j4e.api.ISettings;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.api.service.report.IReportService;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service ExternalReport_DsService
 */
public class ExternalReport_DsService extends AbstractEntityDsService<ExternalReport_Ds, ExternalReport_Ds, ExternalReport_DsParam, ExternalReport>
		implements IDsService<ExternalReport_Ds, ExternalReport_Ds, ExternalReport_DsParam> {

	@Autowired
	private IReportLogService reportLogService;
	@Autowired
	private IExternalReportService externalReportService;
	@Autowired
	private ISourceUploadHistoryService sourceUploadHistoryService;
	@Autowired
	private ISettings settings;

	@Override
	protected void postFind(IQueryBuilder<ExternalReport_Ds, ExternalReport_Ds, ExternalReport_DsParam> builder, List<ExternalReport_Ds> result)
			throws Exception {
		super.postFind(builder, result);

		String externalScheme = this.settings.get(Constants.PROP_SYS_EXTERNAL_SCHEME);
		String externalServerName = this.settings.get(Constants.PROP_SYS_EXTERNAL_SERVER_NAME);
		String externalServerPort = this.settings.get(Constants.PROP_SYS_EXTERNAL_SERVER_PORT);
		String externalContextPath = this.settings.get(Constants.PROP_SYS_EXTERNAL_CONTEXT_PATH);
		String url = new StringBuilder(externalScheme).append("://").append(externalServerName).append(":").append(externalServerPort)
				.append(externalContextPath).toString();

		for (ExternalReport_Ds ds : result) {
			ReportLog reportLog = this.reportLogService.findLastEntry(ds.getId());
			if (reportLog != null) {
				ds.setLastExecution(reportLog.getRunDate());
				ds.setLastExecutionStatus(reportLog.getStatus().getName());
			}
			if (ds.getDesignName() != null) {
				ds.setUrl(url + Constants.SERVLETPATH_DATA + Constants.CTXPATH_DS + "/" + ExternalReport_Ds.ALIAS + "." + ds.getReportCode() + "?"
						+ Constants.REQUEST_PARAM_ACTION + "=" + Constants.DS_ACTION_DOWNLOAD_RPT_DESIGN + "&" + Constants.REQUEST_PARAM_FILE_NAME
						+ "=" + ds.getDesignName());
			}
		}
	}

	@Override
	public void doUpgrade(File file, Object config) throws Exception {
		super.doUpgrade(file, config);

		List<ExternalReport> reports = this.externalReportService.findEntitiesByAttributes(new HashMap<>());
		for (ExternalReport er : reports) {
			if (!StringUtils.isEmpty(er.getDesignName()) && CollectionUtils.isEmpty(er.getSourceUploadHistories())) {
				String newPath = file.getParent().substring(0, file.getParent().lastIndexOf("\\") + 1) + "reportdesign" + File.separator;
				File fileDesign = new File(newPath + er.getDesignName());
				if (fileDesign.exists()) {
					IReportService reportService = this.getApplicationContext().getBean("reportService", IReportService.class);
					byte[] data = Files.readAllBytes(fileDesign.toPath());
					reportService.uploadReport(data, er.getDesignName());
					this.externalReportService.parseAndSaveParameters(er, data, er.getDesignName());
					SourceUploadHistory sourceHistory = new SourceUploadHistory();
					sourceHistory.setActive(true);
					sourceHistory.setFileName(er.getDesignName());
					sourceHistory.setName(er.getName());
					er = this.externalReportService.findById(er.getId());
					sourceHistory.setExternalReport(er);
					sourceHistory.setUploadDate(new Date());
					sourceHistory.setNotes("");
					sourceHistory.setUploader(Session.user.get().getCode());
					sourceHistory.setCreatedBy(Session.user.get().getCode());
					this.sourceUploadHistoryService.insert(sourceHistory);
				}
			}
		}
	}
}
