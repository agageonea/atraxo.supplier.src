/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.businessArea.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.businessArea.BusinessArea;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = BusinessArea.class)
public class BusinessArea_Ds extends AbstractAuditable_Ds<BusinessArea> {

	public static final String ALIAS = "ad_BusinessArea_Ds";

	public static final String F_NAME = "name";
	public static final String F_DSMODEL = "dsModel";
	public static final String F_DSNAME = "dsName";
	public static final String F_DSALIAS = "dsAlias";

	@DsField
	private String name;

	@DsField
	private String dsModel;

	@DsField
	private String dsName;

	@DsField
	private String dsAlias;

	/**
	 * Default constructor
	 */
	public BusinessArea_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public BusinessArea_Ds(BusinessArea e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDsModel() {
		return this.dsModel;
	}

	public void setDsModel(String dsModel) {
		this.dsModel = dsModel;
	}

	public String getDsName() {
		return this.dsName;
	}

	public void setDsName(String dsName) {
		this.dsName = dsName;
	}

	public String getDsAlias() {
		return this.dsAlias;
	}

	public void setDsAlias(String dsAlias) {
		this.dsAlias = dsAlias;
	}
}
