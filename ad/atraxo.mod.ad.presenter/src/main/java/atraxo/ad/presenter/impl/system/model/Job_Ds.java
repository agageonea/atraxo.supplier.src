/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.system.model;

import atraxo.abstracts.presenter.impl.notenant.model.AbstractTypeNT_Ds;
import atraxo.ad.domain.impl.system.Job;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Job.class, sort = {@SortField(field = Job_Ds.F_NAME)})
public class Job_Ds extends AbstractTypeNT_Ds<Job> {

	public static final String ALIAS = "ad_Job_Ds";

	public static final String F_JAVACLASS = "javaClass";

	@DsField
	private String javaClass;

	/**
	 * Default constructor
	 */
	public Job_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Job_Ds(Job e) {
		super(e);
	}

	public String getJavaClass() {
		return this.javaClass;
	}

	public void setJavaClass(String javaClass) {
		this.javaClass = javaClass;
	}
}
