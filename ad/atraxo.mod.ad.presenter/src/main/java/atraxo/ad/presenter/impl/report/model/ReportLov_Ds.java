/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.report.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeWithCodeLov_Ds;
import atraxo.ad.domain.impl.report.Report;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Report.class, sort = {@SortField(field = ReportLov_Ds.F_CODE)})
public class ReportLov_Ds extends AbstractTypeWithCodeLov_Ds<Report> {

	public static final String ALIAS = "ad_ReportLov_Ds";

	/**
	 * Default constructor
	 */
	public ReportLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ReportLov_Ds(Report e) {
		super(e);
	}
}
