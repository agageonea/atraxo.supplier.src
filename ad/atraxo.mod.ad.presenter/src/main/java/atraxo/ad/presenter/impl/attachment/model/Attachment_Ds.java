/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.presenter.impl.attachment.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractAuditable_Ds;
import atraxo.ad.domain.impl.ad.TAttachmentType;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Attachment.class)
@RefLookups({@RefLookup(refId = Attachment_Ds.F_TYPEID, namedQuery = AttachmentType.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = Attachment_Ds.F_TYPE)})})
public class Attachment_Ds extends AbstractAuditable_Ds<Attachment> {

	public static final String ALIAS = "ad_Attachment_Ds";

	public static final String F_TARGETREFID = "targetRefid";
	public static final String F_TARGETALIAS = "targetAlias";
	public static final String F_TARGETTYPE = "targetType";
	public static final String F_CATEGTYPE = "categType";
	public static final String F_NAME = "name";
	public static final String F_LOCATION = "location";
	public static final String F_CONTENTTYPE = "contentType";
	public static final String F_URL = "url";
	public static final String F_FILENAME = "fileName";
	public static final String F_TYPEID = "typeId";
	public static final String F_TYPE = "type";
	public static final String F_CATEGORY = "category";
	public static final String F_BASEURL = "baseUrl";
	public static final String F_FILEPATH = "filePath";

	@DsField
	private String targetRefid;

	@DsField
	private String targetAlias;

	@DsField
	private String targetType;

	@DsField(join = "left", path = "type.category")
	private TAttachmentType categType;

	@DsField
	private String name;

	@DsField
	private String location;

	@DsField
	private String contentType;

	@DsField(fetch = false)
	private String url;

	@DsField
	private String fileName;

	@DsField(join = "left", path = "type.id")
	private String typeId;

	@DsField(join = "left", path = "type.name")
	private String type;

	@DsField(join = "left", path = "type.category")
	private TAttachmentType category;

	@DsField(join = "left", path = "type.baseUrl")
	private String baseUrl;

	@DsField(fetch = false)
	private String filePath;

	/**
	 * Default constructor
	 */
	public Attachment_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Attachment_Ds(Attachment e) {
		super(e);
	}

	public String getTargetRefid() {
		return this.targetRefid;
	}

	public void setTargetRefid(String targetRefid) {
		this.targetRefid = targetRefid;
	}

	public String getTargetAlias() {
		return this.targetAlias;
	}

	public void setTargetAlias(String targetAlias) {
		this.targetAlias = targetAlias;
	}

	public String getTargetType() {
		return this.targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

	public TAttachmentType getCategType() {
		return this.categType;
	}

	public void setCategType(TAttachmentType categType) {
		this.categType = categType;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getTypeId() {
		return this.typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public TAttachmentType getCategory() {
		return this.category;
	}

	public void setCategory(TAttachmentType category) {
		this.category = category;
	}

	public String getBaseUrl() {
		return this.baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getFilePath() {
		return this.filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
}
