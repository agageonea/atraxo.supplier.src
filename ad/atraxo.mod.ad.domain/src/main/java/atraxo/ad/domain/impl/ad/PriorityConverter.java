/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link Priority} enum.
 * Generated code. Do not modify in this file.
 */
public class PriorityConverter implements AttributeConverter<Priority, String> {

	@Override
	public String convertToDatabaseColumn(Priority value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null Priority.");
		}
		return value.getName();
	}

	@Override
	public Priority convertToEntityAttribute(String value) {
		return Priority.getByName(value);
	}

}
