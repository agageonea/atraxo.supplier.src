/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.scheduler;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.ReadOnly;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link QuartzLock} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = QuartzLock.TABLE_NAME)
@ReadOnly
@Cache(type = CacheType.NONE)
public class QuartzLock implements Serializable {

	public static final String TABLE_NAME = "QRTZ_LOCKS";

	private static final long serialVersionUID = -8865917134914502125L;

	@Id
	@NotBlank
	@Column(name = "sched_name", nullable = false, length = 255)
	private String schedulerName;

	@Id
	@NotBlank
	@Column(name = "lock_name", nullable = false, length = 255)
	private String lockName;

	@Transient
	private Long version;

	public String getSchedulerName() {
		return this.schedulerName;
	}

	public void setSchedulerName(String schedulerName) {
		this.schedulerName = schedulerName;
	}

	public String getLockName() {
		return this.lockName;
	}

	public void setLockName(String lockName) {
		this.lockName = lockName;
	}

	public Long getVersion() {
		return 1L;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * called before persist
	 */
	@PrePersist
	public void prePersist() {
	}

	/**
	 * called before update
	 */
	@PreUpdate
	public void preUpdate() {
	}

}
