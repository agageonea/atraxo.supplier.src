/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.externalReport;

import atraxo.abstracts.domain.impl.tenant.AbstractType;
import atraxo.ad.domain.impl.ad.LastExecutionStatus;
import atraxo.ad.domain.impl.ad.LastExecutionStatusConverter;
import atraxo.ad.domain.impl.businessArea.BusinessArea;
import atraxo.ad.domain.impl.externalReport.ExternalReportParameter;
import atraxo.ad.domain.impl.externalReport.ReportLog;
import atraxo.ad.domain.impl.externalReport.SourceUploadHistory;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link ExternalReport} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = ExternalReport.NQ_FIND_BY_NAME, query = "SELECT e FROM ExternalReport e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ExternalReport.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = ExternalReport.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "name"})})
public class ExternalReport extends AbstractType implements Serializable {

	public static final String TABLE_NAME = "AD_EXTERNAL_REPORT";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "ExternalReport.findByName";

	@Column(name = "system")
	private Boolean system;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_execution")
	private Date lastExecution;

	@Column(name = "last_execution_status", length = 32)
	@Convert(converter = LastExecutionStatusConverter.class)
	private LastExecutionStatus lastExecutionStatus;

	@Column(name = "design_name", length = 200)
	private String designName;

	@Column(name = "report_code", length = 200)
	private String reportCode;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = BusinessArea.class)
	@JoinColumn(name = "business_area_id", referencedColumnName = "id")
	private BusinessArea businessArea;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = ExternalReportParameter.class, mappedBy = "externalReport", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<ExternalReportParameter> params;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = ReportLog.class, mappedBy = "externalReport", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<ReportLog> reportLogs;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = SourceUploadHistory.class, mappedBy = "externalReport", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	private Collection<SourceUploadHistory> sourceUploadHistories;

	public Boolean getSystem() {
		return this.system;
	}

	public void setSystem(Boolean system) {
		this.system = system;
	}

	public Date getLastExecution() {
		return this.lastExecution;
	}

	public void setLastExecution(Date lastExecution) {
		this.lastExecution = lastExecution;
	}

	public LastExecutionStatus getLastExecutionStatus() {
		return this.lastExecutionStatus;
	}

	public void setLastExecutionStatus(LastExecutionStatus lastExecutionStatus) {
		this.lastExecutionStatus = lastExecutionStatus;
	}

	public String getDesignName() {
		return this.designName;
	}

	public void setDesignName(String designName) {
		this.designName = designName;
	}

	public String getReportCode() {
		return this.reportCode;
	}

	public void setReportCode(String reportCode) {
		this.reportCode = reportCode;
	}

	public BusinessArea getBusinessArea() {
		return this.businessArea;
	}

	public void setBusinessArea(BusinessArea businessArea) {
		if (businessArea != null) {
			this.__validate_client_context__(businessArea.getClientId());
		}
		this.businessArea = businessArea;
	}

	public Collection<ExternalReportParameter> getParams() {
		return this.params;
	}

	public void setParams(Collection<ExternalReportParameter> params) {
		this.params = params;
	}

	/**
	 * @param e
	 */
	public void addToParams(ExternalReportParameter e) {
		if (this.params == null) {
			this.params = new ArrayList<>();
		}
		e.setExternalReport(this);
		this.params.add(e);
	}
	public Collection<ReportLog> getReportLogs() {
		return this.reportLogs;
	}

	public void setReportLogs(Collection<ReportLog> reportLogs) {
		this.reportLogs = reportLogs;
	}

	/**
	 * @param e
	 */
	public void addToReportLogs(ReportLog e) {
		if (this.reportLogs == null) {
			this.reportLogs = new ArrayList<>();
		}
		e.setExternalReport(this);
		this.reportLogs.add(e);
	}
	public Collection<SourceUploadHistory> getSourceUploadHistories() {
		return this.sourceUploadHistories;
	}

	public void setSourceUploadHistories(
			Collection<SourceUploadHistory> sourceUploadHistories) {
		this.sourceUploadHistories = sourceUploadHistories;
	}

	/**
	 * @param e
	 */
	public void addToSourceUploadHistories(SourceUploadHistory e) {
		if (this.sourceUploadHistories == null) {
			this.sourceUploadHistories = new ArrayList<>();
		}
		e.setExternalReport(this);
		this.sourceUploadHistories.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
