/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum LanguageCodes {

	_EMPTY_("", ""), _ENGLISH_("English", "en"), _GERMAN_("German", "de"), _FRENCH_(
			"French", "fr"), _SPANISH_("Spanish", "es"), _PORTUGUESE_(
			"Portuguese", "pt"), _CHINESE_("Chinese", "zh");

	private String name;
	private String code;

	private LanguageCodes(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static LanguageCodes getByName(String name) {
		for (LanguageCodes status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent LanguageCodes with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static LanguageCodes getByCode(String code) {
		for (LanguageCodes status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent LanguageCodes with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
