/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum LastExecutionStatus {

	_EMPTY_(""), _UNKNOWN_("Unknown"), _SUCCESS_("Success"), _FAILURE_(
			"Failure");

	private String name;

	private LastExecutionStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static LastExecutionStatus getByName(String name) {
		for (LastExecutionStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent LastExecutionStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
