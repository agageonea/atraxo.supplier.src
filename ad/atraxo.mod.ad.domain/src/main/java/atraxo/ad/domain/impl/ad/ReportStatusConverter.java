/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ReportStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class ReportStatusConverter
		implements
			AttributeConverter<ReportStatus, String> {

	@Override
	public String convertToDatabaseColumn(ReportStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ReportStatus.");
		}
		return value.getName();
	}

	@Override
	public ReportStatus convertToEntityAttribute(String value) {
		return ReportStatus.getByName(value);
	}

}
