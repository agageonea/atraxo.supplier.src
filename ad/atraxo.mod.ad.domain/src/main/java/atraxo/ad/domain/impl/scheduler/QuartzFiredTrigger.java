/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.scheduler;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.ReadOnly;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link QuartzFiredTrigger} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = QuartzFiredTrigger.TABLE_NAME)
@ReadOnly
@Cache(type = CacheType.NONE)
public class QuartzFiredTrigger implements Serializable {

	public static final String TABLE_NAME = "QRTZ_FIRED_TRIGGERS";

	private static final long serialVersionUID = -8865917134914502125L;

	@Id
	@NotBlank
	@Column(name = "sched_name", nullable = false, length = 255)
	private String schedulerName;

	@Id
	@NotBlank
	@Column(name = "entry_id", nullable = false, length = 255)
	private String entryID;

	@NotBlank
	@Column(name = "trigger_name", nullable = false, length = 255)
	private String triggerName;

	@NotBlank
	@Column(name = "trigger_group", nullable = false, length = 255)
	private String triggerGroup;

	@NotBlank
	@Column(name = "instance_name", nullable = false, length = 255)
	private String instanceName;

	@NotNull
	@Column(name = "fired_time", nullable = false, precision = 13)
	private Long firedTime;

	@NotNull
	@Column(name = "sched_time", nullable = false, precision = 13)
	private Long scheduledTime;

	@NotNull
	@Column(name = "priority", nullable = false, precision = 13)
	private Long priority;

	@NotBlank
	@Column(name = "state", nullable = false, length = 16)
	private String state;

	@Column(name = "job_name", length = 255)
	private String jobName;

	@Column(name = "job_group", length = 255)
	private String jobGroup;

	@Column(name = "is_nonconcurrent", length = 1)
	private String isNonConcurrent;

	@Column(name = "requests_recovery", length = 1)
	private String requestsRecovery;

	@Transient
	private Long version;

	public String getSchedulerName() {
		return this.schedulerName;
	}

	public void setSchedulerName(String schedulerName) {
		this.schedulerName = schedulerName;
	}

	public String getEntryID() {
		return this.entryID;
	}

	public void setEntryID(String entryID) {
		this.entryID = entryID;
	}

	public String getTriggerName() {
		return this.triggerName;
	}

	public void setTriggerName(String triggerName) {
		this.triggerName = triggerName;
	}

	public String getTriggerGroup() {
		return this.triggerGroup;
	}

	public void setTriggerGroup(String triggerGroup) {
		this.triggerGroup = triggerGroup;
	}

	public String getInstanceName() {
		return this.instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public Long getFiredTime() {
		return this.firedTime;
	}

	public void setFiredTime(Long firedTime) {
		this.firedTime = firedTime;
	}

	public Long getScheduledTime() {
		return this.scheduledTime;
	}

	public void setScheduledTime(Long scheduledTime) {
		this.scheduledTime = scheduledTime;
	}

	public Long getPriority() {
		return this.priority;
	}

	public void setPriority(Long priority) {
		this.priority = priority;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getJobName() {
		return this.jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobGroup() {
		return this.jobGroup;
	}

	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}

	public String getIsNonConcurrent() {
		return this.isNonConcurrent;
	}

	public void setIsNonConcurrent(String isNonConcurrent) {
		this.isNonConcurrent = isNonConcurrent;
	}

	public String getRequestsRecovery() {
		return this.requestsRecovery;
	}

	public void setRequestsRecovery(String requestsRecovery) {
		this.requestsRecovery = requestsRecovery;
	}

	public Long getVersion() {
		return 1L;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * called before persist
	 */
	@PrePersist
	public void prePersist() {
	}

	/**
	 * called before update
	 */
	@PreUpdate
	public void preUpdate() {
	}

}
