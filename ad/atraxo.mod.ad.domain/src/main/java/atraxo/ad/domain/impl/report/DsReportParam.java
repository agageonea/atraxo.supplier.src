/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.report;

import atraxo.abstracts.domain.impl.tenant.AbstractAuditable;
import atraxo.ad.domain.impl.report.DsReport;
import atraxo.ad.domain.impl.report.ReportParam;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link DsReportParam} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = DsReportParam.NQ_FIND_BY_REPPARAM_DS, query = "SELECT e FROM DsReportParam e WHERE e.clientId = :clientId and e.dsReport = :dsReport and e.reportParam = :reportParam", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = DsReportParam.NQ_FIND_BY_REPPARAM_DS_PRIMITIVE, query = "SELECT e FROM DsReportParam e WHERE e.clientId = :clientId and e.dsReport.id = :dsReportId and e.reportParam.id = :reportParamId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = DsReportParam.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = DsReportParam.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "dsreport_id", "reportparam_id"})})
public class DsReportParam extends AbstractAuditable implements Serializable {

	public static final String TABLE_NAME = "AD_RPT_DS_PARAM";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: RepParam_ds.
	 */
	public static final String NQ_FIND_BY_REPPARAM_DS = "DsReportParam.findByRepParam_ds";
	/**
	 * Named query find by unique key: RepParam_ds using the ID field for references.
	 */
	public static final String NQ_FIND_BY_REPPARAM_DS_PRIMITIVE = "DsReportParam.findByRepParam_ds_PRIMITIVE";

	/** Reference to the data-source field. 
	 */
	@Column(name = "dsfield", length = 255)
	private String dsField;

	@Column(name = "staticvalue", length = 1000)
	private String staticValue;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = DsReport.class)
	@JoinColumn(name = "dsreport_id", referencedColumnName = "id")
	private DsReport dsReport;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ReportParam.class)
	@JoinColumn(name = "reportparam_id", referencedColumnName = "id")
	private ReportParam reportParam;

	public String getDsField() {
		return this.dsField;
	}

	public void setDsField(String dsField) {
		this.dsField = dsField;
	}

	public String getStaticValue() {
		return this.staticValue;
	}

	public void setStaticValue(String staticValue) {
		this.staticValue = staticValue;
	}

	public DsReport getDsReport() {
		return this.dsReport;
	}

	public void setDsReport(DsReport dsReport) {
		if (dsReport != null) {
			this.__validate_client_context__(dsReport.getClientId());
		}
		this.dsReport = dsReport;
	}
	public ReportParam getReportParam() {
		return this.reportParam;
	}

	public void setReportParam(ReportParam reportParam) {
		if (reportParam != null) {
			this.__validate_client_context__(reportParam.getClientId());
		}
		this.reportParam = reportParam;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.dsReport == null) ? 0 : this.dsReport.hashCode());
		result = prime
				* result
				+ ((this.reportParam == null) ? 0 : this.reportParam.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		DsReportParam other = (DsReportParam) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.dsReport == null) {
			if (other.dsReport != null) {
				return false;
			}
		} else if (!this.dsReport.equals(other.dsReport)) {
			return false;
		}
		if (this.reportParam == null) {
			if (other.reportParam != null) {
				return false;
			}
		} else if (!this.reportParam.equals(other.reportParam)) {
			return false;
		}
		return true;
	}
}
