/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.system;

import atraxo.abstracts.domain.impl.notenant.AbstractTypeNT;
import atraxo.ad.domain.impl.system.DateFormatMask;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link DateFormat} domain entity.
 * Generated code. Do not modify in this file.
 */
/**
 * Redefined date formats available to users.
 */
@NamedQueries({@NamedQuery(name = DateFormat.NQ_FIND_BY_NAME, query = "SELECT e FROM DateFormat e WHERE e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = DateFormat.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = DateFormat.TABLE_NAME
		+ "_UK1", columnNames = {"name"})})
public class DateFormat extends AbstractTypeNT implements Serializable {

	public static final String TABLE_NAME = "SYS_DTFMT";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "DateFormat.findByName";

	@OneToMany(fetch = FetchType.LAZY, targetEntity = DateFormatMask.class, mappedBy = "dateFormat", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<DateFormatMask> masks;

	public Collection<DateFormatMask> getMasks() {
		return this.masks;
	}

	public void setMasks(Collection<DateFormatMask> masks) {
		this.masks = masks;
	}

	/**
	 * @param e
	 */
	public void addToMasks(DateFormatMask e) {
		if (this.masks == null) {
			this.masks = new ArrayList<>();
		}
		e.setDateFormat(this);
		this.masks.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
