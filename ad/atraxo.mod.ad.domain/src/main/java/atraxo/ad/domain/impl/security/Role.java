/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.security;

import atraxo.abstracts.domain.impl.tenant.AbstractTypeWithCode;
import atraxo.ad.domain.impl.security.AccessControl;
import atraxo.ad.domain.impl.security.Menu;
import atraxo.ad.domain.impl.security.MenuItem;
import atraxo.ad.domain.impl.security.User;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link Role} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Role.NQ_FIND_BY_CODE, query = "SELECT e FROM Role e WHERE e.clientId = :clientId and e.code = :code", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Role.NQ_FIND_BY_NAME, query = "SELECT e FROM Role e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Role.TABLE_NAME, uniqueConstraints = {
		@UniqueConstraint(name = Role.TABLE_NAME + "_UK1", columnNames = {
				"clientid", "code"}),
		@UniqueConstraint(name = Role.TABLE_NAME + "_UK2", columnNames = {
				"clientid", "name"})})
public class Role extends AbstractTypeWithCode implements Serializable {

	public static final String TABLE_NAME = "AD_ROLE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "Role.findByCode";
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "Role.findByName";

	@ManyToMany(mappedBy = "roles")
	private Collection<User> users;

	@ManyToMany
	@JoinTable(name = "AD_ROLE_ACL", joinColumns = {@JoinColumn(name = "roles_id")}, inverseJoinColumns = {@JoinColumn(name = "accesscontrols_id")})
	private Collection<AccessControl> accessControls;

	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(name = "AD_ROLE_MENU", joinColumns = {@JoinColumn(name = "roles_id")}, inverseJoinColumns = {@JoinColumn(name = "menus_id")})
	private Collection<Menu> menus;

	@ManyToMany
	@JoinTable(name = "AD_ROLE_MENUITEM", joinColumns = {@JoinColumn(name = "roles_id")}, inverseJoinColumns = {@JoinColumn(name = "menuitems_id")})
	private Collection<MenuItem> menuItems;

	public Collection<User> getUsers() {
		return this.users;
	}

	public void setUsers(Collection<User> users) {
		this.users = users;
	}

	public Collection<AccessControl> getAccessControls() {
		return this.accessControls;
	}

	public void setAccessControls(Collection<AccessControl> accessControls) {
		this.accessControls = accessControls;
	}

	public Collection<Menu> getMenus() {
		return this.menus;
	}

	public void setMenus(Collection<Menu> menus) {
		this.menus = menus;
	}

	public Collection<MenuItem> getMenuItems() {
		return this.menuItems;
	}

	public void setMenuItems(Collection<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
