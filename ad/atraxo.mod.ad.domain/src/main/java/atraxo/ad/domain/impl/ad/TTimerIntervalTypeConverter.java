/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TTimerIntervalType} enum.
 * Generated code. Do not modify in this file.
 */
public class TTimerIntervalTypeConverter
		implements
			AttributeConverter<TTimerIntervalType, String> {

	@Override
	public String convertToDatabaseColumn(TTimerIntervalType value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null TTimerIntervalType.");
		}
		return value.getName();
	}

	@Override
	public TTimerIntervalType convertToEntityAttribute(String value) {
		return TTimerIntervalType.getByName(value);
	}

}
