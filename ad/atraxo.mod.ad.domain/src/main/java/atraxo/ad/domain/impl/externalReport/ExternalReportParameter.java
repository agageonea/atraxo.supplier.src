/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.externalReport;

import atraxo.abstracts.domain.impl.tenant.AbstractAuditable;
import atraxo.ad.domain.impl.ad.ReportParamType;
import atraxo.ad.domain.impl.ad.ReportParamTypeConverter;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link ExternalReportParameter} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = ExternalReportParameter.NQ_FIND_BY_KEY, query = "SELECT e FROM ExternalReportParameter e WHERE e.clientId = :clientId and e.externalReport = :externalReport and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ExternalReportParameter.NQ_FIND_BY_KEY_PRIMITIVE, query = "SELECT e FROM ExternalReportParameter e WHERE e.clientId = :clientId and e.externalReport.id = :externalReportId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ExternalReportParameter.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = ExternalReportParameter.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "external_report_id", "name"})})
public class ExternalReportParameter extends AbstractAuditable
		implements
			Serializable {

	public static final String TABLE_NAME = "AD_EXTERNAL_REPORTS_PARAMS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Key.
	 */
	public static final String NQ_FIND_BY_KEY = "ExternalReportParameter.findByKey";
	/**
	 * Named query find by unique key: Key using the ID field for references.
	 */
	public static final String NQ_FIND_BY_KEY_PRIMITIVE = "ExternalReportParameter.findByKey_PRIMITIVE";

	@NotBlank
	@Column(name = "name", nullable = false, length = 255)
	private String name;

	@Column(name = "description", length = 1000)
	private String description;

	@Column(name = "parameter_value", length = 1000)
	private String paramValue;

	@Column(name = "default_value", length = 1000)
	private String defaultValue;

	@NotBlank
	@Column(name = "type", nullable = false, length = 32)
	@Convert(converter = ReportParamTypeConverter.class)
	private ReportParamType type;

	@Column(name = "read_only")
	private Boolean mandatory;

	@Column(name = "ds_field_name", length = 255)
	private String dsFieldName;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ExternalReport.class)
	@JoinColumn(name = "external_report_id", referencedColumnName = "id")
	private ExternalReport externalReport;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getParamValue() {
		return this.paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public ReportParamType getType() {
		return this.type;
	}

	public void setType(ReportParamType type) {
		this.type = type;
	}

	public Boolean getMandatory() {
		return this.mandatory;
	}

	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}

	public String getDsFieldName() {
		return this.dsFieldName;
	}

	public void setDsFieldName(String dsFieldName) {
		this.dsFieldName = dsFieldName;
	}

	public ExternalReport getExternalReport() {
		return this.externalReport;
	}

	public void setExternalReport(ExternalReport externalReport) {
		if (externalReport != null) {
			this.__validate_client_context__(externalReport.getClientId());
		}
		this.externalReport = externalReport;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime
				* result
				+ ((this.externalReport == null) ? 0 : this.externalReport
						.hashCode());
		result = prime * result
				+ ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		ExternalReportParameter other = (ExternalReportParameter) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.externalReport == null) {
			if (other.externalReport != null) {
				return false;
			}
		} else if (!this.externalReport.equals(other.externalReport)) {
			return false;
		}
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		return true;
	}
}
