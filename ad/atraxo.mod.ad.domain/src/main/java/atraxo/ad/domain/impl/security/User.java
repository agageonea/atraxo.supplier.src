/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.security;

import atraxo.abstracts.domain.impl.tenant.AbstractTypeWithCode;
import atraxo.ad.domain.impl.ad.LanguageCodes;
import atraxo.ad.domain.impl.ad.LanguageCodesConverter;
import atraxo.ad.domain.impl.ad.TNumberSeparator;
import atraxo.ad.domain.impl.ad.TNumberSeparatorConverter;
import atraxo.ad.domain.impl.security.Role;
import atraxo.ad.domain.impl.security.UserGroup;
import atraxo.ad.domain.impl.system.DateFormat;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link User} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = User.NQ_FIND_BY_CODE, query = "SELECT e FROM User e WHERE e.clientId = :clientId and e.code = :code", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = User.NQ_FIND_BY_LOGIN, query = "SELECT e FROM User e WHERE e.clientId = :clientId and e.loginName = :loginName", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = User.TABLE_NAME, uniqueConstraints = {
		@UniqueConstraint(name = User.TABLE_NAME + "_UK1", columnNames = {
				"clientid", "code"}),
		@UniqueConstraint(name = User.TABLE_NAME + "_UK2", columnNames = {
				"clientid", "loginname"})})
public class User extends AbstractTypeWithCode implements Serializable {

	public static final String TABLE_NAME = "AD_USR";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "User.findByCode";
	/**
	 * Named query find by unique key: Login.
	 */
	public static final String NQ_FIND_BY_LOGIN = "User.findByLogin";

	@NotBlank
	@Column(name = "loginname", nullable = false, length = 255)
	private String loginName;

	@Column(name = "password", length = 255)
	private String password;

	@Column(name = "email", length = 128)
	private String email;

	@NotNull
	@Column(name = "locked", nullable = false)
	private Boolean locked;

	@Column(name = "decimalseparator", length = 1)
	@Convert(converter = TNumberSeparatorConverter.class)
	private TNumberSeparator decimalSeparator;

	@Column(name = "thousandseparator", length = 1)
	@Convert(converter = TNumberSeparatorConverter.class)
	private TNumberSeparator thousandSeparator;

	@NotBlank
	@Column(name = "language_code", nullable = false, length = 50)
	@Convert(converter = LanguageCodesConverter.class)
	private LanguageCodes userLanguage;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = DateFormat.class)
	@JoinColumn(name = "dateformat_id", referencedColumnName = "id")
	private DateFormat dateFormat;

	@ManyToMany
	@JoinTable(name = "AD_USR_ROLE", joinColumns = {@JoinColumn(name = "users_id")}, inverseJoinColumns = {@JoinColumn(name = "roles_id")})
	private Collection<Role> roles;

	@ManyToMany
	@JoinTable(name = "AD_USR_USRGRP", joinColumns = {@JoinColumn(name = "users_id")}, inverseJoinColumns = {@JoinColumn(name = "groups_id")})
	private Collection<UserGroup> groups;

	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getLocked() {
		return this.locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public TNumberSeparator getDecimalSeparator() {
		return this.decimalSeparator;
	}

	public void setDecimalSeparator(TNumberSeparator decimalSeparator) {
		this.decimalSeparator = decimalSeparator;
	}

	public TNumberSeparator getThousandSeparator() {
		return this.thousandSeparator;
	}

	public void setThousandSeparator(TNumberSeparator thousandSeparator) {
		this.thousandSeparator = thousandSeparator;
	}

	public LanguageCodes getUserLanguage() {
		return this.userLanguage;
	}

	public void setUserLanguage(LanguageCodes userLanguage) {
		this.userLanguage = userLanguage;
	}

	public DateFormat getDateFormat() {
		return this.dateFormat;
	}

	public void setDateFormat(DateFormat dateFormat) {
		this.dateFormat = dateFormat;
	}

	public Collection<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	public Collection<UserGroup> getGroups() {
		return this.groups;
	}

	public void setGroups(Collection<UserGroup> groups) {
		this.groups = groups;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.locked == null) {
			this.locked = new Boolean(false);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
