/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.scheduler;

import atraxo.ad.domain.impl.scheduler.JobInstance;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.ReadOnly;
import seava.j4e.api.Constants;
import seava.j4e.api.model.IModelWithId;

/**
 * Entity class for {@link JobExecution} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = JobExecution.TABLE_NAME)
@ReadOnly
@Cache(type = CacheType.NONE)
public class JobExecution implements Serializable, IModelWithId<Long> {

	public static final String TABLE_NAME = "BATCH_JOB_EXECUTION";

	private static final long serialVersionUID = -8865917134914502125L;

	@Id
	@GeneratedValue(generator = Constants.UUID_GENERATOR_NAME)
	@NotNull
	@Column(name = "job_execution_id", nullable = false, precision = 20)
	private Long id;

	@Version
	@Column(name = "version", precision = 20)
	private Long version;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time", nullable = false)
	private Date createTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_time")
	private Date startTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_time")
	private Date endTime;

	@Column(name = "status", length = 10)
	private String status;

	@Column(name = "exit_code", length = 2500)
	private String exitCode;

	@Column(name = "exit_message", length = 2500)
	private String exitMessage;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_updated")
	private Date lastUpdated;

	@Column(name = "job_configuration_location", length = 2500)
	private String jobConfigurationLocation;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = JobInstance.class)
	@JoinColumn(name = "job_instance_id", referencedColumnName = "job_instance_id")
	private JobInstance jobInstance;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getExitCode() {
		return this.exitCode;
	}

	public void setExitCode(String exitCode) {
		this.exitCode = exitCode;
	}

	public String getExitMessage() {
		return this.exitMessage;
	}

	public void setExitMessage(String exitMessage) {
		this.exitMessage = exitMessage;
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getJobConfigurationLocation() {
		return this.jobConfigurationLocation;
	}

	public void setJobConfigurationLocation(String jobConfigurationLocation) {
		this.jobConfigurationLocation = jobConfigurationLocation;
	}

	public JobInstance getJobInstance() {
		return this.jobInstance;
	}

	public void setJobInstance(JobInstance jobInstance) {
		this.jobInstance = jobInstance;
	}

	/**
	 * called before persist
	 */
	@PrePersist
	public void prePersist() {
	}

	/**
	 * called before update
	 */
	@PreUpdate
	public void preUpdate() {
	}

}
