/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.scheduler;

import atraxo.abstracts.domain.impl.tenant.AbstractType;
import atraxo.ad.domain.impl.ad.TTimerIntervalType;
import atraxo.ad.domain.impl.ad.TTimerIntervalTypeConverter;
import atraxo.ad.domain.impl.ad.TTimerType;
import atraxo.ad.domain.impl.ad.TTimerTypeConverter;
import atraxo.ad.domain.impl.scheduler.JobContext;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link JobTimer} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = JobTimer.NQ_FIND_BY_NAME, query = "SELECT e FROM JobTimer e WHERE e.clientId = :clientId and e.jobContext = :jobContext and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = JobTimer.NQ_FIND_BY_NAME_PRIMITIVE, query = "SELECT e FROM JobTimer e WHERE e.clientId = :clientId and e.jobContext.id = :jobContextId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = JobTimer.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = JobTimer.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "jobcontext_id", "name"})})
public class JobTimer extends AbstractType implements Serializable {

	public static final String TABLE_NAME = "AD_JOBTRG";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "JobTimer.findByName";
	/**
	 * Named query find by unique key: Name using the ID field for references.
	 */
	public static final String NQ_FIND_BY_NAME_PRIMITIVE = "JobTimer.findByName_PRIMITIVE";

	@NotBlank
	@Column(name = "type", nullable = false, length = 16)
	@Convert(converter = TTimerTypeConverter.class)
	private TTimerType type;

	@Column(name = "cronexpression", length = 1000)
	private String cronExpression;

	@Column(name = "repeatcount", precision = 8)
	private Integer repeatCount;

	@Column(name = "repeatinterval", precision = 4)
	private Integer repeatInterval;

	@Column(name = "repeatintervaltype", length = 16)
	@Convert(converter = TTimerIntervalTypeConverter.class)
	private TTimerIntervalType repeatIntervalType;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "starttime", nullable = false)
	private Date startTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "endtime")
	private Date endTime;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = JobContext.class)
	@JoinColumn(name = "jobcontext_id", referencedColumnName = "id")
	private JobContext jobContext;

	public TTimerType getType() {
		return this.type;
	}

	public void setType(TTimerType type) {
		this.type = type;
	}

	public String getCronExpression() {
		return this.cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public Integer getRepeatCount() {
		return this.repeatCount;
	}

	public void setRepeatCount(Integer repeatCount) {
		this.repeatCount = repeatCount;
	}

	public Integer getRepeatInterval() {
		return this.repeatInterval;
	}

	public void setRepeatInterval(Integer repeatInterval) {
		this.repeatInterval = repeatInterval;
	}

	public TTimerIntervalType getRepeatIntervalType() {
		return this.repeatIntervalType;
	}

	public void setRepeatIntervalType(TTimerIntervalType repeatIntervalType) {
		this.repeatIntervalType = repeatIntervalType;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public JobContext getJobContext() {
		return this.jobContext;
	}

	public void setJobContext(JobContext jobContext) {
		if (jobContext != null) {
			this.__validate_client_context__(jobContext.getClientId());
		}
		this.jobContext = jobContext;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
