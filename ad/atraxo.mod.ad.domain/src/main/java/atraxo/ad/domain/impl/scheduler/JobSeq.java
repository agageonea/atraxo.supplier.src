/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.scheduler;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.ReadOnly;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;
import seava.j4e.api.Constants;
import seava.j4e.api.model.IModelWithId;

/**
 * Entity class for {@link JobSeq} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = JobSeq.NQ_FIND_BY_UN, query = "SELECT e FROM JobSeq e WHERE e.uniqueKey = :uniqueKey", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = JobSeq.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = JobSeq.TABLE_NAME
		+ "_UK1", columnNames = {"unique_key"})})
@ReadOnly
@Cache(type = CacheType.NONE)
public class JobSeq implements Serializable, IModelWithId<Long> {

	public static final String TABLE_NAME = "BATCH_JOB_SEQ";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Un.
	 */
	public static final String NQ_FIND_BY_UN = "JobSeq.findByUn";

	@Id
	@GeneratedValue(generator = Constants.UUID_GENERATOR_NAME)
	@NotNull
	@Column(name = "id", nullable = false, precision = 20)
	private Long id;

	@NotBlank
	@Column(name = "unique_key", nullable = false, length = 1)
	private String uniqueKey;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getUniqueKey() {
		return this.uniqueKey;
	}

	public void setUniqueKey(String uniqueKey) {
		this.uniqueKey = uniqueKey;
	}

	/**
	 * called before persist
	 */
	@PrePersist
	public void prePersist() {
	}

	/**
	 * called before update
	 */
	@PreUpdate
	public void preUpdate() {
	}

}
