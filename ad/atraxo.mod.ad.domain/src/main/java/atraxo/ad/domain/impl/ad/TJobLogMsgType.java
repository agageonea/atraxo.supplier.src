/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TJobLogMsgType {

	_EMPTY_(""), _INFO_("info"), _ERROR_("error"), _WARNING_("warning");

	private String name;

	private TJobLogMsgType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TJobLogMsgType getByName(String name) {
		for (TJobLogMsgType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TJobLogMsgType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
