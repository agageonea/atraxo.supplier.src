/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.scheduler;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.ReadOnly;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;
import seava.j4e.api.Constants;
import seava.j4e.api.model.IModelWithId;

/**
 * Entity class for {@link JobInstance} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = JobInstance.NQ_FIND_BY_UN, query = "SELECT e FROM JobInstance e WHERE e.jobName = :jobName and e.jobKey = :jobKey", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = JobInstance.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = JobInstance.TABLE_NAME
		+ "_UK1", columnNames = {"job_name", "job_key"})})
@ReadOnly
@Cache(type = CacheType.NONE)
public class JobInstance implements Serializable, IModelWithId<Long> {

	public static final String TABLE_NAME = "BATCH_JOB_INSTANCE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Un.
	 */
	public static final String NQ_FIND_BY_UN = "JobInstance.findByUn";

	@Id
	@GeneratedValue(generator = Constants.UUID_GENERATOR_NAME)
	@NotNull
	@Column(name = "job_instance_id", nullable = false, precision = 20)
	private Long id;

	@Version
	@Column(name = "version", precision = 20)
	private Long version;

	@NotBlank
	@Column(name = "job_name", nullable = false, length = 100)
	private String jobName;

	@NotBlank
	@Column(name = "job_key", nullable = false, length = 32)
	private String jobKey;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getJobName() {
		return this.jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobKey() {
		return this.jobKey;
	}

	public void setJobKey(String jobKey) {
		this.jobKey = jobKey;
	}

	/**
	 * called before persist
	 */
	@PrePersist
	public void prePersist() {
	}

	/**
	 * called before update
	 */
	@PreUpdate
	public void preUpdate() {
	}

}
