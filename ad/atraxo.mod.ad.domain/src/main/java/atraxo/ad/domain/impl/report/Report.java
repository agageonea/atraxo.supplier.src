/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.report;

import atraxo.abstracts.domain.impl.tenant.AbstractTypeWithCode;
import atraxo.ad.domain.impl.report.ReportParam;
import atraxo.ad.domain.impl.report.ReportServer;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link Report} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Report.NQ_FIND_BY_CODE, query = "SELECT e FROM Report e WHERE e.clientId = :clientId and e.code = :code", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Report.NQ_FIND_BY_NAME, query = "SELECT e FROM Report e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Report.TABLE_NAME, uniqueConstraints = {
		@UniqueConstraint(name = Report.TABLE_NAME + "_UK1", columnNames = {
				"clientid", "code"}),
		@UniqueConstraint(name = Report.TABLE_NAME + "_UK2", columnNames = {
				"clientid", "name"})})
public class Report extends AbstractTypeWithCode implements Serializable {

	public static final String TABLE_NAME = "AD_RPT";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "Report.findByCode";
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "Report.findByName";

	@Column(name = "contextpath", length = 255)
	private String contextPath;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ReportServer.class)
	@JoinColumn(name = "reportserver_id", referencedColumnName = "id")
	private ReportServer reportServer;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = ReportParam.class, mappedBy = "report")
	private Collection<ReportParam> reportParameters;

	public String getContextPath() {
		return this.contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	public ReportServer getReportServer() {
		return this.reportServer;
	}

	public void setReportServer(ReportServer reportServer) {
		if (reportServer != null) {
			this.__validate_client_context__(reportServer.getClientId());
		}
		this.reportServer = reportServer;
	}

	public Collection<ReportParam> getReportParameters() {
		return this.reportParameters;
	}

	public void setReportParameters(Collection<ReportParam> reportParameters) {
		this.reportParameters = reportParameters;
	}

	/**
	 * @param e
	 */
	public void addToReportParameters(ReportParam e) {
		if (this.reportParameters == null) {
			this.reportParameters = new ArrayList<>();
		}
		e.setReport(this);
		this.reportParameters.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
