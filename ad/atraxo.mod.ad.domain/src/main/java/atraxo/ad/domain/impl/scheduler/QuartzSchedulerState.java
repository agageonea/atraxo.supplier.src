/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.scheduler;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.ReadOnly;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link QuartzSchedulerState} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = QuartzSchedulerState.TABLE_NAME)
@ReadOnly
@Cache(type = CacheType.NONE)
public class QuartzSchedulerState implements Serializable {

	public static final String TABLE_NAME = "QRTZ_SCHEDULER_STATE";

	private static final long serialVersionUID = -8865917134914502125L;

	@Id
	@NotBlank
	@Column(name = "sched_name", nullable = false, length = 255)
	private String schedulerName;

	@Id
	@NotBlank
	@Column(name = "instance_name", nullable = false, length = 255)
	private String instanceName;

	@Column(name = "last_checkin_time", precision = 13)
	private Long lastCheckinTime;

	@Column(name = "checkin_interval", precision = 13)
	private Long checkinInterval;

	@Transient
	private Long version;

	public String getSchedulerName() {
		return this.schedulerName;
	}

	public void setSchedulerName(String schedulerName) {
		this.schedulerName = schedulerName;
	}

	public String getInstanceName() {
		return this.instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public Long getLastCheckinTime() {
		return this.lastCheckinTime;
	}

	public void setLastCheckinTime(Long lastCheckinTime) {
		this.lastCheckinTime = lastCheckinTime;
	}

	public Long getCheckinInterval() {
		return this.checkinInterval;
	}

	public void setCheckinInterval(Long checkinInterval) {
		this.checkinInterval = checkinInterval;
	}

	public Long getVersion() {
		return 1L;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * called before persist
	 */
	@PrePersist
	public void prePersist() {
	}

	/**
	 * called before update
	 */
	@PreUpdate
	public void preUpdate() {
	}

}
