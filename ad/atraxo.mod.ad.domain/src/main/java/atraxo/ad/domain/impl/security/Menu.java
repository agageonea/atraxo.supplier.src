/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.security;

import atraxo.abstracts.domain.impl.tenant.AbstractType;
import atraxo.ad.domain.impl.ad.TMenuTag;
import atraxo.ad.domain.impl.ad.TMenuTagConverter;
import atraxo.ad.domain.impl.security.Role;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Menu} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = Menu.NQ_FIND_BY_NAME, query = "SELECT e FROM Menu e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Menu.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Menu.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "name"})})
public class Menu extends AbstractType implements Serializable {

	public static final String TABLE_NAME = "AD_MENU";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "Menu.findByName";

	@NotNull
	@Column(name = "sequenceno", nullable = false, precision = 4)
	private Integer sequenceNo;

	@NotBlank
	@Column(name = "title", nullable = false, length = 255)
	private String title;

	@Column(name = "tag", length = 32)
	@Convert(converter = TMenuTagConverter.class)
	private TMenuTag tag;

	@Column(name = "glyph", length = 64)
	private String glyph;

	@ManyToMany(mappedBy = "menus")
	private Collection<Role> roles;

	public Integer getSequenceNo() {
		return this.sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public TMenuTag getTag() {
		return this.tag;
	}

	public void setTag(TMenuTag tag) {
		this.tag = tag;
	}

	public String getGlyph() {
		return this.glyph;
	}

	public void setGlyph(String glyph) {
		this.glyph = glyph;
	}

	public Collection<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
