/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum Project {

	_EMPTY_(""), _SUPPLIERONE_("SupplierOne"), _MARKETPLACE_("MarketPlace");

	private String name;

	private Project(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static Project getByName(String name) {
		for (Project status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Project with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
