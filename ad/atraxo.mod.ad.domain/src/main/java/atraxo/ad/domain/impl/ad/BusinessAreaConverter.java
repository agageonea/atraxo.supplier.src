/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link BusinessArea} enum.
 * Generated code. Do not modify in this file.
 */
public class BusinessAreaConverter
		implements
			AttributeConverter<BusinessArea, String> {

	@Override
	public String convertToDatabaseColumn(BusinessArea value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null BusinessArea.");
		}
		return value.getName();
	}

	@Override
	public BusinessArea convertToEntityAttribute(String value) {
		return BusinessArea.getByName(value);
	}

}
