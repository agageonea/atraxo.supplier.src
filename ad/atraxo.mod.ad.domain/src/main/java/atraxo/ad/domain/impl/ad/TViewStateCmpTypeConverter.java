/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TViewStateCmpType} enum.
 * Generated code. Do not modify in this file.
 */
public class TViewStateCmpTypeConverter
		implements
			AttributeConverter<TViewStateCmpType, String> {

	@Override
	public String convertToDatabaseColumn(TViewStateCmpType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TViewStateCmpType.");
		}
		return value.getName();
	}

	@Override
	public TViewStateCmpType convertToEntityAttribute(String value) {
		return TViewStateCmpType.getByName(value);
	}

}
