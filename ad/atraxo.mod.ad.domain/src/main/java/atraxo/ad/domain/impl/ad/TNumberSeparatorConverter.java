/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TNumberSeparator} enum.
 * Generated code. Do not modify in this file.
 */
public class TNumberSeparatorConverter
		implements
			AttributeConverter<TNumberSeparator, String> {

	@Override
	public String convertToDatabaseColumn(TNumberSeparator value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TNumberSeparator.");
		}
		return value.getName();
	}

	@Override
	public TNumberSeparator convertToEntityAttribute(String value) {
		return TNumberSeparator.getByName(value);
	}

}
