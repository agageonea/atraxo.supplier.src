/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.system;

import atraxo.abstracts.domain.impl.tenant.AbstractType;
import atraxo.ad.domain.impl.ad.TViewStateCmpType;
import atraxo.ad.domain.impl.ad.TViewStateCmpTypeConverter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link ViewState} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = ViewState.TABLE_NAME)
public class ViewState extends AbstractType implements Serializable {

	public static final String TABLE_NAME = "AD_VIEW_STATE";

	private static final long serialVersionUID = -8865917134914502125L;

	@NotBlank
	@Column(name = "cmp", nullable = false, length = 255)
	private String cmp;

	@NotBlank
	@Column(name = "cmptype", nullable = false, length = 16)
	@Convert(converter = TViewStateCmpTypeConverter.class)
	private TViewStateCmpType cmpType;

	@Column(name = "value", length = 4000)
	private String value;

	public String getCmp() {
		return this.cmp;
	}

	public void setCmp(String cmp) {
		this.cmp = cmp;
	}

	public TViewStateCmpType getCmpType() {
		return this.cmpType;
	}

	public void setCmpType(TViewStateCmpType cmpType) {
		this.cmpType = cmpType;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
