/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TMenuTag} enum.
 * Generated code. Do not modify in this file.
 */
public class TMenuTagConverter implements AttributeConverter<TMenuTag, String> {

	@Override
	public String convertToDatabaseColumn(TMenuTag value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TMenuTag.");
		}
		return value.getName();
	}

	@Override
	public TMenuTag convertToEntityAttribute(String value) {
		return TMenuTag.getByName(value);
	}

}
