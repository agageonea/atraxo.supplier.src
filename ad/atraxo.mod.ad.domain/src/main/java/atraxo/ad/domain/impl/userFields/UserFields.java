/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.userFields;

import atraxo.abstracts.domain.impl.tenant.AbstractAuditable;
import atraxo.ad.domain.impl.ad.UserFieldType;
import atraxo.ad.domain.impl.ad.UserFieldTypeConverter;
import atraxo.ad.domain.impl.businessArea.BusinessArea;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link UserFields} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = UserFields.NQ_FIND_BY_NAME, query = "SELECT e FROM UserFields e WHERE e.clientId = :clientId and e.name = :name and e.businessArea = :businessArea", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = UserFields.NQ_FIND_BY_NAME_PRIMITIVE, query = "SELECT e FROM UserFields e WHERE e.clientId = :clientId and e.name = :name and e.businessArea.id = :businessAreaId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = UserFields.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = UserFields.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "name", "business_area_id"})})
public class UserFields extends AbstractAuditable implements Serializable {

	public static final String TABLE_NAME = "AD_USER_FIELDS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "UserFields.findByName";
	/**
	 * Named query find by unique key: Name using the ID field for references.
	 */
	public static final String NQ_FIND_BY_NAME_PRIMITIVE = "UserFields.findByName_PRIMITIVE";

	@Column(name = "name", length = 64)
	private String name;

	@Column(name = "description", length = 1000)
	private String description;

	@Column(name = "default_value", length = 64)
	private String defaultValue;

	@Column(name = "type", length = 50)
	@Convert(converter = UserFieldTypeConverter.class)
	private UserFieldType type;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = BusinessArea.class)
	@JoinColumn(name = "business_area_id", referencedColumnName = "id")
	private BusinessArea businessArea;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public UserFieldType getType() {
		return this.type;
	}

	public void setType(UserFieldType type) {
		this.type = type;
	}

	public BusinessArea getBusinessArea() {
		return this.businessArea;
	}

	public void setBusinessArea(BusinessArea businessArea) {
		if (businessArea != null) {
			this.__validate_client_context__(businessArea.getClientId());
		}
		this.businessArea = businessArea;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.name == null) ? 0 : this.name.hashCode());
		result = prime
				* result
				+ ((this.businessArea == null) ? 0 : this.businessArea
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		UserFields other = (UserFields) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		if (this.businessArea == null) {
			if (other.businessArea != null) {
				return false;
			}
		} else if (!this.businessArea.equals(other.businessArea)) {
			return false;
		}
		return true;
	}
}
