/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TAttachmentType {

	_EMPTY_(""), _LINK_("link"), _UPLOAD_("upload");

	private String name;

	private TAttachmentType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TAttachmentType getByName(String name) {
		for (TAttachmentType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TAttachmentType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
