/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.scheduler;

import atraxo.abstracts.domain.impl.tenant.AbstractAuditable;
import atraxo.ad.domain.impl.ad.TJobLogMsgType;
import atraxo.ad.domain.impl.ad.TJobLogMsgTypeConverter;
import atraxo.ad.domain.impl.scheduler.JobLog;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link JobLogMessage} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = JobLogMessage.TABLE_NAME)
public class JobLogMessage extends AbstractAuditable implements Serializable {

	public static final String TABLE_NAME = "AD_JOBLOG_MSG";

	private static final long serialVersionUID = -8865917134914502125L;

	@NotBlank
	@Column(name = "messagetype", nullable = false, length = 16)
	@Convert(converter = TJobLogMsgTypeConverter.class)
	private TJobLogMsgType messageType;

	@Column(name = "message", length = 4000)
	private String message;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = JobLog.class)
	@JoinColumn(name = "joblog_id", referencedColumnName = "id")
	private JobLog jobLog;

	public TJobLogMsgType getMessageType() {
		return this.messageType;
	}

	public void setMessageType(TJobLogMsgType messageType) {
		this.messageType = messageType;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public JobLog getJobLog() {
		return this.jobLog;
	}

	public void setJobLog(JobLog jobLog) {
		if (jobLog != null) {
			this.__validate_client_context__(jobLog.getClientId());
		}
		this.jobLog = jobLog;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
