/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.userFields;

import atraxo.abstracts.domain.impl.tenant.AbstractAuditable;
import atraxo.ad.domain.impl.userFields.UserFields;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link UserFieldValues} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = UserFieldValues.TABLE_NAME)
public class UserFieldValues extends AbstractAuditable implements Serializable {

	public static final String TABLE_NAME = "AD_USER_FIELD_VALUES";

	private static final long serialVersionUID = -8865917134914502125L;

	@Column(name = "value", length = 1000)
	private String value;

	@NotNull
	@Column(name = "targetrefid", nullable = false, precision = 11)
	private Integer targetRefid;

	@NotBlank
	@Column(name = "targetalias", nullable = false, length = 32)
	private String targetAlias;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = UserFields.class)
	@JoinColumn(name = "user_field_id", referencedColumnName = "id")
	private UserFields userField;

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getTargetRefid() {
		return this.targetRefid;
	}

	public void setTargetRefid(Integer targetRefid) {
		this.targetRefid = targetRefid;
	}

	public String getTargetAlias() {
		return this.targetAlias;
	}

	public void setTargetAlias(String targetAlias) {
		this.targetAlias = targetAlias;
	}

	public UserFields getUserField() {
		return this.userField;
	}

	public void setUserField(UserFields userField) {
		if (userField != null) {
			this.__validate_client_context__(userField.getClientId());
		}
		this.userField = userField;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
