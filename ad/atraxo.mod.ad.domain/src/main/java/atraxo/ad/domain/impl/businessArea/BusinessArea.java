/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.businessArea;

import atraxo.abstracts.domain.impl.tenant.AbstractAuditable;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link BusinessArea} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = BusinessArea.NQ_FIND_BY_NAME, query = "SELECT e FROM BusinessArea e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = BusinessArea.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = BusinessArea.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "name"})})
public class BusinessArea extends AbstractAuditable implements Serializable {

	public static final String TABLE_NAME = "AD_BUSINESS_AREAS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "BusinessArea.findByName";

	@NotBlank
	@Column(name = "name", nullable = false, length = 255)
	private String name;

	@NotBlank
	@Column(name = "ds_name", nullable = false, length = 255)
	private String dsName;

	@NotBlank
	@Column(name = "ds_model", nullable = false, length = 255)
	private String dsModel;

	@NotBlank
	@Column(name = "ds_alias", nullable = false, length = 255)
	private String dsAlias;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDsName() {
		return this.dsName;
	}

	public void setDsName(String dsName) {
		this.dsName = dsName;
	}

	public String getDsModel() {
		return this.dsModel;
	}

	public void setDsModel(String dsModel) {
		this.dsModel = dsModel;
	}

	public String getDsAlias() {
		return this.dsAlias;
	}

	public void setDsAlias(String dsAlias) {
		this.dsAlias = dsAlias;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
