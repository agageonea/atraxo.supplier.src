/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.externalReport;

import atraxo.abstracts.domain.impl.tenant.AbstractAuditable;
import atraxo.ad.domain.impl.ad.ReportFormat;
import atraxo.ad.domain.impl.ad.ReportFormatConverter;
import atraxo.ad.domain.impl.ad.ReportStatus;
import atraxo.ad.domain.impl.ad.ReportStatusConverter;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link ReportLog} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = ReportLog.TABLE_NAME)
public class ReportLog extends AbstractAuditable implements Serializable {

	public static final String TABLE_NAME = "AD_REPORTS_LOG";

	private static final long serialVersionUID = -8865917134914502125L;

	@NotBlank
	@Column(name = "name", nullable = false, length = 255)
	private String name;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "run_on", nullable = false)
	private Date runDate;

	@NotBlank
	@Column(name = "format", nullable = false, length = 32)
	@Convert(converter = ReportFormatConverter.class)
	private ReportFormat format;

	@Column(name = "status", length = 32)
	@Convert(converter = ReportStatusConverter.class)
	private ReportStatus status;

	@Column(name = "log", length = 1000)
	private String log;

	@Column(name = "report_id", precision = 10)
	private Integer reportId;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ExternalReport.class)
	@JoinColumn(name = "external_report_id", referencedColumnName = "id")
	private ExternalReport externalReport;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getRunDate() {
		return this.runDate;
	}

	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}

	public ReportFormat getFormat() {
		return this.format;
	}

	public void setFormat(ReportFormat format) {
		this.format = format;
	}

	public ReportStatus getStatus() {
		return this.status;
	}

	public void setStatus(ReportStatus status) {
		this.status = status;
	}

	public String getLog() {
		return this.log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public Integer getReportId() {
		return this.reportId;
	}

	public void setReportId(Integer reportId) {
		this.reportId = reportId;
	}

	public ExternalReport getExternalReport() {
		return this.externalReport;
	}

	public void setExternalReport(ExternalReport externalReport) {
		if (externalReport != null) {
			this.__validate_client_context__(externalReport.getClientId());
		}
		this.externalReport = externalReport;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
