/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TTimerType} enum.
 * Generated code. Do not modify in this file.
 */
public class TTimerTypeConverter
		implements
			AttributeConverter<TTimerType, String> {

	@Override
	public String convertToDatabaseColumn(TTimerType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TTimerType.");
		}
		return value.getName();
	}

	@Override
	public TTimerType convertToEntityAttribute(String value) {
		return TTimerType.getByName(value);
	}

}
