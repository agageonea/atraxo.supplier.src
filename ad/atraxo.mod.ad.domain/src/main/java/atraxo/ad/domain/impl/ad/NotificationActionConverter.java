/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link NotificationAction} enum.
 * Generated code. Do not modify in this file.
 */
public class NotificationActionConverter
		implements
			AttributeConverter<NotificationAction, String> {

	@Override
	public String convertToDatabaseColumn(NotificationAction value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null NotificationAction.");
		}
		return value.getName();
	}

	@Override
	public NotificationAction convertToEntityAttribute(String value) {
		return NotificationAction.getByName(value);
	}

}
