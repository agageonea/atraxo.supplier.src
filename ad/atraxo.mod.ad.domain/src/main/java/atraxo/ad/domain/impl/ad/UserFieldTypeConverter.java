/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link UserFieldType} enum.
 * Generated code. Do not modify in this file.
 */
public class UserFieldTypeConverter
		implements
			AttributeConverter<UserFieldType, String> {

	@Override
	public String convertToDatabaseColumn(UserFieldType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null UserFieldType.");
		}
		return value.getName();
	}

	@Override
	public UserFieldType convertToEntityAttribute(String value) {
		return UserFieldType.getByName(value);
	}

}
