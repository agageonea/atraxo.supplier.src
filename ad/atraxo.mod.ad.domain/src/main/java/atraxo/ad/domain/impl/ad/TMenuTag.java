/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TMenuTag {

	_EMPTY_(""), _TOP_("top"), _LEFT_("left");

	private String name;

	private TMenuTag(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TMenuTag getByName(String name) {
		for (TMenuTag status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TMenuTag with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
