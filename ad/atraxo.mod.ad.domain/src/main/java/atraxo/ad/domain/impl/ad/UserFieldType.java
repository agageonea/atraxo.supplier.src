/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum UserFieldType {

	_EMPTY_(""), _STRING_("String"), _NUMERIC_("Numeric"), _DATETIME_(
			"DateTime");

	private String name;

	private UserFieldType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static UserFieldType getByName(String name) {
		for (UserFieldType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent UserFieldType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
