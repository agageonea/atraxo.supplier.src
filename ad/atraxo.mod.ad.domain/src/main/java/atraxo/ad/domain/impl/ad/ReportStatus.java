/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ReportStatus {

	_EMPTY_(""), _SUCCESS_("Success"), _IN_PROGRESS_("In progress"), _FAILED_(
			"Failed");

	private String name;

	private ReportStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ReportStatus getByName(String name) {
		for (ReportStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent ReportStatus with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
