/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.report;

import atraxo.abstracts.domain.impl.tenant.AbstractAuditable;
import atraxo.ad.domain.impl.report.DsReport;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link DsReportUsage} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = DsReportUsage.NQ_FIND_BY_REPORTUSAGE_DS, query = "SELECT e FROM DsReportUsage e WHERE e.clientId = :clientId and e.dsReport = :dsReport", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = DsReportUsage.NQ_FIND_BY_REPORTUSAGE_DS_PRIMITIVE, query = "SELECT e FROM DsReportUsage e WHERE e.clientId = :clientId and e.dsReport.id = :dsReportId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = DsReportUsage.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = DsReportUsage.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "dsreport_id"})})
public class DsReportUsage extends AbstractAuditable implements Serializable {

	public static final String TABLE_NAME = "AD_RPT_DS_USAGE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: ReportUsage_ds.
	 */
	public static final String NQ_FIND_BY_REPORTUSAGE_DS = "DsReportUsage.findByReportUsage_ds";
	/**
	 * Named query find by unique key: ReportUsage_ds using the ID field for references.
	 */
	public static final String NQ_FIND_BY_REPORTUSAGE_DS_PRIMITIVE = "DsReportUsage.findByReportUsage_ds_PRIMITIVE";

	@Column(name = "framename", length = 255)
	private String frameName;

	@Column(name = "toolbarkey", length = 1000)
	private String toolbarKey;

	@Column(name = "dckey", length = 1000)
	private String dcKey;

	@Column(name = "sequenceno", precision = 4)
	private Integer sequenceNo;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = DsReport.class)
	@JoinColumn(name = "dsreport_id", referencedColumnName = "id")
	private DsReport dsReport;

	public String getFrameName() {
		return this.frameName;
	}

	public void setFrameName(String frameName) {
		this.frameName = frameName;
	}

	public String getToolbarKey() {
		return this.toolbarKey;
	}

	public void setToolbarKey(String toolbarKey) {
		this.toolbarKey = toolbarKey;
	}

	public String getDcKey() {
		return this.dcKey;
	}

	public void setDcKey(String dcKey) {
		this.dcKey = dcKey;
	}

	public Integer getSequenceNo() {
		return this.sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public DsReport getDsReport() {
		return this.dsReport;
	}

	public void setDsReport(DsReport dsReport) {
		if (dsReport != null) {
			this.__validate_client_context__(dsReport.getClientId());
		}
		this.dsReport = dsReport;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
