/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.scheduler;

import atraxo.abstracts.domain.impl.tenant.AbstractAuditable;
import atraxo.ad.domain.impl.scheduler.JobContext;
import atraxo.ad.domain.impl.scheduler.JobLogMessage;
import atraxo.ad.domain.impl.scheduler.JobTimer;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * Entity class for {@link JobLog} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = JobLog.TABLE_NAME)
public class JobLog extends AbstractAuditable implements Serializable {

	public static final String TABLE_NAME = "AD_JOBLOG";

	private static final long serialVersionUID = -8865917134914502125L;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "starttime", nullable = false)
	private Date startTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "endtime")
	private Date endTime;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = JobContext.class)
	@JoinColumn(name = "jobcontext_id", referencedColumnName = "id")
	private JobContext jobContext;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = JobTimer.class)
	@JoinColumn(name = "jobtimer_id", referencedColumnName = "id")
	private JobTimer jobTimer;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = JobLogMessage.class, mappedBy = "jobLog", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<JobLogMessage> messages;

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public JobContext getJobContext() {
		return this.jobContext;
	}

	public void setJobContext(JobContext jobContext) {
		if (jobContext != null) {
			this.__validate_client_context__(jobContext.getClientId());
		}
		this.jobContext = jobContext;
	}
	public JobTimer getJobTimer() {
		return this.jobTimer;
	}

	public void setJobTimer(JobTimer jobTimer) {
		if (jobTimer != null) {
			this.__validate_client_context__(jobTimer.getClientId());
		}
		this.jobTimer = jobTimer;
	}

	public Collection<JobLogMessage> getMessages() {
		return this.messages;
	}

	public void setMessages(Collection<JobLogMessage> messages) {
		this.messages = messages;
	}

	/**
	 * @param e
	 */
	public void addToMessages(JobLogMessage e) {
		if (this.messages == null) {
			this.messages = new ArrayList<>();
		}
		e.setJobLog(this);
		this.messages.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
