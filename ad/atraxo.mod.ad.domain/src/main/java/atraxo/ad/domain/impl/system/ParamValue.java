/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.system;

import atraxo.abstracts.domain.impl.tenant.AbstractAuditable;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link ParamValue} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = ParamValue.TABLE_NAME)
public class ParamValue extends AbstractAuditable implements Serializable {

	public static final String TABLE_NAME = "AD_PARAMVAL";

	private static final long serialVersionUID = -8865917134914502125L;

	@NotBlank
	@Column(name = "sysparam", nullable = false, length = 64)
	private String sysParam;

	@Column(name = "value", length = 1000)
	private String value;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "validfrom", nullable = false)
	private Date validFrom;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "validto", nullable = false)
	private Date validTo;

	public String getSysParam() {
		return this.sysParam;
	}

	public void setSysParam(String sysParam) {
		this.sysParam = sysParam;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
