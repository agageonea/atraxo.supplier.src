/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TNumberSeparator {

	_EMPTY_(""), _POINT_("."), _COMMA_(",");

	private String name;

	private TNumberSeparator(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TNumberSeparator getByName(String name) {
		for (TNumberSeparator status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent TNumberSeparator with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
