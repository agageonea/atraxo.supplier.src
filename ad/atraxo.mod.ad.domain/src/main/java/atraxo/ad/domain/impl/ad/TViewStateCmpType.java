/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TViewStateCmpType {

	_EMPTY_(""), _FRAME_DCGRID_("frame-dcgrid"), _FRAME_DCEGRID_(
			"frame-dcegrid");

	private String name;

	private TViewStateCmpType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TViewStateCmpType getByName(String name) {
		for (TViewStateCmpType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent TViewStateCmpType with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
