/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.scheduler;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.ReadOnly;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link QuartzBlobTrigger} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = QuartzBlobTrigger.TABLE_NAME)
@ReadOnly
@Cache(type = CacheType.NONE)
public class QuartzBlobTrigger implements Serializable {

	public static final String TABLE_NAME = "QRTZ_BLOB_TRIGGERS";

	private static final long serialVersionUID = -8865917134914502125L;

	@Id
	@NotBlank
	@Column(name = "sched_name", nullable = false, length = 255)
	private String schedulerName;

	@Id
	@NotBlank
	@Column(name = "trigger_name", nullable = false, length = 255)
	private String triggerName;

	@Id
	@NotBlank
	@Column(name = "trigger_group", nullable = false, length = 255)
	private String triggerGroup;

	@Lob
	@Column(name = "blob_data")
	private byte[] blobData;

	@Transient
	private Long version;

	public String getSchedulerName() {
		return this.schedulerName;
	}

	public void setSchedulerName(String schedulerName) {
		this.schedulerName = schedulerName;
	}

	public String getTriggerName() {
		return this.triggerName;
	}

	public void setTriggerName(String triggerName) {
		this.triggerName = triggerName;
	}

	public String getTriggerGroup() {
		return this.triggerGroup;
	}

	public void setTriggerGroup(String triggerGroup) {
		this.triggerGroup = triggerGroup;
	}

	public byte[] getBlobData() {
		return this.blobData;
	}

	public void setBlobData(byte[] blobData) {
		this.blobData = blobData;
	}

	public Long getVersion() {
		return 1L;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * called before persist
	 */
	@PrePersist
	public void prePersist() {
	}

	/**
	 * called before update
	 */
	@PreUpdate
	public void preUpdate() {
	}

}
