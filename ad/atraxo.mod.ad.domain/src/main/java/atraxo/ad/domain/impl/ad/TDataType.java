/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TDataType {

	_EMPTY_(""), _STRING_("string"), _TEXT_("text"), _INTEGER_("integer"), _DECIMAL_(
			"decimal"), _BOOLEAN_("boolean"), _DATE_("date");

	private String name;

	private TDataType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TDataType getByName(String name) {
		for (TDataType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TDataType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
