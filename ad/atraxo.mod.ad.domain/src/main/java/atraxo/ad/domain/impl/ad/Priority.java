/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum Priority {

	_EMPTY_(""), _MAX_("Max"), _HIGH_("High"), _DEFAULT_("Default");

	private String name;

	private Priority(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static Priority getByName(String name) {
		for (Priority status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Priority with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
