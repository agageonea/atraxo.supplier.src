/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TAttachmentType} enum.
 * Generated code. Do not modify in this file.
 */
public class TAttachmentTypeConverter
		implements
			AttributeConverter<TAttachmentType, String> {

	@Override
	public String convertToDatabaseColumn(TAttachmentType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TAttachmentType.");
		}
		return value.getName();
	}

	@Override
	public TAttachmentType convertToEntityAttribute(String value) {
		return TAttachmentType.getByName(value);
	}

}
