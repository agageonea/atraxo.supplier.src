/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.report;

import atraxo.abstracts.domain.impl.tenant.AbstractAuditable;
import atraxo.ad.domain.impl.report.DsReportParam;
import atraxo.ad.domain.impl.report.Report;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link DsReport} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = DsReport.NQ_FIND_BY_REP_DS, query = "SELECT e FROM DsReport e WHERE e.clientId = :clientId and e.report = :report and e.dataSource = :dataSource", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = DsReport.NQ_FIND_BY_REP_DS_PRIMITIVE, query = "SELECT e FROM DsReport e WHERE e.clientId = :clientId and e.report.id = :reportId and e.dataSource = :dataSource", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = DsReport.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = DsReport.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "report_id", "datasource"})})
public class DsReport extends AbstractAuditable implements Serializable {

	public static final String TABLE_NAME = "AD_RPT_DS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Rep_ds.
	 */
	public static final String NQ_FIND_BY_REP_DS = "DsReport.findByRep_ds";
	/**
	 * Named query find by unique key: Rep_ds using the ID field for references.
	 */
	public static final String NQ_FIND_BY_REP_DS_PRIMITIVE = "DsReport.findByRep_ds_PRIMITIVE";

	/** Reference to the data-source. 
	 */
	@NotBlank
	@Column(name = "datasource", nullable = false, length = 255)
	private String dataSource;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Report.class)
	@JoinColumn(name = "report_id", referencedColumnName = "id")
	private Report report;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = DsReportParam.class, mappedBy = "dsReport")
	private Collection<DsReportParam> dsReportParams;

	public String getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public Report getReport() {
		return this.report;
	}

	public void setReport(Report report) {
		if (report != null) {
			this.__validate_client_context__(report.getClientId());
		}
		this.report = report;
	}

	public Collection<DsReportParam> getDsReportParams() {
		return this.dsReportParams;
	}

	public void setDsReportParams(Collection<DsReportParam> dsReportParams) {
		this.dsReportParams = dsReportParams;
	}

	/**
	 * @param e
	 */
	public void addToDsReportParams(DsReportParam e) {
		if (this.dsReportParams == null) {
			this.dsReportParams = new ArrayList<>();
		}
		e.setDsReport(this);
		this.dsReportParams.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
