/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.system;

import atraxo.abstracts.domain.impl.tenant.AbstractAuditable;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link TargetRule} domain entity.
 * Generated code. Do not modify in this file.
 */
/** Define rules to dynamically linked objects. */
@NamedQueries({
		@NamedQuery(name = TargetRule.NQ_FIND_BY_TARGETRULE, query = "SELECT e FROM TargetRule e WHERE e.clientId = :clientId and e.sourceRefId = :sourceRefId and e.targetAlias = :targetAlias", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = TargetRule.NQ_FIND_BY_TARGETRULE_PRIMITIVE, query = "SELECT e FROM TargetRule e WHERE e.clientId = :clientId and e.sourceRefId.id = :sourceRefIdId and e.targetAlias = :targetAlias", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = TargetRule.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = TargetRule.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "sourcerefid", "targetalias"})})
public class TargetRule extends AbstractAuditable implements Serializable {

	public static final String TABLE_NAME = "AD_TARGET_RULE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: TargetRule.
	 */
	public static final String NQ_FIND_BY_TARGETRULE = "TargetRule.findByTargetRule";
	/**
	 * Named query find by unique key: TargetRule using the ID field for references.
	 */
	public static final String NQ_FIND_BY_TARGETRULE_PRIMITIVE = "TargetRule.findByTargetRule_PRIMITIVE";

	@NotBlank
	@Column(name = "targetalias", nullable = false, length = 255)
	private String targetAlias;

	@NotBlank
	@Column(name = "targettype", nullable = false, length = 255)
	private String targetType;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AttachmentType.class)
	@JoinColumn(name = "sourcerefid", referencedColumnName = "id")
	private AttachmentType sourceRefId;

	public String getTargetAlias() {
		return this.targetAlias;
	}

	public void setTargetAlias(String targetAlias) {
		this.targetAlias = targetAlias;
	}

	public String getTargetType() {
		return this.targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

	public AttachmentType getSourceRefId() {
		return this.sourceRefId;
	}

	public void setSourceRefId(AttachmentType sourceRefId) {
		if (sourceRefId != null) {
			this.__validate_client_context__(sourceRefId.getClientId());
		}
		this.sourceRefId = sourceRefId;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
