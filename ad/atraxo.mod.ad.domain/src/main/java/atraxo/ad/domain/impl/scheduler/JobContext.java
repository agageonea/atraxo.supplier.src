/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.scheduler;

import atraxo.abstracts.domain.impl.tenant.AbstractType;
import atraxo.ad.domain.impl.scheduler.JobContextParam;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link JobContext} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = JobContext.NQ_FIND_BY_NAME, query = "SELECT e FROM JobContext e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = JobContext.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = JobContext.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "name"})})
public class JobContext extends AbstractType implements Serializable {

	public static final String TABLE_NAME = "AD_JOBCTX";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "JobContext.findByName";

	@NotBlank
	@Column(name = "jobname", nullable = false, length = 255)
	private String jobName;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = JobContextParam.class, mappedBy = "jobContext", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<JobContextParam> params;

	public String getJobName() {
		return this.jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public Collection<JobContextParam> getParams() {
		return this.params;
	}

	public void setParams(Collection<JobContextParam> params) {
		this.params = params;
	}

	/**
	 * @param e
	 */
	public void addToParams(JobContextParam e) {
		if (this.params == null) {
			this.params = new ArrayList<>();
		}
		e.setJobContext(this);
		this.params.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
