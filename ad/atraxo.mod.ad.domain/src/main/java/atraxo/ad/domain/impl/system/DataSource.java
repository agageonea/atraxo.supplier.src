/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.system;

import atraxo.abstracts.domain.impl.notenant.AbstractTypeNT;
import atraxo.ad.domain.impl.system.DataSourceField;
import atraxo.ad.domain.impl.system.DataSourceRpc;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link DataSource} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = DataSource.NQ_FIND_BY_NAME, query = "SELECT e FROM DataSource e WHERE e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = DataSource.NQ_FIND_BY_MODEL, query = "SELECT e FROM DataSource e WHERE e.model = :model", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = DataSource.TABLE_NAME, uniqueConstraints = {
		@UniqueConstraint(name = DataSource.TABLE_NAME + "_UK1", columnNames = {"name"}),
		@UniqueConstraint(name = DataSource.TABLE_NAME + "_UK2", columnNames = {"model"})})
public class DataSource extends AbstractTypeNT implements Serializable {

	public static final String TABLE_NAME = "SYS_DS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "DataSource.findByName";
	/**
	 * Named query find by unique key: Model.
	 */
	public static final String NQ_FIND_BY_MODEL = "DataSource.findByModel";

	@NotBlank
	@Column(name = "model", nullable = false, length = 255)
	private String model;

	@NotNull
	@Column(name = "isasgn", nullable = false)
	private Boolean isAsgn;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = DataSourceField.class, mappedBy = "dataSource", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<DataSourceField> fields;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = DataSourceRpc.class, mappedBy = "dataSource", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<DataSourceRpc> serviceMethods;

	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Boolean getIsAsgn() {
		return this.isAsgn;
	}

	public void setIsAsgn(Boolean isAsgn) {
		this.isAsgn = isAsgn;
	}

	public Collection<DataSourceField> getFields() {
		return this.fields;
	}

	public void setFields(Collection<DataSourceField> fields) {
		this.fields = fields;
	}

	/**
	 * @param e
	 */
	public void addToFields(DataSourceField e) {
		if (this.fields == null) {
			this.fields = new ArrayList<>();
		}
		e.setDataSource(this);
		this.fields.add(e);
	}
	public Collection<DataSourceRpc> getServiceMethods() {
		return this.serviceMethods;
	}

	public void setServiceMethods(Collection<DataSourceRpc> serviceMethods) {
		this.serviceMethods = serviceMethods;
	}

	/**
	 * @param e
	 */
	public void addToServiceMethods(DataSourceRpc e) {
		if (this.serviceMethods == null) {
			this.serviceMethods = new ArrayList<>();
		}
		e.setDataSource(this);
		this.serviceMethods.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.isAsgn == null) {
			this.isAsgn = new Boolean(false);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
