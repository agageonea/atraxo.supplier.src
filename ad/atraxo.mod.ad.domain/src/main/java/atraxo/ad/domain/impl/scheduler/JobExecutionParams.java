/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.scheduler;

import atraxo.ad.domain.impl.scheduler.JobExecution;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.ReadOnly;
import org.hibernate.validator.constraints.NotBlank;
import seava.j4e.api.Constants;
import seava.j4e.api.model.IModelWithId;

/**
 * Entity class for {@link JobExecutionParams} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = JobExecutionParams.TABLE_NAME)
@ReadOnly
@Cache(type = CacheType.NONE)
public class JobExecutionParams implements Serializable, IModelWithId<Long> {

	public static final String TABLE_NAME = "BATCH_JOB_EXECUTION_PARAMS";

	private static final long serialVersionUID = -8865917134914502125L;

	@Id
	@GeneratedValue(generator = Constants.UUID_GENERATOR_NAME)
	@NotNull
	@Column(name = "id", nullable = false, precision = 20)
	private Long id;

	@NotBlank
	@Column(name = "type_cd", nullable = false, length = 6)
	private String typeCd;

	@NotBlank
	@Column(name = "key_name", nullable = false, length = 100)
	private String keyName;

	@Column(name = "string_val", length = 250)
	private String stringVal;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_val")
	private Date dateVal;

	@Column(name = "long_val", precision = 20)
	private Long longVal;

	@Column(name = "double_val", precision = 20)
	private Long doubleVal;

	@NotBlank
	@Column(name = "identifying", nullable = false, length = 1)
	private String identifying;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = JobExecution.class)
	@JoinColumn(name = "job_execution_id", referencedColumnName = "job_execution_id")
	private JobExecution jobExecution;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getTypeCd() {
		return this.typeCd;
	}

	public void setTypeCd(String typeCd) {
		this.typeCd = typeCd;
	}

	public String getKeyName() {
		return this.keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	public String getStringVal() {
		return this.stringVal;
	}

	public void setStringVal(String stringVal) {
		this.stringVal = stringVal;
	}

	public Date getDateVal() {
		return this.dateVal;
	}

	public void setDateVal(Date dateVal) {
		this.dateVal = dateVal;
	}

	public Long getLongVal() {
		return this.longVal;
	}

	public void setLongVal(Long longVal) {
		this.longVal = longVal;
	}

	public Long getDoubleVal() {
		return this.doubleVal;
	}

	public void setDoubleVal(Long doubleVal) {
		this.doubleVal = doubleVal;
	}

	public String getIdentifying() {
		return this.identifying;
	}

	public void setIdentifying(String identifying) {
		this.identifying = identifying;
	}

	public JobExecution getJobExecution() {
		return this.jobExecution;
	}

	public void setJobExecution(JobExecution jobExecution) {
		this.jobExecution = jobExecution;
	}

	/**
	 * called before persist
	 */
	@PrePersist
	public void prePersist() {
	}

	/**
	 * called before update
	 */
	@PreUpdate
	public void preUpdate() {
	}

}
