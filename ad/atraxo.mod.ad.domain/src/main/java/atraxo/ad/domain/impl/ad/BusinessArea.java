/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum BusinessArea {

	_EMPTY_("", ""), _ACCOUNT_MANAGEMENT_("Account management",
			"fmbas_Customers_Ds"), _SUPPLY_MANAGEMENT_("Supply management",
			"fmbas_Suppliers_Ds"), _SALES_CONTRACTS_("Sales contracts",
			"cmm_ContractCustomer_Ds"), _PURCHASE_CONTRACTS_(
			"Purchase contracts", "cmm_Contract_Ds"), _FUEL_TICKETS_(
			"Fuel tickets", "ops_FuelTicket_Ds"), _FUEL_EVENTS_("Fuel events",
			"acc_FuelEvents_Ds"), _SALES_INVOICES_("Sales invoices",
			"acc_OutgoingInvoice_Ds"), _PURCHASE_INVOICES_("Purchase invoices",
			"acc_Invoice_Ds");

	private String name;
	private String dsName;

	private BusinessArea(String name, String dsName) {
		this.name = name;
		this.dsName = dsName;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static BusinessArea getByName(String name) {
		for (BusinessArea status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent BusinessArea with name: "
				+ name);
	}

	public String getDsName() {
		return this.dsName;
	}

	public static BusinessArea getByDsName(String code) {
		for (BusinessArea status : values()) {
			if (status.getDsName().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent BusinessArea with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
