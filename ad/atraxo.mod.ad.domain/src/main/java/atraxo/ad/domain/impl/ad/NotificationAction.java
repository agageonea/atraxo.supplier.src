/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum NotificationAction {

	_EMPTY_(""), _NOACTION_("NoAction"), _GOTO_("GoTo");

	private String name;

	private NotificationAction(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static NotificationAction getByName(String name) {
		for (NotificationAction status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent NotificationAction with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
