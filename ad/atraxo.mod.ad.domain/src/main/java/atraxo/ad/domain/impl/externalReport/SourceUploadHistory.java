/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.externalReport;

import atraxo.abstracts.domain.impl.tenant.AbstractAuditable;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link SourceUploadHistory} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = SourceUploadHistory.TABLE_NAME)
public class SourceUploadHistory extends AbstractAuditable
		implements
			Serializable {

	public static final String TABLE_NAME = "AD_REPORTS_SOURCE_UPLOAD_HISTORY";

	private static final long serialVersionUID = -8865917134914502125L;

	@NotBlank
	@Column(name = "name", nullable = false, length = 255)
	private String name;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upload_date", nullable = false)
	private Date uploadDate;

	@Column(name = "uploader", length = 1000)
	private String uploader;

	@Column(name = "file_name", length = 255)
	private String fileName;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ExternalReport.class)
	@JoinColumn(name = "external_report_id", referencedColumnName = "id")
	private ExternalReport externalReport;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getUploadDate() {
		return this.uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getUploader() {
		return this.uploader;
	}

	public void setUploader(String uploader) {
		this.uploader = uploader;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public ExternalReport getExternalReport() {
		return this.externalReport;
	}

	public void setExternalReport(ExternalReport externalReport) {
		if (externalReport != null) {
			this.__validate_client_context__(externalReport.getClientId());
		}
		this.externalReport = externalReport;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
