/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.attachment;

import atraxo.abstracts.domain.impl.tenant.AbstractType;
import atraxo.ad.domain.impl.ad.TAttachmentType;
import atraxo.ad.domain.impl.ad.TAttachmentTypeConverter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link AttachmentType} domain entity.
 * Generated code. Do not modify in this file.
 */
/** Attachment type definition.*/
@NamedQueries({@NamedQuery(name = AttachmentType.NQ_FIND_BY_NAME, query = "SELECT e FROM AttachmentType e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = AttachmentType.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = AttachmentType.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "name"})})
public class AttachmentType extends AbstractType implements Serializable {

	public static final String TABLE_NAME = "AD_ATCH_TYPE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "AttachmentType.findByName";

	@NotBlank
	@Column(name = "category", nullable = false, length = 8)
	@Convert(converter = TAttachmentTypeConverter.class)
	private TAttachmentType category;

	/** Physical location where attachments of this type should be copied if they are uploaded. */
	@Column(name = "uploadpath", length = 1000)
	private String uploadPath;

	/** Base URL to use when the attachment is requested. Is ignored if the attachment specifies an absolute URL */
	@NotBlank
	@Column(name = "baseurl", nullable = false, length = 255)
	private String baseUrl;

	public TAttachmentType getCategory() {
		return this.category;
	}

	public void setCategory(TAttachmentType category) {
		this.category = category;
	}

	public String getUploadPath() {
		return this.uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	public String getBaseUrl() {
		return this.baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
