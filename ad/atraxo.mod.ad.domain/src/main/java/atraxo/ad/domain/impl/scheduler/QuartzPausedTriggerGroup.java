/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.scheduler;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.ReadOnly;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link QuartzPausedTriggerGroup} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = QuartzPausedTriggerGroup.TABLE_NAME)
@ReadOnly
@Cache(type = CacheType.NONE)
public class QuartzPausedTriggerGroup implements Serializable {

	public static final String TABLE_NAME = "QRTZ_PAUSED_TRIGGER_GRPS";

	private static final long serialVersionUID = -8865917134914502125L;

	@Id
	@NotBlank
	@Column(name = "sched_name", nullable = false, length = 255)
	private String schedulerName;

	@Id
	@NotBlank
	@Column(name = "trigger_group", nullable = false, length = 255)
	private String triggerGroup;

	@Transient
	private Long version;

	public String getSchedulerName() {
		return this.schedulerName;
	}

	public void setSchedulerName(String schedulerName) {
		this.schedulerName = schedulerName;
	}

	public String getTriggerGroup() {
		return this.triggerGroup;
	}

	public void setTriggerGroup(String triggerGroup) {
		this.triggerGroup = triggerGroup;
	}

	public Long getVersion() {
		return 1L;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * called before persist
	 */
	@PrePersist
	public void prePersist() {
	}

	/**
	 * called before update
	 */
	@PreUpdate
	public void preUpdate() {
	}

}
