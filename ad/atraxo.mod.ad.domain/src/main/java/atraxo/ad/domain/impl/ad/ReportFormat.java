/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ReportFormat {

	_EMPTY_(""), _PDF_("PDF"), _CSV_("CSV"), _XLSX_("XLSX"), _DOC_("DOC");

	private String name;

	private ReportFormat(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ReportFormat getByName(String name) {
		for (ReportFormat status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent ReportFormat with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
