/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.system;

import atraxo.abstracts.domain.impl.notenant.AbstractTypeWithCodeNT;
import atraxo.ad.domain.impl.ad.Project;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Client} domain entity.
 * Generated code. Do not modify in this file.
 */
/**
 * Application client(tenant) definition. 
 */
@NamedQueries({@NamedQuery(name = Client.NQ_FIND_BY_CODE, query = "SELECT e FROM Client e WHERE e.code = :code", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Client.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Client.TABLE_NAME
		+ "_UK1", columnNames = {"code"})})
public class Client extends AbstractTypeWithCodeNT implements Serializable {

	public static final String TABLE_NAME = "SYS_CLIENT";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "Client.findByCode";

	/**
	 * Specify which is the administrator role. Access rights are not checked
	 * for the users having this role granted so they have access to ALL of the
	 * functions.
	 */
	@Column(name = "adminrole", length = 32)
	private String adminRole;

	/**
	 * Client workspace location. Valid absolute directory. If it does not exist
	 * it is created.
	 */
	@NotBlank
	@Column(name = "workspacepath", nullable = false, length = 255)
	private String workspacePath;

	/**
	 * Import files path
	 */
	@NotBlank
	@Column(name = "importpath", nullable = false, length = 255)
	private String importPath;

	/**
	 * Export files path
	 */
	@NotBlank
	@Column(name = "exportpath", nullable = false, length = 255)
	private String exportPath;

	/**
	 * Temporary files path
	 */
	@NotBlank
	@Column(name = "temppath", nullable = false, length = 255)
	private String tempPath;

	@Transient
	private Project product;

	public String getAdminRole() {
		return this.adminRole;
	}

	public void setAdminRole(String adminRole) {
		this.adminRole = adminRole;
	}

	public String getWorkspacePath() {
		return this.workspacePath;
	}

	public void setWorkspacePath(String workspacePath) {
		this.workspacePath = workspacePath;
	}

	public String getImportPath() {
		return this.importPath;
	}

	public void setImportPath(String importPath) {
		this.importPath = importPath;
	}

	public String getExportPath() {
		return this.exportPath;
	}

	public void setExportPath(String exportPath) {
		this.exportPath = exportPath;
	}

	public String getTempPath() {
		return this.tempPath;
	}

	public void setTempPath(String tempPath) {
		this.tempPath = tempPath;
	}

	public Project getProduct() {
		return this.product;
	}

	public void setProduct(Project product) {
		this.product = product;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
