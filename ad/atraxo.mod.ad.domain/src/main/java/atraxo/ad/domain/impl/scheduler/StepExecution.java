/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.scheduler;

import atraxo.ad.domain.impl.scheduler.JobExecution;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.ReadOnly;
import org.hibernate.validator.constraints.NotBlank;
import seava.j4e.api.Constants;
import seava.j4e.api.model.IModelWithId;

/**
 * Entity class for {@link StepExecution} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = StepExecution.TABLE_NAME)
@ReadOnly
@Cache(type = CacheType.NONE)
public class StepExecution implements Serializable, IModelWithId<Long> {

	public static final String TABLE_NAME = "BATCH_STEP_EXECUTION";

	private static final long serialVersionUID = -8865917134914502125L;

	@Id
	@GeneratedValue(generator = Constants.UUID_GENERATOR_NAME)
	@NotNull
	@Column(name = "step_execution_id", nullable = false, precision = 20)
	private Long id;

	@Version
	@NotNull
	@Column(name = "version", nullable = false, precision = 20)
	private Long version;

	@NotBlank
	@Column(name = "step_name", nullable = false, length = 100)
	private String stepName;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_time", nullable = false)
	private Date startTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_time")
	private Date endTime;

	@Column(name = "status", length = 10)
	private String status;

	@Column(name = "commit_count", precision = 20)
	private Long commitCount;

	@Column(name = "read_count", precision = 20)
	private Long readCount;

	@Column(name = "filter_count", precision = 20)
	private Long filterCount;

	@Column(name = "write_count", precision = 20)
	private Long writeCount;

	@Column(name = "read_skip_count", precision = 20)
	private Long readSkipCount;

	@Column(name = "write_skip_count", precision = 20)
	private Long writeSkipCount;

	@Column(name = "process_skip_count", precision = 20)
	private Long processSkipCount;

	@Column(name = "rollback_count", precision = 20)
	private Long rollbackCount;

	@Column(name = "exit_code", length = 2500)
	private String exitCode;

	@Column(name = "exit_message", length = 2500)
	private String exitMessage;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_updated")
	private Date lastUpdated;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = JobExecution.class)
	@JoinColumn(name = "job_execution_id", referencedColumnName = "job_execution_id")
	private JobExecution jobExecStep;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getStepName() {
		return this.stepName;
	}

	public void setStepName(String stepName) {
		this.stepName = stepName;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getCommitCount() {
		return this.commitCount;
	}

	public void setCommitCount(Long commitCount) {
		this.commitCount = commitCount;
	}

	public Long getReadCount() {
		return this.readCount;
	}

	public void setReadCount(Long readCount) {
		this.readCount = readCount;
	}

	public Long getFilterCount() {
		return this.filterCount;
	}

	public void setFilterCount(Long filterCount) {
		this.filterCount = filterCount;
	}

	public Long getWriteCount() {
		return this.writeCount;
	}

	public void setWriteCount(Long writeCount) {
		this.writeCount = writeCount;
	}

	public Long getReadSkipCount() {
		return this.readSkipCount;
	}

	public void setReadSkipCount(Long readSkipCount) {
		this.readSkipCount = readSkipCount;
	}

	public Long getWriteSkipCount() {
		return this.writeSkipCount;
	}

	public void setWriteSkipCount(Long writeSkipCount) {
		this.writeSkipCount = writeSkipCount;
	}

	public Long getProcessSkipCount() {
		return this.processSkipCount;
	}

	public void setProcessSkipCount(Long processSkipCount) {
		this.processSkipCount = processSkipCount;
	}

	public Long getRollbackCount() {
		return this.rollbackCount;
	}

	public void setRollbackCount(Long rollbackCount) {
		this.rollbackCount = rollbackCount;
	}

	public String getExitCode() {
		return this.exitCode;
	}

	public void setExitCode(String exitCode) {
		this.exitCode = exitCode;
	}

	public String getExitMessage() {
		return this.exitMessage;
	}

	public void setExitMessage(String exitMessage) {
		this.exitMessage = exitMessage;
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public JobExecution getJobExecStep() {
		return this.jobExecStep;
	}

	public void setJobExecStep(JobExecution jobExecStep) {
		this.jobExecStep = jobExecStep;
	}

	/**
	 * called before persist
	 */
	@PrePersist
	public void prePersist() {
	}

	/**
	 * called before update
	 */
	@PreUpdate
	public void preUpdate() {
	}

}
