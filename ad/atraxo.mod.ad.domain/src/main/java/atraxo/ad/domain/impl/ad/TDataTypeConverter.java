/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TDataType} enum.
 * Generated code. Do not modify in this file.
 */
public class TDataTypeConverter
		implements
			AttributeConverter<TDataType, String> {

	@Override
	public String convertToDatabaseColumn(TDataType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TDataType.");
		}
		return value.getName();
	}

	@Override
	public TDataType convertToEntityAttribute(String value) {
		return TDataType.getByName(value);
	}

}
