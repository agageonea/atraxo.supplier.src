/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link Category} enum.
 * Generated code. Do not modify in this file.
 */
public class CategoryConverter implements AttributeConverter<Category, String> {

	@Override
	public String convertToDatabaseColumn(Category value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null Category.");
		}
		return value.getName();
	}

	@Override
	public Category convertToEntityAttribute(String value) {
		return Category.getByName(value);
	}

}
