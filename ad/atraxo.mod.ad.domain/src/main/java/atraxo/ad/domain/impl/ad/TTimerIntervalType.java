/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TTimerIntervalType {

	_EMPTY_(""), _SECONDS_("seconds"), _MINUTES_("minutes"), _HOURS_("hours");

	private String name;

	private TTimerIntervalType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TTimerIntervalType getByName(String name) {
		for (TTimerIntervalType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent TTimerIntervalType with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
