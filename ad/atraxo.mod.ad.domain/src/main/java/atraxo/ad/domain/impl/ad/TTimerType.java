/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TTimerType {

	_EMPTY_(""), _SIMPLE_("simple"), _CRON_("cron");

	private String name;

	private TTimerType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TTimerType getByName(String name) {
		for (TTimerType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TTimerType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
