/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ReportParamType} enum.
 * Generated code. Do not modify in this file.
 */
public class ReportParamTypeConverter
		implements
			AttributeConverter<ReportParamType, String> {

	@Override
	public String convertToDatabaseColumn(ReportParamType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ReportParamType.");
		}
		return value.getName();
	}

	@Override
	public ReportParamType convertToEntityAttribute(String value) {
		return ReportParamType.getByName(value);
	}

}
