/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.scheduler;

import atraxo.ad.domain.impl.scheduler.JobExecution;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.ReadOnly;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link JobExecutionContext} domain entity.
 * Generated code. Do not modify in this file.
 */
@Entity
@Table(name = JobExecutionContext.TABLE_NAME)
@ReadOnly
@Cache(type = CacheType.NONE)
public class JobExecutionContext implements Serializable {

	public static final String TABLE_NAME = "BATCH_JOB_EXECUTION_CONTEXT";

	private static final long serialVersionUID = -8865917134914502125L;

	@NotBlank
	@Column(name = "short_context", nullable = false, length = 2500)
	private String shortContext;

	@Column(name = "serialized_context", length = 4000)
	private String serializedContext;

	@Id
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = JobExecution.class)
	@JoinColumn(name = "job_execution_id", referencedColumnName = "job_execution_id")
	private JobExecution jobExecution;

	public String getShortContext() {
		return this.shortContext;
	}

	public void setShortContext(String shortContext) {
		this.shortContext = shortContext;
	}

	public String getSerializedContext() {
		return this.serializedContext;
	}

	public void setSerializedContext(String serializedContext) {
		this.serializedContext = serializedContext;
	}

	public JobExecution getJobExecution() {
		return this.jobExecution;
	}

	public void setJobExecution(JobExecution jobExecution) {
		this.jobExecution = jobExecution;
	}

	/**
	 * called before persist
	 */
	@PrePersist
	public void prePersist() {
	}

	/**
	 * called before update
	 */
	@PreUpdate
	public void preUpdate() {
	}

}
