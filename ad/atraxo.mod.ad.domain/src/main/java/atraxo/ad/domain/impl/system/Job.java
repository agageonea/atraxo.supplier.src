/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.system;

import atraxo.abstracts.domain.impl.notenant.AbstractTypeNT;
import atraxo.ad.domain.impl.system.JobParam;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Job} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Job.NQ_FIND_BY_NAME, query = "SELECT e FROM Job e WHERE e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Job.NQ_FIND_BY_JCLASS, query = "SELECT e FROM Job e WHERE e.javaClass = :javaClass", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Job.TABLE_NAME, uniqueConstraints = {
		@UniqueConstraint(name = Job.TABLE_NAME + "_UK1", columnNames = {"name"}),
		@UniqueConstraint(name = Job.TABLE_NAME + "_UK2", columnNames = {"javaclass"})})
public class Job extends AbstractTypeNT implements Serializable {

	public static final String TABLE_NAME = "SYS_JOB";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "Job.findByName";
	/**
	 * Named query find by unique key: Jclass.
	 */
	public static final String NQ_FIND_BY_JCLASS = "Job.findByJclass";

	@NotBlank
	@Column(name = "javaclass", nullable = false, length = 255)
	private String javaClass;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = JobParam.class, mappedBy = "job", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<JobParam> params;

	public String getJavaClass() {
		return this.javaClass;
	}

	public void setJavaClass(String javaClass) {
		this.javaClass = javaClass;
	}

	public Collection<JobParam> getParams() {
		return this.params;
	}

	public void setParams(Collection<JobParam> params) {
		this.params = params;
	}

	/**
	 * @param e
	 */
	public void addToParams(JobParam e) {
		if (this.params == null) {
			this.params = new ArrayList<>();
		}
		e.setJob(this);
		this.params.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
