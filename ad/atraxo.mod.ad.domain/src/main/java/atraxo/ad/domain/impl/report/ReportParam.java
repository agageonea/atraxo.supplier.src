/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.report;

import atraxo.abstracts.domain.impl.tenant.AbstractType;
import atraxo.ad.domain.impl.ad.TDataType;
import atraxo.ad.domain.impl.ad.TDataTypeConverter;
import atraxo.ad.domain.impl.report.Report;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link ReportParam} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = ReportParam.NQ_FIND_BY_NAME, query = "SELECT e FROM ReportParam e WHERE e.clientId = :clientId and e.report = :report and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ReportParam.NQ_FIND_BY_NAME_PRIMITIVE, query = "SELECT e FROM ReportParam e WHERE e.clientId = :clientId and e.report.id = :reportId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ReportParam.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = ReportParam.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "report_id", "name"})})
public class ReportParam extends AbstractType implements Serializable {

	public static final String TABLE_NAME = "AD_RPT_PRM";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "ReportParam.findByName";
	/**
	 * Named query find by unique key: Name using the ID field for references.
	 */
	public static final String NQ_FIND_BY_NAME_PRIMITIVE = "ReportParam.findByName_PRIMITIVE";

	@NotBlank
	@Column(name = "title", nullable = false, length = 255)
	private String title;

	@NotNull
	@Column(name = "sequenceno", nullable = false, precision = 4)
	private Integer sequenceNo;

	@NotNull
	@Column(name = "mandatory", nullable = false)
	private Boolean mandatory;

	@NotNull
	@Column(name = "noedit", nullable = false)
	private Boolean noEdit;

	@NotBlank
	@Column(name = "datatype", nullable = false, length = 32)
	@Convert(converter = TDataTypeConverter.class)
	private TDataType dataType;

	@Column(name = "defaultvalue", length = 1000)
	private String defaultValue;

	@Column(name = "listofvalues", length = 1000)
	private String listOfValues;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Report.class)
	@JoinColumn(name = "report_id", referencedColumnName = "id")
	private Report report;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getSequenceNo() {
		return this.sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public Boolean getMandatory() {
		return this.mandatory;
	}

	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}

	public Boolean getNoEdit() {
		return this.noEdit;
	}

	public void setNoEdit(Boolean noEdit) {
		this.noEdit = noEdit;
	}

	public TDataType getDataType() {
		return this.dataType;
	}

	public void setDataType(TDataType dataType) {
		this.dataType = dataType;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getListOfValues() {
		return this.listOfValues;
	}

	public void setListOfValues(String listOfValues) {
		this.listOfValues = listOfValues;
	}

	public Report getReport() {
		return this.report;
	}

	public void setReport(Report report) {
		if (report != null) {
			this.__validate_client_context__(report.getClientId());
		}
		this.report = report;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.mandatory == null) {
			this.mandatory = new Boolean(false);
		}
		if (this.noEdit == null) {
			this.noEdit = new Boolean(false);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
