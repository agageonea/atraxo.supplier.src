/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.security;

import atraxo.abstracts.domain.impl.tenant.AbstractType;
import atraxo.ad.domain.impl.security.Menu;
import atraxo.ad.domain.impl.security.MenuItem;
import atraxo.ad.domain.impl.security.Role;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link MenuItem} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = MenuItem.NQ_FIND_BY_NAME, query = "SELECT e FROM MenuItem e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = MenuItem.NQ_FIND_BY_FRAME, query = "SELECT e FROM MenuItem e WHERE e.clientId = :clientId and e.frame = :frame", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = MenuItem.NQ_FIND_BY_TITLE, query = "SELECT e FROM MenuItem e WHERE e.clientId = :clientId and e.title = :title", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = MenuItem.TABLE_NAME, uniqueConstraints = {
		@UniqueConstraint(name = MenuItem.TABLE_NAME + "_UK1", columnNames = {
				"clientid", "name"}),
		@UniqueConstraint(name = MenuItem.TABLE_NAME + "_UK2", columnNames = {
				"clientid", "frame"}),
		@UniqueConstraint(name = MenuItem.TABLE_NAME + "_UK3", columnNames = {
				"clientid", "title"})})
public class MenuItem extends AbstractType implements Serializable {

	public static final String TABLE_NAME = "AD_MENU_ITEM";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "MenuItem.findByName";
	/**
	 * Named query find by unique key: Frame.
	 */
	public static final String NQ_FIND_BY_FRAME = "MenuItem.findByFrame";
	/**
	 * Named query find by unique key: Title.
	 */
	public static final String NQ_FIND_BY_TITLE = "MenuItem.findByTitle";

	@Column(name = "sequenceno", precision = 4)
	private Integer sequenceNo;

	@NotBlank
	@Column(name = "title", nullable = false, length = 255)
	private String title;

	@Column(name = "frame", length = 255)
	private String frame;

	@Column(name = "bundle", length = 255)
	private String bundle;

	@Column(name = "iconurl", length = 255)
	private String iconUrl;

	@Column(name = "separatorbefore")
	private Boolean separatorBefore;

	@Column(name = "separatorafter")
	private Boolean separatorAfter;

	@Transient
	private Boolean leafNode;

	@Column(name = "glyph", length = 64)
	private String glyph;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = MenuItem.class)
	@JoinColumn(name = "menuitem_id", referencedColumnName = "id")
	private MenuItem menuItem;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Menu.class)
	@JoinColumn(name = "menu_id", referencedColumnName = "id")
	private Menu menu;

	@ManyToMany(mappedBy = "menuItems")
	private Collection<Role> roles;

	public Integer getSequenceNo() {
		return this.sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFrame() {
		return this.frame;
	}

	public void setFrame(String frame) {
		this.frame = frame;
	}

	public String getBundle() {
		return this.bundle;
	}

	public void setBundle(String bundle) {
		this.bundle = bundle;
	}

	public String getIconUrl() {
		return this.iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public Boolean getSeparatorBefore() {
		return this.separatorBefore;
	}

	public void setSeparatorBefore(Boolean separatorBefore) {
		this.separatorBefore = separatorBefore;
	}

	public Boolean getSeparatorAfter() {
		return this.separatorAfter;
	}

	public void setSeparatorAfter(Boolean separatorAfter) {
		this.separatorAfter = separatorAfter;
	}

	public Boolean getLeafNode() {
		return (this.frame != null && !this.frame.equals(""));
	}

	public void setLeafNode(Boolean leafNode) {
		this.leafNode = leafNode;
	}

	public String getGlyph() {
		return this.glyph;
	}

	public void setGlyph(String glyph) {
		this.glyph = glyph;
	}

	public MenuItem getMenuItem() {
		return this.menuItem;
	}

	public void setMenuItem(MenuItem menuItem) {
		if (menuItem != null) {
			this.__validate_client_context__(menuItem.getClientId());
		}
		this.menuItem = menuItem;
	}
	public Menu getMenu() {
		return this.menu;
	}

	public void setMenu(Menu menu) {
		if (menu != null) {
			this.__validate_client_context__(menu.getClientId());
		}
		this.menu = menu;
	}

	public Collection<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
