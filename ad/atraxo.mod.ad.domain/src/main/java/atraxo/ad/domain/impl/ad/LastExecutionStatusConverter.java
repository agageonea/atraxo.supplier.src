/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link LastExecutionStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class LastExecutionStatusConverter
		implements
			AttributeConverter<LastExecutionStatus, String> {

	@Override
	public String convertToDatabaseColumn(LastExecutionStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null LastExecutionStatus.");
		}
		return value.getName();
	}

	@Override
	public LastExecutionStatus convertToEntityAttribute(String value) {
		return LastExecutionStatus.getByName(value);
	}

}
