/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ad.domain.impl.ad;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TJobLogMsgType} enum.
 * Generated code. Do not modify in this file.
 */
public class TJobLogMsgTypeConverter
		implements
			AttributeConverter<TJobLogMsgType, String> {

	@Override
	public String convertToDatabaseColumn(TJobLogMsgType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TJobLogMsgType.");
		}
		return value.getName();
	}

	@Override
	public TJobLogMsgType convertToEntityAttribute(String value) {
		return TJobLogMsgType.getByName(value);
	}

}
