/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link DealType} enum.
 * Generated code. Do not modify in this file.
 */
public class DealTypeConverter implements AttributeConverter<DealType, String> {

	@Override
	public String convertToDatabaseColumn(DealType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null DealType.");
		}
		return value.getName();
	}

	@Override
	public DealType convertToEntityAttribute(String value) {
		return DealType.getByName(value);
	}

}
