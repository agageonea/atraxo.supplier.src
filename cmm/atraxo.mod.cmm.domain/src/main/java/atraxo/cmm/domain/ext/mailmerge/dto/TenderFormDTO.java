package atraxo.cmm.domain.ext.mailmerge.dto;

import org.codehaus.jackson.annotate.JsonProperty;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;

/**
 * @author apetho
 */
public class TenderFormDTO extends AbstractDto {

	@JsonProperty("tender_code")
	private String tenderCode;

	@JsonProperty("holder_code")
	private String holderCode;

	@JsonProperty("version")
	private Long version;

	@JsonProperty("tender_name")
	private String tenderName;

	@JsonProperty("holder_name")
	private String holderName;

	@JsonProperty("location")
	private TenderLocationFormDTO location;

	public String getTenderCode() {
		return this.tenderCode;
	}

	public void setTenderCode(String tenderCode) {
		this.tenderCode = tenderCode;
	}

	public String getHolderCode() {
		return this.holderCode;
	}

	public void setHolderCode(String holderCode) {
		this.holderCode = holderCode;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getTenderName() {
		return this.tenderName;
	}

	public void setTenderName(String tenderName) {
		this.tenderName = tenderName;
	}

	public String getHolderName() {
		return this.holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public TenderLocationFormDTO getLocation() {
		return this.location;
	}

	public void setLocation(TenderLocationFormDTO location) {
		this.location = location;
	}
}
