/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link BidStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class BidStatusConverter
		implements
			AttributeConverter<BidStatus, String> {

	@Override
	public String convertToDatabaseColumn(BidStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null BidStatus.");
		}
		return value.getName();
	}

	@Override
	public BidStatus convertToEntityAttribute(String value) {
		return BidStatus.getByName(value);
	}

}
