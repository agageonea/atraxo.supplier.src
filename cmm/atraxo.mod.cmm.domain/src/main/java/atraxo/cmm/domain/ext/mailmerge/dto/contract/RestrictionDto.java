package atraxo.cmm.domain.ext.mailmerge.dto.contract;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;

public class RestrictionDto extends AbstractDto {

	private String type;
	private String operator;
	private String value;
	private String unit;

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOperator() {
		return this.operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
}
