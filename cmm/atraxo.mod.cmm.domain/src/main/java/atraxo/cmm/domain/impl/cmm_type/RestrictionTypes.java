/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum RestrictionTypes {

	_EMPTY_("", ""), _VOLUME_("Volume", "VO"), _AIRCRAFT_TYPE_("Aircraft type",
			"AT"), _FUELING_TYPE_("Fueling type", "FT"), _TRANSPORT_TYPE_(
			"Transport type", "TT"), _FLIGHT_TYPE_("Flight type", "FL"), _REGISTRATION_NUMBER_(
			"Registration number", "RN"), _TIME_OF_OPERATION_(
			"Time of operation", "TM"), _SHIP_TO_("Ship to", "AR");

	private String name;
	private String code;

	private RestrictionTypes(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static RestrictionTypes getByName(String name) {
		for (RestrictionTypes status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent RestrictionTypes with name: " + name);
	}

	public String getCode() {
		return this.code;
	}

	public static RestrictionTypes getByCode(String code) {
		for (RestrictionTypes status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent RestrictionTypes with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
