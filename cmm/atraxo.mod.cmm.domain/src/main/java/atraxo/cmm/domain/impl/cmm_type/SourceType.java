/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum SourceType {

	_EMPTY_("", ""), _EX_REFINERY_("Ex-Refinery", "E"), _GOVERNMENT_(
			"Government", "G");

	private String name;
	private String code;

	private SourceType(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static SourceType getByName(String name) {
		for (SourceType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent SourceType with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static SourceType getByCode(String code) {
		for (SourceType status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent SourceType with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
