/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum PaymentType {

	_EMPTY_("", ""), _CONTRACT_("Contract", "CO"), _FUEL_CARD_("Fuel Card",
			"CN"), _CREDIT_CARD_("Credit Card", "CC"), _CASH_("Cash", "CA");

	private String name;
	private String code;

	private PaymentType(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static PaymentType getByName(String name) {
		for (PaymentType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent PaymentType with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static PaymentType getByCode(String code) {
		for (PaymentType status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent PaymentType with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
