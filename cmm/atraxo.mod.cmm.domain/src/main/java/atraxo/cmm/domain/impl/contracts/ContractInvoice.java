/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.contracts;

import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.DealTypeConverter;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link ContractInvoice} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = ContractInvoice.NQ_FIND_BY_INVOICEID, query = "SELECT e FROM ContractInvoice e WHERE e.clientId = :clientId and e.invoiceId = :invoiceId and e.dealType = :dealType", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ContractInvoice.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ContractInvoice e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ContractInvoice.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = ContractInvoice.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "invoice_id", "deal_type"})})
public class ContractInvoice extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "CMM_CONTRACT_INVOICE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: InvoiceId.
	 */
	public static final String NQ_FIND_BY_INVOICEID = "ContractInvoice.findByInvoiceId";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ContractInvoice.findByBusiness";

	@NotNull
	@Column(name = "invoice_id", nullable = false, precision = 11)
	private Integer invoiceId;

	@NotBlank
	@Column(name = "invoice_code", nullable = false, length = 100)
	private String invoiceCode;

	@NotBlank
	@Column(name = "invoice_status", nullable = false, length = 100)
	private String invoiceStatus;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "valid_from", nullable = false)
	private Date validFrom;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "valid_to", nullable = false)
	private Date validTo;

	@NotBlank
	@Column(name = "deal_type", nullable = false, length = 32)
	@Convert(converter = DealTypeConverter.class)
	private DealType dealType;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contract.class)
	@JoinColumn(name = "contract_id", referencedColumnName = "id")
	private Contract contract;

	public Integer getInvoiceId() {
		return this.invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getInvoiceCode() {
		return this.invoiceCode;
	}

	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	public String getInvoiceStatus() {
		return this.invoiceStatus;
	}

	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public DealType getDealType() {
		return this.dealType;
	}

	public void setDealType(DealType dealType) {
		this.dealType = dealType;
	}

	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		if (contract != null) {
			this.__validate_client_context__(contract.getClientId());
		}
		this.contract = contract;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
