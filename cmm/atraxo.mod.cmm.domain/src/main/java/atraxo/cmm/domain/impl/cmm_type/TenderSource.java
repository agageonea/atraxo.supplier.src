/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TenderSource {

	_EMPTY_(""), _CAPTURED_("Captured"), _IMPORTED_("Imported");

	private String name;

	private TenderSource(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TenderSource getByName(String name) {
		for (TenderSource status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TenderSource with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
