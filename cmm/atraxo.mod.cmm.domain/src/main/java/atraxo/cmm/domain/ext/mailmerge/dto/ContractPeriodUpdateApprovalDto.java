package atraxo.cmm.domain.ext.mailmerge.dto;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;

/**
 * Email DTO used for the Contract Period Update Approval Process
 *
 * @author mbotorogea
 */
public class ContractPeriodUpdateApprovalDto extends AbstractDto {

	private static final long serialVersionUID = 6659358206331541053L;

	private String title;
	private String fullName;

	private String fullnameOfRequester;

	private String contractCode;
	private String contractHolder;
	private String customerName;
	private String contractLocation;
	private String changes;

	private String approvalRequestNote;

	private String approverNameFirstLevel;
	private String approvalNoteFirstLevel;

	private String approverNameSecondLevel;
	private String approvalNoteSecondLevel;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFullnameOfRequester() {
		return this.fullnameOfRequester;
	}

	public void setFullnameOfRequester(String fullnameOfRequester) {
		this.fullnameOfRequester = fullnameOfRequester;
	}

	public String getContractCode() {
		return this.contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public String getContractHolder() {
		return this.contractHolder;
	}

	public void setContractHolder(String contractHolder) {
		this.contractHolder = contractHolder;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getContractLocation() {
		return this.contractLocation;
	}

	public void setContractLocation(String contractLocation) {
		this.contractLocation = contractLocation;
	}

	public String getApprovalRequestNote() {
		return this.approvalRequestNote;
	}

	public void setApprovalRequestNote(String approvalRequestNote) {
		this.approvalRequestNote = approvalRequestNote;
	}

	public String getApproverNameFirstLevel() {
		return this.approverNameFirstLevel;
	}

	public void setApproverNameFirstLevel(String approverNameFirstLevel) {
		this.approverNameFirstLevel = approverNameFirstLevel;
	}

	public String getApprovalNoteFirstLevel() {
		return this.approvalNoteFirstLevel;
	}

	public void setApprovalNoteFirstLevel(String approvalNoteFirstLevel) {
		this.approvalNoteFirstLevel = approvalNoteFirstLevel;
	}

	public String getApproverNameSecondLevel() {
		return this.approverNameSecondLevel;
	}

	public void setApproverNameSecondLevel(String approverNameSecondLevel) {
		this.approverNameSecondLevel = approverNameSecondLevel;
	}

	public String getApprovalNoteSecondLevel() {
		return this.approvalNoteSecondLevel;
	}

	public void setApprovalNoteSecondLevel(String approvalNoteSecondLevel) {
		this.approvalNoteSecondLevel = approvalNoteSecondLevel;
	}

	public String getChanges() {
		return changes;
	}

	public void setChanges(String changes) {
		this.changes = changes;
	}

}
