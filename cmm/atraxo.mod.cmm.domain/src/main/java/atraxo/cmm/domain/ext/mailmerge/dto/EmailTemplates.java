package atraxo.cmm.domain.ext.mailmerge.dto;

import java.util.List;

import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;

public class EmailTemplates {

	public EmailTemplates(EmailDto email, List<ContractTemplate> contractTemplates) {
		this.email = email;
		this.contractTemplates = contractTemplates;
	}

	private EmailDto email;
	List<ContractTemplate> contractTemplates;

	public EmailDto getEmail() {
		return this.email;
	}

	public void setEmail(EmailDto email) {
		this.email = email;
	}

	public List<ContractTemplate> getContractTemplates() {
		return this.contractTemplates;
	}

	public void setContractTemplates(List<ContractTemplate> contractTemplates) {
		this.contractTemplates = contractTemplates;
	}

}
