/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ContractType {

	_EMPTY_(""), _PRODUCT_("Product"), _FUELING_SERVICE_("Fueling Service"), _GENERAL_SERVICE_(
			"General Service"), _INSPECTION_("Inspection"), _STORAGE_("Storage"), _CSO_(
			"CSO");

	private String name;

	private ContractType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ContractType getByName(String name) {
		for (ContractType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent ContractType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
