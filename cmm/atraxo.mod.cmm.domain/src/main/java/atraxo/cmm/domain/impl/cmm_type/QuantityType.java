/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum QuantityType {

	_EMPTY_("", ""), _NET_VOLUME_("Net volume", "NT"), _GROSS_VOLUME_(
			"Gross volume", "GR");

	private String name;
	private String code;

	private QuantityType(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static QuantityType getByName(String name) {
		for (QuantityType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent QuantityType with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static QuantityType getByCode(String code) {
		for (QuantityType status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent QuantityType with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
