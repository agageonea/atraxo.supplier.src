package atraxo.cmm.domain.ext.mailmerge.dto.contract;

public class AverageMethodDto {

	private String code;
	private String name;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
