/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link Product} enum.
 * Generated code. Do not modify in this file.
 */
public class ProductConverter implements AttributeConverter<Product, String> {

	@Override
	public String convertToDatabaseColumn(Product value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null Product.");
		}
		return value.getName();
	}

	@Override
	public Product convertToEntityAttribute(String value) {
		return Product.getByName(value);
	}

}
