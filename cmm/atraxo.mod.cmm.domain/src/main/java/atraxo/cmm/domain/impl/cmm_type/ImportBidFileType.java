/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ImportBidFileType {

	_EMPTY_(""), _XLS_("XLS"), _XML_("XML");

	private String name;

	private ImportBidFileType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ImportBidFileType getByName(String name) {
		for (ImportBidFileType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent ImportBidFileType with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
