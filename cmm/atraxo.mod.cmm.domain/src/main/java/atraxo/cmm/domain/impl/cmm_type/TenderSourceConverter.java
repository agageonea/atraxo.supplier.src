/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TenderSource} enum.
 * Generated code. Do not modify in this file.
 */
public class TenderSourceConverter
		implements
			AttributeConverter<TenderSource, String> {

	@Override
	public String convertToDatabaseColumn(TenderSource value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TenderSource.");
		}
		return value.getName();
	}

	@Override
	public TenderSource convertToEntityAttribute(String value) {
		return TenderSource.getByName(value);
	}

}
