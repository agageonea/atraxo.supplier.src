package atraxo.cmm.domain.ext.mailmerge.dto;

import java.util.ArrayList;
import java.util.List;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;

/**
 * Email DTO used for the Ship To Update Approval Process
 *
 * @author mbotorogea
 */
public class ContractShipToUpdateApprovalDto extends AbstractDto {

	private static final long serialVersionUID = 6659358206331541053L;

	private String title;
	private String fullName;

	private String fullnameOfRequester;

	private String contractCode;
	private String contractHolder;
	private String customerName;
	private String contractLocation;

	private String approvalRequestNote;

	private List<ProductChangeDto> shipToChange = new ArrayList<>();

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFullnameOfRequester() {
		return this.fullnameOfRequester;
	}

	public void setFullnameOfRequester(String fullnameOfRequester) {
		this.fullnameOfRequester = fullnameOfRequester;
	}

	public String getContractCode() {
		return this.contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public String getContractHolder() {
		return this.contractHolder;
	}

	public void setContractHolder(String contractHolder) {
		this.contractHolder = contractHolder;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getContractLocation() {
		return this.contractLocation;
	}

	public void setContractLocation(String contractLocation) {
		this.contractLocation = contractLocation;
	}

	public String getApprovalRequestNote() {
		return this.approvalRequestNote;
	}

	public void setApprovalRequestNote(String approvalRequestNote) {
		this.approvalRequestNote = approvalRequestNote;
	}

	public List<ProductChangeDto> getShipToChange() {
		return this.shipToChange;
	}

	public void setShipToChange(List<ProductChangeDto> shipToChange) {
		this.shipToChange = shipToChange;
	}

}
