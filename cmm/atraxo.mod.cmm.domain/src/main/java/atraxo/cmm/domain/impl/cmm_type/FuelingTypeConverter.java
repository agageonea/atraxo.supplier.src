/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FuelingType} enum.
 * Generated code. Do not modify in this file.
 */
public class FuelingTypeConverter
		implements
			AttributeConverter<FuelingType, String> {

	@Override
	public String convertToDatabaseColumn(FuelingType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null FuelingType.");
		}
		return value.getName();
	}

	@Override
	public FuelingType convertToEntityAttribute(String value) {
		return FuelingType.getByName(value);
	}

}
