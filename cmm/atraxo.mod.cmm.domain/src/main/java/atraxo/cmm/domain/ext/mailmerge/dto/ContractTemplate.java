package atraxo.cmm.domain.ext.mailmerge.dto;

import java.util.ArrayList;
import java.util.List;

import atraxo.cmm.domain.ext.mailmerge.dto.contract.ContractDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.CustomerDtoPriceNotification;
import atraxo.fmbas.domain.impl.customer.CustomerNotification;

public class ContractTemplate {

	private CustomerNotification notification;
	private List<ContractDto> contracts;
	private CustomerDtoPriceNotification customer;

	public CustomerNotification getNotification() {
		return this.notification;
	}

	public void setNotification(CustomerNotification notification) {
		this.notification = notification;
	}

	public List<ContractDto> getContracts() {
		return this.contracts;
	}

	public void setContracts(List<ContractDto> contracts) {
		this.contracts = contracts;
	}

	public void addContract(ContractDto contract) {
		if (this.contracts == null) {
			this.contracts = new ArrayList<>();
		}
		this.contracts.add(contract);
	}

	public CustomerDtoPriceNotification getCustomer() {
		return this.customer;
	}

	public void setCustomer(CustomerDtoPriceNotification customer) {
		this.customer = customer;
	}

}
