/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link BidApprovalStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class BidApprovalStatusConverter
		implements
			AttributeConverter<BidApprovalStatus, String> {

	@Override
	public String convertToDatabaseColumn(BidApprovalStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null BidApprovalStatus.");
		}
		return value.getName();
	}

	@Override
	public BidApprovalStatus convertToEntityAttribute(String value) {
		return BidApprovalStatus.getByName(value);
	}

}
