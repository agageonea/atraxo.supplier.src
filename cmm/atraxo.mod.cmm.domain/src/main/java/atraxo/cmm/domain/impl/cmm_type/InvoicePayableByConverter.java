/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link InvoicePayableBy} enum.
 * Generated code. Do not modify in this file.
 */
public class InvoicePayableByConverter
		implements
			AttributeConverter<InvoicePayableBy, String> {

	@Override
	public String convertToDatabaseColumn(InvoicePayableBy value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null InvoicePayableBy.");
		}
		return value.getName();
	}

	@Override
	public InvoicePayableBy convertToEntityAttribute(String value) {
		return InvoicePayableBy.getByName(value);
	}

}
