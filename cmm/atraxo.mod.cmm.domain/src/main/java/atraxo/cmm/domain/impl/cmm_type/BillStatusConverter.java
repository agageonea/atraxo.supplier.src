/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link BillStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class BillStatusConverter
		implements
			AttributeConverter<BillStatus, String> {

	@Override
	public String convertToDatabaseColumn(BillStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null BillStatus.");
		}
		return value.getName();
	}

	@Override
	public BillStatus convertToEntityAttribute(String value) {
		return BillStatus.getByName(value);
	}

}
