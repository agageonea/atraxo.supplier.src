package atraxo.cmm.domain.ext.mailmerge.dto;

import java.math.BigDecimal;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;

public class ShipToDto extends AbstractDto {

	private String name;
	private String iataCode;
	private BigDecimal allocatedVolume;
	private String volumeUnit;
	private String contractedVolumePeriod;

	public String getContractedVolumePeriod() {
		return this.contractedVolumePeriod;
	}

	public void setContractedVolumePeriod(String contractedVolumePeriod) {
		this.contractedVolumePeriod = contractedVolumePeriod;
	}

	public String getVolumeUnit() {
		return this.volumeUnit;
	}

	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public BigDecimal getAllocatedVolume() {
		return this.allocatedVolume;
	}

	public void setAllocatedVolume(BigDecimal allocatedVolume) {
		this.allocatedVolume = allocatedVolume;
	}
}
