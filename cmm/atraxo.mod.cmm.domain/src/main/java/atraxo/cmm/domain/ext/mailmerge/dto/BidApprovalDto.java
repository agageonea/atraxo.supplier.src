package atraxo.cmm.domain.ext.mailmerge.dto;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;

/**
 * Email DTO used for the Bid approval process
 *
 * @author vhojda
 */
public class BidApprovalDto extends AbstractDto {

	/**
	 *
	 */
	private static final long serialVersionUID = -9141471447418109970L;

	private String title;
	private String name;

	private String fullnameOfRequester;

	private String tenderName;
	private String tenderHolder;
	private String bidLocation;

	private String approveNote;
	private String taskNote;

	private String approverNameFirstLevel;
	private String approvalNoteFirstLevel;

	private String approverNameSecondLevel;
	private String approvalNoteSecondLevel;

	private String approverNameThirdLevel;
	private String approvalNoteThirdLevel;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFullnameOfRequester() {
		return this.fullnameOfRequester;
	}

	public void setFullnameOfRequester(String fullnameOfRequester) {
		this.fullnameOfRequester = fullnameOfRequester;
	}

	public String getTenderName() {
		return this.tenderName;
	}

	public void setTenderName(String tenderName) {
		this.tenderName = tenderName;
	}

	public String getTenderHolder() {
		return this.tenderHolder;
	}

	public void setTenderHolder(String tenderHolder) {
		this.tenderHolder = tenderHolder;
	}

	public String getBidLocation() {
		return this.bidLocation;
	}

	public void setBidLocation(String bidLocation) {
		this.bidLocation = bidLocation;
	}

	public String getApproveNote() {
		return this.approveNote;
	}

	public void setApproveNote(String approveNote) {
		this.approveNote = approveNote;
	}

	public String getTaskNote() {
		return this.taskNote;
	}

	public void setTaskNote(String taskNote) {
		this.taskNote = taskNote;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getApproverNameFirstLevel() {
		return this.approverNameFirstLevel;
	}

	public void setApproverNameFirstLevel(String approverNameFirstLevel) {
		this.approverNameFirstLevel = approverNameFirstLevel;
	}

	public String getApprovalNoteFirstLevel() {
		return this.approvalNoteFirstLevel;
	}

	public void setApprovalNoteFirstLevel(String approvalNoteFirstLevel) {
		this.approvalNoteFirstLevel = approvalNoteFirstLevel;
	}

	public String getApproverNameSecondLevel() {
		return this.approverNameSecondLevel;
	}

	public void setApproverNameSecondLevel(String approverNameSecondLevel) {
		this.approverNameSecondLevel = approverNameSecondLevel;
	}

	public String getApprovalNoteSecondLevel() {
		return this.approvalNoteSecondLevel;
	}

	public void setApprovalNoteSecondLevel(String approvalNoteSecondLevel) {
		this.approvalNoteSecondLevel = approvalNoteSecondLevel;
	}

	public String getApproverNameThirdLevel() {
		return this.approverNameThirdLevel;
	}

	public void setApproverNameThirdLevel(String approverNameThirdLevel) {
		this.approverNameThirdLevel = approverNameThirdLevel;
	}

	public String getApprovalNoteThirdLevel() {
		return this.approvalNoteThirdLevel;
	}

	public void setApprovalNoteThirdLevel(String approvalNoteThirdLevel) {
		this.approvalNoteThirdLevel = approvalNoteThirdLevel;
	}

	@Override
	public String toString() {
		return "BidApprovalDto [title=" + this.title + ", name=" + this.name + ", fullnameOfRequester=" + this.fullnameOfRequester + ", tenderName="
				+ this.tenderName + ", tenderHolder=" + this.tenderHolder + ", bidLocation=" + this.bidLocation + ", approveNote=" + this.approveNote
				+ ", taskNote=" + this.taskNote + ", approverNameFirstLevel=" + this.approverNameFirstLevel + ", approvalNoteFirstLevel="
				+ this.approvalNoteFirstLevel + ", approverNameSecondLevel=" + this.approverNameSecondLevel + ", approvalNoteSecondLevel="
				+ this.approvalNoteSecondLevel + ", approverNameThirdLevel=" + this.approverNameThirdLevel + ", approvalNoteThirdLevel="
				+ this.approvalNoteThirdLevel + "]";
	}

}
