/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TenderStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class TenderStatusConverter
		implements
			AttributeConverter<TenderStatus, String> {

	@Override
	public String convertToDatabaseColumn(TenderStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TenderStatus.");
		}
		return value.getName();
	}

	@Override
	public TenderStatus convertToEntityAttribute(String value) {
		return TenderStatus.getByName(value);
	}

}
