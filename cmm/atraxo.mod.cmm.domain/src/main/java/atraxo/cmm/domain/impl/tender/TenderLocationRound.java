/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.tender;

import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link TenderLocationRound} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = TenderLocationRound.NQ_FIND_BY_TENDER_LOCATION_ROUND, query = "SELECT e FROM TenderLocationRound e WHERE e.clientId = :clientId and e.tenderLocation = :tenderLocation and e.roundNo = :roundNo", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = TenderLocationRound.NQ_FIND_BY_TENDER_LOCATION_ROUND_PRIMITIVE, query = "SELECT e FROM TenderLocationRound e WHERE e.clientId = :clientId and e.tenderLocation.id = :tenderLocationId and e.roundNo = :roundNo", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = TenderLocationRound.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM TenderLocationRound e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = TenderLocationRound.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = TenderLocationRound.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "tenderlocation_id", "round_no"})})
public class TenderLocationRound extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "CMM_TENDER_LOCATION_ROUNDS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Tender_location_round.
	 */
	public static final String NQ_FIND_BY_TENDER_LOCATION_ROUND = "TenderLocationRound.findByTender_location_round";
	/**
	 * Named query find by unique key: Tender_location_round using the ID field for references.
	 */
	public static final String NQ_FIND_BY_TENDER_LOCATION_ROUND_PRIMITIVE = "TenderLocationRound.findByTender_location_round_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "TenderLocationRound.findByBusiness";

	@Column(name = "round_no", precision = 2)
	private Integer roundNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "bidding_from")
	private Date biddingFrom;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "bidding_to")
	private Date biddingTo;

	@Temporal(TemporalType.DATE)
	@Column(name = "bid_opening")
	private Date bidOpening;

	@Column(name = "active")
	private Boolean active;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = TenderLocation.class)
	@JoinColumn(name = "tenderlocation_id", referencedColumnName = "id")
	private TenderLocation tenderLocation;

	public Integer getRoundNo() {
		return this.roundNo;
	}

	public void setRoundNo(Integer roundNo) {
		this.roundNo = roundNo;
	}

	public Date getBiddingFrom() {
		return this.biddingFrom;
	}

	public void setBiddingFrom(Date biddingFrom) {
		this.biddingFrom = biddingFrom;
	}

	public Date getBiddingTo() {
		return this.biddingTo;
	}

	public void setBiddingTo(Date biddingTo) {
		this.biddingTo = biddingTo;
	}

	public Date getBidOpening() {
		return this.bidOpening;
	}

	public void setBidOpening(Date bidOpening) {
		this.bidOpening = bidOpening;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public TenderLocation getTenderLocation() {
		return this.tenderLocation;
	}

	public void setTenderLocation(TenderLocation tenderLocation) {
		if (tenderLocation != null) {
			this.__validate_client_context__(tenderLocation.getClientId());
		}
		this.tenderLocation = tenderLocation;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime
				* result
				+ ((this.tenderLocation == null) ? 0 : this.tenderLocation
						.hashCode());
		result = prime * result
				+ ((this.roundNo == null) ? 0 : this.roundNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		TenderLocationRound other = (TenderLocationRound) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.tenderLocation == null) {
			if (other.tenderLocation != null) {
				return false;
			}
		} else if (!this.tenderLocation.equals(other.tenderLocation)) {
			return false;
		}
		if (this.roundNo == null) {
			if (other.roundNo != null) {
				return false;
			}
		} else if (!this.roundNo.equals(other.roundNo)) {
			return false;
		}
		return true;
	}
}
