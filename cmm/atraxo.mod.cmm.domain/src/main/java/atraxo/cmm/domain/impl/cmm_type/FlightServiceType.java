/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FlightServiceType {

	_EMPTY_("", ""), _AD_HOC_("Ad-hoc", "AH"), _SCHEDULED_("Scheduled", "S"), _GENERAL_AVIATION_(
			"General Aviation", "GA");

	private String name;
	private String code;

	private FlightServiceType(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FlightServiceType getByName(String name) {
		for (FlightServiceType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FlightServiceType with name: " + name);
	}

	public String getCode() {
		return this.code;
	}

	public static FlightServiceType getByCode(String code) {
		for (FlightServiceType status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FlightServiceType with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
