/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FuelTicketFuelingOperation {

	_EMPTY_("", ""), _UPLIFT_("Uplift", "UP"), _PRE_FUEL_("Pre-fuel", "PF"), _RE_FUEL_(
			"Re-fuel", "RF"), _DE_FUEL_("De-fuel", "DF"), _FUEL_DUMP_(
			"Fuel dump", "DP"), _DE_FUEL_TO_DRUM_("De-fuel to drum", "DD"), _FUEL_INTO_DRUM_(
			"Fuel into drum", "FD"), _FUEL_INTO_TRUCK_("Fuel into truck", "FT");

	private String name;
	private String code;

	private FuelTicketFuelingOperation(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FuelTicketFuelingOperation getByName(String name) {
		for (FuelTicketFuelingOperation status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FuelTicketFuelingOperation with name: " + name);
	}

	public String getCode() {
		return this.code;
	}

	public static FuelTicketFuelingOperation getByCode(String code) {
		for (FuelTicketFuelingOperation status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FuelTicketFuelingOperation with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
