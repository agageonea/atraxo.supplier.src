/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum IATAServiceLevel {

	_EMPTY_("", ""), _IATA_SERVICE_LEVEL_1_("IATA Service level 1", "IT1"), _IATA_SERVICE_LEVEL_2_(
			"IATA Service level 2", "IT2"), _IATA_SERVICE_LEVEL_3_(
			"IATA Service level 3", "IT3"), _IATA_SERVICE_LEVEL_4_(
			"IATA Service level 4", "IT4");

	private String name;
	private String code;

	private IATAServiceLevel(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static IATAServiceLevel getByName(String name) {
		for (IATAServiceLevel status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent IATAServiceLevel with name: " + name);
	}

	public String getCode() {
		return this.code;
	}

	public static IATAServiceLevel getByCode(String code) {
		for (IATAServiceLevel status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent IATAServiceLevel with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
