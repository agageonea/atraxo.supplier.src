/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.prices;

import atraxo.cmm.domain.ext.ContractPriceComponentCheckResult;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link ContractPriceComponent} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = ContractPriceComponent.NQ_FIND_BY_BUSINESSKEY, query = "SELECT e FROM ContractPriceComponent e WHERE e.clientId = :clientId and e.contrPriceCtgry = :contrPriceCtgry and e.validFrom = :validFrom and e.validTo = :validTo", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ContractPriceComponent.NQ_FIND_BY_BUSINESSKEY_PRIMITIVE, query = "SELECT e FROM ContractPriceComponent e WHERE e.clientId = :clientId and e.contrPriceCtgry.id = :contrPriceCtgryId and e.validFrom = :validFrom and e.validTo = :validTo", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ContractPriceComponent.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ContractPriceComponent e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ContractPriceComponent.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = ContractPriceComponent.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "contr_price_ctgry_id",
		"valid_from", "valid_to"})})
public class ContractPriceComponent extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "CMM_CONTR_PRICE_CMPNT";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: BusinessKey.
	 */
	public static final String NQ_FIND_BY_BUSINESSKEY = "ContractPriceComponent.findByBusinessKey";
	/**
	 * Named query find by unique key: BusinessKey using the ID field for references.
	 */
	public static final String NQ_FIND_BY_BUSINESSKEY_PRIMITIVE = "ContractPriceComponent.findByBusinessKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ContractPriceComponent.findByBusiness";

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "valid_from", nullable = false)
	private Date validFrom;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "valid_to", nullable = false)
	private Date validTo;

	@NotNull
	@Column(name = "provisional", nullable = false)
	private Boolean provisional;

	@NotNull
	@Column(name = "price", nullable = false, precision = 19, scale = 6)
	private BigDecimal price;

	@NotNull
	@Column(name = "withhold", nullable = false)
	private Boolean withHold;

	@Transient
	private String toleranceMessage;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ContractPriceCategory.class)
	@JoinColumn(name = "contr_price_ctgry_id", referencedColumnName = "id")
	private ContractPriceCategory contrPriceCtgry;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "crncy_id", referencedColumnName = "id")
	private Currencies currency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_id", referencedColumnName = "id")
	private Unit unit;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = ContractPriceComponentConv.class, mappedBy = "contractPriceComponent", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<ContractPriceComponentConv> convertedPrices;

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Boolean getProvisional() {
		return this.provisional;
	}

	public void setProvisional(Boolean provisional) {
		this.provisional = provisional;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Boolean getWithHold() {
		return this.withHold;
	}

	public void setWithHold(Boolean withHold) {
		this.withHold = withHold;
	}

	public String getToleranceMessage() {
		return ContractPriceComponentCheckResult.resultMap.get() != null
				? ContractPriceComponentCheckResult.resultMap.get().get(
						this.getId())
				: "";
	}

	public void setToleranceMessage(String toleranceMessage) {
		if (ContractPriceComponentCheckResult.resultMap.get() == null) {
			ContractPriceComponentCheckResult.resultMap
					.set(new HashMap<Integer, String>());
		}
		ContractPriceComponentCheckResult.resultMap.get().put(this.getId(),
				toleranceMessage);
	}

	public ContractPriceCategory getContrPriceCtgry() {
		return this.contrPriceCtgry;
	}

	public void setContrPriceCtgry(ContractPriceCategory contrPriceCtgry) {
		if (contrPriceCtgry != null) {
			this.__validate_client_context__(contrPriceCtgry.getClientId());
		}
		this.contrPriceCtgry = contrPriceCtgry;
	}
	public Currencies getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currencies currency) {
		if (currency != null) {
			this.__validate_client_context__(currency.getClientId());
		}
		this.currency = currency;
	}
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		if (unit != null) {
			this.__validate_client_context__(unit.getClientId());
		}
		this.unit = unit;
	}

	public Collection<ContractPriceComponentConv> getConvertedPrices() {
		return this.convertedPrices;
	}

	public void setConvertedPrices(
			Collection<ContractPriceComponentConv> convertedPrices) {
		this.convertedPrices = convertedPrices;
	}

	/**
	 * @param e
	 */
	public void addToConvertedPrices(ContractPriceComponentConv e) {
		if (this.convertedPrices == null) {
			this.convertedPrices = new ArrayList<>();
		}
		e.setContractPriceComponent(this);
		this.convertedPrices.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime
				* result
				+ ((this.contrPriceCtgry == null) ? 0 : this.contrPriceCtgry
						.hashCode());
		result = prime * result
				+ ((this.validFrom == null) ? 0 : this.validFrom.hashCode());
		result = prime * result
				+ ((this.validTo == null) ? 0 : this.validTo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		ContractPriceComponent other = (ContractPriceComponent) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.contrPriceCtgry == null) {
			if (other.contrPriceCtgry != null) {
				return false;
			}
		} else if (!this.contrPriceCtgry.equals(other.contrPriceCtgry)) {
			return false;
		}
		if (this.validFrom == null) {
			if (other.validFrom != null) {
				return false;
			}
		} else if (!this.validFrom.equals(other.validFrom)) {
			return false;
		}
		if (this.validTo == null) {
			if (other.validTo != null) {
				return false;
			}
		} else if (!this.validTo.equals(other.validTo)) {
			return false;
		}
		return true;
	}
}
