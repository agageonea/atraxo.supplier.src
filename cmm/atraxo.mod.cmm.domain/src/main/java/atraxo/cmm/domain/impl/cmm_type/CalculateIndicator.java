/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum CalculateIndicator {

	_EMPTY_(""), _INCLUDED_("Included"), _CALCULATE_ONLY_("Calculate only");

	private String name;

	private CalculateIndicator(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static CalculateIndicator getByName(String name) {
		for (CalculateIndicator status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent CalculateIndicator with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
