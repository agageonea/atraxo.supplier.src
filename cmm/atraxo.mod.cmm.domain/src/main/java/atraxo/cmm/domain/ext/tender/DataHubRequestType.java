package atraxo.cmm.domain.ext.tender;

import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * @author abolindu
 */
public enum DataHubRequestType {
	SINGLE_REQUEST("Single Request"), BULK_REQUESTS("Bulk Request");

	private String requestType;

	private DataHubRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getStatus() {
		return this.requestType;
	}

	public static DataHubRequestType getByStatus(String requestType) {
		for (DataHubRequestType rt : values()) {
			if (rt.getStatus().equalsIgnoreCase(requestType)) {
				return rt;
			}
		}
		throw new InvalidEnumException("Inexistent DataHubRequestType with status: " + requestType);
	}

}
