package atraxo.cmm.domain.ext.contracts;

import java.math.BigDecimal;

import atraxo.cmm.domain.impl.cmm_type.PricingMethod;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;

public class SupplierContract {

	private Contract contract;
	private String flightType;
	private String type;
	private String delivery;
	private String scope;
	private PricingMethod priceBasis;
	private BigDecimal basePrice;
	private BigDecimal differential;
	private BigDecimal intoPlaneFee;
	private BigDecimal otherFees;
	private BigDecimal taxes;
	private BigDecimal perFlightFee;
	private Integer exposure;
	private String unit;
	private Integer paymentTerms;
	private String creditTerms;
	private String exchangeRateOffset;
	private String indexOffset;
	private Suppliers intoPlaneAgent;
	private Quotation quotation;

	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public String getFlightType() {
		return this.flightType;
	}

	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDelivery() {
		return this.delivery;
	}

	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}

	public String getScope() {
		return this.scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public PricingMethod getPriceBasis() {
		return this.priceBasis;
	}

	public void setPriceBasis(PricingMethod priceBasis) {
		this.priceBasis = priceBasis;
	}

	public BigDecimal getBasePrice() {
		return this.basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public BigDecimal getDifferential() {
		return this.differential;
	}

	public void setDifferential(BigDecimal differential) {
		this.differential = differential;
	}

	public BigDecimal getIntoPlaneFee() {
		return this.intoPlaneFee;
	}

	public void setIntoPlaneFee(BigDecimal intoPlaneFee) {
		this.intoPlaneFee = intoPlaneFee;
	}

	public BigDecimal getOtherFees() {
		return this.otherFees;
	}

	public void setOtherFees(BigDecimal otherFees) {
		this.otherFees = otherFees;
	}

	public BigDecimal getTaxes() {
		return this.taxes;
	}

	public void setTaxes(BigDecimal taxes) {
		this.taxes = taxes;
	}

	public BigDecimal getPerFlightFee() {
		return this.perFlightFee;
	}

	public void setPerFlightFee(BigDecimal perFlightFee) {
		this.perFlightFee = perFlightFee;
	}

	public Integer getExposure() {
		return this.exposure;
	}

	public void setExposure(Integer exposure) {
		this.exposure = exposure;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	public String getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(String period) {
		this.exchangeRateOffset = period;
	}

	public String getIndexOffset() {
		return this.indexOffset;
	}

	public void setIndexOffset(String indexOffset) {
		this.indexOffset = indexOffset;
	}

	public void setIntoPlaneAgent(Suppliers intoPlaneAgent) {
		this.intoPlaneAgent = intoPlaneAgent;
	}

	public Suppliers getIntoPlaneAgent() {
		return this.intoPlaneAgent;
	}

	public Quotation getQuotation() {
		return this.quotation;
	}

	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}

}
