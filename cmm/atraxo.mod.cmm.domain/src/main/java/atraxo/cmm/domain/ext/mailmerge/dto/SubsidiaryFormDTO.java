package atraxo.cmm.domain.ext.mailmerge.dto;

import org.codehaus.jackson.annotate.JsonProperty;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;

public class SubsidiaryFormDTO extends AbstractDto {

	@JsonProperty("iata_code")
	private String iataCode;

	@JsonProperty("name")
	private String name;

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIata_code(String iataCode) {
		this.iataCode = iataCode;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
