/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ContractScope} enum.
 * Generated code. Do not modify in this file.
 */
public class ContractScopeConverter
		implements
			AttributeConverter<ContractScope, String> {

	@Override
	public String convertToDatabaseColumn(ContractScope value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ContractScope.");
		}
		return value.getName();
	}

	@Override
	public ContractScope convertToEntityAttribute(String value) {
		return ContractScope.getByName(value);
	}

}
