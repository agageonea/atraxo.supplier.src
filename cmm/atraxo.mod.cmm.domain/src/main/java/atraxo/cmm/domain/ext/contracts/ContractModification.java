package atraxo.cmm.domain.ext.contracts;

public enum ContractModification {
	RESET, EFFECTIVE, UPDATE
}
