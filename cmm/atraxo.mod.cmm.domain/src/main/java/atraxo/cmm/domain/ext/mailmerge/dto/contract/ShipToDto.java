package atraxo.cmm.domain.ext.mailmerge.dto.contract;

import java.math.BigDecimal;
import java.util.Date;

public class ShipToDto {

	private Date validFrom;
	private Date validTo;
	private BigDecimal allocatedVolume;
	private BigDecimal adjustedVolume;
	private BigDecimal actualVolume;
	private BigDecimal offeredVolume;
	private CustomerDto customer;

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public BigDecimal getAllocatedVolume() {
		return this.allocatedVolume;
	}

	public void setAllocatedVolume(BigDecimal allocatedVolume) {
		this.allocatedVolume = allocatedVolume;
	}

	public BigDecimal getAdjustedVolume() {
		return this.adjustedVolume;
	}

	public void setAdjustedVolume(BigDecimal adjustedVolume) {
		this.adjustedVolume = adjustedVolume;
	}

	public BigDecimal getActualVolume() {
		return this.actualVolume;
	}

	public void setActualVolume(BigDecimal actualVolume) {
		this.actualVolume = actualVolume;
	}

	public BigDecimal getOfferedVolume() {
		return this.offeredVolume;
	}

	public void setOfferedVolume(BigDecimal offeredVolume) {
		this.offeredVolume = offeredVolume;
	}

	public CustomerDto getCustomer() {
		return this.customer;
	}

	public void setCustomer(CustomerDto customer) {
		this.customer = customer;
	}

}
