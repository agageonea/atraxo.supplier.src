/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.contracts;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link MarkedContract} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = MarkedContract.NQ_FIND_BY_CONTRACTID, query = "SELECT e FROM MarkedContract e WHERE e.clientId = :clientId and e.contractId = :contractId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = MarkedContract.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM MarkedContract e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = MarkedContract.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = MarkedContract.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "contract_id"})})
public class MarkedContract extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "CMM_MARKED_CONTRACT";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: ContractId.
	 */
	public static final String NQ_FIND_BY_CONTRACTID = "MarkedContract.findByContractId";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "MarkedContract.findByBusiness";

	@NotNull
	@Column(name = "contract_id", nullable = false, precision = 11)
	private Integer contractId;

	@Column(name = "compute")
	private Boolean compute;

	public Integer getContractId() {
		return this.contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public Boolean getCompute() {
		return this.compute;
	}

	public void setCompute(Boolean compute) {
		this.compute = compute;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
