/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ProductIATA {

	_EMPTY_("", ""), _ADT_("ADT", "Additive Fee"), _ADTV_("ADTV",
			"Additive (Generic)"), _AG100_("AG100",
			"Aviation Gasoline 100/130 low"), _AG1HL_("AG1HL",
			"Aviation Gasoline 100/130 high lead"), _AG80_("AG80",
			"Aviation Gasoline 80/87"), _AHF_("AHF", "After-hours Fee"), _AIF_(
			"AIF", "Additive Injection Fee"), _APT_("APT", "Airport Fee"), _ATT_(
			"ATT", "Attendance Fee"), _CCF_("CCF", "Cash collection fee"), _CHN_(
			"CHN", "Channel Fee"), _CNF_("CNF", "Consortium Fee"), _CNT_("CNT",
			"Container Surcharge"), _COM_("COM", "Combustible Fee"), _CON_(
			"CON", "Concession Fee"), _CSF_("CSF", "Compulsory Storage Fee"), _CUF_(
			"CUF", "Combined User Fee"), _DEF_("DEF", "Defuelling Fee"), _DEH_(
			"DEH", "Hangar/Maintenance Defuel Fee"), _DIF_("DIF",
			"Formula Differential "), _DIS_("DIS", "Discount"), _DMF_("DMF",
			"Demurrage Fee"), _ENV_("ENV", "Environmental Fee"), _EWF_("EWF",
			"Extended Wing Fee"), _FLF_("FLF", "Fuel Flowage Fee"), _FMJ_(
			"FMJ", "Force Majeure Fee"), _FRF_("FRF", "Freight Fee"), _GAF_(
			"GAF", "General Aviation Fee"), _GSF_("GSF", "Ground Service Fee"), _HNG_(
			"HNG", "Hangar/Maintenance Fuel Fee"), _HUF_("HUF", "Hook Up Fee"), _HYD_(
			"HYD", "Hydrant Fee"), _INF_("INF", "Infrastructure Charge"), _INS_(
			"INS", "Inspection Fee"), _ITP_("ITP", "Into Plane Fee"), _JA1A_(
			"JA1A", "Jet Fuel Jet A-1 with Additive"), _JAA_("JAA",
			"Jet Fuel Jet A with Additive"), _JETA_("JETA", "Jet Fuel Jet A"), _JETA1_(
			"JETA1", "Jet Fuel Jet A-1"), _JP4_("JP4", "Jet Fuel Military JP4"), _JP5_(
			"JP5", "Jet Fuel Military JP5"), _JP8_("JP8",
			"Jet Fuel Military JP8"), _JSF_("JSF", "Joint Storage Fee"), _LVF_(
			"LVF", "Low Volume Fee"), _MVF_("MVF", "Multi-Vehicle Fee"), _NFF_(
			"NFF", "No Fuel Fee"), _NON_("NON", "None"), _OFF_("OFF",
			"Fueling Off-ramp"), _OTH_("OTH", "Other"), _OVT_("OVT",
			"Overtime Fee"), _OWF_("OWF", "Overwing Fee"), _PLF_("PLF",
			"Pipeline Fee"), _REB_("REB", "Rebate"), _STR_("STR", "Storage Fee"), _SYS_(
			"SYS", "System Fee"), _THP_("THP", "Throughput Fee"), _TPF_("TPF",
			"Trade Promotion Fee"), _TRF_("TRF", "Trucking Fee"), _TS1_("TS1",
			"Jet Fuel TS-1");

	private String name;
	private String description;

	private ProductIATA(String name, String description) {
		this.name = name;
		this.description = description;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ProductIATA getByName(String name) {
		for (ProductIATA status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent ProductIATA with name: "
				+ name);
	}

	public String getDescription() {
		return this.description;
	}

	public static ProductIATA getByDescription(String code) {
		for (ProductIATA status : values()) {
			if (status.getDescription().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent ProductIATA with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
