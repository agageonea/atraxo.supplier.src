/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum BiddingStatus {

	_EMPTY_(""), _NEW_("New"), _IN_NEGOTIATION_("In negotiation"), _FINISHED_(
			"Finished"), _NO_BID_("No bid");

	private String name;

	private BiddingStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static BiddingStatus getByName(String name) {
		for (BiddingStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent BiddingStatus with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
