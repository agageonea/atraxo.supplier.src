/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TransmissionStatus {

	_EMPTY_(""), _NEW_("New"), _IN_PROGRESS_("In progress"), _TRANSMITTED_(
			"Transmitted"), _FAILED_("Failed"), _RECEIVED_("Received"), _READ_(
			"Read"), _PROCESSED_("Processed");

	private String name;

	private TransmissionStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TransmissionStatus getByName(String name) {
		for (TransmissionStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent TransmissionStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
