/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link RestrictionTypes} enum.
 * Generated code. Do not modify in this file.
 */
public class RestrictionTypesConverter
		implements
			AttributeConverter<RestrictionTypes, String> {

	@Override
	public String convertToDatabaseColumn(RestrictionTypes value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null RestrictionTypes.");
		}
		return value.getName();
	}

	@Override
	public RestrictionTypes convertToEntityAttribute(String value) {
		return RestrictionTypes.getByName(value);
	}

}
