/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link SourceType} enum.
 * Generated code. Do not modify in this file.
 */
public class SourceTypeConverter
		implements
			AttributeConverter<SourceType, String> {

	@Override
	public String convertToDatabaseColumn(SourceType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null SourceType.");
		}
		return value.getName();
	}

	@Override
	public SourceType convertToEntityAttribute(String value) {
		return SourceType.getByName(value);
	}

}
