/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ExportTypeBid} enum.
 * Generated code. Do not modify in this file.
 */
public class ExportTypeBidConverter
		implements
			AttributeConverter<ExportTypeBid, String> {

	@Override
	public String convertToDatabaseColumn(ExportTypeBid value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ExportTypeBid.");
		}
		return value.getName();
	}

	@Override
	public ExportTypeBid convertToEntityAttribute(String value) {
		return ExportTypeBid.getByName(value);
	}

}
