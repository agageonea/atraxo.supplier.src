/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum PaymentChoise {

	_EMPTY_("", ""), _OPEN_CREDIT_("Open credit", "Open invoice"), _PREPAYMENT_(
			"Prepayment", "Prepayment");

	private String name;
	private String code;

	private PaymentChoise(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static PaymentChoise getByName(String name) {
		for (PaymentChoise status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent PaymentChoise with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static PaymentChoise getByCode(String code) {
		for (PaymentChoise status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent PaymentChoise with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
