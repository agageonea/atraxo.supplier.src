/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.contracts;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.ContractChangeType;
import atraxo.fmbas.domain.impl.fmbas_type.ContractChangeTypeConverter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link ContractChange} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = ContractChange.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ContractChange e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ContractChange.TABLE_NAME)
public class ContractChange extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "CMM_CONTRACT_CHANGE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ContractChange.findByBusiness";

	@NotNull
	@Column(name = "contract_id", nullable = false, precision = 11)
	private Integer contractId;

	@NotBlank
	@Column(name = "type", nullable = false, length = 50)
	@Convert(converter = ContractChangeTypeConverter.class)
	private ContractChangeType type;

	@Column(name = "message", length = 2500)
	private String message;

	public Integer getContractId() {
		return this.contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public ContractChangeType getType() {
		return this.type;
	}

	public void setType(ContractChangeType type) {
		this.type = type;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
