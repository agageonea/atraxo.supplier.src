/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.prices;

import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicatorConverter;
import atraxo.cmm.domain.impl.cmm_type.QuantityType;
import atraxo.cmm.domain.impl.cmm_type.QuantityTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.VatApplicability;
import atraxo.cmm.domain.impl.cmm_type.VatApplicabilityConverter;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriodConverter;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link ContractPriceCategory} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = ContractPriceCategory.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ContractPriceCategory e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ContractPriceCategory.TABLE_NAME)
public class ContractPriceCategory extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "CMM_CONTR_PRICE_CTGRY";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ContractPriceCategory.findByBusiness";

	@Column(name = "name", length = 100)
	private String name;

	@Column(name = "continous")
	private Boolean continous;

	@Column(name = "include_in_average")
	private Boolean includeInAverage;

	@Column(name = "quantity_type", length = 32)
	@Convert(converter = QuantityTypeConverter.class)
	private QuantityType quantityType;

	@Column(name = "exchange_rate_offset", length = 32)
	@Convert(converter = MasterAgreementsPeriodConverter.class)
	private MasterAgreementsPeriod exchangeRateOffset;

	@Column(name = "restrictions")
	private Boolean restriction;

	@Column(name = "vat", length = 32)
	@Convert(converter = VatApplicabilityConverter.class)
	private VatApplicability vat;

	@Column(name = "default_price_ctgy")
	private Boolean defaultPriceCtgy;

	@Column(name = "comments", length = 255)
	private String comments;

	@Column(name = "calculate_indicator", length = 32)
	@Convert(converter = CalculateIndicatorConverter.class)
	private CalculateIndicator calculateIndicator;

	@Column(name = "src_contr_price_cat_id", precision = 11)
	private Integer resaleRef;

	@Column(name = "orderindex", precision = 10)
	private Integer orderIndex;

	@Transient
	private String forex;

	@Transient
	private BigDecimal initialPrice;

	@Transient
	private Integer initialCurrId;

	@Transient
	private Integer initialUnitId;

	@Transient
	private String priceCategoryIdsJson;

	@Transient
	private Boolean orderChanged;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contract.class)
	@JoinColumn(name = "contract_id", referencedColumnName = "id")
	private Contract contract;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = PriceCategory.class)
	@JoinColumn(name = "price_category_id", referencedColumnName = "id")
	private PriceCategory priceCategory;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = PricingBase.class)
	@JoinColumn(name = "contr_price_id", referencedColumnName = "id")
	private PricingBase pricingBases;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FinancialSources.class)
	@JoinColumn(name = "financial_source_id", referencedColumnName = "id")
	private FinancialSources financialSource;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AverageMethod.class)
	@JoinColumn(name = "average_method_id", referencedColumnName = "id")
	private AverageMethod averageMethod;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = ContractPriceComponent.class, mappedBy = "contrPriceCtgry", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<ContractPriceComponent> priceComponents;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = ContractPriceRestriction.class, mappedBy = "contractPriceCategory", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<ContractPriceRestriction> priceRestrictions;

	@ManyToMany(mappedBy = "parentPriceCategory")
	@JoinTable(name = "BAS_CONTRACT_PRICE_CTGRY_ASSIGN", joinColumns = {@JoinColumn(name = "PRICE_CATEGORY_ID")}, inverseJoinColumns = {@JoinColumn(name = "PRICE_CATGRPRY_ASSIGN_ID")})
	private Collection<ContractPriceCategory> childPriceCategory;

	@ManyToMany
	@JoinTable(name = "BAS_CONTRACT_PRICE_CTGRY_ASSIGN", joinColumns = {@JoinColumn(name = "PRICE_CATGRPRY_ASSIGN_ID")}, inverseJoinColumns = {@JoinColumn(name = "PRICE_CATEGORY_ID")})
	private Collection<ContractPriceCategory> parentPriceCategory;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getContinous() {
		return this.continous;
	}

	public void setContinous(Boolean continous) {
		this.continous = continous;
	}

	public Boolean getIncludeInAverage() {
		return this.includeInAverage;
	}

	public void setIncludeInAverage(Boolean includeInAverage) {
		this.includeInAverage = includeInAverage;
	}

	public QuantityType getQuantityType() {
		return this.quantityType;
	}

	public void setQuantityType(QuantityType quantityType) {
		this.quantityType = quantityType;
	}

	public MasterAgreementsPeriod getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(MasterAgreementsPeriod exchangeRateOffset) {
		this.exchangeRateOffset = exchangeRateOffset;
	}

	public Boolean getRestriction() {
		return this.restriction;
	}

	public void setRestriction(Boolean restriction) {
		this.restriction = restriction;
	}

	public VatApplicability getVat() {
		return this.vat;
	}

	public void setVat(VatApplicability vat) {
		this.vat = vat;
	}

	public Boolean getDefaultPriceCtgy() {
		return this.defaultPriceCtgy;
	}

	public void setDefaultPriceCtgy(Boolean defaultPriceCtgy) {
		this.defaultPriceCtgy = defaultPriceCtgy;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public CalculateIndicator getCalculateIndicator() {
		return this.calculateIndicator;
	}

	public void setCalculateIndicator(CalculateIndicator calculateIndicator) {
		this.calculateIndicator = calculateIndicator;
	}

	public Integer getResaleRef() {
		return this.resaleRef;
	}

	public void setResaleRef(Integer resaleRef) {
		this.resaleRef = resaleRef;
	}

	public Integer getOrderIndex() {
		return this.orderIndex;
	}

	public void setOrderIndex(Integer orderIndex) {
		this.orderIndex = orderIndex;
	}

	public String getForex() {
		return this.getFinancialSource().getCode() + "/"
				+ this.getAverageMethod().getName();
	}

	public void setForex(String forex) {
		this.forex = forex;
	}

	public BigDecimal getInitialPrice() {
		return this.initialPrice;
	}

	public void setInitialPrice(BigDecimal initialPrice) {
		this.initialPrice = initialPrice;
	}

	public Integer getInitialCurrId() {
		return this.initialCurrId;
	}

	public void setInitialCurrId(Integer initialCurrId) {
		this.initialCurrId = initialCurrId;
	}

	public Integer getInitialUnitId() {
		return this.initialUnitId;
	}

	public void setInitialUnitId(Integer initialUnitId) {
		this.initialUnitId = initialUnitId;
	}

	public String getPriceCategoryIdsJson() {
		return this.priceCategoryIdsJson;
	}

	public void setPriceCategoryIdsJson(String priceCategoryIdsJson) {
		this.priceCategoryIdsJson = priceCategoryIdsJson;
	}

	public Boolean getOrderChanged() {
		return this.orderChanged;
	}

	public void setOrderChanged(Boolean orderChanged) {
		this.orderChanged = orderChanged;
	}

	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		if (contract != null) {
			this.__validate_client_context__(contract.getClientId());
		}
		this.contract = contract;
	}
	public PriceCategory getPriceCategory() {
		return this.priceCategory;
	}

	public void setPriceCategory(PriceCategory priceCategory) {
		if (priceCategory != null) {
			this.__validate_client_context__(priceCategory.getClientId());
		}
		this.priceCategory = priceCategory;
	}
	public PricingBase getPricingBases() {
		return this.pricingBases;
	}

	public void setPricingBases(PricingBase pricingBases) {
		if (pricingBases != null) {
			this.__validate_client_context__(pricingBases.getClientId());
		}
		this.pricingBases = pricingBases;
	}
	public FinancialSources getFinancialSource() {
		return this.financialSource;
	}

	public void setFinancialSource(FinancialSources financialSource) {
		if (financialSource != null) {
			this.__validate_client_context__(financialSource.getClientId());
		}
		this.financialSource = financialSource;
	}
	public AverageMethod getAverageMethod() {
		return this.averageMethod;
	}

	public void setAverageMethod(AverageMethod averageMethod) {
		if (averageMethod != null) {
			this.__validate_client_context__(averageMethod.getClientId());
		}
		this.averageMethod = averageMethod;
	}

	public Collection<ContractPriceComponent> getPriceComponents() {
		return this.priceComponents;
	}

	public void setPriceComponents(
			Collection<ContractPriceComponent> priceComponents) {
		this.priceComponents = priceComponents;
	}

	/**
	 * @param e
	 */
	public void addToPriceComponents(ContractPriceComponent e) {
		if (this.priceComponents == null) {
			this.priceComponents = new ArrayList<>();
		}
		e.setContrPriceCtgry(this);
		this.priceComponents.add(e);
	}
	public Collection<ContractPriceRestriction> getPriceRestrictions() {
		return this.priceRestrictions;
	}

	public void setPriceRestrictions(
			Collection<ContractPriceRestriction> priceRestrictions) {
		this.priceRestrictions = priceRestrictions;
	}

	/**
	 * @param e
	 */
	public void addToPriceRestrictions(ContractPriceRestriction e) {
		if (this.priceRestrictions == null) {
			this.priceRestrictions = new ArrayList<>();
		}
		e.setContractPriceCategory(this);
		this.priceRestrictions.add(e);
	}
	public Collection<ContractPriceCategory> getChildPriceCategory() {
		return this.childPriceCategory;
	}

	public void setChildPriceCategory(
			Collection<ContractPriceCategory> childPriceCategory) {
		this.childPriceCategory = childPriceCategory;
	}

	public Collection<ContractPriceCategory> getParentPriceCategory() {
		return this.parentPriceCategory;
	}

	public void setParentPriceCategory(
			Collection<ContractPriceCategory> parentPriceCategory) {
		this.parentPriceCategory = parentPriceCategory;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
