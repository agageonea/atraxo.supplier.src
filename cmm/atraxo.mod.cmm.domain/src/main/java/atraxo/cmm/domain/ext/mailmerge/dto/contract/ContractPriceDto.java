package atraxo.cmm.domain.ext.mailmerge.dto.contract;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class ContractPriceDto {

	private String name;
	private String categoryName;
	private Date validFrom;
	private BigDecimal oldPrice;
	private BigDecimal newPrice;
	private BigDecimal oldSettlementPrice;
	private BigDecimal newSettlementPrice;
	private String settlementCurrencyUnit;
	private BigDecimal newExchangeRate;
	private BigDecimal oldExchangeRate;
	private Boolean restriction;
	private String iataCode;
	private String iataType;
	private BigDecimal price;
	private String currency;
	private String currencyUnit;
	private List<RestrictionDto> restrictions;
	private Integer orderIndex;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public BigDecimal getOldPrice() {
		return this.oldPrice;
	}

	public void setOldPrice(BigDecimal oldPrice) {
		this.oldPrice = oldPrice;
	}

	public BigDecimal getNewPrice() {
		return this.newPrice;
	}

	public void setNewPrice(BigDecimal newPrice) {
		this.newPrice = newPrice;
	}

	public BigDecimal getOldSettlementPrice() {
		return this.oldSettlementPrice;
	}

	public void setOldSettlementPrice(BigDecimal oldSettlementPrice) {
		this.oldSettlementPrice = oldSettlementPrice;
	}

	public BigDecimal getNewSettlementPrice() {
		return this.newSettlementPrice;
	}

	public void setNewSettlementPrice(BigDecimal newSettlementPrice) {
		this.newSettlementPrice = newSettlementPrice;
	}

	public String getSettlementCurrencyUnit() {
		return this.settlementCurrencyUnit;
	}

	public void setSettlementCurrencyUnit(String settlementCurrencyUnit) {
		this.settlementCurrencyUnit = settlementCurrencyUnit;
	}

	public BigDecimal getNewExchangeRate() {
		return this.newExchangeRate;
	}

	public void setNewExchangeRate(BigDecimal newExchangeRate) {
		this.newExchangeRate = newExchangeRate;
	}

	public BigDecimal getOldExchangeRate() {
		return this.oldExchangeRate;
	}

	public void setOldExchangeRate(BigDecimal oldExchangeRate) {
		this.oldExchangeRate = oldExchangeRate;
	}

	public Boolean getRestriction() {
		return this.restriction;
	}

	public void setRestriction(Boolean restriction) {
		this.restriction = restriction;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public String getIataType() {
		return this.iataType;
	}

	public void setIataType(String iataType) {
		this.iataType = iataType;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCurrencyUnit() {
		return this.currencyUnit;
	}

	public void setCurrencyUnit(String currencyUnit) {
		this.currencyUnit = currencyUnit;
	}

	public List<RestrictionDto> getRestrictions() {
		return this.restrictions;
	}

	public void setRestrictions(List<RestrictionDto> restrictions) {
		this.restrictions = restrictions;
	}

	public Integer getOrderIndex() {
		return this.orderIndex;
	}

	public void setOrderIndex(Integer orderIndex) {
		this.orderIndex = orderIndex;
	}

}
