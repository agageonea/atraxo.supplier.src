/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TaxType {

	_EMPTY_("", ""), _UNSPECIFIED_("Unspecified", "NA"), _BONDED_("Bonded",
			"BD"), _DOMESTIC_("Domestic", "DOM"), _FOREIGN_TRADE_ZONE_(
			"Foreign trade zone", "FTZ"), _DUTY_FREE_("Duty free", "DF"), _DUTY_PAID_(
			"Duty paid", "DP"), _OTHER_("Other", "OT");

	private String name;
	private String iataCode;

	private TaxType(String name, String iataCode) {
		this.name = name;
		this.iataCode = iataCode;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TaxType getByName(String name) {
		for (TaxType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TaxType with name: " + name);
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public static TaxType getByIataCode(String code) {
		for (TaxType status : values()) {
			if (status.getIataCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TaxType with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
