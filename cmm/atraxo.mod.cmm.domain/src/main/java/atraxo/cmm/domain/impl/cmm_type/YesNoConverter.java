/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link YesNo} enum.
 * Generated code. Do not modify in this file.
 */
public class YesNoConverter implements AttributeConverter<YesNo, String> {

	@Override
	public String convertToDatabaseColumn(YesNo value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null YesNo.");
		}
		return value.getName();
	}

	@Override
	public YesNo convertToEntityAttribute(String value) {
		return YesNo.getByName(value);
	}

}
