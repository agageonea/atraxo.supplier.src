/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link EventType} enum.
 * Generated code. Do not modify in this file.
 */
public class EventTypeConverter
		implements
			AttributeConverter<EventType, String> {

	@Override
	public String convertToDatabaseColumn(EventType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null EventType.");
		}
		return value.getName();
	}

	@Override
	public EventType convertToEntityAttribute(String value) {
		return EventType.getByName(value);
	}

}
