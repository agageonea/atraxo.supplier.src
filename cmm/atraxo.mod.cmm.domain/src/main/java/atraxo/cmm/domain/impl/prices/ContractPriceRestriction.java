/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.prices;

import atraxo.cmm.domain.impl.cmm_type.Operators;
import atraxo.cmm.domain.impl.cmm_type.OperatorsConverter;
import atraxo.cmm.domain.impl.cmm_type.RestrictionTypes;
import atraxo.cmm.domain.impl.cmm_type.RestrictionTypesConverter;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link ContractPriceRestriction} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = ContractPriceRestriction.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ContractPriceRestriction e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ContractPriceRestriction.TABLE_NAME)
public class ContractPriceRestriction extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "CMM_CONTR_PRICE_RESTRICT";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ContractPriceRestriction.findByBusiness";

	@Column(name = "restriction_type", length = 32)
	@Convert(converter = RestrictionTypesConverter.class)
	private RestrictionTypes restrictionType;

	@Column(name = "operator", length = 32)
	@Convert(converter = OperatorsConverter.class)
	private Operators operator;

	@Column(name = "value", length = 100)
	private String value;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ContractPriceCategory.class)
	@JoinColumn(name = "contr_price_ctgry_id", referencedColumnName = "id")
	private ContractPriceCategory contractPriceCategory;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_id", referencedColumnName = "id")
	private Unit unit;

	public RestrictionTypes getRestrictionType() {
		return this.restrictionType;
	}

	public void setRestrictionType(RestrictionTypes restrictionType) {
		this.restrictionType = restrictionType;
	}

	public Operators getOperator() {
		return this.operator;
	}

	public void setOperator(Operators operator) {
		this.operator = operator;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public ContractPriceCategory getContractPriceCategory() {
		return this.contractPriceCategory;
	}

	public void setContractPriceCategory(
			ContractPriceCategory contractPriceCategory) {
		if (contractPriceCategory != null) {
			this.__validate_client_context__(contractPriceCategory
					.getClientId());
		}
		this.contractPriceCategory = contractPriceCategory;
	}
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		if (unit != null) {
			this.__validate_client_context__(unit.getClientId());
		}
		this.unit = unit;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
