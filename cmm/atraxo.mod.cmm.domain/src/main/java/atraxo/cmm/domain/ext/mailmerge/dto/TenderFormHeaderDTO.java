package atraxo.cmm.domain.ext.mailmerge.dto;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;

public class TenderFormHeaderDTO extends AbstractDto {

	private TenderFormDTO tender;
	private SubsidiaryFormDTO subsidiary;

	public TenderFormDTO getTender() {
		return this.tender;
	}

	public void setTender(TenderFormDTO tender) {
		this.tender = tender;
	}

	public SubsidiaryFormDTO getSubsidiary() {
		return this.subsidiary;
	}

	public void setSubsidiary(SubsidiaryFormDTO subsidiary) {
		this.subsidiary = subsidiary;
	}

}
