/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum BillStatus {

	_EMPTY_(""), _NOT_BILLED_("Not billed"), _DRAFT_("Draft"), _CHECK_FAILED_(
			"Check failed"), _CHECK_PASSED_("Check passed"), _NEEDS_RECHECK_(
			"Needs recheck"), _AWAITING_APPROVAL_("Awaiting approval"), _AWAITING_PAYMENT_(
			"Awaiting payment"), _PAID_("Paid"), _NO_CONTRACT_("No Contract"), _CREDITED_(
			"Credited");

	private String name;

	private BillStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static BillStatus getByName(String name) {
		for (BillStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent BillStatus with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
