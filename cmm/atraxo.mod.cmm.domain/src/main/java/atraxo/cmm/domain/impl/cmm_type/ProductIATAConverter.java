/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ProductIATA} enum.
 * Generated code. Do not modify in this file.
 */
public class ProductIATAConverter
		implements
			AttributeConverter<ProductIATA, String> {

	@Override
	public String convertToDatabaseColumn(ProductIATA value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ProductIATA.");
		}
		return value.getName();
	}

	@Override
	public ProductIATA convertToEntityAttribute(String value) {
		return ProductIATA.getByName(value);
	}

}
