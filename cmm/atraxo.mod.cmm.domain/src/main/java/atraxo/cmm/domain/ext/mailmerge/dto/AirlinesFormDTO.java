package atraxo.cmm.domain.ext.mailmerge.dto;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonProperty;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;

public class AirlinesFormDTO extends AbstractDto {

	@JsonProperty("iata_code")
	private String iataCode;

	@JsonProperty("volume")
	private BigDecimal volume;

	@JsonProperty("volume_unit")
	private String volumeUnit;

	@JsonProperty("comment")
	private String comment;

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public BigDecimal getVolume() {
		return this.volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public String getVolumeUnit() {
		return this.volumeUnit;
	}

	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
