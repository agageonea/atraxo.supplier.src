/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FlightTypeIndicator} enum.
 * Generated code. Do not modify in this file.
 */
public class FlightTypeIndicatorConverter
		implements
			AttributeConverter<FlightTypeIndicator, String> {

	@Override
	public String convertToDatabaseColumn(FlightTypeIndicator value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null FlightTypeIndicator.");
		}
		return value.getName();
	}

	@Override
	public FlightTypeIndicator convertToEntityAttribute(String value) {
		return FlightTypeIndicator.getByName(value);
	}

}
