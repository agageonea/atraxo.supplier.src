/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ReviewPeriod} enum.
 * Generated code. Do not modify in this file.
 */
public class ReviewPeriodConverter
		implements
			AttributeConverter<ReviewPeriod, String> {

	@Override
	public String convertToDatabaseColumn(ReviewPeriod value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ReviewPeriod.");
		}
		return value.getName();
	}

	@Override
	public ReviewPeriod convertToEntityAttribute(String value) {
		return ReviewPeriod.getByName(value);
	}

}
