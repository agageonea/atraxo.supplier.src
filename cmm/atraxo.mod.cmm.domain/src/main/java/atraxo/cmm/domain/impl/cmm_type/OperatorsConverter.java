/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link Operators} enum.
 * Generated code. Do not modify in this file.
 */
public class OperatorsConverter
		implements
			AttributeConverter<Operators, String> {

	@Override
	public String convertToDatabaseColumn(Operators value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null Operators.");
		}
		return value.getName();
	}

	@Override
	public Operators convertToEntityAttribute(String value) {
		return Operators.getByName(value);
	}

}
