/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum PendingAction {

	_EMPTY_(""), _ADD_("Add"), _UPDATE_("Update"), _DELETE_("Delete");

	private String name;

	private PendingAction(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static PendingAction getByName(String name) {
		for (PendingAction status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent PendingAction with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
