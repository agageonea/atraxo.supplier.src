/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.tender;

import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatusConverter;
import atraxo.cmm.domain.impl.cmm_type.Period;
import atraxo.cmm.domain.impl.cmm_type.PeriodConverter;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link TenderLocationAirlines} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = TenderLocationAirlines.NQ_FIND_BY_TENDER_LOCATION_AIRLINE, query = "SELECT e FROM TenderLocationAirlines e WHERE e.clientId = :clientId and e.tenderLocation = :tenderLocation and e.airline = :airline", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = TenderLocationAirlines.NQ_FIND_BY_TENDER_LOCATION_AIRLINE_PRIMITIVE, query = "SELECT e FROM TenderLocationAirlines e WHERE e.clientId = :clientId and e.tenderLocation.id = :tenderLocationId and e.airline.id = :airlineId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = TenderLocationAirlines.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM TenderLocationAirlines e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = TenderLocationAirlines.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = TenderLocationAirlines.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "tender_location_id", "airline_id"})})
public class TenderLocationAirlines extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "CMM_TENDER_LOCATION_AIRLINES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Tender_location_airline.
	 */
	public static final String NQ_FIND_BY_TENDER_LOCATION_AIRLINE = "TenderLocationAirlines.findByTender_location_airline";
	/**
	 * Named query find by unique key: Tender_location_airline using the ID field for references.
	 */
	public static final String NQ_FIND_BY_TENDER_LOCATION_AIRLINE_PRIMITIVE = "TenderLocationAirlines.findByTender_location_airline_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "TenderLocationAirlines.findByBusiness";

	@Column(name = "volume", precision = 19, scale = 6)
	private BigDecimal volume;

	@Column(name = "volume_period", length = 32)
	@Convert(converter = PeriodConverter.class)
	private Period volumePeriod;

	@NotBlank
	@Column(name = "bidding_status", nullable = false, length = 32)
	@Convert(converter = BiddingStatusConverter.class)
	private BiddingStatus biddingStatus;

	@Column(name = "notes", length = 4000)
	private String notes;

	@NotNull
	@Column(name = "isvalid", nullable = false)
	private Boolean isValid;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = TenderLocation.class)
	@JoinColumn(name = "tender_location_id", referencedColumnName = "id")
	private TenderLocation tenderLocation;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "airline_id", referencedColumnName = "id")
	private Customer airline;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_id", referencedColumnName = "id")
	private Unit unit;

	public BigDecimal getVolume() {
		return this.volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public Period getVolumePeriod() {
		return this.volumePeriod;
	}

	public void setVolumePeriod(Period volumePeriod) {
		this.volumePeriod = volumePeriod;
	}

	public BiddingStatus getBiddingStatus() {
		return this.biddingStatus;
	}

	public void setBiddingStatus(BiddingStatus biddingStatus) {
		this.biddingStatus = biddingStatus;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}

	public TenderLocation getTenderLocation() {
		return this.tenderLocation;
	}

	public void setTenderLocation(TenderLocation tenderLocation) {
		if (tenderLocation != null) {
			this.__validate_client_context__(tenderLocation.getClientId());
		}
		this.tenderLocation = tenderLocation;
	}
	public Customer getAirline() {
		return this.airline;
	}

	public void setAirline(Customer airline) {
		if (airline != null) {
			this.__validate_client_context__(airline.getClientId());
		}
		this.airline = airline;
	}
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		if (unit != null) {
			this.__validate_client_context__(unit.getClientId());
		}
		this.unit = unit;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.isValid == null) {
			this.isValid = new Boolean(true);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime
				* result
				+ ((this.tenderLocation == null) ? 0 : this.tenderLocation
						.hashCode());
		result = prime * result
				+ ((this.airline == null) ? 0 : this.airline.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		TenderLocationAirlines other = (TenderLocationAirlines) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.tenderLocation == null) {
			if (other.tenderLocation != null) {
				return false;
			}
		} else if (!this.tenderLocation.equals(other.tenderLocation)) {
			return false;
		}
		if (this.airline == null) {
			if (other.airline != null) {
				return false;
			}
		} else if (!this.airline.equals(other.airline)) {
			return false;
		}
		return true;
	}
}
