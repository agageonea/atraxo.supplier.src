/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link BusinessType} enum.
 * Generated code. Do not modify in this file.
 */
public class BusinessTypeConverter
		implements
			AttributeConverter<BusinessType, String> {

	@Override
	public String convertToDatabaseColumn(BusinessType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null BusinessType.");
		}
		return value.getName();
	}

	@Override
	public BusinessType convertToEntityAttribute(String value) {
		return BusinessType.getByName(value);
	}

}
