/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.contracts;

import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatusConverter;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.BidStatusConverter;
import atraxo.cmm.domain.impl.cmm_type.ContractScope;
import atraxo.cmm.domain.impl.cmm_type.ContractScopeConverter;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractStatusConverter;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.ContractSubTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.ContractTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.DealTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.EventType;
import atraxo.cmm.domain.impl.cmm_type.EventTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicatorConverter;
import atraxo.cmm.domain.impl.cmm_type.InvoicePayableBy;
import atraxo.cmm.domain.impl.cmm_type.InvoicePayableByConverter;
import atraxo.cmm.domain.impl.cmm_type.IsSpot;
import atraxo.cmm.domain.impl.cmm_type.IsSpotConverter;
import atraxo.cmm.domain.impl.cmm_type.PaymentType;
import atraxo.cmm.domain.impl.cmm_type.PaymentTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.Period;
import atraxo.cmm.domain.impl.cmm_type.PeriodConverter;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.ProductConverter;
import atraxo.cmm.domain.impl.cmm_type.QuantityType;
import atraxo.cmm.domain.impl.cmm_type.QuantityTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.ReviewPeriod;
import atraxo.cmm.domain.impl.cmm_type.ReviewPeriodConverter;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.cmm_type.TaxTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatusConverter;
import atraxo.cmm.domain.impl.cmm_type.VatApplicability;
import atraxo.cmm.domain.impl.cmm_type.VatApplicabilityConverter;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.ContractInvoice;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.domain.impl.abstracts.AbstractSubsidiary;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTermConverter;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreqConverter;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceType;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceTypeConverter;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriodConverter;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDayConverter;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link Contract} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Contract.NQ_FIND_BY_CODE, query = "SELECT e FROM Contract e WHERE e.clientId = :clientId and e.code = :code and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Contract.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Contract e WHERE e.clientId = :clientId and e.id = :id and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Contract.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Contract.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "code"})})
public class Contract extends AbstractSubsidiary implements Serializable {

	public static final String TABLE_NAME = "CMM_CONTRACTS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "Contract.findByCode";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Contract.findByBusiness";

	@Column(name = "code", length = 32)
	private String code;

	@Column(name = "deal_type", length = 32)
	@Convert(converter = DealTypeConverter.class)
	private DealType dealType;

	@Column(name = "type", length = 32)
	@Convert(converter = ContractTypeConverter.class)
	private ContractType type;

	@Column(name = "flight_service_type", length = 16)
	@Convert(converter = FlightServiceTypeConverter.class)
	private FlightServiceType flightServiceType;

	@Column(name = "subtype", length = 32)
	@Convert(converter = ContractSubTypeConverter.class)
	private ContractSubType subType;

	@Column(name = "scope", length = 32)
	@Convert(converter = ContractScopeConverter.class)
	private ContractScope scope;

	@Column(name = "isspot", length = 32)
	@Convert(converter = IsSpotConverter.class)
	private IsSpot isSpot;

	@Temporal(TemporalType.DATE)
	@Column(name = "valid_from")
	private Date validFrom;

	@Temporal(TemporalType.DATE)
	@Column(name = "valid_to")
	private Date validTo;

	@Column(name = "status", length = 32)
	@Convert(converter = ContractStatusConverter.class)
	private ContractStatus status;

	@Column(name = "limited_to", length = 16)
	@Convert(converter = FlightTypeIndicatorConverter.class)
	private FlightTypeIndicator limitedTo;

	@Column(name = "product", length = 32)
	@Convert(converter = ProductConverter.class)
	private Product product;

	@Column(name = "tax", length = 32)
	@Convert(converter = TaxTypeConverter.class)
	private TaxType tax;

	@Column(name = "quantity_type", length = 32)
	@Convert(converter = QuantityTypeConverter.class)
	private QuantityType quantityType;

	@Column(name = "contracted_volume_period", length = 32)
	@Convert(converter = PeriodConverter.class)
	private Period period;

	@Column(name = "volume_tolerance", precision = 4)
	private Integer volumeTolerance;

	@Column(name = "volume_share", precision = 4)
	private Integer volumeShare;

	@Column(name = "counterparty_reference", length = 32)
	private String counterpartyReference;

	@Column(name = "settlement_decimals", precision = 4)
	private Integer settlementDecimals;

	@Column(name = "exchange_rate_offset", length = 32)
	@Convert(converter = MasterAgreementsPeriodConverter.class)
	private MasterAgreementsPeriod exchangeRateOffset;

	@Column(name = "payment_terms", precision = 4)
	private Integer paymentTerms;

	@Column(name = "payment_reference_day", length = 32)
	@Convert(converter = PaymentDayConverter.class)
	private PaymentDay paymentRefDay;

	@Column(name = "invoice_frequency", length = 32)
	@Convert(converter = InvoiceFreqConverter.class)
	private InvoiceFreq invoiceFreq;

	@Column(name = "invoice_frequency_number", precision = 2)
	private Integer invoiceFreqNumber;

	@Column(name = "credit_terms", length = 32)
	@Convert(converter = CreditTermConverter.class)
	private CreditTerm creditTerms;

	@Column(name = "invoice_type", length = 32)
	@Convert(converter = InvoiceTypeConverter.class)
	private InvoiceType invoiceType;

	@Column(name = "invoice_payable_by", length = 32)
	@Convert(converter = InvoicePayableByConverter.class)
	private InvoicePayableBy invoicePayableBy;

	@Column(name = "vat", length = 32)
	@Convert(converter = VatApplicabilityConverter.class)
	private VatApplicability vat;

	@Column(name = "review_period", length = 32)
	@Convert(converter = ReviewPeriodConverter.class)
	private ReviewPeriod reviewPeriod;

	@Column(name = "review_first_parameter", length = 32)
	private String reviewFirstParam;

	@Column(name = "review_second_parameter", length = 32)
	private String reviewSecondParam;

	@Column(name = "review_notification", precision = 4)
	private Integer reviewNotification;

	@Column(name = "payment_comment", length = 255)
	private String paymentComment;

	@Column(name = "event_type", length = 32)
	@Convert(converter = EventTypeConverter.class)
	private EventType eventType;

	@Column(name = "awarded_volume", precision = 19, scale = 6)
	private BigDecimal awardedVolume;

	@Column(name = "offered_volume", precision = 19, scale = 6)
	private BigDecimal offeredVolume;

	@Column(name = "iata_service_level", precision = 1)
	private Integer iataServiceLevel;

	@Transient
	private String forex;

	@Column(name = "hard_copy")
	private Boolean hardCopy;

	@NotNull
	@Column(name = "iscontract", nullable = false)
	private Boolean isContract;

	@NotNull
	@Column(name = "readonly", nullable = false)
	private Boolean readOnly;

	@NotNull
	@Column(name = "isblueprint", nullable = false)
	private Boolean isBlueprint;

	@Column(name = "bid_status", length = 32)
	@Convert(converter = BidStatusConverter.class)
	private BidStatus bidStatus;

	@Column(name = "bid_transmission_status", length = 32)
	@Convert(converter = TransmissionStatusConverter.class)
	private TransmissionStatus bidTransmissionStatus;

	@Column(name = "bid_cancel_transmission_status", length = 32)
	@Convert(converter = TransmissionStatusConverter.class)
	private TransmissionStatus bidCancelTransmitionStatus;

	@Column(name = "bid_accept_award_transmission_status", length = 32)
	@Convert(converter = TransmissionStatusConverter.class)
	private TransmissionStatus bidAcceptAwardTransmissionStatus;

	@Column(name = "bid_decline_award_transmission_status", length = 32)
	@Convert(converter = TransmissionStatusConverter.class)
	private TransmissionStatus bidDeclineAwardTransmissionStatus;

	@Column(name = "bid_approval_status", length = 32)
	@Convert(converter = BidApprovalStatusConverter.class)
	private BidApprovalStatus bidApprovalStatus;

	@Column(name = "bid_revision", precision = 8)
	private Integer bidRevision;

	@Column(name = "bid_version", length = 2)
	private String bidVersion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "bid_valid_from")
	private Date bidValidFrom;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "bid_valid_to")
	private Date bidValidTo;

	@Column(name = "bid_volume_per_airline")
	private Boolean bidVolumePerAirline;

	@Column(name = "bid_prepayed_days", precision = 4)
	private Integer bidPrepaidDays;

	@Column(name = "bid_payement_freq", length = 32)
	@Convert(converter = InvoiceFreqConverter.class)
	private InvoiceFreq bidPayementFreq;

	@Column(name = "bid_payment_type", length = 32)
	@Convert(converter = PaymentTypeConverter.class)
	private PaymentType bidPaymentType;

	@Column(name = "bid_prepaid_amount", precision = 21, scale = 6)
	private BigDecimal bidPrepaidAmount;

	@Column(name = "bid_prepay_first_delivery_date", precision = 4)
	private Integer bidPrepayFirstDeliveryDate;

	@Column(name = "bid_bank_guarantee")
	private Boolean bidBankGuarantee;

	@Column(name = "bid_operating_hours", length = 100)
	private String bidOperatingHours;

	@Column(name = "bid_title_transfer", length = 100)
	private String bidTitleTransfer;

	@Column(name = "bid_astm_specification", length = 20)
	private String bidAstmSpecification;

	@Column(name = "bid_has_total_volume")
	private Boolean bidHasTotalVolume;

	@Column(name = "bid_package_identifier", length = 100)
	private String bidPackageIdentifier;

	@Column(name = "period_approval_status", length = 32)
	@Convert(converter = BidApprovalStatusConverter.class)
	private BidApprovalStatus periodApprovalStatus;

	@Column(name = "price_approval_status", length = 32)
	@Convert(converter = BidApprovalStatusConverter.class)
	private BidApprovalStatus priceApprovalStatus;

	@Column(name = "ship_to_approval_status", length = 32)
	@Convert(converter = BidApprovalStatusConverter.class)
	private BidApprovalStatus shipToApprovalStatus;

	@Column(name = "blueprint_original_contract_reference_id", precision = 11)
	private Integer blueprintOriginalContractReference;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "location_id", referencedColumnName = "id")
	private Locations location;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "holder_id", referencedColumnName = "id")
	private Customer holder;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Suppliers.class)
	@JoinColumn(name = "into_plane_agent_id", referencedColumnName = "id")
	private Suppliers intoPlaneAgent;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = UserSupp.class)
	@JoinColumn(name = "responsible_buyer_id", referencedColumnName = "id")
	private UserSupp responsibleBuyer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contacts.class)
	@JoinColumn(name = "contact_id", referencedColumnName = "id")
	private Contacts contact;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "contract_volume_unit_id", referencedColumnName = "id")
	private Unit contractVolumeUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contract.class)
	@JoinColumn(name = "resale_reference_id", referencedColumnName = "id")
	private Contract resaleRef;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "settlement_crncy_id", referencedColumnName = "id")
	private Currencies settlementCurr;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "settlement_unit_id", referencedColumnName = "id")
	private Unit settlementUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AverageMethod.class)
	@JoinColumn(name = "averaging_method_id", referencedColumnName = "id")
	private AverageMethod averageMethod;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FinancialSources.class)
	@JoinColumn(name = "financial_source_id", referencedColumnName = "id")
	private FinancialSources financialSource;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "risk_holder_id", referencedColumnName = "id")
	private Customer riskHolder;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Suppliers.class)
	@JoinColumn(name = "supplier_id", referencedColumnName = "id")
	private Suppliers supplier;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "bill_to_id", referencedColumnName = "id")
	private Customer billTo;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Tender.class)
	@JoinColumn(name = "tender_identification_id", referencedColumnName = "id")
	private Tender bidTenderIdentification;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = TenderLocation.class)
	@JoinColumn(name = "tender_location_id", referencedColumnName = "id")
	private TenderLocation bidTenderLocation;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contract.class)
	@JoinColumn(name = "bid_reference_id", referencedColumnName = "id")
	private Contract bidReference;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ExternalReport.class)
	@JoinColumn(name = "invoice_template_id", referencedColumnName = "id")
	private ExternalReport invoiceTemplate;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = ContractInvoice.class, mappedBy = "contract", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	private Collection<ContractInvoice> contractInvoice;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = ContractPriceCategory.class, mappedBy = "contract")
	private Collection<ContractPriceCategory> priceCategories;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = PricingBase.class, mappedBy = "contract", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<PricingBase> pricingBases;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = ShipTo.class, mappedBy = "contract", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<ShipTo> shipTo;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public DealType getDealType() {
		return this.dealType;
	}

	public void setDealType(DealType dealType) {
		this.dealType = dealType;
	}

	public ContractType getType() {
		return this.type;
	}

	public void setType(ContractType type) {
		this.type = type;
	}

	public FlightServiceType getFlightServiceType() {
		return this.flightServiceType;
	}

	public void setFlightServiceType(FlightServiceType flightServiceType) {
		this.flightServiceType = flightServiceType;
	}

	public ContractSubType getSubType() {
		return this.subType;
	}

	public void setSubType(ContractSubType subType) {
		this.subType = subType;
	}

	public ContractScope getScope() {
		return this.scope;
	}

	public void setScope(ContractScope scope) {
		this.scope = scope;
	}

	public IsSpot getIsSpot() {
		return this.isSpot;
	}

	public void setIsSpot(IsSpot isSpot) {
		this.isSpot = isSpot;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public ContractStatus getStatus() {
		return this.status;
	}

	public void setStatus(ContractStatus status) {
		this.status = status;
	}

	public FlightTypeIndicator getLimitedTo() {
		return this.limitedTo;
	}

	public void setLimitedTo(FlightTypeIndicator limitedTo) {
		this.limitedTo = limitedTo;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public TaxType getTax() {
		return this.tax;
	}

	public void setTax(TaxType tax) {
		this.tax = tax;
	}

	public QuantityType getQuantityType() {
		return this.quantityType;
	}

	public void setQuantityType(QuantityType quantityType) {
		this.quantityType = quantityType;
	}

	public Period getPeriod() {
		return this.period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public Integer getVolumeTolerance() {
		return this.volumeTolerance;
	}

	public void setVolumeTolerance(Integer volumeTolerance) {
		this.volumeTolerance = volumeTolerance;
	}

	public Integer getVolumeShare() {
		return this.volumeShare;
	}

	public void setVolumeShare(Integer volumeShare) {
		this.volumeShare = volumeShare;
	}

	public String getCounterpartyReference() {
		return this.counterpartyReference;
	}

	public void setCounterpartyReference(String counterpartyReference) {
		this.counterpartyReference = counterpartyReference;
	}

	public Integer getSettlementDecimals() {
		return this.settlementDecimals;
	}

	public void setSettlementDecimals(Integer settlementDecimals) {
		this.settlementDecimals = settlementDecimals;
	}

	public MasterAgreementsPeriod getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(MasterAgreementsPeriod exchangeRateOffset) {
		this.exchangeRateOffset = exchangeRateOffset;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public PaymentDay getPaymentRefDay() {
		return this.paymentRefDay;
	}

	public void setPaymentRefDay(PaymentDay paymentRefDay) {
		this.paymentRefDay = paymentRefDay;
	}

	public InvoiceFreq getInvoiceFreq() {
		return this.invoiceFreq;
	}

	public void setInvoiceFreq(InvoiceFreq invoiceFreq) {
		this.invoiceFreq = invoiceFreq;
	}

	public Integer getInvoiceFreqNumber() {
		return this.invoiceFreqNumber;
	}

	public void setInvoiceFreqNumber(Integer invoiceFreqNumber) {
		this.invoiceFreqNumber = invoiceFreqNumber;
	}

	public CreditTerm getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(CreditTerm creditTerms) {
		this.creditTerms = creditTerms;
	}

	public InvoiceType getInvoiceType() {
		return this.invoiceType;
	}

	public void setInvoiceType(InvoiceType invoiceType) {
		this.invoiceType = invoiceType;
	}

	public InvoicePayableBy getInvoicePayableBy() {
		return this.invoicePayableBy;
	}

	public void setInvoicePayableBy(InvoicePayableBy invoicePayableBy) {
		this.invoicePayableBy = invoicePayableBy;
	}

	public VatApplicability getVat() {
		return this.vat;
	}

	public void setVat(VatApplicability vat) {
		this.vat = vat;
	}

	public ReviewPeriod getReviewPeriod() {
		return this.reviewPeriod;
	}

	public void setReviewPeriod(ReviewPeriod reviewPeriod) {
		this.reviewPeriod = reviewPeriod;
	}

	public String getReviewFirstParam() {
		return this.reviewFirstParam;
	}

	public void setReviewFirstParam(String reviewFirstParam) {
		this.reviewFirstParam = reviewFirstParam;
	}

	public String getReviewSecondParam() {
		return this.reviewSecondParam;
	}

	public void setReviewSecondParam(String reviewSecondParam) {
		this.reviewSecondParam = reviewSecondParam;
	}

	public Integer getReviewNotification() {
		return this.reviewNotification;
	}

	public void setReviewNotification(Integer reviewNotification) {
		this.reviewNotification = reviewNotification;
	}

	public String getPaymentComment() {
		return this.paymentComment;
	}

	public void setPaymentComment(String paymentComment) {
		this.paymentComment = paymentComment;
	}

	public EventType getEventType() {
		return this.eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public BigDecimal getAwardedVolume() {
		return this.awardedVolume;
	}

	public void setAwardedVolume(BigDecimal awardedVolume) {
		this.awardedVolume = awardedVolume;
	}

	public BigDecimal getOfferedVolume() {
		return this.offeredVolume;
	}

	public void setOfferedVolume(BigDecimal offeredVolume) {
		this.offeredVolume = offeredVolume;
	}

	public Integer getIataServiceLevel() {
		return this.iataServiceLevel;
	}

	public void setIataServiceLevel(Integer iataServiceLevel) {
		this.iataServiceLevel = iataServiceLevel;
	}

	public String getForex() {
		return this.getFinancialSource().getCode() + "/"
				+ this.getAverageMethod().getName();
	}

	public void setForex(String forex) {
		this.forex = forex;
	}

	public Boolean getHardCopy() {
		return this.hardCopy;
	}

	public void setHardCopy(Boolean hardCopy) {
		this.hardCopy = hardCopy;
	}

	public Boolean getIsContract() {
		return this.isContract;
	}

	public void setIsContract(Boolean isContract) {
		this.isContract = isContract;
	}

	public Boolean getReadOnly() {
		return this.readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	public Boolean getIsBlueprint() {
		return this.isBlueprint;
	}

	public void setIsBlueprint(Boolean isBlueprint) {
		this.isBlueprint = isBlueprint;
	}

	public BidStatus getBidStatus() {
		return this.bidStatus;
	}

	public void setBidStatus(BidStatus bidStatus) {
		this.bidStatus = bidStatus;
	}

	public TransmissionStatus getBidTransmissionStatus() {
		return this.bidTransmissionStatus;
	}

	public void setBidTransmissionStatus(
			TransmissionStatus bidTransmissionStatus) {
		this.bidTransmissionStatus = bidTransmissionStatus;
	}

	public TransmissionStatus getBidCancelTransmitionStatus() {
		return this.bidCancelTransmitionStatus;
	}

	public void setBidCancelTransmitionStatus(
			TransmissionStatus bidCancelTransmitionStatus) {
		this.bidCancelTransmitionStatus = bidCancelTransmitionStatus;
	}

	public TransmissionStatus getBidAcceptAwardTransmissionStatus() {
		return this.bidAcceptAwardTransmissionStatus;
	}

	public void setBidAcceptAwardTransmissionStatus(
			TransmissionStatus bidAcceptAwardTransmissionStatus) {
		this.bidAcceptAwardTransmissionStatus = bidAcceptAwardTransmissionStatus;
	}

	public TransmissionStatus getBidDeclineAwardTransmissionStatus() {
		return this.bidDeclineAwardTransmissionStatus;
	}

	public void setBidDeclineAwardTransmissionStatus(
			TransmissionStatus bidDeclineAwardTransmissionStatus) {
		this.bidDeclineAwardTransmissionStatus = bidDeclineAwardTransmissionStatus;
	}

	public BidApprovalStatus getBidApprovalStatus() {
		return this.bidApprovalStatus;
	}

	public void setBidApprovalStatus(BidApprovalStatus bidApprovalStatus) {
		this.bidApprovalStatus = bidApprovalStatus;
	}

	public Integer getBidRevision() {
		return this.bidRevision;
	}

	public void setBidRevision(Integer bidRevision) {
		this.bidRevision = bidRevision;
	}

	public String getBidVersion() {
		return this.bidVersion;
	}

	public void setBidVersion(String bidVersion) {
		this.bidVersion = bidVersion;
	}

	public Date getBidValidFrom() {
		return this.bidValidFrom;
	}

	public void setBidValidFrom(Date bidValidFrom) {
		this.bidValidFrom = bidValidFrom;
	}

	public Date getBidValidTo() {
		return this.bidValidTo;
	}

	public void setBidValidTo(Date bidValidTo) {
		this.bidValidTo = bidValidTo;
	}

	public Boolean getBidVolumePerAirline() {
		return this.bidVolumePerAirline;
	}

	public void setBidVolumePerAirline(Boolean bidVolumePerAirline) {
		this.bidVolumePerAirline = bidVolumePerAirline;
	}

	public Integer getBidPrepaidDays() {
		return this.bidPrepaidDays;
	}

	public void setBidPrepaidDays(Integer bidPrepaidDays) {
		this.bidPrepaidDays = bidPrepaidDays;
	}

	public InvoiceFreq getBidPayementFreq() {
		return this.bidPayementFreq;
	}

	public void setBidPayementFreq(InvoiceFreq bidPayementFreq) {
		this.bidPayementFreq = bidPayementFreq;
	}

	public PaymentType getBidPaymentType() {
		return this.bidPaymentType;
	}

	public void setBidPaymentType(PaymentType bidPaymentType) {
		this.bidPaymentType = bidPaymentType;
	}

	public BigDecimal getBidPrepaidAmount() {
		return this.bidPrepaidAmount;
	}

	public void setBidPrepaidAmount(BigDecimal bidPrepaidAmount) {
		this.bidPrepaidAmount = bidPrepaidAmount;
	}

	public Integer getBidPrepayFirstDeliveryDate() {
		return this.bidPrepayFirstDeliveryDate;
	}

	public void setBidPrepayFirstDeliveryDate(Integer bidPrepayFirstDeliveryDate) {
		this.bidPrepayFirstDeliveryDate = bidPrepayFirstDeliveryDate;
	}

	public Boolean getBidBankGuarantee() {
		return this.bidBankGuarantee;
	}

	public void setBidBankGuarantee(Boolean bidBankGuarantee) {
		this.bidBankGuarantee = bidBankGuarantee;
	}

	public String getBidOperatingHours() {
		return this.bidOperatingHours;
	}

	public void setBidOperatingHours(String bidOperatingHours) {
		this.bidOperatingHours = bidOperatingHours;
	}

	public String getBidTitleTransfer() {
		return this.bidTitleTransfer;
	}

	public void setBidTitleTransfer(String bidTitleTransfer) {
		this.bidTitleTransfer = bidTitleTransfer;
	}

	public String getBidAstmSpecification() {
		return this.bidAstmSpecification;
	}

	public void setBidAstmSpecification(String bidAstmSpecification) {
		this.bidAstmSpecification = bidAstmSpecification;
	}

	public Boolean getBidHasTotalVolume() {
		return this.bidHasTotalVolume;
	}

	public void setBidHasTotalVolume(Boolean bidHasTotalVolume) {
		this.bidHasTotalVolume = bidHasTotalVolume;
	}

	public String getBidPackageIdentifier() {
		return this.bidPackageIdentifier;
	}

	public void setBidPackageIdentifier(String bidPackageIdentifier) {
		this.bidPackageIdentifier = bidPackageIdentifier;
	}

	public BidApprovalStatus getPeriodApprovalStatus() {
		return this.periodApprovalStatus;
	}

	public void setPeriodApprovalStatus(BidApprovalStatus periodApprovalStatus) {
		this.periodApprovalStatus = periodApprovalStatus;
	}

	public BidApprovalStatus getPriceApprovalStatus() {
		return this.priceApprovalStatus;
	}

	public void setPriceApprovalStatus(BidApprovalStatus priceApprovalStatus) {
		this.priceApprovalStatus = priceApprovalStatus;
	}

	public BidApprovalStatus getShipToApprovalStatus() {
		return this.shipToApprovalStatus;
	}

	public void setShipToApprovalStatus(BidApprovalStatus shipToApprovalStatus) {
		this.shipToApprovalStatus = shipToApprovalStatus;
	}

	public Integer getBlueprintOriginalContractReference() {
		return this.blueprintOriginalContractReference;
	}

	public void setBlueprintOriginalContractReference(
			Integer blueprintOriginalContractReference) {
		this.blueprintOriginalContractReference = blueprintOriginalContractReference;
	}

	public Locations getLocation() {
		return this.location;
	}

	public void setLocation(Locations location) {
		if (location != null) {
			this.__validate_client_context__(location.getClientId());
		}
		this.location = location;
	}
	public Customer getHolder() {
		return this.holder;
	}

	public void setHolder(Customer holder) {
		if (holder != null) {
			this.__validate_client_context__(holder.getClientId());
		}
		this.holder = holder;
	}
	public Suppliers getIntoPlaneAgent() {
		return this.intoPlaneAgent;
	}

	public void setIntoPlaneAgent(Suppliers intoPlaneAgent) {
		if (intoPlaneAgent != null) {
			this.__validate_client_context__(intoPlaneAgent.getClientId());
		}
		this.intoPlaneAgent = intoPlaneAgent;
	}
	public UserSupp getResponsibleBuyer() {
		return this.responsibleBuyer;
	}

	public void setResponsibleBuyer(UserSupp responsibleBuyer) {
		if (responsibleBuyer != null) {
			this.__validate_client_context__(responsibleBuyer.getClientId());
		}
		this.responsibleBuyer = responsibleBuyer;
	}
	public Contacts getContact() {
		return this.contact;
	}

	public void setContact(Contacts contact) {
		if (contact != null) {
			this.__validate_client_context__(contact.getClientId());
		}
		this.contact = contact;
	}
	public Unit getContractVolumeUnit() {
		return this.contractVolumeUnit;
	}

	public void setContractVolumeUnit(Unit contractVolumeUnit) {
		if (contractVolumeUnit != null) {
			this.__validate_client_context__(contractVolumeUnit.getClientId());
		}
		this.contractVolumeUnit = contractVolumeUnit;
	}
	public Contract getResaleRef() {
		return this.resaleRef;
	}

	public void setResaleRef(Contract resaleRef) {
		if (resaleRef != null) {
			this.__validate_client_context__(resaleRef.getClientId());
		}
		this.resaleRef = resaleRef;
	}
	public Currencies getSettlementCurr() {
		return this.settlementCurr;
	}

	public void setSettlementCurr(Currencies settlementCurr) {
		if (settlementCurr != null) {
			this.__validate_client_context__(settlementCurr.getClientId());
		}
		this.settlementCurr = settlementCurr;
	}
	public Unit getSettlementUnit() {
		return this.settlementUnit;
	}

	public void setSettlementUnit(Unit settlementUnit) {
		if (settlementUnit != null) {
			this.__validate_client_context__(settlementUnit.getClientId());
		}
		this.settlementUnit = settlementUnit;
	}
	public AverageMethod getAverageMethod() {
		return this.averageMethod;
	}

	public void setAverageMethod(AverageMethod averageMethod) {
		if (averageMethod != null) {
			this.__validate_client_context__(averageMethod.getClientId());
		}
		this.averageMethod = averageMethod;
	}
	public FinancialSources getFinancialSource() {
		return this.financialSource;
	}

	public void setFinancialSource(FinancialSources financialSource) {
		if (financialSource != null) {
			this.__validate_client_context__(financialSource.getClientId());
		}
		this.financialSource = financialSource;
	}
	public Customer getRiskHolder() {
		return this.riskHolder;
	}

	public void setRiskHolder(Customer riskHolder) {
		if (riskHolder != null) {
			this.__validate_client_context__(riskHolder.getClientId());
		}
		this.riskHolder = riskHolder;
	}
	public Suppliers getSupplier() {
		return this.supplier;
	}

	public void setSupplier(Suppliers supplier) {
		if (supplier != null) {
			this.__validate_client_context__(supplier.getClientId());
		}
		this.supplier = supplier;
	}
	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		if (customer != null) {
			this.__validate_client_context__(customer.getClientId());
		}
		this.customer = customer;
	}
	public Customer getBillTo() {
		return this.billTo;
	}

	public void setBillTo(Customer billTo) {
		if (billTo != null) {
			this.__validate_client_context__(billTo.getClientId());
		}
		this.billTo = billTo;
	}
	public Tender getBidTenderIdentification() {
		return this.bidTenderIdentification;
	}

	public void setBidTenderIdentification(Tender bidTenderIdentification) {
		if (bidTenderIdentification != null) {
			this.__validate_client_context__(bidTenderIdentification
					.getClientId());
		}
		this.bidTenderIdentification = bidTenderIdentification;
	}
	public TenderLocation getBidTenderLocation() {
		return this.bidTenderLocation;
	}

	public void setBidTenderLocation(TenderLocation bidTenderLocation) {
		if (bidTenderLocation != null) {
			this.__validate_client_context__(bidTenderLocation.getClientId());
		}
		this.bidTenderLocation = bidTenderLocation;
	}
	public Contract getBidReference() {
		return this.bidReference;
	}

	public void setBidReference(Contract bidReference) {
		if (bidReference != null) {
			this.__validate_client_context__(bidReference.getClientId());
		}
		this.bidReference = bidReference;
	}
	public ExternalReport getInvoiceTemplate() {
		return this.invoiceTemplate;
	}

	public void setInvoiceTemplate(ExternalReport invoiceTemplate) {
		if (invoiceTemplate != null) {
			this.__validate_client_context__(invoiceTemplate.getClientId());
		}
		this.invoiceTemplate = invoiceTemplate;
	}

	public Collection<ContractInvoice> getContractInvoice() {
		return this.contractInvoice;
	}

	public void setContractInvoice(Collection<ContractInvoice> contractInvoice) {
		this.contractInvoice = contractInvoice;
	}

	/**
	 * @param e
	 */
	public void addToContractInvoice(ContractInvoice e) {
		if (this.contractInvoice == null) {
			this.contractInvoice = new ArrayList<>();
		}
		e.setContract(this);
		this.contractInvoice.add(e);
	}
	public Collection<ContractPriceCategory> getPriceCategories() {
		return this.priceCategories;
	}

	public void setPriceCategories(
			Collection<ContractPriceCategory> priceCategories) {
		this.priceCategories = priceCategories;
	}

	/**
	 * @param e
	 */
	public void addToPriceCategories(ContractPriceCategory e) {
		if (this.priceCategories == null) {
			this.priceCategories = new ArrayList<>();
		}
		e.setContract(this);
		this.priceCategories.add(e);
	}
	public Collection<PricingBase> getPricingBases() {
		return this.pricingBases;
	}

	public void setPricingBases(Collection<PricingBase> pricingBases) {
		this.pricingBases = pricingBases;
	}

	/**
	 * @param e
	 */
	public void addToPricingBases(PricingBase e) {
		if (this.pricingBases == null) {
			this.pricingBases = new ArrayList<>();
		}
		e.setContract(this);
		this.pricingBases.add(e);
	}
	public Collection<ShipTo> getShipTo() {
		return this.shipTo;
	}

	public void setShipTo(Collection<ShipTo> shipTo) {
		this.shipTo = shipTo;
	}

	/**
	 * @param e
	 */
	public void addToShipTo(ShipTo e) {
		if (this.shipTo == null) {
			this.shipTo = new ArrayList<>();
		}
		e.setContract(this);
		this.shipTo.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
