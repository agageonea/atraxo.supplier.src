package atraxo.cmm.domain.ext.mailmerge.dto;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;

public class TenderBidDto extends AbstractDto {

	private BidDto bids;

	public BidDto getBids() {
		return this.bids;
	}

	public void setBids(BidDto bids) {
		this.bids = bids;
	}
}
