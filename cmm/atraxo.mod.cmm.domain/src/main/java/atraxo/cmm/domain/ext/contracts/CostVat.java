package atraxo.cmm.domain.ext.contracts;

import java.math.BigDecimal;

public class CostVat {

	private BigDecimal vat;
	private BigDecimal cost;

	public CostVat(BigDecimal cost, BigDecimal vat) {
		super();
		this.vat = vat;
		this.cost = cost;
	}

	public BigDecimal getVat() {
		return this.vat;
	}

	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}

	public BigDecimal getCost() {
		return this.cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

}
