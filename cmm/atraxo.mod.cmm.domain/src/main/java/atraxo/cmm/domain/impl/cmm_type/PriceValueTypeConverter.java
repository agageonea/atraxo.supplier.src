/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link PriceValueType} enum.
 * Generated code. Do not modify in this file.
 */
public class PriceValueTypeConverter
		implements
			AttributeConverter<PriceValueType, String> {

	@Override
	public String convertToDatabaseColumn(PriceValueType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null PriceValueType.");
		}
		return value.getName();
	}

	@Override
	public PriceValueType convertToEntityAttribute(String value) {
		return PriceValueType.getByName(value);
	}

}
