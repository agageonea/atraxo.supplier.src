/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.shipTo;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.customer.Customer;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link ShipTo} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = ShipTo.NQ_FIND_BY_CONTRACT_CUSTOMER, query = "SELECT e FROM ShipTo e WHERE e.clientId = :clientId and e.contract = :contract and e.customer = :customer", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ShipTo.NQ_FIND_BY_CONTRACT_CUSTOMER_PRIMITIVE, query = "SELECT e FROM ShipTo e WHERE e.clientId = :clientId and e.contract.id = :contractId and e.customer.id = :customerId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ShipTo.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ShipTo e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ShipTo.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = ShipTo.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "contract_id", "customer_id"})})
public class ShipTo extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "CMM_SHIPTO";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Contract_customer.
	 */
	public static final String NQ_FIND_BY_CONTRACT_CUSTOMER = "ShipTo.findByContract_customer";
	/**
	 * Named query find by unique key: Contract_customer using the ID field for references.
	 */
	public static final String NQ_FIND_BY_CONTRACT_CUSTOMER_PRIMITIVE = "ShipTo.findByContract_customer_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ShipTo.findByBusiness";

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "valid_from", nullable = false)
	private Date validFrom;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "valid_to", nullable = false)
	private Date validTo;

	@Column(name = "allocated_volume", precision = 21, scale = 6)
	private BigDecimal allocatedVolume;

	@Column(name = "adjusted_volume", precision = 21, scale = 6)
	private BigDecimal adjustedVolume;

	@Column(name = "actual_volume", precision = 21, scale = 6)
	private BigDecimal actualVolume;

	@Column(name = "offered_volume", precision = 21, scale = 6)
	private BigDecimal offeredVolume;

	@Column(name = "tender_volume", precision = 21, scale = 6)
	private BigDecimal tenderBidVolume;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contract.class)
	@JoinColumn(name = "contract_id", referencedColumnName = "id")
	private Contract contract;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private Customer customer;

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public BigDecimal getAllocatedVolume() {
		return this.allocatedVolume;
	}

	public void setAllocatedVolume(BigDecimal allocatedVolume) {
		this.allocatedVolume = allocatedVolume;
	}

	public BigDecimal getAdjustedVolume() {
		return this.adjustedVolume;
	}

	public void setAdjustedVolume(BigDecimal adjustedVolume) {
		this.adjustedVolume = adjustedVolume;
	}

	public BigDecimal getActualVolume() {
		return this.actualVolume;
	}

	public void setActualVolume(BigDecimal actualVolume) {
		this.actualVolume = actualVolume;
	}

	public BigDecimal getOfferedVolume() {
		return this.offeredVolume;
	}

	public void setOfferedVolume(BigDecimal offeredVolume) {
		this.offeredVolume = offeredVolume;
	}

	public BigDecimal getTenderBidVolume() {
		return this.tenderBidVolume;
	}

	public void setTenderBidVolume(BigDecimal tenderBidVolume) {
		this.tenderBidVolume = tenderBidVolume;
	}

	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		if (contract != null) {
			this.__validate_client_context__(contract.getClientId());
		}
		this.contract = contract;
	}
	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		if (customer != null) {
			this.__validate_client_context__(customer.getClientId());
		}
		this.customer = customer;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.contract == null) ? 0 : this.contract.hashCode());
		result = prime * result
				+ ((this.customer == null) ? 0 : this.customer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		ShipTo other = (ShipTo) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.contract == null) {
			if (other.contract != null) {
				return false;
			}
		} else if (!this.contract.equals(other.contract)) {
			return false;
		}
		if (this.customer == null) {
			if (other.customer != null) {
				return false;
			}
		} else if (!this.customer.equals(other.customer)) {
			return false;
		}
		return true;
	}
}
