/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.prices;

import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link ContractPriceComponentConv} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = ContractPriceComponentConv.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ContractPriceComponentConv e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ContractPriceComponentConv.TABLE_NAME)
public class ContractPriceComponentConv extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "CMM_CONTR_PRICE_CMPNT_CONV";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ContractPriceComponentConv.findByBusiness";

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "valid_from", nullable = false)
	private Date validFrom;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "valid_to", nullable = false)
	private Date validTo;

	@NotNull
	@Column(name = "price", nullable = false, precision = 19, scale = 6)
	private BigDecimal price;

	@NotBlank
	@Column(name = "crncy_unit_cd", nullable = false, length = 10)
	private String currencyUnitCd;

	@NotNull
	@Column(name = "equivalent", nullable = false, precision = 19, scale = 6)
	private BigDecimal equivalent;

	@Column(name = "equiv_crncy_unit_cd", length = 10)
	private String equivCrncyUnitCd;

	@Column(name = "exch_rate", precision = 19, scale = 6)
	private BigDecimal exchangeRate;

	@NotBlank
	@Column(name = "exch_rate_crncy_cds", nullable = false, length = 10)
	private String exchangeRateCurrnecyCDS;

	@Temporal(TemporalType.DATE)
	@Column(name = "of_date")
	private Date ofDate;

	@NotNull
	@Column(name = "prvsn", nullable = false)
	private Boolean provisional;

	@NotNull
	@Column(name = "whld_pay", nullable = false)
	private Boolean whldPay;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ContractPriceComponent.class)
	@JoinColumn(name = "contr_price_cmpnt_id", referencedColumnName = "id")
	private ContractPriceComponent contractPriceComponent;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "equiv_unit_id", referencedColumnName = "id")
	private Unit equivalentUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "equiv_crncy_id", referencedColumnName = "id")
	private Currencies equivalentCurrency;

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getCurrencyUnitCd() {
		return this.currencyUnitCd;
	}

	public void setCurrencyUnitCd(String currencyUnitCd) {
		this.currencyUnitCd = currencyUnitCd;
	}

	public BigDecimal getEquivalent() {
		return this.equivalent;
	}

	public void setEquivalent(BigDecimal equivalent) {
		this.equivalent = equivalent;
	}

	public String getEquivCrncyUnitCd() {
		return this.equivCrncyUnitCd;
	}

	public void setEquivCrncyUnitCd(String equivCrncyUnitCd) {
		this.equivCrncyUnitCd = equivCrncyUnitCd;
	}

	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getExchangeRateCurrnecyCDS() {
		return this.exchangeRateCurrnecyCDS;
	}

	public void setExchangeRateCurrnecyCDS(String exchangeRateCurrnecyCDS) {
		this.exchangeRateCurrnecyCDS = exchangeRateCurrnecyCDS;
	}

	public Date getOfDate() {
		return this.ofDate;
	}

	public void setOfDate(Date ofDate) {
		this.ofDate = ofDate;
	}

	public Boolean getProvisional() {
		return this.provisional;
	}

	public void setProvisional(Boolean provisional) {
		this.provisional = provisional;
	}

	public Boolean getWhldPay() {
		return this.whldPay;
	}

	public void setWhldPay(Boolean whldPay) {
		this.whldPay = whldPay;
	}

	public ContractPriceComponent getContractPriceComponent() {
		return this.contractPriceComponent;
	}

	public void setContractPriceComponent(
			ContractPriceComponent contractPriceComponent) {
		if (contractPriceComponent != null) {
			this.__validate_client_context__(contractPriceComponent
					.getClientId());
		}
		this.contractPriceComponent = contractPriceComponent;
	}
	public Unit getEquivalentUnit() {
		return this.equivalentUnit;
	}

	public void setEquivalentUnit(Unit equivalentUnit) {
		if (equivalentUnit != null) {
			this.__validate_client_context__(equivalentUnit.getClientId());
		}
		this.equivalentUnit = equivalentUnit;
	}
	public Currencies getEquivalentCurrency() {
		return this.equivalentCurrency;
	}

	public void setEquivalentCurrency(Currencies equivalentCurrency) {
		if (equivalentCurrency != null) {
			this.__validate_client_context__(equivalentCurrency.getClientId());
		}
		this.equivalentCurrency = equivalentCurrency;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
