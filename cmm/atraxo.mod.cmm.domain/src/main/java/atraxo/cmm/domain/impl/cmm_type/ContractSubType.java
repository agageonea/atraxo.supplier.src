/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ContractSubType {

	_EMPTY_("", ""), _NA_("Unspecified", "NA"), _INTO_PLANE_("Into plane",
			"PSI"), _EX_HYDRANT_("Ex-Hydrant", "PRI"), _AIRLINE_SUPPLY_(
			"Airline Supply", ""), _INTO_STORAGE_("Into Storage", "PRO"), _SERVICE_(
			"Service", "FSI");

	private String name;
	private String code;

	private ContractSubType(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ContractSubType getByName(String name) {
		for (ContractSubType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent ContractSubType with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static ContractSubType getByCode(String code) {
		for (ContractSubType status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent ContractSubType with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
