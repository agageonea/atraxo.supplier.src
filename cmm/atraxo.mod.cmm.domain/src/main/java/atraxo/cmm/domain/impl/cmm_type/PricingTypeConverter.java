/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link PricingType} enum.
 * Generated code. Do not modify in this file.
 */
public class PricingTypeConverter
		implements
			AttributeConverter<PricingType, String> {

	@Override
	public String convertToDatabaseColumn(PricingType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null PricingType.");
		}
		return value.getName();
	}

	@Override
	public PricingType convertToEntityAttribute(String value) {
		return PricingType.getByName(value);
	}

}
