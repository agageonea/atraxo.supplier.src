package atraxo.cmm.domain.ext.mailmerge.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;

public class TenderLocationFormDTO extends AbstractDto {

	private static final long serialVersionUID = -4349681746114518052L;

	@JsonProperty("location_iata")
	private String locationIata;

	@JsonProperty("total_volume")
	private BigDecimal totalVolume;

	@JsonProperty("volume_period")
	private String volumePeriod;

	@JsonProperty("agreement_from")
	private Date agreementFrom;

	@JsonProperty("location_icao")
	private String locationIcao;

	@JsonProperty("volume_unit")
	private String volumeUnit;

	@JsonProperty("agreement_to")
	private Date agreementTo;

	@JsonProperty("fuel_product")
	private String fuelProduct;

	@JsonProperty("tax_type")
	private String taxType;

	@JsonProperty("flight_service_type")
	private String flightServiceType;

	@JsonProperty("delivery_point")
	private String deliveryPoint;

	@JsonProperty("package_identifier")
	private String packageIdentifier;

	@JsonProperty("airlines")
	private List<AirlinesFormDTO> airlines;

	public String getLocationIata() {
		return this.locationIata;
	}

	public void setLocationIata(String locationIata) {
		this.locationIata = locationIata;
	}

	public BigDecimal getTotalVolume() {
		return this.totalVolume;
	}

	public void setTotalVolume(BigDecimal totalVolume) {
		this.totalVolume = totalVolume;
	}

	public String getVolumePeriod() {
		return this.volumePeriod;
	}

	public void setVolumePeriod(String volumePeriod) {
		this.volumePeriod = volumePeriod;
	}

	public Date getAgreementFrom() {
		return this.agreementFrom;
	}

	public void setAgreementFrom(Date agreementFrom) {
		this.agreementFrom = agreementFrom;
	}

	public String getLocationIcao() {
		return this.locationIcao;
	}

	public void setLocationIcao(String locationIcao) {
		this.locationIcao = locationIcao;
	}

	public String getVolumeUnit() {
		return this.volumeUnit;
	}

	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}

	public Date getAgreementTo() {
		return this.agreementTo;
	}

	public void setAgreementTo(Date agreementTo) {
		this.agreementTo = agreementTo;
	}

	public String getFuelProduct() {
		return this.fuelProduct;
	}

	public void setFuelProduct(String fuelProduct) {
		this.fuelProduct = fuelProduct;
	}

	public String getTaxType() {
		return this.taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public String getFlightServiceType() {
		return this.flightServiceType;
	}

	public void setFlightServiceType(String flightServiceType) {
		this.flightServiceType = flightServiceType;
	}

	public String getDeliveryPoint() {
		return this.deliveryPoint;
	}

	public void setDeliveryPoint(String deliveryPoint) {
		this.deliveryPoint = deliveryPoint;
	}

	public String getPackageIdentifier() {
		return this.packageIdentifier;
	}

	public void setPackageIdentifier(String packageIdentifier) {
		this.packageIdentifier = packageIdentifier;
	}

	public List<AirlinesFormDTO> getAirlines() {
		return this.airlines;
	}

	public void setAirlines(List<AirlinesFormDTO> airlines) {
		this.airlines = airlines;
	}

}
