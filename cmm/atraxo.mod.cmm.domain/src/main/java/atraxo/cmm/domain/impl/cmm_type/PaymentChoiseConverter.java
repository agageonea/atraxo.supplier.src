/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link PaymentChoise} enum.
 * Generated code. Do not modify in this file.
 */
public class PaymentChoiseConverter
		implements
			AttributeConverter<PaymentChoise, String> {

	@Override
	public String convertToDatabaseColumn(PaymentChoise value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null PaymentChoise.");
		}
		return value.getName();
	}

	@Override
	public PaymentChoise convertToEntityAttribute(String value) {
		return PaymentChoise.getByName(value);
	}

}
