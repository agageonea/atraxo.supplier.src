/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum PricingMethod {

	_EMPTY_(""), _INDEX_("Index"), _FIXED_("Fixed");

	private String name;

	private PricingMethod(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static PricingMethod getByName(String name) {
		for (PricingMethod status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent PricingMethod with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
