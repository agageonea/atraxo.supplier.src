/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.priceUpdate;

import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.DealTypeConverter;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdate;
import atraxo.fmbas.domain.impl.abstracts.AbstractSubsidiary;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link PriceUpdateCategories} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = PriceUpdateCategories.NQ_FIND_BY_BKEY, query = "SELECT e FROM PriceUpdateCategories e WHERE e.clientId = :clientId and e.priceUpdate = :priceUpdate and e.contract = :contract and e.contractPriceCatId = :contractPriceCatId and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = PriceUpdateCategories.NQ_FIND_BY_BKEY_PRIMITIVE, query = "SELECT e FROM PriceUpdateCategories e WHERE e.clientId = :clientId and e.priceUpdate.id = :priceUpdateId and e.contract = :contract and e.contractPriceCatId = :contractPriceCatId and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = PriceUpdateCategories.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM PriceUpdateCategories e WHERE e.clientId = :clientId and e.id = :id and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = PriceUpdateCategories.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = PriceUpdateCategories.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "price_update_id", "contract",
		"contract_price_cat_id"})})
public class PriceUpdateCategories extends AbstractSubsidiary
		implements
			Serializable {

	public static final String TABLE_NAME = "CMM_PRICE_UPDATE_CATEGORIES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: BKey.
	 */
	public static final String NQ_FIND_BY_BKEY = "PriceUpdateCategories.findByBKey";
	/**
	 * Named query find by unique key: BKey using the ID field for references.
	 */
	public static final String NQ_FIND_BY_BKEY_PRIMITIVE = "PriceUpdateCategories.findByBKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "PriceUpdateCategories.findByBusiness";

	@Column(name = "contract", length = 32)
	private String contract;

	@Column(name = "price_cat_name", length = 64)
	private String priceCatName;

	@Column(name = "contract_price_cat_id", precision = 8)
	private Integer contractPriceCatId;

	@Column(name = "deal_type", length = 32)
	@Convert(converter = DealTypeConverter.class)
	private DealType dealType;

	@Column(name = "price", precision = 19, scale = 6)
	private BigDecimal price;

	@Column(name = "selected")
	private Boolean selected;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = PriceUpdate.class)
	@JoinColumn(name = "price_update_id", referencedColumnName = "id")
	private PriceUpdate priceUpdate;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "location_id", referencedColumnName = "id")
	private Locations location;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "contract_holder_id", referencedColumnName = "id")
	private Customer contractHolder;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "counterparty_id", referencedColumnName = "id")
	private Customer counterparty;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "currency_id", referencedColumnName = "id")
	private Currencies currency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_id", referencedColumnName = "id")
	private Unit unit;

	public String getContract() {
		return this.contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	public String getPriceCatName() {
		return this.priceCatName;
	}

	public void setPriceCatName(String priceCatName) {
		this.priceCatName = priceCatName;
	}

	public Integer getContractPriceCatId() {
		return this.contractPriceCatId;
	}

	public void setContractPriceCatId(Integer contractPriceCatId) {
		this.contractPriceCatId = contractPriceCatId;
	}

	public DealType getDealType() {
		return this.dealType;
	}

	public void setDealType(DealType dealType) {
		this.dealType = dealType;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Boolean getSelected() {
		return this.selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public PriceUpdate getPriceUpdate() {
		return this.priceUpdate;
	}

	public void setPriceUpdate(PriceUpdate priceUpdate) {
		if (priceUpdate != null) {
			this.__validate_client_context__(priceUpdate.getClientId());
		}
		this.priceUpdate = priceUpdate;
	}
	public Locations getLocation() {
		return this.location;
	}

	public void setLocation(Locations location) {
		if (location != null) {
			this.__validate_client_context__(location.getClientId());
		}
		this.location = location;
	}
	public Customer getContractHolder() {
		return this.contractHolder;
	}

	public void setContractHolder(Customer contractHolder) {
		if (contractHolder != null) {
			this.__validate_client_context__(contractHolder.getClientId());
		}
		this.contractHolder = contractHolder;
	}
	public Customer getCounterparty() {
		return this.counterparty;
	}

	public void setCounterparty(Customer counterparty) {
		if (counterparty != null) {
			this.__validate_client_context__(counterparty.getClientId());
		}
		this.counterparty = counterparty;
	}
	public Currencies getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currencies currency) {
		if (currency != null) {
			this.__validate_client_context__(currency.getClientId());
		}
		this.currency = currency;
	}
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		if (unit != null) {
			this.__validate_client_context__(unit.getClientId());
		}
		this.unit = unit;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime
				* result
				+ ((this.priceUpdate == null) ? 0 : this.priceUpdate.hashCode());
		result = prime * result
				+ ((this.contract == null) ? 0 : this.contract.hashCode());
		result = prime
				* result
				+ ((this.contractPriceCatId == null)
						? 0
						: this.contractPriceCatId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		PriceUpdateCategories other = (PriceUpdateCategories) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.priceUpdate == null) {
			if (other.priceUpdate != null) {
				return false;
			}
		} else if (!this.priceUpdate.equals(other.priceUpdate)) {
			return false;
		}
		if (this.contract == null) {
			if (other.contract != null) {
				return false;
			}
		} else if (!this.contract.equals(other.contract)) {
			return false;
		}
		if (this.contractPriceCatId == null) {
			if (other.contractPriceCatId != null) {
				return false;
			}
		} else if (!this.contractPriceCatId.equals(other.contractPriceCatId)) {
			return false;
		}
		return true;
	}
}
