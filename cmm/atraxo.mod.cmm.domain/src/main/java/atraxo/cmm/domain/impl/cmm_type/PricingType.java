/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum PricingType {

	_EMPTY_("", ""), _INDEX_("Index", "Index"), _MARKET_("Market", "Fixed");

	private String name;
	private String code;

	private PricingType(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static PricingType getByName(String name) {
		for (PricingType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent PricingType with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static PricingType getByCode(String code) {
		for (PricingType status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent PricingType with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
