package atraxo.cmm.domain.ext;

import java.util.Map;

public class ContractPriceComponentCheckResult {

	public static final ThreadLocal<Map<Integer, String>> resultMap = new ThreadLocal<>();

	private ContractPriceComponentCheckResult() {
		// empty private constructor
	}

}
