/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum BidStatus {

	_EMPTY_(""), _DRAFT_("Draft"), _SUBMITTED_("Submitted"), _AWARDED_(
			"Awarded"), _DECLINED_("Declined"), _CANCELLED_("Cancelled"), _AWARD_ACCEPTED_(
			"Award accepted"), _AWARD_DECLINED_("Award declined");

	private String name;

	private BidStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static BidStatus getByName(String name) {
		for (BidStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent BidStatus with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
