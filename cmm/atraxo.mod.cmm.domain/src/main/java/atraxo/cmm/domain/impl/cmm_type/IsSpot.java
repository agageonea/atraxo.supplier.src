/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum IsSpot {

	_EMPTY_(""), _TERM_("Term"), _SPOT_("Spot");

	private String name;

	private IsSpot(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static IsSpot getByName(String name) {
		for (IsSpot status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent IsSpot with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
