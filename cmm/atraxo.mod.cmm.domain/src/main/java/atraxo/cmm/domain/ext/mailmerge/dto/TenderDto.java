package atraxo.cmm.domain.ext.mailmerge.dto;

import atraxo.fmbas.domain.ext.mailmerge.dto.CustomerDto;

public class TenderDto {

	private CustomerDto holder;
	private String code;
	private String name;
	private Long version;
	private String contact;

	public CustomerDto getHolder() {
		return this.holder;
	}

	public void setHolder(CustomerDto holder) {
		this.holder = holder;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getContact() {
		return this.contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}
}
