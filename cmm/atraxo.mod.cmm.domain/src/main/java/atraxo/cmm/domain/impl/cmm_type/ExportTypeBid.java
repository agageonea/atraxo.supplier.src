/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ExportTypeBid {

	_EMPTY_(""), _PDF_("PDF"), _XML_("XML"), _XLSX_("XLSX");

	private String name;

	private ExportTypeBid(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ExportTypeBid getByName(String name) {
		for (ExportTypeBid status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent ExportTypeBid with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
