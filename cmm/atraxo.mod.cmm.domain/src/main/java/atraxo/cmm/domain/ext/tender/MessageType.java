package atraxo.cmm.domain.ext.tender;

import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * @author abolindu
 */
public enum MessageType {
	FUEL_TENDER_INVITATION("FuelTenderInvitation"),
	FUEL_TENDER_NO_BID("FuelTenderNoBid"),
	FUEL_TENDER_UPDATE("FuelTenderUpdate"),
	FUEL_TENDER_BID_AWARD("FuelTenderBidAward"),
	FUEL_TENDER_BID_DECLINE("FuelTenderBidDecline"),
	FUEL_TENDER_CANCEL("FuelTenderCancel"),
	FUEL_TENDER_NEW_ROUND("FuelTenderNewRound"),
	FUEL_TENDER_BID("FuelTenderBid"),
	FUEL_TENDER_CANCEL_BID("FuelTenderCancelBid"),
	FUEL_TENDER_ACCEPT_AWARD("FuelTenderAcceptAward"),
	FUEL_TENDER_DECLINE_AWARD("FuelTenderDeclineAward");

	private String type;

	private MessageType(String type) {
		this.type = type;
	}

	public String getType() {
		return this.type;
	}

	public static MessageType getByType(String type) {
		for (MessageType mType : values()) {
			if (mType.getType().equalsIgnoreCase(type)) {
				return mType;
			}
		}
		throw new InvalidEnumException("Inexistent MessageType with type: " + type);
	}
}
