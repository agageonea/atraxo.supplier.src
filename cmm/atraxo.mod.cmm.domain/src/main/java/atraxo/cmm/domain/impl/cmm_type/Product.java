/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum Product {

	_EMPTY_("", ""), _JET_A_("Jet-A", "JETA"), _JET_A1_("Jet-A1", "JETA1"), _TS_1_(
			"TS-1", "TS1"), _F_34_("F-34", "F34"), _JP_8_("JP-8", "JP8"), _RP_4_(
			"RP-4", "RP4"), _T1_("T1", "T1"), _A1_BIO_("A1-BIO", "A1BIO"), _CAT_2_(
			"Cat 2", "CAT2"), _AVGAS_("AVGAS", "AG100"), _AG_1_HL_("AG-1-HL",
			"AG1HL"), _AG_80_("AG-80", "AG80"), _JA_1_A_("JA-1-A", "JA1A"), _JAA_(
			"Jaa", "JAA"), _JP_5_("JP-5", "JP5"), _NON_("NON", "NON"), _OTH_(
			"OTH", "OTH");

	private String name;
	private String iataName;

	private Product(String name, String iataName) {
		this.name = name;
		this.iataName = iataName;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static Product getByName(String name) {
		for (Product status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Product with name: " + name);
	}

	public String getIataName() {
		return this.iataName;
	}

	public static Product getByIataName(String code) {
		for (Product status : values()) {
			if (status.getIataName().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Product with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
