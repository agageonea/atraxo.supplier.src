/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum Operators {

	_EMPTY_("", ""), _EQUAL_("=", "EQ"), _LOWER_("<", "LT"), _HIGHER_(">", "GT"), _LOWER_EQ_(
			"<=", "LE"), _HIGHER_EQ_(">=", "GE"), _IN_("IN", "IN"), _NOT_EQUAL_(
			"<>", "NE");

	private String name;
	private String code;

	private Operators(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static Operators getByName(String name) {
		for (Operators status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Operators with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static Operators getByCode(String code) {
		for (Operators status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Operators with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
