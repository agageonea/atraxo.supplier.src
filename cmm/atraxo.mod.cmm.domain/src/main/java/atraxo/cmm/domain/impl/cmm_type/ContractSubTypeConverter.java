/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ContractSubType} enum.
 * Generated code. Do not modify in this file.
 */
public class ContractSubTypeConverter
		implements
			AttributeConverter<ContractSubType, String> {

	@Override
	public String convertToDatabaseColumn(ContractSubType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ContractSubType.");
		}
		return value.getName();
	}

	@Override
	public ContractSubType convertToEntityAttribute(String value) {
		return ContractSubType.getByName(value);
	}

}
