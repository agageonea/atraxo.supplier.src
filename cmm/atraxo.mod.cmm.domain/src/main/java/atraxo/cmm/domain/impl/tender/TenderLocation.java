/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.tender;

import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatusConverter;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.ContractSubTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.PaymentChoise;
import atraxo.cmm.domain.impl.cmm_type.PaymentChoiseConverter;
import atraxo.cmm.domain.impl.cmm_type.PaymentType;
import atraxo.cmm.domain.impl.cmm_type.PaymentTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.Period;
import atraxo.cmm.domain.impl.cmm_type.PeriodConverter;
import atraxo.cmm.domain.impl.cmm_type.PricingType;
import atraxo.cmm.domain.impl.cmm_type.PricingTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.ProductConverter;
import atraxo.cmm.domain.impl.cmm_type.SourceType;
import atraxo.cmm.domain.impl.cmm_type.SourceTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.cmm_type.TaxTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatusConverter;
import atraxo.cmm.domain.impl.cmm_type.YesNo;
import atraxo.cmm.domain.impl.cmm_type.YesNoConverter;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.cmm.domain.impl.tender.TenderLocationExpectedPrice;
import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.DataProvider;
import atraxo.fmbas.domain.impl.fmbas_type.DataProviderConverter;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreqConverter;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceType;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceTypeConverter;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriodConverter;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.fmbas_type.OperatorConverter;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDayConverter;
import atraxo.fmbas.domain.impl.fmbas_type.TenderVolumeOffer;
import atraxo.fmbas.domain.impl.fmbas_type.TenderVolumeOfferConverter;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link TenderLocation} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = TenderLocation.NQ_FIND_BY_CODE, query = "SELECT e FROM TenderLocation e WHERE e.clientId = :clientId and e.location = :location and e.tender = :tender and e.taxType = :taxType and e.fuelProduct = :fuelProduct and e.flightServiceType = :flightServiceType and e.deliveryPoint = :deliveryPoint", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = TenderLocation.NQ_FIND_BY_CODE_PRIMITIVE, query = "SELECT e FROM TenderLocation e WHERE e.clientId = :clientId and e.location.id = :locationId and e.tender.id = :tenderId and e.taxType = :taxType and e.fuelProduct = :fuelProduct and e.flightServiceType = :flightServiceType and e.deliveryPoint = :deliveryPoint", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = TenderLocation.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM TenderLocation e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = TenderLocation.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = TenderLocation.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "location_id", "tender_id",
		"tax_type", "fuel_product", "flight_service_type", "delivery_point"})})
public class TenderLocation extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "CMM_TENDER_LOCATION";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "TenderLocation.findByCode";
	/**
	 * Named query find by unique key: Code using the ID field for references.
	 */
	public static final String NQ_FIND_BY_CODE_PRIMITIVE = "TenderLocation.findByCode_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "TenderLocation.findByBusiness";

	@NotBlank
	@Column(name = "location_bidding_status", nullable = false, length = 32)
	@Convert(converter = BiddingStatusConverter.class)
	private BiddingStatus locationBiddingStatus;

	@Column(name = "ad_hoc_location")
	private Boolean adHocLoc;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "agreement_from", nullable = false)
	private Date agreementFrom;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "agreement_to", nullable = false)
	private Date agreementTo;

	@Column(name = "volume", precision = 19, scale = 6)
	private BigDecimal volume;

	@Column(name = "has_total_volume")
	private Boolean hasTotalVolume;

	@Column(name = "periodtype", length = 32)
	@Convert(converter = PeriodConverter.class)
	private Period periodType;

	@Column(name = "fuel_product", length = 32)
	@Convert(converter = ProductConverter.class)
	private Product fuelProduct;

	@Column(name = "tax_type", length = 32)
	@Convert(converter = TaxTypeConverter.class)
	private TaxType taxType;

	@Column(name = "astmspecification", length = 100)
	private String astmSpecification;

	@Column(name = "iata_service_level", precision = 1)
	private Integer iataServiceLevel;

	@Column(name = "flight_service_type", length = 16)
	@Convert(converter = FlightServiceTypeConverter.class)
	private FlightServiceType flightServiceType;

	@Column(name = "delivery_point", length = 32)
	@Convert(converter = ContractSubTypeConverter.class)
	private ContractSubType deliveryPoint;

	@Column(name = "operating_hours", length = 100)
	private String operatingHours;

	@Column(name = "refueler_code", length = 32)
	private String refuelerCode;

	@Column(name = "title_transfer", length = 100)
	private String titleTransfer;

	@Column(name = "fuel_density", precision = 19, scale = 6)
	private BigDecimal fuelDensity;

	@Column(name = "operator", length = 32)
	@Convert(converter = OperatorConverter.class)
	private Operator operator;

	@Column(name = "conversion_factor", precision = 19, scale = 6)
	private BigDecimal conversionFactor;

	@Column(name = "comments", length = 2500)
	private String comments;

	@Column(name = "payment_terms", precision = 6)
	private Long paymentTerms;

	@Column(name = "payment_ref_date", length = 32)
	@Convert(converter = PaymentDayConverter.class)
	private PaymentDay paymentRefDate;

	@Column(name = "invoice_frequency", length = 32)
	@Convert(converter = InvoiceFreqConverter.class)
	private InvoiceFreq invoiceFrequency;

	@Column(name = "invoice_type", length = 32)
	@Convert(converter = InvoiceTypeConverter.class)
	private InvoiceType invoiceType;

	@Column(name = "number_of_days_prepaid", precision = 6)
	private Long numberOfDaysPrepaid;

	@Column(name = "preapayment_day_offset", precision = 6)
	private Long prepaymentDayOffset;

	@Column(name = "prepayment_frequency", length = 32)
	@Convert(converter = InvoiceFreqConverter.class)
	private InvoiceFreq prepaymentFrequency;

	@Column(name = "prepayment_amount", precision = 19, scale = 6)
	private BigDecimal prepaymentAmount;

	@Column(name = "payment_method", length = 32)
	@Convert(converter = PaymentTypeConverter.class)
	private PaymentType paymentMethod;

	@Column(name = "deposit_required", length = 32)
	@Convert(converter = YesNoConverter.class)
	private YesNo depositRequired;

	@Column(name = "exchange_rate_offset", length = 32)
	@Convert(converter = MasterAgreementsPeriodConverter.class)
	private MasterAgreementsPeriod exchangeRateOffset;

	@Column(name = "data_provider", length = 32)
	@Convert(converter = DataProviderConverter.class)
	private DataProvider dataProvider;

	@Column(name = "index_averaging_period", length = 32)
	@Convert(converter = MasterAgreementsPeriodConverter.class)
	private MasterAgreementsPeriod indexAveragingPeriod;

	@Column(name = "rate", precision = 19, scale = 6)
	private BigDecimal rate;

	@Column(name = "source_type", length = 32)
	@Convert(converter = SourceTypeConverter.class)
	private SourceType sourceType;

	@Column(name = "source_name", length = 100)
	private String sourceName;

	@Column(name = "credit_terms", length = 32)
	@Convert(converter = PaymentChoiseConverter.class)
	private PaymentChoise creditTerms;

	@Column(name = "pricing_base", length = 32)
	@Convert(converter = PricingTypeConverter.class)
	private PricingType pricingBase;

	@Column(name = "package_identifier", length = 100)
	private String packageIdentifier;

	@NotBlank
	@Column(name = "transmission_status", nullable = false, length = 32)
	@Convert(converter = TransmissionStatusConverter.class)
	private TransmissionStatus transmissionStatus;

	@NotNull
	@Column(name = "isvalid", nullable = false)
	private Boolean isValid;

	@Column(name = "volume_offer", length = 32)
	@Convert(converter = TenderVolumeOfferConverter.class)
	private TenderVolumeOffer volumeOffer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "location_id", referencedColumnName = "id")
	private Locations location;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Tender.class)
	@JoinColumn(name = "tender_id", referencedColumnName = "id")
	private Tender tender;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "volume_unit_id", referencedColumnName = "id")
	private Unit volumeUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "density_weight_unit_id", referencedColumnName = "id")
	private Unit densityWeightUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "density_volume_unit_id", referencedColumnName = "id")
	private Unit densityVolumeUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "density_conv_from_unit_id", referencedColumnName = "id")
	private Unit densityConvFromUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "density_conv_to_unit_id", referencedColumnName = "id")
	private Unit densityConvToUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "market_currency_id", referencedColumnName = "id")
	private Currencies marketCurrency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "market_pricing_unit_id", referencedColumnName = "id")
	private Unit marketPricingUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FinancialSources.class)
	@JoinColumn(name = "financial_source_id", referencedColumnName = "id")
	private FinancialSources financialSource;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AverageMethod.class)
	@JoinColumn(name = "averaging_method_id", referencedColumnName = "id")
	private AverageMethod averagingMethod;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = TimeSerie.class)
	@JoinColumn(name = "index_provider_id", referencedColumnName = "id")
	private TimeSerie indexProvider;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AverageMethod.class)
	@JoinColumn(name = "index_averaging_method_id", referencedColumnName = "id")
	private AverageMethod indexAveragingMethod;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "settlement_currency_id", referencedColumnName = "id")
	private Currencies settlementCurrency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "settlement_unit_id", referencedColumnName = "id")
	private Unit settlementUnit;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = TenderLocationAirlines.class, mappedBy = "tenderLocation", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<TenderLocationAirlines> tenderLocationAirlines;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = Contract.class, mappedBy = "bidTenderLocation")
	private Collection<Contract> bids;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = TenderLocationRound.class, mappedBy = "tenderLocation", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<TenderLocationRound> tenderLocationRounds;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = TenderLocationExpectedPrice.class, mappedBy = "tenderLocation", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<TenderLocationExpectedPrice> tenderLocationExpectedPrices;

	public BiddingStatus getLocationBiddingStatus() {
		return this.locationBiddingStatus;
	}

	public void setLocationBiddingStatus(BiddingStatus locationBiddingStatus) {
		this.locationBiddingStatus = locationBiddingStatus;
	}

	public Boolean getAdHocLoc() {
		return this.adHocLoc;
	}

	public void setAdHocLoc(Boolean adHocLoc) {
		this.adHocLoc = adHocLoc;
	}

	public Date getAgreementFrom() {
		return this.agreementFrom;
	}

	public void setAgreementFrom(Date agreementFrom) {
		this.agreementFrom = agreementFrom;
	}

	public Date getAgreementTo() {
		return this.agreementTo;
	}

	public void setAgreementTo(Date agreementTo) {
		this.agreementTo = agreementTo;
	}

	public BigDecimal getVolume() {
		return this.volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public Boolean getHasTotalVolume() {
		return this.hasTotalVolume;
	}

	public void setHasTotalVolume(Boolean hasTotalVolume) {
		this.hasTotalVolume = hasTotalVolume;
	}

	public Period getPeriodType() {
		return this.periodType;
	}

	public void setPeriodType(Period periodType) {
		this.periodType = periodType;
	}

	public Product getFuelProduct() {
		return this.fuelProduct;
	}

	public void setFuelProduct(Product fuelProduct) {
		this.fuelProduct = fuelProduct;
	}

	public TaxType getTaxType() {
		return this.taxType;
	}

	public void setTaxType(TaxType taxType) {
		this.taxType = taxType;
	}

	public String getAstmSpecification() {
		return this.astmSpecification;
	}

	public void setAstmSpecification(String astmSpecification) {
		this.astmSpecification = astmSpecification;
	}

	public Integer getIataServiceLevel() {
		return this.iataServiceLevel;
	}

	public void setIataServiceLevel(Integer iataServiceLevel) {
		this.iataServiceLevel = iataServiceLevel;
	}

	public FlightServiceType getFlightServiceType() {
		return this.flightServiceType;
	}

	public void setFlightServiceType(FlightServiceType flightServiceType) {
		this.flightServiceType = flightServiceType;
	}

	public ContractSubType getDeliveryPoint() {
		return this.deliveryPoint;
	}

	public void setDeliveryPoint(ContractSubType deliveryPoint) {
		this.deliveryPoint = deliveryPoint;
	}

	public String getOperatingHours() {
		return this.operatingHours;
	}

	public void setOperatingHours(String operatingHours) {
		this.operatingHours = operatingHours;
	}

	public String getRefuelerCode() {
		return this.refuelerCode;
	}

	public void setRefuelerCode(String refuelerCode) {
		this.refuelerCode = refuelerCode;
	}

	public String getTitleTransfer() {
		return this.titleTransfer;
	}

	public void setTitleTransfer(String titleTransfer) {
		this.titleTransfer = titleTransfer;
	}

	public BigDecimal getFuelDensity() {
		return this.fuelDensity;
	}

	public void setFuelDensity(BigDecimal fuelDensity) {
		this.fuelDensity = fuelDensity;
	}

	public Operator getOperator() {
		return this.operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public BigDecimal getConversionFactor() {
		return this.conversionFactor;
	}

	public void setConversionFactor(BigDecimal conversionFactor) {
		this.conversionFactor = conversionFactor;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Long getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Long paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public PaymentDay getPaymentRefDate() {
		return this.paymentRefDate;
	}

	public void setPaymentRefDate(PaymentDay paymentRefDate) {
		this.paymentRefDate = paymentRefDate;
	}

	public InvoiceFreq getInvoiceFrequency() {
		return this.invoiceFrequency;
	}

	public void setInvoiceFrequency(InvoiceFreq invoiceFrequency) {
		this.invoiceFrequency = invoiceFrequency;
	}

	public InvoiceType getInvoiceType() {
		return this.invoiceType;
	}

	public void setInvoiceType(InvoiceType invoiceType) {
		this.invoiceType = invoiceType;
	}

	public Long getNumberOfDaysPrepaid() {
		return this.numberOfDaysPrepaid;
	}

	public void setNumberOfDaysPrepaid(Long numberOfDaysPrepaid) {
		this.numberOfDaysPrepaid = numberOfDaysPrepaid;
	}

	public Long getPrepaymentDayOffset() {
		return this.prepaymentDayOffset;
	}

	public void setPrepaymentDayOffset(Long prepaymentDayOffset) {
		this.prepaymentDayOffset = prepaymentDayOffset;
	}

	public InvoiceFreq getPrepaymentFrequency() {
		return this.prepaymentFrequency;
	}

	public void setPrepaymentFrequency(InvoiceFreq prepaymentFrequency) {
		this.prepaymentFrequency = prepaymentFrequency;
	}

	public BigDecimal getPrepaymentAmount() {
		return this.prepaymentAmount;
	}

	public void setPrepaymentAmount(BigDecimal prepaymentAmount) {
		this.prepaymentAmount = prepaymentAmount;
	}

	public PaymentType getPaymentMethod() {
		return this.paymentMethod;
	}

	public void setPaymentMethod(PaymentType paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public YesNo getDepositRequired() {
		return this.depositRequired;
	}

	public void setDepositRequired(YesNo depositRequired) {
		this.depositRequired = depositRequired;
	}

	public MasterAgreementsPeriod getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(MasterAgreementsPeriod exchangeRateOffset) {
		this.exchangeRateOffset = exchangeRateOffset;
	}

	public DataProvider getDataProvider() {
		return this.dataProvider;
	}

	public void setDataProvider(DataProvider dataProvider) {
		this.dataProvider = dataProvider;
	}

	public MasterAgreementsPeriod getIndexAveragingPeriod() {
		return this.indexAveragingPeriod;
	}

	public void setIndexAveragingPeriod(
			MasterAgreementsPeriod indexAveragingPeriod) {
		this.indexAveragingPeriod = indexAveragingPeriod;
	}

	public BigDecimal getRate() {
		return this.rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public SourceType getSourceType() {
		return this.sourceType;
	}

	public void setSourceType(SourceType sourceType) {
		this.sourceType = sourceType;
	}

	public String getSourceName() {
		return this.sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public PaymentChoise getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(PaymentChoise creditTerms) {
		this.creditTerms = creditTerms;
	}

	public PricingType getPricingBase() {
		return this.pricingBase;
	}

	public void setPricingBase(PricingType pricingBase) {
		this.pricingBase = pricingBase;
	}

	public String getPackageIdentifier() {
		return this.packageIdentifier;
	}

	public void setPackageIdentifier(String packageIdentifier) {
		this.packageIdentifier = packageIdentifier;
	}

	public TransmissionStatus getTransmissionStatus() {
		return this.transmissionStatus;
	}

	public void setTransmissionStatus(TransmissionStatus transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public Boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}

	public TenderVolumeOffer getVolumeOffer() {
		return this.volumeOffer;
	}

	public void setVolumeOffer(TenderVolumeOffer volumeOffer) {
		this.volumeOffer = volumeOffer;
	}

	public Locations getLocation() {
		return this.location;
	}

	public void setLocation(Locations location) {
		if (location != null) {
			this.__validate_client_context__(location.getClientId());
		}
		this.location = location;
	}
	public Tender getTender() {
		return this.tender;
	}

	public void setTender(Tender tender) {
		if (tender != null) {
			this.__validate_client_context__(tender.getClientId());
		}
		this.tender = tender;
	}
	public Unit getVolumeUnit() {
		return this.volumeUnit;
	}

	public void setVolumeUnit(Unit volumeUnit) {
		if (volumeUnit != null) {
			this.__validate_client_context__(volumeUnit.getClientId());
		}
		this.volumeUnit = volumeUnit;
	}
	public Unit getDensityWeightUnit() {
		return this.densityWeightUnit;
	}

	public void setDensityWeightUnit(Unit densityWeightUnit) {
		if (densityWeightUnit != null) {
			this.__validate_client_context__(densityWeightUnit.getClientId());
		}
		this.densityWeightUnit = densityWeightUnit;
	}
	public Unit getDensityVolumeUnit() {
		return this.densityVolumeUnit;
	}

	public void setDensityVolumeUnit(Unit densityVolumeUnit) {
		if (densityVolumeUnit != null) {
			this.__validate_client_context__(densityVolumeUnit.getClientId());
		}
		this.densityVolumeUnit = densityVolumeUnit;
	}
	public Unit getDensityConvFromUnit() {
		return this.densityConvFromUnit;
	}

	public void setDensityConvFromUnit(Unit densityConvFromUnit) {
		if (densityConvFromUnit != null) {
			this.__validate_client_context__(densityConvFromUnit.getClientId());
		}
		this.densityConvFromUnit = densityConvFromUnit;
	}
	public Unit getDensityConvToUnit() {
		return this.densityConvToUnit;
	}

	public void setDensityConvToUnit(Unit densityConvToUnit) {
		if (densityConvToUnit != null) {
			this.__validate_client_context__(densityConvToUnit.getClientId());
		}
		this.densityConvToUnit = densityConvToUnit;
	}
	public Currencies getMarketCurrency() {
		return this.marketCurrency;
	}

	public void setMarketCurrency(Currencies marketCurrency) {
		if (marketCurrency != null) {
			this.__validate_client_context__(marketCurrency.getClientId());
		}
		this.marketCurrency = marketCurrency;
	}
	public Unit getMarketPricingUnit() {
		return this.marketPricingUnit;
	}

	public void setMarketPricingUnit(Unit marketPricingUnit) {
		if (marketPricingUnit != null) {
			this.__validate_client_context__(marketPricingUnit.getClientId());
		}
		this.marketPricingUnit = marketPricingUnit;
	}
	public FinancialSources getFinancialSource() {
		return this.financialSource;
	}

	public void setFinancialSource(FinancialSources financialSource) {
		if (financialSource != null) {
			this.__validate_client_context__(financialSource.getClientId());
		}
		this.financialSource = financialSource;
	}
	public AverageMethod getAveragingMethod() {
		return this.averagingMethod;
	}

	public void setAveragingMethod(AverageMethod averagingMethod) {
		if (averagingMethod != null) {
			this.__validate_client_context__(averagingMethod.getClientId());
		}
		this.averagingMethod = averagingMethod;
	}
	public TimeSerie getIndexProvider() {
		return this.indexProvider;
	}

	public void setIndexProvider(TimeSerie indexProvider) {
		if (indexProvider != null) {
			this.__validate_client_context__(indexProvider.getClientId());
		}
		this.indexProvider = indexProvider;
	}
	public AverageMethod getIndexAveragingMethod() {
		return this.indexAveragingMethod;
	}

	public void setIndexAveragingMethod(AverageMethod indexAveragingMethod) {
		if (indexAveragingMethod != null) {
			this.__validate_client_context__(indexAveragingMethod.getClientId());
		}
		this.indexAveragingMethod = indexAveragingMethod;
	}
	public Currencies getSettlementCurrency() {
		return this.settlementCurrency;
	}

	public void setSettlementCurrency(Currencies settlementCurrency) {
		if (settlementCurrency != null) {
			this.__validate_client_context__(settlementCurrency.getClientId());
		}
		this.settlementCurrency = settlementCurrency;
	}
	public Unit getSettlementUnit() {
		return this.settlementUnit;
	}

	public void setSettlementUnit(Unit settlementUnit) {
		if (settlementUnit != null) {
			this.__validate_client_context__(settlementUnit.getClientId());
		}
		this.settlementUnit = settlementUnit;
	}

	public Collection<TenderLocationAirlines> getTenderLocationAirlines() {
		return this.tenderLocationAirlines;
	}

	public void setTenderLocationAirlines(
			Collection<TenderLocationAirlines> tenderLocationAirlines) {
		this.tenderLocationAirlines = tenderLocationAirlines;
	}

	/**
	 * @param e
	 */
	public void addToTenderLocationAirlines(TenderLocationAirlines e) {
		if (this.tenderLocationAirlines == null) {
			this.tenderLocationAirlines = new ArrayList<>();
		}
		e.setTenderLocation(this);
		this.tenderLocationAirlines.add(e);
	}
	public Collection<Contract> getBids() {
		return this.bids;
	}

	public void setBids(Collection<Contract> bids) {
		this.bids = bids;
	}

	/**
	 * @param e
	 */
	public void addToBids(Contract e) {
		if (this.bids == null) {
			this.bids = new ArrayList<>();
		}
		e.setBidTenderLocation(this);
		this.bids.add(e);
	}
	public Collection<TenderLocationRound> getTenderLocationRounds() {
		return this.tenderLocationRounds;
	}

	public void setTenderLocationRounds(
			Collection<TenderLocationRound> tenderLocationRounds) {
		this.tenderLocationRounds = tenderLocationRounds;
	}

	/**
	 * @param e
	 */
	public void addToTenderLocationRounds(TenderLocationRound e) {
		if (this.tenderLocationRounds == null) {
			this.tenderLocationRounds = new ArrayList<>();
		}
		e.setTenderLocation(this);
		this.tenderLocationRounds.add(e);
	}
	public Collection<TenderLocationExpectedPrice> getTenderLocationExpectedPrices() {
		return this.tenderLocationExpectedPrices;
	}

	public void setTenderLocationExpectedPrices(
			Collection<TenderLocationExpectedPrice> tenderLocationExpectedPrices) {
		this.tenderLocationExpectedPrices = tenderLocationExpectedPrices;
	}

	/**
	 * @param e
	 */
	public void addToTenderLocationExpectedPrices(TenderLocationExpectedPrice e) {
		if (this.tenderLocationExpectedPrices == null) {
			this.tenderLocationExpectedPrices = new ArrayList<>();
		}
		e.setTenderLocation(this);
		this.tenderLocationExpectedPrices.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.isValid == null) {
			this.isValid = new Boolean(true);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.location == null) ? 0 : this.location.hashCode());
		result = prime * result
				+ ((this.tender == null) ? 0 : this.tender.hashCode());
		result = prime * result
				+ ((this.taxType == null) ? 0 : this.taxType.hashCode());
		result = prime
				* result
				+ ((this.fuelProduct == null) ? 0 : this.fuelProduct.hashCode());
		result = prime
				* result
				+ ((this.flightServiceType == null)
						? 0
						: this.flightServiceType.hashCode());
		result = prime
				* result
				+ ((this.deliveryPoint == null) ? 0 : this.deliveryPoint
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		TenderLocation other = (TenderLocation) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.location == null) {
			if (other.location != null) {
				return false;
			}
		} else if (!this.location.equals(other.location)) {
			return false;
		}
		if (this.tender == null) {
			if (other.tender != null) {
				return false;
			}
		} else if (!this.tender.equals(other.tender)) {
			return false;
		}
		if (this.taxType == null) {
			if (other.taxType != null) {
				return false;
			}
		} else if (!this.taxType.equals(other.taxType)) {
			return false;
		}
		if (this.fuelProduct == null) {
			if (other.fuelProduct != null) {
				return false;
			}
		} else if (!this.fuelProduct.equals(other.fuelProduct)) {
			return false;
		}
		if (this.flightServiceType == null) {
			if (other.flightServiceType != null) {
				return false;
			}
		} else if (!this.flightServiceType.equals(other.flightServiceType)) {
			return false;
		}
		if (this.deliveryPoint == null) {
			if (other.deliveryPoint != null) {
				return false;
			}
		} else if (!this.deliveryPoint.equals(other.deliveryPoint)) {
			return false;
		}
		return true;
	}
}
