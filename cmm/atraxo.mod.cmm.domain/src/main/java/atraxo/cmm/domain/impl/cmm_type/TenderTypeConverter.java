/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TenderType} enum.
 * Generated code. Do not modify in this file.
 */
public class TenderTypeConverter
		implements
			AttributeConverter<TenderType, String> {

	@Override
	public String convertToDatabaseColumn(TenderType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TenderType.");
		}
		return value.getName();
	}

	@Override
	public TenderType convertToEntityAttribute(String value) {
		return TenderType.getByName(value);
	}

}
