/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link IsSpot} enum.
 * Generated code. Do not modify in this file.
 */
public class IsSpotConverter implements AttributeConverter<IsSpot, String> {

	@Override
	public String convertToDatabaseColumn(IsSpot value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null IsSpot.");
		}
		return value.getName();
	}

	@Override
	public IsSpot convertToEntityAttribute(String value) {
		return IsSpot.getByName(value);
	}

}
