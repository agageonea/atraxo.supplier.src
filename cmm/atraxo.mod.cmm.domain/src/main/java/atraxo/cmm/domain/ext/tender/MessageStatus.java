package atraxo.cmm.domain.ext.tender;

import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * @author abolindu
 */
public enum MessageStatus {
	RECEIVED("Received", "RC"), PROCESSED("Open", "PR"), READ("Read", "RD"), ERROR("Incomplete", "ER");

	private String status;
	private String iataCode;

	private MessageStatus(String status, String iataCode) {
		this.status = status;
		this.iataCode = iataCode;
	}

	public String getStatus() {
		return this.status;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public static MessageStatus getByStatus(String status) {
		for (MessageStatus mStatus : values()) {
			if (mStatus.getStatus().equalsIgnoreCase(status)) {
				return mStatus;
			}
		}
		throw new InvalidEnumException("Inexistent MessageStatus with status: " + status);
	}

	public static MessageStatus getByIataCode(String code) {
		for (MessageStatus mStatus : values()) {
			if (mStatus.getIataCode().equalsIgnoreCase(code)) {
				return mStatus;
			}
		}
		throw new InvalidEnumException("Inexistent MessageStatus with iata code: " + code);
	}

}
