/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FuelDelivery {

	_EMPTY_("", ""), _EX_HIDRANT_("Ex hidrant", "PRI"), _INTO_PLANE_(
			"Into Plane", "PSI"), _INTO_STORAGE_("Into Storage", "PRO"), _FUELING_SERVICE_(
			"Fueling Service", "FSI");

	private String name;
	private String code;

	private FuelDelivery(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FuelDelivery getByName(String name) {
		for (FuelDelivery status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent FuelDelivery with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static FuelDelivery getByCode(String code) {
		for (FuelDelivery status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent FuelDelivery with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
