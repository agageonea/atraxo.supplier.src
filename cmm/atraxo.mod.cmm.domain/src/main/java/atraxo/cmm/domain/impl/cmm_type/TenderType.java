/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TenderType {

	_EMPTY_("", ""), _PRODUCT_("Product", "P"), _FUELING_SERVICE_(
			"Fueling Service", "F");

	private String name;
	private String code;

	private TenderType(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TenderType getByName(String name) {
		for (TenderType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TenderType with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static TenderType getByCode(String code) {
		for (TenderType status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TenderType with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
