package atraxo.cmm.domain.ext.mailmerge.dto.contract;

public class CurrencyUnitDto {

	private String isoNumericCode;
	private String alphabeticCode;
	private String name;

	public String getIsoNumericCode() {
		return this.isoNumericCode;
	}

	public void setIsoNumericCode(String isoNumericCode) {
		this.isoNumericCode = isoNumericCode;
	}

	public String getAlphabeticCode() {
		return this.alphabeticCode;
	}

	public void setAlphabeticCode(String alphabeticCode) {
		this.alphabeticCode = alphabeticCode;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
