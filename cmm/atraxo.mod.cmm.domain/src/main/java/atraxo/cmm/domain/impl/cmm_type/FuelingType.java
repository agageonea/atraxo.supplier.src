/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FuelingType {

	_EMPTY_("", ""), _RE_FUELER_("Re-Fueler", "REF"), _HYDRANT_("Hydrant",
			"HYD");

	private String name;
	private String code;

	private FuelingType(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FuelingType getByName(String name) {
		for (FuelingType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent FuelingType with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static FuelingType getByCode(String code) {
		for (FuelingType status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent FuelingType with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
