package atraxo.cmm.domain.ext.mailmerge.dto;

import java.util.Date;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;

/**
 * Price Update DTO containing the info used for sending email in the price Update Workflow
 *
 * @author vhojda
 */
public class PriceUpdateDto extends AbstractDto {

	/**
	 *
	 */
	private static final long serialVersionUID = 7017730818882352430L;

	private String title;
	private String fullName;
	private String fullnameOfRequester;
	private String priceCategoryName;
	private String priceUpdateLocationArea;
	private String newPriceValue;
	private String currencyCode;
	private String unitCode;
	private Date validFrom;

	private String approvalRequestNote;

	private String approverNameFirstLevel;
	private String approvalNoteFirstLevel;

	private String approverNameSecondLevel;
	private String approvalNoteSecondLevel;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFullnameOfRequester() {
		return this.fullnameOfRequester;
	}

	public void setFullnameOfRequester(String fullnameOfRequester) {
		this.fullnameOfRequester = fullnameOfRequester;
	}

	public String getPriceCategoryName() {
		return this.priceCategoryName;
	}

	public void setPriceCategoryName(String priceCategoryName) {
		this.priceCategoryName = priceCategoryName;
	}

	public String getPriceUpdateLocationArea() {
		return this.priceUpdateLocationArea;
	}

	public void setPriceUpdateLocationArea(String priceUpdateLocationArea) {
		this.priceUpdateLocationArea = priceUpdateLocationArea;
	}

	public String getNewPriceValue() {
		return this.newPriceValue;
	}

	public void setNewPriceValue(String newPriceValue) {
		this.newPriceValue = newPriceValue;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public String getApprovalRequestNote() {
		return this.approvalRequestNote;
	}

	public void setApprovalRequestNote(String approvalRequestNote) {
		this.approvalRequestNote = approvalRequestNote;
	}

	public String getApproverNameFirstLevel() {
		return this.approverNameFirstLevel;
	}

	public void setApproverNameFirstLevel(String approverNameFirstLevel) {
		this.approverNameFirstLevel = approverNameFirstLevel;
	}

	public String getApprovalNoteFirstLevel() {
		return this.approvalNoteFirstLevel;
	}

	public void setApprovalNoteFirstLevel(String approvalNoteFirstLevel) {
		this.approvalNoteFirstLevel = approvalNoteFirstLevel;
	}

	public String getApproverNameSecondLevel() {
		return this.approverNameSecondLevel;
	}

	public void setApproverNameSecondLevel(String approverNameSecondLevel) {
		this.approverNameSecondLevel = approverNameSecondLevel;
	}

	public String getApprovalNoteSecondLevel() {
		return this.approvalNoteSecondLevel;
	}

	public void setApprovalNoteSecondLevel(String approvalNoteSecondLevel) {
		this.approvalNoteSecondLevel = approvalNoteSecondLevel;
	}

}
