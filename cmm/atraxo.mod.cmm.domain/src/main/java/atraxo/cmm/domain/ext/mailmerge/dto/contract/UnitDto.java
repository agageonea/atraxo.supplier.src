package atraxo.cmm.domain.ext.mailmerge.dto.contract;

import java.math.BigDecimal;

public class UnitDto {

	private String code;
	private String name;
	private String iataCode;
	private BigDecimal factor;
	private String unitTypeIndicator;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public BigDecimal getFactor() {
		return this.factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

	public String getUnitTypeIndicator() {
		return this.unitTypeIndicator;
	}

	public void setUnitTypeIndicator(String unitTypeIndicator) {
		this.unitTypeIndicator = unitTypeIndicator;
	}

}
