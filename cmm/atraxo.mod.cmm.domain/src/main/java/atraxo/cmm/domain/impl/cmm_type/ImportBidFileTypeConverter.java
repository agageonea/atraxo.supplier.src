/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ImportBidFileType} enum.
 * Generated code. Do not modify in this file.
 */
public class ImportBidFileTypeConverter
		implements
			AttributeConverter<ImportBidFileType, String> {

	@Override
	public String convertToDatabaseColumn(ImportBidFileType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ImportBidFileType.");
		}
		return value.getName();
	}

	@Override
	public ImportBidFileType convertToEntityAttribute(String value) {
		return ImportBidFileType.getByName(value);
	}

}
