/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum EventType {

	_EMPTY_(""), _TRANSPORT_("Transport"), _PERIOD_("Period"), _MOVEMENT_(
			"Movement"), _CSO_EVENT_("CSO event");

	private String name;

	private EventType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static EventType getByName(String name) {
		for (EventType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent EventType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
