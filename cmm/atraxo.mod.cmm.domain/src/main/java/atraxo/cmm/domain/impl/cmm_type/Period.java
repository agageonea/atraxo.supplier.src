/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum Period {

	_EMPTY_("", ""), _CONTRACT_PERIOD_("Contract period", "T"), _MONTHLY_(
			"Monthly", "M"), _YEARLY_("Yearly", "Y");

	private String name;
	private String code;

	private Period(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static Period getByName(String name) {
		for (Period status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Period with name: " + name);
	}

	public String getCode() {
		return this.code;
	}

	public static Period getByCode(String code) {
		for (Period status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Period with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
