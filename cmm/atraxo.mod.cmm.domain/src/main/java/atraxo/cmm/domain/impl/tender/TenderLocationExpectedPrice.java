/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.tender;

import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.categories.IataPC;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link TenderLocationExpectedPrice} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = TenderLocationExpectedPrice.NQ_FIND_BY_TENDER_LOCATION_EXPECTED_FEES, query = "SELECT e FROM TenderLocationExpectedPrice e WHERE e.clientId = :clientId and e.tenderLocation = :tenderLocation and e.iataPriceCategory = :iataPriceCategory", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = TenderLocationExpectedPrice.NQ_FIND_BY_TENDER_LOCATION_EXPECTED_FEES_PRIMITIVE, query = "SELECT e FROM TenderLocationExpectedPrice e WHERE e.clientId = :clientId and e.tenderLocation.id = :tenderLocationId and e.iataPriceCategory.id = :iataPriceCategoryId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = TenderLocationExpectedPrice.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM TenderLocationExpectedPrice e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = TenderLocationExpectedPrice.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = TenderLocationExpectedPrice.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "tender_location_id",
		"iata_price_category_id"})})
public class TenderLocationExpectedPrice extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "CMM_TENDER_LOCATION_EXP_PRICES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Tender_location_expected_fees.
	 */
	public static final String NQ_FIND_BY_TENDER_LOCATION_EXPECTED_FEES = "TenderLocationExpectedPrice.findByTender_location_expected_fees";
	/**
	 * Named query find by unique key: Tender_location_expected_fees using the ID field for references.
	 */
	public static final String NQ_FIND_BY_TENDER_LOCATION_EXPECTED_FEES_PRIMITIVE = "TenderLocationExpectedPrice.findByTender_location_expected_fees_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "TenderLocationExpectedPrice.findByBusiness";

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = TenderLocation.class)
	@JoinColumn(name = "tender_location_id", referencedColumnName = "id")
	private TenderLocation tenderLocation;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = IataPC.class)
	@JoinColumn(name = "iata_price_category_id", referencedColumnName = "id")
	private IataPC iataPriceCategory;
	public TenderLocation getTenderLocation() {
		return this.tenderLocation;
	}

	public void setTenderLocation(TenderLocation tenderLocation) {
		if (tenderLocation != null) {
			this.__validate_client_context__(tenderLocation.getClientId());
		}
		this.tenderLocation = tenderLocation;
	}
	public IataPC getIataPriceCategory() {
		return this.iataPriceCategory;
	}

	public void setIataPriceCategory(IataPC iataPriceCategory) {
		if (iataPriceCategory != null) {
			this.__validate_client_context__(iataPriceCategory.getClientId());
		}
		this.iataPriceCategory = iataPriceCategory;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime
				* result
				+ ((this.tenderLocation == null) ? 0 : this.tenderLocation
						.hashCode());
		result = prime
				* result
				+ ((this.iataPriceCategory == null)
						? 0
						: this.iataPriceCategory.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		TenderLocationExpectedPrice other = (TenderLocationExpectedPrice) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.tenderLocation == null) {
			if (other.tenderLocation != null) {
				return false;
			}
		} else if (!this.tenderLocation.equals(other.tenderLocation)) {
			return false;
		}
		if (this.iataPriceCategory == null) {
			if (other.iataPriceCategory != null) {
				return false;
			}
		} else if (!this.iataPriceCategory.equals(other.iataPriceCategory)) {
			return false;
		}
		return true;
	}
}
