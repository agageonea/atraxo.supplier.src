package atraxo.cmm.domain.ext.invoice;

public enum InvoiceStatus {

	DRAFT("Draft"), CHECK_PASSED("Check passed"), CHECK_FAILED("Check failed"), AWAITING_APPROVAL("Awaiting approval"), AWAITING_PAYMENT(
			"Awaiting payment"), NEEDS_RECHECK("Needs recheck"), PAID("Paid"), NO_CONTRACT("No Contract");

	private String name;

	private InvoiceStatus(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

}
