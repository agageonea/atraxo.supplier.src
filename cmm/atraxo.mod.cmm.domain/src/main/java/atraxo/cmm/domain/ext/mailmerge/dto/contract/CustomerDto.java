package atraxo.cmm.domain.ext.mailmerge.dto.contract;

import java.util.Date;

public class CustomerDto {

	private String code;
	private String name;
	private String status;
	private String type;
	private String email;
	private String phoneNo;
	private String faxNo;
	private String website;
	private String natureOfBusiness;
	private String aoc;
	private Date aocValidFrom;
	private Date aocValidTo;
	private String tradeLicense;
	private Date tradeLicenseValidFrom;
	private Date tradeLicenseValidTo;
	private String vatRegNumber;
	private String taxIdNumber;
	private String incomeTaxNumber;
	private String accountNumber;
	private String iataCode;
	private String icaoCode;
	private String businessType;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNo() {
		return this.phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getFaxNo() {
		return this.faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getNatureOfBusiness() {
		return this.natureOfBusiness;
	}

	public void setNatureOfBusiness(String natureOfBusiness) {
		this.natureOfBusiness = natureOfBusiness;
	}

	public String getAoc() {
		return this.aoc;
	}

	public void setAoc(String aoc) {
		this.aoc = aoc;
	}

	public Date getAocValidFrom() {
		return this.aocValidFrom;
	}

	public void setAocValidFrom(Date aocValidFrom) {
		this.aocValidFrom = aocValidFrom;
	}

	public Date getAocValidTo() {
		return this.aocValidTo;
	}

	public void setAocValidTo(Date aocValidTo) {
		this.aocValidTo = aocValidTo;
	}

	public String getTradeLicense() {
		return this.tradeLicense;
	}

	public void setTradeLicense(String tradeLicense) {
		this.tradeLicense = tradeLicense;
	}

	public Date getTradeLicenseValidFrom() {
		return this.tradeLicenseValidFrom;
	}

	public void setTradeLicenseValidFrom(Date tradeLicenseValidFrom) {
		this.tradeLicenseValidFrom = tradeLicenseValidFrom;
	}

	public Date getTradeLicenseValidTo() {
		return this.tradeLicenseValidTo;
	}

	public void setTradeLicenseValidTo(Date tradeLicenseValidTo) {
		this.tradeLicenseValidTo = tradeLicenseValidTo;
	}

	public String getVatRegNumber() {
		return this.vatRegNumber;
	}

	public void setVatRegNumber(String vatRegNumber) {
		this.vatRegNumber = vatRegNumber;
	}

	public String getTaxIdNumber() {
		return this.taxIdNumber;
	}

	public void setTaxIdNumber(String taxIdNumber) {
		this.taxIdNumber = taxIdNumber;
	}

	public String getIncomeTaxNumber() {
		return this.incomeTaxNumber;
	}

	public void setIncomeTaxNumber(String incomeTaxNumber) {
		this.incomeTaxNumber = incomeTaxNumber;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public String getIcaoCode() {
		return this.icaoCode;
	}

	public void setIcaoCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}

	public String getBusinessType() {
		return this.businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

}
