package atraxo.cmm.domain.ext.mailmerge.dto.contract;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;

public class ContractDto extends AbstractDto {

	private String number;
	private String dealType;
	private String type;
	private String flightServiceType;
	private String subType;
	private String scope;
	private String termOrSpot;
	private Date validFrom;
	private Date validTo;
	private String status;
	private String flightType;
	private String product;
	private String taxType;
	private String quantityType;
	private String period;
	private BigDecimal volumeTolerance;
	private BigDecimal sharePercentage;
	private String counterparty;
	private BigDecimal settlementDecimals;
	private String exchangeRateOffset;
	private BigDecimal paymentTerms;
	private String paymentRefDay;
	private String invoiceFrequency;
	private BigDecimal invoiceFrequencyNumber;
	private String creditTerms;
	private String invoiceType;
	private String invoicePayableBy;
	private String vat;
	private String reviewPeriod;
	private String reviewFirstDate;
	private String reviewSecondDate;
	private String reviewNotification;
	private String paymentComment;
	private String eventType;
	private BigDecimal awardedVolume;
	private BigDecimal offeredVolume;
	private BigDecimal iataServiceLevel;
	private String usingForexOf;
	private Boolean hardCopy;
	private String periodApprovalStatus;
	private String priceApprovalStatus;
	private String shipToApprovalStatus;
	private LocationDto location;
	private CustomerDto holder;
	private SupplierDto intoPlaneAgent;
	private ResponsibleBuyerDto responsibleBuyer;
	private ContactDto contact;
	private UnitDto contractVolumeUnit;
	private CurrencyDto settlementCurrency;
	private UnitDto settlementUnit;
	private AverageMethodDto averageMethod;
	private FinancialSourcesDto financialSources;
	private CustomerDto riskHolder;
	private SupplierDto supplier;
	private CustomerDto customer;
	private CustomerDto billTo;
	private List<ShipToDto> shipTos;

	private List<ContractPriceDto> prices;

	public void addContractPrice(ContractPriceDto price) {
		if (this.prices == null) {
			this.prices = new ArrayList<>();
		}
		this.prices.add(price);
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDealType() {
		return this.dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFlightServiceType() {
		return this.flightServiceType;
	}

	public void setFlightServiceType(String flightServiceType) {
		this.flightServiceType = flightServiceType;
	}

	public String getSubType() {
		return this.subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getScope() {
		return this.scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getTermOrSpot() {
		return this.termOrSpot;
	}

	public void setTermOrSpot(String termOrSpot) {
		this.termOrSpot = termOrSpot;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFlightType() {
		return this.flightType;
	}

	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public String getProduct() {
		return this.product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getTaxType() {
		return this.taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public String getQuantityType() {
		return this.quantityType;
	}

	public void setQuantityType(String quantityType) {
		this.quantityType = quantityType;
	}

	public String getPeriod() {
		return this.period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public BigDecimal getVolumeTolerance() {
		return this.volumeTolerance;
	}

	public void setVolumeTolerance(BigDecimal volumeTolerance) {
		this.volumeTolerance = volumeTolerance;
	}

	public BigDecimal getSharePercentage() {
		return this.sharePercentage;
	}

	public void setSharePercentage(BigDecimal sharePercentage) {
		this.sharePercentage = sharePercentage;
	}

	public String getCounterparty() {
		return this.counterparty;
	}

	public void setCounterparty(String counterparty) {
		this.counterparty = counterparty;
	}

	public BigDecimal getSettlementDecimals() {
		return this.settlementDecimals;
	}

	public void setSettlementDecimals(BigDecimal settlementDecimals) {
		this.settlementDecimals = settlementDecimals;
	}

	public String getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(String exchangeRateOffset) {
		this.exchangeRateOffset = exchangeRateOffset;
	}

	public BigDecimal getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(BigDecimal paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getPaymentRefDay() {
		return this.paymentRefDay;
	}

	public void setPaymentRefDay(String paymentRefDay) {
		this.paymentRefDay = paymentRefDay;
	}

	public String getInvoiceFrequency() {
		return this.invoiceFrequency;
	}

	public void setInvoiceFrequency(String invoiceFrequency) {
		this.invoiceFrequency = invoiceFrequency;
	}

	public BigDecimal getInvoiceFrequencyNumber() {
		return this.invoiceFrequencyNumber;
	}

	public void setInvoiceFrequencyNumber(BigDecimal invoiceFrequencyNumber) {
		this.invoiceFrequencyNumber = invoiceFrequencyNumber;
	}

	public String getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	public String getInvoiceType() {
		return this.invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getInvoicePayableBy() {
		return this.invoicePayableBy;
	}

	public void setInvoicePayableBy(String invoicePayableBy) {
		this.invoicePayableBy = invoicePayableBy;
	}

	public String getVat() {
		return this.vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	public String getReviewPeriod() {
		return this.reviewPeriod;
	}

	public void setReviewPeriod(String reviewPeriod) {
		this.reviewPeriod = reviewPeriod;
	}

	public String getReviewFirstDate() {
		return this.reviewFirstDate;
	}

	public void setReviewFirstDate(String reviewFirstDate) {
		this.reviewFirstDate = reviewFirstDate;
	}

	public String getReviewSecondDate() {
		return this.reviewSecondDate;
	}

	public void setReviewSecondDate(String reviewSecondDate) {
		this.reviewSecondDate = reviewSecondDate;
	}

	public String getReviewNotification() {
		return this.reviewNotification;
	}

	public void setReviewNotification(String reviewNotification) {
		this.reviewNotification = reviewNotification;
	}

	public String getPaymentComment() {
		return this.paymentComment;
	}

	public void setPaymentComment(String paymentComment) {
		this.paymentComment = paymentComment;
	}

	public String getEventType() {
		return this.eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public BigDecimal getAwardedVolume() {
		return this.awardedVolume;
	}

	public void setAwardedVolume(BigDecimal awardedVolume) {
		this.awardedVolume = awardedVolume;
	}

	public BigDecimal getOfferedVolume() {
		return this.offeredVolume;
	}

	public void setOfferedVolume(BigDecimal offeredVolume) {
		this.offeredVolume = offeredVolume;
	}

	public BigDecimal getIataServiceLevel() {
		return this.iataServiceLevel;
	}

	public void setIataServiceLevel(BigDecimal iataServiceLevel) {
		this.iataServiceLevel = iataServiceLevel;
	}

	public String getUsingForexOf() {
		return this.usingForexOf;
	}

	public void setUsingForexOf(String usingForexOf) {
		this.usingForexOf = usingForexOf;
	}

	public Boolean getHardCopy() {
		return this.hardCopy;
	}

	public void setHardCopy(Boolean hardCopy) {
		this.hardCopy = hardCopy;
	}

	public String getPeriodApprovalStatus() {
		return this.periodApprovalStatus;
	}

	public void setPeriodApprovalStatus(String periodApprovalStatus) {
		this.periodApprovalStatus = periodApprovalStatus;
	}

	public String getPriceApprovalStatus() {
		return this.priceApprovalStatus;
	}

	public void setPriceApprovalStatus(String priceApprovalStatus) {
		this.priceApprovalStatus = priceApprovalStatus;
	}

	public String getShipToApprovalStatus() {
		return this.shipToApprovalStatus;
	}

	public void setShipToApprovalStatus(String shipToApprovalStatus) {
		this.shipToApprovalStatus = shipToApprovalStatus;
	}

	public LocationDto getLocation() {
		return this.location;
	}

	public void setLocation(LocationDto location) {
		this.location = location;
	}

	public CustomerDto getHolder() {
		return this.holder;
	}

	public void setHolder(CustomerDto holder) {
		this.holder = holder;
	}

	public SupplierDto getIntoPlaneAgent() {
		return this.intoPlaneAgent;
	}

	public void setIntoPlaneAgent(SupplierDto intoPlaneAgent) {
		this.intoPlaneAgent = intoPlaneAgent;
	}

	public ResponsibleBuyerDto getResponsibleBuyer() {
		return this.responsibleBuyer;
	}

	public void setResponsibleBuyer(ResponsibleBuyerDto responsibleBuyer) {
		this.responsibleBuyer = responsibleBuyer;
	}

	public ContactDto getContact() {
		return this.contact;
	}

	public void setContact(ContactDto contact) {
		this.contact = contact;
	}

	public UnitDto getContractVolumeUnit() {
		return this.contractVolumeUnit;
	}

	public void setContractVolumeUnit(UnitDto contractVolumeUnit) {
		this.contractVolumeUnit = contractVolumeUnit;
	}

	public CurrencyDto getSettlementCurrency() {
		return this.settlementCurrency;
	}

	public void setSettlementCurrency(CurrencyDto settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}

	public UnitDto getSettlementUnit() {
		return this.settlementUnit;
	}

	public void setSettlementUnit(UnitDto settlementUnit) {
		this.settlementUnit = settlementUnit;
	}

	public AverageMethodDto getAverageMethod() {
		return this.averageMethod;
	}

	public void setAverageMethod(AverageMethodDto averageMethod) {
		this.averageMethod = averageMethod;
	}

	public FinancialSourcesDto getFinancialSources() {
		return this.financialSources;
	}

	public void setFinancialSources(FinancialSourcesDto financialSources) {
		this.financialSources = financialSources;
	}

	public CustomerDto getRiskHolder() {
		return this.riskHolder;
	}

	public void setRiskHolder(CustomerDto riskHolder) {
		this.riskHolder = riskHolder;
	}

	public SupplierDto getSupplier() {
		return this.supplier;
	}

	public void setSupplier(SupplierDto supplier) {
		this.supplier = supplier;
	}

	public CustomerDto getCustomer() {
		return this.customer;
	}

	public void setCustomer(CustomerDto customer) {
		this.customer = customer;
	}

	public CustomerDto getBillTo() {
		return this.billTo;
	}

	public void setBillTo(CustomerDto billTo) {
		this.billTo = billTo;
	}

	public List<ShipToDto> getShipTos() {
		return this.shipTos;
	}

	public void setShipTos(List<ShipToDto> shipTos) {
		this.shipTos = shipTos;
	}

	public void addToShipTo(ShipToDto dto) {
		if (this.getShipTos() == null) {
			this.shipTos = new ArrayList<>();
		}
		this.shipTos.add(dto);
	}

	public List<ContractPriceDto> getPrices() {
		return this.prices;
	}

	public void setPrices(List<ContractPriceDto> prices) {
		this.prices = prices;
	}
}
