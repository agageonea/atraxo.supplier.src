/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TransmissionStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class TransmissionStatusConverter
		implements
			AttributeConverter<TransmissionStatus, String> {

	@Override
	public String convertToDatabaseColumn(TransmissionStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null TransmissionStatus.");
		}
		return value.getName();
	}

	@Override
	public TransmissionStatus convertToEntityAttribute(String value) {
		return TransmissionStatus.getByName(value);
	}

}
