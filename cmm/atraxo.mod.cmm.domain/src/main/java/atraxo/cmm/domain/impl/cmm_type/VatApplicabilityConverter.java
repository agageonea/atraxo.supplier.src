/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link VatApplicability} enum.
 * Generated code. Do not modify in this file.
 */
public class VatApplicabilityConverter
		implements
			AttributeConverter<VatApplicability, String> {

	@Override
	public String convertToDatabaseColumn(VatApplicability value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null VatApplicability.");
		}
		return value.getName();
	}

	@Override
	public VatApplicability convertToEntityAttribute(String value) {
		return VatApplicability.getByName(value);
	}

}
