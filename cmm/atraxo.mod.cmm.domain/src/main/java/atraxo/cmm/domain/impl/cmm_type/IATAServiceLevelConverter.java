/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link IATAServiceLevel} enum.
 * Generated code. Do not modify in this file.
 */
public class IATAServiceLevelConverter
		implements
			AttributeConverter<IATAServiceLevel, String> {

	@Override
	public String convertToDatabaseColumn(IATAServiceLevel value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null IATAServiceLevel.");
		}
		return value.getName();
	}

	@Override
	public IATAServiceLevel convertToEntityAttribute(String value) {
		return IATAServiceLevel.getByName(value);
	}

}
