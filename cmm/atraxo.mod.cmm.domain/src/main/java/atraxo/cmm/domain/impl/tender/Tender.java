/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.tender;

import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatusConverter;
import atraxo.cmm.domain.impl.cmm_type.TenderSource;
import atraxo.cmm.domain.impl.cmm_type.TenderSourceConverter;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.domain.impl.cmm_type.TenderStatusConverter;
import atraxo.cmm.domain.impl.cmm_type.TenderType;
import atraxo.cmm.domain.impl.cmm_type.TenderTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatusConverter;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.domain.impl.abstracts.AbstractSubsidiary;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.TenderVolumeOffer;
import atraxo.fmbas.domain.impl.fmbas_type.TenderVolumeOfferConverter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Tender} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Tender.NQ_FIND_BY_CODE, query = "SELECT e FROM Tender e WHERE e.clientId = :clientId and e.code = :code and e.tenderVersion = :tenderVersion and e.holder = :holder and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Tender.NQ_FIND_BY_CODE_PRIMITIVE, query = "SELECT e FROM Tender e WHERE e.clientId = :clientId and e.code = :code and e.tenderVersion = :tenderVersion and e.holder.id = :holderId and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Tender.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Tender e WHERE e.clientId = :clientId and e.id = :id and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Tender.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Tender.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "code", "tender_version",
		"holder_id"})})
public class Tender extends AbstractSubsidiary implements Serializable {

	public static final String TABLE_NAME = "CMM_TENDER";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "Tender.findByCode";
	/**
	 * Named query find by unique key: Code using the ID field for references.
	 */
	public static final String NQ_FIND_BY_CODE_PRIMITIVE = "Tender.findByCode_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Tender.findByBusiness";

	@Column(name = "code", length = 20)
	private String code;

	@Column(name = "name", length = 100)
	private String name;

	@NotNull
	@Column(name = "tender_version", nullable = false, precision = 6)
	private Long tenderVersion;

	@Column(name = "type", length = 32)
	@Convert(converter = TenderTypeConverter.class)
	private TenderType type;

	@NotBlank
	@Column(name = "contactperson", nullable = false, length = 200)
	private String contactPerson;

	@NotBlank
	@Column(name = "email", nullable = false, length = 100)
	private String email;

	@Column(name = "phone", length = 50)
	private String phone;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "bidding_period_from", nullable = false)
	private Date biddingPeriodFrom;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "bidding_period_to", nullable = false)
	private Date biddingPeriodTo;

	@Column(name = "comments", length = 2500)
	private String comments;

	@NotBlank
	@Column(name = "status", nullable = false, length = 32)
	@Convert(converter = TenderStatusConverter.class)
	private TenderStatus status;

	@NotBlank
	@Column(name = "bidding_status", nullable = false, length = 32)
	@Convert(converter = BiddingStatusConverter.class)
	private BiddingStatus biddingStatus;

	@NotBlank
	@Column(name = "transmission_status", nullable = false, length = 32)
	@Convert(converter = TransmissionStatusConverter.class)
	private TransmissionStatus transmissionStatus;

	@NotBlank
	@Column(name = "source", nullable = false, length = 32)
	@Convert(converter = TenderSourceConverter.class)
	private TenderSource source;

	@NotNull
	@Column(name = "closed", nullable = false)
	private Boolean closed;

	@Temporal(TemporalType.DATE)
	@Column(name = "canceled_on")
	private Date canceledOn;

	@Temporal(TemporalType.DATE)
	@Column(name = "updated_on")
	private Date updatedOn;

	@Temporal(TemporalType.DATE)
	@Column(name = "published_on")
	private Date publishedOn;

	@Column(name = "msg_id", precision = 20)
	private Long msgId;

	@Column(name = "no_of_rounds", precision = 2)
	private Integer noOfRounds;

	@Column(name = "volume_offer", length = 32)
	@Convert(converter = TenderVolumeOfferConverter.class)
	private TenderVolumeOffer volumeOffer;

	@Column(name = "schema_version", length = 10)
	private String schemaVersion;

	@Column(name = "is_read")
	private Boolean isRead;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "holder_id", referencedColumnName = "id")
	private Customer holder;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contacts.class)
	@JoinColumn(name = "contact_id", referencedColumnName = "id")
	private Contacts contact;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = TenderLocation.class, mappedBy = "tender", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<TenderLocation> tenderLocation;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getTenderVersion() {
		return this.tenderVersion;
	}

	public void setTenderVersion(Long tenderVersion) {
		this.tenderVersion = tenderVersion;
	}

	public TenderType getType() {
		return this.type;
	}

	public void setType(TenderType type) {
		this.type = type;
	}

	public String getContactPerson() {
		return this.contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getBiddingPeriodFrom() {
		return this.biddingPeriodFrom;
	}

	public void setBiddingPeriodFrom(Date biddingPeriodFrom) {
		this.biddingPeriodFrom = biddingPeriodFrom;
	}

	public Date getBiddingPeriodTo() {
		return this.biddingPeriodTo;
	}

	public void setBiddingPeriodTo(Date biddingPeriodTo) {
		this.biddingPeriodTo = biddingPeriodTo;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public TenderStatus getStatus() {
		return this.status;
	}

	public void setStatus(TenderStatus status) {
		this.status = status;
	}

	public BiddingStatus getBiddingStatus() {
		return this.biddingStatus;
	}

	public void setBiddingStatus(BiddingStatus biddingStatus) {
		this.biddingStatus = biddingStatus;
	}

	public TransmissionStatus getTransmissionStatus() {
		return this.transmissionStatus;
	}

	public void setTransmissionStatus(TransmissionStatus transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public TenderSource getSource() {
		return this.source;
	}

	public void setSource(TenderSource source) {
		this.source = source;
	}

	public Boolean getClosed() {
		return this.closed;
	}

	public void setClosed(Boolean closed) {
		this.closed = closed;
	}

	public Date getCanceledOn() {
		return this.canceledOn;
	}

	public void setCanceledOn(Date canceledOn) {
		this.canceledOn = canceledOn;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Date getPublishedOn() {
		return this.publishedOn;
	}

	public void setPublishedOn(Date publishedOn) {
		this.publishedOn = publishedOn;
	}

	public Long getMsgId() {
		return this.msgId;
	}

	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}

	public Integer getNoOfRounds() {
		return this.noOfRounds;
	}

	public void setNoOfRounds(Integer noOfRounds) {
		this.noOfRounds = noOfRounds;
	}

	public TenderVolumeOffer getVolumeOffer() {
		return this.volumeOffer;
	}

	public void setVolumeOffer(TenderVolumeOffer volumeOffer) {
		this.volumeOffer = volumeOffer;
	}

	public String getSchemaVersion() {
		return this.schemaVersion;
	}

	public void setSchemaVersion(String schemaVersion) {
		this.schemaVersion = schemaVersion;
	}

	public Boolean getIsRead() {
		return this.isRead;
	}

	public void setIsRead(Boolean isRead) {
		this.isRead = isRead;
	}

	public Customer getHolder() {
		return this.holder;
	}

	public void setHolder(Customer holder) {
		if (holder != null) {
			this.__validate_client_context__(holder.getClientId());
		}
		this.holder = holder;
	}
	public Contacts getContact() {
		return this.contact;
	}

	public void setContact(Contacts contact) {
		if (contact != null) {
			this.__validate_client_context__(contact.getClientId());
		}
		this.contact = contact;
	}

	public Collection<TenderLocation> getTenderLocation() {
		return this.tenderLocation;
	}

	public void setTenderLocation(Collection<TenderLocation> tenderLocation) {
		this.tenderLocation = tenderLocation;
	}

	/**
	 * @param e
	 */
	public void addToTenderLocation(TenderLocation e) {
		if (this.tenderLocation == null) {
			this.tenderLocation = new ArrayList<>();
		}
		e.setTender(this);
		this.tenderLocation.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.closed == null) {
			this.closed = new Boolean(false);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.code == null) ? 0 : this.code.hashCode());
		result = prime
				* result
				+ ((this.tenderVersion == null) ? 0 : this.tenderVersion
						.hashCode());
		result = prime * result
				+ ((this.holder == null) ? 0 : this.holder.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Tender other = (Tender) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.code == null) {
			if (other.code != null) {
				return false;
			}
		} else if (!this.code.equals(other.code)) {
			return false;
		}
		if (this.tenderVersion == null) {
			if (other.tenderVersion != null) {
				return false;
			}
		} else if (!this.tenderVersion.equals(other.tenderVersion)) {
			return false;
		}
		if (this.holder == null) {
			if (other.holder != null) {
				return false;
			}
		} else if (!this.holder.equals(other.holder)) {
			return false;
		}
		return true;
	}
}
