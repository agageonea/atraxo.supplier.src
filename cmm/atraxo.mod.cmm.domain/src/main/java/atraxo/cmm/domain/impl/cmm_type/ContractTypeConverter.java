/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ContractType} enum.
 * Generated code. Do not modify in this file.
 */
public class ContractTypeConverter
		implements
			AttributeConverter<ContractType, String> {

	@Override
	public String convertToDatabaseColumn(ContractType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ContractType.");
		}
		return value.getName();
	}

	@Override
	public ContractType convertToEntityAttribute(String value) {
		return ContractType.getByName(value);
	}

}
