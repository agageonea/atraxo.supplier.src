/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link Period} enum.
 * Generated code. Do not modify in this file.
 */
public class PeriodConverter implements AttributeConverter<Period, String> {

	@Override
	public String convertToDatabaseColumn(Period value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null Period.");
		}
		return value.getName();
	}

	@Override
	public Period convertToEntityAttribute(String value) {
		return Period.getByName(value);
	}

}
