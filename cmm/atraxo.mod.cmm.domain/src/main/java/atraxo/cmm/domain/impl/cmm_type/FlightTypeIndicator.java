/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FlightTypeIndicator {

	_EMPTY_(""), _UNSPECIFIED_("Unspecified"), _DOMESTIC_("Domestic"), _INTERNATIONAL_(
			"International");

	private String name;

	private FlightTypeIndicator(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FlightTypeIndicator getByName(String name) {
		for (FlightTypeIndicator status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FlightTypeIndicator with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
