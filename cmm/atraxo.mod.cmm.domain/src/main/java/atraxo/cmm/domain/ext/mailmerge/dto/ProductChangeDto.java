package atraxo.cmm.domain.ext.mailmerge.dto;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;

public class ProductChangeDto extends AbstractDto {

	/**
	 *
	 */
	private static final long serialVersionUID = -4513686273276368660L;

	private String changes;

	public ProductChangeDto(String message) {
		this.changes = message;
	}

	public String getChanges() {
		return this.changes;
	}

	public void setChanges(String changes) {
		this.changes = changes;
	}

}
