/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link BiddingStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class BiddingStatusConverter
		implements
			AttributeConverter<BiddingStatus, String> {

	@Override
	public String convertToDatabaseColumn(BiddingStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null BiddingStatus.");
		}
		return value.getName();
	}

	@Override
	public BiddingStatus convertToEntityAttribute(String value) {
		return BiddingStatus.getByName(value);
	}

}
