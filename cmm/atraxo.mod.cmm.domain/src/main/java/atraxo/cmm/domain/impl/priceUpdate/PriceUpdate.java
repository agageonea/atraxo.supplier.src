/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.priceUpdate;

import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatusConverter;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdateCategories;
import atraxo.fmbas.domain.impl.abstracts.AbstractSubsidiary;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link PriceUpdate} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = PriceUpdate.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM PriceUpdate e WHERE e.clientId = :clientId and e.id = :id and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = PriceUpdate.TABLE_NAME)
public class PriceUpdate extends AbstractSubsidiary implements Serializable {

	public static final String TABLE_NAME = "CMM_PRICE_UPDATE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "PriceUpdate.findByBusiness";

	@Column(name = "reason", length = 255)
	private String reason;

	@Column(name = "new_price", precision = 19, scale = 6)
	private BigDecimal newPrice;

	@Temporal(TemporalType.DATE)
	@Column(name = "valid_from")
	private Date validFrom;

	@Column(name = "approval_status", length = 32)
	@Convert(converter = BidApprovalStatusConverter.class)
	private BidApprovalStatus approvalStatus;

	@Column(name = "published")
	private Boolean published;

	@Temporal(TemporalType.DATE)
	@Column(name = "published_on")
	private Date publishedOn;

	@Temporal(TemporalType.DATE)
	@Column(name = "submitted_on")
	private Date submittedOn;

	@Temporal(TemporalType.DATE)
	@Column(name = "approved_on")
	private Date approvedOn;

	@Column(name = "deal_type", length = 255)
	private String dealType;

	@Column(name = "contract_status", length = 255)
	private String contractStatus;

	@Column(name = "contract_type", length = 255)
	private String contractType;

	@Column(name = "delivery_point", length = 255)
	private String deliveryPoint;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = PriceCategory.class)
	@JoinColumn(name = "price_category_id", referencedColumnName = "id")
	private PriceCategory priceCategory;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "currency_id", referencedColumnName = "id")
	private Currencies currency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_id", referencedColumnName = "id")
	private Unit unit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = UserSupp.class)
	@JoinColumn(name = "published_by_id", referencedColumnName = "id")
	private UserSupp publishedBy;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = UserSupp.class)
	@JoinColumn(name = "submitted_by_id", referencedColumnName = "id")
	private UserSupp submittedBy;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = UserSupp.class)
	@JoinColumn(name = "approved_by_id", referencedColumnName = "id")
	private UserSupp approvedBy;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "location_id", referencedColumnName = "id")
	private Locations location;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "contract_holder_id", referencedColumnName = "id")
	private Customer contractHolder;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "counterparty_id", referencedColumnName = "id")
	private Customer counterparty;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "bill_to_id", referencedColumnName = "id")
	private Customer billTo;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "ship_to_id", referencedColumnName = "id")
	private Customer shipTo;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = PriceUpdateCategories.class, mappedBy = "priceUpdate", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<PriceUpdateCategories> priceUpdateCategories;

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getNewPrice() {
		return this.newPrice;
	}

	public void setNewPrice(BigDecimal newPrice) {
		this.newPrice = newPrice;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public BidApprovalStatus getApprovalStatus() {
		return this.approvalStatus;
	}

	public void setApprovalStatus(BidApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public Boolean getPublished() {
		return this.published;
	}

	public void setPublished(Boolean published) {
		this.published = published;
	}

	public Date getPublishedOn() {
		return this.publishedOn;
	}

	public void setPublishedOn(Date publishedOn) {
		this.publishedOn = publishedOn;
	}

	public Date getSubmittedOn() {
		return this.submittedOn;
	}

	public void setSubmittedOn(Date submittedOn) {
		this.submittedOn = submittedOn;
	}

	public Date getApprovedOn() {
		return this.approvedOn;
	}

	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}

	public String getDealType() {
		return this.dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getContractStatus() {
		return this.contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

	public String getContractType() {
		return this.contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getDeliveryPoint() {
		return this.deliveryPoint;
	}

	public void setDeliveryPoint(String deliveryPoint) {
		this.deliveryPoint = deliveryPoint;
	}

	public PriceCategory getPriceCategory() {
		return this.priceCategory;
	}

	public void setPriceCategory(PriceCategory priceCategory) {
		if (priceCategory != null) {
			this.__validate_client_context__(priceCategory.getClientId());
		}
		this.priceCategory = priceCategory;
	}
	public Currencies getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currencies currency) {
		if (currency != null) {
			this.__validate_client_context__(currency.getClientId());
		}
		this.currency = currency;
	}
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		if (unit != null) {
			this.__validate_client_context__(unit.getClientId());
		}
		this.unit = unit;
	}
	public UserSupp getPublishedBy() {
		return this.publishedBy;
	}

	public void setPublishedBy(UserSupp publishedBy) {
		if (publishedBy != null) {
			this.__validate_client_context__(publishedBy.getClientId());
		}
		this.publishedBy = publishedBy;
	}
	public UserSupp getSubmittedBy() {
		return this.submittedBy;
	}

	public void setSubmittedBy(UserSupp submittedBy) {
		if (submittedBy != null) {
			this.__validate_client_context__(submittedBy.getClientId());
		}
		this.submittedBy = submittedBy;
	}
	public UserSupp getApprovedBy() {
		return this.approvedBy;
	}

	public void setApprovedBy(UserSupp approvedBy) {
		if (approvedBy != null) {
			this.__validate_client_context__(approvedBy.getClientId());
		}
		this.approvedBy = approvedBy;
	}
	public Locations getLocation() {
		return this.location;
	}

	public void setLocation(Locations location) {
		if (location != null) {
			this.__validate_client_context__(location.getClientId());
		}
		this.location = location;
	}
	public Customer getContractHolder() {
		return this.contractHolder;
	}

	public void setContractHolder(Customer contractHolder) {
		if (contractHolder != null) {
			this.__validate_client_context__(contractHolder.getClientId());
		}
		this.contractHolder = contractHolder;
	}
	public Customer getCounterparty() {
		return this.counterparty;
	}

	public void setCounterparty(Customer counterparty) {
		if (counterparty != null) {
			this.__validate_client_context__(counterparty.getClientId());
		}
		this.counterparty = counterparty;
	}
	public Customer getBillTo() {
		return this.billTo;
	}

	public void setBillTo(Customer billTo) {
		if (billTo != null) {
			this.__validate_client_context__(billTo.getClientId());
		}
		this.billTo = billTo;
	}
	public Customer getShipTo() {
		return this.shipTo;
	}

	public void setShipTo(Customer shipTo) {
		if (shipTo != null) {
			this.__validate_client_context__(shipTo.getClientId());
		}
		this.shipTo = shipTo;
	}

	public Collection<PriceUpdateCategories> getPriceUpdateCategories() {
		return this.priceUpdateCategories;
	}

	public void setPriceUpdateCategories(
			Collection<PriceUpdateCategories> priceUpdateCategories) {
		this.priceUpdateCategories = priceUpdateCategories;
	}

	/**
	 * @param e
	 */
	public void addToPriceUpdateCategories(PriceUpdateCategories e) {
		if (this.priceUpdateCategories == null) {
			this.priceUpdateCategories = new ArrayList<>();
		}
		e.setPriceUpdate(this);
		this.priceUpdateCategories.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
