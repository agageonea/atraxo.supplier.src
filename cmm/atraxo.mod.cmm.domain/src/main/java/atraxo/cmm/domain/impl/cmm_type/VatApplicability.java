/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum VatApplicability {

	_EMPTY_("", ""), _ALL_EVENTS_("All events", "ANY"), _DOMESTIC_EVENTS_(
			"Domestic events", "DOM"), _INTERNATIONAL_EVENTS_(
			"International events", "INT"), _NOT_APPLICABLE_("Not applicable",
			"NA");

	private String name;
	private String code;

	private VatApplicability(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static VatApplicability getByName(String name) {
		for (VatApplicability status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent VatApplicability with name: " + name);
	}

	public String getCode() {
		return this.code;
	}

	public static VatApplicability getByCode(String code) {
		for (VatApplicability status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent VatApplicability with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
