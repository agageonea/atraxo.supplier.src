package atraxo.cmm.domain.ext.mailmerge.dto;

import java.util.Date;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;

/**
 * Email DTO used for the Contract approval process
 *
 * @author vhojda
 */
public class ContractApprovalDto extends AbstractDto {

	/**
	 *
	 */
	private static final long serialVersionUID = 6659358206331541053L;

	private String title;
	private String fullName;

	private String fullnameOfRequester;

	private String contractCode;
	private String contractHolder;
	private String supplierName;
	private String customerName;
	private Date validFrom;
	private Date validTo;
	private String contractLocation;

	private String approvalRequestNote;

	private String approverNameFirstLevel;
	private String approvalNoteFirstLevel;

	private String approverNameSecondLevel;
	private String approvalNoteSecondLevel;

	private String approverNameThirdLevel;
	private String approvalNoteThirdLevel;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFullnameOfRequester() {
		return this.fullnameOfRequester;
	}

	public void setFullnameOfRequester(String fullnameOfRequester) {
		this.fullnameOfRequester = fullnameOfRequester;
	}

	public String getContractCode() {
		return this.contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public String getContractHolder() {
		return this.contractHolder;
	}

	public void setContractHolder(String contractHolder) {
		this.contractHolder = contractHolder;
	}

	public String getSupplierName() {
		return this.supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public String getContractLocation() {
		return this.contractLocation;
	}

	public void setContractLocation(String contractLocation) {
		this.contractLocation = contractLocation;
	}

	public String getApprovalRequestNote() {
		return this.approvalRequestNote;
	}

	public void setApprovalRequestNote(String approvalRequestNote) {
		this.approvalRequestNote = approvalRequestNote;
	}

	public String getApproverNameFirstLevel() {
		return this.approverNameFirstLevel;
	}

	public void setApproverNameFirstLevel(String approverNameFirstLevel) {
		this.approverNameFirstLevel = approverNameFirstLevel;
	}

	public String getApprovalNoteFirstLevel() {
		return this.approvalNoteFirstLevel;
	}

	public void setApprovalNoteFirstLevel(String approvalNoteFirstLevel) {
		this.approvalNoteFirstLevel = approvalNoteFirstLevel;
	}

	public String getApproverNameSecondLevel() {
		return this.approverNameSecondLevel;
	}

	public void setApproverNameSecondLevel(String approverNameSecondLevel) {
		this.approverNameSecondLevel = approverNameSecondLevel;
	}

	public String getApprovalNoteSecondLevel() {
		return this.approvalNoteSecondLevel;
	}

	public void setApprovalNoteSecondLevel(String approvalNoteSecondLevel) {
		this.approvalNoteSecondLevel = approvalNoteSecondLevel;
	}

	public String getApproverNameThirdLevel() {
		return this.approverNameThirdLevel;
	}

	public void setApproverNameThirdLevel(String approverNameThirdLevel) {
		this.approverNameThirdLevel = approverNameThirdLevel;
	}

	public String getApprovalNoteThirdLevel() {
		return this.approvalNoteThirdLevel;
	}

	public void setApprovalNoteThirdLevel(String approvalNoteThirdLevel) {
		this.approvalNoteThirdLevel = approvalNoteThirdLevel;
	}

}
