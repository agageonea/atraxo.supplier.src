package atraxo.cmm.domain.ext.tender.converter;

public enum ErrorCode {

	INVALID_CONTENT,
	INTERNAL_ERROR;
	
}
