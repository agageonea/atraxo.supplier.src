/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link PendingAction} enum.
 * Generated code. Do not modify in this file.
 */
public class PendingActionConverter
		implements
			AttributeConverter<PendingAction, String> {

	@Override
	public String convertToDatabaseColumn(PendingAction value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null PendingAction.");
		}
		return value.getName();
	}

	@Override
	public PendingAction convertToEntityAttribute(String value) {
		return PendingAction.getByName(value);
	}

}
