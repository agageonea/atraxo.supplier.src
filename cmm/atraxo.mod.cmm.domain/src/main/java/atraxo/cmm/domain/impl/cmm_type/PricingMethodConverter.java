/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link PricingMethod} enum.
 * Generated code. Do not modify in this file.
 */
public class PricingMethodConverter
		implements
			AttributeConverter<PricingMethod, String> {

	@Override
	public String convertToDatabaseColumn(PricingMethod value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null PricingMethod.");
		}
		return value.getName();
	}

	@Override
	public PricingMethod convertToEntityAttribute(String value) {
		return PricingMethod.getByName(value);
	}

}
