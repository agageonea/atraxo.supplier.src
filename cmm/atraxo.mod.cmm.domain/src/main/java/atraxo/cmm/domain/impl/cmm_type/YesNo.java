/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum YesNo {

	_EMPTY_("", ""), _True_("Yes", "Y"), _False_("No", "N");

	private String name;
	private String code;

	private YesNo(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static YesNo getByName(String name) {
		for (YesNo status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent YesNo with name: " + name);
	}

	public String getCode() {
		return this.code;
	}

	public static YesNo getByCode(String code) {
		for (YesNo status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent YesNo with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
