/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.contracts;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link ContractTemplate} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = ContractTemplate.NQ_FIND_BY_TEMPLATES, query = "SELECT e FROM ContractTemplate e WHERE e.clientId = :clientId and e.contractType = :contractType and e.deliverySubtype = :deliverySubtype", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ContractTemplate.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ContractTemplate e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ContractTemplate.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = ContractTemplate.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "contract_type",
		"delivery_subtype"})})
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class ContractTemplate extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "CMM_TEMPLATES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Templates.
	 */
	public static final String NQ_FIND_BY_TEMPLATES = "ContractTemplate.findByTemplates";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ContractTemplate.findByBusiness";

	@Column(name = "contract_type", length = 32)
	private String contractType;

	@Column(name = "delivery_subtype", length = 32)
	private String deliverySubtype;

	@Column(name = "scope", length = 32)
	private String scope;

	@Column(name = "dialog", length = 32)
	private String dialog;

	public String getContractType() {
		return this.contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getDeliverySubtype() {
		return this.deliverySubtype;
	}

	public void setDeliverySubtype(String deliverySubtype) {
		this.deliverySubtype = deliverySubtype;
	}

	public String getScope() {
		return this.scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getDialog() {
		return this.dialog;
	}

	public void setDialog(String dialog) {
		this.dialog = dialog;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
