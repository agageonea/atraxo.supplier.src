package atraxo.cmm.domain.ext.mailmerge.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import atraxo.cmm.domain.ext.mailmerge.dto.contract.ContractPriceDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.CustomerDto;

public class BidDto extends AbstractDto {

	private CustomerDto holder;
	private TenderDto tender;
	private String location;
	private Integer revision;
	private String version;
	private Date validFrom;
	private Date validTo;
	private Date contractValidFrom;
	private Date contractValidTo;
	private String contactFullName;
	private Integer iataServiceLevel;
	private String tax;
	private String titleTransfer;
	private String astmSpecification;
	private String operatingHours;
	private String intoPlaneAgentName;
	private String product;
	private BigDecimal offeredVolume;
	private String offeredVolumeUnit;
	private String contractedVolumePeriod;
	private Integer volumeTolerance;
	private String quantityType;
	private String settlementCurrency;
	private String settlementUnit;
	private Integer paymentTerms;
	private String paymentReferenceDay;
	private String invoiceFrequency;
	private String paymentType;
	private String invoiceType;
	private String financialSource;
	private String averageMethod;
	private String exchangeRateOffset;
	private String creditTerms;
	private Boolean bankGuarantee;
	private Integer prepaidDays;
	private String paymentFrequency;
	private BigDecimal prepaidAmount;
	private Integer prepayFirstDeliveryDate;
	private String paymentComment;

	private List<ShipToDto> shipTo;
	private List<ContractPriceDto> prices;

	public String getOfferedVolumeUnit() {
		return this.offeredVolumeUnit;
	}

	public void setOfferedVolumeUnit(String offeredVolumeUnit) {
		this.offeredVolumeUnit = offeredVolumeUnit;
	}

	public String getContractedVolumePeriod() {
		return this.contractedVolumePeriod;
	}

	public void setContractedVolumePeriod(String contractedVolumePeriod) {
		this.contractedVolumePeriod = contractedVolumePeriod;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public TenderDto getTender() {
		return this.tender;
	}

	public void setTender(TenderDto tender) {
		this.tender = tender;
	}

	public String getPaymentFrequency() {
		return this.paymentFrequency;
	}

	public void setPaymentFrequency(String paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}

	public CustomerDto getHolder() {
		return this.holder;
	}

	public void setHolder(CustomerDto holder) {
		this.holder = holder;
	}

	public Integer getRevision() {
		return this.revision;
	}

	public void setRevision(Integer revision) {
		this.revision = revision;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Date getContractValidFrom() {
		return this.contractValidFrom;
	}

	public void setContractValidFrom(Date contractValidFrom) {
		this.contractValidFrom = contractValidFrom;
	}

	public Date getContractValidTo() {
		return this.contractValidTo;
	}

	public void setContractValidTo(Date contractValidTo) {
		this.contractValidTo = contractValidTo;
	}

	public String getContactFullName() {
		return this.contactFullName;
	}

	public void setContactFullName(String contactFullName) {
		this.contactFullName = contactFullName;
	}

	public Integer getIataServiceLevel() {
		return this.iataServiceLevel;
	}

	public void setIataServiceLevel(Integer iataServiceLevel) {
		this.iataServiceLevel = iataServiceLevel;
	}

	public String getTax() {
		return this.tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getTitleTransfer() {
		return this.titleTransfer;
	}

	public void setTitleTransfer(String titleTransfer) {
		this.titleTransfer = titleTransfer;
	}

	public String getAstmSpecification() {
		return this.astmSpecification;
	}

	public void setAstmSpecification(String astmSpecification) {
		this.astmSpecification = astmSpecification;
	}

	public String getOperatingHours() {
		return this.operatingHours;
	}

	public void setOperatingHours(String operatingHours) {
		this.operatingHours = operatingHours;
	}

	public String getIntoPlaneAgentName() {
		return this.intoPlaneAgentName;
	}

	public void setIntoPlaneAgentName(String intoPlaneAgentName) {
		this.intoPlaneAgentName = intoPlaneAgentName;
	}

	public String getProduct() {
		return this.product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public BigDecimal getOfferedVolume() {
		return this.offeredVolume;
	}

	public void setOfferedVolume(BigDecimal offeredVolume) {
		this.offeredVolume = offeredVolume;
	}

	public Integer getVolumeTolerance() {
		return this.volumeTolerance;
	}

	public void setVolumeTolerance(Integer volumeTolerance) {
		this.volumeTolerance = volumeTolerance;
	}

	public String getQuantityType() {
		return this.quantityType;
	}

	public void setQuantityType(String quantityType) {
		this.quantityType = quantityType;
	}

	public String getSettlementCurrency() {
		return this.settlementCurrency;
	}

	public void setSettlementCurrency(String settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}

	public String getSettlementUnit() {
		return this.settlementUnit;
	}

	public void setSettlementUnit(String settlementUnit) {
		this.settlementUnit = settlementUnit;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getPaymentReferenceDay() {
		return this.paymentReferenceDay;
	}

	public void setPaymentReferenceDay(String paymentReferenceDay) {
		this.paymentReferenceDay = paymentReferenceDay;
	}

	public String getInvoiceFrequency() {
		return this.invoiceFrequency;
	}

	public void setInvoiceFrequency(String invoiceFrequency) {
		this.invoiceFrequency = invoiceFrequency;
	}

	public String getPaymentType() {
		return this.paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getInvoiceType() {
		return this.invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getFinancialSource() {
		return this.financialSource;
	}

	public void setFinancialSource(String financialSourceName) {
		this.financialSource = financialSourceName;
	}

	public String getAverageMethod() {
		return this.averageMethod;
	}

	public void setAverageMethod(String averageMethodName) {
		this.averageMethod = averageMethodName;
	}

	public String getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(String exchangeRateOffset) {
		this.exchangeRateOffset = exchangeRateOffset;
	}

	public String getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	public Boolean getBankGuarantee() {
		return this.bankGuarantee;
	}

	public void setBankGuarantee(Boolean bankGuarantee) {
		this.bankGuarantee = bankGuarantee;
	}

	public Integer getPrepaidDays() {
		return this.prepaidDays;
	}

	public void setPrepaidDays(Integer prepaidDays) {
		this.prepaidDays = prepaidDays;
	}

	public String getPayementFreq() {
		return this.paymentFrequency;
	}

	public void setPayementFreq(String payementFreq) {
		this.paymentFrequency = payementFreq;
	}

	public BigDecimal getPrepaidAmount() {
		return this.prepaidAmount;
	}

	public void setPrepaidAmount(BigDecimal prepaidAmount) {
		this.prepaidAmount = prepaidAmount;
	}

	public Integer getPrepayFirstDeliveryDate() {
		return this.prepayFirstDeliveryDate;
	}

	public void setPrepayFirstDeliveryDate(Integer prepayFirstDeliveryDate) {
		this.prepayFirstDeliveryDate = prepayFirstDeliveryDate;
	}

	public String getPaymentComment() {
		return this.paymentComment;
	}

	public void setPaymentComment(String paymentComment) {
		this.paymentComment = paymentComment;
	}

	public List<ShipToDto> getShipTo() {
		return this.shipTo;
	}

	public void setShipTo(List<ShipToDto> shipTo) {
		this.shipTo = shipTo;
	}

	public List<ContractPriceDto> getPrices() {
		return this.prices;
	}

	public void setPrices(List<ContractPriceDto> prices) {
		this.prices = prices;
	}
}
