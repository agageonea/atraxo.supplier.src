/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.contracts;

import atraxo.cmm.domain.impl.cmm_type.QuantityType;
import atraxo.cmm.domain.impl.cmm_type.VatApplicability;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriodConverter;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.fmbas_type.OperatorConverter;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link PricingBase} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = PricingBase.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM PricingBase e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = PricingBase.TABLE_NAME)
public class PricingBase extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "CMM_CONTR_PRICES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "PricingBase.findByBusiness";

	@Temporal(TemporalType.DATE)
	@Column(name = "valid_from")
	private Date validFrom;

	@Temporal(TemporalType.DATE)
	@Column(name = "valid_to")
	private Date validTo;

	@Column(name = "quotation_offset", length = 32)
	@Convert(converter = MasterAgreementsPeriodConverter.class)
	private MasterAgreementsPeriod quotationOffset;

	@Column(name = "operator", length = 32)
	@Convert(converter = OperatorConverter.class)
	private Operator operator;

	@Column(name = "conversion_factor", precision = 19, scale = 6)
	private BigDecimal factor;

	@Column(name = "decimals", precision = 3)
	private Integer decimals;

	@Column(name = "description", length = 100)
	private String description;

	@Transient
	private BigDecimal initialPrice;

	@Transient
	private Date initialPriceValidFrom;

	@Transient
	private Date initialPriceValidTo;

	@Transient
	private Integer initialCurrId;

	@Transient
	private Integer initialUnitId;

	@Transient
	private QuantityType quantityType;

	@Transient
	private Boolean continous;

	@Transient
	private Boolean defauftPC;

	@Transient
	private Boolean restriction;

	@Transient
	private Integer financialSourceId;

	@Transient
	private Integer averageMethodId;

	@Transient
	private MasterAgreementsPeriod exchangeRateOffset;

	@Transient
	private VatApplicability vat;

	@Transient
	private String priceCategoryIdsJson;

	@Transient
	private Integer orderIndex;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contract.class)
	@JoinColumn(name = "contract_id", referencedColumnName = "id")
	private Contract contract;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Quotation.class)
	@JoinColumn(name = "quotation_id", referencedColumnName = "id")
	private Quotation quotation;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "conversion_unit_id", referencedColumnName = "id")
	private Unit convUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AverageMethod.class)
	@JoinColumn(name = "average_method_id", referencedColumnName = "id")
	private AverageMethod avgMethod;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = PriceCategory.class)
	@JoinColumn(name = "price_category_id", referencedColumnName = "id")
	private PriceCategory priceCat;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = ContractPriceCategory.class, mappedBy = "pricingBases", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<ContractPriceCategory> contractPriceCategories;

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public MasterAgreementsPeriod getQuotationOffset() {
		return this.quotationOffset;
	}

	public void setQuotationOffset(MasterAgreementsPeriod quotationOffset) {
		this.quotationOffset = quotationOffset;
	}

	public Operator getOperator() {
		return this.operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public BigDecimal getFactor() {
		return this.factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

	public Integer getDecimals() {
		return this.decimals;
	}

	public void setDecimals(Integer decimals) {
		this.decimals = decimals;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getInitialPrice() {
		return this.initialPrice;
	}

	public void setInitialPrice(BigDecimal initialPrice) {
		this.initialPrice = initialPrice;
	}

	public Date getInitialPriceValidFrom() {
		return this.initialPriceValidFrom;
	}

	public void setInitialPriceValidFrom(Date initialPriceValidFrom) {
		this.initialPriceValidFrom = initialPriceValidFrom;
	}

	public Date getInitialPriceValidTo() {
		return this.initialPriceValidTo;
	}

	public void setInitialPriceValidTo(Date initialPriceValidTo) {
		this.initialPriceValidTo = initialPriceValidTo;
	}

	public Integer getInitialCurrId() {
		return this.initialCurrId;
	}

	public void setInitialCurrId(Integer initialCurrId) {
		this.initialCurrId = initialCurrId;
	}

	public Integer getInitialUnitId() {
		return this.initialUnitId;
	}

	public void setInitialUnitId(Integer initialUnitId) {
		this.initialUnitId = initialUnitId;
	}

	public QuantityType getQuantityType() {
		return this.quantityType;
	}

	public void setQuantityType(QuantityType quantityType) {
		this.quantityType = quantityType;
	}

	public Boolean getContinous() {
		return this.continous;
	}

	public void setContinous(Boolean continous) {
		this.continous = continous;
	}

	public Boolean getDefauftPC() {
		return this.defauftPC;
	}

	public void setDefauftPC(Boolean defauftPC) {
		this.defauftPC = defauftPC;
	}

	public Boolean getRestriction() {
		return this.restriction;
	}

	public void setRestriction(Boolean restriction) {
		this.restriction = restriction;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public Integer getAverageMethodId() {
		return this.averageMethodId;
	}

	public void setAverageMethodId(Integer averageMethodId) {
		this.averageMethodId = averageMethodId;
	}

	public MasterAgreementsPeriod getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(MasterAgreementsPeriod exchangeRateOffset) {
		this.exchangeRateOffset = exchangeRateOffset;
	}

	public VatApplicability getVat() {
		return this.vat;
	}

	public void setVat(VatApplicability vat) {
		this.vat = vat;
	}

	public String getPriceCategoryIdsJson() {
		return this.priceCategoryIdsJson;
	}

	public void setPriceCategoryIdsJson(String priceCategoryIdsJson) {
		this.priceCategoryIdsJson = priceCategoryIdsJson;
	}

	public Integer getOrderIndex() {
		return this.orderIndex;
	}

	public void setOrderIndex(Integer orderIndex) {
		this.orderIndex = orderIndex;
	}

	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		if (contract != null) {
			this.__validate_client_context__(contract.getClientId());
		}
		this.contract = contract;
	}
	public Quotation getQuotation() {
		return this.quotation;
	}

	public void setQuotation(Quotation quotation) {
		if (quotation != null) {
			this.__validate_client_context__(quotation.getClientId());
		}
		this.quotation = quotation;
	}
	public Unit getConvUnit() {
		return this.convUnit;
	}

	public void setConvUnit(Unit convUnit) {
		if (convUnit != null) {
			this.__validate_client_context__(convUnit.getClientId());
		}
		this.convUnit = convUnit;
	}
	public AverageMethod getAvgMethod() {
		return this.avgMethod;
	}

	public void setAvgMethod(AverageMethod avgMethod) {
		if (avgMethod != null) {
			this.__validate_client_context__(avgMethod.getClientId());
		}
		this.avgMethod = avgMethod;
	}
	public PriceCategory getPriceCat() {
		return this.priceCat;
	}

	public void setPriceCat(PriceCategory priceCat) {
		if (priceCat != null) {
			this.__validate_client_context__(priceCat.getClientId());
		}
		this.priceCat = priceCat;
	}

	public Collection<ContractPriceCategory> getContractPriceCategories() {
		return this.contractPriceCategories;
	}

	public void setContractPriceCategories(
			Collection<ContractPriceCategory> contractPriceCategories) {
		this.contractPriceCategories = contractPriceCategories;
	}

	/**
	 * @param e
	 */
	public void addToContractPriceCategories(ContractPriceCategory e) {
		if (this.contractPriceCategories == null) {
			this.contractPriceCategories = new ArrayList<>();
		}
		e.setPricingBases(this);
		this.contractPriceCategories.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
