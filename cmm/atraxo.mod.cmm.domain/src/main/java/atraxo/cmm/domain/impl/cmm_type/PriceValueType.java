/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.domain.impl.cmm_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum PriceValueType {

	_EMPTY_(""), _HIGH_PRICE_("High price"), _LOW_PRICE_("Low price"), _MEAN_PRICE_(
			"Mean price"), _USER_CALCULATED_PRICE_("User calculated price");

	private String name;

	private PriceValueType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static PriceValueType getByName(String name) {
		for (PriceValueType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent PriceValueType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
