package atraxo.cmm.business.ext.bid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.ext.bid.IBidService;
import atraxo.cmm.business.api.ext.datahub.IDataHubClientRequestService;
import atraxo.cmm.business.api.shipTo.IShipToService;
import atraxo.cmm.business.api.tender.ITenderLocationAirlinesService;
import atraxo.cmm.business.api.tender.ITenderLocationService;
import atraxo.cmm.business.ext.contracts.service.AbstractDataHubClientRequestService;
import atraxo.cmm.business.ext.contracts.service.CopyContractService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.utils.NewBidOption;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.TenderSource;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.business.ext.util.LambdaUtil;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBid;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderResponse;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderServiceService;

public class BidService extends AbstractDataHubClientRequestService<Contract>
		implements IDataHubClientRequestService<Contract, FuelTenderBid>, IBidService {
	private static final String DEFAULT_NEW_COPY_ACTION = "Created as copy";
	private static final String DEFAULT_NEW_REVISION_ACTION = "New revision";

	@Autowired
	private ISystemParameterService sysParamService;
	@Autowired
	private IContractService contractService;
	@Autowired
	private IShipToService shipToService;
	@Autowired
	private ITenderLocationService tenderLocationService;
	@Autowired
	private ITenderLocationAirlinesService tenderLocationAirlineService;
	@Autowired
	private CopyContractService copyContractService;

	@Override
	@Transactional
	@History(type = Contract.class, value = "Award Tender Bid")
	public void awardTenderBid(List<Contract> bids) throws BusinessException {
		for (Contract bid : bids) {
			bid.setBidStatus(BidStatus._AWARDED_);
			bid.setAwardedVolume(bid.getOfferedVolume());
			Collection<ShipTo> shipToList = bid.getShipTo();
			for (ShipTo shipToElement : shipToList) {
				shipToElement.setAllocatedVolume(shipToElement.getOfferedVolume());
			}
			this.contractService.update(bid);

			// Update tender location bidding status
			this.propagateBiddingStatus(bid, BidStatus._AWARDED_.getName(), "Bid Awarded : " + this.getBidIdentificationMessage(bid));
		}
	}

	@Override
	public Integer copySupplierBid(Contract sourceBid, Integer tenderLocationId, String copyBidOptionName, String reason) throws BusinessException {
		TenderLocation tenderLocation = this.tenderLocationService.findByBusiness(tenderLocationId);
		NewBidOption copyBidOption = NewBidOption.valueOf(copyBidOptionName);
		Contract newContract = this.copyContractService.copySupplierBid(sourceBid, tenderLocation, copyBidOption);

		if (newContract != null) {
			String objectValue = copyBidOption.equals(NewBidOption.NEW_REVISION) ? DEFAULT_NEW_REVISION_ACTION : DEFAULT_NEW_COPY_ACTION;
			String remark = reason != null ? reason : "";
			this.generateSaveToHistory(newContract, objectValue, remark, Contract.class);
			return newContract.getId();
		}
		throw new BusinessException(CmmErrorCode.BID_TENDER_COPY_BID_ERROR, CmmErrorCode.BID_TENDER_COPY_BID_ERROR.getErrMsg());

	}

	@Override
	@Transactional
	@History(type = Contract.class, value = "Decline Tender Bid")
	public void declineTenderBid(List<Contract> bids) throws BusinessException {
		for (Contract bid : bids) {
			bid.setBidStatus(BidStatus._DECLINED_);
			this.contractService.update(bid);

			// Update tender location bidding status
			this.propagateBiddingStatus(bid, BidStatus._AWARDED_.getName(), "Bid Declined : " + this.getBidIdentificationMessage(bid));
		}
	}

	/**
	 * @param contract
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public Integer copyBid(Contract contract) throws BusinessException {
		return this.copyContractService.copyBid(contract);
	}

	private String getBidIdentificationMessage(Contract bid) {
		StringBuilder bidIdentification = new StringBuilder();

		bidIdentification.append("Tender Name: ").append(bid.getBidTenderIdentification().getName()).append(" - ");
		bidIdentification.append("Tender Issuer Code: ").append(bid.getBidTenderIdentification().getHolder().getCode()).append(" - ");
		bidIdentification.append("Location Code: ").append(bid.getLocation().getCode()).append(" - ");
		bidIdentification.append("Bid Version: ").append(bid.getBidVersion());

		return bidIdentification.toString();
	}

	@Override
	public FuelTenderResponse submitRequest(FuelTenderServiceService client, FuelTenderBid dto) throws BusinessException {
		return client.getFuelTenderServicePort().fuelTenderBid(dto);
	}

	/**
	 * Update bids based on communication results.
	 *
	 * @param execResult
	 * @param list
	 * @param dtoClass
	 * @throws BusinessException
	 */
	@Override
	public void processDataHubBulkWsResult(WSExecutionResult execResult, Collection<Contract> list, Class<FuelTenderBid> dtoClass, String reason)
			throws BusinessException {
		// to be implemented when bulk export will be supported
	}

	/**
	 * Process and finalize the response from DataHub. Make the necessary adjustments to the entity.
	 *
	 * @param result
	 * @param dtoClass
	 * @param reason
	 * @throws BusinessException
	 */
	@Override
	public void processDataHubSingleWsResult(WSExecutionResult result, Contract entity, Class<FuelTenderBid> dtoClass, String reason)
			throws BusinessException {
		if (result != null) {
			Contract bid = this.contractService.findById(entity.getId());

			TransmissionStatus transmissionStatus = result.isSuccess() ? TransmissionStatus._TRANSMITTED_ : TransmissionStatus._FAILED_;
			bid.setBidTransmissionStatus(transmissionStatus);

			if (result.isSuccess()) {
				bid.setBidStatus(BidStatus._SUBMITTED_);
			}

			// update bidding status for the tender location
			this.propagateBiddingStatus(bid, BiddingStatus._IN_NEGOTIATION_.getName(),
					"Bid Submited to Issuer : " + this.getBidIdentificationMessage(entity));

			// Update bid status and transmission status
			this.contractService.updateWithoutBusinessLogic(Arrays.asList(bid));

			// Save publish action in history
			if (result.isSuccess()) {
				this.generateSaveToHistory(bid, bid.getBidStatus().getName(), reason, Contract.class);
			}
			
		}
	}

	/**
	 * Propagate Bidding Status to <code>TenderLocationAirlines</code> , <code>TenderLocation</code> , <code>Tender</code> based on the processed bid
	 * status
	 *
	 * @param processedBid
	 * @param action
	 * @param reason
	 * @throws BusinessException
	 */
	@Override
	public void propagateBiddingStatus(Contract processedBid, String action, String reason) throws BusinessException {
		List<Customer> customers = new ArrayList<>();
		for (ShipTo shipTo : processedBid.getShipTo()) {
			customers.add(shipTo.getCustomer());
		}

		boolean importedSubmited = TenderSource._IMPORTED_.equals(processedBid.getBidTenderIdentification().getSource())
				&& BidStatus._SUBMITTED_.equals(processedBid.getBidStatus());

		boolean capturedApproved = TenderSource._CAPTURED_.equals(processedBid.getBidTenderIdentification().getSource())
				&& BidApprovalStatus._APPROVED_.equals(processedBid.getBidApprovalStatus());

		// For submited or approved bids finish bidding status for all airlines
		if (importedSubmited || capturedApproved) {
			this.updateBiddingStatus(processedBid, processedBid.getShipTo(), BiddingStatus._IN_NEGOTIATION_, action, reason);
		}

		// For awarded bids finish bidding status for all airlines
		if (BidStatus._AWARDED_.equals(processedBid.getBidStatus())) {
			this.updateBiddingStatus(processedBid, processedBid.getShipTo(), BiddingStatus._FINISHED_, action, reason);
		}

		// Find all transmited bids
		if (BidStatus._DECLINED_.equals(processedBid.getBidStatus())) {
			List<Contract> transmitedBids = this.getTransmitedBidsForLocation(processedBid);

			List<ShipTo> transmitedShipTos = new ArrayList<>();
			if (!customers.isEmpty() && !transmitedBids.isEmpty()) {
				Map<String, Object> params = new HashMap<>();
				params.put("customer", customers);
				params.put("contract", transmitedBids);
				transmitedShipTos = this.shipToService.findEntitiesByAttributes(params);
			}

			// If all the bids are declined then the bidding status is Finished
			List<ShipTo> declinedShipTos = transmitedShipTos.stream().filter(s -> BidStatus._DECLINED_.equals(s.getContract().getBidStatus()))
					.collect(Collectors.toList());

			if (declinedShipTos.size() == transmitedShipTos.size()) {
				this.updateBiddingStatus(processedBid, declinedShipTos, BiddingStatus._FINISHED_, action, reason);
			}
		}

	}

	private List<Contract> getTransmitedBidsForLocation(Contract bid) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("bidTenderLocation", bid.getBidTenderLocation());
		params.put("bidStatus", LambdaUtil.filterEnum(BidStatus.class, a -> !BidStatus._DRAFT_.equals(a)));

		// Verify approval status if workflow is enabled
		if (this.sysParamService.getBidApprovalWorkflow()) {
			params.put("bidApprovalStatus", LambdaUtil.filterEnum(BidApprovalStatus.class, BidApprovalStatus._APPROVED_::equals));
		}

		// For Imported Tenders verify that the bid was Transmitted
		if (TenderSource._IMPORTED_.equals(bid.getBidTenderLocation().getTender().getSource())) {
			params.put("bidTransmissionStatus", LambdaUtil.filterEnum(TransmissionStatus.class, TransmissionStatus._TRANSMITTED_::equals));
		}

		// Return all submited bids
		return this.contractService.findEntitiesByAttributes(params);
	}

	private void updateBiddingStatus(Contract bid, Collection<ShipTo> shipTos, BiddingStatus biddingStatus, String action, String reason)
			throws BusinessException {

		TenderLocation tenderLocation = bid.getBidTenderLocation();

		// If bid has total volume or no shipTo's update location else airlines
		if (bid.getBidHasTotalVolume() || shipTos == null || shipTos.isEmpty()) {
			this.updateLocationBiddingStatus(tenderLocation, action);
		} else {
			this.updateAirlinesBiddingStatus(tenderLocation, shipTos, biddingStatus, action, reason);
		}
	}

	private void updateLocationBiddingStatus(TenderLocation tenderLocation, String action) throws BusinessException {
		BiddingStatus locationBiddingStatus = this.tenderLocationService.calculateBiddingStatus(tenderLocation);

		if (!tenderLocation.getLocationBiddingStatus().equals(locationBiddingStatus)) {
			this.tenderLocationService.updateBiddingStatus(tenderLocation, locationBiddingStatus, action, locationBiddingStatus.getName());
		}
	}

	private void updateAirlinesBiddingStatus(TenderLocation location, Collection<ShipTo> shipTos, BiddingStatus biddingStatus, String action,
			String reason) throws BusinessException {

		List<TenderLocationAirlines> airlines = new ArrayList<>();
		for (ShipTo shipTo : shipTos) {
			for (TenderLocationAirlines airline : shipTo.getContract().getBidTenderLocation().getTenderLocationAirlines()) {
				if (shipTo.getCustomer().equals(airline.getAirline())) {
					airlines.add(airline);
				}
			}
		}
		this.tenderLocationAirlineService.updateBiddingStatus(location, airlines, biddingStatus, action, reason);
	}
}
