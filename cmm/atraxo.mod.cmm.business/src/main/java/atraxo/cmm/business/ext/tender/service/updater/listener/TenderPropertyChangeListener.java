package atraxo.cmm.business.ext.tender.service.updater.listener;

import java.beans.PropertyChangeEvent;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import seava.j4e.api.exceptions.ActionNotSupportedException;
import seava.j4e.commons.utils.listener.ChangeListener;
import seava.j4e.commons.utils.listener.changes.ValueChanged;

/**
 * Builds the message which will be logged to history of the Tender
 *
 * @author abolindu
 */
public class TenderPropertyChangeListener extends ChangeListener {

	private static final Logger LOG = LoggerFactory.getLogger(TenderPropertyChangeListener.class);

	private static final String TEDNER_VALUE_CHANGED = "2004";
	private static final String TENDER_RECEIVER_VALUE_CHANGED = "2005";
	private static final String TENDER_ROUND_VALUE_CHANGED = "2006";

	public static final String NULL_REPLACER = "nothing";

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt instanceof seava.j4e.commons.utils.event.PropertyChangeEvent) {
			this.propertyChanged((seava.j4e.commons.utils.event.PropertyChangeEvent) evt);
		}
	}

	private void propertyChanged(seava.j4e.commons.utils.event.PropertyChangeEvent event) {
		ValueChanged valueChanged;
		Object oldValue = event.getOldValue();
		Object newValue = event.getNewValue();
		String propertyName = event.getPropertyName();
		Object entity = event.getEntity();

		try {

			Object originalValue = oldValue != null ? oldValue : NULL_REPLACER;
			valueChanged = new ValueChanged(propertyName, originalValue, newValue, entity, getMessageCode(entity));
			this.changes.addToValueChanges(Arrays.asList(valueChanged));

		} catch (ActionNotSupportedException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
		}

	}

	private static String getMessageCode(Object entity) throws ActionNotSupportedException {
		if (entity instanceof Tender || entity instanceof TenderLocation) {
			return TEDNER_VALUE_CHANGED;
		} else if (entity instanceof TenderLocationAirlines) {
			return TENDER_RECEIVER_VALUE_CHANGED;
		} else if (entity instanceof TenderLocationRound) {
			return TENDER_ROUND_VALUE_CHANGED;
		} else {
			throw new ActionNotSupportedException("No message defined for entity: " + entity.getClass());
		}
	}
}
