package atraxo.cmm.business.ext.bpm.delegate.priceCategory.approval;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.Expression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.contracts.IContractChangeService;
import atraxo.cmm.business.ext.bpm.delegate.ContractDelegate;
import atraxo.cmm.business.ext.contracts.service.ContractChangeUtil;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.ext.mailmerge.dto.ContractPriceUpdateApprovalDto;
import atraxo.cmm.domain.ext.mailmerge.dto.ProductChangeDto;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.ContractChange;
import atraxo.fmbas.business.api.workflow.IWorkflowParameterService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailAddressesDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.impl.fmbas_type.ContractChangeType;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;
import seava.j4e.api.exceptions.BusinessException;

/**
 * <code>ContractDelegate</code> approval delegate class that participates in the Price Approval Workflow
 *
 * @author vhojda
 */
public class PriceCategoryApprovalStageDelegate extends ContractDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(PriceCategoryApprovalStageDelegate.class);

	@Autowired
	private IWorkflowParameterService workflowParameterService;

	@Autowired
	private IContractChangeService contractChangeService;

	private Expression role;

	@Override
	public void execute() throws Exception {
		Contract contract = this.getContract();

		if (contract != null) {
			this.setNextApprovalStageRequiredVariable(true);

			WorkflowParameter param = this.workflowParameterService.findByKey(Long.valueOf(this.workflowId), this.getStageApprovalRolesVariable());
			List<UserSupp> users = this.extractApprovers(this.getStageApprovalRolesVariable(), param.getMandatory());

			if (!CollectionUtils.isEmpty(users)) {
				EmailDto emailDto = this.extractEmailDto(contract, users);
				if (emailDto != null) {
					this.execution.setVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_DTO, emailDto);
				}
			} else {
				this.setNextApprovalStageRequiredVariable(false);
			}
			this.resetMailComponentVariables();
		}
	}

	/**
	 * @param contract
	 * @param users
	 * @return
	 * @throws BusinessException
	 */
	@SuppressWarnings("unchecked")
	protected EmailDto extractEmailDto(Contract contract, List<UserSupp> users) throws BusinessException {
		EmailDto emailDTO = new EmailDto();

		// extract data from contract
		String contractCode = "";
		String contractHolder = "";
		String customerName = "";
		String contractLocation = "";

		try {
			contractCode = ContractChangeUtil.checkContractCode(contract.getCode());
			contractHolder = contract.getHolder().getName();
			contractLocation = contract.getLocation().getName();
			customerName = contract.getCustomer().getName();
		} catch (Exception e) {
			LOGGER.error("ERROR:could not extract data from the contract " + contract.getId() + " in order to send the mail ! ", e);
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
					String.format(CmmErrorCode.CONTRACT_WORKFLOW_EMAIL_EXTRACT_DATA_ERROR.getErrMsg(), contract.getId()));
			emailDTO = null;
		}

		if (emailDTO != null) {
			// set the subject
			emailDTO.setSubject(
					String.format(CmmErrorCode.WKF_CONTRACT_CHANGE_PRICE_SUBJECT.getErrMsg(), contractCode, contractLocation, customerName));

			// set attachments
			Object emailAttachments = this.execution.getVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_ATTACHMENTS);
			if (emailAttachments != null && emailAttachments instanceof List) {
				emailDTO.setAttachments((List<String>) emailAttachments);
			}

			// common email data for all receivers taken from workflow
			String fullNameOfRequester = this.getVariableFromWorkflow(WorkflowVariablesConstants.FULLNAME_REQUESTER_VAR);
			String approvalNote = this.getVariableFromWorkflow(WorkflowVariablesConstants.APPROVE_NOTE);

			List<ProductChangeDto> changes = this.extractPriceChanges(contract);

			List<EmailAddressesDto> emailAddressesDtoList = new ArrayList<>();
			for (UserSupp user : users) {

				ContractPriceUpdateApprovalDto dataDto = new ContractPriceUpdateApprovalDto();
				dataDto.setFullnameOfRequester(fullNameOfRequester);
				dataDto.setTitle(user.getTitle().getName());
				dataDto.setFullName(user.getFirstName() + " " + user.getLastName());

				dataDto.setContractCode(contractCode);
				dataDto.setContractHolder(contractHolder);
				dataDto.setContractLocation(contractLocation);
				dataDto.setCustomerName(customerName);
				dataDto.setApprovalRequestNote(approvalNote);

				EmailAddressesDto emailAddressesDto = new EmailAddressesDto();
				emailAddressesDto.setAddress(user.getEmail());
				emailAddressesDto.setData(dataDto);
				emailAddressesDtoList.add(emailAddressesDto);

				dataDto.setPriceChange(changes);
			}

			emailDTO.setTo(emailAddressesDtoList);

		}

		return emailDTO;
	}

	/**
	 * @param contract
	 * @return
	 * @throws BusinessException
	 */
	private List<ProductChangeDto> extractPriceChanges(Contract contract) throws BusinessException {
		List<ProductChangeDto> changesDtos = new ArrayList<>();

		List<ContractChange> changes = this.contractChangeService.getContractChanges(contract, ContractChangeType._PRICE_);
		changes.forEach(c -> changesDtos.add(new ProductChangeDto(c.getMessage())));

		return changesDtos;
	}

	@Override
	public String getSessionUserCode() {
		return null;
	}

	/**
	 * @return
	 */
	private String getStageApprovalRolesVariable() {
		return (String) this.role.getValue(this.execution);
	}

	/**
	 * @param required
	 */
	protected void setNextApprovalStageRequiredVariable(boolean required) {
		this.execution.setVariable(WorkflowVariablesConstants.NEXT_APPROVAL_STAGE_REQUIRED, required);
	}

}
