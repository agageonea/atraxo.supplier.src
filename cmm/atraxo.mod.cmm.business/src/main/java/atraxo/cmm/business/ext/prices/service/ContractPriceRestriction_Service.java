/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.ext.prices.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.contracts.IContractChangeService;
import atraxo.cmm.business.api.prices.IContractPriceRestrictionService;
import atraxo.cmm.business.ext.contracts.service.ContractChangeUtil;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.RestrictionTypes;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.fmbas.domain.impl.fmbas_type.ContractChangeType;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link ContractPriceRestriction} domain entity.
 */
public class ContractPriceRestriction_Service extends atraxo.cmm.business.impl.prices.ContractPriceRestriction_Service
		implements IContractPriceRestrictionService {

	@Autowired
	private IContractChangeService contractChangeService;

	@Override
	protected void preInsert(ContractPriceRestriction e) throws BusinessException {
		super.preInsert(e);
		this.validateRequiredFields(e);
		// this.checkRestrictions(e);
	}

	@Override
	protected void postInsert(ContractPriceRestriction e) throws BusinessException {
		super.preInsert(e);

		// "log" the insert price restriction change
		Contract contract = e.getContractPriceCategory().getContract();
		if (ContractChangeUtil.checkBlueprintProductChanges(contract)) {
			this.contractChangeService.addNewChange(contract, ContractChangeType._PRICE_,
					ContractChangeUtil.buildMessageForPriceRestrictionInsert(e));
		}
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);

		// "log" the delete price restriction change
		for (Object id : ids) {
			ContractPriceRestriction restriction = this.findById(id);
			Contract contract = restriction.getContractPriceCategory().getContract();
			if (ContractChangeUtil.checkBlueprintProductChanges(contract)) {
				this.contractChangeService.addNewChange(contract, ContractChangeType._PRICE_,
						ContractChangeUtil.buildMessageForPriceRestrictionDelete(restriction));
			}
		}
	}

	@Override
	protected void preUpdate(ContractPriceRestriction e) throws BusinessException {
		super.preUpdate(e);
		this.validateRequiredFields(e);
		this.checkRestrictions(e);

		// "log" the update price restriction change
		Contract contract = e.getContractPriceCategory().getContract();
		if (ContractChangeUtil.checkBlueprintProductChanges(contract)) {
			ContractPriceRestriction oldRestriction = this.findById(e.getId());
			this.contractChangeService.addNewChange(contract, ContractChangeType._PRICE_,
					ContractChangeUtil.buildMessageForPriceRestrictionUpdate(oldRestriction, e));
		}
	}

	protected void validateRequiredFields(ContractPriceRestriction e) throws BusinessException {

		if (e.getRestrictionType() != null && !e.getRestrictionType().equals(RestrictionTypes._EMPTY_)) {
			if (e.getRestrictionType().equals(RestrictionTypes._VOLUME_)) {
				if (e.getOperator() == null || StringUtils.isEmpty(e.getValue()) || e.getUnit() == null) {
					throw new BusinessException(CmmErrorCode.REQUIRED_FIELDS_NULL, CmmErrorCode.REQUIRED_FIELDS_NULL.getErrMsg());
				}
			} else if (!e.getRestrictionType().equals(RestrictionTypes._VOLUME_) && (e.getOperator() == null || StringUtils.isEmpty(e.getValue()))) {
				throw new BusinessException(CmmErrorCode.REQUIRED_FIELDS_NULL_NOT_VOLUME, CmmErrorCode.REQUIRED_FIELDS_NULL_NOT_VOLUME.getErrMsg());
			}
		} else {
			throw new BusinessException(CmmErrorCode.REQUIRED_RESTRICTION, CmmErrorCode.REQUIRED_RESTRICTION.getErrMsg());
		}

	}

	/**
	 * Verify if this restriction is exist in data base with same value and price category.
	 *
	 * @param e
	 * @throws BusinessException
	 */
	private void checkRestrictions(ContractPriceRestriction e) throws BusinessException {
		String sql = "select e from " + ContractPriceRestriction.class.getSimpleName() + " e where " + "e.clientId = :clientId and "
				+ "e.contractPriceCategory.priceCategory.id = :priceCategoryId and " + "e.contractPriceCategory.contract.id = :contrId and "
				+ "e.restrictionType = :restrictionType and " + "e.operator = :operator and " + "e.value = :value";
		List<ContractPriceRestriction> cprs = this.getEntityManager().createQuery(sql, ContractPriceRestriction.class)
				.setParameter("priceCategoryId", e.getContractPriceCategory().getPriceCategory().getId())
				.setParameter("contrId", e.getContractPriceCategory().getContract().getId()).setParameter("restrictionType", e.getRestrictionType())
				.setParameter("operator", e.getOperator()).setParameter("value", e.getValue())
				.setParameter("clientId", Session.user.get().getClientId()).getResultList();
		if (!CollectionUtils.isEmpty(cprs)) {
			for (ContractPriceRestriction temp : cprs) {
				if (!temp.getId().equals(e.getId())) {
					throw new BusinessException(CmmErrorCode.PC_PRICE_IS_SAME, CmmErrorCode.PC_PRICE_IS_SAME.getErrMsg());
				}
			}
		}
	}
}
