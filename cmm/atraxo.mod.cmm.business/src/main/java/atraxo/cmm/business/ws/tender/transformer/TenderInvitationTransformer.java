package atraxo.cmm.business.ws.tender.transformer;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.api.ext.datahub.IDataHubInTransformer;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.TenderSource;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.domain.impl.cmm_type.TenderType;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.fmbas_type.TenderVolumeOffer;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.commons.utils.SoneEnumUtils;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderInvitation;
import seava.j4e.iata.fuelplus.iata.tender.VolumeOfferType;

public class TenderInvitationTransformer extends DataHubInTransformer<FuelTenderInvitation, Tender>
		implements IDataHubInTransformer<Tender, FuelTenderInvitation> {

	private static final Logger LOGGER = LoggerFactory.getLogger(TenderInvitationTransformer.class);

	@Override
	public Tender transformDTOToModel(FuelTenderInvitation invitation) throws BusinessException, JAXBException {
		Tender tender = new Tender();

		try {
			tender.setSchemaVersion(invitation.getSchemaVersion());
			tender.setCode(invitation.getTenderHeader().getTenderIdentification().getTenderCode());
			tender.setTenderVersion(invitation.getTenderHeader().getTenderIdentification().getTenderVersion().longValue());
			if (invitation.getTenderHeader().getNoOfRounds() != null) {
				tender.setNoOfRounds(invitation.getTenderHeader().getNoOfRounds().intValue());
			}

			if (invitation.getTenderHeader().getTenderIdentification().getTenderName() != null) {
				tender.setName(invitation.getTenderHeader().getTenderIdentification().getTenderName());
			}
			if (invitation.getTenderHeader().getTenderIdentification().getTenderHolderCode() != null) {
				tender.setHolder(this.findCustomer(invitation.getTenderHeader().getTenderIdentification().getTenderHolderCode(), "TenderHolderCode"));
			}
			tender.setMsgId(invitation.getTenderHeader().getTenderIdentification().getMsgId());

			tender.setContact(this.getContact(tender.getHolder(), invitation.getTenderHeader().getTenderHolderContact().getContact()));

			tender.setContactPerson(invitation.getTenderHeader().getTenderHolderContact().getContact().getContactPerson());
			tender.setEmail(invitation.getTenderHeader().getTenderHolderContact().getContact().getEmail());

			if (invitation.getTenderHeader().getTenderHolderContact().getContact().getPhone() != null) {
				tender.setPhone(invitation.getTenderHeader().getTenderHolderContact().getContact().getPhone());
			}

			tender.setType(TenderType.getByCode(invitation.getTenderHeader().getTenderType().value()));

			if (invitation.getTenderHeader().getBiddingPeriod() != null) {

				if (invitation.getTenderHeader().getBiddingPeriod().getFrom() == null) {
					throw new BusinessException(BusinessErrorCode.VALID_FROM_NULL, BusinessErrorCode.VALID_FROM_NULL.getErrMsg());
				} else if (invitation.getTenderHeader().getBiddingPeriod().getTo() == null) {
					throw new BusinessException(BusinessErrorCode.VALID_TO_NULL, BusinessErrorCode.VALID_TO_NULL.getErrMsg());
				} else {
					if (invitation.getTenderHeader().getBiddingPeriod().getFrom().toGregorianCalendar()
							.before(invitation.getTenderHeader().getBiddingPeriod().getTo().toGregorianCalendar())) {
						tender.setBiddingPeriodFrom(invitation.getTenderHeader().getBiddingPeriod().getFrom().toGregorianCalendar().getTime());
						tender.setBiddingPeriodTo(invitation.getTenderHeader().getBiddingPeriod().getTo().toGregorianCalendar().getTime());
					} else {
						throw new BusinessException(BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
								BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO.getErrMsg());
					}
				}

			} else {
				throw new BusinessException(BusinessErrorCode.VALID_FROM_TO_NULL, BusinessErrorCode.VALID_FROM_TO_NULL.getErrMsg());
			}

			tender.setComments(invitation.getTenderHeader().getComments());

			tender.setStatus(TenderStatus._NEW_);
			tender.setBiddingStatus(BiddingStatus._NEW_);
			tender.setTransmissionStatus(TransmissionStatus._NEW_);
			tender.setSource(TenderSource._IMPORTED_);
			tender.setPublishedOn(new Date());
			tender.setClosed(false);
			tender.setVolumeOffer(this.getTenderVolumeOffer(invitation));

			// Add Tender Locations
			List<atraxo.cmm.domain.impl.tender.TenderLocation> tenderLocations = this.buildTenderLocations(tender, invitation.getTenderLocations());
			tender.setTenderLocation(tenderLocations);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw e;
		}

		SoneEnumUtils.setEmptyValues(tender);
		return tender;
	}

	private TenderVolumeOffer getTenderVolumeOffer(FuelTenderInvitation invitation) {
		VolumeOfferType tenderInvitationVolumeOffer = invitation.getTenderHeader().getVolumeOfferType();
		if (tenderInvitationVolumeOffer != null) {
			return TenderVolumeOffer.getByCode(tenderInvitationVolumeOffer.value());
		} else {
			return TenderVolumeOffer._UNDEFINED_;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubInTransformer#transformDTOToModels(java.lang.Object)
	 */
	@Override
	public Collection<Tender> transformDTOToModels(FuelTenderInvitation dto) throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}
}
