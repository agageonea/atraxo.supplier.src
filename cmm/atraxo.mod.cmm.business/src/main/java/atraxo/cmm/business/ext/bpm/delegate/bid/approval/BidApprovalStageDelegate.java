/**
 *
 */
package atraxo.cmm.business.ext.bpm.delegate.bid.approval;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.Expression;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.ext.bpm.delegate.ContractDelegate;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.ext.mailmerge.dto.BidApprovalDto;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.workflow.IWorkflowParameterService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailAddressesDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;

/**
 * Top <code>ContractDelegate</code> class with common behaviour for all the delegates that participate in the Bid Approval Workflow
 *
 * @author vhojda
 */
public class BidApprovalStageDelegate extends ContractDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(BidApprovalStageDelegate.class);

	@Autowired
	protected IWorkflowParameterService workflowParameterService;

	private Expression role;

	private Expression step;

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.bpm.delegate.AbstractJavaDelegate#execute()
	 */
	@Override
	public void execute() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}

		Contract bid = this.getBidContract();

		if (bid != null) {

			// reset the next approval stage variable
			this.setNextApprovalStageRequiredVariable(true);

			WorkflowParameter param = this.workflowParameterService.findByKey(Long.valueOf(this.workflowId), this.getStageApprovalRolesVariable());

			// extract the approvers and setup the email data
			List<UserSupp> users = this.extractApprovers(this.getStageApprovalRolesVariable(), param.getMandatory());

			if (!CollectionUtils.isEmpty(users)) {
				EmailDto emailDTO = this.extractEmailDto(bid, users);

				if (emailDTO != null) {
					this.execution.setVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_DTO, emailDTO);
				}
			} else {
				this.setNextApprovalStageRequiredVariable(false);
			}

			// make sure to reset the mail components
			this.resetMailComponentVariables();

		} else {
			// do nothing, the workflow will take care of it
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
	}

	/**
	 * @return
	 */
	private String getStageApprovalRolesVariable() {
		String approverRoleCodes = (String) this.role.getValue(this.execution);
		return approverRoleCodes;
	}

	/**
	 * @return
	 */
	private int getStageApprovalStepVariable() {
		String stepStringVar = (String) this.step.getValue(this.execution);
		return Integer.parseInt(stepStringVar);
	}

	/**
	 * @param required
	 */
	private void setNextApprovalStageRequiredVariable(boolean required) {
		this.execution.setVariable(WorkflowVariablesConstants.NEXT_APPROVAL_STAGE_REQUIRED, required);
	}

	/**
	 * @param bid
	 * @param users
	 * @return
	 */
	private EmailDto extractEmailDto(Contract bid, List<UserSupp> users) {
		EmailDto emailDTO = new EmailDto();

		// extract data from bid
		String tenderIssuer = null;
		String tenderLocation = null;
		String tenderName = null;
		try {
			tenderIssuer = bid.getBidTenderIdentification().getHolder().getName();
			tenderLocation = bid.getBidTenderLocation().getLocation().getName();
			tenderName = bid.getBidTenderIdentification().getName();
		} catch (Exception e) {
			LOGGER.error("ERROR:could not extract data from the bid " + bid.getId() + " in order to send the mail ! ", e);
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
					String.format(CmmErrorCode.BID_APPROVAL_WORKFLOW_EMAIL_EXTRACT_DATA_ERROR.getErrMsg(), bid.getId()));
			emailDTO = null;
		}

		if (emailDTO != null) {
			// set the subject
			emailDTO.setSubject("Bid for " + tenderIssuer + "/" + tenderLocation + " awaiting your approval");

			// set attachments
			Object emailAttachments = this.execution.getVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_ATTACHMENTS);
			if (emailAttachments != null && emailAttachments instanceof List) {
				emailDTO.setAttachments((List<String>) emailAttachments);
			}

			List<EmailAddressesDto> emailAddressesDtoList = new ArrayList<>();
			for (UserSupp user : users) {

				BidApprovalDto dataDto = new BidApprovalDto();
				dataDto.setFullnameOfRequester(this.getVariableFromWorkflow(WorkflowVariablesConstants.FULLNAME_REQUESTER_VAR));
				dataDto.setBidLocation(tenderLocation);
				dataDto.setTenderHolder(tenderIssuer);
				dataDto.setTenderName(tenderName);
				dataDto.setName(user.getFirstName() + " " + user.getLastName());
				dataDto.setTitle(user.getTitle().getName());
				dataDto.setApproveNote(this.getVariableFromWorkflow((WorkflowVariablesConstants.APPROVE_NOTE)));
				dataDto.setTaskNote(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_VAR));

				this.updateBidEmailDto(dataDto);

				EmailAddressesDto emailAddressesDto = new EmailAddressesDto();
				emailAddressesDto.setAddress(user.getEmail());
				emailAddressesDto.setData(dataDto);
				emailAddressesDtoList.add(emailAddressesDto);
			}

			emailDTO.setTo(emailAddressesDtoList);

		}

		return emailDTO;
	}

	/**
	 * Updates the email <code>BidApprovalDto</code> with information taken from workflow variables
	 *
	 * @param dataDto
	 */
	private void updateBidEmailDto(BidApprovalDto dataDto) {
		String approvalNote = this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_VAR);
		String approvalUser = this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR);

		int approvalStep = this.getStageApprovalStepVariable();
		if (approvalStep == 2) {
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL1, approvalNote);
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL1, approvalUser);
		}

		if (approvalStep == 3) {
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL2, approvalNote);
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL2, approvalUser);
		}

		if (approvalStep == 4) {
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL3, approvalNote);
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL3, approvalUser);
		}

		// all
		dataDto.setApprovalNoteFirstLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL1));
		dataDto.setApproverNameFirstLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL1));

		dataDto.setApprovalNoteSecondLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL2));
		dataDto.setApproverNameSecondLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL2));

		dataDto.setApprovalNoteThirdLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL3));
		dataDto.setApproverNameThirdLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL3));

	}

	@Override
	public String getSessionUserCode() {
		return null;
	}

}
