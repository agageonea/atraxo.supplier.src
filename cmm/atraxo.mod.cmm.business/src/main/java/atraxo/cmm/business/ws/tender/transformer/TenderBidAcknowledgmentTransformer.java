package atraxo.cmm.business.ws.tender.transformer;

import java.util.List;

import javax.xml.bind.JAXBException;

import atraxo.cmm.domain.ext.tender.MessageStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcknowledge;

public class TenderBidAcknowledgmentTransformer extends DataHubOutTransformer<Contract, FuelTenderAcknowledge> {

	@Override
	public FuelTenderAcknowledge transformModelToDTO(Contract modelEntity, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException {
		FuelTenderAcknowledge acknowledge = new FuelTenderAcknowledge();
		acknowledge.setTenderIdentification(this.buildTenderIdentification(modelEntity.getBidTenderIdentification(), msgId));
		acknowledge.setBidIdentification(this.buildBidIdentification(modelEntity));

		String type = this.setMessageType(receiverInterface);
		acknowledge.setMessageType(type);
		acknowledge.setMessageStatus(seava.j4e.iata.fuelplus.iata.tender.MessageStatus.fromValue(MessageStatus.RECEIVED.getIataCode()));
		acknowledge.setRefMsgId(refMsgId);
		acknowledge.setSchemaVersion(modelEntity.getBidTenderIdentification().getSchemaVersion());

		return acknowledge;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubOutTransformer#transformModelToDTO(atraxo.fmbas.domain.impl.abstracts.AbstractEntity, long,
	 * java.lang.String)
	 */
	@Override
	public FuelTenderAcknowledge transformModelToDTO(Contract modelEntity, long msgId, String reason) throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubOutTransformer#transformModelsToDTO(java.util.List, long, java.lang.String)
	 */
	@Override
	public FuelTenderAcknowledge transformModelsToDTO(List<Contract> modelEntities, long msgId, String reason)
			throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubOutTransformer#transformModelsToDTO(java.util.List, long, long,
	 * atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames)
	 */
	@Override
	public FuelTenderAcknowledge transformModelsToDTO(List<Contract> modelEntities, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}
}
