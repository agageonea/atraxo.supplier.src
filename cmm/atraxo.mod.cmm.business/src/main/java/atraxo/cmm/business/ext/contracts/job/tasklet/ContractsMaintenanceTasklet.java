package atraxo.cmm.business.ext.contracts.job.tasklet;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;

public class ContractsMaintenanceTasklet implements Tasklet {

	@Autowired
	private IContractService service;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		StringBuilder contractInfoStatus = new StringBuilder(
				"The job has been succesfully run |Info : ACTIVE = AC ~ EFFECTIVE = EF ~ EXPIRED = EX" + System.lineSeparator());
		this.service.updateStatus(contractInfoStatus);
		chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put("contractInfo", contractInfoStatus);
		return RepeatStatus.FINISHED;
	}

}
