package atraxo.cmm.business.ext.contracts.messageobject;

/**
 * @author apetho
 */
public class MessageContractObject {

	private Integer invoiceId;
	private String status;

	/**
	 * @param invoiceId
	 * @param status
	 */
	public MessageContractObject(Integer invoiceId, String status) {
		super();
		this.invoiceId = invoiceId;
		this.status = status;
	}

	/**
	 * @return
	 */
	public Integer getInvoiceId() {
		return this.invoiceId;
	}

	/**
	 * @param invoiceId
	 */
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	/**
	 * @return
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
