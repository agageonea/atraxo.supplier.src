/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.ext.tender.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.Years;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.ext.tender.ITenderProcessor;
import atraxo.cmm.business.api.tender.ITenderLocationService;
import atraxo.cmm.business.api.tender.ITenderService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.aop.history.Action;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.annotation.Reason;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link Tender} domain entity.
 */
public class Tender_Service extends atraxo.cmm.business.impl.tender.Tender_Service implements ITenderService {

	@Autowired
	private ISystemParameterService systemParamsService;
	@Autowired
	private IUnitService unitService;
	@Autowired
	private ITenderLocationService tenderLocationService;
	@Autowired
	private ITenderProcessor tenderProcessor;

	@Override
	protected void postInsert(Tender e) throws BusinessException {
		super.postInsert(e);
		if (e.getCode() == null || "".equalsIgnoreCase(e.getCode())) {
			Tender t = this.findByRefid(e.getRefid());
			int n = String.valueOf(t.getId()).length();
			StringBuilder code = new StringBuilder();
			code.append("TDR");
			for (int i = 1; i <= 5 - n; ++i) {
				code.append("0");
			}
			t.setCode(code.append(t.getId()).toString());
			this.update(t);
		}
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);

		List<Tender> tenders = this.findByIds(ids);
		List<TenderLocation> tenderLocs = new ArrayList<>();

		for (Tender td : tenders) {
			tenderLocs.addAll(td.getTenderLocation());
		}
		List<Object> tenderLocIds = this.collectIds(tenderLocs);

		if (!CollectionUtils.isEmpty(tenderLocIds)) {
			this.tenderLocationService.deleteByIds(tenderLocIds);
		}
	}

	@Override
	public BigDecimal getTotalVolume(Tender tender) throws BusinessException {
		Map<String, Object> tenderParams = new HashMap<>();
		tenderParams.put("tender", tender);
		List<TenderLocation> list = this.findEntitiesByAttributes(TenderLocation.class, tenderParams);

		Map<String, Object> subsidiaryParams = new HashMap<>();
		subsidiaryParams.put("refid", Session.user.get().getClient().getActiveSubsidiaryId());
		Customer subsidiary = this.findEntityByAttributes(Customer.class, subsidiaryParams);

		BigDecimal vol = BigDecimal.ZERO;
		Double density = Double.parseDouble(this.systemParamsService.getDensity());
		if (!CollectionUtils.isEmpty(list)) {
			Unit toUnit;
			if (subsidiary.getDefaultVolumeUnit() != null) {
				toUnit = subsidiary.getDefaultVolumeUnit();
			} else {
				toUnit = this.unitService.findByCode(this.systemParamsService.getSysVol());
			}

			for (TenderLocation e : list) {
				if (e.getVolumeUnit() != null && e.getVolume() != null && e.getIsValid()) {

					BigDecimal locationVolume = e.getVolume();

					// calculate volume based on period type and contract period
					DateTime sDate = new DateTime(e.getAgreementFrom());
					DateTime eDate = new DateTime(e.getAgreementTo());

					Months months = Months.monthsBetween(sDate, eDate).plus(Months.ONE);
					Years years = Years.yearsBetween(sDate, eDate);

					BigDecimal factor = BigDecimal.ONE;
					switch (e.getPeriodType()) {
					case _MONTHLY_:
						factor = new BigDecimal(months.getMonths());
						break;
					case _YEARLY_:
						factor = new BigDecimal((years.getYears() > 0) ? years.getYears() : 1);
						break;
					default:
						break;
					}

					locationVolume = locationVolume.multiply(factor, MathContext.DECIMAL64);
					vol = vol.add(this.unitService.convert(e.getVolumeUnit(), toUnit, locationVolume, density));
				}
			}
		}
		return vol;
	}

	@Override
	@Transactional
	@History(value = "Publish", type = Tender.class)
	public void publish(Tender tender) throws BusinessException {
		tender.setStatus(TenderStatus._NEW_);
		tender.setPublishedOn(new Date());
		this.update(tender);
	}

	@Override
	@Transactional
	@History(value = "Unpublish", type = Tender.class)
	public void unpublish(Tender tender, @Reason String reason) throws BusinessException {
		String hql = "select  max(e.tenderVersion) from " + Tender.class.getSimpleName()
				+ " e where e.code = :code and e.holder = :holder and e.clientId = :clientId";
		Long maxVersion = 1l;
		try {
			maxVersion = this.getEntityManager().createQuery(hql, Long.class).setParameter("code", tender.getCode())
					.setParameter("holder", tender.getHolder()).setParameter("clientId", Session.user.get().getClientId()).getSingleResult();
		} catch (NoResultException | NonUniqueResultException e) {
			throw new BusinessException(CmmErrorCode.TENDER_VERSION_CANOT_FOUND,
					String.format(CmmErrorCode.TENDER_VERSION_CANOT_FOUND.getErrMsg(), tender.getCode(), tender.getHolder()), e);
		}
		tender.setStatus(TenderStatus._DRAFT_);
		tender.setTenderVersion(maxVersion + 1);

		this.update(tender);
	}

	@Override
	@History(type = Tender.class)
	public void updateBiddingStatus(Tender tender, BiddingStatus biddingStatus, @Action String action) throws BusinessException {
		tender.setBiddingStatus(biddingStatus);
		this.update(tender);
	}

	@Override
	public BiddingStatus calculateBiddingStatus(Tender tender) throws BusinessException {
		List<TenderLocation> locations = new ArrayList<>();
		locations.addAll(tender.getTenderLocation());

		// if at leat one location is In Negotiation then the bidding is In Negotiation
		TenderLocation inProgress = locations.stream().filter(loc -> BiddingStatus._IN_NEGOTIATION_.equals(loc.getLocationBiddingStatus())).findAny()
				.orElse(null);

		if (inProgress != null) {
			return BiddingStatus._IN_NEGOTIATION_;
		}

		// if all the locations are Finished then the bidding status is Finished
		List<TenderLocation> finished = locations.stream().filter(loc -> BiddingStatus._FINISHED_.equals(loc.getLocationBiddingStatus()))
				.collect(Collectors.toList());

		if (finished.size() == tender.getTenderLocation().size()) {
			return BiddingStatus._FINISHED_;
		}

		return tender.getBiddingStatus();
	}

	@Override
	@Transactional
	public void markRead(Tender tender) throws BusinessException {
		this.tenderProcessor.sendReadNotification(tender);
		tender.setIsRead(true);
		this.update(tender);
	}

}
