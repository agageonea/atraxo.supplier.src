package atraxo.cmm.business.ext.tender;

public class TenderDefaultConstants {
	public static final String DEFAULT_FUEL_PRODUCT = "JETA1";
	public static final String DEFAULT_FUEL_PRODUCT_TAX_TYPE = "NA";
	public static final String DEFAULT_ASTM_SPECIFICATION = "-";
	public static final String DEFAULT_CONTRACT_SUB_TYPE = "PSI";
	public static final String DEFAULT_VAT_APPLICABILITY = "Not applicable";

	public static final String DEFAULT_PAYMENT_TYPE = "CO";
	public static final String DEFAULT_CREDIT_TERM = "Open credit";
	public static final String DEFAULT_PAYMENT_REF_DAY = "ID";
	public static final String DEFAULT_INVOICE_FREQ = "W/7";
	public static final String DEFAULT_INVOICE_TYPE = "XML_3_1_0";

	public static final Integer DEFAULT_PAYMENT_TERMS = 3;
	public static final int DEFAULT_EXCHANGE_RATE_OFFSET = -1;

	public static final String PARAM_CAPTURE_BID_VERSION = "captureBidVersion";
	public static final String PARAM_CAPTURE_BID_FROM_HOLDER_MA = "captureBidFromHolderMA";
	public static final String PARAM_CAPTURE_BID_FROM_RECEIVER_MA = "captureBidFromReceiverMA";
	public static final String PARAM_CAPTURE_BID_FROM_EXISTING_DATA = "captureBidFromExistingBids";

	private TenderDefaultConstants() {
		super();
	}

}
