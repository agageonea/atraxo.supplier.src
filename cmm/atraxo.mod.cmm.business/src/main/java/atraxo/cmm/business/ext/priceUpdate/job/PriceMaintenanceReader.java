package atraxo.cmm.business.ext.priceUpdate.job;

import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.priceUpdate.IPriceUpdateService;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdate;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Reader for Price Update publishing job.
 *
 * @author zspeter
 */
public class PriceMaintenanceReader implements ItemReader<PriceUpdate> {

	@Autowired
	private IPriceUpdateService updSrv;
	@Autowired
	private ISystemParameterService paramSrv;

	@Override
	public PriceUpdate read() throws BusinessException {
		boolean flag = this.paramSrv.getPriceUpdateApprovalWorkflow();
		return this.updSrv.findForPublishing(flag);
	}

}
