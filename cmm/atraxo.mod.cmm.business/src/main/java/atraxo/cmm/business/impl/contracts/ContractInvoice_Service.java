/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.contracts;

import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.ContractInvoice;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ContractInvoice} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ContractInvoice_Service
		extends
			AbstractEntityService<ContractInvoice> {

	/**
	 * Public constructor for ContractInvoice_Service
	 */
	public ContractInvoice_Service() {
		super();
	}

	/**
	 * Public constructor for ContractInvoice_Service
	 * 
	 * @param em
	 */
	public ContractInvoice_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ContractInvoice> getEntityClass() {
		return ContractInvoice.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ContractInvoice
	 */
	public ContractInvoice findByInvoiceId(Integer invoiceId, DealType dealType) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ContractInvoice.NQ_FIND_BY_INVOICEID,
							ContractInvoice.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("invoiceId", invoiceId)
					.setParameter("dealType", dealType).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ContractInvoice", "invoiceId, dealType"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ContractInvoice", "invoiceId, dealType"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return ContractInvoice
	 */
	public ContractInvoice findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ContractInvoice.NQ_FIND_BY_BUSINESS,
							ContractInvoice.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ContractInvoice", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ContractInvoice", "id"), nure);
		}
	}

	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<ContractInvoice>
	 */
	public List<ContractInvoice> findByContract(Contract contract) {
		return this.findByContractId(contract.getId());
	}
	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<ContractInvoice>
	 */
	public List<ContractInvoice> findByContractId(Integer contractId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ContractInvoice e where e.clientId = :clientId and e.contract.id = :contractId",
						ContractInvoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractId", contractId).getResultList();
	}
}
