package atraxo.cmm.business.ext.bpm.delegate;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;

/**
 * Top <code>ContractDelegate</code> that is responsible with calculating if the volume of the contract/bid is over the threshold ones taken from
 * parameters; these boolean values will also be set on the workflow variables <code>Map</code> as internal variables;
 *
 * @author vhojda
 */
public abstract class CalculateVolumesDelegate extends ContractDelegate {

	private static final String CONTRACT_VOLUME_UNDEFINED_MSG = "Undefined contract volume";

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ext.bpm.delegate.ContractDelegate#getContract()
	 */
	protected abstract Contract getCurrentContract() throws Exception;

	@Override
	public void execute() throws Exception {

		Contract contract = this.getCurrentContract();
		if (contract != null) {

			boolean contractVolumeDefined = contract.getIsContract() ? (contract.getAwardedVolume() != null) : (contract.getOfferedVolume() != null);
			this.execution.setVariable(WorkflowVariablesConstants.CONTRACT_VOLUME_DEFINED, contractVolumeDefined);

			if (!contractVolumeDefined) {
				// set up the operation (reject),the reject note and the "user" who rejected
				this.execution.setVariable(WorkflowVariablesConstants.TASK_ACCEPTANCE_VAR, false);
				this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_VAR, CONTRACT_VOLUME_UNDEFINED_MSG);
				this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME, this.getCoreUserCode());
			} else {
				// set over lower volume
				Boolean overLowerVolume = this.isContractOverVolume(contract, WorkflowVariablesConstants.THRESHOLD_VOLUME_LOWER);
				if (overLowerVolume != null) {
					this.execution.setVariable(WorkflowVariablesConstants.CONTRACT_OVER_LOWER_VOLUME, overLowerVolume.booleanValue());
				} else {
					this.execution.setVariable(WorkflowVariablesConstants.CONTRACT_OVER_LOWER_VOLUME, false);
				}

				// set over upper volume
				Boolean overUpperVolume = this.isContractOverVolume(contract, WorkflowVariablesConstants.THRESHOLD_VOLUME_UPPER);
				if (overUpperVolume != null) {
					this.execution.setVariable(WorkflowVariablesConstants.CONTRACT_OVER_UPPER_VOLUME, overUpperVolume.booleanValue());
				} else {
					this.execution.setVariable(WorkflowVariablesConstants.CONTRACT_OVER_UPPER_VOLUME, false);
				}
			}
		}
	}

	@Override
	public String getSessionUserCode() {
		return null;
	}

}
