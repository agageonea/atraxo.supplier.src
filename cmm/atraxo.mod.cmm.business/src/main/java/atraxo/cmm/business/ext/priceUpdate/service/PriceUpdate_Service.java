/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.ext.priceUpdate.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import atraxo.cmm.business.api.priceUpdate.IPriceUpdateService;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.exceptions.NoPriceComponentsException;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdate;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdateCategories;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.ext.aop.history.Action;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import seava.j4e.api.annotation.Reason;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link PriceUpdate} domain entity.
 */
public class PriceUpdate_Service extends atraxo.cmm.business.impl.priceUpdate.PriceUpdate_Service implements IPriceUpdateService {

	private static final String SEPARATOR = ",";
	private static final Logger LOG = LoggerFactory.getLogger(PriceUpdate_Service.class);

	@Autowired
	private IContractPriceCategoryService service;
	@Autowired
	private ICustomerService custSrv;

	@Override
	@History(type = PriceUpdate.class, value = "Published")
	@Transactional
	public void publish(List<PriceUpdate> list) throws BusinessException {
		this.update(list);
	}

	@Override
	@Transactional
	public void refresh(String refId) throws BusinessException {
		try {
			PriceUpdate e = this.findByRefid(refId);
			this.refresh(e);
			this.update(e);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new BusinessException(CmmErrorCode.PRICE_UPDATE_NOT_FIND, CmmErrorCode.PRICE_UPDATE_NOT_FIND.getErrMsg(), e);
		}

	}

	@Override
	public void refresh(PriceUpdate e) throws BusinessException {
		List<Contract> contracts = this.findContracts(e);
		List<PriceUpdateCategories> list = this.buildUpdateCategories(contracts, e.getPriceCategory(), e.getCurrency());
		for (PriceUpdateCategories puc : list) {
			puc.setPriceUpdate(e);
			if (!e.getPriceUpdateCategories().contains(puc)) {
				e.addToPriceUpdateCategories(puc);
			}
		}
		Iterator<PriceUpdateCategories> iter = e.getPriceUpdateCategories().iterator();
		while (iter.hasNext()) {
			PriceUpdateCategories puc = iter.next();
			if (!list.contains(puc)) {
				iter.remove();
			}
		}
	}

	@Override
	public PriceUpdate findForPublishing(boolean onlyApproved) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("published", false);
		if (onlyApproved) {
			params.put("approvalStatus", BidApprovalStatus._APPROVED_);
		}
		List<PriceUpdate> list = this.findEntitiesByAttributes(params);
		if (CollectionUtils.isEmpty(list)) {
			return null;
		} else {
			return list.get(0);
		}
	}

	@Override
	protected void preInsert(PriceUpdate e) throws BusinessException {
		super.postInsert(e);

		List<Contract> list = this.findContracts(e);
		this.buildUpdateCategories(list, e);
	}

	private List<PriceUpdateCategories> buildUpdateCategories(List<Contract> contracts, PriceCategory pc, Currencies currency)
			throws BusinessException {
		List<PriceUpdateCategories> list = new ArrayList<>();
		for (Contract c : contracts) {
			for (ContractPriceCategory cpc : c.getPriceCategories()) {
				if (cpc.getPriceCategory().equals(pc)) {
					PriceUpdateCategories puc = this.createPriceCategory(cpc, currency);
					if (puc != null) {
						list.add(puc);
					}
				}
			}
		}
		return list;
	}

	@Override
	@Transactional
	@History(type = PriceUpdate.class, value = "Captured")
	public void insert(PriceUpdate e) throws BusinessException {
		super.insert(e);
	}

	@Override
	@Transactional
	@History(type = PriceUpdate.class, value = "Captured")
	public void insert(List<PriceUpdate> list) throws BusinessException {
		super.insert(list);
	}

	private void buildUpdateCategories(List<Contract> contracts, PriceUpdate e) throws BusinessException {
		for (Contract c : contracts) {
			for (ContractPriceCategory cpc : c.getPriceCategories()) {
				if (cpc.getPriceCategory().getId().equals(e.getPriceCategory().getId())) {
					PriceUpdateCategories puc = this.createPriceCategory(cpc, e.getCurrency());
					if (puc != null) {
						e.addToPriceUpdateCategories(puc);
					}
				}
			}
		}
	}

	private PriceUpdateCategories createPriceCategory(ContractPriceCategory cpc, Currencies currency) throws BusinessException {
		Date date = Calendar.getInstance().getTime();
		if (date.compareTo(cpc.getContract().getValidTo()) >= 0) {
			date = cpc.getContract().getValidTo();
		}
		try {
			ContractPriceComponent priceComponent = this.service.getPriceComponent(cpc, date);
			if (currency != null && !currency.getCode().equals(priceComponent.getCurrency().getCode())) {
				return null;
			}

			PriceUpdateCategories e = new PriceUpdateCategories();
			e.setContract(cpc.getContract().getCode());
			e.setContractHolder(cpc.getContract().getHolder());
			e.setContractPriceCatId(cpc.getId());
			if (cpc.getContract().getDealType().equals(DealType._BUY_)) {
				Customer c = this.custSrv.findById(cpc.getContract().getSupplier().getId());
				e.setCounterparty(c);
			} else {
				e.setCounterparty(cpc.getContract().getCustomer());
			}
			e.setPrice(priceComponent.getPrice());
			e.setCurrency(priceComponent.getCurrency());
			e.setDealType(cpc.getContract().getDealType());
			e.setLocation(cpc.getContract().getLocation());
			e.setPriceCatName(cpc.getPriceCategory().getName());
			e.setUnit(priceComponent.getUnit());
			e.setSelected(false);
			e.setClientId(Session.user.get().getClientId());
			return e;
		} catch (NoPriceComponentsException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
			// do nothing
			return null;
		}

	}

	private List<Contract> findContracts(PriceUpdate e) throws BusinessException {
		CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
		Class<Contract> entityClass = Contract.class;
		CriteriaQuery<Contract> cq = cb.createQuery(entityClass);
		Root<Contract> root = cq.from(entityClass);
		cq.select(root);
		List<Predicate> predicateList = new ArrayList<>();
		List<Predicate> orPredicateList = new ArrayList<>();
		if (e.getLocation() != null) {
			predicateList.add(cb.equal(root.get("location"), e.getLocation()));
		} else {
			Customer c = this.findEntityService(Customer.class).findByRefid(Session.user.get().getClient().getActiveSubsidiaryId());
			predicateList.add(root.get("location").in(c.getAssignedArea().getLocations()));
		}
		if (!StringUtils.isEmpty(e.getDealType()) && e.getCounterparty() != null) {
			if (!e.getDealType().contains(SEPARATOR)) {
				switch (DealType.getByName(e.getDealType())) {
				case _SELL_:
					predicateList.add(cb.equal(root.get("customer"), e.getCounterparty()));
					break;
				case _BUY_:
					Suppliers sup = new Suppliers();
					sup.setId(e.getCounterparty().getId());
					predicateList.add(cb.equal(root.get("supplier"), sup));
					break;
				default:
					break;
				}
			} else {
				orPredicateList.add(cb.equal(root.get("customer"), e.getCounterparty()));
				Suppliers sup = new Suppliers();
				sup.setId(e.getCounterparty().getId());
				orPredicateList.add(cb.equal(root.get("supplier"), sup));
			}
		}
		if (e.getContractHolder() != null) {
			predicateList.add(cb.equal(root.get("holder"), e.getContractHolder()));
		}
		if (e.getBillTo() != null) {
			predicateList.add(cb.equal(root.get("billTo"), e.getBillTo()));
		}
		this.addToPredicateList(e.getDealType(), cb, root, predicateList, "dealType", DealType.class);
		this.addToPredicateList(e.getContractStatus(), cb, root, predicateList, "status", ContractStatus.class);
		this.addToPredicateList(e.getContractType(), cb, root, predicateList, "type", ContractType.class);
		this.addToPredicateList(e.getDeliveryPoint(), cb, root, predicateList, "subType", ContractSubType.class);

		predicateList.add(root.get("subsidiaryId").in(Session.user.get().getProfile().getOrganizationIds()));
		predicateList.add(cb.equal(root.get("clientId"), Session.user.get().getClient().getId()));
		predicateList.add(cb.equal(root.get("isBlueprint"), false));

		Predicate p1 = cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
		Predicate p2 = cb.or(orPredicateList.toArray(new Predicate[orPredicateList.size()]));
		cq.where(cb.and(p1, p2));
		TypedQuery<Contract> query = this.getEntityManager().createQuery(cq);
		List<Contract> resultList = query.getResultList();
		this.filterByShipTo(e, resultList);
		return resultList;
	}

	private void filterByShipTo(PriceUpdate e, List<Contract> resultList) {
		if (e.getShipTo() != null) {
			Iterator<Contract> iter = resultList.iterator();
			while (iter.hasNext()) {
				Contract c = iter.next();
				boolean found = false;
				for (ShipTo s : c.getShipTo()) {
					if (s.getCustomer().getCode().equals(e.getShipTo().getCode())) {
						found = true;
					}
				}
				if (!found) {
					iter.remove();
				}
			}
		}
	}

	@SuppressWarnings({ "unchecked" })
	private <T extends Object> void addToPredicateList(String string, CriteriaBuilder cb, Root<Contract> root, List<Predicate> predicateList,
			String fieldName, Class<T> clazz) throws BusinessException {
		try {
			java.lang.reflect.Method m = clazz.getMethod("getByName", String.class);
			if (!StringUtils.isEmpty(string)) {
				List<String> list = Arrays.asList(string.split(SEPARATOR));
				if (list.size() > 1) {
					List<T> l = new ArrayList<>();
					for (String str : list) {
						l.add((T) m.invoke(null, str));
					}
					predicateList.add(root.get(fieldName).in(l));
				} else {
					predicateList.add(cb.equal(root.get(fieldName), m.invoke(null, string)));
				}
			}
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}
	}

	@Override
	@Transactional
	@History(type = PriceUpdate.class)
	public void approve(List<PriceUpdate> list, @Action String action, @Reason String reason) throws BusinessException {
		for (PriceUpdate elem : list) {
			elem.setApprovalStatus(BidApprovalStatus._APPROVED_);
		}
		this.update(list);
	}

	@Override
	@Transactional
	@History(type = PriceUpdate.class)
	public void reject(List<PriceUpdate> list, @Action String action, @Reason String reason) throws BusinessException {
		for (PriceUpdate elem : list) {
			elem.setApprovalStatus(BidApprovalStatus._REJECTED_);
		}
		this.update(list);
	}

	@Override
	@Transactional
	@History(type = PriceUpdate.class, value = "Entity has been reset because of workflow error")
	public void reset(Integer id) throws BusinessException {
		PriceUpdate e = this.findById(id);
		e.setApprovalStatus(BidApprovalStatus._NEW_);
		this.update(e);
	}

	@Override
	@Transactional
	@History(type = PriceUpdate.class)
	public void submitForApproval(PriceUpdate e, BidApprovalStatus status, @Action String action, @Reason String reason) throws BusinessException {
		e.setApprovalStatus(status);
		this.update(e);
	}

}
