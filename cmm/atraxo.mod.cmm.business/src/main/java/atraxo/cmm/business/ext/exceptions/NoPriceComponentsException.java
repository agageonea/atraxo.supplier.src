package atraxo.cmm.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.IErrorCode;

public class NoPriceComponentsException extends BusinessException {

	private static final long serialVersionUID = 3228796993772724037L;

	public NoPriceComponentsException(IErrorCode errorCode, String errorDetails) {
		super(errorCode, errorDetails);
	}

}
