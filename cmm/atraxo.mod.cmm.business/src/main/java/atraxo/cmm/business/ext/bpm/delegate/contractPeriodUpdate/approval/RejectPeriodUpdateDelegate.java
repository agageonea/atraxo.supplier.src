package atraxo.cmm.business.ext.bpm.delegate.contractPeriodUpdate.approval;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.ext.bpm.delegate.ContractDelegate;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;

/**
 * @author abolindu
 */
public class RejectPeriodUpdateDelegate extends ContractDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(RejectPeriodUpdateDelegate.class);

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#execute()
	 */
	@Override
	public void execute() throws Exception {
		Contract contract = this.getContract();
		if (contract != null) {
			this.contractSrv.rejectPeriodUpdate(contract, this.getNote());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#getSessionUserCode()
	 */
	@Override
	public String getSessionUserCode() {
		return this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME);
	}
}
