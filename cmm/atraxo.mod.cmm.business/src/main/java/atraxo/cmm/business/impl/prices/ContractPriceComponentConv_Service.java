/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.prices;

import atraxo.cmm.business.api.prices.IContractPriceComponentConvService;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ContractPriceComponentConv} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ContractPriceComponentConv_Service
		extends
			AbstractEntityService<ContractPriceComponentConv>
		implements
			IContractPriceComponentConvService {

	/**
	 * Public constructor for ContractPriceComponentConv_Service
	 */
	public ContractPriceComponentConv_Service() {
		super();
	}

	/**
	 * Public constructor for ContractPriceComponentConv_Service
	 * 
	 * @param em
	 */
	public ContractPriceComponentConv_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ContractPriceComponentConv> getEntityClass() {
		return ContractPriceComponentConv.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ContractPriceComponentConv
	 */
	public ContractPriceComponentConv findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							ContractPriceComponentConv.NQ_FIND_BY_BUSINESS,
							ContractPriceComponentConv.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ContractPriceComponentConv", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ContractPriceComponentConv", "id"), nure);
		}
	}

	/**
	 * Find by reference: contractPriceComponent
	 *
	 * @param contractPriceComponent
	 * @return List<ContractPriceComponentConv>
	 */
	public List<ContractPriceComponentConv> findByContractPriceComponent(
			ContractPriceComponent contractPriceComponent) {
		return this.findByContractPriceComponentId(contractPriceComponent
				.getId());
	}
	/**
	 * Find by ID of reference: contractPriceComponent.id
	 *
	 * @param contractPriceComponentId
	 * @return List<ContractPriceComponentConv>
	 */
	public List<ContractPriceComponentConv> findByContractPriceComponentId(
			Integer contractPriceComponentId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ContractPriceComponentConv e where e.clientId = :clientId and e.contractPriceComponent.id = :contractPriceComponentId",
						ContractPriceComponentConv.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractPriceComponentId",
						contractPriceComponentId).getResultList();
	}
	/**
	 * Find by reference: equivalentUnit
	 *
	 * @param equivalentUnit
	 * @return List<ContractPriceComponentConv>
	 */
	public List<ContractPriceComponentConv> findByEquivalentUnit(
			Unit equivalentUnit) {
		return this.findByEquivalentUnitId(equivalentUnit.getId());
	}
	/**
	 * Find by ID of reference: equivalentUnit.id
	 *
	 * @param equivalentUnitId
	 * @return List<ContractPriceComponentConv>
	 */
	public List<ContractPriceComponentConv> findByEquivalentUnitId(
			Integer equivalentUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ContractPriceComponentConv e where e.clientId = :clientId and e.equivalentUnit.id = :equivalentUnitId",
						ContractPriceComponentConv.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("equivalentUnitId", equivalentUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: equivalentCurrency
	 *
	 * @param equivalentCurrency
	 * @return List<ContractPriceComponentConv>
	 */
	public List<ContractPriceComponentConv> findByEquivalentCurrency(
			Currencies equivalentCurrency) {
		return this.findByEquivalentCurrencyId(equivalentCurrency.getId());
	}
	/**
	 * Find by ID of reference: equivalentCurrency.id
	 *
	 * @param equivalentCurrencyId
	 * @return List<ContractPriceComponentConv>
	 */
	public List<ContractPriceComponentConv> findByEquivalentCurrencyId(
			Integer equivalentCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ContractPriceComponentConv e where e.clientId = :clientId and e.equivalentCurrency.id = :equivalentCurrencyId",
						ContractPriceComponentConv.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("equivalentCurrencyId", equivalentCurrencyId)
				.getResultList();
	}
}
