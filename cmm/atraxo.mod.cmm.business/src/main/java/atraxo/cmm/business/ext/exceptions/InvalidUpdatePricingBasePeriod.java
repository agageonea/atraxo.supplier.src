package atraxo.cmm.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;

public class InvalidUpdatePricingBasePeriod extends BusinessException {
	public InvalidUpdatePricingBasePeriod() {
		super(CmmErrorCode.INVALID_PRICING_BASE_UPDATE_PERIOD, CmmErrorCode.INVALID_PRICING_BASE_UPDATE_PERIOD.getErrMsg());
	}

}
