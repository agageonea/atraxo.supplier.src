package atraxo.cmm.business.ws.tender.transformer;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.fmbas.business.api.categories.IIataPCService;
import atraxo.fmbas.business.api.contacts.IContactsService;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.financialSources.IFinancialSourcesService;
import atraxo.fmbas.business.api.geo.ILocationsService;
import atraxo.fmbas.business.api.notes.INotesService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.timeserie.IAverageMethodService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.SingleResultException;
import atraxo.fmbas.domain.impl.categories.IataPC;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.commons.utils.SoneEnumUtils;
import seava.j4e.iata.fuelplus.iata.tender.Contact;
import seava.j4e.iata.fuelplus.iata.tender.PUOMBase;
import seava.j4e.iata.fuelplus.iata.tender.ServiceLevel;

/**
 * @author zspeter
 * @param <E>
 * @param <T>
 */
public abstract class DataHubTransformer {

	private static final Logger LOGGER = LoggerFactory.getLogger(DataHubTransformer.class);

	protected static final String OBJECT_TYPE = "objectType";
	protected static final String OBJECT_ID = "objectId";

	private static final String ICAO_CODE = "icaoCode";
	private static final String EMAIL2 = "email";
	private static final String LAST_NAME = "lastName";
	private static final String FIRST_NAME = "firstName";
	protected static final String IATA_CODE = "iataCode";

	@Autowired
	protected IContactsService contactService;
	@Autowired
	protected ICustomerService customerService;
	@Autowired
	protected ILocationsService locationsService;
	@Autowired
	protected IUnitService unitService;
	@Autowired
	protected ISystemParameterService systemParamsService;
	@Autowired
	protected INotesService noteService;
	@Autowired
	protected ICurrenciesService currencyService;
	@Autowired
	protected IFinancialSourcesService financialSourceService;
	@Autowired
	protected IAverageMethodService averageMethodService;
	@Autowired
	protected ITimeSerieService timeSerieService;
	@Autowired
	protected IIataPCService iataPCService;

	/**
	 * @param customer
	 * @param tenderHolderContact
	 * @return
	 * @throws BusinessException
	 */
	protected Contacts getContact(Customer customer, Contact tenderHolderContact) throws BusinessException {
		if (tenderHolderContact.getContactPerson().trim().isEmpty()) {
			throw new BusinessException(BusinessErrorCode.CONTACT_NULL, BusinessErrorCode.CONTACT_NULL.getErrMsg());
		}

		String contactName = tenderHolderContact.getContactPerson();

		String firstName;
		String lastName = "";

		int nameSeparator = contactName.indexOf(' ');
		if (nameSeparator != -1) {
			lastName = contactName.substring(nameSeparator + 1, contactName.length());
			firstName = contactName.substring(0, nameSeparator);
		} else {
			firstName = contactName;
		}

		String email = tenderHolderContact.getEmail();

		Contacts contact = this.findContact(customer, email, firstName, lastName);

		if (contact == null) {
			contact = new Contacts();
			contact.setActive(Boolean.TRUE);
			contact.setIsPrimary(Boolean.FALSE);
			contact.setFirstName(firstName);
			contact.setLastName(lastName);
			contact.setEmail(email);
			contact.setBusinessPhone(tenderHolderContact.getPhone());

			contact.setObjectId(customer.getId());
			contact.setObjectType(Customer.class.getSimpleName());

			SoneEnumUtils.setEmptyValues(contact);
			this.contactService.insert(contact);
		}

		return contact;
	}

	/**
	 * @param code
	 * @param property
	 * @return
	 * @throws SingleResultException
	 */
	protected Unit findUnit(PUOMBase code, String property) throws SingleResultException {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put(IATA_CODE, code.value());
			return this.unitService.findEntityByAttributes(params);
		} catch (ApplicationException nre) {
			throw new SingleResultException(String.format(CmmErrorCode.SINGLE_RESULT_EXCEPTION_MESSAGE.getErrMsg(), "Unit", code, property), nre);
		} catch (Exception e) {
			LOGGER.warn("Unknown error when extracting unit :" + e.getLocalizedMessage(), e);
			throw new SingleResultException(
					String.format(CmmErrorCode.SINGLE_RESULT_EXCEPTION_MESSAGE.getErrMsg(), "Unit", code, property) + "[" + e.getMessage() + "]", e);
		}
	}

	/**
	 * @param customer
	 * @param email
	 * @param firstName
	 * @param lastName
	 * @return
	 */
	protected Contacts findContact(Customer customer, String email, String firstName, String lastName) {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put(OBJECT_ID, customer.getId());
			params.put(OBJECT_TYPE, Customer.class.getSimpleName());
			params.put(FIRST_NAME, firstName);
			params.put(LAST_NAME, lastName);
			params.put(EMAIL2, email);

			return this.contactService.findEntityByAttributes(params);
		} catch (Exception e) {
			LOGGER.warn("Could not find Contact " + firstName + " " + lastName + " for Customer " + customer.getCode(), e);
			return null;
		}
	}

	/**
	 * @param code
	 * @param property
	 * @return
	 * @throws BusinessException
	 */
	protected Customer findCustomer(String code, String property) throws BusinessException {
		Customer customer = null;
		try {
			// First try and find customer by IATA Code
			Map<String, Object> params = new HashMap<>();
			params.put(IATA_CODE, code);

			customer = this.customerService.findEntityByAttributes(params);
		} catch (ApplicationException e) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(e.getErrorCode())) {
				LOGGER.warn("Duplicate customer found for iata code : " + code, e);
			} else {
				LOGGER.warn("Could not find customer with iata code : " + code, e);
			}
		}

		if (customer == null) {
			try {
				// If no customer is found by IATA Code then try and find by code
				customer = this.customerService.findByCode(code);
			} catch (ApplicationException nre1) {
				if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre1.getErrorCode())) {
					throw nre1;
				}
				throw new SingleResultException(String.format(CmmErrorCode.SINGLE_RESULT_EXCEPTION_MESSAGE.getErrMsg(), "Customer", code, property),
						nre1);
			}
		}

		return customer;
	}

	/**
	 * @param iataCode
	 * @param icaoCode
	 * @return
	 * @throws SingleResultException
	 */
	protected Locations findLocation(String iataCode, String icaoCode) throws SingleResultException {
		try {
			Map<String, Object> params = new HashMap<>();
			if (iataCode != null) {
				params.put(IATA_CODE, iataCode);
			} else {
				params.put(ICAO_CODE, icaoCode);
			}
			return this.locationsService.findEntityByAttributes(params);
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw new SingleResultException(nre.getMessage(), nre);
			}
			String code = iataCode != null ? iataCode : icaoCode;
			String property = iataCode != null ? "LocationCodeIATA" : "LocationCodeICAO";
			throw new SingleResultException(String.format(CmmErrorCode.SINGLE_RESULT_EXCEPTION_MESSAGE.getErrMsg(), "Location", code, property), nre);
		} catch (Exception e) {
			throw new SingleResultException(e.getMessage(), e);
		}
	}

	/**
	 * @param serviceLevel
	 * @return
	 */
	protected Integer getIataServiceLevel(ServiceLevel serviceLevel) {
		Integer iataServiceLevel = null;
		switch (serviceLevel) {
		case IT_1:
			iataServiceLevel = 1;
			break;
		case IT_2:
			iataServiceLevel = 2;
			break;
		case IT_3:
			iataServiceLevel = 3;
			break;
		case IT_4:
			iataServiceLevel = 4;
			break;
		}
		return iataServiceLevel;
	}

	/**
	 * @param code
	 * @param property
	 * @return
	 * @throws SingleResultException
	 */
	protected Currencies findCurrency(String code, String property) throws SingleResultException {
		try {
			return this.currencyService.findByCode(code);
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw new SingleResultException(nre.getMessage(), nre);
			}
			throw new SingleResultException(String.format(CmmErrorCode.SINGLE_RESULT_EXCEPTION_MESSAGE.getErrMsg(), "Currency", code, property), nre);
		} catch (Exception e) {
			throw new SingleResultException(e.getMessage(), e);
		}
	}

	/**
	 * @param code
	 * @param property
	 * @return
	 * @throws BusinessException
	 */
	protected FinancialSources findFinancialSource(String code, String property) throws BusinessException {
		try {
			return this.financialSourceService.findByCode(code);
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw new SingleResultException(nre.getMessage(), nre);
			}
			throw new SingleResultException(
					String.format(CmmErrorCode.SINGLE_RESULT_EXCEPTION_MESSAGE.getErrMsg(), "Financial Source", code, property), nre);
		} catch (Exception e) {
			throw new SingleResultException(e.getMessage(), e);
		}
	}

	/**
	 * @param name
	 * @param property
	 * @return
	 * @throws BusinessException
	 */
	protected AverageMethod findAverageMethod(String name, String property) throws BusinessException {
		try {
			return this.averageMethodService.findByName(name);
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw new SingleResultException(nre.getMessage(), nre);
			}
			throw new SingleResultException(String.format(CmmErrorCode.SINGLE_RESULT_EXCEPTION_MESSAGE.getErrMsg(), "Average Method", name, property),
					nre);
		} catch (Exception e) {
			throw new SingleResultException(e.getMessage(), e);
		}
	}

	/**
	 * @param code
	 * @return
	 * @throws SingleResultException
	 */
	protected IataPC findIATAPC(String code) throws SingleResultException {
		try {
			return this.iataPCService.findByCode(code);
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw new SingleResultException(nre.getMessage(), nre);
			}
			throw new SingleResultException(
					String.format(CmmErrorCode.SINGLE_RESULT_EXCEPTION_MESSAGE.getErrMsg(), "IATA Price category", code, "ItemProductID"), nre);
		} catch (Exception e) {
			throw new SingleResultException(e.getMessage(), e);
		}
	}

}
