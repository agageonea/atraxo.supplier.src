package atraxo.cmm.business.ext.bpm.delegate.contract.approval;

import atraxo.cmm.business.ext.bpm.delegate.CalculateVolumesDelegate;
import atraxo.cmm.domain.impl.contracts.Contract;

/**
 * @author vhojda
 */
public class ContractApprovalCalculateVolumesDelegate extends CalculateVolumesDelegate {

	@Override
	protected Contract getCurrentContract() throws Exception {
		return this.getContract();
	}

}
