package atraxo.cmm.business.ext.contracts.delegate;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import atraxo.cmm.business.api.contracts.IPricingBaseService;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.prices.delegate.CompositePriceCategoryBd;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class PricingBase_Bd extends AbstractBusinessDelegate {

	public static final String INDEX = "Index";

	/**
	 * Verify the dates. Valid "to" must be greater than valid "from". If the valid "from" is bigger than valid "to" throw an exception.
	 *
	 * @param from - Starting date.
	 * @param to - Ending date.
	 * @throws BusinessException
	 */
	public void verifyDates(Date from, Date to) throws BusinessException {
		if (from != null && to != null && from.after(to)) {
			throw new BusinessException(CmmErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
					CmmErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO.getErrMsg());
		}
	}

	/**
	 * Save the price category.
	 *
	 * @param priceCategory
	 * @param pb
	 * @param cpc
	 * @returns a new created <code>ContractPriceCategory</code>
	 * @throws BusinessException
	 */

	public ContractPriceCategory createPriceCtgry(PriceCategory priceCategory, PricingBase pb, ContractPriceCategory cpc) throws BusinessException {
		IContractPriceCategoryService pService = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
		cpc = this.buildContractPriceCategory(priceCategory, pb, cpc);
		boolean add = true;
		if (cpc.getId() != null) {
			for (ContractPriceCategory cat : pb.getContractPriceCategories()) {
				if (cpc.getId().equals(cat.getId())) {
					add = false;
				}
			}
		}
		if (add) {
			pb.addToContractPriceCategories(cpc);
		}
		pService.insertWithoutPersist(cpc);
		return cpc;
	}

	/**
	 * @param priceCategory
	 * @param pb
	 * @param cpc
	 * @returns builds a <code>ContractPriceCategory</code> using a <code>PricingBase</code> and a <code>PriceCategory</code>
	 * @throws BusinessException
	 */

	private ContractPriceCategory buildContractPriceCategory(PriceCategory priceCategory, PricingBase pb, ContractPriceCategory cpc)
			throws BusinessException {
		IPricingBaseService pbService = (IPricingBaseService) this.findEntityService(PricingBase.class);
		if (cpc == null) {
			cpc = new ContractPriceCategory();
		}

		cpc.setName(pb.getDescription());
		cpc.setContract(pb.getContract());
		cpc.setVat(pb.getVat());
		IEntityService<PricingBase> srv = this.findEntityService(PricingBase.class);
		AverageMethod method = pb.getContract().getAverageMethod();

		if (pb.getAverageMethodId() != null) {
			method = srv.findById(pb.getAverageMethodId(), AverageMethod.class);
		}

		cpc.setAverageMethod(method);
		FinancialSources source = pb.getContract().getFinancialSource();

		if (pb.getFinancialSourceId() != null) {
			source = srv.findById(pb.getFinancialSourceId(), FinancialSources.class);
		}

		cpc.setFinancialSource(source);
		cpc.setRestriction(pb.getRestriction() != null ? pb.getRestriction() : Boolean.FALSE);
		cpc.setExchangeRateOffset(pb.getExchangeRateOffset() == null || MasterAgreementsPeriod._EMPTY_.equals(pb.getExchangeRateOffset())
				? pb.getContract().getExchangeRateOffset() : pb.getExchangeRateOffset());
		cpc.setPriceCategory(priceCategory);
		cpc.setPricingBases(pb);
		cpc.setInitialPrice(pb.getInitialPrice());
		cpc.setInitialCurrId(pb.getInitialCurrId());
		cpc.setInitialUnitId(pb.getInitialUnitId());
		cpc.setQuantityType(pb.getQuantityType());

		if (!CalculateIndicator._CALCULATE_ONLY_.equals(cpc.getCalculateIndicator())) {
			cpc.setCalculateIndicator(CalculateIndicator._INCLUDED_);
		}

		cpc.setContinous(pbService.isProduct(pb) || pb.getContinous() == null ? Boolean.TRUE : pb.getContinous());
		cpc.setDefaultPriceCtgy(pb.getDefauftPC());
		this.getBusinessDelegate(CompositePriceCategoryBd.class).assignParentPriceCategory(pb.getPriceCategoryIdsJson(), cpc);
		this.getBusinessDelegate(CompositePriceCategoryBd.class).updateParentPriceCategories(cpc, CalculateIndicator._CALCULATE_ONLY_);
		return cpc;
	}

	/**
	 * Check if the new pricing base are the same period with an old pricing base.
	 *
	 * @param list - the existing pricing bases , PricingBase list
	 * @param e - the new PricingBase
	 * @throws BusinessException
	 */
	public void checkOverlapping(List<PricingBase> list, PricingBase e) throws BusinessException {
		for (PricingBase pb1 : list) {
			for (PricingBase pb2 : list) {
				if ((pb1.getValidFrom().equals(e.getValidFrom()) || pb2.getValidFrom().equals(e.getValidFrom()))
						&& (pb1.getValidTo().equals(e.getValidTo()) || pb2.getValidTo().equals(e.getValidTo()))) {
					throw new BusinessException(CmmErrorCode.PB_OVERLAPING, CmmErrorCode.PB_OVERLAPING.getErrMsg());
				}
			}
		}
	}

	/**
	 * Check if the pricing base period is bigger than the contact period.
	 *
	 * @param e - PricingBase
	 * @throws BusinessException
	 */
	public void checkPeriods(PricingBase e) throws BusinessException {
		if (e.getValidFrom().before(e.getContract().getValidFrom())) {
			throw new BusinessException(CmmErrorCode.PB_VALIDITY, CmmErrorCode.PB_VALIDITY.getErrMsg());
		}
		if (e.getValidTo().after(e.getContract().getValidTo())) {
			throw new BusinessException(CmmErrorCode.PB_VALIDITY, CmmErrorCode.PB_VALIDITY.getErrMsg());
		}
	}

	/**
	 * Check if the date is between the given dates.
	 *
	 * @param from - start of the period.
	 * @param to - period's end.
	 * @param date - the date which must be checked.
	 * @return True if date is between from and to. The period margins are not included.
	 */
	public boolean isBetweenDates(Date from, Date to, Date date) {
		return date.compareTo(from) > 0 && date.compareTo(to) < 0;
	}

	/**
	 * Check if the date is between the given dates.
	 *
	 * @param from - start of the period.
	 * @param to - period's end.
	 * @param date - the date which must be checked.
	 * @return True if date is between from and to. The period margins are * included.
	 */
	public boolean isBetweenDatesMarginIncluded(Date from, Date to, Date date) {
		return date.compareTo(from) >= 0 && date.compareTo(to) <= 0;
	}

	/**
	 * Calculate the last day of month.
	 *
	 * @param date - {@link Date}.
	 * @param quantity - {@link Integer}.
	 * @param dateUnit
	 * @return - {@link} last day of month.
	 */
	public Date calculateDateWithOffset(Date date, Integer quantity, Integer dateUnit) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(dateUnit, quantity);
		return cal.getTime();
	}
}
