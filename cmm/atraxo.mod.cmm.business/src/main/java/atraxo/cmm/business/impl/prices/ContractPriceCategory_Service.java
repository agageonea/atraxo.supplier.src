/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.prices;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ContractPriceCategory} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ContractPriceCategory_Service
		extends
			AbstractEntityService<ContractPriceCategory> {

	/**
	 * Public constructor for ContractPriceCategory_Service
	 */
	public ContractPriceCategory_Service() {
		super();
	}

	/**
	 * Public constructor for ContractPriceCategory_Service
	 * 
	 * @param em
	 */
	public ContractPriceCategory_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ContractPriceCategory> getEntityClass() {
		return ContractPriceCategory.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ContractPriceCategory
	 */
	public ContractPriceCategory findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							ContractPriceCategory.NQ_FIND_BY_BUSINESS,
							ContractPriceCategory.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ContractPriceCategory", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ContractPriceCategory", "id"), nure);
		}
	}

	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByContract(Contract contract) {
		return this.findByContractId(contract.getId());
	}
	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByContractId(Integer contractId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ContractPriceCategory e where e.clientId = :clientId and e.contract.id = :contractId",
						ContractPriceCategory.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractId", contractId).getResultList();
	}
	/**
	 * Find by reference: priceCategory
	 *
	 * @param priceCategory
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByPriceCategory(
			PriceCategory priceCategory) {
		return this.findByPriceCategoryId(priceCategory.getId());
	}
	/**
	 * Find by ID of reference: priceCategory.id
	 *
	 * @param priceCategoryId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByPriceCategoryId(
			Integer priceCategoryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ContractPriceCategory e where e.clientId = :clientId and e.priceCategory.id = :priceCategoryId",
						ContractPriceCategory.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("priceCategoryId", priceCategoryId)
				.getResultList();
	}
	/**
	 * Find by reference: pricingBases
	 *
	 * @param pricingBases
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByPricingBases(
			PricingBase pricingBases) {
		return this.findByPricingBasesId(pricingBases.getId());
	}
	/**
	 * Find by ID of reference: pricingBases.id
	 *
	 * @param pricingBasesId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByPricingBasesId(
			Integer pricingBasesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ContractPriceCategory e where e.clientId = :clientId and e.pricingBases.id = :pricingBasesId",
						ContractPriceCategory.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("pricingBasesId", pricingBasesId).getResultList();
	}
	/**
	 * Find by reference: financialSource
	 *
	 * @param financialSource
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByFinancialSource(
			FinancialSources financialSource) {
		return this.findByFinancialSourceId(financialSource.getId());
	}
	/**
	 * Find by ID of reference: financialSource.id
	 *
	 * @param financialSourceId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByFinancialSourceId(
			Integer financialSourceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ContractPriceCategory e where e.clientId = :clientId and e.financialSource.id = :financialSourceId",
						ContractPriceCategory.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("financialSourceId", financialSourceId)
				.getResultList();
	}
	/**
	 * Find by reference: averageMethod
	 *
	 * @param averageMethod
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByAverageMethod(
			AverageMethod averageMethod) {
		return this.findByAverageMethodId(averageMethod.getId());
	}
	/**
	 * Find by ID of reference: averageMethod.id
	 *
	 * @param averageMethodId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByAverageMethodId(
			Integer averageMethodId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ContractPriceCategory e where e.clientId = :clientId and e.averageMethod.id = :averageMethodId",
						ContractPriceCategory.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("averageMethodId", averageMethodId)
				.getResultList();
	}
	/**
	 * Find by reference: priceComponents
	 *
	 * @param priceComponents
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByPriceComponents(
			ContractPriceComponent priceComponents) {
		return this.findByPriceComponentsId(priceComponents.getId());
	}
	/**
	 * Find by ID of reference: priceComponents.id
	 *
	 * @param priceComponentsId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByPriceComponentsId(
			Integer priceComponentsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from ContractPriceCategory e, IN (e.priceComponents) c where e.clientId = :clientId and c.id = :priceComponentsId",
						ContractPriceCategory.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("priceComponentsId", priceComponentsId)
				.getResultList();
	}
	/**
	 * Find by reference: priceRestrictions
	 *
	 * @param priceRestrictions
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByPriceRestrictions(
			ContractPriceRestriction priceRestrictions) {
		return this.findByPriceRestrictionsId(priceRestrictions.getId());
	}
	/**
	 * Find by ID of reference: priceRestrictions.id
	 *
	 * @param priceRestrictionsId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByPriceRestrictionsId(
			Integer priceRestrictionsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from ContractPriceCategory e, IN (e.priceRestrictions) c where e.clientId = :clientId and c.id = :priceRestrictionsId",
						ContractPriceCategory.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("priceRestrictionsId", priceRestrictionsId)
				.getResultList();
	}
	/**
	 * Find by reference: childPriceCategory
	 *
	 * @param childPriceCategory
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByChildPriceCategory(
			ContractPriceCategory childPriceCategory) {
		return this.findByChildPriceCategoryId(childPriceCategory.getId());
	}
	/**
	 * Find by ID of reference: childPriceCategory.id
	 *
	 * @param childPriceCategoryId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByChildPriceCategoryId(
			Integer childPriceCategoryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from ContractPriceCategory e, IN (e.childPriceCategory) c where e.clientId = :clientId and c.id = :childPriceCategoryId",
						ContractPriceCategory.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("childPriceCategoryId", childPriceCategoryId)
				.getResultList();
	}
	/**
	 * Find by reference: parentPriceCategory
	 *
	 * @param parentPriceCategory
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByParentPriceCategory(
			ContractPriceCategory parentPriceCategory) {
		return this.findByParentPriceCategoryId(parentPriceCategory.getId());
	}
	/**
	 * Find by ID of reference: parentPriceCategory.id
	 *
	 * @param parentPriceCategoryId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByParentPriceCategoryId(
			Integer parentPriceCategoryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from ContractPriceCategory e, IN (e.parentPriceCategory) c where e.clientId = :clientId and c.id = :parentPriceCategoryId",
						ContractPriceCategory.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("parentPriceCategoryId", parentPriceCategoryId)
				.getResultList();
	}
}
