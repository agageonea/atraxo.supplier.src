package atraxo.cmm.business.ws.tender.transformer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import atraxo.cmm.domain.ext.tender.MessageStatus;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.customer.Customer;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcknowledge;

public class TenderAcknowledgmentTransformer extends DataHubOutTransformer<Tender, FuelTenderAcknowledge> {

	@Override
	public FuelTenderAcknowledge transformModelToDTO(Tender modelEntity, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException {
		FuelTenderAcknowledge acknowledge = new FuelTenderAcknowledge();
		acknowledge.setTenderIdentification(this.buildTenderIdentification(modelEntity, msgId));

		Map<String, Object> params = new HashMap<>();
		params.put("isSubsidiary", true);
		params.put("code", Session.user.get().getClientCode());
		Customer subsidiary = this.customerService.findEntityByAttributes(params);

		if (subsidiary.getIataCode() != null) {
			acknowledge.setBidderCode(subsidiary.getIataCode());
		} else {
			acknowledge.setBidderCode(subsidiary.getCode());
		}

		String type = this.setMessageType(receiverInterface);
		acknowledge.setMessageType(type);
		acknowledge.setMessageStatus(seava.j4e.iata.fuelplus.iata.tender.MessageStatus.fromValue(MessageStatus.RECEIVED.getIataCode()));
		acknowledge.setRefMsgId(refMsgId);
		acknowledge.setSchemaVersion(modelEntity.getSchemaVersion());
		return acknowledge;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubTransformer#transformModelToDTO(atraxo.fmbas.domain.impl.abstracts.AbstractEntity, long,
	 * java.lang.String)
	 */
	@Override
	public FuelTenderAcknowledge transformModelToDTO(Tender modelEntity, long msgId, String reason) throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubTransformer#transformModelsToDTO(java.util.List, long, java.lang.String)
	 */
	@Override
	public FuelTenderAcknowledge transformModelsToDTO(List<Tender> modelEntities, long msgId, String reason) throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubTransformer#transformModelsToDTO(java.util.List, long, long,
	 * atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames)
	 */
	@Override
	public FuelTenderAcknowledge transformModelsToDTO(List<Tender> modelEntities, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

}
