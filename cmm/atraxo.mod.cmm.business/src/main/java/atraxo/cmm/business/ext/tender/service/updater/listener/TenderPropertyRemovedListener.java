package atraxo.cmm.business.ext.tender.service.updater.listener;

import java.beans.PropertyChangeEvent;
import java.util.Arrays;
import java.util.EventObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.cmm.domain.impl.tender.TenderLocationExpectedPrice;
import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import seava.j4e.api.exceptions.ActionNotSupportedException;
import seava.j4e.commons.utils.event.PropertyRemovedEvent;
import seava.j4e.commons.utils.listener.ChangeListener;
import seava.j4e.commons.utils.listener.changes.ObjectRemoved;

/**
 * Builds the message which will be logged to history of the Tender
 *
 * @author abolindu
 */
public class TenderPropertyRemovedListener extends ChangeListener {

	private Logger LOG = LoggerFactory.getLogger(TenderPropertyRemovedListener.class);

	private static final String TENDER_LOCATION_REMOVED = "2007";
	private static final String TENDER_RECEIVER_REMOVED = "2008";
	private static final String TENDER_ROUND_REMOVED = "2009";
	private static final String TENDER_EXPECTED_PRICES_REMOVED = "2010";

	@Override
	public void propertyChange(PropertyChangeEvent evt) {

		if (evt instanceof PropertyRemovedEvent) {
			try {
				this.propertyRemoved(evt);
			} catch (ActionNotSupportedException e) {
				if (this.LOG.isDebugEnabled()) {
					this.LOG.debug(e.getMessage(), e);
				}
			}
		}
	}

	private void propertyRemoved(EventObject event) throws ActionNotSupportedException {
		Object affectedEntity = ((PropertyRemovedEvent) event).getRemovedEntity();
		Object sourceEntity = ((PropertyRemovedEvent) event).getSourceEntity();
		String message = getMessageCode(affectedEntity != null ? affectedEntity : sourceEntity);
		ObjectRemoved objectRemoved = new ObjectRemoved(affectedEntity, sourceEntity, message);
		this.changes.addToObjectRemovedList(Arrays.asList(objectRemoved));
	}

	private static String getMessageCode(Object entity) throws ActionNotSupportedException {
		if (entity instanceof TenderLocation) {
			return TENDER_LOCATION_REMOVED;
		} else if (entity instanceof TenderLocationAirlines) {
			return TENDER_RECEIVER_REMOVED;
		} else if (entity instanceof TenderLocationRound) {
			return TENDER_ROUND_REMOVED;
		} else if (entity instanceof TenderLocationExpectedPrice) {
			return TENDER_EXPECTED_PRICES_REMOVED;
		} else {
			throw new ActionNotSupportedException("No message defined for entity: " + entity.getClass());
		}
	}

}
