package atraxo.cmm.business.ext.bpm.delegate.contractShipTo.approval;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate;

/**
 * @author abolindu
 */
public class RejectShipToUpdateDelegate extends AbstractJavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(RejectShipToUpdateDelegate.class);

	@Autowired
	private IContractService contractSrv;

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#execute()
	 */
	@Override
	public void execute() throws Exception {
		Object contractIdString = this.execution.getVariable(WorkflowVariablesConstants.CONTRACT_ID_VAR);
		if (contractIdString != null) {
			Integer contractId = Integer.parseInt(contractIdString.toString());

			Contract contract = null;
			try {
				contract = this.contractSrv.findByBusiness(contractId);
			} catch (Exception e) {
				LOGGER.error("ERROR:could not find a contract for ID " + contractId, e);
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						String.format(CmmErrorCode.CONTRACT_WORKFLOW_ENTITY_NOT_EXISTING.getErrMsg(), contractId), e);
			}
			if (contract != null) {
				this.contractSrv.rejectShipToUpdate(contract, this.getNote());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#getSessionUserCode()
	 */
	@Override
	public String getSessionUserCode() {
		return this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME);
	}
}
