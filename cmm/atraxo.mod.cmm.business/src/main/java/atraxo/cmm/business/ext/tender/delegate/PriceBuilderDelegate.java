package atraxo.cmm.business.ext.tender.delegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.ext.contracts.delegate.PricingBase_Bd;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.PricingType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.fmbas.business.ext.util.EntityCloner;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;
import seava.j4e.commons.utils.SoneEnumUtils;

/**
 * @author zspeter
 */
public class PriceBuilderDelegate extends AbstractBusinessDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(PriceBuilderDelegate.class);

	public Collection<PricingBase> getPricingBases(Contract contract, Contract newContract)
			throws InstantiationException, IllegalAccessException, BusinessException {

		Collection<PricingBase> retList = new ArrayList<>();
		HashMap<Integer, ContractPriceCategory> map = new HashMap<>();
		List<PricingBase> usedPbList = new ArrayList<>();
		List<PricingBase> pbList = new ArrayList<>(contract.getPricingBases());

		Collections.sort(pbList, this::sortByContractPriceCategoryParent);

		for (PricingBase pb : pbList) {
			this.getPricingBase(newContract, retList, map, usedPbList, pb, 0);
		}

		return retList;
	}

	/**
	 * @param o1
	 * @param o2
	 * @return
	 */
	private int sortByContractPriceCategoryParent(PricingBase o1, PricingBase o2) {
		ContractPriceCategory cpc1;
		ContractPriceCategory cpc2;
		if (o1.getContractPriceCategories() == null || o2.getContractPriceCategories() == null || o1.getContractPriceCategories().size() != 1
				|| o2.getContractPriceCategories().size() != 1) {
			return 0;
		} else {
			cpc1 = o1.getContractPriceCategories().iterator().next();
			cpc2 = o2.getContractPriceCategories().iterator().next();
		}
		if (CollectionUtils.isEmpty(cpc1.getParentPriceCategory()) && !CollectionUtils.isEmpty(cpc2.getParentPriceCategory())) {
			return -1;
		}
		if (CollectionUtils.isEmpty(cpc1.getParentPriceCategory()) && CollectionUtils.isEmpty(cpc2.getParentPriceCategory())) {
			return 1;
		}
		return 0;
	}

	/**
	 * @param newContract
	 * @param retList
	 * @param map
	 * @param usedPbList
	 * @param sourcePb
	 * @param counter
	 * @throws BusinessException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	private void getPricingBase(Contract newContract, Collection<PricingBase> retList, HashMap<Integer, ContractPriceCategory> map,
			List<PricingBase> usedPbList, PricingBase sourcePb, int counter)
			throws BusinessException, InstantiationException, IllegalAccessException {

		if (usedPbList.contains(sourcePb)) {
			return;
		}

		if (counter > 2) {
			throw new BusinessException(CmmErrorCode.MORE_THAN_TWO_PERCENTAGE, CmmErrorCode.MORE_THAN_TWO_PERCENTAGE.getErrMsg());
		}

		for (ContractPriceCategory cpc : sourcePb.getContractPriceCategories()) {
			for (ContractPriceCategory cpcParent : cpc.getParentPriceCategory()) {
				if (!map.containsKey(cpcParent.getId())) {
					this.getPricingBase(newContract, retList, map, usedPbList, cpcParent.getPricingBases(), counter + 1);
				}
			}
		}

		// set new contract data on the pricing base
		PricingBase newPb = EntityCloner.cloneEntity(sourcePb);
		newPb.setContract(newContract);
		newPb.setValidFrom(newContract.getValidFrom());
		newPb.setValidTo(newContract.getValidTo());

		SoneEnumUtils.setEmptyValues(newPb);

		// for index regenerate price components and conversions
		if (newPb.getPriceCat().getName().equals(PricingType._INDEX_.getName())) {
			newPb.setContractPriceCategories(this.buildIndex(sourcePb, newPb));
		} else {
			newPb.setContractPriceCategories(this.getPriceCategory(sourcePb, newPb, map));
		}

		usedPbList.add(sourcePb);
		retList.add(newPb);
	}

	private Collection<ContractPriceCategory> buildIndex(PricingBase pb, PricingBase newPb)
			throws InstantiationException, IllegalAccessException, BusinessException {

		this.setTransientFields(pb, newPb);

		Collection<ContractPriceCategory> retList = new ArrayList<>();
		Collection<ContractPriceCategory> inList = new ArrayList<>(pb.getContractPriceCategories());

		for (ContractPriceCategory cpc : inList) {
			ContractPriceCategory newCpc = new ContractPriceCategory();

			newPb.setVat(cpc.getVat());
			newCpc.setPriceRestrictions(this.getPriceRestrictions(cpc, newCpc));

			newCpc = this.getBusinessDelegate(PricingBase_Bd.class).createPriceCtgry(newPb.getPriceCat(), newPb, newCpc);

			retList.add(newCpc);
		}
		return retList;
	}

	private void setTransientFields(PricingBase pb, PricingBase newPb) {
		ContractPriceCategory category = !pb.getContractPriceCategories().isEmpty() ? pb.getContractPriceCategories().iterator().next() : null;
		if (category != null) {
			newPb.setDefauftPC(category.getDefaultPriceCtgy());
			newPb.setInitialPrice(category.getInitialPrice());
			newPb.setInitialCurrId(category.getInitialCurrId());
			newPb.setInitialUnitId(category.getInitialUnitId());
			newPb.setQuantityType(category.getQuantityType());
			newPb.setContinous(category.getContinous());
			newPb.setRestriction(category.getRestriction());
			newPb.setFinancialSourceId(category.getFinancialSource().getId());
			newPb.setAverageMethodId(category.getAverageMethod().getId());
			newPb.setExchangeRateOffset(category.getExchangeRateOffset());
		}
	}

	private Collection<ContractPriceCategory> getPriceCategory(PricingBase pb, PricingBase newPb, HashMap<Integer, ContractPriceCategory> map)
			throws InstantiationException, IllegalAccessException {

		Collection<ContractPriceCategory> retList = new ArrayList<>();
		for (ContractPriceCategory cpc : pb.getContractPriceCategories()) {
			ContractPriceCategory newCpc = EntityCloner.cloneEntity(cpc);
			newCpc.setPricingBases(newPb);
			newCpc.setContract(newPb.getContract());
			newCpc.setPriceRestrictions(this.getPriceRestrictions(cpc, newCpc));
			newCpc.setPriceComponents(this.getPriceComponent(cpc, newCpc));
			newCpc.setChildPriceCategory(new ArrayList<ContractPriceCategory>());

			map.put(cpc.getId(), newCpc);
			if (cpc.getParentPriceCategory() != null && !cpc.getParentPriceCategory().isEmpty()) {
				newCpc.setParentPriceCategory(new ArrayList<ContractPriceCategory>());
				for (ContractPriceCategory cpcParent : cpc.getParentPriceCategory()) {
					if (map.containsKey(cpcParent.getId())) {
						newCpc.getParentPriceCategory().add(map.get(cpcParent.getId()));
						cpcParent.getChildPriceCategory().add(newCpc);
					}
				}
			}
			retList.add(newCpc);
		}
		return retList;
	}

	private Collection<ContractPriceRestriction> getPriceRestrictions(ContractPriceCategory cpc, ContractPriceCategory newCpc)
			throws InstantiationException, IllegalAccessException {

		Collection<ContractPriceRestriction> retList = new ArrayList<>();
		for (ContractPriceRestriction cpr : cpc.getPriceRestrictions()) {
			ContractPriceRestriction newCpr = EntityCloner.cloneEntity(cpr);
			newCpr.setContractPriceCategory(newCpc);
			retList.add(newCpr);
		}

		return retList;
	}

	private Collection<ContractPriceComponent> getPriceComponent(ContractPriceCategory cpc, ContractPriceCategory newCpc)
			throws InstantiationException, IllegalAccessException {

		Collection<ContractPriceComponent> retList = new ArrayList<>();
		List<ContractPriceComponent> priceComponents = new ArrayList<>(cpc.getPriceComponents());

		if (!CollectionUtils.isEmpty(priceComponents)) {
			priceComponents.sort((p1, p2) -> p2.getValidFrom().compareTo(p1.getValidFrom()));

			ContractPriceComponent cpr = priceComponents.iterator().next();

			ContractPriceComponent newE = EntityCloner.cloneEntity(cpr);
			newE.setContrPriceCtgry(newCpc);
			newE.setValidFrom(newCpc.getContract().getValidFrom());
			newE.setValidTo(newCpc.getContract().getValidTo());
			newE.setConvertedPrices(this.getPriceComponentConverted(cpr, newE));

			retList.add(newE);
		}

		return retList;
	}

	private Collection<ContractPriceComponentConv> getPriceComponentConverted(ContractPriceComponent e, ContractPriceComponent newCpc)
			throws InstantiationException, IllegalAccessException {

		Collection<ContractPriceComponentConv> retList = new ArrayList<>();
		for (ContractPriceComponentConv cpr : e.getConvertedPrices()) {
			ContractPriceComponentConv newE = EntityCloner.cloneEntity(cpr);
			newE.setValidFrom(newCpc.getValidFrom());
			newE.setValidTo(newCpc.getValidTo());
			newE.setContractPriceComponent(newCpc);
			retList.add(newE);
		}
		return retList;
	}

}
