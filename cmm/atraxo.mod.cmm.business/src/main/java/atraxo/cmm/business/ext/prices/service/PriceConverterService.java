package atraxo.cmm.business.ext.prices.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.financialSources.IFinancialSourcesService;
import atraxo.fmbas.business.api.timeserie.IAverageMethodService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.ext.types.timeseries.AverageMethodCode;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;
import seava.j4e.business.utils.DateUtils;

public class PriceConverterService extends AbstractBusinessBaseService {

	@Autowired
	private UnitConverterService unitConverterService;
	@Autowired
	private IUnitService unitService;
	@Autowired
	private ICurrenciesService currencyService;
	@Autowired
	private IFinancialSourcesService finSrcService;
	@Autowired
	private IAverageMethodService avgMthdService;

	/**
	 * Convert price.
	 *
	 * @param fromUnit
	 * @param toUnit
	 * @param fromCurrency
	 * @param toCurrency
	 * @param value
	 * @param exchRateDate
	 * @param financialSource
	 * @param averageMethod
	 * @param density
	 * @param strict
	 * @return
	 * @throws BusinessException
	 */
	public ConversionResult convert(Unit fromUnit, Unit toUnit, Currencies fromCurrency, Currencies toCurrency, BigDecimal value, Date exchRateDate,
			FinancialSources financialSource, AverageMethod averageMethod, Density density, boolean strict, MasterAgreementsPeriod offset)
			throws BusinessException {
		ConversionResult result = new ConversionResult(value, BigDecimal.ONE);
		if (!fromCurrency.getId().equals(toCurrency.getId())) {
			ExchangeRate_Bd exchangeRateBd = this.getBusinessDelegate(ExchangeRate_Bd.class);
			exchRateDate = DateUtils.modifyDate(exchRateDate, AverageMethodCode.getByName(averageMethod.getCode()).getCalField(), offset.getDelta());
			result = exchangeRateBd.convert(fromCurrency, toCurrency, exchRateDate, value, strict, averageMethod, financialSource);
		}
		if (!fromUnit.getId().equals(toUnit.getId())) {
			result.setValue(this.unitConverterService.convert(toUnit, fromUnit, result.getValue(), density));
		}
		return result;
	}

	public ConversionResult convert(Unit fromUnit, Unit toUnit, Currencies fromCurrency, Currencies toCurrency, BigDecimal value, Date exchRateDate,
			FinancialSources financialSource, AverageMethod averageMethod, PricingBase pricingBase, boolean strict, MasterAgreementsPeriod offset)
			throws BusinessException {
		ConversionResult result = new ConversionResult(value, BigDecimal.ONE);
		if (!fromCurrency.getId().equals(toCurrency.getId())) {
			ExchangeRate_Bd exchangeRateBd = this.getBusinessDelegate(ExchangeRate_Bd.class);
			exchRateDate = DateUtils.modifyDate(exchRateDate, AverageMethodCode.valueOf(averageMethod.getCode().toUpperCase()).getCalField(),
					offset.getDelta());
			result = exchangeRateBd.convert(fromCurrency, toCurrency, exchRateDate, value, strict, averageMethod, financialSource);
		}
		if (!fromUnit.getId().equals(toUnit.getId())) {
			Operator operator = pricingBase.getOperator();
			BigDecimal factor = pricingBase.getFactor();
			switch (operator) {
			case _DIVIDE_:
				factor = BigDecimal.ONE.divide(factor, MathContext.DECIMAL64);
				break;
			case _MULTIPLY_:
			default:
			}
			result.setValue(this.unitConverterService.convert(toUnit, fromUnit, result.getValue(), factor, pricingBase.getConvUnit(),
					pricingBase.getQuotation().getUnit()));
		}
		return result;
	}

	public ConversionResult convert(String fromUnitCode, String toUnitCode, String fromCurrencyCode, String toCurrencyCode, BigDecimal value,
			Date exchRateDate, int financialSourceId, int averageMethodId, PricingBase pricingBase, boolean strict, MasterAgreementsPeriod offset)
			throws BusinessException {
		Unit fromUnit = this.unitService.findByCode(fromUnitCode);
		Unit toUnit = this.unitService.findByCode(toUnitCode);

		Currencies fromCurrency = this.currencyService.findByCode(fromCurrencyCode);
		Currencies toCurrency = this.currencyService.findByCode(toCurrencyCode);

		FinancialSources financialSource = this.finSrcService.findById(financialSourceId);

		AverageMethod averageMethod = this.avgMthdService.findById(averageMethodId);

		return this.convert(fromUnit, toUnit, fromCurrency, toCurrency, value, exchRateDate, financialSource, averageMethod, pricingBase, strict,
				offset);

	}

	public ConversionResult convert(int fromUnitId, int toUnitId, int fromCurrencyId, int toCurrencyId, BigDecimal value, Date exchRateDate,
			int financialSourceId, int averageMethodId, Density density, boolean strict, MasterAgreementsPeriod offset) throws BusinessException {

		Unit fromUnit = this.unitService.findById(fromUnitId);
		Unit toUnit = this.unitService.findById(toUnitId);

		Currencies fromCurrency = this.currencyService.findById(fromCurrencyId);
		Currencies toCurrency = this.currencyService.findById(toCurrencyId);

		FinancialSources financialSource = this.finSrcService.findById(financialSourceId);

		AverageMethod averageMethod = this.avgMthdService.findById(averageMethodId);
		return this.convert(fromUnit, toUnit, fromCurrency, toCurrency, value, exchRateDate, financialSource, averageMethod, density, strict, offset);
	}

	public ConversionResult convert(String fromUnitCode, String toUnitCode, String fromCurrencyCode, String toCurrencyCode, BigDecimal value,
			Date exchRateDate, String financialSourceCode, String averageMethodCode, Density density, boolean strict, MasterAgreementsPeriod offset)
			throws BusinessException {

		Unit fromUnit = this.unitService.findByCode(fromUnitCode);
		Unit toUnit = this.unitService.findByCode(toUnitCode);

		Currencies fromCurrency = this.currencyService.findByCode(fromCurrencyCode);
		Currencies toCurrency = this.currencyService.findByCode(toCurrencyCode);

		FinancialSources financialSource = this.finSrcService.findByCode(financialSourceCode);

		AverageMethod averageMethod = this.avgMthdService.findByName(averageMethodCode);

		return this.convert(fromUnit, toUnit, fromCurrency, toCurrency, value, exchRateDate, financialSource, averageMethod, density, strict, offset);
	}

	public ConversionResult convert(String fromUnitCode, String toUnitCode, String fromCurrencyCode, String toCurrencyCode, BigDecimal value,
			Date exchRateDate, int financialSourceId, int averageMethodId, Density density, boolean strict, MasterAgreementsPeriod offset)
			throws BusinessException {

		Unit fromUnit = this.unitService.findByCode(fromUnitCode);
		Unit toUnit = this.unitService.findByCode(toUnitCode);

		Currencies fromCurrency = this.currencyService.findByCode(fromCurrencyCode);
		Currencies toCurrency = this.currencyService.findByCode(toCurrencyCode);

		FinancialSources financialSource = this.finSrcService.findById(financialSourceId);

		AverageMethod averageMethod = this.avgMthdService.findById(averageMethodId);

		return this.convert(fromUnit, toUnit, fromCurrency, toCurrency, value, exchRateDate, financialSource, averageMethod, density, strict, offset);
	}

}
