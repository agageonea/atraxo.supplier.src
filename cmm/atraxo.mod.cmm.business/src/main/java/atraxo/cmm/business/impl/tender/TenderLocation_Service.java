/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.tender;

import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.cmm.domain.impl.tender.TenderLocationExpectedPrice;
import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link TenderLocation} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class TenderLocation_Service
		extends
			AbstractEntityService<TenderLocation> {

	/**
	 * Public constructor for TenderLocation_Service
	 */
	public TenderLocation_Service() {
		super();
	}

	/**
	 * Public constructor for TenderLocation_Service
	 * 
	 * @param em
	 */
	public TenderLocation_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<TenderLocation> getEntityClass() {
		return TenderLocation.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return TenderLocation
	 */
	public TenderLocation findByCode(Locations location, Tender tender,
			TaxType taxType, Product fuelProduct,
			FlightServiceType flightServiceType, ContractSubType deliveryPoint) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(TenderLocation.NQ_FIND_BY_CODE,
							TenderLocation.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("location", location)
					.setParameter("tender", tender)
					.setParameter("taxType", taxType)
					.setParameter("fuelProduct", fuelProduct)
					.setParameter("flightServiceType", flightServiceType)
					.setParameter("deliveryPoint", deliveryPoint)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(
					J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TenderLocation",
							"location, tender, taxType, fuelProduct, flightServiceType, deliveryPoint"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(
					J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TenderLocation",
							"location, tender, taxType, fuelProduct, flightServiceType, deliveryPoint"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return TenderLocation
	 */
	public TenderLocation findByCode(Long locationId, Long tenderId,
			TaxType taxType, Product fuelProduct,
			FlightServiceType flightServiceType, ContractSubType deliveryPoint) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(TenderLocation.NQ_FIND_BY_CODE_PRIMITIVE,
							TenderLocation.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("locationId", locationId)
					.setParameter("tenderId", tenderId)
					.setParameter("taxType", taxType)
					.setParameter("fuelProduct", fuelProduct)
					.setParameter("flightServiceType", flightServiceType)
					.setParameter("deliveryPoint", deliveryPoint)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(
					J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TenderLocation",
							"locationId, tenderId, taxType, fuelProduct, flightServiceType, deliveryPoint"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(
					J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TenderLocation",
							"locationId, tenderId, taxType, fuelProduct, flightServiceType, deliveryPoint"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return TenderLocation
	 */
	public TenderLocation findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(TenderLocation.NQ_FIND_BY_BUSINESS,
							TenderLocation.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TenderLocation", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TenderLocation", "id"), nure);
		}
	}

	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByLocation(Locations location) {
		return this.findByLocationId(location.getId());
	}
	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByLocationId(Integer locationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocation e where e.clientId = :clientId and e.location.id = :locationId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("locationId", locationId).getResultList();
	}
	/**
	 * Find by reference: tender
	 *
	 * @param tender
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByTender(Tender tender) {
		return this.findByTenderId(tender.getId());
	}
	/**
	 * Find by ID of reference: tender.id
	 *
	 * @param tenderId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByTenderId(Integer tenderId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocation e where e.clientId = :clientId and e.tender.id = :tenderId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("tenderId", tenderId).getResultList();
	}
	/**
	 * Find by reference: volumeUnit
	 *
	 * @param volumeUnit
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByVolumeUnit(Unit volumeUnit) {
		return this.findByVolumeUnitId(volumeUnit.getId());
	}
	/**
	 * Find by ID of reference: volumeUnit.id
	 *
	 * @param volumeUnitId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByVolumeUnitId(Integer volumeUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocation e where e.clientId = :clientId and e.volumeUnit.id = :volumeUnitId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("volumeUnitId", volumeUnitId).getResultList();
	}
	/**
	 * Find by reference: densityWeightUnit
	 *
	 * @param densityWeightUnit
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByDensityWeightUnit(Unit densityWeightUnit) {
		return this.findByDensityWeightUnitId(densityWeightUnit.getId());
	}
	/**
	 * Find by ID of reference: densityWeightUnit.id
	 *
	 * @param densityWeightUnitId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByDensityWeightUnitId(
			Integer densityWeightUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocation e where e.clientId = :clientId and e.densityWeightUnit.id = :densityWeightUnitId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("densityWeightUnitId", densityWeightUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: densityVolumeUnit
	 *
	 * @param densityVolumeUnit
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByDensityVolumeUnit(Unit densityVolumeUnit) {
		return this.findByDensityVolumeUnitId(densityVolumeUnit.getId());
	}
	/**
	 * Find by ID of reference: densityVolumeUnit.id
	 *
	 * @param densityVolumeUnitId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByDensityVolumeUnitId(
			Integer densityVolumeUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocation e where e.clientId = :clientId and e.densityVolumeUnit.id = :densityVolumeUnitId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("densityVolumeUnitId", densityVolumeUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: densityConvFromUnit
	 *
	 * @param densityConvFromUnit
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByDensityConvFromUnit(
			Unit densityConvFromUnit) {
		return this.findByDensityConvFromUnitId(densityConvFromUnit.getId());
	}
	/**
	 * Find by ID of reference: densityConvFromUnit.id
	 *
	 * @param densityConvFromUnitId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByDensityConvFromUnitId(
			Integer densityConvFromUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocation e where e.clientId = :clientId and e.densityConvFromUnit.id = :densityConvFromUnitId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("densityConvFromUnitId", densityConvFromUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: densityConvToUnit
	 *
	 * @param densityConvToUnit
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByDensityConvToUnit(Unit densityConvToUnit) {
		return this.findByDensityConvToUnitId(densityConvToUnit.getId());
	}
	/**
	 * Find by ID of reference: densityConvToUnit.id
	 *
	 * @param densityConvToUnitId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByDensityConvToUnitId(
			Integer densityConvToUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocation e where e.clientId = :clientId and e.densityConvToUnit.id = :densityConvToUnitId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("densityConvToUnitId", densityConvToUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: marketCurrency
	 *
	 * @param marketCurrency
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByMarketCurrency(Currencies marketCurrency) {
		return this.findByMarketCurrencyId(marketCurrency.getId());
	}
	/**
	 * Find by ID of reference: marketCurrency.id
	 *
	 * @param marketCurrencyId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByMarketCurrencyId(Integer marketCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocation e where e.clientId = :clientId and e.marketCurrency.id = :marketCurrencyId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("marketCurrencyId", marketCurrencyId)
				.getResultList();
	}
	/**
	 * Find by reference: marketPricingUnit
	 *
	 * @param marketPricingUnit
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByMarketPricingUnit(Unit marketPricingUnit) {
		return this.findByMarketPricingUnitId(marketPricingUnit.getId());
	}
	/**
	 * Find by ID of reference: marketPricingUnit.id
	 *
	 * @param marketPricingUnitId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByMarketPricingUnitId(
			Integer marketPricingUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocation e where e.clientId = :clientId and e.marketPricingUnit.id = :marketPricingUnitId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("marketPricingUnitId", marketPricingUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: financialSource
	 *
	 * @param financialSource
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByFinancialSource(
			FinancialSources financialSource) {
		return this.findByFinancialSourceId(financialSource.getId());
	}
	/**
	 * Find by ID of reference: financialSource.id
	 *
	 * @param financialSourceId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByFinancialSourceId(
			Integer financialSourceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocation e where e.clientId = :clientId and e.financialSource.id = :financialSourceId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("financialSourceId", financialSourceId)
				.getResultList();
	}
	/**
	 * Find by reference: averagingMethod
	 *
	 * @param averagingMethod
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByAveragingMethod(
			AverageMethod averagingMethod) {
		return this.findByAveragingMethodId(averagingMethod.getId());
	}
	/**
	 * Find by ID of reference: averagingMethod.id
	 *
	 * @param averagingMethodId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByAveragingMethodId(
			Integer averagingMethodId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocation e where e.clientId = :clientId and e.averagingMethod.id = :averagingMethodId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("averagingMethodId", averagingMethodId)
				.getResultList();
	}
	/**
	 * Find by reference: indexProvider
	 *
	 * @param indexProvider
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByIndexProvider(TimeSerie indexProvider) {
		return this.findByIndexProviderId(indexProvider.getId());
	}
	/**
	 * Find by ID of reference: indexProvider.id
	 *
	 * @param indexProviderId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByIndexProviderId(Integer indexProviderId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocation e where e.clientId = :clientId and e.indexProvider.id = :indexProviderId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("indexProviderId", indexProviderId)
				.getResultList();
	}
	/**
	 * Find by reference: indexAveragingMethod
	 *
	 * @param indexAveragingMethod
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByIndexAveragingMethod(
			AverageMethod indexAveragingMethod) {
		return this.findByIndexAveragingMethodId(indexAveragingMethod.getId());
	}
	/**
	 * Find by ID of reference: indexAveragingMethod.id
	 *
	 * @param indexAveragingMethodId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByIndexAveragingMethodId(
			Integer indexAveragingMethodId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocation e where e.clientId = :clientId and e.indexAveragingMethod.id = :indexAveragingMethodId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("indexAveragingMethodId", indexAveragingMethodId)
				.getResultList();
	}
	/**
	 * Find by reference: settlementCurrency
	 *
	 * @param settlementCurrency
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findBySettlementCurrency(
			Currencies settlementCurrency) {
		return this.findBySettlementCurrencyId(settlementCurrency.getId());
	}
	/**
	 * Find by ID of reference: settlementCurrency.id
	 *
	 * @param settlementCurrencyId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findBySettlementCurrencyId(
			Integer settlementCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocation e where e.clientId = :clientId and e.settlementCurrency.id = :settlementCurrencyId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("settlementCurrencyId", settlementCurrencyId)
				.getResultList();
	}
	/**
	 * Find by reference: settlementUnit
	 *
	 * @param settlementUnit
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findBySettlementUnit(Unit settlementUnit) {
		return this.findBySettlementUnitId(settlementUnit.getId());
	}
	/**
	 * Find by ID of reference: settlementUnit.id
	 *
	 * @param settlementUnitId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findBySettlementUnitId(Integer settlementUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocation e where e.clientId = :clientId and e.settlementUnit.id = :settlementUnitId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("settlementUnitId", settlementUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: tenderLocationAirlines
	 *
	 * @param tenderLocationAirlines
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByTenderLocationAirlines(
			TenderLocationAirlines tenderLocationAirlines) {
		return this.findByTenderLocationAirlinesId(tenderLocationAirlines
				.getId());
	}
	/**
	 * Find by ID of reference: tenderLocationAirlines.id
	 *
	 * @param tenderLocationAirlinesId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByTenderLocationAirlinesId(
			Integer tenderLocationAirlinesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from TenderLocation e, IN (e.tenderLocationAirlines) c where e.clientId = :clientId and c.id = :tenderLocationAirlinesId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("tenderLocationAirlinesId",
						tenderLocationAirlinesId).getResultList();
	}
	/**
	 * Find by reference: bids
	 *
	 * @param bids
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByBids(Contract bids) {
		return this.findByBidsId(bids.getId());
	}
	/**
	 * Find by ID of reference: bids.id
	 *
	 * @param bidsId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByBidsId(Integer bidsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from TenderLocation e, IN (e.bids) c where e.clientId = :clientId and c.id = :bidsId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("bidsId", bidsId).getResultList();
	}
	/**
	 * Find by reference: tenderLocationRounds
	 *
	 * @param tenderLocationRounds
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByTenderLocationRounds(
			TenderLocationRound tenderLocationRounds) {
		return this.findByTenderLocationRoundsId(tenderLocationRounds.getId());
	}
	/**
	 * Find by ID of reference: tenderLocationRounds.id
	 *
	 * @param tenderLocationRoundsId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByTenderLocationRoundsId(
			Integer tenderLocationRoundsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from TenderLocation e, IN (e.tenderLocationRounds) c where e.clientId = :clientId and c.id = :tenderLocationRoundsId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("tenderLocationRoundsId", tenderLocationRoundsId)
				.getResultList();
	}
	/**
	 * Find by reference: tenderLocationExpectedPrices
	 *
	 * @param tenderLocationExpectedPrices
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByTenderLocationExpectedPrices(
			TenderLocationExpectedPrice tenderLocationExpectedPrices) {
		return this
				.findByTenderLocationExpectedPricesId(tenderLocationExpectedPrices
						.getId());
	}
	/**
	 * Find by ID of reference: tenderLocationExpectedPrices.id
	 *
	 * @param tenderLocationExpectedPricesId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByTenderLocationExpectedPricesId(
			Integer tenderLocationExpectedPricesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from TenderLocation e, IN (e.tenderLocationExpectedPrices) c where e.clientId = :clientId and c.id = :tenderLocationExpectedPricesId",
						TenderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("tenderLocationExpectedPricesId",
						tenderLocationExpectedPricesId).getResultList();
	}
}
