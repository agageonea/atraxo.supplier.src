package atraxo.cmm.business.ext.mailMerge.delegate.builders;

import atraxo.cmm.domain.ext.mailmerge.dto.contract.FinancialSourcesDto;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;

/**
 * @author apetho
 */
public final class FinancialSourceDtoBuilder {

	private FinancialSourceDtoBuilder() {

	}

	public static final FinancialSourcesDto buildFinancialSource(FinancialSources financialSources) {
		FinancialSourcesDto dto = new FinancialSourcesDto();
		if (financialSources != null) {
			dto.setCode(financialSources.getCode());
			dto.setName(financialSources.getName());
		}
		return dto;
	}

}
