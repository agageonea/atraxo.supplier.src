package atraxo.cmm.business.ext.exceptions;

import atraxo.cmm.domain.impl.cmm_type.Operators;
import atraxo.cmm.domain.impl.cmm_type.RestrictionTypes;
import seava.j4e.api.exceptions.BusinessException;

public class InvalidRestrictionOperatorException extends BusinessException {

	private static final long serialVersionUID = -6097508934823762639L;

	public InvalidRestrictionOperatorException(RestrictionTypes restrictionType, Operators operator) {
		super(CmmErrorCode.INVALII_RESTRICTION_OPEATOR,
				String.format(CmmErrorCode.INVALII_RESTRICTION_OPEATOR.getErrMsg(), operator.getName(), restrictionType.getName()));
	}

}
