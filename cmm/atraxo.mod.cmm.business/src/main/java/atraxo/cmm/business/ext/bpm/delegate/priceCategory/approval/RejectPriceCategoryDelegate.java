/**
 *
 */
package atraxo.cmm.business.ext.bpm.delegate.priceCategory.approval;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.ext.bpm.delegate.ContractDelegate;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;

/**
 * @author vhojda
 */
public class RejectPriceCategoryDelegate extends ContractDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(RejectPriceCategoryDelegate.class);

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.bpm.delegate.AbstractJavaDelegate#execute()
	 */
	@Override
	public void execute() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}

		Contract contract = this.getContract();
		if (contract != null) {
			this.contractSrv.rejectPriceUpdate(contract, this.getNote());
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#getSessionUserCode()
	 */
	@Override
	public String getSessionUserCode() {
		return this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME);
	}

}
