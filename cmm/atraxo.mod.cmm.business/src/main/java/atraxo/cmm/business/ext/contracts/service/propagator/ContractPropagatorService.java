package atraxo.cmm.business.ext.contracts.service.propagator;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.ext.contracts.service.CopyContractService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.fmbas.business.ext.util.EntityCloner;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * @author apetho
 */
public class ContractPropagatorService extends AbstractBusinessBaseService {

	private static final String SHIP_TO = "shipTo";
	private static final String RISK_HOLDER = "riskHolder";
	private static final String PRICING_BASES = "pricingBases";
	private static final String READ_ONLY = "readOnly";
	private static final String STATUS = "status";
	private static final String DEAL_TYPE = "dealType";
	private static final String CODE = "code";
	private static final String BILL_TO = "billTo";
	private static final String CUSTOMER = "customer";
	private static final String SUPPLIER = "supplier";
	private static final String HOLDER = "holder";
	private static final String RESALE_REF = "resaleRef";
	private static final String SUBSIDIARY_ID = "subsidiaryId";
	private static final Logger LOG = LoggerFactory.getLogger(ContractPropagatorService.class);

	@Autowired
	private CopyContractService copyContractService;

	/**
	 * @param from
	 * @param to
	 * @throws BusinessException
	 */
	public void propagateChanges(Contract from, Contract to) throws BusinessException {
		if (LOG.isDebugEnabled()) {
			LOG.info("Start to propagate contract changes");
		}
		List<Field> fields = new ArrayList<>();

		try {
			fields.add(Contract.class.getDeclaredField(RESALE_REF));
			fields.add(Contract.class.getDeclaredField(HOLDER));
			fields.add(Contract.class.getDeclaredField(SUPPLIER));
			fields.add(Contract.class.getDeclaredField(CUSTOMER));
			fields.add(Contract.class.getDeclaredField(BILL_TO));
			fields.add(Contract.class.getDeclaredField(CODE));
			fields.add(Contract.class.getDeclaredField(DEAL_TYPE));
			fields.add(Contract.class.getDeclaredField(STATUS));
			fields.add(Contract.class.getDeclaredField(READ_ONLY));
			fields.add(Contract.class.getDeclaredField(PRICING_BASES));
			fields.add(Contract.class.getDeclaredField(RISK_HOLDER));
			fields.add(Contract.class.getDeclaredField(SHIP_TO));
			// fields.add(Contract.class.getDeclaredField(SUBSIDIARY_ID));
			EntityCloner.cloneEntity(from, to, fields);
			to.getPricingBases().clear();
			to.getPriceCategories().clear();
			to.getPricingBases().addAll(this.copyContractService.getPricingBases(from, to, true));
			this.propagateShipTo(from, to);
		} catch (NoSuchFieldException | SecurityException | InstantiationException | IllegalAccessException e) {
			LOG.error("Propagate purchase contract problems.", e);
		}
		if (LOG.isDebugEnabled()) {
			LOG.info("End propagate contract changes");
		}
	}

	public void propagateShipTo(Contract from, Contract to) throws InstantiationException, IllegalAccessException {
		if (!CollectionUtils.isEmpty(from.getShipTo())) {
			for (ShipTo st : from.getShipTo()) {
				ShipTo temp;
				if ((temp = this.contrainsShipTo(to.getShipTo(), st)) == null) {
					ShipTo newShipTo = EntityCloner.cloneEntity(st);
					newShipTo.setContract(to);
					to.addToShipTo(newShipTo);
				} else {
					temp.setValidFrom(st.getValidFrom());
					temp.setValidTo(st.getValidTo());
					temp.setAllocatedVolume(st.getAllocatedVolume());
					temp.setAdjustedVolume(st.getAdjustedVolume());
					temp.setOfferedVolume(st.getOfferedVolume());
					temp.setTenderBidVolume(st.getTenderBidVolume());
				}
			}
		}
		if (!CollectionUtils.isEmpty(to.getShipTo())) {
			if (CollectionUtils.isEmpty(from.getShipTo())) {
				to.getShipTo().clear();
			} else {
				Iterator<ShipTo> iter = to.getShipTo().iterator();
				while (iter.hasNext()) {
					ShipTo temp = iter.next();
					if (this.contrainsShipTo(from.getShipTo(), temp) == null) {
						iter.remove();
					}
				}
			}
		}
	}

	private ShipTo contrainsShipTo(Collection<ShipTo> shipTos, ShipTo shipTo) {
		for (ShipTo st : shipTos) {
			if (st.getCustomer().getId().equals(shipTo.getCustomer().getId())) {
				return st;
			}
		}
		return null;
	}
}
