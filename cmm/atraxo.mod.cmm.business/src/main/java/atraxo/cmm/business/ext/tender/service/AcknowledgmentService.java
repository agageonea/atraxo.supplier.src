package atraxo.cmm.business.ext.tender.service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.ext.datahub.IDataHubClientRequestService;
import atraxo.cmm.business.api.ext.tender.IAcknowledgmentService;
import atraxo.cmm.business.api.tender.ITenderLocationService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.ext.tender.MessageStatus;
import atraxo.cmm.domain.ext.tender.MessageType;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcknowledge;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderResponse;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderServiceService;

/**
 * @author zspeter
 */
public class AcknowledgmentService extends AbstractBusinessBaseService
		implements IDataHubClientRequestService<Tender, FuelTenderAcknowledge>, IAcknowledgmentService {

	private static final Logger LOG = LoggerFactory.getLogger(AcknowledgmentService.class);

	@Autowired
	private IContractService contractService;
	@Autowired
	private ITenderLocationService tenderLocationService;
	@Autowired
	private ExternalInterfaceMessageHistoryFacade interfaceMessageHistoryFacade;

	@Override
	public FuelTenderResponse submitRequest(FuelTenderServiceService client, FuelTenderAcknowledge dto) throws BusinessException {
		return client.getFuelTenderServicePort().fuelTenderAcknowledgement(dto);
	}

	@Override
	public void fuelTenderAcknowledgement(Long refMsgId, MessageType messageType, MessageStatus messageStatus) throws BusinessException {
		List<Object> objectIds = this.getObjectIdByRefId(refMsgId, messageType);
		TransmissionStatus transmissionStatus = this.getTransmissionStatusFromIATA(messageStatus);

		switch (messageType) {
		case FUEL_TENDER_BID:
			List<Contract> bids = this.contractService.findByIds(objectIds);
			bids.forEach(bid -> bid.setBidTransmissionStatus(transmissionStatus));

			this.contractService.update(bids);
			break;
		case FUEL_TENDER_ACCEPT_AWARD:
			List<Contract> acceptBids = this.contractService.findByIds(objectIds);
			acceptBids.forEach(acceptBid -> acceptBid.setBidAcceptAwardTransmissionStatus(transmissionStatus));

			this.contractService.update(acceptBids);
			break;
		case FUEL_TENDER_DECLINE_AWARD:
			List<Contract> declinedAwardBids = this.contractService.findByIds(objectIds);
			declinedAwardBids.forEach(declinedAwardBid -> declinedAwardBid.setBidDeclineAwardTransmissionStatus(transmissionStatus));

			this.contractService.update(declinedAwardBids);
			break;
		case FUEL_TENDER_NO_BID:
			List<TenderLocation> noBidTenderLocations = this.tenderLocationService.findByIds(objectIds);
			noBidTenderLocations.forEach(noBidTenderLocation -> noBidTenderLocation.setTransmissionStatus(transmissionStatus));

			this.tenderLocationService.update(noBidTenderLocations);
			break;
		case FUEL_TENDER_CANCEL_BID:
			List<Contract> cancelBids = this.contractService.findByIds(objectIds);
			cancelBids.forEach(cancelBid -> cancelBid.setBidCancelTransmitionStatus(transmissionStatus));

			this.contractService.update(cancelBids);
			break;
		default:
			break;
		}
	}

	private TransmissionStatus getTransmissionStatusFromIATA(MessageStatus mStatus) {
		TransmissionStatus transmissionStatus = TransmissionStatus._TRANSMITTED_;
		switch (mStatus) {
		case RECEIVED:
			transmissionStatus = TransmissionStatus._RECEIVED_;
			break;
		case READ:
			transmissionStatus = TransmissionStatus._READ_;
			break;
		case PROCESSED:
			transmissionStatus = TransmissionStatus._PROCESSED_;
			break;
		case ERROR:
			transmissionStatus = TransmissionStatus._FAILED_;
			break;
		}

		return transmissionStatus;
	}

	private List<Object> getObjectIdByRefId(Long refMsgId, MessageType messageType) throws BusinessException {
		List<ExternalInterfaceMessageHistory> list = this.interfaceMessageHistoryFacade.findByMessageId(Long.toString(refMsgId));
		if (list == null || list.isEmpty()) {
			throw new BusinessException(CmmErrorCode.IATA_ACKNOLEGMENT_NO_MSG_ID_FOUND,
					String.format(CmmErrorCode.IATA_ACKNOLEGMENT_NO_MSG_ID_FOUND.getErrMsg(), refMsgId, messageType));
		}

		return list.stream().map(ExternalInterfaceMessageHistory::getObjectId).collect(Collectors.toList());
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubClientRequestService#processDataHubWsResult(atraxo.fmbas.domain.ws.dto.WSExecutionResult,
	 * java.util.Collection, java.lang.Class, java.lang.String)
	 */
	@Override
	public void processDataHubBulkWsResult(WSExecutionResult result, Collection<Tender> list, Class<FuelTenderAcknowledge> dtoClass, String reason)
			throws BusinessException {
		LOG.info("Acknowledgment synchronous response received: %s", result);
	}

	/**
	 * Process and finalize the response from DataHub. Make the necessary adjustments to the entity.
	 *
	 * @param result
	 * @param dtoClass
	 * @param reason
	 * @throws BusinessException
	 */
	@Override
	public void processDataHubSingleWsResult(WSExecutionResult result, Tender entity, Class<FuelTenderAcknowledge> dtoClass, String reason)
			throws BusinessException {
		LOG.info("Acknowledgment synchronous response received: %s", result);
	}

}
