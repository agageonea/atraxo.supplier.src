package atraxo.cmm.business.ext.mailMerge.delegate.builders;

import atraxo.cmm.domain.ext.mailmerge.dto.contract.AverageMethodDto;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;

/**
 * @author apetho
 */
public final class AverageMethodDtoBuilder {

	private AverageMethodDtoBuilder() {

	}

	public static final AverageMethodDto buildAverageMethod(AverageMethod averageMethod) {
		AverageMethodDto dto = new AverageMethodDto();
		if (averageMethod != null) {
			dto.setCode(averageMethod.getCode());
			dto.setName(averageMethod.getName());
		}
		return dto;
	}
}
