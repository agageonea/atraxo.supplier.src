package atraxo.cmm.business.ws.tender.transformer;

import java.util.List;

import javax.xml.bind.JAXBException;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.iata.fuelplus.iata.tender.AcceptedBids;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcceptAward;

public class TenderBidAcceptAwardTransformer extends DataHubOutTransformer<Contract, FuelTenderAcceptAward> {

	@Override
	public FuelTenderAcceptAward transformModelToDTO(Contract modelEntity, long msgId, String reason) throws BusinessException, JAXBException {
		FuelTenderAcceptAward acceptAwardBid = new FuelTenderAcceptAward();
		acceptAwardBid.setTenderIdentification(this.buildTenderIdentification(modelEntity.getBidTenderIdentification(), msgId));

		AcceptedBids bids = new AcceptedBids();
		bids.getBid().add(this.buildBid(modelEntity));
		acceptAwardBid.setAcceptedBids(bids);
		acceptAwardBid.setSchemaVersion(modelEntity.getBidTenderIdentification().getSchemaVersion());

		return acceptAwardBid;
	}

	@Override
	public FuelTenderAcceptAward transformModelToDTO(Contract modelEntity, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

	@Override
	public FuelTenderAcceptAward transformModelsToDTO(List<Contract> modelEntities, long msgId, String reason)
			throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

	@Override
	public FuelTenderAcceptAward transformModelsToDTO(List<Contract> modelEntities, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}
}
