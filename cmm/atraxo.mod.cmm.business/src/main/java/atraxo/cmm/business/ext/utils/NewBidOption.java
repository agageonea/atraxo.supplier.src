package atraxo.cmm.business.ext.utils;

/**
 * Enum with options regarding the new bid operation: the bid can be copied by creating a new bid with new version, new location or new revision;
 *
 * @author vhojda
 */
public enum NewBidOption {

	NEW_VERSION, NEW_LOCATION, NEW_REVISION;
}
