/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.ext.tender.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import atraxo.cmm.business.api.tender.ITenderLocationAirlinesService;
import atraxo.cmm.business.api.tender.ITenderLocationService;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link TenderLocationAirlines} domain entity.
 */
public class TenderLocationAirlines_Service extends atraxo.cmm.business.impl.tender.TenderLocationAirlines_Service
		implements ITenderLocationAirlinesService {

	@Autowired
	private ITenderLocationService locationService;

	@Override
	protected void postInsert(List<TenderLocationAirlines> list) throws BusinessException {
		super.postInsert(list);
		this.locationService.updateVolume(list.get(0).getTenderLocation().getId(), Boolean.FALSE);
	}

	@Override
	protected void postUpdate(List<TenderLocationAirlines> list) throws BusinessException {
		super.postUpdate(list);
		this.locationService.updateVolume(list.get(0).getTenderLocation().getId(), Boolean.FALSE);
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		context.put("locationId", this.findByBusiness((Integer) ids.get(0)).getTenderLocation().getId());
		context.put("resetLocationVolume", Boolean.TRUE);
	}

	@Override
	protected void postDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.postDeleteByIds(ids, context);
		this.locationService.updateVolume((Integer) context.get("locationId"), Boolean.TRUE);
	}

	@Override
	@Transactional
	public void updateBiddingStatus(TenderLocation location, List<TenderLocationAirlines> airlines, BiddingStatus biddingStatus, String action,
			String reason) throws BusinessException {

		for (TenderLocationAirlines airline : airlines) {
			for (TenderLocationAirlines locationAirline : location.getTenderLocationAirlines()) {
				if (airline.equals(locationAirline)) {
					locationAirline.setBiddingStatus(biddingStatus);
				}
			}
		}

		// Update Tender Location Bidding Status based on airlines bidding status
		BiddingStatus locationBiddingStatus = this.locationService.calculateBiddingStatus(location);
		if (location.getLocationBiddingStatus().equals(locationBiddingStatus)) {
			this.onUpdate(airlines);
		} else {
			this.locationService.updateBiddingStatus(location, locationBiddingStatus, action, locationBiddingStatus.getName());
		}
	}

}
