package atraxo.cmm.business.ext.exceptions;

import java.util.Locale;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.ext.exceptions.ExceptionErrorGroup;
import seava.j4e.api.exceptions.IErrorCode;
import seava.j4e.api.session.Session;

public enum CmmErrorCode implements IErrorCode {

	INVALID_CONTRACT_DATE(4001, "The selected date is out of the contract period.", ExceptionErrorGroup.ERROR),

	INVALID_PRICE_CATEGORY_CONTRACT(4002, "Cannot find price category for an inexistent but referred contract.", ExceptionErrorGroup.ERROR),

	EFFECTIVE_EXPIRED_CONTRACTS(4003, "Effective/Expired contracts cannot be deleted.", ExceptionErrorGroup.ERROR),

	FIELD_REVIEW_DATE1(4004, "Field Price Review Date 1 is required to have a value for this case.", ExceptionErrorGroup.ERROR),

	FIELD_REVIEW_DATE2(4005, "Field Price Review Date 2 is required to have a value for this case.", ExceptionErrorGroup.ERROR),

	FIELD_REVIEW_DATE12(4006, "Fields Price Review Date 1 and 2 are required to have a value for this case.", ExceptionErrorGroup.ERROR),

	VALIDITY_FOR_EXPIRED_EFFECTIVE_CONTRACT(
			4007,
			"Valid from cannot be greater than today for Effective/Expired contracts.",
			ExceptionErrorGroup.ERROR),

	REFRESH_CONTRACT_LIST(4008, "Please refresh your contract list.", ExceptionErrorGroup.ERROR),

	CONTRACT_NOT_FOUND(4009, "Contract not found based on search criteria.", ExceptionErrorGroup.ERROR),

	QUOTATION_VALUES_NOT_FOUND(4010, "Quotation (index) values for %s are required on %s or before.", ExceptionErrorGroup.ERROR),
	QUOTATION_NOT_FOUND(
			4011,
			"Energy quotation with time series code %s and average method %s not found. Please publish the time series.",
			ExceptionErrorGroup.ERROR),
	INVALID_PRICE_CATEGORY(4012, "Invalid price category. Please select and Index or Fixed based price category", "Error"),
	QUOTATION_NOT_FOUND_BY_ID(4013, "Selected quotation not found in database.", "Error"),
	QUOTATION_INVALID_DATA(4014, "The selected quotation has no %s defined.", "Error"),

	PB_OVERLAPING(4020, "The validity of this price is overlapping with other price.", ExceptionErrorGroup.ERROR),

	PB_VALIDITY(4021, "The validity of price must be within contract period validity.", ExceptionErrorGroup.ERROR),

	PB_QUOTATION_REQUIERED(4022, "Quotation is required.", ExceptionErrorGroup.ERROR),

	PB_PERIOD_REQUIRED(4023, "Period is required.", ExceptionErrorGroup.ERROR),

	PB_OPERATOR_REQUIRED(4024, "Operator is required.", ExceptionErrorGroup.ERROR),

	PB_FACTOR_REQUIRED(4025, "Factor is required.", ExceptionErrorGroup.ERROR),

	PB_TO_REQUIRED(4026, "To is required.", ExceptionErrorGroup.ERROR),

	PB_PRICE_OVERLAPING(4027, "This pricing base is overlapping with other pricing base.", ExceptionErrorGroup.ERROR),

	INVALID_PRICING_BASE_UPDATE_PERIOD(4028, "The pricing base new period must interset the updated pricing base period.", ExceptionErrorGroup.ERROR),

	PB_CANNOT_EXTEND(4029, "Price category cannot be deleted because the other price category cannot be extended.", ExceptionErrorGroup.ERROR),

	EFFECTIVE_EXPIRED_GENERATED_CONTRACTS(
			4030,
			"You have some Effective/Expired generated contracts, those cannot be deleted.",
			ExceptionErrorGroup.ERROR),

	RPTDESIGN_IS_MISSING(4031, "Please select one of the available templates for the PDF Format invoice type.", ExceptionErrorGroup.ERROR),

	PRICES_EMPTY(4040, "Price field(s) cannot be empty.", ExceptionErrorGroup.ERROR),

	PC_SAME(4041, "A contract price from the same price category already exists on this contract.", ExceptionErrorGroup.ERROR),

	PC_PRICE_IS_SAME(4042, "Other price from the same category has the same restrictions.", ExceptionErrorGroup.ERROR),

	REQUIRED_FIELDS_NULL(4043, "Operation type, value and unit are required.", ExceptionErrorGroup.ERROR),

	REQUIRED_FIELDS_NULL_NOT_VOLUME(4044, "Operation type and value are required.", ExceptionErrorGroup.ERROR),

	REQUIRED_RESTRICTION(4045, "The restriction type is required.", ExceptionErrorGroup.ERROR),

	DEL_CPCATEGORY(4046, "Cannot delete Contract Price category because another price category is calculated from them.", ExceptionErrorGroup.ERROR),

	INVALID_PRICECOMPONENT_DATE(4047, "The selected date is out of the contract price category period.", ExceptionErrorGroup.ERROR),

	PROPAGATION_PRICE_CATEGORY_NOT_FOUND(4048, "Unable to find the coresponding resale contract price category.", ExceptionErrorGroup.ERROR),

	VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO(4050, "Valid from can not be greater than valid to.", ExceptionErrorGroup.ERROR),

	CONTRACT_IS_OVERLAPING(4060, "The contract is overlapping with ", ExceptionErrorGroup.ERROR),

	CONTRACT_ALREADY_EXISTS(4061, "%s already exists for the same conditions and a new one can't be activated.", ExceptionErrorGroup.ERROR),

	RESET_CONTRACTS_ERROR(4062, "Contract reset is not possible because a Fuel Event is assigned to it.", ExceptionErrorGroup.ERROR),
	REMOVE_INVOICE_LINK(4063, "Contract-invoice link delete is not possible.", ExceptionErrorGroup.ERROR),
	UPDATE_CONTRACT_PERIOD(
			4064,
			"Contract validity period can't be changed as the contract is linked to one or more released invoices: %s",
			ExceptionErrorGroup.ERROR),
	NO_COMPANY_PROFILE_FOUND(4065, "No company profile found with client code %s.", ExceptionErrorGroup.ERROR),
	FUEL_PRICE_NOT_EXISTS(
			4066,
			"%s type price component not found for contract %s, for contract price category %s on %s!",
			ExceptionErrorGroup.ERROR),
	INVALID_CUSTOMER(4067, "The selected customer is invalid", ExceptionErrorGroup.ERROR),
	SAME_HOLDER_CUSTOMER_SUPPLIER(
			4068,
			"You have selected the same company as holder and counterparty. Please revise and try again.",
			ExceptionErrorGroup.ERROR),
	INVALII_RESTRICTION_OPEATOR(4070, "The operator %s is not valid for restriction %s.", ExceptionErrorGroup.ERROR),

	MORE_THAN_TWO_PERCENTAGE(4080, "More than two percentage is used.", ExceptionErrorGroup.ERROR),

	CONTRACT_ALREADY_EXISTS_IN_APPROVAL(
			4099,
			"We can't submit the contract for approval. There is another %s contract %s for the same:"
					+ "<ul><li>Location: %s</li><li>Contract period: %s</li><li>Contract holder: %s</li><li>Customer: %s</li><li>Ship-to(s): %s</li><li>Contract type: %s</li><li>Delivery point: %s</li><li>Scope: %s</li><li>Event type: %s</li><li>Product: %s</li></ul>",
			ExceptionErrorGroup.ERROR),

	CONTRACT_ALREADY_EXISTS_WITH_DETAILS(
			4100,
			"We can't make the contract active. There is another active or effective %s contract %s for the same:"
					+ "<ul><li>Location: %s</li><li>Contract period: %s</li><li>Contract holder: %s</li><li>Customer: %s</li><li>Ship-to(s): %s</li><li>Contract type: %s</li><li>Delivery point: %s</li><li>Scope: %s</li><li>Event type: %s</li><li>Product: %s</li></ul>",
			ExceptionErrorGroup.ERROR),
	CONTRACT_ALREADY_EXISTS_WITH_DETAILS_UPDATE(
			4101,
			"We can't update the contract. There is another active or effective %s contract %s for the same:"
					+ "<ul><li>Location: %s</li><li>Contract period: %s</li><li>Contract holder: %s</li><li>Customer: %s</li><li>Ship-to(s): %s</li><li>Contract type: %s</li><li>Delivery point: %s</li><li>Scope: %s</li><li>Event type: %s</li><li>Product: %s</li></ul>",
			ExceptionErrorGroup.ERROR),

	SELL_CONTACT_CUSTOMER_MUST_BE_ACTIVE(4102, "The selected customer is not active!", ExceptionErrorGroup.ERROR),

	COMPOSITE_PRICE_MUST_HAVE_A_CHILD(4103, "The assigned price component is missing!", ExceptionErrorGroup.ERROR),
	CONTRACT_READ_ONLY(4104, "Contract is read only.", ExceptionErrorGroup.ERROR),
	THE_CONTRACT_MUST_BE_APPROVED(4105, "The contract must be approved!", ExceptionErrorGroup.ERROR),
	SELL_CONTRACT_COULD_NOT_CREATE_DRAFT(4106, "Could not create an editable draft for your contract.", ExceptionErrorGroup.ERROR),

	FINCANCIAL_SOURCE_IS_MISSING(4110, "The financial source is missing!", ExceptionErrorGroup.ERROR),
	AVERAGE_METHOD_IS_MISSING(4111, "The average method is missing!", ExceptionErrorGroup.ERROR),
	BID_TENDER_PRODUCT_NOT_FOUND(4112, "This product is not supported!", ExceptionErrorGroup.ERROR),
	BID_TENDER_PREPAY_REQUIRED_FIELDS_NULL(4113, "Prepayment fields are required!", ExceptionErrorGroup.ERROR),
	BID_TENDER_COPY_BID_ERROR(4114, "Copy Bid Failed!", ExceptionErrorGroup.ERROR),
	BID_ALREADY_EXISTS_WITH_DETAILS(
			4115,
			"The system can't finish the bid. There is another finished bid for the same:<ul>"
					+ "<li>Tender Code: %s</li><li> Tender Version: %s</li><li> Holder: %s</li><li> Location: %s</li>"
					+ "<li> Bid Revision: %s</li><li> Bid Version: %s</li><li> Product type: %s</li></li>" + "<li> Tax type: %s</li></li></ul>",
			ExceptionErrorGroup.ERROR),
	PRODUCT_PRICE_CATEGORY_IS_MISSING(4116, "There are no product prices captured", ExceptionErrorGroup.ERROR),
	BID_NO_VOLUME(4117, "The volumes setup is not completed", ExceptionErrorGroup.ERROR),
	BID_FINISH_INVALID(4118, "The system can't finish the bid: %s", ExceptionErrorGroup.ERROR),
	TENDER_NO_LOCATION(4119, "The following tenders cannot be published, at least one location must be defined: %s", ExceptionErrorGroup.VALIDATION),
	TENDER_WITH_SUBMITED_BIDS(
			4120,
			"The following tenders have bids that are AWARDED or ACCEPTED and cannot be unpublished: %s",
			ExceptionErrorGroup.ERROR),
	TENDER_NO_VOLUME(4121, "The volumes setup is not completed: %s", ExceptionErrorGroup.VALIDATION),
	TENDER_NO_RESULT(4122, "No Tenders found for code: %s - version: %s - holder: %s ", ExceptionErrorGroup.ERROR),
	TENDER_LOCATION_RPC_ACTION_ERROR(
			4123,
			"Action cannot be executed on the following Tender Location because they have been changed or deleted by another user: <br/> %s <br/> The records have been reloaded. <br>Please try again.",
			ExceptionErrorGroup.VALIDATION),
	APPROVED_SUBMITTED_BIDS(4124, "Approved/Submitted bids cannot be deleted.", ExceptionErrorGroup.ERROR),
	SUBMITED_TENDER_LOCATIONS_NO_BID(
			4125,
			"The following locations have been submited for Decline Bidding: <br/> %s",
			ExceptionErrorGroup.VALIDATION),
	SUBMITED_TENDER_LOCATIONS_NO_BID_EMPTY(
			4126,
			"Tender does not contain any unsubmited locations in which you do not operate",
			ExceptionErrorGroup.VALIDATION),
	TENDER_VERSION_CANOT_FOUND(4127, "No Tenders found for code: %s - holder: %s.", ExceptionErrorGroup.ERROR),

	PRICE_UPDATE_JOB_RESULT(
			4130,
			"The following contracts have been updates: \r\n %s \r\n The following contract have been skipped from update: \r\n %s",
			ExceptionErrorGroup.VALIDATION),
	NO_CONTRACTS(4131, "No Contracts!", ExceptionErrorGroup.VALIDATION),
	INVALID_CONTRACT_FOR_PRICE_UPDATE(4132, "Contracts with different currencies are selected. (%s)", ExceptionErrorGroup.VALIDATION),
	OUT_RANGED_FUEL_EVENTS(
			4200,
			"Contract period cannot be modified because of the following out ranged fuel events (Supplier - Ship to - Fueling date): %s",
			ExceptionErrorGroup.ERROR),
	BID_APPROVAL_WORKFLOW_RESULT_SUBMIT(4250, "Successfully submitted <b>%s</b> bids out of existing <b>%s</b> .", ExceptionErrorGroup.ERROR),
	BID_APPROVAL_WORKFLOW_RESULT_CONCURENCY_CHECK(
			4251,
			"<b>%s</b> bids out of the existing <b>%s</b> have failed the concurency check, please reload records.",
			ExceptionErrorGroup.ERROR),
	BID_APPROVAL_WORKFLOW_RESULT_INVALID_BIDS(
			4252,
			"<b>%s</b> bids have failed the check and cannot be submitted. <br/> Please check the reason of failure below. Correct the bid and try again to submit.",
			ExceptionErrorGroup.ERROR),
	BID_STATUS_NOT_ACCEPTED(4300, "The bid must be awarded and the award must be accepted!", ExceptionErrorGroup.ERROR),
	BID_STATUS_NOT_AWARDED(4301, "The bid must be awarded!", ExceptionErrorGroup.ERROR),
	BID_STATUS_NOT_NEW(4302, "The bid must be new and the transmission status new/failed!", ExceptionErrorGroup.ERROR),
	BID_SOURCE_NOT_IMPORTED(
			4303,
			"The following bid(s) are not generated from an imported tender and cannot be submited: %s",
			ExceptionErrorGroup.ERROR),
	BID_TENDER_CANCELED(4304, "The following bid(s) are generated from a canceled tender and cannot be submited: %s", ExceptionErrorGroup.ERROR),
	BID_VERSION_EXCEEDED(4305, "Maximum supported number of bids has been reached", ExceptionErrorGroup.ERROR),
	BID_BIDDING_FINISHED(4306, "The following bid(s) have the location bidding status finished: %s", ExceptionErrorGroup.ERROR),
	BID_OPTION_UNKNOWN(4307, "Unknown bid option. Please contract system administrator.", ExceptionErrorGroup.ERROR),
	NEW_BID_REVISION_CONCURRENCY_NOT_OK(
			4308,
			"Can not create a new bid revision, the bid was modified in the meantime, please refresh and try again.",
			ExceptionErrorGroup.ERROR),
	NEW_BID_REVISION_LAST_REVISION_NOT_OK(4309, "Last revision was not transmitted. Please transmit it and try again.", ExceptionErrorGroup.ERROR),

	CONTRACT_RPC_ACTION_ERROR(
			4400,
			"Action cannot be executed on the following Contract(s) because they have been changed or deleted by another user: <br/> %s <br/> The records have been reloaded. <br>Please try again.",
			ExceptionErrorGroup.VALIDATION),
	TENDER_RPC_ACTION_ERROR(
			4401,
			"Action cannot be executed on the following Tender Invitation(s) because they have been changed or deleted by another user: <br/> %s <br/> The records have been reloaded. <br>Please try again.",
			ExceptionErrorGroup.VALIDATION),
	BID_RPC_ACTION_ERROR(
			4402,
			"Action cannot be executed on the following Bid(s) because they have been changed or deleted by another user: <br/> %s <br/> The records have been reloaded. <br>Please try again and if you still cannot proceed close the dialog and reprocess.",
			ExceptionErrorGroup.VALIDATION),
	PRICEUPDATE_DELETE_ACTION_ERROR(
			4403,
			"Action cannot be executed on the following Price Update(s) because they have been changed or deleted by another user: <br/> Please reload the records. ",
			ExceptionErrorGroup.VALIDATION),
	PRICEUPDATECATEGORY_DELETE_CONCURENCY_ACTION_ERROR(
			4404,
			"Action cannot be executed on the following Price Categories because the Price Update has been changed or deleted by another user: <br/> Please reload the records. ",
			ExceptionErrorGroup.VALIDATION),
	PRICE_UPDATE_WORKFLOW_ERROR(4405, "Workflow instance identification error. Price Update is reseted.", ExceptionErrorGroup.ERROR),

	PRICE_UPDATE_WORKFLOW_CONCURRENCY_NOT_START_ERROR(
			4406,
			"Approval process could not be started, the price update was modified in the meantime, please refresh and try again.",
			ExceptionErrorGroup.ERROR),

	PRICE_UPDATE_WORKFLOW_NOT_START_REASON(4407, "Approval process could not be started. Reason : ", ExceptionErrorGroup.ERROR),

	PRICE_UPDATE_WORKFLOW_NO_SYS_PARAM(
			4408,
			"Can not submit for approval, please check the 'Use price update publishing approval workflow' system parameter.",
			ExceptionErrorGroup.ERROR),

	PRICE_UPDATE_WORKFLOW_ENTITY_NOT_EXISTING(4409, "Could not retrieve a price update for ID %s", ExceptionErrorGroup.ERROR),
	PRICE_UPDATE_NOT_FIND(4410, "The selected price update does not exist in database. Please refresh your page.", ExceptionErrorGroup.ERROR),
	BID_WKF_VALIDATION_ACTION_ERROR(
			4411,
			"Action cannot be executed on the following Bid(s) because of the following reasons: <br/> %s <br/> Review and complete the worklow definition. <br>Please try again afterwards.",
			ExceptionErrorGroup.VALIDATION),
	PRICE_UPDATE_WORKFLOW_APPROVED(4412, "The selected price update have been already approved by another user.", ExceptionErrorGroup.ERROR),
	BID_APPROVAL_WORKFLOW_RESULT_CHECK(4421, "Check bid history for specific error description.", ExceptionErrorGroup.ERROR),

	NO_SIBLING_FOUND(4500, "No sibling contract found.", ExceptionErrorGroup.ERROR),

	BID_APPROVAL_WORKFLOW_EMAIL_EXTRACT_DATA_ERROR(
			4422,
			"Could not extract data from the bid %s  in order to send the mail !",
			ExceptionErrorGroup.ERROR),
	BID_APPROVAL_WORKFLOW_ENTITY_NOT_EXISTING(4423, "Could not retrieve a bid for ID %s", ExceptionErrorGroup.ERROR),
	BID_APPROVAL_WORKFLOW_ENTITY_ID_NO_PARSE(4424, "Could not parse the received Bid ID %s", ExceptionErrorGroup.ERROR),
	BID_APPROVAL_WORKFLOW_ENTITY_ID_EMPTY(4425, "The id for the bid is empty !", ExceptionErrorGroup.ERROR),

	BID_WORKFLOW_APPROVED(
			4426,
			"The selected tender bid has been changed in the meantime and can no longer be approved/rejected. Please reload the data and try again.",
			ExceptionErrorGroup.ERROR),
	BID_WORKFLOW_IDENTIFICATION_ERROR(
			4427,
			"Workflow instance identification error. Tender bid is reset, workflow instance is terminated.",
			ExceptionErrorGroup.ERROR),

	SELL_CONTRACT_WORKFLOW_NO_SYS_PARAM_REVERT(
			4429,
			"Can not revert ship to, please check the 'Use sales contract approval workflow' system parameter.",
			ExceptionErrorGroup.ERROR),
	SELL_CONTRACT_WORKFLOW_NO_SYS_PARAM(
			4430,
			"Can not submit for approval, please check the 'Use sales contract approval workflow' system parameter.",
			ExceptionErrorGroup.ERROR),
	PURCHASE_CONTRACT_WORKFLOW_NO_SYS_PARAM(
			4431,
			"Can not submit for approval, please check the 'Use purchase contract approval workflow' system parameter.",
			ExceptionErrorGroup.ERROR),
	CONTRACT_WORKFLOW_ENTITY_NOT_EXISTING(4432, "Could not retrieve a contract for ID %s", ExceptionErrorGroup.ERROR),
	CONTRACT_WORKFLOW_NOT_START_REASON(4433, "Approval process could not be started. Reason : ", ExceptionErrorGroup.ERROR),
	CONTRACT_WORKFLOW_CONCURRENCY_NOT_START_ERROR(
			4434,
			"Approval process could not be started, the contract was modified in the meantime, please refresh and try again.",
			ExceptionErrorGroup.ERROR),
	CONTRACT_WORKFLOW_ENTITY_ID_NO_PARSE(4435, "Could not parse the received Contract ID %s", ExceptionErrorGroup.ERROR),
	CONTRACT_WORKFLOW_EMAIL_EXTRACT_DATA_ERROR(
			4436,
			"Could not extract data from the contract %s  in order to send the mail !",
			ExceptionErrorGroup.ERROR),
	CONTRACT_WORKFLOW_ENTITY_ID_EMPTY(4437, "The id for the contract is empty !", ExceptionErrorGroup.ERROR),
	CONTRACT_WORKFLOW_APPROVED(
			4438,
			"The selected contract has been changed in the meantime and can no longer be approved/rejected. Please reload the data and try again.",
			ExceptionErrorGroup.ERROR),
	CONTRACT_WORKFLOW_IDENTIFICATION_ERROR(
			4439,
			"Workflow instance identification error. Contract is reset, workflow instance is terminated.",
			ExceptionErrorGroup.ERROR),
	CONTRACT_WORKFLOW_COULD_NOT_FIND_SHIP_TO_FOR_ID(4440, "Could not find an ShipTo for ID '%s'", ExceptionErrorGroup.ERROR),
	CONTRACT_WORKFLOW_COULD_NOT_PARSE_SHIP_TO_ID(4441, "Could not parse the Ship-To ID '%s'", ExceptionErrorGroup.ERROR),
	CONTRACT_WORKFLOW_SHIP_TO_EMPTY_ID(4442, "The id for the Ship-To is empty !", ExceptionErrorGroup.ERROR),
	CONTRACT_WORKFLOW_SHIP_TO_VARIABLE_NOT_FOUND(4443, "The variable for the id of the Ship-To could not be found !", ExceptionErrorGroup.ERROR),
	CONTRACT_WORKFLOW_AWARDED_VOLUME_NOT_SET(
			4444,
			"The contract could not be submitted for approval. Contract awarded volume must be captured.",
			ExceptionErrorGroup.ERROR),

	VOLUME_UNIT_BY_CODE_NOT_EXISTING(4450, "Could not find a volume unit for CODE %s", ExceptionErrorGroup.ERROR),
	VOLUME_UNIT_VOLUME_EMPTY(4451, "The threshold volume unit variable is empty !", ExceptionErrorGroup.ERROR),
	VOLUME_UNIT_NOT_FOUND(4452, "Could not find threshold unit volume variable!", ExceptionErrorGroup.ERROR),
	VOLUME_THRESHOLD_TRANSFORM_ERROR(4453, "Could not transform the volume %s.", ExceptionErrorGroup.ERROR),
	VOLUME_THRESHOLD_EMPTY(4454, "The threshold volume variable is empty !", ExceptionErrorGroup.ERROR),
	VOLUME_THRESHOLD_NOT_FOUND(4455, "Could not find threshold volume variable!", ExceptionErrorGroup.ERROR),

	SHIPTO_WORKFLOW_NOT_START_REASON(4461, "Approval process could not be started. Reason : ", ExceptionErrorGroup.ERROR),
	SHIPTO_AWAITING_APPROVAL_CANOT_EDIT(4462, "Ship to with awaiting approval status can not be edit.", ExceptionErrorGroup.ERROR),
	SHIPTO_ADDED(4463, "Added", ExceptionErrorGroup.VALIDATION),
	SHIPTO_REMOVED(4464, "Removed", ExceptionErrorGroup.VALIDATION),
	SHIPTO_ADJUSTED_VOLUME(4465, "Adjusted volume from %s to %s", ExceptionErrorGroup.VALIDATION),
	SHIPTO_ADJUSTED_VALID_FROM(4466, "Adjusted valid from from %s to %s", ExceptionErrorGroup.VALIDATION),
	SHIPTO_ADJUSTED_VALID_TO(4467, "Adjusted valid to from %s to %s", ExceptionErrorGroup.VALIDATION),

	SHIPTO_WORKFLOW_APPROVED(
			4469,
			"The selected ship to has been changed in the meantime and can no longer be approved/rejected. Please reload the data and try again.",
			ExceptionErrorGroup.ERROR),
	SHIPTO_WORKFLOW_IDENTIFICATION_ERROR(
			4470,
			"Workflow instance identification error. Ship to is reset, workflow instance is terminated.",
			ExceptionErrorGroup.ERROR),
	SHIPTO_WORKFLOW_NO_TASK(4471, "We can't find any workflow instance entity for this ship to.", ExceptionErrorGroup.ERROR),
	SHIPTO_APPROVED_ROLLBACK(
			4472,
			"The selected ship to has been changed in the meantime and can no longer be reverted. Please reload the data and try again.",
			ExceptionErrorGroup.ERROR),
	SHIPTO_AWAITING_APPROVAL(
			4473,
			"Ship-to list has been updated and has to be approved before can be made effective.",
			ExceptionErrorGroup.VALIDATION),
	SHIPTO_ROLLED_BACK_MESSAGE(4474, "Rolled Back %s 'ship to' %s.", ExceptionErrorGroup.VALIDATION),
	PRICE_AWAITING_APPROVAL(
			4475,
			"Prices list has been updated and has to be approved before can be made effective.",
			ExceptionErrorGroup.VALIDATION),
	SHIPTO_AWAITING_PUBLISH(
			4476,
			"Ship-to list has been updated and has to be publiched before can be made effective.",
			ExceptionErrorGroup.VALIDATION),
	PRICE_AWAITING_PUBLISH(
			4477,
			"Prices list has been updated and has to be published before can be made effective.",
			ExceptionErrorGroup.VALIDATION),
	PERIOD_AWAITING_APPROVAL(
			4478,
			"Contract period has been updated and has to be approved before can be made effective.",
			ExceptionErrorGroup.VALIDATION),
	PERIOD_AWAITING_PUBLISH(
			4479,
			"Contract period has been updated and has to be published before can be made effective.",
			ExceptionErrorGroup.VALIDATION),

	WKF_CONTRACT_CHANGE_PRICE_SUBJECT(
			4480,
			"Contract price categories update request for %s at %s for %s awaiting your approval",
			ExceptionErrorGroup.VALIDATION),
	WKF_CONTRACT_CHANGE_SHIP_TO_SUBJECT(
			4481,
			"Contract ship-to update request for %s at %s for %s awaiting your approval",
			ExceptionErrorGroup.VALIDATION),
	WKF_CONTRACT_CHANGE_PERIOD_SUBJECT(
			4482,
			"Contract period update request for %s at %s for %s awaiting your approval",
			ExceptionErrorGroup.VALIDATION),
	PERIOD_WKF_INVALID(
			4483,
			"Contract period is invalid. The starting date of the contract is greater than the end date of the contract.",
			ExceptionErrorGroup.VALIDATION),

	BLUEPRINT_CONTRACT_PUBLISH(4501, "Blueprint contract cannot be merged into the original contract", ExceptionErrorGroup.ERROR),
	BLUEPRINT_CONTRACT_DISCARD(4502, "Blueprint contract cannot be discarded", ExceptionErrorGroup.ERROR),
	BLUEPRINT_CONTRACT_PUBLISH_VALIDATION(4503, "Cannot publish contract, unapproved changes are present!", ExceptionErrorGroup.VALIDATION),
	BLUEPRINT_CONTRACT_PUBLISH_WORKFLOW_VALIDATION(4504, "Cannot publish contract while workflow is not completed!", ExceptionErrorGroup.VALIDATION),

	ENTITY_CLONER_INSTANTION_ERROR(4505, "%s", "Error"),
	INVALID_SUBSIDIARIES(4506, "Cannot generate internale resale contract between thirdparties.", ExceptionErrorGroup.ERROR),
	DELETE_INTERNAL_RESALE_CONTRACT_MUST_BE_DRAFT(
			4508,
			"For the internal resale contract to be deleted, the status of the purchase contract must be draft.",
			ExceptionErrorGroup.ERROR),
	INTERNAL_RESALE_CONTRACT_NOT_FOUND(4509, "Internal resale contract not found!", ExceptionErrorGroup.ERROR),
	// import bid -start
	IMPORT_BID_XML_DIFFERENT_CLIENT(4510, "The received bidder code %s is not the same with the current client !", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_INDEX_PRICE_INVALID_XML(4511, "Could not extract Index price due to invalid XML for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_TAX_PRICE_NOT_PERCENTAGE(
			4512,
			"The price category from <TaxOnTax> with sort id %s must be a percentage !",
			ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_AIRLINE_CUSTOMER(
			4513,
			"Could not extract customer from code %S when processing airlines volume for bid %S",
			ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_DATA_PROVIDER(4514, "Could not extract data provider from code %s for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_EXCHANGE_RATE(4515, "Could not extract master agreement period from name %s for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_FLIGHT_SERVICE_TYPE(4516, "Could not extract flight service type %s for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_IATA_PC(4517, "Could not extract Iata PC from code %s for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_INVOICE_FREQ(4518, "Could not extract invoice frequency from code %s for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_INVOICE_TYPE(4519, "Could not extract invoice type from code %s for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_OPERATOR(4520, "Could not extract conversion operator from code %s for bid  %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_OPERATORS(4521, "Could not extract operators from code %s for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_PAYMENT_FREQUENCY(4522, "Could not extract invoice frequency from code %s for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_PAYMENT_DAY(4523, "Could not extract payment day type from code %s for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_PAYMENT_TYPE(4524, "Could not extract payment type from code %s for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_PERIOD_TYPE(4525, "Could not extract period type %s from total volume for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_PRODUCT(4526, "Could not extract product %s for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_QUANTITY_TYPE(4527, "Could not extract quantity type from code %s for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_QUOTATION_1(
			4528,
			"Could not find an active timeserie with code%s, data provider %s, unit conversion from %s to %s, conversion operator %s with factor %s for bid %s",
			ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_QUOTATION_2(
			4529,
			"Could not find an active quotation for timeserie  %s with averaging method %s for bid %s",
			ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_RESTRICTION_TYPE(4530, "Could not extract restriction types from code %s for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_SUB_TYPE(4531, "Could not extract contract sub type from code %s for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_SUPPLIERS(4532, "Could not extract suppliers from code %s for bid ", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_TAX_TYPE(4533, "Could not extract tax type %s for bid %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_TENDER_1(
			4534,
			"Could not find holder for code %s when extracting Tender for version %s and tender code %s!",
			ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_TENDER_2(
			4535,
			"Could not find tender for code %s, version %s and holder %s when extracting Tender!",
			ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_EXTRACT_TENDER_LOCATION(
			4536,
			"Could not extract tender location for locations with code %s and tender with code %s and tax type %s and product %s and flight service type %s and delivery point %s for bid %s",
			ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_NO_FIXED_PRICE_CATEGORY(
			4537,
			"No FIXED price category found, please check with system administrator !",
			ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_NO_INDEX_PRICE_CATEGORY(
			4538,
			"No INDEX price category found, please check with system administrator !",
			ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_NO_PRICE_CATEGORY(4539, "No price category found with IATA code %s and price ind %s", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_INCORRECT_USE_TYPE_TAX(4540, "Incorrect use type TAX for product fee for bid %s", ExceptionErrorGroup.ERROR),

	IMPORT_BID_XML_CHECK_LOC_DIFFERENT_FLIGHT_SERVICE_TYPE(
			4542,
			"Different flight service type for bid %s : %s(imported) vs %s(in system)",
			ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_CHECK_LOC_DIFFERENT_TAX_TYPE(
			4543,
			"Can't import because of different product tax type for bid %s : %s(imported) vs %s(in system)",
			ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_CHECK_LOC_DIFFERENT_FUEL_PRODUCT(
			4544,
			"Can't import because of different fuel product name for bid %s : %s(imported) vs%s(in system)",
			ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_CHECK_LOC_DIFFERENT_LOCATION(
			4545,
			"Can't import because of different IATA location code for bid %s : %s(imported) vs %s(in system)",
			ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_CHECK_LOC_DIFFERENT_ICAO_CODE(
			4546,
			"Can't import because of different ICAO location code for bid %s : %s vs %s(in system)",
			ExceptionErrorGroup.ERROR),

	IMPORT_BID_IMPORT_XML_CAN_NOT_PROCESS(4547, "Could not process XML from received data !", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XLS_CAN_NOT_PROCESS(4548, "Could not process XLS from received data !", ExceptionErrorGroup.ERROR),
	IMPORT_BID_UNKNOWN_APPLICATION_TYPE(4549, "Unknown application type !", ExceptionErrorGroup.ERROR),
	IMPORT_BID_COMMUNICATION_ERROR(4560, "Did not receive the file due to communication issues, please retry !", ExceptionErrorGroup.ERROR),

	IMPORT_BID_EXTRACT_TENDER_LOCATION(4561, "Could not extract tender location from received data ![%s]", ExceptionErrorGroup.ERROR),

	IMPORT_BID_XML_PARSE_ERROR(4562, "Could not parse received xml! [%s]", ExceptionErrorGroup.ERROR),
	IMPORT_BID_XML_VALIDATE_ERROR(4563, "Could not validate received xml! [%s]", ExceptionErrorGroup.ERROR),

	IMPORT_BID_OK(4564, "Sucessfully imported %s bid(s).", ExceptionErrorGroup.ERROR),

	SINGLE_RESULT_EXCEPTION_MESSAGE(4565, "Unknown %s %s present in file, please check %s and reprocess", ExceptionErrorGroup.ERROR),

	LINK_ATTACHMENT_TYPE_NOT_FOUND(4566, "Could not find attachment type of type %s in the system.", ExceptionErrorGroup.ERROR),

	IMPORT_BID_NO_PRICE_ERROR(4567, "Could not extract a price category from received data !", ExceptionErrorGroup.ERROR),

	IATA_ACKNOLEGMENT_NO_MSG_ID_FOUND(4568, "Could not find outbound message for id %s and message type %s !", ExceptionErrorGroup.ERROR),

	DATAHUB_INVALID_METHOD_CALL(4600, "Could not send request to datahub for object %s", ExceptionErrorGroup.ERROR),

	VALIDATION_ERRORS(4900, "Validation errors:", ExceptionErrorGroup.VALIDATION),
	COMMUNICATION_PROBLEM(5000, "Communication problem. Please contact system administrator.", ExceptionErrorGroup.ERROR),
	HTTP_PROBLEM(5001, "%s: %s", ExceptionErrorGroup.ERROR),

	NO_EMAIL_SENT(
			4610,
			"No emails were sent. Please make sure that there is a price update notification defined for the respective customer, also assigned to an area that the contract location is a part of, and that a template is correctly defined for this job.",
			ExceptionErrorGroup.ERROR),

	EX_RATE_IN_USE(
			6000,
			"Average Method with code %s can not be deactivated because it is used for Contract with code %s.",
			ExceptionErrorGroup.ERROR),

	TENDER_LOCATION_NOT_VALID(6100, "Tender Location not valid.", ExceptionErrorGroup.ERROR),
	FUEL_RECEIVER_NOT_VALID(6101, "Receiver with code %s not valid.", ExceptionErrorGroup.ERROR),
	BID_NOT_VALID(
			6102,
			"Action can not be performed for following bids: <br/> %s <br/> Tender Location or Fuel Receiver are not valid anymore.",
			ExceptionErrorGroup.ERROR);

	private final Logger LOG = LoggerFactory.getLogger(CmmErrorCode.class);

	private ResourceBundle boundle;

	private final int errNo;
	private final String errMsg;
	private final String errGroup;

	private CmmErrorCode(int errNo, String errMsg, String errGroup) {
		this.errNo = errNo;
		this.errMsg = errMsg;
		this.errGroup = errGroup;
	}

	@Override
	public String getErrGroup() {
		return this.errGroup;
	}

	@Override
	public int getErrNo() {
		return this.errNo;
	}

	@Override
	public String getErrMsg() {
		try {
			String language = Session.user.get().getSettings().getLanguage();
			if (language.equals("sys")) {
				this.boundle = ResourceBundle.getBundle("locale.sone.cmm.error.messages");
			} else {
				this.boundle = ResourceBundle.getBundle("locale.sone.cmm.error.messages", new Locale(language));
			}
		} catch (NullPointerException e) {
			this.LOG.trace("No session while testing", e);
			this.boundle = ResourceBundle.getBundle("locale.sone.cmm.error.messages");
		}
		return this.boundle.containsKey(Integer.toString(this.errNo)) ? this.boundle.getString(Integer.toString(this.errNo)) : this.errMsg;
	}

}
