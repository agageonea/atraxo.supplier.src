/**
 *
 */
package atraxo.cmm.business.ext.bid;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.ext.bid.IBidValidator;
import atraxo.cmm.business.api.tender.ITenderLocationAirlinesService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.fmbas.business.api.validationMessage.IValidationMessageService;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import atraxo.fmbas.domain.impl.fmbas_type.ValidationMessageSeverity;
import atraxo.fmbas.domain.impl.validationMessage.ValidationMessage;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public class BidValidator implements IBidValidator {

	@Autowired
	private IValidationMessageService validationMessageService;
	@Autowired
	private ITenderLocationAirlinesService tenderLocationAirlineService;
	@Autowired
	private IContractService contractService;

	@Override
	public List<ValidationMessage> validateBid(int bidId) throws BusinessException {
		Contract bid = this.contractService.findById(bidId, Contract.class);
		return this.validateBid(bid);
	}

	@Override
	public List<ValidationMessage> validateBid(Contract bid) throws BusinessException {
		List<ValidationMessage> errorList = new ArrayList<>();

		errorList.addAll(this.checkOfferedVolume(bid));
		errorList.addAll(this.checkProductPriceCategory(bid));
		errorList.addAll(this.checkLocationAndAirline(bid));

		return errorList;
	}

	private List<ValidationMessage> checkLocationAndAirline(Contract bid) throws BusinessException {
		List<ValidationMessage> messages = new ArrayList<>();
		if (bid.getBidTenderLocation().getIsValid()) {
			Boolean airlineIsValid = false;
			for (ShipTo bidShipTo : bid.getShipTo()) {
				TenderLocationAirlines tenderLocationAirline = this.tenderLocationAirlineService
						.findByTender_location_airline(bid.getBidTenderLocation(), bidShipTo.getCustomer());
				if (tenderLocationAirline.getIsValid()) {
					airlineIsValid = true;
					break;
				}
			}
			if (!airlineIsValid) {
				ValidationMessage hasNoValidAirlineValidation = this.validationMessageService.createValidationMessage(bid,
						"No valid airlines are assigned to this bid.", ValidationMessageSeverity._INFO_);
				messages.add(hasNoValidAirlineValidation);
			}
		} else {
			ValidationMessage hasNoValidLocationValidation = this.validationMessageService.createValidationMessage(bid, "The location is not valid.",
					ValidationMessageSeverity._INFO_);
			messages.add(hasNoValidLocationValidation);
		}
		return messages;
	}

	private List<ValidationMessage> checkOfferedVolume(Contract bid) throws BusinessException {
		List<ValidationMessage> messages = new ArrayList<>();
		if (bid.getOfferedVolume() == null || bid.getOfferedVolume().compareTo(BigDecimal.ZERO) == 0) {
			messages.add(this.validationMessageService.createValidationMessage(bid, CmmErrorCode.BID_NO_VOLUME.getErrMsg(),
					ValidationMessageSeverity._INFO_));
		}
		return messages;

	}

	private List<ValidationMessage> checkProductPriceCategory(Contract bid) throws BusinessException {
		Collection<ValidationMessage> messages = new ArrayList<>();
		boolean hasProductPrice = false;
		if (bid.getPricingBases() != null) {
			for (PricingBase pb : bid.getPricingBases()) {
				for (ContractPriceCategory priceCat : pb.getContractPriceCategories()) {
					if (priceCat.getPriceCategory().getType().equals(PriceType._PRODUCT_)) {
						hasProductPrice = true;
					}
				}
			}
		}
		if (!hasProductPrice) {
			ValidationMessage hasProductPriceCategoryValidation = this.validationMessageService.createValidationMessage(bid,
					CmmErrorCode.PRODUCT_PRICE_CATEGORY_IS_MISSING.getErrMsg(), ValidationMessageSeverity._INFO_);
			messages.add(hasProductPriceCategoryValidation);
		}
		return new ArrayList<>(messages);
	}

	@Override
	public void clearExistingValidations(Contract bid) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("objectId", bid.getRefid());
		params.put("objectType", bid.getClass().getSimpleName());
		List<ValidationMessage> validationMessages = this.validationMessageService.findEntitiesByAttributes(params);
		this.validationMessageService.delete(validationMessages);

	}

	@Override
	public void checkBidExistence(Contract bid) throws BusinessException {
		if (!bid.getIsContract()) {
			this.checkBusinessKey(bid);
		}
	}

	private void checkBusinessKey(Contract bid) throws BusinessException {
		// Create warning if bid already exists (bid identification fields)
		Map<String, Object> identificationParams = new HashMap<>();
		identificationParams.put("holder", bid.getHolder());
		identificationParams.put("bidVersion", bid.getBidVersion());
		identificationParams.put("bidRevision", bid.getBidRevision());
		identificationParams.put("location", bid.getBidTenderLocation().getLocation());
		identificationParams.put("bidTenderIdentification", bid.getBidTenderIdentification());
		identificationParams.put("product", Product.getByIataName(bid.getProduct().getIataName()));
		identificationParams.put("flightServiceType", bid.getFlightServiceType());
		identificationParams.put("tax", bid.getTax());

		if (!this.contractService.findEntitiesByAttributes(identificationParams).isEmpty()) {
			throw new BusinessException(CmmErrorCode.BID_ALREADY_EXISTS_WITH_DETAILS,
					String.format(CmmErrorCode.BID_ALREADY_EXISTS_WITH_DETAILS.getErrMsg(), bid.getBidTenderIdentification().getCode(),
							bid.getBidTenderIdentification().getTenderVersion(), bid.getHolder().getCode(),
							bid.getBidTenderLocation().getLocation().getIataCode(), bid.getBidRevision(), bid.getBidVersion(),
							bid.getProduct().getName(), bid.getTax().getName()));
		}
	}

}
