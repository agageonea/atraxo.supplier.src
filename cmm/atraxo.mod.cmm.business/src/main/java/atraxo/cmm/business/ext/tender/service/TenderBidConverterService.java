package atraxo.cmm.business.ext.tender.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.xml.sax.SAXException;

import atraxo.cmm.business.api.ext.tender.ITenderBidConverterService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.ext.tender.converter.ErrorModel;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBid;

public class TenderBidConverterService implements ITenderBidConverterService {

	private static final Logger LOG = LoggerFactory.getLogger(TenderBidConverterService.class);

	private String url;

	public TenderBidConverterService(String url) {
		super();
		this.url = url;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.ext.tender.ITenderBidConverterService#convert(byte[])
	 */
	@Override
	public byte[] convert(byte[] resource, String fileName) throws BusinessException {
		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(this.url);
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.addBinaryBody("file", resource, ContentType.APPLICATION_OCTET_STREAM, fileName);
			HttpEntity multipart = builder.build();
			post.setEntity(multipart);

			HttpResponse response = client.execute(post);
			HttpEntity responseEntity = response.getEntity();
			String responseString = EntityUtils.toString(responseEntity, "UTF-8");

			ObjectMapper mapper = new ObjectMapper();
			if (response.getStatusLine().getStatusCode() == 200) {
				FuelTenderBid bid = mapper.readValue(responseString, FuelTenderBid.class);
				this.generateXml(bid, out);
				return out.toByteArray();
			} else {
				ErrorModel error = mapper.readValue(responseString, ErrorModel.class);
				throw new BusinessException(CmmErrorCode.HTTP_PROBLEM,
						String.format(CmmErrorCode.HTTP_PROBLEM.getErrMsg(), error.getErrorCode(), error.getMessage()));
			}
		} catch (ClientProtocolException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
			throw new BusinessException(CmmErrorCode.COMMUNICATION_PROBLEM, CmmErrorCode.COMMUNICATION_PROBLEM.getErrMsg(), e);
		} catch (IOException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}
	}

	/**
	 * @param bid
	 * @param out
	 * @throws BusinessException
	 */
	private void generateXml(FuelTenderBid bid, ByteArrayOutputStream out) throws BusinessException {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(FuelTenderBid.class);
			Marshaller marshaller = jaxbContext.createMarshaller();
			SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = factory.newSchema(ClassPathResource.class.getResource("/xsd/iata/IATA_Fuel_Tender.xsd"));
			marshaller.setSchema(schema);
			marshaller.marshal(bid, out);
		} catch (JAXBException | SAXException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}
	}
}
