package atraxo.cmm.business.ext.contracts.job.notification;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import atraxo.ad.business.api.system.IDateFormatMaskService;
import atraxo.ad.business.api.system.IDateFormatService;
import atraxo.ad.domain.impl.system.DateFormat;
import atraxo.ad.domain.impl.system.DateFormatMask;
import atraxo.cmm.business.ext.mailMerge.delegate.EntityDtoDelegate;
import atraxo.cmm.domain.ext.mailmerge.dto.ContractTemplate;
import atraxo.cmm.domain.ext.mailmerge.dto.EmailTemplates;
import atraxo.fmbas.business.api.customer.ICustomerNotificationService;
import atraxo.fmbas.business.ext.exceptions.mailMerge.MailMergeApplicationFaulExeption;
import atraxo.fmbas.business.ext.exceptions.mailMerge.MailMergeTemplateNotExistsExeption;
import atraxo.fmbas.business.ext.exceptions.mailMerge.MailMergeValidationFaultExeption;
import atraxo.fmbas.business.ext.mailMerge.MailMergeManager;
import atraxo.fmbas.business.ext.mailMerge.service.MailMerge_Service;
import atraxo.fmbas.domain.impl.customer.CustomerNotification;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * @author zspeter
 */
public class NotificationsItemProcessor implements ItemProcessor<CustomerData, CustomerData> {

	private static final Logger LOG = LoggerFactory.getLogger(NotificationsItemProcessor.class);

	@Autowired
	private ICustomerNotificationService customerNotificationService;
	@Autowired
	private EntityDtoDelegate dtoDelegate;
	@Autowired
	private MailMerge_Service mailService;
	@Autowired
	private IDateFormatMaskService dtfmtMaskService;
	@Autowired
	private IDateFormatService dtfmtService;

	private ExecutionContext executionContext;
	private StepExecution stepExecution;

	protected static final String NO_TEMPLATE = "no_template";
	protected static final String NO_CONTACTS = "no_contacts";
	protected static final String NOTIFIED = "notified";
	private static final String PRICES_EFFECTIVE_FROM = "PRICES EFFECTIVE FROM";
	private static final String PRICES_VALID_TO = "PRICES VALID TO";

	private static final String ORIGINAL = "original";

	private enum ParamName {
		SUBSIDIARY, DATEFORMAT, OUTPUTFILEFORMAT
	}

	/**
	 * @param stepExecution
	 */
	@BeforeStep
	public void storeExecutionContext(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
		this.executionContext = stepExecution.getExecutionContext();
	}

	@SuppressWarnings("unchecked")
	@Override
	public CustomerData process(CustomerData item) throws Exception {
		String dateFormat = this.stepExecution.getJobParameters().getString(ParamName.DATEFORMAT.toString());
		String fileExtension = this.stepExecution.getJobParameters().getString(ParamName.OUTPUTFILEFORMAT.toString());
		String subsidiary = this.stepExecution.getJobParameters().getString(ParamName.SUBSIDIARY.toString());
		String customStartDateStr = this.stepExecution.getJobParameters().getString(PRICES_EFFECTIVE_FROM);
		String customEndDateStr = this.stepExecution.getJobParameters().getString(PRICES_VALID_TO);
		Date customStartDate = null;
		Date customEndDate = null;
		SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd yyyy hh:mm:ss");
		if (!StringUtils.isEmpty(customStartDateStr)) {
			customStartDate = sdf.parse(customStartDateStr);
			LOG.debug("Customer start date:" + customStartDate);
		}
		if (!StringUtils.isEmpty(customStartDateStr)) {
			customEndDate = sdf.parse(customEndDateStr);
			LOG.debug("Customer end date:" + customEndDate);
		}
		DateFormat df = this.dtfmtService.findByName(dateFormat);
		DateFormatMask dateFormatMask = this.dtfmtMaskService.findByName(df, "JAVA_DATE_FORMAT");
		try {
			String hql = "select e from " + CustomerNotification.class.getSimpleName()
					+ " e where e.clientId =:clientId and e.customer.id=:customerId and e.notificationEvent.name=:notificationEvent";
			List<CustomerNotification> notifications = this.customerNotificationService.getEntityManager()
					.createQuery(hql, CustomerNotification.class).setParameter("clientId", Session.user.get().getClientId())
					.setParameter("customerId", item.getCustomerId()).setParameter("notificationEvent", "Contract price update").getResultList();

			if (CollectionUtils.isEmpty(notifications)) {
				throw new MailMergeTemplateNotExistsExeption();
			}
			this.sendMail(item, dateFormatMask.getValue(), fileExtension, subsidiary, notifications, customStartDate, customEndDate);
		} catch (MailMergeTemplateNotExistsExeption e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
			List<String> failed = new ArrayList<>();
			if (this.executionContext.containsKey(NO_TEMPLATE)) {
				failed = (List<String>) this.executionContext.get(NO_TEMPLATE);
			}
			failed.add(item.getCode());
			this.executionContext.put(NO_TEMPLATE, failed);
		} catch (MailMergeApplicationFaulExeption | MailMergeValidationFaultExeption e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
			List<String> failed = new ArrayList<>();
			if (this.executionContext.containsKey(NO_CONTACTS)) {
				failed = (List<String>) this.executionContext.get(NO_CONTACTS);
			}
			failed.add(item.getCode());
			this.executionContext.put(NO_CONTACTS, failed);
		}
		return item;
	}

	@SuppressWarnings("unchecked")
	private void sendMail(CustomerData item, String dateFormat, String fileExtension, String subsidiary, List<CustomerNotification> notifications,
			Date customStartDate, Date customEndDate) throws BusinessException {
		if (!item.getContractsList().isEmpty()) {
			EmailTemplates emailTemplates = this.dtoDelegate.generateData(item, subsidiary, customStartDate, customEndDate, notifications);
			if (emailTemplates != null) {
				for (ContractTemplate contractTemplate : emailTemplates.getContractTemplates()) {
					LOG.debug("Contract price update notification customer:" + contractTemplate.getCustomer().getCode() + " contract number:"
							+ contractTemplate.getContracts().size());
					String fileExtensionFinal = this.getFileExtension(fileExtension, contractTemplate.getNotification().getTemplate().getName());
					String json = !emailTemplates.getEmail().getTo().isEmpty()
							? MailMergeManager.getDataJson(contractTemplate.getCustomer(), fileExtensionFinal, emailTemplates.getEmail(), dateFormat)
							: null;
					if (json != null) {
						LOG.debug("Send email to customer:" + contractTemplate.getCustomer().getCode());
						this.mailService.sendMail(contractTemplate.getNotification().getTemplate(), contractTemplate.getNotification().getEmailBody(),
								json);
					}
				}
				List<String> success = new ArrayList<>();
				if (this.executionContext.containsKey(NOTIFIED)) {
					success = (List<String>) this.executionContext.get(NOTIFIED);
				}
				success.add(item.getCode());
				this.executionContext.put(NOTIFIED, success);
			}

		}
	}

	private String getFileExtension(String fileExtension, String fileName) {
		if (fileExtension.equalsIgnoreCase(NotificationsItemProcessor.ORIGINAL)) {
			return fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length());
		} else {
			return fileExtension.toLowerCase().trim();
		}
	}
}
