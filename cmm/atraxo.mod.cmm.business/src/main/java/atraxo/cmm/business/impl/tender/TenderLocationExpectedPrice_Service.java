/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.tender;

import atraxo.cmm.business.api.tender.ITenderLocationExpectedPriceService;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationExpectedPrice;
import atraxo.fmbas.domain.impl.categories.IataPC;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link TenderLocationExpectedPrice} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class TenderLocationExpectedPrice_Service
		extends
			AbstractEntityService<TenderLocationExpectedPrice>
		implements
			ITenderLocationExpectedPriceService {

	/**
	 * Public constructor for TenderLocationExpectedPrice_Service
	 */
	public TenderLocationExpectedPrice_Service() {
		super();
	}

	/**
	 * Public constructor for TenderLocationExpectedPrice_Service
	 * 
	 * @param em
	 */
	public TenderLocationExpectedPrice_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<TenderLocationExpectedPrice> getEntityClass() {
		return TenderLocationExpectedPrice.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return TenderLocationExpectedPrice
	 */
	public TenderLocationExpectedPrice findByTender_location_expected_fees(
			TenderLocation tenderLocation, IataPC iataPriceCategory) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							TenderLocationExpectedPrice.NQ_FIND_BY_TENDER_LOCATION_EXPECTED_FEES,
							TenderLocationExpectedPrice.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("tenderLocation", tenderLocation)
					.setParameter("iataPriceCategory", iataPriceCategory)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TenderLocationExpectedPrice",
							"tenderLocation, iataPriceCategory"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TenderLocationExpectedPrice",
							"tenderLocation, iataPriceCategory"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return TenderLocationExpectedPrice
	 */
	public TenderLocationExpectedPrice findByTender_location_expected_fees(
			Long tenderLocationId, Long iataPriceCategoryId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							TenderLocationExpectedPrice.NQ_FIND_BY_TENDER_LOCATION_EXPECTED_FEES_PRIMITIVE,
							TenderLocationExpectedPrice.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("tenderLocationId", tenderLocationId)
					.setParameter("iataPriceCategoryId", iataPriceCategoryId)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TenderLocationExpectedPrice",
							"tenderLocationId, iataPriceCategoryId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TenderLocationExpectedPrice",
							"tenderLocationId, iataPriceCategoryId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return TenderLocationExpectedPrice
	 */
	public TenderLocationExpectedPrice findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							TenderLocationExpectedPrice.NQ_FIND_BY_BUSINESS,
							TenderLocationExpectedPrice.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TenderLocationExpectedPrice", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TenderLocationExpectedPrice", "id"), nure);
		}
	}

	/**
	 * Find by reference: tenderLocation
	 *
	 * @param tenderLocation
	 * @return List<TenderLocationExpectedPrice>
	 */
	public List<TenderLocationExpectedPrice> findByTenderLocation(
			TenderLocation tenderLocation) {
		return this.findByTenderLocationId(tenderLocation.getId());
	}
	/**
	 * Find by ID of reference: tenderLocation.id
	 *
	 * @param tenderLocationId
	 * @return List<TenderLocationExpectedPrice>
	 */
	public List<TenderLocationExpectedPrice> findByTenderLocationId(
			Integer tenderLocationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocationExpectedPrice e where e.clientId = :clientId and e.tenderLocation.id = :tenderLocationId",
						TenderLocationExpectedPrice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("tenderLocationId", tenderLocationId)
				.getResultList();
	}
	/**
	 * Find by reference: iataPriceCategory
	 *
	 * @param iataPriceCategory
	 * @return List<TenderLocationExpectedPrice>
	 */
	public List<TenderLocationExpectedPrice> findByIataPriceCategory(
			IataPC iataPriceCategory) {
		return this.findByIataPriceCategoryId(iataPriceCategory.getId());
	}
	/**
	 * Find by ID of reference: iataPriceCategory.id
	 *
	 * @param iataPriceCategoryId
	 * @return List<TenderLocationExpectedPrice>
	 */
	public List<TenderLocationExpectedPrice> findByIataPriceCategoryId(
			Integer iataPriceCategoryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocationExpectedPrice e where e.clientId = :clientId and e.iataPriceCategory.id = :iataPriceCategoryId",
						TenderLocationExpectedPrice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("iataPriceCategoryId", iataPriceCategoryId)
				.getResultList();
	}
}
