package atraxo.cmm.business.ws.tender.transformer;

import java.util.List;

import javax.xml.bind.JAXBException;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.iata.fuelplus.iata.tender.DeclinedBid;
import seava.j4e.iata.fuelplus.iata.tender.DeclinedBids;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderDeclineAward;

public class TenderBidDeclineAwardTransformer extends DataHubOutTransformer<Contract, FuelTenderDeclineAward> {

	@Override
	public FuelTenderDeclineAward transformModelToDTO(Contract modelEntity, long msgId, String reason) throws BusinessException, JAXBException {
		FuelTenderDeclineAward declineAwardBid = new FuelTenderDeclineAward();
		declineAwardBid.setTenderIdentification(this.buildTenderIdentification(modelEntity.getBidTenderIdentification(), msgId));

		DeclinedBids bids = new DeclinedBids();
		bids.getDeclinedBid().add(this.buildDeclinedBid(modelEntity, reason));
		declineAwardBid.setDeclinedBids(bids);
		declineAwardBid.setSchemaVersion(modelEntity.getBidTenderIdentification().getSchemaVersion());

		return declineAwardBid;
	}

	@Override
	public FuelTenderDeclineAward transformModelToDTO(Contract modelEntity, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

	@Override
	public FuelTenderDeclineAward transformModelsToDTO(List<Contract> modelEntities, long msgId, String reason)
			throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

	@Override
	public FuelTenderDeclineAward transformModelsToDTO(List<Contract> modelEntities, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

	private DeclinedBid buildDeclinedBid(Contract contract, String reason) throws BusinessException {
		DeclinedBid declinedBid = new DeclinedBid();
		declinedBid.setBidIdentification(this.buildBidIdentification(contract));
		declinedBid.setComments(reason);

		return declinedBid;
	}
}
