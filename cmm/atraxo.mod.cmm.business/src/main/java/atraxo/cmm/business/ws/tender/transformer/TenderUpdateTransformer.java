package atraxo.cmm.business.ws.tender.transformer;

import java.util.Collection;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.api.ext.datahub.IDataHubInTransformer;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderUpdate;

public class TenderUpdateTransformer extends DataHubInTransformer<FuelTenderUpdate, Tender>
		implements IDataHubInTransformer<Tender, FuelTenderUpdate> {

	private static final Logger LOGGER = LoggerFactory.getLogger(TenderUpdateTransformer.class);

	@Override
	public Tender transformDTOToModel(FuelTenderUpdate update) throws BusinessException, JAXBException {
		Tender tender = new Tender();

		try {
			tender.setSchemaVersion(update.getSchemaVersion());
			tender.setCode(update.getTenderHeaderUpdate().getTenderIdentification().getTenderCode());
			tender.setTenderVersion(update.getTenderHeaderUpdate().getTenderIdentification().getTenderVersion().longValueExact());

			if (update.getTenderHeaderUpdate().getTenderIdentification().getTenderName() != null) {
				tender.setName(update.getTenderHeaderUpdate().getTenderIdentification().getTenderName());
			}
			if (update.getTenderHeaderUpdate().getTenderIdentification().getTenderHolderCode() != null) {
				tender.setHolder(
						this.findCustomer(update.getTenderHeaderUpdate().getTenderIdentification().getTenderHolderCode(), "TenderHolderCode"));
			}
			tender.setMsgId(update.getTenderHeaderUpdate().getTenderIdentification().getMsgId());

			tender.setContact(this.getContact(tender.getHolder(), update.getTenderHeaderUpdate().getTenderHolderContact().getContact()));
			tender.setContactPerson(update.getTenderHeaderUpdate().getTenderHolderContact().getContact().getContactPerson());
			tender.setEmail(update.getTenderHeaderUpdate().getTenderHolderContact().getContact().getEmail());
			if (update.getTenderHeaderUpdate().getTenderHolderContact().getContact().getPhone() != null) {
				tender.setPhone(update.getTenderHeaderUpdate().getTenderHolderContact().getContact().getPhone());
			}

			if (update.getTenderHeaderUpdate().getBiddingPeriod() != null) {

				if (update.getTenderHeaderUpdate().getBiddingPeriod().getFrom() == null) {
					throw new BusinessException(BusinessErrorCode.VALID_FROM_NULL, BusinessErrorCode.VALID_FROM_NULL.getErrMsg());
				} else if (update.getTenderHeaderUpdate().getBiddingPeriod().getTo() == null) {
					throw new BusinessException(BusinessErrorCode.VALID_TO_NULL, BusinessErrorCode.VALID_TO_NULL.getErrMsg());
				} else {
					if (update.getTenderHeaderUpdate().getBiddingPeriod().getFrom().toGregorianCalendar()
							.before(update.getTenderHeaderUpdate().getBiddingPeriod().getTo().toGregorianCalendar())) {
						tender.setBiddingPeriodFrom(update.getTenderHeaderUpdate().getBiddingPeriod().getFrom().toGregorianCalendar().getTime());
						tender.setBiddingPeriodTo(update.getTenderHeaderUpdate().getBiddingPeriod().getTo().toGregorianCalendar().getTime());
					} else {
						throw new BusinessException(BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
								BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO.getErrMsg());
					}
				}

			} else {
				throw new BusinessException(BusinessErrorCode.VALID_FROM_TO_NULL, BusinessErrorCode.VALID_FROM_TO_NULL.getErrMsg());
			}
			tender.setComments(update.getTenderHeaderUpdate().getComments());

			// Add Tender Locations
			List<atraxo.cmm.domain.impl.tender.TenderLocation> tenderLocations = this.buildTenderLocations(tender, update.getTenderLocations());
			tender.setTenderLocation(tenderLocations);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw e;
		}
		return tender;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.ext.datahub.IDataHubInTransformer#transformDTOToModels(java.lang.Object)
	 */
	@Override
	public Collection<Tender> transformDTOToModels(FuelTenderUpdate dto) throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}
}
