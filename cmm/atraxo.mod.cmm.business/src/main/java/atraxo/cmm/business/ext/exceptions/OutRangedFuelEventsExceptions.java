package atraxo.cmm.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.IErrorCode;

public class OutRangedFuelEventsExceptions extends BusinessException {

	public OutRangedFuelEventsExceptions(IErrorCode errorCode, String errorDetails) {
		super(errorCode, errorDetails);
	}

}
