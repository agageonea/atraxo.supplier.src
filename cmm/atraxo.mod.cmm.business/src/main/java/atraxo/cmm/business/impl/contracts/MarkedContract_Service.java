/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.contracts;

import atraxo.cmm.domain.impl.contracts.MarkedContract;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link MarkedContract} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class MarkedContract_Service
		extends
			AbstractEntityService<MarkedContract> {

	/**
	 * Public constructor for MarkedContract_Service
	 */
	public MarkedContract_Service() {
		super();
	}

	/**
	 * Public constructor for MarkedContract_Service
	 * 
	 * @param em
	 */
	public MarkedContract_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<MarkedContract> getEntityClass() {
		return MarkedContract.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return MarkedContract
	 */
	public MarkedContract findByContractId(Integer contractId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(MarkedContract.NQ_FIND_BY_CONTRACTID,
							MarkedContract.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("contractId", contractId).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"MarkedContract", "contractId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"MarkedContract", "contractId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return MarkedContract
	 */
	public MarkedContract findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(MarkedContract.NQ_FIND_BY_BUSINESS,
							MarkedContract.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"MarkedContract", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"MarkedContract", "id"), nure);
		}
	}

}
