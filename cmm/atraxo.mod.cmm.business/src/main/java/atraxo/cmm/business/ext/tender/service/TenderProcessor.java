/**
 *
 */
package atraxo.cmm.business.ext.tender.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.xml.ws.handler.MessageContext;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.ad.business.api.attachment.IAttachmentService;
import atraxo.ad.business.api.attachment.IAttachmentTypeService;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.cmm.business.api.ext.datahub.IDataHubClientRequestService;
import atraxo.cmm.business.api.ext.tender.IAcknowledgmentService;
import atraxo.cmm.business.api.ext.tender.ITenderProcessor;
import atraxo.cmm.business.api.ext.tender.updater.ITenderUpdaterService;
import atraxo.cmm.business.api.tender.ITenderLocationService;
import atraxo.cmm.business.api.tender.ITenderService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ws.tender.client.DataHubSoapClient;
import atraxo.cmm.business.ws.tender.transformer.TenderAcknowledgmentTransformer;
import atraxo.cmm.business.ws.tender.transformer.TenderInvitationTransformer;
import atraxo.cmm.business.ws.tender.transformer.TenderReadAcknowledgmentTransformer;
import atraxo.cmm.business.ws.tender.transformer.TenderUpdateTransformer;
import atraxo.cmm.domain.ext.tender.DataHubRequestType;
import atraxo.cmm.domain.ext.tender.MessageStatus;
import atraxo.cmm.domain.ext.tender.MessageType;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import atraxo.fmbas.business.api.contacts.IContactsService;
import atraxo.fmbas.business.api.dictionary.IDictionaryService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.DuplicateEntityException;
import atraxo.fmbas.business.ext.exceptions.SingleResultException;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import atraxo.fmbas.domain.ws.dto.WSExecutionType;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.commons.utils.XMLUtils;
import seava.j4e.iata.fuelplus.iata.tender.Documents;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcknowledge;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderCancel;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderInvitation;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderNewRound;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderResponse;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderUpdate;
import seava.j4e.iata.fuelplus.iata.tender.LocationHeader;
import seava.j4e.iata.fuelplus.iata.tender.LocationsRound.LocationRound;

/**
 * @author zspeter
 */
public class TenderProcessor extends AbstractMessageProcessor implements ITenderProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(TenderProcessor.class);

	private static final String QUERY_PARAM_BID_TENDER_IDENTIFICATION = "bidTenderIdentification";

	private static final String HISTORY_MSG_TENDER_CANCEL = "Tender Cancel";

	@Autowired
	private IAcknowledgmentService ackService;
	@Autowired
	private IDictionaryService dictionaryService;
	@Autowired
	private TenderInvitationTransformer tenderInvitationTransformer;
	@Autowired
	private TenderUpdateTransformer tenderUpdateTransformer;
	@Autowired
	private ITenderService tenderService;
	@Autowired
	private ITenderUpdaterService updaterService;
	@Autowired
	private IContactsService contactService;
	@Autowired
	private IAttachmentTypeService attachmentTypeService;
	@Autowired
	private IAttachmentService attachementService;
	@Autowired
	private TenderAcknowledgmentTransformer tenderAckTransformer;
	@Autowired
	private TenderReadAcknowledgmentTransformer tenderReadAckTransformer;
	@Autowired
	private ITenderLocationService tenderLocationService;

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.tender.ITenderProcessor#fuelTenderInvitation(seava.j4e.iata.fuelplus.iata.tender.FuelTenderInvitation)
	 */
	@Override
	public FuelTenderResponse fuelTenderInvitation(FuelTenderInvitation tenderInvitation, MessageContext messageContext) {
		FuelTenderResponse response = new FuelTenderResponse();

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		// start constructing the interface execution result
		WSExecutionResult execResult = new WSExecutionResult();
		execResult.setExecutionType(WSExecutionType.IMPORT_DATA);
		execResult.setSuccess(false);

		try {
			this.initSessionAndInterface(ExtInterfaceNames.FUEL_TENDER_INVITATION_INTERFACE.getValue(), messageContext);

			if (this.getFuelTenderInterface().getEnabled()) {

				// Translate FuelTenderInvitation parameter
				this.dictionaryService.translate(this.getFuelTenderInterface(), tenderInvitation);

				// Build Tender from FuelTenderInvitation
				Tender tender = this.tenderInvitationTransformer.transformDTOToModel(tenderInvitation);
				long msgId = tenderInvitation.getTenderHeader().getTenderIdentification().getMsgId();

				// Verify if the Tender already exists
				this.checkBusinessKey(tender);
				this.activateRounds(tender);
				// Save Header, Locations and Volumes
				tender.setIsRead(false);
				this.saveTender(tender);
				this.saveAttachments(tenderInvitation.getTenderHeader().getTenderIdentification().getDocuments(), tender);

				// Save Change History
				this.historyService.saveChangeHistory(tender, tender.getSource().getName(), "New Tender Invitation");

				// Add header comments as notes
				this.saveNotesIfComments(tender, tender.getComments());

				// set the XML as received data
				String xml = XMLUtils.toString(tenderInvitation, FuelTenderInvitation.class, true, true);

				execResult.setExecutionData(xml);
				execResult.setSuccess(Boolean.TRUE);

				stopWatch.stop();

				// save interface history
				this.interfaceHistoryService.saveInterfaceHistory(stopWatch, execResult, this.getFuelTenderInterface());

				this.sendNotificationFromTender(tender);

				response.setResponse(SUCCESS_MSG);

				// send acknowledgement
				this.sendAcknowledgement(tender, msgId, ExtInterfaceNames.FUEL_TENDER_INVITATION_INTERFACE);
			} else {
				throw new BusinessException(BusinessErrorCode.INTERFACE_DISABLED, BusinessErrorCode.INTERFACE_DISABLED.getErrMsg());
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			execResult.setExecutionData(e.getLocalizedMessage() + "::" + ExceptionUtils.getStackTrace(e));

			try {
				this.interfaceHistoryService.saveInterfaceHistory(stopWatch, execResult, this.getFuelTenderInterface());
				this.interfaceHistoryService.sendNotification(this.getFuelTenderInterface(), null);
			} catch (Exception e1) {
				LOGGER.error(e1.getMessage(), e1);
			}
			response.setResponse(ERROR_MSG);
			response.setErrorDescription(e.getLocalizedMessage());
		}
		return response;
	}

	/**
	 * @param tender
	 */
	private void activateRounds(Tender tender) {
		for (TenderLocation loc : tender.getTenderLocation()) {
			if (!CollectionUtils.isEmpty(loc.getTenderLocationRounds())) {
				List<TenderLocationRound> rounds = new ArrayList<>(loc.getTenderLocationRounds());
				if (rounds.stream().filter(TenderLocationRound::getActive).count() == 0) {
					rounds.sort((o1, o2) -> o1.getRoundNo().compareTo(o2.getRoundNo()));
					rounds.stream().forEach(e -> e.setActive(false));
					Optional<TenderLocationRound> firstElement = rounds.stream().findFirst();
					if (firstElement.isPresent()) {
						firstElement.get().setActive(true);
					}
				}
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.tender.ITenderProcessor#fuelTenderNewRound(seava.j4e.iata.fuelplus.iata.tender.FuelTenderNewRound)
	 */
	@Override
	public FuelTenderResponse fuelTenderNewRound(FuelTenderNewRound tenderNewRound, MessageContext messageContext) {
		FuelTenderResponse response = new FuelTenderResponse();

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		// start constructing the interface execution result
		WSExecutionResult execResult = new WSExecutionResult();
		execResult.setExecutionType(WSExecutionType.IMPORT_DATA);
		execResult.setSuccess(false);

		try {
			this.initSessionAndInterface(ExtInterfaceNames.FUEL_TENDER_NEW_ROUND_INTERFACE.getValue(), messageContext);

			if (this.getFuelTenderInterface().getEnabled()) {
				// identifies the tender that corresponds with the tenderIdentification code
				Tender th = this.findTendersByIdentification(tenderNewRound.getTenderIdentification());
				long msgId = tenderNewRound.getTenderIdentification().getMsgId();

				for (LocationRound loc : tenderNewRound.getLocationsRound().getLocationRound()) {

					// returns the locations of the tender based on tender and locationHeader from XML
					List<TenderLocation> tenderLocations = this.findTenderLocationsByLocationHeader(th, loc.getLocationHeader());

					for (TenderLocation tenderLocation : tenderLocations) {

						// update existing rounds
						this.updateTenderLocationRounds(tenderLocation, loc);

						// Save Change History
						this.historyService.saveChangeHistory(tenderLocation, BiddingStatus._NEW_.getName(), "Tender New Round");
					}

				}

				// set the XML as received data
				String xml = XMLUtils.toString(tenderNewRound, FuelTenderNewRound.class, true, true);

				execResult.setExecutionData(xml);
				execResult.setSuccess(Boolean.TRUE);

				stopWatch.stop();

				// save interface history
				this.interfaceHistoryService.saveInterfaceHistory(stopWatch, execResult, this.getFuelTenderInterface());

				response.setResponse(SUCCESS_MSG);

				// send acknowledgement
				this.sendAcknowledgement(th, msgId, ExtInterfaceNames.FUEL_TENDER_NEW_ROUND_INTERFACE);
			} else {
				throw new BusinessException(BusinessErrorCode.INTERFACE_DISABLED, BusinessErrorCode.INTERFACE_DISABLED.getErrMsg());
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			execResult.setExecutionData(e.getLocalizedMessage() + "::" + ExceptionUtils.getStackTrace(e));

			try {
				this.interfaceHistoryService.saveInterfaceHistory(stopWatch, execResult, this.getFuelTenderInterface());
			} catch (BusinessException e1) {
				LOGGER.error(e.getMessage(), e1);
			}

			response.setResponse(ERROR_MSG);
			response.setErrorDescription(e.getLocalizedMessage() + "::" + ExceptionUtils.getStackTrace(e));
		}

		return response;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.tender.ITenderProcessor#fuelTenderCancel(seava.j4e.iata.fuelplus.iata.tender.FuelTenderCancel)
	 */
	@Override
	public FuelTenderResponse fuelTenderCancel(FuelTenderCancel tenderCancel, MessageContext messageContext) {
		FuelTenderResponse response = new FuelTenderResponse();

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		// start constructing the interface execution result
		WSExecutionResult execResult = new WSExecutionResult();
		execResult.setExecutionType(WSExecutionType.IMPORT_DATA);
		execResult.setSuccess(false);

		try {
			this.initSessionAndInterface(ExtInterfaceNames.FUEL_TENDER_CANCEL_INTERFACE.getValue(), messageContext);

			if (this.getFuelTenderInterface().getEnabled()) {
				Tender th = this.findTendersByIdentification(tenderCancel.getTenderIdentification());
				long msgId = tenderCancel.getTenderIdentification().getMsgId();

				// Update Bidding Status for all locations
				for (TenderLocation location : th.getTenderLocation()) {
					location.setLocationBiddingStatus(BiddingStatus._FINISHED_);

					// Save Change History
					this.historyService.saveChangeHistory(location, BiddingStatus._FINISHED_.getName(), HISTORY_MSG_TENDER_CANCEL);
				}

				// Find and update bids that were already transmitted
				Map<String, Object> params = new HashMap<>();
				params.put(QUERY_PARAM_BID_TENDER_IDENTIFICATION, th);
				params.put("bidTransmissionStatus", TransmissionStatus._TRANSMITTED_);
				List<Contract> bids = this.contractService.findEntitiesByAttributes(params);

				for (Contract bid : bids) {
					bid.setBidStatus(BidStatus._DECLINED_);
					this.contractService.update(bid);

					// Save Change History
					this.historyService.saveChangeHistory(bid, BidStatus._DECLINED_.getName(), HISTORY_MSG_TENDER_CANCEL);
				}

				th.setStatus(TenderStatus._CANCELED_);
				th.setBiddingStatus(BiddingStatus._FINISHED_);
				th.setCanceledOn(new Date());
				this.tenderService.update(th);

				// save notes
				this.saveNotesIfComments(th, tenderCancel.getComments());

				// Save Change History
				this.historyService.saveChangeHistory(th, TenderStatus._CANCELED_.getName(), HISTORY_MSG_TENDER_CANCEL);

				// set the XML as received data
				String xml = XMLUtils.toString(tenderCancel, FuelTenderCancel.class, true, true);

				execResult.setExecutionData(xml);
				execResult.setSuccess(Boolean.TRUE);

				stopWatch.stop();

				// save interface history
				this.interfaceHistoryService.saveInterfaceHistory(stopWatch, execResult, this.getFuelTenderInterface());

				response.setResponse(SUCCESS_MSG);

				// send acknowledgement
				this.sendAcknowledgement(th, msgId, ExtInterfaceNames.FUEL_TENDER_CANCEL_INTERFACE);
			} else {
				throw new BusinessException(BusinessErrorCode.INTERFACE_DISABLED, BusinessErrorCode.INTERFACE_DISABLED.getErrMsg());
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			execResult.setExecutionData(e.getLocalizedMessage() + "::" + ExceptionUtils.getStackTrace(e));

			try {
				this.interfaceHistoryService.saveInterfaceHistory(stopWatch, execResult, this.getFuelTenderInterface());
			} catch (Exception e1) {
				LOGGER.error(e1.getMessage(), e1);
			}

			response.setResponse(ERROR_MSG);
			response.setErrorDescription(e.getLocalizedMessage());
		}

		return response;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.tender.ITenderProcessor#fuelTenderUpdate(seava.j4e.iata.fuelplus.iata.tender.FuelTenderUpdate)
	 */
	@Override
	public FuelTenderResponse fuelTenderUpdate(FuelTenderUpdate tenderUpdate, MessageContext messageContext) {
		FuelTenderResponse response = new FuelTenderResponse();

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		WSExecutionResult execResult = new WSExecutionResult();
		execResult.setExecutionType(WSExecutionType.IMPORT_DATA);
		execResult.setSuccess(false);

		try {
			this.initSessionAndInterface(ExtInterfaceNames.FUEL_TENDER_UPDATE_INTERFACE.getValue(), messageContext);
			if (this.getFuelTenderInterface().getEnabled()) {

				// Translate FuelTenderUpdate parameter
				this.dictionaryService.translate(this.getFuelTenderInterface(), tenderUpdate);

				Tender receivedTender = this.tenderUpdateTransformer.transformDTOToModel(tenderUpdate);
				Tender originalTender = this.updaterService.updateTender(receivedTender);
				this.saveAttachments(tenderUpdate.getTenderHeaderUpdate().getTenderIdentification().getDocuments(), originalTender);

				String xml = XMLUtils.toString(tenderUpdate, FuelTenderUpdate.class, true, true);
				execResult.setExecutionData(xml);
				execResult.setSuccess(Boolean.TRUE);
				stopWatch.stop();

				this.interfaceHistoryService.saveInterfaceHistory(stopWatch, execResult, this.getFuelTenderInterface());
				this.sendNotificationFromTender(originalTender);
				response.setResponse(SUCCESS_MSG);

				// send acknowledgement
				long msgId = tenderUpdate.getTenderHeaderUpdate().getTenderIdentification().getMsgId();
				this.sendAcknowledgement(receivedTender, msgId, ExtInterfaceNames.FUEL_TENDER_UPDATE_INTERFACE);
			} else {
				throw new BusinessException(BusinessErrorCode.INTERFACE_DISABLED, BusinessErrorCode.INTERFACE_DISABLED.getErrMsg());
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			execResult.setExecutionData(e.getLocalizedMessage() + "::" + ExceptionUtils.getStackTrace(e));

			try {
				this.interfaceHistoryService.saveInterfaceHistory(stopWatch, execResult, this.getFuelTenderInterface());
				this.interfaceHistoryService.sendNotification(this.getFuelTenderInterface(), null);
			} catch (Exception e1) {
				LOGGER.error(e1.getMessage(), e1);
			}
			response.setResponse(ERROR_MSG);
			response.setErrorDescription(e.getLocalizedMessage());
		}
		return response;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.tender.ITenderProcessor#fuelTenderAcknowledgement(seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcknowledge)
	 */
	@Override
	public FuelTenderResponse fuelTenderAcknowledgement(FuelTenderAcknowledge tenderAcknowledgment, MessageContext messageContext) {
		FuelTenderResponse response = new FuelTenderResponse();

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		// start constructing the interface execution result
		WSExecutionResult execResult = new WSExecutionResult();
		execResult.setExecutionType(WSExecutionType.IMPORT_DATA);
		execResult.setSuccess(false);

		try {
			this.initSessionAndInterface(ExtInterfaceNames.FUEL_TENDER_ACKNOWLEDGMENT_INTERFACE.getValue(), messageContext);

			if (this.getFuelTenderInterface().getEnabled()) {
				Long refMsgId = tenderAcknowledgment.getRefMsgId();
				MessageType mType = MessageType.getByType(tenderAcknowledgment.getMessageType());
				MessageStatus mStatus = MessageStatus.getByIataCode(tenderAcknowledgment.getMessageStatus().name());

				// Acknowledge message
				this.ackService.fuelTenderAcknowledgement(refMsgId, mType, mStatus);

				// set the XML as received data
				String xml = XMLUtils.toString(tenderAcknowledgment, FuelTenderAcknowledge.class, true, true);

				execResult.setExecutionData(xml);
				execResult.setSuccess(Boolean.TRUE);
				stopWatch.stop();

				// save interface history
				this.interfaceHistoryService.saveInterfaceHistory(stopWatch, execResult, this.getFuelTenderInterface());

				// Update interface message history
				this.updateMessageHistory(xml, execResult, tenderAcknowledgment);

				response.setResponse(SUCCESS_MSG);
			} else {
				throw new BusinessException(BusinessErrorCode.INTERFACE_DISABLED, BusinessErrorCode.INTERFACE_DISABLED.getErrMsg());
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			execResult.setExecutionData(e.getLocalizedMessage() + "::" + ExceptionUtils.getStackTrace(e));

			try {
				this.interfaceHistoryService.saveInterfaceHistory(stopWatch, execResult, this.getFuelTenderInterface());
			} catch (Exception e1) {
				LOGGER.error(e1.getMessage(), e1);
			}

			response.setResponse(ERROR_MSG);
			response.setErrorDescription(e.getLocalizedMessage());
		}

		return response;
	}

	private void checkBusinessKey(Tender header) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("code", header.getCode());
		params.put("tenderVersion", header.getTenderVersion());
		params.put(QUERY_PARAM_HOLDER, header.getHolder());

		if (!CollectionUtils.isEmpty(this.tenderService.findEntitiesByAttributes(params))) {
			throw new DuplicateEntityException(Tender.class, "Tender Invitation # - TenderCode - TenderHolderCode - TenderVersion");
		}
	}

	private void saveTender(Tender tender) throws BusinessException {
		// Insert Contact if does not exist
		if (tender.getContact() != null && tender.getContact().getId() == null) {
			this.contactService.insert(tender.getContact());
		}

		this.tenderService.insert(tender);
	}

	/**
	 * @param documents
	 * @param tender
	 * @throws BusinessException
	 */
	private void saveAttachments(Documents documents, Tender tender) throws BusinessException {
		List<Attachment> list = new ArrayList<>();
		if (documents != null) {
			AttachmentType type = this.attachmentTypeService.findExternalLink();
			for (String documentUri : documents.getDocumentURI()) {
				Attachment attachment = new Attachment();
				attachment.setActive(true);
				attachment.setContentType(type.getName());
				attachment.setFileName(documentUri);
				attachment.setLocation(documentUri);
				attachment.setName(documentUri);
				attachment.setType(type);
				attachment.setUrl(documentUri);
				attachment.setTargetAlias(Tender.class.getSimpleName());
				attachment.setTargetRefid(tender.getRefid());
				list.add(attachment);
			}
			this.attachementService.insert(list);
		}
	}

	/**
	 * @param tender
	 * @param receiverInterface
	 * @throws BusinessException
	 */
	@SuppressWarnings("unchecked")
	private void sendAcknowledgement(Tender tender, long msgId, ExtInterfaceNames receiverInterface) throws BusinessException {
		ExternalInterface externalInterface = this.interfaceService.findByName(ExtInterfaceNames.FUEL_TENDER_PROCESSING_RESULT.getValue());
		ExtInterfaceParameters extInterfaceParams = new ExtInterfaceParameters(externalInterface.getParams());

		// Prepare params for datahub soap client
		DataHubSoapClient<Tender, FuelTenderAcknowledge> dataHubSoapClient = new DataHubSoapClient<>(externalInterface, extInterfaceParams,
				Arrays.asList(tender), this.interfaceHistoryFacade, this.interfaceMessageHistoryFacade, this.tenderAckTransformer,
				FuelTenderAcknowledge.class, DataHubRequestType.SINGLE_REQUEST);

		dataHubSoapClient.setRequestService((IDataHubClientRequestService<Tender, FuelTenderAcknowledge>) this.ackService);
		dataHubSoapClient.setReceiverInterface(receiverInterface);
		dataHubSoapClient.setRefMsgId(msgId);
		dataHubSoapClient.start(1);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void sendReadNotification(Tender tender) throws BusinessException {

		ExternalInterface externalInterface = this.interfaceService.findByName(ExtInterfaceNames.FUEL_TENDER_PROCESSING_RESULT.getValue());
		ExtInterfaceParameters extInterfaceParams = new ExtInterfaceParameters(externalInterface.getParams());

		// Prepare params for datahub soap client
		DataHubSoapClient<Tender, FuelTenderAcknowledge> dataHubSoapClient = new DataHubSoapClient<>(externalInterface, extInterfaceParams,
				Arrays.asList(tender), this.interfaceHistoryFacade, this.interfaceMessageHistoryFacade, this.tenderReadAckTransformer,
				FuelTenderAcknowledge.class, DataHubRequestType.SINGLE_REQUEST);

		dataHubSoapClient.setRequestService((IDataHubClientRequestService<Tender, FuelTenderAcknowledge>) this.ackService);
		dataHubSoapClient.setReceiverInterface(ExtInterfaceNames.FUEL_TENDER_INVITATION_INTERFACE);
		dataHubSoapClient.setRefMsgId(tender.getMsgId());
		dataHubSoapClient.start(1);
	}

	/**
	 * Updates the current rounds of a specific <code>TenderLocation</code>, given received new <code>LocationRound</code>
	 *
	 * @param tenderLocation
	 * @param receivedRound
	 * @throws BusinessException
	 */
	private void updateTenderLocationRounds(TenderLocation tenderLocation, LocationRound receivedRound) throws BusinessException {
		// deactivate all rounds
		for (TenderLocationRound round : tenderLocation.getTenderLocationRounds()) {
			// deactivate first
			round.setActive(false);
		}

		// get current existing round
		TenderLocationRound currentExistingRound = this.getExistingRound(tenderLocation, receivedRound);

		// update current active round (if the case)
		if (currentExistingRound != null) {
			// activate
			currentExistingRound.setActive(true);
			currentExistingRound.setBiddingFrom(receivedRound.getBiddingPeriod().getFrom().toGregorianCalendar().getTime());
			currentExistingRound.setBiddingTo(receivedRound.getBiddingPeriod().getTo().toGregorianCalendar().getTime());
			currentExistingRound.setBidOpening(receivedRound.getBidOpening().toGregorianCalendar().getTime());
		} else {
			// create a new one
			TenderLocationRound newRound = new TenderLocationRound();
			newRound.setActive(true);
			newRound.setBiddingFrom(receivedRound.getBiddingPeriod().getFrom().toGregorianCalendar().getTime());
			newRound.setBiddingTo(receivedRound.getBiddingPeriod().getTo().toGregorianCalendar().getTime());
			newRound.setBidOpening(receivedRound.getBidOpening().toGregorianCalendar().getTime());
			newRound.setRoundNo(receivedRound.getBiddingRound().intValue());
			newRound.setTenderLocation(tenderLocation);
			tenderLocation.addToTenderLocationRounds(newRound);
		}

		// update in DB
		tenderLocation.setLocationBiddingStatus(BiddingStatus._NEW_);
		this.tenderLocationService.update(tenderLocation);

	}

	/**
	 * Retrieves the current active round for a tender location (if any)
	 *
	 * @param tenderLocation
	 * @param receivedRound
	 * @return
	 */
	private TenderLocationRound getExistingRound(TenderLocation tenderLocation, LocationRound receivedRound) {
		TenderLocationRound existingRound = null;
		for (TenderLocationRound round : tenderLocation.getTenderLocationRounds()) {
			if (round.getRoundNo().equals(receivedRound.getBiddingRound().intValue())) {
				existingRound = round;
				break;
			}
		}
		return existingRound;
	}

	private List<TenderLocation> findTenderLocationsByLocationHeader(Tender tender, LocationHeader locHead) throws BusinessException {
		Locations location = this.getLocation(locHead.getLocationCodeIATA(), locHead.getLocationCodeICAO());
		TaxType taxType = TaxType.getByIataCode(locHead.getProductTaxType().value());
		Product product = Product.getByIataName(locHead.getFuelProduct().value());
		FlightServiceType flightServiceType = FlightServiceType.getByCode(locHead.getFlightServiceType().value());
		try {
			Map<String, Object> locationParams = new HashMap<>();
			locationParams.put("location", location);
			locationParams.put("tender", tender);
			locationParams.put("taxType", taxType);
			locationParams.put("fuelProduct", product);
			locationParams.put("flightServiceType", flightServiceType);

			return this.tenderLocationService.findEntitiesByAttributes(locationParams);
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw new SingleResultException(
						"Multiple  Location found for " + location.getCode() + " present in file, please check LocationHeader and reprocess", nre);
			}
			throw new SingleResultException(
					String.format(CmmErrorCode.SINGLE_RESULT_EXCEPTION_MESSAGE.getErrMsg(), "Location", location.getCode(), "LocationCode"), nre);
		}
	}

	/**
	 * @param message
	 * @param status
	 * @param parameters
	 * @throws BusinessException
	 */
	private void updateMessageHistory(String message, WSExecutionResult result, FuelTenderAcknowledge parameters) throws BusinessException {
		List<ExternalInterfaceMessageHistory> list = this.interfaceMessageHistoryFacade.findByMessageId(Long.toString(parameters.getRefMsgId()));
		for (ExternalInterfaceMessageHistory messageHistory : list) {
			messageHistory.setStatus(result.isSuccess() ? ExternalInterfaceMessageHistoryStatus._ACKNOWLEDGED_
					: ExternalInterfaceMessageHistoryStatus._FAILED_NEGATIVE_ACKNOWLEDGEMENT_);
			messageHistory.setResponseTime(new Date());
			messageHistory.setResponseMessageId(Long.toString(parameters.getTenderIdentification().getMsgId()));
			this.interfaceMessageHistoryFacade.update(messageHistory);
		}
		this.interfaceMessageHistoryFacade.uploadToS3(message, Long.toString(parameters.getTenderIdentification().getMsgId()), false);
	}
}
