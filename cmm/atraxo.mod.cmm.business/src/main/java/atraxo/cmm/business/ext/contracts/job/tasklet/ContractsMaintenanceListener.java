package atraxo.cmm.business.ext.contracts.job.tasklet;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;

import seava.j4e.scheduler.batch.listener.DefaultActionListener;

/**
 * @author aradu
 */
public class ContractsMaintenanceListener extends DefaultActionListener {

	@Override
	public void afterJob(JobExecution jobExecution) {
		super.afterJob(jobExecution);

		if (jobExecution.getExecutionContext().containsKey("contractInfo")) {
			ExitStatus status = jobExecution.getExitStatus();
			StringBuilder sb = new StringBuilder();

			sb.append(jobExecution.getExecutionContext().get("contractInfo"));
			status = new ExitStatus(status.getExitCode(), sb.toString());
			jobExecution.setExitStatus(status);
		}
	}
}
