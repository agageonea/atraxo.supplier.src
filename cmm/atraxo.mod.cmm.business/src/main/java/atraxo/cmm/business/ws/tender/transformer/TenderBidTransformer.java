package atraxo.cmm.business.ws.tender.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.ad.business.api.attachment.IAttachmentService;
import atraxo.ad.business.api.attachment.IAttachmentTypeService;
import atraxo.ad.domain.impl.ad.TAttachmentType;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.ext.bid.IBidValidator;
import atraxo.cmm.business.api.ext.contracts.IPriceBuilder;
import atraxo.cmm.business.api.ext.datahub.IDataHubInTransformer;
import atraxo.cmm.business.api.ext.datahub.IDataHubOutTransformer;
import atraxo.cmm.business.api.tender.ITenderLocationService;
import atraxo.cmm.business.api.tender.ITenderService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractScope;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.EventType;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.InvoicePayableBy;
import atraxo.cmm.domain.impl.cmm_type.IsSpot;
import atraxo.cmm.domain.impl.cmm_type.Operators;
import atraxo.cmm.domain.impl.cmm_type.PaymentType;
import atraxo.cmm.domain.impl.cmm_type.Period;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.QuantityType;
import atraxo.cmm.domain.impl.cmm_type.RestrictionTypes;
import atraxo.cmm.domain.impl.cmm_type.ReviewPeriod;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.cmm_type.VatApplicability;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.business.api.categories.IIataPCService;
import atraxo.fmbas.business.api.categories.IPriceCategoryService;
import atraxo.fmbas.business.api.exchangerate.IExchangeRateService;
import atraxo.fmbas.business.api.quotation.IQuotationService;
import atraxo.fmbas.business.api.suppliers.ISuppliersService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.SingleResultException;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.categories.IataPC;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.fmbas_type.DataProvider;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceType;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import atraxo.fmbas.domain.impl.fmbas_type.Use;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.notes.Notes;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.utils.DateUtils;
import seava.j4e.commons.utils.SoneEnumUtils;
import seava.j4e.iata.fuelplus.iata.tender.AirlinesVolumes;
import seava.j4e.iata.fuelplus.iata.tender.AirlinesVolumes.AirlinesVolume;
import seava.j4e.iata.fuelplus.iata.tender.Bid;
import seava.j4e.iata.fuelplus.iata.tender.Bid.BidData.ProductsFees;
import seava.j4e.iata.fuelplus.iata.tender.BidHeader;
import seava.j4e.iata.fuelplus.iata.tender.BidTenderHeader;
import seava.j4e.iata.fuelplus.iata.tender.BidTenderHeader.BidderContact;
import seava.j4e.iata.fuelplus.iata.tender.Bids;
import seava.j4e.iata.fuelplus.iata.tender.Contact;
import seava.j4e.iata.fuelplus.iata.tender.Decision;
import seava.j4e.iata.fuelplus.iata.tender.Documents;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBid;
import seava.j4e.iata.fuelplus.iata.tender.Index;
import seava.j4e.iata.fuelplus.iata.tender.IndexConversion;
import seava.j4e.iata.fuelplus.iata.tender.LocationHeader;
import seava.j4e.iata.fuelplus.iata.tender.Market;
import seava.j4e.iata.fuelplus.iata.tender.OpenInvoice;
import seava.j4e.iata.fuelplus.iata.tender.PackageIdentifier;
import seava.j4e.iata.fuelplus.iata.tender.Payment;
import seava.j4e.iata.fuelplus.iata.tender.Prepayment;
import seava.j4e.iata.fuelplus.iata.tender.Price;
import seava.j4e.iata.fuelplus.iata.tender.ProductFee;
import seava.j4e.iata.fuelplus.iata.tender.ProductPricing;
import seava.j4e.iata.fuelplus.iata.tender.ProductServiceDetails;
import seava.j4e.iata.fuelplus.iata.tender.Restriction;
import seava.j4e.iata.fuelplus.iata.tender.Restrictions;
import seava.j4e.iata.fuelplus.iata.tender.Tax;
import seava.j4e.iata.fuelplus.iata.tender.TaxOnTax;
import seava.j4e.iata.fuelplus.iata.tender.TaxOnTaxes;
import seava.j4e.iata.fuelplus.iata.tender.TaxTypeBase;
import seava.j4e.iata.fuelplus.iata.tender.Taxes;
import seava.j4e.iata.fuelplus.iata.tender.TenderIdentification;
import seava.j4e.iata.fuelplus.iata.tender.TotalVolume;
import seava.j4e.iata.fuelplus.iata.tender.VATApplicability;

public class TenderBidTransformer extends DataHubOutTransformer<Contract, FuelTenderBid>
		implements IDataHubOutTransformer<Contract, FuelTenderBid>, IDataHubInTransformer<Contract, FuelTenderBid> {

	/**
	 *
	 */
	private static final String EXCHANGE_RATE_AVERAGEING_METHOD = "ExchangeRate AverageingMethod";
	/**
	 *
	 */
	private static final String EXCHANGE_RATE_FINANCIAL_SOURCE = "ExchangeRate FinancialSource";
	private static final String MAP_N_PLUS_1 = "N+1";
	private static final String MAP_N_PLUS_0 = "N+0";
	private static final String MAP_N_MINUS_1 = "N-1";
	private static final String MAP_N_MINUS_2 = "N-2";

	private static final String TIMESERIES2 = "timeseries";
	private static final String AVG_METHOD_INDICATOR = "avgMethodIndicator";
	private static final String DATA_PROVIDER = "dataProvider";
	private static final String ARITHM_OPER = "arithmOper";
	private static final String CONV_FCTR = "convFctr";
	private static final String UNIT2_ID = "unit2Id";
	private static final String UNIT_ID = "unitId";
	private static final String EXTERNAL_SERIE_NAME = "externalSerieName";
	private static final String TYPE = "type";
	private static final String IS_INDEX_BASED = "isIndexBased";
	private static final String IATA = "iata";
	private static final String PRICE_PER = "pricePer";
	private static final String NOT_APPLICABLLE_VAT = VatApplicability._NOT_APPLICABLE_.getCode();

	private static final Logger LOGGER = LoggerFactory.getLogger(TenderBidTransformer.class);

	@Autowired
	private ITenderService tenderService;
	@Autowired
	private IContractService contractService;
	@Autowired
	private ISuppliersService suppliersService;
	@Autowired
	private IUserSuppService userSuppService;
	@Autowired
	private ITenderLocationService tenderLocationService;
	@Autowired
	private IPriceCategoryService priceCategoryService;
	@Autowired
	private IIataPCService iIataPCService;
	@Autowired
	private IQuotationService quotationService;
	@Autowired
	private IExchangeRateService exRateService;
	@Autowired
	private IPriceBuilder priceBuilder;
	@Autowired
	private IAttachmentService attachmentService;
	@Autowired
	private IAttachmentTypeService attachmentTypeService;
	@Autowired
	private IBidValidator bidValidator;

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubOutTransformer#transformModelToDTO(atraxo.fmbas.domain.impl.abstracts.AbstractEntity, long,
	 * java.lang.String)
	 */
	@Override
	public FuelTenderBid transformModelToDTO(Contract modelEntity, long msgId, String reason) throws BusinessException, JAXBException {
		FuelTenderBid bid = new FuelTenderBid();
		bid.setBidTenderHeader(this.createBidTenderHeader(modelEntity, msgId));
		Bids bids = new Bids();
		bids.getBid().add(this.buildBid(modelEntity));
		bid.setBids(bids);
		bid.setSchemaVersion(modelEntity.getBidTenderIdentification().getSchemaVersion());
		return bid;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubOutTransformer#transformModelToDTO(atraxo.fmbas.domain.impl.abstracts.AbstractEntity, long, long,
	 * atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames)
	 */
	@Override
	public FuelTenderBid transformModelToDTO(Contract modelEntity, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubOutTransformer#transformModelsToDTO(java.util.List, long, java.lang.String)
	 */
	@Override
	public FuelTenderBid transformModelsToDTO(List<Contract> modelEntities, long msgId, String reason) throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubOutTransformer#transformModelsToDTO(java.util.List, long, long,
	 * atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames)
	 */
	@Override
	public FuelTenderBid transformModelsToDTO(List<Contract> modelEntities, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

	@Override
	public Collection<Contract> transformDTOToModels(FuelTenderBid fuelTenderBid) throws BusinessException, JAXBException {
		List<Contract> bids = new ArrayList<>();

		Tender tender = this.extractTender(fuelTenderBid.getBidTenderHeader().getTenderIdentification());

		for (Bid bid : fuelTenderBid.getBids().getBid()) {
			Contract newBid = this.createBidFromTenderAndBid(tender, bid);
			bids.add(newBid);
		}

		return bids;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.ext.datahub.IDataHubInTransformer#transformDTOToModel(java.lang.Object)
	 */
	@Override
	public Contract transformDTOToModel(FuelTenderBid dto) throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

	/**
	 * @param tender
	 * @param contacts
	 * @param iataTenderBid
	 * @return
	 * @throws BusinessException
	 */
	public Contract createBidFromTenderAndBid(Tender tender, Bid iataTenderBid) throws BusinessException {
		String bidderCode = iataTenderBid.getBidIdentification().getBidderCode();

		Contract bid = this.contractService.create();

		// readonly, isContract(bid), isBlueprint
		bid.setReadOnly(false);
		bid.setIsContract(false);
		bid.setIsBlueprint(false);

		// statuses
		bid.setBidStatus(BidStatus._DRAFT_);
		bid.setBidTransmissionStatus(TransmissionStatus._NEW_);
		bid.setBidCancelTransmitionStatus(TransmissionStatus._NEW_);
		bid.setBidAcceptAwardTransmissionStatus(TransmissionStatus._NEW_);
		bid.setBidDeclineAwardTransmissionStatus(TransmissionStatus._NEW_);
		bid.setBidApprovalStatus(BidApprovalStatus._NEW_);
		bid.setPeriodApprovalStatus(BidApprovalStatus._NEW_);
		bid.setBidApprovalStatus(BidApprovalStatus._NEW_);
		bid.setPriceApprovalStatus(BidApprovalStatus._NEW_);
		bid.setPeriodApprovalStatus(BidApprovalStatus._NEW_);
		bid.setShipToApprovalStatus(BidApprovalStatus._NEW_);

		bid.setStatus(ContractStatus._DRAFT_);
		bid.setBidTenderIdentification(tender);

		// contact, tender
		bid.setContact(tender.getContact());

		// Set responsible buyer (logged user)
		UserSupp respBuyer = this.userSuppService.findByLogin(Session.user.get().getLoginName());
		bid.setResponsibleBuyer(respBuyer);

		// holder
		Customer bidder = this.findCustomer(iataTenderBid.getBidIdentification().getBidderCode(), "BidderCode");
		this.checkBidder(bidder);
		bid.setHolder(bidder);

		// customer
		bid.setCustomer(tender.getHolder());

		// bid revision
		bid.setBidRevision(iataTenderBid.getBidIdentification().getBidRevision().intValue());

		// bid version
		bid.setBidVersion(iataTenderBid.getBidIdentification().getBidVersion());

		LocationHeader iataLocationHeader = iataTenderBid.getBidIdentification().getLocationHeader();

		// product type
		Product product = this.extractProduct(iataLocationHeader.getFuelProduct().value(), bidderCode);
		bid.setProduct(product);

		// tax type
		TaxType taxType = this.extractTaxType(iataLocationHeader.getProductTaxType().name(), bidderCode);
		bid.setTax(taxType);

		// flight service type
		FlightServiceType flightServiceType = this.extractFlightServiceType(iataLocationHeader.getFlightServiceType().name(), bidderCode);
		bid.setFlightServiceType(flightServiceType);

		// locations
		Locations locations = this.findLocation(iataLocationHeader.getLocationCodeIATA(), iataLocationHeader.getLocationCodeICAO());
		bid.setLocation(locations);

		// tender location
		ProductServiceDetails iataProductServiceDetails = iataTenderBid.getBidData().getProductServiceDetails();
		ContractSubType bidSubType = this.extractSubType(iataProductServiceDetails.getDeliveryPoint().name(), bidderCode);

		TenderLocation tenderLocation = this.extractTenderLocation(locations, tender, taxType, product, flightServiceType, bidderCode, bidSubType);
		bid.setBidTenderLocation(tenderLocation);

		BidHeader iataBidHeader = iataTenderBid.getBidData().getBidHeader();

		// agreement period
		bid.setValidFrom(DateUtils.fromXMLGregorianCalendar(iataBidHeader.getAgreementPeriod().getFrom()));
		bid.setValidTo(DateUtils.fromXMLGregorianCalendar(iataBidHeader.getAgreementPeriod().getTo()));

		// bid validity period
		if (iataBidHeader.getBidValidityPeriod() != null) {
			bid.setBidValidFrom(DateUtils.fromXMLGregorianCalendar(iataBidHeader.getBidValidityPeriod().getFrom()));
			bid.setBidValidTo(DateUtils.fromXMLGregorianCalendar(iataBidHeader.getBidValidityPeriod().getTo()));
		} else {
			Date currentDate = new Date();
			bid.setBidValidFrom(currentDate);
			bid.setBidValidTo(currentDate);
		}

		// quantity type
		if (iataBidHeader.getBidVolumeMeasurement() != null) {
			bid.setQuantityType(this.extractQuantityType(iataBidHeader.getBidVolumeMeasurement().name(), bidderCode));
		} else {
			bid.setQuantityType(QuantityType._GROSS_VOLUME_);
		}

		// volume tolerance
		if (iataBidHeader.getVolumeTolerance() != null) {
			bid.setVolumeTolerance(iataBidHeader.getVolumeTolerance().intValue());
		}

		// tender volumes
		TotalVolume totalVolume = iataTenderBid.getBidData().getTotalVolume();
		bid.setBidHasTotalVolume(totalVolume != null);
		if (totalVolume != null) {
			bid.setOfferedVolume(totalVolume.getVolume());
			bid.setSettlementUnit(this.findUnit(totalVolume.getUOM(), "TotalVolume UOM"));
			bid.setPeriod(this.extractPeriodType(totalVolume.getPeriodType().name(), bidderCode));
		}

		// airlines volumes
		AirlinesVolumes iataAirlinesVolumes = iataTenderBid.getBidData().getAirlinesVolumes();
		if (iataAirlinesVolumes != null) {
			Double density = this.getSystemDensity();
			Unit settlementUnit = this.unitService.findByCode(this.getSysVolUnit());

			bid.setPeriod(this.extractPeriodType(iataAirlinesVolumes.getPeriodType().name(), bidderCode));

			// set fuel receivers
			BigDecimal totalOfferedVolume = BigDecimal.ZERO;
			Unit airlineUnit = this.findUnit(iataAirlinesVolumes.getUOM(), "AirlinesVolume UOM");
			for (AirlinesVolume airlinesVolume : iataAirlinesVolumes.getAirlinesVolume()) {
				ShipTo bidAirlineVolume = new ShipTo();

				BigDecimal convertedVolume = this.unitService.convert(airlineUnit, settlementUnit, airlinesVolume.getVolume(), density);
				totalOfferedVolume = totalOfferedVolume.add(convertedVolume);

				bidAirlineVolume.setTenderBidVolume(convertedVolume);
				bidAirlineVolume.setOfferedVolume(convertedVolume);

				bidAirlineVolume.setCustomer(this.extractAirlineCustomer(airlinesVolume.getAirlineCode(), bidderCode));

				bidAirlineVolume.setValidFrom(bid.getValidFrom());
				bidAirlineVolume.setValidTo(bid.getValidTo());
				bidAirlineVolume.setContract(bid);

				bid.addToShipTo(bidAirlineVolume);
			}
			bid.setOfferedVolume(totalOfferedVolume);
		}

		// product services

		bid.setBidAstmSpecification(iataProductServiceDetails.getFuelSpecification());

		bid.setSubType(bidSubType);

		if (iataProductServiceDetails.getIATAServiceLevel() != null) {
			bid.setIataServiceLevel(this.getIataServiceLevel(iataProductServiceDetails.getIATAServiceLevel()));
		}

		if (iataProductServiceDetails.getRefuelerCompanyCode() != null) {
			bid.setIntoPlaneAgent(this.extractSuppliers(iataProductServiceDetails.getRefuelerCompanyCode(), bidderCode));
		}

		if (iataProductServiceDetails.getOperatingHours() != null) {
			bid.setBidOperatingHours(iataProductServiceDetails.getOperatingHours());
		}

		if (iataProductServiceDetails.getTitleTransfer() != null) {
			bid.setBidTitleTransfer(iataProductServiceDetails.getTitleTransfer());
		}

		// payment terms
		Payment iataPayment = iataTenderBid.getBidData().getPayment();
		if (iataPayment.getPrepayment() != null) {
			Prepayment iataPrepayment = iataPayment.getPrepayment();
			bid.setCreditTerms(CreditTerm._PREPAYMENT_);
			bid.setBidPrepaidDays(iataPrepayment.getNumberOfDaysPrepaid().intValue());
			bid.setBidPrepayFirstDeliveryDate(iataPrepayment.getPaymentFirstDeliveryDate().intValue());
			bid.setBidPayementFreq(this.extractPayementFrequency(iataPrepayment.getPaymentFrequency().value(), bidderCode));
			bid.setBidPrepaidAmount(iataPrepayment.getAmount());
			// default values
			bid.setPaymentTerms(Integer.valueOf(0));
			bid.setPaymentRefDay(PaymentDay._INVOICE_DATE_);
			bid.setInvoiceFreq(InvoiceFreq._WEEKLY_);
			bid.setInvoiceType(InvoiceType._PAPER_);
		} else if (iataPayment.getOpenInvoice() != null) {
			bid.setCreditTerms(CreditTerm._OPEN_CREDIT_);
			OpenInvoice iataOpenInvoice = iataPayment.getOpenInvoice();
			bid.setPaymentTerms(iataOpenInvoice.getPaymentTerms().intValue());
			bid.setPaymentRefDay(this.extractPaymentDay(iataOpenInvoice.getPaymentReferenceDateType().name(), bidderCode));
			bid.setInvoiceFreq(this.extractInvoiceFreq(iataOpenInvoice.getInvoiceFrequency().value(), bidderCode));
			bid.setInvoiceType(this.extractInvoiceType(iataOpenInvoice.getInvoiceType().value(), bidderCode));
		}
		bid.setSettlementCurr(this.findCurrency(iataPayment.getPaymentCurrency().name(), "PaymentCurrency"));
		bid.setSettlementUnit(this.findUnit(iataPayment.getPricingUnit(), "PricingUnit"));

		bid.setBidBankGuarantee(Decision.Y.equals(iataPayment.getGuaranteesDepositsRequired()));

		// method of payment
		if (iataPayment.getMethodOfPayment() != null) {
			bid.setBidPaymentType(this.extractPaymentType(iataPayment.getMethodOfPayment().name(), bidderCode));
		} else {
			bid.setBidPaymentType(PaymentType._CONTRACT_);
		}

		// exchange rates (financial source, avg method, offset)
		seava.j4e.iata.fuelplus.iata.tender.ExchangeRate iataExchangeRate = iataPayment.getExchangeRate();
		bid.setFinancialSource(this.findFinancialSource(iataExchangeRate.getFinancialSource().name(), EXCHANGE_RATE_FINANCIAL_SOURCE));
		bid.setAverageMethod(this.findAverageMethod(iataExchangeRate.getAverageingMethod().name(), EXCHANGE_RATE_AVERAGEING_METHOD));
		bid.setExchangeRateOffset(this.extractExchangeRateOffset(iataExchangeRate.getAveragingOffset(), bidderCode));

		bid.setEventType(EventType._EMPTY_);
		bid.setInvoicePayableBy(InvoicePayableBy._EMPTY_);
		bid.setReviewPeriod(ReviewPeriod._EMPTY_);
		bid.setLimitedTo(FlightTypeIndicator._UNSPECIFIED_);
		bid.setDealType(DealType._SELL_);
		bid.setIsSpot(IsSpot._TERM_);
		bid.setScope(ContractScope._INTO_PLANE_);
		bid.setType(ContractType.getByName(tender.getType().getName()));
		bid.setVat(this.getVATApplicability(iataTenderBid.getBidData().getVATApplicability()));

		// package identifier
		PackageIdentifier iataPackageIdentifier = iataTenderBid.getBidIdentification().getPackageIdentifier();
		if (iataPackageIdentifier != null) {
			bid.setBidPackageIdentifier(iataPackageIdentifier.getPackageCode());
		}

		// fill in empty values
		SoneEnumUtils.setEmptyValues(bid);

		// prices
		ProductPricing iataProductPricing = iataTenderBid.getBidData().getProductPricing();
		Market iataMarket = iataProductPricing.getMarket();
		Index iataIndex = iataProductPricing.getIndex();

		List<ContractPriceCategory> contractPriceCategories = new ArrayList<>();

		ContractPriceCategory parentPriceCategory = null;
		if (iataMarket != null) {
			// create a fixed price
			parentPriceCategory = this.createFixedPrice(bid, iataMarket);

		} else if (iataIndex != null) {
			// create an index price

			IndexConversion iataIndexConversion = iataIndex.getIndexConversion();
			if (iataIndexConversion != null) {
				Quotation quotation = this.extractQuotation(iataIndexConversion, iataIndex, bidderCode);
				parentPriceCategory = this.createIndexPrice(bid, quotation, iataIndex, bidderCode);
			} else {
				throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_INDEX_PRICE_INVALID_XML,
						String.format(CmmErrorCode.IMPORT_BID_XML_INDEX_PRICE_INVALID_XML.getErrMsg(), bidderCode));
			}
		}

		// SONAR check ( it shouldn't be possible to be in this situation )
		if (parentPriceCategory == null) {
			throw new BusinessException(CmmErrorCode.IMPORT_BID_NO_PRICE_ERROR, CmmErrorCode.IMPORT_BID_NO_PRICE_ERROR.getErrMsg());
		}

		contractPriceCategories.add(parentPriceCategory);

		ProductsFees iataProductsFees = iataTenderBid.getBidData().getProductsFees();
		for (ProductFee iataProductFee : iataProductsFees.getProductFee()) {

			IataPC iataPC = this.extractIataPC(iataProductFee.getItemProductID().value(), bidderCode);

			if (iataPC.getUse().equals(Use._PRODUCT_)) {
				// try to set the name if possible
				if (!StringUtils.isEmpty(iataProductFee.getItemProductAlias())) {
					parentPriceCategory.setName(iataProductFee.getItemProductAlias());
				}

				// add taxes for product
				this.createTaxes(iataProductFee, bid, contractPriceCategories, parentPriceCategory, bidderCode);

			} else if (iataPC.getUse().equals(Use._FEE_)) {
				// add fee
				ContractPriceCategory cpc = this.createFeePrice(bid, iataProductFee, iataPC, bidderCode);
				contractPriceCategories.add(cpc);

				// add taxes for fee
				this.createTaxes(iataProductFee, bid, contractPriceCategories, cpc, bidderCode);

			} else if (iataPC.getUse().equals(Use._TAX_)) {
				throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_INCORRECT_USE_TYPE_TAX,
						String.format(CmmErrorCode.IMPORT_BID_XML_INCORRECT_USE_TYPE_TAX.getErrMsg(), bidderCode));
			}

		}

		// make connections in DB through children for prices
		for (ContractPriceCategory categ : contractPriceCategories) {
			categ.setContract(bid);
			bid.addToPricingBases(categ.getPricingBases());
		}
		bid.setPriceCategories(contractPriceCategories);
		bid.setCustomer(tender.getHolder());

		// Check bid existance
		this.bidValidator.checkBidExistence(bid);

		// insert the bid/contract
		this.contractService.insert(bid);

		// notes
		if (!StringUtils.isEmpty(iataBidHeader.getComments())) {
			Notes note = new Notes();
			note.setNotes(iataBidHeader.getComments());
			note.setObjectId(bid.getId());
			note.setObjectType(Contract.class.getSimpleName());
			this.noteService.insert(note);
		}

		// documents
		Documents iataDocuments = iataTenderBid.getBidIdentification().getDocuments();
		if (iataDocuments != null && !iataDocuments.getDocumentURI().isEmpty()) {
			AttachmentType linkAttachmentType = this.getAttachmentType(TAttachmentType._LINK_);

			List<Attachment> attachments = new ArrayList<>();
			for (String iataDocURI : iataDocuments.getDocumentURI()) {
				Attachment newAttachment = new Attachment();
				newAttachment.setName(iataDocURI);
				newAttachment.setTargetRefid(bid.getId() + "");
				newAttachment.setTargetAlias(Contract.class.getSimpleName());
				newAttachment.setType(linkAttachmentType);
				newAttachment.setFileName("");
				attachments.add(newAttachment);
			}
			this.attachmentService.insert(attachments);
		}

		return bid;
	}

	/**
	 * @param type
	 * @return
	 * @throws BusinessException
	 */
	public AttachmentType getAttachmentType(TAttachmentType type) throws BusinessException {
		AttachmentType attachmentType;

		Map<String, Object> params = new HashMap<>();
		params.put("category", type);
		List<AttachmentType> types = this.attachmentTypeService.findEntitiesByAttributes(params);

		if (types.isEmpty()) {
			throw new BusinessException(CmmErrorCode.LINK_ATTACHMENT_TYPE_NOT_FOUND,
					String.format(CmmErrorCode.LINK_ATTACHMENT_TYPE_NOT_FOUND.getErrMsg(), type.name()));
		}
		if (types.size() > 1) {
			LOGGER.warn("Found multiple attachment types for docType %s ! Will use the first one with ID %s.", type, types.get(0).getId());
		}
		attachmentType = types.get(0);

		return attachmentType;
	}

	/**
	 * @param iataProductFee
	 * @param bid
	 * @param contractPriceCategories
	 * @param parentPriceCategory
	 * @param bidderCode
	 * @throws BusinessException
	 */
	private void createTaxes(ProductFee iataProductFee, Contract bid, List<ContractPriceCategory> contractPriceCategories,
			ContractPriceCategory parentPriceCategory, String bidderCode) throws BusinessException {
		// add taxes
		Taxes iataTaxes = iataProductFee.getTaxes();
		if (iataTaxes != null) {
			for (Tax iataTax : iataTaxes.getTax()) {

				ContractPriceCategory cpcTax = this.createTaxPrice(bid, contractPriceCategories, iataTax.getExchangeRate(), iataTax.getPrice(),
						iataTax.getTaxType(), iataTax.getTaxAlias(), parentPriceCategory, bidderCode, false, iataTax.getSortID().toString(),
						iataTax.getVATApplicability());

				Restrictions iataTaxRestrictions = iataTax.getRestrictions();
				if (iataTaxRestrictions != null) {
					this.updateRestrictions(iataTaxRestrictions, cpcTax, bidderCode);
				}

				// tax on tax
				TaxOnTaxes iataTaxOnTaxes = iataTax.getTaxOnTaxes();
				if (iataTaxOnTaxes != null) {
					for (TaxOnTax iataTaxOnTax : iataTaxOnTaxes.getTaxOnTax()) {

						ContractPriceCategory cpcTaxOnTax = this.createTaxPrice(bid, contractPriceCategories, iataTaxOnTax.getExchangeRate(),
								iataTaxOnTax.getPrice(), iataTaxOnTax.getTaxType(), iataTaxOnTax.getTaxAlias(), cpcTax, bidderCode, true,
								iataTaxOnTax.getSortID().toString(), iataTaxOnTax.getVATApplicability());

						Restrictions iataTaxOnTaxRestrictions = iataTaxOnTax.getRestrictions();
						if (iataTaxOnTaxRestrictions != null) {
							this.updateRestrictions(iataTaxOnTaxRestrictions, cpcTaxOnTax, bidderCode);
						}
					}
				}
			}
		}
	}

	/**
	 * @param bid
	 * @param contractPriceCategories
	 * @param iataExchangeRate
	 * @param iataPrice
	 * @param iataTaxType
	 * @param taxAlias
	 * @param parentPriceCategory
	 * @param bidderCode
	 * @param checkPercentage true if info taken from TaxOnTax, false if taken from Tax
	 * @return
	 * @throws BusinessException
	 */
	private ContractPriceCategory createTaxPrice(Contract bid, List<ContractPriceCategory> contractPriceCategories,
			seava.j4e.iata.fuelplus.iata.tender.ExchangeRate iataExchangeRate, Price iataPrice, TaxTypeBase iataTaxType, String taxAlias,
			ContractPriceCategory parentPriceCategory, String bidderCode, boolean checkPercentage, String sortId, VATApplicability vatApplicability)
			throws BusinessException {

		PriceInd priceInd = this.getPriceInd(iataPrice.getPricingUnitRateType());
		IataPC iataPC = this.extractIataPC(iataTaxType.value(), bidderCode);
		PriceCategory priceCategory = this.getPriceCategory(priceInd, iataPC);

		// if needs percentage then make sure to check it
		if (checkPercentage && !priceCategory.getPricePer().equals(PriceInd._PERCENT_)) {
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_TAX_PRICE_NOT_PERCENTAGE,
					String.format(CmmErrorCode.IMPORT_BID_XML_TAX_PRICE_NOT_PERCENTAGE.getErrMsg(), sortId));
		}

		ContractPriceCategory contractPriceCategory = this.findContractPriceCategory(contractPriceCategories, iataPC, taxAlias);
		if (contractPriceCategory == null) {

			PricingBase pricingBase = new PricingBase();
			pricingBase.setVat(this.getVATApplicability(vatApplicability));
			pricingBase.setValidTo(bid.getValidTo());
			pricingBase.setValidFrom(bid.getValidFrom());
			pricingBase.setContract(bid);

			FinancialSources finSources = null;
			AverageMethod avgMethod;
			try {
				finSources = this.findFinancialSource(iataExchangeRate.getFinancialSource().name(), EXCHANGE_RATE_FINANCIAL_SOURCE);
			} catch (NullPointerException e) {
				throw new BusinessException(CmmErrorCode.FINCANCIAL_SOURCE_IS_MISSING, CmmErrorCode.FINCANCIAL_SOURCE_IS_MISSING.getErrMsg(), e);
			}
			try {
				avgMethod = this.findAverageMethod(iataExchangeRate.getAverageingMethod().name(), EXCHANGE_RATE_AVERAGEING_METHOD);
			} catch (NullPointerException e) {
				throw new BusinessException(CmmErrorCode.AVERAGE_METHOD_IS_MISSING, CmmErrorCode.AVERAGE_METHOD_IS_MISSING.getErrMsg(), e);
			}

			this.checkExchangeRate(finSources, avgMethod);
			pricingBase.setFinancialSourceId(finSources.getId());
			pricingBase.setAverageMethodId(avgMethod.getId());
			pricingBase.setExchangeRateOffset(this.extractExchangeRateOffset(iataExchangeRate.getAveragingOffset(), bidderCode));

			pricingBase.setOperator(Operator._EMPTY_);
			pricingBase.setQuotationOffset(bid.getExchangeRateOffset());

			pricingBase.setQuantityType(bid.getQuantityType());

			pricingBase.setDefauftPC(true);
			pricingBase.setPriceCat(priceCategory);
			pricingBase.setDescription(StringUtils.isEmpty(taxAlias) ? priceCategory.getName() : taxAlias);

			pricingBase.setInitialPrice(iataPrice.getPricingUnitRate());
			pricingBase.setInitialCurrId(this.findCurrency(iataPrice.getPricingCurrencyCode().name(), "Price PricingCurrency").getId());
			pricingBase.setInitialUnitId(this.findUnit(iataPrice.getPricingUOM(), "Price PricingUOM").getId());

			this.priceBuilder.buildPricingBases(pricingBase);

			contractPriceCategory = pricingBase.getContractPriceCategories().iterator().next();
			contractPriceCategories.add(contractPriceCategory);

		}

		// if percentage add the new price to the parent one
		if (priceCategory.getPricePer().equals(PriceInd._PERCENT_)) {
			if (contractPriceCategory.getParentPriceCategory() == null) {
				contractPriceCategory.setParentPriceCategory(new ArrayList<>());
				contractPriceCategory.getParentPriceCategory().add(parentPriceCategory);
			}

			List<ContractPriceCategory> newContractPriceCategories = new ArrayList<>();
			for (ContractPriceCategory category : contractPriceCategory.getParentPriceCategory()) {
				if (!category.getPriceCategory().getIata().getCode().equals(parentPriceCategory.getPriceCategory().getIata().getCode())
						&& !category.getName().equals(parentPriceCategory.getName())) {
					newContractPriceCategories.add(parentPriceCategory);
				}
			}
			contractPriceCategory.getParentPriceCategory().addAll(newContractPriceCategories);
		}

		return contractPriceCategory;
	}

	/**
	 * @param finSources
	 * @param avgMethod
	 * @throws BusinessException
	 */
	private void checkExchangeRate(FinancialSources finSources, AverageMethod avgMethod) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("finsource", finSources);
		params.put("avgMethodIndicator", avgMethod);
		List<ExchangeRate> exRateList = this.exRateService.findEntitiesByAttributes(params);
		if (exRateList.isEmpty()) {
			throw new BusinessException(BusinessErrorCode.NO_EXCHANGE_RATE_FOUND,
					String.format(BusinessErrorCode.NO_EXCHANGE_RATE_FOUND.getErrMsg(), finSources.getCode(), avgMethod.getCode()));
		}
	}

	/**
	 * Searches for a scpecific <code>ContractPriceCategory</code>, given a <code>List</code> of <code>ContractPriceCategory</code> and a
	 * <code>IataPC</code> and alias (name)
	 *
	 * @param contractPriceCategories
	 * @param iataPC
	 * @param taxAlias
	 * @return
	 */
	private ContractPriceCategory findContractPriceCategory(List<ContractPriceCategory> contractPriceCategories, IataPC iataPC, String taxAlias) {
		ContractPriceCategory retContractPriceCategory = null;

		boolean alsoCheckTaxAlias = !StringUtils.isEmpty(taxAlias);
		for (ContractPriceCategory cpc : contractPriceCategories) {
			if (cpc.getPriceCategory().getIata().equals(iataPC)) {
				if (alsoCheckTaxAlias && cpc.getName().equals(taxAlias)) {
					retContractPriceCategory = cpc;
				}
			}
			if (retContractPriceCategory != null) {
				break;
			}
		}

		return retContractPriceCategory;
	}

	/**
	 * @param bid
	 * @param iataMarket
	 * @return
	 * @throws BusinessException
	 */
	private ContractPriceCategory createFixedPrice(Contract bid, Market iataMarket) throws BusinessException {
		PricingBase pricingBase = new PricingBase();
		pricingBase.setVat(this.getVATApplicability(iataMarket.getVATApplicability()));
		pricingBase.setValidTo(bid.getValidTo());
		pricingBase.setValidFrom(bid.getValidFrom());
		pricingBase.setContract(bid);

		this.checkExchangeRate(bid.getFinancialSource(), bid.getAverageMethod());
		pricingBase.setFinancialSourceId(bid.getFinancialSource().getId());
		pricingBase.setAverageMethodId(bid.getAverageMethod().getId());
		pricingBase.setQuotationOffset(bid.getExchangeRateOffset());
		pricingBase.setOperator(Operator._EMPTY_);

		pricingBase.setDefauftPC(true);
		pricingBase.setQuantityType(bid.getQuantityType());
		pricingBase.setPriceCat(this.getFixedPriceCategory());
		pricingBase.setDescription(iataMarket.getSourceName() == null ? pricingBase.getPriceCat().getName() : iataMarket.getSourceName());

		pricingBase.setInitialPrice(iataMarket.getRate());
		pricingBase.setInitialCurrId(this.findCurrency(iataMarket.getCurrency().name(), "Market Currency").getId());
		pricingBase.setInitialUnitId(this.findUnit(iataMarket.getUOM(), "Market UOM").getId());

		this.priceBuilder.buildPricingBases(pricingBase);

		return pricingBase.getContractPriceCategories().iterator().next();
	}

	/**
	 * @param bid
	 * @param iataProductFee
	 * @param iataPC
	 * @param bidderCode
	 * @throws BusinessException
	 */
	private ContractPriceCategory createFeePrice(Contract bid, ProductFee iataProductFee, IataPC iataPC, String bidderCode) throws BusinessException {
		PricingBase pricingBase = new PricingBase();
		pricingBase.setVat(this.getVATApplicability(iataProductFee.getVATApplicability()));
		pricingBase.setValidTo(bid.getValidTo());
		pricingBase.setValidFrom(bid.getValidFrom());
		pricingBase.setContract(bid);

		FinancialSources finSources = null;
		AverageMethod avgMethod;
		try {
			finSources = this.findFinancialSource(iataProductFee.getExchangeRate().getFinancialSource().name(), EXCHANGE_RATE_FINANCIAL_SOURCE);
		} catch (NullPointerException e) {
			throw new BusinessException(CmmErrorCode.FINCANCIAL_SOURCE_IS_MISSING, CmmErrorCode.FINCANCIAL_SOURCE_IS_MISSING.getErrMsg(), e);
		}

		try {
			avgMethod = this.findAverageMethod(iataProductFee.getExchangeRate().getAverageingMethod().name(), EXCHANGE_RATE_AVERAGEING_METHOD);
		} catch (NullPointerException e) {
			throw new BusinessException(CmmErrorCode.AVERAGE_METHOD_IS_MISSING, CmmErrorCode.AVERAGE_METHOD_IS_MISSING.getErrMsg(), e);
		}

		this.checkExchangeRate(finSources, avgMethod);
		pricingBase.setFinancialSourceId(finSources.getId());
		pricingBase.setAverageMethodId(avgMethod.getId());
		pricingBase.setExchangeRateOffset(this.extractExchangeRateOffset(iataProductFee.getExchangeRate().getAveragingOffset(), bidderCode));

		pricingBase.setOperator(Operator._EMPTY_);
		pricingBase.setQuotationOffset(bid.getExchangeRateOffset());

		pricingBase.setQuantityType(bid.getQuantityType());

		PriceInd priceInd = this.getPriceInd(iataProductFee.getPrice().getPricingUnitRateType());
		PriceCategory priceCategory = this.getPriceCategory(priceInd, iataPC);

		pricingBase.setDefauftPC(true);
		pricingBase.setPriceCat(priceCategory);
		pricingBase.setDescription(
				StringUtils.isEmpty(iataProductFee.getItemProductAlias()) ? priceCategory.getName() : iataProductFee.getItemProductAlias());

		Price iataPrice = iataProductFee.getPrice();
		pricingBase.setInitialPrice(iataPrice.getPricingUnitRate());
		pricingBase.setInitialCurrId(this.findCurrency(iataPrice.getPricingCurrencyCode().name(), "Price PricingCurrency").getId());
		pricingBase.setInitialUnitId(this.findUnit(iataPrice.getPricingUOM(), "Price PricingUOM").getId());

		this.priceBuilder.buildPricingBases(pricingBase);

		// restrictions
		Restrictions iataRestrictions = iataProductFee.getRestrictions();
		if (iataRestrictions != null) {
			ContractPriceCategory contractPriceCategory = pricingBase.getContractPriceCategories().iterator().next();
			this.updateRestrictions(iataRestrictions, contractPriceCategory, bidderCode);
		}

		return pricingBase.getContractPriceCategories().iterator().next();
	}

	/**
	 * @param bid
	 * @param quotation
	 * @param iataIndex
	 * @param bidderCode
	 * @throws BusinessException
	 */
	private ContractPriceCategory createIndexPrice(Contract bid, Quotation quotation, Index iataIndex, String bidderCode) throws BusinessException {
		PricingBase pricingBase = new PricingBase();
		pricingBase.setVat(this.getVATApplicability(iataIndex.getVATApplicability()));
		pricingBase.setValidTo(bid.getValidTo());
		pricingBase.setValidFrom(bid.getValidFrom());
		pricingBase.setContract(bid);
		pricingBase.setFinancialSourceId(bid.getFinancialSource().getId());
		pricingBase.setQuotationOffset(this.extractExchangeRateOffset(iataIndex.getAveragingOffset(), bidderCode));
		pricingBase.setContinous(true);

		pricingBase.setAvgMethod(quotation.getAvgMethodIndicator());
		pricingBase.setQuotation(quotation);
		pricingBase.setOperator(quotation.getTimeseries().getConvFctr());
		pricingBase.setFactor(quotation.getTimeseries().getArithmOper());
		pricingBase.setDecimals(quotation.getTimeseries().getDecimals());
		pricingBase.setConvUnit(quotation.getTimeseries().getUnit2Id());

		pricingBase.setDefauftPC(true);
		pricingBase.setQuantityType(bid.getQuantityType());
		pricingBase.setPriceCat(this.getIndexPriceCategory());
		pricingBase.setDescription(pricingBase.getPriceCat().getName());

		this.priceBuilder.buildPricingBases(pricingBase);

		return pricingBase.getContractPriceCategories().iterator().next();
	}

	/**
	 * @param iataRestrictions
	 * @param contractPriceCategory
	 * @param pricingBase
	 * @param bidderCode
	 * @throws BusinessException
	 */
	private void updateRestrictions(Restrictions iataRestrictions, ContractPriceCategory contractPriceCategory, String bidderCode)
			throws BusinessException {
		List<ContractPriceRestriction> priceRestrictionsProduct = new ArrayList<>();
		for (Restriction iataRestriction : iataRestrictions.getRestriction()) {
			ContractPriceRestriction restriction = new ContractPriceRestriction();
			restriction.setOperator(this.extractOperators(iataRestriction.getOperator().name(), bidderCode));
			restriction.setRestrictionType(this.extractRestrictionType(iataRestriction.getScope().name(), bidderCode));
			if (iataRestriction.getUOM() != null) {// this is needed since restriction UOM is not mandatory
				restriction.setUnit(this.findUnit(iataRestriction.getUOM(), "Restriction UOM"));
			}
			restriction.setValue(iataRestriction.getValue1());
			restriction.setContractPriceCategory(contractPriceCategory);
			priceRestrictionsProduct.add(restriction);
		}
		contractPriceCategory.setPriceRestrictions(priceRestrictionsProduct);
		contractPriceCategory.setRestriction(!priceRestrictionsProduct.isEmpty());

	}

	/**
	 * @param priceInd
	 * @param iataPC
	 * @return
	 * @throws BusinessException
	 */
	private PriceCategory getPriceCategory(PriceInd priceInd, IataPC iataPC) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put(PRICE_PER, priceInd);
		params.put(IATA, iataPC);
		List<PriceCategory> priceCategories = this.priceCategoryService.findEntitiesByAttributes(params);
		if (priceCategories.isEmpty()) {
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_NO_PRICE_CATEGORY,
					String.format(CmmErrorCode.IMPORT_BID_XML_NO_PRICE_CATEGORY.getErrMsg(), iataPC.getCode(), priceInd.name()));
		}
		return priceCategories.get(0);
	}

	/**
	 * Gets the unique index price category
	 *
	 * @return
	 * @throws BusinessException
	 */
	private PriceCategory getIndexPriceCategory() throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put(IS_INDEX_BASED, Boolean.TRUE);
		List<PriceCategory> indexPriceCategories = this.priceCategoryService.findEntitiesByAttributes(params);
		if (indexPriceCategories.isEmpty()) {
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_NO_INDEX_PRICE_CATEGORY,
					CmmErrorCode.IMPORT_BID_XML_NO_INDEX_PRICE_CATEGORY.getErrMsg());
		} else {
			return indexPriceCategories.get(0);
		}

	}

	/**
	 * Gets the unique fixed price category
	 *
	 * @return
	 * @throws BusinessException
	 */
	private PriceCategory getFixedPriceCategory() throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put(IS_INDEX_BASED, Boolean.FALSE);
		params.put(TYPE, PriceType._PRODUCT_);
		params.put(PRICE_PER, PriceInd._VOLUME_);
		List<PriceCategory> fixedPriceCategories = this.priceCategoryService.findEntitiesByAttributes(params);
		if (fixedPriceCategories.isEmpty()) {
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_NO_FIXED_PRICE_CATEGORY,
					CmmErrorCode.IMPORT_BID_XML_NO_FIXED_PRICE_CATEGORY.getErrMsg());
		} else {
			return fixedPriceCategories.get(0);
		}

	}

	/**
	 * @param iataIndexConversion
	 * @param iataIndex
	 * @param bidderCode
	 * @return
	 * @throws SingleResultException
	 */
	private Quotation extractQuotation(IndexConversion iataIndexConversion, Index iataIndex, String bidderCode) throws BusinessException {
		Quotation activeQuotation;

		Unit fromUnit = this.findUnit(iataIndexConversion.getFromUnit(), "IndexConversion FromUnit");
		Unit toUnit = this.findUnit(iataIndexConversion.getToUnit(), "IndexConversion ToUnit");
		Operator indexOperator = this.extractOperator(iataIndexConversion.getConvOperator().name(), bidderCode);
		DataProvider dataProvider = this.extractDataProvider(iataIndex.getIndexProvider().name(), bidderCode);
		AverageMethod avgMethod = this.findAverageMethod(iataIndex.getAverageingMethod().name(), "Index AverageingMethod");

		// extract first the time series
		Map<String, Object> paramsTS = new HashMap<>();
		paramsTS.put(EXTERNAL_SERIE_NAME, iataIndex.getIndexProviderCode());
		paramsTS.put(UNIT_ID, fromUnit);
		paramsTS.put(UNIT2_ID, toUnit);
		paramsTS.put(CONV_FCTR, indexOperator);
		paramsTS.put(ARITHM_OPER, iataIndexConversion.getFactor());
		paramsTS.put(DATA_PROVIDER, dataProvider);

		List<TimeSerie> timeSeries = this.timeSerieService.findEntitiesByAttributes(paramsTS);
		if (timeSeries.isEmpty()) {
			LOGGER.warn(CmmErrorCode.IMPORT_BID_XML_EXTRACT_QUOTATION_1.getErrMsg(), iataIndex.getIndexProviderCode(), dataProvider.getName(),
					fromUnit.getCode(), toUnit.getCode(), indexOperator.getName(), iataIndexConversion.getFactor(), bidderCode);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_QUOTATION_1,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_QUOTATION_1.getErrMsg(), iataIndex.getIndexProviderCode(),
							dataProvider.getName(), fromUnit.getCode(), toUnit.getCode(), indexOperator.getName(),
							iataIndexConversion.getFactor().toString(), bidderCode));
		}

		TimeSerie activeTimeSerie = timeSeries.get(0);
		Map<String, Object> paramsQuot = new HashMap<>();
		paramsQuot.put(AVG_METHOD_INDICATOR, avgMethod);
		paramsQuot.put(TIMESERIES2, activeTimeSerie);
		List<Quotation> quotations = this.quotationService.findEntitiesByAttributes(paramsQuot);

		if (quotations.isEmpty()) {
			LOGGER.warn(CmmErrorCode.IMPORT_BID_XML_EXTRACT_QUOTATION_2.getErrMsg(), activeTimeSerie.getExternalSerieName(), avgMethod.getCode(),
					bidderCode);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_QUOTATION_2,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_QUOTATION_2.getErrMsg(), activeTimeSerie.getExternalSerieName(),
							avgMethod.getCode(), bidderCode));

		}

		// active quotation is the first
		activeQuotation = quotations.get(0);
		return activeQuotation;
	}

	/**
	 * @param code
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private Operator extractOperator(String code, String bidderCode) throws BusinessException {
		Operator operator = null;
		try {
			operator = Operator.getByCode(code);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_OPERATOR.getErrMsg(), code, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_OPERATOR,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_OPERATOR.getErrMsg(), code, bidderCode), e);
		}
		return operator;
	}

	/**
	 * @param code
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private DataProvider extractDataProvider(String code, String bidderCode) throws BusinessException {
		DataProvider dataProvider = null;
		try {
			dataProvider = DataProvider.getByCode(code.toUpperCase());
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_DATA_PROVIDER.getErrMsg(), code, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_DATA_PROVIDER,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_DATA_PROVIDER.getErrMsg(), code, bidderCode), e);
		}
		return dataProvider;
	}

	/**
	 * @param name
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private MasterAgreementsPeriod extractExchangeRateOffset(String name, String bidderCode) throws BusinessException {
		MasterAgreementsPeriod masterAgreemenentPeriod;
		if (name.equals(MAP_N_MINUS_1)) {
			masterAgreemenentPeriod = MasterAgreementsPeriod._PREVIOUS_;
		} else if (name.equals(MAP_N_PLUS_0)) {
			masterAgreemenentPeriod = MasterAgreementsPeriod._CURRENT_;
		} else if (name.equals(MAP_N_PLUS_1)) {
			masterAgreemenentPeriod = MasterAgreementsPeriod._CURRENT__1_;
		} else if (name.equals(MAP_N_MINUS_2)) {
			masterAgreemenentPeriod = MasterAgreementsPeriod._CURRENT__2_;
		} else {
			LOGGER.warn(CmmErrorCode.IMPORT_BID_XML_EXTRACT_EXCHANGE_RATE.getErrMsg(), name, bidderCode);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_EXCHANGE_RATE,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_EXCHANGE_RATE.getErrMsg(), name, bidderCode));
		}
		return masterAgreemenentPeriod;
	}

	/**
	 * @param code
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private IataPC extractIataPC(String code, String bidderCode) throws BusinessException {
		IataPC iataPC = null;
		try {
			iataPC = this.iIataPCService.findByCode(code);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_IATA_PC.getErrMsg(), code, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_IATA_PC,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_IATA_PC.getErrMsg(), code, bidderCode), e);
		}
		return iataPC;
	}

	/**
	 * @param name
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private Operators extractOperators(String name, String bidderCode) throws BusinessException {
		Operators operators = null;
		try {
			operators = Operators.getByCode(name);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_OPERATORS.getErrMsg(), name, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_OPERATORS,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_OPERATORS.getErrMsg(), name, bidderCode), e);
		}
		return operators;
	}

	/**
	 * @param name
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private RestrictionTypes extractRestrictionType(String name, String bidderCode) throws BusinessException {
		RestrictionTypes restrictionTypes = null;
		try {
			restrictionTypes = RestrictionTypes.getByCode(name);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_RESTRICTION_TYPE.getErrMsg(), name, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_RESTRICTION_TYPE,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_RESTRICTION_TYPE.getErrMsg(), name, bidderCode), e);
		}
		return restrictionTypes;
	}

	/**
	 * @param name
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private PaymentType extractPaymentType(String name, String bidderCode) throws BusinessException {
		PaymentType paymentType = null;
		try {
			paymentType = PaymentType.getByCode(name);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_PAYMENT_TYPE.getErrMsg(), name, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_PAYMENT_TYPE,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_PAYMENT_TYPE.getErrMsg(), name, bidderCode), e);
		}
		return paymentType;
	}

	/**
	 * @param name
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private InvoiceType extractInvoiceType(String name, String bidderCode) throws BusinessException {
		InvoiceType invoiceType = null;
		try {
			invoiceType = InvoiceType.getByCode(name);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_INVOICE_TYPE.getErrMsg(), name, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_INVOICE_TYPE,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_INVOICE_TYPE.getErrMsg(), name, bidderCode), e);
		}
		return invoiceType;
	}

	/**
	 * @param name
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private InvoiceFreq extractInvoiceFreq(String name, String bidderCode) throws BusinessException {
		InvoiceFreq invoiceFreq = null;
		try {
			invoiceFreq = InvoiceFreq.getByCode(name);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_INVOICE_FREQ.getErrMsg(), name, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_INVOICE_FREQ,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_INVOICE_FREQ.getErrMsg(), name, bidderCode), e);
		}
		return invoiceFreq;
	}

	/**
	 * @param name
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private PaymentDay extractPaymentDay(String name, String bidderCode) throws BusinessException {
		PaymentDay paymentDay = null;
		try {
			paymentDay = PaymentDay.getByCode(name);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_PAYMENT_DAY.getErrMsg(), name, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_PAYMENT_DAY,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_PAYMENT_DAY.getErrMsg(), name, bidderCode), e);
		}
		return paymentDay;
	}

	/**
	 * @param name
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private InvoiceFreq extractPayementFrequency(String name, String bidderCode) throws BusinessException {
		InvoiceFreq subType = null;
		try {
			subType = InvoiceFreq.getByCode(name);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_PAYMENT_FREQUENCY.getErrMsg(), name, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_PAYMENT_FREQUENCY,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_PAYMENT_FREQUENCY.getErrMsg(), name, bidderCode), e);
		}
		return subType;
	}

	private String getSysVolUnit() throws BusinessException {
		return this.systemParamsService.getSysVol();
	}

	private Double getSystemDensity() throws BusinessException {
		return Double.parseDouble(this.systemParamsService.getDensity());
	}

	/**
	 * @param customer
	 * @return
	 * @throws BusinessException
	 */
	private Customer checkBidder(Customer customer) throws BusinessException {
		String currentSubsidiaryID = Session.user.get().getClient().getActiveSubsidiaryId();
		if (!customer.getRefid().equals(currentSubsidiaryID)) {
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_DIFFERENT_CLIENT,
					String.format(CmmErrorCode.IMPORT_BID_XML_DIFFERENT_CLIENT.getErrMsg(), customer.getCode()));
		}
		return customer;
	}

	/**
	 * @param tenderID
	 * @return
	 * @throws BusinessException
	 */
	private Tender extractTender(TenderIdentification tenderID) throws BusinessException {
		Tender tender;

		Customer holder = null;
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("iataCode", tenderID.getTenderHolderCode());
			holder = this.customerService.findEntityByAttributes(params);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_TENDER_1.getErrMsg(), tenderID.getTenderHolderCode(),
					tenderID.getTenderVersion().longValue(), tenderID.getTenderCode()), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_TENDER_1,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_TENDER_1.getErrMsg(), tenderID.getTenderHolderCode(),
							tenderID.getTenderVersion().longValue(), tenderID.getTenderCode()),
					e);
		}
		try {
			tender = this.tenderService.findByCode(tenderID.getTenderCode(), tenderID.getTenderVersion().longValue(), holder);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_TENDER_2.getErrMsg(), tenderID.getTenderCode(),
					tenderID.getTenderVersion().longValue(), holder.getCode()), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_TENDER_2,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_TENDER_2.getErrMsg(), tenderID.getTenderCode(),
							tenderID.getTenderVersion().longValue(), holder.getCode()),
					e);
		}

		return tender;
	}

	/**
	 * @param bid
	 * @return
	 * @throws BusinessException
	 */
	private BidTenderHeader createBidTenderHeader(Contract bid, long msgId) {
		BidTenderHeader bidHeader = new BidTenderHeader();

		if (bid.getHolder().getIataCode() != null) {
			bidHeader.setBidderCode(bid.getHolder().getIataCode());
		} else {
			bidHeader.setBidderCode(bid.getHolder().getCode());
		}
		bidHeader.setBidderName(bid.getHolder().getName());

		Map<String, Object> params = new HashMap<>();
		params.put(OBJECT_ID, bid.getBidTenderIdentification().getId());
		params.put(OBJECT_TYPE, Tender.class.getSimpleName());
		List<Notes> notes = this.noteService.findEntitiesByAttributes(params);

		if (!notes.isEmpty()) {
			StringBuilder comments = new StringBuilder();
			for (Notes note : notes) {
				comments.append(note.getNotes());
				comments.append(System.lineSeparator());
			}
			bidHeader.setComments(comments.toString());
		}

		bidHeader.setTenderIdentification(this.buildTenderIdentification(bid.getBidTenderIdentification(), msgId));

		// Set Bidder detailes
		BidderContact bidderContact = new BidderContact();
		Contact contact = new Contact();
		if (bid.getResponsibleBuyer() != null) {
			contact.setContactPerson(this.getContactFullName(bid.getResponsibleBuyer().getFirstName(), bid.getResponsibleBuyer().getLastName()));
			contact.setEmail(bid.getResponsibleBuyer().getEmail());
			contact.setPhone(bid.getResponsibleBuyer().getBusinessPhone().isEmpty() ? null : bid.getResponsibleBuyer().getBusinessPhone());
			// TODO when bid.getResponsibleBuyer is null it crashes with no response received from server.
		} else {
			contact.setContactPerson("");
			contact.setEmail("");
			contact.setPhone(null);
		}
		bidderContact.setContact(contact);
		bidHeader.setBidderContact(bidderContact);

		return bidHeader;
	}

	private String getContactFullName(String firstName, String lastName) {
		if (firstName == null || lastName == null) {
			return "";
		}
		return new StringBuilder(firstName).append(" ").append(lastName).toString();
	}

	/**
	 * @param code
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private Suppliers extractSuppliers(String code, String bidderCode) throws BusinessException {
		Suppliers suppliers = null;
		try {
			suppliers = this.suppliersService.findByCode(code);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_SUPPLIERS.getErrMsg(), code, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_SUPPLIERS,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_SUPPLIERS.getErrMsg(), code, bidderCode), e);
		}
		return suppliers;
	}

	/**
	 * @param name
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private ContractSubType extractSubType(String name, String bidderCode) throws BusinessException {
		ContractSubType subType = null;
		try {
			subType = ContractSubType.getByCode(name);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_SUB_TYPE.getErrMsg(), name, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_SUB_TYPE,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_SUB_TYPE.getErrMsg(), name, bidderCode), e);
		}
		return subType;
	}

	/**
	 * @param name
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private QuantityType extractQuantityType(String name, String bidderCode) throws BusinessException {
		QuantityType quantityType = null;
		try {
			quantityType = QuantityType.getByCode(name);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_QUANTITY_TYPE.getErrMsg(), name, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_QUANTITY_TYPE,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_QUANTITY_TYPE.getErrMsg(), name, bidderCode), e);
		}
		return quantityType;
	}

	/**
	 * @param code
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private Customer extractAirlineCustomer(String code, String bidderCode) throws BusinessException {
		Customer customer = null;
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("iataCode", code);
			customer = this.customerService.findEntityByAttributes(params);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_AIRLINE_CUSTOMER.getErrMsg(), code, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_AIRLINE_CUSTOMER,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_AIRLINE_CUSTOMER.getErrMsg(), code, bidderCode), e);
		}
		return customer;
	}

	/**
	 * @param name
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private FlightServiceType extractFlightServiceType(String name, String bidderCode) throws BusinessException {
		FlightServiceType flightServiceType = null;
		try {
			flightServiceType = FlightServiceType.getByCode(name);
		} catch (Exception e) {
			LOGGER.warn("Could not extract flight service type " + name + " for bid " + bidderCode, e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_FLIGHT_SERVICE_TYPE,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_FLIGHT_SERVICE_TYPE.getErrMsg(), name, bidderCode), e);
		}
		return flightServiceType;
	}

	/**
	 * @param name
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private TaxType extractTaxType(String name, String bidderCode) throws BusinessException {
		TaxType taxType = null;
		try {
			taxType = TaxType.getByIataCode(name);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_TAX_TYPE.getErrMsg(), name, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_TAX_TYPE,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_TAX_TYPE.getErrMsg(), name, bidderCode), e);
		}
		return taxType;
	}

	/**
	 * @param name
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private Product extractProduct(String name, String bidderCode) throws BusinessException {
		Product product = null;
		try {
			product = Product.getByIataName(name);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_PRODUCT.getErrMsg(), name, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_PRODUCT,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_PRODUCT.getErrMsg(), name, bidderCode), e);
		}
		return product;
	}

	/**
	 * @param locations
	 * @param tender
	 * @param taxType
	 * @param product
	 * @param flightServiceType
	 * @param bidderCode
	 * @param bidSubType
	 * @return
	 * @throws BusinessException
	 */
	private TenderLocation extractTenderLocation(Locations locations, Tender tender, TaxType taxType, Product product,
			FlightServiceType flightServiceType, String bidderCode, ContractSubType bidSubType) throws BusinessException {
		TenderLocation tenderLocation = null;
		try {
			tenderLocation = this.tenderLocationService.findByCode(locations, tender, taxType, product, flightServiceType, bidSubType);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_TENDER_LOCATION.getErrMsg(), locations.getCode(), tender.getCode(),
					taxType.getIataCode(), product.getIataName(), flightServiceType.name(), bidSubType.name(), bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_TENDER_LOCATION,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_TENDER_LOCATION.getErrMsg(), locations.getCode(), tender.getCode(),
							taxType.getIataCode(), product.getIataName(), flightServiceType.name(), bidSubType.name(), bidderCode),
					e);
		}
		return tenderLocation;
	}

	/**
	 * @param name
	 * @param bidderCode
	 * @return
	 * @throws BusinessException
	 */
	private Period extractPeriodType(String name, String bidderCode) throws BusinessException {
		Period period = null;
		try {
			period = Period.getByCode(name);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_PERIOD_TYPE.getErrMsg(), name, bidderCode), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_EXTRACT_PERIOD_TYPE,
					String.format(CmmErrorCode.IMPORT_BID_XML_EXTRACT_PERIOD_TYPE.getErrMsg(), name, bidderCode), e);
		}
		return period;
	}

	private VatApplicability getVATApplicability(VATApplicability vatApplicability) {
		return VatApplicability.getByCode(vatApplicability == null ? NOT_APPLICABLLE_VAT : vatApplicability.value());
	}

}
