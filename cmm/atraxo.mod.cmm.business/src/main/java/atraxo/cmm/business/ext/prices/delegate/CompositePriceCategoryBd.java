package atraxo.cmm.business.ext.prices.delegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

/**
 * @author apetho
 */
public class CompositePriceCategoryBd extends AbstractBusinessDelegate {

	private static final String ID = "id";
	private static final Logger LOG = LoggerFactory.getLogger(CompositePriceCategoryBd.class);

	/**
	 * @param jsonStr
	 * @param cpc
	 * @throws BusinessException
	 */
	public void assignParentPriceCategory(String jsonStr, ContractPriceCategory cpc) throws BusinessException {
		if (!PriceInd._COMPOSITE_.equals(cpc.getPriceCategory().getPricePer())) {
			return;
		}

		if ((jsonStr == null || jsonStr.isEmpty()) && CollectionUtils.isEmpty(cpc.getParentPriceCategory())) {
			throw new BusinessException(CmmErrorCode.COMPOSITE_PRICE_MUST_HAVE_A_CHILD, CmmErrorCode.COMPOSITE_PRICE_MUST_HAVE_A_CHILD.getErrMsg());
		}
		try {
			JSONArray jsonArray = new JSONArray(jsonStr);
			if (jsonArray.length() == 0) {
				throw new BusinessException(CmmErrorCode.COMPOSITE_PRICE_MUST_HAVE_A_CHILD,
						CmmErrorCode.COMPOSITE_PRICE_MUST_HAVE_A_CHILD.getErrMsg());
			}
			Collection<ContractPriceCategory> oldList = cpc.getParentPriceCategory();
			cpc.setParentPriceCategory(new ArrayList<ContractPriceCategory>());
			for (int i = 0; i < jsonArray.length(); ++i) {
				ContractPriceCategory priceCategory = this.getPriceCategory(jsonArray, i);
				cpc.getParentPriceCategory().add(priceCategory);
			}
			this.checkParentsStatus(cpc, oldList, true);
		} catch (JSONException e) {
			LOG.info("Invalid json : " + jsonStr, e);
		}
	}

	/**
	 * @param ids
	 * @throws BusinessException
	 */
	public void deleteCompositePriceCategory(List<ContractPriceCategory> deleteList, List<ContractPriceCategory> priceCategoryList)
			throws BusinessException {
		Set<ContractPriceCategory> oldList = new HashSet<>();
		for (ContractPriceCategory cpc : deleteList) {
			if (PriceInd._COMPOSITE_.equals(cpc.getPriceCategory().getPricePer())) {
				if (CollectionUtils.isEmpty(cpc.getParentPriceCategory())) {
					continue;
				}
				for (ContractPriceCategory cParrent : cpc.getParentPriceCategory()) {
					for (ContractPriceCategory contractCpc : priceCategoryList) {
						if (contractCpc.getId() == cParrent.getId()) {
							oldList.add(contractCpc);
						}
					}
				}
			}
		}
		this.checkParentsStatus(null, oldList, false);
	}

	/**
	 * @param cpc
	 * @param oldList
	 * @throws BusinessException
	 */
	private void checkParentsStatus(ContractPriceCategory cpc, Collection<ContractPriceCategory> oldList, boolean runUpdate)
			throws BusinessException {
		IContractPriceCategoryService cpcService = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
		List<ContractPriceCategory> updateList = new ArrayList<>();
		if (oldList == null) {
			return;
		}
		for (ContractPriceCategory temp : oldList) {
			if (cpc != null && cpc.getParentPriceCategory().contains(temp)) {
				continue;
			}
			if (cpcService.getCompositeChildsCategory(temp).size() == 1) {
				temp.setCalculateIndicator(CalculateIndicator._INCLUDED_);
				updateList.add(temp);
			}
		}
		if (!updateList.isEmpty() && runUpdate) {
			IContractPriceCategoryService pService = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
			pService.update(updateList);
		}
	}

	/**
	 * @param pService
	 * @param jsonArray
	 * @param i
	 * @return
	 * @throws JSONException
	 * @throws BusinessException
	 */
	private ContractPriceCategory getPriceCategory(JSONArray jsonArray, int i) throws JSONException, BusinessException {
		IContractPriceCategoryService pService = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
		JSONObject json = jsonArray.getJSONObject(i);
		String idStr = json.getString(ID);
		Integer priceCategoryId = Integer.parseInt(idStr);
		return pService.findById(priceCategoryId);
	}

	/**
	 * @param cpc
	 * @param indicator
	 * @throws BusinessException
	 */
	public void updateParentPriceCategories(ContractPriceCategory cpc, CalculateIndicator indicator) throws BusinessException {
		List<ContractPriceCategory> updateList = new ArrayList<>();
		if (cpc.getParentPriceCategory() == null || !PriceInd._COMPOSITE_.equals(cpc.getPriceCategory().getPricePer())) {
			return;
		}
		for (ContractPriceCategory tempPrice : cpc.getParentPriceCategory()) {
			if (!indicator.equals(tempPrice.getCalculateIndicator())) {
				tempPrice.setCalculateIndicator(indicator);
				updateList.add(tempPrice);
			}
		}
		IContractPriceCategoryService pService = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
		pService.update(updateList);
	}

}
