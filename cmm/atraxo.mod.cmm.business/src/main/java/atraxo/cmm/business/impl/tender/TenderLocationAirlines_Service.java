/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.tender;

import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link TenderLocationAirlines} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class TenderLocationAirlines_Service
		extends
			AbstractEntityService<TenderLocationAirlines> {

	/**
	 * Public constructor for TenderLocationAirlines_Service
	 */
	public TenderLocationAirlines_Service() {
		super();
	}

	/**
	 * Public constructor for TenderLocationAirlines_Service
	 * 
	 * @param em
	 */
	public TenderLocationAirlines_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<TenderLocationAirlines> getEntityClass() {
		return TenderLocationAirlines.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return TenderLocationAirlines
	 */
	public TenderLocationAirlines findByTender_location_airline(
			TenderLocation tenderLocation, Customer airline) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							TenderLocationAirlines.NQ_FIND_BY_TENDER_LOCATION_AIRLINE,
							TenderLocationAirlines.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("tenderLocation", tenderLocation)
					.setParameter("airline", airline).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(
					J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TenderLocationAirlines", "tenderLocation, airline"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(
					J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TenderLocationAirlines", "tenderLocation, airline"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return TenderLocationAirlines
	 */
	public TenderLocationAirlines findByTender_location_airline(
			Long tenderLocationId, Long airlineId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							TenderLocationAirlines.NQ_FIND_BY_TENDER_LOCATION_AIRLINE_PRIMITIVE,
							TenderLocationAirlines.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("tenderLocationId", tenderLocationId)
					.setParameter("airlineId", airlineId).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TenderLocationAirlines",
							"tenderLocationId, airlineId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TenderLocationAirlines",
							"tenderLocationId, airlineId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return TenderLocationAirlines
	 */
	public TenderLocationAirlines findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							TenderLocationAirlines.NQ_FIND_BY_BUSINESS,
							TenderLocationAirlines.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TenderLocationAirlines", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TenderLocationAirlines", "id"), nure);
		}
	}

	/**
	 * Find by reference: tenderLocation
	 *
	 * @param tenderLocation
	 * @return List<TenderLocationAirlines>
	 */
	public List<TenderLocationAirlines> findByTenderLocation(
			TenderLocation tenderLocation) {
		return this.findByTenderLocationId(tenderLocation.getId());
	}
	/**
	 * Find by ID of reference: tenderLocation.id
	 *
	 * @param tenderLocationId
	 * @return List<TenderLocationAirlines>
	 */
	public List<TenderLocationAirlines> findByTenderLocationId(
			Integer tenderLocationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocationAirlines e where e.clientId = :clientId and e.tenderLocation.id = :tenderLocationId",
						TenderLocationAirlines.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("tenderLocationId", tenderLocationId)
				.getResultList();
	}
	/**
	 * Find by reference: airline
	 *
	 * @param airline
	 * @return List<TenderLocationAirlines>
	 */
	public List<TenderLocationAirlines> findByAirline(Customer airline) {
		return this.findByAirlineId(airline.getId());
	}
	/**
	 * Find by ID of reference: airline.id
	 *
	 * @param airlineId
	 * @return List<TenderLocationAirlines>
	 */
	public List<TenderLocationAirlines> findByAirlineId(Integer airlineId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocationAirlines e where e.clientId = :clientId and e.airline.id = :airlineId",
						TenderLocationAirlines.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("airlineId", airlineId).getResultList();
	}
	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<TenderLocationAirlines>
	 */
	public List<TenderLocationAirlines> findByUnit(Unit unit) {
		return this.findByUnitId(unit.getId());
	}
	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<TenderLocationAirlines>
	 */
	public List<TenderLocationAirlines> findByUnitId(Integer unitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocationAirlines e where e.clientId = :clientId and e.unit.id = :unitId",
						TenderLocationAirlines.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitId", unitId).getResultList();
	}
}
