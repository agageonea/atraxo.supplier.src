package atraxo.cmm.business.ext.tender.service.updater;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.ext.tender.updater.ITenderUpdaterService;
import atraxo.cmm.business.api.tender.ITenderService;
import atraxo.cmm.business.ext.tender.service.updater.committer.TenderChangesCommitterService;
import atraxo.cmm.business.ext.tender.service.updater.listener.TenderPropertyAddedListener;
import atraxo.cmm.business.ext.tender.service.updater.listener.TenderPropertyChangeListener;
import atraxo.cmm.business.ext.tender.service.updater.listener.TenderPropertyRemovedListener;
import atraxo.cmm.business.ext.tender.service.updater.support.TenderChangedSupport;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import seava.j4e.api.exceptions.ActionNotSupportedException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.commons.utils.comparator.Comparator;
import seava.j4e.commons.utils.comparator.EventType;
import seava.j4e.commons.utils.listener.changes.Changes;

/**
 * Check the fields of the new Tender to figure out the differences and commits them logging to history each action
 *
 * @author abolindu
 */
public class TenderUpdaterService implements ITenderUpdaterService {

	private static final String MSG_FIELD = "msgId";
	private static final String IS_VALID_FIELD = "isValid";

	private Comparator comparator;

	@Autowired
	private ITenderService tenderService;
	@Autowired
	private TenderChangesCommitterService commiterService;
	@Autowired
	private TenderPropertyChangeListener propertyChangeListener;
	@Autowired
	private TenderPropertyAddedListener propertyAddedListener;
	@Autowired
	private TenderPropertyRemovedListener propertyRemovedListener;
	@Autowired
	private IContractService contractService;

	@PostConstruct
	void init() {
		this.comparator = new Comparator(new TenderChangedSupport(Comparator.class));
		this.comparator.addListener(this.propertyChangeListener, this.propertyAddedListener, this.propertyRemovedListener);
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.ext.tender.ITenderUpdaterService#updateTender(atraxo.cmm.domain.impl.tender.Tender)
	 */
	@Override
	public Tender updateTender(Tender receivedTender) throws Exception {
		Tender originalTender = this.findOriginalTender(receivedTender);
		this.compare(originalTender, receivedTender);
		Changes tenderChanges = this.comparator.getChanges();

		originalTender.setStatus(TenderStatus._UPDATED_);
		originalTender.setUpdatedOn(new Date());
		this.commiterService.commitChanges(tenderChanges);
		this.tenderService.update(originalTender);
		this.validateBid(originalTender);

		return originalTender;
	}

	private void validateBid(Tender tender) throws BusinessException {
		List<Contract> bids = this.contractService.findByBidTenderIdentification(tender);
		this.contractService.validateBid(bids);
	}

	/**
	 * Compares two Tenders
	 *
	 * @param originalTender
	 * @param receivedTender
	 * @return 0 if the Tenders are the same and -1 if they are different
	 * @throws ReflectiveOperationException
	 * @throws ActionNotSupportedException
	 * @throws CloneNotSupportedException
	 */
	public int compare(Tender originalTender, Tender receivedTender) throws ReflectiveOperationException {
		Map<String, Function<Object, EventType>> specialCases = this.defineSpecialCases();
		this.comparator.withComparisonSecialCases(specialCases);
		this.comparator.withIgnoredFields(TenderStatus.class, BiddingStatus.class, TransmissionStatus.class, Contacts.class,
				TenderLocationRound.class);
		this.comparator.compare(originalTender, receivedTender);

		if (this.comparator.hasChanges()) {
			return -1;
		}
		return 0;
	}

	private Map<String, Function<Object, EventType>> defineSpecialCases() {
		Map<String, Function<Object, EventType>> specialCases = new HashMap<>();

		specialCases.put(MSG_FIELD, object -> EventType.DO_NOTHING);

		specialCases.put(IS_VALID_FIELD, isValid -> {
			if ((boolean) isValid) {
				return EventType.OBJECT_ADDED;
			} else {
				return EventType.OBJECT_REMOVED;
			}
		});

		return specialCases;
	}

	private Tender findOriginalTender(Tender header) {
		Map<String, Object> params = new HashMap<>();
		params.put("code", header.getCode());
		params.put("tenderVersion", header.getTenderVersion());
		params.put("holder", header.getHolder());

		return this.tenderService.findEntityByAttributes(params);
	}

	public Comparator getComparator() {
		return this.comparator;
	}
}