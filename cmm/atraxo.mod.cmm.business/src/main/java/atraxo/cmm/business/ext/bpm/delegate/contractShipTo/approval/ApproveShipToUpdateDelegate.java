package atraxo.cmm.business.ext.bpm.delegate.contractShipTo.approval;

import atraxo.cmm.business.ext.bpm.delegate.ContractDelegate;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.domain.impl.fmbas_type.ContractChangeType;

/**
 * @author mbotorogea
 */
public class ApproveShipToUpdateDelegate extends ContractDelegate {

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#execute()
	 */
	@Override
	public void execute() throws Exception {
		Contract contract = this.getContract();
		if (contract != null) {
			this.contractChangeService.removeContractChange(contract, ContractChangeType._SHIPTO_);
			this.contractSrv.approveShipToUpdate(contract, this.getNote());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#getSessionUserCode()
	 */
	@Override
	public String getSessionUserCode() {
		return this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME);
	}
}
