package atraxo.cmm.business.ext.bpm.delegate.priceUpdate.approval;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.priceUpdate.IPriceUpdateService;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdate;
import atraxo.fmbas.business.api.custom.workflow.IWorkflowTerminatorService;
import atraxo.fmbas.business.ext.bpm.WorkflowMsgConstants;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

public class PriceUpdateApprovalWorkflowTerminator extends AbstractBusinessBaseService implements IWorkflowTerminatorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PriceUpdateApprovalWorkflowTerminator.class);

	@Autowired
	private IPriceUpdateService priceUpdateSrv;

	@Override
	public void terminateWorkflow(WorkflowInstance workflowInstance, String reason) throws BusinessException {

		List<WorkflowInstanceEntity> entities = new ArrayList<>(workflowInstance.getEntitites());

		for (WorkflowInstanceEntity entity : entities) {

			// reject the price update
			PriceUpdate priceUpdate = null;
			try {
				priceUpdate = this.priceUpdateSrv.findByBusiness(entity.getObjectId());
			} catch (Exception e) {
				LOGGER.warn("WARNING: could not retrieve a price update for ID " + entity.getObjectId()
						+ " ! Will not update the entity since it doesn't exist !", e);
			}
			if (priceUpdate != null) {
				this.priceUpdateSrv.reject(Arrays.asList(priceUpdate), WorkflowMsgConstants.WKF_RESULT_REJECTED,
						StringUtils.isEmpty(reason) ? WorkflowMsgConstants.APPROVAL_WORKFLOW_TERMINATED_COMMENT : reason);
			}
		}
	}
}
