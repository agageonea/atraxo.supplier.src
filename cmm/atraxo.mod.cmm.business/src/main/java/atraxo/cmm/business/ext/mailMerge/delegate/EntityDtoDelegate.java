package atraxo.cmm.business.ext.mailMerge.delegate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.ad.business.api.system.IDateFormatMaskService;
import atraxo.ad.business.api.system.IDateFormatService;
import atraxo.ad.domain.impl.system.DateFormat;
import atraxo.ad.domain.impl.system.DateFormatMask;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.ext.contracts.job.notification.CustomerData;
import atraxo.cmm.business.ext.mailMerge.delegate.builders.ContractDtoBuilder;
import atraxo.cmm.business.ext.mailMerge.delegate.builders.PriceDtoBuilder;
import atraxo.cmm.domain.ext.mailmerge.dto.BidDto;
import atraxo.cmm.domain.ext.mailmerge.dto.ContractTemplate;
import atraxo.cmm.domain.ext.mailmerge.dto.EmailTemplates;
import atraxo.cmm.domain.ext.mailmerge.dto.ShipToDto;
import atraxo.cmm.domain.ext.mailmerge.dto.TenderBidDto;
import atraxo.cmm.domain.ext.mailmerge.dto.TenderDto;
import atraxo.cmm.domain.ext.mailmerge.dto.contract.ContractDto;
import atraxo.cmm.domain.ext.mailmerge.dto.contract.ContractPriceDto;
import atraxo.cmm.domain.ext.mailmerge.dto.contract.RestrictionDto;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.fmbas.business.api.contacts.IContactsService;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.mailMerge.MailMergeManager;
import atraxo.fmbas.business.ext.util.EntityCloner;
import atraxo.fmbas.domain.ext.mailmerge.dto.CustomerDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.CustomerDtoPriceNotification;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailAddressesDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDataDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.SubsidiaryDto;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.customer.CustomerNotification;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

/**
 * @author abolindu
 */
/**
 * @author abolindu
 */
public class EntityDtoDelegate extends AbstractBusinessDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(EntityDtoDelegate.class);

	@Autowired
	private IContactsService contactsService;
	@Autowired
	private ISystemParameterService systemParameterService;
	@Autowired
	private ICustomerService customerService;
	@Autowired
	private IContractPriceCategoryService categoryService;
	@Autowired
	private IDateFormatService dtfmtService;
	@Autowired
	private IDateFormatMaskService dtfmtMaskService;

	/**
	 * Generates data for PriceUpdateNotification template
	 *
	 * @param customerData
	 * @param dateFormat
	 * @param fileExtension
	 * @param subsidiaryCode
	 * @param notificationEvent
	 * @return
	 * @throws BusinessException
	 */
	public EmailTemplates generateData(CustomerData customerData, String subsidiaryCode, Date customStartDate, Date customEndDate,
			List<CustomerNotification> notifications) throws BusinessException {
		List<ContractTemplate> contractTemplates = this.getCustomerData(customerData, customStartDate, customEndDate, notifications);
		if (CollectionUtils.isEmpty(contractTemplates)) {
			return null;
		}
		SubsidiaryDto subsidiary = this.getSubsidiaryDate(subsidiaryCode);
		EmailDto email = this.getEmailData(customerData, CollectionUtils.isEmpty(contractTemplates) ? null : contractTemplates.get(0).getCustomer(),
				subsidiary);
		return new EmailTemplates(email, contractTemplates);
	}

	/**
	 * Generates data for BidExport template
	 *
	 * @param bid
	 * @param fileExtension
	 * @return
	 * @throws BusinessException
	 */
	public String generateExportData(Contract bid, String fileExtension) throws BusinessException {
		DateFormat df = this.dtfmtService.findByName("dd-mm-yyyy");
		DateFormatMask dateFormatMask = this.dtfmtMaskService.findByName(df, "JAVA_DATE_FORMAT");
		BidDto bidDto = this.getBidData(bid);
		TenderBidDto tenderBidDto = new TenderBidDto();
		tenderBidDto.setBids(bidDto);
		return MailMergeManager.getDataJson(tenderBidDto, fileExtension, null, dateFormatMask.getValue());
	}

	/**
	 * Generate data for e-mail
	 *
	 * @param customerData
	 * @param customerDto
	 * @param subsidiary
	 * @param notificationEvent
	 * @return
	 * @throws BusinessException
	 */
	private EmailDto getEmailData(CustomerData customerData, CustomerDtoPriceNotification customerDto, SubsidiaryDto subsidiary)
			throws BusinessException {
		EmailDto email = new EmailDto();
		List<EmailAddressesDto> adresses = new ArrayList<>();
		List<Contacts> contacts = this.contactsService.findByObjectIdObjectType(customerData.getCustomerId(), Customer.class.getSimpleName());
		CustomerDtoPriceNotification emailCustomerDto = EntityCloner.cloneEntity(customerDto);
		emailCustomerDto.setContracts(null);
		for (Contacts contact : contacts) {
			EmailAddressesDto address = new EmailAddressesDto();
			EmailDataDto data = new EmailDataDto();
			if (this.containsNotification(contact.getNotification())) {
				data.setFirstName(contact.getFirstName());
				data.setLastName(contact.getLastName());
				data.setTitle(contact.getTitle() != null ? contact.getTitle().getName() : "");
				data.setEmail(contact.getEmail());
				data.setSubsidiary(subsidiary);
				data.setCustomer(emailCustomerDto);
				address.setTitle(contact.getTitle().getName());
				address.setName(contact.getFirstName() + " " + contact.getLastName());
				address.setAddress(contact.getEmail());
				address.setData(data);
				adresses.add(address);
			}
		}
		if (customerData.getResponsibleBuyer() == null) {
			email.setFrom(this.systemParameterService.getNoReplyAddress());
		} else {
			email.setFrom(customerData.getResponsibleBuyer().getEmail());
		}
		email.setTo(adresses);
		email.setSubject(BusinessErrorCode.PRICE_UPDATE_NOTIFICATION_JOB_SUBJECT.getErrMsg());
		return email;
	}

	private boolean containsNotification(Collection<NotificationEvent> notifications) {
		if (CollectionUtils.isEmpty(notifications)) {
			return false;
		}
		for (NotificationEvent ne : notifications) {
			if ("Contract price update".equalsIgnoreCase(ne.getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param customerData
	 * @return
	 * @throws BusinessException
	 */
	private List<ContractTemplate> getCustomerData(CustomerData customerData, Date customStartDate, Date customEndDate,
			List<CustomerNotification> notifications) throws BusinessException {
		CustomerDtoPriceNotification customerDto = this.buildCustomerDto(customerData, customStartDate, customEndDate);
		Integer decimals = this.systemParameterService.getDecimalsForCurrency();
		List<Contract> contracts = customerData.getContractsList();
		List<ContractTemplate> contractTemplates = new ArrayList<>();
		if (!contracts.isEmpty()) {
			for (Contract contract : contracts) {
				LOG.debug("Processing contract:" + contract.getCode());
				CustomerNotification notificationForContract = this.getBestNotificationByLocation(notifications, contract);
				if (notificationForContract == null) {
					LOG.debug("For contract:" + contract.getCode() + " is no template defined");
					continue;
				}
				ContractDto contractDto = ContractDtoBuilder.buildContract(contract);
				List<ContractPriceCategory> contractPriceCategories = new ArrayList<>(contract.getPriceCategories());
				contractDto.setPrices(
						PriceDtoBuilder.buildContractPrice(contractPriceCategories, decimals, this.categoryService, customStartDate, customEndDate));
				this.addToContractTemplateList(contractTemplates, contractDto, notificationForContract, customerDto);
				LOG.debug("Contract:" + contract.getCode() + " added to notifications list.");
			}
		}
		return contractTemplates;
	}

	private CustomerDtoPriceNotification buildCustomerDto(CustomerData customerData, Date customStartDate, Date customEndDate) {
		CustomerDtoPriceNotification customerDto = new CustomerDtoPriceNotification();
		customerDto.setName(customerData.getName());
		customerDto.setCustomStartDate(customStartDate);
		customerDto.setCustomEndDate(customEndDate);
		if (customerData.getResponsibleBuyer() != null) {
			customerDto.setAccountManager(customerData.getResponsibleBuyer().getFirstName() + " " + customerData.getResponsibleBuyer().getLastName());
		}
		return customerDto;
	}

	private void addToContractTemplateList(List<ContractTemplate> contractTemplates, ContractDto contractDto,
			CustomerNotification notificationForContract, CustomerDtoPriceNotification customerDto) {
		ContractTemplate contractTemplate = null;
		for (ContractTemplate ct : contractTemplates) {
			if (ct.getNotification().getId() == notificationForContract.getId()) {
				contractTemplate = ct;
			}
		}
		if (contractTemplate != null) {
			contractTemplate.addContract(contractDto);
		} else {
			contractTemplate = new ContractTemplate();
			contractTemplate.setNotification(notificationForContract);
			contractTemplate.addContract(contractDto);
			contractTemplate.setCustomer(EntityCloner.cloneEntity(customerDto));
			contractTemplates.add(contractTemplate);
		}
		contractTemplate.getCustomer().addContract(contractDto);
	}

	private CustomerNotification getBestNotificationByLocation(List<CustomerNotification> notifications, Contract contract) {
		Locations location = contract.getLocation();
		int areaSize = 0;
		CustomerNotification bestNotification = null;
		for (CustomerNotification notification : notifications) {
			Area area = notification.getAssignedArea();
			Collection<Locations> locations = area.getLocations();
			if ((bestNotification == null && this.areaContainsLocation(locations, location))
					|| (areaSize > locations.size() && this.areaContainsLocation(locations, location))) {
				bestNotification = notification;
				areaSize = area.getLocations().size();
			}
		}
		return bestNotification;
	}

	private boolean areaContainsLocation(Collection<Locations> locations, Locations location) {
		for (Locations l : locations) {
			if (location.getId() == l.getId()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param bidData
	 * @return
	 * @throws BusinessException
	 */
	private BidDto getBidData(Contract bidData) throws BusinessException {
		BidDto bids = new BidDto();
		CustomerDto holder = new CustomerDto();
		TenderDto tender = new TenderDto();
		List<ShipToDto> shipToDtoList = new ArrayList<>();
		List<ContractPriceDto> priceDtoList = new ArrayList<>();

		holder.setName(bidData.getHolder().getName());
		holder.setOfficeStreet(bidData.getHolder().getOfficeStreet());
		holder.setOfficePostalCode(bidData.getHolder().getOfficePostalCode());
		holder.setOfficeCity(bidData.getHolder().getOfficeCity());
		holder.setOfficeCountry(bidData.getHolder().getOfficeCountry() != null ? bidData.getHolder().getOfficeCountry().getName() : "");
		bids.setHolder(holder);

		tender.setCode(bidData.getBidTenderIdentification().getCode());
		tender.setName(bidData.getBidTenderIdentification().getName());
		tender.setVersion(bidData.getBidTenderIdentification().getTenderVersion());
		holder = new CustomerDto();
		holder.setName(bidData.getBidTenderIdentification().getHolder().getName());
		holder.setCode(bidData.getBidTenderIdentification().getHolder().getCode());
		tender.setHolder(holder);
		tender.setContact(this.getContactFullName(bidData.getBidTenderIdentification().getContact().getFirstName(),
				bidData.getBidTenderIdentification().getContact().getLastName()));
		bids.setTender(tender);

		bids.setLocation(bidData.getLocation() != null ? bidData.getLocation().getName() : "");
		bids.setRevision(bidData.getBidRevision());
		bids.setVersion(bidData.getBidVersion());
		bids.setValidFrom(bidData.getBidValidFrom());
		bids.setValidTo(bidData.getBidValidTo());
		bids.setContractValidFrom(bidData.getValidFrom());
		bids.setContractValidTo(bidData.getValidTo());
		bids.setContactFullName(
				bidData.getContact() != null ? this.getContactFullName(bidData.getContact().getFirstName(), bidData.getContact().getLastName()) : "");

		bids.setIataServiceLevel(bidData.getIataServiceLevel());
		bids.setTax(bidData.getTax() != null ? bidData.getTax().getName() : "");
		bids.setTitleTransfer(bidData.getBidTitleTransfer());
		bids.setAstmSpecification(bidData.getBidAstmSpecification());
		bids.setOperatingHours(bidData.getBidOperatingHours());
		bids.setIntoPlaneAgentName(bidData.getIntoPlaneAgent() != null ? bidData.getIntoPlaneAgent().getName() : "");

		bids.setProduct(bidData.getProduct().getName());
		bids.setOfferedVolume(bidData.getOfferedVolume());
		bids.setOfferedVolumeUnit(bidData.getContractVolumeUnit().getCode());
		bids.setContractedVolumePeriod(bidData.getPeriod().getName());
		bids.setVolumeTolerance(bidData.getVolumeTolerance());
		bids.setQuantityType(bidData.getQuantityType().getName());
		for (ShipTo shipToData : bidData.getShipTo()) {
			ShipToDto shipTo = new ShipToDto();
			shipTo.setName(shipToData.getCustomer().getName());
			shipTo.setIataCode(shipToData.getCustomer().getIataCode());
			shipTo.setAllocatedVolume(shipToData.getOfferedVolume());
			shipTo.setVolumeUnit(bidData.getSettlementUnit().getCode());
			shipTo.setContractedVolumePeriod(bidData.getPeriod().getName());
			shipToDtoList.add(shipTo);
		}
		bids.setShipTo(shipToDtoList);

		bids.setSettlementCurrency(bidData.getSettlementCurr().getCode());
		bids.setSettlementUnit(bidData.getSettlementUnit().getCode());
		bids.setPaymentTerms(bidData.getPaymentTerms());
		bids.setPaymentReferenceDay(bidData.getPaymentRefDay().getName());
		bids.setInvoiceFrequency(bidData.getInvoiceFreq().getName());
		bids.setPaymentType(bidData.getBidPaymentType().getName());
		bids.setInvoiceType(bidData.getInvoiceType().getName());
		bids.setFinancialSource(bidData.getFinancialSource().getName());
		bids.setAverageMethod(bidData.getAverageMethod().getName());
		bids.setExchangeRateOffset(bidData.getExchangeRateOffset().getName());

		bids.setCreditTerms(bidData.getCreditTerms().getName());
		bids.setBankGuarantee(bidData.getBidBankGuarantee());
		bids.setPrepaidDays(bidData.getBidPrepaidDays());
		bids.setPaymentFrequency(bidData.getBidPayementFreq().getName());
		bids.setPrepaidAmount(bidData.getBidPrepaidAmount());
		bids.setPrepayFirstDeliveryDate(bidData.getBidPrepayFirstDeliveryDate());
		bids.setPaymentComment(bidData.getPaymentComment());

		Date today = Calendar.getInstance().getTime();
		today = DateUtils.truncate(today, Calendar.DAY_OF_MONTH);
		if (today.compareTo(bidData.getValidFrom()) < 0) {
			priceDtoList.addAll(this.getOutOfIntervalPrices(bidData, today));
		} else {
			priceDtoList.addAll(this.getCategoryPrices(bidData, today));
		}
		bids.setPrices(priceDtoList);

		return bids;
	}

	/**
	 * @param bidData
	 * @param today
	 * @return
	 * @throws BusinessException
	 */
	private List<ContractPriceDto> getOutOfIntervalPrices(Contract bidData, Date today) throws BusinessException {
		List<ContractPriceDto> priceDtoList = new ArrayList<>();

		Iterator<ContractPriceCategory> iterator = bidData.getPriceCategories().iterator();
		while (iterator.hasNext()) {
			ContractPriceCategory price = iterator.next();

			List<ContractPriceComponent> priceComponentsList = (List<ContractPriceComponent>) price.getPriceComponents();
			Collections.sort(priceComponentsList, new Comparator<ContractPriceComponent>() {
				@Override
				public int compare(ContractPriceComponent o1, ContractPriceComponent o2) {
					return o1.getValidFrom().compareTo(o2.getValidFrom());
				}
			});

			if (priceComponentsList.get(0).getValidFrom().compareTo(bidData.getValidFrom()) != 0) {
				iterator.remove();
			}
		}

		ContractPriceDto prices = null;
		for (ContractPriceCategory price : bidData.getPriceCategories()) {
			if (!CollectionUtils.isEmpty(price.getChildPriceCategory())) {
				continue;
			}

			for (ContractPriceComponent priceComponent : price.getPriceComponents()) {
				List<ContractPriceComponentConv> convList = (List<ContractPriceComponentConv>) priceComponent.getConvertedPrices();
				if (today.compareTo(bidData.getValidFrom()) < 0) {
					Collections.sort(convList, new Comparator<ContractPriceComponentConv>() {
						@Override
						public int compare(ContractPriceComponentConv o1, ContractPriceComponentConv o2) {
							return o1.getValidFrom().compareTo(o2.getValidFrom());
						}
					});

					Integer decimals = this.systemParameterService.getDecimalsForCurrency();
					prices = this.setPrices(price, priceComponent);
					this.calculateCompositePrice(price, convList.get(0), bidData.getValidFrom());
					prices.setPrice(convList.get(0).getPrice().setScale(decimals, RoundingMode.HALF_UP));
					prices.setRestrictions(this.setRestrictions(price));
				}
			}
			priceDtoList.add(prices);
		}
		return priceDtoList;
	}

	/**
	 * @param bidData
	 * @param today
	 * @return
	 * @throws BusinessException
	 */
	private List<ContractPriceDto> getCategoryPrices(Contract bidData, Date today) throws BusinessException {
		List<ContractPriceDto> priceDtoList = new ArrayList<>();

		for (ContractPriceCategory price : bidData.getPriceCategories()) {
			if (!CollectionUtils.isEmpty(price.getChildPriceCategory())) {
				continue;
			}

			for (ContractPriceComponent priceComponent : price.getPriceComponents()) {
				if (today.compareTo(priceComponent.getValidFrom()) > 0 && today.compareTo(priceComponent.getValidTo()) < 0) {
					for (ContractPriceComponentConv conv : priceComponent.getConvertedPrices()) {
						if (today.compareTo(conv.getValidFrom()) > 0 && today.compareTo(conv.getValidTo()) < 0) {
							Integer decimals = this.systemParameterService.getDecimalsForCurrency();
							ContractPriceDto prices = this.setPrices(price, priceComponent);
							this.calculateCompositePrice(price, conv, today);
							prices.setPrice(conv.getPrice().setScale(decimals, RoundingMode.HALF_UP));
							prices.setRestrictions(this.setRestrictions(price));
							priceDtoList.add(prices);
						}
					}
				}
			}
		}
		return priceDtoList;
	}

	/**
	 * @param price
	 * @return
	 */
	private List<RestrictionDto> setRestrictions(ContractPriceCategory price) {
		List<RestrictionDto> restrictionDtoList = new ArrayList<>();
		for (ContractPriceRestriction priceRestriction : price.getPriceRestrictions()) {
			RestrictionDto restrictions = new RestrictionDto();
			restrictions.setType(priceRestriction.getRestrictionType().getName());
			restrictions.setOperator(priceRestriction.getOperator().getName());
			restrictions.setValue(priceRestriction.getValue());
			restrictions.setUnit(priceRestriction.getUnit() != null ? priceRestriction.getUnit().getCode() : "");
			restrictionDtoList.add(restrictions);
		}
		return restrictionDtoList;
	}

	/**
	 * @param price
	 * @param priceComponent
	 * @return
	 */
	private ContractPriceDto setPrices(ContractPriceCategory price, ContractPriceComponent priceComponent) {
		ContractPriceDto prices = new ContractPriceDto();
		prices.setName(price.getName());
		prices.setCategoryName(price.getPriceCategory().getName());
		prices.setIataCode(price.getPricingBases().getPriceCat().getIata().getCode());
		prices.setIataType(price.getPricingBases().getPriceCat().getIata().getUse().getName());
		prices.setRestriction(price.getRestriction());
		prices.setCurrency(priceComponent.getCurrency().getCode());
		// prices.setUnit(priceComponent.getUnit().getCode());
		return prices;
	}

	private String getContactFullName(String firstName, String lastName) {
		if (firstName == null || lastName == null) {
			return "";
		}
		return new StringBuilder(firstName).append(" ").append(lastName).toString();
	}

	/**
	 * @param contractPriceCategory
	 * @param conv
	 * @param date
	 * @throws BusinessException
	 */
	private void calculateCompositePrice(ContractPriceCategory contractPriceCategory, ContractPriceComponentConv conv, Date date)
			throws BusinessException {
		if (!PriceInd._COMPOSITE_.equals(conv.getContractPriceComponent().getContrPriceCtgry().getPriceCategory().getPricePer())) {
			return;
		}
		BigDecimal settlementPrice = this.categoryService.getPriceInCurrencyUnit(contractPriceCategory,
				contractPriceCategory.getContract().getSettlementUnit().getCode(), contractPriceCategory.getContract().getSettlementCurr().getCode(),
				date);
		BigDecimal price = this.categoryService.getPriceInCurrencyUnit(contractPriceCategory, conv.getEquivalentUnit().getCode(),
				conv.getEquivalentCurrency().getCode(), date);
		conv.setPrice(price);
		conv.setEquivalent(settlementPrice);
	}

	/**
	 * @param subsidiaryCode
	 * @return
	 */
	private SubsidiaryDto getSubsidiaryDate(String subsidiaryCode) {
		SubsidiaryDto subsidiary = new SubsidiaryDto();
		if (!subsidiaryCode.isEmpty()) {
			Customer customer = this.customerService.findByCode(subsidiaryCode);
			subsidiary.setFax(customer.getFaxNo());
			subsidiary.setName(customer.getName());
			subsidiary.setPhone(customer.getPhoneNo());
			subsidiary.setWeb(customer.getWebsite());
		}
		return subsidiary;
	}

}
