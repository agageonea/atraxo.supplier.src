/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.shipTo;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.fmbas.domain.impl.customer.Customer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ShipTo} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ShipTo_Service extends AbstractEntityService<ShipTo> {

	/**
	 * Public constructor for ShipTo_Service
	 */
	public ShipTo_Service() {
		super();
	}

	/**
	 * Public constructor for ShipTo_Service
	 * 
	 * @param em
	 */
	public ShipTo_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ShipTo> getEntityClass() {
		return ShipTo.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ShipTo
	 */
	public ShipTo findByContract_customer(Contract contract, Customer customer) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ShipTo.NQ_FIND_BY_CONTRACT_CUSTOMER,
							ShipTo.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("contract", contract)
					.setParameter("customer", customer).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ShipTo", "contract, customer"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ShipTo", "contract, customer"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return ShipTo
	 */
	public ShipTo findByContract_customer(Long contractId, Long customerId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							ShipTo.NQ_FIND_BY_CONTRACT_CUSTOMER_PRIMITIVE,
							ShipTo.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("contractId", contractId)
					.setParameter("customerId", customerId).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ShipTo", "contractId, customerId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ShipTo", "contractId, customerId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return ShipTo
	 */
	public ShipTo findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ShipTo.NQ_FIND_BY_BUSINESS, ShipTo.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ShipTo", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ShipTo", "id"), nure);
		}
	}

	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<ShipTo>
	 */
	public List<ShipTo> findByContract(Contract contract) {
		return this.findByContractId(contract.getId());
	}
	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<ShipTo>
	 */
	public List<ShipTo> findByContractId(Integer contractId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ShipTo e where e.clientId = :clientId and e.contract.id = :contractId",
						ShipTo.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractId", contractId).getResultList();
	}
	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<ShipTo>
	 */
	public List<ShipTo> findByCustomer(Customer customer) {
		return this.findByCustomerId(customer.getId());
	}
	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<ShipTo>
	 */
	public List<ShipTo> findByCustomerId(Integer customerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ShipTo e where e.clientId = :clientId and e.customer.id = :customerId",
						ShipTo.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerId", customerId).getResultList();
	}
}
