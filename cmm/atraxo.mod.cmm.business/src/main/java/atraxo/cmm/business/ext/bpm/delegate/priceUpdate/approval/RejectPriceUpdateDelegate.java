/**
 *
 */
package atraxo.cmm.business.ext.bpm.delegate.priceUpdate.approval;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.priceUpdate.IPriceUpdateService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdate;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate;

/**
 * @author vhojda
 */
public class RejectPriceUpdateDelegate extends AbstractJavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(RejectPriceUpdateDelegate.class);

	@Autowired
	private IPriceUpdateService priceUpdateSrv;

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.bpm.delegate.AbstractJavaDelegate#execute()
	 */
	@Override
	public void execute() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}

		// update the PRICE UPDATE and the history
		Object priceUpdateIdString = this.execution.getVariable(WorkflowVariablesConstants.PRICE_UPDATE_ID_VAR);
		if (priceUpdateIdString != null) {
			Integer priceUpdateId = Integer.parseInt(priceUpdateIdString.toString());
			PriceUpdate priceUpdate = null;
			try {
				priceUpdate = this.priceUpdateSrv.findByBusiness(priceUpdateId);
			} catch (Exception e) {
				LOGGER.error("ERROR:could not find a price update for ID " + priceUpdateId, e);
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						String.format(CmmErrorCode.PRICE_UPDATE_WORKFLOW_ENTITY_NOT_EXISTING.getErrMsg(), priceUpdateId));
			}
			if (priceUpdate != null) {
				this.priceUpdateSrv.reject(Arrays.asList(priceUpdate), BidApprovalStatus._REJECTED_.getName(), this.getNote());
			}
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#getSessionUserCode()
	 */
	@Override
	public String getSessionUserCode() {
		return this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME);
	}

}
