package atraxo.cmm.business.ext.priceUpdate.job;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.priceUpdate.IPriceUpdateService;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdate;

/**
 * Writer for Price Update publishing job.
 *
 * @author zspeter
 */
public class PriceMaintenanceWriter implements ItemWriter<PriceUpdate> {

	@Autowired
	private IPriceUpdateService srv;

	@SuppressWarnings("unchecked")
	@Override
	public void write(List<? extends PriceUpdate> items) throws Exception {
		this.srv.publish((List<PriceUpdate>) items);

	}

}
