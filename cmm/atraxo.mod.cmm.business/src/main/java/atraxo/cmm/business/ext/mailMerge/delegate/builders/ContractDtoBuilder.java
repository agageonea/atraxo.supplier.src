package atraxo.cmm.business.ext.mailMerge.delegate.builders;

import java.math.BigDecimal;
import java.util.Collection;

import org.springframework.util.CollectionUtils;

import atraxo.cmm.domain.ext.mailmerge.dto.contract.ContactDto;
import atraxo.cmm.domain.ext.mailmerge.dto.contract.ContractDto;
import atraxo.cmm.domain.ext.mailmerge.dto.contract.ResponsibleBuyerDto;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;

/**
 * @author apetho
 */
public final class ContractDtoBuilder {

	private ContractDtoBuilder() {

	}

	public static final ContractDto buildContract(Contract contract) {
		ContractDto dto = new ContractDto();
		if (contract != null) {
			dto.setNumber(contract.getCode());
			if (contract.getDealType() != null) {
				dto.setDealType(contract.getDealType().getName());
			}
			if (contract.getType() != null) {
				dto.setType(contract.getType().getName());
			}
			if (contract.getFlightServiceType() != null) {
				dto.setFlightServiceType(contract.getFlightServiceType().getName());
			}
			if (contract.getSubType() != null) {
				dto.setSubType(contract.getSubType().getName());
			}
			if (contract.getScope() != null) {
				dto.setScope(contract.getScope().getName());
			}
			if (contract.getIsSpot() != null) {
				dto.setTermOrSpot(contract.getIsSpot().getName());
			}
			dto.setValidFrom(contract.getValidFrom());
			dto.setValidTo(contract.getValidTo());
			if (contract.getStatus() != null) {
				dto.setStatus(contract.getStatus().getName());
			}
			if (contract.getLimitedTo() != null) {
				dto.setFlightType(contract.getLimitedTo().getName());
			}
			if (contract.getProduct() != null) {
				dto.setProduct(contract.getProduct().getName());
			}
			if (contract.getTax() != null) {
				dto.setTaxType(contract.getTax().getName());
			}
			if (contract.getQuantityType() != null) {
				dto.setQuantityType(contract.getQuantityType().getName());
			}
			if (contract.getPeriod() != null) {
				dto.setPeriod(contract.getPeriod().getName());
			}
			dto.setVolumeTolerance(contract.getVolumeTolerance() != null ? BigDecimal.valueOf(contract.getVolumeTolerance()) : BigDecimal.ZERO);
			dto.setSharePercentage(contract.getVolumeShare() != null ? BigDecimal.valueOf(contract.getVolumeShare()) : BigDecimal.ZERO);
			dto.setCounterparty(contract.getCounterpartyReference());
			dto.setSettlementDecimals(
					contract.getSettlementDecimals() != null ? BigDecimal.valueOf(contract.getSettlementDecimals()) : BigDecimal.ZERO);
			dto.setExchangeRateOffset(contract.getExchangeRateOffset().getName());
			dto.setPaymentTerms(contract.getPaymentTerms() != null ? BigDecimal.valueOf(contract.getPaymentTerms()) : BigDecimal.ZERO);
			if (contract.getPaymentRefDay() != null) {
				dto.setPaymentRefDay(contract.getPaymentRefDay().getName());
			}
			if (contract.getInvoiceFreq() != null) {
				dto.setInvoiceFrequency(contract.getInvoiceFreq().getName());
			}
			dto.setInvoiceFrequencyNumber(
					contract.getInvoiceFreqNumber() != null ? BigDecimal.valueOf(contract.getInvoiceFreqNumber()) : BigDecimal.ZERO);
			if (contract.getCreditTerms() != null) {
				dto.setCreditTerms(contract.getCreditTerms().getName());
			}
			if (contract.getInvoiceType() != null) {
				dto.setInvoiceType(contract.getInvoiceType().getName());
			}
			if (contract.getInvoicePayableBy() != null) {
				dto.setInvoicePayableBy(contract.getInvoicePayableBy().getName());
			}
			if (contract.getVat() != null) {
				dto.setVat(contract.getVat().getName());
			}
			if (contract.getReviewPeriod() != null) {
				dto.setReviewPeriod(contract.getReviewPeriod().getName());
			}
			dto.setReviewFirstDate(contract.getReviewFirstParam());
			dto.setReviewSecondDate(contract.getReviewSecondParam());
			if (contract.getReviewNotification() != null) {
				dto.setReviewNotification(contract.getReviewNotification().toString());
			}
			dto.setPaymentComment(contract.getPaymentComment());
			if (contract.getEventType() != null) {
				dto.setEventType(contract.getEventType().getName());
			}
			dto.setAwardedVolume(contract.getAwardedVolume());
			dto.setOfferedVolume(contract.getOfferedVolume());
			dto.setIataServiceLevel(contract.getIataServiceLevel() != null ? BigDecimal.valueOf(contract.getIataServiceLevel()) : BigDecimal.ZERO);
			dto.setUsingForexOf(contract.getForex());
			dto.setHardCopy(contract.getHardCopy());
			if (contract.getPeriodApprovalStatus() != null) {
				dto.setPeriodApprovalStatus(contract.getPeriodApprovalStatus().getName());
			}
			if (contract.getPriceApprovalStatus() != null) {
				dto.setPriceApprovalStatus(contract.getPriceApprovalStatus().getName());
			}
			if (contract.getShipToApprovalStatus() != null) {
				dto.setShipToApprovalStatus(contract.getShipToApprovalStatus().getName());
			}
			dto.setLocation(LocationDtoBuilder.buildLocation(contract.getLocation()));
			dto.setHolder(CustomerDtoBuilder.buildCustomer(contract.getHolder()));
			dto.setIntoPlaneAgent(SupplierDtoBuilder.buildSupplier(contract.getIntoPlaneAgent()));
			dto.setResponsibleBuyer(ContactDtoBuilder.buildContact(getRespBuyerEmail(contract), getRespBuyerTitle(contract),
					getRespBuyerFirstName(contract), getRespBuyerLastName(contract), ResponsibleBuyerDto.class));
			dto.setContact(ContactDtoBuilder.buildContact(getContactEmail(contract), getContactTitle(contract), getContactFirstName(contract),
					getContactLastName(contract), ContactDto.class));
			dto.setContractVolumeUnit(UnitDtoBuilder.buildUnit(contract.getContractVolumeUnit()));
			dto.setSettlementCurrency(CurrencyDtoBulder.buildCurrency(contract.getSettlementCurr()));
			dto.setSettlementUnit(UnitDtoBuilder.buildUnit(contract.getSettlementUnit()));
			dto.setAverageMethod(AverageMethodDtoBuilder.buildAverageMethod(contract.getAverageMethod()));
			dto.setFinancialSources(FinancialSourceDtoBuilder.buildFinancialSource(contract.getFinancialSource()));
			dto.setRiskHolder(CustomerDtoBuilder.buildCustomer(contract.getRiskHolder()));
			dto.setSupplier(SupplierDtoBuilder.buildSupplier(contract.getSupplier()));
			dto.setCustomer(CustomerDtoBuilder.buildCustomer(contract.getCustomer()));
			dto.setBillTo(CustomerDtoBuilder.buildCustomer(contract.getBillTo()));
			buildShipTo(dto, contract);
		}
		return dto;
	}

	private static String getRespBuyerEmail(Contract contract) {
		if (contract.getResponsibleBuyer() != null && contract.getResponsibleBuyer().getEmail() != null) {
			return contract.getResponsibleBuyer().getEmail();
		}
		return "";
	}

	private static String getRespBuyerTitle(Contract contract) {
		if (contract.getResponsibleBuyer() != null && contract.getResponsibleBuyer().getTitle() != null) {
			return contract.getResponsibleBuyer().getTitle().getName();
		}
		return "";
	}

	private static String getRespBuyerFirstName(Contract contract) {
		if (contract.getResponsibleBuyer() != null && contract.getResponsibleBuyer().getFirstName() != null) {
			return contract.getResponsibleBuyer().getFirstName();
		}
		return "";
	}

	private static String getRespBuyerLastName(Contract contract) {
		if (contract.getResponsibleBuyer() != null && contract.getResponsibleBuyer().getLastName() != null) {
			return contract.getResponsibleBuyer().getLastName();
		}
		return "";
	}

	private static String getContactEmail(Contract contract) {
		if (contract.getContact() != null && contract.getContact().getEmail() != null) {
			return contract.getContact().getEmail();
		}
		return "";
	}

	private static String getContactTitle(Contract contract) {
		if (contract.getContact() != null && contract.getContact().getTitle() != null) {
			return contract.getContact().getTitle().getName();
		}
		return "";
	}

	private static String getContactFirstName(Contract contract) {
		if (contract.getContact() != null && contract.getContact().getFirstName() != null) {
			return contract.getContact().getFirstName();
		}
		return "";
	}

	private static String getContactLastName(Contract contract) {
		if (contract.getContact() != null && contract.getContact().getLastName() != null) {
			return contract.getContact().getLastName();
		}
		return "";
	}

	static void buildShipTo(ContractDto dto, Contract contract) {
		Collection<ShipTo> shipTos = contract.getShipTo();
		if (CollectionUtils.isEmpty(shipTos)) {
			return;
		}
		for (ShipTo shipTo : shipTos) {
			dto.addToShipTo(ShipToDtoBuilder.builShipTo(shipTo));
		}
	}

}
