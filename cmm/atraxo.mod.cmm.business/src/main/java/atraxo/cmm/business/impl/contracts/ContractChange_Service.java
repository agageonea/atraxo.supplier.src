/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.contracts;

import atraxo.cmm.domain.impl.contracts.ContractChange;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ContractChange} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ContractChange_Service
		extends
			AbstractEntityService<ContractChange> {

	/**
	 * Public constructor for ContractChange_Service
	 */
	public ContractChange_Service() {
		super();
	}

	/**
	 * Public constructor for ContractChange_Service
	 * 
	 * @param em
	 */
	public ContractChange_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ContractChange> getEntityClass() {
		return ContractChange.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ContractChange
	 */
	public ContractChange findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ContractChange.NQ_FIND_BY_BUSINESS,
							ContractChange.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ContractChange", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ContractChange", "id"), nure);
		}
	}

}
