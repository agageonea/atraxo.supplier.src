package atraxo.cmm.business.ws.tender.transformer;

import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.ad.business.api.attachment.IAttachmentService;
import atraxo.ad.domain.impl.ad.TAttachmentType;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.cmm.business.api.ext.datahub.IDataHubOutTransformer;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.api.tender.ITenderLocationAirlinesService;
import atraxo.cmm.business.api.tender.ITenderLocationService;
import atraxo.cmm.domain.ext.tender.MessageType;
import atraxo.cmm.domain.impl.cmm_type.PricingMethod;
import atraxo.cmm.domain.impl.cmm_type.QuantityType;
import atraxo.cmm.domain.impl.cmm_type.RestrictionTypes;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.fmbas.business.api.categories.IPriceCategoryService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import atraxo.fmbas.domain.impl.fmbas_type.Use;
import atraxo.fmbas.domain.impl.notes.Notes;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.utils.DateUtils;
import seava.j4e.iata.fuelplus.iata.tender.AgreementPeriod;
import seava.j4e.iata.fuelplus.iata.tender.AirlinesVolumes;
import seava.j4e.iata.fuelplus.iata.tender.AirlinesVolumes.AirlinesVolume;
import seava.j4e.iata.fuelplus.iata.tender.AveragingMethod;
import seava.j4e.iata.fuelplus.iata.tender.BaseBasis;
import seava.j4e.iata.fuelplus.iata.tender.Bid;
import seava.j4e.iata.fuelplus.iata.tender.Bid.BidData;
import seava.j4e.iata.fuelplus.iata.tender.Bid.BidData.ProductsFees;
import seava.j4e.iata.fuelplus.iata.tender.BidHeader;
import seava.j4e.iata.fuelplus.iata.tender.BidIdentification;
import seava.j4e.iata.fuelplus.iata.tender.BidValidityPeriod;
import seava.j4e.iata.fuelplus.iata.tender.CFBase;
import seava.j4e.iata.fuelplus.iata.tender.CurrencyCodeBase;
import seava.j4e.iata.fuelplus.iata.tender.Decision;
import seava.j4e.iata.fuelplus.iata.tender.DeliveryPoint;
import seava.j4e.iata.fuelplus.iata.tender.Documents;
import seava.j4e.iata.fuelplus.iata.tender.ExchangeRate;
import seava.j4e.iata.fuelplus.iata.tender.FinancialSource;
import seava.j4e.iata.fuelplus.iata.tender.FlightServiceType;
import seava.j4e.iata.fuelplus.iata.tender.FrequencyBase;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcceptAward;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBid;
import seava.j4e.iata.fuelplus.iata.tender.Index;
import seava.j4e.iata.fuelplus.iata.tender.IndexConversion;
import seava.j4e.iata.fuelplus.iata.tender.IndexProvider;
import seava.j4e.iata.fuelplus.iata.tender.InvoiceType;
import seava.j4e.iata.fuelplus.iata.tender.LocationHeader;
import seava.j4e.iata.fuelplus.iata.tender.LocationNoBid;
import seava.j4e.iata.fuelplus.iata.tender.LogicalOperator;
import seava.j4e.iata.fuelplus.iata.tender.Market;
import seava.j4e.iata.fuelplus.iata.tender.OpenInvoice;
import seava.j4e.iata.fuelplus.iata.tender.PUOMBase;
import seava.j4e.iata.fuelplus.iata.tender.PackageIdentifier;
import seava.j4e.iata.fuelplus.iata.tender.Payment;
import seava.j4e.iata.fuelplus.iata.tender.PaymentTermsDateBasis;
import seava.j4e.iata.fuelplus.iata.tender.PaymentType;
import seava.j4e.iata.fuelplus.iata.tender.Prepayment;
import seava.j4e.iata.fuelplus.iata.tender.Price;
import seava.j4e.iata.fuelplus.iata.tender.ProductCode;
import seava.j4e.iata.fuelplus.iata.tender.ProductFee;
import seava.j4e.iata.fuelplus.iata.tender.ProductIDBase;
import seava.j4e.iata.fuelplus.iata.tender.ProductIDCustoms;
import seava.j4e.iata.fuelplus.iata.tender.ProductPricing;
import seava.j4e.iata.fuelplus.iata.tender.ProductServiceDetails;
import seava.j4e.iata.fuelplus.iata.tender.RateTypeBase;
import seava.j4e.iata.fuelplus.iata.tender.Restriction;
import seava.j4e.iata.fuelplus.iata.tender.RestrictionScope;
import seava.j4e.iata.fuelplus.iata.tender.Restrictions;
import seava.j4e.iata.fuelplus.iata.tender.ServiceLevel;
import seava.j4e.iata.fuelplus.iata.tender.SourceType14;
import seava.j4e.iata.fuelplus.iata.tender.Tax;
import seava.j4e.iata.fuelplus.iata.tender.TaxCategoryBase;
import seava.j4e.iata.fuelplus.iata.tender.TaxOnTax;
import seava.j4e.iata.fuelplus.iata.tender.TaxOnTaxes;
import seava.j4e.iata.fuelplus.iata.tender.TaxTypeBase;
import seava.j4e.iata.fuelplus.iata.tender.Taxes;
import seava.j4e.iata.fuelplus.iata.tender.TenderIdentification;
import seava.j4e.iata.fuelplus.iata.tender.TotalVolume;
import seava.j4e.iata.fuelplus.iata.tender.VATApplicability;
import seava.j4e.iata.fuelplus.iata.tender.VolumePeriod;

public abstract class DataHubOutTransformer<E extends AbstractEntity, T> extends DataHubTransformer implements IDataHubOutTransformer<E, T> {

	private static final String CONTRACT = "contract";
	private static final String PRICE_CATEGORY = "priceCategory";

	private static final Logger LOGGER = LoggerFactory.getLogger(DataHubOutTransformer.class);
	protected static final String MSG = "Method not implemented yet.";

	@Autowired
	private IAttachmentService attachmentService;
	@Autowired
	private IPriceCategoryService priceCatService;
	@Autowired
	private IContractPriceCategoryService contractPriceCatService;
	@Autowired
	private ITenderLocationService locationService;
	@Autowired
	private ITenderLocationAirlinesService airlinesService;

	@SuppressWarnings("unchecked")
	protected Class<T> iataClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];

	@Override
	public T transformModelToDTO(E entity, long msgId) throws BusinessException, JAXBException {
		return this.transformModelToDTO(entity, msgId, null);
	}

	@Override
	public T transformModelsToDTO(List<E> entities, long msgId) throws BusinessException, JAXBException {
		return this.transformModelsToDTO(entities, msgId, null);
	}

	protected Bid buildBid(Contract contract) throws BusinessException {
		BidData bidData = new BidData();
		bidData.setBidHeader(this.buildBidHeader(contract));

		// Add airline or total volume
		if (!contract.getShipTo().isEmpty()) {
			bidData.setAirlinesVolumes(this.buildAirlinesVolumes(contract));
		} else {
			bidData.setTotalVolume(this.buildTotalVolume(contract));
		}

		bidData.setProductServiceDetails(this.getProductServiceDetails(contract));
		bidData.setProductPricing(this.buildProductPricing(contract));
		bidData.setPayment(this.buildPayment(contract));
		bidData.setProductsFees(this.buildProductFees(contract));
		bidData.setVATApplicability(VATApplicability.fromValue(contract.getVat().getCode()));

		Bid bid = new Bid();
		bid.setBidData(bidData);
		bid.setBidIdentification(this.buildBidIdentification(contract));
		return bid;
	}

	protected TenderIdentification buildTenderIdentification(Tender tender, long msgId) {
		TenderIdentification tenderIdenttification = new TenderIdentification();
		tenderIdenttification.setTenderCode(tender.getCode());
		tenderIdenttification.setTenderName(tender.getName());
		tenderIdenttification
				.setTenderHolderCode(tender.getHolder().getIataCode() != null ? tender.getHolder().getIataCode() : tender.getHolder().getCode());
		tenderIdenttification.setTenderHolderName(tender.getHolder().getName());
		tenderIdenttification.setTenderVersion(BigInteger.valueOf(tender.getTenderVersion()));
		tenderIdenttification.setMsgId(msgId);
		return tenderIdenttification;
	}

	/**
	 * @param contract
	 * @return
	 * @throws BusinessException
	 */
	protected BidIdentification buildBidIdentification(Contract contract) throws BusinessException {
		BidIdentification bIdent = new BidIdentification();
		if (contract.getHolder().getIataCode() != null) {
			bIdent.setBidderCode(contract.getHolder().getIataCode());
		} else {
			bIdent.setBidderCode(contract.getHolder().getCode());
		}

		bIdent.setBidRevision(contract.getBidRevision() == null ? BigInteger.ONE : BigInteger.valueOf(contract.getBidRevision()));
		bIdent.setBidVersion(contract.getBidVersion());

		if (contract.getBidPackageIdentifier() != null) {
			PackageIdentifier packageIdentifier = new PackageIdentifier();
			packageIdentifier.setPackageCode(contract.getBidPackageIdentifier());
			bIdent.setPackageIdentifier(packageIdentifier);
		}

		List<Attachment> attachments = this.attachmentService.getDocumentsByType(contract.getId(), Contract.class.getSimpleName(),
				TAttachmentType._LINK_);
		Documents docs = new Documents();
		for (Attachment attach : attachments) {
			docs.getDocumentURI().add(attach.getUrl());
		}
		bIdent.setDocuments(docs.getDocumentURI().isEmpty() ? null : docs);
		bIdent.setLocationHeader(this.buildLocationHeader(contract.getBidTenderLocation(), contract));
		return bIdent;
	}

	protected BidHeader buildBidHeader(Contract contract) throws BusinessException {
		BidHeader bidHeader = new BidHeader();
		if (contract.getHolder().getIataCode() != null) {
			bidHeader.setBidderBidCode(contract.getHolder().getIataCode());
		}
		if (!contract.getQuantityType().equals(QuantityType._EMPTY_)) {
			bidHeader.setBidVolumeMeasurement(BaseBasis.fromValue(contract.getQuantityType().getCode()));
		}
		if (contract.getVolumeTolerance() != null) {
			bidHeader.setVolumeTolerance(BigDecimal.valueOf(contract.getVolumeTolerance()));
		}

		if (contract.getOfferedVolume() != null && !contract.getOfferedVolume().equals(BigDecimal.ZERO)) {
			Double density = Double.parseDouble(this.systemParamsService.getDensity());
			Unit sysUnit = this.unitService.findByCode(this.systemParamsService.getSysVol());

			BigDecimal tenderVolume = BigDecimal.ZERO;
			tenderVolume = tenderVolume
					.add(this.unitService.convert(sysUnit, contract.getSettlementUnit(), contract.getBidTenderLocation().getVolume(), density));

			BigDecimal percentageVolume = BigDecimal.ZERO;
			if (tenderVolume.compareTo(BigDecimal.ZERO) > 0) {
				percentageVolume = contract.getOfferedVolume().divide(tenderVolume, 2, RoundingMode.HALF_UP);
			}
			bidHeader.setPercentageOfTenderVolume(percentageVolume);
		}

		Map<String, Object> params = new HashMap<>();
		params.put(OBJECT_ID, contract.getId());
		params.put(OBJECT_TYPE, Contract.class.getSimpleName());
		List<Notes> notes = this.noteService.findEntitiesByAttributes(params);

		if (!notes.isEmpty()) {
			StringBuilder comments = new StringBuilder();
			for (Notes note : notes) {
				comments.append(note.getNotes());
				comments.append(System.lineSeparator());
			}
			bidHeader.setComments(comments.toString());
		}

		AgreementPeriod agrPer = new AgreementPeriod();
		agrPer.setFrom(DateUtils.toXMLGregorianCalendar(contract.getValidFrom()));
		agrPer.setTo(DateUtils.toXMLGregorianCalendar(contract.getValidTo()));
		bidHeader.setAgreementPeriod(agrPer);

		if (contract.getBidValidFrom() != null && contract.getBidValidTo() != null) {
			BidValidityPeriod bidValidPer = new BidValidityPeriod();
			bidValidPer.setFrom(DateUtils.toXMLGregorianCalendar(contract.getBidValidFrom()));
			bidValidPer.setTo(DateUtils.toXMLGregorianCalendar(contract.getBidValidTo()));
			bidHeader.setBidValidityPeriod(bidValidPer);
		}
		return bidHeader;
	}

	protected List<LocationNoBid> buildLocationsHeader(List<TenderLocation> tenderLocations, Contract contract) {
		List<LocationNoBid> list = new ArrayList<>();
		for (TenderLocation tenderLocation : tenderLocations) {
			LocationNoBid locationNoBid = new LocationNoBid();
			locationNoBid.setLocationHeader(this.buildLocationHeader(tenderLocation, contract));
			list.add(locationNoBid);
		}
		return list;
	}

	protected LocationHeader buildLocationHeader(TenderLocation tenderLocation, Contract contract) {
		LocationHeader locationHeader = new LocationHeader();
		locationHeader.setLocationCodeIATA(tenderLocation.getLocation().getIataCode());
		locationHeader.setLocationCodeICAO(tenderLocation.getLocation().getIcaoCode());
		locationHeader.setFlightServiceType(FlightServiceType.fromValue(tenderLocation.getFlightServiceType().getCode()));

		if (contract != null) {
			locationHeader.setFuelProduct(ProductCode.fromValue(contract.getProduct().getIataName()));
		} else {
			locationHeader.setFuelProduct(ProductCode.fromValue(tenderLocation.getFuelProduct().getIataName()));
		}

		if (contract != null) {
			if (contract.getTax() != null && !StringUtils.isEmpty(contract.getTax().getIataCode())) {
				locationHeader.setProductTaxType(ProductIDCustoms.valueOf(contract.getTax().getIataCode()));
			}
		} else {
			locationHeader.setProductTaxType(ProductIDCustoms.valueOf(tenderLocation.getTaxType().getIataCode()));
		}
		return locationHeader;
	}

	protected Taxes addFees(ContractPriceCategory priceCat) throws BusinessException {
		Taxes feeTaxes = new Taxes();
		BigInteger taxSortID = BigInteger.ZERO;

		for (ContractPriceCategory priceChildCat : priceCat.getChildPriceCategory()) {
			taxSortID = taxSortID.add(BigInteger.ONE);

			Tax tax = this.buildTax(priceChildCat);
			tax.setSortID(taxSortID);
			tax.setTaxAlias(priceChildCat.getName());
			feeTaxes.getTax().add(tax);
		}
		return feeTaxes;
	}

	protected Taxes addTaxes(Contract contract, ContractPriceCategory priceCat) throws BusinessException {
		Taxes productTaxes = new Taxes();
		BigInteger taxSortID = BigInteger.ZERO;

		for (ContractPriceCategory taxCat : contract.getPriceCategories()) {
			if (taxCat.getPriceCategory().getIata().getUse().equals(Use._TAX_)
					&& (taxCat.getParentPriceCategory().isEmpty() || taxCat.getParentPriceCategory().contains(priceCat))) {

				taxSortID = taxSortID.add(BigInteger.ONE);

				Tax tax = this.buildTax(taxCat);
				tax.setSortID(taxSortID);
				tax.setTaxAlias(taxCat.getName());
				productTaxes.getTax().add(tax);
			}
		}
		return productTaxes;
	}

	protected ProductServiceDetails getProductServiceDetails(Contract contract) throws BusinessException {
		ProductServiceDetails productServiceDetails = new ProductServiceDetails();
		productServiceDetails.setDeliveryPoint(DeliveryPoint.fromValue(contract.getSubType().getCode()));
		productServiceDetails.setFuelSpecification(contract.getBidAstmSpecification());
		if (contract.getIataServiceLevel() != null) {
			productServiceDetails.setIATAServiceLevel(ServiceLevel.fromValue("IT" + contract.getIataServiceLevel()));
		}
		if (contract.getBidOperatingHours() != null) {
			productServiceDetails.setOperatingHours(contract.getBidOperatingHours());
		}
		if (contract.getIntoPlaneAgent() != null) {
			productServiceDetails.setRefuelerCompanyCode(contract.getIntoPlaneAgent().getIataCode());
		}
		if (contract.getBidTitleTransfer() != null) {
			productServiceDetails.setTitleTransfer(contract.getBidTitleTransfer());
		}
		return productServiceDetails;
	}

	protected TotalVolume buildTotalVolume(Contract contract) throws BusinessException {
		TotalVolume totalVolume = new TotalVolume();
		totalVolume.setVolume(this.getContractVolume(contract));
		totalVolume.setUOM(PUOMBase.fromValue(contract.getSettlementUnit().getIataCode()));
		totalVolume.setPeriodType(VolumePeriod.fromValue(contract.getPeriod().getCode()));
		return totalVolume;
	}

	protected AirlinesVolumes buildAirlinesVolumes(Contract contract) throws BusinessException {
		AirlinesVolumes airVolumes = new AirlinesVolumes();
		for (ShipTo shipTo : contract.getShipTo()) {
			TenderLocation tenderLocation = this.locationService.findByCode(contract.getLocation(), contract.getBidTenderIdentification(),
					contract.getTax(), contract.getProduct(), contract.getFlightServiceType(), contract.getSubType());
			TenderLocationAirlines airline = this.airlinesService.findByTender_location_airline(tenderLocation, shipTo.getCustomer());
			if (airline.getIsValid()) {
				// Verify if IATA Code exists
				if (shipTo.getCustomer().getIataCode() == null) {
					throw new BusinessException(BusinessErrorCode.IATA_NOT_EXISTS, BusinessErrorCode.IATA_NOT_EXISTS.getErrMsg());
				}

				AirlinesVolume airVol = new AirlinesVolume();
				airVol.setAirlineCode(shipTo.getCustomer().getIataCode());
				airVol.setVolume(this.getShipToVolume(shipTo));

				airVolumes.getAirlinesVolume().add(airVol);
				airVolumes.setUOM(PUOMBase.fromValue(shipTo.getContract().getSettlementUnit().getIataCode()));

			}
			airVolumes.setPeriodType(VolumePeriod.fromValue(contract.getPeriod().getCode()));
		}
		return airVolumes;
	}

	protected ProductPricing buildProductPricing(Contract contract) throws BusinessException {
		ProductPricing productPricing = new ProductPricing();
		// Add Index or Market
		ContractPriceCategory contractPriceCatIndex = this.getContractPriceCategory(contract, PricingMethod._INDEX_);
		if (contractPriceCatIndex != null) {
			Index index = new Index();
			try {
				index.setIndexProvider(
						IndexProvider.valueOf(contractPriceCatIndex.getPricingBases().getQuotation().getTimeseries().getDataProvider().getCode()));
			} catch (IllegalArgumentException e) {
				index.setIndexProvider(IndexProvider.OTHER);

				LOGGER.info("Could not find IATA IndexProvider11 from "
						+ contractPriceCatIndex.getPricingBases().getQuotation().getTimeseries().getDataProvider().getCode(), e);
			}
			index.setIndexProviderCode(contractPriceCatIndex.getPricingBases().getQuotation().getTimeseries().getExternalSerieName());
			index.setAverageingMethod(
					AveragingMethod.fromValue(contractPriceCatIndex.getPricingBases().getQuotation().getAvgMethodIndicator().getCode()));
			index.setAveragingOffset(this.getOffset(contractPriceCatIndex.getPricingBases().getQuotationOffset()));
			index.setVATApplicability(VATApplicability.fromValue(contractPriceCatIndex.getVat().getCode()));

			// set the index conversion also
			IndexConversion indexConversion = new IndexConversion();
			indexConversion.setDensity(BigDecimal.valueOf(Double.parseDouble(this.systemParamsService.getDensity())));
			indexConversion.setDensityWeightUnit(this.getProductUOM(this.systemParamsService.getSysWeight()));
			indexConversion.setDensityVolumeUnit(this.getProductUOM(this.systemParamsService.getSysVol()));
			indexConversion.setFromUnit(PUOMBase.fromValue(contractPriceCatIndex.getPricingBases().getQuotation().getUnit().getIataCode()));
			indexConversion.setToUnit(PUOMBase.fromValue(contractPriceCatIndex.getPricingBases().getConvUnit().getIataCode()));
			indexConversion.setConvOperator(CFBase.fromValue(contractPriceCatIndex.getPricingBases().getOperator().getCode()));
			indexConversion.setFactor(contractPriceCatIndex.getPricingBases().getFactor());
			index.setIndexConversion(indexConversion);

			productPricing.setIndex(index);
		} else {
			ContractPriceCategory contractPriceCatMarket = this.getContractPriceCategory(contract, PricingMethod._FIXED_);
			if (contractPriceCatMarket != null) {
				ContractPriceComponent priceComponent = this.contractPriceCatService.getPriceComponent(contractPriceCatMarket,
						contract.getValidFrom());

				if (priceComponent != null) {
					Market market = new Market();
					market.setSourceType(SourceType14.E);
					market.setRate(priceComponent.getPrice());
					market.setCurrency(CurrencyCodeBase.fromValue(priceComponent.getCurrency().getCode()));
					market.setUOM(this.getProductUOM(priceComponent.getUnit().getCode()));
					market.setVATApplicability(VATApplicability.valueOf(contractPriceCatMarket.getVat().getCode()));
					productPricing.setMarket(market);
				}
			}
		}
		return productPricing;
	}

	protected Payment buildPayment(Contract contract) throws BusinessException {
		Payment payment = new Payment();
		payment.setPaymentCurrency(CurrencyCodeBase.fromValue(contract.getSettlementCurr().getCode()));
		payment.setPricingUnit(PUOMBase.fromValue(contract.getSettlementUnit().getIataCode()));

		if (contract.getBidPaymentType() != null) {
			payment.setMethodOfPayment(PaymentType.fromValue(contract.getBidPaymentType().getCode()));
		}
		if (contract.getBidBankGuarantee() != null) {
			payment.setGuaranteesDepositsRequired(contract.getBidBankGuarantee() ? Decision.Y : Decision.N);
		}

		if (contract.getCreditTerms().equals(CreditTerm._PREPAYMENT_)) {
			Prepayment prepayment = new Prepayment();
			prepayment.setNumberOfDaysPrepaid(BigInteger.valueOf(contract.getBidPrepaidDays()));
			prepayment.setPaymentFirstDeliveryDate(BigInteger.valueOf(contract.getBidPrepayFirstDeliveryDate()));
			prepayment.setPaymentFrequency(FrequencyBase.fromValue(contract.getBidPayementFreq().getCode()));
			prepayment.setAmount(contract.getBidPrepaidAmount());
			payment.setPrepayment(prepayment);
		} else {
			OpenInvoice openInvoice = new OpenInvoice();
			openInvoice.setPaymentTerms(BigInteger.valueOf(contract.getPaymentTerms()));
			openInvoice.setPaymentReferenceDateType(PaymentTermsDateBasis.fromValue(contract.getPaymentRefDay().getCode()));
			openInvoice.setInvoiceFrequency(FrequencyBase.fromValue(contract.getInvoiceFreq().getCode()));
			openInvoice.setInvoiceType(InvoiceType.fromValue(contract.getInvoiceType().getCode()));
			payment.setOpenInvoice(openInvoice);
		}

		ExchangeRate exchangeRate = new ExchangeRate();
		try {
			exchangeRate.setFinancialSource(FinancialSource.fromValue(contract.getFinancialSource().getCode()));
		} catch (IllegalArgumentException e) {
			LOGGER.error("Could not find IATA ExchangeRateSource7 from " + contract.getFinancialSource().getCode(), e);
		}
		exchangeRate.setAverageingMethod(AveragingMethod.fromValue(contract.getAverageMethod().getCode()));
		exchangeRate.setAveragingOffset(this.getOffset(contract.getExchangeRateOffset()));
		payment.setExchangeRate(exchangeRate);
		return payment;
	}

	protected ProductsFees buildProductFees(Contract contract) throws BusinessException {
		BigInteger priceCatSortID = BigInteger.ZERO;
		ProductsFees productsFees = new ProductsFees();

		// Get Product Price and Taxes
		ProductFee productFee;
		for (ContractPriceCategory priceCat : contract.getPriceCategories()) {
			if (priceCat.getPriceCategory().getType().equals(PriceType._PRODUCT_)) {
				priceCatSortID = priceCatSortID.add(BigInteger.ONE);

				productFee = this.buildFee(priceCat);
				productFee.setSortID(priceCatSortID);

				// Add Taxes
				Taxes productTaxes = this.addTaxes(contract, priceCat);

				productFee.setTaxes(productTaxes);
				productFee.setVATApplicability(VATApplicability.fromValue(priceCat.getVat().getCode()));
				productsFees.getProductFee().add(productFee);
			}
		}

		// Get Other Price Fees
		ProductFee fee;
		for (ContractPriceCategory priceCat : contract.getPriceCategories()) {
			if (!priceCat.getPriceCategory().getType().equals(PriceType._PRODUCT_)
					&& priceCat.getPriceCategory().getIata().getUse().equals(Use._FEE_)) {

				priceCatSortID = priceCatSortID.add(BigInteger.ONE);

				fee = this.buildFee(priceCat);
				fee.setSortID(priceCatSortID);

				if (!CollectionUtils.isEmpty(priceCat.getChildPriceCategory())) {
					Taxes feeTaxes = this.addFees(priceCat);

					fee.setTaxes(feeTaxes);
				}
				productsFees.getProductFee().add(fee);
			}
		}
		return productsFees;
	}

	private ProductFee buildFee(ContractPriceCategory priceCat) throws BusinessException {
		ProductFee productFee = new ProductFee();
		productFee.setItemProductAlias(priceCat.getName());

		if (priceCat.getPriceCategory().getType().equals(PriceType._PRODUCT_)) {
			productFee.setItemProductID(ProductIDBase.fromValue(priceCat.getContract().getProduct().getIataName()));
		} else {
			productFee.setItemProductID(ProductIDBase.fromValue(priceCat.getPriceCategory().getIata().getCode()));
		}

		if (priceCat.getComments() != null) {
			productFee.setExplanation(priceCat.getComments());
		}

		ContractPriceComponent priceComponent = this.contractPriceCatService.getPriceComponent(priceCat, priceCat.getContract().getValidFrom());

		// set Price
		Price price = new Price();
		price.setPricingUnitRateType(this.getPricingUnitRate(priceCat.getPriceCategory().getPricePer()));
		price.setPricingUnitRate(priceComponent.getPrice());
		price.setPricingCurrencyCode(CurrencyCodeBase.fromValue(priceComponent.getCurrency().getCode()));
		price.setPricingUOM(PUOMBase.fromValue(priceComponent.getUnit().getIataCode()));
		productFee.setPrice(price);

		// set Exchange Rate
		ExchangeRate exchangeRate = new ExchangeRate();
		try {
			exchangeRate.setFinancialSource(FinancialSource.fromValue(this.getFinancialSourceFromCode(priceCat)));
		} catch (IllegalArgumentException e) {
			LOGGER.error("Could not find IATA ExchangeRateSource7 from " + this.getFinancialSourceFromCode(priceCat), e);
		}
		exchangeRate.setAverageingMethod(AveragingMethod.fromValue(priceCat.getAverageMethod().getCode()));
		exchangeRate.setAveragingOffset(this.getOffset(priceCat.getExchangeRateOffset()));
		productFee.setExchangeRate(exchangeRate);
		productFee.setVATApplicability(VATApplicability.fromValue(priceCat.getVat().getCode()));

		// set Restrictions
		Restrictions restrictions = new Restrictions();
		if (priceCat.getRestriction()) {
			restrictions = this.getPriceCategoryRestrictions(priceCat);
		}
		productFee.setRestrictions(restrictions.getRestriction().isEmpty() ? null : restrictions);
		return productFee;
	}

	private Restrictions getPriceCategoryRestrictions(ContractPriceCategory priceCat) throws BusinessException {
		Restrictions restrictions = new Restrictions();
		for (ContractPriceRestriction priceRes : priceCat.getPriceRestrictions()) {
			Restriction restriction = new Restriction();
			restriction.setScope(RestrictionScope.fromValue(priceRes.getRestrictionType().getCode()));
			restriction.setOperator(LogicalOperator.fromValue(priceRes.getOperator().getCode()));

			String restrictionValue;
			if (RestrictionTypes._SHIP_TO_.equals(priceRes.getRestrictionType())) {
				restrictionValue = this.findCustomer(priceRes.getValue(), "Restriction Value").getIataCode();
			} else {
				restrictionValue = priceRes.getValue();
			}
			restriction.setValue1(restrictionValue);

			if (priceRes.getUnit() != null) {
				restriction.setUOM(PUOMBase.fromValue(priceRes.getUnit().getIataCode()));
			}

			restrictions.getRestriction().add(restriction);
		}
		return restrictions;
	}

	protected Tax buildTax(ContractPriceCategory taxCat) throws BusinessException {
		Tax tax = new Tax();

		tax.setTaxType(TaxTypeBase.fromValue(taxCat.getPriceCategory().getIata().getCode()));
		tax.setTaxCategoryCode(TaxCategoryBase.S);

		ContractPriceComponent taxPriceComponent = this.contractPriceCatService.getPriceComponent(taxCat, taxCat.getContract().getValidFrom());

		tax.setVATApplicability(VATApplicability.fromValue(taxCat.getVat().getCode()));

		Price taxPrice = new Price();
		taxPrice.setPricingUnitRateType(this.getPricingUnitRate(taxCat.getPriceCategory().getPricePer()));
		taxPrice.setPricingUnitRate(taxPriceComponent.getPrice());
		taxPrice.setPricingCurrencyCode(CurrencyCodeBase.fromValue(taxPriceComponent.getCurrency().getCode()));
		taxPrice.setPricingUOM(PUOMBase.fromValue(taxPriceComponent.getUnit().getIataCode()));
		tax.setPrice(taxPrice);

		ExchangeRate taxExchangeRate = new ExchangeRate();
		taxExchangeRate.setFinancialSource(FinancialSource.fromValue(this.getFinancialSourceFromCode(taxCat)));
		taxExchangeRate.setAverageingMethod(AveragingMethod.fromValue(taxCat.getAverageMethod().getCode()));
		taxExchangeRate.setAveragingOffset(this.getOffset(taxCat.getExchangeRateOffset()));
		tax.setExchangeRate(taxExchangeRate);

		Restrictions restrictions = new Restrictions();
		if (taxCat.getRestriction()) {
			restrictions = this.getPriceCategoryRestrictions(taxCat);
		}
		tax.setRestrictions(restrictions);

		if (!taxCat.getChildPriceCategory().isEmpty()) {
			TaxOnTaxes taxOnTaxes = new TaxOnTaxes();
			BigInteger taxOnTaxSortID = BigInteger.ZERO;

			for (ContractPriceCategory taxChildCat : taxCat.getChildPriceCategory()) {
				taxOnTaxSortID = taxOnTaxSortID.add(BigInteger.ONE);

				TaxOnTax taxOnTax = this.buidTaxonTax(taxChildCat);
				taxOnTax.setSortID(taxOnTaxSortID);
				taxOnTaxes.getTaxOnTax().add(taxOnTax);
			}

			tax.setTaxOnTaxes(taxOnTaxes);
		}
		return tax;
	}

	protected TaxOnTax buidTaxonTax(ContractPriceCategory priceCat) throws BusinessException {
		TaxOnTax taxOnTax = new TaxOnTax();
		taxOnTax.setTaxType(TaxTypeBase.fromValue(priceCat.getPriceCategory().getIata().getCode()));
		taxOnTax.setTaxCategoryCode(TaxCategoryBase.S);

		ContractPriceComponent taxPriceComponent = this.contractPriceCatService.getPriceComponent(priceCat, priceCat.getContract().getValidFrom());

		Price taxOnTaxPrice = new Price();
		taxOnTaxPrice.setPricingUnitRateType(this.getPricingUnitRate(priceCat.getPriceCategory().getPricePer()));
		taxOnTaxPrice.setPricingUnitRate(taxPriceComponent.getPrice());
		taxOnTaxPrice.setPricingCurrencyCode(CurrencyCodeBase.fromValue(taxPriceComponent.getCurrency().getCode()));
		taxOnTaxPrice.setPricingUOM(PUOMBase.fromValue(taxPriceComponent.getUnit().getIataCode()));
		taxOnTax.setPrice(taxOnTaxPrice);

		ExchangeRate taxOnTaxExchangeRate = new ExchangeRate();
		taxOnTaxExchangeRate.setFinancialSource(FinancialSource.fromValue(this.getFinancialSourceFromCode(priceCat)));
		taxOnTaxExchangeRate.setAverageingMethod(AveragingMethod.fromValue(priceCat.getAverageMethod().getCode()));
		taxOnTaxExchangeRate.setAveragingOffset(this.getOffset(priceCat.getExchangeRateOffset()));
		taxOnTax.setExchangeRate(taxOnTaxExchangeRate);
		taxOnTax.setTaxAlias(priceCat.getName());

		Restrictions restrictions = new Restrictions();
		if (priceCat.getRestriction()) {
			restrictions = this.getPriceCategoryRestrictions(priceCat);
		}
		taxOnTax.setRestrictions(restrictions);
		taxOnTax.setVATApplicability(VATApplicability.fromValue(priceCat.getVat().getCode()));
		return taxOnTax;
	}

	private String getFinancialSourceFromCode(ContractPriceCategory priceCat) {
		String financialSourceCode = priceCat.getFinancialSource().getCode();
		return "INTERNAL".equals(financialSourceCode) ? "INT" : financialSourceCode;
	}

	protected ContractPriceCategory getContractPriceCategory(Contract contract, PricingMethod method) {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put(PRICE_CATEGORY, this.priceCatService.findByName(method.getName()));
			params.put(CONTRACT, contract);
			return this.contractPriceCatService.findEntityByAttributes(params);
		} catch (ApplicationException e) {
			LOGGER.info("Could not find Contract Price Category for Contract " + contract.getCode() + " and Pricing Method " + method.getName(), e);
			return null;
		}
	}

	private BigDecimal getShipToVolume(ShipTo shipTo) {
		BigDecimal volume = BigDecimal.ZERO;
		if (FuelTenderBid.class.equals(this.iataClass)) {
			volume = shipTo.getOfferedVolume();
		}
		if (FuelTenderAcceptAward.class.equals(this.iataClass)) {
			volume = shipTo.getAllocatedVolume();
		}
		return volume;
	}

	private BigDecimal getContractVolume(Contract contract) {
		BigDecimal volume = BigDecimal.ZERO;
		if (FuelTenderBid.class.equals(this.iataClass)) {
			volume = contract.getOfferedVolume();
		}
		if (FuelTenderAcceptAward.class.equals(this.iataClass)) {
			volume = contract.getAwardedVolume();
		}
		return volume;
	}

	protected String setMessageType(ExtInterfaceNames interfaceName) {
		String type = null;
		switch (interfaceName) {
		case FUEL_TENDER_INVITATION_INTERFACE:
			type = MessageType.FUEL_TENDER_INVITATION.getType();
			break;
		case FUEL_TENDER_UPDATE_INTERFACE:
			type = MessageType.FUEL_TENDER_UPDATE.getType();
			break;
		case FUEL_TENDER_BID_AWARD_INTERFACE:
			type = MessageType.FUEL_TENDER_BID_AWARD.getType();
			break;
		case FUEL_TENDER_BID_DECLINE_INTERFACE:
			type = MessageType.FUEL_TENDER_BID_DECLINE.getType();
			break;
		case FUEL_TENDER_CANCEL_INTERFACE:
			type = MessageType.FUEL_TENDER_CANCEL.getType();
			break;
		case FUEL_TENDER_NEW_ROUND_INTERFACE:
			type = MessageType.FUEL_TENDER_NEW_ROUND.getType();
			break;
		default:
			break;
		}
		return type;
	}

	protected PUOMBase getProductUOM(String code) throws BusinessException {
		switch (code) {
		case "BL":
			return PUOMBase.BBL;
		case "CN":
			return PUOMBase.CAN;
		case "TO":
			return PUOMBase.MT;
		case "%":
			return PUOMBase.PCT;
		case "UG":
			return PUOMBase.USG;
		default:
			return PUOMBase.fromValue(code);
		}
	}

	protected RateTypeBase getPricingUnitRate(PriceInd ind) {
		switch (ind) {
		case _VOLUME_:
		case _COMPOSITE_:
			return RateTypeBase.UR;
		case _EVENT_:
			return RateTypeBase.FF;
		case _PERCENT_:
			return RateTypeBase.P;
		default:
			return null;
		}
	}

	protected PriceInd getPriceInd(RateTypeBase rtb) {
		switch (rtb) {
		case UR:
			return PriceInd._VOLUME_;
		case FF:
			return PriceInd._EVENT_;
		case P:
			return PriceInd._PERCENT_;
		default:
			return null;
		}
	}

	protected String getOffset(MasterAgreementsPeriod period) {
		switch (period) {
		case _EMPTY_:
		case _CURRENT_:
		case _CURRENT__1_:
			return "N+" + period.getDelta();
		default:
			return "N" + period.getDelta();
		}
	}
}
