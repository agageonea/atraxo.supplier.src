package atraxo.cmm.business.ext.prices.delegate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import atraxo.cmm.business.api.ext.prices.builder.IComponentPriceBuilder;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.api.prices.IContractPriceComponentConvService;
import atraxo.cmm.business.api.prices.IContractPriceComponentService;
import atraxo.cmm.business.ext.contracts.delegate.PricingBase_Bd;
import atraxo.cmm.business.ext.prices.service.PriceConverterService;
import atraxo.cmm.business.ext.prices.service.builder.ComponentPriceBuilder;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.fmbas.business.ext.sys.delegate.SystemParameterDelegate;
import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

/**
 * @author apetho
 */
public class ContractPriceComponentConv_Bd extends AbstractBusinessDelegate {

	private static final String SEPARATOR = "/";

	/**
	 * Delete the converted contract price component if the parent contract price component is deleted.
	 *
	 * @param ids - {@link Object} - the deleted contract price component id.
	 * @throws BusinessException
	 */
	public void deleteConvertedPriceComponent(ContractPriceComponent e) throws BusinessException {
		IContractPriceComponentConvService srv = (IContractPriceComponentConvService) this.findEntityService(ContractPriceComponentConv.class);
		List<Object> ids = new ArrayList<>();
		for (ContractPriceComponentConv converted : e.getConvertedPrices()) {
			ids.add(converted.getId());
		}
		srv.deleteByIds(ids);
	}

	/**
	 * Delete and recreate the {@link ContractPriceComponentConv} object.
	 *
	 * @param e - {@link ContractPriceComponent}
	 * @throws BusinessException
	 */
	public void updateConvertedPriceComponent(ContractPriceComponent e) throws BusinessException {
		this.updateConvertedPriceComponent(e, false);
	}

	/**
	 * Delete and recreate the {@link ContractPriceComponentConv} object.
	 *
	 * @param e - {@link ContractPriceComponent}
	 * @throws BusinessException
	 */
	public void updateConvertedPriceComponent(ContractPriceComponent e, boolean persist) throws BusinessException {
		this.deleteConvertedPriceComponent(e);
		e.setConvertedPrices(null);
		this.insertContractPriceComponentConv(e, persist);
	}

	/**
	 * Get the {@link ContractPriceComponent} list and update the converted values.
	 *
	 * @param e - {@link ContractPriceCategory}.
	 * @throws BusinessException
	 */
	public void updateConvertedPriceComponent(ContractPriceCategory e) throws BusinessException {
		Collection<ContractPriceComponent> components = e.getPriceComponents();
		for (ContractPriceComponent c : components) {
			this.updateConvertedPriceComponent(c);
		}
	}

	/**
	 * Get the {@link Contract} and update the converted values.
	 *
	 * @param c - {@link Contract}.
	 * @throws BusinessException
	 */
	public void updateConvertedPriceComponent(Contract c) throws BusinessException {
		IContractPriceCategoryService contractPriceServiceService = (IContractPriceCategoryService) this
				.findEntityService(ContractPriceCategory.class);
		IContractPriceComponentService cntrPriceCmpntService = (IContractPriceComponentService) this.findEntityService(ContractPriceComponent.class);
		IComponentPriceBuilder priceBuilderService = this.getApplicationContext().getBean(ComponentPriceBuilder.class);
		List<ContractPriceCategory> cpcList = contractPriceServiceService.findByContract(c);
		for (ContractPriceCategory cpc : cpcList) {
			List<ContractPriceComponent> components = cntrPriceCmpntService.findByContrPriceCtgry(cpc);
			for (ContractPriceComponent e : components) {
				priceBuilderService.createConvertedPrices(e);
			}
		}
	}

	/**
	 * Generate and insert a new {@link ContractPriceComponentConv}.
	 *
	 * @param e - {@link ContractPriceComponent}.
	 * @throws BusinessException
	 */
	public void insertContractPriceComponentConv(ContractPriceComponent e, boolean shouldInsert) throws BusinessException {
		IComponentPriceBuilder priceBuilder = this.getApplicationContext().getBean(ComponentPriceBuilder.class);
		priceBuilder.createConvertedPrices(e);
		if (e.getConvertedPrices() != null && shouldInsert) {
			List<ContractPriceComponentConv> list = new ArrayList<>(e.getConvertedPrices());
			IContractPriceComponentConvService srv = (IContractPriceComponentConvService) this.findEntityService(ContractPriceComponentConv.class);
			srv.insert(list);
		}
	}

	/**
	 * Generate and insert a new {@link ContractPriceComponentConv}.
	 *
	 * @param e - {@link ContractPriceComponent}.
	 * @throws BusinessException
	 */
	public void insertContractPriceComponentConv(ContractPriceComponent e) throws BusinessException {
		this.insertContractPriceComponentConv(e, false);
	}

	/**
	 * Generate a new {@link ContractPriceComponentConv} from the {@link ContractPriceComponent}.
	 *
	 * @param e - {@link ContractPriceComponent}.
	 * @return - the generated {@link ContractPriceComponentConv}
	 * @throws BusinessException
	 */
	public ContractPriceComponentConv generateConvertedPriceComponent(ContractPriceComponent e) throws BusinessException {
		Density density = this.getBusinessDelegate(SystemParameterDelegate.class).getSystemDensity();
		return this.generateConvertedPriceComponent(e, e.getValidFrom(), e.getValidTo(), density);
	}

	/**
	 * Generate a new {@link ContractPriceComponentConv} from the {@link ContractPriceComponent}.
	 *
	 * @param e - {@link ContractPriceComponent}.
	 * @param validFrom - {@link Date}.
	 * @param validTo - {@link Date}.
	 * @return - the generated {@link ContractPriceComponentConv}
	 * @throws BusinessException
	 */
	private ContractPriceComponentConv generateConvertedPriceComponent(ContractPriceComponent e, Date validFrom, Date validTo, Density density)
			throws BusinessException {
		Date date = this.getExchangeRateDate(validFrom, validTo);
		Contract c = e.getContrPriceCtgry().getContract();
		ConversionResult convResult = this.getCalculatedValueForConverted(e, c, date, density);
		ContractPriceComponentConv newConvComp = new ContractPriceComponentConv();
		newConvComp.setValidFrom(validFrom);
		newConvComp.setValidTo(validTo);
		newConvComp.setPrice(e.getPrice());
		newConvComp.setCurrencyUnitCd(e.getCurrency().getCode() + SEPARATOR + e.getUnit().getCode());
		newConvComp.setEquivalent(convResult.getValue());
		newConvComp.setEquivalentCurrency(c.getSettlementCurr());
		newConvComp.setEquivalentUnit(c.getSettlementUnit());
		if (e.getUnit().getUnittypeInd().equals(UnitType._EVENT_)) {
			newConvComp.setEquivalentUnit(e.getUnit());
		}
		newConvComp.setExchangeRate(convResult.getFactor());
		newConvComp.setWhldPay(e.getWithHold());
		newConvComp.setProvisional(e.getProvisional());
		newConvComp.setContractPriceComponent(e);
		newConvComp.setExchangeRateCurrnecyCDS(e.getCurrency().getCode() + SEPARATOR + c.getSettlementCurr().getCode());
		newConvComp.setOfDate(date);
		e.addToConvertedPrices(newConvComp);
		return newConvComp;
	}

	/**
	 * Calculate the converted price and the conversion factor.
	 *
	 * @param e - original {@link ContractPriceComponent}.
	 * @param c - {@link Contract}.
	 * @param date - {@link Date}.
	 * @return - the calculated value {@link BigDecimal}.
	 * @throws BusinessException
	 */
	private ConversionResult getCalculatedValueForConverted(ContractPriceComponent e, Contract c, Date date, Density density)
			throws BusinessException {
		PriceConverterService priceConverterService = this.getApplicationContext().getBean(PriceConverterService.class);
		Unit toUnit = c.getSettlementUnit();
		Unit fromUnit = e.getUnit();
		if (fromUnit.getUnittypeInd().equals(UnitType._EVENT_)) {
			toUnit = fromUnit;
		}
		BigDecimal value = e.getPrice();

		Currencies fromCurrency = e.getCurrency();
		Currencies toCurrency = c.getSettlementCurr();
		AverageMethod am = e.getContrPriceCtgry().getAverageMethod();
		FinancialSources fs = e.getContrPriceCtgry().getFinancialSource();
		MasterAgreementsPeriod offset = e.getContrPriceCtgry().getExchangeRateOffset();
		boolean strict = false;
		if (this.isIndex(e)) {
			return priceConverterService.convert(fromUnit, toUnit, fromCurrency, toCurrency, value, date, fs, am,
					e.getContrPriceCtgry().getPricingBases(), strict, offset);
		}
		return priceConverterService.convert(fromUnit, toUnit, fromCurrency, toCurrency, value, date, fs, am, density, strict, offset);
	}

	private boolean isIndex(ContractPriceComponent e) {
		return e.getContrPriceCtgry().getPricingBases() != null
				&& PricingBase_Bd.INDEX.equalsIgnoreCase(e.getContrPriceCtgry().getPricingBases().getPriceCat().getName());
	}

	/**
	 * Calculate from the contract price component's valid from field the exchange rate date.
	 *
	 * @param e - {@link ContractPriceComponent}.
	 * @return - the date to calculate the {@link ExchangeRateValue}.
	 */
	private Date getExchangeRateDate(Date validFrom, Date validTo) {
		Date date = GregorianCalendar.getInstance().getTime();
		if (!this.isBetweenDates(validFrom, validTo, date)) {
			if (validTo.before(date)) {
				date = validTo;
			} else if (validFrom.after(date)) {
				date = validFrom;
			}
		}
		return date;
	}

	/**
	 * Check if the date is between the given dates.
	 *
	 * @param from - start of the period.
	 * @param to - period's end.
	 * @param date - the date which must be checked.
	 * @return True if date is between from and to. The period margins are not included.
	 */
	public boolean isBetweenDates(Date from, Date to, Date date) {
		return date.compareTo(from) > -1 && date.compareTo(to) < 1;
	}

	/**
	 * Calculate the last day of month.
	 *
	 * @param date - {@link Date}.
	 * @param quantity - {@link Integer}.
	 * @return - {@link} last day of month.
	 */
	public Date calculateDateWithOffset(Date date, Integer quantity, Integer dateUnit) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(dateUnit, quantity);
		return cal.getTime();
	}

}
