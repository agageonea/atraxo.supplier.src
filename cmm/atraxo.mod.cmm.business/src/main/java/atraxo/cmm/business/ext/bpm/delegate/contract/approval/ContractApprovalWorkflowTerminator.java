package atraxo.cmm.business.ext.bpm.delegate.contract.approval;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.custom.workflow.IWorkflowTerminatorService;
import atraxo.fmbas.business.ext.bpm.WorkflowMsgConstants;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

public class ContractApprovalWorkflowTerminator extends AbstractBusinessBaseService implements IWorkflowTerminatorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContractApprovalWorkflowTerminator.class);

	@Autowired
	private IContractService contractSrv;

	@Override
	public void terminateWorkflow(WorkflowInstance workflowInstance, String reason) throws BusinessException {

		List<WorkflowInstanceEntity> entities = new ArrayList<>(workflowInstance.getEntitites());

		for (WorkflowInstanceEntity entity : entities) {
			// reject the contract
			Contract contract = null;
			try {
				contract = this.contractSrv.findByBusiness(entity.getObjectId());
			} catch (Exception e) {
				LOGGER.warn("WARNING: could not retrieve a contract for ID " + entity.getObjectId()
						+ " ! Will not update the entity since it doesn't exist !", e);
			}
			if (contract != null) {
				this.contractSrv.reject(contract, WorkflowMsgConstants.WKF_RESULT_REJECTED,
						StringUtils.isEmpty(reason) ? WorkflowMsgConstants.APPROVAL_WORKFLOW_TERMINATED_COMMENT : reason);

			}
		}
	}
}
