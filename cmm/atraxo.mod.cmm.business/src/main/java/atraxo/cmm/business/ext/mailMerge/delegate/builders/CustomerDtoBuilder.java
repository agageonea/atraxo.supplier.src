package atraxo.cmm.business.ext.mailMerge.delegate.builders;

import atraxo.cmm.domain.ext.mailmerge.dto.contract.CustomerDto;
import atraxo.fmbas.domain.impl.customer.Customer;

/**
 * @author apetho
 */
public final class CustomerDtoBuilder {

	private CustomerDtoBuilder() {

	}

	public static CustomerDto buildCustomer(Customer customer) {
		CustomerDto dto = new CustomerDto();
		if (customer != null) {
			dto.setCode(customer.getCode());
			dto.setName(customer.getName());
			if (customer.getStatus() != null) {
				dto.setStatus(customer.getStatus().getName());
			}
			if (customer.getType() != null) {
				dto.setType(customer.getType().getName());
			}
			dto.setEmail(customer.getEmail());
			dto.setPhoneNo(customer.getPhoneNo());
			dto.setPhoneNo(customer.getPhoneNo());
			dto.setFaxNo(customer.getFaxNo());
			dto.setWebsite(customer.getWebsite());
			if (customer.getNatureOfBusiness() != null) {
				dto.setNatureOfBusiness(customer.getNatureOfBusiness().getName());
			}
			dto.setAoc(customer.getAoc());
			dto.setAocValidFrom(customer.getAocValidFrom());
			dto.setAocValidTo(customer.getAocValidTo());
			dto.setTradeLicense(customer.getTradeLicense());
			dto.setTradeLicenseValidFrom(customer.getTlValidFrom());
			dto.setTradeLicenseValidTo(customer.getTlValidTo());
			dto.setVatRegNumber(customer.getVatRegNumber());
			dto.setTaxIdNumber(customer.getTaxIdNumber());
			dto.setIncomeTaxNumber(customer.getIncomeTaxNumber());
			dto.setAccountNumber(customer.getAccountNumber());
			dto.setIataCode(customer.getIataCode());
			dto.setIcaoCode(customer.getIcaoCode());
			if (customer.getBusiness() != null) {
				dto.setBusinessType(customer.getBusiness().getName());
			}
		}
		return dto;
	}

}
