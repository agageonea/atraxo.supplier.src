package atraxo.cmm.business.ext.mailMerge.delegate.builders;

import atraxo.cmm.domain.ext.mailmerge.dto.contract.RestrictionDto;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;

/**
 * @author apetho
 */
public final class RestricionDtoBuilder {

	private RestricionDtoBuilder() {

	}

	public static final RestrictionDto buildRestriction(ContractPriceRestriction contractPriceRestriction) {
		RestrictionDto dto = new RestrictionDto();
		if (contractPriceRestriction != null) {
			dto.setOperator(contractPriceRestriction.getOperator().getName());
			dto.setType(contractPriceRestriction.getRestrictionType().getName());
			dto.setUnit(contractPriceRestriction.getUnit() != null ? contractPriceRestriction.getUnit().getCode() : "");
			dto.setValue(contractPriceRestriction.getValue());
		}
		return dto;
	}

}
