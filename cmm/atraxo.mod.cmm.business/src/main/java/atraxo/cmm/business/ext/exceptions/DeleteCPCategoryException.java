package atraxo.cmm.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;

public class DeleteCPCategoryException extends BusinessException {

	public DeleteCPCategoryException() {
		super(CmmErrorCode.DEL_CPCATEGORY, CmmErrorCode.DEL_CPCATEGORY.getErrMsg());
	}

}
