package atraxo.cmm.business.ext.aop.contracts;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.contracts.IPricingBaseService;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.api.prices.IContractPriceComponentService;
import atraxo.cmm.business.api.prices.IContractPriceRestrictionService;
import atraxo.cmm.business.api.shipTo.IShipToService;
import atraxo.cmm.business.ext.contracts.service.propagator.ChildEntityPropagatorService;
import atraxo.cmm.business.ext.contracts.service.propagator.ContractPropagatorService;
import atraxo.cmm.business.ext.exceptions.SiblingContractNotFoundException;
import atraxo.cmm.business.ext.prices.service.ContractPriceCategory_Service;
import atraxo.cmm.business.ext.prices.service.ContractPriceComponent_Service;
import atraxo.cmm.business.ext.prices.service.ContractPriceRestriction_Service;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.commons.descriptor.AsgnContext;

/**
 * Aspect to synchronize purchase contract with corresponding sale contract.
 *
 * @author zspeter
 */
@Aspect
@Order(Ordered.LOWEST_PRECEDENCE)
public class PropagatePurchaseContractAspect {

	private static final String MSG_3 = "Sell Contract code: ";
	private static final String MSG_2 = "Mark contract aspect when inserting contract price subcomponents";
	private static final String MSG_1 = "Purchase Contract code: ";
	private static final String MSG = "Propagate purchase contract aspect when updating from contract price subcomponents";
	private static final Logger LOG = LoggerFactory.getLogger(PropagatePurchaseContractAspect.class);

	private static final ThreadLocal<Boolean> marker = new ThreadLocal<>();

	@Autowired
	private IContractService contSrv;
	@Autowired
	private ChildEntityPropagatorService childPropagator;
	@Autowired
	private ContractPropagatorService propagatorService;
	@Autowired
	private IContractPriceCategoryService catSrv;
	@Autowired
	private IPricingBaseService pricingBaseService;
	@Autowired
	private IShipToService shipToService;
	@Autowired
	private IContractPriceComponentService contractPriceComponentService;
	@Autowired
	private IContractPriceRestrictionService contractPriceRestrictionService;

	/**
	 * Capture any update method from: <ul> <li>ContractPriceCategory_Service</li> <li>ContractPriceComponent_Service</li>
	 * <li>ContractPriceRestriction_Service</li> <li>ContractPriceComponentConv_Service</li> </ul>
	 *
	 * @param pjp
	 * @throws BusinessException
	 */
	@Around("execution(* *.update(..)) && bean(ContractPrice*)")
	public void propagateContractChildsUpdate(ProceedingJoinPoint pjp) throws Throwable {
		if (LOG.isDebugEnabled()) {
			LOG.debug(PropagatePurchaseContractAspect.MSG);
		}
		boolean propagated = false;
		if (PropagatePurchaseContractAspect.marker.get() != null && PropagatePurchaseContractAspect.marker.get()) {
			propagated = true;
		}
		PropagatePurchaseContractAspect.marker.set(true);
		try {
			pjp.proceed();
			if (!propagated) {
				this.processPropagate(pjp);
			}
		} finally {
			if (PropagatePurchaseContractAspect.marker.get() != null && !propagated) {
				PropagatePurchaseContractAspect.marker.remove();
			}

		}
	}

	/**
	 * @param jp
	 * @throws BusinessException
	 */
	@Around("(execution(* *.update(..)) || execution(* *.submitForApproval(..)) || execution(* *.approve(..)) || execution(* *.reject(..))) && bean(Contract)")
	public void propagateContractUpdate(ProceedingJoinPoint jp) throws Throwable {
		boolean propagated = false;
		if (PropagatePurchaseContractAspect.marker.get() != null && PropagatePurchaseContractAspect.marker.get()) {
			propagated = true;
		}
		PropagatePurchaseContractAspect.marker.set(true);
		if (LOG.isDebugEnabled()) {
			LOG.debug(PropagatePurchaseContractAspect.MSG);
		}
		try {
			jp.proceed();
			if (!propagated) {
				this.processPropagate(jp);
			}
		} finally {
			if (PropagatePurchaseContractAspect.marker.get() != null && !propagated) {
				PropagatePurchaseContractAspect.marker.remove();
			}
		}
	}

	/**
	 * Capture any update on PricingBase_Service
	 *
	 * @param jp
	 * @throws BusinessException
	 */
	@Around("execution(* *.update(..)) && bean(PricingBase)")
	public void propagateContractPBUpdate(ProceedingJoinPoint jp) throws Throwable {
		boolean propagated = false;
		if (PropagatePurchaseContractAspect.marker.get() != null && PropagatePurchaseContractAspect.marker.get()) {
			propagated = true;
		}
		PropagatePurchaseContractAspect.marker.set(true);
		LOG.info("entering aspect");
		if (LOG.isDebugEnabled()) {
			LOG.debug("Propagate contract aspect on pricing base update");
		}
		try {
			PricingBase pb = null;
			PricingBase sibling = null;
			if (!propagated) {
				pb = this.getPricingBase(jp);
			}
			jp.proceed();
			if (!propagated) {
				sibling = this.getSiblingPb(pb);
				if (sibling != null) {
					this.propagatePbUpdate(pb, sibling);
				}
			}
		} finally {
			if (PropagatePurchaseContractAspect.marker.get() != null && !propagated) {
				PropagatePurchaseContractAspect.marker.remove();
			}
		}
	}

	/**
	 * Capture any update on ShipTo_Service
	 *
	 * @param jp
	 * @throws BusinessException
	 */
	@Around("execution(* *.update(..)) && bean(ShipTo)")
	public void propagateContractShipToUpdate(ProceedingJoinPoint jp) throws Throwable {
		boolean propagated = false;
		if (PropagatePurchaseContractAspect.marker.get() != null && PropagatePurchaseContractAspect.marker.get()) {
			propagated = true;
		}
		PropagatePurchaseContractAspect.marker.set(true);
		LOG.info("entering aspect");
		if (LOG.isDebugEnabled()) {
			LOG.debug("Propagate contract aspect on ShipTo update");
		}
		try {
			jp.proceed();
			if (!propagated) {
				this.processPropagate(jp);
			}
		} finally {
			if (PropagatePurchaseContractAspect.marker.get() != null && !propagated) {
				PropagatePurchaseContractAspect.marker.remove();
			}
		}
	}

	private void propagatePbUpdate(PricingBase pb, PricingBase sibling) throws BusinessException {
		this.childPropagator.propagatePbUpdate(pb, sibling);
		this.pricingBaseService.updatePricingBase(sibling);

	}

	private PricingBase getPricingBase(ProceedingJoinPoint jp) {
		for (Object obj : jp.getArgs()) {
			if (obj instanceof List) {
				List<?> list = (List<?>) obj;
				for (Object e : list) {
					if (e instanceof PricingBase) {
						return (PricingBase) e;
					}
				}
			} else if (obj instanceof PricingBase) {
				return (PricingBase) obj;
			}
		}
		return null;
	}

	private PricingBase getSiblingPb(PricingBase pb) throws BusinessException {
		ContractPriceCategory cat = pb.getContractPriceCategories().iterator().next();
		ContractPriceCategory siblingCat = this.catSrv.findByResaleRef(cat);
		if (siblingCat != null) {
			return siblingCat.getPricingBases();
		}
		return null;
	}

	/**
	 * @param jp
	 * @throws BusinessException
	 */
	@Around("execution(* *.doSave(..)) && bean(defaultAsgnTxService*)")
	public void propagateAssign(ProceedingJoinPoint jp) throws Throwable {
		boolean propagated = false;
		if (PropagatePurchaseContractAspect.marker.get() != null && PropagatePurchaseContractAspect.marker.get()) {
			propagated = true;
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug(PropagatePurchaseContractAspect.MSG);
		}
		try {
			jp.proceed();
			if (!propagated) {
				this.internalAssignPropagate(jp);
			}
		} finally {
			if (PropagatePurchaseContractAspect.marker.get() != null && !propagated) {
				PropagatePurchaseContractAspect.marker.remove();
			}
		}
	}

	/**
	 * @param jp
	 * @throws BusinessException
	 */
	@Around("execution(* *.insert(..)) && bean(ContractPriceComponent)")
	public void propagatePriceComponentInsert(ProceedingJoinPoint jp) throws Throwable {
		boolean propagated = false;
		if (PropagatePurchaseContractAspect.marker.get() != null && PropagatePurchaseContractAspect.marker.get()) {
			propagated = true;
		}
		PropagatePurchaseContractAspect.marker.set(true);
		if (LOG.isDebugEnabled()) {
			LOG.debug(PropagatePurchaseContractAspect.MSG_2);
		}
		try {
			jp.proceed();
			if (!propagated) {
				this.propagateNewPriceComponent(jp);
			}
		} finally {
			if (PropagatePurchaseContractAspect.marker.get() != null && !propagated) {
				PropagatePurchaseContractAspect.marker.remove();
			}
		}
	}

	/**
	 * @param jp
	 * @throws BusinessException
	 */
	@Around("execution(* *.insert(..)) && bean(ContractPriceRestriction*)")
	public void propagateRestrictionInsert(ProceedingJoinPoint jp) throws Throwable {
		boolean propagated = false;
		if (PropagatePurchaseContractAspect.marker.get() != null && PropagatePurchaseContractAspect.marker.get()) {
			propagated = true;
		}
		PropagatePurchaseContractAspect.marker.set(true);
		if (LOG.isDebugEnabled()) {
			LOG.debug(PropagatePurchaseContractAspect.MSG_2);
		}
		try {
			jp.proceed();
			if (!propagated) {
				this.propagateNewPriceRestriction(jp);
			}
		} finally {
			if (PropagatePurchaseContractAspect.marker.get() != null && !propagated) {
				PropagatePurchaseContractAspect.marker.remove();
			}
		}
	}

	/**
	 * @param jp
	 * @throws BusinessException
	 */
	@Around("execution(* *.insert(..)) && bean(PricingBase*)")
	public void propagatePricingBaseInsert(ProceedingJoinPoint jp) throws Throwable {
		boolean propagated = false;
		if (PropagatePurchaseContractAspect.marker.get() != null && PropagatePurchaseContractAspect.marker.get()) {
			propagated = true;
		}
		PropagatePurchaseContractAspect.marker.set(true);
		if (LOG.isDebugEnabled()) {
			LOG.debug(PropagatePurchaseContractAspect.MSG_2);
		}
		try {
			jp.proceed();
			if (!propagated) {
				this.propagateNewPricingBases(jp);
			}
		} finally {
			if (PropagatePurchaseContractAspect.marker.get() != null && !propagated) {
				PropagatePurchaseContractAspect.marker.remove();
			}

		}
	}

	/**
	 * @param jp
	 * @throws BusinessException
	 */
	@Around("execution(* *.insert(..)) && bean(ShipTo*)")
	public void propagateShipToInsert(ProceedingJoinPoint jp) throws Throwable {
		boolean propagated = false;
		if (PropagatePurchaseContractAspect.marker.get() != null && PropagatePurchaseContractAspect.marker.get()) {
			propagated = true;
		}
		PropagatePurchaseContractAspect.marker.set(true);
		if (LOG.isDebugEnabled()) {
			LOG.debug(PropagatePurchaseContractAspect.MSG_2);
		}
		try {
			jp.proceed();
			if (!propagated) {
				this.propagateNewShipTos(jp);
			}
		} finally {
			if (PropagatePurchaseContractAspect.marker.get() != null && !propagated) {
				PropagatePurchaseContractAspect.marker.remove();
			}
		}
	}

	/**
	 * @param pjp
	 * @throws Throwable
	 */
	@Around("execution(* *.deleteByIds(..)) && bean(PricingBase*)")
	public void propagateContractDeleteFromPb(ProceedingJoinPoint pjp) throws Throwable {
		boolean propagated = false;
		if (PropagatePurchaseContractAspect.marker.get() != null && PropagatePurchaseContractAspect.marker.get()) {
			propagated = true;
		}
		PropagatePurchaseContractAspect.marker.set(true);
		if (LOG.isDebugEnabled()) {
			LOG.debug(PropagatePurchaseContractAspect.MSG);
		}
		try {
			List<Integer> entityIdList = this.getId(pjp);
			List<Object> contractids = this.buildContractIdList(entityIdList, PricingBase.class);

			pjp.proceed();
			if (!propagated) {
				this.processPropagate(contractids, entityIdList, PricingBase.class);
			}
		} finally {
			if (PropagatePurchaseContractAspect.marker.get() != null && !propagated) {
				PropagatePurchaseContractAspect.marker.remove();
			}
		}
	}

	/**
	 * @param pjp
	 * @throws Throwable
	 */
	@Around("execution(* *.deleteByIds(..)) && bean(ContractPrice*)")
	public void propagateContractDeleteFromPriceCategory(ProceedingJoinPoint pjp) throws Throwable {
		boolean propagated = false;
		if (PropagatePurchaseContractAspect.marker.get() != null && PropagatePurchaseContractAspect.marker.get()) {
			propagated = true;
		}
		PropagatePurchaseContractAspect.marker.set(true);
		if (LOG.isDebugEnabled()) {
			LOG.debug(PropagatePurchaseContractAspect.MSG);
		}

		try {
			if (!propagated) {
				Class<? extends AbstractEntity> clazz = null;
				if (pjp.getTarget() instanceof ContractPriceCategory_Service) {
					clazz = ContractPriceCategory.class;
				} else if (pjp.getTarget() instanceof ContractPriceComponent_Service) {
					clazz = ContractPriceComponent.class;
				} else if (pjp.getTarget() instanceof ContractPriceRestriction_Service) {
					clazz = ContractPriceRestriction.class;
				} else {
					LOG.info("Invalid class: " + pjp.getTarget().getClass().getName());
				}
				if (clazz != null) {
					List<Integer> entityIdList = this.getId(pjp);
					List<Object> contractids = this.buildContractIdList(entityIdList, clazz);
					pjp.proceed();
					this.processPropagate(contractids, entityIdList, clazz);
				} else {
					pjp.proceed();
				}
			} else {
				pjp.proceed();
			}
		} finally {
			if (PropagatePurchaseContractAspect.marker.get() != null && !propagated) {
				PropagatePurchaseContractAspect.marker.remove();
			}
		}
	}

	/**
	 * @param pjp
	 * @throws Throwable
	 */
	@Around("execution(* *.deleteByIds(..)) && bean(ShipTo*)")
	public void propagateContractDeleteFromShipTo(ProceedingJoinPoint pjp) throws Throwable {
		boolean propagated = false;
		if (PropagatePurchaseContractAspect.marker.get() != null && PropagatePurchaseContractAspect.marker.get()) {
			propagated = true;
		}
		PropagatePurchaseContractAspect.marker.set(true);
		if (LOG.isDebugEnabled()) {
			LOG.debug(PropagatePurchaseContractAspect.MSG);
		}
		try {
			List<Integer> entityIdList = this.getId(pjp);
			List<Object> contractids = this.buildContractIdList(entityIdList, ShipTo.class);
			pjp.proceed();
			if (!propagated) {
				this.processPropagate(contractids, entityIdList, ShipTo.class);
			}
		} finally {
			if (PropagatePurchaseContractAspect.marker.get() != null && !propagated) {
				PropagatePurchaseContractAspect.marker.remove();
			}
		}
	}

	/**
	 * @param pjp
	 * @throws Throwable
	 */
	@Around("execution(* *.deleteByIds(..)) && bean(Contract)")
	public void propagateContractDelete(ProceedingJoinPoint pjp) throws Throwable {
		boolean propagated = false;
		if (PropagatePurchaseContractAspect.marker.get() != null && PropagatePurchaseContractAspect.marker.get()) {
			propagated = true;
		}
		PropagatePurchaseContractAspect.marker.set(true);
		if (LOG.isDebugEnabled()) {
			LOG.debug(PropagatePurchaseContractAspect.MSG);
		}
		try {
			List<Integer> contractIdList = this.getId(pjp);
			List<Contract> contracts = this.contSrv.findByIds(this.buildContractIdList(contractIdList, Contract.class));
			List<Object> contractsIdsToModify = new ArrayList<>();
			for (Contract c : contracts) {
				try {
					Contract temp = this.contSrv.findSiblingContract(c);
					contractsIdsToModify.add(temp.getId());
				} catch (SiblingContractNotFoundException e) {
					LOG.info("Sibling contract not found.", e);
				}
			}
			pjp.proceed();
			if (!propagated && !contractsIdsToModify.isEmpty()) {
				this.contSrv.deleteByIds(contractsIdsToModify);
			}
		} finally {
			if (PropagatePurchaseContractAspect.marker.get() != null && !propagated) {
				PropagatePurchaseContractAspect.marker.remove();
			}
		}
	}

	private void processPropagate(List<Object> contractIds, List<Integer> chlidIds, Class<? extends AbstractEntity> clazz) throws BusinessException {
		if (CollectionUtils.isEmpty(contractIds)) {
			return;
		}
		List<Contract> contracts = this.contSrv.findByIds(contractIds);
		for (Contract contract : contracts) {
			try {
				LOG.debug(PropagatePurchaseContractAspect.MSG_1 + contract.getCode());
				this.removeChildElements(contract, chlidIds, clazz);
				Contract sibling = this.contSrv.findSiblingContract(contract);
				LOG.debug(MSG_3 + sibling.getCode());
				this.propagatorService.propagateChanges(contract, sibling);
				this.contSrv.persistChanges(sibling);
			} catch (SiblingContractNotFoundException e) {
				if (LOG.isDebugEnabled()) {
					LOG.debug(e.getMessage(), e);
				}
			}
		}
	}

	/**
	 * @param chlidIds
	 * @param clazz
	 */
	private void removeChildElements(Contract c, List<Integer> chlidIds, Class<? extends AbstractEntity> clazz) {
		switch (clazz.getSimpleName()) {
		case "ShipTo":
			this.removeElements(chlidIds, c.getShipTo());
			break;
		case "PricingBase":
			this.removeElements(chlidIds, c.getPricingBases());
			break;
		case "ContractPriceCategory":
			this.removeElements(chlidIds, c.getPriceCategories());
			break;
		case "ContractPriceComponent":
			for (ContractPriceCategory cpc : c.getPriceCategories()) {
				this.removeElements(chlidIds, cpc.getPriceComponents());
			}
			break;
		case "ContractPriceRestriction":
			for (ContractPriceCategory cpc : c.getPriceCategories()) {
				this.removeElements(chlidIds, cpc.getPriceRestrictions());
			}
			break;
		default:
			break;
		}

	}

	private <E extends AbstractEntity> void removeElements(List<Integer> chlidIds, Collection<E> list) {
		for (Iterator<E> iter = list.iterator(); iter.hasNext();) {
			E shipTo = iter.next();
			if (chlidIds.contains(shipTo.getId())) {
				iter.remove();
			}
		}
	}

	private void processPropagate(ProceedingJoinPoint jp) throws BusinessException {
		Contract c = this.getContract(jp);
		if (!this.isPurchaseContract(c)) {
			return;
		}
		try {
			Contract temp = this.contSrv.findByBusiness(c.getId());
			LOG.debug(PropagatePurchaseContractAspect.MSG_1 + temp.getCode());
			Contract sibling = this.contSrv.findSiblingContract(temp);
			LOG.debug(MSG_3 + sibling.getCode());

			this.propagatorService.propagateChanges(temp, sibling);
			this.contSrv.persistChanges(sibling);
		} catch (SiblingContractNotFoundException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
		}
	}

	private void propagateNewShipTos(ProceedingJoinPoint jp) throws BusinessException {
		Contract c = this.getContract(jp);
		if (!this.isPurchaseContract(c)) {
			return;
		}
		try {
			Contract temp = this.contSrv.findByBusiness(c.getId());
			LOG.debug(PropagatePurchaseContractAspect.MSG_1 + temp.getCode());
			Contract sibling = this.contSrv.findSiblingContract(temp);
			LOG.debug(MSG_3 + sibling.getCode());

			List<ShipTo> list = this.childPropagator.createNewShipTos(jp.getArgs()[0], sibling);
			this.shipToService.insert(list);
		} catch (SiblingContractNotFoundException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
		}

	}

	private void propagateNewPricingBases(ProceedingJoinPoint jp) throws BusinessException {
		Contract c = this.getContract(jp);
		if (!this.isPurchaseContract(c)) {
			return;
		}
		try {
			Contract temp = this.contSrv.findByBusiness(c.getId());
			LOG.debug(PropagatePurchaseContractAspect.MSG_1 + temp.getCode());
			Contract sibling = this.contSrv.findSiblingContract(temp);
			LOG.debug(MSG_3 + sibling.getCode());

			@SuppressWarnings("unchecked")
			List<PricingBase> list = (List<PricingBase>) jp.getArgs()[0];
			PricingBase pb = this.childPropagator.createPricingBase(list.get(0), sibling);
			this.pricingBaseService.insert(pb);
		} catch (SiblingContractNotFoundException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
		}
	}

	private void propagateNewPriceComponent(ProceedingJoinPoint jp) throws BusinessException {
		Contract c = this.getContract(jp);
		if (!this.isPurchaseContract(c)) {
			return;
		}
		try {
			Contract temp = this.contSrv.findByBusiness(c.getId());
			LOG.debug(PropagatePurchaseContractAspect.MSG_1 + temp.getCode());
			Contract sibling = this.contSrv.findSiblingContract(temp);
			LOG.debug(MSG_3 + sibling.getCode());

			ContractPriceComponent comp = this.getContractPriceComponent(jp);
			if (comp != null) {
				ContractPriceComponent cpc = this.childPropagator.createPriceComponent(comp, sibling);
				this.contractPriceComponentService.insert(cpc);
			}
		} catch (SiblingContractNotFoundException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
		}
	}

	private void propagateNewPriceRestriction(ProceedingJoinPoint jp) throws BusinessException {
		Contract c = this.getContract(jp);
		if (!this.isPurchaseContract(c)) {
			return;
		}
		try {
			Contract temp = this.contSrv.findByBusiness(c.getId());
			LOG.debug(PropagatePurchaseContractAspect.MSG_1 + temp.getCode());
			Contract sibling = this.contSrv.findSiblingContract(temp);
			LOG.debug(MSG_3 + sibling.getCode());

			@SuppressWarnings("unchecked")
			List<ContractPriceRestriction> list = (List<ContractPriceRestriction>) jp.getArgs()[0];
			List<ContractPriceRestriction> newRestrictions = this.childPropagator.createPriceRestrictions(list, sibling);
			this.contractPriceRestrictionService.insert(newRestrictions);
		} catch (SiblingContractNotFoundException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
		}
	}

	private List<Object> buildContractIdList(List<Integer> ids, Class<?> clazz) {
		List<Object> retList = new ArrayList<>();

		for (Integer id : ids) {
			if (clazz.equals(PricingBase.class)) {
				try {
					PricingBase pb = this.pricingBaseService.findByBusiness(id);
					retList.add(pb.getContract().getId());
				} catch (ApplicationException e) {
					if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(e.getErrorCode())) {
						throw e;
					}
					LOG.info("Pricing base not found.", e);
				}
			}
			if (clazz.equals(ContractPriceCategory.class)) {
				try {
					ContractPriceCategory cat = this.catSrv.findByBusiness(id);
					retList.add(cat.getContract().getId());
				} catch (ApplicationException e) {
					if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(e.getErrorCode())) {
						throw e;
					}
					LOG.info("Pricing base not found.", e);
				}
			}
			if (clazz.equals(ShipTo.class)) {
				try {
					ShipTo st = this.shipToService.findByBusiness(id);
					retList.add(st.getContract().getId());
				} catch (ApplicationException e) {
					if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(e.getErrorCode())) {
						throw e;
					}
					LOG.info("ShipTo not found.", e);
				}
			}
			if (clazz.equals(ContractPriceComponent.class)) {
				try {
					ContractPriceComponent cpc = this.contractPriceComponentService.findByBusiness(id);
					retList.add(cpc.getContrPriceCtgry().getContract().getId());
				} catch (ApplicationException e) {
					if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(e.getErrorCode())) {
						throw e;
					}
					LOG.info("Contract price component not found.", e);
				}
			}
			if (clazz.equals(ContractPriceRestriction.class)) {
				try {
					ContractPriceRestriction cpr = this.contractPriceRestrictionService.findByBusiness(id);
					retList.add(cpr.getContractPriceCategory().getContract().getId());
				} catch (ApplicationException e) {
					if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(e.getErrorCode())) {
						throw e;
					}
					LOG.info("Contract price component not found.", e);
				}
			}
			if (clazz.equals(Contract.class)) {
				retList.add(id);
			}
		}
		return retList;
	}

	private List<Integer> getId(ProceedingJoinPoint pjp) {
		List<Integer> retList = new ArrayList<>();
		for (Object obj : pjp.getArgs()) {
			if (obj instanceof List) {
				List<?> list = (List<?>) obj;
				for (Object e : list) {
					if (e instanceof Integer) {
						retList.add((Integer) e);
					}
				}
			}
		}
		return retList;
	}

	private ContractPriceComponent getContractPriceComponent(ProceedingJoinPoint jp) throws BusinessException {
		ContractPriceComponent e = null;
		for (Object obj : jp.getArgs()) {
			if (obj instanceof List) {
				List<?> list = (List<?>) obj;
				if (!CollectionUtils.isEmpty(list)) {
					Object object = list.get(0);
					e = this.getContractPriceComponent(object);
				} else {
					e = this.getContractPriceComponent(obj);
				}
			}
		}
		return e;
	}

	private ContractPriceComponent getContractPriceComponent(Object obj) {
		if (obj instanceof ContractPriceComponent) {
			return (ContractPriceComponent) obj;
		} else if (obj instanceof ContractPriceComponentConv) {
			return ((ContractPriceComponentConv) obj).getContractPriceComponent();
		}
		return null;
	}

	private Contract getContract(ProceedingJoinPoint jp) throws BusinessException {
		Contract c = null;
		try {
			for (Object obj : jp.getArgs()) {
				if (obj instanceof AbstractEntity) {
					c = this.getContract(obj);
				} else if (obj instanceof List) {
					List<?> list = (List<?>) obj;
					if (!list.isEmpty()) {
						c = this.getContract(list.get(0));
					}
				}
			}
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			LOG.error("Contract cannot be retrived using reflection.", e);
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}
		return c;
	}

	private Contract getContract(Object obj) throws IllegalAccessException, InvocationTargetException, BusinessException {
		Contract c = null;
		if (obj instanceof Contract) {
			c = (Contract) obj;
			return c;
		}

		Class<?> type = obj.getClass();
		for (Method m : type.getDeclaredMethods()) {
			if (m.getReturnType().equals(ContractPriceComponent.class) && m.getName().startsWith("get")) {
				ContractPriceComponent comp = (ContractPriceComponent) m.invoke(obj);
				c = comp.getContrPriceCtgry().getContract();
			}
			if (m.getReturnType().equals(ContractPriceCategory.class) && m.getName().startsWith("get")) {
				ContractPriceCategory cpc = (ContractPriceCategory) m.invoke(obj);
				c = cpc.getContract();
			}
			if (m.getReturnType().equals(Contract.class) && m.getName().startsWith("get")) {
				c = (Contract) m.invoke(obj);
			}
			if (this.isPurchaseContract(c)) {
				break;
			}
		}
		return c;
	}

	private boolean isPurchaseContract(Contract c) {
		if (c != null && DealType._BUY_.equals(c.getDealType())) {
			return true;
		}
		return false;
	}

	private void internalAssignPropagate(ProceedingJoinPoint jp) throws BusinessException {
		boolean flag = false;
		Integer objectId = null;
		for (Object obj : jp.getArgs()) {
			if (obj instanceof AsgnContext) {
				AsgnContext ctx = (AsgnContext) obj;
				if (ctx.getLeftTable().equalsIgnoreCase(ContractPriceCategory.TABLE_NAME)) {
					flag = true;
				}
			}
			if (obj instanceof String && StringUtils.isNumeric((String) obj)) {
				objectId = Integer.parseInt((String) obj);
			}
		}
		if (flag && objectId != null) {
			ContractPriceCategory cat = this.catSrv.findById(objectId);
			if (cat != null) {
				try {
					Contract c = cat.getContract();
					LOG.debug(PropagatePurchaseContractAspect.MSG_1 + c.getCode());
					Contract sibling = this.contSrv.findSiblingContract(c);
					ContractPriceCategory cpc = this.childPropagator.propagateContractPriceCategory(cat, sibling);
					this.catSrv.update(cpc);
				} catch (SiblingContractNotFoundException e) {
					if (LOG.isDebugEnabled()) {
						LOG.debug(e.getMessage(), e);
					}
				}

			}
		}
	}

}
