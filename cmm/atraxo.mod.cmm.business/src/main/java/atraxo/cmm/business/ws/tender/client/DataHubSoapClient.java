package atraxo.cmm.business.ws.tender.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.xml.bind.JAXBException;
import javax.xml.ws.WebServiceException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.api.ext.datahub.IDataHubClientRequestService;
import atraxo.cmm.business.ws.tender.transformer.DataHubOutTransformer;
import atraxo.cmm.domain.ext.tender.DataHubRequestType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.business.ws.ExternalInterfaceWSHelper;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceHistory;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfacesHistoryStatus;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import atraxo.fmbas.domain.ws.dto.WSExecutionType;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.Session;
import seava.j4e.commons.utils.XMLUtils;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcknowledge;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderResponse;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderServiceService;

public class DataHubSoapClient<E extends AbstractEntity, T> implements Callable<WSExecutionResult> {

	private static final Logger LOGGER = LoggerFactory.getLogger(DataHubSoapClient.class);

	private static final String DATA_HUB_SUCCESS_MSG = "OK";

	private IUser user;
	private ExternalInterface externalInterface;
	private ExtInterfaceParameters externalInterfaceParams;
	private ExternalInterfaceHistoryFacade interfaceHistoryFacade;
	private ExternalInterfaceMessageHistoryFacade interfaceMessageHistoryFacade;

	private List<E> list;
	private Class<T> dtoClass;
	private IDataHubClientRequestService<E, T> requestService;
	private DataHubOutTransformer<E, T> transformer;

	private DataHubRequestType requestType;
	private FuelTenderServiceService client;

	private Long refMsgId;
	private String reason;
	private ExtInterfaceNames receiverInterface;

	/**
	 * Add context thread params thru this contructor
	 *
	 * @param externalInterface
	 * @param externalInterfaceParams
	 * @param list
	 * @param interfaceHistoryFacade
	 * @param interfaceMessageHistoryFacade
	 * @param transformer
	 * @param dtoClass
	 * @param requestType
	 */
	public DataHubSoapClient(ExternalInterface externalInterface, ExtInterfaceParameters externalInterfaceParams, List<E> list,
			ExternalInterfaceHistoryFacade interfaceHistoryFacade, ExternalInterfaceMessageHistoryFacade interfaceMessageHistoryFacade,
			DataHubOutTransformer<E, T> transformer, Class<T> dtoClass, DataHubRequestType requestType) {

		this.dtoClass = dtoClass;
		this.requestType = requestType;

		this.user = Session.user.get();
		this.externalInterface = externalInterface;
		this.externalInterfaceParams = externalInterfaceParams;
		this.interfaceHistoryFacade = interfaceHistoryFacade;
		this.interfaceMessageHistoryFacade = interfaceMessageHistoryFacade;

		this.list = list;
		this.transformer = transformer;
	}

	/**
	 * Schedule the start of this callable thread by passing nr. of seconds as param
	 *
	 * @param seconds
	 * @return
	 * @throws BusinessException
	 */
	public WSExecutionResult start(int seconds) throws BusinessException {
		ExecutorService srv = Executors.newSingleThreadExecutor();

		Future<WSExecutionResult> future = srv.submit(this);
		try {
			return future.get(seconds, TimeUnit.SECONDS);
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error(e.getMessage(), e);
			srv.shutdown();
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		} catch (TimeoutException e) {
			LOGGER.warn("External interface execution takes more than 10 seconds. " + e.getMessage(), e);
			srv.shutdown();
			return null;
		} finally {
			srv.shutdown();
		}
	}

	/**
	 * Process the thread main bussiness logic
	 *
	 * @return WSExecutionResult
	 * @throws Exception
	 */
	@Override
	public WSExecutionResult call() throws Exception {
		StopWatch watch = new StopWatch();

		// Start counter
		watch.start();

		// Start constructing the interface execution result
		WSExecutionResult execResult = this.createWsExecutionResult();
		List<WSExecutionResult> executionHistoryList = new ArrayList<>();
		boolean isBulkRequest = this.requestType.equals(DataHubRequestType.BULK_REQUESTS);

		// Set session user
		Session.user.set(this.user);

		try {
			// Create Datahub Client
			this.createClient();

			// Submit request to DataHub
			executionHistoryList = isBulkRequest ? this.submitBulkRequest() : this.submitSingleRequest();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);

			execResult.setExecutionData(e.getLocalizedMessage() + "::" + ExceptionUtils.getStackTrace(e));

			// Execute finishing operations
			if (this.requestService != null) {
				if (isBulkRequest) {
					this.requestService.processDataHubBulkWsResult(execResult, this.list, this.dtoClass, this.reason);
				} else {
					for (E entity : this.list) {
						this.requestService.processDataHubSingleWsResult(execResult, entity, this.dtoClass, this.reason);
					}
				}
			}

			executionHistoryList.add(execResult);

			// we need to extract the required data in case it fails
			this.extractDTOandSaveMessageHistory(execResult);
		} finally {
			watch.stop();

			// Save the execution result to the external interface history
			this.saveInterfaceHistory(watch, executionHistoryList);
		}

		return execResult;
	}

	private List<WSExecutionResult> submitBulkRequest() throws BusinessException {
		String xml = null;
		Long msgId = null;

		List<WSExecutionResult> executionHistoryList = new ArrayList<>();

		// Create execution result with default data
		WSExecutionResult execResult = this.createWsExecutionResult();

		try {
			// Generate random 10 digit number
			msgId = ThreadLocalRandom.current().nextLong(1L, 10000000000L);

			// Build DTO for the request
			T dto = this.buildDTO(null, msgId);

			FuelTenderResponse response = this.requestService.submitRequest(this.client, dto);

			// Build execution result
			xml = XMLUtils.toString(dto, dto.getClass(), true, true);
			execResult.setSuccess(DATA_HUB_SUCCESS_MSG.equals(response.getResponse()) ? Boolean.TRUE : Boolean.FALSE);
			execResult.setExecutionData(execResult.isSuccess() ? xml : response.getErrorDescription());

			// Add execution result to list
			executionHistoryList.add(execResult);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);

			// Save the execution result to the external interface history
			execResult.setExecutionData(e.getLocalizedMessage() + "::" + ExceptionUtils.getStackTrace(e));

			// Add execution result to list
			executionHistoryList.add(execResult);
		} finally {
			// Execute finishing operations (if the case)
			if (this.requestService != null) {
				this.requestService.processDataHubBulkWsResult(execResult, this.list, this.dtoClass, this.reason);
			}

			// Save message history facade
			for (E entity : this.list) {
				this.saveInterfaceMessageHistory(entity, execResult, msgId, xml);
			}
		}

		return executionHistoryList;
	}

	private List<WSExecutionResult> submitSingleRequest() throws BusinessException {
		String xml = null;
		List<WSExecutionResult> executionHistoryList = new ArrayList<>();

		for (E entity : this.list) {
			// Create execution result for each entity
			WSExecutionResult execResult = this.createWsExecutionResult();

			// Generate random 10 digit number
			Long msgId = ThreadLocalRandom.current().nextLong(1L, 10000000000L);

			// Build DTO for the request
			try {
				T dto = this.buildDTO(entity, msgId);
				FuelTenderResponse response = this.requestService.submitRequest(this.client, dto);

				// Build execution result
				xml = XMLUtils.toString(dto, dto.getClass(), true, true);
				execResult.setSuccess(DATA_HUB_SUCCESS_MSG.equals(response.getResponse()) ? Boolean.TRUE : Boolean.FALSE);
				execResult.setExecutionData(execResult.isSuccess() ? xml : response.getErrorDescription());

				// Execute finishing operations
				if (this.requestService != null) {
					this.requestService.processDataHubSingleWsResult(execResult, entity, this.dtoClass, this.reason);
				}
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);

				// Save the execution result to the external interface history
				execResult.setExecutionData(e.getLocalizedMessage() + "::" + ExceptionUtils.getStackTrace(e));

				// Add execution result to list
				executionHistoryList.add(execResult);
			}

			// Add execution result to list
			executionHistoryList.add(execResult);

			// Save message history facade
			this.saveInterfaceMessageHistory(entity, execResult, msgId, xml);
		}

		return executionHistoryList;
	}

	private void createClient() throws BusinessException, InterruptedException {
		WebServiceException wsException = null;

		// Build the target url from the interface params (wsdl)
		String endpointAddress = this.externalInterfaceParams.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WSDL);

		URL url;
		try {
			url = new URL(endpointAddress);
		} catch (MalformedURLException e) {
			throw new BusinessException(ErrorCode.COMMUNICATION_ERROR, e);
		}

		int retryNr = Integer.parseInt(this.externalInterfaceParams.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_NR_RETRIES));
		long retryInt = Long.parseLong(this.externalInterfaceParams.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_RETRIES_INTERVAL));

		// set as -1 just to enter the cycle (and call DATA HUB Client) at least once
		int retries = -1;

		while (retries < retryNr) {
			try {
				if (retryNr == 0) {
					retries = 0;
				}

				retries++;
				this.client = new FuelTenderServiceService(url);
			} catch (WebServiceException e1) {
				LOGGER.error("ERROR WebService:could not create client for FuelTenderService, will try for retryNumber " + retryNr, e1);
				wsException = e1;
				if (retryNr == 0) {
					// this is needed because there needs to be a delay before setting it to Failed (first is In progress)
					Thread.sleep(Long.valueOf(3) * 1000);
				} else if (retries != retryNr) {
					// for the last cycle don't wait up to perform finishing operations
					Thread.sleep(retryInt * 60 * 1000);
				}
			}
		}

		if (wsException != null) {
			LOGGER.error(wsException.getMessage(), wsException);
			throw wsException;
		}
	}

	private void saveInterfaceHistory(StopWatch stopWatch, List<WSExecutionResult> results) throws BusinessException {
		List<ExternalInterfaceHistory> historyList = new ArrayList<>();

		for (WSExecutionResult result : results) {
			ExternalInterfaceHistory history = new ExternalInterfaceHistory();
			history.setExternalInterface(this.externalInterface);
			history.setRequester(Session.user.get().getClientCode());
			history.setRequestReceived(new Date(stopWatch.getStartTime()));
			history.setResponseSent(new Date(stopWatch.getStartTime() + stopWatch.getTime()));
			history.setStatus(result.isSuccess() ? ExternalInterfacesHistoryStatus._SUCCESS_ : ExternalInterfacesHistoryStatus._FAILURE_);
			history.setResult(result.getTrimmedExecutionData());

			historyList.add(history);
		}

		this.interfaceHistoryFacade.insert(historyList);
	}

	private void saveInterfaceMessageHistory(E entity, WSExecutionResult result, Long msgId, String xml) throws BusinessException {
		// Save Message History for all messages except Ack
		if (!FuelTenderAcknowledge.class.equals(this.dtoClass)) {
			ExternalInterfaceMessageHistory messageHistory = new ExternalInterfaceMessageHistory();
			messageHistory.setMessageId(msgId.toString());
			messageHistory.setExternalInterface(this.externalInterface);
			messageHistory.setExecutionTime(GregorianCalendar.getInstance().getTime());
			messageHistory.setObjectRefId(entity.getRefid());
			messageHistory.setObjectId(entity.getId());

			// Set object type, in case of contract set bid
			messageHistory.setObjectType(entity instanceof Contract ? "Bid" : entity.getClass().getSimpleName());
			messageHistory.setStatus(
					result.isSuccess() ? ExternalInterfaceMessageHistoryStatus._SENT_ : ExternalInterfaceMessageHistoryStatus._FAILED_COMMUNICATION_);

			this.interfaceMessageHistoryFacade.insert(messageHistory);

			// Save message to webDav
			this.interfaceMessageHistoryFacade.uploadToS3(xml, msgId.toString(), true);
		}

	}

	private T buildDTO(E entity, Long msgId) throws BusinessException, JAXBException {
		T dto;
		boolean isBulkRequest = this.requestType.equals(DataHubRequestType.BULK_REQUESTS);

		if (this.refMsgId != null && this.receiverInterface != null) {
			dto = isBulkRequest ? this.transformer.transformModelsToDTO(this.list, msgId, this.refMsgId, this.receiverInterface)
					: this.transformer.transformModelToDTO(entity, msgId, this.refMsgId, this.receiverInterface);
		} else if (this.reason != null) {
			dto = isBulkRequest ? this.transformer.transformModelsToDTO(this.list, msgId, this.reason)
					: this.transformer.transformModelToDTO(entity, msgId, this.reason);
		} else {
			dto = isBulkRequest ? this.transformer.transformModelsToDTO(this.list, msgId) : this.transformer.transformModelToDTO(entity, msgId);
		}
		return dto;
	}

	private void extractDTOandSaveMessageHistory(WSExecutionResult execResult) throws BusinessException, JAXBException {
		for (E entity : this.list) {
			Long msgId = ThreadLocalRandom.current().nextLong(1, 10000000000L);

			T dto = this.buildDTO(entity, msgId);
			String xml = XMLUtils.toString(dto, dto.getClass(), true, true);

			// Save to message history
			this.saveInterfaceMessageHistory(entity, execResult, msgId, xml);
		}
	}

	/**
	 * @return
	 */
	private WSExecutionResult createWsExecutionResult() {
		WSExecutionResult execResult = new WSExecutionResult();
		execResult.setExecutionType(WSExecutionType.EXPORT_DATA);
		execResult.setSuccess(false);
		return execResult;
	}

	public void setRequestService(IDataHubClientRequestService<E, T> requestService) {
		this.requestService = requestService;
	}

	public void setRefMsgId(long refMsgId) {
		this.refMsgId = refMsgId;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public void setReceiverInterface(ExtInterfaceNames receiverInterface) {
		this.receiverInterface = receiverInterface;
	}

}