package atraxo.cmm.business.ext.mailMerge.delegate.builders;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.domain.ext.mailmerge.dto.contract.ContractPriceDto;
import atraxo.cmm.domain.ext.mailmerge.dto.contract.RestrictionDto;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author apetho
 */
public final class PriceDtoBuilder {

	private PriceDtoBuilder() {

	}

	public static List<ContractPriceDto> buildContractPrice(List<ContractPriceCategory> contractPriceCategories, Integer decimals,
			IContractPriceCategoryService categoryService, Date customStartDate, Date customEndDate) throws BusinessException {
		List<ContractPriceDto> contractPricesList = new ArrayList<>();
		Date today = Calendar.getInstance().getTime();
		contractPriceCategories.sort(new Comparator<ContractPriceCategory>() {
			@Override
			public int compare(ContractPriceCategory o1, ContractPriceCategory o2) {
				return o1.getOrderIndex() < o2.getOrderIndex() ? -1 : 1;
			}

		});
		today = DateUtils.truncate(today, Calendar.DAY_OF_MONTH);
		Date yesterday = DateUtils.addDays(today, -1);
		for (ContractPriceCategory contractPriceCategory : contractPriceCategories) {
			if (!CollectionUtils.isEmpty(contractPriceCategory.getChildPriceCategory())
					&& isComposite(contractPriceCategory.getChildPriceCategory())) {
				continue;
			}
			ContractPriceDto contractPriceDto = new ContractPriceDto();
			contractPriceDto.setName(contractPriceCategory.getName());
			contractPriceDto.setCategoryName(contractPriceCategory.getPriceCategory().getName());

			contractPriceDto.setOrderIndex(contractPriceCategory.getOrderIndex());

			buildRestrictions(contractPriceCategory, contractPriceDto);

			List<ContractPriceComponent> contractPriceComponents = new ArrayList<>(contractPriceCategory.getPriceComponents());
			for (ContractPriceComponent contractPriceComponent : contractPriceComponents) {
				if (yesterday.equals(contractPriceComponent.getValidTo())) {

					if (PriceInd._COMPOSITE_.equals(contractPriceCategory.getPriceCategory().getPricePer())) {
						BigDecimal settlementPrice = categoryService.getPriceInCurrencyUnit(contractPriceCategory,
								contractPriceCategory.getContract().getSettlementUnit().getCode(),
								contractPriceCategory.getContract().getSettlementCurr().getCode(), yesterday);

						contractPriceDto.setOldPrice(settlementPrice.setScale(decimals, RoundingMode.HALF_UP));
						contractPriceDto.setOldSettlementPrice(settlementPrice.setScale(decimals, RoundingMode.HALF_UP));
						contractPriceDto.setValidFrom(contractPriceComponent.getValidFrom());
						contractPriceDto.setOldExchangeRate(calculateExchRate(contractPriceComponent, contractPriceCategory.getContract(),
								contractPriceDto.getOldSettlementPrice()));
					} else {
						List<ContractPriceComponentConv> contractPriceComponentConv = new ArrayList<>(contractPriceComponent.getConvertedPrices());
						Collections.sort(contractPriceComponentConv, new Comparator<ContractPriceComponentConv>() {
							// order DESC by validFrom
							@Override
							public int compare(ContractPriceComponentConv o1, ContractPriceComponentConv o2) {
								return o2.getValidFrom().compareTo(o1.getValidFrom());
							}
						});
						ContractPriceComponentConv conv = contractPriceComponentConv.get(0);
						contractPriceDto.setOldPrice(conv.getPrice().setScale(decimals, RoundingMode.HALF_UP));
						contractPriceDto.setOldSettlementPrice(conv.getEquivalent().setScale(decimals, RoundingMode.HALF_UP));
						contractPriceDto.setValidFrom(conv.getValidFrom());
						contractPriceDto.setOldExchangeRate(conv.getExchangeRate());
					}
				}

				if (today.compareTo(contractPriceComponent.getValidFrom()) >= 0 && today.compareTo(contractPriceComponent.getValidTo()) <= 0) {
					List<ContractPriceComponentConv> contractPriceComponentConv = new ArrayList<>(contractPriceComponent.getConvertedPrices());
					if (contractPriceDto.getValidFrom() == null) {
						contractPriceDto.setValidFrom(contractPriceComponent.getValidFrom());
					}

					if (PriceInd._COMPOSITE_.equals(contractPriceCategory.getPriceCategory().getPricePer())) {
						BigDecimal settlementPrice = categoryService.getPriceInCurrencyUnit(contractPriceCategory,
								contractPriceCategory.getContract().getSettlementUnit().getCode(),
								contractPriceCategory.getContract().getSettlementCurr().getCode(), today);

						contractPriceDto.setSettlementCurrencyUnit(
								CurrencyUnitDtoBuilder.buildCurrecyDto(contractPriceComponent.getCurrency(), contractPriceComponent.getUnit()));
						contractPriceDto.setNewSettlementPrice(settlementPrice.setScale(decimals, RoundingMode.HALF_UP));
						contractPriceDto.setNewPrice(settlementPrice.setScale(decimals, RoundingMode.HALF_UP));
						contractPriceDto.setCurrencyUnit(
								CurrencyUnitDtoBuilder.buildCurrecyDto(contractPriceComponent.getCurrency(), contractPriceComponent.getUnit()));
						contractPriceDto.setNewExchangeRate(calculateExchRate(contractPriceComponent, contractPriceCategory.getContract(),
								contractPriceDto.getOldSettlementPrice()));
					} else {
						for (ContractPriceComponentConv conv : contractPriceComponentConv) {
							if (today.compareTo(conv.getValidFrom()) >= 0 && today.compareTo(conv.getValidTo()) <= 0) {
								contractPriceDto.setSettlementCurrencyUnit(
										CurrencyUnitDtoBuilder.buildCurrecyDto(conv.getEquivalentCurrency(), conv.getEquivalentUnit()));
								contractPriceDto.setNewSettlementPrice(conv.getEquivalent().setScale(decimals, RoundingMode.HALF_UP));
								contractPriceDto.setNewPrice(conv.getPrice().setScale(decimals, RoundingMode.HALF_UP));
								contractPriceDto.setCurrencyUnit(CurrencyUnitDtoBuilder.buildCurrecyDto(contractPriceComponent.getCurrency(),
										contractPriceComponent.getUnit()));
								contractPriceDto.setNewExchangeRate(conv.getExchangeRate());
								break;
							}
						}
					}
				}
			}
			contractPricesList.add(contractPriceDto);
		}
		return contractPricesList;

	}

	private static boolean isComposite(Collection<ContractPriceCategory> priceCategories) {
		if (CollectionUtils.isEmpty(priceCategories)) {
			return true;
		}
		for (ContractPriceCategory cpc : priceCategories) {
			if (!PriceInd._COMPOSITE_.equals(cpc.getPriceCategory().getPricePer())) {
				return false;
			}
		}
		return true;
	}

	private static void buildRestrictions(ContractPriceCategory contractPriceCategory, ContractPriceDto contractPriceDto) {
		if (!CollectionUtils.isEmpty(contractPriceCategory.getPriceRestrictions())) {
			List<RestrictionDto> restricionDtos = new ArrayList<>();
			for (ContractPriceRestriction contractPriceRestriction : contractPriceCategory.getPriceRestrictions()) {
				restricionDtos.add(RestricionDtoBuilder.buildRestriction(contractPriceRestriction));
			}
			contractPriceDto.setRestrictions(restricionDtos);
		}
	}

	/**
	 * @param comp
	 * @param contract
	 * @param settPrice
	 * @return
	 * @throws BusinessException
	 */
	private static BigDecimal calculateExchRate(ContractPriceComponent comp, Contract contract, BigDecimal settPrice) throws BusinessException {
		if (PriceInd._COMPOSITE_.equals(comp.getContrPriceCtgry().getPriceCategory().getPricePer())) {
			return BigDecimal.ONE;
		}
		if (comp.getConvertedPrices() != null) {
			Date today = Calendar.getInstance().getTime();
			for (ContractPriceComponentConv conv : comp.getConvertedPrices()) {
				if (conv.getValidFrom().compareTo(today) <= 0 && conv.getValidTo().compareTo(today) >= 0) {
					return conv.getExchangeRate();
				}
			}
		}
		return BigDecimal.ONE;
	}

}
