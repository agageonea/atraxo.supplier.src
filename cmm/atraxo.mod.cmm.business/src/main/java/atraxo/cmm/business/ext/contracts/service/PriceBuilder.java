package atraxo.cmm.business.ext.contracts.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.contracts.IPricingBaseService;
import atraxo.cmm.business.api.ext.contracts.IPriceBuilder;
import atraxo.cmm.business.api.ext.prices.builder.IComponentPriceBuilder;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.utils.DatePeriod;
import atraxo.cmm.business.ext.utils.DateUtils;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.fmbas.business.api.quotation.IQuotationValueService;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * @author zspeter
 */
public class PriceBuilder extends AbstractBusinessBaseService implements IPriceBuilder {

	private static final Logger LOG = LoggerFactory.getLogger(PriceBuilder.class);
	@Autowired
	private IQuotationValueService quotationValueService;
	@Autowired
	private IPricingBaseService pbService;
	@Autowired
	private IContractPriceCategoryService cpcService;
	@Autowired
	private IComponentPriceBuilder componentPriceBuilder;

	/**
	 * Create and add child entities to pricing base.
	 *
	 * @param pb
	 * @throws BusinessException
	 */
	@Override
	public void buildPricingBases(PricingBase pb) throws BusinessException {
		ContractPriceCategory cpc = this.buildContractPriceCategory(pb.getPriceCat(), pb);
		this.buildContractPriceCategory(cpc);
	}

	/**
	 * Create and add child entities for contract price categories.
	 *
	 * @param e
	 * @throws BusinessException
	 */
	@Override
	public void buildContractPriceCategory(ContractPriceCategory e) throws BusinessException {
		if (e.getPriceCategory().getIsIndexBased()) {
			this.buildFromQuotation(e);
		} else {
			this.createFromInitialPrice(e);
		}
	}

	/**
	 * @param jsonStr
	 * @param cpc
	 * @throws BusinessException
	 */
	@Override
	public void assignParentPriceCategory(String jsonStr, ContractPriceCategory cpc) throws BusinessException {
		if ((jsonStr == null || jsonStr.isEmpty()) && CollectionUtils.isEmpty(cpc.getParentPriceCategory())) {
			throw new BusinessException(CmmErrorCode.COMPOSITE_PRICE_MUST_HAVE_A_CHILD, CmmErrorCode.COMPOSITE_PRICE_MUST_HAVE_A_CHILD.getErrMsg());
		}
		try {
			JSONArray jsonArray = new JSONArray(jsonStr);
			if (jsonArray.length() == 0) {
				throw new BusinessException(CmmErrorCode.COMPOSITE_PRICE_MUST_HAVE_A_CHILD,
						CmmErrorCode.COMPOSITE_PRICE_MUST_HAVE_A_CHILD.getErrMsg());
			}
			Collection<ContractPriceCategory> oldList = cpc.getParentPriceCategory();
			cpc.setParentPriceCategory(new ArrayList<ContractPriceCategory>());
			for (int i = 0; i < jsonArray.length(); ++i) {
				JSONObject json = jsonArray.getJSONObject(i);
				ContractPriceCategory priceCategory = this.getPriceCategory(json);
				cpc.getParentPriceCategory().add(priceCategory);
			}
			this.checkParentsStatus(cpc, oldList, true);
		} catch (JSONException e) {
			LOG.info("Invalid json : " + jsonStr, e);
		}
	}

	/**
	 * @param e
	 * @throws BusinessException
	 */
	@Override
	public void updateChilds(ContractPriceCategory e) throws BusinessException {
		if (e.getPricingBases() != null && e.getPriceCategory().getIsIndexBased()) {
			e.setPriceComponents(null);
			this.buildFromQuotation(e);
		} else {
			if (e.getContinous()) {
				this.extendsToMargins(e.getPricingBases());
			}
			for (ContractPriceComponent comp : e.getPriceComponents()) {
				this.componentPriceBuilder.createConvertedPrices(comp);

			}
		}
	}

	/**
	 * @param cpc
	 * @param indicator
	 * @throws BusinessException
	 */
	@Override
	public void updateParentPriceCategories(ContractPriceCategory cpc, CalculateIndicator indicator) throws BusinessException {
		List<ContractPriceCategory> updateList = new ArrayList<>();
		if (cpc.getParentPriceCategory() == null || !PriceInd._COMPOSITE_.equals(cpc.getPriceCategory().getPricePer())) {
			return;
		}
		for (ContractPriceCategory parent : cpc.getParentPriceCategory()) {
			if (!indicator.equals(parent.getCalculateIndicator())) {
				parent.setCalculateIndicator(indicator);
				updateList.add(parent);
			}
		}
		this.cpcService.update(updateList);

	}

	/**
	 * If a PricingBase is created and it does not fit the entire Contract period, it will be extended
	 *
	 * @param e
	 */
	@Override
	public void extendsNotContinous(PricingBase e) {
		if (!e.getContinous()) {
			e.setInitialPriceValidFrom(e.getValidFrom());
			e.setInitialPriceValidTo(e.getValidTo());
			e.setValidFrom(e.getContract().getValidFrom());
			e.setValidTo(e.getContract().getValidTo());
		}
	}

	/**
	 * If a PricingBase is created and it does not fit the entire Contract period, it will be extended
	 *
	 * @param e
	 * @throws BusinessException
	 */
	@Override
	public void extendsToMargins(PricingBase e) throws BusinessException {
		if (this.pbService.isProduct(e)) {
			List<PricingBase> list = this.pbService.getNeighbourProductPbs(e);
			if (!list.isEmpty()) {
				PricingBase first = list.get(0);
				if (first.getValidFrom().after(e.getValidFrom()) && e.getValidFrom().after(e.getContract().getValidFrom())) {
					e.setValidFrom(e.getContract().getValidFrom());
				}
				PricingBase last = list.get(list.size() - 1);
				if (last.getValidTo().before(e.getValidTo()) && e.getValidTo().before(e.getContract().getValidTo())) {
					e.setValidTo(e.getContract().getValidTo());
				}
			} else {
				e.setValidFrom(e.getContract().getValidFrom());
				e.setValidTo(e.getContract().getValidTo());
			}
		} else {
			e.setValidFrom(e.getContract().getValidFrom());
			e.setValidTo(e.getContract().getValidTo());
		}
	}

	/**
	 * @param e
	 * @return
	 * @throws BusinessException
	 */
	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.ext.contracts.IPriceBuilder#updateMargins(java.util.List, java.util.Date, java.util.Date)
	 */
	@Override
	public List<PricingBase> updateMargins(List<PricingBase> list, Date contractValidFrom, Date contractValidTo) throws BusinessException {
		List<PricingBase> updatedList = new ArrayList<>();
		List<PricingBase> productList = new ArrayList<>();
		List<PricingBase> feeList = new ArrayList<>();

		for (PricingBase pb : list) {
			if (this.pbService.isProduct(pb)) {
				productList.add(pb);
			} else if (this.pbService.isFeeOrTax(pb)) {
				feeList.add(pb);
			} else if (PriceInd._COMPOSITE_.equals(pb.getPriceCat().getPricePer()) || PriceInd._PERCENT_.equals(pb.getPriceCat().getPricePer())) {
				pb.setValidFrom(contractValidFrom);
				pb.setValidTo(contractValidTo);
				updatedList.add(pb);
			}
		}

		for (PricingBase pb : feeList) {
			if (pb.getContinous()) {
				pb.setValidFrom(contractValidFrom);
				pb.setValidTo(contractValidTo);
			} else {
				if (pb.getValidFrom().before(contractValidFrom)) {
					pb.setValidFrom(contractValidFrom);
				}
				if (pb.getValidTo().after(contractValidTo)) {
					pb.setValidTo(contractValidTo);
				}
			}
		}

		// sort accending
		productList.sort((o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()));
		Iterator<PricingBase> iterator = productList.iterator();
		while (iterator.hasNext()) {
			PricingBase pb = iterator.next();
			PricingBase first = productList.get(0);
			PricingBase last = productList.get(productList.size() - 1);

			if (pb.getValidTo().before(contractValidFrom)) {
				iterator.remove();
			} else if (pb.getValidFrom().before(contractValidFrom) && pb.getValidTo().after(contractValidFrom)) {
				pb.setValidFrom(contractValidFrom);
			} else if (pb.equals(first) && pb.getValidFrom().after(contractValidFrom)) {
				pb.setValidFrom(contractValidFrom);
			}
			if (pb.getValidFrom().after(contractValidTo)) {
				iterator.remove();
			} else if (pb.getValidFrom().before(contractValidTo) && pb.getValidTo().after(contractValidTo)) {
				pb.setValidTo(contractValidTo);
			} else if (pb.equals(last) && pb.getValidTo().before(contractValidTo)) {
				pb.setValidTo(contractValidTo);
			}
		}

		updatedList.addAll(productList);
		updatedList.addAll(feeList);
		return updatedList;
	}

	/**
	 * @param cpc
	 * @param oldList
	 * @throws BusinessException
	 */
	private void checkParentsStatus(ContractPriceCategory cpc, Collection<ContractPriceCategory> oldList, boolean update) throws BusinessException {
		List<ContractPriceCategory> updateList = new ArrayList<>();
		if (oldList == null) {
			return;
		}
		for (ContractPriceCategory temp : oldList) {
			if (update && cpc.getParentPriceCategory().contains(temp)) {
				continue;
			}

			if (this.cpcService.getCompositeChildsCategory(temp).size() == 1) {
				temp.setCalculateIndicator(CalculateIndicator._INCLUDED_);
				updateList.add(temp);
			}
		}
		if (!updateList.isEmpty()) {
			this.cpcService = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
			this.cpcService.update(updateList);
		}
	}

	/**
	 * @param json
	 * @return
	 * @throws JSONException
	 * @throws BusinessException
	 */
	private ContractPriceCategory getPriceCategory(JSONObject json) throws JSONException, BusinessException {
		String idStr = json.getString("id");
		Integer priceCategoryId = Integer.parseInt(idStr);
		return this.cpcService.findById(priceCategoryId);
	}

	/**
	 * If a PricingBase is an index, it is build using Quotation
	 *
	 * @param category
	 * @throws BusinessException
	 */
	private void buildFromQuotation(ContractPriceCategory category) throws BusinessException {

		PricingBase pb = category.getPricingBases();
		List<QuotationValue> quotationValues = this.pbService.getQuotationValues(pb);
		quotationValues.sort((o1, o2) -> o1.getValidFromDate().compareTo(o2.getValidFromDate()));
		QuotationValue last = quotationValues.get(quotationValues.size() - 1);

		category.setPriceComponents(null);

		for (QuotationValue quotationValue : quotationValues) {
			DatePeriod period = DateUtils.calculateContractPriceComponentPeriod(pb, quotationValue);
			if (quotationValue.equals(last)) {
				Calendar endDateCal = Calendar.getInstance();
				endDateCal.setTime(pb.getValidTo());
				period.setEndDate(endDateCal);
			}
			// if the calculated period is COMPLETELY OUTSIDE the validty period then do not add the new contract price component
			boolean periodNotInContractValidity = (DateUtils.compareDates(period.getStartDate().getTime(), category.getPricingBases().getValidFrom(),
					true) < 0 && DateUtils.compareDates(period.getEndDate().getTime(), category.getPricingBases().getValidFrom(), true) < 0)
					|| (DateUtils.compareDates(period.getStartDate().getTime(), category.getPricingBases().getValidTo(), true) > 0
							&& DateUtils.compareDates(period.getEndDate().getTime(), category.getPricingBases().getValidTo(), true) > 0);

			if (!periodNotInContractValidity) {
				ContractPriceComponent component = this.componentPriceBuilder.buildContractPriceComponent(category, quotationValue.getRate(),
						quotationValue.getIsProvisioned(), period.toArray());
				category.addToPriceComponents(component);
				this.componentPriceBuilder.createConvertedPrices(component);
			}
		}

		if (CollectionUtils.isEmpty(category.getPriceComponents())) {
			DatePeriod period = DateUtils.calculateContractPriceComponentPeriod(pb, last);
			Calendar endDateCal = Calendar.getInstance();
			endDateCal.setTime(pb.getValidTo());
			period.setEndDate(endDateCal);
			ContractPriceComponent component = this.componentPriceBuilder.buildContractPriceComponent(category, last.getRate(),
					last.getIsProvisioned(), period.toArray());
			category.addToPriceComponents(component);

			this.componentPriceBuilder.createConvertedPrices(component);

		}
		List<ContractPriceComponent> compList = new ArrayList<>(category.getPriceComponents());
		if (!compList.isEmpty()) {
			Collections.sort(compList, (o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()));

			ContractPriceComponent firstComp = compList.get(0);

			if (firstComp.getValidFrom().compareTo(pb.getValidFrom()) > 0 && this.pbService.isIndex(pb) && DateUtils.isMonday(pb.getValidFrom())) {
				QuotationValue quotationValue = this.quotationValueService.getQuotationValueByDate(pb.getQuotation(),
						org.apache.commons.lang.time.DateUtils.addDays(pb.getValidFrom(), -1));
				DatePeriod period = DateUtils.calculateContractPriceComponentPeriod(firstComp, pb, quotationValue);
				ContractPriceComponent component = this.componentPriceBuilder.buildContractPriceComponent(category, quotationValue.getRate(),
						quotationValue.getIsProvisioned(), period.toArray());
				category.addToPriceComponents(component);
				this.componentPriceBuilder.createConvertedPrices(component);
			}
		}
	}

	/**
	 * @param e
	 * @throws BusinessException
	 */
	private void createFromInitialPrice(ContractPriceCategory e) throws BusinessException {
		Date from = e.getPricingBases().getInitialPriceValidFrom() != null ? e.getPricingBases().getInitialPriceValidFrom()
				: e.getPricingBases().getValidFrom();
		Date to = e.getPricingBases().getInitialPriceValidTo() != null ? e.getPricingBases().getInitialPriceValidTo()
				: e.getPricingBases().getValidTo();
		ContractPriceComponent component = this.componentPriceBuilder.buildContractPriceComponent(e, e.getInitialPrice(), true, from, to);
		e.addToPriceComponents(component);
		this.componentPriceBuilder.createConvertedPrices(component);

	}

	/**
	 * @param priceCategory
	 * @param pb
	 * @return
	 * @throws BusinessException
	 */
	private ContractPriceCategory buildContractPriceCategory(PriceCategory priceCategory, PricingBase pb) throws BusinessException {
		ContractPriceCategory cpc = new ContractPriceCategory();
		cpc.setName(pb.getDescription());
		cpc.setContract(pb.getContract());
		cpc.setOrderIndex(pb.getOrderIndex());
		cpc.setVat(pb.getVat());
		AverageMethod method = pb.getContract().getAverageMethod();
		if (pb.getAverageMethodId() != null) {
			method = this.quotationValueService.findById(pb.getAverageMethodId(), AverageMethod.class);
		}
		cpc.setAverageMethod(method);
		FinancialSources source = pb.getContract().getFinancialSource();
		if (pb.getFinancialSourceId() != null) {
			source = this.quotationValueService.findById(pb.getFinancialSourceId(), FinancialSources.class);
		}
		cpc.setFinancialSource(source);
		cpc.setRestriction(pb.getRestriction() != null ? pb.getRestriction() : Boolean.FALSE);
		cpc.setExchangeRateOffset(pb.getExchangeRateOffset() == null || MasterAgreementsPeriod._EMPTY_.equals(pb.getExchangeRateOffset())
				? pb.getContract().getExchangeRateOffset()
				: pb.getExchangeRateOffset());
		cpc.setPriceCategory(priceCategory);
		cpc.setInitialPrice(pb.getInitialPrice());
		cpc.setInitialCurrId(pb.getInitialCurrId());
		cpc.setInitialUnitId(pb.getInitialUnitId());
		cpc.setQuantityType(pb.getQuantityType());
		if (!CalculateIndicator._CALCULATE_ONLY_.equals(cpc.getCalculateIndicator())) {
			cpc.setCalculateIndicator(CalculateIndicator._INCLUDED_);
		}
		cpc.setContinous(this.pbService.isProduct(pb) || pb.getContinous() == null ? Boolean.TRUE : pb.getContinous());
		cpc.setDefaultPriceCtgy(pb.getDefauftPC());
		pb.setContractPriceCategories(null);
		pb.addToContractPriceCategories(cpc);
		return cpc;
	}
}
