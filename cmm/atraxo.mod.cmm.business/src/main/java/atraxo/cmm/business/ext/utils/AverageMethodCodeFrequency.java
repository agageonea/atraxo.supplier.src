package atraxo.cmm.business.ext.utils;

import java.util.Calendar;

import atraxo.fmbas.domain.impl.fmbas_type.AverageMethodCode;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;

/**
 * Wrapper utility class over <code>AverageMethodCode</code>, establishing the frequency for different <code>AverageMethodCode</code>s
 *
 * @author vhojda
 */
public enum AverageMethodCodeFrequency {
	DAILY, MONTHLY, WEEKLY, SEMIMONTLHY, FORTNIGHT;

	/**
	 * @param averageMethod
	 * @return
	 */
	public static AverageMethodCodeFrequency getByAverageMethod(AverageMethod averageMethod) {
		AverageMethodCode avgMthCode = AverageMethodCode.getByName(averageMethod.getCode());
		return getByAverageMethodCode(avgMthCode);
	}

	/**
	 * @param averageMethodCode
	 * @return
	 */
	public static AverageMethodCodeFrequency getByAverageMethodCode(AverageMethodCode averageMethodCode) {
		if (averageMethodCode.equals(AverageMethodCode._DC_)) {
			return DAILY;
		} else if (averageMethodCode.equals(AverageMethodCode._WC_) || averageMethodCode.equals(AverageMethodCode._WT_)) {
			return AverageMethodCodeFrequency.WEEKLY;
		} else if (averageMethodCode.equals(AverageMethodCode._MC_) || averageMethodCode.equals(AverageMethodCode._MT_)) {
			return AverageMethodCodeFrequency.MONTHLY;
		} else if (averageMethodCode.equals(AverageMethodCode._FC_) || averageMethodCode.equals(AverageMethodCode._FT_)) {
			return AverageMethodCodeFrequency.FORTNIGHT;
		} else if (averageMethodCode.equals(AverageMethodCode._SC_) || averageMethodCode.equals(AverageMethodCode._ST_)) {
			return SEMIMONTLHY;
		}
		throw new IllegalArgumentException("Cannot create AverageMethodCodeFrequency based on average method code: " + averageMethodCode);
	}

	/**
	 * @param cal
	 * @return
	 */
	public boolean firstDayOfTheInterval(Calendar cal) {
		if (this.equals(DAILY)) {
			return true;
		}
		if (this.equals(WEEKLY) && (cal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY)) {
			return true;
		}
		if (this.equals(MONTHLY) && (cal.get(Calendar.DAY_OF_MONTH) == 1)) {
			return true;
		}
		if (this.equals(SEMIMONTLHY) && (cal.get(Calendar.DAY_OF_MONTH) == 1 || cal.get(Calendar.DAY_OF_MONTH) == 16)) {
			return true;
		}
		return (this.equals(FORTNIGHT) && (cal.get(Calendar.DAY_OF_MONTH) == 11 || cal.get(Calendar.DAY_OF_MONTH) == 26));
	}

}
