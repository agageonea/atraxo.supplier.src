package atraxo.cmm.business.ext.tender.delegate;

import java.util.ArrayList;
import java.util.List;

import atraxo.cmm.domain.ext.mailmerge.dto.AirlinesFormDTO;
import atraxo.cmm.domain.ext.mailmerge.dto.SubsidiaryFormDTO;
import atraxo.cmm.domain.ext.mailmerge.dto.TenderFormDTO;
import atraxo.cmm.domain.ext.mailmerge.dto.TenderFormHeaderDTO;
import atraxo.cmm.domain.ext.mailmerge.dto.TenderLocationFormDTO;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.fmbas.domain.impl.customer.Customer;

public class TenderFormDelegate {

	public static TenderFormHeaderDTO buildTenderFormDTO(TenderLocation tenderLocation, Customer subsidiary) {
		TenderFormDTO tenderDTO = new TenderFormDTO();
		tenderDTO.setHolderCode(tenderLocation.getTender().getHolder().getIataCode());
		tenderDTO.setHolderName(tenderLocation.getTender().getHolder().getName());
		tenderDTO.setTenderCode(tenderLocation.getTender().getCode());
		tenderDTO.setTenderName(tenderLocation.getTender().getName());
		tenderDTO.setVersion(tenderLocation.getTender().getTenderVersion());

		TenderLocationFormDTO locationDTO = new TenderLocationFormDTO();
		locationDTO.setAgreementFrom(tenderLocation.getAgreementFrom());
		locationDTO.setAgreementTo(tenderLocation.getAgreementTo());
		locationDTO.setLocationIata(tenderLocation.getLocation().getIataCode());
		locationDTO.setLocationIcao(tenderLocation.getLocation().getIcaoCode());
		locationDTO.setTotalVolume(tenderLocation.getVolume());
		locationDTO.setVolumePeriod(tenderLocation.getPeriodType().getName());
		locationDTO.setVolumeUnit(tenderLocation.getVolumeUnit().getCode());
		locationDTO.setFuelProduct(tenderLocation.getFuelProduct().getIataName());
		locationDTO.setTaxType(tenderLocation.getTaxType().getIataCode());
		locationDTO.setFlightServiceType(tenderLocation.getFlightServiceType().getCode());
		locationDTO.setDeliveryPoint(tenderLocation.getDeliveryPoint().getCode());
		locationDTO.setPackageIdentifier(tenderLocation.getPackageIdentifier());
		tenderDTO.setLocation(locationDTO);

		List<AirlinesFormDTO> airlines = new ArrayList<>();
		locationDTO.setAirlines(airlines);
		for (TenderLocationAirlines tla : tenderLocation.getTenderLocationAirlines()) {
			AirlinesFormDTO af = new AirlinesFormDTO();
			af.setComment(tla.getNotes());
			af.setIataCode(tla.getAirline().getIataCode());
			af.setVolume(tla.getVolume());
			af.setVolumeUnit(tla.getUnit().getCode());
			airlines.add(af);
		}

		SubsidiaryFormDTO subsidiaryFormDTO = new SubsidiaryFormDTO();
		subsidiaryFormDTO.setIata_code(subsidiary.getIataCode());
		subsidiaryFormDTO.setName(subsidiary.getName());

		TenderFormHeaderDTO header = new TenderFormHeaderDTO();
		header.setSubsidiary(subsidiaryFormDTO);
		header.setTender(tenderDTO);

		return header;
	}

}
