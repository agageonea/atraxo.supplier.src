package atraxo.cmm.business.ext.contracts.job.exportData;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ws.WSContractToDTOTransformer;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.business.ws.ExternalInterfaceWSHelper;
import atraxo.fmbas.business.ws.client.PumaRestClient;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;

public class ExportContractDataTasklet implements Tasklet {

	private static final Logger LOG = LoggerFactory.getLogger(ExportContractDataTasklet.class);

	public static final String KEY_NO_CONTRACTS = "no_contracts";
	public static final String KEY_INTERFACE_DISABLE = "export_job_details";
	@Autowired
	private IContractService contractService;
	@Autowired
	private ExternalInterfaceHistoryFacade externalInterfaceHistoryFacade;
	@Autowired
	private IExternalInterfaceService externalInterfaceService;
	@Autowired
	private ExternalInterfaceMessageHistoryFacade messageFacade;
	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		if (LOG.isDebugEnabled()) {
			LOG.debug("START execute()");
		}
		List<Contract> contracts = this.contractService.findAllEffectiveSalesContracts();
		this.removeSubsidiariesContracts(contracts);
		if (contracts != null) {
			if (contracts.isEmpty()) {
				chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put(KEY_NO_CONTRACTS, "");
			} else {
				ExternalInterface extInterface = this.externalInterfaceService.findByName(ExtInterfaceNames.EXPORT_CONTRACTS_TO_EBITS.getValue());
				if (extInterface.getEnabled()) {
					ExtInterfaceParameters params = new ExtInterfaceParameters(extInterface.getParams());
					params.getParamMap().remove(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_DESTINATION_ADDRESS);
					PumaRestClient<Contract> client = new PumaRestClient<>(this.taskExecutor, extInterface, params, contracts,
							this.externalInterfaceHistoryFacade, new WSContractToDTOTransformer(), this.messageFacade);
					client.start(1);
				} else {
					chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put(KEY_INTERFACE_DISABLE, "");
				}
			}
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("END execute()");
		}
		return RepeatStatus.FINISHED;
	}

	private void removeSubsidiariesContracts(List<Contract> contracts) {
		if (CollectionUtils.isEmpty(contracts)) {
			return;
		}
		Iterator<Contract> iter = contracts.iterator();
		while (iter.hasNext()) {
			Contract c = iter.next();
			if (c.getCustomer().getIsSubsidiary() && c.getHolder().getIsSubsidiary()) {
				iter.remove();
			}
		}
	}

}
