package atraxo.cmm.business.ext.contracts.job.notification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import seava.j4e.api.exceptions.BusinessException;

public class NotificationsItemReader implements ItemReader<CustomerData> {

	private static final Logger LOG = LoggerFactory.getLogger(NotificationsItemReader.class);

	@Autowired
	private IContractService constractService;
	@Autowired
	private ICustomerService customerService;

	private StepExecution stepExecution;
	private ExecutionContext executionContext;
	private List<Integer> customerIds;

	private enum ParamName {
		NOTIFIED_CUSTOMERS, UPDATEDONLY, CUSTOMER, YES
	}

	@BeforeStep
	public void storeExecutionContext(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
		this.executionContext = stepExecution.getExecutionContext();
		this.customerIds = new ArrayList<>();
	}

	@Override
	public CustomerData read() throws Exception {
		Customer customer = this.getCustomer();
		CustomerData customerData;
		if (customer != null && customer.getName() != null) {
			LOG.debug("Customer readed:" + customer.getName());
			customerData = this.generateCustomerData(customer);
			this.customerIds.add(customer.getId());
			this.executionContext.put(ParamName.NOTIFIED_CUSTOMERS.toString(), this.customerIds);
			return customerData;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private Customer getCustomer() throws BusinessException {
		String jobParameter = this.stepExecution.getJobParameters().getString(ParamName.CUSTOMER.toString());
		List<Customer> customers = new ArrayList<>();
		List<Integer> ids = (List<Integer>) this.executionContext.get(ParamName.NOTIFIED_CUSTOMERS.toString());
		Customer customer = null;
		if (jobParameter.isEmpty()) {
			if (!this.executionContext.containsKey(ParamName.NOTIFIED_CUSTOMERS.toString())) {
				Map<String, Object> params = new HashMap<>();
				params.put("status", CustomerStatus._ACTIVE_);
				customers = this.customerService.findEntitiesByAttributes(params);
			} else {
				customers = this.customerService.findCustomer(ids);
			}
		} else {
			customer = this.customerService.findByCode(jobParameter);
		}
		if (!customers.isEmpty()) {
			customer = customers.get(0);
		}
		if (ids != null && customer != null && ids.contains(customer.getId())) {
			return null;
		}
		return customer;
	}

	private List<Contract> getContracts(String jobParameter, Customer customer) throws BusinessException {
		List<Contract> contracts = this.constractService.findByCustomerId(customer.getId());
		Iterator<Contract> iterator = contracts.listIterator();
		while (iterator.hasNext()) {
			Contract contract = iterator.next();
			if (contract.getDealType().equals(DealType._BUY_)
					|| (!contract.getStatus().equals(ContractStatus._EFFECTIVE_) || contract.getIsBlueprint())) {
				iterator.remove();
			}
		}
		if (jobParameter.equalsIgnoreCase(ParamName.YES.toString())) {
			iterator = contracts.listIterator();
			while (iterator.hasNext()) {
				Contract contract = iterator.next();
				if (!this.constractService.isUpdated(contract.getId(), contract.getSettlementUnit().getCode(),
						contract.getSettlementCurr().getCode())) {
					iterator.remove();
				}
			}
		}
		return contracts;
	}

	private CustomerData generateCustomerData(Customer customer) throws BusinessException {
		String jobUpdatedParam = this.stepExecution.getJobParameters().getString(ParamName.UPDATEDONLY.toString());
		CustomerData customerData = new CustomerData();
		customerData.setCustomerId(customer.getId());
		customerData.setName(customer.getName());
		customerData.setCode(customer.getCode());
		customerData.setContractsList(this.getContracts(jobUpdatedParam, customer));
		customerData.setResponsibleBuyer(customer.getResponsibleBuyer());
		return customerData;
	}
}
