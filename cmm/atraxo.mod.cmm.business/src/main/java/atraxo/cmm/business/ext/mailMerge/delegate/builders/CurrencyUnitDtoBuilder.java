package atraxo.cmm.business.ext.mailMerge.delegate.builders;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.uom.Unit;

public final class CurrencyUnitDtoBuilder {

	private CurrencyUnitDtoBuilder() {

	}

	public static final String buildCurrecyDto(Currencies currency, Unit unit) {
		return currency.getCode() + "/" + unit.getCode();
	}

}
