package atraxo.cmm.business.ext.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Utility specialized class with the role of calculating different periods for averaging methods with different frequencies and offset.
 *
 * @author vhojda
 * @author zspeter
 */
public class AverageMethodCodeFrequencyPeriodCalculator {

	/**
	 * 
	 */
	private static final String EXCEPTION_MSG = "Period start date has an invalid value: %s";
	private AverageMethodCodeFrequency frequency;
	private Date date;
	private Integer offset;

	/**
	 * Default constructor;
	 *
	 * @param frequency
	 * @param date
	 * @param offset
	 */
	public AverageMethodCodeFrequencyPeriodCalculator(AverageMethodCodeFrequency frequency, Date date, Integer offset) {
		super();
		this.frequency = frequency;
		this.date = date;
		this.offset = offset;
	}

	/**
	 * Calculates the period of time depending on the current input data( that is frequency, date and offset );
	 *
	 * @return
	 */
	public DatePeriod calculatePeriod() {
		DatePeriod datePeriod = this.getCurrentDatePeriod();

		boolean negativeOffset = this.offset < 0;
		int offsetNoSign = negativeOffset ? this.offset * (-1) : this.offset;

		for (int i = 0; i < offsetNoSign; i++) {
			if (negativeOffset) {
				datePeriod = this.calculatesPreviousPeriod(datePeriod);
			} else {
				datePeriod = this.calculateNextPeriod(datePeriod);
			}
		}

		return datePeriod;
	}

	/**
	 * Calculates next period, given the current one.
	 *
	 * @param period
	 * @return
	 */
	public DatePeriod calculateNextPeriod(DatePeriod period) {
		Calendar startCal = (Calendar) period.getStartDate().clone();
		Calendar endCal = (Calendar) period.getEndDate().clone();
		switch (this.frequency) {
		case DAILY:
			startCal.add(Calendar.DAY_OF_MONTH, +1);
			endCal.add(Calendar.DAY_OF_MONTH, +1);
			break;
		case WEEKLY:
			startCal.add(Calendar.WEEK_OF_MONTH, +1);
			endCal.add(Calendar.WEEK_OF_MONTH, +1);
			break;
		case SEMIMONTLHY:
			if (startCal.get(Calendar.DAY_OF_MONTH) == 1) {
				startCal.set(Calendar.DAY_OF_MONTH, 16);
				endCal.set(Calendar.DAY_OF_MONTH, startCal.getActualMaximum(Calendar.DAY_OF_MONTH));
			} else if (startCal.get(Calendar.DAY_OF_MONTH) == 16) {
				startCal.add(Calendar.MONTH, +1);
				startCal.set(Calendar.DAY_OF_MONTH, 1);
				endCal.add(Calendar.MONTH, +1);
				endCal.set(Calendar.DAY_OF_MONTH, 15);
			} else {
				throw new IllegalArgumentException(
						String.format(EXCEPTION_MSG, new SimpleDateFormat().format(startCal.getTime())));
			}
			break;
		case FORTNIGHT:
			if (startCal.get(Calendar.DAY_OF_MONTH) == 11) {
				startCal.set(Calendar.DAY_OF_MONTH, 26);
				endCal.add(Calendar.MONTH, +1);
				endCal.set(Calendar.DAY_OF_MONTH, 10);
			} else if (startCal.get(Calendar.DAY_OF_MONTH) == 26) {
				startCal.add(Calendar.MONTH, +1);
				startCal.set(Calendar.DAY_OF_MONTH, 11);
				endCal.set(Calendar.DAY_OF_MONTH, 25);
			} else {
				throw new IllegalArgumentException(
						String.format(EXCEPTION_MSG, new SimpleDateFormat().format(startCal.getTime())));
			}
			break;
		case MONTHLY:
			startCal.add(Calendar.MONTH, +1);
			endCal.add(Calendar.MONTH, +1);
			endCal.set(Calendar.DAY_OF_MONTH, startCal.getActualMaximum(Calendar.DAY_OF_MONTH));
			break;
		default:
			break;
		}

		return new DatePeriod(startCal, endCal);
	}

	/**
	 * Calculates previous period, given the current one.
	 *
	 * @param period
	 * @return
	 */
	public DatePeriod calculatesPreviousPeriod(DatePeriod period) {
		Calendar startCal = (Calendar) period.getStartDate().clone();
		Calendar endCal = (Calendar) period.getEndDate().clone();

		switch (this.frequency) {
		case DAILY:
			startCal.add(Calendar.DAY_OF_MONTH, -1);
			endCal.add(Calendar.DAY_OF_MONTH, -1);
			break;
		case WEEKLY:
			startCal.add(Calendar.WEEK_OF_MONTH, -1);
			endCal.add(Calendar.WEEK_OF_MONTH, -1);
			break;
		case SEMIMONTLHY:
			if (startCal.get(Calendar.DAY_OF_MONTH) == 1) {
				startCal.add(Calendar.MONTH, -1);
				startCal.set(Calendar.DAY_OF_MONTH, 16);
				endCal.add(Calendar.MONTH, -1);
				endCal.set(Calendar.DAY_OF_MONTH, startCal.getActualMaximum(Calendar.DAY_OF_MONTH));
			} else if (startCal.get(Calendar.DAY_OF_MONTH) == 16) {
				startCal.set(Calendar.DAY_OF_MONTH, 1);
				endCal.set(Calendar.DAY_OF_MONTH, 15);
			} else {
				throw new IllegalArgumentException(
						String.format(EXCEPTION_MSG, new SimpleDateFormat().format(startCal.getTime())));
			}
			break;
		case FORTNIGHT:
			if (startCal.get(Calendar.DAY_OF_MONTH) == 11) {
				startCal.add(Calendar.MONTH, -1);
				startCal.set(Calendar.DAY_OF_MONTH, 26);
				endCal.set(Calendar.DAY_OF_MONTH, 10);
			} else if (startCal.get(Calendar.DAY_OF_MONTH) == 26) {
				startCal.set(Calendar.DAY_OF_MONTH, 11);
				endCal.add(Calendar.MONTH, -1);
				endCal.set(Calendar.DAY_OF_MONTH, 25);
			} else {
				throw new IllegalArgumentException(
						String.format(EXCEPTION_MSG, new SimpleDateFormat().format(startCal.getTime())));
			}
			break;
		case MONTHLY:
			startCal.add(Calendar.MONTH, -1);
			endCal.add(Calendar.MONTH, -1);
			endCal.set(Calendar.DAY_OF_MONTH, startCal.getActualMaximum(Calendar.DAY_OF_MONTH));
			break;
		default:
			break;
		}
		return new DatePeriod(startCal, endCal);
	}

	/**
	 * Calculates the current period, given the current date; depending on the type of frequency the current period is different.
	 *
	 * @return
	 */
	public DatePeriod getCurrentDatePeriod() {

		Calendar startCal = Calendar.getInstance();
		startCal.setFirstDayOfWeek(Calendar.MONDAY);
		startCal.setTime(this.date);

		Calendar endCal = Calendar.getInstance();
		endCal.setFirstDayOfWeek(Calendar.MONDAY);
		endCal.setTime(this.date);

		switch (this.frequency) {
		case WEEKLY:
			startCal.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
			endCal.setTime(startCal.getTime());
			endCal.add(Calendar.DAY_OF_MONTH, +6);
			break;
		case SEMIMONTLHY:
			if (startCal.get(Calendar.DAY_OF_MONTH) <= 15) {
				startCal.set(Calendar.DAY_OF_MONTH, 1);
				endCal.set(Calendar.DAY_OF_MONTH, 15);
			} else {
				startCal.set(Calendar.DAY_OF_MONTH, 16);
				endCal.set(Calendar.DAY_OF_MONTH, endCal.getActualMaximum(Calendar.DAY_OF_MONTH));
			}
			break;
		case FORTNIGHT:
			if (startCal.get(Calendar.DAY_OF_MONTH) <= 10) {
				startCal.add(Calendar.MONTH, -1);
				startCal.set(Calendar.DAY_OF_MONTH, 26);
				endCal.set(Calendar.DAY_OF_MONTH, 10);
			} else if (startCal.get(Calendar.DAY_OF_MONTH) <= 25) {
				startCal.set(Calendar.DAY_OF_MONTH, 11);
				endCal.set(Calendar.DAY_OF_MONTH, 25);
			} else {
				startCal.set(Calendar.DAY_OF_MONTH, 26);
				endCal.add(Calendar.MONTH, +1);
				endCal.set(Calendar.DAY_OF_MONTH, 10);
			}
			break;
		case MONTHLY:
			startCal.set(Calendar.DAY_OF_MONTH, 1);
			endCal.set(Calendar.DAY_OF_MONTH, endCal.getActualMaximum(Calendar.DAY_OF_MONTH));
			break;
		case DAILY:
		default:
			// do nothing period as good as is
			break;
		}
		return new DatePeriod(startCal, endCal);
	}

}
