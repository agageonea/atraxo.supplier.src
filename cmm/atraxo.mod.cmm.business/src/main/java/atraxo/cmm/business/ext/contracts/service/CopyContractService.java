package atraxo.cmm.business.ext.contracts.service;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.ad.business.api.attachment.IAttachmentService;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.ext.bid.IBidVersionService;
import atraxo.cmm.business.api.tender.ITenderLocationAirlinesService;
import atraxo.cmm.business.ext.contracts.delegate.CopyContractUtils;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.tender.TenderDefaultConstants;
import atraxo.cmm.business.ext.utils.NewBidOption;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractScope;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.InvoicePayableBy;
import atraxo.cmm.domain.impl.cmm_type.IsSpot;
import atraxo.cmm.domain.impl.cmm_type.Period;
import atraxo.cmm.domain.impl.cmm_type.ReviewPeriod;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.business.api.notes.INotesService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.util.EntityCloner;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.history.ChangeHistory;
import atraxo.fmbas.domain.impl.notes.Notes;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

public class CopyContractService extends AbstractBusinessBaseService {

	private static final Logger LOG = LoggerFactory.getLogger(CopyContractService.class);

	public static final String BLUEPRINT_CODE_SUFIX = "_DRAFT";
	private static final String CODE = "code";
	private static final String VALID_TO = "validTo";
	private static final String VALID_FROM = "validFrom";
	private static final String IS_BLUEPRINT = "isBlueprint";
	private static final String PERIOD_APPROVAL_STATUS = "periodApprovalStatus";
	private static final String PRICE_APPROVAL_STATUS = "priceApprovalStatus";
	private static final String SHIP_TO_APPROVAL_STATUS = "shipToApprovalStatus";
	private static final String BLUEPRINT_ORIGINAL_CONTRACT_REFERENCE = "blueprintOriginalContractReference";
	private static final String SHIP_TO = "shipTo";
	private static final String PRICING_BASES = "pricingBases";
	private static final String PRICE_CATEGORIES = "priceCategories";
	private static final String OBJECT_ID = "objectId";
	private static final String OBJECT_TYPE = "objectType";
	private static final String RESALE_REF = "resaleRef";

	private static final String TARGET_REF_ID = "targetRefid";
	private static final String TARGET_ALIAS = "targetAlias";

	@Autowired
	private ICustomerService customerService;
	@Autowired
	private IContractService contractService;
	@Autowired
	private IBidVersionService bidVersionService;
	@Autowired
	private IChangeHistoryService historyService;
	@Autowired
	private IAttachmentService attachmentService;
	@Autowired
	private ISystemParameterService systemParamsService;
	@Autowired
	private IUnitService unitService;
	@Autowired
	private INotesService notesService;
	@Autowired
	private ITenderLocationAirlinesService tenderLocationAirlinesService;

	/**
	 * Generates the sell contract from a purchase
	 *
	 * @param contract
	 * @param company
	 * @return
	 * @throws BusinessException
	 */
	public Contract generateContract(Contract contract, AbstractEntity company) throws BusinessException {
		if (contract.getDealType().equals(DealType._SELL_)) {
			return null;
		}
		Contract generatedContract = this.copyContract(contract, company, DealType._SELL_, contract, (Customer) company);
		return this.saveNewContract(company, DealType._SELL_, generatedContract);
	}

	public Contract generateInternalResale(Contract originalContract) throws BusinessException {
		if (DealType._SELL_.equals(originalContract.getDealType())) {
			return null;
		}
		try {
			Contract contract = this.generateContract(originalContract, originalContract.getHolder(), DealType._SELL_, originalContract,
					originalContract.getRiskHolder(), true);
			this.assignNewShipTo(contract);
			contract.setHolder(this.customerService.findById(originalContract.getSupplier().getId()));
			contract.setCustomer(originalContract.getHolder());
			contract.setSupplier(null);
			contract.setReadOnly(true);
			return this.saveNewContract(contract.getCustomer(), DealType._SELL_, contract);
		} catch (BeansException e) {
			LOG.info("Sale contract generation problem.", e);
		}
		return null;
	}

	/**
	 * Create a copy of the original contract and set it as blueprint.
	 *
	 * @param original
	 * @return
	 * @throws BusinessException
	 */
	public Contract generateBlueprintSaleContract(Contract original) throws BusinessException {
		try {
			Contract blueprint = this.generateContract(original, original.getHolder(), DealType._SELL_, null, original.getRiskHolder(), true);
			blueprint.setIsBlueprint(true);
			blueprint.setStatus(original.getStatus());
			blueprint.setCustomer(original.getCustomer());
			blueprint.setRiskHolder(original.getRiskHolder());
			blueprint.setCode(original.getCode() + BLUEPRINT_CODE_SUFIX);
			blueprint.setBidApprovalStatus(original.getBidApprovalStatus());
			blueprint.setPeriodApprovalStatus(BidApprovalStatus._NEW_);
			blueprint.setShipToApprovalStatus(BidApprovalStatus._NEW_);
			blueprint.setPriceApprovalStatus(BidApprovalStatus._NEW_);
			blueprint.setBlueprintOriginalContractReference(original.getId());
			this.copyShipTo(blueprint, original);
			this.contractService.insertWithoutBusinessLogic(blueprint);

			// Copy childs (history,attachments)
			this.copyHistory(original, blueprint);
			this.copyAttachments(original, blueprint);

			// Update original with blueprint reference
			Contract originalDB = this.contractService.findById(original.getId());
			originalDB.setBlueprintOriginalContractReference(blueprint.getId());
			this.contractService.update(originalDB);

			return blueprint;
		} catch (BeansException e) {
			throw new BusinessException(CmmErrorCode.SELL_CONTRACT_COULD_NOT_CREATE_DRAFT,
					CmmErrorCode.SELL_CONTRACT_COULD_NOT_CREATE_DRAFT.getErrMsg(), e);
		}
	}

	/**
	 * Merge blueprint contract into the original contract (only base properties, not childs). The following ignored fields are set {code, readOnly,
	 * isBlueprint, blueprintOriginalContractReference, validFrom, validTo}
	 *
	 * @param blueprint
	 * @param original
	 * @throws BusinessException
	 */
	public void mergeBlueprintToOriginalSaleContract(Contract blueprint, Contract original) throws BusinessException {
		List<Field> ignoreFields = new ArrayList<>();

		try {
			// specify ignored fields
			ignoreFields.add(Contract.class.getDeclaredField(CODE));
			ignoreFields.add(Contract.class.getDeclaredField(IS_BLUEPRINT));
			ignoreFields.add(Contract.class.getDeclaredField(BLUEPRINT_ORIGINAL_CONTRACT_REFERENCE));
			ignoreFields.add(Contract.class.getDeclaredField(VALID_FROM));
			ignoreFields.add(Contract.class.getDeclaredField(VALID_TO));

			ignoreFields.add(Contract.class.getDeclaredField(PERIOD_APPROVAL_STATUS));
			ignoreFields.add(Contract.class.getDeclaredField(PRICE_APPROVAL_STATUS));
			ignoreFields.add(Contract.class.getDeclaredField(SHIP_TO_APPROVAL_STATUS));

			ignoreFields.add(Contract.class.getDeclaredField(PRICING_BASES));
			ignoreFields.add(Contract.class.getDeclaredField(PRICE_CATEGORIES));
			ignoreFields.add(Contract.class.getDeclaredField(SHIP_TO));
			ignoreFields.add(Contract.class.getDeclaredField(RESALE_REF));

			// Clone contract (blueprint -> original)
			EntityCloner.cloneEntity(blueprint, original, ignoreFields);

		} catch (NoSuchFieldException | SecurityException | BeansException e) {
			throw new BusinessException(CmmErrorCode.ENTITY_CLONER_INSTANTION_ERROR,
					String.format(CmmErrorCode.ENTITY_CLONER_INSTANTION_ERROR.getErrMsg(), "Could merge contract " + blueprint.getCode()), e);
		}
	}

	public void copyHistory(Contract fromContract, Contract toContract) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put(OBJECT_ID, fromContract.getId());
		params.put(OBJECT_TYPE, Contract.class.getSimpleName());

		List<ChangeHistory> fromContractHistories = this.historyService.findEntitiesByAttributes(params);
		List<ChangeHistory> toContractHistories = new ArrayList<>();

		try {
			ChangeHistory newContractHisory;
			for (ChangeHistory originalContractHistory : fromContractHistories) {
				newContractHisory = EntityCloner.cloneEntity(originalContractHistory);
				newContractHisory.setCreatedAt(originalContractHistory.getCreatedAt());
				newContractHisory.setCreatedBy(originalContractHistory.getCreatedBy());
				newContractHisory.setObjectId(toContract.getId());
				toContractHistories.add(newContractHisory);
			}
		} catch (BeansException e) {
			throw new BusinessException(CmmErrorCode.ENTITY_CLONER_INSTANTION_ERROR, String.format(
					CmmErrorCode.ENTITY_CLONER_INSTANTION_ERROR.getErrMsg(), "Could not copy history for contract " + fromContract.getCode()), e);
		}

		this.historyService.insert(toContractHistories);
	}

	public void copyAttachments(Contract fromContract, Contract toContract) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put(TARGET_REF_ID, fromContract.getId());
		params.put(TARGET_ALIAS, Contract.class.getSimpleName());

		List<Attachment> originalAttachments = this.attachmentService.findEntitiesByAttributes(params);

		try {
			Attachment newAttachment;
			for (Attachment originalAttachment : originalAttachments) {
				newAttachment = EntityCloner.cloneEntity(originalAttachment, new String[] { "id" });
				newAttachment.setTargetRefid(toContract.getId().toString());
				newAttachment.setRefid(originalAttachment.getRefid());

				this.attachmentService.insert(newAttachment);
			}
		} catch (BeansException e) {
			throw new BusinessException(CmmErrorCode.ENTITY_CLONER_INSTANTION_ERROR, String.format(
					CmmErrorCode.ENTITY_CLONER_INSTANTION_ERROR.getErrMsg(), "Could not copy attachments for contract " + fromContract.getCode()), e);
		}
	}

	public Contract copySaleContract(Contract contract, AbstractEntity company) throws BusinessException {
		return this.copyContract(contract, company, DealType._SELL_, null, contract.getRiskHolder());
	}

	public Contract copyBuyContract(Contract contract, AbstractEntity company) throws BusinessException {
		return this.copyContract(contract, company, DealType._BUY_, null, contract.getRiskHolder());
	}

	/**
	 * @param contract
	 * @return
	 * @throws BusinessException
	 */
	public Integer copyBid(Contract contract) throws BusinessException {
		try {
			Contract newContract = EntityCloner.cloneEntity(contract);
			newContract.setVersion(newContract.getVersion() + 1);
			this.contractService.insertWithoutBusinessLogic(newContract);
			Contract c = this.contractService.findByRefid(newContract.getRefid());
			return c.getId();
		} catch (BeansException e) {
			LOG.error("Error", e);
		}
		return null;
	}

	/**
	 * @param originalBid
	 * @param tenderLocation
	 * @param copyBidOption
	 * @return
	 * @throws BusinessException
	 */
	public Contract copySupplierBid(Contract originalBid, TenderLocation tenderLocation, NewBidOption copyBidOption) throws BusinessException {
		Contract newContract = null;
		try {
			if (LOG.isDebugEnabled()) {
				LOG.info("Start bid copy.");
			}

			Contract newBid = this.generateContract(originalBid, originalBid.getCustomer(), DealType._EMPTY_, null, originalBid.getRiskHolder(),
					false);

			// set tender
			newBid.setBidTenderIdentification(tenderLocation.getTender());

			newBid.setTax(tenderLocation.getTaxType());
			newBid.setFlightServiceType(tenderLocation.getFlightServiceType());
			newBid.setBidTenderLocation(tenderLocation);
			newBid.setLocation(tenderLocation.getLocation());
			newBid.setBidApprovalStatus(BidApprovalStatus._NEW_);
			newBid.setBidTransmissionStatus(TransmissionStatus._NEW_);
			newBid.setBidCancelTransmitionStatus(TransmissionStatus._NEW_);
			newBid.setBidAcceptAwardTransmissionStatus(TransmissionStatus._NEW_);
			newBid.setBidDeclineAwardTransmissionStatus(TransmissionStatus._NEW_);
			newBid.setBidStatus(BidStatus._DRAFT_);
			newBid.setShipTo(null);
			newBid.setHolder(originalBid.getHolder());
			newBid.setContact(originalBid.getContact());
			newBid.setValidFrom(tenderLocation.getAgreementFrom());
			newBid.setValidTo(tenderLocation.getAgreementTo());
			newBid.setBidValidFrom(tenderLocation.getTender().getBiddingPeriodFrom());
			newBid.setBidValidTo(tenderLocation.getTender().getBiddingPeriodTo());
			newBid.setBidHasTotalVolume(tenderLocation.getHasTotalVolume());
			newBid.setProduct(tenderLocation.getFuelProduct());

			if (!tenderLocation.getDeliveryPoint().equals(ContractSubType._EMPTY_)) {
				newBid.setSubType(tenderLocation.getDeliveryPoint());
			} else {
				newBid.setSubType(ContractSubType.getByCode(TenderDefaultConstants.DEFAULT_CONTRACT_SUB_TYPE));
			}

			this.adjustPriceBasesPeriod(newBid);
			this.setVolumeBasedProperties(originalBid, newBid);

			if (copyBidOption.equals(NewBidOption.NEW_REVISION)) {
				newBid.setBidVersion(newBid.getBidVersion());
				Integer lastRevision = this.calculateBidRevision(tenderLocation, originalBid.getBidVersion());
				newBid.setBidRevision(lastRevision + 1);
			} else if (copyBidOption.equals(NewBidOption.NEW_LOCATION) || copyBidOption.equals(NewBidOption.NEW_VERSION)) {
				newBid.setBidRevision(1);
				this.setBidVersion(newBid, tenderLocation);
			}

			newContract = this.saveNewContract(originalBid.getCustomer(), newBid.getDealType(), newBid);

			// also, copy notes & documents AFTER bid is persisted (so it has a designated DB ID)
			this.copyNotes(originalBid, newContract);
			this.copyAttachments(originalBid, newContract);

		} catch (BeansException e) {
			LOG.warn("Copy bid problem", e);
		}

		if (LOG.isDebugEnabled()) {
			LOG.info("End bid copy.");
		}
		return newContract;
	}

	/**
	 * @param originalBid
	 * @param newBid
	 * @throws BusinessException
	 */
	private void copyNotes(Contract originalBid, Contract newBid) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put(OBJECT_ID, originalBid.getId());
		params.put(OBJECT_TYPE, originalBid.getClass().getSimpleName());
		List<Notes> notes = this.notesService.findEntitiesByAttributes(params);
		if (!notes.isEmpty()) {
			List<Notes> newNotes = new ArrayList<>();
			for (Notes note : notes) {
				Notes newNote = new Notes();
				newNote.setObjectId(newBid.getId());
				newNote.setObjectType(newBid.getClass().getSimpleName());
				newNote.setNotes(note.getNotes());
				newNotes.add(newNote);
			}
			this.notesService.insert(newNotes);
		}
	}

	/**
	 * @param tenderLocation
	 * @param bidVersion
	 * @return
	 * @throws BusinessException
	 */
	private Integer calculateBidRevision(TenderLocation tenderLocation, String bidVersion) throws BusinessException {
		return this.contractService.calculateRevisionVersion((List<Contract>) tenderLocation.getBids(), bidVersion);
	}

	/**
	 * @param contract
	 * @param tenderLocation
	 * @throws BusinessException
	 */
	private void setBidVersion(Contract contract, TenderLocation tenderLocation) {
		String bidVersion = this.bidVersionService.calculateBidVersion((List<Contract>) tenderLocation.getBids());
		contract.setBidVersion(bidVersion);
	}

	/**
	 * @param originalBid
	 * @param newBid
	 * @throws BusinessException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	private void setVolumeBasedProperties(Contract originalBid, Contract newBid) throws BusinessException {

		Double density = Double.parseDouble(this.systemParamsService.getDensity());

		Boolean sameLocation = newBid.getBidTenderLocation().equals(originalBid.getBidTenderLocation());
		BigDecimal bidOfferedVolume = sameLocation ? originalBid.getOfferedVolume() : newBid.getBidTenderLocation().getVolume();
		BigDecimal convertedVolume = this.unitService.convert(newBid.getBidTenderLocation().getVolumeUnit(), newBid.getSettlementUnit(),
				bidOfferedVolume, density);

		newBid.setAwardedVolume(null);
		newBid.setOfferedVolume(convertedVolume);
		newBid.setPeriod(newBid.getBidTenderLocation().getPeriodType());
		newBid.setContractVolumeUnit(newBid.getBidTenderLocation().getVolumeUnit());

		// volume information, fuel receivers
		if (sameLocation) {
			this.copyShipTo(newBid, originalBid);
		} else {
			// get ONLY the tender location airlines that are valid
			Map<String, Object> params = new HashMap<>();
			params.put("tenderLocation", newBid.getBidTenderLocation());
			params.put("isValid", Boolean.TRUE);
			List<TenderLocationAirlines> tenderLocationAirlines = this.tenderLocationAirlinesService.findEntitiesByAttributes(params);

			this.copyShipToFromTenderLocation(newBid, density, tenderLocationAirlines);
		}
	}

	/**
	 * @param newBid
	 * @param density
	 * @param tenderLocationAirlines
	 * @throws BusinessException
	 */
	private void copyShipToFromTenderLocation(Contract newBid, Double density, List<TenderLocationAirlines> tenderLocationAirlines)
			throws BusinessException {
		BigDecimal convertedVolume;
		if (!CollectionUtils.isEmpty(tenderLocationAirlines)) {
			for (TenderLocationAirlines tenderLocationAirline : tenderLocationAirlines) {
				if (tenderLocationAirline.getAirline() != null) {
					ShipTo bidAirlineVolume = new ShipTo();

					// set offered volume from airline
					BigDecimal shipToOfferedVolume = tenderLocationAirline.getVolume();

					convertedVolume = this.unitService.convert(newBid.getBidTenderLocation().getVolumeUnit(), newBid.getSettlementUnit(),
							shipToOfferedVolume, density);

					Customer airline = this.customerService.findById(tenderLocationAirline.getAirline().getId());
					bidAirlineVolume.setCustomer(airline);
					bidAirlineVolume.setOfferedVolume(convertedVolume);
					bidAirlineVolume.setTenderBidVolume(convertedVolume);

					bidAirlineVolume.setContract(newBid);
					bidAirlineVolume.setValidTo(newBid.getBidTenderLocation().getAgreementTo());
					bidAirlineVolume.setValidFrom(newBid.getBidTenderLocation().getAgreementFrom());

					newBid.addToShipTo(bidAirlineVolume);
				}
			}
		}
	}

	private void adjustPriceBasesPeriod(Contract contract) {
		List<PricingBase> pbList = new ArrayList<>(contract.getPricingBases());
		pbList.sort((o1, o2) -> o1.getValidFrom().compareTo(o2.getValidTo()));
		Iterator<PricingBase> iter = pbList.iterator();
		while (iter.hasNext()) {
			PricingBase pb = iter.next();
			if (pb.getValidTo().before(contract.getValidFrom())) {
				iter.remove();
			} else if (pb.getValidFrom().after(contract.getValidTo())) {
				iter.remove();
			} else if (pb.getValidFrom().compareTo(contract.getValidFrom()) <= 0 && pb.getValidTo().compareTo(contract.getValidFrom()) >= 0) {
				pb.setValidFrom(contract.getValidFrom());
			} else if (pb.getValidFrom().compareTo(contract.getValidTo()) <= 0 && pb.getValidTo().compareTo(contract.getValidTo()) >= 0) {
				pb.setValidTo(contract.getValidTo());
			}
		}
		contract.setPricingBases(pbList);
	}

	public Contract genearetSaleContractFromBid(Contract originalContract) {
		try {
			Contract newContract = this.generateContract(originalContract, originalContract.getCustomer(), DealType._SELL_, null,
					originalContract.getRiskHolder(), false);
			this.assignNewShipTo(newContract);

			newContract.setBidReference(originalContract);
			newContract.setIsContract(true);
			newContract.setBidStatus(BidStatus._EMPTY_);
			newContract.setBidTransmissionStatus(TransmissionStatus._EMPTY_);
			newContract.setBidRevision(null);
			newContract.setBidVersion(null);
			newContract.setBidTenderIdentification(null);
			newContract.setBidTenderLocation(null);
			newContract.setBidApprovalStatus(BidApprovalStatus._NEW_);
			if (newContract.getScope() == null || ContractScope._EMPTY_.equals(newContract.getScope())) {
				newContract.setScope(ContractScope._INTO_PLANE_);
			}
			if (newContract.getIsSpot() == null || IsSpot._EMPTY_.equals(newContract.getIsSpot())) {
				newContract.setIsSpot(IsSpot._TERM_);
			}
			if (newContract.getTax() == null || TaxType._EMPTY_.equals(newContract.getTax())) {
				newContract.setTax(TaxType._UNSPECIFIED_);
			}
			if (newContract.getReviewPeriod() == null || ReviewPeriod._EMPTY_.equals(newContract.getReviewPeriod())) {
				newContract.setReviewPeriod(ReviewPeriod._NO_);
			}
			if (newContract.getCreditTerms() == null || CreditTerm._EMPTY_.equals(newContract.getCreditTerms())) {
				newContract.setCreditTerms(CreditTerm._OPEN_CREDIT_);
			}
			if (newContract.getPaymentRefDay() == null || PaymentDay._EMPTY_.equals(newContract.getPaymentRefDay())) {
				newContract.setPaymentRefDay(PaymentDay._INVOICE_DATE_);
			}
			if (InvoiceFreq._EVERY_3_DAYS_.equals(newContract.getInvoiceFreq())) {
				newContract.setInvoiceFreq(InvoiceFreq._EVERY_);
				newContract.setInvoiceFreqNumber(3);
			}
			if (InvoiceFreq._EVERY_10_DAYS_.equals(newContract.getInvoiceFreq())) {
				newContract.setInvoiceFreq(InvoiceFreq._EVERY_);
				newContract.setInvoiceFreqNumber(10);
			}

			newContract.setShipTo(null);
			this.copyShipTo(newContract, originalContract);
			return this.saveNewContract(originalContract.getCustomer(), DealType._SELL_, newContract);
		} catch (BeansException | BusinessException e) {
			LOG.info("Sale contract generation problem.", e);
		}
		return null;
	}

	/**
	 * @param contract
	 * @param company
	 * @param dealType
	 * @param resaleRef
	 * @param riskHolder
	 * @return
	 * @throws BusinessException
	 */
	private Contract copyContract(Contract contract, AbstractEntity company, DealType dealType, Contract resaleRef, Customer riskHolder)
			throws BusinessException {
		contract.setSupplier(null);
		try {
			Contract newContract = this.generateContract(contract, company, dealType, resaleRef, riskHolder, false);
			this.assignNewShipTo(newContract);
			return newContract;
		} catch (BeansException e) {
			LOG.error("Error", e);
		}
		return null;
	}

	/**
	 * @param company
	 * @param dealType
	 * @param newContract
	 * @return
	 * @throws BusinessException
	 */
	public Contract saveNewContract(AbstractEntity company, DealType dealType, Contract newContract) throws BusinessException {
		this.contractService.insertWithoutBusinessLogic(newContract);
		if (newContract.getCode() == null || newContract.getCode().matches("")) {
			Contract c = this.contractService.findByRefid(newContract.getRefid());
			int n = String.valueOf(c.getId()).length();
			StringBuilder code = new StringBuilder();
			if (DealType._BUY_.equals(dealType)) {
				code.append("PC");
			} else if (DealType._SELL_.equals(dealType)) {
				code.append("SC");
				c.setBillTo((Customer) company);
				c.setInvoicePayableBy(InvoicePayableBy._EMPTY_);
			} else if (DealType._EMPTY_.equals(dealType)) {
				code.append("NC");
			}
			for (int i = 1; i <= 5 - n; ++i) {
				code.append("0");
			}
			c.setCode(code.toString() + c.getId());
			this.contractService.update(c);
			return c;
		}
		return null;
	}

	private Contract generateContract(Contract contract, AbstractEntity company, DealType dealType, Contract resaleRef, Customer riskHolder,
			boolean keepPriceReferance) throws BusinessException {

		Contract newContract = EntityCloner.cloneEntity(contract);
		newContract.setPriceCategories(null);
		newContract.setPricingBases(null);
		newContract.setContact(null);
		newContract.setCode("");
		newContract.setResaleRef(resaleRef);
		newContract.setStatus(ContractStatus._DRAFT_);
		newContract.setShipTo(null);
		newContract.setOfferedVolume(null);
		newContract.setPeriod(Period._CONTRACT_PERIOD_);
		newContract.setVolumeTolerance(null);
		newContract.setVolumeShare(null);
		newContract.setDealType(dealType);
		newContract.setReadOnly(false);
		newContract.setBidApprovalStatus(BidApprovalStatus._NEW_);
		newContract.setPeriodApprovalStatus(BidApprovalStatus._NEW_);
		newContract.setPriceApprovalStatus(BidApprovalStatus._NEW_);
		newContract.setShipToApprovalStatus(BidApprovalStatus._NEW_);
		newContract.setHardCopy(false);
		newContract.setPeriod(contract.getPeriod());
		newContract.setBlueprintOriginalContractReference(null);
		if (DealType._BUY_.equals(dealType)) {
			newContract.setSupplier((Suppliers) company);
			newContract.setRiskHolder(riskHolder);
		} else if (DealType._SELL_.equals(dealType)) {
			newContract.setCustomer((Customer) company);
			newContract.setRiskHolder((Customer) company);
		}
		newContract.setPricingBases(this.getPricingBases(contract, newContract, keepPriceReferance));

		if (resaleRef != null) {
			this.copyShipTo(newContract, resaleRef);
		}
		return newContract;
	}

	/**
	 * Create and assign a new ship to to the contract, where the customer is the customer from the contract and the validity period is the same with
	 * the contract.
	 *
	 * @param contract {@link Contract}.
	 */
	public void assignNewShipTo(Contract contract) {
		if (!DealType._SELL_.equals(contract.getDealType()) || contract.getCustomer() == null) {
			return;
		}
		if (contract.getShipTo() != null) {
			for (ShipTo shipTo : contract.getShipTo()) {
				if (shipTo.getCustomer().equals(contract.getCustomer())) {
					return;
				}
			}
		}
		ShipTo shipTo = new ShipTo();
		shipTo.setContract(contract);
		shipTo.setCustomer(contract.getCustomer());
		shipTo.setValidFrom(contract.getValidFrom());
		shipTo.setValidTo(contract.getValidTo());
		contract.addToShipTo(shipTo);
	}

	/**
	 * @param contract
	 * @param refContract
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public void copyShipTo(Contract contract, Contract refContract) {
		if (refContract.getShipTo() == null) {
			return;
		}
		for (ShipTo shipTo : refContract.getShipTo()) {
			ShipTo newShipTo = EntityCloner.cloneEntity(shipTo);
			newShipTo.setContract(contract);
			contract.addToShipTo(newShipTo);
		}
	}

	/**
	 * @param contract
	 * @param newContract
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws BusinessException
	 */
	public Collection<PricingBase> getPricingBases(Contract contract, Contract newContract, boolean isInternalResale) throws BusinessException {

		List<PricingBase> retList = new ArrayList<>();
		HashMap<Integer, ContractPriceCategory> map = new HashMap<>();
		List<PricingBase> usedPbList = new ArrayList<>();
		List<PricingBase> pbList = new ArrayList<>(contract.getPricingBases());

		Collections.sort(pbList, this::sortByContractPriceCategoryParent);

		for (PricingBase pb : pbList) {
			this.getPricingBase(newContract, retList, map, usedPbList, pb, isInternalResale, 0);
		}
		return retList;
	}

	/**
	 * @param o1
	 * @param o2
	 * @return
	 */
	private int sortByContractPriceCategoryParent(PricingBase o1, PricingBase o2) {
		ContractPriceCategory cpc1;
		ContractPriceCategory cpc2;
		if (o1.getContractPriceCategories() == null || o2.getContractPriceCategories() == null || o1.getContractPriceCategories().size() != 1
				|| o2.getContractPriceCategories().size() != 1) {
			return 0;
		} else {
			cpc1 = o1.getContractPriceCategories().iterator().next();
			cpc2 = o2.getContractPriceCategories().iterator().next();
		}
		if (CollectionUtils.isEmpty(cpc1.getParentPriceCategory()) && !CollectionUtils.isEmpty(cpc2.getParentPriceCategory())) {
			return -1;
		}
		if (CollectionUtils.isEmpty(cpc1.getParentPriceCategory()) && CollectionUtils.isEmpty(cpc2.getParentPriceCategory())) {
			return 1;
		}
		return 0;
	}

	private void getPricingBase(Contract newContract, Collection<PricingBase> retList, HashMap<Integer, ContractPriceCategory> map,
			List<PricingBase> usedPbList, PricingBase sourcePb, boolean isInternalResale, int counter) throws BusinessException {

		if (usedPbList.contains(sourcePb)) {
			return;
		}
		if (counter > 2) {
			throw new BusinessException(CmmErrorCode.MORE_THAN_TWO_PERCENTAGE, CmmErrorCode.MORE_THAN_TWO_PERCENTAGE.getErrMsg());
		}
		for (ContractPriceCategory cpc : sourcePb.getContractPriceCategories()) {
			for (ContractPriceCategory cpcParent : cpc.getParentPriceCategory()) {
				if (!map.containsKey(cpcParent.getId())) {
					this.getPricingBase(newContract, retList, map, usedPbList, cpcParent.getPricingBases(), isInternalResale, ++counter);
				}
			}
		}
		PricingBase newPb = EntityCloner.cloneEntity(sourcePb);
		newPb.setContract(newContract);
		newPb.setContractPriceCategories(CopyContractUtils.getPriceCategory(sourcePb, newPb, map, isInternalResale));
		usedPbList.add(sourcePb);
		retList.add(newPb);
	}
}
