package atraxo.cmm.business.ws.tender.transformer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.domain.ext.tender.MessageStatus;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.iata.fuelplus.iata.tender.Contact;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcknowledge;

public class TenderReadAcknowledgmentTransformer extends DataHubOutTransformer<Tender, FuelTenderAcknowledge> {
	@Autowired
	private IUserSuppService userService;

	@Override
	public FuelTenderAcknowledge transformModelToDTO(Tender modelEntity, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException {
		FuelTenderAcknowledge acknowledge = new FuelTenderAcknowledge();
		acknowledge.setTenderIdentification(this.buildTenderIdentification(modelEntity, msgId));

		Map<String, Object> params = new HashMap<>();
		params.put("isSubsidiary", true);
		params.put("code", Session.user.get().getClientCode());
		Customer subsidiary = this.customerService.findEntityByAttributes(params);

		if (subsidiary.getIataCode() != null) {
			acknowledge.setBidderCode(subsidiary.getIataCode());
		} else {
			acknowledge.setBidderCode(subsidiary.getCode());
		}
		String userCode = Session.user.get().getCode();
		UserSupp user = this.userService.findByCode(userCode);
		Contact contact = new Contact();
		contact.setContactPerson(user.getFirstName() + " " + user.getLastName());
		contact.setEmail(user.getEmail());
		contact.setPhone(user.getBusinessPhone());

		acknowledge.setContact(contact);
		acknowledge.setRefMsgId(refMsgId);
		String type = this.setMessageType(receiverInterface);
		acknowledge.setMessageType(type);
		acknowledge.setMessageStatus(seava.j4e.iata.fuelplus.iata.tender.MessageStatus.fromValue(MessageStatus.READ.getIataCode()));
		acknowledge.setRefMsgId(refMsgId);

		return acknowledge;
	}

	@Override
	public FuelTenderAcknowledge transformModelToDTO(Tender modelEntity, long msgId, String reason) throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

	@Override
	public FuelTenderAcknowledge transformModelsToDTO(List<Tender> modelEntities, long msgId, String reason) throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

	@Override
	public FuelTenderAcknowledge transformModelsToDTO(List<Tender> modelEntities, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

}
