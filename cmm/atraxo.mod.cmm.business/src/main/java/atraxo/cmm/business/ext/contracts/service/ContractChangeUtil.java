package atraxo.cmm.business.ext.contracts.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.cmm.domain.impl.shipTo.ShipTo;

/**
 * @author vhojda
 */
public class ContractChangeUtil {

	private static final String DATE_FORMAT = "yyyy-MM-dd";

	/**
	 *
	 */
	private ContractChangeUtil() {

	}

	/**
	 * Chesks if a specific <code>Contract</code> is a blueprint in order to insert/log new <code>ProductChange</code>s
	 *
	 * @param contract
	 * @return
	 */
	public static boolean checkBlueprintProductChanges(Contract contract) {
		return contract.getBlueprintOriginalContractReference() != null;
	}

	/**
	 * @param date
	 * @return
	 */
	private static String format(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		return sdf.format(date);
	}

	/**
	 * @param oldContract
	 * @param newContract
	 * @return
	 */
	public static String buildMessageForPeriodChange(Contract oldContract, Contract newContract) {
		StringBuilder sb = new StringBuilder();
		sb.append("Changed old period from ");
		sb.append(format(oldContract.getValidFrom()));
		sb.append(" : ");
		sb.append(format(oldContract.getValidTo()));
		sb.append(" to new period ");
		sb.append(format(newContract.getValidFrom()));
		sb.append(":");
		sb.append(format(newContract.getValidTo()));
		sb.append(".");
		return sb.toString();
	}

	/**
	 * @param newPrice
	 * @return
	 */
	public static String buildMessageForPriceInsert(PricingBase newPrice) {
		StringBuilder sb = new StringBuilder();
		sb.append("Inserted new price category with name '");
		sb.append(newPrice.getPriceCat().getName());
		sb.append("' for price category of type '");
		sb.append(newPrice.getPriceCat().getType());
		sb.append("' for period ");
		sb.append(format(newPrice.getValidFrom()));
		sb.append(" : ");
		sb.append(format(newPrice.getValidTo()));
		sb.append(".");
		return sb.toString();
	}

	/**
	 * @param oldPriceCategory
	 * @param newPriceCategory
	 * @return
	 */
	public static String buildMessageForPriceCategoryUpdate(ContractPriceCategory oldPriceCategory, ContractPriceCategory newPriceCategory) {
		StringBuilder sb = new StringBuilder();
		sb.append("Updated ");
		sb.append(getPriceCategoryDescriptor(newPriceCategory));
		sb.append("'.");
		sb.append(getChange(oldPriceCategory.getName(), newPriceCategory.getName(), "name"));
		sb.append(getChange(oldPriceCategory.getVat().getName(), newPriceCategory.getVat().getName(), "VAT applicability"));
		sb.append(getChange(oldPriceCategory.getQuantityType().getName(), newPriceCategory.getQuantityType().getName(), "quantity type"));
		sb.append(getChange(oldPriceCategory.getComments(), newPriceCategory.getComments(), "comments"));
		sb.append(getChange(oldPriceCategory.getRestriction().toString(), newPriceCategory.getRestriction().toString(), "apply restrictions"));
		return sb.toString();
	}

	/**
	 * @param oldPricingBase
	 * @param newPricingBase
	 * @return
	 */
	public static String buildMessageForPriceBaseUpdate(PricingBase oldPricingBase, PricingBase newPricingBase) {
		StringBuilder sbSuffix = new StringBuilder("");
		sbSuffix.append(getChange(oldPricingBase.getQuotation().getName(), newPricingBase.getQuotation().getName(), "quotation"));
		sbSuffix.append(getChange(oldPricingBase.getQuotationOffset().getName(), newPricingBase.getQuotationOffset().getName(), "period"));
		sbSuffix.append(getChange(oldPricingBase.getDecimals().toString(), newPricingBase.getDecimals().toString(), "decimals"));
		sbSuffix.append(getChange(oldPricingBase.getDescription(), newPricingBase.getDescription(), "description"));
		sbSuffix.append(getChange(oldPricingBase.getOperator().getName(), newPricingBase.getOperator().getName(), "operator"));
		sbSuffix.append(getChange(oldPricingBase.getFactor(), newPricingBase.getFactor(), "factor"));
		sbSuffix.append(getChange(oldPricingBase.getConvUnit().getName(), newPricingBase.getConvUnit().getName(), "conversion unit"));

		if (!StringUtils.isEmpty(sbSuffix.toString())) {
			StringBuilder sbPrefix = new StringBuilder();
			sbPrefix.append("Updated ");
			sbPrefix.append(getPricingBaseDescriptor(oldPricingBase));
			sbPrefix.append("'.");
			sbPrefix.append(sbSuffix.toString());
			return sbPrefix.toString();
		}

		return null;
	}

	/**
	 * @param contractPriceCateg
	 * @return
	 */
	public static String buildMessageForPriceDelete(ContractPriceCategory priceCateg) {
		StringBuilder sb = new StringBuilder();
		sb.append("Deleted ");
		sb.append(getPriceCategoryDescriptor(priceCateg));
		sb.append("'.");
		return sb.toString();
	}

	/**
	 * @param restriction
	 * @return
	 */
	public static String buildMessageForPriceRestrictionInsert(ContractPriceRestriction restriction) {
		ContractPriceCategory priceCateg = restriction.getContractPriceCategory();
		StringBuilder sb = new StringBuilder();
		sb.append("Updated ");
		sb.append(getPriceCategoryDescriptor(priceCateg));
		sb.append("'. ");
		sb.append("Added new price restriction ");
		sb.append(getPriceRestrictionDescriptor(restriction));
		sb.append(".");
		return sb.toString();

	}

	/**
	 * @param restriction
	 * @return
	 */
	public static String buildMessageForPriceRestrictionDelete(ContractPriceRestriction restriction) {
		ContractPriceCategory priceCateg = restriction.getContractPriceCategory();
		StringBuilder sb = new StringBuilder();
		sb.append("Updated ");
		sb.append(getPriceCategoryDescriptor(priceCateg));
		sb.append("'. ");
		sb.append("Deleted existing price restriction ");
		sb.append(getPriceRestrictionDescriptor(restriction));
		sb.append(".");
		return sb.toString();
	}

	/**
	 * @param oldRestriction
	 * @param newRestriction
	 * @return
	 */
	public static String buildMessageForPriceRestrictionUpdate(ContractPriceRestriction oldRestriction, ContractPriceRestriction newRestriction) {
		ContractPriceCategory priceCateg = newRestriction.getContractPriceCategory();
		StringBuilder sb = new StringBuilder();
		sb.append("Updated ");
		sb.append(getPriceCategoryDescriptor(priceCateg));
		sb.append("'. ");
		sb.append("Changed existing price restriction from ");
		sb.append(getPriceRestrictionDescriptor(oldRestriction));
		sb.append(" to ");
		sb.append(getPriceRestrictionDescriptor(newRestriction));
		sb.append(".");
		return sb.toString();
	}

	/**
	 * @param newShipTo
	 * @return
	 */
	public static String buildMessageForShipToInsert(ShipTo newShipTo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Inserted new ");
		sb.append(getShipToDescriptor(newShipTo));
		sb.append(".");
		return sb.toString();
	}

	/**
	 * @param shipTo
	 * @return
	 */
	public static String buildMessageForShipToDelete(ShipTo shipTo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Deleted existing ");
		sb.append(getShipToDescriptor(shipTo));
		sb.append(".");
		return sb.toString();
	}

	/**
	 * @param oldShipTo
	 * @param newShipTo
	 * @return
	 */
	public static String buildMessageForShipToUpdate(ShipTo oldShipTo, ShipTo newShipTo) {
		StringBuilder sb = new StringBuilder();
		sb.append("Updated existing ");
		sb.append(getShipToDescriptor(oldShipTo));
		sb.append(" to new ");
		sb.append(getShipToDescriptor(newShipTo));
		sb.append(".");
		return sb.toString();
	}

	public static String buildMessageForAttachmentToInsert(Attachment newAttachment, String contractCode) {
		StringBuilder sb = new StringBuilder();
		sb.append("Inserted new ");
		sb.append(getAttachmentToDescriptor(newAttachment, contractCode));
		sb.append(".");
		return sb.toString();
	}

	public static String buildMessageForAttachmentToDelete(Attachment newAttachment, String contractCode) {
		StringBuilder sb = new StringBuilder();
		sb.append("Deleted existing ");
		sb.append(getAttachmentToDescriptor(newAttachment, contractCode));
		sb.append(".");
		return sb.toString();
	}

	/**
	 * @param prop1
	 * @param prop2
	 * @param name
	 * @return
	 */
	private static String getChange(String prop1, String prop2, String name) {
		StringBuilder sb = new StringBuilder("");
		if (prop1 != null && prop2 != null && !prop1.equals(prop2)) {
			sb.append("Changed ");
			sb.append(name);
			sb.append(" from '");
			sb.append(prop1);
			sb.append("' to '");
			sb.append(prop2);
			sb.append("'. ");
		}
		return sb.toString();
	}

	private static String getChange(BigDecimal prop1, BigDecimal prop2, String name) {
		StringBuilder sb = new StringBuilder("");
		if (prop1 != null && prop2 != null) {
			BigDecimal p1 = prop1.setScale(12, BigDecimal.ROUND_UNNECESSARY);
			BigDecimal p2 = prop1.setScale(12, BigDecimal.ROUND_UNNECESSARY);
			if (!p1.equals(p2)) {
				sb.append("Changed ");
				sb.append(name);
				sb.append(" from '");
				sb.append(prop1.toString());
				sb.append("' to '");
				sb.append(prop2.toString());
				sb.append("'. ");
			}
		}
		return sb.toString();
	}

	/**
	 * @param priceCateg
	 * @return
	 */
	private static String getPriceCategoryDescriptor(ContractPriceCategory priceCateg) {
		StringBuilder sb = new StringBuilder();
		sb.append(" price category with name '");
		sb.append(priceCateg.getName());
		sb.append("' and type '");
		sb.append(priceCateg.getPriceCategory().getType().getName());
		return sb.toString();
	}

	/**
	 * @param priceCateg
	 * @return
	 */
	private static String getPricingBaseDescriptor(PricingBase pricingBase) {
		StringBuilder sb = new StringBuilder();
		sb.append(" price with name '");
		sb.append(pricingBase.getPriceCat().getName());
		sb.append("' and type '");
		sb.append(pricingBase.getPriceCat().getType().getName());
		return sb.toString();
	}

	/**
	 * @param shipTo
	 * @return
	 */
	private static String getShipToDescriptor(ShipTo shipTo) {
		StringBuilder sb = new StringBuilder();
		sb.append(" ship-to for contract '");
		sb.append(checkContractCode(shipTo.getContract().getCode()));
		sb.append("' and customer '");
		sb.append(shipTo.getCustomer().getName());
		sb.append("' with actual volume ");
		sb.append(shipTo.getActualVolume() != null ? shipTo.getActualVolume().toPlainString() : "N/A");
		sb.append(" for period ");
		sb.append(format(shipTo.getValidFrom()));
		sb.append(" : ");
		sb.append(format(shipTo.getValidTo()));
		return sb.toString();
	}

	/**
	 * @param r
	 * @return
	 */
	private static String getPriceRestrictionDescriptor(ContractPriceRestriction r) {
		StringBuilder sb = new StringBuilder("");
		sb.append("price restriction of type '");
		sb.append(r.getRestrictionType().getName());
		sb.append("', operation '");
		sb.append(r.getOperator().getName());
		sb.append("', value '");
		sb.append(r.getValue());
		if (r.getUnit() != null) {
			sb.append("' and unit '");
			sb.append(r.getUnit().getName());
		}
		sb.append("' ");
		return sb.toString();
	}

	private static String getAttachmentToDescriptor(Attachment attachment, String contractCode) {
		StringBuilder sb = new StringBuilder();
		sb.append("document for contract '").append(checkContractCode(contractCode)).append("'");
		sb.append(", type '").append(attachment.getType().getName()).append("'");
		sb.append(", name '").append(attachment.getName()).append("'");
		return sb.toString();
	}

	/**
	 * Removes the "_DRAFT" from a contract code if the case
	 *
	 * @param code
	 * @return
	 */
	public static String checkContractCode(String code) {
		if (code != null && code.endsWith(CopyContractService.BLUEPRINT_CODE_SUFIX)) {
			return code.split(CopyContractService.BLUEPRINT_CODE_SUFIX)[0];
		}
		return code;
	}

}
