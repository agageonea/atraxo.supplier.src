/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.ext.prices.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.contracts.IContractChangeService;
import atraxo.cmm.business.api.contracts.IPricingBaseService;
import atraxo.cmm.business.api.ext.contracts.IPriceBuilder;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.api.prices.IContractPriceComponentConvService;
import atraxo.cmm.business.ext.contracts.service.ContractChangeUtil;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.exceptions.DeleteCPCategoryException;
import atraxo.cmm.business.ext.exceptions.NoPriceComponentsException;
import atraxo.cmm.business.ext.prices.delegate.CompositePriceCategoryBd;
import atraxo.cmm.business.ext.prices.delegate.ContractPriceCategory_Bd;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.sys.delegate.SystemParameterDelegate;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.fmbas_type.ContractChangeType;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link ContractPriceCategory} domain entity.
 */
public class ContractPriceCategory_Service extends atraxo.cmm.business.impl.prices.ContractPriceCategory_Service
		implements IContractPriceCategoryService {

	private static final Logger LOG = LoggerFactory.getLogger(ContractPriceCategory_Service.class);
	private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);
	private static final String PERCENTAGE = "%";

	@Autowired
	private IPricingBaseService pbService;
	@Autowired
	private IContractPriceComponentConvService contractPriceComponentConvService;
	@Autowired
	PriceConverterService priceConverterService;
	@Autowired
	IContractChangeService contractChangeService;
	@Autowired
	private IPriceBuilder priceBuilder;

	@Override
	protected void preInsert(ContractPriceCategory e) throws BusinessException {
		super.preInsert(e);
		this.checkRestrictions(e);
	}

	@Override
	protected void preUpdate(ContractPriceCategory e) throws BusinessException {
		// "log" the new inserted price category change
		if (ContractChangeUtil.checkBlueprintProductChanges(e.getContract())) {
			ContractPriceCategory oldPriceCategory = this.findById(e.getId());
			this.contractChangeService.addNewChange(e.getContract(), ContractChangeType._PRICE_,
					ContractChangeUtil.buildMessageForPriceCategoryUpdate(oldPriceCategory, e));
		}
	}

	@Override
	protected void postUpdate(ContractPriceCategory e) throws BusinessException {
		this.priceBuilder.updateParentPriceCategories(e, CalculateIndicator._CALCULATE_ONLY_);
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void insert(List<ContractPriceCategory> list) throws BusinessException {
		super.insert(list);
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void update(List<ContractPriceCategory> list) throws BusinessException {
		super.update(list);
	}

	@Override
	public void updateAsIs(ContractPriceCategory e) throws BusinessException {
		super.onUpdate(e);
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void deleteByIds(List<Object> ids) throws BusinessException {
		super.deleteByIds(ids);
	}

	@Override
	public void insertWithoutPersist(ContractPriceCategory e) throws BusinessException {
		this.preInsert(e);
		this.postInsert(e);
	}

	@Override
	public void updateWithoutPersist(ContractPriceCategory e) throws BusinessException {
		this.preUpdate(e);
		this.postUpdate(e);
	}

	@Override
	protected void postInsert(ContractPriceCategory e) throws BusinessException {
		ContractPriceCategory_Bd bd = this.getBusinessDelegate(ContractPriceCategory_Bd.class);
		e.setPriceComponents(null);
		if (e.getPricingBases() != null && this.pbService.isIndex(e.getPricingBases())) {
			bd.generateContractPriceComponent(e, false);
		} else {
			bd.createContractPriceComponent(e);
		}
	}

	@Override
	public ContractPriceCategory findByResaleRef(ContractPriceCategory e) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("resaleRef", e.getId());
		List<ContractPriceCategory> list = this.findEntitiesByAttributes(params);
		Iterator<ContractPriceCategory> iter = list.iterator();
		while (iter.hasNext()) {
			ContractPriceCategory cpc = iter.next();
			if (!cpc.getContract().getReadOnly()) {
				iter.remove();
			}
		}
		if (list.size() == 1) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public ContractPriceCategory findBluePrintByResaleRef(ContractPriceCategory e) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("resaleRef", e.getId());
		List<ContractPriceCategory> list = this.findEntitiesByAttributes(params);
		if (list.size() == 1) {
			return list.get(0);
		}
		return null;
	}

	@Override
	protected void onDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		if (CollectionUtils.isEmpty(ids)) {
			return;
		}
		List<Object> pbList = new ArrayList<>();
		List<ContractPriceCategory> deleteList = new ArrayList<>();
		for (Object id : ids) {
			ContractPriceCategory cpc = this.findById(id);
			deleteList.add(cpc);
			if (cpc.getChildPriceCategory() != null && !cpc.getChildPriceCategory().isEmpty()) {
				throw new DeleteCPCategoryException();
			}
			pbList.add(cpc.getPricingBases().getId());
		}
		Contract c = deleteList.get(0).getContract();
		Map<String, Object> params = new HashMap<>();
		params.put("contract", c);
		List<ContractPriceCategory> pcList = this.findEntitiesByAttributes(ContractPriceCategory.class, params);
		this.getBusinessDelegate(CompositePriceCategoryBd.class).deleteCompositePriceCategory(deleteList, pcList);

		deleteList.forEach(cpc -> pcList.removeIf(cat -> (cat.getId() == cpc.getId())));
		pcList.sort((c1, c2) -> c1.getOrderIndex().compareTo(c2.getOrderIndex()));
		int i = 1;
		for (ContractPriceCategory cp : pcList) {
			cp.setOrderIndex(i++);
		}
		if (!CollectionUtils.isEmpty(pcList)) {
			this.onUpdate(pcList);
		}
		this.pbService.deleteByIds(pbList);
	}

	/**
	 * @param e
	 * @throws BusinessException
	 */
	private void checkRestrictions(ContractPriceCategory e) throws BusinessException {
		if (!e.getRestriction() && e.getPricingBases() == null) {
			Map<String, Object> params = new HashMap<>();
			params.put("priceCategory", e.getPriceCategory());
			params.put("contract", e.getContract());
			List<ContractPriceCategory> list = this.findEntitiesByAttributes(ContractPriceCategory.class, params);
			for (ContractPriceCategory cpc : list) {
				if (!cpc.getId().equals(e.getId()) && !cpc.getRestriction()) {
					throw new BusinessException(CmmErrorCode.PC_SAME, CmmErrorCode.PC_SAME.getErrMsg());
				}
			}
		}
	}

	@Override
	public ContractPriceComponent getPriceComponent(ContractPriceCategory priceCategory, Date date) throws BusinessException {
		if (priceCategory.getPriceComponents() == null || priceCategory.getPriceComponents().isEmpty()) {
			throw new NoPriceComponentsException(BusinessErrorCode.PRICE_COMPONENT_NOT_FOUND_FOR_PRICE_CATEGORY,
					String.format(BusinessErrorCode.PRICE_COMPONENT_NOT_FOUND_FOR_PRICE_CATEGORY.getErrMsg(), priceCategory.getName()));
		}
		List<ContractPriceComponent> priceComponents = new ArrayList<>(priceCategory.getPriceComponents());

		priceComponents.sort((o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()));
		date = DateUtils.truncate(date, Calendar.DATE);
		for (ContractPriceComponent priceComponent : priceComponents) {
			if (priceComponent.getValidFrom().compareTo(date) <= 0 && priceComponent.getValidTo().compareTo(date) >= 0) {
				return priceComponent;
			}
		}
		throw new NoPriceComponentsException(BusinessErrorCode.PRICE_COMPONENT_NOT_FOUND_FOR_PRICE_CATEGORY,
				String.format(BusinessErrorCode.PRICE_COMPONENT_NOT_FOUND_FOR_PRICE_CATEGORY.getErrMsg(), priceCategory.getName()));

	}

	@Override
	public BigDecimal getPriceInCurrencyUnit(ContractPriceCategory priceCategory, String unitCode, String currencyCode, Date date)
			throws BusinessException {
		BigDecimal price = BigDecimal.ZERO;
		PriceInd pricePer = priceCategory.getPriceCategory().getPricePer();
		if (pricePer.equals(PriceInd._PERCENT_) || PriceInd._COMPOSITE_.equals(pricePer)) {
			price = this.getPriceForComposite(priceCategory, unitCode, currencyCode, date, price, pricePer);
		} else {
			price = this.getPrice(priceCategory, unitCode, currencyCode, date);
		}
		return price;
	}

	/**
	 * Get price for percentage price category or composite price category.
	 *
	 * @param priceCategory
	 * @param unitCode
	 * @param currencyCode
	 * @param date
	 * @param price
	 * @param pricePer
	 * @return
	 * @throws BusinessException
	 */
	private BigDecimal getPriceForComposite(ContractPriceCategory priceCategory, String unitCode, String currencyCode, Date date, BigDecimal price,
			PriceInd pricePer) throws BusinessException {
		for (ContractPriceCategory category : priceCategory.getParentPriceCategory()) {
			if (!category.equals(priceCategory)) {
				if (category.getPriceCategory().getPricePer().equals(PriceInd._EVENT_)) {
					LOG.warn("Can not calculate price for contract price category \"" + priceCategory.getName()
							+ "\" because the parent contract price category \"" + category.getName() + "\" is EVENT based.");
					price = BigDecimal.ZERO;
					break;
				}

				try {
					price = price.add(this.getPriceInCurrencyUnit(category, unitCode, currencyCode, date));
				} catch (NoPriceComponentsException e) {
					LOG.info("Price Component not found for " + priceCategory.getName() + " for date " + date, e);
				}
			}
		}
		try {
			ContractPriceComponent comp = this.getPriceComponent(priceCategory, date);
			if (pricePer.equals(PriceInd._PERCENT_)) {
				price = price.multiply(comp.getPrice().divide(ONE_HUNDRED, MathContext.DECIMAL64), MathContext.DECIMAL64);
			}
		} catch (NoPriceComponentsException e) {
			LOG.info("Price Component not found for " + priceCategory.getName() + " for date " + date, e);
		}
		return price;
	}

	private BigDecimal getPrice(ContractPriceCategory priceCategory, String unitCode, String currencyCode, Date date) throws BusinessException {
		ContractPriceComponent priceComponent = this.getPriceComponent(priceCategory, date);
		BigDecimal price = this.getConvertedPrice(unitCode, currencyCode, priceComponent);
		if (price.equals(BigDecimal.ZERO)) {
			if (unitCode.equals(PERCENTAGE)) {
				return priceComponent.getConvertedPrices().iterator().next().getEquivalent();
			}
			if (this.isIndex(priceComponent)) {
				price = this.convertPrice(priceComponent.getUnit().getCode(), unitCode, priceComponent.getCurrency().getCode(), currencyCode,
						priceComponent.getPrice(), priceCategory.getFinancialSource().getId(), priceCategory.getAverageMethod().getId(),
						priceCategory.getPricingBases(), priceCategory.getExchangeRateOffset(), date);
			} else {
				price = this.convertPrice(priceComponent.getUnit().getCode(), unitCode, priceComponent.getCurrency().getCode(), currencyCode,
						priceComponent.getPrice(), priceCategory.getFinancialSource().getId(), priceCategory.getAverageMethod().getId(),
						priceCategory.getExchangeRateOffset(), date);
			}
		}
		return price;
	}

	private boolean isIndex(ContractPriceComponent priceComponent) throws BusinessException {
		if (priceComponent.getContrPriceCtgry().getPricingBases() != null) {
			return this.pbService.isIndex(priceComponent.getContrPriceCtgry().getPricingBases());
		}
		return false;
	}

	private BigDecimal convertPrice(String fromUnitCode, String toUnitCode, String fromCurrencyCode, String toCurrencyCode, BigDecimal price,
			Integer finSrcId, Integer avgMthdId, PricingBase pricingBase, MasterAgreementsPeriod offset, Date date) throws BusinessException {
		return this.priceConverterService.convert(fromUnitCode, toUnitCode, fromCurrencyCode, toCurrencyCode, price, date, finSrcId.intValue(),
				avgMthdId.intValue(), pricingBase, false, offset).getValue();

	}

	private BigDecimal getConvertedPrice(String unitCode, String currencyCode, ContractPriceComponent priceComponent) throws BusinessException {
		Unit unit = ((IUnitService) this.findEntityService(Unit.class)).findByCode(unitCode);
		Currencies currency = ((ICurrenciesService) this.findEntityService(Currencies.class)).findByCode(currencyCode);
		Map<String, Object> params = new HashMap<>();
		params.put("contractPriceComponent", priceComponent);
		params.put("equivalentUnit", unit);
		params.put("equivalentCurrency", currency);
		List<ContractPriceComponentConv> list = this.contractPriceComponentConvService.findEntitiesByAttributes(ContractPriceComponentConv.class,
				params);
		BigDecimal price = BigDecimal.ZERO;
		Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH);
		for (ContractPriceComponentConv priceComponentConv : list) {
			if ((date.after(priceComponentConv.getValidFrom()) || date.equals(priceComponentConv.getValidFrom()))
					&& (date.before(priceComponentConv.getValidTo()) || date.equals(priceComponentConv.getValidTo()))) {
				price = priceComponentConv.getEquivalent();
				break;
			}
		}
		return price;
	}

	@Override
	public BigDecimal convertPrice(String fromUnitCode, String toUnitCode, String fromCurrencyCode, String toCurrencyCode, BigDecimal price,
			Integer finSrcId, Integer avgMthdId, MasterAgreementsPeriod offset, Date date) throws BusinessException {
		Density density = this.getBusinessDelegate(SystemParameterDelegate.class).getSystemDensity();
		return this.convertPrice(fromUnitCode, toUnitCode, fromCurrencyCode, toCurrencyCode, price, finSrcId, avgMthdId, offset, density, date);
	}

	private BigDecimal convertPrice(String fromUnitCode, String toUnitCode, String fromCurrencyCode, String toCurrencyCode, BigDecimal price,
			Integer finSrcId, Integer avgMthdId, MasterAgreementsPeriod offset, Density density, Date date) throws BusinessException {

		return this.priceConverterService.convert(fromUnitCode, toUnitCode, fromCurrencyCode, toCurrencyCode, price, date, finSrcId.intValue(),
				avgMthdId.intValue(), density, false, offset).getValue();
	}

	@Override
	public List<ContractPriceCategory> getDefaultContractPriceCategories(Integer priceCategoryId, Integer contractId) throws BusinessException {
		String hql = "select e from " + ContractPriceCategory.class.getSimpleName()
				+ " e where e.clientId =:clientId and e.contract.id = :contractId and e.priceCategory.id=:priceCategoryId and e.defaultPriceCtgy =true";
		return this.getEntityManager().createQuery(hql, ContractPriceCategory.class).setParameter("contractId", contractId)
				.setParameter("clientId", Session.user.get().getClientId()).setParameter("priceCategoryId", priceCategoryId).getResultList();
	}

	/**
	 * @param e {@link ContractPriceCategory}.
	 * @return - List of {@link ContractPriceCategory}.
	 */
	public List<ContractPriceCategory> getDefaultContractPriceCategories(ContractPriceCategory e) {
		Collection<ContractPriceCategory> list = e.getContract().getPriceCategories();
		List<ContractPriceCategory> retList = new ArrayList<>();
		for (ContractPriceCategory cpc : list) {
			if (cpc.getDefaultPriceCtgy() && cpc.getPriceCategory().getId().equals(e.getPriceCategory().getId())) {
				retList.add(cpc);
			}
		}
		return retList;
	}

	/**
	 * Returns only the composite prices
	 *
	 * @param temp
	 * @return
	 */
	@Override
	public List<ContractPriceCategory> getCompositeChildsCategory(ContractPriceCategory temp) throws BusinessException {
		List<ContractPriceCategory> childs = (List<ContractPriceCategory>) temp.getChildPriceCategory();
		Iterator<ContractPriceCategory> iterator = childs.iterator();
		while (iterator.hasNext()) {
			ContractPriceCategory cpc = iterator.next();
			if (cpc.getPriceCategory().getPricePer().equals(PriceInd._PERCENT_)) {
				iterator.remove();
			}
		}
		return childs;
	}

	/**
	 * Used for not continuous Taxes and Fees
	 *
	 * @param category
	 * @param fuelingDate
	 * @return
	 */
	@Override
	public Boolean hasValidInterval(Date fuelingDate, ContractPriceCategory category) throws BusinessException {
		Date fuelEventEndDate = atraxo.cmm.business.ext.utils.DateUtils.removeTime(fuelingDate);
		for (ContractPriceComponent component : category.getPriceComponents()) {
			if (fuelEventEndDate.compareTo(component.getValidTo()) <= 0 && fuelEventEndDate.compareTo(component.getValidFrom()) >= 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Search contract price category based on exchange rate financial source, average method and currencies.
	 *
	 * @param e - Exchange rate.
	 * @return a list of Contract price categories that can be affected by the exchange rate
	 */
	@Override
	public List<ContractPriceCategory> findByExchangeRate(ExchangeRate e) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("financialSource", e.getFinsource());
		params.put("averageMethod", e.getAvgMethodIndicator());
		List<ContractPriceCategory> entities = this.findEntitiesByAttributes(params);
		Iterator<ContractPriceCategory> iter = entities.iterator();
		while (iter.hasNext()) {
			ContractPriceCategory cpc = iter.next();
			if (!cpc.getContract().getSettlementCurr().getId().equals(e.getCurrency1().getId())
					&& !cpc.getContract().getSettlementCurr().getId().equals(e.getCurrency2().getId())) {
				iter.remove();
			}
		}
		return entities;
	}
}
