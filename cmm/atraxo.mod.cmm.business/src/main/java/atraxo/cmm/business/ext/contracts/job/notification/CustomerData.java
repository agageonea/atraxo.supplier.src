package atraxo.cmm.business.ext.contracts.job.notification;

import java.util.List;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.user.UserSupp;

public class CustomerData {

	private Integer customerId;
	private String name;
	private String code;
	private List<Contract> contractsList;
	private UserSupp responsibleBuyer;

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<Contract> getContractsList() {
		return this.contractsList;
	}

	public void setContractsList(List<Contract> contractsList) {
		this.contractsList = contractsList;
	}

	public UserSupp getResponsibleBuyer() {
		return this.responsibleBuyer;
	}

	public void setResponsibleBuyer(UserSupp responsibleBuyer) {
		this.responsibleBuyer = responsibleBuyer;
	}
}
