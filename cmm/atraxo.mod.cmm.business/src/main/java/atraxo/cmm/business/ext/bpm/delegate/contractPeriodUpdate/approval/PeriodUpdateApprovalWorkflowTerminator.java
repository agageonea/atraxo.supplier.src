package atraxo.cmm.business.ext.bpm.delegate.contractPeriodUpdate.approval;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.custom.workflow.IWorkflowTerminatorService;
import atraxo.fmbas.business.ext.bpm.WorkflowMsgConstants;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * @author abolindu
 */
public class PeriodUpdateApprovalWorkflowTerminator extends AbstractBusinessBaseService implements IWorkflowTerminatorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PeriodUpdateApprovalWorkflowTerminator.class);

	@Autowired
	private IContractService contractSrv;

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.api.custom.workflow.IWorkflowTerminatorService#terminateWorkflow(atraxo.fmbas.domain.impl.workflow.WorkflowInstance,
	 * java.lang.String)
	 */
	@Override
	public void terminateWorkflow(WorkflowInstance workflowInstance, String reason) throws BusinessException {

		List<WorkflowInstanceEntity> entities = new ArrayList<>(workflowInstance.getEntitites());

		if (!CollectionUtils.isEmpty(entities)) {
			for (WorkflowInstanceEntity entity : entities) {
				Contract contract = null;
				try {
					contract = this.contractSrv.findByBusiness(entity.getObjectId());
				} catch (Exception e) {
					LOGGER.warn("WARNING: could not retrieve a Contract for ID " + entity.getObjectId()
							+ " ! Will not update the entity since it doesn't exist !", e);
				}

				if (contract != null) {
					this.contractSrv.rejectPeriodUpdate(contract,
							StringUtils.isEmpty(reason) ? WorkflowMsgConstants.APPROVAL_PERIOD_WORKFLOW_TERMINATED_COMMENT : reason);
				}
			}
		}
	}
}
