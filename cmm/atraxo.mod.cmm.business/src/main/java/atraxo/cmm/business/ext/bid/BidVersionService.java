/**
 *
 */
package atraxo.cmm.business.ext.bid;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.ext.bid.IBidVersionService;
import atraxo.cmm.business.ext.utils.BidVersionCalculator;
import atraxo.cmm.domain.impl.contracts.Contract;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public class BidVersionService implements IBidVersionService {

	private static final String MIN_CHAR_VERSION = "A";

	/**
	 * @param bids
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public String calculateBidVersion(List<Contract> bids) {
		if (CollectionUtils.isEmpty(bids)) {
			return MIN_CHAR_VERSION;
		}

		List<String> lst = new ArrayList<>();
		for (Contract bid : bids) {
			lst.add(bid.getBidVersion());
		}

		BidVersionCalculator calc = new BidVersionCalculator();
		return calc.calculateNewVersion(lst);
	}

}
