package atraxo.cmm.business.ext.prices.service.builder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IPricingBaseService;
import atraxo.cmm.business.api.ext.prices.builder.IComponentPriceBuilder;
import atraxo.cmm.business.api.prices.IContractPriceComponentService;
import atraxo.cmm.business.ext.prices.service.PriceConverterService;
import atraxo.cmm.business.ext.utils.AverageMethodCodeFrequency;
import atraxo.cmm.business.ext.utils.DatePeriod;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.exchangerate.IExchangeRateService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.exceptions.ExchangeRateNotFoundException;
import atraxo.fmbas.business.ext.sys.delegate.SystemParameterDelegate;
import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * @author zspeter
 */
public class ComponentPriceBuilder extends AbstractBusinessBaseService implements IComponentPriceBuilder {

	private static final String SEPARATOR = "/";
	private static final Logger LOG = LoggerFactory.getLogger(ComponentPriceBuilder.class);
	@Autowired
	private IExchangeRateService exchSrv;
	@Autowired
	private IPricingBaseService pbSrv;
	@Autowired
	private IContractPriceComponentService cpcSrv;
	@Autowired
	private PriceConverterService priceConverterSrv;

	/*
	 * (non-Javadoc)
	 * @see
	 * atraxo.cmm.business.api.ext.prices.builder.IComponentPriceBuilder#createConvertedPrices(atraxo.cmm.domain.impl.prices.ContractPriceComponent)
	 */
	@Override
	public void createConvertedPrices(ContractPriceComponent e) throws BusinessException {
		if (!e.getContrPriceCtgry().getPriceCategory().getPricePer().equals(PriceInd._PERCENT_)
				&& !e.getContrPriceCtgry().getPriceCategory().getPricePer().equals(PriceInd._COMPOSITE_)) {
			e.setConvertedPrices(null);
			this.generateConvertedPriceComponents(e);
		}
	}

	/**
	 * Used for the secial case regarding quottion WC when the price category starts on monday
	 *
	 * @param category
	 * @param price
	 * @param isProvisioned
	 * @param period
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public ContractPriceComponent buildContractPriceComponent(ContractPriceCategory category, BigDecimal price, boolean isProvisioned, Date... period)
			throws BusinessException {
		return this.buildContractPriceComponentFromPeriod(category, price, isProvisioned, new DatePeriod(period[0], period[1]));
	}

	/**
	 * @param category
	 * @param price
	 * @param isProvisioned
	 * @param datePeriod
	 * @return
	 * @throws BusinessException
	 */
	public ContractPriceComponent buildContractPriceComponentFromPeriod(ContractPriceCategory category, BigDecimal price, boolean isProvisioned,
			DatePeriod datePeriod) throws BusinessException {
		ContractPriceComponent component = new ContractPriceComponent();
		component.setContrPriceCtgry(category);
		component.setPrice(price != null ? price : BigDecimal.ZERO);
		this.setCurrencyAndUnit(component, category);
		this.setValidity(component, category, datePeriod);
		component.setProvisional(isProvisioned);
		component.setWithHold(false);
		return component;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.ext.prices.builder.IComponentPriceBuilder#extendsToMargins(atraxo.cmm.domain.impl.prices.ContractPriceComponent)
	 */
	@Override
	public void extendsToMargins(ContractPriceComponent e) {
		if (e.getContrPriceCtgry().getContinous()) {
			List<ContractPriceComponent> list = new ArrayList<>();
			if (e.getContrPriceCtgry().getPriceComponents() != null) {
				list.addAll(e.getContrPriceCtgry().getPriceComponents());
			}
			list.remove(e);
			if (!list.isEmpty()) {
				list.sort((o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()));
				ContractPriceComponent first = list.get(0);
				if (first.getValidFrom().after(e.getValidFrom()) && e.getValidFrom().after(e.getContrPriceCtgry().getContract().getValidFrom())) {
					e.setValidFrom(e.getContrPriceCtgry().getPricingBases().getValidFrom());
				}
				ContractPriceComponent last = list.get(list.size() - 1);
				if (last.getValidTo().before(e.getValidTo()) && e.getValidTo().before(e.getContrPriceCtgry().getContract().getValidTo())) {
					e.setValidTo(e.getContrPriceCtgry().getPricingBases().getValidTo());
				}
			} else {
				e.setValidFrom(e.getContrPriceCtgry().getPricingBases().getValidFrom());
				e.setValidTo(e.getContrPriceCtgry().getPricingBases().getValidTo());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.ext.prices.builder.IComponentPriceBuilder#updateMargins(java.util.List, java.util.Date, java.util.Date)
	 */
	@Override
	public void updateMargins(List<ContractPriceComponent> list, Date pbValidFrom, Date pbValidTo) {
		list.sort((o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()));
		Iterator<ContractPriceComponent> iterator = list.iterator();
		while (iterator.hasNext()) {
			ContractPriceComponent cpc = iterator.next();
			ContractPriceComponent first = list.get(0);
			ContractPriceComponent last = list.get(list.size() - 1);

			if (cpc.getValidTo().before(pbValidFrom)) {
				iterator.remove();
			} else if (cpc.getValidFrom().before(pbValidFrom) && cpc.getValidTo().after(pbValidFrom)) {
				cpc.setValidFrom(pbValidFrom);
			} else if (cpc.equals(first) && cpc.getValidFrom().after(pbValidFrom)) {
				cpc.setValidFrom(pbValidFrom);
			}
			if (cpc.getValidFrom().after(pbValidTo)) {
				iterator.remove();
			} else if (cpc.getValidFrom().before(pbValidTo) && cpc.getValidTo().after(pbValidTo)) {
				cpc.setValidTo(pbValidTo);
			} else if (cpc.equals(last) && cpc.getValidTo().before(pbValidTo)) {
				cpc.setValidTo(pbValidTo);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.ext.prices.builder.IComponentPriceBuilder#updateNeibours(atraxo.cmm.domain.impl.prices.ContractPriceComponent,
	 * boolean)
	 */
	@Override
	public void updateNeibours(ContractPriceComponent e, boolean shouldInsert) throws BusinessException {
		ContractPriceCategory cpc = e.getContrPriceCtgry();
		if ((this.pbSrv.isIndex(cpc.getPricingBases()) && cpc.getContinous()) || cpc.getPriceComponents() == null) {
			return;
		}
		List<ContractPriceComponent> list = new ArrayList<>(cpc.getPriceComponents());
		list.sort((o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()));
		Date leftMargin = DateUtils.addDays(e.getValidFrom(), -1);
		Date rightMargin = DateUtils.addDays(e.getValidTo(), 1);
		ContractPriceComponent leftElement = null;
		ContractPriceComponent toDuplicate = null;
		Iterator<ContractPriceComponent> iter = list.iterator();
		while (iter.hasNext()) {
			ContractPriceComponent c = iter.next();
			if (shouldInsert && toDuplicate != null && c.getValidFrom().after(rightMargin)) {
				this.duplicate(e, cpc, toDuplicate, c.getValidFrom());
				toDuplicate = null;
			}
			if (!c.equals(e)) {
				if (c.getValidFrom().before(leftMargin) && c.getValidTo().compareTo(leftMargin) <= 0) {
					leftElement = c;
				} else if (c.getValidFrom().compareTo(leftMargin) <= 0 && c.getValidTo().after(leftMargin)) {
					if (c.getValidTo().compareTo(rightMargin) >= 0) {
						toDuplicate = c;
					}
					leftElement = null;
					c.setValidTo(leftMargin);
					this.createConvertedPrices(c);
				} else if (c.getValidFrom().compareTo(e.getValidFrom()) >= 0 && c.getValidTo().compareTo(e.getValidTo()) <= 0) {
					iter.remove();
				} else {
					if ((c.getValidFrom().before(rightMargin) && c.getValidTo().compareTo(rightMargin) >= 0)
							|| (c.getValidFrom().after(rightMargin))) {
						c.setValidFrom(rightMargin);
						this.createConvertedPrices(c);
						break;
					}
				}
			}
		}
		cpc.setPriceComponents(list);
		if (shouldInsert && toDuplicate != null && e.getValidTo().before(e.getContrPriceCtgry().getContract().getValidTo())) {
			this.duplicate(e, cpc, toDuplicate, e.getContrPriceCtgry().getContract().getValidTo());
		}
		if (leftElement != null) {
			leftElement.setValidTo(leftMargin);
			this.createConvertedPrices(leftElement);
		}
	}

	/**
	 * Sets contract price component currency and unit based on contract price category.
	 *
	 * @param component
	 * @param category
	 * @throws BusinessException
	 */
	private void setCurrencyAndUnit(ContractPriceComponent component, ContractPriceCategory category) throws BusinessException {
		IPricingBaseService pbService = (IPricingBaseService) this.findEntityService(PricingBase.class);
		if (category.getPricingBases() != null) {
			if (pbService.isIndex(category.getPricingBases()) && category.getPricingBases().getQuotation() != null) {
				component.setUnit(category.getPricingBases().getQuotation().getUnit());
				component.setCurrency(category.getPricingBases().getQuotation().getCurrency());
			} else {
				IUnitService unitService = (IUnitService) this.findEntityService(Unit.class);
				ICurrenciesService currService = (ICurrenciesService) this.findEntityService(Currencies.class);
				Unit unit = category.getInitialUnitId() != null ? unitService.findById(category.getInitialUnitId())
						: category.getContract().getSettlementUnit();
				component.setUnit(unit);
				Currencies currency = category.getInitialCurrId() != null ? currService.findById(category.getInitialCurrId())
						: category.getContract().getSettlementCurr();
				component.setCurrency(currency);
			}
		} else {
			component.setUnit(category.getContract().getSettlementUnit());
			component.setCurrency(category.getContract().getSettlementCurr());
		}
	}

	private void setValidity(ContractPriceComponent component, ContractPriceCategory category, DatePeriod datePeriod) {
		if (datePeriod != null) {
			component.setValidFrom(datePeriod.getStartDate().getTime());
			component.setValidTo(datePeriod.getEndDate().getTime());
		}
		if (category.getPricingBases() != null) {
			if (component.getValidFrom().before(category.getPricingBases().getValidFrom())) {
				component.setValidFrom(category.getPricingBases().getValidFrom());
			}
			if (component.getValidTo().after(category.getPricingBases().getValidTo())) {
				component.setValidTo(category.getContract().getValidTo());
			}
		}
	}

	/**
	 * @param e
	 * @param cpc
	 * @param toDuplicate
	 * @param validTo
	 * @throws BusinessException
	 */
	private void duplicate(ContractPriceComponent e, ContractPriceCategory cpc, ContractPriceComponent toDuplicate, Date validTo)
			throws BusinessException {
		ContractPriceComponent generatedComp = new ContractPriceComponent();
		generatedComp.setId(null);
		generatedComp.setCurrency(toDuplicate.getCurrency());
		generatedComp.setUnit(toDuplicate.getUnit());
		generatedComp.setPrice(toDuplicate.getPrice());
		generatedComp.setProvisional(toDuplicate.getProvisional());
		generatedComp.setValidFrom(DateUtils.addDays(e.getValidTo(), 1));
		generatedComp.setValidTo(validTo);
		generatedComp.setWithHold(toDuplicate.getWithHold());
		cpc.addToPriceComponents(generatedComp);
		this.createConvertedPrices(generatedComp);
	}

	/**
	 * Generate a new {@link ContractPriceComponentConv} list from the {@link ContractPriceComponent}.
	 *
	 * @param e - {@link ContractPriceComponent}.
	 * @return - The generated list.
	 * @throws BusinessException
	 */
	private List<ContractPriceComponentConv> generateConvertedPriceComponents(ContractPriceComponent e) throws BusinessException {
		List<ContractPriceComponentConv> list = new ArrayList<>();
		Density density = this.getBusinessDelegate(SystemParameterDelegate.class).getSystemDensity();

		if (e.getUnit().getId().equals(e.getContrPriceCtgry().getContract().getSettlementUnit().getId())
				&& e.getCurrency().getId().equals(e.getContrPriceCtgry().getContract().getSettlementCurr().getId())) {
			list.add(this.generateConvertedPriceComponent(e, e.getValidFrom(), e.getValidTo(), density));
		} else {
			List<Date[]> dateList = this.calculatePeriods(e);
			for (Date[] dates : dateList) {
				list.add(this.generateConvertedPriceComponent(e, dates[0], dates[1], density));
			}
		}
		return list;
	}

	/**
	 * Generate a new {@link ContractPriceComponentConv} from the {@link ContractPriceComponent}.
	 *
	 * @param e - {@link ContractPriceComponent}.
	 * @param validFrom - {@link Date}.
	 * @param validTo - {@link Date}.
	 * @return - the generated {@link ContractPriceComponentConv}
	 * @throws BusinessException
	 */
	private ContractPriceComponentConv generateConvertedPriceComponent(ContractPriceComponent e, Date validFrom, Date validTo, Density density)
			throws BusinessException {
		Date date = this.getExchangeRateDate(validFrom, validTo);
		Contract c = e.getContrPriceCtgry().getContract();
		ConversionResult convResult = this.getCalculatedValueForConverted(e, c, date, density);
		ContractPriceComponentConv newConvComp = new ContractPriceComponentConv();
		newConvComp.setValidFrom(validFrom);
		newConvComp.setValidTo(validTo);
		newConvComp.setPrice(e.getPrice());
		newConvComp.setCurrencyUnitCd(e.getCurrency().getCode() + SEPARATOR + e.getUnit().getCode());
		newConvComp.setEquivalent(convResult.getValue());
		newConvComp.setEquivalentCurrency(c.getSettlementCurr());
		newConvComp.setEquivalentUnit(c.getSettlementUnit());
		if (e.getUnit().getUnittypeInd().equals(UnitType._EVENT_)) {
			newConvComp.setEquivalentUnit(e.getUnit());
		}
		newConvComp.setExchangeRate(convResult.getFactor());
		newConvComp.setWhldPay(e.getWithHold());
		newConvComp.setProvisional(e.getProvisional());
		newConvComp.setContractPriceComponent(e);
		newConvComp.setExchangeRateCurrnecyCDS(e.getCurrency().getCode() + SEPARATOR + c.getSettlementCurr().getCode());
		newConvComp.setOfDate(date);
		e.addToConvertedPrices(newConvComp);
		return newConvComp;
	}

	/**
	 * Calculate from the contract price component's valid from field the exchange rate date.
	 *
	 * @param e - {@link ContractPriceComponent}.
	 * @return - the date to calculate the {@link ExchangeRateValue}.
	 */
	private Date getExchangeRateDate(Date validFrom, Date validTo) {
		Date date = GregorianCalendar.getInstance().getTime();
		if (!this.isBetweenDates(validFrom, validTo, date)) {
			if (validTo.before(date)) {
				date = validTo;
			} else if (validFrom.after(date)) {
				date = validFrom;
			}
		}
		return date;
	}

	/**
	 * Check if the date is between the given dates.
	 *
	 * @param from - start of the period.
	 * @param to - period's end.
	 * @param date - the date which must be checked.
	 * @return True if date is between from and to. The period margins are not included.
	 */
	private boolean isBetweenDates(Date from, Date to, Date date) {
		return date.compareTo(from) > -1 && date.compareTo(to) < 1;
	}

	/**
	 * Calculate the converted price and the conversion factor.
	 *
	 * @param e - original {@link ContractPriceComponent}.
	 * @param c - {@link Contract}.
	 * @param date - {@link Date}.
	 * @return - the calculated value {@link BigDecimal}.
	 * @throws BusinessException
	 */
	private ConversionResult getCalculatedValueForConverted(ContractPriceComponent e, Contract c, Date date, Density density)
			throws BusinessException {
		Unit toUnit = c.getSettlementUnit();
		Unit fromUnit = e.getUnit();
		if (fromUnit.getUnittypeInd().equals(UnitType._EVENT_)) {
			toUnit = fromUnit;
		}

		BigDecimal value = e.getPrice();
		Currencies fromCurrency = e.getCurrency();
		Currencies toCurrency = c.getSettlementCurr();
		AverageMethod am = e.getContrPriceCtgry().getAverageMethod();
		FinancialSources fs = e.getContrPriceCtgry().getFinancialSource();
		MasterAgreementsPeriod offset = e.getContrPriceCtgry().getExchangeRateOffset();
		boolean strict = false;
		if (this.cpcSrv.isIndex(e)) {
			return this.priceConverterSrv.convert(fromUnit, toUnit, fromCurrency, toCurrency, value, date, fs, am,
					e.getContrPriceCtgry().getPricingBases(), strict, offset);
		}
		return this.priceConverterSrv.convert(fromUnit, toUnit, fromCurrency, toCurrency, value, date, fs, am, density, strict, offset);
	}

	/**
	 * calculate the {@link ExchangeRateValue}s periods.
	 *
	 * @param validFrom - {@link Date}.
	 * @param validTo - {@link Date}.
	 * @param offset - String offset.
	 * @return - {@link List}<{@link Date}[]> of small periods.
	 * @throws BusinessException
	 */
	private List<Date[]> calculatePeriods(ContractPriceComponent e) throws BusinessException {
		Date to = e.getValidTo();
		Date from = e.getValidFrom();
		AverageMethodCodeFrequency avgMthdCodeFrequency = AverageMethodCodeFrequency.getByAverageMethod(e.getContrPriceCtgry().getAverageMethod());
		List<Date[]> list = new ArrayList<>();

		List<ExchangeRateValue> exchValues = new ArrayList<>();
		try {
			exchValues = this.getExchangeRateValues(e);
		} catch (ExchangeRateNotFoundException exception) {
			LOG.warn("No direct Exchange Rate found.", exception);
		}

		if (exchValues.isEmpty()) {
			Date[] dates = new Date[2];
			dates[0] = from;
			dates[1] = to;
			list.add(dates);
			return list;
		}
		ExchangeRateValue exchRateValue = exchValues.get(0);
		Calendar cal = GregorianCalendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		from = DateUtils.truncate(from, Calendar.DAY_OF_MONTH);
		to = DateUtils.truncate(to, Calendar.DAY_OF_MONTH);
		cal.setTime(from);
		Date[] dates = new Date[2];
		dates[0] = from;
		while (cal.getTime().compareTo(to) <= 0) {
			cal.add(Calendar.DAY_OF_YEAR, 1);
			if (cal.getTime().after(exchRateValue.getVldToDtRef())) {
				dates[1] = to;
				list.add(dates);
				dates = new Date[2];
				dates[0] = DateUtils.addDays(to, 1);
				break;
			}

			if (avgMthdCodeFrequency.firstDayOfTheInterval(cal)) {
				dates = this.calculateDates(list, cal, dates);
			}
		}
		if (to.compareTo(dates[0]) >= 0) {
			dates[1] = to;
			list.add(dates);
		}
		return list;
	}

	/**
	 * @param list
	 * @param cal
	 * @param dates
	 * @return
	 */
	private Date[] calculateDates(List<Date[]> list, Calendar cal, Date[] dates) {
		cal.add(Calendar.DAY_OF_YEAR, -1);
		dates[1] = cal.getTime();
		list.add(dates);
		Date[] dateArr = new Date[2];
		cal.add(Calendar.DAY_OF_YEAR, 1);
		dateArr[0] = cal.getTime();
		return dateArr;
	}

	/**
	 * @param e
	 * @return
	 * @throws BusinessException
	 */
	private List<ExchangeRateValue> getExchangeRateValues(ContractPriceComponent e) throws BusinessException {
		List<ExchangeRateValue> exchValues = new ArrayList<>();
		if (!e.getCurrency().equals(e.getContrPriceCtgry().getContract().getSettlementCurr())) {
			ExchangeRate exchangeRate;
			try {
				exchangeRate = this.exchSrv.getExchangeRate(e.getCurrency(), e.getContrPriceCtgry().getContract().getSettlementCurr(),
						e.getContrPriceCtgry().getFinancialSource(), e.getContrPriceCtgry().getAverageMethod());
			} catch (ExchangeRateNotFoundException exception) {
				LOG.warn("Exchange rate not found  ", exception);
				exchangeRate = this.exchSrv.getExchangeRate(e.getContrPriceCtgry().getContract().getSettlementCurr(), e.getCurrency(),
						e.getContrPriceCtgry().getFinancialSource(), e.getContrPriceCtgry().getAverageMethod());
			}
			exchValues.addAll(exchangeRate.getExchangeRateValue());
		}
		exchValues.sort((o1, o2) -> o2.getVldFrDtRef().compareTo(o1.getVldFrDtRef()));
		return exchValues;
	}
}
