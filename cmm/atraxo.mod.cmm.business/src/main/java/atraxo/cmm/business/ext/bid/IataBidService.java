package atraxo.cmm.business.ext.bid;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.api.ext.bid.IIataBidService;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.commons.utils.XMLUtils;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBid;

/**
 * @author abolindu
 */
public class IataBidService implements IIataBidService {

	private static final Logger LOG = LoggerFactory.getLogger(IataBidService.class);

	@Override
	public void generateIataXml(FuelTenderBid fuelTenderBid, OutputStream out) throws BusinessException {
		try {
			String xml = XMLUtils.toString(fuelTenderBid, FuelTenderBid.class, false, true);
			out.write(xml.getBytes(Charset.forName("UTF-8")));
		} catch (JAXBException | IOException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}
	}
}
