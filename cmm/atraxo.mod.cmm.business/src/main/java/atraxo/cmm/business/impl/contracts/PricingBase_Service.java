/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.contracts;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link PricingBase} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class PricingBase_Service extends AbstractEntityService<PricingBase> {

	/**
	 * Public constructor for PricingBase_Service
	 */
	public PricingBase_Service() {
		super();
	}

	/**
	 * Public constructor for PricingBase_Service
	 * 
	 * @param em
	 */
	public PricingBase_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<PricingBase> getEntityClass() {
		return PricingBase.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return PricingBase
	 */
	public PricingBase findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(PricingBase.NQ_FIND_BY_BUSINESS,
							PricingBase.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"PricingBase", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"PricingBase", "id"), nure);
		}
	}

	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByContract(Contract contract) {
		return this.findByContractId(contract.getId());
	}
	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByContractId(Integer contractId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PricingBase e where e.clientId = :clientId and e.contract.id = :contractId",
						PricingBase.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractId", contractId).getResultList();
	}
	/**
	 * Find by reference: quotation
	 *
	 * @param quotation
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByQuotation(Quotation quotation) {
		return this.findByQuotationId(quotation.getId());
	}
	/**
	 * Find by ID of reference: quotation.id
	 *
	 * @param quotationId
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByQuotationId(Integer quotationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PricingBase e where e.clientId = :clientId and e.quotation.id = :quotationId",
						PricingBase.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("quotationId", quotationId).getResultList();
	}
	/**
	 * Find by reference: convUnit
	 *
	 * @param convUnit
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByConvUnit(Unit convUnit) {
		return this.findByConvUnitId(convUnit.getId());
	}
	/**
	 * Find by ID of reference: convUnit.id
	 *
	 * @param convUnitId
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByConvUnitId(Integer convUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PricingBase e where e.clientId = :clientId and e.convUnit.id = :convUnitId",
						PricingBase.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("convUnitId", convUnitId).getResultList();
	}
	/**
	 * Find by reference: avgMethod
	 *
	 * @param avgMethod
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByAvgMethod(AverageMethod avgMethod) {
		return this.findByAvgMethodId(avgMethod.getId());
	}
	/**
	 * Find by ID of reference: avgMethod.id
	 *
	 * @param avgMethodId
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByAvgMethodId(Integer avgMethodId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PricingBase e where e.clientId = :clientId and e.avgMethod.id = :avgMethodId",
						PricingBase.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("avgMethodId", avgMethodId).getResultList();
	}
	/**
	 * Find by reference: priceCat
	 *
	 * @param priceCat
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByPriceCat(PriceCategory priceCat) {
		return this.findByPriceCatId(priceCat.getId());
	}
	/**
	 * Find by ID of reference: priceCat.id
	 *
	 * @param priceCatId
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByPriceCatId(Integer priceCatId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PricingBase e where e.clientId = :clientId and e.priceCat.id = :priceCatId",
						PricingBase.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("priceCatId", priceCatId).getResultList();
	}
	/**
	 * Find by reference: contractPriceCategories
	 *
	 * @param contractPriceCategories
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByContractPriceCategories(
			ContractPriceCategory contractPriceCategories) {
		return this.findByContractPriceCategoriesId(contractPriceCategories
				.getId());
	}
	/**
	 * Find by ID of reference: contractPriceCategories.id
	 *
	 * @param contractPriceCategoriesId
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByContractPriceCategoriesId(
			Integer contractPriceCategoriesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from PricingBase e, IN (e.contractPriceCategories) c where e.clientId = :clientId and c.id = :contractPriceCategoriesId",
						PricingBase.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractPriceCategoriesId",
						contractPriceCategoriesId).getResultList();
	}
}
