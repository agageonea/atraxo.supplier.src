package atraxo.cmm.business.ws.tender.transformer;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.Years;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.ext.datahub.IDataHubInTransformer;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.tender.TenderDefaultConstants;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.PaymentChoise;
import atraxo.cmm.domain.impl.cmm_type.PaymentType;
import atraxo.cmm.domain.impl.cmm_type.Period;
import atraxo.cmm.domain.impl.cmm_type.PricingType;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.SourceType;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.cmm_type.YesNo;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.cmm.domain.impl.tender.TenderLocationExpectedPrice;
import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.SingleResultException;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.DataProvider;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceType;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.fmbas_type.TenderVolumeOffer;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.InvalidEnumException;
import seava.j4e.commons.utils.SoneEnumUtils;
import seava.j4e.iata.fuelplus.iata.tender.AirlinesVolumes.AirlinesVolume;
import seava.j4e.iata.fuelplus.iata.tender.ExpectedFee;
import seava.j4e.iata.fuelplus.iata.tender.ExpectedTax;
import seava.j4e.iata.fuelplus.iata.tender.FlightServiceType;
import seava.j4e.iata.fuelplus.iata.tender.Index;
import seava.j4e.iata.fuelplus.iata.tender.ProductService;
import seava.j4e.iata.fuelplus.iata.tender.ProductServiceDetails;
import seava.j4e.iata.fuelplus.iata.tender.Round;
import seava.j4e.iata.fuelplus.iata.tender.TenderLocation;
import seava.j4e.iata.fuelplus.iata.tender.TenderLocations;
import seava.j4e.iata.fuelplus.iata.tender.VolumeOfferType;

public abstract class DataHubInTransformer<T, E extends AbstractEntity> extends DataHubTransformer implements IDataHubInTransformer<E, T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(DataHubInTransformer.class);
	protected static final String MSG = "Method not implemented yet.";

	protected List<atraxo.cmm.domain.impl.tender.TenderLocation> buildTenderLocations(Tender tender, TenderLocations iataLocations)
			throws BusinessException {

		List<atraxo.cmm.domain.impl.tender.TenderLocation> tenderLocations = new ArrayList<>();

		// Extract Tender Locations based on the Product Services
		for (TenderLocation iataTenderLocation : iataLocations.getTenderLocation()) {
			if (iataTenderLocation.getProductServices() != null) {
				for (ProductService iataProductService : iataTenderLocation.getProductServices().getProductService()) {
					tenderLocations.add(this.extractTenderLocation(tender, iataTenderLocation, iataProductService));
				}
			} else {
				tenderLocations.add(this.extractTenderLocation(tender, iataTenderLocation, null));
			}
		}
		return tenderLocations;
	}

	private atraxo.cmm.domain.impl.tender.TenderLocation extractTenderLocation(Tender tender, TenderLocation iataTenderLocation,
			ProductService iataProductService) throws BusinessException {

		atraxo.cmm.domain.impl.tender.TenderLocation tenderLocation = new atraxo.cmm.domain.impl.tender.TenderLocation();
		tenderLocation.setTender(tender);
		tenderLocation.setIsValid(true);
		tenderLocation.setVolumeUnit(this.unitService.findByCode(this.systemParamsService.getSysVol()));

		tenderLocation.setLocationBiddingStatus(BiddingStatus._NEW_);
		tenderLocation.setTransmissionStatus(TransmissionStatus._NEW_);
		tenderLocation.setVolumeOffer(this.getTenderVolumeOffer(iataTenderLocation));

		// Find matching product
		Product product = Product.getByIataName(TenderDefaultConstants.DEFAULT_FUEL_PRODUCT);
		try {
			product = Product.getByIataName(iataTenderLocation.getLocationHeader().getFuelProduct().value());
		} catch (InvalidEnumException e) {
			LOGGER.warn(e.getMessage(), e);
		}
		tenderLocation.setFuelProduct(product);

		// Find matching tax type
		TaxType taxType = TaxType.getByIataCode(TenderDefaultConstants.DEFAULT_FUEL_PRODUCT_TAX_TYPE);
		try {
			taxType = TaxType.getByIataCode(iataTenderLocation.getLocationHeader().getProductTaxType().name());
		} catch (Exception e) {
			LOGGER.warn(e.getMessage(), e);
		}
		tenderLocation.setTaxType(taxType);

		// Find location by IATA code optional - backup ICAO code madatory
		tenderLocation.setLocation(this.findLocation(iataTenderLocation.getLocationHeader().getLocationCodeIATA(),
				iataTenderLocation.getLocationHeader().getLocationCodeICAO()));

		// Find matching flight service type
		tenderLocation.setAdHocLoc(Boolean.FALSE);
		if (iataTenderLocation.getLocationHeader().getFlightServiceType() != null) {
			tenderLocation.setAdHocLoc(
					iataTenderLocation.getLocationHeader().getFlightServiceType().equals(FlightServiceType.AH) ? Boolean.TRUE : Boolean.FALSE);
			tenderLocation.setFlightServiceType(atraxo.cmm.domain.impl.cmm_type.FlightServiceType
					.getByCode(iataTenderLocation.getLocationHeader().getFlightServiceType().value()));
		} else {
			tenderLocation.setFlightServiceType(atraxo.cmm.domain.impl.cmm_type.FlightServiceType._SCHEDULED_);
		}

		// Init Tender Location Airlines
		List<TenderLocationAirlines> airlineVolumes = new ArrayList<>();
		tenderLocation.setTenderLocationAirlines(airlineVolumes);

		// AgreementPeriod
		if (iataTenderLocation.getAgreementPeriod().getFrom().toGregorianCalendar()
				.before(iataTenderLocation.getAgreementPeriod().getTo().toGregorianCalendar())) {
			tenderLocation.setAgreementFrom(iataTenderLocation.getAgreementPeriod().getFrom().toGregorianCalendar().getTime());
			tenderLocation.setAgreementTo(iataTenderLocation.getAgreementPeriod().getTo().toGregorianCalendar().getTime());
		} else {
			throw new BusinessException(BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
					"Agreement Period From can not be greater than Agreement Period To!");
		}

		if (iataTenderLocation.getProductServices() != null) {
			// Product Services
			this.buildProductServices(tenderLocation, iataTenderLocation, iataProductService);

			// Payment
			this.buildPayment(tenderLocation, iataProductService);

			// Product Pricing
			this.buildProductPricing(tenderLocation, iataProductService);

			// Expected Pricing
			this.buildExpectedPrices(tenderLocation, iataProductService);
		}

		// Airline volumes
		this.buildVolumes(tenderLocation, iataTenderLocation);

		// Bidding Rounds
		this.addBiddingRounds(tenderLocation, iataTenderLocation, tender);

		// Set default empty valued for enum properties
		SoneEnumUtils.setEmptyValues(tenderLocation);
		return tenderLocation;
	}

	protected void addBiddingRounds(atraxo.cmm.domain.impl.tender.TenderLocation tenderLocation, TenderLocation l, Tender tender) {
		if (l.getBiddingRounds() == null || CollectionUtils.isEmpty(l.getBiddingRounds().getRound())) {
			// SONE-6285 - When no round is in the Tender Invitation, one is created with the data from the tender
			TenderLocationRound round = new TenderLocationRound();
			round.setRoundNo(1);
			round.setBiddingFrom(tender.getBiddingPeriodFrom());
			round.setBiddingTo(tender.getBiddingPeriodTo());
			round.setBidOpening(tender.getBiddingPeriodTo());
			round.setActive(false);
			tenderLocation.addToTenderLocationRounds(round);
			// SONE-6285 - End
		} else {
			for (Round r : l.getBiddingRounds().getRound()) {
				TenderLocationRound round = new TenderLocationRound();
				round.setRoundNo(r.getRoundNo().intValue());
				round.setBiddingFrom(r.getBiddingPeriod().getFrom().toGregorianCalendar().getTime());
				round.setBiddingTo(r.getBiddingPeriod().getTo().toGregorianCalendar().getTime());
				round.setActive(false);
				if (r.getBidOpening() != null) {
					round.setBidOpening(r.getBidOpening().toGregorianCalendar().getTime());
				}
				tenderLocation.addToTenderLocationRounds(round);
			}
		}
	}

	protected void buildProductServices(atraxo.cmm.domain.impl.tender.TenderLocation tenderLocation, TenderLocation l, ProductService service)
			throws BusinessException {

		ProductServiceDetails details = service.getProductServiceDetails();

		if (service.getProductPricing() != null) {
			Index index = service.getProductPricing().getIndex();
			if (index != null) {
				tenderLocation.setFuelDensity(index.getIndexConversion().getDensity());

				if (index.getIndexConversion().getDensityWeightUnit() != null) {
					tenderLocation.setDensityWeightUnit(
							this.findUnit(index.getIndexConversion().getDensityWeightUnit(), "Product Services - DensityWeightUnit"));
				}

				if (index.getIndexConversion().getDensityVolumeUnit() != null) {
					tenderLocation.setDensityVolumeUnit(
							this.findUnit(index.getIndexConversion().getDensityVolumeUnit(), "Product Services - DensityVolumeUnit"));
				}

				tenderLocation.setDensityConvFromUnit(this.findUnit(index.getIndexConversion().getFromUnit(), "Product Services - FromUnit"));

				tenderLocation.setDensityConvToUnit(this.findUnit(index.getIndexConversion().getToUnit(), "Product Services - ToUnit"));

				tenderLocation.setOperator(Operator.getByCode(index.getIndexConversion().getConvOperator().value()));
				tenderLocation.setConversionFactor(index.getIndexConversion().getFactor());
			}
		}
		tenderLocation.setDeliveryPoint(ContractSubType.getByCode(details.getDeliveryPoint().value()));
		tenderLocation.setOperatingHours(details.getOperatingHours());
		tenderLocation.setTitleTransfer(details.getTitleTransfer());
		tenderLocation.setAstmSpecification(details.getFuelSpecification());
		tenderLocation.setComments(details.getComments());

		if (details.getIATAServiceLevel() != null) {
			tenderLocation.setIataServiceLevel(this.getIataServiceLevel(details.getIATAServiceLevel()));
		}
		if (details.getRefuelerCompanyCode() != null) {
			tenderLocation.setRefuelerCode(details.getRefuelerCompanyCode());
		}
	}

	protected void buildExpectedPrices(atraxo.cmm.domain.impl.tender.TenderLocation tenderLocation, ProductService service) throws BusinessException {
		if (service.getExpectedFeesAndTaxes() != null) {
			if (service.getExpectedFeesAndTaxes().getExpectedFees() != null
					&& !CollectionUtils.isEmpty(service.getExpectedFeesAndTaxes().getExpectedFees().getExpectedFee())) {
				for (ExpectedFee fee : service.getExpectedFeesAndTaxes().getExpectedFees().getExpectedFee()) {
					TenderLocationExpectedPrice expectedFees = new TenderLocationExpectedPrice();
					expectedFees.setTenderLocation(tenderLocation);
					expectedFees.setIataPriceCategory(super.findIATAPC(fee.getItemProductID().value()));
					tenderLocation.addToTenderLocationExpectedPrices(expectedFees);
				}
			}
			if (service.getExpectedFeesAndTaxes().getExpectedTaxes() != null
					&& !CollectionUtils.isEmpty(service.getExpectedFeesAndTaxes().getExpectedTaxes().getExpectedTax())) {
				for (ExpectedTax tax : service.getExpectedFeesAndTaxes().getExpectedTaxes().getExpectedTax()) {
					TenderLocationExpectedPrice expectedTaxes = new TenderLocationExpectedPrice();
					expectedTaxes.setTenderLocation(tenderLocation);
					expectedTaxes.setIataPriceCategory(super.findIATAPC(tax.getTaxType().value()));
					tenderLocation.addToTenderLocationExpectedPrices(expectedTaxes);
				}
			}
		}
	}

	protected void buildPayment(atraxo.cmm.domain.impl.tender.TenderLocation tenderLocation, ProductService service) throws BusinessException {
		if (service.getPayment() != null) {
			if (service.getPayment().getOpenInvoice() != null) {
				tenderLocation.setCreditTerms(PaymentChoise._OPEN_CREDIT_);
				tenderLocation.setPaymentTerms(service.getPayment().getOpenInvoice().getPaymentTerms().longValue());
				tenderLocation.setPaymentRefDate(PaymentDay.getByCode(service.getPayment().getOpenInvoice().getPaymentReferenceDateType().value()));
				tenderLocation.setInvoiceFrequency(InvoiceFreq.getByCode(service.getPayment().getOpenInvoice().getInvoiceFrequency().value()));
				tenderLocation.setInvoiceType(InvoiceType.getByCode(service.getPayment().getOpenInvoice().getInvoiceType().value()));
			} else if (service.getPayment().getPrepayment() != null) {
				tenderLocation.setCreditTerms(PaymentChoise._PREPAYMENT_);
				tenderLocation.setNumberOfDaysPrepaid(service.getPayment().getPrepayment().getNumberOfDaysPrepaid().longValue());
				tenderLocation.setPrepaymentDayOffset(service.getPayment().getPrepayment().getPaymentFirstDeliveryDate().longValue());
				tenderLocation.setPrepaymentFrequency(InvoiceFreq.getByCode(service.getPayment().getPrepayment().getPaymentFrequency().value()));
				tenderLocation.setPrepaymentAmount(service.getPayment().getPrepayment().getAmount());
			}

			tenderLocation.setMarketCurrency(this.findCurrency(service.getPayment().getPaymentCurrency().value(), "PaymentCurrency"));
			tenderLocation.setMarketPricingUnit(this.findUnit(service.getPayment().getPricingUnit(), "Payment - PricingUnit"));

			tenderLocation.setFinancialSource(
					this.findFinancialSource(service.getPayment().getExchangeRate().getFinancialSource().value(), "ExchangeRate - FinancialSource"));

			tenderLocation.setAveragingMethod(
					this.findAverageMethod(service.getPayment().getExchangeRate().getAverageingMethod().value(), "Exchange Rate - AverageingMethod"));

			tenderLocation.setExchangeRateOffset(MasterAgreementsPeriod.getByName(service.getPayment().getExchangeRate().getAveragingOffset()));

			if (service.getPayment().getMethodOfPayment() != null) {
				tenderLocation.setPaymentMethod(PaymentType.getByCode(service.getPayment().getMethodOfPayment().value()));
			}
			if (service.getPayment().getGuaranteesDepositsRequired() != null) {
				tenderLocation.setDepositRequired(YesNo.getByCode(service.getPayment().getGuaranteesDepositsRequired().value()));
			}
		}
	}

	protected void buildProductPricing(atraxo.cmm.domain.impl.tender.TenderLocation tenderLocation, ProductService service) throws BusinessException {
		if (service.getProductPricing() != null) {
			if (service.getProductPricing().getIndex() != null) {
				tenderLocation.setPricingBase(PricingType._INDEX_);
				tenderLocation.setDataProvider(DataProvider.getByCode(service.getProductPricing().getIndex().getIndexProvider().value()));
				tenderLocation.setIndexProvider(this.findTimeSerie(service.getProductPricing().getIndex().getIndexProviderCode(),
						"Product Pricing - Index - Index provider code"));
				tenderLocation.setIndexAveragingPeriod(MasterAgreementsPeriod.getByName(service.getProductPricing().getIndex().getAveragingOffset()));
				tenderLocation.setIndexAveragingMethod(this.findAverageMethod(service.getProductPricing().getIndex().getAverageingMethod().value(),
						"Product Pricing Index - AverageingMethod"));
			} else if (service.getProductPricing().getMarket() != null) {
				tenderLocation.setPricingBase(PricingType._MARKET_);
				tenderLocation.setRate(service.getProductPricing().getMarket().getRate());
				tenderLocation.setMarketCurrency(
						this.findCurrency(service.getProductPricing().getMarket().getCurrency().value(), "Product Pricing - Market - Currency"));
				tenderLocation.setMarketPricingUnit(
						this.findUnit(service.getProductPricing().getMarket().getUOM(), "Product Pricing - Index - PricingUnit"));

				tenderLocation.setSourceType(SourceType.getByCode(service.getProductPricing().getMarket().getSourceType().value()));

				if (service.getProductPricing().getMarket().getSourceName() != null) {
					tenderLocation.setSourceName(service.getProductPricing().getMarket().getSourceName());
				}
			}
		}
	}

	protected void buildVolumes(atraxo.cmm.domain.impl.tender.TenderLocation tenderLocation, TenderLocation l) throws BusinessException {
		BigDecimal locationVolume = BigDecimal.ZERO;
		if (l.getAirlinesVolumes() != null && !CollectionUtils.isEmpty(l.getAirlinesVolumes().getAirlinesVolume())) {
			tenderLocation.setHasTotalVolume(false);
			tenderLocation.setPeriodType(Period.getByCode(l.getAirlinesVolumes().getPeriodType().value()));

			for (AirlinesVolume a : l.getAirlinesVolumes().getAirlinesVolume()) {
				TenderLocationAirlines airlineVolume = new TenderLocationAirlines();
				airlineVolume.setTenderLocation(tenderLocation);
				airlineVolume.setAirline(this.findCustomer(a.getAirlineCode(), "AirlineCode"));
				airlineVolume.setUnit(this.findUnit(l.getAirlinesVolumes().getUOM(), "Airline Volume - " + airlineVolume.getAirline() + " - UOM"));
				airlineVolume.setVolumePeriod(tenderLocation.getPeriodType());
				airlineVolume.setVolume(a.getVolume());
				airlineVolume.setNotes(a.getComments());
				airlineVolume.setBiddingStatus(BiddingStatus._NEW_);
				airlineVolume.setIsValid(true);

				locationVolume = locationVolume
						.add(this.getSysVolumePerContractPeriod(tenderLocation.getAgreementFrom(), tenderLocation.getAgreementTo(),
								airlineVolume.getVolumePeriod(), airlineVolume.getUnit(), tenderLocation.getVolumeUnit(), airlineVolume.getVolume()));

				tenderLocation.getTenderLocationAirlines().add(airlineVolume);
			}

			tenderLocation.setVolume(locationVolume);

		} else if (l.getTotalVolume() != null) {
			tenderLocation.setHasTotalVolume(true);
			tenderLocation.setPeriodType(Period.getByCode(l.getTotalVolume().getPeriodType().value()));
			tenderLocation.setVolume(l.getTotalVolume().getVolume());
			tenderLocation.setVolumeUnit(this.findUnit(l.getTotalVolume().getUOM(), "Total Volume - UOM"));
		}
	}

	protected BigDecimal getSysVolumePerContractPeriod(Date startDate, Date endDate, Period period, Unit fromUnit, Unit toUnit,
			BigDecimal airlineVolume) throws BusinessException {

		BigDecimal contractVolume = BigDecimal.ZERO;

		// calculate volume based on period type and contract period
		DateTime sDate = new DateTime(startDate);
		DateTime eDate = new DateTime(endDate);

		Months months = Months.monthsBetween(sDate, eDate).plus(Months.ONE);
		Years years = Years.yearsBetween(sDate, eDate);

		BigDecimal factor = BigDecimal.ONE;
		switch (period) {
		case _MONTHLY_:
			factor = new BigDecimal(months.getMonths());
			break;
		case _YEARLY_:
			factor = new BigDecimal((years.getYears() > 0) ? years.getYears() : 1);
			break;
		default:
			break;
		}

		Double density = Double.parseDouble(this.systemParamsService.getDensity());
		contractVolume = contractVolume.add(this.unitService.convert(fromUnit, toUnit, airlineVolume, density));
		contractVolume = contractVolume.multiply(factor, MathContext.DECIMAL64);

		return contractVolume;
	}

	protected TimeSerie findTimeSerie(String code, String property) throws SingleResultException {
		try {
			return this.timeSerieService.findByCode(code);
		} catch (ApplicationException nre) {
			throw new SingleResultException(String.format(CmmErrorCode.SINGLE_RESULT_EXCEPTION_MESSAGE.getErrMsg(), "Index Provider", code, property),
					nre);
		} catch (Exception e) {
			throw new SingleResultException(e.getMessage(), e);
		}
	}

	private TenderVolumeOffer getTenderVolumeOffer(TenderLocation invitation) {
		VolumeOfferType tenderInvitationVolumeOffer = invitation.getVolumeOfferType();
		if (tenderInvitationVolumeOffer != null) {
			return TenderVolumeOffer.getByCode(tenderInvitationVolumeOffer.value());
		} else {
			return TenderVolumeOffer._UNDEFINED_;
		}
	}

}
