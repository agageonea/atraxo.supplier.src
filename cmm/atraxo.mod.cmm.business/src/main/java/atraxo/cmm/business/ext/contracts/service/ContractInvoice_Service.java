/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.ext.contracts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.contracts.IContractInvoiceService;
import atraxo.cmm.business.ext.contracts.messageobject.MessageContractObject;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.ext.invoice.InvoiceStatus;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.ContractInvoice;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;

/**
 * Business extensions specific for {@link ContractInvoice} domain entity.
 */
public class ContractInvoice_Service extends atraxo.cmm.business.impl.contracts.ContractInvoice_Service implements IContractInvoiceService {

	private static final Logger LOG = LoggerFactory.getLogger(ContractInvoice.class);

	@Override
	@Transactional
	public void insertUpdateLink(Contract contract, Integer invoiceId, String invoiceCode, String status, Date validFrom, Date validTo,
			DealType dealType) throws BusinessException {
		if (contract != null && invoiceId != null && status != null) {
			ContractInvoice ci;
			try {
				ci = this.findByInvoiceId(invoiceId, dealType);
				ci.setInvoiceStatus(status);
				ci.setValidFrom(validFrom);
				ci.setValidTo(validTo);
				this.update(ci);
			} catch (ApplicationException nre) {
				LOG.warn("Could not find or update invoice with id " + invoiceId + ".Will insert new contract invoice !", nre);
				ci = new ContractInvoice();
				ci.setContract(contract);
				ci.setInvoiceId(invoiceId);
				ci.setInvoiceStatus(status);
				ci.setInvoiceCode(invoiceCode);
				ci.setValidFrom(validFrom);
				ci.setValidTo(validTo);
				ci.setDealType(dealType);
				this.insert(ci);
			}
		}
	}

	@Override
	public void removeLink(Contract contract) throws BusinessException {
		if (this.canModifyStatus(contract)) {
			List<ContractInvoice> contractInvoices = this.findByContract(contract);
			this.delete(contractInvoices);
			for (ContractInvoice ci : contractInvoices) {
				this.sendMessage("invoiceStatusChanel", new MessageContractObject(ci.getInvoiceId(), InvoiceStatus.NO_CONTRACT.getName()));
			}
		} else {
			throw new BusinessException(CmmErrorCode.REMOVE_INVOICE_LINK, CmmErrorCode.REMOVE_INVOICE_LINK.getErrMsg());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.contracts.IContractInvoiceService#canModify(atraxo.cmm.domain.impl.contracts.Contract) return ""-if can you update
	 * the contract, else return the invoice codes which status is awaiting payment or paid.
	 */
	@Override
	public boolean canModifyStatus(Contract contract) throws BusinessException {
		List<ContractInvoice> contractInvoices = this.findByContract(contract);
		return CollectionUtils.isEmpty(contractInvoices);
	}

	private boolean canModify(ContractInvoice ci) {
		return ci.getInvoiceStatus().equalsIgnoreCase(InvoiceStatus.AWAITING_PAYMENT.getName())
				|| ci.getInvoiceStatus().equalsIgnoreCase(InvoiceStatus.PAID.getName())
				|| ci.getInvoiceStatus().equalsIgnoreCase(BillStatus._AWAITING_PAYMENT_.getName())
				|| ci.getInvoiceStatus().equalsIgnoreCase(BillStatus._PAID_.getName());
	}

	@Override
	public void removeLink(Integer invoiceId, DealType dealType) throws BusinessException {
		try {
			ContractInvoice ci = this.findByInvoiceId(invoiceId, dealType);
			this.deleteById(ci.getId());
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw nre;
			}
			LOG.warn("No contract invoice link found " + nre.getMessage(), nre);
		}
	}

	@Override
	public String canModifyDates(Contract contract) throws BusinessException {
		List<ContractInvoice> contractInvoices = this.findByContract(contract);
		StringBuilder sb = new StringBuilder();
		for (ContractInvoice ci : contractInvoices) {
			if ((ci.getValidFrom().before(contract.getValidFrom()) || ci.getValidTo().after(contract.getValidTo())) && (this.canModify(ci))) {
				sb.append("".equals(sb.toString()) ? ci.getInvoiceCode() : ", " + ci.getInvoiceCode());
			}
		}
		return sb.toString();
	}

	@Override
	public void removeOutContract(Contract contract) throws BusinessException {
		List<ContractInvoice> contractInvoices = this.findByContract(contract);
		List<ContractInvoice> removeList = new ArrayList<>();
		boolean flag = false;
		for (ContractInvoice ci : contractInvoices) {
			if (ci.getValidFrom().before(contract.getValidFrom()) || ci.getValidTo().after(contract.getValidTo())) {
				removeList.add(ci);
				flag = true;
			}
		}
		if (flag) {
			if (DealType._BUY_.equals(contract.getDealType())) {
				this.sendMessage("removeSupplierContract", contract);
			} else {
				this.sendMessage("maintainDeliveryNotesContracts", contract);
			}
		}
		this.delete(removeList);
	}
}
