package atraxo.cmm.business.ext.prices.delegate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import atraxo.cmm.business.api.contracts.IPricingBaseService;
import atraxo.cmm.business.api.prices.IContractPriceComponentService;
import atraxo.cmm.business.ext.prices.service.builder.ComponentPriceBuilder;
import atraxo.cmm.business.ext.utils.DatePeriod;
import atraxo.cmm.business.ext.utils.DateUtils;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.fmbas.business.api.quotation.IQuotationValueService;
import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

/**
 * @author zspeter
 */
public class ContractPriceCategory_Bd extends AbstractBusinessDelegate {

	/**
	 * @param category
	 * @throws BusinessException
	 */
	public void createContractPriceComponent(ContractPriceCategory category) throws BusinessException {
		ComponentPriceBuilder cpcBuilder = this.getApplicationContext().getBean(ComponentPriceBuilder.class);

		Date from = category.getPricingBases().getInitialPriceValidFrom() != null ? category.getPricingBases().getInitialPriceValidFrom()
				: category.getPricingBases().getValidFrom();
		Date to = category.getPricingBases().getInitialPriceValidTo() != null ? category.getPricingBases().getInitialPriceValidTo()
				: category.getPricingBases().getValidTo();
		ContractPriceComponent component = cpcBuilder.buildContractPriceComponent(category, category.getInitialPrice(), true, from, to);
		IContractPriceComponentService pService = (IContractPriceComponentService) this.findEntityService(ContractPriceComponent.class);
		category.addToPriceComponents(component);
		pService.insertWithoutPersist(component);
	}

	/**
	 * @param category
	 * @param insertConvertedPrice
	 * @throws BusinessException
	 */
	public void generateContractPriceComponent(ContractPriceCategory category, boolean insertConvertedPrice) throws BusinessException {
		IContractPriceComponentService pService = (IContractPriceComponentService) this.findEntityService(ContractPriceComponent.class);
		IPricingBaseService pbService = (IPricingBaseService) this.findEntityService(PricingBase.class);

		PricingBase pb = category.getPricingBases();
		List<QuotationValue> quotationValues = pbService.getQuotationValues(pb);
		ComponentPriceBuilder cpcBuilder = this.getApplicationContext().getBean(ComponentPriceBuilder.class);
		quotationValues.sort((o1, o2) -> o1.getValidFromDate().compareTo(o2.getValidFromDate()));
		QuotationValue last = quotationValues.get(quotationValues.size() - 1);

		for (QuotationValue quotationValue : quotationValues) {

			DatePeriod period = DateUtils.calculateContractPriceComponentPeriod(pb, quotationValue);

			// if the calculated period is COMPLETELY OUTSIDE the validty period then do not add the new contract price component
			boolean periodNotInContractValidity = (DateUtils.compareDates(period.getStartDate().getTime(), category.getPricingBases().getValidFrom(),
					true) < 0 && DateUtils.compareDates(period.getEndDate().getTime(), category.getPricingBases().getValidFrom(), true) < 0)
					|| (DateUtils.compareDates(period.getStartDate().getTime(), category.getPricingBases().getValidTo(), true) > 0
							&& DateUtils.compareDates(period.getEndDate().getTime(), category.getPricingBases().getValidTo(), true) > 0);

			if (!periodNotInContractValidity) {

				ContractPriceComponent component = cpcBuilder.buildContractPriceComponentFromPeriod(category, quotationValue.getRate(),
						quotationValue.getIsProvisioned(), period);
				category.addToPriceComponents(component);

				this.getBusinessDelegate(ContractPriceComponentConv_Bd.class).insertContractPriceComponentConv(component, insertConvertedPrice);
			}
		}

		if (CollectionUtils.isEmpty(category.getPriceComponents())) {
			DatePeriod period = DateUtils.calculateContractPriceComponentPeriod(pb, last);
			Calendar endDateCal = Calendar.getInstance();
			endDateCal.setTime(pb.getValidTo());
			period.setEndDate(endDateCal);
			ContractPriceComponent component = cpcBuilder.buildContractPriceComponentFromPeriod(category, last.getRate(), last.getIsProvisioned(),
					period);
			category.addToPriceComponents(component);

			this.getBusinessDelegate(ContractPriceComponentConv_Bd.class).insertContractPriceComponentConv(component, insertConvertedPrice);

		}
		List<ContractPriceComponent> cpcList = new ArrayList<>(category.getPriceComponents());
		if (!cpcList.isEmpty()) {
			Collections.sort(cpcList, (o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()));

			ContractPriceComponent firstCpc = cpcList.get(0);
			ContractPriceComponent secondCpc = cpcList.get(0);

			if (firstCpc.getValidFrom().compareTo(pb.getValidFrom()) > 0 && pbService.isIndex(pb) && DateUtils.isMonday(pb.getValidFrom())) {
				IQuotationValueService quotationValueService = (IQuotationValueService) this.findEntityService(QuotationValue.class);
				QuotationValue quotationValue = quotationValueService.getQuotationValueByDate(pb.getQuotation(),
						org.apache.commons.lang.time.DateUtils.addDays(pb.getValidFrom(), -1));
				DatePeriod period = DateUtils.calculateContractPriceComponentPeriod(secondCpc, pb, quotationValue);
				ContractPriceComponent component = cpcBuilder.buildContractPriceComponentFromPeriod(category, quotationValue.getRate(),
						quotationValue.getIsProvisioned(), period);
				category.addToPriceComponents(component);
				this.getBusinessDelegate(ContractPriceComponentConv_Bd.class).insertContractPriceComponentConv(component, insertConvertedPrice);
			}
		}

		this.checkFirstAndLastEntityValidity(category, pService);
	}

	/**
	 * @param category
	 * @throws BusinessException
	 */
	public void reGenerateContractPriceComponent(ContractPriceCategory category) throws BusinessException {
		category.setPriceComponents(new ArrayList<ContractPriceComponent>());
		this.generateContractPriceComponent(category, false);
	}

	/**
	 * If the period is smaller or biggest than pricing base period extend the original period.
	 *
	 * @param category
	 * @param pService
	 * @throws BusinessException
	 */
	private void checkFirstAndLastEntityValidity(ContractPriceCategory category, IContractPriceComponentService pService) throws BusinessException {
		List<ContractPriceComponent> cpcList = new ArrayList<>(category.getPriceComponents());
		Collections.sort(cpcList, (o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()));
		if (!cpcList.isEmpty()) {
			PricingBase pb = category.getPricingBases();
			ContractPriceComponent cpc = cpcList.get(0);
			if (cpc.getValidFrom().compareTo(pb.getValidFrom()) != 0) {
				cpc.setValidFrom(pb.getValidFrom());
				pService.updateWithoutPersist(cpc);
			}
			if (!cpcList.isEmpty()) {
				cpc = cpcList.get(cpcList.size() - 1);
			} else {
				cpc = pService.findById(cpc.getId());
			}
			if (cpc.getValidTo().compareTo(pb.getValidTo()) != 0) {
				cpc.setValidTo(pb.getValidTo());
				cpc.getConvertedPrices().clear();
				this.getBusinessDelegate(ContractPriceComponentConv_Bd.class).insertContractPriceComponentConv(cpc, false);
			}
		}
	}

	/**
	 * @param e
	 * @throws BusinessException
	 */
	public void extendToMargins(ContractPriceCategory e) throws BusinessException {
		List<ContractPriceComponent> list = new ArrayList<>(e.getPriceComponents());
		Collections.sort(list, new Comparator<ContractPriceComponent>() {
			@Override
			public int compare(ContractPriceComponent o1, ContractPriceComponent o2) {
				return o1.getValidFrom().compareTo(o2.getValidFrom());
			}
		});
		Date leftMargin = e.getPricingBases().getValidFrom();
		Date rightMargin = e.getPricingBases().getValidTo();
		List<Object> delList = new ArrayList<>();
		List<ContractPriceComponent> updList = new ArrayList<>();
		boolean first = true;
		ContractPriceComponent lastElement = null;

		for (ContractPriceComponent c : list) {
			lastElement = c;
			if (first && e.getContinous() && c.getValidFrom().after(leftMargin)) {
				c.setValidFrom(leftMargin);
				updList.add(c);
			}
			first = false;
			if (c.getValidTo().before(leftMargin)) {
				e.getPriceComponents().remove(c);
				delList.add(c.getId());
			}
			if (c.getValidFrom().after(rightMargin)) {
				e.getPriceComponents().remove(c);
				delList.add(c.getId());
			}
			if (c.getValidFrom().before(leftMargin) && c.getValidTo().compareTo(leftMargin) >= 0) {
				c.setValidFrom(leftMargin);
				updList.add(c);
			}
			if (c.getValidFrom().compareTo(rightMargin) <= 0 && c.getValidTo().after(rightMargin)) {
				c.setValidTo(rightMargin);
				updList.add(c);
			}
		}
		if (lastElement != null && e.getContinous() && lastElement.getValidTo().before(rightMargin)) {
			lastElement.setValidTo(rightMargin);
			updList.add(lastElement);
		}
	}

	/**
	 * @param category
	 * @throws BusinessException
	 */
	public void updateContractPriceComponent(ContractPriceCategory category) throws BusinessException {
		Iterator<ContractPriceComponent> iter = category.getPriceComponents().iterator();
		List<Object> delIds = new ArrayList<>();
		List<ContractPriceComponent> updList = new ArrayList<>();
		while (iter.hasNext()) {
			ContractPriceComponent e = iter.next();
			if (e.getValidTo().before(category.getPricingBases().getValidFrom())) {
				delIds.add(e.getId());
			} else if (e.getValidTo().after(category.getPricingBases().getValidFrom())
					&& e.getValidFrom().before(category.getPricingBases().getValidFrom())) {
				e.setValidFrom(category.getPricingBases().getValidFrom());
				updList.add(e);
			} else if (e.getValidTo().after(category.getPricingBases().getValidTo())
					&& e.getValidFrom().before(category.getPricingBases().getValidTo())) {
				e.setValidTo(category.getPricingBases().getValidTo());
				updList.add(e);
			} else if (e.getValidFrom().after(category.getPricingBases().getValidTo())) {
				delIds.add(e.getId());
			}
		}
		IContractPriceComponentService srv = (IContractPriceComponentService) this.findEntityService(ContractPriceComponent.class);
		srv.update(updList);
		srv.deleteByIds(delIds);
	}

}
