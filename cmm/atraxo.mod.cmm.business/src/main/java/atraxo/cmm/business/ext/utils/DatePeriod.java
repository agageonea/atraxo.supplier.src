package atraxo.cmm.business.ext.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * Utility helper class, representing a Period of Dates.
 *
 * @author vhojda
 */
public class DatePeriod {

	private Calendar startDate;
	private Calendar endDate;

	/**
	 * Default forced constructor;
	 *
	 * @param startDate
	 * @param endDate
	 */
	public DatePeriod(Calendar startDate, Calendar endDate) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public DatePeriod(Date validFrom, Date validTo) {
		this.startDate = Calendar.getInstance();
		this.startDate.setTime(validFrom);

		this.endDate = Calendar.getInstance();
		this.endDate.setTime(validTo);
	}

	public Calendar getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}

	public Calendar getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}

	public Date[] toArray() {
		Date[] dates = new Date[2];
		dates[0] = this.startDate.getTime();
		dates[1] = this.endDate.getTime();
		return dates;
	}

	@Override
	public String toString() {
		return "DatePeriod [" + this.startDate.getTime().toLocaleString() + " - " + this.endDate.getTime().toLocaleString() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.endDate == null) ? 0 : this.endDate.hashCode());
		result = prime * result + ((this.startDate == null) ? 0 : this.startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		DatePeriod other = (DatePeriod) obj;
		if (this.endDate == null) {
			if (other.endDate != null) {
				return false;
			}
		} else if (!this.endDate.equals(other.endDate)) {
			return false;
		}
		if (this.startDate == null) {
			if (other.startDate != null) {
				return false;
			}
		} else if (!this.startDate.equals(other.startDate)) {
			return false;
		}
		return true;
	}

}
