package atraxo.cmm.business.ws.tender.transformer;

import java.util.List;

import javax.xml.bind.JAXBException;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.iata.fuelplus.iata.tender.CancelledBid;
import seava.j4e.iata.fuelplus.iata.tender.CancelledBids;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderCancelBid;

public class TenderBidCancelTransformer extends DataHubOutTransformer<Contract, FuelTenderCancelBid> {

	@Override
	public FuelTenderCancelBid transformModelsToDTO(List<Contract> entities, long msgId, String reason) throws BusinessException, JAXBException {
		FuelTenderCancelBid cancelBid = new FuelTenderCancelBid();
		CancelledBids canceledBids = new CancelledBids();
		String schemaVersion = null;
		for (Contract contract : entities) {
			schemaVersion = contract.getBidTenderIdentification().getSchemaVersion();
			cancelBid.setTenderIdentification(this.buildTenderIdentification(contract.getBidTenderIdentification(), msgId));

			canceledBids.getCancelledBid().add(this.buildCancelledBid(contract, reason));
		}

		cancelBid.setCancelledBids(canceledBids);
		cancelBid.setSchemaVersion(schemaVersion);
		return cancelBid;
	}

	@Override
	public FuelTenderCancelBid transformModelToDTO(Contract contract, long msgId, String reason) throws BusinessException, JAXBException {
		FuelTenderCancelBid cancelBid = new FuelTenderCancelBid();
		cancelBid.setTenderIdentification(this.buildTenderIdentification(contract.getBidTenderIdentification(), msgId));

		CancelledBids canceledBids = new CancelledBids();
		canceledBids.getCancelledBid().add(this.buildCancelledBid(contract, reason));
		cancelBid.setCancelledBids(canceledBids);
		cancelBid.setSchemaVersion(contract.getBidTenderIdentification().getSchemaVersion());

		return cancelBid;
	}

	private CancelledBid buildCancelledBid(Contract contract, String reason) throws BusinessException {
		CancelledBid cancelledBid = new CancelledBid();
		cancelledBid.setBidIdentification(this.buildBidIdentification(contract));
		cancelledBid.setComments(reason);

		return cancelledBid;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubOutTransformer#transformModelToDTO(atraxo.fmbas.domain.impl.abstracts.AbstractEntity, long, long,
	 * atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames)
	 */
	@Override
	public FuelTenderCancelBid transformModelToDTO(Contract modelEntity, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubOutTransformer#transformModelsToDTO(java.util.List, long, long,
	 * atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames)
	 */
	@Override
	public FuelTenderCancelBid transformModelsToDTO(List<Contract> modelEntities, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException {
		throw new ApplicationException(ErrorCode.G_RUNTIME_ERROR, MSG);
	}
}
