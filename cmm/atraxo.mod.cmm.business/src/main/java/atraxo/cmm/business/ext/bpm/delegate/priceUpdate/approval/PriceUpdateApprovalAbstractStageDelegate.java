package atraxo.cmm.business.ext.bpm.delegate.priceUpdate.approval;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.priceUpdate.IPriceUpdateService;
import atraxo.cmm.domain.ext.mailmerge.dto.PriceUpdateDto;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdate;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.workflow.IWorkflowParameterService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.ApprovalDelegate;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailAddressesDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;
import seava.j4e.commons.utils.BigDecimalFormater;

/**
 * Price Update Workflow Stage Delegate; contains common methods and functionality for a Price Update Workflow Stage (first, second or third)
 *
 * @author vhojda
 */
public abstract class PriceUpdateApprovalAbstractStageDelegate extends ApprovalDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(PriceUpdateApprovalAbstractStageDelegate.class);

	/**
	 * Updates a specific <code>PriceUpdateDto</code> with specific information
	 *
	 * @param priceUpdateDto
	 */
	protected abstract void updatePriceEmailDto(PriceUpdateDto priceUpdateDto);

	/**
	 * Gets the roles approver variable for the current stage
	 *
	 * @return
	 */
	protected abstract String getStageApprovalRolesVariable();

	@Autowired
	protected IPriceUpdateService priceUpdateService;

	@Autowired
	protected ICustomerService customerService;

	@Autowired
	protected IWorkflowParameterService workflowParameterService;

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.bpm.delegate.AbstractJavaDelegate#execute()
	 */
	@Override
	public void execute() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}

		// reset the next approval stage variable
		this.setNextApprovalStageRequiredVariable(true);

		WorkflowParameter param = this.workflowParameterService.findByKey(Long.valueOf(this.workflowId), this.getStageApprovalRolesVariable());

		// extract the approvers and setup the email data
		List<UserSupp> users = this.extractApprovers(this.getStageApprovalRolesVariable(), param.getMandatory());

		if (!CollectionUtils.isEmpty(users)) {
			PriceUpdate priceUpdate = this.getPriceUpdate();
			if (priceUpdate != null) {
				EmailDto emailDTO = this.extractEmailDto(users, priceUpdate);
				if (emailDTO != null) {
					this.execution.setVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_DTO, emailDTO);
				}
			}

		} else {
			this.setNextApprovalStageRequiredVariable(false);
		}

		// make sure to reset the mail components
		this.resetMailComponentVariables();

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
	}

	/**
	 * @param required
	 */
	protected void setNextApprovalStageRequiredVariable(boolean required) {
		this.execution.setVariable(WorkflowVariablesConstants.NEXT_APPROVAL_STAGE_REQUIRED, required);
	}

	/**
	 * Extracts and sets on thw workflow all the needed EmailDto data
	 *
	 * @param users list of users
	 * @param priceUpdate the price Update object
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	protected EmailDto extractEmailDto(List<UserSupp> users, PriceUpdate priceUpdate) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START extractEmailDto()");
		}

		String priceCategoryName = "";
		String priceUpdateLocationArea = "";
		String newPriceValue = "";
		String currencyCode = "";
		String unitCode = "";
		Date validFrom = null;

		EmailDto emailDTO = new EmailDto();

		// extract data from the Price update object
		try {
			priceCategoryName = priceUpdate.getPriceCategory().getName();
			if (priceUpdate.getLocation() != null) {// this CAN be null
				priceUpdateLocationArea = priceUpdate.getLocation().getName();
			} else {
				String subsidiaryId = priceUpdate.getSubsidiaryId();
				Customer company = this.customerService.findByRefid(subsidiaryId);
				if (company.getAssignedArea() != null) {
					priceUpdateLocationArea = company.getAssignedArea().getName();
				}
			}
			if (priceUpdate.getCurrency() != null) {
				currencyCode = priceUpdate.getCurrency().getCode();
			}
			if (priceUpdate.getNewPrice() != null) {
				newPriceValue = BigDecimalFormater.dynamicNumberFormat(priceUpdate.getNewPrice(), this.systemParamsService.getDecimalsForUnit());
			}
			unitCode = priceUpdate.getUnit().getCode();
			validFrom = priceUpdate.getValidFrom();
		} catch (Exception e) {
			LOGGER.error("ERROR:could not extract data from the price update " + priceUpdate.getId() + " in order to send the mail ! ", e);
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
					"Could not extract data from the price update " + priceUpdate.getId() + "  in order to send the mail !");
			emailDTO = null;
		}

		if (emailDTO != null) {

			// set subject
			emailDTO.setSubject("Price update request for " + priceCategoryName
					+ (!StringUtils.isEmpty(priceUpdateLocationArea) ? (" at " + priceUpdateLocationArea) : "") + " awaiting your approval");

			// set attachments
			Object emailAttachments = this.execution.getVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_ATTACHMENTS);

			if (emailAttachments != null && emailAttachments instanceof List) {
				emailDTO.setAttachments((List<String>) emailAttachments);
			}

			// common email data for all receivers taken from workflow
			String fullNameOfRequester = this.getVariableFromWorkflow(WorkflowVariablesConstants.FULLNAME_REQUESTER_VAR);
			String approvalNote = this.getVariableFromWorkflow((WorkflowVariablesConstants.APPROVE_NOTE));

			// set receivers along with the data (JSON format)
			List<EmailAddressesDto> emailAddressesDtoList = new ArrayList<>();
			for (UserSupp user : users) {

				PriceUpdateDto dataDto = new PriceUpdateDto();
				dataDto.setFullnameOfRequester(fullNameOfRequester);
				dataDto.setCurrencyCode(currencyCode);
				dataDto.setFullName(user.getFirstName() + " " + user.getLastName());
				dataDto.setNewPriceValue(newPriceValue);
				dataDto.setTitle(user.getTitle().getName());

				dataDto.setUnitCode(unitCode);
				dataDto.setValidFrom(validFrom);
				dataDto.setPriceCategoryName(priceCategoryName);
				dataDto.setPriceUpdateLocationArea(priceUpdateLocationArea);

				dataDto.setApprovalRequestNote(approvalNote);

				// set customer price update email data for each approval step
				this.updatePriceEmailDto(dataDto);

				EmailAddressesDto emailAddressesDto = new EmailAddressesDto();
				emailAddressesDto.setAddress(user.getEmail());
				emailAddressesDto.setData(dataDto);
				emailAddressesDtoList.add(emailAddressesDto);
			}

			// set the receivers
			emailDTO.setTo(emailAddressesDtoList);
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END extractEmailDto()");
		}

		return emailDTO;
	}

	/**
	 * Gets the Price Update
	 *
	 * @return
	 * @throws Exception
	 */
	protected PriceUpdate getPriceUpdate() throws Exception {
		PriceUpdate priceUpdate = null;

		String priceUpdateIdString = (String) this.execution.getVariable(WorkflowVariablesConstants.PRICE_UPDATE_ID_VAR);
		if (!StringUtils.isEmpty(priceUpdateIdString)) {
			try {
				Integer priceUpdateId = Integer.parseInt(priceUpdateIdString);
				try {
					priceUpdate = this.priceUpdateService.findByBusiness(priceUpdateId);
				} catch (Exception e) {
					LOGGER.error("ERROR:could not find a price update for ID ", priceUpdateId);
					this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
							"Could not find a price update for ID " + priceUpdateId, e);
				}

			} catch (NumberFormatException e) {
				LOGGER.error("ERROR:could not parse the received Price Update ID ", priceUpdateIdString);
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						"Could not parse the received Price Update ID " + priceUpdateIdString, e);
			}
		} else {
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
					"The id for the price update is empty !" + priceUpdateIdString);
		}

		return priceUpdate;
	}
}
