package atraxo.cmm.business.ext.mailMerge.delegate.builders;

import atraxo.cmm.domain.ext.mailmerge.dto.contract.LocationDto;
import atraxo.fmbas.domain.impl.geo.Locations;

/**
 * @author apetho
 */
public final class LocationDtoBuilder {

	private LocationDtoBuilder() {
	}

	public static final LocationDto buildLocation(Locations location) {
		LocationDto dto = new LocationDto();
		if (location != null) {
			dto.setCode(location.getCode());
			dto.setName(location.getName());
			dto.setIataCode(location.getIataCode());
			dto.setIcaoCode(location.getIcaoCode());
			dto.setLatitude(location.getLatitude());
			dto.setLongitude(location.getLongitude());
			dto.setValidFrom(location.getValidFrom());
			dto.setValidTo(location.getValidTo());
		}
		return dto;
	}
}
