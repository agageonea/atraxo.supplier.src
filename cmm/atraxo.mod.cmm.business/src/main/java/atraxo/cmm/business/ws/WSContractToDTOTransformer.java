/**
 *
 */
package atraxo.cmm.business.ws;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.cmm.domain.ws.CONTRACTDETAILS;
import atraxo.cmm.domain.ws.CONTRACTDETAILS.CONTRACTS;
import atraxo.cmm.domain.ws.CONTRACTDETAILS.CONTRACTS.CONTRACT;
import atraxo.cmm.domain.ws.CONTRACTDETAILS.HEADER;
import atraxo.fmbas.business.api.ws.WSModelToDTOTransformer;
import atraxo.fmbas.business.ws.ExternalInterfaceWSHelper;
import atraxo.fmbas.domain.ws.dto.WSPayloadDataDTO;
import atraxo.fmbas.domain.ws.dto.WSTransportDataType;
import seava.j4e.commons.utils.XMLUtils;

/**
 * @author vhojda
 */
public class WSContractToDTOTransformer implements WSModelToDTOTransformer<Contract> {

	private static final Logger LOG = LoggerFactory.getLogger(WSContractToDTOTransformer.class);

	private static DateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	/**
	 *
	 */
	public WSContractToDTOTransformer() {

	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.api.ws.WSModelToDTOTransformer#transformModelToDTO(java.util.Collection, java.util.Map)
	 */
	@Override
	public WSPayloadDataDTO transformModelToDTO(Collection<Contract> modelEntities, Map<String, String> paramMap) throws JAXBException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("START transformModelToDTO()");
		}

		WSPayloadDataDTO returnedDto = new WSPayloadDataDTO();
		CONTRACTDETAILS contractDetails = new CONTRACTDETAILS();

		// set the contracts
		CONTRACTS contracts = new CONTRACTS();
		contractDetails.setCONTRACTS(contracts);
		int batchId = 1;
		for (Contract modelEntity : modelEntities) {
			Contract contractModel = null;
			try {
				contractModel = modelEntity;
			} catch (ClassCastException e) {
				LOG.error("ERROR: Could not cast " + modelEntity + " to Contract !!!!", e);
				throw e;
			}
			if (contractModel != null) {
				List<ShipTo> shipToList = new ArrayList<>(contractModel.getShipTo());
				if (shipToList.isEmpty()) {
					if (contractModel.getCustomer().getAccountNumber() != null && !contractModel.getCustomer().getAccountNumber().isEmpty()) {
						contracts.getCONTRACT().add(this.toCONTRACT(contractModel, null, batchId));
					}
				} else {
					for (ShipTo shipTo : shipToList) {
						if (shipTo.getCustomer().getAccountNumber() != null && !shipTo.getCustomer().getAccountNumber().isEmpty()) {
							contracts.getCONTRACT().add(this.toCONTRACT(shipTo.getContract(), shipTo, batchId));
						}
					}
				}
				batchId++;
			}
		}

		// set header
		contractDetails.setHEADER(this.toHEADER(contracts.getCONTRACT().size(), paramMap));

		// set the payload
		returnedDto.setPayload(XMLUtils.toXML(contractDetails, contractDetails.getClass()));

		if (LOG.isDebugEnabled()) {
			LOG.debug("END transformModelToDTO()");
		}
		return returnedDto;
	}

	/**
	 * @param contract
	 * @return
	 */
	private CONTRACT toCONTRACT(Contract contractModel, ShipTo shipTo, int batchId) {
		CONTRACT contract = new CONTRACT();
		contract.setCONTRACTCODE(shipTo != null ? shipTo.getCustomer().getAccountNumber() : contractModel.getCustomer().getAccountNumber());
		contract.setGSAPCONTRACTNO(contractModel.getCode());
		contract.setSOLDTONUMBER(shipTo != null ? shipTo.getCustomer().getAccountNumber() : contractModel.getCustomer().getAccountNumber());
		contract.setIATACODE(contractModel.getLocation().getIataCode());
		contract.setMATERIALNUMBER(contractModel.getProduct().getIataName());
		contract.setIATAPRODUCTCODE(contractModel.getProduct().getIataName());
		Date validFrom = shipTo != null ? shipTo.getValidFrom() : contractModel.getValidFrom();
		Date validTo = shipTo != null ? shipTo.getValidTo() : contractModel.getValidTo();
		contract.setVALIDFROM(sdfDate.format(validFrom));
		contract.setVALIDTO(this.convertDate(validTo));
		contract.setPERCENTAGESPLIT("100");
		contract.setBATCHID(String.valueOf(batchId));
		return contract;
	}

	/**
	 * Creates <code>HEADER</code>, given some of the properties
	 *
	 * @param headerData
	 * @return
	 */
	private HEADER toHEADER(int entitiesSize, Map<String, String> paramMap) {
		HEADER header = new HEADER();
		header.setCOUNT(String.valueOf(entitiesSize));
		header.setENV(paramMap.get(ExternalInterfaceWSHelper.EXTERNAL_PARAM_ENVIRONMENT));
		header.setINTERFACEID("E-BITS_MASTER");
		header.setINTERFACENAME("MASTERDATA");
		header.setMESSAGEID(paramMap.get(ExternalInterfaceWSHelper.EXTERNAL_PARAM_MESSAGE_ID));
		header.setSOURCESYSTEM("supplierONE");
		header.setTARGETSYSTEM("eBits AVReFueling");
		return header;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.api.ws.WSModelToDTOTransformer#getTransportDataType ()
	 */
	@Override
	public WSTransportDataType getTransportDataType() {
		return WSTransportDataType.CONTRACTS;
	}

	/**
	 * @param date
	 * @return - String
	 */
	private String convertDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return sdfDate.format(cal.getTime());
	}

}
