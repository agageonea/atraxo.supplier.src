package atraxo.cmm.business.ext.mailMerge.delegate.builders;

import atraxo.cmm.domain.ext.mailmerge.dto.contract.SupplierDto;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;

/**
 * @author apetho
 */
public final class SupplierDtoBuilder {

	private SupplierDtoBuilder() {

	}

	public static final SupplierDto buildSupplier(Suppliers supplier) {
		SupplierDto dto = new SupplierDto();
		if (supplier != null) {
			dto.setCode(supplier.getCode());
			dto.setIataCode(supplier.getIataCode());
			dto.setName(supplier.getName());
		}
		return dto;
	}
}
