/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.contracts;

import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.ContractInvoice;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Contract} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Contract_Service extends AbstractEntityService<Contract> {

	/**
	 * Public constructor for Contract_Service
	 */
	public Contract_Service() {
		super();
	}

	/**
	 * Public constructor for Contract_Service
	 * 
	 * @param em
	 */
	public Contract_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Contract> getEntityClass() {
		return Contract.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Contract
	 */
	public Contract findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Contract.NQ_FIND_BY_CODE, Contract.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Contract", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Contract", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Contract
	 */
	public Contract findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Contract.NQ_FIND_BY_BUSINESS,
							Contract.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Contract", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Contract", "id"), nure);
		}
	}

	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<Contract>
	 */
	public List<Contract> findByLocation(Locations location) {
		return this.findByLocationId(location.getId());
	}
	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<Contract>
	 */
	public List<Contract> findByLocationId(Integer locationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.location.id = :locationId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("locationId", locationId).getResultList();
	}
	/**
	 * Find by reference: holder
	 *
	 * @param holder
	 * @return List<Contract>
	 */
	public List<Contract> findByHolder(Customer holder) {
		return this.findByHolderId(holder.getId());
	}
	/**
	 * Find by ID of reference: holder.id
	 *
	 * @param holderId
	 * @return List<Contract>
	 */
	public List<Contract> findByHolderId(Integer holderId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.holder.id = :holderId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("holderId", holderId).getResultList();
	}
	/**
	 * Find by reference: intoPlaneAgent
	 *
	 * @param intoPlaneAgent
	 * @return List<Contract>
	 */
	public List<Contract> findByIntoPlaneAgent(Suppliers intoPlaneAgent) {
		return this.findByIntoPlaneAgentId(intoPlaneAgent.getId());
	}
	/**
	 * Find by ID of reference: intoPlaneAgent.id
	 *
	 * @param intoPlaneAgentId
	 * @return List<Contract>
	 */
	public List<Contract> findByIntoPlaneAgentId(Integer intoPlaneAgentId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.intoPlaneAgent.id = :intoPlaneAgentId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("intoPlaneAgentId", intoPlaneAgentId)
				.getResultList();
	}
	/**
	 * Find by reference: responsibleBuyer
	 *
	 * @param responsibleBuyer
	 * @return List<Contract>
	 */
	public List<Contract> findByResponsibleBuyer(UserSupp responsibleBuyer) {
		return this.findByResponsibleBuyerId(responsibleBuyer.getId());
	}
	/**
	 * Find by ID of reference: responsibleBuyer.id
	 *
	 * @param responsibleBuyerId
	 * @return List<Contract>
	 */
	public List<Contract> findByResponsibleBuyerId(String responsibleBuyerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.responsibleBuyer.id = :responsibleBuyerId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("responsibleBuyerId", responsibleBuyerId)
				.getResultList();
	}
	/**
	 * Find by reference: contact
	 *
	 * @param contact
	 * @return List<Contract>
	 */
	public List<Contract> findByContact(Contacts contact) {
		return this.findByContactId(contact.getId());
	}
	/**
	 * Find by ID of reference: contact.id
	 *
	 * @param contactId
	 * @return List<Contract>
	 */
	public List<Contract> findByContactId(Integer contactId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.contact.id = :contactId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contactId", contactId).getResultList();
	}
	/**
	 * Find by reference: contractVolumeUnit
	 *
	 * @param contractVolumeUnit
	 * @return List<Contract>
	 */
	public List<Contract> findByContractVolumeUnit(Unit contractVolumeUnit) {
		return this.findByContractVolumeUnitId(contractVolumeUnit.getId());
	}
	/**
	 * Find by ID of reference: contractVolumeUnit.id
	 *
	 * @param contractVolumeUnitId
	 * @return List<Contract>
	 */
	public List<Contract> findByContractVolumeUnitId(
			Integer contractVolumeUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.contractVolumeUnit.id = :contractVolumeUnitId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractVolumeUnitId", contractVolumeUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: resaleRef
	 *
	 * @param resaleRef
	 * @return List<Contract>
	 */
	public List<Contract> findByResaleRef(Contract resaleRef) {
		return this.findByResaleRefId(resaleRef.getId());
	}
	/**
	 * Find by ID of reference: resaleRef.id
	 *
	 * @param resaleRefId
	 * @return List<Contract>
	 */
	public List<Contract> findByResaleRefId(Integer resaleRefId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.resaleRef.id = :resaleRefId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("resaleRefId", resaleRefId).getResultList();
	}
	/**
	 * Find by reference: settlementCurr
	 *
	 * @param settlementCurr
	 * @return List<Contract>
	 */
	public List<Contract> findBySettlementCurr(Currencies settlementCurr) {
		return this.findBySettlementCurrId(settlementCurr.getId());
	}
	/**
	 * Find by ID of reference: settlementCurr.id
	 *
	 * @param settlementCurrId
	 * @return List<Contract>
	 */
	public List<Contract> findBySettlementCurrId(Integer settlementCurrId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.settlementCurr.id = :settlementCurrId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("settlementCurrId", settlementCurrId)
				.getResultList();
	}
	/**
	 * Find by reference: settlementUnit
	 *
	 * @param settlementUnit
	 * @return List<Contract>
	 */
	public List<Contract> findBySettlementUnit(Unit settlementUnit) {
		return this.findBySettlementUnitId(settlementUnit.getId());
	}
	/**
	 * Find by ID of reference: settlementUnit.id
	 *
	 * @param settlementUnitId
	 * @return List<Contract>
	 */
	public List<Contract> findBySettlementUnitId(Integer settlementUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.settlementUnit.id = :settlementUnitId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("settlementUnitId", settlementUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: averageMethod
	 *
	 * @param averageMethod
	 * @return List<Contract>
	 */
	public List<Contract> findByAverageMethod(AverageMethod averageMethod) {
		return this.findByAverageMethodId(averageMethod.getId());
	}
	/**
	 * Find by ID of reference: averageMethod.id
	 *
	 * @param averageMethodId
	 * @return List<Contract>
	 */
	public List<Contract> findByAverageMethodId(Integer averageMethodId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.averageMethod.id = :averageMethodId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("averageMethodId", averageMethodId)
				.getResultList();
	}
	/**
	 * Find by reference: financialSource
	 *
	 * @param financialSource
	 * @return List<Contract>
	 */
	public List<Contract> findByFinancialSource(FinancialSources financialSource) {
		return this.findByFinancialSourceId(financialSource.getId());
	}
	/**
	 * Find by ID of reference: financialSource.id
	 *
	 * @param financialSourceId
	 * @return List<Contract>
	 */
	public List<Contract> findByFinancialSourceId(Integer financialSourceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.financialSource.id = :financialSourceId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("financialSourceId", financialSourceId)
				.getResultList();
	}
	/**
	 * Find by reference: riskHolder
	 *
	 * @param riskHolder
	 * @return List<Contract>
	 */
	public List<Contract> findByRiskHolder(Customer riskHolder) {
		return this.findByRiskHolderId(riskHolder.getId());
	}
	/**
	 * Find by ID of reference: riskHolder.id
	 *
	 * @param riskHolderId
	 * @return List<Contract>
	 */
	public List<Contract> findByRiskHolderId(Integer riskHolderId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.riskHolder.id = :riskHolderId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("riskHolderId", riskHolderId).getResultList();
	}
	/**
	 * Find by reference: supplier
	 *
	 * @param supplier
	 * @return List<Contract>
	 */
	public List<Contract> findBySupplier(Suppliers supplier) {
		return this.findBySupplierId(supplier.getId());
	}
	/**
	 * Find by ID of reference: supplier.id
	 *
	 * @param supplierId
	 * @return List<Contract>
	 */
	public List<Contract> findBySupplierId(Integer supplierId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.supplier.id = :supplierId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("supplierId", supplierId).getResultList();
	}
	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<Contract>
	 */
	public List<Contract> findByCustomer(Customer customer) {
		return this.findByCustomerId(customer.getId());
	}
	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<Contract>
	 */
	public List<Contract> findByCustomerId(Integer customerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.customer.id = :customerId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerId", customerId).getResultList();
	}
	/**
	 * Find by reference: billTo
	 *
	 * @param billTo
	 * @return List<Contract>
	 */
	public List<Contract> findByBillTo(Customer billTo) {
		return this.findByBillToId(billTo.getId());
	}
	/**
	 * Find by ID of reference: billTo.id
	 *
	 * @param billToId
	 * @return List<Contract>
	 */
	public List<Contract> findByBillToId(Integer billToId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.billTo.id = :billToId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("billToId", billToId).getResultList();
	}
	/**
	 * Find by reference: bidTenderIdentification
	 *
	 * @param bidTenderIdentification
	 * @return List<Contract>
	 */
	public List<Contract> findByBidTenderIdentification(
			Tender bidTenderIdentification) {
		return this.findByBidTenderIdentificationId(bidTenderIdentification
				.getId());
	}
	/**
	 * Find by ID of reference: bidTenderIdentification.id
	 *
	 * @param bidTenderIdentificationId
	 * @return List<Contract>
	 */
	public List<Contract> findByBidTenderIdentificationId(
			Integer bidTenderIdentificationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.bidTenderIdentification.id = :bidTenderIdentificationId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("bidTenderIdentificationId",
						bidTenderIdentificationId).getResultList();
	}
	/**
	 * Find by reference: bidTenderLocation
	 *
	 * @param bidTenderLocation
	 * @return List<Contract>
	 */
	public List<Contract> findByBidTenderLocation(
			TenderLocation bidTenderLocation) {
		return this.findByBidTenderLocationId(bidTenderLocation.getId());
	}
	/**
	 * Find by ID of reference: bidTenderLocation.id
	 *
	 * @param bidTenderLocationId
	 * @return List<Contract>
	 */
	public List<Contract> findByBidTenderLocationId(Integer bidTenderLocationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.bidTenderLocation.id = :bidTenderLocationId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("bidTenderLocationId", bidTenderLocationId)
				.getResultList();
	}
	/**
	 * Find by reference: bidReference
	 *
	 * @param bidReference
	 * @return List<Contract>
	 */
	public List<Contract> findByBidReference(Contract bidReference) {
		return this.findByBidReferenceId(bidReference.getId());
	}
	/**
	 * Find by ID of reference: bidReference.id
	 *
	 * @param bidReferenceId
	 * @return List<Contract>
	 */
	public List<Contract> findByBidReferenceId(Integer bidReferenceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.bidReference.id = :bidReferenceId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("bidReferenceId", bidReferenceId).getResultList();
	}
	/**
	 * Find by reference: invoiceTemplate
	 *
	 * @param invoiceTemplate
	 * @return List<Contract>
	 */
	public List<Contract> findByInvoiceTemplate(ExternalReport invoiceTemplate) {
		return this.findByInvoiceTemplateId(invoiceTemplate.getId());
	}
	/**
	 * Find by ID of reference: invoiceTemplate.id
	 *
	 * @param invoiceTemplateId
	 * @return List<Contract>
	 */
	public List<Contract> findByInvoiceTemplateId(String invoiceTemplateId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contract e where e.clientId = :clientId and e.invoiceTemplate.id = :invoiceTemplateId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("invoiceTemplateId", invoiceTemplateId)
				.getResultList();
	}
	/**
	 * Find by reference: contractInvoice
	 *
	 * @param contractInvoice
	 * @return List<Contract>
	 */
	public List<Contract> findByContractInvoice(ContractInvoice contractInvoice) {
		return this.findByContractInvoiceId(contractInvoice.getId());
	}
	/**
	 * Find by ID of reference: contractInvoice.id
	 *
	 * @param contractInvoiceId
	 * @return List<Contract>
	 */
	public List<Contract> findByContractInvoiceId(Integer contractInvoiceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Contract e, IN (e.contractInvoice) c where e.clientId = :clientId and c.id = :contractInvoiceId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractInvoiceId", contractInvoiceId)
				.getResultList();
	}
	/**
	 * Find by reference: priceCategories
	 *
	 * @param priceCategories
	 * @return List<Contract>
	 */
	public List<Contract> findByPriceCategories(
			ContractPriceCategory priceCategories) {
		return this.findByPriceCategoriesId(priceCategories.getId());
	}
	/**
	 * Find by ID of reference: priceCategories.id
	 *
	 * @param priceCategoriesId
	 * @return List<Contract>
	 */
	public List<Contract> findByPriceCategoriesId(Integer priceCategoriesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Contract e, IN (e.priceCategories) c where e.clientId = :clientId and c.id = :priceCategoriesId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("priceCategoriesId", priceCategoriesId)
				.getResultList();
	}
	/**
	 * Find by reference: pricingBases
	 *
	 * @param pricingBases
	 * @return List<Contract>
	 */
	public List<Contract> findByPricingBases(PricingBase pricingBases) {
		return this.findByPricingBasesId(pricingBases.getId());
	}
	/**
	 * Find by ID of reference: pricingBases.id
	 *
	 * @param pricingBasesId
	 * @return List<Contract>
	 */
	public List<Contract> findByPricingBasesId(Integer pricingBasesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Contract e, IN (e.pricingBases) c where e.clientId = :clientId and c.id = :pricingBasesId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("pricingBasesId", pricingBasesId).getResultList();
	}
	/**
	 * Find by reference: shipTo
	 *
	 * @param shipTo
	 * @return List<Contract>
	 */
	public List<Contract> findByShipTo(ShipTo shipTo) {
		return this.findByShipToId(shipTo.getId());
	}
	/**
	 * Find by ID of reference: shipTo.id
	 *
	 * @param shipToId
	 * @return List<Contract>
	 */
	public List<Contract> findByShipToId(Integer shipToId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Contract e, IN (e.shipTo) c where e.clientId = :clientId and c.id = :shipToId",
						Contract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("shipToId", shipToId).getResultList();
	}
}
