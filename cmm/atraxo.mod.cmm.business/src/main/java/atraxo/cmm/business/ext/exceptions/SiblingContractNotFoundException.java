package atraxo.cmm.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.IErrorCode;

/**
 * @author zspeter
 */
public class SiblingContractNotFoundException extends BusinessException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1891044242977693701L;

	/**
	 * Default constructor.
	 */
	public SiblingContractNotFoundException() {
		super(CmmErrorCode.CONTRACT_NOT_FOUND, "Cannot find a subling contract for a sell contract.");
	}

	public SiblingContractNotFoundException(IErrorCode errorCode, String errorDetails) {
		super(errorCode, errorDetails);
	}
}
