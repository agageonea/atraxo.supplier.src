/**
 *
 */
package atraxo.cmm.business.ext.bpm.delegate.contract.approval;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.ext.bpm.delegate.ContractDelegate;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author vhojda
 */
public class ApproveContractDelegate extends ContractDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApproveContractDelegate.class);

	private static final String FAILED_TO_ACTIVATE = "Failed to activate:";

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.bpm.delegate.AbstractJavaDelegate#execute()
	 */
	@Override
	public void execute() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}

		Contract contract = this.getContract();
		if (contract != null) {
			// assume approved
			contract.setBidApprovalStatus(BidApprovalStatus._APPROVED_);

			// attempt to activate
			try {
				this.contractSrv.setActive(contract);
			} catch (BusinessException e) {
				// business exception, so reject the contract approval
				if (LOGGER.isTraceEnabled()) {
					LOGGER.trace("Got a known Business Exception, proceeed with rejecting the activation normally !", e);
				}
				this.contractSrv.reject(contract, BidApprovalStatus._REJECTED_.getName(), FAILED_TO_ACTIVATE + e.getMessage());
			}

			// if activated before, approve
			if (contract.getStatus().equals(ContractStatus._ACTIVE_)) {
				this.contractSrv.approve(contract, BidApprovalStatus._APPROVED_.getName(), this.getNote());
			}
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#getSessionUserCode()
	 */
	@Override
	public String getSessionUserCode() {
		return this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME);
	}

}
