package atraxo.cmm.business.ext.bpm.delegate;

import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractChangeService;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ext.bpm.util.ContractVolumeExtracter;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.ApprovalDelegate;
import atraxo.fmbas.domain.impl.uom.Unit;

/**
 * Abstract <code>ApprovalDelegate</code> class with the scope of offering common ground for delegates that use contracts.
 *
 * @author vhojda
 */
public abstract class ContractDelegate extends ApprovalDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContractDelegate.class);

	@Autowired
	protected IUnitService unitSrv;

	@Autowired
	protected IContractService contractSrv;

	@Autowired
	protected ContractVolumeExtracter contractVolumeExtracter;

	@Autowired
	protected IContractChangeService contractChangeService;

	/**
	 * Gets the Contract
	 *
	 * @return
	 * @throws Exception
	 */
	protected Contract getContract() throws Exception {
		Contract contract = null;

		String bidIdString = (String) this.execution.getVariable(WorkflowVariablesConstants.CONTRACT_ID_VAR);
		if (!StringUtils.isEmpty(bidIdString)) {
			try {
				Integer contractId = Integer.parseInt(bidIdString);
				try {
					contract = this.contractSrv.findByBusiness(contractId);
				} catch (Exception e) {
					LOGGER.error("ERROR:could not find a contract for ID " + contractId, e);
					this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
							String.format(CmmErrorCode.CONTRACT_WORKFLOW_ENTITY_NOT_EXISTING.getErrMsg(), contractId), e);
				}

			} catch (NumberFormatException e) {
				LOGGER.error("ERROR:could not parse the received Contract ID " + bidIdString, e);
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						String.format(CmmErrorCode.CONTRACT_WORKFLOW_ENTITY_ID_NO_PARSE.getErrMsg(), bidIdString), e);
			}
		} else {
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
					CmmErrorCode.CONTRACT_WORKFLOW_ENTITY_ID_EMPTY.getErrMsg());
		}

		return contract;
	}

	/**
	 * Gets the Contract BID
	 *
	 * @return
	 * @throws Exception
	 */
	protected Contract getBidContract() throws Exception {
		Contract bidContract = null;

		String bidIdString = (String) this.execution.getVariable(WorkflowVariablesConstants.BID_ID_VAR);
		if (!StringUtils.isEmpty(bidIdString)) {
			try {
				Integer bidId = Integer.parseInt(bidIdString);
				try {
					bidContract = this.contractSrv.findByBusiness(bidId);
				} catch (Exception e) {
					LOGGER.error("ERROR:could not find a bid for ID " + bidId, e);
					this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
							String.format(CmmErrorCode.BID_APPROVAL_WORKFLOW_ENTITY_NOT_EXISTING.getErrMsg(), bidId), e);
				}

			} catch (NumberFormatException e) {
				LOGGER.error("ERROR:could not parse the received Bid ID " + bidIdString, e);
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						String.format(CmmErrorCode.BID_APPROVAL_WORKFLOW_ENTITY_ID_NO_PARSE.getErrMsg(), bidIdString), e);
			}
		} else {
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
					CmmErrorCode.BID_APPROVAL_WORKFLOW_ENTITY_ID_EMPTY.getErrMsg());
		}

		return bidContract;
	}

	/**
	 * Calculates if the volume of a contract is over(bigger than) the one specified by a certain workflow variable
	 *
	 * @param contract the contract in case
	 * @param volumeWkfVar the name of the variable that specifies the threshold volume
	 * @returns true if the contract volume is over (bigger) that the specified by the workflow volumne, false otherwise and null if errors occured
	 *          during processing/extraction
	 * @throws Exception
	 */
	protected Boolean isContractOverVolume(Contract contract, String volumeWkfVar) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START extractContractVolume()");
		}
		Boolean overVolume = null;

		Unit thresholdUnit = this.getUnitVariableFromWorkflow(WorkflowVariablesConstants.THRESHOLD_VOLUME_UNIT);
		Integer thresholdVolume = this.getVolumeVariableFromWorkflow(volumeWkfVar);
		if (thresholdUnit != null && thresholdVolume != null) {
			overVolume = this.contractVolumeExtracter.isOverVolume(contract, thresholdUnit, thresholdVolume);
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END extractContractVolume()");
		}

		return overVolume;
	}

	/**
	 * Extracts the <code>Unit</code> variable from workflow.
	 *
	 * @param varName the name of the workflow variable
	 * @returns the <code>Unit</code> if found, null otherwise
	 * @throws Exception
	 */
	protected Unit getUnitVariableFromWorkflow(String varName) throws Exception {
		Unit thresholdUnit = null;
		// check variable volume unit
		Object volUnitVarObj = this.execution.getVariable(varName);
		if (volUnitVarObj != null) {
			String volUnitVarString = volUnitVarObj.toString();
			if (!StringUtils.isEmpty(volUnitVarString)) {
				try {
					thresholdUnit = this.unitSrv.findByCode(volUnitVarString);
				} catch (Exception e) {
					LOGGER.error("ERROR:could not find a volume unit for CODE " + volUnitVarString, e);
					this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
							String.format(CmmErrorCode.VOLUME_UNIT_BY_CODE_NOT_EXISTING.getErrMsg(), volUnitVarString), e);
				}
			} else {
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						CmmErrorCode.VOLUME_UNIT_VOLUME_EMPTY.getErrMsg());
			}
		} else {
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE, CmmErrorCode.VOLUME_UNIT_NOT_FOUND.getErrMsg());
		}

		return thresholdUnit;
	}

	/**
	 * Extracts the volume variable from workflow
	 *
	 * @param varName the name of the workflow variable
	 * @returns the <code>Integer</code> volume if found, null otherwise
	 * @throws Exception
	 */
	protected Integer getVolumeVariableFromWorkflow(String varName) throws Exception {
		Integer thresholdVolume = null;
		// check variable volume
		Object volVarObj = this.execution.getVariable(varName);
		if (volVarObj != null) {
			String volVarString = volVarObj.toString();
			if (!StringUtils.isEmpty(volVarString)) {

				try {
					thresholdVolume = Integer.parseInt(volVarString);
				} catch (NumberFormatException e) {
					LOGGER.error("ERROR:could not transform the volume " + volVarString, e);
					this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
							String.format(CmmErrorCode.VOLUME_THRESHOLD_TRANSFORM_ERROR.getErrMsg(), volVarString), e);
				}
			} else {
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE, CmmErrorCode.VOLUME_THRESHOLD_EMPTY.getErrMsg());
			}
		} else {
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE, CmmErrorCode.VOLUME_THRESHOLD_NOT_FOUND.getErrMsg());
		}
		return thresholdVolume;
	}

}
