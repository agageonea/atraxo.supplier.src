package atraxo.cmm.business.ext.tender.service.updater.committer;

import java.util.LinkedList;
import java.util.List;

import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.cmm.domain.impl.tender.TenderLocationExpectedPrice;
import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import atraxo.fmbas.domain.ext.history.HistoryValue;
import seava.j4e.commons.utils.listener.changes.ObjectAdded;
import seava.j4e.commons.utils.listener.changes.ObjectRemoved;
import seava.j4e.commons.utils.listener.changes.ValueChanged;

/**
 * Creates the JSON message which will be logged in Tender/TenderLocation history
 *
 * @author abolindu
 */
public class TenderMessageBuilderService {

	public HistoryValue buildHistoryMessageValueAdded(ObjectAdded objectAdded) {
		return this.buildPropertyMessage(objectAdded.getAddedEntity(), objectAdded.getMessageCode());
	}

	public HistoryValue buildHistoryMessageValueRemoved(ObjectRemoved objectRemoved) {
		return this.buildPropertyMessage(objectRemoved.getRemovedEntity(), objectRemoved.getMessageCode());
	}

	public HistoryValue buildHistoryMessageValueChanged(ValueChanged valueChanged, Object entityIdentifier) {
		return this.buildPropertyChangedMessage(valueChanged, entityIdentifier);
	}

	private HistoryValue buildPropertyMessage(Object entity, String messageCode) {
		HistoryValue historyValue = new HistoryValue();
		historyValue.setCode(messageCode);

		List<Object> arguments = new LinkedList<>();
		if (entity instanceof TenderLocation) {

			TenderLocation location = (TenderLocation) entity;
			arguments.add(location.getLocation().getCode());
			arguments.add(location.getFuelProduct().getName());
			arguments.add(location.getTaxType().getName());
			arguments.add(location.getFlightServiceType().getName());
			arguments.add(location.getDeliveryPoint().getName());

		} else if (entity instanceof TenderLocationAirlines) {

			TenderLocationAirlines airline = (TenderLocationAirlines) entity;
			arguments.add(airline.getAirline().getCode());

		} else if (entity instanceof TenderLocationRound) {

			TenderLocationRound round = (TenderLocationRound) entity;
			arguments.add(round.getRoundNo());

		} else if (entity instanceof TenderLocationExpectedPrice) {

			TenderLocationExpectedPrice expectedPrice = (TenderLocationExpectedPrice) entity;
			arguments.add(expectedPrice.getIataPriceCategory().getCode());
		}

		historyValue.setArguments(arguments);
		return historyValue;
	}

	private HistoryValue buildPropertyChangedMessage(ValueChanged valueChanged, Object entityIdentifier) {
		HistoryValue historyValue = new HistoryValue();
		historyValue.setCode(valueChanged.getMessageCode());

		List<Object> arguments = new LinkedList<>();
		arguments.add(valueChanged.getFieldName());
		arguments.add(valueChanged.getOldValue());
		arguments.add(valueChanged.getNewValue());

		if (entityIdentifier != null) {
			arguments.add(entityIdentifier);
		}

		historyValue.setArguments(arguments);
		return historyValue;
	}
}
