/**
 *
 */
package atraxo.cmm.business.ext.bpm.delegate.bid.approval;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate;

/**
 * @author vhojda
 */
public class RejectBidDelegate extends AbstractJavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(RejectBidDelegate.class);

	@Autowired
	private IContractService contractSrv;

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.bpm.delegate.AbstractJavaDelegate#execute()
	 */
	@Override
	public void execute() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}

		// update the BID and the history
		Object bidIdString = this.execution.getVariable(WorkflowVariablesConstants.BID_ID_VAR);
		if (bidIdString != null) {
			Integer bidId = Integer.parseInt(bidIdString.toString());
			Contract bid = null;
			try {
				bid = this.contractSrv.findByBusiness(bidId);
			} catch (Exception e) {
				LOGGER.error("ERROR:could not find a bid for ID " + bidId, e);
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						String.format(CmmErrorCode.BID_APPROVAL_WORKFLOW_ENTITY_NOT_EXISTING.getErrMsg(), bidId));
			}

			if (bid != null) {
				this.contractSrv.reject(bid, BidApprovalStatus._REJECTED_.getName(), this.getNote());
			}

		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#getSessionUserCode()
	 */
	@Override
	public String getSessionUserCode() {
		return this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME);
	}

}
