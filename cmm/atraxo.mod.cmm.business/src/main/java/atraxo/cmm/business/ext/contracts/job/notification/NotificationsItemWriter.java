package atraxo.cmm.business.ext.contracts.job.notification;

import java.util.List;

import org.springframework.batch.item.ItemWriter;

public class NotificationsItemWriter implements ItemWriter<Boolean>{

	@Override
	public void write(List<? extends Boolean> items) throws Exception {
		// this job does not need a writer
	}
}
