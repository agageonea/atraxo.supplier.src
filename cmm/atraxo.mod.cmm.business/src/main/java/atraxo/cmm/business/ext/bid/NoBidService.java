/**
 *
 */
package atraxo.cmm.business.ext.bid;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.ext.datahub.IDataHubClientRequestService;
import atraxo.cmm.business.api.tender.ITenderLocationService;
import atraxo.cmm.business.ext.contracts.service.AbstractDataHubClientRequestService;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderNoBid;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderResponse;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderServiceService;

/**
 * @author zspeter
 */
public class NoBidService extends AbstractDataHubClientRequestService<TenderLocation>
		implements IDataHubClientRequestService<TenderLocation, FuelTenderNoBid> {

	@Autowired
	private ITenderLocationService service;

	/**
	 * Submits the request to DataHub and receives a response
	 *
	 * @param client
	 * @param dto
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public FuelTenderResponse submitRequest(FuelTenderServiceService client, FuelTenderNoBid dto) throws BusinessException {
		return client.getFuelTenderServicePort().fuelTenderNoBid(dto);
	}

	/**
	 * Process and finalize the response from DataHub. Make the necessary adjustments to the entity.
	 *
	 * @param result
	 * @param list
	 * @param dtoClass
	 * @param reason
	 * @throws BusinessException
	 */
	@Override
	public void processDataHubBulkWsResult(WSExecutionResult result, Collection<TenderLocation> list, Class<FuelTenderNoBid> dtoClass, String reason)
			throws BusinessException {
		if (list != null) {
			List<Object> ids = list.stream().map(TenderLocation::getId).collect(Collectors.toList());
			List<TenderLocation> tenderLocations = this.service.findByIds(ids);

			TransmissionStatus transmissionStatus = result.isSuccess() ? TransmissionStatus._TRANSMITTED_ : TransmissionStatus._FAILED_;

			for (TenderLocation tenderLocation : tenderLocations) {
				// Set transmission status to location
				tenderLocation.setTransmissionStatus(transmissionStatus);

				// Set biddingStatus to No Bid if datahub response ok (Decline Bidding)
				if (FuelTenderNoBid.class.equals(dtoClass) && result.isSuccess()) {
					tenderLocation.setLocationBiddingStatus(BiddingStatus._NO_BID_);
					this.generateSaveToHistory(tenderLocation, tenderLocation.getLocationBiddingStatus().getName(), reason, TenderLocation.class);
				}
			}
			// Update locations
			this.service.update(tenderLocations);
		}
	}

	/**
	 * Process and finalize the response from DataHub. Make the necessary adjustments to the entity.
	 *
	 * @param result
	 * @param dtoClass
	 * @param reason
	 * @throws BusinessException
	 */
	@Override
	public void processDataHubSingleWsResult(WSExecutionResult result, TenderLocation entity, Class<FuelTenderNoBid> dtoClass, String reason)
			throws BusinessException {
		// to be implemented when single export will be supported
	}

}
