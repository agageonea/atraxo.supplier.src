package atraxo.cmm.business.ext.bpm.delegate.bid.approval;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.custom.workflow.IWorkflowTerminatorService;
import atraxo.fmbas.business.ext.bpm.WorkflowMsgConstants;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

public class BidApprovalWorkflowTerminator extends AbstractBusinessBaseService implements IWorkflowTerminatorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BidApprovalWorkflowTerminator.class);

	@Autowired
	private IContractService contractSrv;

	@Override
	public void terminateWorkflow(WorkflowInstance workflowInstance, String reason) throws BusinessException {

		List<WorkflowInstanceEntity> entities = new ArrayList<>(workflowInstance.getEntitites());

		for (WorkflowInstanceEntity entity : entities) {
			Integer idEntity = entity.getObjectId();

			// set the bid approval status to rejected
			Contract bidContract = null;
			try {
				bidContract = this.contractSrv.findByBusiness(idEntity);
			} catch (Exception e) {
				LOGGER.warn("WARNING: could not retrieve a contract for ID " + idEntity + " ! Will not update the entity since it doesn't exist !",
						e);
			}
			if (bidContract != null) {
				this.contractSrv.reject(bidContract, WorkflowMsgConstants.WKF_RESULT_REJECTED,
						StringUtils.isEmpty(reason) ? WorkflowMsgConstants.APPROVAL_WORKFLOW_TERMINATED_COMMENT : reason);
			}
		}
	}
}
