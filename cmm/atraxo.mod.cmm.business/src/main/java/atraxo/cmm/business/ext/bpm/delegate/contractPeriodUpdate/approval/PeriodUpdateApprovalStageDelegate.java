package atraxo.cmm.business.ext.bpm.delegate.contractPeriodUpdate.approval;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.Expression;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractChangeService;
import atraxo.cmm.business.ext.bpm.delegate.ContractDelegate;
import atraxo.cmm.business.ext.contracts.service.ContractChangeUtil;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.ext.mailmerge.dto.ContractPeriodUpdateApprovalDto;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.ContractChange;
import atraxo.fmbas.business.api.workflow.IWorkflowParameterService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailAddressesDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.impl.fmbas_type.ContractChangeType;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Top <code>ContractDelegate</code> class with common behaviour for all the delegates that participate in the Contract Approval Workflow
 *
 * @author mbotorogea
 */
public class PeriodUpdateApprovalStageDelegate extends ContractDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(PeriodUpdateApprovalStageDelegate.class);

	@Autowired
	private IWorkflowParameterService workflowParameterService;

	@Autowired
	private IContractChangeService contractChangeService;

	private Expression role;

	private Expression step;

	@Override
	public void execute() throws Exception {
		Contract contract = this.getContract();

		if (contract != null) {
			this.setNextApprovalStageRequiredVariable(true);

			WorkflowParameter param = this.workflowParameterService.findByKey(Long.valueOf(this.workflowId), this.getStageApprovalRolesVariable());
			List<UserSupp> users = this.extractApprovers(this.getStageApprovalRolesVariable(), param.getMandatory());

			if (!CollectionUtils.isEmpty(users)) {

				EmailDto emailDto = this.extractEmailDto(contract, users);
				if (emailDto != null) {
					this.execution.setVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_DTO, emailDto);
				}
			} else {
				this.setNextApprovalStageRequiredVariable(false);
			}
			this.resetMailComponentVariables();
		}
	}

	/**
	 * @param contract
	 * @param users
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	protected EmailDto extractEmailDto(Contract contract, List<UserSupp> users) throws BusinessException {
		EmailDto emailDTO = new EmailDto();

		// extract data from contract
		String contractCode = "";
		String contractHolder = "";
		String customerName = "";
		String contractLocation = "";

		try {
			contractCode = ContractChangeUtil.checkContractCode(contract.getCode());
			contractHolder = contract.getHolder().getName();
			contractLocation = contract.getLocation().getName();
			customerName = contract.getCustomer().getName();
		} catch (Exception e) {
			LOGGER.error("ERROR:could not extract data from the contract " + contract.getId() + " in order to send the mail ! ", e);
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
					String.format(CmmErrorCode.CONTRACT_WORKFLOW_EMAIL_EXTRACT_DATA_ERROR.getErrMsg(), contract.getId()));
			emailDTO = null;
		}

		if (emailDTO != null) {
			// set the subject
			emailDTO.setSubject(
					String.format(CmmErrorCode.WKF_CONTRACT_CHANGE_PERIOD_SUBJECT.getErrMsg(), contractCode, contractLocation, customerName));

			// set attachments
			Object emailAttachments = this.execution.getVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_ATTACHMENTS);
			if (emailAttachments != null && emailAttachments instanceof List) {
				emailDTO.setAttachments((List<String>) emailAttachments);
			}

			// common email data for all receivers taken from workflow
			String fullNameOfRequester = this.getVariableFromWorkflow(WorkflowVariablesConstants.FULLNAME_REQUESTER_VAR);
			String approvalNote = this.getVariableFromWorkflow(WorkflowVariablesConstants.APPROVE_NOTE);

			// extract the change
			List<ContractChange> contractPeriodChanges = this.contractChangeService.getContractChanges(contract, ContractChangeType._PERIOD_);
			String change = contractPeriodChanges.get(contractPeriodChanges.size() - 1).getMessage();

			List<EmailAddressesDto> emailAddressesDtoList = new ArrayList<>();
			for (UserSupp user : users) {

				ContractPeriodUpdateApprovalDto dataDto = new ContractPeriodUpdateApprovalDto();
				dataDto.setFullnameOfRequester(fullNameOfRequester);
				dataDto.setTitle(user.getTitle().getName());
				dataDto.setFullName(user.getFirstName() + " " + user.getLastName());

				dataDto.setContractCode(contractCode);
				dataDto.setContractHolder(contractHolder);
				dataDto.setContractLocation(contractLocation);

				dataDto.setCustomerName(customerName);

				dataDto.setChanges(change);

				dataDto.setApprovalRequestNote(approvalNote);

				// set customer price update email data for each approval step
				this.updateContractEmailDto(dataDto);

				EmailAddressesDto emailAddressesDto = new EmailAddressesDto();
				emailAddressesDto.setAddress(user.getEmail());
				emailAddressesDto.setData(dataDto);
				emailAddressesDtoList.add(emailAddressesDto);
			}

			emailDTO.setTo(emailAddressesDtoList);

		}

		return emailDTO;
	}

	/**
	 * Updates the email <code>ContractPeriodUpdateApprovalDto</code> with information taken from workflow variables
	 *
	 * @param dataDto
	 */
	private void updateContractEmailDto(ContractPeriodUpdateApprovalDto dataDto) {
		String approvalNote = this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_VAR);
		String approvalUser = this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR);

		int stepVar = this.getStageApprovalStepVariable();
		if (stepVar == 2) {
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL1, approvalNote);
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL1, approvalUser);
		}

		if (stepVar == 3) {
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL2, approvalNote);
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL2, approvalUser);
		}

		// set the approvers (all of approvers need to be set)
		dataDto.setApprovalNoteFirstLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL1));
		dataDto.setApproverNameFirstLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL1));

		dataDto.setApprovalNoteSecondLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL2));
		dataDto.setApproverNameSecondLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL2));
	}

	@Override
	public String getSessionUserCode() {
		return null;
	}

	/**
	 * @return
	 */
	private String getStageApprovalRolesVariable() {
		return (String) this.role.getValue(this.execution);
	}

	/**
	 * @return
	 */
	private int getStageApprovalStepVariable() {
		String stepVar = (String) this.step.getValue(this.execution);
		return Integer.parseInt(stepVar);
	}

	/**
	 * @param required
	 */
	protected void setNextApprovalStageRequiredVariable(boolean required) {
		this.execution.setVariable(WorkflowVariablesConstants.NEXT_APPROVAL_STAGE_REQUIRED, required);
	}

}
