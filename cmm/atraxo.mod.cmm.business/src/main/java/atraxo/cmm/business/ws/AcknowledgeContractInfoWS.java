package atraxo.cmm.business.ws;

import java.io.IOException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
@WebService(targetNamespace = "http://ws.business.cmm.atraxo/", endpointInterface = "AcknowledgeContractInfo")
public interface AcknowledgeContractInfoWS {

	/**
	 * @param request
	 * @return
	 * @throws JAXBException
	 * @throws SAXException
	 * @throws IOException
	 * @throws BusinessException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	@WebMethod(operationName = "acknowledgeContractsEBits", action = "acknowledgeContractsEBits")
	@WebResult(name = "MSG", targetNamespace = "http://Pumaenergy.Common.eAviation.Schemas.Message_RequestResponse")
	MSG acknowledgeContractsEBits(
			@WebParam(name = "MSG", targetNamespace = "http://Pumaenergy.Common.eAviation.Schemas.Message_RequestResponse") MSG request)
			throws JAXBException, SAXException, IOException, BusinessException, InstantiationException, IllegalAccessException;

}
