package atraxo.cmm.business.ext.exceptions;

import java.util.Date;

import seava.j4e.api.enums.DateFormatAttribute;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

public class FuelPriceNotExistsException extends BusinessException {

	private static final long serialVersionUID = 1983910999475761166L;

	public FuelPriceNotExistsException(String priceCategoryType, String contractCode, String priceCategoryName, Date date) {
		super(CmmErrorCode.FUEL_PRICE_NOT_EXISTS, String.format(CmmErrorCode.FUEL_PRICE_NOT_EXISTS.getErrMsg(), priceCategoryType, contractCode,
				priceCategoryName, Session.user.get().getSettings().getDateFormat(DateFormatAttribute.JAVA_DATE_FORMAT).format(date)));
	}

}
