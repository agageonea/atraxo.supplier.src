package atraxo.cmm.business.ext.contracts.job.notification;

import java.util.List;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;

import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import seava.j4e.scheduler.batch.listener.DefaultActionListener;

public class NotificationsListener extends DefaultActionListener {

	@SuppressWarnings("unchecked")
	@Override
	public void afterJob(JobExecution jobExecution) {
		super.afterJob(jobExecution);
		ExitStatus status = jobExecution.getExitStatus();
		StringBuilder sb = new StringBuilder();
		List<StepExecution> stepExecution = (List<StepExecution>) jobExecution.getStepExecutions();

		for (StepExecution se : stepExecution) {
			ExecutionContext executionContext = se.getExecutionContext();
			if (executionContext.containsKey(NotificationsItemProcessor.NOTIFIED)) {
				List<String> customers = (List<String>) executionContext.get(NotificationsItemProcessor.NOTIFIED);
				sb.append(BusinessErrorCode.PRICE_UPDATE_NOTIFICATION_JOB_SENT.getErrMsg()).append("\n");
				for (String customer : customers) {
					sb.append(" - ").append(customer).append("; \n");
				}
			}
			if (executionContext.containsKey(NotificationsItemProcessor.NO_TEMPLATE)) {
				List<String> customers = (List<String>) executionContext.get(NotificationsItemProcessor.NO_TEMPLATE);
				sb.append(BusinessErrorCode.PRICE_UPDATE_NOTIFICATION_JOB_NO_TEMPLATE.getErrMsg()).append("\n");
				for (String customer : customers) {
					sb.append(" - ").append(customer).append("; \n");
				}
			}
			if (executionContext.containsKey(NotificationsItemProcessor.NO_CONTACTS)) {
				List<String> customers = (List<String>) executionContext.get(NotificationsItemProcessor.NO_CONTACTS);
				sb.append(BusinessErrorCode.PRICE_UPDATE_NOTIFICATION_JOB_NO_CONTACTS.getErrMsg()).append("\n");
				for (String customer : customers) {
					sb.append(" - ").append(customer).append("; \n");
				}
			}
			if (sb.toString().isEmpty()) {
				sb.append(CmmErrorCode.NO_EMAIL_SENT.getErrMsg()).append("\n");
			}
		}
		status = new ExitStatus(status.getExitCode(), sb.toString());
		jobExecution.setExitStatus(status);
	}
}
