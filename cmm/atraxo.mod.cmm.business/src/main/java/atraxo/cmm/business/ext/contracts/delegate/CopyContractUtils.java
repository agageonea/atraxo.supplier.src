package atraxo.cmm.business.ext.contracts.delegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.fmbas.business.ext.util.EntityCloner;

/**
 * @author zspeter
 */
public class CopyContractUtils {

	private CopyContractUtils() {
	}

	/**
	 * @param pb
	 * @param newPb
	 * @param map
	 * @return
	 */
	public static Collection<ContractPriceCategory> getPriceCategory(PricingBase pb, PricingBase newPb, Map<Integer, ContractPriceCategory> map,
			boolean isInternalResale) {
		Collection<ContractPriceCategory> retList = new ArrayList<>();
		for (ContractPriceCategory cpc : pb.getContractPriceCategories()) {
			ContractPriceCategory newCpc = EntityCloner.cloneEntity(cpc);
			if (isInternalResale) {
				newCpc.setResaleRef(cpc.getId());
			}
			newCpc.setPricingBases(newPb);
			newCpc.setContract(newPb.getContract());
			newCpc.setPriceRestrictions(getPriceRestrictions(cpc, newCpc));
			newCpc.setPriceComponents(getPriceComponent(cpc, newCpc));

			copyManyToManyRelation(map, cpc, newCpc);
			newCpc.setOrderIndex(cpc.getOrderIndex());
			retList.add(newCpc);
		}
		return retList;
	}

	/**
	 * @param map
	 * @param cpc
	 * @param newCpc
	 */
	public static void copyManyToManyRelation(Map<Integer, ContractPriceCategory> map, ContractPriceCategory cpc, ContractPriceCategory newCpc) {
		newCpc.setChildPriceCategory(new ArrayList<ContractPriceCategory>());
		map.put(cpc.getId(), newCpc);
		if (cpc.getParentPriceCategory() != null && !cpc.getParentPriceCategory().isEmpty()) {
			newCpc.setParentPriceCategory(new ArrayList<ContractPriceCategory>());
			for (ContractPriceCategory cpcParent : cpc.getParentPriceCategory()) {
				if (map.containsKey(cpcParent.getId())) {
					newCpc.getParentPriceCategory().add(map.get(cpcParent.getId()));
					// map.get(cpcParent.getId()).getChildPriceCategory().add(newCpc);
				}
			}
		}
	}

	private static Collection<ContractPriceRestriction> getPriceRestrictions(ContractPriceCategory cpc, ContractPriceCategory newCpc) {
		Collection<ContractPriceRestriction> retList = new ArrayList<>();
		for (ContractPriceRestriction cpr : cpc.getPriceRestrictions()) {
			ContractPriceRestriction newCpr = EntityCloner.cloneEntity(cpr);
			newCpr.setContractPriceCategory(newCpc);
			retList.add(newCpr);
		}
		return retList;
	}

	private static Collection<ContractPriceComponent> getPriceComponent(ContractPriceCategory cpc, ContractPriceCategory newCpc) {
		Collection<ContractPriceComponent> retList = new ArrayList<>();
		for (ContractPriceComponent cpr : cpc.getPriceComponents()) {
			ContractPriceComponent newE = EntityCloner.cloneEntity(cpr);
			newE.setContrPriceCtgry(newCpc);
			newE.setConvertedPrices(getPriceComponentConverted(cpr, newE));
			retList.add(newE);
		}
		return retList;
	}

	public static Collection<ContractPriceComponentConv> getPriceComponentConverted(ContractPriceComponent e, ContractPriceComponent newCpc) {
		Collection<ContractPriceComponentConv> retList = new ArrayList<>();
		for (ContractPriceComponentConv cpr : e.getConvertedPrices()) {
			ContractPriceComponentConv newE = EntityCloner.cloneEntity(cpr);
			newE.setContractPriceComponent(newCpc);
			retList.add(newE);
		}
		return retList;
	}

}
