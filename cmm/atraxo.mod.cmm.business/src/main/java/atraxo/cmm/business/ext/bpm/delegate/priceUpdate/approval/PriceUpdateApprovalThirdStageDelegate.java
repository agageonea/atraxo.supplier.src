/**
 *
 */
package atraxo.cmm.business.ext.bpm.delegate.priceUpdate.approval;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.domain.ext.mailmerge.dto.PriceUpdateDto;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;

/**
 * @author vhojda
 */
public class PriceUpdateApprovalThirdStageDelegate extends PriceUpdateApprovalAbstractStageDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(PriceUpdateApprovalThirdStageDelegate.class);

	/*
	 * (non-Javadoc)
	 * @see
	 * atraxo.cmm.business.ext.bpm.delegate.price.update.PriceUpdateAbstractStageDelegate#updatePriceEmailDto(atraxo.cmm.domain.ext.mailmerge.dto.
	 * PriceUpdateDto)
	 */
	@Override
	public void updatePriceEmailDto(PriceUpdateDto priceUpdateDto) {
		priceUpdateDto.setApprovalNoteFirstLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL1));
		priceUpdateDto.setApproverNameFirstLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL1));

		priceUpdateDto.setApprovalNoteSecondLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_VAR));
		priceUpdateDto.setApproverNameSecondLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR));
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ext.bpm.delegate.price.update.PriceUpdateAbstractStageDelegate#getStageApprovalRoleVariable()
	 */
	@Override
	public String getStageApprovalRolesVariable() {
		return WorkflowVariablesConstants.APPROVER_ROLES_LVL3;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#getSessionUserCode()
	 */
	@Override
	public String getSessionUserCode() {
		return null;
	}

}
