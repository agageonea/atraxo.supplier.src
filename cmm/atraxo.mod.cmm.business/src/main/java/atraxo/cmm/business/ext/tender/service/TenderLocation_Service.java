/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.ext.tender.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.Years;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.ad.business.api.system.IDateFormatMaskService;
import atraxo.ad.business.api.system.IDateFormatService;
import atraxo.ad.domain.impl.system.DateFormat;
import atraxo.ad.domain.impl.system.DateFormatMask;
import atraxo.cmm.business.api.tender.ITenderLocationAirlinesService;
import atraxo.cmm.business.api.tender.ITenderLocationService;
import atraxo.cmm.business.api.tender.ITenderService;
import atraxo.cmm.business.ext.tender.delegate.TenderFormDelegate;
import atraxo.cmm.domain.ext.mailmerge.dto.TenderFormHeaderDTO;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.Period;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.templates.ITemplateService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.aop.history.Action;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.mailMerge.MailMergeManager;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.templates.Template;
import seava.j4e.api.annotation.Reason;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link TenderLocation} domain entity.
 */
public class TenderLocation_Service extends atraxo.cmm.business.impl.tender.TenderLocation_Service implements ITenderLocationService {

	@Autowired
	private ITenderLocationAirlinesService tenderLocationAirlinesService;
	@Autowired
	private IUnitService unitService;
	@Autowired
	private ISystemParameterService systemParamsService;
	@Autowired
	private ITenderService tenderService;
	@Autowired
	private ITemplateService templateService;
	@Autowired
	private IDateFormatService dtfmtService;
	@Autowired
	private IDateFormatMaskService dtfmtMaskService;
	@Autowired
	private ICustomerService customerService;

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);

		List<TenderLocation> tenderLocs = this.findByIds(ids);
		List<TenderLocationAirlines> tenderLocAirlines = new ArrayList<>();

		for (TenderLocation tdLoc : tenderLocs) {
			tenderLocAirlines.addAll(tdLoc.getTenderLocationAirlines());
		}
		List<Object> tenderLocAirlineIds = this.collectIds(tenderLocAirlines);

		if (!CollectionUtils.isEmpty(tenderLocAirlineIds)) {
			this.tenderLocationAirlinesService.deleteByIds(tenderLocAirlineIds);
		}
	}

	@Override
	protected void preUpdate(TenderLocation e) throws BusinessException {
		super.preUpdate(e);

		// iata standard not support period for location with airlines (put contract period as default for calculation reasons)
		if (!e.getTenderLocationAirlines().isEmpty()) {
			BigDecimal calculatedVolume = BigDecimal.ZERO;
			for (TenderLocationAirlines airline : e.getTenderLocationAirlines()) {
				if (airline.getIsValid()) {
					calculatedVolume = calculatedVolume.add(this.calculateVolumePerAgreementPeriod(e, airline));
				}
			}

			e.setHasTotalVolume(false);
			e.setPeriodType(Period._CONTRACT_PERIOD_);
			e.setVolume(calculatedVolume);
		} else {
			e.setHasTotalVolume(true);
		}

	}

	@Override
	public void updateVolume(Integer locationId, Boolean resetVolume) throws BusinessException {
		TenderLocation location = this.findByBusiness(locationId);
		if (resetVolume) {
			location.setVolume(BigDecimal.ZERO);
		}
		this.update(this.findByBusiness(locationId));
	}

	@Override
	@Transactional
	@History(type = TenderLocation.class)
	public void updateBiddingStatus(TenderLocation location, BiddingStatus biddingStatus, @Action String action, @Reason String reason)
			throws BusinessException {

		location.setLocationBiddingStatus(biddingStatus);

		// Update Tender Bidding Status based on locations bidding status
		BiddingStatus tenderBiddingStatus = this.tenderService.calculateBiddingStatus(location.getTender());
		if (!location.getTender().getBiddingStatus().equals(tenderBiddingStatus)) {
			this.tenderService.updateBiddingStatus(location.getTender(), tenderBiddingStatus, tenderBiddingStatus.getName());
		} else {
			this.update(location);
		}
	}

	@Override
	public BiddingStatus calculateBiddingStatus(TenderLocation location) throws BusinessException {
		List<TenderLocationAirlines> airlines = new ArrayList<>();
		airlines.addAll(location.getTenderLocationAirlines());

		// If at leat one airline is In Negotiation then the bidding is In Negotiation
		TenderLocationAirlines inProgress = airlines.stream().filter(airline -> BiddingStatus._IN_NEGOTIATION_.equals(airline.getBiddingStatus()))
				.findAny().orElse(null);

		if (inProgress != null) {
			return BiddingStatus._IN_NEGOTIATION_;
		}

		// if all the locations are Finished then the bidding status is Finished
		List<TenderLocationAirlines> finished = airlines.stream().filter(airline -> BiddingStatus._FINISHED_.equals(airline.getBiddingStatus()))
				.collect(Collectors.toList());

		if (finished.size() == location.getTenderLocationAirlines().size()) {
			return BiddingStatus._FINISHED_;
		}

		return location.getLocationBiddingStatus();
	}

	// Util
	// =======================================================================================================

	@Override
	public BigDecimal calculateVolumePerAgreementPeriod(TenderLocation location, TenderLocationAirlines airline) throws BusinessException {
		// Verify that all the values needed for the calculations are provided
		if (location.getVolumeUnit() == null || airline.getUnit() == null || airline.getVolume() == null
				|| Period._EMPTY_.equals(airline.getVolumePeriod())) {
			return BigDecimal.ZERO;
		}

		BigDecimal contractVolume = BigDecimal.ZERO;

		// calculate volume based on period type and contract period
		DateTime sDate = new DateTime(location.getAgreementFrom());
		DateTime eDate = new DateTime(location.getAgreementTo());

		Months months = Months.monthsBetween(sDate, eDate).plus(Months.ONE);
		Years years = Years.yearsBetween(sDate, eDate);

		BigDecimal factor = BigDecimal.ONE;
		switch (airline.getVolumePeriod()) {
		case _MONTHLY_:
			factor = new BigDecimal(months.getMonths());
			break;
		case _YEARLY_:
			factor = new BigDecimal((years.getYears() > 0) ? years.getYears() : 1);
			break;
		default:
			break;
		}

		Double density = Double.parseDouble(this.systemParamsService.getDensity());
		contractVolume = contractVolume.add(this.unitService.convert(airline.getUnit(), location.getVolumeUnit(), airline.getVolume(), density));
		contractVolume = contractVolume.multiply(factor, MathContext.DECIMAL64);

		return contractVolume;
	}

	@Override
	public byte[] generateBidForm(Integer locationId) throws BusinessException {
		TenderLocation tenderLocation = this.findById(locationId);
		Customer subsidiary = this.customerService.findByRefid(Session.user.get().getClient().getActiveSubsidiaryId());
		TenderFormHeaderDTO header = TenderFormDelegate.buildTenderFormDTO(tenderLocation, subsidiary);
		Map<String, Object> params = new HashMap<>();
		params.put("systemUse", true);
		List<Template> templates = this.templateService.findEntitiesByAttributes(params);
		String mesasge = this.generateExportData(header);
		if (!CollectionUtils.isEmpty(templates)) {
			Template template = null;
			for (Template t : templates) {
				if ("Bid capture template".equalsIgnoreCase(t.getType().getName())) {
					template = t;
				}
			}
			if (template != null) {
				return this.templateService.getPreview(template, mesasge);
			} else {
				throw new BusinessException(BusinessErrorCode.TEMPLATE_NOT_FOUND, BusinessErrorCode.TEMPLATE_NOT_FOUND.getErrMsg());
			}
		} else {
			throw new BusinessException(BusinessErrorCode.TEMPLATE_NOT_FOUND, BusinessErrorCode.TEMPLATE_NOT_FOUND.getErrMsg());
		}
	}

	private String generateExportData(TenderFormHeaderDTO header) throws BusinessException {
		DateFormat df = this.dtfmtService.findByName("dd-mm-yyyy");
		DateFormatMask dateFormatMask = this.dtfmtMaskService.findByName(df, "JAVA_DATE_FORMAT");
		return MailMergeManager.getDataJson(header, "XSLX", null, dateFormatMask.getValue());
	}

}
