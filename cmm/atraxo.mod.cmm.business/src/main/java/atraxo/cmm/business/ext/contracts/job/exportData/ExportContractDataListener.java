package atraxo.cmm.business.ext.contracts.job.exportData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;

import seava.j4e.scheduler.batch.listener.DefaultActionListener;

public class ExportContractDataListener extends DefaultActionListener {

	final static Logger logger = LoggerFactory.getLogger(ExportContractDataTasklet.class);

	public static final String KEY_NO_CONTRACTS = "no_contracts";
	public static final String KEY_INTERFACE_DISABLE = "export_job_details";

	@Override
	public void afterJob(JobExecution jobExecution) {
		super.afterJob(jobExecution);
		// remake the exist status
		ExitStatus currentExisingStatus = jobExecution.getExitStatus();
		StringBuilder sb = new StringBuilder();
		if (jobExecution.getExecutionContext().containsKey(KEY_NO_CONTRACTS)) {
			sb.append("No Contracts to export");
			jobExecution.setExitStatus(new ExitStatus(currentExisingStatus.getExitCode(), sb.toString()));
		} else if (jobExecution.getExecutionContext().containsKey(KEY_INTERFACE_DISABLE)) {
			sb.append("\"Export Contracts to eBits\" External Interface is disabled");
			currentExisingStatus = new ExitStatus(ExitStatus.FAILED.getExitCode(), sb.toString());
			jobExecution.setStatus(BatchStatus.FAILED);
			jobExecution.setExitStatus(currentExisingStatus);
		} else {
			sb.append("Check Execution History of the \"Export Contracts to eBits\" External Interface");
			jobExecution.setExitStatus(new ExitStatus(currentExisingStatus.getExitCode(), sb.toString()));
		}
	}
}
