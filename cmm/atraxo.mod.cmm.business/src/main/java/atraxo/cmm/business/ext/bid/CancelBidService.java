/**
 *
 */
package atraxo.cmm.business.ext.bid;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.ext.datahub.IDataHubClientRequestService;
import atraxo.cmm.business.ext.contracts.service.AbstractDataHubClientRequestService;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderCancelBid;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderResponse;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderServiceService;

/**
 * @author zspeter
 */
public class CancelBidService extends AbstractDataHubClientRequestService<Contract>
		implements IDataHubClientRequestService<Contract, FuelTenderCancelBid> {

	@Autowired
	private IContractService contractService;

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubClientRequestService#submitRequest(seava.j4e.iata.fuelplus.iata.tender.FuelTenderServiceService,
	 * java.lang.Object)
	 */
	@Override
	public FuelTenderResponse submitRequest(FuelTenderServiceService client, FuelTenderCancelBid dto) throws BusinessException {
		return client.getFuelTenderServicePort().fuelTenderCancelBid(dto);
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubClientRequestService#processDataHubWsResult(atraxo.fmbas.domain.ws.dto.WSExecutionResult,
	 * java.util.Collection, java.lang.Class, java.lang.String)
	 */
	@Override
	public void processDataHubBulkWsResult(WSExecutionResult result, Collection<Contract> list, Class<FuelTenderCancelBid> dtoClass, String reason)
			throws BusinessException {
		// to be implemented when bulk export will be supported
	}

	/**
	 * Process and finalize the response from DataHub. Make the necessary adjustments to the entity.
	 *
	 * @param result
	 * @param dtoClass
	 * @param reason
	 * @throws BusinessException
	 */
	@Override
	public void processDataHubSingleWsResult(WSExecutionResult result, Contract entity, Class<FuelTenderCancelBid> dtoClass, String reason)
			throws BusinessException {

		if (result != null) {
			Contract bid = this.contractService.findById(entity.getId());

			TransmissionStatus transmissionStatus = result.isSuccess() ? TransmissionStatus._TRANSMITTED_ : TransmissionStatus._FAILED_;
			bid.setBidCancelTransmitionStatus(transmissionStatus);

			if (result.isSuccess()) {
				bid.setBidStatus(BidStatus._CANCELLED_);
			}

			// Update bid status and transmission status
			this.contractService.updateWithoutBusinessLogic(Arrays.asList(bid));

			// Save publish action in history
			if (result.isSuccess()) {
				this.generateSaveToHistory(bid, bid.getBidStatus().getName(), reason, Contract.class);
			}
		}
	}

}
