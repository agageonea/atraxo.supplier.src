package atraxo.cmm.business.ext.mailMerge.delegate.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.domain.ext.mailmerge.dto.contract.AbstractContactDto;

/**
 * @author apetho
 */
public final class ContactDtoBuilder {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContactDtoBuilder.class);

	private ContactDtoBuilder() {

	}

	@SuppressWarnings("unchecked")
	public static final <T extends AbstractContactDto> T buildContact(String email, String title, String firstName, String lastName, Class<T> clazz) {
		try {
			AbstractContactDto dto = clazz.newInstance();
			dto.setEmail(email);
			dto.setFirstName(firstName);
			dto.setLastName(lastName);
			dto.setTitle(title);
			return (T) dto;
		} catch (InstantiationException | IllegalAccessException e) {
			LOGGER.warn("Contact initialization problem.", e);
		}
		return null;
	}

}
