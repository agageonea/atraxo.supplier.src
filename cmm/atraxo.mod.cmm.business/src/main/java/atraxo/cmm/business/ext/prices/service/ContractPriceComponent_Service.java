/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.ext.prices.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import atraxo.cmm.business.api.contracts.IPricingBaseService;
import atraxo.cmm.business.api.prices.IContractPriceComponentConvService;
import atraxo.cmm.business.api.prices.IContractPriceComponentService;
import atraxo.cmm.business.ext.contracts.delegate.PricingBase_Bd;
import atraxo.cmm.business.ext.prices.delegate.ContractPriceComponentConv_Bd;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link ContractPriceComponent} domain entity.
 */
public class ContractPriceComponent_Service extends atraxo.cmm.business.impl.prices.ContractPriceComponent_Service
		implements IContractPriceComponentService {

	@Autowired
	private IPricingBaseService pbService;

	private static final String CATEGORY_ID = "categoryId";

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void insert(List<ContractPriceComponent> list) throws BusinessException {
		super.insert(list);
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void update(List<ContractPriceComponent> list) throws BusinessException {
		super.update(list);
	}

	@Override
	public void insertWithoutPersist(ContractPriceComponent e) throws BusinessException {
		this.preInsert(e);
		this.postInsert(e);
	}

	@Override
	public void updateWithoutPersist(ContractPriceComponent e) throws BusinessException {
		this.preUpdate(e);
		this.postUpdate(e);
	}

	@Override
	protected void preInsert(ContractPriceComponent e) throws BusinessException {
		super.preInsert(e);
		this.extendsToMargins(e);
		if (!e.getContrPriceCtgry().getPriceCategory().getPricePer().equals(PriceInd._PERCENT_)) {
			e.setConvertedPrices(null);
			this.getBusinessDelegate(ContractPriceComponentConv_Bd.class).insertContractPriceComponentConv(e, false);
		}
	}

	@Override
	protected void preUpdate(ContractPriceComponent e) throws BusinessException {
		super.preUpdate(e);
		this.extendsToMargins(e);
		this.getBusinessDelegate(ContractPriceComponentConv_Bd.class).updateConvertedPriceComponent(e);

	}

	@Override
	protected void postUpdate(ContractPriceComponent e) throws BusinessException {
		super.postUpdate(e);
		if (!this.pbService.isIndex(e.getContrPriceCtgry().getPricingBases()) && e.getContrPriceCtgry().getContinous()) {
			List<ContractPriceComponent> list = new ArrayList<>();
			if (e.getContrPriceCtgry().getPriceComponents() != null) {
				list.addAll(e.getContrPriceCtgry().getPriceComponents());
			}
			list.sort((o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()));
			Date leftMargin = DateUtils.addDays(e.getValidFrom(), -1);
			Date rightMargin = DateUtils.addDays(e.getValidTo(), 1);
			ContractPriceComponent leftElement = null;
			List<Object> delList = new ArrayList<>();
			List<ContractPriceComponent> updList = new ArrayList<>();
			for (ContractPriceComponent c : list) {
				if (c.equals(e)) {
					continue;
				}
				if (c.getValidFrom().before(leftMargin) && c.getValidTo().compareTo(leftMargin) <= 0) {
					leftElement = c;
					continue;
				}
				if (c.getValidFrom().compareTo(leftMargin) <= 0 && c.getValidTo().after(leftMargin)) {
					leftElement = null;
					c.setValidTo(leftMargin);
					updList.add(c);
					continue;
				}
				if (c.getValidFrom().compareTo(e.getValidFrom()) >= 0 && c.getValidTo().compareTo(e.getValidTo()) <= 0) {
					// swallow pricing base
					delList.add(c.getId());
					continue;
				}
				if (c.getValidFrom().before(rightMargin) && c.getValidTo().compareTo(rightMargin) >= 0) {
					c.setValidFrom(rightMargin);
					updList.add(c);
					break;
				}
				if (c.getValidFrom().after(rightMargin)) {
					c.setValidFrom(rightMargin);
					updList.add(c);
					break;
				}
			}
			if (leftElement != null) {
				leftElement.setValidTo(leftMargin);
				updList.add(leftElement);
			}
			this.updateAsIs(updList);
			this.deleteByIds(delList);
		}
	}

	@Override
	protected void postInsert(ContractPriceComponent e) throws BusinessException {
		super.postInsert(e);
		if (!this.pbService.isIndex(e.getContrPriceCtgry().getPricingBases())) {
			this.updateOtherComponents(e);
		}
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		if (!CollectionUtils.isEmpty(ids)) {
			Integer id = (Integer) ids.get(0);
			ContractPriceComponent c = this.findById(id);
			Boolean continous = c.getContrPriceCtgry().getContinous();
			if (continous != null && continous) {
				context.put(CATEGORY_ID, c.getContrPriceCtgry().getId());
			}
			ContractPriceComponentConv_Bd bd = this.getBusinessDelegate(ContractPriceComponentConv_Bd.class);
			for (Object obj : ids) {
				ContractPriceComponent e = this.findById(obj);
				bd.deleteConvertedPriceComponent(e);
			}
		}
	}

	@Override
	protected void postDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.postDeleteByIds(ids, context);
		if (context.containsKey(CATEGORY_ID)) {
			Object categoryId = context.get(CATEGORY_ID);
			ContractPriceCategory category = this.findById(categoryId, ContractPriceCategory.class);
			List<ContractPriceComponent> list = new ArrayList<>(category.getPriceComponents());
			list.sort((o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()));
			List<ContractPriceComponent> updList = new ArrayList<>();
			Date leftMargin = category.getPricingBases().getValidFrom();
			ContractPriceComponent c = null;
			for (ContractPriceComponent e : list) {
				if (c == null) {
					c = e;
					if (c.getValidFrom().after(leftMargin)) {
						c.setValidFrom(leftMargin);
						updList.add(c);
					}
					continue;
				}
				leftMargin = DateUtils.addDays(e.getValidFrom(), -1);
				if (c.getValidTo().before(leftMargin)) {
					c.setValidTo(leftMargin);
					updList.add(c);
				}
				c = e;
			}
			if (c != null && c.getValidTo().before(category.getPricingBases().getValidTo())) {
				c.setValidTo(category.getPricingBases().getValidTo());
				updList.add(c);
			}
			this.updateAsIs(updList);
		}
	}

	private void extendsToMargins(ContractPriceComponent e) {
		if (e.getContrPriceCtgry().getContinous()) {
			List<ContractPriceComponent> list = new ArrayList<>();
			if (e.getContrPriceCtgry().getPriceComponents() != null) {
				list.addAll(e.getContrPriceCtgry().getPriceComponents());
			}
			list.remove(e);
			if (!list.isEmpty()) {
				list.sort((o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()));
				ContractPriceComponent first = list.get(0);
				if (first.getValidFrom().after(e.getValidFrom()) && e.getValidFrom().after(e.getContrPriceCtgry().getContract().getValidFrom())) {
					e.setValidFrom(e.getContrPriceCtgry().getPricingBases().getValidFrom());
				}
				ContractPriceComponent last = list.get(list.size() - 1);
				if (last.getValidTo().before(e.getValidTo()) && e.getValidTo().before(e.getContrPriceCtgry().getContract().getValidTo())) {
					e.setValidTo(e.getContrPriceCtgry().getPricingBases().getValidTo());
				}
			} else {
				e.setValidFrom(e.getContrPriceCtgry().getPricingBases().getValidFrom());
				e.setValidTo(e.getContrPriceCtgry().getPricingBases().getValidTo());
			}
		}
	}

	private void updateOtherComponents(ContractPriceComponent e) throws BusinessException {
		List<ContractPriceComponent> list = new ArrayList<>();
		if (e.getContrPriceCtgry().getPriceComponents() != null) {
			list.addAll(e.getContrPriceCtgry().getPriceComponents());
		}
		list.sort((o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()));
		List<Object> delList = new ArrayList<>();
		List<ContractPriceComponent> updList = new ArrayList<>();
		List<ContractPriceComponent> insList = new ArrayList<>();
		for (ContractPriceComponent c : list) {
			if (c.equals(e)) {
				continue;
			}
			if (c.getValidFrom().compareTo(e.getValidFrom()) < 0 && c.getValidTo().compareTo(e.getValidFrom()) >= 0
					&& c.getValidTo().compareTo(e.getValidTo()) <= 0) {
				c.setValidTo(DateUtils.addDays(e.getValidFrom(), -1));
				updList.add(c);
			} else if (c.getValidFrom().compareTo(e.getValidFrom()) >= 0 && c.getValidFrom().compareTo(e.getValidTo()) <= 0
					&& c.getValidTo().compareTo(e.getValidTo()) > 0) {
				c.setValidFrom(DateUtils.addDays(e.getValidTo(), 1));
				updList.add(c);
			} else if (c.getValidFrom().compareTo(e.getValidFrom()) >= 0 && c.getValidTo().compareTo(e.getValidTo()) <= 0) {
				delList.add(c.getId());
			} else if (c.getValidFrom().compareTo(e.getValidFrom()) < 0 && c.getValidTo().compareTo(e.getValidTo()) > 0) {
				ContractPriceComponent generatedComp = new ContractPriceComponent();
				BeanUtils.copyProperties(c, generatedComp);
				generatedComp.setId(null);
				generatedComp.setValidFrom(DateUtils.addDays(e.getValidTo(), 1));
				c.setValidTo(DateUtils.addDays(e.getValidFrom(), -1));
				updList.add(c);
				insList.add(generatedComp);
			}
		}
		this.updateAsIs(updList);
		this.deleteConvertedComponents(delList);
		this.onDeleteByIds(delList, null);
		this.insert(insList);
	}

	/**
	 * @param ids A list of pricing base ids.
	 * @throws BusinessException
	 */
	private void deleteConvertedComponents(List<Object> ids) throws BusinessException {
		IContractPriceComponentConvService srv = (IContractPriceComponentConvService) this.findEntityService(ContractPriceComponentConv.class);
		List<ContractPriceComponentConv> list = new ArrayList<>();
		for (Object id : ids) {
			list.addAll(srv.findByContractPriceComponentId((Integer) id));
		}
		srv.delete(list);
	}

	private void updateAsIs(List<ContractPriceComponent> updList) throws BusinessException {
		ContractPriceComponentConv_Bd bd = this.getBusinessDelegate(ContractPriceComponentConv_Bd.class);
		for (ContractPriceComponent c : updList) {
			c.setConvertedPrices(null);
			bd.insertContractPriceComponentConv(c, false);
			this.onUpdate(c);

		}
	}

	@Override
	public List<ContractPriceComponent> getPricingComponentsWithoutPricingBases(Contract contract) throws BusinessException {
		String hql = "select e from " + ContractPriceComponent.class.getSimpleName()
				+ " e where e.clientId = :clientId and e.contrPriceCtgry.contract.id = :contractId and e.contrPriceCtgry.pricingBases IS NULL order by e.validFrom ";
		return this.getEntityManager().createQuery(hql, ContractPriceComponent.class).setParameter("contractId", contract.getId())
				.setParameter("clientId", Session.user.get().getClientId()).getResultList();
	}

	@Override
	public Boolean isIndex(ContractPriceComponent e) throws BusinessException {
		return e.getContrPriceCtgry().getPricingBases() != null
				&& PricingBase_Bd.INDEX.equalsIgnoreCase(e.getContrPriceCtgry().getPricingBases().getPriceCat().getName());
	}
}
