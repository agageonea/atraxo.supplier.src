package atraxo.cmm.business.ext.bpm.util;

import java.math.BigDecimal;
import java.math.MathContext;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.ext.utils.DateUtils;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author vhojda
 */
public class ContractVolumeExtracter {

	@Autowired
	private ISystemParameterService systemParamsService;

	@Autowired
	protected IUnitService unitSrv;

	/**
	 * Calculates if the volume of a specific <code>Contract</code> is over (bigger than) a specified volume, represented by parameters (value and
	 * measurement unit)
	 *
	 * @param contract
	 * @param thresholdUnit
	 * @param thresholdVolume
	 * @returns true if the volume is bigger than the one of the contract, false otherwise and null if contract doesn't have a volume defined
	 * @throws BusinessException
	 */
	public Boolean isOverVolume(Contract contract, Unit thresholdUnit, Integer thresholdVolume) throws BusinessException {
		Boolean overVolume = null;

		// Convert bid volume to workflow volume
		Double density = Double.parseDouble(this.systemParamsService.getDensity());

		BigDecimal vol = null;
		if (!contract.getIsContract()) {// bid
			if (contract.getOfferedVolume() == null) {
				return null;
			}
			vol = this.unitSrv.convert(contract.getContractVolumeUnit(), thresholdUnit, contract.getOfferedVolume(), density);
		} else {// contract
			if (contract.getAwardedVolume() == null) {
				return null;
			}
			vol = this.unitSrv.convert(contract.getSettlementUnit(), thresholdUnit, contract.getAwardedVolume(), density);
		}

		// calculate volume based on period type
		int months = DateUtils.monthsBetweenDates(contract.getValidFrom(), contract.getValidTo()) + 1;

		BigDecimal dYear = new BigDecimal(12);
		BigDecimal dMonths = new BigDecimal(months);

		BigDecimal volumePerContract = null;
		switch (contract.getPeriod()) {
		case _MONTHLY_:
			volumePerContract = vol.multiply(dMonths, MathContext.DECIMAL64);
			break;
		case _YEARLY_:
			volumePerContract = vol.divide(dYear, MathContext.DECIMAL64).multiply(dMonths, MathContext.DECIMAL64);
			break;
		case _CONTRACT_PERIOD_:
			volumePerContract = vol;
			break;
		case _EMPTY_:
			volumePerContract = vol;
			break;
		default:
			break;
		}

		if (volumePerContract != null) {
			overVolume = volumePerContract.intValue() > thresholdVolume;
		}

		return overVolume;

	}

}
