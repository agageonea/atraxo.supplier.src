/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.ext.contracts.service;

import java.util.List;

import atraxo.cmm.business.api.contracts.IMarkedContractService;
import atraxo.cmm.domain.impl.contracts.MarkedContract;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link MarkedContract} domain entity.
 */
public class MarkedContract_Service extends atraxo.cmm.business.impl.contracts.MarkedContract_Service implements IMarkedContractService {

	@Override
	public MarkedContract findAnEntity() throws BusinessException {
		String qlString = "SELECT e FROM MarkedContract e WHERE e.clientId = :clientId ";
		List<MarkedContract> list = this.getEntityManager().createQuery(qlString, this.getEntityClass())
				.setParameter("clientId", Session.user.get().getClientId()).setMaxResults(1).getResultList();
		if (!list.isEmpty()) {
			return list.get(0);
		}
		return null;
	}

}
