package atraxo.cmm.business.ext.contracts.delegate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.ext.exceptions.SupplierContractNotFoundException;
import atraxo.cmm.business.ext.prices.service.PriceConverterService;
import atraxo.cmm.domain.ext.contracts.SupplierContract;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.PricingMethod;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.fmbas.business.ext.sys.delegate.SystemParameterDelegate;
import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class SupplierContract_Bd extends AbstractBusinessDelegate {

	private final static Logger LOGGER = LoggerFactory.getLogger(SupplierContract_Bd.class);

	public SupplierContract buildSupplierContract(Date dateFrom, Date dateTo, Contract contract, Unit unit, Currencies currency,
			AverageMethod avgMthd, FinancialSources financialSource, MasterAgreementsPeriod offset) throws BusinessException {
		ContractStatus state = contract.getStatus();
		if ((state.equals(ContractStatus._ACTIVE_) || state.equals(ContractStatus._EFFECTIVE_)) && contract.getValidFrom().compareTo(dateFrom) <= 0
				&& contract.getValidTo().compareTo(dateTo) >= 0) {
			Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH);
			if (date.before(contract.getValidFrom())) {
				date = contract.getValidFrom();
			}
			SupplierContract supplierContract = new SupplierContract();
			supplierContract.setContract(contract);
			supplierContract.setUnit(currency.getCode() + "/" + unit.getCode());
			supplierContract
					.setBasePrice(this.getPriceForType(contract, PriceType._PRODUCT_, null, currency, unit, avgMthd, financialSource, offset, date));
			supplierContract.setDifferential(
					this.getPriceForType(contract, PriceType._DIFFERENTIAL_, null, currency, unit, avgMthd, financialSource, offset, date));
			supplierContract.setIntoPlaneFee(this.getPriceForType(contract, PriceType._INTO_PLANE_FEE_, PriceInd._VOLUME_, currency, unit, avgMthd,
					financialSource, offset, date));
			supplierContract.setOtherFees(
					this.getPriceForType(contract, PriceType._OTHER_FEE_, PriceInd._VOLUME_, currency, unit, avgMthd, financialSource, offset, date));
			supplierContract.setTaxes(this.getPriceForType(contract, PriceType._TAX_, null, currency, unit, avgMthd, financialSource, offset, date));
			supplierContract.setPerFlightFee(
					this.getPriceForType(contract, PriceType._OTHER_FEE_, PriceInd._EVENT_, currency, unit, avgMthd, financialSource, offset, date));
			supplierContract.setPaymentTerms(contract.getPaymentTerms());
			supplierContract.setCreditTerms(contract.getCreditTerms().getName());
			this.setContractSpecificAttributes(contract, PriceType._PRODUCT_, supplierContract);
			return supplierContract;
		}
		throw new SupplierContractNotFoundException("Invalid Supplier contract.");
	}

	private void setContractSpecificAttributes(Contract contract, PriceType type, SupplierContract supplierContract) {
		Collection<ContractPriceCategory> priceCategories = contract.getPriceCategories();
		for (Iterator<ContractPriceCategory> iterator = priceCategories.iterator(); iterator.hasNext();) {
			ContractPriceCategory contractPriceCategory = iterator.next();
			PriceType priceCategoryType = contractPriceCategory.getPriceCategory().getType();
			if (type.equals(priceCategoryType)) {
				for (Iterator<ContractPriceComponent> iter = contractPriceCategory.getPriceComponents().iterator(); iter.hasNext();) {
					ContractPriceComponent priceComponent = iter.next();
					supplierContract.setFlightType(priceComponent.getContrPriceCtgry().getContract().getLimitedTo().getName());
					supplierContract.setType(priceComponent.getContrPriceCtgry().getContract().getType().getName());
					supplierContract.setDelivery(priceComponent.getContrPriceCtgry().getContract().getSubType().getName());
					supplierContract.setScope(priceComponent.getContrPriceCtgry().getContract().getScope().getName());
					if (priceComponent.getContrPriceCtgry().getPriceCategory().getIsSys()
							&& priceComponent.getContrPriceCtgry().getPriceCategory().getIsIndexBased()) {
						supplierContract.setPriceBasis(PricingMethod._INDEX_);
						supplierContract.setIndexOffset(priceComponent.getContrPriceCtgry().getPricingBases().getQuotationOffset().getName());
					} else {
						try {
							supplierContract.setPriceBasis(
									PricingMethod.getByName(priceComponent.getContrPriceCtgry().getPriceCategory().getName().toUpperCase()));
						} catch (Exception e) {
							LOGGER.warn(
									"Could not set the basis price by retrieving the pricing method based on price category, will use the default FIXED one !",
									e);
							supplierContract.setPriceBasis(PricingMethod._FIXED_);
						}
					}

					supplierContract.setExchangeRateOffset(priceComponent.getContrPriceCtgry().getExchangeRateOffset().getName());
					supplierContract.setIntoPlaneAgent(priceComponent.getContrPriceCtgry().getContract().getIntoPlaneAgent());
					supplierContract.setQuotation(priceComponent.getContrPriceCtgry().getPricingBases() != null
							? priceComponent.getContrPriceCtgry().getPricingBases().getQuotation() : null);
					supplierContract.setExposure(0);
				}
			}
		}
	}

	/**
	 * Calculates the sum of prices of <code>type</code> and <code>per</code> for a period between <code>dateFrom</code> and <code>dateTo</code>.
	 *
	 * @param contract
	 * @param type
	 * @param priceCategoryPer
	 * @return
	 * @throws BusinessException
	 */
	private BigDecimal getPriceForType(Contract contract, PriceType type, PriceInd priceCategoryPer, Currencies currency, Unit unit,
			AverageMethod averageMethod, FinancialSources financialSource, MasterAgreementsPeriod offset, Date date) throws BusinessException {
		Collection<ContractPriceCategory> priceCategories = contract.getPriceCategories();
		BigDecimal price = BigDecimal.ZERO;
		PricingBase pricingBase = null;
		for (Iterator<ContractPriceCategory> iterator = priceCategories.iterator(); iterator.hasNext();) {
			ContractPriceCategory contractPriceCategory = iterator.next();
			PriceType priceCategoryType = contractPriceCategory.getPriceCategory().getType();
			if (contractPriceCategory.getDefaultPriceCtgy() || priceCategoryType.equals(PriceType._PRODUCT_)) {
				PriceInd categoryPer = contractPriceCategory.getPriceCategory().getPricePer();
				if (type.equals(priceCategoryType) && (priceCategoryPer == null || categoryPer.equals(priceCategoryPer))) {
					pricingBase = contractPriceCategory.getPricingBases();
					List<ContractPriceComponent> priceComponents = new ArrayList<>(contractPriceCategory.getPriceComponents());
					List<ContractPriceComponentConv> convPriceComponents = new ArrayList<>();
					for (ContractPriceComponent priceComponent : priceComponents) {
						convPriceComponents.addAll(priceComponent.getConvertedPrices());
					}
					if (!convPriceComponents.isEmpty()) {
						Collections.sort(convPriceComponents, new Comparator<ContractPriceComponentConv>() {
							@Override
							// DESC order
							public int compare(ContractPriceComponentConv arg0, ContractPriceComponentConv arg1) {
								return arg1.getValidTo().compareTo(arg0.getValidTo());
							}

						});
						for (ContractPriceComponentConv convPriceComponent : convPriceComponents) {
							if (convPriceComponent.getValidFrom().compareTo(date) <= 0 && convPriceComponent.getValidTo().compareTo(date) >= 0) {
								price = price.add(convPriceComponent.getEquivalent());
								break;
							}
						}
					}
				}
			}
		}

		return this.convertPrice(contract, currency, unit, price, averageMethod, financialSource, offset, pricingBase);
	}

	private BigDecimal convertPrice(Contract contract, Currencies currency, Unit unit, BigDecimal price, AverageMethod averageMethod,
			FinancialSources financialSource, MasterAgreementsPeriod offset, PricingBase pricingBase) throws BusinessException {
		PriceConverterService priceConverterService = this.getApplicationContext().getBean(PriceConverterService.class);
		ConversionResult result;
		if (this.isIndex(pricingBase)) {
			result = priceConverterService.convert(contract.getSettlementUnit(), unit, contract.getSettlementCurr(), currency, price,
					GregorianCalendar.getInstance().getTime(), financialSource, averageMethod, pricingBase, false, offset);
		} else {
			Density density = this.getBusinessDelegate(SystemParameterDelegate.class).getSystemDensity();
			result = priceConverterService.convert(contract.getSettlementUnit(), unit, contract.getSettlementCurr(), currency, price,
					GregorianCalendar.getInstance().getTime(), financialSource, averageMethod, density, false, offset);
		}
		return result.getValue();
	}

	private boolean isIndex(PricingBase pricingBase) {
		return pricingBase != null && PricingBase_Bd.INDEX.equalsIgnoreCase(pricingBase.getPriceCat().getName());
	}

}
