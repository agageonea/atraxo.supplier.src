/**
 *
 */
package atraxo.cmm.business.ext.bid;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.handler.MessageContext;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.ext.bid.IBidProcessor;
import atraxo.cmm.business.api.ext.bid.IBidService;
import atraxo.cmm.business.api.ext.datahub.IDataHubClientRequestService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.tender.service.AbstractMessageProcessor;
import atraxo.cmm.business.ws.tender.client.DataHubSoapClient;
import atraxo.cmm.business.ws.tender.transformer.TenderBidAcknowledgmentTransformer;
import atraxo.cmm.domain.ext.tender.DataHubRequestType;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.SingleResultException;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import atraxo.fmbas.domain.ws.dto.WSExecutionType;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.commons.utils.XMLUtils;
import seava.j4e.iata.fuelplus.iata.tender.AirlinesVolumes.AirlinesVolume;
import seava.j4e.iata.fuelplus.iata.tender.AwardedBid;
import seava.j4e.iata.fuelplus.iata.tender.BidIdentification;
import seava.j4e.iata.fuelplus.iata.tender.DeclinedBid;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcceptAward;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcknowledge;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBid;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBidAward;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBidDecline;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderCancelBid;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderDeclineAward;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderNoBid;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderResponse;
import seava.j4e.iata.fuelplus.iata.tender.PUOMBase;

/**
 * @author zspeter
 */
public class BidProcessor extends AbstractMessageProcessor implements IBidProcessor {

	private static final String QUERY_PARAM_BID_REVISION = "bidRevision";
	private static final String QUERY_PARAM_BID_VERSION = "bidVersion";
	private static final String QUERY_PARAM_BID_FLIGHT_SERVICE_TYPE = "flightServiceType";
	private static final String QUERY_PARAM_BID_PRODUCT = "product";
	private static final String QUERY_PARAM_BID_TAX_TYPE = "tax";
	private static final String QUERY_PARAM_BID_TENDER_IDENTIFICATION = "bidTenderIdentification";

	private static final String QUERY_PARAM_BID_LOCATION = "location";

	private static final String METHOD_NOT_SUPPORTED = "Method not supported";
	private static final Logger LOGGER = LoggerFactory.getLogger(BidProcessor.class);

	@Autowired
	private IBidService bidService;
	@Autowired
	private ISystemParameterService systemParamsService;
	@Autowired
	private IUnitService unitService;
	@Autowired
	private TenderBidAcknowledgmentTransformer tenderBidAckTransformer;
	@Autowired
	@Qualifier("tenderBidClientRequestService")
	private IDataHubClientRequestService<Contract, FuelTenderAcknowledge> requestService;

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.ext.bid.IBidProcessor#fuelTenderAcceptAward(seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcceptAward)
	 */
	@Override
	public FuelTenderResponse fuelTenderAcceptAward(FuelTenderAcceptAward parameters) {
		return this.methodNotSupported();
	}

	@Override
	public FuelTenderResponse fuelTenderDeclineAward(FuelTenderDeclineAward parameters) {
		return this.methodNotSupported();
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.ext.bid.IBidProcessor#fuelTenderBidDecline(seava.j4e.iata.fuelplus.iata.tender.FuelTenderBidDecline)
	 */
	@Override
	public FuelTenderResponse fuelTenderBidDecline(FuelTenderBidDecline parameters, MessageContext messageContext) {
		List<Contract> declinedList = new ArrayList<>();
		FuelTenderResponse response = new FuelTenderResponse();

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		// start constructing the interface execution result
		WSExecutionResult execResult = new WSExecutionResult();
		execResult.setExecutionType(WSExecutionType.IMPORT_DATA);
		execResult.setSuccess(false);

		try {
			this.initSessionAndInterface(ExtInterfaceNames.FUEL_TENDER_BID_DECLINE_INTERFACE.getValue(), messageContext);

			if (this.getFuelTenderInterface().getEnabled()) {
				Tender tender = this.findTendersByIdentification(parameters.getTenderIdentification());
				long msgId = parameters.getTenderIdentification().getMsgId();

				for (DeclinedBid declinedBid : parameters.getDeclinedBids().getDeclinedBid()) {
					Contract bid = this.findBidsByIdentification(tender, declinedBid.getBidIdentification());

					bid.setBidStatus(BidStatus._DECLINED_);
					this.contractService.update(bid);

					// Update Location and Tender Bidding Status
					this.bidService.propagateBiddingStatus(bid, BidStatus._DECLINED_.getName(), "Decline Tender Bid");

					// save notes
					this.saveNotesIfComments(bid, declinedBid.getComments());

					// Save status change to history
					this.historyService.saveChangeHistory(bid, BidStatus._DECLINED_.getName(), "Decline Tender Bid");

					declinedList.add(bid);
				}

				// set the XML as received data
				String xml = XMLUtils.toString(parameters, FuelTenderBidDecline.class, true, true);

				execResult.setExecutionData(xml);
				execResult.setSuccess(Boolean.TRUE);

				stopWatch.stop();

				// save interface history
				this.interfaceHistoryService.saveInterfaceHistory(stopWatch, execResult, this.getFuelTenderInterface());

				// send notification
				this.sendNotificationFromTender(tender);

				response.setResponse(SUCCESS_MSG);

				// send acknowledgement
				this.sendAcknowledgement(declinedList, msgId, ExtInterfaceNames.FUEL_TENDER_BID_DECLINE_INTERFACE);
			} else {
				throw new BusinessException(BusinessErrorCode.INTERFACE_DISABLED, BusinessErrorCode.INTERFACE_DISABLED.getErrMsg());
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			execResult.setExecutionData(e.getLocalizedMessage() + "::" + ExceptionUtils.getStackTrace(e));

			try {
				this.interfaceHistoryService.saveInterfaceHistory(stopWatch, execResult, this.getFuelTenderInterface());
			} catch (Exception e1) {
				LOGGER.error(e1.getMessage(), e1);
			}

			response.setResponse(ERROR_MSG);
			response.setErrorDescription(e.getLocalizedMessage());
		}

		return response;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.ext.bid.IBidProcessor#fuelTenderBidAward(seava.j4e.iata.fuelplus.iata.tender.FuelTenderBidAward)
	 */
	@Override
	public FuelTenderResponse fuelTenderBidAward(FuelTenderBidAward parameters, MessageContext messageContext) {
		FuelTenderResponse response = new FuelTenderResponse();

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		// start constructing the interface execution result
		WSExecutionResult execResult = new WSExecutionResult();
		execResult.setExecutionType(WSExecutionType.IMPORT_DATA);
		execResult.setSuccess(false);

		try {
			this.initSessionAndInterface(ExtInterfaceNames.FUEL_TENDER_BID_AWARD_INTERFACE.getValue(), messageContext);

			if (this.getFuelTenderInterface().getEnabled()) {
				List<Contract> awardedList = new ArrayList<>();
				Tender tender = this.findTendersByIdentification(parameters.getTenderIdentification());
				long msgId = parameters.getTenderIdentification().getMsgId();

				for (AwardedBid awardedBid : parameters.getAwardedBids().getAwardedBid()) {
					Contract bid = this.findBidsByIdentification(tender, awardedBid.getBidIdentification());

					BigDecimal awardedVolume = this.getBidAwardedVolume(awardedBid, bid);

					bid.setBidStatus(BidStatus._AWARDED_);
					bid.setAwardedVolume(awardedVolume);
					this.contractService.update(bid);

					// Update Location and Tender Bidding Status
					this.bidService.propagateBiddingStatus(bid, BidStatus._AWARDED_.getName(), "Award Tender Bid");

					// save notes
					this.saveNotesIfComments(bid, awardedBid.getComments());

					this.historyService.saveChangeHistory(bid, BidStatus._AWARDED_.getName(), "Award Tender Bid");
					awardedList.add(bid);
				}

				// set the XML as received data-
				String xml = XMLUtils.toString(parameters, FuelTenderBidAward.class, true, true);

				execResult.setExecutionData(xml);
				execResult.setSuccess(Boolean.TRUE);

				stopWatch.stop();

				// save interface history
				this.interfaceHistoryService.saveInterfaceHistory(stopWatch, execResult, this.getFuelTenderInterface());

				response.setResponse(SUCCESS_MSG);

				// send acknowledgement
				this.sendAcknowledgement(awardedList, msgId, ExtInterfaceNames.FUEL_TENDER_BID_AWARD_INTERFACE);
			} else {
				throw new BusinessException(BusinessErrorCode.INTERFACE_DISABLED, BusinessErrorCode.INTERFACE_DISABLED.getErrMsg());
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			execResult.setExecutionData(e.getLocalizedMessage() + "::" + ExceptionUtils.getStackTrace(e));

			try {
				this.interfaceHistoryService.saveInterfaceHistory(stopWatch, execResult, this.getFuelTenderInterface());
			} catch (Exception e1) {
				LOGGER.error(e1.getMessage(), e1);
			}

			response.setResponse(ERROR_MSG);
			response.setErrorDescription(e.getLocalizedMessage());
		}

		return response;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.ext.bid.IBidProcessor#fuelTenderNoBid(seava.j4e.iata.fuelplus.iata.tender.FuelTenderNoBid)
	 */
	@Override
	public FuelTenderResponse fuelTenderNoBid(FuelTenderNoBid parameters) {
		return this.methodNotSupported();
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.ext.bid.IBidProcessor#fuelTenderBid(seava.j4e.iata.fuelplus.iata.tender.FuelTenderBid)
	 */
	@Override
	public FuelTenderResponse fuelTenderBid(FuelTenderBid parameters) {
		return this.methodNotSupported();
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.ext.bid.IBidProcessor#fuelTenderCancelBid(seava.j4e.iata.fuelplus.iata.tender.FuelTenderCancelBid)
	 */
	@Override
	public FuelTenderResponse fuelTenderCancelBid(FuelTenderCancelBid parameters) {
		return this.methodNotSupported();
	}

	private FuelTenderResponse methodNotSupported() {
		FuelTenderResponse response = new FuelTenderResponse();
		response.setResponse(ERROR_MSG);
		response.setErrorDescription(METHOD_NOT_SUPPORTED);
		return response;
	}

	private Contract findBidsByIdentification(Tender tenderIdent, BidIdentification bidIdent) throws BusinessException {
		String bidderCode = bidIdent.getBidderCode();
		String bidVersion = bidIdent.getBidVersion();
		Integer bidRevision = bidIdent.getBidRevision().intValue();

		String locationIataCode = bidIdent.getLocationHeader().getLocationCodeIATA();
		String locationIcaoCode = bidIdent.getLocationHeader().getLocationCodeICAO();

		FlightServiceType flightServiceType = FlightServiceType.getByCode(bidIdent.getLocationHeader().getFlightServiceType().name());
		Product fuelProduct = Product.getByIataName(bidIdent.getLocationHeader().getFuelProduct().value());
		TaxType productTaxType = TaxType.getByIataCode(bidIdent.getLocationHeader().getProductTaxType().name());

		Customer bidder = this.getCustomer(bidderCode, "BidIdentification - BidderCode");
		Locations bidLocation = this.getLocation(locationIataCode, locationIcaoCode);

		try {
			Map<String, Object> identificationParams = new HashMap<>();
			identificationParams.put(QUERY_PARAM_BID_TENDER_IDENTIFICATION, tenderIdent);
			identificationParams.put(QUERY_PARAM_HOLDER, bidder);
			identificationParams.put(QUERY_PARAM_BID_REVISION, bidRevision);
			identificationParams.put(QUERY_PARAM_BID_VERSION, bidVersion);
			identificationParams.put(QUERY_PARAM_BID_LOCATION, bidLocation);
			identificationParams.put(QUERY_PARAM_BID_FLIGHT_SERVICE_TYPE, flightServiceType);
			identificationParams.put(QUERY_PARAM_BID_PRODUCT, fuelProduct);
			identificationParams.put(QUERY_PARAM_BID_TAX_TYPE, productTaxType);

			return this.contractService.findEntityByAttributes(identificationParams);
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw new SingleResultException("Multiple Bid found for identification in file, please check BidIdentification and reprocess", nre);
			}
			throw new SingleResultException(String.format(CmmErrorCode.SINGLE_RESULT_EXCEPTION_MESSAGE.getErrMsg(), "Bid", "", "BidIdentification"),
					nre);
		}
	}

	private BigDecimal getBidAwardedVolume(AwardedBid awardedBid, Contract bid) throws BusinessException {
		BigDecimal awardedVolume = BigDecimal.ZERO;
		Double density = Double.parseDouble(this.systemParamsService.getDensity());

		if (awardedBid.getAwardedVolume().getAirlinesVolumes() != null
				&& !CollectionUtils.isEmpty(awardedBid.getAwardedVolume().getAirlinesVolumes().getAirlinesVolume())) {

			Unit fromUnit = this.getUnit(awardedBid.getAwardedVolume().getAirlinesVolumes().getUOM(), "AirlinesVolume - UOM");
			for (AirlinesVolume airlineVolume : awardedBid.getAwardedVolume().getAirlinesVolumes().getAirlinesVolume()) {
				for (ShipTo s : bid.getShipTo()) {
					if (s.getCustomer().getIataCode().equals(airlineVolume.getAirlineCode())
							|| s.getCustomer().getCode().equals(airlineVolume.getAirlineCode())) {
						BigDecimal allocatedVolume = this.unitService.convert(fromUnit, bid.getSettlementUnit(), airlineVolume.getVolume(), density);

						s.setAllocatedVolume(allocatedVolume);

						awardedVolume = awardedVolume.add(allocatedVolume);
					}
				}
			}
		} else if (awardedBid.getAwardedVolume().getTotalVolume() != null) {
			Unit fromUnit = this.getUnit(awardedBid.getAwardedVolume().getTotalVolume().getUOM(), "TotalVolume - UOM");
			awardedVolume = this.unitService.convert(fromUnit, bid.getSettlementUnit(), awardedBid.getAwardedVolume().getTotalVolume().getVolume(),
					density);
		}
		return awardedVolume;
	}

	/**
	 * @param bids
	 * @param receiverInterface
	 * @throws BusinessException
	 */
	private void sendAcknowledgement(List<Contract> bids, long msgId, ExtInterfaceNames receiverInterface) throws BusinessException {
		ExternalInterface externalInterface = this.interfaceService.findByName(ExtInterfaceNames.FUEL_TENDER_PROCESSING_RESULT.getValue());
		ExtInterfaceParameters extInterfaceParams = new ExtInterfaceParameters(externalInterface.getParams());

		// Prepare params for datahub soap client
		DataHubSoapClient<Contract, FuelTenderAcknowledge> dataHubSoapClient = new DataHubSoapClient<>(externalInterface, extInterfaceParams, bids,
				this.interfaceHistoryFacade, this.interfaceMessageHistoryFacade, this.tenderBidAckTransformer, FuelTenderAcknowledge.class,
				DataHubRequestType.SINGLE_REQUEST);

		dataHubSoapClient.setRequestService(this.requestService);
		dataHubSoapClient.setReceiverInterface(receiverInterface);
		dataHubSoapClient.setRefMsgId(msgId);
		dataHubSoapClient.start(1);
	}

	private Unit getUnit(PUOMBase code, String property) throws BusinessException {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put(QUERY_PARAM_IATA_CODE, code.value());

			return this.unitService.findEntityByAttributes(params);
		} catch (ApplicationException nre) {
			throw new SingleResultException(String.format(CmmErrorCode.SINGLE_RESULT_EXCEPTION_MESSAGE.getErrMsg(), "Unit", code, property), nre);
		}
	}

}
