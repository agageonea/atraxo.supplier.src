/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.contracts;

import atraxo.cmm.business.api.contracts.IContractTemplateService;
import atraxo.cmm.domain.impl.contracts.ContractTemplate;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ContractTemplate} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ContractTemplate_Service
		extends
			AbstractEntityService<ContractTemplate>
		implements
			IContractTemplateService {

	/**
	 * Public constructor for ContractTemplate_Service
	 */
	public ContractTemplate_Service() {
		super();
	}

	/**
	 * Public constructor for ContractTemplate_Service
	 * 
	 * @param em
	 */
	public ContractTemplate_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ContractTemplate> getEntityClass() {
		return ContractTemplate.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ContractTemplate
	 */
	public ContractTemplate findByTemplates(String contractType,
			String deliverySubtype) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ContractTemplate.NQ_FIND_BY_TEMPLATES,
							ContractTemplate.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("contractType", contractType)
					.setParameter("deliverySubtype", deliverySubtype)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(
					J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ContractTemplate", "contractType, deliverySubtype"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(
					J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ContractTemplate", "contractType, deliverySubtype"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return ContractTemplate
	 */
	public ContractTemplate findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ContractTemplate.NQ_FIND_BY_BUSINESS,
							ContractTemplate.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ContractTemplate", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ContractTemplate", "id"), nure);
		}
	}

}
