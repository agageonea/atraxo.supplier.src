/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.prices;

import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ContractPriceRestriction} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ContractPriceRestriction_Service
		extends
			AbstractEntityService<ContractPriceRestriction> {

	/**
	 * Public constructor for ContractPriceRestriction_Service
	 */
	public ContractPriceRestriction_Service() {
		super();
	}

	/**
	 * Public constructor for ContractPriceRestriction_Service
	 * 
	 * @param em
	 */
	public ContractPriceRestriction_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ContractPriceRestriction> getEntityClass() {
		return ContractPriceRestriction.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ContractPriceRestriction
	 */
	public ContractPriceRestriction findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							ContractPriceRestriction.NQ_FIND_BY_BUSINESS,
							ContractPriceRestriction.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ContractPriceRestriction", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ContractPriceRestriction", "id"), nure);
		}
	}

	/**
	 * Find by reference: contractPriceCategory
	 *
	 * @param contractPriceCategory
	 * @return List<ContractPriceRestriction>
	 */
	public List<ContractPriceRestriction> findByContractPriceCategory(
			ContractPriceCategory contractPriceCategory) {
		return this
				.findByContractPriceCategoryId(contractPriceCategory.getId());
	}
	/**
	 * Find by ID of reference: contractPriceCategory.id
	 *
	 * @param contractPriceCategoryId
	 * @return List<ContractPriceRestriction>
	 */
	public List<ContractPriceRestriction> findByContractPriceCategoryId(
			Integer contractPriceCategoryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ContractPriceRestriction e where e.clientId = :clientId and e.contractPriceCategory.id = :contractPriceCategoryId",
						ContractPriceRestriction.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractPriceCategoryId",
						contractPriceCategoryId).getResultList();
	}
	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<ContractPriceRestriction>
	 */
	public List<ContractPriceRestriction> findByUnit(Unit unit) {
		return this.findByUnitId(unit.getId());
	}
	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<ContractPriceRestriction>
	 */
	public List<ContractPriceRestriction> findByUnitId(Integer unitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ContractPriceRestriction e where e.clientId = :clientId and e.unit.id = :unitId",
						ContractPriceRestriction.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitId", unitId).getResultList();
	}
}
