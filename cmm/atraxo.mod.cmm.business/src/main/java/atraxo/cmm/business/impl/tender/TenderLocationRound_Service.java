/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.tender;

import atraxo.cmm.business.api.tender.ITenderLocationRoundService;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link TenderLocationRound} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class TenderLocationRound_Service
		extends
			AbstractEntityService<TenderLocationRound>
		implements
			ITenderLocationRoundService {

	/**
	 * Public constructor for TenderLocationRound_Service
	 */
	public TenderLocationRound_Service() {
		super();
	}

	/**
	 * Public constructor for TenderLocationRound_Service
	 * 
	 * @param em
	 */
	public TenderLocationRound_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<TenderLocationRound> getEntityClass() {
		return TenderLocationRound.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return TenderLocationRound
	 */
	public TenderLocationRound findByTender_location_round(
			TenderLocation tenderLocation, Integer roundNo) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							TenderLocationRound.NQ_FIND_BY_TENDER_LOCATION_ROUND,
							TenderLocationRound.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("tenderLocation", tenderLocation)
					.setParameter("roundNo", roundNo).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TenderLocationRound", "tenderLocation, roundNo"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TenderLocationRound", "tenderLocation, roundNo"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return TenderLocationRound
	 */
	public TenderLocationRound findByTender_location_round(
			Long tenderLocationId, Integer roundNo) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							TenderLocationRound.NQ_FIND_BY_TENDER_LOCATION_ROUND_PRIMITIVE,
							TenderLocationRound.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("tenderLocationId", tenderLocationId)
					.setParameter("roundNo", roundNo).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(
					J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TenderLocationRound", "tenderLocationId, roundNo"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(
					J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TenderLocationRound", "tenderLocationId, roundNo"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return TenderLocationRound
	 */
	public TenderLocationRound findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(TenderLocationRound.NQ_FIND_BY_BUSINESS,
							TenderLocationRound.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TenderLocationRound", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TenderLocationRound", "id"), nure);
		}
	}

	/**
	 * Find by reference: tenderLocation
	 *
	 * @param tenderLocation
	 * @return List<TenderLocationRound>
	 */
	public List<TenderLocationRound> findByTenderLocation(
			TenderLocation tenderLocation) {
		return this.findByTenderLocationId(tenderLocation.getId());
	}
	/**
	 * Find by ID of reference: tenderLocation.id
	 *
	 * @param tenderLocationId
	 * @return List<TenderLocationRound>
	 */
	public List<TenderLocationRound> findByTenderLocationId(
			Integer tenderLocationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TenderLocationRound e where e.clientId = :clientId and e.tenderLocation.id = :tenderLocationId",
						TenderLocationRound.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("tenderLocationId", tenderLocationId)
				.getResultList();
	}
}
