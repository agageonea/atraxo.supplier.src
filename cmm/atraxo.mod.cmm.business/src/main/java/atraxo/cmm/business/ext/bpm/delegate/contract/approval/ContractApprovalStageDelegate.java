/**
 *
 */
package atraxo.cmm.business.ext.bpm.delegate.contract.approval;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.Expression;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.ext.bpm.delegate.ContractDelegate;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.ext.mailmerge.dto.ContractApprovalDto;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.workflow.IWorkflowParameterService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailAddressesDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;

/**
 * Top <code>ContractDelegate</code> class with common behaviour for all the delegates that participate in the Sell/Purchase Contract Approval
 * Workflow
 *
 * @author vhojda
 */
public class ContractApprovalStageDelegate extends ContractDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContractApprovalStageDelegate.class);

	@Autowired
	protected IWorkflowParameterService workflowParameterService;

	private Expression role;

	private Expression step;

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.bpm.delegate.AbstractJavaDelegate#execute()
	 */
	@Override
	public void execute() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}

		Contract contract = this.getContract();
		if (contract != null) {

			// reset the next approval stage variable
			this.setNextApprovalStageRequiredVariable(true);

			WorkflowParameter param = this.workflowParameterService.findByKey(Long.valueOf(this.workflowId), this.getStageApprovalRolesVariable());

			// extract the approvers and setup the email data
			List<UserSupp> users = this.extractApprovers(this.getStageApprovalRolesVariable(), param.getMandatory());

			if (!CollectionUtils.isEmpty(users)) {
				EmailDto emailDTO = this.extractEmailDto(contract, users);
				if (emailDTO != null) {
					this.execution.setVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_DTO, emailDTO);
				}
			} else {
				this.setNextApprovalStageRequiredVariable(false);
			}

			// make sure to reset the mail components
			this.resetMailComponentVariables();

		} else {
			// do nothing, the workflow will take care of it
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
	}

	/**
	 * @return
	 */
	private String getStageApprovalRolesVariable() {
		return (String) this.role.getValue(this.execution);
	}

	/**
	 * @return
	 */
	private int getStageApprovalStepVariable() {
		String approvalStep = (String) this.step.getValue(this.execution);
		return Integer.parseInt(approvalStep);
	}

	/**
	 * @param required
	 */
	protected void setNextApprovalStageRequiredVariable(boolean required) {
		this.execution.setVariable(WorkflowVariablesConstants.NEXT_APPROVAL_STAGE_REQUIRED, required);
	}

	/**
	 * @param contract
	 * @param users
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	protected EmailDto extractEmailDto(Contract contract, List<UserSupp> users) throws Exception {
		EmailDto emailDTO = new EmailDto();

		// extract data from contract
		String contractCode = "";
		String contractHolder = "";
		String supplierName = "";
		String customerName = "";
		String contractLocation = "";
		try {
			contractCode = contract.getCode();
			contractHolder = contract.getHolder().getName();
			contractLocation = contract.getLocation().getName();
			if (contract.getDealType().equals(DealType._BUY_)) {
				supplierName = contract.getSupplier().getName();
			} else if (contract.getDealType().equals(DealType._SELL_)) {
				customerName = contract.getCustomer().getName();
			}
		} catch (Exception e) {
			LOGGER.error("ERROR:could not extract data from the contract " + contract.getId() + " in order to send the mail ! ", e);
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
					String.format(CmmErrorCode.CONTRACT_WORKFLOW_EMAIL_EXTRACT_DATA_ERROR.getErrMsg(), contract.getId()));
			emailDTO = null;
		}

		if (emailDTO != null) {
			// set the subject
			if (contract.getDealType().equals(DealType._BUY_)) {
				emailDTO.setSubject(
						"Purchase Contract " + contractCode + " for " + contractLocation + " with " + supplierName + " awaiting your approval");
			} else if (contract.getDealType().equals(DealType._SELL_)) {
				emailDTO.setSubject(
						"Sales Contract " + contractCode + " for " + contractLocation + " with " + customerName + " awaiting your approval");
			}

			// set attachments
			Object emailAttachments = this.execution.getVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_ATTACHMENTS);
			if (emailAttachments != null && emailAttachments instanceof List) {
				emailDTO.setAttachments((List<String>) emailAttachments);
			}

			// common email data for all receivers taken from workflow
			String fullNameOfRequester = this.getVariableFromWorkflow(WorkflowVariablesConstants.FULLNAME_REQUESTER_VAR);
			String approvalNote = this.getVariableFromWorkflow(WorkflowVariablesConstants.APPROVE_NOTE);

			List<EmailAddressesDto> emailAddressesDtoList = new ArrayList<>();
			for (UserSupp user : users) {

				ContractApprovalDto dataDto = new ContractApprovalDto();
				dataDto.setFullnameOfRequester(fullNameOfRequester);
				dataDto.setTitle(user.getTitle().getName());
				dataDto.setFullName(user.getFirstName() + " " + user.getLastName());

				dataDto.setContractCode(contractCode);
				dataDto.setContractHolder(contractHolder);
				dataDto.setContractLocation(contractLocation);

				dataDto.setCustomerName(customerName);
				dataDto.setSupplierName(supplierName);
				dataDto.setValidFrom(contract.getValidFrom());
				dataDto.setValidTo(contract.getValidTo());

				dataDto.setApprovalRequestNote(approvalNote);

				// set customer price update email data for each approval step
				this.updateContractEmailDto(dataDto);

				EmailAddressesDto emailAddressesDto = new EmailAddressesDto();
				emailAddressesDto.setAddress(user.getEmail());
				emailAddressesDto.setData(dataDto);
				emailAddressesDtoList.add(emailAddressesDto);
			}

			emailDTO.setTo(emailAddressesDtoList);

		}

		return emailDTO;
	}

	/**
	 * Updates the email <code>ContractApprovalDto</code> with information taken from workflow variables
	 *
	 * @param dataDto
	 */
	private void updateContractEmailDto(ContractApprovalDto dataDto) {
		String approvalNote = this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_VAR);
		String approvalUser = this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR);

		int approvalStep = this.getStageApprovalStepVariable();
		if (approvalStep == 2) {
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL1, approvalNote);
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL1, approvalUser);
		}

		if (approvalStep == 3) {
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL2, approvalNote);
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL2, approvalUser);
		}

		if (approvalStep == 4) {
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL3, approvalNote);
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL3, approvalUser);
		}

		// set the approvers (all of approvers need to be set)
		dataDto.setApprovalNoteFirstLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL1));
		dataDto.setApproverNameFirstLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL1));

		dataDto.setApprovalNoteSecondLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL2));
		dataDto.setApproverNameSecondLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL2));

		dataDto.setApprovalNoteThirdLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_VAR_LVL3));
		dataDto.setApproverNameThirdLevel(this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR_LVL3));
	}

	@Override
	public String getSessionUserCode() {
		return null;
	}

}
