package atraxo.cmm.business.ws;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;
import javax.xml.bind.JAXBException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.xml.sax.SAXException;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceMessageHistoryService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ws.GUIDMessageGenerator;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceHistory;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfacesHistoryStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ProcessStatus;
import atraxo.fmbas.domain.impl.fmbas_type.TransportStatus;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import atraxo.fmbas.domain.ws.dto.WSExecutionType;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import atraxo.fmbas.domain.ws.messgeXSD.MSG.HeaderCommon;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.commons.utils.XMLUtils;

/**
 * @author agageonea
 */
@WebService
public class AcknowledgeContractInfoWSImpl implements AcknowledgeContractInfoWS {

	private static final Logger LOGGER = LoggerFactory.getLogger(AcknowledgeContractInfoWSImpl.class);

	@Autowired
	private IExternalInterfaceService externalInterfaceSrv;
	@Autowired
	private IUserSuppService userSrv;
	@Autowired
	private ExternalInterfaceHistoryFacade historyFacade;
	@Autowired
	private IExternalInterfaceMessageHistoryService messageService;
	@Autowired
	private ExternalInterfaceMessageHistoryFacade messageFacade;

	@Override
	public MSG acknowledgeContractsEBits(MSG request)
			throws JAXBException, SAXException, IOException, BusinessException, InstantiationException, IllegalAccessException {
		MSG response = new MSG();

		HeaderCommon header = request.getHeaderCommon();
		response.setHeaderCommon(header);

		// BEGIN - Workaround for copying header and message request
		MSG originalMSG = request.getClass().newInstance();
		BeanUtils.copyProperties(request, originalMSG);
		HeaderCommon originalHeader = header.getClass().newInstance();
		BeanUtils.copyProperties(header, originalHeader);
		originalMSG.setHeaderCommon(originalHeader);
		// END - Workaround

		// start constructing the interface execution result
		WSExecutionResult execResult = new WSExecutionResult();
		execResult.setExecutionType(WSExecutionType.EXPORT_DATA);
		execResult.setSuccess(false);// assume false

		// first, try to set up the client Session (even if disabled)
		boolean foundErrors = false;
		String clientIdentifier = null;
		String correlationID = header.getCorrelationID();
		try {
			clientIdentifier = GUIDMessageGenerator.extractClientIdentifier(correlationID);
			this.userSrv.createSessionUser(clientIdentifier);
		} catch (Exception e) {
			foundErrors = true;
			LOGGER.error("ERROR: could not process received WS Client ID !!", e);
			this.fillHeaderErrorData(String.format(BusinessErrorCode.INTERFACE_NO_CLIENT_IDENTIFIER.getErrMsg(), clientIdentifier), header,
					execResult, e);
		}

		// if no errors so far and interface is active proceed with operations
		if (!foundErrors) {

			ExternalInterface ackExportContractsExternalInterface = this.externalInterfaceSrv
					.findByName(ExtInterfaceNames.ACK_EXPORT_CONTRACT_DATA_EBITS.getValue());
			if (ackExportContractsExternalInterface.getEnabled()) {
				// set the XML as received data
				String xmlMSG = XMLUtils.toString(request, MSG.class, true, true);
				execResult.setExecutionData(xmlMSG);

				// set result
				execResult.setSuccess(header.getTransportStatus().equals(TransportStatus._SENT_.getName()));

				// save history
				this.saveHistory(execResult, ackExportContractsExternalInterface);
				this.historyFacade.updateHistoryOutboundFromACK(execResult.isSuccess(), response.getHeaderCommon().getCorrelationID(), false);

			} else {
				this.fillHeaderErrorData(BusinessErrorCode.INTERFACE_DISABLED.getErrMsg(), header, execResult, null);
			}

			// set up MSG Ids (they depend on the session)
			header.setCorrelationID(correlationID);
			header.setMsgID(GUIDMessageGenerator.generateMessageID());
		}

		// set the header statuses
		header.setTransportStatus(TransportStatus._PROCESSED_.getName());
		header.setProcessStatus(execResult.isSuccess() ? ProcessStatus._SUCCESS_.getName() : ProcessStatus._FAILURE_.getName());
		this.updateMessageHistory(originalMSG,
				originalMSG.getHeaderCommon().getTransportStatus().equals(TransportStatus._SENT_.getName()) && !foundErrors);
		return response;
	}

	private void fillHeaderErrorData(String errorMsg, HeaderCommon header, WSExecutionResult execResult, Exception e) {
		header.setErrorDescription(errorMsg);
		if (e != null) {
			header.setProcessingErrorDescription(e.getLocalizedMessage());
		}
		execResult.appendExecutionData(
				header.getErrorDescription() + (e != null ? header.getProcessingErrorDescription() + "::" + ExceptionUtils.getStackTrace(e) : ""));
	}

	private void saveHistory(WSExecutionResult result, ExternalInterface ackExportContractsExternalInterface) throws BusinessException {
		ExternalInterfaceHistory history = new ExternalInterfaceHistory();
		history.setExternalInterface(ackExportContractsExternalInterface);
		history.setRequester(Session.user.get().getClientCode());
		history.setRequestReceived(new Date());
		history.setResponseSent(new Date());
		history.setStatus(result.isSuccess() ? ExternalInterfacesHistoryStatus._SUCCESS_ : ExternalInterfacesHistoryStatus._FAILURE_);
		history.setResult(result.getTrimmedExecutionData());
		this.historyFacade.insert(history);

	}

	/**
	 * @param messageId
	 * @param success
	 * @throws BusinessException
	 * @throws JAXBException
	 */
	private void updateMessageHistory(MSG request, boolean success) throws BusinessException, JAXBException {
		Map<String, Object> params = new HashMap<>();
		params.put("messageId", request.getHeaderCommon().getCorrelationID());
		List<ExternalInterfaceMessageHistory> messagesHistory = this.messageService.findEntitiesByAttributes(params);
		if (messagesHistory != null && !messagesHistory.isEmpty()) {
			for (ExternalInterfaceMessageHistory message : messagesHistory) {
				message.setStatus(success ? ExternalInterfaceMessageHistoryStatus._ACKNOWLEDGED_ : ExternalInterfaceMessageHistoryStatus._FAILED_);
				message.setResponseTime(new Date());
				message.setResponseMessageId(request.getHeaderCommon().getMsgID());
			}
			this.messageFacade.update(messagesHistory);
			this.messageFacade.uploadToS3(XMLUtils.toString(request, MSG.class, true, true), request.getHeaderCommon().getMsgID(), false);
		}
	}
}
