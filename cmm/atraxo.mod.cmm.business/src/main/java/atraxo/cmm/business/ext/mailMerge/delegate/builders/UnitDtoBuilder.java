package atraxo.cmm.business.ext.mailMerge.delegate.builders;

import atraxo.cmm.domain.ext.mailmerge.dto.contract.UnitDto;
import atraxo.fmbas.domain.impl.uom.Unit;

/**
 * @author apetho
 */
public final class UnitDtoBuilder {

	private UnitDtoBuilder() {

	}

	public static final UnitDto buildUnit(Unit unit) {
		UnitDto dto = new UnitDto();
		if (unit != null) {
			dto.setCode(unit.getCode());
			dto.setName(unit.getName());
			dto.setIataCode(unit.getIataCode());
			dto.setFactor(unit.getFactor());
			if (unit.getUnittypeInd() != null) {
				dto.setUnitTypeIndicator(unit.getUnittypeInd().getName());
			}
		}
		return dto;
	}

}
