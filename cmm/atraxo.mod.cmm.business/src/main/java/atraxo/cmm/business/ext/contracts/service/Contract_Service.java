/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.ext.contracts.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import atraxo.ad.business.api.attachment.IAttachmentService;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.cmm.business.api.contracts.IContractChangeService;
import atraxo.cmm.business.api.contracts.IContractInvoiceService;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.contracts.IPricingBaseService;
import atraxo.cmm.business.api.ext.bid.IBidValidator;
import atraxo.cmm.business.api.ext.contracts.IPriceBuilder;
import atraxo.cmm.business.api.ext.prices.builder.IComponentPriceBuilder;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.api.shipTo.IShipToService;
import atraxo.cmm.business.ext.contracts.delegate.CompositeBd;
import atraxo.cmm.business.ext.contracts.delegate.SupplierContract_Bd;
import atraxo.cmm.business.ext.contracts.service.propagator.ContractPropagatorService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.exceptions.FuelPriceNotExistsException;
import atraxo.cmm.business.ext.exceptions.NoPriceComponentsException;
import atraxo.cmm.business.ext.exceptions.SiblingContractNotFoundException;
import atraxo.cmm.business.ext.exceptions.SupplierContractNotFoundException;
import atraxo.cmm.business.ext.prices.delegate.ContractPriceComponentConv_Bd;
import atraxo.cmm.domain.ext.contracts.CostVat;
import atraxo.cmm.domain.ext.contracts.SupplierContract;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.cmm_type.ContractScope;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.cmm_type.VatApplicability;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.ContractChange;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.financialSources.IFinancialSourcesService;
import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.timeserie.IAverageMethodService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.api.validationMessage.IValidationMessageService;
import atraxo.fmbas.business.api.vat.IVatService;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.business.ext.aop.history.Action;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.ContractChangeType;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.history.ChangeHistory;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.validationMessage.ValidationMessage;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.annotation.Reason;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.business.utils.DateUtils;

/**
 * Business extensions specific for {@link Contract} domain entity.
 */
public class Contract_Service extends atraxo.cmm.business.impl.contracts.Contract_Service implements IContractService {

	private static final String PUBLISHED = "Published";
	private static final String SIBLING_CONTRACT_NOT_FOUND = "Sibling contract not found!";
	private static final String CHANNEL_CHECK_OUT_RANGED_FUEL_EVENTS = "checkOutRangedFuelEventsChannel";

	private static final Logger LOG = LoggerFactory.getLogger(Contract_Service.class);

	private static final String SELECT_E_FROM = "select e from ";
	private static final String SUPPLIER = "supplier";
	private static final String CUSTOMER = "customer";
	private static final String STATUS = "status";
	private static final String LOCATION = "location";
	private static final String SCOPE = "scope";
	private static final String SUB_TYPE = "subType";
	private static final String IS_CONTRACT = "isContract";
	private static final String IS_BLUEPRINT = "isBlueprint";
	private static final String LIMITED_TO = "limitedTo";
	private static final String DEAL_TYPE = "dealType";
	private static final String CLIENT_ID = "clientId";
	private static final String TYPE2 = "type";
	private static final String PRODUCT = "product";
	private static final String OBJECT_ID = "objectId";
	private static final String OBJECT_TYPE = "objectType";

	private static final String TARGET_REF_ID = "targetRefid";
	private static final String TARGET_ALIAS = "targetAlias";

	private static final String CONTRACT_RESET = "Contract reset";

	@Autowired
	private IVatService vatService;
	@Autowired
	private IContractInvoiceService contractInvoiceService;
	@Autowired
	private IContractPriceCategoryService contractPriceCategoryService;
	@Autowired
	private IUnitService unitService;
	@Autowired
	private ICurrenciesService currencyService;
	@Autowired
	private IAverageMethodService averageMethodService;
	@Autowired
	private IFinancialSourcesService financialSourcesService;
	@Autowired
	private IShipToService shipToService;
	@Autowired
	private IPricingBaseService pbService;
	@Autowired
	private IChangeHistoryService historyService;
	@Autowired
	private IAttachmentService attachmentService;
	@Autowired
	private ISystemParameterService sysParamService;
	@Autowired
	private IWorkflowBpmManager workflowBpmManager;
	@Autowired
	private IWorkflowInstanceEntityService workflowEntityService;
	@Autowired
	private IPriceBuilder priceBuilderService;
	@Autowired
	private IComponentPriceBuilder componentPriceBuilder;
	@Autowired
	private IContractChangeService contractChangeService;
	@Autowired
	private ContractPropagatorService contractPropagatorService;
	@Autowired
	private CopyContractService copyContractService;
	@Autowired
	private IBidValidator bidValidator;
	@Autowired
	private IValidationMessageService validationMessageService;

	private final class ValidFromComparator implements Comparator<PricingBase> {
		@Override
		public int compare(PricingBase o1, PricingBase o2) {
			return o1.getValidFrom().compareTo(o2.getValidFrom());
		}
	}

	@Override
	@Transactional
	public void persistChanges(Contract e) throws BusinessException {
		this.checkUpdateConditions(e);
		ContractUtil.setReadOnly(e);
		this.onUpdate(e);
	}

	@Override
	protected void preInsert(Contract contract) throws BusinessException {
		super.preInsert(contract);
		ContractUtil.verifyDates(contract.getValidFrom(), contract.getValidTo());
		ContractUtil.checkRewPeriod(contract);
		this.copyContractService.assignNewShipTo(contract);
		ContractUtil.setSubsidiary(contract);
	}

	/**
	 * @param bid
	 * @throws BusinessException
	 */
	@Override
	@Transactional
	public void validateBid(Contract bid) throws BusinessException {
		if (!bid.getIsContract()) {
			this.bidValidator.clearExistingValidations(bid);
			List<ValidationMessage> messages = this.bidValidator.validateBid(bid);
			this.validationMessageService.insert(messages);
		}
	}

	@Override
	@Transactional
	public void validateBid(List<Contract> bids) throws BusinessException {
		List<ValidationMessage> messages = new ArrayList<>();
		for (Contract bid : bids) {
			if (!bid.getIsContract()) {
				this.bidValidator.clearExistingValidations(bid);
				messages.addAll(this.bidValidator.validateBid(bid));
			}
		}
		this.validationMessageService.insert(messages);
	}

	@Override
	protected void postInsert(Contract e) throws BusinessException {
		super.postInsert(e);
		this.validateBid(e);
		if (e.getCode() == null || "".equalsIgnoreCase(e.getCode())) {
			Contract c = this.findByRefid(e.getRefid());
			ContractUtil.buildCode(c);
			this.onUpdate(c);
		}
	}

	@Override
	protected void preUpdate(Contract contract) throws BusinessException {
		this.checkUpdateConditions(contract);
		this.validateBid(contract);

		if (!contract.getIsContract() && BidStatus._DRAFT_.equals(contract.getBidStatus())) {
			this.recalculateShipTo(contract);
		}

		this.sendMessage(CHANNEL_CHECK_OUT_RANGED_FUEL_EVENTS, contract);
		this.recalculatePricingBases(contract);
		Map<String, List<String>> categoryMap = CompositeBd.getAssignedCategoryNameList(contract);
		CompositeBd.updateCompositePrice(contract, categoryMap);
		ContractUtil.setReadOnly(contract);
		ContractUtil.setSubsidiary(contract);

		// check if there is a change in period for a "copy" contract, in order to "log" the change
		if (ContractChangeUtil.checkBlueprintProductChanges(contract)) {
			Contract oldContract = this.findById(contract.getId());

			Date oldValidFrom = oldContract.getValidFrom();
			Date oldValidTo = oldContract.getValidTo();
			Date newValidFrom = contract.getValidFrom();
			Date newValidTo = contract.getValidTo();

			if (!oldValidTo.equals(newValidTo) || !oldValidFrom.equals(newValidFrom)) {
				this.contractChangeService.addNewChange(contract, ContractChangeType._PERIOD_,
						ContractChangeUtil.buildMessageForPeriodChange(oldContract, contract));
			}
		}

	}

	private void checkUpdateConditions(Contract contract) throws BusinessException {
		ContractUtil.verifyDates(contract.getValidFrom(), contract.getValidTo());
		ContractUtil.checkEffectiveExpired(contract);
		ContractUtil.checkRewPeriod(contract);
		if (!ContractStatus._DRAFT_.equals(contract.getStatus())) {
			this.checkIfCanActivateUpdate(contract, true);
		}

		if (!contract.getIsContract() && CreditTerm._PREPAYMENT_.equals(contract.getCreditTerms())) {
			ContractUtil.checkPrepayMandatoryFields(contract);
		}
		String canUpdateContract = this.contractInvoiceService.canModifyDates(contract);
		if (!StringUtils.isEmpty(canUpdateContract)) {
			throw new BusinessException(CmmErrorCode.UPDATE_CONTRACT_PERIOD,
					String.format(CmmErrorCode.UPDATE_CONTRACT_PERIOD.getErrMsg(), canUpdateContract));
		}
	}

	private void recalculateShipTo(Contract contract) throws BusinessException {
		Contract oldContract = this.findById(contract.getId());

		if (!contract.getSettlementUnit().equals(oldContract.getSettlementUnit())) {
			Double density = Double.parseDouble(this.sysParamService.getDensity());

			BigDecimal bidOfferedVolume = this.unitService.convert(oldContract.getSettlementUnit(), contract.getSettlementUnit(),
					contract.getOfferedVolume(), density);
			contract.setOfferedVolume(bidOfferedVolume);

			if (!contract.getBidHasTotalVolume()) {
				for (ShipTo shipTo : contract.getShipTo()) {
					// make necesary conversions to shipTo required volume
					if (shipTo.getTenderBidVolume() != null) {
						BigDecimal requiredVolume = this.unitService.convert(oldContract.getSettlementUnit(), contract.getSettlementUnit(),
								shipTo.getTenderBidVolume(), density);

						shipTo.setTenderBidVolume(requiredVolume);
					}

					// make necesary conversions to shipTo offered volume
					if (shipTo.getOfferedVolume() != null) {
						BigDecimal offeredVolume = this.unitService.convert(oldContract.getSettlementUnit(), contract.getSettlementUnit(),
								shipTo.getOfferedVolume(), density);

						shipTo.setOfferedVolume(offeredVolume);
					}
				}
			}
		}
	}

	private void setTransientFields(Collection<PricingBase> pbList) {
		for (PricingBase pb : pbList) {
			ContractPriceCategory category = !pb.getContractPriceCategories().isEmpty() ? pb.getContractPriceCategories().iterator().next() : null;
			if (category != null) {
				pb.setDefauftPC(category.getDefaultPriceCtgy());
				pb.setInitialPrice(category.getInitialPrice());
				pb.setInitialCurrId(category.getInitialCurrId());
				pb.setInitialUnitId(category.getInitialUnitId());
				pb.setQuantityType(category.getQuantityType());
				pb.setContinous(category.getContinous());
				pb.setRestriction(category.getRestriction());
				pb.setFinancialSourceId(category.getFinancialSource().getId());
				pb.setAverageMethodId(category.getAverageMethod().getId());
				pb.setExchangeRateOffset(category.getExchangeRateOffset());
				pb.setVat(category.getVat());
			}
		}
	}

	@Override
	protected void postUpdate(Contract e) throws BusinessException {
		super.postUpdate(e);
		ContractPriceComponentConv_Bd convBd = this.getBusinessDelegate(ContractPriceComponentConv_Bd.class);
		convBd.updateConvertedPriceComponent(e);
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		List<Contract> contracts = this.findByIds(ids);
		for (Contract c : contracts) {
			// check concurency for contracts
			if (!c.getIsBlueprint() && (ContractStatus._EFFECTIVE_.equals(c.getStatus()) || ContractStatus._EXPIRED_.equals(c.getStatus()))) {
				throw new BusinessException(CmmErrorCode.EFFECTIVE_EXPIRED_CONTRACTS, CmmErrorCode.EFFECTIVE_EXPIRED_CONTRACTS.getErrMsg());
			}
			this.bidValidator.clearExistingValidations(c);

			// check concurency for bids
			if (!c.getIsContract() && !this.canDeleteBid(c)) {
				throw new BusinessException(CmmErrorCode.APPROVED_SUBMITTED_BIDS, CmmErrorCode.APPROVED_SUBMITTED_BIDS.getErrMsg());
			}
		}

		for (Object id : ids) {
			Contract contract = this.findById(id);
			this.pbService.delete(new ArrayList<PricingBase>(contract.getPricingBases()));
			this.contractPriceCategoryService.delete(new ArrayList<ContractPriceCategory>(contract.getPriceCategories()));
			this.shipToService.delete(new ArrayList<ShipTo>(contract.getShipTo()));
			if (DealType._BUY_.equals(contract.getDealType())) {
				this.deleteGeneratedContracts(id);
			}
		}
	}

	private void deleteGeneratedContracts(Object id) throws BusinessException {
		StringBuilder sb = new StringBuilder();
		sb.append(Contract_Service.SELECT_E_FROM).append(Contract.class.getSimpleName()).append(" e ");
		sb.append("where  e.clientId = :clientId and e.resaleRef.id =:resaleId ");
		List<Contract> contracts = this.getEntityManager().createQuery(sb.toString(), Contract.class).setParameter("resaleId", id)
				.setParameter(Contract_Service.CLIENT_ID, Session.user.get().getClientId()).getResultList();
		if (!CollectionUtils.isEmpty(contracts)) {
			for (Contract contract : contracts) {
				contract.setResaleRef(null);
			}
			this.update(contracts);
		}
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	@History(value = "Captured", type = Contract.class)
	public void insert(List<Contract> list) throws BusinessException {
		super.insert(list);
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void update(List<Contract> list) throws BusinessException {
		super.update(list);
	}

	@Override
	public List<SupplierContract> getSupplierContracts(Locations location, Date dateFrom, Date dateTo, Currencies currency, Unit unit,
			AverageMethod avgMthd, FinancialSources finSrc, MasterAgreementsPeriod offset) throws BusinessException {
		StringBuilder sb = new StringBuilder();
		sb.append(Contract_Service.SELECT_E_FROM).append(Contract.class.getSimpleName()).append(" e ");
		sb.append("where  e.clientId = :clientId and (e.status = :status1 or e.status = :status2) ");
		sb.append("and e.dealType = :dealType ");
		sb.append("and e.location = :location ");
		sb.append("and e.validFrom <= :dateFrom ");
		sb.append("and e.validTo >= :dateTo ");
		List<Contract> contracts = this.getEntityManager().createQuery(sb.toString(), Contract.class).setParameter("status1", ContractStatus._ACTIVE_)
				.setParameter("status2", ContractStatus._EFFECTIVE_).setParameter(LOCATION, location).setParameter("dateFrom", dateFrom)
				.setParameter("dateTo", dateTo).setParameter(DEAL_TYPE, DealType._BUY_).setParameter(CLIENT_ID, Session.user.get().getClientId())
				.getResultList();

		List<SupplierContract> supplierContracts = new ArrayList<>();
		SupplierContract_Bd bd = this.getBusinessDelegate(SupplierContract_Bd.class);
		for (Contract contract : contracts) {
			try {
				SupplierContract supplierContract = bd.buildSupplierContract(dateFrom, dateTo, contract, unit, currency, avgMthd, finSrc, offset);
				if (supplierContract.getBasePrice().compareTo(BigDecimal.ZERO) > 0) {
					supplierContracts.add(supplierContract);
				}
			} catch (SupplierContractNotFoundException e) {
				Contract_Service.LOG.info("Supplier not found!.", e);
			}
		}
		return supplierContracts;
	}

	@Override
	public SupplierContract getSupplierContract(Integer contractId, Date dateFrom, Date dateTo, Integer currencyId, Integer unitId, Integer avgMthdId,
			Integer finSrcId, MasterAgreementsPeriod offset) throws BusinessException {
		Contract contract = this.findById(contractId);
		Unit unit = this.unitService.findById(unitId);
		Currencies currency = this.currencyService.findById(currencyId);
		AverageMethod avgMthd = this.averageMethodService.findById(avgMthdId);
		FinancialSources financialSource = this.financialSourcesService.findById(finSrcId);
		SupplierContract_Bd bd = this.getBusinessDelegate(SupplierContract_Bd.class);
		return bd.buildSupplierContract(dateFrom, dateTo, contract, unit, currency, avgMthd, financialSource, offset);
	}

	@Override
	public BigDecimal getPrice(Integer id, String unitCode, String currencyCode, Date date) throws BusinessException {
		CostVat price = this.getPriceVat(id, unitCode, currencyCode, date, null);
		return price.getCost();
	}

	@Override
	public Boolean isUpdated(Integer id, String unitCode, String currencyCode) throws BusinessException {
		Calendar calendar = Calendar.getInstance();
		BigDecimal todayPrice = this.getPrice(id, unitCode, currencyCode, calendar.getTime());
		calendar.add(Calendar.DATE, -1);
		BigDecimal yesterdayPrice = this.getPrice(id, unitCode, currencyCode, calendar.getTime());
		return !todayPrice.equals(yesterdayPrice);
	}

	@Transactional
	@Override
	@History(value = "Set Active", type = Contract.class)
	public void setActive(Contract contract) throws BusinessException {
		if (!ContractStatus._DRAFT_.equals(contract.getStatus())) {
			return;
		}
		ContractUtil.verifyCompanies(contract);
		this.checkIfCanActivateUpdate(contract, false);
		Contract sContract = null;
		try {
			sContract = this.findSiblingContract(contract);
		} catch (SiblingContractNotFoundException e) {
			Contract_Service.LOG.info(Contract_Service.SIBLING_CONTRACT_NOT_FOUND, e);
		}
		if (sContract != null) {
			this.setActive(sContract);
		}
		contract.setStatus(ContractStatus._ACTIVE_);
		this.onUpdate(contract);
	}

	@Override
	public List<Contract> findBySupplierStatusLocationScopeType(Suppliers supplier, ContractStatus status, Locations location, ContractScope scope,
			ContractType type) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put(Contract_Service.SUPPLIER, supplier);
		params.put(Contract_Service.STATUS, status);
		params.put(Contract_Service.LOCATION, location);
		params.put(Contract_Service.SCOPE, scope);
		params.put(Contract_Service.TYPE2, type);
		return this.findEntitiesByAttributes(params);
	}

	@Override
	public List<Contract> findByDateCustomerSupplierLocationFlightType(Date date, Suppliers supplier, Locations location, String type,
			String dealType) throws BusinessException {
		String hql = SELECT_E_FROM + Contract.class.getSimpleName()
				+ " e where e.clientId =:clientId and (e.status =:statusEffective or e.status=:statusExpired) and e.supplier =:supplier and e.location =:location and e.limitedTo = :type and e.dealType =:dealType and :date BETWEEN e.validFrom AND e.validTo";

		return this.getEntityManager().createQuery(hql, Contract.class).setParameter("date", date)
				.setParameter("statusEffective", ContractStatus._EFFECTIVE_).setParameter("statusExpired", ContractStatus._EXPIRED_)
				.setParameter(Contract_Service.SUPPLIER, supplier).setParameter(Contract_Service.LOCATION, location)
				.setParameter(Contract_Service.TYPE2, type).setParameter(Contract_Service.CLIENT_ID, supplier.getClientId())
				.setParameter(Contract_Service.DEAL_TYPE, dealType).getResultList();

	}

	@Override
	public List<Contract> findByHolderLocation(Customer customer, Locations location, Boolean isContract) throws BusinessException {
		StringBuilder hql = new StringBuilder();
		hql.append(Contract_Service.SELECT_E_FROM + Contract.class.getSimpleName());
		hql.append(" e where e.clientId = :clientId and e.isContract = :isContract and e.location = :location and e.dealType != :dealType ");

		if (isContract) {
			hql.append("and e.status != :status");
		} else {
			hql.append("and e.bidStatus != :status");
		}

		hql.append(" and e.customer = :customer and e.subsidiaryId in :subsidiaryIds order by e.createdAt desc");

		TypedQuery<Contract> query = this.getEntityManager().createQuery(hql.toString(), Contract.class);
		query.setParameter(Contract_Service.CUSTOMER, customer);
		query.setParameter(Contract_Service.LOCATION, location);
		query.setParameter(Contract_Service.IS_CONTRACT, isContract);
		query.setParameter(Contract_Service.DEAL_TYPE, DealType._BUY_);

		if (isContract) {
			query.setParameter(Contract_Service.STATUS, ContractStatus._DRAFT_);
		} else {
			query.setParameter(Contract_Service.STATUS, BidStatus._DRAFT_);
		}

		query.setParameter(Contract_Service.CLIENT_ID, Session.user.get().getClientId());
		query.setParameter("subsidiaryIds", Session.user.get().getProfile().getOrganizationIds());

		return query.getResultList();
	}

	@Override
	public List<Contract> findByLocation(Locations location, Boolean isContract) throws BusinessException {
		StringBuilder hql = new StringBuilder();
		hql.append(Contract_Service.SELECT_E_FROM + Contract.class.getSimpleName());
		hql.append(" e where e.clientId =:clientId and e.isContract = :isContract and e.location =:location and e.dealType != :dealType ");

		if (isContract) {
			hql.append("and e.status != :status");
		} else {
			hql.append("and e.bidStatus != :status");
		}

		hql.append(" and e.subsidiaryId in :subsidiaryIds order by e.createdAt desc");

		TypedQuery<Contract> query = this.getEntityManager().createQuery(hql.toString(), Contract.class);
		query.setParameter(Contract_Service.LOCATION, location);
		query.setParameter(Contract_Service.IS_CONTRACT, isContract);
		query.setParameter(Contract_Service.DEAL_TYPE, DealType._BUY_);

		if (isContract) {
			query.setParameter(Contract_Service.STATUS, ContractStatus._DRAFT_);
		} else {
			query.setParameter(Contract_Service.STATUS, BidStatus._DRAFT_);
		}

		query.setParameter(Contract_Service.CLIENT_ID, Session.user.get().getClientId());
		query.setParameter("subsidiaryIds", Session.user.get().getProfile().getOrganizationIds());

		return query.getResultList();
	}

	@Override
	public void updateStatus(StringBuilder contractInfo) throws BusinessException {
		this.updateStatus(ContractStatus._ACTIVE_, ContractStatus._EFFECTIVE_, null, contractInfo);
		this.updateStatus(ContractStatus._EFFECTIVE_, ContractStatus._EXPIRED_, null, contractInfo);
		this.updateStatus(ContractStatus._EXPIRED_, ContractStatus._EFFECTIVE_, null, contractInfo);
	}

	@Override
	@Transactional
	public void updateStatus(Contract contract) throws BusinessException {
		Contract sContract = null;
		try {
			sContract = this.findSiblingContract(contract);
		} catch (SiblingContractNotFoundException e) {
			Contract_Service.LOG.info(Contract_Service.SIBLING_CONTRACT_NOT_FOUND, e);
		}
		if (sContract != null) {
			this.updateStatus(sContract);
		}

		this.updateStatus(ContractStatus._ACTIVE_, ContractStatus._EFFECTIVE_, Arrays.asList(contract), null);
		contract = this.findById(contract.getId());
		this.updateStatus(ContractStatus._EFFECTIVE_, ContractStatus._EXPIRED_, Arrays.asList(contract), null);
	}

	private void updateStatus(ContractStatus from, ContractStatus to, List<Contract> contracts, StringBuilder contractInfo) throws BusinessException {
		if (contracts == null) {
			Map<String, Object> params = new HashMap<>();

			params.put(Contract_Service.STATUS, from);
			contracts = this.findEntitiesByAttributes(params);
		}
		List<Contract> updList = new ArrayList<>();
		Date today = DateUtils.getTodayWithoutTime();
		for (Contract contract : contracts) {
			String fromStatus = contract.getStatus().getName();
			switch (to) {
			case _EFFECTIVE_:
				if (contract.getValidFrom().compareTo(today) <= 0 && contract.getValidTo().compareTo(today) >= 0
						&& !to.equals(contract.getStatus())) {
					contract.setStatus(to);
					updList.add(contract);
				}
				break;
			case _EXPIRED_:
				if (contract.getValidTo().compareTo(today) < 0 && !to.equals(contract.getStatus())) {
					contract.setStatus(to);
					updList.add(contract);
				}
				break;
			default:
				break;
			}
			if (contractInfo != null && fromStatus != contract.getStatus().toString()) {
				contractInfo.append(contract.getCode() + " : " + fromStatus.substring(0, 2).toUpperCase() + " -> "
						+ contract.getStatus().getName().substring(0, 2).toUpperCase() + System.lineSeparator());
			}
		}
		this.updateWithoutBusinessLogic(updList);
		this.saveInHistory(updList, today);
	}

	private void saveInHistory(List<Contract> updList, Date today) throws BusinessException {
		for (Contract c : updList) {
			if (ContractStatus._EFFECTIVE_.equals(c.getStatus()) && c.getValidTo().compareTo(today) >= 0
					|| ContractStatus._EXPIRED_.equals(c.getStatus())) {
				this.generateSaveToHistory(c, "Update Status", c.getStatus().getName());
			}
		}
	}

	private void generateSaveToHistory(Contract contract, String objectValue, String remark) throws BusinessException {
		ChangeHistory ch = new ChangeHistory();
		ch.setObjectId(contract.getId());
		ch.setObjectType(Contract.class.getSimpleName());
		ch.setObjectValue(objectValue);
		ch.setRemarks(remark);
		this.historyService.insert(ch);
	}

	@Override
	@Transactional
	@History(value = "Reset", type = Contract.class)
	public void reset(Contract contract, @Reason String remark) throws BusinessException {
		Contract sContract = null;
		try {
			sContract = this.findSiblingContract(contract);
		} catch (SiblingContractNotFoundException e) {
			Contract_Service.LOG.info(Contract_Service.SIBLING_CONTRACT_NOT_FOUND, e);
		}
		if (sContract != null) {
			this.reset(sContract, remark);
		}
		Date validTo = contract.getValidTo();
		try {
			contract.setValidTo(contract.getValidFrom());
			this.sendMessage(CHANNEL_CHECK_OUT_RANGED_FUEL_EVENTS, contract);
		} finally {
			contract.setValidTo(validTo);
		}
		if (this.contractInvoiceService.canModifyStatus(contract)) {
			contract.setStatus(ContractStatus._DRAFT_);
			contract.setBidApprovalStatus(BidApprovalStatus._NEW_);
			contract.setPeriodApprovalStatus(BidApprovalStatus._NEW_);
			this.onUpdate(contract);
			if (DealType._BUY_.equals(contract.getDealType())) {
				this.sendMessage("resetSupplierContract", contract);
			} else if (DealType._SELL_.equals(contract.getDealType())) {
				this.sendMessage("removeSaleContract", contract);
			}
			this.contractInvoiceService.removeLink(contract);
		} else {
			throw new BusinessException(CmmErrorCode.RESET_CONTRACTS_ERROR, CmmErrorCode.RESET_CONTRACTS_ERROR.getErrMsg());
		}
		this.terminateAllWorkflows(contract);
	}

	/**
	 * Recalculate price bases period if the contract period was modified.
	 *
	 * @param c - {@link Contract}
	 * @throws BusinessException
	 */
	private void recalculatePricingBases(Contract c) throws BusinessException {
		List<PricingBase> list = new ArrayList<>(c.getPricingBases());
		if (CollectionUtils.isEmpty(list)) {
			return;
		}
		this.setTransientFields(list);
		Collections.sort(list, new ValidFromComparator());
		List<PricingBase> productPbList = this.pbService.buildProductPbList(c);

		productPbList.stream().sorted(Comparator.nullsLast((o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()))).findFirst();
		if (!productPbList.isEmpty()) {
			PricingBase first = productPbList.get(0);
			if (first.getValidFrom().after(c.getValidFrom())) {
				// extends left margin to validFrom
				first.setValidFrom(c.getValidFrom());
				this.pbService.updateWithoutPersist(first);
			}
			PricingBase last = productPbList.get(productPbList.size() - 1);
			if (last.getValidTo().before(c.getValidTo())) {
				// extends right margin to validTo
				last.setValidTo(c.getValidTo());
				this.pbService.updateWithoutPersist(last);
			}
		}
		for (PricingBase pb : list) {
			if (!this.pbService.isProduct(pb)) {
				pb.setValidFrom(c.getValidFrom());
				pb.setValidTo(c.getValidTo());
				this.pbService.updateWithoutPersist(pb);
				continue;
			}
			if (pb.getValidTo().before(c.getValidFrom())) {
				c.getPricingBases().remove(pb);
				pb.setContract(null);

			} else if (pb.getValidFrom().before(c.getValidFrom())) {
				pb.setValidFrom(c.getValidFrom());
				this.pbService.updateWithoutPersist(pb);

			} else if (pb.getValidFrom().after(c.getValidTo())) {
				c.getPricingBases().remove(pb);
				pb.setContract(null);

			} else if (pb.getValidTo().after(c.getValidTo())) {
				pb.setValidTo(c.getValidTo());
				this.pbService.updateWithoutPersist(pb);
			}
		}
	}

	@Override
	public void checkIfCanActivateUpdate(Contract contract, Boolean update) throws BusinessException {

		boolean restrictBlacklisted = this.sysParamService.getRestrictBlacklisted();
		boolean sysPurchaseAproval = this.sysParamService.getPurchaseContractApprovalWorkflow();
		boolean sysSaleApproval = this.sysParamService.getSellContractApprovalWorkflow();
		if (DealType._SELL_.equals(contract.getDealType())) {
			if (!contract.getReadOnly() && !update && sysSaleApproval && !BidApprovalStatus._APPROVED_.equals(contract.getBidApprovalStatus())) {
				throw new BusinessException(CmmErrorCode.THE_CONTRACT_MUST_BE_APPROVED, CmmErrorCode.THE_CONTRACT_MUST_BE_APPROVED.getErrMsg());
			}
			if (restrictBlacklisted) {
				if (CustomerStatus._BLOCKED_.equals(contract.getCustomer().getStatus())) {
					throw new BusinessException(CmmErrorCode.SELL_CONTACT_CUSTOMER_MUST_BE_ACTIVE,
							CmmErrorCode.SELL_CONTACT_CUSTOMER_MUST_BE_ACTIVE.getErrMsg());
				}
			}
		} else {
			if (!update && sysPurchaseAproval && !BidApprovalStatus._APPROVED_.equals(contract.getBidApprovalStatus())) {
				throw new BusinessException(CmmErrorCode.THE_CONTRACT_MUST_BE_APPROVED, CmmErrorCode.THE_CONTRACT_MUST_BE_APPROVED.getErrMsg());
			}
		}
		List<Contract> contracts = this.findByBusinessKey(contract);
		if (!contracts.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			for (Contract c : contracts) {
				sb.append(c.getCode() + " ");
			}
			Contract c = contracts.get(0);
			StringBuilder sbShipTo = new StringBuilder();
			if (c.getShipTo() != null) {
				for (ShipTo st : c.getShipTo()) {
					sbShipTo.append(sbShipTo.length() == 0 ? st.getCustomer().getCode() : ", " + st.getCustomer().getCode());
				}
			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
			String company = c.getCustomer() == null ? c.getSupplier().getCode() : c.getCustomer().getCode();
			if (update) {
				throw new BusinessException(CmmErrorCode.CONTRACT_ALREADY_EXISTS_WITH_DETAILS_UPDATE,
						String.format(CmmErrorCode.CONTRACT_ALREADY_EXISTS_WITH_DETAILS_UPDATE.getErrMsg(), c.getDealType().getName(), c.getCode(),
								c.getLocation().getCode(),
								new StringBuilder().append(sdf.format(c.getValidFrom())).append("-").append(sdf.format(c.getValidTo())).toString(),
								c.getHolder().getCode(), company, sbShipTo.toString(), c.getType().getName(), c.getSubType().getName(),
								c.getScope().getName(), c.getLimitedTo().getName(), c.getProduct().getName()));
			} else {
				throw new BusinessException(CmmErrorCode.CONTRACT_ALREADY_EXISTS_WITH_DETAILS,
						String.format(CmmErrorCode.CONTRACT_ALREADY_EXISTS_WITH_DETAILS.getErrMsg(), c.getDealType().getName(), c.getCode(),
								c.getLocation().getCode(),
								new StringBuilder().append(sdf.format(c.getValidFrom())).append("-").append(sdf.format(c.getValidTo())).toString(),
								c.getHolder().getCode(), company, sbShipTo.toString(), c.getType().getName(), c.getSubType().getName(),
								c.getScope().getName(), c.getLimitedTo().getName(), c.getProduct().getName()));
			}
		}
	}

	@Override
	public void checkIfCanSendForAppoval(Contract contract) throws BusinessException {

		List<Contract> contracts = this.findByBusinessKey(contract);
		if (!contracts.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			for (Contract c : contracts) {
				sb.append(c.getCode() + " ");
			}
			Contract c = contracts.get(0);
			StringBuilder sbShipTo = new StringBuilder();
			if (c.getShipTo() != null) {
				for (ShipTo st : c.getShipTo()) {
					sbShipTo.append(sbShipTo.length() == 0 ? st.getCustomer().getCode() : ", " + st.getCustomer().getCode());
				}
			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
			String company = c.getCustomer() == null ? c.getSupplier().getCode() : c.getCustomer().getCode();
			throw new BusinessException(CmmErrorCode.CONTRACT_ALREADY_EXISTS_IN_APPROVAL,
					String.format(CmmErrorCode.CONTRACT_ALREADY_EXISTS_IN_APPROVAL.getErrMsg(), c.getDealType().getName(), c.getCode(),
							c.getLocation().getCode(),
							new StringBuilder().append(sdf.format(c.getValidFrom())).append("-").append(sdf.format(c.getValidTo())).toString(),
							c.getHolder().getCode(), company, sbShipTo.toString(), c.getType().getName(), c.getSubType().getName(),
							c.getScope().getName(), c.getLimitedTo().getName(), c.getProduct().getName()));
		}
	}

	private List<Contract> findByBusinessKey(Contract contract) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put(Contract_Service.LOCATION, contract.getLocation());
		params.put(Contract_Service.DEAL_TYPE, contract.getDealType());
		params.put(Contract_Service.LIMITED_TO, contract.getLimitedTo());
		params.put(Contract_Service.TYPE2, contract.getType());
		params.put(Contract_Service.SUB_TYPE, contract.getSubType());
		params.put(Contract_Service.SCOPE, contract.getScope());
		params.put(Contract_Service.PRODUCT, contract.getProduct());
		params.put(Contract_Service.IS_BLUEPRINT, false);
		List<Contract> contracts = this.findEntitiesByAttributes(params);
		OUTER: for (Iterator<Contract> iterator = contracts.iterator(); iterator.hasNext();) {
			Contract c = iterator.next();
			if (c.getCode().equals(contract.getCode())) {
				iterator.remove();
				continue;
			}
			if (c.getStatus().equals(ContractStatus._DRAFT_)) {
				iterator.remove();
				continue;
			}
			if (contract.getBlueprintOriginalContractReference() != null && contract.getBlueprintOriginalContractReference().equals(c.getId())) {
				iterator.remove();
				continue;
			}
			if (!contract.getHolder().equals(c.getHolder())) {
				iterator.remove();
				continue;
			}
			Date from = c.getValidFrom().before(contract.getValidFrom()) ? contract.getValidFrom() : c.getValidFrom();
			Date to = c.getValidTo().before(contract.getValidTo()) ? c.getValidTo() : contract.getValidTo();
			switch (c.getDealType()) {
			case _BUY_:
				if (c.getSupplier().equals(contract.getSupplier()) && from.before(to) && ContractUtil.isCrossShipTosPeriod(contract, c)) {
					continue OUTER;
				}
				break;
			case _SELL_:
				if (c.getCustomer().equals(contract.getCustomer()) && from.before(to) && ContractUtil.isCrossShipTosPeriod(contract, c)) {
					continue OUTER;
				}
				break;
			default:
				// do nothings
			}
			iterator.remove();
		}
		return contracts;
	}

	@Override
	public List<Contract> findContractByDeliveryDateCustomerLocation(Integer customerId, Date deliveryDate, DealType dealType, Locations location)
			throws BusinessException {
		String hql = Contract_Service.SELECT_E_FROM + Contract.class.getSimpleName()
				+ " e where e.clientId =:clientId and e.customer.id = :customerId and e.dealType =:dealType and e.location.id =:locationId";
		List<Contract> contracts = this.getEntityManager().createQuery(hql, Contract.class)
				.setParameter(Contract_Service.CLIENT_ID, Session.user.get().getClientId()).setParameter("customerId", customerId)
				.setParameter(Contract_Service.DEAL_TYPE, dealType).setParameter("locationId", location.getId()).getResultList();
		List<Contract> retList = new ArrayList<>();
		for (Contract contract : contracts) {
			if (deliveryDate.compareTo(contract.getValidFrom()) > -1 && deliveryDate.compareTo(contract.getValidTo()) < 1
					&& (contract.getStatus().equals(ContractStatus._EFFECTIVE_) || contract.getStatus().equals(ContractStatus._EXPIRED_))) {
				retList.add(contract);
			}
		}
		return retList;
	}

	@Override
	public CostVat getPriceVat(Integer id, String unitCode, String currencyCode, Date date, FlightTypeIndicator eventType) throws BusinessException {
		Contract contract = this.findById(id);
		BigDecimal price = BigDecimal.ZERO;
		BigDecimal vatSum = BigDecimal.ZERO;
		Date internDate = date;
		if (date.before(contract.getValidFrom())) {
			internDate = contract.getValidFrom();
		}
		if (date.after(contract.getValidTo())) {
			internDate = contract.getValidTo();
		}
		Map<String, BigDecimal> usedPriceCategories = new HashMap<>();
		for (ContractPriceCategory priceCategory : contract.getPriceCategories()) {
			try {
				PriceInd priceCategoryInd = priceCategory.getPriceCategory().getPricePer();
				if (!PriceInd._EVENT_.equals(priceCategoryInd)
						&& !CalculateIndicator._CALCULATE_ONLY_.equals(priceCategory.getCalculateIndicator())) {
					boolean canCalculateVatSum = this.canCalculateVatSum(eventType, contract, priceCategory.getVat(), false);
					BigDecimal convertedPrice = BigDecimal.ZERO;
					try {
						convertedPrice = this.contractPriceCategoryService.getPriceInCurrencyUnit(priceCategory, unitCode, currencyCode, internDate);
					} catch (NoPriceComponentsException e) {
						LOG.info("Price component not found for contract " + contract.getCode() + ", for contract price category "
								+ priceCategory.getName() + " on " + internDate, e);
						if (PriceType._PRODUCT_.equals(priceCategory.getPriceCategory().getType())
								&& internDate.after(priceCategory.getPricingBases().getValidFrom())
								&& internDate.before(priceCategory.getPricingBases().getValidFrom())) {
							throw new FuelPriceNotExistsException(PriceType._PRODUCT_.getName(), contract.getCode(), priceCategory.getName(),
									internDate);
						}
						continue;
					}
					if (!usedPriceCategories.keySet().contains(priceCategory.getPriceCategory().getName())) {
						if (canCalculateVatSum) {
							vatSum = vatSum.add(convertedPrice);
						}
						price = price.add(convertedPrice);
						usedPriceCategories.put(priceCategory.getPriceCategory().getName(), convertedPrice);
					} else if (priceCategory.getDefaultPriceCtgy()) {
						if (canCalculateVatSum) {
							vatSum = vatSum.subtract(usedPriceCategories.get(priceCategory.getPriceCategory().getName())).add(convertedPrice);
						}
						price = price.subtract(usedPriceCategories.get(priceCategory.getPriceCategory().getName())).add(convertedPrice);
						usedPriceCategories.put(priceCategory.getPriceCategory().getName(), convertedPrice);
					}
				}
			} catch (NoPriceComponentsException e) {
				Contract_Service.LOG.warn("Price component not found for contract " + contract.getCode() + " on " + internDate, e);
			}
		}
		if (this.canCalculateVatSum(eventType, contract, contract.getVat(), true)) {
			vatSum = price;
		}
		return new CostVat(price, this.calculateVat(contract.getLocation(), internDate, vatSum));
	}

	private boolean canCalculateVatSum(FlightTypeIndicator eventType, Contract contract, VatApplicability vat, boolean isContract) {
		if (eventType == null) {
			return false;
		}
		boolean contractVatNot = !isContract && contract.getVat().equals(VatApplicability._NOT_APPLICABLE_)
				|| isContract && !contract.getVat().equals(VatApplicability._NOT_APPLICABLE_);
		boolean allEvents = vat.equals(VatApplicability._ALL_EVENTS_) && FlightTypeIndicator._UNSPECIFIED_.equals(eventType);
		boolean domestic = FlightTypeIndicator._DOMESTIC_.equals(eventType)
				&& (vat.equals(VatApplicability._ALL_EVENTS_) || vat.equals(VatApplicability._DOMESTIC_EVENTS_));
		boolean international = FlightTypeIndicator._INTERNATIONAL_.equals(eventType)
				&& (vat.equals(VatApplicability._ALL_EVENTS_) || vat.equals(VatApplicability._INTERNATIONAL_EVENTS_));
		return contractVatNot && (allEvents || domestic || international);
	}

	private BigDecimal calculateVat(Locations location, Date date, BigDecimal value) throws BusinessException {
		BigDecimal vatNum = this.vatService.getVat(date, location.getCountry());
		return value.multiply(vatNum.divide(BigDecimal.valueOf(100), MathContext.DECIMAL64), MathContext.DECIMAL64);
	}

	@Override
	@Transactional
	public void insertWithoutBusinessLogic(Contract contract) throws BusinessException {
		this.onInsert(contract);
	}

	@Override
	public Contract generateResaleContract(Contract contract, Customer customer) throws BusinessException {
		return this.copyContractService.generateContract(contract, customer);
	}

	@Override
	@Transactional
	public Contract generateInternalResaleContract(Contract contract) throws BusinessException {
		Contract newContract = this.copyContractService.generateInternalResale(contract);
		this.generateSaveToHistory(newContract, "Generated internal resale contract", "");
		return newContract;
	}

	@Override
	public Integer copyAndSaveSaleContract(Contract contract, Customer customer) throws BusinessException {
		Contract newContract = this.copyContractService.copySaleContract(contract, customer);
		return this.copyContractService.saveNewContract(customer, DealType._SELL_, newContract).getId();
	}

	@Override
	public Integer copyAndSaveBuyContract(Contract contract, Suppliers supplier) throws BusinessException {
		Contract newContract = this.copyContractService.copyBuyContract(contract, supplier);
		return this.copyContractService.saveNewContract(supplier, DealType._BUY_, newContract).getId();
	}

	@Override
	public List<Contract> findSaleContracts(Integer holderId, Customer customer, FlightTypeIndicator flightType, ContractType type,
			Locations location, Date date, Product productType) throws BusinessException {
		Customer holder = this.findById(holderId, Customer.class);
		Map<String, Object> params = new HashMap<>();
		params.put(Contract_Service.DEAL_TYPE, DealType._SELL_);
		params.put("holder", holder);
		params.put(Contract_Service.TYPE2, type);
		params.put(Contract_Service.LOCATION, location);
		params.put("product", productType);
		params.put("isBlueprint", false);
		List<Contract> list = this.findEntitiesByAttributes(params);
		ContractUtil.filterContracts(customer, date, list, flightType);
		return list;
	}

	@Override
	public List<Contract> findSaleContracts(FlightTypeIndicator flightType, ContractType type, Locations location, Date date, Product productType)
			throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put(Contract_Service.DEAL_TYPE, DealType._SELL_);
		params.put(Contract_Service.TYPE2, type);
		params.put(Contract_Service.LOCATION, location);
		params.put(Contract_Service.PRODUCT, productType);
		params.put("isBlueprint", false);
		List<Contract> list = this.findEntitiesByAttributes(params);
		ContractUtil.filterContracts(null, date, list, flightType);
		return list;
	}

	@Override
	public List<Contract> findPurchaseContracts(Suppliers supplier, FlightTypeIndicator flightType, Locations location, Date date,
			Product productType) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put(Contract_Service.DEAL_TYPE, DealType._BUY_);
		params.put(Contract_Service.SUPPLIER, supplier);
		params.put(Contract_Service.LOCATION, location);
		params.put(Contract_Service.PRODUCT, productType);
		List<Contract> list = this.findEntitiesByAttributes(params);
		ContractUtil.filterContracts(null, date, list, flightType);
		return list;
	}

	@Override
	@Transactional
	public void updateWithoutBusinessLogic(List<Contract> contracts) throws BusinessException {
		for (Contract contract : contracts) {
			this.onUpdate(contract);
		}
	}

	@Override
	public List<Contract> findAllEffectiveSalesContracts() throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put(Contract_Service.IS_CONTRACT, Boolean.TRUE);
		params.put(Contract_Service.IS_BLUEPRINT, Boolean.FALSE);
		params.put(Contract_Service.STATUS, ContractStatus._EFFECTIVE_);
		params.put(Contract_Service.DEAL_TYPE, DealType._SELL_);
		return this.findEntitiesByAttributes(params);
	}

	@Override
	@Transactional
	@History(type = Contract.class, value = "Cancelled")
	public void cancelTenderBid(List<Contract> bids, @Reason String reason) throws BusinessException {
		for (Contract bid : bids) {
			bid.setBidStatus(BidStatus._CANCELLED_);
		}
		this.updateWithoutBusinessLogic(bids);
	}

	@Override
	@Transactional
	@History(type = Contract.class, value = "Award accepted")
	public void acceptAwardTenderBid(Contract bid) throws BusinessException {
		bid.setBidStatus(BidStatus._AWARD_ACCEPTED_);
		this.update(bid);
	}

	@Override
	@Transactional
	@History(type = Contract.class, value = "Award declined")
	public void declineAwardTenderBid(Contract bid, @Reason String reason) throws BusinessException {
		bid.setBidStatus(BidStatus._AWARD_DECLINED_);
		this.update(bid);
	}

	@Override
	@Transactional
	@History(type = Contract.class, value = "Generate sale contract")
	public Integer generateSaleContractFromBid(Contract bid) throws BusinessException {
		Integer newContractId = this.copyContractService.genearetSaleContractFromBid(bid).getId();
		ChangeHistory history = new ChangeHistory();
		history.setObjectId(newContractId);
		history.setObjectType(Contract.class.getSimpleName());
		history.setObjectValue("Created from bid");
		this.historyService.insert(history);
		return newContractId;
	}

	@Override
	@Transactional
	@History(type = Contract.class)
	public void submitForApproval(Contract contract, BidApprovalStatus status, @Action String action, @Reason String reason)
			throws BusinessException {
		Contract sContract = null;
		try {
			sContract = this.findSiblingContract(contract);
		} catch (SiblingContractNotFoundException e) {
			Contract_Service.LOG.info(Contract_Service.SIBLING_CONTRACT_NOT_FOUND, e);
		}
		if (sContract != null) {
			this.submitForApproval(sContract, status, action, reason);
		}
		contract.setBidApprovalStatus(status);
		this.update(contract);
	}

	@Override
	public Contract findSiblingContract(Contract c) throws BusinessException {
		if (!DealType._BUY_.equals(c.getDealType())) {
			throw new SiblingContractNotFoundException();
		}

		Map<String, Object> params = new HashMap<>();
		params.put("resaleRef", c);
		List<Contract> list = this.findEntitiesByAttributes(params);
		for (Contract e : list) {
			if (e.getReadOnly()) {
				return e;
			}
			if (c.getSupplier().getCode().equals(e.getHolder().getCode()) && c.getHolder().getCode().equals(e.getCustomer().getCode())) {
				return e;
			}
		}
		throw new SiblingContractNotFoundException(CmmErrorCode.NO_SIBLING_FOUND, CmmErrorCode.NO_SIBLING_FOUND.getErrMsg());
	}

	@Override
	@Transactional
	@History(type = Contract.class)
	public void approve(Contract contract, @Action String action, @Reason String reason) throws BusinessException {
		Contract sContract = null;
		try {
			sContract = this.findSiblingContract(contract);
		} catch (SiblingContractNotFoundException e) {
			Contract_Service.LOG.info(Contract_Service.SIBLING_CONTRACT_NOT_FOUND, e);
		}
		if (sContract != null) {
			this.approve(sContract, action, reason);
		}
		contract.setBidApprovalStatus(BidApprovalStatus._APPROVED_);
		this.update(contract);
	}

	@Override
	@Transactional
	@History(type = Contract.class)
	public void reject(Contract contract, @Action String action, @Reason String reason) throws BusinessException {
		Contract sContract = null;
		try {
			sContract = this.findSiblingContract(contract);
		} catch (SiblingContractNotFoundException e) {
			Contract_Service.LOG.info(Contract_Service.SIBLING_CONTRACT_NOT_FOUND, e);
		}

		if (sContract != null) {
			this.reject(sContract, action, reason);
		}

		// for bids set the Bid Status to Draft so that the user can modify the bid before sending it again for approval
		if (!contract.getIsContract()) {
			contract.setBidStatus(BidStatus._DRAFT_);
		}

		contract.setBidApprovalStatus(BidApprovalStatus._REJECTED_);
		this.update(contract);
	}

	@Override
	@Transactional
	@History(type = Contract.class)
	public void submitForApprovalPeriod(Contract contract, BidApprovalStatus status, @Action String action, @Reason String reason)
			throws BusinessException {
		contract.setPeriodApprovalStatus(status);
		this.onUpdate(contract);
	}

	@Override
	@Transactional
	public void approvePeriodUpdate(Contract e, @Reason String reason) throws BusinessException {
		e.setPeriodApprovalStatus(BidApprovalStatus._APPROVED_);
		this.checkContractConditions(e);
		this.setTransientFields(e.getPricingBases());

		List<PricingBase> pbList = this.priceBuilderService.updateMargins((List<PricingBase>) e.getPricingBases(), e.getValidFrom(), e.getValidTo());
		for (PricingBase pb : pbList) {
			if (this.pbService.isIndex(pb)) {
				this.priceBuilderService.buildPricingBases(pb);
			} else {
				ContractPriceCategory cpc = pb.getContractPriceCategories().iterator().next();
				this.componentPriceBuilder.updateMargins((List<ContractPriceComponent>) cpc.getPriceComponents(), pb.getValidFrom(), pb.getValidTo());
				for (ContractPriceComponent comp : cpc.getPriceComponents()) {
					this.componentPriceBuilder.createConvertedPrices(comp);
				}
			}
		}
		e.setPricingBases(pbList);

		// Update contract
		this.onUpdate(e);

		// Save action to original
		this.generateSaveToHistory(e, "Period Update Approved", reason);

		// Publish changes to original if all conditions are met
		if (ContractUtil.checkIfCanPublishAutomatically(e)) {
			Contract original = this.findById(e.getBlueprintOriginalContractReference());
			this.publishBlueprint(e, original);
		}
	}

	@Override
	@Transactional
	@History(value = "Period Update Rejected", type = Contract.class)
	public void rejectPeriodUpdate(Contract e, @Reason String reason) throws BusinessException {
		e.setPeriodApprovalStatus(BidApprovalStatus._REJECTED_);
		this.onUpdate(e);
	}

	@Override
	@Transactional
	@History(type = Contract.class)
	public void addHistoryEntry(Contract e, @Action String action, @Reason String reason) throws BusinessException {
		this.update(e);
	}

	@Override
	public void terminateAllWorkflows(Contract contract) throws BusinessException {
		List<WorkflowInstanceEntity> contractWorkflows = this.workflowEntityService.getEntityesByObjectIdObjectType(contract.getId(),
				contract.getClass().getSimpleName());
		for (WorkflowInstanceEntity workflow : contractWorkflows) {
			this.workflowBpmManager.terminateWorkflow(workflow.getWorkflowInstance().getId(), CONTRACT_RESET);
		}
	}

	@Override
	public void checkContractConditions(Contract contract) throws BusinessException {
		this.checkUpdateConditions(contract);

		if (!contract.getIsContract() && BidStatus._DRAFT_.equals(contract.getBidStatus())) {
			this.recalculateShipTo(contract);
		}
		this.sendMessage("checkOutRangedFuelEventsChannel", contract);
	}

	@Override
	@Transactional
	@History(type = Contract.class)
	public void submitForApprovalPrice(Contract e, BidApprovalStatus status, @Action String action, @Reason String reason) throws BusinessException {
		e.setPriceApprovalStatus(status);
		this.onUpdate(e);
	}

	@Override
	@Transactional
	@History(type = Contract.class)
	public void submitForApprovalShipTo(Contract e, BidApprovalStatus status, @Action String action, @Reason String reason) throws BusinessException {
		e.setShipToApprovalStatus(status);
		this.onUpdate(e);
	}

	@Override
	@Transactional
	public void approvePriceUpdate(Contract e, @Reason String reason) throws BusinessException {
		e.setPriceApprovalStatus(BidApprovalStatus._APPROVED_);

		// Update contract
		this.onUpdate(e);

		// Save action to original
		this.generateSaveToHistory(e, "Price Update Approved", reason);

		// Publish changes to original if all conditions are met
		if (ContractUtil.checkIfCanPublishAutomatically(e)) {
			Contract original = this.findById(e.getBlueprintOriginalContractReference());
			this.publishBlueprint(e, original);
		}
	}

	@Override
	@Transactional
	@History(value = "Price Update Rejected", type = Contract.class)
	public void rejectPriceUpdate(Contract e, @Reason String reason) throws BusinessException {
		e.setPriceApprovalStatus(BidApprovalStatus._REJECTED_);
		this.onUpdate(e);
	}

	@Override
	@Transactional
	public void approveShipToUpdate(Contract e, String reason) throws BusinessException {
		e.setShipToApprovalStatus(BidApprovalStatus._APPROVED_);

		// Update contract
		this.onUpdate(e);

		// Save action to original
		this.generateSaveToHistory(e, "Ship To Update Approved", reason);

		// Publish changes to original if all conditions are met
		if (ContractUtil.checkIfCanPublishAutomatically(e)) {
			Contract original = this.findById(e.getBlueprintOriginalContractReference());
			this.publishBlueprint(e, original);
		}
	}

	@Override
	@Transactional
	@History(value = "Ship To Update Rejected", type = Contract.class)
	public void rejectShipToUpdate(Contract e, String reason) throws BusinessException {
		e.setShipToApprovalStatus(BidApprovalStatus._REJECTED_);
		this.onUpdate(e);
	}

	/**
	 * Drop temporary changes performed over an existing contract and return contract to the initial state. Will terminate all active workflows and
	 * clear temporary changes, history and attachements
	 *
	 * @param Contract
	 * @return
	 */
	@Override
	@Transactional
	public void discardBlueprint(Contract blueprint, Contract original) throws BusinessException {
		// Terminat all workflow instances
		this.terminateAllWorkflows(blueprint);

		// Remove blueprint and childs (history,attachments)
		this.clearBlueprint(blueprint);

		// Remove blueprint reference from original
		original.setBlueprintOriginalContractReference(null);

		this.onUpdate(original);
	}

	/**
	 * Merge the blueprint contract into the original one. Will merge the data based on workflows (Period, ShipTo, Price) only if the approval status
	 * is Approved. Will merge also the history and documents.
	 *
	 * @param blueprint
	 * @throws BusinessException
	 */
	@Override
	@Transactional
	public void publishBlueprint(Contract blueprint, Contract original) throws BusinessException {
		Boolean isWorkflowEnabled = this.sysParamService.getSellContractApprovalWorkflow();

		try {
			// Merge blueprint contract into the original contract
			this.copyContractService.mergeBlueprintToOriginalSaleContract(blueprint, original);

			// Clone period if approved
			boolean hasPeriodChanges = !this.contractChangeService.getContractChanges(blueprint, ContractChangeType._PERIOD_).isEmpty();
			if (ContractUtil.canPublishChanges(isWorkflowEnabled, hasPeriodChanges, blueprint.getPeriodApprovalStatus())) {
				original.setValidFrom(blueprint.getValidFrom());
				original.setValidTo(blueprint.getValidTo());
				original.setPeriodApprovalStatus(blueprint.getPeriodApprovalStatus());
			}

			// Clone prices if approved
			boolean hasPriceChanges = !this.contractChangeService.getContractChanges(blueprint, ContractChangeType._PRICE_).isEmpty();
			if (ContractUtil.canPublishChanges(isWorkflowEnabled, hasPeriodChanges, blueprint.getPeriodApprovalStatus())
					|| ContractUtil.canPublishChanges(isWorkflowEnabled, hasPriceChanges, blueprint.getPriceApprovalStatus())) {
				original.getPricingBases().clear();
				original.getPriceCategories().clear();
				original.getPricingBases().addAll(this.copyContractService.getPricingBases(blueprint, original, false));
				if (ContractUtil.canPublishChanges(isWorkflowEnabled, hasPriceChanges, blueprint.getPriceApprovalStatus())) {
					original.setPriceApprovalStatus(blueprint.getPriceApprovalStatus());
				}
			}

			// Clone shipTo if approved
			boolean hasShipToChanges = !this.contractChangeService.getContractChanges(blueprint, ContractChangeType._SHIPTO_).isEmpty();
			if (ContractUtil.canPublishChanges(isWorkflowEnabled, hasShipToChanges, blueprint.getShipToApprovalStatus())) {
				this.contractPropagatorService.propagateShipTo(blueprint, original);
				original.setShipToApprovalStatus(blueprint.getShipToApprovalStatus());
			}

			original.setBlueprintOriginalContractReference(null);

			// Update original
			this.update(original);

			// Add attachemnts from blueprint to original
			Map<String, Object> targetParam = new HashMap<>();
			targetParam.put(TARGET_REF_ID, original.getId());
			targetParam.put(TARGET_ALIAS, Contract.class.getSimpleName());
			List<Attachment> originalAttachementList = this.attachmentService.findEntitiesByAttributes(targetParam);

			this.copyContractService.copyAttachments(blueprint, original);
			this.attachmentService.delete(originalAttachementList);

			// Add history from blueprint to original
			Map<String, Object> objectParam = new HashMap<>();
			objectParam.put(OBJECT_ID, original.getId());
			objectParam.put(OBJECT_TYPE, Contract.class.getSimpleName());
			List<ChangeHistory> originalHistoryList = this.historyService.findEntitiesByAttributes(objectParam);

			this.copyContractService.copyHistory(blueprint, original);
			this.historyService.delete(originalHistoryList);
		} catch (SecurityException | InstantiationException | IllegalAccessException e) {
			throw new BusinessException(CmmErrorCode.BLUEPRINT_CONTRACT_PUBLISH, CmmErrorCode.BLUEPRINT_CONTRACT_PUBLISH.getErrMsg(), e);
		}

		// Clear blueprint
		this.clearBlueprint(blueprint);

		// Save action to original
		this.generateSaveToHistory(original, PUBLISHED, null);
	}

	@Override
	@Transactional
	public Contract getBlueprintSaleContract(Contract contract) throws BusinessException {
		// Is a blueprint, return itself
		if (contract.getIsBlueprint()) {
			return contract;
		}

		// It is not a blueprint and has a reference to the blueprint, return the blueprint
		if (contract.getBlueprintOriginalContractReference() != null) {
			return this.findById(contract.getBlueprintOriginalContractReference());
		}

		// It is not a blueprint and has no reference to a blueprint, create a blueprint and return it
		return this.copyContractService.generateBlueprintSaleContract(contract);
	}

	/**
	 * Clear contract blueprint, history and attachemnts
	 *
	 * @param blueprint
	 * @throws BusinessException
	 */
	private void clearBlueprint(Contract blueprint) throws BusinessException {
		// Clear all contract changes
		List<ContractChange> contractChanges = this.contractChangeService.getContractChanges(blueprint);
		this.contractChangeService.delete(contractChanges);

		// Clear contract history
		Map<String, Object> objectParam = new HashMap<>();
		objectParam.put(Contract_Service.OBJECT_ID, blueprint.getId());
		objectParam.put(Contract_Service.OBJECT_TYPE, Contract.class.getSimpleName());

		List<ChangeHistory> history = this.historyService.findEntitiesByAttributes(objectParam);
		this.historyService.delete(history);

		// Clear contract attachments
		Map<String, Object> targetParam = new HashMap<>();
		targetParam.put(Contract_Service.TARGET_REF_ID, blueprint.getId());
		targetParam.put(Contract_Service.TARGET_ALIAS, blueprint.getClass().getSimpleName());

		List<Attachment> attachments = this.attachmentService.findEntitiesByAttributes(targetParam);
		this.attachmentService.delete(attachments);

		// Delete contract blueprint
		this.deleteById(blueprint.getId());
	}

	@Override
	public Integer calculateRevisionVersion(List<Contract> bids, String version) throws BusinessException {
		Integer revision = Integer.valueOf(1);
		for (Contract bid : bids) {
			if (bid.getBidVersion() != null && bid.getBidRevision() != null && bid.getBidVersion().equals(version)
					&& bid.getBidRevision() > revision) {
				revision = bid.getBidRevision();
			}
		}
		return revision;
	}

	@Override
	public Boolean hasResaleReference(Contract contract) {
		if (DealType._BUY_.equals(contract.getDealType())) {
			Map<String, Object> params = new HashMap<>();
			params.put("resaleRef", contract);
			List<Contract> salesContracts = this.findEntitiesByAttributes(params);
			if (!salesContracts.isEmpty()) {
				return true;
			}
		} else if (DealType._SELL_.equals(contract.getDealType()) && (contract.getResaleRef() != null)) {
			return true;
		}
		return false;
	}

	/**
	 * @param bid
	 * @return
	 * @throws BusinessException
	 */
	private Boolean canDeleteBid(Contract bid) throws BusinessException {
		Boolean canDelete = true;

		if (BidStatus._DRAFT_.equals(bid.getBidStatus())) {
			if (this.sysParamService.getBidApprovalWorkflow() && BidApprovalStatus._APPROVED_.equals(bid.getBidApprovalStatus())) {
				canDelete = false;
			}
		} else if (BidStatus._SUBMITTED_.equals(bid.getBidStatus()) && (TransmissionStatus._IN_PROGRESS_.equals(bid.getBidTransmissionStatus())
				|| TransmissionStatus._TRANSMITTED_.equals(bid.getBidTransmissionStatus()))) {
			canDelete = false;
		} else {
			canDelete = false;
		}

		return canDelete;
	}

	@Override
	@Transactional
	public void deleteInternalResale(Contract contract) throws BusinessException {
		this.deleteById(contract.getId());

	}

}