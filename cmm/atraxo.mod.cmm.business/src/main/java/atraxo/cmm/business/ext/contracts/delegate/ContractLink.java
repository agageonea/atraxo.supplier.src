package atraxo.cmm.business.ext.contracts.delegate;

import atraxo.cmm.domain.ext.contracts.ContractModification;

public class ContractLink {

	private Integer contractId;
	private ContractModification contractModification;

	public ContractLink(Integer contractId, ContractModification contractModification) {
		super();
		this.contractId = contractId;
		this.contractModification = contractModification;
	}

	public ContractLink() {
		super();
	}

	public Integer getContractId() {
		return this.contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public ContractModification getContractModification() {
		return this.contractModification;
	}

	public void setContractModification(ContractModification contractModification) {
		this.contractModification = contractModification;
	}
}
