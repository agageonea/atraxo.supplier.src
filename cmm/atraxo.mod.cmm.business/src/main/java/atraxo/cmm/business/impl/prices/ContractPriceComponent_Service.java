/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.prices;

import atraxo.cmm.domain.ext.ContractPriceComponentCheckResult;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ContractPriceComponent} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ContractPriceComponent_Service
		extends
			AbstractEntityService<ContractPriceComponent> {

	/**
	 * Public constructor for ContractPriceComponent_Service
	 */
	public ContractPriceComponent_Service() {
		super();
	}

	/**
	 * Public constructor for ContractPriceComponent_Service
	 * 
	 * @param em
	 */
	public ContractPriceComponent_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ContractPriceComponent> getEntityClass() {
		return ContractPriceComponent.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ContractPriceComponent
	 */
	public ContractPriceComponent findByBusinessKey(
			ContractPriceCategory contrPriceCtgry, Date validFrom, Date validTo) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							ContractPriceComponent.NQ_FIND_BY_BUSINESSKEY,
							ContractPriceComponent.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("contrPriceCtgry", contrPriceCtgry)
					.setParameter("validFrom", validFrom)
					.setParameter("validTo", validTo).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ContractPriceComponent",
							"contrPriceCtgry, validFrom, validTo"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ContractPriceComponent",
							"contrPriceCtgry, validFrom, validTo"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return ContractPriceComponent
	 */
	public ContractPriceComponent findByBusinessKey(Long contrPriceCtgryId,
			Date validFrom, Date validTo) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							ContractPriceComponent.NQ_FIND_BY_BUSINESSKEY_PRIMITIVE,
							ContractPriceComponent.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("contrPriceCtgryId", contrPriceCtgryId)
					.setParameter("validFrom", validFrom)
					.setParameter("validTo", validTo).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ContractPriceComponent",
							"contrPriceCtgryId, validFrom, validTo"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ContractPriceComponent",
							"contrPriceCtgryId, validFrom, validTo"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return ContractPriceComponent
	 */
	public ContractPriceComponent findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							ContractPriceComponent.NQ_FIND_BY_BUSINESS,
							ContractPriceComponent.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ContractPriceComponent", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ContractPriceComponent", "id"), nure);
		}
	}

	/**
	 * Find by reference: contrPriceCtgry
	 *
	 * @param contrPriceCtgry
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> findByContrPriceCtgry(
			ContractPriceCategory contrPriceCtgry) {
		return this.findByContrPriceCtgryId(contrPriceCtgry.getId());
	}
	/**
	 * Find by ID of reference: contrPriceCtgry.id
	 *
	 * @param contrPriceCtgryId
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> findByContrPriceCtgryId(
			Integer contrPriceCtgryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ContractPriceComponent e where e.clientId = :clientId and e.contrPriceCtgry.id = :contrPriceCtgryId",
						ContractPriceComponent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contrPriceCtgryId", contrPriceCtgryId)
				.getResultList();
	}
	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> findByCurrency(Currencies currency) {
		return this.findByCurrencyId(currency.getId());
	}
	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> findByCurrencyId(Integer currencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ContractPriceComponent e where e.clientId = :clientId and e.currency.id = :currencyId",
						ContractPriceComponent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currencyId", currencyId).getResultList();
	}
	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> findByUnit(Unit unit) {
		return this.findByUnitId(unit.getId());
	}
	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> findByUnitId(Integer unitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ContractPriceComponent e where e.clientId = :clientId and e.unit.id = :unitId",
						ContractPriceComponent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitId", unitId).getResultList();
	}
	/**
	 * Find by reference: convertedPrices
	 *
	 * @param convertedPrices
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> findByConvertedPrices(
			ContractPriceComponentConv convertedPrices) {
		return this.findByConvertedPricesId(convertedPrices.getId());
	}
	/**
	 * Find by ID of reference: convertedPrices.id
	 *
	 * @param convertedPricesId
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> findByConvertedPricesId(
			Integer convertedPricesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from ContractPriceComponent e, IN (e.convertedPrices) c where e.clientId = :clientId and c.id = :convertedPricesId",
						ContractPriceComponent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("convertedPricesId", convertedPricesId)
				.getResultList();
	}
}
