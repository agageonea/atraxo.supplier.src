package atraxo.cmm.business.ext.exceptions;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import seava.j4e.api.exceptions.BusinessException;

public class SupplierContractNotFoundException extends BusinessException {

	public SupplierContractNotFoundException(String errorDetails) {
		super(BusinessErrorCode.SUPPLIER_CONTRACT_NOT_FOUND, errorDetails);
	}

}
