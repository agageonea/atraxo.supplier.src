package atraxo.cmm.business.ext.tender.service.updater.listener;

import java.beans.PropertyChangeEvent;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.cmm.domain.impl.tender.TenderLocationExpectedPrice;
import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import seava.j4e.api.exceptions.ActionNotSupportedException;
import seava.j4e.commons.utils.event.PropertyAddedEvent;
import seava.j4e.commons.utils.listener.ChangeListener;
import seava.j4e.commons.utils.listener.changes.ObjectAdded;

/**
 * Builds the message which will be logged to history of the Tender
 *
 * @author abolindu
 */
public class TenderPropertyAddedListener extends ChangeListener {

	private Logger LOG = LoggerFactory.getLogger(TenderPropertyAddedListener.class);

	private static final String TENDER_LOCATION_ADDED = "2000";
	private static final String TENDER_RECEIVER_ADDED = "2001";
	private static final String TENDER_ROUND_ADDED = "2002";
	private static final String TENDER_EXPECTED_PRICES_ADDED = "2003";

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt instanceof PropertyAddedEvent) {
			try {
				this.propertyAdded((PropertyAddedEvent) evt);
			} catch (ActionNotSupportedException e) {
				if (this.LOG.isDebugEnabled()) {
					this.LOG.debug(e.getMessage(), e);
				}
			}
		}
	}

	private void propertyAdded(PropertyAddedEvent event) throws ActionNotSupportedException {
		Object affectedEntity = event.getAddedEntity();
		Object sourceEntity = event.getSourceEntity();
		String messageCode = getMessageCode(affectedEntity != null ? affectedEntity : sourceEntity);
		ObjectAdded objectAdded = new ObjectAdded(affectedEntity, sourceEntity, messageCode);
		this.changes.addToObjectAddedList(Arrays.asList(objectAdded));
	}

	private static String getMessageCode(Object entity) throws ActionNotSupportedException {
		if (entity instanceof TenderLocation) {
			return TENDER_LOCATION_ADDED;
		} else if (entity instanceof TenderLocationAirlines) {
			return TENDER_RECEIVER_ADDED;
		} else if (entity instanceof TenderLocationRound) {
			return TENDER_ROUND_ADDED;
		} else if (entity instanceof TenderLocationExpectedPrice) {
			return TENDER_EXPECTED_PRICES_ADDED;
		} else {
			throw new ActionNotSupportedException("No message defined for entity: " + entity.getClass());
		}
	}
}