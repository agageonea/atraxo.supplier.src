package atraxo.cmm.business.ext.contracts.service.propagator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.ext.contracts.IPriceBuilder;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.ext.contracts.delegate.CopyContractUtils;
import atraxo.cmm.business.ext.contracts.delegate.PricingBase_Bd;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.fmbas.business.ext.util.EntityCloner;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * Servive that propagate contract child entities from purchase contract to sale contract.
 *
 * @author zspeter
 */
public class ChildEntityPropagatorService extends AbstractBusinessBaseService {

	@Autowired
	private IPriceBuilder builder;
	@Autowired
	private IContractPriceCategoryService srv;

	/**
	 * @param pricingBase
	 * @param contract
	 * @return
	 * @throws BusinessException
	 */
	public PricingBase createPricingBase(PricingBase pricingBase, Contract contract) throws BusinessException {
		try {
			String json = this.buildNewJsonString(pricingBase);
			PricingBase newPb = EntityCloner.cloneEntity(pricingBase);
			newPb.setContractPriceCategories(null);
			newPb.setContract(contract);
			newPb.setPriceCategoryIdsJson(json);
			this.builder.buildPricingBases(newPb);
			ContractPriceCategory cpc = newPb.getContractPriceCategories().iterator().next();
			cpc.setResaleRef(pricingBase.getContractPriceCategories().iterator().next().getId());
			return newPb;
		} catch (BeansException | JSONException e) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e.getMessage(), e);
		}
	}

	private String buildNewJsonString(PricingBase pricingBase) throws JSONException, BusinessException {
		JSONArray newJsonArray = new JSONArray();
		if (pricingBase.getPriceCat().getPricePer().equals(PriceInd._COMPOSITE_)) {
			JSONArray jsonArray = new JSONArray(pricingBase.getPriceCategoryIdsJson());
			if (jsonArray.length() == 0) {
				throw new BusinessException(CmmErrorCode.COMPOSITE_PRICE_MUST_HAVE_A_CHILD,
						CmmErrorCode.COMPOSITE_PRICE_MUST_HAVE_A_CHILD.getErrMsg());
			}

			for (int i = 0; i < jsonArray.length(); ++i) {
				JSONObject json = jsonArray.getJSONObject(i);
				ContractPriceCategory priceCategory = this.getPriceCategory(json);
				Map<String, Integer> map = new HashMap<>();
				map.put("id", priceCategory.getId());
				newJsonArray.put(map);
			}
		}
		return newJsonArray.toString();
	}

	/**
	 * @param json
	 * @return
	 * @throws JSONException
	 * @throws BusinessException
	 */
	private ContractPriceCategory getPriceCategory(JSONObject json) throws JSONException, BusinessException {
		String idStr = json.getString("id");
		Integer priceCategoryId = Integer.parseInt(idStr);

		Map<String, Object> params = new HashMap<>();
		params.put("resaleRef", priceCategoryId);
		ContractPriceCategory cpc = null;
		try {
			cpc = this.srv.getEntityManager()
					.createQuery("select e from ContractPriceCategory e where e.resaleRef =:priceCatId", ContractPriceCategory.class)
					.setParameter("priceCatId", priceCategoryId).getSingleResult();
		} catch (NoResultException | NonUniqueResultException e) {
			throw new BusinessException(CmmErrorCode.PROPAGATION_PRICE_CATEGORY_NOT_FOUND,
					CmmErrorCode.PROPAGATION_PRICE_CATEGORY_NOT_FOUND.getErrMsg(), e);
		}
		return cpc;
	}

	private HashMap<Integer, ContractPriceCategory> fillMap(PricingBase pricingBase, Contract contract) {
		List<Integer> list = new ArrayList<>();
		for (ContractPriceCategory cpc : pricingBase.getContractPriceCategories()) {
			for (ContractPriceCategory parent : cpc.getParentPriceCategory()) {
				list.add(parent.getId());
			}
		}
		HashMap<Integer, ContractPriceCategory> map = new HashMap<>();
		for (ContractPriceCategory cpc : contract.getPriceCategories()) {
			if (list.contains(cpc.getResaleRef())) {
				map.put(cpc.getResaleRef(), cpc);
			}
		}
		return map;
	}

	/**
	 * @param cpc
	 * @param contract
	 * @return
	 * @throws BusinessException
	 */
	public ContractPriceComponent createPriceComponent(ContractPriceComponent cpc, Contract contract) throws BusinessException {
		try {
			for (ContractPriceCategory cat : contract.getPriceCategories()) {
				if (cat.getResaleRef().equals(cpc.getContrPriceCtgry().getId())) {
					ContractPriceComponent newE = EntityCloner.cloneEntity(cpc);
					newE.setContrPriceCtgry(cat);
					newE.setConvertedPrices(CopyContractUtils.getPriceComponentConverted(cpc, newE));
					return newE;
				}
			}
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, "Price Component cannot be propagated. Please contact Suppport team.");
		} catch (BeansException e) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e.getMessage(), e);
		}
	}

	/**
	 * @param list
	 * @param contract
	 * @return
	 * @throws BusinessException
	 */
	public List<ContractPriceRestriction> createPriceRestrictions(List<ContractPriceRestriction> list, Contract contract) throws BusinessException {
		List<ContractPriceRestriction> newList = new ArrayList<>();
		for (ContractPriceRestriction r : list) {
			for (ContractPriceCategory cat : contract.getPriceCategories()) {
				this.createPriceRestriction(newList, r, cat);
			}
		}
		return newList;
	}

	/**
	 * @param args
	 * @param contract
	 * @return
	 * @throws BusinessException
	 */
	public List<ShipTo> createNewShipTos(Object args, Contract contract) throws BusinessException {
		List<ShipTo> shipTos = new ArrayList<>();

		if (args instanceof List) {
			List<?> list = (List<?>) args;
			for (Iterator<?> iterator = list.iterator(); iterator.hasNext();) {
				Object object = iterator.next();
				ShipTo shipTo = this.createNewShipTo(contract, object);
				if (shipTo != null) {
					shipTos.add(shipTo);
				}

			}
		} else {
			ShipTo shipTo = this.createNewShipTo(contract, args);
			if (shipTo != null) {
				shipTos.add(shipTo);
			}
		}
		return shipTos;
	}

	/**
	 * @param entity
	 * @param contract
	 * @return
	 * @throws BusinessException
	 */
	public ContractPriceCategory propagateContractPriceCategory(ContractPriceCategory entity, Contract contract) throws BusinessException {
		HashMap<Integer, ContractPriceCategory> map = this.fillMap(entity.getPricingBases(), contract);
		for (ContractPriceCategory cpc : contract.getPriceCategories()) {
			if (cpc.getResaleRef().equals(entity.getId())) {
				CopyContractUtils.copyManyToManyRelation(map, entity, cpc);
				return cpc;
			}
		}
		throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, "Price Category cannot be propagated. Please contact Suppport team.");
	}

	/**
	 * @param pb
	 * @param sibling
	 * @throws BusinessException
	 */
	public void propagatePbUpdate(PricingBase pb, PricingBase sibling) throws BusinessException {
		sibling.setValidFrom(pb.getValidFrom());
		sibling.setValidTo(pb.getValidTo());
		sibling.setQuotation(pb.getQuotation());
		sibling.setQuotationOffset(pb.getQuotationOffset());
		sibling.setDecimals(pb.getDecimals());
		sibling.setDescription(pb.getDescription());
		sibling.setOperator(pb.getOperator());
		sibling.setFactor(pb.getFactor());
		sibling.setConvUnit(pb.getConvUnit());
		sibling.setQuantityType(pb.getQuantityType());
		sibling.setVat(pb.getVat());
		PricingBase_Bd bd = this.getBusinessDelegate(PricingBase_Bd.class);
		ContractPriceCategory cpc = sibling.getContractPriceCategories().iterator().next();
		PriceCategory priceCategory = cpc.getPriceCategory();
		bd.createPriceCtgry(priceCategory, sibling, cpc);
	}

	private ShipTo createNewShipTo(Contract contract, Object object) throws BusinessException {
		ShipTo newShipTo = null;
		if (object instanceof ShipTo) {
			ShipTo shipTo = (ShipTo) object;
			try {
				newShipTo = EntityCloner.cloneEntity(shipTo);
				newShipTo.setContract(contract);
			} catch (BeansException e) {
				throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e.getMessage(), e);
			}
		}
		return newShipTo;

	}

	private void createPriceRestriction(List<ContractPriceRestriction> newList, ContractPriceRestriction r, ContractPriceCategory cat)
			throws BusinessException {
		try {
			if (cat.getResaleRef().equals(r.getContractPriceCategory().getId())) {
				ContractPriceRestriction newR = EntityCloner.cloneEntity(r);
				newR.setContractPriceCategory(cat);
				newList.add(newR);
			}
		} catch (BeansException e) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e.getMessage(), e);
		}
	}

}
