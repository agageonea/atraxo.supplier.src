package atraxo.cmm.business.ext.contracts.delegate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.contracts.IPricingBaseService;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.api.prices.IContractPriceComponentService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.prices.delegate.ContractPriceCategory_Bd;
import atraxo.cmm.business.ext.prices.delegate.ContractPriceComponentConv_Bd;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.fmbas.business.api.categories.IPriceCategoryService;
import atraxo.fmbas.business.api.exchangerate.IExchangeRateService;
import atraxo.fmbas.business.api.quotation.IQuotationService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.domain.ext.types.timeseries.TimeSeriesType;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;
import atraxo.fmbas.domain.impl.fmbas_type.AverageMethodCode;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieAverage;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class Contract_Bd extends AbstractBusinessDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(Contract_Bd.class);

	/**
	 * Update a contract when a time series has been changed.
	 *
	 * @param obj Time series id.
	 * @throws BusinessException
	 */
	public void update(Object obj) throws BusinessException {
		if (obj instanceof Integer) {
			Integer timeSeriesId = (Integer) obj;
			ITimeSerieService tsService = (ITimeSerieService) this.findEntityService(TimeSerie.class);
			TimeSerie timeSerie = tsService.findById(timeSeriesId);
			TimeSeriesType tsType = TimeSeriesType.getByCode(timeSerie.getSerieType());
			switch (tsType) {
			case ENERGYQUOTATION:
				this.updateFromEnergyQuotations(timeSerie);
				break;
			case EXCHANGERATE:
				this.updateFromExchangeRate(timeSerie);
				break;
			default:
				break;
			}

		}
	}

	/**
	 * @param obj
	 * @throws BusinessException
	 */
	@SuppressWarnings("unchecked")
	public void checkAverageMethodsUsage(Object obj) throws BusinessException {
		if (obj instanceof List) {
			List<Object> deactivatedAvgs = (List<Object>) obj;
			ITimeSerieService tsService = (ITimeSerieService) this.findEntityService(TimeSerie.class);
			for (Object avg : deactivatedAvgs) {
				List<TimeSerie> tsList = tsService.findByTimeserieAvg((TimeSerieAverage) avg);
				for (TimeSerie ts : tsList) {
					TimeSeriesType tsType = TimeSeriesType.getByCode(ts.getSerieType());
					switch (tsType) {
					case ENERGYQUOTATION:
						this.checkQuotations((TimeSerieAverage) avg, ts);
						break;
					case EXCHANGERATE:
						this.checkExchangeRates((TimeSerieAverage) avg, ts);
						break;
					default:
						break;
					}
				}
			}
		}
	}

	/**
	 * Check the usage of the average methods
	 *
	 * @param deactivatedAvgs
	 * @throws BusinessException
	 */
	private void checkQuotations(TimeSerieAverage deactivatedAvg, TimeSerie timeSerie) throws BusinessException {
		IPricingBaseService pbService = (IPricingBaseService) this.findEntityService(PricingBase.class);
		IQuotationService quotationService = (IQuotationService) this.findEntityService(Quotation.class);

		List<Quotation> quotationList = quotationService.findByTimeseries(timeSerie);
		for (Quotation quotation : quotationList) {
			List<PricingBase> affectedPrices = pbService.findByQuotation(quotation);
			for (PricingBase pb : affectedPrices) {
				if (pb.getQuotation().getAvgMethodIndicator().equals(deactivatedAvg.getAveragingMethod())) {
					throw new BusinessException(CmmErrorCode.EX_RATE_IN_USE, String.format(CmmErrorCode.EX_RATE_IN_USE.getErrMsg(),
							deactivatedAvg.getAveragingMethod().getCode(), this.getContractCode(pb.getContract())));
				}
			}
		}
	}

	/**
	 * Check the usage of the average methods
	 *
	 * @param deactivatedAvgs
	 * @param timeSerie
	 * @throws BusinessException
	 */
	private void checkExchangeRates(TimeSerieAverage deactivatedAvg, TimeSerie timeSerie) throws BusinessException {
		IContractPriceCategoryService cpcService = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
		ISystemParameterService systemParameterService = (ISystemParameterService) this.findEntityService(SystemParameter.class);

		Set<String> currencies = new HashSet<>();
		currencies.add(systemParameterService.getSysCurrency());

		List<ContractPriceCategory> cpcList = cpcService.findByFinancialSourceId(timeSerie.getFinancialSource().getId());
		for (ContractPriceCategory cpc : cpcList) {
			if (cpc.getAverageMethod().equals(deactivatedAvg.getAveragingMethod())) {
				for (ContractPriceComponent component : cpc.getPriceComponents()) {
					for (ContractPriceComponentConv converted : component.getConvertedPrices()) {
						String exRateCurrnecy = converted.getExchangeRateCurrnecyCDS();
						currencies.addAll(Arrays.asList(exRateCurrnecy.split("/")));

						if (currencies.contains(timeSerie.getCurrency1Id().getCode()) && currencies.contains(timeSerie.getCurrency2Id().getCode())) {
							throw new BusinessException(CmmErrorCode.EX_RATE_IN_USE, String.format(CmmErrorCode.EX_RATE_IN_USE.getErrMsg(),
									deactivatedAvg.getAveragingMethod().getCode(), this.getContractCode(cpc.getContract())));
						}
					}
				}
			}
		}
	}

	/**
	 * If the Contract which uses the average method is a blueprint, returns the code of the original Contract
	 *
	 * @param contract
	 * @return
	 * @throws BusinessException
	 */
	private String getContractCode(Contract contract) throws BusinessException {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		if (contract.getIsBlueprint()) {
			Contract originalContract = contractService.findById(contract.getBlueprintOriginalContractReference());
			return originalContract.getCode();
		} else {
			return contract.getCode();
		}
	}

	private void updateFromExchangeRate(TimeSerie timeSerie) throws BusinessException {
		IContractPriceCategoryService categoryService = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
		IContractPriceComponentService componentService = (IContractPriceComponentService) this.findEntityService(ContractPriceComponent.class);
		IExchangeRateService exchService = (IExchangeRateService) this.findEntityService(ExchangeRate.class);
		ContractPriceComponentConv_Bd bd = this.getBusinessDelegate(ContractPriceComponentConv_Bd.class);
		List<ExchangeRate> exchangeRates = exchService.findByTserie(timeSerie);
		for (ExchangeRate exchangeRate : exchangeRates) {
			List<ContractPriceCategory> categories = categoryService.findByExchangeRate(exchangeRate);
			for (ContractPriceCategory category : categories) {
				LOG.info("Start update price category:" + category.getName());
				// triangulation update time series contract is not updates
				this.updatePriceComponent(componentService, bd, exchangeRate, category);
			}
		}
	}

	private void updatePriceComponent(IContractPriceComponentService componentService, ContractPriceComponentConv_Bd bd, ExchangeRate exchangeRate,
			ContractPriceCategory category) throws BusinessException {
		List<ContractPriceComponent> components = componentService.findByContrPriceCtgry(category);
		for (ContractPriceComponent component : components) {
			if (this.shouldUpdate(exchangeRate, component)) {
				bd.updateConvertedPriceComponent(component, true);
			}
		}
	}

	private boolean shouldUpdate(ExchangeRate exch, ContractPriceComponent component) {
		if ((component.getCurrency().getId().equals(exch.getCurrency1().getId())
				&& component.getContrPriceCtgry().getContract().getSettlementCurr().getId().equals(exch.getCurrency2().getId()))
				|| (component.getCurrency().getId().equals(exch.getCurrency2().getId())
						&& component.getContrPriceCtgry().getContract().getSettlementCurr().getId().equals(exch.getCurrency1().getId()))) {
			Date[] dateArr = this.getAffectedDate(component);
			List<ExchangeRateValue> list = new ArrayList<>(exch.getExchangeRateValue());
			list.sort((o1, o2) -> o2.getModifiedAt().compareTo(o1.getModifiedAt()));
			ExchangeRateValue last = list.get(0);
			for (ExchangeRateValue exchValue : list) {
				if (exchValue.getModifiedAt().compareTo(component.getModifiedAt()) >= 0 && dateArr[0].compareTo(exchValue.getVldToDtRef()) <= 0
						&& dateArr[1].compareTo(exchValue.getVldFrDtRef()) >= 0) {
					return true;
				}
			}
			return last.getModifiedAt().compareTo(component.getModifiedAt()) >= 0;
		}
		return false;
	}

	private Date[] getAffectedDate(ContractPriceComponent component) {
		int periodLength = 1;
		Calendar cal = GregorianCalendar.getInstance();
		switch (component.getContrPriceCtgry().getAverageMethod().getCode()) {
		case "DC":
			periodLength = 1;
			break;
		case "WC":
		case "WT":
			periodLength = 7;
			break;
		case "MC":
		case "MT":
			periodLength = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			break;
		case "ST":
		case "SC":
			periodLength = cal.getActualMaximum(Calendar.DAY_OF_MONTH) / 2;
			break;
		default:
			break;
		}
		Date date = component.getValidFrom();
		switch (component.getContrPriceCtgry().getExchangeRateOffset()) {
		case _CURRENT__2_:
			date = DateUtils.addDays(date, -2 * periodLength);
			break;
		case _PREVIOUS_:
			date = DateUtils.addDays(date, -1 * periodLength);
			break;
		default:
			break;
		}
		Date[] dateArr = new Date[2];
		dateArr[0] = date;
		DateTime dtFrom = new DateTime(component.getValidFrom().getTime());
		DateTime dtTo = new DateTime(component.getValidTo().getTime());
		int amount = Days.daysBetween(dtFrom, dtTo).getDays();
		dateArr[1] = DateUtils.addDays(date, amount);
		return dateArr;
	}

	private void updateFromEnergyQuotations(TimeSerie timeSerie) throws BusinessException {
		IQuotationService quotationService = (IQuotationService) this.findEntityService(Quotation.class);
		List<Quotation> quotations = quotationService.findByTimeseries(timeSerie);
		IPricingBaseService pricingBaseService = (IPricingBaseService) this.findEntityService(PricingBase.class);
		ContractPriceCategory_Bd bd = this.getBusinessDelegate(ContractPriceCategory_Bd.class);

		for (Quotation quotation : quotations) {
			List<PricingBase> pricingBases = pricingBaseService.findByQuotation(quotation);
			for (PricingBase pricingBase : pricingBases) {
				LOG.info("Start update price category:" + pricingBase.getDescription());
				if (!PricingBase_Bd.INDEX.equalsIgnoreCase(pricingBase.getPriceCat().getName()) || !this.shouldUpdate(quotation, pricingBase)) {
					continue;
				}
				Contract contract = pricingBase.getContract();
				PriceCategory priceCategory = this.getIndexPriceCategory();
				List<ContractPriceCategory> contractPriceCategories = this.getContractPriceCategories(priceCategory, contract, pricingBase);
				for (ContractPriceCategory contractPriceCategory : contractPriceCategories) {
					bd.reGenerateContractPriceComponent(contractPriceCategory);
				}
				pricingBase.setModifiedAt(new Date());
				pricingBaseService.updatePricingBase(pricingBase);
			}
		}
	}

	private boolean shouldUpdate(Quotation quot, PricingBase pricingBase) {
		Date[] dateArr = this.getAffectedDate(pricingBase);
		List<QuotationValue> list = new ArrayList<>(quot.getQuotationValues());
		// sort DESC
		list.sort((o1, o2) -> o2.getModifiedAt().compareTo(o1.getModifiedAt()));
		if (!list.isEmpty()) {
			for (QuotationValue quotValue : list) {
				if (quotValue.getModifiedAt().compareTo(pricingBase.getModifiedAt()) >= 0 && dateArr[0].compareTo(quotValue.getValidToDate()) <= 0
						&& dateArr[1].compareTo(quotValue.getValidFromDate()) >= 0) {
					return true;
				}
			}
			List<ContractPriceComponent> componenets = new ArrayList<>(
					pricingBase.getContractPriceCategories().iterator().next().getPriceComponents());
			componenets.sort((o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()));
			if (!componenets.isEmpty() && !list.get(0).getRate().equals(componenets.get(0).getPrice())) {
				return true;
			}
		}
		return false;

	}

	private Date[] getAffectedDate(PricingBase pricingBase) {
		int periodLength = 1;
		Calendar cal = GregorianCalendar.getInstance();
		AverageMethodCode code = AverageMethodCode.getByName(pricingBase.getQuotation().getAvgMethodIndicator().getCode());
		switch (code) {
		case _DC_:
			periodLength = 1;
			break;
		case _WC_:
		case _WT_:
			periodLength = 7;
			break;
		case _MC_:
		case _MT_:
			periodLength = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			break;
		case _ST_:
		case _SC_:
			periodLength = cal.getActualMaximum(Calendar.DAY_OF_MONTH) / 2;
			break;
		case _FT_:
		case _FC_:
			periodLength = cal.getActualMaximum(Calendar.DAY_OF_MONTH) / 2;
			break;
		default:
			break;
		}
		cal.setTime(pricingBase.getValidFrom());
		Date date = cal.getTime();
		switch (pricingBase.getQuotationOffset()) {
		case _CURRENT__2_:
			date = DateUtils.addDays(date, -2 * periodLength);
			break;
		case _PREVIOUS_:
			date = DateUtils.addDays(date, -1 * periodLength);
			break;
		default:
			break;
		}
		Date[] dateArr = new Date[2];
		dateArr[0] = date;
		DateTime dtFrom = new DateTime(pricingBase.getValidFrom().getTime());
		DateTime dtTo = new DateTime(pricingBase.getValidTo().getTime());
		int amount = Days.daysBetween(dtFrom, dtTo).getDays();
		dateArr[1] = DateUtils.addDays(date, amount);
		return dateArr;
	}

	private PriceCategory getIndexPriceCategory() throws BusinessException {
		IPriceCategoryService service = (IPriceCategoryService) this.findEntityService(PriceCategory.class);
		return service.findbyName(PricingBase_Bd.INDEX);
	}

	/**
	 * @param priceCategory
	 * @param contract
	 * @param pricingBase
	 * @return
	 * @throws BusinessException
	 */
	private List<ContractPriceCategory> getContractPriceCategories(PriceCategory priceCategory, Contract contract, PricingBase pricingBase)
			throws BusinessException {
		if (contract == null) {
			throw new BusinessException(CmmErrorCode.INVALID_PRICE_CATEGORY_CONTRACT, CmmErrorCode.INVALID_PRICE_CATEGORY_CONTRACT.getErrMsg());
		}
		List<ContractPriceCategory> contractPriceCategories = new ArrayList<>(pricingBase.getContractPriceCategories());
		List<ContractPriceCategory> filteredContractPriceCategories = new ArrayList<>();
		for (ContractPriceCategory contractPriceCategory : contractPriceCategories) {
			if (contractPriceCategory.getPriceCategory().getId().equals(priceCategory.getId())
					&& contractPriceCategory.getPricingBases().getId().equals(pricingBase.getId())) {
				filteredContractPriceCategories.add(contractPriceCategory);
			}
		}
		return filteredContractPriceCategories;
	}

}
