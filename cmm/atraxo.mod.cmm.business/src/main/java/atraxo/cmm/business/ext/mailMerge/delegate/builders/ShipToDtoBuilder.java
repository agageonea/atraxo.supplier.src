package atraxo.cmm.business.ext.mailMerge.delegate.builders;

import atraxo.cmm.domain.ext.mailmerge.dto.contract.ShipToDto;
import atraxo.cmm.domain.impl.shipTo.ShipTo;

/**
 * @author apetho
 */
public final class ShipToDtoBuilder {

	private ShipToDtoBuilder() {

	}

	public static final ShipToDto builShipTo(ShipTo shipTo) {
		ShipToDto dto = new ShipToDto();
		if (shipTo != null) {
			dto.setActualVolume(shipTo.getActualVolume());
			dto.setAdjustedVolume(shipTo.getAdjustedVolume());
			dto.setAllocatedVolume(shipTo.getAllocatedVolume());
			dto.setOfferedVolume(shipTo.getOfferedVolume());
			dto.setValidFrom(shipTo.getValidFrom());
			dto.setValidTo(shipTo.getValidTo());
			dto.setCustomer(CustomerDtoBuilder.buildCustomer(shipTo.getCustomer()));
		}
		return dto;
	}

}
