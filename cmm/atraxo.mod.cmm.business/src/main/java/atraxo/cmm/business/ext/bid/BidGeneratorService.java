package atraxo.cmm.business.ext.bid;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.ext.tender.IBidGeneratorService;
import atraxo.cmm.business.api.tender.ITenderLocationAirlinesService;
import atraxo.cmm.business.api.tender.ITenderLocationService;
import atraxo.cmm.business.ext.contracts.delegate.PricingBase_Bd;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.tender.TenderDefaultConstants;
import atraxo.cmm.business.ext.tender.delegate.PriceBuilderDelegate;
import atraxo.cmm.business.ext.utils.BidVersionCalculator;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.PaymentChoise;
import atraxo.cmm.domain.impl.cmm_type.PaymentType;
import atraxo.cmm.domain.impl.cmm_type.PricingType;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.cmm_type.VatApplicability;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.fmbas.business.api.categories.IPriceCategoryService;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.ext.subsidiary.ISubsidiaryService;
import atraxo.fmbas.business.api.quotation.IQuotationService;
import atraxo.fmbas.business.api.suppliers.ISuppliersService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceType;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.masteragreements.MasterAgreement;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.AbstractBusinessBaseService;
import seava.j4e.commons.utils.SoneEnumUtils;

/**
 * @author zspeter
 */
public class BidGeneratorService extends AbstractBusinessBaseService implements IBidGeneratorService {

	public static final String INITIAL_BID_VERSION = "A";
	private static final Logger LOG = LoggerFactory.getLogger(BidGeneratorService.class);

	@Autowired
	private IContractService contractService;
	@Autowired
	private ITenderLocationService locationService;
	@Autowired
	private ICustomerService customerService;
	@Autowired
	private ISystemParameterService systemParamsService;
	@Autowired
	private IUnitService unitService;
	@Autowired
	private ITenderLocationAirlinesService tenderLocationAirlineService;
	@Autowired
	private ISuppliersService suppliersService;
	@Autowired
	private ISubsidiaryService subsidiaryService;
	@Autowired
	private IUserSuppService userSuppService;
	@Autowired
	private IPriceCategoryService priceCategoryService;
	@Autowired
	private IQuotationService quotationService;

	@Override
	public int generateBidFromLocation(Integer tenderLocationId, Map<String, Object> params, TenderLocationAirlines... airlines)
			throws BusinessException {

		String bidVersion = (String) params.get(TenderDefaultConstants.PARAM_CAPTURE_BID_VERSION);

		// Generate Bid from Tender Location (all airlines)
		if (airlines == null || airlines.length < 1) {
			return this.generateBidFromLocationForAirline(tenderLocationId, bidVersion, params, null);
		}

		// Set initial version
		String localBidVersion = bidVersion;
		if (localBidVersion == null || localBidVersion.isEmpty()) {
			localBidVersion = INITIAL_BID_VERSION;
		}

		// Generate Bid for each airline and recalculate version
		BidVersionCalculator calc = new BidVersionCalculator();
		for (TenderLocationAirlines airline : airlines) {
			if (airline.getIsValid()) {
				this.generateBidFromLocationForAirline(tenderLocationId, localBidVersion, params, airline);
				localBidVersion = calc.calculateNewVersion(Arrays.asList(localBidVersion));
			}
		}

		return -1;
	}

	private int generateBidFromLocationForAirline(Integer tenderLocationId, String bidVersion, Map<String, Object> params,
			TenderLocationAirlines airline) throws BusinessException {
		TenderLocation tenderLocation = this.locationService.findById(tenderLocationId);
		Contract bid = this.contractService.create();

		bid.setReadOnly(false);
		bid.setIsContract(false);
		bid.setBidTenderIdentification(tenderLocation.getTender());
		bid.setBidPackageIdentifier(tenderLocation.getPackageIdentifier());

		Customer subsidiary = this.customerService.findByRefid(tenderLocation.getTender().getSubsidiaryId());
		bid.setHolder(subsidiary);

		bid.setContact(tenderLocation.getTender().getContact());

		UserSupp respBuyer = this.userSuppService.findByLogin(Session.user.get().getLoginName());
		bid.setResponsibleBuyer(respBuyer);

		bid.setBidTenderLocation(tenderLocation);
		bid.setLocation(tenderLocation.getLocation());
		bid.setType(ContractType.getByName(tenderLocation.getTender().getType().getName()));
		bid.setFlightServiceType(tenderLocation.getFlightServiceType());

		// Agreement conditions
		bid.setProduct(tenderLocation.getFuelProduct());
		bid.setValidFrom(tenderLocation.getAgreementFrom());
		bid.setValidTo(tenderLocation.getAgreementTo());
		bid.setBidValidFrom(tenderLocation.getTender().getBiddingPeriodFrom());
		bid.setBidValidTo(tenderLocation.getTender().getBiddingPeriodTo());

		if (tenderLocation.getAstmSpecification() != null && !tenderLocation.getAstmSpecification().isEmpty()) {
			bid.setBidAstmSpecification(tenderLocation.getAstmSpecification());
		} else {
			bid.setBidAstmSpecification(TenderDefaultConstants.DEFAULT_ASTM_SPECIFICATION);
		}

		this.setIplAgent(tenderLocation, bid);

		bid.setIataServiceLevel(tenderLocation.getIataServiceLevel());
		bid.setBidOperatingHours(tenderLocation.getOperatingHours());

		if (!tenderLocation.getDeliveryPoint().equals(ContractSubType._EMPTY_)) {
			bid.setSubType(tenderLocation.getDeliveryPoint());
		} else {
			bid.setSubType(ContractSubType.getByCode(TenderDefaultConstants.DEFAULT_CONTRACT_SUB_TYPE));
		}

		bid.setBidTitleTransfer(tenderLocation.getTitleTransfer());
		bid.setTax(tenderLocation.getTaxType());

		bid.setBidStatus(BidStatus._DRAFT_);
		bid.setBidTransmissionStatus(TransmissionStatus._NEW_);
		bid.setBidCancelTransmitionStatus(TransmissionStatus._NEW_);
		bid.setBidAcceptAwardTransmissionStatus(TransmissionStatus._NEW_);
		bid.setBidDeclineAwardTransmissionStatus(TransmissionStatus._NEW_);
		bid.setBidApprovalStatus(BidApprovalStatus._NEW_);
		bid.setPeriodApprovalStatus(BidApprovalStatus._NEW_);

		bid.setBidRevision(Integer.valueOf(1));
		bid.setBidVersion(!StringUtils.isEmpty(bidVersion) ? bidVersion : INITIAL_BID_VERSION);
		bid.setVat(VatApplicability.getByName(TenderDefaultConstants.DEFAULT_VAT_APPLICABILITY));
		bid.setBidHasTotalVolume(tenderLocation.getHasTotalVolume());

		if (bid.getIsBlueprint() == null) {
			bid.setIsBlueprint(false);
		}

		// Find the customer based on the parameters
		Customer customer = tenderLocation.getTender().getHolder();
		bid.setCustomer(tenderLocation.getTender().getHolder());

		SoneEnumUtils.setEmptyValues(bid);

		Contract lastBid = this.getLastBid(tenderLocation, customer);
		Contract lastContract = this.getLastContract(tenderLocation, customer);

		// Copy payment terms from existing contract or last bid or master agreement or tender invitation
		this.copyPaymentTerms(bid, airline, lastContract, lastBid, tenderLocation, params);

		// Copy pricing data from existing contract or last bid or tender.
		this.copyPricing(bid, lastContract, lastBid, tenderLocation);

		// Copy airlines to shipTo from tender locations
		this.setVolumeBasedProperties(tenderLocation, bid, airline);

		this.contractService.insert(bid);

		return bid.getId();
	}

	private void setVolumeBasedProperties(TenderLocation tenderLocation, Contract bid, TenderLocationAirlines locAirline) throws BusinessException {
		Double density = Double.parseDouble(this.systemParamsService.getDensity());
		BigDecimal convertedVolume = this.unitService.convert(tenderLocation.getVolumeUnit(), bid.getSettlementUnit(), tenderLocation.getVolume(),
				density);
		bid.setOfferedVolume(convertedVolume);

		// volume information, fuel receivers
		bid.setPeriod(tenderLocation.getPeriodType());
		List<TenderLocationAirlines> tenderLocationAirlines;
		if (locAirline == null) {
			tenderLocationAirlines = this.tenderLocationAirlineService.findByTenderLocation(tenderLocation);
		} else {
			tenderLocationAirlines = Arrays.asList(locAirline);
		}

		if (!CollectionUtils.isEmpty(tenderLocationAirlines)) {
			for (TenderLocationAirlines tenderLocationAirline : tenderLocationAirlines) {
				if (tenderLocationAirline.getIsValid() && tenderLocationAirline.getAirline() != null) {
					ShipTo bidAirlineVolume = new ShipTo();
					convertedVolume = this.unitService.convert(tenderLocationAirline.getUnit(), bid.getSettlementUnit(),
							tenderLocationAirline.getVolume(), density);
					bidAirlineVolume.setTenderBidVolume(convertedVolume);
					bidAirlineVolume.setOfferedVolume(convertedVolume);
					Customer airline = this.customerService.findById(tenderLocationAirline.getAirline().getId());
					bidAirlineVolume.setCustomer(airline);
					bidAirlineVolume.setValidFrom(tenderLocation.getAgreementFrom());
					bidAirlineVolume.setValidTo(tenderLocation.getAgreementTo());
					bidAirlineVolume.setContract(bid);

					bid.addToShipTo(bidAirlineVolume);
				}
			}
		}
		bid.setContractVolumeUnit(tenderLocation.getVolumeUnit());
	}

	private Contract getLastContract(TenderLocation tenderLocation, Customer customer) throws BusinessException {
		Contract lastContract = null;
		List<Contract> lastHolderContracts = this.contractService.findByHolderLocation(customer, tenderLocation.getLocation(), true);
		if (!CollectionUtils.isEmpty(lastHolderContracts)) {
			lastContract = lastHolderContracts.get(0);
		} else {
			List<Contract> lastLocationContracts = this.contractService.findByLocation(tenderLocation.getLocation(), true);
			if (!CollectionUtils.isEmpty(lastLocationContracts)) {
				lastContract = lastLocationContracts.get(0);
			}
		}
		return lastContract;
	}

	private void copyPaymentTerms(Contract bid, TenderLocationAirlines airline, Contract lastContract, Contract lastBid,
			TenderLocation tenderLocation, Map<String, Object> params) throws BusinessException {

		boolean copyCompleted = false;

		boolean captureBidFromHolderMA = (boolean) params.get(TenderDefaultConstants.PARAM_CAPTURE_BID_FROM_HOLDER_MA);
		boolean captureBidFromReceiverMA = (boolean) params.get(TenderDefaultConstants.PARAM_CAPTURE_BID_FROM_RECEIVER_MA);
		boolean captureBidFromExistingData = (boolean) params.get(TenderDefaultConstants.PARAM_CAPTURE_BID_FROM_EXISTING_DATA);

		// Copy Payment Terms from Tender Holder Effective Master Agrement
		if (captureBidFromHolderMA) {
			copyCompleted = this.copyPaymentTerms(bid, bid.getBidTenderIdentification().getHolder());
		}

		// Copy Payment Terms from Tender Location Fuel Receiver Effective Master Agrement
		if (!copyCompleted && captureBidFromReceiverMA && airline != null) {
			copyCompleted = this.copyPaymentTerms(bid, airline.getAirline());
		}

		// Copy Payment Terms from existing contracts and bids
		if (!copyCompleted && captureBidFromExistingData) {
			if (lastContract != null) {
				copyCompleted = this.copyPaymentAndInvoicingTerms(bid, lastContract);
			} else if (lastBid != null) {
				copyCompleted = this.copyPaymentAndInvoicingTerms(bid, lastBid);
			}
		}

		// Copy Payment Terms from Tender Location and default data
		if (!copyCompleted) {
			this.copyPaymentAndInvoicingTerms(bid, tenderLocation);
		}
	}

	private boolean copyPaymentTerms(Contract bid, Customer customer) {
		try {
			MasterAgreement ma = this.customerService.getValidEffectiveMasterAgreement(customer, GregorianCalendar.getInstance().getTime());
			this.copyPaymentAndInvoicingTerms(bid, ma);

			return true;
		} catch (Exception e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
			LOG.warn("Master Agreement not found. Copy payment terms from existing data or tender location.");

			return false;
		}
	}

	private void copyPricing(Contract bid, Contract lastContract, Contract lastBid, TenderLocation tenderLocation) throws BusinessException {
		if (lastContract != null) {
			this.copyPricing(bid, lastContract);
		} else if (lastBid != null) {
			this.copyPricing(bid, lastBid);
		} else {
			this.copyPricing(bid, tenderLocation);
		}
	}

	private Contract getLastBid(TenderLocation tenderLocation, Customer customer) throws BusinessException {
		Contract lastBid = null;
		List<Contract> lastHolderBids = this.contractService.findByHolderLocation(customer, tenderLocation.getLocation(), false);
		if (!CollectionUtils.isEmpty(lastHolderBids)) {
			lastBid = lastHolderBids.get(0);
		} else {
			List<Contract> lastLocationBids = this.contractService.findByLocation(tenderLocation.getLocation(), false);
			if (!CollectionUtils.isEmpty(lastLocationBids)) {
				lastBid = lastLocationBids.get(0);
			}
		}
		return lastBid;
	}

	private void setIplAgent(TenderLocation tenderLocation, Contract bid) {
		if (tenderLocation.getRefuelerCode() != null && tenderLocation.getRefuelerCode().length() > 0) {
			try {
				bid.setIntoPlaneAgent(this.suppliersService.findByCode(tenderLocation.getRefuelerCode()));
			} catch (Exception e) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Unable to set Into Plane Agent. Entity not found", e);
				}
			}
		}
	}

	private boolean copyPaymentAndInvoicingTerms(Contract to, Contract from) {
		to.setSettlementCurr(from.getSettlementCurr());
		to.setSettlementUnit(from.getSettlementUnit());
		to.setContractVolumeUnit(from.getContractVolumeUnit());

		if (!from.getBidPaymentType().equals(PaymentType._EMPTY_)) {
			to.setBidPaymentType(from.getBidPaymentType());
		} else {
			to.setBidPaymentType(PaymentType.getByCode(TenderDefaultConstants.DEFAULT_PAYMENT_TYPE));
		}

		to.setVat(from.getVat());
		to.setPaymentTerms(from.getPaymentTerms());
		to.setPaymentRefDay(from.getPaymentRefDay());
		to.setInvoiceFreq(from.getInvoiceFreq());
		to.setInvoiceFreqNumber(from.getInvoiceFreqNumber());
		to.setInvoiceType(from.getInvoiceType());
		to.setFinancialSource(from.getFinancialSource());
		to.setAverageMethod(from.getAverageMethod());
		to.setExchangeRateOffset(from.getExchangeRateOffset());
		to.setCreditTerms(from.getCreditTerms());
		to.setBidBankGuarantee(from.getBidBankGuarantee());
		to.setBidPrepaidDays(from.getBidPrepaidDays());
		to.setBidPayementFreq(from.getBidPayementFreq());
		to.setBidPrepaidAmount(from.getBidPrepaidAmount());
		to.setBidPrepayFirstDeliveryDate(from.getBidPrepayFirstDeliveryDate());

		return true;
	}

	private void copyPaymentAndInvoicingTerms(Contract to, MasterAgreement from) throws BusinessException {
		// set fields defined and mandatory in master agreement
		to.setSettlementCurr(from.getCurrency());
		to.setFinancialSource(from.getFinancialsource());
		to.setAverageMethod(from.getAverageMethod());
		to.setExchangeRateOffset(from.getPeriod());

		// set default mandatory fields to contract (not existend of added in master agreement)
		Unit settlementUnit = this.subsidiaryService.getDefaultVolumeUnit();

		to.setSettlementUnit(settlementUnit);
		to.setBidPaymentType(PaymentType.getByCode(TenderDefaultConstants.DEFAULT_PAYMENT_TYPE));
		to.setCreditTerms(CreditTerm.getByName(TenderDefaultConstants.DEFAULT_CREDIT_TERM));

		if (from.getPaymentTerms() != null) {
			to.setPaymentTerms(from.getPaymentTerms());
		} else {
			to.setPaymentTerms(TenderDefaultConstants.DEFAULT_PAYMENT_TERMS);
		}

		if (!from.getReferenceTo().equals(PaymentDay._EMPTY_)) {
			to.setPaymentRefDay(from.getReferenceTo());
		} else {
			to.setPaymentRefDay(PaymentDay.getByCode(TenderDefaultConstants.DEFAULT_PAYMENT_REF_DAY));
		}

		if (!from.getInvoiceFrequency().equals(InvoiceFreq._EMPTY_)) {
			to.setInvoiceFreq(from.getInvoiceFrequency());
		} else {
			to.setInvoiceFreq(InvoiceFreq.getByCode(TenderDefaultConstants.DEFAULT_INVOICE_FREQ));
		}

		if (!from.getInvoiceType().equals(InvoiceType._EMPTY_)) {
			to.setInvoiceType(from.getInvoiceType());
		} else {
			to.setInvoiceType(InvoiceType.getByCode(TenderDefaultConstants.DEFAULT_INVOICE_TYPE));
		}

	}

	private void copyPaymentAndInvoicingTerms(Contract to, TenderLocation from) throws BusinessException {
		// payment temrs copy or set defaults for empty data
		if (from.getSettlementCurrency() != null) {
			to.setSettlementCurr(from.getSettlementCurrency());
		} else {
			to.setSettlementCurr(this.subsidiaryService.getDefaultCurrency());
		}

		if (from.getSettlementUnit() != null) {
			to.setSettlementUnit(from.getSettlementUnit());
		} else {
			to.setSettlementUnit(this.subsidiaryService.getDefaultVolumeUnit());
		}

		to.setBidBankGuarantee(false);
		to.setContractVolumeUnit(from.getVolumeUnit());

		if (!from.getPaymentMethod().equals(PaymentType._EMPTY_)) {
			to.setBidPaymentType(from.getPaymentMethod());
		} else {
			to.setBidPaymentType(PaymentType.getByCode(TenderDefaultConstants.DEFAULT_PAYMENT_TYPE));
		}

		if (from.getPaymentTerms() != null) {
			to.setPaymentTerms(from.getPaymentTerms().intValue());
		} else {
			to.setPaymentTerms(TenderDefaultConstants.DEFAULT_PAYMENT_TERMS);
		}

		if (!from.getPaymentRefDate().equals(PaymentDay._EMPTY_)) {
			to.setPaymentRefDay(from.getPaymentRefDate());
		} else {
			to.setPaymentRefDay(PaymentDay.getByCode(TenderDefaultConstants.DEFAULT_PAYMENT_REF_DAY));
		}

		if (!from.getInvoiceFrequency().equals(InvoiceFreq._EMPTY_)) {
			to.setInvoiceFreq(from.getInvoiceFrequency());
		} else {
			to.setInvoiceFreq(InvoiceFreq.getByCode(TenderDefaultConstants.DEFAULT_INVOICE_FREQ));
		}

		if (!from.getInvoiceType().equals(InvoiceType._EMPTY_)) {
			to.setInvoiceType(from.getInvoiceType());
		} else {
			to.setInvoiceType(InvoiceType.getByCode(TenderDefaultConstants.DEFAULT_INVOICE_TYPE));
		}

		if (from.getFinancialSource() != null) {
			to.setFinancialSource(from.getFinancialSource());
		} else {
			to.setFinancialSource(this.subsidiaryService.getDefaultFinancialSource());
		}

		if (from.getAveragingMethod() != null) {
			to.setAverageMethod(from.getAveragingMethod());
		} else {
			to.setAverageMethod(this.subsidiaryService.getDefaultAverageMethod());
		}

		if (!from.getExchangeRateOffset().equals(MasterAgreementsPeriod._EMPTY_)) {
			to.setExchangeRateOffset(from.getExchangeRateOffset());
		} else {
			to.setExchangeRateOffset(MasterAgreementsPeriod.getByDelta(TenderDefaultConstants.DEFAULT_EXCHANGE_RATE_OFFSET));
		}

		// invoicing terms copy or set defaults for empty data
		to.setBidPayementFreq(from.getPrepaymentFrequency());
		to.setBidPrepaidAmount(from.getPrepaymentAmount());

		if (!from.getCreditTerms().equals(PaymentChoise._EMPTY_)) {
			to.setCreditTerms(CreditTerm.getByName(from.getCreditTerms().getName()));
		} else {
			to.setCreditTerms(CreditTerm.getByName(TenderDefaultConstants.DEFAULT_CREDIT_TERM));
		}

		if (from.getNumberOfDaysPrepaid() != null) {
			to.setBidPrepaidDays(from.getNumberOfDaysPrepaid().intValue());
		}

		if (from.getPrepaymentDayOffset() != null) {
			to.setBidPrepayFirstDeliveryDate(from.getPrepaymentDayOffset().intValue());
		}
	}

	private void copyPricing(Contract to, Contract from) throws BusinessException {
		try {
			to.setPricingBases(this.getBusinessDelegate(PriceBuilderDelegate.class).getPricingBases(from, to));
		} catch (InstantiationException | IllegalAccessException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
			throw new BusinessException(CmmErrorCode.BID_TENDER_COPY_BID_ERROR, CmmErrorCode.BID_TENDER_COPY_BID_ERROR.getErrMsg(), e);
		}
	}

	private void copyPricing(Contract contract, TenderLocation tenderLocation) {
		try {
			if (tenderLocation.getPricingBase().equals(PricingType._INDEX_)) {
				this.buildIndexPrice(contract, tenderLocation);
			} else if (tenderLocation.getPricingBase().equals(PricingType._MARKET_)) {
				this.buildMarketPrice(contract, tenderLocation);
			}
		} catch (BusinessException e) {
			LOG.warn(e.getMessage(), e);
		}
	}

	private void buildMarketPrice(Contract contract, TenderLocation tenderLocation) throws BusinessException {
		if (tenderLocation.getRate() != null && tenderLocation.getRate().compareTo(BigDecimal.ZERO) > 0) {
			PriceCategory priceCat = this.priceCategoryService.findbyName(PricingType._MARKET_.getCode());

			PricingBase pricingBase = new PricingBase();
			pricingBase.setPriceCat(priceCat);
			pricingBase.setDescription(PricingType._MARKET_.getCode());
			pricingBase.setValidFrom(tenderLocation.getAgreementFrom());
			pricingBase.setValidTo(tenderLocation.getAgreementTo());

			pricingBase.setInitialPrice(tenderLocation.getRate());

			if (tenderLocation.getMarketCurrency() != null) {
				pricingBase.setInitialCurrId(tenderLocation.getMarketCurrency().getId());
			} else {
				pricingBase.setInitialCurrId(this.subsidiaryService.getDefaultCurrency().getId());
			}

			if (tenderLocation.getMarketPricingUnit() != null) {
				pricingBase.setInitialUnitId(tenderLocation.getMarketPricingUnit().getId());
			} else {
				pricingBase.setInitialUnitId(this.subsidiaryService.getDefaultVolumeUnit().getId());
			}

			pricingBase.setVat(VatApplicability.getByName(TenderDefaultConstants.DEFAULT_VAT_APPLICABILITY));
			pricingBase.setFinancialSourceId(this.subsidiaryService.getDefaultFinancialSource().getId());
			pricingBase.setAverageMethodId(this.subsidiaryService.getDefaultAverageMethod().getId());
			pricingBase.setExchangeRateOffset(MasterAgreementsPeriod.getByDelta(TenderDefaultConstants.DEFAULT_EXCHANGE_RATE_OFFSET));

			contract.addToPricingBases(pricingBase);

			SoneEnumUtils.setEmptyValues(pricingBase);
			this.getBusinessDelegate(PricingBase_Bd.class).createPriceCtgry(priceCat, pricingBase, null);
		}
	}

	private void buildIndexPrice(Contract contract, TenderLocation tenderLocation) throws BusinessException {
		PricingBase pricingBase = new PricingBase();
		Quotation quotation = this.getQuotation(tenderLocation);

		if (quotation != null) {
			pricingBase.setQuotation(quotation);
			pricingBase.setValidFrom(tenderLocation.getAgreementFrom());
			pricingBase.setValidTo(tenderLocation.getAgreementTo());
			pricingBase.setVat(VatApplicability.getByName(TenderDefaultConstants.DEFAULT_VAT_APPLICABILITY));
			pricingBase.setDescription(PricingType._INDEX_.getCode());
			pricingBase.setDecimals(quotation.getTimeseries().getDecimals());

			if (tenderLocation.getDensityConvToUnit() != null) {
				pricingBase.setConvUnit(tenderLocation.getDensityConvToUnit());
			} else {
				pricingBase.setConvUnit(tenderLocation.getIndexProvider().getUnit2Id());
			}

			if (!Operator._EMPTY_.equals(tenderLocation.getOperator())) {
				pricingBase.setOperator(tenderLocation.getOperator());
			} else {
				pricingBase.setOperator(tenderLocation.getIndexProvider().getConvFctr());
			}

			if (tenderLocation.getConversionFactor() != null) {
				pricingBase.setFactor(tenderLocation.getConversionFactor());
			} else {
				pricingBase.setFactor(tenderLocation.getIndexProvider().getArithmOper());
			}

			if (!MasterAgreementsPeriod._EMPTY_.equals(tenderLocation.getIndexAveragingPeriod())) {
				pricingBase.setQuotationOffset(tenderLocation.getIndexAveragingPeriod());
			} else {
				pricingBase.setQuotationOffset(MasterAgreementsPeriod.getByDelta(TenderDefaultConstants.DEFAULT_EXCHANGE_RATE_OFFSET));
			}

			PriceCategory priceCat = this.priceCategoryService.findbyName(PricingType._INDEX_.getCode());
			pricingBase.setPriceCat(priceCat);
			contract.addToPricingBases(pricingBase);

			SoneEnumUtils.setEmptyValues(pricingBase);
			this.getBusinessDelegate(PricingBase_Bd.class).createPriceCtgry(priceCat, pricingBase, null);
		}
	}

	private Quotation getQuotation(TenderLocation from) throws BusinessException {
		Quotation quotation = null;
		try {
			if (from.getIndexProvider() != null && from.getIndexAveragingMethod() != null) {
				quotation = this.quotationService.getByTimeseriesAndAvgMethod(from.getIndexProvider().getId(), from.getIndexAveragingMethod());
			}
		} catch (EntityNotFoundException e) {
			throw new BusinessException(CmmErrorCode.QUOTATION_NOT_FOUND, String.format(CmmErrorCode.QUOTATION_NOT_FOUND.getErrMsg(),
					from.getIndexProvider().getExternalSerieName(), from.getIndexAveragingMethod().getName()), e);
		}

		return quotation;
	}

}
