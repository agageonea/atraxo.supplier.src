package atraxo.cmm.business.ext.bpm.delegate.priceCategory.approval;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.custom.workflow.IWorkflowTerminatorService;
import atraxo.fmbas.business.ext.bpm.WorkflowMsgConstants;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * @author vhojda
 */
public class PriceCategoryApprovalWorkflowTerminator extends AbstractBusinessBaseService implements IWorkflowTerminatorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PriceCategoryApprovalWorkflowTerminator.class);

	@Autowired
	private IContractService contractSrv;

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.api.custom.workflow.IWorkflowTerminatorService#terminateWorkflow(atraxo.fmbas.domain.impl.workflow.WorkflowInstance,
	 * java.lang.String)
	 */
	@Override
	public void terminateWorkflow(WorkflowInstance workflowInstance, String reason) throws BusinessException {

		List<WorkflowInstanceEntity> entities = new ArrayList<>(workflowInstance.getEntitites());

		for (WorkflowInstanceEntity entity : entities) {
			// reject the contract
			Contract contract = null;
			try {
				contract = this.contractSrv.findByBusiness(entity.getObjectId());
			} catch (Exception e) {
				LOGGER.warn("WARNING: could not retrieve a contract for ID " + entity.getObjectId()
						+ " ! Will not update the entity since it doesn't exist !", e);
			}
			if (contract != null) {
				this.contractSrv.rejectPriceUpdate(contract,
						StringUtils.isEmpty(reason) ? WorkflowMsgConstants.APPROVAL_PRICE_WORKFLOW_TERMINATED_COMMENT : reason);

			}
		}
	}
}
