/**
 *
 */
package atraxo.cmm.business.ext.contracts.service;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.history.ChangeHistory;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public abstract class AbstractDataHubClientRequestService<E extends AbstractEntity> {

	@Autowired
	private IChangeHistoryService historyService;

	protected void generateSaveToHistory(E entity, String objectValue, String remark, Class<E> clazz) throws BusinessException {
		ChangeHistory ch = new ChangeHistory();
		ch.setObjectId(entity.getId());
		ch.setObjectType(clazz.getSimpleName());
		ch.setObjectValue(objectValue);
		ch.setRemarks(remark);
		this.historyService.insert(ch);
	}

}
