/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.priceUpdate;

import atraxo.cmm.business.api.priceUpdate.IPriceUpdateCategoriesService;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdate;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdateCategories;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link PriceUpdateCategories} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class PriceUpdateCategories_Service
		extends
			AbstractEntityService<PriceUpdateCategories>
		implements
			IPriceUpdateCategoriesService {

	/**
	 * Public constructor for PriceUpdateCategories_Service
	 */
	public PriceUpdateCategories_Service() {
		super();
	}

	/**
	 * Public constructor for PriceUpdateCategories_Service
	 * 
	 * @param em
	 */
	public PriceUpdateCategories_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<PriceUpdateCategories> getEntityClass() {
		return PriceUpdateCategories.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return PriceUpdateCategories
	 */
	public PriceUpdateCategories findByBKey(PriceUpdate priceUpdate,
			String contract, Integer contractPriceCatId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(PriceUpdateCategories.NQ_FIND_BY_BKEY,
							PriceUpdateCategories.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("priceUpdate", priceUpdate)
					.setParameter("contract", contract)
					.setParameter("contractPriceCatId", contractPriceCatId)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"PriceUpdateCategories",
							"priceUpdate, contract, contractPriceCatId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"PriceUpdateCategories",
							"priceUpdate, contract, contractPriceCatId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return PriceUpdateCategories
	 */
	public PriceUpdateCategories findByBKey(Long priceUpdateId,
			String contract, Integer contractPriceCatId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							PriceUpdateCategories.NQ_FIND_BY_BKEY_PRIMITIVE,
							PriceUpdateCategories.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("priceUpdateId", priceUpdateId)
					.setParameter("contract", contract)
					.setParameter("contractPriceCatId", contractPriceCatId)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"PriceUpdateCategories",
							"priceUpdateId, contract, contractPriceCatId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"PriceUpdateCategories",
							"priceUpdateId, contract, contractPriceCatId"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return PriceUpdateCategories
	 */
	public PriceUpdateCategories findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							PriceUpdateCategories.NQ_FIND_BY_BUSINESS,
							PriceUpdateCategories.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"PriceUpdateCategories", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"PriceUpdateCategories", "id"), nure);
		}
	}

	/**
	 * Find by reference: priceUpdate
	 *
	 * @param priceUpdate
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByPriceUpdate(PriceUpdate priceUpdate) {
		return this.findByPriceUpdateId(priceUpdate.getId());
	}
	/**
	 * Find by ID of reference: priceUpdate.id
	 *
	 * @param priceUpdateId
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByPriceUpdateId(Integer priceUpdateId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdateCategories e where e.clientId = :clientId and e.priceUpdate.id = :priceUpdateId",
						PriceUpdateCategories.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("priceUpdateId", priceUpdateId).getResultList();
	}
	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByLocation(Locations location) {
		return this.findByLocationId(location.getId());
	}
	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByLocationId(Integer locationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdateCategories e where e.clientId = :clientId and e.location.id = :locationId",
						PriceUpdateCategories.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("locationId", locationId).getResultList();
	}
	/**
	 * Find by reference: contractHolder
	 *
	 * @param contractHolder
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByContractHolder(
			Customer contractHolder) {
		return this.findByContractHolderId(contractHolder.getId());
	}
	/**
	 * Find by ID of reference: contractHolder.id
	 *
	 * @param contractHolderId
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByContractHolderId(
			Integer contractHolderId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdateCategories e where e.clientId = :clientId and e.contractHolder.id = :contractHolderId",
						PriceUpdateCategories.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractHolderId", contractHolderId)
				.getResultList();
	}
	/**
	 * Find by reference: counterparty
	 *
	 * @param counterparty
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByCounterparty(Customer counterparty) {
		return this.findByCounterpartyId(counterparty.getId());
	}
	/**
	 * Find by ID of reference: counterparty.id
	 *
	 * @param counterpartyId
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByCounterpartyId(
			Integer counterpartyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdateCategories e where e.clientId = :clientId and e.counterparty.id = :counterpartyId",
						PriceUpdateCategories.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("counterpartyId", counterpartyId).getResultList();
	}
	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByCurrency(Currencies currency) {
		return this.findByCurrencyId(currency.getId());
	}
	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByCurrencyId(Integer currencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdateCategories e where e.clientId = :clientId and e.currency.id = :currencyId",
						PriceUpdateCategories.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currencyId", currencyId).getResultList();
	}
	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByUnit(Unit unit) {
		return this.findByUnitId(unit.getId());
	}
	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByUnitId(Integer unitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdateCategories e where e.clientId = :clientId and e.unit.id = :unitId",
						PriceUpdateCategories.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitId", unitId).getResultList();
	}
}
