package atraxo.cmm.business.ext.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.fmbas.domain.ext.types.Offset;
import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Utility class to calculate price components start and/or end dates.
 *
 * @author zspeter
 */
/**
 * @author vhojda
 */
public final class DateUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(DateUtils.class);

	private DateUtils() {
		// private constructor because this class is an utility class and it doesn't need to be created objects out of it
	}

	/**
	 * @param pb
	 * @param qv
	 * @return
	 * @throws BusinessException
	 */
	public static DatePeriod calculateContractPriceComponentPeriod(PricingBase pb, QuotationValue qv) {
		AverageMethodCodeFrequency frequency = AverageMethodCodeFrequency.getByAverageMethod(pb.getQuotation().getAvgMethodIndicator());

		String offset = pb.getQuotationOffset().getName();
		Integer amountOffset = Offset.getByName(offset.toLowerCase()).getDelta() * (-1);

		AverageMethodCodeFrequencyPeriodCalculator calc = new AverageMethodCodeFrequencyPeriodCalculator(frequency, qv.getValidFromDate(),
				amountOffset);
		return calc.calculatePeriod();
	}

	/**
	 * Used for the special case regarding quotions.
	 *
	 * @param previousComponent
	 * @param pb
	 * @param qv
	 * @return
	 * @throws BusinessException
	 */
	public static DatePeriod calculateContractPriceComponentPeriod(ContractPriceComponent previousComponent, PricingBase pb, QuotationValue qv) {

		AverageMethodCodeFrequency frequency = AverageMethodCodeFrequency.getByAverageMethod(pb.getQuotation().getAvgMethodIndicator());

		String offset = pb.getQuotationOffset().getName();
		Integer amountOffset = Offset.getByName(offset.toLowerCase()).getDelta() * (-1);

		AverageMethodCodeFrequencyPeriodCalculator calc = new AverageMethodCodeFrequencyPeriodCalculator(frequency, qv.getValidFromDate(),
				amountOffset);
		DatePeriod datePeriod = calc.calculatePeriod();
		Date validFrom = datePeriod.getStartDate().getTime();
		Date validTo = datePeriod.getEndDate().getTime();

		if (validTo.before(previousComponent.getValidFrom()) || validTo.after(previousComponent.getValidFrom())) {
			validTo = org.apache.commons.lang.time.DateUtils.addDays(previousComponent.getValidFrom(), -1);
		}

		return new DatePeriod(validFrom, validTo);
	}

	/**
	 * Calculate the last day of month.
	 *
	 * @param date - {@link Date}.
	 * @param quantity - {@link Integer}.
	 * @param dateUnit -{@link Integer}
	 * @return - {@link} last day of month.
	 */
	public static Date calculateDateWithOffset(Date date, int quantity, int dateUnit) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(dateUnit, quantity);
		return cal.getTime();
	}

	/**
	 * Compares two dates, having the option to ignore the hour/minutes/milisecconds so that only the DAY is taken in consideration
	 *
	 * @param date1 the first date
	 * @param date2 the second date
	 * @param ignoreHour true if hour should be ignored, false otherwise
	 * @returns -1 (lower) if the first date is before the second one, 0 (equal) if the two dates are the same or 1 (higher) if the first one is after
	 *          the second one
	 */
	public static int compareDates(Date date1, Date date2, boolean ignoreHour) {
		if (ignoreHour) {
			Calendar cal1 = getCalendarFromDateIgnoreHours(date1);
			Calendar cal2 = getCalendarFromDateIgnoreHours(date2);
			return cal1.compareTo(cal2);
		} else {
			return date1.compareTo(date2);
		}
	}

	/**
	 * Compares two dates, having the option to ignore the hour/minutes/milisecconds so that only the DAY is taken in consideration
	 *
	 * @param date1 the first date
	 * @param date2 the second date
	 * @param ignoreHour true if hour should be ignored, false otherwise
	 * @returns -1 (lower) if the first date is before the second one, 0 (equal) if the two dates are the same or 1 (higher) if the first one is after
	 *          the second one
	 */
	public static int compareDates(Calendar date1, Calendar date2, boolean ignoreHour) {
		return compareDates(date1.getTime(), date2.getTime(), ignoreHour);
	}

	/**
	 * @param date
	 * @return
	 */
	public static Calendar getCalendarFromDateIgnoreHours(Date date) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.set(Calendar.HOUR, 12);
		cal.set(Calendar.AM_PM, Calendar.AM);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal;
	}

	/**
	 * @param cal
	 * @return
	 */
	public static Calendar getCalendarFromCalendarIgnoreHours(Calendar cal) {
		return getCalendarFromDateIgnoreHours(cal.getTime());
	}

	/**
	 * @param date
	 * @return
	 */
	public static boolean inPastMonth(Date date) {
		Calendar presentCal = Calendar.getInstance();
		int monthPresent = presentCal.get(Calendar.YEAR) * 12 + presentCal.get(Calendar.MONTH);

		Calendar dateCal = Calendar.getInstance();
		dateCal.setTime(date);
		int monthDate = dateCal.get(Calendar.YEAR) * 12 + dateCal.get(Calendar.MONTH);
		return monthDate < monthPresent;
	}

	/**
	 * @return
	 */
	public static Calendar getPresentIgnoreHours() {
		return getCalendarFromDateIgnoreHours(new Date());
	}

	/**
	 * Gets the next day of the week (Monday, Tuesday, etc) starting from a specifric date
	 *
	 * @param initiaCalendar
	 * @param calendarDayOfWeek
	 * @return
	 */
	public static Calendar getNextCalendarDayOfTheWeek(Calendar initiaCalendar, int calendarDayOfWeek) {
		Calendar cal = DateUtils.getCalendarFromCalendarIgnoreHours(initiaCalendar);
		if (cal.get(Calendar.DAY_OF_WEEK) == calendarDayOfWeek) {
			return cal;
		} else {
			cal.add(Calendar.DAY_OF_MONTH, 1);
			return getNextCalendarDayOfTheWeek(cal, calendarDayOfWeek);
		}
	}

	/**
	 * Calculates the number of different months between two dates
	 *
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static int monthsBetweenDates(Date d1, Date d2) {
		int months;

		Calendar c1 = Calendar.getInstance();
		c1.setTime(d1);
		Calendar c2 = Calendar.getInstance();
		c2.setTime(d2);

		int months1 = c1.get(Calendar.YEAR) * 12 + c1.get(Calendar.MONTH);
		int months2 = c2.get(Calendar.YEAR) * 12 + c2.get(Calendar.MONTH);

		if (d1.after(d2)) {
			months = months1 - months2;
		} else {
			months = months2 - months1;
		}

		return months;
	}

	/**
	 * @param date
	 * @return
	 */
	public static boolean isMonday(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY;
	}

	public static Date removeTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/**
	 * Check if the date is in the period
	 *
	 * @param date
	 * @param periodFrom
	 * @param periodTo
	 * @param ignoreHour
	 * @return
	 */
	public static Boolean dateInPeriod(Date date, Date periodFrom, Date periodTo, boolean ignoreHour) {
		boolean isDateAfterPeriodFrom = compareDates(date, periodFrom, ignoreHour) >= 0;
		boolean isDateBeforePeriodTo = compareDates(date, periodTo, ignoreHour) <= 0;

		return isDateAfterPeriodFrom && isDateBeforePeriodTo;
	}

}
