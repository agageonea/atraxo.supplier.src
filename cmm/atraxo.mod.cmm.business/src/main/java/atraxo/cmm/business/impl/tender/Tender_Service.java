/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.tender;

import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.customer.Customer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Tender} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Tender_Service extends AbstractEntityService<Tender> {

	/**
	 * Public constructor for Tender_Service
	 */
	public Tender_Service() {
		super();
	}

	/**
	 * Public constructor for Tender_Service
	 * 
	 * @param em
	 */
	public Tender_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Tender> getEntityClass() {
		return Tender.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Tender
	 */
	public Tender findByCode(String code, Long tenderVersion, Customer holder) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Tender.NQ_FIND_BY_CODE, Tender.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("code", code)
					.setParameter("tenderVersion", tenderVersion)
					.setParameter("holder", holder).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Tender", "code, tenderVersion, holder"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Tender", "code, tenderVersion, holder"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Tender
	 */
	public Tender findByCode(String code, Long tenderVersion, Long holderId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Tender.NQ_FIND_BY_CODE_PRIMITIVE,
							Tender.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("code", code)
					.setParameter("tenderVersion", tenderVersion)
					.setParameter("holderId", holderId).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Tender", "code, tenderVersion, holderId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Tender", "code, tenderVersion, holderId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Tender
	 */
	public Tender findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Tender.NQ_FIND_BY_BUSINESS, Tender.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Tender", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Tender", "id"), nure);
		}
	}

	/**
	 * Find by reference: holder
	 *
	 * @param holder
	 * @return List<Tender>
	 */
	public List<Tender> findByHolder(Customer holder) {
		return this.findByHolderId(holder.getId());
	}
	/**
	 * Find by ID of reference: holder.id
	 *
	 * @param holderId
	 * @return List<Tender>
	 */
	public List<Tender> findByHolderId(Integer holderId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Tender e where e.clientId = :clientId and e.holder.id = :holderId",
						Tender.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("holderId", holderId).getResultList();
	}
	/**
	 * Find by reference: contact
	 *
	 * @param contact
	 * @return List<Tender>
	 */
	public List<Tender> findByContact(Contacts contact) {
		return this.findByContactId(contact.getId());
	}
	/**
	 * Find by ID of reference: contact.id
	 *
	 * @param contactId
	 * @return List<Tender>
	 */
	public List<Tender> findByContactId(Integer contactId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Tender e where e.clientId = :clientId and e.contact.id = :contactId",
						Tender.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contactId", contactId).getResultList();
	}
	/**
	 * Find by reference: tenderLocation
	 *
	 * @param tenderLocation
	 * @return List<Tender>
	 */
	public List<Tender> findByTenderLocation(TenderLocation tenderLocation) {
		return this.findByTenderLocationId(tenderLocation.getId());
	}
	/**
	 * Find by ID of reference: tenderLocation.id
	 *
	 * @param tenderLocationId
	 * @return List<Tender>
	 */
	public List<Tender> findByTenderLocationId(Integer tenderLocationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Tender e, IN (e.tenderLocation) c where e.clientId = :clientId and c.id = :tenderLocationId",
						Tender.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("tenderLocationId", tenderLocationId)
				.getResultList();
	}
}
