package atraxo.cmm.business.ext.priceUpdate.job;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.api.prices.IContractPriceComponentService;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdate;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdateCategories;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Processor for Price Update publishing job.
 *
 * @author zspeter
 */
public class PriceMaintenanceProcessor implements ItemProcessor<PriceUpdate, PriceUpdate> {

	private static final Logger LOG = LoggerFactory.getLogger(PriceMaintenanceProcessor.class);
	static final String SKIP = "skip";
	static final String UPD = "upd";
	@Autowired
	private IContractPriceCategoryService catSrv;
	@Autowired
	private IContractPriceComponentService compSrv;
	@Autowired
	private IUserSuppService userSrv;

	private ExecutionContext executionContext;

	/**
	 * @param stepExecution
	 */
	@BeforeStep
	public void storeExecutionContext(StepExecution stepExecution) {
		this.executionContext = stepExecution.getExecutionContext();
	}

	@SuppressWarnings("unchecked")
	@Override
	public PriceUpdate process(PriceUpdate item) throws Exception {
		List<ContractPriceComponent> insList = new ArrayList<>();
		List<ContractPriceComponent> updList = new ArrayList<>();
		Set<String> updContracts = new HashSet<>();
		Set<String> skipedContract = new HashSet<>();
		if (this.executionContext.containsKey(UPD)) {
			updContracts = (Set<String>) this.executionContext.get(UPD);
		}

		if (this.executionContext.containsKey(SKIP)) {
			skipedContract = (Set<String>) this.executionContext.get(SKIP);
		}

		for (PriceUpdateCategories e : item.getPriceUpdateCategories()) {
			if (!e.getSelected()) {
				continue;
			}
			ContractPriceCategory category = this.catSrv.findById(e.getContractPriceCatId());
			if (category != null) {
				this.manageCategory(item, insList, updList, updContracts, skipedContract, e, category);
				// this.manageBlueprintCategory(item, insList, updList, updContracts, skipedContract, e, category);
			} else {
				skipedContract.add(e.getContract());
			}

		}
		if (!insList.isEmpty()) {
			this.compSrv.insert(insList);
		}
		if (!updList.isEmpty()) {
			this.compSrv.update(updList);
		}
		item.setPublished(true);
		item.setPublishedOn(Calendar.getInstance().getTime());
		try {
			UserSupp user = this.userSrv.findByLogin(Session.user.get().getCode());
			item.setPublishedBy(user);
		} catch (ApplicationException e) {
			LOG.warn(e.getMessage(), e);
		}
		this.executionContext.put(UPD, updContracts);
		this.executionContext.put(SKIP, skipedContract);
		return item;
	}

	private void manageBlueprintCategory(PriceUpdate item, List<ContractPriceComponent> insList, List<ContractPriceComponent> updList,
			Set<String> updContracts, Set<String> skipedContract, PriceUpdateCategories e, ContractPriceCategory category) throws BusinessException {
		ContractPriceCategory blueprintCategory = this.catSrv.findBluePrintByResaleRef(category);
		if ((blueprintCategory != null) && blueprintCategory.getContract().getIsBlueprint()) {
			this.manageCategory(item, insList, updList, updContracts, skipedContract, e, blueprintCategory);

		}
	}

	private void manageCategory(PriceUpdate item, List<ContractPriceComponent> insList, List<ContractPriceComponent> updList,
			Set<String> updContracts, Set<String> skipedContract, PriceUpdateCategories e, ContractPriceCategory category) {
		if ((category != null) && (category.getContract().getValidFrom().compareTo(item.getValidFrom()) <= 0)
				&& (category.getContract().getValidTo().compareTo(item.getValidFrom()) >= 0)) {
			ContractPriceComponent comp = new ContractPriceComponent();
			comp.setValidFrom(item.getValidFrom());
			comp.setValidTo(category.getContract().getValidTo());
			comp.setContrPriceCtgry(category);
			if (category.getPriceComponents().contains(comp)) {
				comp = this.changePriceComponenet(category, comp);
				updList.add(comp);
			} else {
				insList.add(comp);
			}
			comp.setWithHold(false);
			comp.setCurrency(item.getCurrency());
			comp.setPrice(item.getNewPrice());
			comp.setUnit(item.getUnit());
			comp.setProvisional(false);
			if (!category.getContract().getIsBlueprint()) {
				updContracts.add(category.getContract().getCode());
			}
		} else {
			if ((category != null) && !category.getContract().getIsBlueprint()) {
				skipedContract.add(e.getContract());
			}
		}
	}

	private ContractPriceComponent changePriceComponenet(ContractPriceCategory category, ContractPriceComponent comp) {
		for (ContractPriceComponent existing : category.getPriceComponents()) {
			if (existing.equals(comp)) {
				return existing;
			}
		}
		return comp;
	}
}
