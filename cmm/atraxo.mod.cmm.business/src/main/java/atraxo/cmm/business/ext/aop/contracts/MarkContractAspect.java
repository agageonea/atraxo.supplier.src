package atraxo.cmm.business.ext.aop.contracts;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

import atraxo.cmm.business.api.contracts.IMarkedContractService;
import atraxo.cmm.business.ext.contracts.service.ContractUtil;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.MarkedContract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;

/**
 * @author zspeter
 */
@Aspect
@Order(1)
public class MarkContractAspect {

	private static final String MSG = "Mark contract aspect when updating from contract price subcomponents";
	private static final Logger LOG = LoggerFactory.getLogger(MarkContractAspect.class);

	@Autowired
	private IMarkedContractService mcSrv;

	/**
	 * @param jp
	 * @throws BusinessException
	 */
	@After("execution(* *.update(..)) && bean(ContractPrice*)")
	public void markContractUpdate(JoinPoint jp) throws BusinessException {
		if (LOG.isDebugEnabled()) {
			LOG.debug(MSG);
		}
		this.markContract(jp);
	}

	/**
	 * @param jp
	 * @throws BusinessException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	@After("execution(* *.publishBlueprint(..)) && bean(Contract*)")
	public void markPublishBlueprint(JoinPoint jp) throws BusinessException {
		if (LOG.isDebugEnabled()) {
			LOG.debug(MSG);
		}
		for (Object obj : jp.getArgs()) {
			if (obj instanceof AbstractEntity) {
				try {
					this.markContract(obj);
				} catch (IllegalAccessException | InvocationTargetException e) {
					LOG.error("Contract cannot be retrived using reflection.", e);
					throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
				}
			}
		}
	}

	/**
	 * @param jp
	 * @throws BusinessException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	@After("(execution(* *.approvePeriodUpdate(..)) || execution(* *.approvePriceUpdate(..)) || execution(* *.approveShipToUpdate(..))) && bean(Contract*)")
	public void markApproveContractWorkflows(JoinPoint jp) throws BusinessException {
		if (LOG.isDebugEnabled()) {
			LOG.debug(MSG);
		}
		for (Object obj : jp.getArgs()) {
			if (obj instanceof Contract) {
				Contract c = (Contract) obj;
				if (ContractUtil.checkIfCanPublishAutomatically(c)) {
					this.createMarkContract(c.getBlueprintOriginalContractReference());
				}
			}
		}
	}

	/**
	 * @param jp
	 * @throws BusinessException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	@After("execution(* *.persistChanges(..)) && bean(Contract*)")
	public void markOnPropagate(JoinPoint jp) throws BusinessException {
		if (LOG.isDebugEnabled()) {
			LOG.debug(MSG);
		}
		for (Object obj : jp.getArgs()) {
			if (obj instanceof Contract) {
				Contract c = (Contract) obj;
				this.createMarkContract(c.getId());
			}
		}
	}

	/**
	 * @param jp
	 * @throws BusinessException
	 */
	@Before("execution(* *.deleteByIds(..)) && bean(ContractPriceComponent*)")
	public void markContractDeleteFromComp(JoinPoint jp) throws BusinessException {
		if (LOG.isDebugEnabled()) {
			LOG.debug(MSG);
		}
		this.markContractOnDeleteComponent(jp);
	}

	/**
	 * @param jp
	 * @throws BusinessException
	 */
	@Before("execution(* *.deleteByIds(..)) && bean(PricingBase*)")
	public void markContractDeleteFromPb(JoinPoint jp) throws BusinessException {
		if (LOG.isDebugEnabled()) {
			LOG.debug(MSG);
		}
		this.markContractOnDeletePriceBase(jp);
	}

	/**
	 * @param jp
	 * @throws BusinessException
	 */
	@After("execution(* *.insert(..)) && bean(ContractPrice*)")
	public void markContractInsert(JoinPoint jp) throws BusinessException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Mark contract aspect when inserting contract price subcomponents");
		}
		this.markContract(jp);
	}

	/**
	 * @param jp
	 * @throws BusinessException
	 */
	@After("execution(* *.update(..)) && bean(PricingBase)")
	public void markContractPBUpdate(JoinPoint jp) throws BusinessException {
		LOG.info("entering aspect");
		if (LOG.isDebugEnabled()) {
			LOG.debug("Mark contract aspect on pricing base update");
		}
		this.markContract(jp);
	}

	/**
	 * @param jp
	 * @throws BusinessException
	 */
	@After("execution(* *.updatePricingBase(..)) && bean(PricingBase)")
	public void markContractPBupdatePricingBase(JoinPoint jp) throws BusinessException {
		LOG.info("entering aspect");
		if (LOG.isDebugEnabled()) {
			LOG.debug("Mark contract aspect on pricing base update");
		}
		this.markContract(jp);
	}

	/**
	 * @param jp
	 * @throws BusinessException
	 */
	@After("execution(* *.insert(..)) && bean(PricingBase)")
	public void markContractPBInsert(JoinPoint jp) throws BusinessException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Mark contract aspect on pricing base insert");
		}
		this.markContract(jp);
	}

	private void markContract(Object obj) throws IllegalAccessException, InvocationTargetException, BusinessException {
		Class<?> type = obj.getClass();
		Contract c = null;
		if (obj instanceof Contract) {
			c = (Contract) obj;
		} else {
			for (Method m : type.getDeclaredMethods()) {
				if (m.getReturnType().equals(ContractPriceComponent.class) && m.getName().startsWith("get")) {
					ContractPriceComponent comp = (ContractPriceComponent) m.invoke(obj);
					c = comp.getContrPriceCtgry().getContract();
				}
				if (m.getReturnType().equals(ContractPriceCategory.class) && m.getName().startsWith("get")) {
					ContractPriceCategory cpc = (ContractPriceCategory) m.invoke(obj);
					c = cpc.getContract();
				}
				if (m.getReturnType().equals(Contract.class) && m.getName().startsWith("get")) {
					c = (Contract) m.invoke(obj);
				}
				if (c != null) {
					break;
				}
			}
		}
		if (c != null) {
			if ((c.getIsBlueprint() == null || !c.getIsBlueprint())
					&& (!type.equals(ContractPriceCategory.class) || (type.equals(ContractPriceCategory.class)
							&& (((ContractPriceCategory) obj).getOrderChanged() == null || !((ContractPriceCategory) obj).getOrderChanged())))) {
				this.createMarkContract(c.getId());
			}
		}
	}

	private void createMarkContract(Integer cId) throws BusinessException {
		MarkedContract mc = new MarkedContract();
		mc.setCompute(true);
		mc.setContractId(cId);
		try {
			this.mcSrv.findByContractId(cId);
			LOG.info("Contract already marked or cannot mark it.");
		} catch (ApplicationException e) {
			LOG.info("Could not find contract for id " + cId + "! Will insert this new Marked Contract !");
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
			this.mcSrv.insert(mc);
		}
	}

	private void markContractOnDeleteComponent(JoinPoint jp) throws BusinessException {
		for (Object obj : jp.getArgs()) {
			if (obj instanceof Integer) {
				this.markContractFromComponent((Integer) obj);
			} else if (obj instanceof List) {
				List<?> list = (List<?>) obj;
				if (!list.isEmpty()) {
					for (Object o : list) {
						this.markContractFromComponent((Integer) o);
					}
				}
			}
		}
	}

	private void markContractOnDeletePriceBase(JoinPoint jp) throws BusinessException {
		for (Object obj : jp.getArgs()) {
			if (obj instanceof Integer) {
				this.markContractFromPriceBases((Integer) obj);
			} else if (obj instanceof List) {
				List<?> list = (List<?>) obj;
				if (!list.isEmpty()) {
					this.markContractFromPriceBases((Integer) list.get(0));
				}
			}
		}
	}

	private void markContractFromPriceBases(Integer pbId) throws BusinessException {
		PricingBase pb = this.mcSrv.findById(pbId, PricingBase.class);
		if (pb != null && (pb.getContract().getIsBlueprint() == null || !pb.getContract().getIsBlueprint())) {
			this.createMarkContract(pb.getContract().getId());
		}
	}

	private void markContractFromComponent(Integer cpcId) throws BusinessException {
		if (cpcId == null) {
			return;
		}
		ContractPriceComponent comp = this.mcSrv.findById(cpcId, ContractPriceComponent.class);
		if (comp != null
				&& (comp.getContrPriceCtgry().getContract().getIsBlueprint() == null || !comp.getContrPriceCtgry().getContract().getIsBlueprint())) {
			this.createMarkContract(comp.getContrPriceCtgry().getContract().getId());
		}
	}

	private void markContract(JoinPoint jp) throws BusinessException {
		try {
			for (Object obj : jp.getArgs()) {
				if (obj instanceof AbstractEntity) {
					this.markContract(obj);
				} else if (obj instanceof List) {
					List<?> list = (List<?>) obj;
					if (!list.isEmpty()) {
						for (Object o : list) {
							this.markContract(o);
						}
					}
				}
			}
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			LOG.error("Contract cannot be retrived using reflection.", e);
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}
	}

}
