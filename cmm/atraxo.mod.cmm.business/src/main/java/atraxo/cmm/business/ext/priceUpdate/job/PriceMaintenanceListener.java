package atraxo.cmm.business.ext.priceUpdate.job;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.util.StringUtils;

import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import seava.j4e.scheduler.batch.listener.DefaultActionListener;

/**
 * @author zspeter
 */
public class PriceMaintenanceListener extends DefaultActionListener {

	@SuppressWarnings("unchecked")
	@Override
	public void afterJob(JobExecution jobExecution) {
		super.afterJob(jobExecution);
		ExitStatus status = jobExecution.getExitStatus();
		List<StepExecution> stepExecution = (List<StepExecution>) jobExecution.getStepExecutions();
		Set<String> skipedList = new HashSet<>();
		Set<String> updList = new HashSet<>();
		for (StepExecution se : stepExecution) {
			ExecutionContext executionContext = se.getExecutionContext();
			if (executionContext.containsKey(PriceMaintenanceProcessor.SKIP)) {
				skipedList.addAll((Set<String>) executionContext.get(PriceMaintenanceProcessor.SKIP));
			}
			if (executionContext.containsKey(PriceMaintenanceProcessor.UPD)) {
				updList.addAll((Set<String>) executionContext.get(PriceMaintenanceProcessor.UPD));
			}
		}
		String updStr = StringUtils.collectionToCommaDelimitedString(updList);
		String skipedStr = StringUtils.collectionToCommaDelimitedString(skipedList);
		if (StringUtils.isEmpty(updStr)) {
			updStr = CmmErrorCode.NO_CONTRACTS.getErrMsg();
		}
		if (StringUtils.isEmpty(skipedStr)) {
			skipedStr = CmmErrorCode.NO_CONTRACTS.getErrMsg();
		}
		if (status.equals(ExitStatus.COMPLETED)) {
			status = new ExitStatus(status.getExitCode(), String.format(CmmErrorCode.PRICE_UPDATE_JOB_RESULT.getErrMsg(), updStr, skipedStr));
		}
		jobExecution.setExitStatus(status);
	}
}
