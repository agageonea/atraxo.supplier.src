package atraxo.cmm.business.ws.tender.transformer;

import java.util.List;

import javax.xml.bind.JAXBException;

import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.customer.Customer;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderNoBid;
import seava.j4e.iata.fuelplus.iata.tender.LocationsNoBid;

public class TenderNoBidTransformer extends DataHubOutTransformer<TenderLocation, FuelTenderNoBid> {

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubOutTransformer#transformModelToDTO(atraxo.fmbas.domain.impl.abstracts.AbstractEntity, long,
	 * java.lang.String)
	 */
	@Override
	public FuelTenderNoBid transformModelToDTO(TenderLocation modelEntity, long msgId, String reason) throws BusinessException, JAXBException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubOutTransformer#transformModelToDTO(atraxo.fmbas.domain.impl.abstracts.AbstractEntity, long, long,
	 * atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames)
	 */
	@Override
	public FuelTenderNoBid transformModelToDTO(TenderLocation modelEntity, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubOutTransformer#transformModelsToDTO(java.util.List, long, java.lang.String)
	 */
	@Override
	public FuelTenderNoBid transformModelsToDTO(List<TenderLocation> modelEntities, long msgId, String reason)
			throws BusinessException, JAXBException {

		FuelTenderNoBid tenderNoBid = new FuelTenderNoBid();
		tenderNoBid.setSchemaVersion(modelEntities.get(0).getTender().getSchemaVersion());
		tenderNoBid.setTenderIdentification(this.buildTenderIdentification(modelEntities.get(0).getTender(), msgId));

		String bidderCode;
		Customer client = this.customerService.findByCode(Session.user.get().getClientCode());
		if (client.getIataCode() != null) {
			bidderCode = client.getIataCode();
		} else {
			bidderCode = client.getCode();
		}
		tenderNoBid.setBidderCode(bidderCode);

		LocationsNoBid locationsNoBid = new LocationsNoBid();
		locationsNoBid.getLocationNoBid().addAll(this.buildLocationsHeader(modelEntities, null));
		tenderNoBid.setLocationsNoBid(locationsNoBid);

		return tenderNoBid;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.ws.tender.IDataHubOutTransformer#transformModelsToDTO(java.util.List, long, long,
	 * atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames)
	 */
	@Override
	public FuelTenderNoBid transformModelsToDTO(List<TenderLocation> modelEntities, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException {
		// TODO Auto-generated method stub
		return null;
	}
}
