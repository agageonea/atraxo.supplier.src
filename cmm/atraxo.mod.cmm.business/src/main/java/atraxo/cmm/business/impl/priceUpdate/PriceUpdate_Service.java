/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.impl.priceUpdate;

import atraxo.cmm.domain.impl.priceUpdate.PriceUpdate;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdateCategories;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link PriceUpdate} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class PriceUpdate_Service extends AbstractEntityService<PriceUpdate> {

	/**
	 * Public constructor for PriceUpdate_Service
	 */
	public PriceUpdate_Service() {
		super();
	}

	/**
	 * Public constructor for PriceUpdate_Service
	 * 
	 * @param em
	 */
	public PriceUpdate_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<PriceUpdate> getEntityClass() {
		return PriceUpdate.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return PriceUpdate
	 */
	public PriceUpdate findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(PriceUpdate.NQ_FIND_BY_BUSINESS,
							PriceUpdate.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"PriceUpdate", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"PriceUpdate", "id"), nure);
		}
	}

	/**
	 * Find by reference: priceCategory
	 *
	 * @param priceCategory
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByPriceCategory(PriceCategory priceCategory) {
		return this.findByPriceCategoryId(priceCategory.getId());
	}
	/**
	 * Find by ID of reference: priceCategory.id
	 *
	 * @param priceCategoryId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByPriceCategoryId(Integer priceCategoryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdate e where e.clientId = :clientId and e.priceCategory.id = :priceCategoryId",
						PriceUpdate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("priceCategoryId", priceCategoryId)
				.getResultList();
	}
	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByCurrency(Currencies currency) {
		return this.findByCurrencyId(currency.getId());
	}
	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByCurrencyId(Integer currencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdate e where e.clientId = :clientId and e.currency.id = :currencyId",
						PriceUpdate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currencyId", currencyId).getResultList();
	}
	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByUnit(Unit unit) {
		return this.findByUnitId(unit.getId());
	}
	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByUnitId(Integer unitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdate e where e.clientId = :clientId and e.unit.id = :unitId",
						PriceUpdate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitId", unitId).getResultList();
	}
	/**
	 * Find by reference: publishedBy
	 *
	 * @param publishedBy
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByPublishedBy(UserSupp publishedBy) {
		return this.findByPublishedById(publishedBy.getId());
	}
	/**
	 * Find by ID of reference: publishedBy.id
	 *
	 * @param publishedById
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByPublishedById(String publishedById) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdate e where e.clientId = :clientId and e.publishedBy.id = :publishedById",
						PriceUpdate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("publishedById", publishedById).getResultList();
	}
	/**
	 * Find by reference: submittedBy
	 *
	 * @param submittedBy
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findBySubmittedBy(UserSupp submittedBy) {
		return this.findBySubmittedById(submittedBy.getId());
	}
	/**
	 * Find by ID of reference: submittedBy.id
	 *
	 * @param submittedById
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findBySubmittedById(String submittedById) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdate e where e.clientId = :clientId and e.submittedBy.id = :submittedById",
						PriceUpdate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("submittedById", submittedById).getResultList();
	}
	/**
	 * Find by reference: approvedBy
	 *
	 * @param approvedBy
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByApprovedBy(UserSupp approvedBy) {
		return this.findByApprovedById(approvedBy.getId());
	}
	/**
	 * Find by ID of reference: approvedBy.id
	 *
	 * @param approvedById
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByApprovedById(String approvedById) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdate e where e.clientId = :clientId and e.approvedBy.id = :approvedById",
						PriceUpdate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("approvedById", approvedById).getResultList();
	}
	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByLocation(Locations location) {
		return this.findByLocationId(location.getId());
	}
	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByLocationId(Integer locationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdate e where e.clientId = :clientId and e.location.id = :locationId",
						PriceUpdate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("locationId", locationId).getResultList();
	}
	/**
	 * Find by reference: contractHolder
	 *
	 * @param contractHolder
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByContractHolder(Customer contractHolder) {
		return this.findByContractHolderId(contractHolder.getId());
	}
	/**
	 * Find by ID of reference: contractHolder.id
	 *
	 * @param contractHolderId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByContractHolderId(Integer contractHolderId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdate e where e.clientId = :clientId and e.contractHolder.id = :contractHolderId",
						PriceUpdate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractHolderId", contractHolderId)
				.getResultList();
	}
	/**
	 * Find by reference: counterparty
	 *
	 * @param counterparty
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByCounterparty(Customer counterparty) {
		return this.findByCounterpartyId(counterparty.getId());
	}
	/**
	 * Find by ID of reference: counterparty.id
	 *
	 * @param counterpartyId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByCounterpartyId(Integer counterpartyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdate e where e.clientId = :clientId and e.counterparty.id = :counterpartyId",
						PriceUpdate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("counterpartyId", counterpartyId).getResultList();
	}
	/**
	 * Find by reference: billTo
	 *
	 * @param billTo
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByBillTo(Customer billTo) {
		return this.findByBillToId(billTo.getId());
	}
	/**
	 * Find by ID of reference: billTo.id
	 *
	 * @param billToId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByBillToId(Integer billToId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdate e where e.clientId = :clientId and e.billTo.id = :billToId",
						PriceUpdate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("billToId", billToId).getResultList();
	}
	/**
	 * Find by reference: shipTo
	 *
	 * @param shipTo
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByShipTo(Customer shipTo) {
		return this.findByShipToId(shipTo.getId());
	}
	/**
	 * Find by ID of reference: shipTo.id
	 *
	 * @param shipToId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByShipToId(Integer shipToId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceUpdate e where e.clientId = :clientId and e.shipTo.id = :shipToId",
						PriceUpdate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("shipToId", shipToId).getResultList();
	}
	/**
	 * Find by reference: priceUpdateCategories
	 *
	 * @param priceUpdateCategories
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByPriceUpdateCategories(
			PriceUpdateCategories priceUpdateCategories) {
		return this
				.findByPriceUpdateCategoriesId(priceUpdateCategories.getId());
	}
	/**
	 * Find by ID of reference: priceUpdateCategories.id
	 *
	 * @param priceUpdateCategoriesId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByPriceUpdateCategoriesId(
			Integer priceUpdateCategoriesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from PriceUpdate e, IN (e.priceUpdateCategories) c where e.clientId = :clientId and c.id = :priceUpdateCategoriesId",
						PriceUpdate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("priceUpdateCategoriesId",
						priceUpdateCategoriesId).getResultList();
	}
}
