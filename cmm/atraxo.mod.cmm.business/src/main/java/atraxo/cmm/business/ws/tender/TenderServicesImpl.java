/**
 * Please modify this class to meet your needs This class is not complete
 */

package atraxo.cmm.business.ws.tender;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.Holder;
import javax.xml.ws.WebServiceContext;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.ext.bid.IBidProcessor;
import atraxo.cmm.business.api.ext.tender.ITenderProcessor;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcceptAward;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcknowledge;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBid;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBidAward;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBidDecline;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderCancel;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderCancelBid;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderDeclineAward;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderInvitation;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderNewRound;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderNoBid;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderResponse;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderService;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderUpdate;

// @SchemaValidation(type = SchemaValidationType.BOTH)
@WebService(serviceName = "FuelTenderServiceService", portName = "FuelTenderServicePort", targetNamespace = "http://www.w3.org/2001/XMLSchema", wsdlLocation = "classpath:xsd/iata/tenderinitfinal.wsdl", endpointInterface = "seava.j4e.iata.fuelplus.iata.tender.FuelTenderService")
public class TenderServicesImpl implements FuelTenderService {

	@Autowired
	private IBidProcessor bidProcessor;
	@Autowired
	private ITenderProcessor tenderProcessor;
	@Resource
	private WebServiceContext context;

	@Override
	public FuelTenderResponse fuelTenderAcceptAward(FuelTenderAcceptAward parameters) {
		return this.fuelTenderAcceptAward(parameters);
	}

	@Override
	public FuelTenderResponse fuelTenderDeclineAward(FuelTenderDeclineAward parameters) {
		return null;
	}

	@Override
	public FuelTenderResponse fuelTenderBidDecline(FuelTenderBidDecline parameters) {
		return this.bidProcessor.fuelTenderBidDecline(parameters, this.context.getMessageContext());

	}

	@Override
	public FuelTenderResponse fuelTenderBidAward(FuelTenderBidAward parameters) {
		return this.bidProcessor.fuelTenderBidAward(parameters, this.context.getMessageContext());
	}

	@Override
	public FuelTenderResponse fuelTenderInvitation(FuelTenderInvitation tenderInvitation) {
		return this.tenderProcessor.fuelTenderInvitation(tenderInvitation, this.context.getMessageContext());
	}

	@Override
	public FuelTenderResponse fuelTenderNewRound(FuelTenderNewRound parameters) {
		return this.tenderProcessor.fuelTenderNewRound(parameters, this.context.getMessageContext());
	}

	@Override
	public FuelTenderResponse fuelTenderNoBid(FuelTenderNoBid parameters) {
		return this.bidProcessor.fuelTenderNoBid(parameters);
	}

	@Override
	public FuelTenderResponse fuelTenderCancel(FuelTenderCancel parameters) {
		return this.tenderProcessor.fuelTenderCancel(parameters, this.context.getMessageContext());
	}

	@Override
	public FuelTenderResponse fuelTenderUpdate(FuelTenderUpdate tenderUpdate) {
		return this.tenderProcessor.fuelTenderUpdate(tenderUpdate, this.context.getMessageContext());
	}

	@Override
	public FuelTenderResponse fuelTenderBid(FuelTenderBid parameters) {
		return this.bidProcessor.fuelTenderBid(parameters);
	}

	@Override
	public FuelTenderResponse fuelTenderCancelBid(FuelTenderCancelBid parameters) {
		return this.bidProcessor.fuelTenderCancelBid(parameters);
	}

	@Override
	public void fuelTenderResponse(Holder<FuelTenderResponse> parameters) {
		// Auto-generated method stub
	}

	@Override
	public FuelTenderResponse fuelTenderAcknowledgement(FuelTenderAcknowledge parameters) {
		return this.tenderProcessor.fuelTenderAcknowledgement(parameters, this.context.getMessageContext());
	}

}
