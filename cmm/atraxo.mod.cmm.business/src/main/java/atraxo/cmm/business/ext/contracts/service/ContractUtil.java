package atraxo.cmm.business.ext.contracts.service;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.ReviewPeriod;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.fmbas.domain.impl.customer.Customer;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.utils.DateUtils;

public class ContractUtil {

	/**
	 * @author zspeter
	 */
	private static final class ContractFlightTypeComparator implements Comparator<Contract> {
		/**
		 *
		 */
		private final FlightTypeIndicator flightType;

		/**
		 * @param flightType
		 */
		private ContractFlightTypeComparator(FlightTypeIndicator flightType) {
			this.flightType = flightType;
		}

		@Override
		public int compare(Contract o1, Contract o2) {
			if (this.flightType.equals(o1.getLimitedTo()) && !this.flightType.equals(o2.getLimitedTo())) {
				return -1;
			} else if (this.flightType.equals(o1.getLimitedTo()) && this.flightType.equals(o2.getLimitedTo())) {
				return 0;
			} else if (!this.flightType.equals(o1.getLimitedTo()) && this.flightType.equals(o2.getLimitedTo())) {
				return 1;
			}
			return 0;
		}
	}

	private static final String PC = "PC";
	private static final String SC = "SC";
	private static final String NC = "NC"; // In case of deal type empty(for bids). NC = no contract

	protected static void setReadOnly(Contract contract) {
		if (DealType._BUY_.equals(contract.getDealType()) || contract.getResaleRef() == null) {
			return;
		}
		Contract purchaseContract = contract.getResaleRef();
		// is internal resale
		if (contract.getHolder().getCode().equals(purchaseContract.getSupplier().getCode())
				&& contract.getCustomer().equals(purchaseContract.getHolder()) && contract.getHolder().getIsSubsidiary()
				&& contract.getCustomer().getIsSubsidiary()) {
			contract.setReadOnly(true);
		}

	}

	protected static void verifyCompanies(Contract contract) throws BusinessException {
		if (DealType._BUY_.equals(contract.getDealType()) && contract.getSupplier() != null
				&& contract.getHolder().getId().equals(contract.getSupplier().getId())) {
			throw new BusinessException(CmmErrorCode.SAME_HOLDER_CUSTOMER_SUPPLIER, CmmErrorCode.SAME_HOLDER_CUSTOMER_SUPPLIER.getErrMsg());
		}
		if (DealType._SELL_.equals(contract.getDealType()) && contract.getCustomer() != null
				&& contract.getHolder().getId().equals(contract.getCustomer().getId())) {
			throw new BusinessException(CmmErrorCode.SAME_HOLDER_CUSTOMER_SUPPLIER, CmmErrorCode.SAME_HOLDER_CUSTOMER_SUPPLIER.getErrMsg());
		}
	}

	protected static void setSubsidiary(Contract e) {
		if (!e.getHolder().getRefid().equalsIgnoreCase(e.getSubsidiaryId())) {
			e.setSubsidiaryId(e.getHolder().getRefid());
		}
	}

	protected static void checkPrepayMandatoryFields(Contract contract) throws BusinessException {
		if (contract.getBidPrepaidDays() == null || contract.getBidPayementFreq() == null || contract.getBidPrepaidAmount() == null
				|| contract.getBidPrepayFirstDeliveryDate() == null) {
			throw new BusinessException(CmmErrorCode.BID_TENDER_PREPAY_REQUIRED_FIELDS_NULL,
					CmmErrorCode.BID_TENDER_PREPAY_REQUIRED_FIELDS_NULL.getErrMsg());
		}
	}

	/**
	 * Check if the fields are filled.
	 *
	 * @param c - {@link Contract}
	 * @throws BusinessException
	 */
	protected static void checkRewPeriod(Contract c) throws BusinessException {
		if ((ReviewPeriod._WEEKLY_.equals(c.getReviewPeriod()))
				|| ReviewPeriod._SEMIMONTHLY_.equals(c.getReviewPeriod()) && "".equalsIgnoreCase(c.getReviewFirstParam())) {
			throw new BusinessException(CmmErrorCode.FIELD_REVIEW_DATE1, CmmErrorCode.FIELD_REVIEW_DATE1.getErrMsg());
		}
		if (ReviewPeriod._MONTHLY_.equals(c.getReviewPeriod()) || ReviewPeriod._QUARTERLY_.equals(c.getReviewPeriod())
				|| ReviewPeriod._SEMIANNUALLY_.equals(c.getReviewPeriod()) || ReviewPeriod._ANNUALLY_.equals(c.getReviewPeriod())) {
			if ("".equalsIgnoreCase(c.getReviewFirstParam()) && "".equalsIgnoreCase(c.getReviewSecondParam())) {
				throw new BusinessException(CmmErrorCode.FIELD_REVIEW_DATE12, CmmErrorCode.FIELD_REVIEW_DATE12.getErrMsg());
			} else if (!"".equalsIgnoreCase(c.getReviewFirstParam()) && "".equalsIgnoreCase(c.getReviewSecondParam())) {
				throw new BusinessException(CmmErrorCode.FIELD_REVIEW_DATE2, CmmErrorCode.FIELD_REVIEW_DATE2.getErrMsg());
			} else if ("".equalsIgnoreCase(c.getReviewFirstParam()) && !"".equalsIgnoreCase(c.getReviewSecondParam())) {
				throw new BusinessException(CmmErrorCode.FIELD_REVIEW_DATE1, CmmErrorCode.FIELD_REVIEW_DATE1.getErrMsg());
			}
		}
	}

	/**
	 * Verify the dates. Valid to must be greater than valid from.
	 *
	 * @param from - {@link Date} valid from.
	 * @param to - {@link Date} valid to.
	 * @throws BusinessException
	 */
	protected static void verifyDates(Date from, Date to) throws BusinessException {
		if (from != null && to != null && from.after(to)) {
			throw new BusinessException(CmmErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
					CmmErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO.getErrMsg());
		}
	}

	protected static void filterContracts(Customer customer, Date date, List<Contract> list, FlightTypeIndicator flightType) {
		Iterator<Contract> iterator = list.iterator();

		while (iterator.hasNext()) {
			boolean found = false;
			Contract c = iterator.next();
			Date validTo = DateUtils.modifyDate(c.getValidTo(), Calendar.DAY_OF_MONTH, 1);
			if ((c.getValidFrom().compareTo(date) > 0 || validTo.compareTo(date) <= 0)
					|| (!c.getStatus().equals(ContractStatus._EFFECTIVE_) && !c.getStatus().equals(ContractStatus._EXPIRED_))) {
				iterator.remove();
				continue;
			}
			boolean removed = filterByFlightType(flightType, iterator, c);
			if (!removed) {
				filterByShipTo(customer, iterator, found, c, date);
			}
		}
		sortContractByFlighType(list, flightType);
	}

	private static void filterByShipTo(Customer customer, Iterator<Contract> iterator, boolean found, Contract c, Date date) {
		Date dateWithoutTime = DateUtils.removeTime(date);
		if (customer != null) {
			for (ShipTo shipTo : c.getShipTo()) {
				if (customer.equals(shipTo.getCustomer()) && shipTo.getValidFrom().compareTo(dateWithoutTime) <= 0
						&& shipTo.getValidTo().compareTo(dateWithoutTime) >= 0) {
					found = true;
					break;
				}
			}
			if (!found) {
				iterator.remove();
			}
		}
	}

	private static boolean filterByFlightType(FlightTypeIndicator flightType, Iterator<Contract> iterator, Contract c) {
		boolean removed = false;
		switch (flightType) {
		case _DOMESTIC_:
			if (FlightTypeIndicator._INTERNATIONAL_.equals(c.getLimitedTo())) {
				iterator.remove();
				removed = true;
			}
			break;
		case _INTERNATIONAL_:
			if (FlightTypeIndicator._DOMESTIC_.equals(c.getLimitedTo())) {
				iterator.remove();
				removed = true;
			}
			break;
		case _UNSPECIFIED_:
			if (FlightTypeIndicator._DOMESTIC_.equals(c.getLimitedTo()) || FlightTypeIndicator._INTERNATIONAL_.equals(c.getLimitedTo())) {
				iterator.remove();
				removed = true;
			}
			break;
		default:
			break;
		}
		return removed;
	}

	protected static List<Contract> sortContractByFlighType(List<Contract> list, final FlightTypeIndicator flightType) {
		Collections.sort(list, new ContractFlightTypeComparator(flightType));
		return list;
	}

	/**
	 * If the status is expired or effective, the contract's valid from date can not be bigger than today.
	 *
	 * @param c - contract which will be updated.
	 * @throws BusinessException
	 */
	protected static void checkEffectiveExpired(Contract c) throws BusinessException {
		if ((ContractStatus._EFFECTIVE_.equals(c.getStatus()) || ContractStatus._EXPIRED_.equals(c.getStatus()))
				&& c.getValidFrom().after(GregorianCalendar.getInstance().getTime())) {
			throw new BusinessException(CmmErrorCode.VALIDITY_FOR_EXPIRED_EFFECTIVE_CONTRACT,
					CmmErrorCode.VALIDITY_FOR_EXPIRED_EFFECTIVE_CONTRACT.getErrMsg());
		}
	}

	protected static void buildCode(Contract c) {
		int n = String.valueOf(c.getId()).length();
		DealType deal = c.getDealType();
		StringBuilder code = new StringBuilder();
		if (DealType._BUY_.equals(deal)) {
			code.append(PC);
		} else if (DealType._SELL_.equals(deal)) {
			code.append(SC);
		} else {
			code.append(NC);
		}
		for (int i = 1; i <= 5 - n; ++i) {
			code.append("0");
		}
		c.setCode(code.append(c.getId()).toString());
	}

	protected static boolean isCrossShipTosPeriod(Contract contract, Contract c) {
		if (CollectionUtils.isEmpty(contract.getShipTo()) && CollectionUtils.isEmpty(c.getShipTo())) {
			return true;
		}
		if (CollectionUtils.isEmpty(contract.getShipTo()) || CollectionUtils.isEmpty(c.getShipTo())) {
			return false;
		}
		for (ShipTo shipTo : contract.getShipTo()) {
			for (ShipTo shipToC : c.getShipTo()) {
				if (shipToC.getCustomer().equals(shipTo.getCustomer())
						&& (isInPeriod(shipToC.getValidFrom(), shipTo.getValidFrom(), shipTo.getValidTo())
								|| isInPeriod(shipToC.getValidTo(), shipTo.getValidFrom(), shipTo.getValidTo())
								|| isInPeriod(shipTo.getValidFrom(), shipToC.getValidFrom(), shipToC.getValidTo())
								|| isInPeriod(shipTo.getValidTo(), shipToC.getValidFrom(), shipToC.getValidTo()))) {
					return true;
				}
			}
		}
		return false;
	}

	private static boolean isInPeriod(Date date, Date from, Date to) {
		return date.compareTo(from) >= 0 && date.compareTo(to) <= 0;
	}

	public static boolean alreadyApprovedOrInApprovalProcess(Contract contract) {
		return BidApprovalStatus._APPROVED_.equals(contract.getBidApprovalStatus())
				|| BidApprovalStatus._AWAITING_APPROVAL_.equals(contract.getBidApprovalStatus());
	}

	public static boolean alreadyInApprovalProcess(Contract contract) {
		// check workflow(s) for approval of existing contract (period, ship-to(s) and price)
		return BidApprovalStatus._AWAITING_APPROVAL_.equals(contract.getPeriodApprovalStatus())
				|| BidApprovalStatus._AWAITING_APPROVAL_.equals(contract.getPriceApprovalStatus())
				|| BidApprovalStatus._AWAITING_APPROVAL_.equals(contract.getShipToApprovalStatus());
	}

	/**
	 * Check if all contract approval statuses are Approved (Period, ShipTo, Price)
	 *
	 * @param blueprint
	 * @throws BusinessException
	 */
	public static boolean checkIfCanPublishAutomatically(Contract blueprint) {
		boolean canPublishPeriod = BidApprovalStatus._NEW_.equals(blueprint.getPeriodApprovalStatus())
				|| BidApprovalStatus._APPROVED_.equals(blueprint.getPeriodApprovalStatus());

		boolean canPublishPrice = BidApprovalStatus._NEW_.equals(blueprint.getPriceApprovalStatus())
				|| BidApprovalStatus._APPROVED_.equals(blueprint.getPriceApprovalStatus());

		boolean canPublishShipTo = BidApprovalStatus._NEW_.equals(blueprint.getShipToApprovalStatus())
				|| BidApprovalStatus._APPROVED_.equals(blueprint.getShipToApprovalStatus());

		return canPublishPeriod && canPublishPrice && canPublishShipTo;
	}

	/**
	 * Verify if a change can be published based on the workflow and active changes
	 *
	 * @param isWorkflowEnabled
	 * @param hasChanges
	 * @param changeApprovalStatus
	 * @return
	 * @throws BusinessException
	 */
	public static boolean canPublishChanges(boolean isWorkflowEnabled, boolean hasChanges, BidApprovalStatus changeApprovalStatus) {

		boolean canPublish = false;
		if (isWorkflowEnabled) {
			if (BidApprovalStatus._APPROVED_.equals(changeApprovalStatus) && !hasChanges) {
				canPublish = true;
			}
		} else {
			if (hasChanges) {
				canPublish = true;
			}
		}

		return canPublish;
	}

}
