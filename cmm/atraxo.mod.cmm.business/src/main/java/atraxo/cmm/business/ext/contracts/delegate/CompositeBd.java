package atraxo.cmm.business.ext.contracts.delegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;

/**
 * @author zspeter
 */
public class CompositeBd {

	private CompositeBd() {
	}

	/**
	 * @param contract
	 * @return
	 */
	public static Map<String, List<String>> getAssignedCategoryNameList(Contract contract) {
		Map<String, List<String>> retMap = new HashMap<>();
		if (CollectionUtils.isEmpty(contract.getPriceCategories())) {
			return retMap;
		}
		for (ContractPriceCategory cpc : contract.getPriceCategories()) {
			if (PriceInd._COMPOSITE_.equals(cpc.getPriceCategory().getPricePer())) {
				if (retMap.containsKey(cpc.getName())) {
					List<String> priceList = retMap.get(cpc.getName());
					addParentPriceCategories(cpc, priceList);
				} else {
					List<String> priceList = new ArrayList<>();
					addParentPriceCategories(cpc, priceList);
					retMap.put(cpc.getName(), priceList);
				}
			}
		}
		return retMap;
	}

	/**
	 * Add <code>cpc</code> parent price categories to <code>priceList</code>.
	 *
	 * @param cpc
	 * @param priceList
	 */
	private static void addParentPriceCategories(ContractPriceCategory cpc, List<String> priceList) {
		if (!CollectionUtils.isEmpty(cpc.getParentPriceCategory())) {
			for (ContractPriceCategory temp : cpc.getParentPriceCategory()) {
				priceList.add(temp.getName());
			}
		}
	}

	/**
	 *
	 * @param contract
	 * @param categoryMap
	 */
	public static void updateCompositePrice(Contract contract, Map<String, List<String>> categoryMap) {
		if (CollectionUtils.isEmpty(contract.getPricingBases())) {
			return;
		}
		List<ContractPriceCategory> categories = new ArrayList<>(contract.getPriceCategories());
		for (ContractPriceCategory composite : categories) {
			if (PriceInd._COMPOSITE_.equals(composite.getPriceCategory().getPricePer())) {
				updateComposite(contract, categoryMap, categories, composite);
			}
		}
	}

	private static void updateComposite(Contract contract, Map<String, List<String>> categoryMap, List<ContractPriceCategory> categories,
			ContractPriceCategory composite) {
		for (ContractPriceComponent comp : composite.getPriceComponents()) {
			comp.setCurrency(contract.getSettlementCurr());
			comp.setUnit(contract.getSettlementUnit());
		}
		List<String> categoryNameList = new ArrayList<>();
		if (categoryMap.containsKey(composite.getName())) {
			categoryNameList.addAll(categoryMap.get(composite.getName()));
		}
		if (composite.getParentPriceCategory() == null) {
			composite.setParentPriceCategory(new ArrayList<ContractPriceCategory>());
		}
		for (String categoryName : categoryNameList) {
			ContractPriceCategory contractCategory = getPriceCategoryByName(categories, categoryName);
			ContractPriceCategory assignedCategory = getPriceCategoryByName(composite.getParentPriceCategory(), categoryName);
			if (contractCategory != null && assignedCategory != null && contractCategory.getId() == null) {
				composite.getParentPriceCategory().add(contractCategory);
			}
		}
	}

	private static ContractPriceCategory getPriceCategoryByName(Collection<ContractPriceCategory> categories, String categoryName) {
		if (CollectionUtils.isEmpty(categories)) {
			return null;
		}
		for (ContractPriceCategory cpc : categories) {
			if (cpc.getName().equals(categoryName)) {
				return cpc;
			}
		}
		return null;
	}
}
