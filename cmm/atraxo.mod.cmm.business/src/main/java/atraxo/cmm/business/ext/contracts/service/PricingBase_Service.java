package atraxo.cmm.business.ext.contracts.service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import atraxo.cmm.business.api.contracts.IContractChangeService;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.contracts.IPricingBaseService;
import atraxo.cmm.business.api.ext.contracts.IPriceBuilder;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.ext.contracts.delegate.PricingBase_Bd;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.prices.delegate.ContractPriceCategory_Bd;
import atraxo.cmm.business.ext.utils.AverageMethodCodeFrequency;
import atraxo.cmm.business.ext.utils.AverageMethodCodeFrequencyPeriodCalculator;
import atraxo.cmm.business.ext.utils.DatePeriod;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.business.ext.util.EntityCloner;
import atraxo.fmbas.domain.ext.types.timeseries.AverageMethodCode;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.fmbas_type.ContractChangeType;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public class PricingBase_Service extends atraxo.cmm.business.impl.contracts.PricingBase_Service implements IPricingBaseService {

	private static final String CONTRACT_ID = "contractId";
	private static final Logger LOG = LoggerFactory.getLogger(PricingBase_Service.class);
	/**
	 * Update pricing base without propagate the changes to price category and price component.
	 */

	@Autowired
	private IContractPriceCategoryService priceCategoryService;
	@Autowired
	private IUnitService unitService;
	@Autowired
	private IPriceBuilder builderService;
	@Autowired
	private UnitConverterService unitConverterService;
	@Autowired
	private IContractChangeService contractChangeService;
	@Autowired
	private IContractService contractService;

	@Override
	@Transactional
	public void updatePricingBase(PricingBase e) throws BusinessException {
		this.onUpdate(e);
		this.updateNeighbours(e);
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void insert(List<PricingBase> list) throws BusinessException {
		super.insert(list);
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void update(List<PricingBase> list) throws BusinessException {
		super.update(list);
	}

	@Override
	public void insertAsIs(PricingBase e) throws BusinessException {
		this.onInsert(e);
	}

	@Override
	public BigDecimal convertUnit(Integer fromUnitId, Integer toUnitId, BigDecimal value, BigDecimal density) throws BusinessException {
		Unit fromUnit = this.unitService.findById(fromUnitId);
		Unit toUnit = this.unitService.findById(toUnitId);
		return this.unitConverterService.convert(fromUnit, toUnit, value, density.doubleValue());
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		this.deleteContractPriceCategories(ids);
		if (!CollectionUtils.isEmpty(ids)) {
			Integer id = (Integer) ids.get(0);
			PricingBase pb = this.findById(id);
			if (this.isProduct(pb)) {
				context.put(CONTRACT_ID, pb.getContract().getId());
			}
		}
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void deleteByIds(List<Object> ids) throws BusinessException {
		super.deleteByIds(ids);
	}

	@Override
	protected void postDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.postDeleteByIds(ids, context);
		if (context.containsKey(CONTRACT_ID)) {
			Object contractId = context.get(CONTRACT_ID);
			Contract contract = this.findById(contractId, Contract.class);
			List<PricingBase> list = new ArrayList<>(contract.getPricingBases());
			this.removeCompositeFromList(list);
			list.sort((o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()));
			List<PricingBase> updList = this.createUpdateListe(contract, list);
			this.updateAsIs(updList);

			this.contractService.validateBid(contract);

		}
	}

	private List<PricingBase> createUpdateListe(Contract contract, List<PricingBase> list) {
		List<PricingBase> updList = new ArrayList<>();
		Date leftMargin = contract.getValidFrom();
		PricingBase pb = null;
		for (PricingBase e : list) {
			if (!this.isProduct(e)) {
				continue;
			}
			if (pb == null) {
				pb = e;
				if (pb.getValidFrom().after(leftMargin)) {
					pb.setValidFrom(leftMargin);
					updList.add(pb);
				}
			} else {
				leftMargin = DateUtils.addDays(e.getValidFrom(), -1);
				if (pb.getValidTo().before(leftMargin)) {
					pb.setValidTo(leftMargin);
					updList.add(pb);
				}
				pb = e;
			}
		}
		if (pb != null && pb.getValidTo().before(contract.getValidTo())) {
			pb.setValidTo(contract.getValidTo());
			updList.add(pb);
		}
		return updList;
	}

	private void removeCompositeFromList(List<PricingBase> pbList) {
		if (CollectionUtils.isEmpty(pbList)) {
			return;
		}
		Iterator<PricingBase> iter = pbList.iterator();
		while (iter.hasNext()) {
			PricingBase pb = iter.next();
			if (PriceInd._COMPOSITE_.equals(pb.getPriceCat().getPricePer())) {
				iter.remove();
			}
		}
	}

	@Override
	protected void preInsert(PricingBase e) throws BusinessException {
		super.preInsert(e);
		this.updateCompositeRelation(e);
	}

	private void updateCompositeRelation(PricingBase e) throws BusinessException {
		if (PriceInd._COMPOSITE_.equals(e.getPriceCat().getPricePer())) {
			ContractPriceCategory cpc = e.getContractPriceCategories().iterator().next();
			this.builderService.assignParentPriceCategory(e.getPriceCategoryIdsJson(), cpc);
			this.builderService.updateParentPriceCategories(cpc, CalculateIndicator._CALCULATE_ONLY_);
		}
	}

	@Override
	protected void postInsert(PricingBase e) throws BusinessException {
		super.postInsert(e);
		if (this.isProduct(e)) {
			this.updateProductPricingBases(e);
		}
		// "log" the new inserted price category change
		if (ContractChangeUtil.checkBlueprintProductChanges(e.getContract())) {
			this.contractChangeService.addNewChange(e.getContract(), ContractChangeType._PRICE_, ContractChangeUtil.buildMessageForPriceInsert(e));
		}
		if (!e.getContract().getPricingBases().contains(e)) {
			e.getContract().addToPricingBases(e);
		}
		this.contractService.validateBid(e.getContract());
	}

	@Override
	public void updateWithoutPersist(PricingBase e) throws BusinessException {
		this.validateQuotation(e);
		this.preUpdate(e);
		this.postUpdate(e);
	}

	/**
	 * @param e
	 * @throws BusinessException
	 */
	private void validateQuotation(PricingBase e) throws BusinessException {
		if (this.isIndex(e)) {
			this.validateQuotationInternal(e);
		}
	}

	/**************************/
	private void validateQuotationInternal(PricingBase pricingBase) throws BusinessException {
		this.checkPricingBasisQuotation(pricingBase);
		this.checkQuotationPeriod(pricingBase);
	}

	/**
	 * Check if the mandatory field are filled.
	 *
	 * @param pricingBase
	 *            - PricingBase
	 * @throws BusinessException
	 */
	private void checkPricingBasisQuotation(PricingBase pricingBase) throws BusinessException {
		if (this.isIndex(pricingBase)) {
			if (pricingBase.getQuotation() == null) {
				throw new BusinessException(CmmErrorCode.PB_QUOTATION_REQUIERED, CmmErrorCode.PB_QUOTATION_REQUIERED.getErrMsg());
			}
			if (pricingBase.getQuotationOffset() == null || MasterAgreementsPeriod._EMPTY_.equals(pricingBase.getQuotationOffset())) {
				throw new BusinessException(CmmErrorCode.PB_PERIOD_REQUIRED, CmmErrorCode.PB_PERIOD_REQUIRED.getErrMsg());
			}
			if (Operator._EMPTY_.equals(pricingBase.getOperator())) {
				throw new BusinessException(CmmErrorCode.PB_OPERATOR_REQUIRED, CmmErrorCode.PB_OPERATOR_REQUIRED.getErrMsg());
			}
			if (pricingBase.getFactor() == null) {
				throw new BusinessException(CmmErrorCode.PB_FACTOR_REQUIRED, CmmErrorCode.PB_FACTOR_REQUIRED.getErrMsg());
			}
			if (pricingBase.getConvUnit() == null) {
				throw new BusinessException(CmmErrorCode.PB_TO_REQUIRED, CmmErrorCode.PB_TO_REQUIRED.getErrMsg());
			}
		}
	}

	/**
	 * Verify if on the pricing bases period's start is quotation value. If no throw an exception.
	 *
	 * @param pb
	 *            - Pricing bases
	 * @throws BusinessException
	 */
	private void checkQuotationPeriod(PricingBase pb) throws BusinessException {
		List<QuotationValue> values = new ArrayList<>(pb.getQuotation().getQuotationValues());
		if (!values.isEmpty()) {
			values.sort((o1, o2) -> o1.getValidFromDate().compareTo(o2.getValidFromDate()));
			Date quotationValidFrom = values.get(0).getValidFromDate();

			// see if the new date period start date is included in the quotation time series items
			AverageMethodCodeFrequencyPeriodCalculator calc = new AverageMethodCodeFrequencyPeriodCalculator(
					AverageMethodCodeFrequency.getByAverageMethod(pb.getQuotation().getAvgMethodIndicator()), pb.getValidFrom(),
					pb.getQuotationOffset().getDelta());
			DatePeriod dp = calc.calculatePeriod();
			Date offsetDate = dp.getStartDate().getTime();

			if (quotationValidFrom.compareTo(offsetDate) > 0) {
				DateFormat sdf = DateFormat.getDateInstance(DateFormat.MEDIUM, java.util.Locale.getDefault());
				throw new BusinessException(CmmErrorCode.QUOTATION_VALUES_NOT_FOUND,
						String.format(CmmErrorCode.QUOTATION_VALUES_NOT_FOUND.getErrMsg(), pb.getQuotation().getCalcVal(), sdf.format(offsetDate)));
			}
		} else {
			throw new BusinessException(CmmErrorCode.QUOTATION_INVALID_DATA,
					String.format(CmmErrorCode.QUOTATION_INVALID_DATA.getErrMsg(), "Valid From"));
		}

	}

	/***************************/

	@Override
	protected void preUpdate(PricingBase e) throws BusinessException {
		super.preUpdate(e);
		this.builderService.extendsToMargins(e);
		if (this.isIndex(e)) {
			if (ContractChangeUtil.checkBlueprintProductChanges(e.getContract())) {
				PricingBase oldPricingBase = this.findById(e.getId());
				String change = ContractChangeUtil.buildMessageForPriceBaseUpdate(oldPricingBase, e);
				if (!StringUtils.isEmpty(change)) {
					this.contractChangeService.addNewChange(e.getContract(), ContractChangeType._PRICE_, change);
				}
			}
			this.generateContractPriceCategory(e, this.getBusinessDelegate(PricingBase_Bd.class));
		}
	}

	@Override
	protected void postUpdate(PricingBase e) throws BusinessException {
		super.postUpdate(e);
		this.updateNeighbours(e);
		this.updateContractPriceComponents(e);
	}

	private void updateNeighbours(PricingBase e) throws BusinessException {
		if (!this.isIndex(e)) {
			return;
		}
		List<PricingBase> list = this.getNeighbourProductPbs(e);
		List<Object> delList = new ArrayList<>();
		Date leftMargin = DateUtils.addDays(e.getValidFrom(), -1);
		Date rightMargin = DateUtils.addDays(e.getValidTo(), 1);
		PricingBase leftElement = null;
		List<PricingBase> updList = new ArrayList<>();
		for (PricingBase pb : list) {
			if (pb.getValidFrom().before(leftMargin) && pb.getValidTo().before(leftMargin)) {
				leftElement = pb;
				continue;
			}
			if (pb.getValidFrom().before(leftMargin) && pb.getValidTo().after(leftMargin)) {
				leftElement = null;
				pb.setValidTo(leftMargin);
				updList.add(pb);
				continue;
			}
			if (pb.getValidFrom().compareTo(e.getValidFrom()) >= 0 && pb.getValidTo().compareTo(e.getValidTo()) <= 0) {
				// swallow pricing base
				delList.add(pb.getId());
				continue;
			}

			if (pb.getValidFrom().before(rightMargin) && pb.getValidTo().after(rightMargin)) {
				pb.setValidFrom(rightMargin);
				updList.add(pb);
				break;
			}
			if (pb.getValidFrom().after(rightMargin)) {
				pb.setValidFrom(rightMargin);
				updList.add(pb);
				break;
			}
			if (pb.getValidFrom().equals(rightMargin)) {
				// we don't need to modified pricing bases on the right side
				break;
			}
		}
		if (leftElement != null) {
			leftElement.setValidTo(leftMargin);
			updList.add(leftElement);
		}
		this.updateAsIs(updList);
		this.deleteContractPriceCategories(delList);
		this.onDeleteByIds(delList, null);
	}

	private void updateProductPricingBases(PricingBase e) throws BusinessException {
		List<PricingBase> list = this.getNeighbourProductPbs(e);
		List<Object> delList = new ArrayList<>();
		List<PricingBase> updList = new ArrayList<>();
		List<PricingBase> insList = new ArrayList<>();
		for (PricingBase pb : list) {
			if (pb.getValidFrom().compareTo(e.getValidFrom()) < 0 && pb.getValidTo().compareTo(e.getValidFrom()) >= 0
					&& pb.getValidTo().compareTo(e.getValidTo()) <= 0) {
				pb.setValidTo(DateUtils.addDays(e.getValidFrom(), -1));
				updList.add(pb);
			} else if (pb.getValidFrom().compareTo(e.getValidFrom()) >= 0 && pb.getValidFrom().compareTo(e.getValidTo()) <= 0
					&& pb.getValidTo().compareTo(e.getValidTo()) > 0) {
				pb.setValidFrom(DateUtils.addDays(e.getValidTo(), 1));
				updList.add(pb);
			} else if (pb.getValidFrom().compareTo(e.getValidFrom()) >= 0 && pb.getValidTo().compareTo(e.getValidTo()) <= 0) {
				delList.add(pb.getId());
			} else if (pb.getValidFrom().compareTo(e.getValidFrom()) < 0 && pb.getValidTo().compareTo(e.getValidTo()) > 0) {

				ContractPriceCategory category = !pb.getContractPriceCategories().isEmpty() ? pb.getContractPriceCategories().iterator().next()
						: null;
				if (category != null) {
					pb.setDefauftPC(category.getDefaultPriceCtgy());
					pb.setInitialPrice(category.getInitialPrice());
					pb.setInitialCurrId(category.getInitialCurrId());
					pb.setInitialUnitId(category.getInitialUnitId());
					pb.setQuantityType(category.getQuantityType());
					pb.setContinous(category.getContinous());
					pb.setRestriction(category.getRestriction());
					pb.setFinancialSourceId(category.getFinancialSource().getId());
					pb.setAverageMethodId(category.getAverageMethod().getId());
					pb.setExchangeRateOffset(category.getExchangeRateOffset());
				}

				try {
					PricingBase generatedPb = EntityCloner.cloneEntity(pb);
					generatedPb.setValidFrom(DateUtils.addDays(e.getValidTo(), 1));
					List<ContractPriceCategory> categories = new ArrayList<>();
					generatedPb.setContractPriceCategories(categories);
					for (ContractPriceCategory c : pb.getContractPriceCategories()) {
						ContractPriceCategory newCategory = EntityCloner.cloneEntity(c);
						categories.add(newCategory);
						newCategory.setPricingBases(generatedPb);
						List<ContractPriceComponent> components = new ArrayList<>();
						newCategory.setPriceComponents(components);
						for (ContractPriceComponent comp : c.getPriceComponents()) {
							if (comp.getValidTo().compareTo(generatedPb.getValidFrom()) >= 0) {
								ContractPriceComponent newComponent = EntityCloner.cloneEntity(comp);
								if (newComponent.getValidFrom().before(generatedPb.getValidFrom())) {
									newComponent.setValidFrom(generatedPb.getValidFrom());
								}
								components.add(newComponent);
								newComponent.setContrPriceCtgry(newCategory);
								List<ContractPriceComponentConv> convertedComponents = new ArrayList<>();
								newComponent.setConvertedPrices(convertedComponents);
								Iterator<ContractPriceComponentConv> iter = comp.getConvertedPrices().iterator();
								while (iter.hasNext()) {
									ContractPriceComponentConv converted = iter.next();
									if (converted.getValidTo().before(newComponent.getValidFrom())) {
										iter.remove();
										continue;
									}
									ContractPriceComponentConv newConverted = EntityCloner.cloneEntity(converted);
									if (newConverted.getValidFrom().before(newComponent.getValidFrom())) {
										newConverted.setValidFrom(newComponent.getValidFrom());
									}
									convertedComponents.add(newConverted);
									newConverted.setContractPriceComponent(newComponent);
								}
							}
						}
					}
					pb.setValidTo(DateUtils.addDays(e.getValidFrom(), -1));
					updList.add(pb);
					insList.add(generatedPb);
				} catch (BeansException e1) {
					LOG.warn(e1.getMessage(), e1);
				}
			}
		}
		this.updateAsIs(updList);
		this.deleteContractPriceCategories(delList);
		this.onDeleteByIds(delList, null);
		this.insertAsIs(insList);
	}

	private void insertAsIs(List<PricingBase> list) throws BusinessException {
		for (PricingBase e : list) {
			this.onInsert(e);
		}
	}

	private void updateAsIs(List<PricingBase> list) throws BusinessException {
		for (PricingBase e : list) {
			this.updateContractPriceComponents(e);
			this.onUpdate(e);
		}
	}

	private void updateContractPriceComponents(PricingBase e) throws BusinessException {
		ContractPriceCategory_Bd bd = this.getBusinessDelegate(ContractPriceCategory_Bd.class);
		ContractPriceCategory category = !e.getContractPriceCategories().isEmpty() ? e.getContractPriceCategories().iterator().next() : null;
		if (category != null) {
			if (this.isIndex(e)) {
				bd.reGenerateContractPriceComponent(category);
			} else {
				bd.extendToMargins(category);
			}
		}
	}

	/**
	 * @param ids
	 *            A list of pricing base ids.
	 * @throws BusinessException
	 */
	private void deleteContractPriceCategories(List<Object> ids) throws BusinessException {
		List<ContractPriceCategory> list = new ArrayList<>();
		for (Object id : ids) {
			list.addAll(this.priceCategoryService.findByPricingBasesId((Integer) id));
		}

		// "log" deletion of price categories
		for (ContractPriceCategory contractPriceCateg : list) {
			Contract contract = contractPriceCateg.getContract();
			if (ContractChangeUtil.checkBlueprintProductChanges(contract)) {
				this.contractChangeService.addNewChange(contract, ContractChangeType._PRICE_,
						ContractChangeUtil.buildMessageForPriceDelete(contractPriceCateg));
			}
		}
		this.priceCategoryService.delete(list);
	}

	/**
	 * Generate contract price category for the new pricing base.
	 *
	 * @param pb
	 *            - new pricing base.
	 * @throws BusinessException
	 */
	private void generateContractPriceCategory(PricingBase pb, PricingBase_Bd bd) throws BusinessException {
		PriceCategory priceCategory = this.getPriceCategory(pb);
		if (pb.getContractPriceCategories() != null) {
			ContractPriceCategory cpcForNew = pb.getContractPriceCategories().iterator().next();
			bd.createPriceCtgry(priceCategory, pb, cpcForNew);
		} else {
			bd.createPriceCtgry(priceCategory, pb, null);

		}
	}

	/**
	 * @param e
	 * @return A list of product type Pricing bases without <code>e</code> ordered by pricing base valid from.
	 * @throws BusinessException
	 */
	@Override
	public List<PricingBase> getNeighbourProductPbs(PricingBase e) throws BusinessException {
		IPricingBaseService pbService = (IPricingBaseService) this.findEntityService(PricingBase.class);
		Contract c = e.getContract();
		List<PricingBase> list = new ArrayList<>(c.getPricingBases());

		Iterator<PricingBase> iter = list.iterator();
		while (iter.hasNext()) {
			PricingBase pb = iter.next();
			if (pb.equals(e) || !pbService.isProduct(pb)) {
				iter.remove();
			}
		}
		Collections.sort(list, (o1, o2) -> o1.getValidFrom().compareTo(o2.getValidTo()));
		return list;
	}

	/**
	 * @param c
	 * @return A list of product type Pricing bases without <code>e</code> ordered by pricing base valid from.
	 * @throws BusinessException
	 */
	@Override
	public List<PricingBase> buildProductPbList(Contract c) throws BusinessException {
		List<PricingBase> list = new ArrayList<>(c.getPricingBases());
		Iterator<PricingBase> iter = list.iterator();
		while (iter.hasNext()) {
			PricingBase pb = iter.next();
			if (!pb.getContractPriceCategories().isEmpty()) {
				pb.setVat(pb.getContractPriceCategories().iterator().next().getVat());
			}
			if (!this.isProduct(pb)) {
				iter.remove();
			}
		}
		Collections.sort(list, (o1, o2) -> o1.getValidFrom().compareTo(o2.getValidTo()));
		return list;
	}

	/**
	 * Get the price category list which are the same name with the pricing base's price category field.
	 *
	 * @param pb
	 *            - pricing base.
	 * @return
	 * @throws BusinessException
	 */
	private PriceCategory getPriceCategory(PricingBase pb) {
		Map<String, Object> params = new HashMap<>();
		params.put("name", pb.getPriceCat().getName());
		return this.findEntityByAttributes(PriceCategory.class, params);
	}

	/**
	 * @param pb
	 * @return true in case that PricingBase is a Product
	 */
	@Override
	public Boolean isProduct(PricingBase pricingBase) {
		return pricingBase.getPriceCat().getType().equals(PriceType._PRODUCT_)
				&& !PriceInd._COMPOSITE_.equals(pricingBase.getPriceCat().getPricePer());
	}

	@Override
	public Boolean isFeeOrTax(PricingBase pricingBase) {
		return pricingBase.getPriceCat().getType().equals(PriceType._OTHER_FEE_) || pricingBase.getPriceCat().getType().equals(PriceType._TAX_)
				|| pricingBase.getPriceCat().getType().equals(PriceType._DIFFERENTIAL_);
	}

	/**
	 * @param pricingBase
	 * @return true in case that PricingBase is an Index
	 */
	@Override
	public Boolean isIndex(PricingBase pricingBase) {
		return pricingBase.getPriceCat().getIsIndexBased();
	}

	/**
	 * Get quotation values for pricing base. Returns all quotation values that validity periods are in, or intersecting pricing base validity period.
	 * In case there are no quotation values that fulfill the previous condition than the method will return the last quotation value.
	 *
	 * @param pricingBase
	 *            .
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public List<QuotationValue> getQuotationValues(PricingBase pricingBase) throws BusinessException {
		List<QuotationValue> quotationValues = new ArrayList<>(pricingBase.getQuotation().getQuotationValues());
		MasterAgreementsPeriod offset = pricingBase.getQuotationOffset();
		AverageMethodCode code = AverageMethodCode.getByName(pricingBase.getQuotation().getAvgMethodIndicator().getCode());
		Date validFrom = pricingBase.getValidFrom();
		if (quotationValues.isEmpty()) {
			DateFormat sdf = DateFormat.getDateInstance(DateFormat.MEDIUM, java.util.Locale.getDefault());
			throw new BusinessException(CmmErrorCode.QUOTATION_VALUES_NOT_FOUND, String.format(CmmErrorCode.QUOTATION_VALUES_NOT_FOUND.getErrMsg(),
					pricingBase.getQuotation().getCalcVal(), sdf.format(validFrom)));
		}
		quotationValues.sort((o1, o2) -> o1.getValidFromDate().compareTo(o2.getValidFromDate()));
		List<QuotationValue> usedquotationValues = new ArrayList<>();
		Date validTo = pricingBase.getValidTo();
		validTo = this.adjustDateByOffset(validTo, code, offset);
		validFrom = this.adjustDateByOffset(validFrom, code, offset);
		for (QuotationValue quotationValue : quotationValues) {
			if (quotationValue.getValidFromDate().compareTo(validTo) <= 0) {
				usedquotationValues.add(quotationValue);
			}
		}
		quotationValues = usedquotationValues;
		// order by validTo date DESC
		quotationValues.sort((o1, o2) -> o2.getValidToDate().compareTo(o1.getValidToDate()));

		usedquotationValues = new ArrayList<>();
		QuotationValue lastUnused = null;
		for (QuotationValue quotationValue : quotationValues) {
			if (quotationValue.getValidToDate().compareTo(validFrom) >= 0) {
				usedquotationValues.add(quotationValue);
			} else {
				lastUnused = quotationValue;
				break;
			}
		}
		if (lastUnused != null) {
			usedquotationValues.add(lastUnused);
		}
		if (usedquotationValues.isEmpty()) {
			usedquotationValues.add(quotationValues.get(0));
		}
		quotationValues = this.fillTheGaps(usedquotationValues);
		return quotationValues;
	}

	/**
	 * in Traded quotations may have gaps between quotation values. It will this gaps with quotation value from left neighbour.
	 *
	 * @param quotationValues
	 */
	private List<QuotationValue> fillTheGaps(List<QuotationValue> quotationValues) {
		QuotationValue temp = null;
		List<QuotationValue> returnList = new ArrayList<>();
		quotationValues.sort((o1, o2) -> o1.getValidFromDate().compareTo(o2.getValidFromDate()));
		for (QuotationValue quotationValue : quotationValues) {
			if (temp != null) {
				Calendar validFromCal = GregorianCalendar.getInstance();
				validFromCal.setTime(quotationValue.getValidFromDate());
				validFromCal.add(Calendar.DAY_OF_YEAR, -1);
				while (validFromCal.getTime().compareTo(temp.getValidToDate()) > 0) {
					QuotationValue newValue = new QuotationValue();
					newValue.setActive(temp.getActive());
					newValue.setClientId(temp.getClientId());
					newValue.setCreatedAt(temp.getCreatedAt());
					newValue.setCreatedBy(temp.getCreatedBy());
					newValue.setIsProvisioned(temp.getIsProvisioned());
					newValue.setModifiedAt(temp.getModifiedAt());
					newValue.setModifiedBy(temp.getModifiedBy());
					newValue.setQuotation(temp.getQuotation());
					newValue.setRate(temp.getRate());
					newValue.setValidFromDate(this.adjustDateByOffset(temp.getValidFromDate(),
							AverageMethodCode.getByName(temp.getQuotation().getAvgMethodIndicator().getCode()), MasterAgreementsPeriod._CURRENT__1_));
					newValue.setValidToDate(this.adjustDateByOffset(temp.getValidToDate(),
							AverageMethodCode.getByName(temp.getQuotation().getAvgMethodIndicator().getCode()), MasterAgreementsPeriod._CURRENT__1_));
					returnList.add(newValue);
					temp = newValue;
				}
			}
			if (!returnList.contains(quotationValue)) {
				returnList.add(quotationValue);
			} else {
				returnList.set(returnList.indexOf(quotationValue), quotationValue);
			}
			temp = quotationValue;
		}
		return returnList;
	}

	/**
	 * @param inDate
	 * @param code
	 * @param offset
	 */
	private Date adjustDateByOffset(Date inDate, AverageMethodCode code, MasterAgreementsPeriod offset) {

		Date date = inDate;
		switch (code) {
		case DC:
		case WC:
		case WT:
			date = seava.j4e.business.utils.DateUtils.modifyDate(inDate, code.getCalField(), offset.getDelta());
			break;
		case MC:
		case MT:
			Calendar cal = GregorianCalendar.getInstance();
			cal.setTime(inDate);
			boolean firstDayOfMonth = 1 == cal.get(Calendar.DAY_OF_MONTH);
			boolean lastDayOFMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH) == cal.get(Calendar.DAY_OF_MONTH);
			date = seava.j4e.business.utils.DateUtils.modifyDate(inDate, code.getCalField(), offset.getDelta());
			cal.setTime(date);
			if (firstDayOfMonth) {
				cal.set(Calendar.DAY_OF_MONTH, 1);
			}
			if (lastDayOFMonth) {
				cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
			}
			date = cal.getTime();
			break;
		case SC:
		case ST:
			if (offset.getDelta() == -2) {
				date = seava.j4e.business.utils.DateUtils.modifyDate(inDate, Calendar.MONTH, -1);
			} else {
				Calendar inCal = GregorianCalendar.getInstance();
				inCal.setTime(inDate);
				if (inCal.get(Calendar.DAY_OF_MONTH) <= 15) {
					date = seava.j4e.business.utils.DateUtils.modifyDate(inDate, Calendar.DAY_OF_YEAR, 15 * offset.getDelta());
				} else {
					int delta = inCal.getActualMaximum(Calendar.DAY_OF_MONTH) - 15;
					date = seava.j4e.business.utils.DateUtils.modifyDate(inDate, Calendar.DAY_OF_YEAR, delta * offset.getDelta());
				}
			}
			break;
		case FC:
		case FT:
			if (offset.getDelta() == -2) {
				date = seava.j4e.business.utils.DateUtils.modifyDate(inDate, Calendar.MONTH, -1);
			} else {
				Calendar inCal = GregorianCalendar.getInstance();
				inCal.setTime(inDate);
				if (inCal.get(Calendar.DAY_OF_MONTH) >= 11 && inCal.get(Calendar.DAY_OF_MONTH) <= 25) {
					date = seava.j4e.business.utils.DateUtils.modifyDate(inDate, Calendar.DAY_OF_YEAR, 15 * offset.getDelta());
				} else {
					if (inCal.get(Calendar.DAY_OF_MONTH) <= 10) {
						inCal.add(Calendar.MONTH, -1);
					}
					int delta = inCal.getActualMaximum(Calendar.DAY_OF_MONTH) - 25 + 10;
					date = seava.j4e.business.utils.DateUtils.modifyDate(inDate, Calendar.DAY_OF_YEAR, delta * offset.getDelta());
				}
			}
			break;
		default:
			break;
		}
		return date;
	}
}
