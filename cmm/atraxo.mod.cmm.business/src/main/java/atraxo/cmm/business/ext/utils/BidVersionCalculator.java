package atraxo.cmm.business.ext.utils;

import java.util.List;

/**
 * Utility specialized class that calculates the next/previous version for a BID, given a <code>List</code> of existing versions.
 *
 * @author vhojda
 */
public class BidVersionCalculator {

	private static final String CHAR_A = "A";
	private static final String CHAR_Z = "Z";

	/**
	 * Calculates the new version, given a <code>List</code> of <code>String</code>s (versions)
	 *
	 * @param s
	 * @return
	 */
	public String calculateNewVersion(List<String> s) {
		String ss = this.calculateLastVersion(s);
		Version v = new Version(ss);
		v.increment();
		return v.getData();
	}

	/**
	 * Calculates the last version, given a <code>List</code> of <code>String</code>s (versions)
	 *
	 * @param s
	 * @return
	 */
	public String calculateLastVersion(List<String> ss) {
		Version v = new Version(CHAR_A);
		for (String sss : ss) {
			Version vv = new Version(sss);
			if (vv.compareTo(v) > 0) {
				v = vv;
			}
		}
		return v.getData();
	}

	/**
	 * Wrapper over a <code>String</code>, representing the version of a BID.
	 *
	 * @author vhojda
	 */
	private class Version {
		private String data;

		public Version(String s) {
			this.data = s;
		}

		public void increment() {
			String d = this.data;

			int zChars = 0;
			for (int i = d.length() - 1; i >= 0; i--) {
				char c = d.charAt(i);
				if (c == CHAR_Z.charAt(0)) {
					zChars++;
				} else {
					char next = (char) ((c) + 1);
					String dd = "";
					for (int j = d.length() - 1; j >= 0; j--) {
						if (j == i) {
							dd = String.valueOf(next) + dd;
						} else {
							dd = String.valueOf(d.charAt(j)) + dd;
						}
					}
					d = dd;
					break;
				}
			}

			if (zChars == d.length()) {
				String dd = "";
				for (int i = 0; i < d.length() + 1; i++) {
					dd = CHAR_A + dd;
				}
				d = dd;
			}

			this.data = d;
		}

		public int compareTo(Version o1) {
			String d1 = this.data;
			String d2 = o1.getData();
			if (d1.equals(d2)) {
				return 0;
			}

			if (d1.length() > d2.length()) {
				return 1;
			} else if (d1.length() < d2.length()) {
				return -1;
			} else {
				int result = 0;
				int size = d1.length();
				for (int i = size - 1; i >= 0; i--) {
					char c1 = d1.charAt(i);
					char c2 = d2.charAt(i);
					if (c1 > c2) {
						result = 1;
					} else if (c1 < c2) {
						result = -1;
					}
				}
				return result;
			}

		}

		public String getData() {
			return this.data;
		}

	}

}
