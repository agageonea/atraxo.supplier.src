/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.ext.shipTo.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractChangeService;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.shipTo.IShipToService;
import atraxo.cmm.business.ext.contracts.service.ContractChangeUtil;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.ext.util.EntityCloner;
import atraxo.fmbas.domain.impl.fmbas_type.ContractChangeType;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link ShipTo} domain entity.
 */
public class ShipTo_Service extends atraxo.cmm.business.impl.shipTo.ShipTo_Service implements IShipToService {

	private static final String CONTRACT = "contract";
	public static final String TEMPORARY_CHANGE_NA = "NA";

	private static final Logger LOG = LoggerFactory.getLogger(ShipTo_Service.class);

	@Autowired
	private IContractService contractService;
	@Autowired
	private ISystemParameterService systemParameterService;
	@Autowired
	private IContractChangeService contractChangeService;

	@Override
	protected void preInsert(ShipTo e) throws BusinessException {
		super.preInsert(e);
		if (ContractStatus._ACTIVE_.equals(e.getContract().getStatus()) || ContractStatus._EFFECTIVE_.equals(e.getContract().getStatus())) {
			Contract contract = this.cloneShipTo(e, null, e);
			if (contract != null && !this.systemParameterService.getSellContractApprovalWorkflow()) {
				this.contractService.checkIfCanActivateUpdate(contract, true);
			}
		}
	}

	@Override
	protected void preUpdate(ShipTo e) throws BusinessException {
		super.preUpdate(e);
		if (ContractStatus._ACTIVE_.equals(e.getContract().getStatus()) || ContractStatus._EFFECTIVE_.equals(e.getContract().getStatus())) {
			this.contractService.checkIfCanActivateUpdate(e.getContract(), true);
		}
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		List<ShipTo> shipTos = this.findByIds(ids, ShipTo.class);
		for (ShipTo shipTo : shipTos) {
			if (ContractStatus._ACTIVE_.equals(shipTo.getContract().getStatus())
					|| ContractStatus._EFFECTIVE_.equals(shipTo.getContract().getStatus())) {
				Contract contract = this.cloneShipTo(shipTo, shipTo, null);
				if (contract != null) {
					this.contractService.checkIfCanActivateUpdate(contract, true);
				}
			}

			// if in blueprint contract "log" the deletion of the existing ship-to
			if (ContractChangeUtil.checkBlueprintProductChanges(shipTo.getContract())) {
				this.contractChangeService.addNewChange(shipTo.getContract(), ContractChangeType._SHIPTO_,
						ContractChangeUtil.buildMessageForShipToDelete(shipTo));
			}
		}
		if (!shipTos.get(0).getContract().getIsContract()) {
			context.put(CONTRACT, shipTos.get(0).getContract());
		}
	}

	private Contract cloneShipTo(ShipTo shipTo, ShipTo toRemove, ShipTo toAdd) {
		try {
			Contract contract = EntityCloner.cloneEntity(shipTo.getContract());
			contract.setShipTo(new ArrayList<ShipTo>());
			for (ShipTo temp : shipTo.getContract().getShipTo()) {
				if (toRemove != null && !toRemove.equals(temp)) {
					contract.addToShipTo(EntityCloner.cloneEntity(temp));
				}
			}
			if (toAdd != null) {
				contract.addToShipTo(EntityCloner.cloneEntity(toAdd));
			}
			return contract;
		} catch (BeansException e) {
			LOG.error("Error", e);
		}
		return null;
	}

	@Override
	protected void postInsert(ShipTo e) throws BusinessException {
		super.postInsert(e);
		if (!e.getContract().getIsContract()) {
			this.updateBidTotals(e.getContract());
		}
		// if in blueprint contract "log" the insertion of the new ship-to
		if (ContractChangeUtil.checkBlueprintProductChanges(e.getContract())) {
			this.contractChangeService.addNewChange(e.getContract(), ContractChangeType._SHIPTO_, ContractChangeUtil.buildMessageForShipToInsert(e));
		}
	}

	@Override
	protected void postUpdate(List<ShipTo> list) throws BusinessException {
		super.postUpdate(list);
		Contract contract = null;
		for (ShipTo shipToElement : list) {
			// if in blueprint contract "log" the update of the existing ship-to
			contract = shipToElement.getContract();
			if (ContractChangeUtil.checkBlueprintProductChanges(contract)) {
				ShipTo oldShipTo = this.findById(shipToElement.getId());
				this.contractChangeService.addNewChange(contract, ContractChangeType._SHIPTO_,
						ContractChangeUtil.buildMessageForShipToUpdate(oldShipTo, shipToElement));
			}
		}
		if (contract != null && !contract.getIsContract()) {
			Contract refreshedContract = this.contractService.findById(contract.getId());
			this.updateBidTotals(refreshedContract);
			this.contractService.update(refreshedContract);
		}

	}

	@Override
	protected void postDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.postDeleteByIds(ids, context);
		if (context.get(CONTRACT) != null) {
			Contract contract = (Contract) context.get(CONTRACT);
			this.updateBidTotals(contract);
			if (!contract.getIsContract()) {
				this.contractService.validateBid(contract);
			}
		}
	}

	private void updateBidTotals(Contract contract) throws BusinessException {
		Collection<ShipTo> shipTos = contract.getShipTo();
		BigDecimal offeredVolume = BigDecimal.ZERO;
		BigDecimal awardedVolume = BigDecimal.ZERO;
		for (ShipTo s : shipTos) {
			offeredVolume = offeredVolume.add(s.getOfferedVolume() == null ? BigDecimal.ZERO : s.getOfferedVolume());
			awardedVolume = awardedVolume.add(s.getAllocatedVolume() == null ? BigDecimal.ZERO : s.getAllocatedVolume());
		}
		contract.setOfferedVolume(offeredVolume);
		contract.setAwardedVolume(awardedVolume);

	}

}