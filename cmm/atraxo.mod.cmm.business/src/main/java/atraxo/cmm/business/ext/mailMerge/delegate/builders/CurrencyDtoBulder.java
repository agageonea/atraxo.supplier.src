package atraxo.cmm.business.ext.mailMerge.delegate.builders;

import atraxo.cmm.domain.ext.mailmerge.dto.contract.CurrencyDto;
import atraxo.fmbas.domain.impl.currencies.Currencies;

/**
 * @author apetho
 */
public final class CurrencyDtoBulder {

	private CurrencyDtoBulder() {

	}

	public static final CurrencyDto buildCurrency(Currencies currency) {
		CurrencyDto dto = new CurrencyDto();
		if (currency != null) {
			dto.setIsoNumericCode(currency.getIsoNo());
			dto.setAlphabeticCode(currency.getCode());
			dto.setName(currency.getName());
		}
		return dto;
	}

}
