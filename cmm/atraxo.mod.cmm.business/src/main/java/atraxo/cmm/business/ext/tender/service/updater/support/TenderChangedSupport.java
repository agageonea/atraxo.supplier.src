package atraxo.cmm.business.ext.tender.service.updater.support;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.ext.tender.service.updater.listener.TenderPropertyAddedListener;
import atraxo.cmm.business.ext.tender.service.updater.listener.TenderPropertyRemovedListener;
import seava.j4e.api.exceptions.ActionNotSupportedException;
import seava.j4e.commons.utils.event.PropertyAddedEvent;
import seava.j4e.commons.utils.event.PropertyRemovedEvent;

public class TenderChangedSupport extends PropertyChangeSupport {

	private static final Logger LOG = LoggerFactory.getLogger(TenderChangedSupport.class);

	private static final long serialVersionUID = 110302982298139314L;

	public TenderChangedSupport(Object sourceBean) {
		super(sourceBean);
	}

	@Override
	public void firePropertyChange(PropertyChangeEvent event) {
		try {
			if (event instanceof seava.j4e.commons.utils.event.PropertyChangeEvent) {
				super.firePropertyChange(event);
			} else if (event instanceof PropertyAddedEvent) {
				this.firePropertyAdded(event);
			} else if (event instanceof PropertyRemovedEvent) {
				this.firePropertyRemoved(event);
			}
		} catch (ActionNotSupportedException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
		}
	}

	private void firePropertyAdded(PropertyChangeEvent event) throws ActionNotSupportedException {
		List<PropertyChangeListener> listeners = Arrays.asList(this.getPropertyChangeListeners());
		if (listeners != null) {
			for (PropertyChangeListener listener : listeners) {
				if (listener instanceof TenderPropertyAddedListener) {
					listener.propertyChange(event);
				}
			}
		}
	}

	private void firePropertyRemoved(PropertyChangeEvent event) throws ActionNotSupportedException {
		List<PropertyChangeListener> listeners = Arrays.asList(this.getPropertyChangeListeners());
		if (listeners != null) {
			for (PropertyChangeListener listener : listeners) {
				if (listener instanceof TenderPropertyRemovedListener) {
					listener.propertyChange(event);
				}
			}
		}
	}
}