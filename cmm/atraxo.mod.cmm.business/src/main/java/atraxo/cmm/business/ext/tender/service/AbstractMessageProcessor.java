/**
 *
 */
package atraxo.cmm.business.ext.tender.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.tender.ITenderService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceHistoryService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.geo.ILocationsService;
import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.business.api.notes.INotesService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.exceptions.SingleResultException;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.notes.Notes;
import seava.j4e.api.ISettings;
import seava.j4e.api.enums.SysParam;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.InvalidConfiguration;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.service.IClientInfoProvider;
import seava.j4e.api.session.IOrganization;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.IUserProfile;
import seava.j4e.api.session.IUserSettings;
import seava.j4e.api.session.IWorkspace;
import seava.j4e.api.session.Session;
import seava.j4e.commons.security.AppClient;
import seava.j4e.commons.security.AppUser;
import seava.j4e.commons.security.AppUserProfile;
import seava.j4e.commons.security.AppUserSettings;
import seava.j4e.commons.security.Organization;
import seava.j4e.iata.fuelplus.iata.tender.TenderIdentification;

/**
 * @author zspeter
 */
public class AbstractMessageProcessor {
	private static final String REQUEST_HEADER_CLIENT_CODE_ATTRIBUTE = "clientcode";

	protected static final String QUERY_PARAM_HOLDER = "holder";
	protected static final String QUERY_PARAM_IATA_CODE = "iataCode";

	protected static final String SUCCESS_MSG = "OK";
	protected static final String ERROR_MSG = "ERR";

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractMessageProcessor.class);

	@Autowired
	protected ExternalInterfaceHistoryFacade interfaceHistoryFacade;
	@Autowired
	protected ExternalInterfaceMessageHistoryFacade interfaceMessageHistoryFacade;
	@Autowired
	protected IChangeHistoryService historyService;
	@Autowired
	protected IContractService contractService;
	@Autowired
	private ILocationsService locationsService;
	@Autowired
	private INotesService noteService;
	@Autowired
	protected IExternalInterfaceHistoryService interfaceHistoryService;
	@Autowired
	private ITenderService tenderService;
	@Autowired
	private IUserSuppService userSrv;
	@Autowired
	protected IExternalInterfaceService interfaceService;
	@Autowired
	private ISettings settings;
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private ICustomerService customerService;

	@Resource
	private WebServiceContext context;

	private ExternalInterface fuelTenderInterface;

	protected void initSessionAndInterface(String interfaceName, MessageContext messageContext) throws BusinessException {
		String clientCode = this.getRequestHeaderAttribute(REQUEST_HEADER_CLIENT_CODE_ATTRIBUTE, messageContext);

		try {
			this.userSrv.createSessionUser(clientCode);
			this.fuelTenderInterface = this.interfaceService.findByName(interfaceName);
		} catch (ApplicationException nre) {
			LOGGER.error("Could not find external interface by identification, will throw BusinessException !", nre);
			throw new BusinessException(CmmErrorCode.NO_COMPANY_PROFILE_FOUND,
					String.format(CmmErrorCode.NO_COMPANY_PROFILE_FOUND.getErrMsg(), clientCode));
		}

		try {
			Map<String, Object> params = new HashMap<>();
			params.put("isSubsidiary", true);
			params.put("code", clientCode);
			Customer subsidiary = this.customerService.findEntityByAttributes(params);

			AppClient client = new AppClient(Session.user.get().getClientId(), clientCode, "");
			IUserSettings userSettings = AppUserSettings.newInstance(this.settings);
			IUserProfile profile = new AppUserProfile(true, null, false, false, false);

			client.setActiveSubsidiaryId(subsidiary.getRefid());

			String userCode = this.settings.getParam(SysParam.CORE_INTERFACE_USER.name());

			// create an incomplete user first
			IUser user = new AppUser(userCode, userCode, null, userCode, null, null, client, userSettings, profile, null, true);
			Session.user.set(user);

			// get the client workspace info
			IWorkspace ws = this.applicationContext.getBean(IClientInfoProvider.class).getClientWorkspace();
			user = new AppUser(userCode, userCode, null, userCode, null, null, client, userSettings, profile, ws, true);
			Session.user.set(user);

			IOrganization org = new Organization(subsidiary.getCode(), subsidiary.getName(), true, subsidiary.getRefid());
			Session.user.get().getProfile().addOrganisation(org);
		} catch (InvalidConfiguration e) {
			LOGGER.warn("Invalid configuration data for init session", e);
		}

	}

	@SuppressWarnings("unchecked")
	private String getRequestHeaderAttribute(String attributeName, MessageContext messageContext) {
		Map<String, List<String>> reguestHeaders = (Map<String, List<String>>) messageContext.get(MessageContext.HTTP_REQUEST_HEADERS);

		for (Map.Entry<String, List<String>> attribute : reguestHeaders.entrySet()) {
			String name = attribute.getKey();
			if (name.equalsIgnoreCase(attributeName)) {
				return attribute.getValue().get(0);
			}
		}

		return null;
	}

	protected ExternalInterface getFuelTenderInterface() {
		return this.fuelTenderInterface;
	}

	protected Tender findTendersByIdentification(TenderIdentification ident) throws BusinessException {
		String tenderCode = ident.getTenderCode();
		Long tenderVersion = ident.getTenderVersion().longValue();

		try {
			Customer tenderHolder = this.getCustomer(ident.getTenderHolderCode(), "TenderIdentification - TenderHolderCode");

			Map<String, Object> identificationParams = new HashMap<>();
			identificationParams.put("code", tenderCode);
			identificationParams.put("tenderVersion", tenderVersion);
			identificationParams.put(QUERY_PARAM_HOLDER, tenderHolder);

			return this.tenderService.findEntityByAttributes(identificationParams);
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw nre;
			}
			LOGGER.error("Could not find tender by identification, will throw BusinessException !", nre);
			throw new BusinessException(CmmErrorCode.TENDER_NO_RESULT, String.format(CmmErrorCode.TENDER_NO_RESULT.getErrMsg(), ident.getTenderCode(),
					ident.getTenderVersion(), ident.getTenderHolderCode()));
		}

	}

	protected Customer getCustomer(String code, String property) throws BusinessException {
		Customer customer = null;
		try {
			// First try and find customer by IATA Code
			Map<String, Object> params = new HashMap<>();
			params.put(QUERY_PARAM_IATA_CODE, code);

			customer = this.customerService.findEntityByAttributes(params);
		} catch (ApplicationException e) {
			LOGGER.warn("Could not find customer with iata code : " + code, e);
		}

		// If no customer is found by IATA Code then try and find by code
		if (customer == null) {
			try {
				customer = this.customerService.findByCode(code);
			} catch (ApplicationException nre1) {
				throw new SingleResultException(String.format(CmmErrorCode.SINGLE_RESULT_EXCEPTION_MESSAGE.getErrMsg(), "Customer", code, property),
						nre1);
			}
		}

		return customer;
	}

	/**
	 * Creates an HashMap with the data regarding the Tender Invitation for it to be sent further down to fmbas for notification processing. Due to
	 * the thing that fmbas doesn't see any cmm entity, I had to use an hashMap to send the information.
	 *
	 * @param tender
	 * @throws BusinessException
	 */
	protected void sendNotificationFromTender(Tender tender) throws BusinessException {
		Map<String, String> tenderMap = new HashMap<>();
		tenderMap.put("tender_issuer", tender.getHolder().getCode());
		tenderMap.put("tender_code", tender.getCode());
		tenderMap.put("tender_name", tender.getName());
		tenderMap.put("tender_version", tender.getVersion().toString());
		tenderMap.put("contact_person", tender.getContactPerson());
		tenderMap.put("contact_email", tender.getEmail());
		tenderMap.put("bidding_from", tender.getBiddingPeriodFrom().toString());
		tenderMap.put("bidding_to", tender.getBiddingPeriodTo().toString());

		this.interfaceHistoryService.sendNotification(this.fuelTenderInterface, tenderMap);
	}

	/**
	 * Saves a Note for a specific <code>AbstractEntity</code>, given an existing comments text
	 *
	 * @param entity
	 * @param comments
	 * @throws BusinessException
	 */
	protected void saveNotesIfComments(AbstractEntity entity, String comments) throws BusinessException {
		if (!StringUtils.isEmpty(comments)) {
			Notes note = new Notes();
			note.setObjectId(entity.getId());
			note.setObjectType(entity.getClass().getSimpleName());
			note.setNotes(comments);
			this.noteService.insert(note);
		}
	}

	protected Locations getLocation(String iataCode, String icaoCode) throws BusinessException {
		try {
			Map<String, Object> params = new HashMap<>();
			if (iataCode != null) {
				params.put(QUERY_PARAM_IATA_CODE, iataCode);
			} else {
				params.put("icaoCode", icaoCode);
			}

			return this.locationsService.findEntityByAttributes(params);
		} catch (ApplicationException nre) {
			String code = iataCode != null ? iataCode : icaoCode;
			String property = iataCode != null ? "LocationCodeIATA" : "LocationCodeICAO";
			throw new SingleResultException(String.format(CmmErrorCode.SINGLE_RESULT_EXCEPTION_MESSAGE.getErrMsg(), "Location", code, property), nre);
		}
	}
}
