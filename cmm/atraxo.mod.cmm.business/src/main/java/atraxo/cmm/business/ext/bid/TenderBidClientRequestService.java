/**
 *
 */
package atraxo.cmm.business.ext.bid;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.api.ext.datahub.IDataHubClientRequestService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcknowledge;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderResponse;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderServiceService;

/**
 * @author zspeter
 */
public class TenderBidClientRequestService implements IDataHubClientRequestService<Contract, FuelTenderAcknowledge> {

	private static final Logger LOG = LoggerFactory.getLogger(TenderBidClientRequestService.class);

	/*
	 * (non-Javadoc)
	 * @see
	 * atraxo.cmm.business.api.ext.datahub.IDataHubClientRequestService#submitRequest(seava.j4e.iata.fuelplus.iata.tender.FuelTenderServiceService,
	 * java.lang.Object)
	 */
	@Override
	public FuelTenderResponse submitRequest(FuelTenderServiceService client, FuelTenderAcknowledge dto) throws BusinessException {
		return client.getFuelTenderServicePort().fuelTenderAcknowledgement(dto);
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.business.api.ext.datahub.IDataHubClientRequestService#processDataHubBulkWsResult(atraxo.fmbas.domain.ws.dto.WSExecutionResult,
	 * java.util.Collection, java.lang.Class, java.lang.String)
	 */
	@Override
	public void processDataHubBulkWsResult(WSExecutionResult result, Collection<Contract> list, Class<FuelTenderAcknowledge> dtoClass, String reason)
			throws BusinessException {
		LOG.info("Acknowledgment synchronous response received: %s", result);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * atraxo.cmm.business.api.ext.datahub.IDataHubClientRequestService#processDataHubSingleWsResult(atraxo.fmbas.domain.ws.dto.WSExecutionResult,
	 * atraxo.fmbas.domain.impl.abstracts.AbstractEntity, java.lang.Class, java.lang.String)
	 */
	@Override
	public void processDataHubSingleWsResult(WSExecutionResult result, Contract entity, Class<FuelTenderAcknowledge> dtoClass, String reason)
			throws BusinessException {
		LOG.info("Acknowledgment synchronous response received: %s", result);
	}

}
