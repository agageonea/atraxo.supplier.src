package atraxo.cmm.business.ext.tender.service.updater.committer;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.cmm.domain.impl.tender.TenderLocationExpectedPrice;
import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.domain.ext.history.HistoryMessage;
import atraxo.fmbas.domain.ext.history.HistoryValue;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.commons.utils.listener.changes.Changes;
import seava.j4e.commons.utils.listener.changes.ObjectAdded;
import seava.j4e.commons.utils.listener.changes.ObjectRemoved;
import seava.j4e.commons.utils.listener.changes.ValueChanged;

/**
 * Commits the changes found after comparison
 *
 * @author abolindu
 */
public class TenderChangesCommitterService {

	@Autowired
	private IChangeHistoryService changeHistoryService;
	@Autowired
	private TenderMessageBuilderService builderService;

	private static final String IS_VALID = "isValid";

	public void commitChanges(Changes changes) throws BusinessException {
		try {
			HistoryMessage historyMessages = new HistoryMessage();

			List<ValueChanged> valueChangedList = changes.getValueChanges();
			historyMessages.merge(this.commitPropertyChanges(valueChangedList));

			List<ObjectAdded> objectAddedList = changes.getObjectAddedList();
			historyMessages.merge(this.commitAddedChanges(objectAddedList));

			List<ObjectRemoved> objectRemovedList = changes.getObjectRemovedList();
			historyMessages.merge(this.commitRemovedChanges(objectRemovedList));

			this.logToHistory(historyMessages);
		} catch (ReflectiveOperationException | IOException e) {
			throw new BusinessException(CmmErrorCode.BID_NOT_VALID, e.getMessage(), e);
		}
	}

	private HistoryMessage commitPropertyChanges(List<ValueChanged> valueChangedList) throws ReflectiveOperationException {
		HistoryMessage historyMessage = new HistoryMessage();
		if (valueChangedList != null) {
			for (ValueChanged valueChanged : valueChangedList) {

				this.setNewValue(valueChanged.getFieldName(), valueChanged.getNewValue(), valueChanged.getSource());
				Object changeSource = valueChanged.getSource();
				Object historySource = changeSource;
				Object entityIdentifier = null;

				if (changeSource.getClass().equals(TenderLocationAirlines.class)) {
					TenderLocationAirlines airline = (TenderLocationAirlines) changeSource;
					historySource = airline.getTenderLocation();
					entityIdentifier = airline.getAirline().getCode();
				} else if (changeSource.getClass().equals(TenderLocationRound.class)) {
					TenderLocationRound round = (TenderLocationRound) changeSource;
					historySource = round.getTenderLocation();
					entityIdentifier = round.getRoundNo();
				} else if (changeSource.getClass().equals(TenderLocationExpectedPrice.class)) {
					TenderLocationExpectedPrice expectedPrice = (TenderLocationExpectedPrice) changeSource;
					historySource = expectedPrice.getTenderLocation();
					entityIdentifier = expectedPrice.getIataPriceCategory().getCode();
				}

				HistoryValue historyValue = this.builderService.buildHistoryMessageValueChanged(valueChanged, entityIdentifier);
				List<HistoryValue> list = historyMessage.computeIfAbsent(historySource, k -> new ArrayList<>());
				list.add(historyValue);
				historyMessage.put(historySource, list);
			}
		}
		return historyMessage;
	}

	private HistoryMessage commitRemovedChanges(List<ObjectRemoved> objectRemovedList) throws ReflectiveOperationException {
		HistoryMessage historyMessage = new HistoryMessage();
		if (objectRemovedList != null) {
			for (ObjectRemoved objectRemoved : objectRemovedList) {

				Object changeSource = objectRemoved.getSource();
				Object removedEntity = objectRemoved.getRemovedEntity();

				if (removedEntity.getClass().equals(TenderLocation.class) || removedEntity.getClass().equals(TenderLocationAirlines.class)) {

					if (removedEntity.getClass().equals(TenderLocation.class)) {

						if (((TenderLocation) removedEntity).getIsValid()) {
							objectRemoved = new ObjectRemoved(removedEntity, ((TenderLocation) removedEntity).getTender(),
									objectRemoved.getMessageCode());
							for (TenderLocationAirlines airline : ((TenderLocation) removedEntity).getTenderLocationAirlines()) {
								this.setNewValue(IS_VALID, false, airline);
							}
						} else {
							continue;
						}

					} else if (removedEntity.getClass().equals(TenderLocationAirlines.class)) {
						if (((TenderLocationAirlines) removedEntity).getIsValid()) {
							objectRemoved = new ObjectRemoved(removedEntity, ((TenderLocationAirlines) removedEntity).getTenderLocation(),
									objectRemoved.getMessageCode());
						} else {
							continue;
						}
					}

					this.setNewValue(IS_VALID, false, removedEntity);

				} else if (changeSource.getClass().equals(TenderLocation.class) && objectRemoved.getRemovedEntity() != null) {
					this.removeFromOriginalList(removedEntity, (TenderLocation) changeSource);
				}

				HistoryValue historyValue = this.builderService.buildHistoryMessageValueRemoved(objectRemoved);
				List<HistoryValue> list = historyMessage.computeIfAbsent(objectRemoved.getSource(), k -> new ArrayList<>());
				list.add(historyValue);
				historyMessage.put(objectRemoved.getSource(), list);
			}
		}
		return historyMessage;
	}

	private HistoryMessage commitAddedChanges(List<ObjectAdded> objectAddedList) throws ReflectiveOperationException {
		HistoryMessage historyMessage = new HistoryMessage();
		if (objectAddedList != null) {
			for (ObjectAdded objectAdded : objectAddedList) {
				Object source = objectAdded.getSource();

				Object newEntity = objectAdded.getAddedEntity();
				if (source.getClass().equals(Tender.class)) {
					this.addToOriginalList(newEntity, source);
				} else if ((source.getClass().equals(TenderLocation.class) || source.getClass().equals(TenderLocationAirlines.class))
						&& objectAdded.getAddedEntity() == null) {
					this.setNewValue(IS_VALID, true, source);

					if (source.getClass().equals(TenderLocation.class)) {
						objectAdded = new ObjectAdded(source, ((TenderLocation) source).getTender(), objectAdded.getMessageCode());
					} else if (source.getClass().equals(TenderLocationAirlines.class)) {
						objectAdded = new ObjectAdded(source, ((TenderLocationAirlines) source).getTenderLocation(), objectAdded.getMessageCode());
					}

				} else if (source.getClass().equals(TenderLocation.class) && objectAdded.getAddedEntity() != null) {
					this.addToOriginalList(newEntity, source);
				}

				HistoryValue historyValue = this.builderService.buildHistoryMessageValueAdded(objectAdded);
				List<HistoryValue> list = historyMessage.computeIfAbsent(objectAdded.getSource(), k -> new ArrayList<>());
				list.add(historyValue);
				historyMessage.put(objectAdded.getSource(), list);
			}
		}
		return historyMessage;
	}

	private void setNewValue(String fieldName, Object newValue, Object entitySource) throws ReflectiveOperationException {
		String methodName = new StringBuilder("set").append(StringUtils.capitalize(fieldName)).toString();
		Method method = entitySource.getClass().getDeclaredMethod(methodName, newValue.getClass());
		method.invoke(entitySource, newValue);
	}

	private void addToOriginalList(Object newEntity, Object entitySource) throws ReflectiveOperationException {
		StringBuilder methodName = new StringBuilder("addTo").append(newEntity.getClass().getSimpleName());
		if (newEntity.getClass().equals(TenderLocationRound.class) || newEntity.getClass().equals(TenderLocationExpectedPrice.class)) {
			methodName.append("s");
		}

		Method method = entitySource.getClass().getDeclaredMethod(methodName.toString(), newEntity.getClass());
		method.invoke(entitySource, newEntity);
	}

	private void removeFromOriginalList(Object removeEntity, TenderLocation sourceEntity) {
		if (removeEntity instanceof TenderLocationRound) {
			List<TenderLocationRound> roundList = (List<TenderLocationRound>) sourceEntity.getTenderLocationRounds();
			this.remove(removeEntity, roundList);
		} else if (removeEntity instanceof TenderLocationExpectedPrice) {
			List<TenderLocationExpectedPrice> expectedPriceList = (List<TenderLocationExpectedPrice>) sourceEntity.getTenderLocationExpectedPrices();
			this.remove(removeEntity, expectedPriceList);
		}
	}

	private void remove(Object removeEntity, List<?> list) {
		Iterator<?> iterator = list.listIterator();
		while (iterator.hasNext()) {
			Object listElement = iterator.next();
			if (removeEntity.equals(listElement)) {
				iterator.remove();
			}
		}
	}

	private void logToHistory(HistoryMessage historyMessages) throws IOException, BusinessException {
		historyMessages.merge(this.buildChangesSummary(historyMessages));

		for (Map.Entry<Object, List<HistoryValue>> message : historyMessages.entrySet()) {
			ObjectWriter ow = new ObjectMapper().writer();
			String json = ow.writeValueAsString(message.getValue());

			this.changeHistoryService.saveChangeHistory((AbstractEntity) message.getKey(), TenderStatus._UPDATED_.getName(), json);
		}
	}

	private HistoryMessage buildChangesSummary(HistoryMessage historyMessages) {
		HistoryMessage summaryMessage = new HistoryMessage();
		for (Map.Entry<Object, List<HistoryValue>> message : historyMessages.entrySet()) {
			if (message.getKey() instanceof TenderLocation) {

				TenderLocation location = (TenderLocation) message.getKey();
				HistoryValue historyValue = new HistoryValue();
				historyValue.setCode("2011");
				List<Object> arguments = new LinkedList<>();
				arguments.add(location.getLocation().getCode());
				arguments.add(location.getFuelProduct().getName());
				arguments.add(location.getTaxType().getName());
				arguments.add(location.getFlightServiceType().getName());
				arguments.add(location.getDeliveryPoint().getName());
				historyValue.setArguments(arguments);

				List<HistoryValue> list = summaryMessage.computeIfAbsent(location.getTender(), k -> new ArrayList<>());
				list.add(historyValue);
				summaryMessage.put(location.getTender(), list);
			}
		}
		return summaryMessage;
	}
}
