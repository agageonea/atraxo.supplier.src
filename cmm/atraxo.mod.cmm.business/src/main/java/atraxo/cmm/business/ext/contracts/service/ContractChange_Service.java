/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.ext.contracts.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.cmm.business.api.contracts.IContractChangeService;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.ContractChange;
import atraxo.fmbas.business.ext.util.LambdaUtil;
import atraxo.fmbas.domain.impl.fmbas_type.ContractChangeType;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link ContractChange} domain entity.
 */
public class ContractChange_Service extends atraxo.cmm.business.impl.contracts.ContractChange_Service implements IContractChangeService {

	private static final String CONTRACT_ID = "contractId";
	private static final String TYPE = "type";

	@Autowired
	private IContractService contractService;

	@Override
	public void addNewChange(Contract contract, ContractChangeType type, String message) throws BusinessException {
		ContractChange change = new ContractChange();
		change.setContractId(contract.getId());
		change.setType(type);
		change.setMessage(message);
		this.insert(change);
	}

	@Override
	public List<ContractChange> getContractChanges(Contract contract) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put(CONTRACT_ID, contract.getId());
		return this.findEntitiesByAttributes(ContractChange.class, params);
	}

	@Override
	public List<ContractChange> getWorkflowContractChanges(Contract contract) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put(CONTRACT_ID, contract.getId());
		params.put(TYPE, LambdaUtil.filterEnum(ContractChangeType.class, c -> !c.equals(ContractChangeType._ATTACHMENT_)));
		return this.findEntitiesByAttributes(ContractChange.class, params);
	}

	@Override
	@Transactional
	public List<ContractChange> getContractChanges(Contract contract, ContractChangeType type) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put(CONTRACT_ID, contract.getId());
		params.put(TYPE, type);
		return this.findEntitiesByAttributes(ContractChange.class, params);
	}

	@Override
	public void removeContractChanges(Contract contract) throws BusinessException {
		List<ContractChange> changes = this.getContractChanges(contract);
		this.delete(changes);
	}

	@Override
	public void removeContractChange(Contract contract, ContractChangeType type) throws BusinessException {
		List<ContractChange> changes = this.getContractChanges(contract, type);
		this.delete(changes);
	}

	@Override
	public void attachmentChange(Attachment attachment) throws BusinessException {
		Contract contract = this.contractService.findById(Integer.parseInt(attachment.getTargetRefid()));

		// If blueprint add change to contract
		if (contract != null && contract.getIsBlueprint()) {
			// Set the change description message based on the action
			String message = attachment.getVersion() == null ? ContractChangeUtil.buildMessageForAttachmentToInsert(attachment, contract.getCode())
					: ContractChangeUtil.buildMessageForAttachmentToDelete(attachment, contract.getCode());

			this.addNewChange(contract, ContractChangeType._ATTACHMENT_, message);
		}
	}

}
