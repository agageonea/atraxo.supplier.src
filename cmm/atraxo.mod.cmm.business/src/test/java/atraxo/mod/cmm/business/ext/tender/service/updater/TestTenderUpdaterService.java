package atraxo.mod.cmm.business.ext.tender.service.updater;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import atraxo.cmm.business.ext.tender.service.updater.TenderUpdaterService;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.mod.cmm.business.utils.DummyEntityFactory;
import seava.j4e.api.exceptions.ActionNotSupportedException;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.Session;
import seava.j4e.commons.security.AppClient;
import seava.j4e.commons.security.AppUser;
import seava.j4e.commons.utils.listener.changes.ObjectAdded;
import seava.j4e.commons.utils.listener.changes.ObjectRemoved;

/**
 * @author zspeter
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring/jee/cmm-business-test-context.xml")
public class TestTenderUpdaterService {

	@Autowired
	private TenderUpdaterService updaterSrv;

	private Date now = GregorianCalendar.getInstance().getTime();

	@BeforeClass
	public static void beforeClass() {
		AppClient client = new AppClient("id", "code", "name");
		IUser user = new AppUser("code", "name", "loginName", "password", null, null, client, null, null, null, false);
		Session.user.set(user);
	}

	@Test
	public void testNewTenderLocation() throws ActionNotSupportedException, ReflectiveOperationException, CloneNotSupportedException {
		Tender oldTender = DummyEntityFactory.getDummyTender(this.now);
		Tender newTender = DummyEntityFactory.getDummyTender(this.now);

		TenderLocation location = DummyEntityFactory.getDummyTenderLocation(this.now);
		location.setFuelProduct(Product._JET_A_);
		newTender.addToTenderLocation(location);

		assertEquals(-1, this.updaterSrv.compare(oldTender, newTender));
		List<ObjectAdded> objectAddedList = this.updaterSrv.getComparator().getChanges().getObjectAddedList();
		assertEquals(1, objectAddedList.size());
		assertEquals(Tender.class, objectAddedList.iterator().next().getSource().getClass());
		assertEquals(TenderLocation.class, objectAddedList.iterator().next().getAddedEntity().getClass());
	}

	@Test
	public void testValidateTenderLocation() throws ActionNotSupportedException, ReflectiveOperationException, CloneNotSupportedException {
		Tender oldTender = DummyEntityFactory.getDummyTender(this.now);
		TenderLocation oldLocation = oldTender.getTenderLocation().iterator().next();
		oldLocation.setIsValid(false);

		Tender newTender = DummyEntityFactory.getDummyTender(this.now);
		TenderLocation newLocation = newTender.getTenderLocation().iterator().next();
		newLocation.setIsValid(true);

		assertEquals(-1, this.updaterSrv.compare(oldTender, newTender));
		List<ObjectAdded> objectAddedList = this.updaterSrv.getComparator().getChanges().getObjectAddedList();
		assertEquals(1, objectAddedList.size());
		assertEquals(TenderLocation.class, objectAddedList.iterator().next().getSource().getClass());
		assertNull(objectAddedList.iterator().next().getAddedEntity());
	}

	@Test
	public void testInvalidateTenderLocation() throws ActionNotSupportedException, ReflectiveOperationException, CloneNotSupportedException {
		Tender oldTender = DummyEntityFactory.getDummyTender(this.now);
		TenderLocation oldLocation = DummyEntityFactory.getDummyTenderLocation(this.now);
		oldLocation.setTaxType(TaxType._DOMESTIC_);
		oldTender.addToTenderLocation(oldLocation);

		Tender newTender = DummyEntityFactory.getDummyTender(this.now);

		assertEquals(-1, this.updaterSrv.compare(oldTender, newTender));
		List<ObjectRemoved> objectRemovedList = this.updaterSrv.getComparator().getChanges().getObjectRemovedList();
		assertEquals(1, objectRemovedList.size());
		assertEquals(Tender.class, objectRemovedList.iterator().next().getSource().getClass());
		assertEquals(TenderLocation.class, objectRemovedList.iterator().next().getRemovedEntity().getClass());
	}

}