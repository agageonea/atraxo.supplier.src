package atraxo.mod.cmm.business.ext.contracts.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;

import org.junit.Test;

import atraxo.cmm.business.ext.contracts.service.ContractUtil;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;

public class TestContractUtil {

	@Test
	public void testCanPublishChanges() throws ParseException {
		assertTrue(ContractUtil.canPublishChanges(false, true, BidApprovalStatus._APPROVED_));
		assertTrue(ContractUtil.canPublishChanges(true, false, BidApprovalStatus._APPROVED_));
		assertFalse(ContractUtil.canPublishChanges(true, true, BidApprovalStatus._APPROVED_));
		assertFalse(ContractUtil.canPublishChanges(false, false, BidApprovalStatus._APPROVED_));
		assertFalse(ContractUtil.canPublishChanges(true, true, BidApprovalStatus._APPROVED_));
	}

}
