package atraxo.mod.cmm.business.ext.mailMerge.delegate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import atraxo.cmm.domain.ext.mailmerge.dto.contract.ContractDto;
import atraxo.cmm.domain.ext.mailmerge.dto.contract.ContractPriceDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.CustomerDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;

class TestDummyObjectGenerator {

	public static CustomerDto generateCustomer() {
		CustomerDto customer = new CustomerDto();
		customer.addContract(generateContract());
		// customer.setLocation("Dub");
		customer.setName("PAV");
		return customer;
	}

	private static AbstractDto generateContract() {
		ContractDto contract = new ContractDto();
		contract.setNumber("Contract 007");
		contract.addContractPrice(generatePrice());
		return contract;
	}

	private static ContractPriceDto generatePrice() {
		ContractPriceDto price = new ContractPriceDto();
		price.setCategoryName("Fix");
		// price.setCurrency("Eur");
		price.setName("TestCategory");
		price.setNewPrice(BigDecimal.TEN);
		price.setNewSettlementPrice(BigDecimal.TEN);
		price.setOldPrice(BigDecimal.ONE);
		price.setOldSettlementPrice(BigDecimal.ONE);
		// price.setSettlementCurrency("Eur");
		// price.setSettlementUnit("Kg");
		// price.setUnit("Kg");
		price.setValidFrom(new Date());
		return price;
	}

	public static List<EmailDto> generateEmail() {
		List<EmailDto> email = new ArrayList<>();
		EmailDto e = new EmailDto();
		e.setFrom("test@test.com");
		e.setSubject("test");
		email.add(e);
		return email;
	}
}
