/**
 *
 */
package atraxo.mod.cmm.business.utils;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.Period;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.cmm_type.TenderSource;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.domain.impl.cmm_type.TenderType;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.cmm.domain.impl.tender.TenderLocationExpectedPrice;
import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import atraxo.fmbas.domain.impl.categories.IataPC;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.ProductSource;
import atraxo.fmbas.domain.impl.fmbas_type.Use;
import atraxo.fmbas.domain.impl.geo.Locations;

/**
 * @author zspeter
 */
public class DummyEntityFactory {

	public static Tender getDummyTender(Date date) {
		Tender tender = new Tender();
		tender.setId(1);
		tender.setHolder(getDummyCustomer());
		tender.setCode("tenderCode");
		tender.setName("TenderName");
		tender.setTenderVersion(1l);
		tender.setType(TenderType._PRODUCT_);
		tender.setContactPerson("contactPerson");
		tender.setEmail("email@tendercode.tld");
		tender.setPhone("phone");
		DateUtils.addDays(date, 7);
		tender.setBiddingPeriodFrom(DateUtils.addDays(date, 7));
		tender.setBiddingPeriodTo(DateUtils.addDays(date, 17));
		tender.setStatus(TenderStatus._NEW_);
		tender.setBiddingStatus(BiddingStatus._EMPTY_);
		tender.setTransmissionStatus(TransmissionStatus._EMPTY_);
		tender.setSource(TenderSource._IMPORTED_);
		tender.setClosed(true);
		tender.setMsgId(1l);
		tender.setNoOfRounds(3);
		tender.addToTenderLocation(getDummyTenderLocation(date));

		return tender;
	}

	/**
	 * @return
	 */
	public static TenderLocation getDummyTenderLocation(Date date) {
		TenderLocation tenderLocation = new TenderLocation();
		tenderLocation.setId(1);
		tenderLocation.setLocation(getDummyLocation());
		tenderLocation.setTaxType(TaxType._EMPTY_);
		tenderLocation.setFuelProduct(Product._JET_A1_);
		tenderLocation.setFlightServiceType(FlightServiceType._AD_HOC_);
		tenderLocation.setDeliveryPoint(ContractSubType._INTO_PLANE_);
		tenderLocation.setLocationBiddingStatus(BiddingStatus._EMPTY_);
		tenderLocation.setAdHocLoc(false);

		tenderLocation.setAgreementFrom(DateUtils.addMonths(date, 1));
		tenderLocation.setAgreementTo(DateUtils.addMonths(date, 2));
		tenderLocation.setVolume(BigDecimal.valueOf(120000));

		tenderLocation.addToTenderLocationRounds(getDummyTenderLocationRound(date));
		tenderLocation.addToTenderLocationExpectedPrices(getDummyTenderLocationExpectedPrice());
		tenderLocation.addToTenderLocationAirlines(getDummyTenderLocationAirlines());
		return tenderLocation;
	}

	/**
	 * @return
	 */
	public static TenderLocationAirlines getDummyTenderLocationAirlines() {
		TenderLocationAirlines airline = new TenderLocationAirlines();
		airline.setId(1);
		airline.setAirline(getDummyCustomer());
		airline.setVolume(BigDecimal.valueOf(120000));
		airline.setVolumePeriod(Period._CONTRACT_PERIOD_);
		airline.setBiddingStatus(BiddingStatus._EMPTY_);
		airline.setIsValid(true);
		return airline;
	}

	/**
	 * @return
	 */
	public static TenderLocationExpectedPrice getDummyTenderLocationExpectedPrice() {
		TenderLocationExpectedPrice expectedPrice = new TenderLocationExpectedPrice();
		expectedPrice.setId(1);
		expectedPrice.setIataPriceCategory(getDummyIataPC());
		return expectedPrice;
	}

	/**
	 * @return
	 */
	public static IataPC getDummyIataPC() {
		IataPC iataPc = new IataPC();
		iataPc.setId(1);
		iataPc.setActive(true);
		iataPc.setCode("JETA1");
		iataPc.setName("JetA1");
		iataPc.setSource(ProductSource._STANDARD_);
		iataPc.setUse(Use._PRODUCT_);
		return iataPc;
	}

	/**
	 * @return
	 */
	public static TenderLocationRound getDummyTenderLocationRound(Date date) {
		TenderLocationRound round = new TenderLocationRound();
		round.setId(1);
		round.setRoundNo(1);

		round.setBiddingFrom(DateUtils.addDays(date, 7));
		round.setBiddingTo(DateUtils.addDays(date, 9));
		round.setBidOpening(DateUtils.addDays(date, 10));
		round.setActive(false);
		return round;
	}

	public static Locations getDummyLocation() {
		Locations location = new Locations();
		location.setCode("BRA");
		location.setName("Brasov");
		location.setId(1);
		return location;
	}

	public static Customer getDummyCustomer() {
		Customer customer = new Customer();
		customer.setCode("customerCode");
		customer.setId(1);
		return customer;
	}
}