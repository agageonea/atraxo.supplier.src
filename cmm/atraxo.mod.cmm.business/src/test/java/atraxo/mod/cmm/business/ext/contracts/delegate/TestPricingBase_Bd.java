package atraxo.mod.cmm.business.ext.contracts.delegate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.ext.contracts.delegate.PricingBase_Bd;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import seava.j4e.api.exceptions.BusinessException;

public class TestPricingBase_Bd {

	private final static Logger LOGGER = LoggerFactory.getLogger(TestPricingBase_Bd.class);

	@Test
	public void testVerificateDatesTrue() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
		Date fromDate = sdf.parse("2014.01.01");
		Date toDate = sdf.parse("2015.01.01");
		PricingBase_Bd bd = new PricingBase_Bd();
		try {
			bd.verifyDates(fromDate, toDate);
			assertTrue(true);
		} catch (BusinessException e) {
			LOGGER.error("BUSINESS EXCEPTION:", e);
			assertFalse(true);
		} finally {
			this.testVerificateDatesFalse(sdf, fromDate, toDate, bd);
		}
	}

	public void testVerificateDatesFalse(SimpleDateFormat sdf, Date fromDate, Date toDate, PricingBase_Bd bd) throws ParseException {

		try {
			bd.verifyDates(toDate, fromDate);
			assertTrue(false);

		} catch (BusinessException e) {
			LOGGER.error("BUSINESS EXCEPTION:", e);
			assertFalse(false);
		} finally {
			this.testIsBetweendates(sdf, fromDate, toDate, bd);
		}
	}

	public void testIsBetweendates(SimpleDateFormat sdf, Date fromDate, Date toDate, PricingBase_Bd bd) throws ParseException {
		Date date = sdf.parse("2014.06.01");
		assertTrue(bd.isBetweenDates(fromDate, toDate, date));
		assertFalse(bd.isBetweenDates(fromDate, date, toDate));
		this.testCheckPeriods(sdf, fromDate, toDate, bd, date);
	}

	public void testCheckPeriods(SimpleDateFormat sdf, Date fromDate, Date toDate, PricingBase_Bd bd, Date date) {
		Contract c = new Contract();
		c.setValidFrom(fromDate);
		c.setValidTo(date);
		PricingBase pb = new PricingBase();
		pb.setContract(c);
		pb.setValidFrom(fromDate);
		pb.setValidTo(toDate);
		try {
			bd.checkPeriods(pb);
			assertTrue(false);
		} catch (BusinessException e) {
			LOGGER.error("BUSINESS EXCEPTION:", e);
			assertFalse(false);
		}
		c.setValidTo(toDate);
		pb.setValidTo(date);
		try {
			bd.checkPeriods(pb);
			assertTrue(true);
		} catch (BusinessException e) {
			LOGGER.error("BUSINESS EXCEPTION:", e);
			assertFalse("Pricing base business delegate check periods.", true);
		}
	}

}
