package atraxo.mod.cmm.business.ext.mailMerge.delegate;

import static org.junit.Assert.assertFalse;

import org.junit.Test;

import atraxo.fmbas.business.ext.mailMerge.delegate.CustomerDtoManager;
import atraxo.fmbas.domain.ext.mailmerge.dto.CustomerDto;
import seava.j4e.api.exceptions.BusinessException;

public class TestDtoManager {

	@Test
	public void testObjectToJson() throws BusinessException {
		CustomerDto customer = TestDummyObjectGenerator.generateCustomer();
		String jsonData = CustomerDtoManager.getCustomerContractJsonData(customer, "yyyy-MM-dd");
		assertFalse(jsonData == null || jsonData.isEmpty());
		// EmailDto email = TestDummyObjectGenerator.generateEmail();
		// String json = DtoManager.getDataJson(customer, "docx", email, "yyyy-MM-dd");
		// assertFalse(json == null || json.isEmpty());
	}

}
