Ext.define("atraxo.cmm.i18n.ds.TenderBid_Ds", {
	airlinesList__lbl: ".Airlines",
	anotherBidSelection__lbl: ".Another Bid Selection",
	approvalNote__lbl: ".Approval Note",
	averageMethodId__lbl: ".Average Method Id",
	averageMethodName__lbl: ".Average Method Name",
	avgMethodId__lbl: ".Average Method(ID)",
	avgMethodName__lbl: ".Average Method(Name)",
	avgMethodName__tlp: ".Name",
	awardedVolume__lbl: ".Awarded volume",
	awardedVolume__tlp: ".Awarded volume",
	bidAcceptAwardTransmissionStatus__lbl: ".Accept award transmission status",
	bidApprovalStatus__lbl: ".Approval Status",
	bidApprovalStatus__tlp: ".Approval Status",
	bidAstmSpecification__lbl: ".ASTM specification",
	bidAstmSpecification__tlp: ".ASTM specification",
	bidBankGuarantee__lbl: ".Bank guarantee",
	bidBankGuarantee__tlp: ".Bid bank guarantee",
	bidCancelReason__lbl: ".Bid Cancel Reason",
	bidCancelTransmitionStatus__lbl: ".Bid cancel transmission status",
	bidDeclineAwardReason__lbl: ".Bid Decline Award Reason",
	bidDeclineAwardTransmissionStatus__lbl: ".Decline award transmission status",
	bidIdentId__lbl: ".Bid Tender Identification(ID)",
	bidLocBiddingStatus__lbl: ".Location Bidding Status",
	bidLocBiddingStatus__tlp: ".Status of the bidding process for a particular location",
	bidLocId__lbl: ".Bid Tender Location(ID)",
	bidLocLocationIdId__lbl: ".Tender location(ID)",
	bidLocVolUnitCode__lbl: ".Volume Unit",
	bidLocVolUnitCode__tlp: ".Code",
	bidLocVolUnitId__lbl: ".Volume Unit(ID)",
	bidLocVolUnitId__tlp: ".Unit of measurement for total volume",
	bidLocVolume__lbl: ".Volume",
	bidLocVolume__tlp: ".Total volume required for location for the indicated period",
	bidOperatingHours__lbl: ".Operating hours",
	bidOperatingHours__tlp: ".Operating hours",
	bidPackageIdentifier__lbl: ".Package Identifier",
	bidPackageIdentifier__tlp: ".Package Identifier",
	bidPayementFreq__lbl: ".Bid payment frequency",
	bidPayementFreq__tlp: ".Bid payment frequency",
	bidPaymentType__lbl: ".Bid payment type",
	bidPaymentType__tlp: ".Bid payment frequency",
	bidPrepaidAmount__lbl: ".Bid prepaid amount",
	bidPrepaidAmount__tlp: ".Bid prepaid amount",
	bidPrepaidDays__lbl: ".Bid prepaid days",
	bidPrepaidDays__tlp: ".Bid prepayed days",
	bidPrepayFirstDeliveryDate__lbl: ".First delivery date: payment date plus ",
	bidPrepayFirstDeliveryDate__tlp: ".First delivery date: payment date plus ",
	bidRevision__lbl: ".Bid revision",
	bidRevision__tlp: ".Bid revision",
	bidRound__lbl: ".Bidding Round",
	bidStatus__lbl: ".Bid status",
	bidStatus__tlp: ".Bid status",
	bidTitleTransfer__lbl: ".Title transfer",
	bidTitleTransfer__tlp: ".Title transfer",
	bidTransmissionStatus__lbl: ".Transmission status",
	bidTransmissionStatus__tlp: ".Transmission status",
	bidValidFrom__lbl: ".Bid valid from",
	bidValidFrom__tlp: ".Bid valid from",
	bidValidTo__lbl: ".Bid deadline",
	bidValidTo__tlp: ".Bid deadline",
	bidVersion__lbl: ".Bid version",
	bidVersion__tlp: ".Bid version",
	bidVolumePerAirline__lbl: ".Bid volume per airlines",
	bidVolumePerAirline__tlp: ".Bid volume per airlines",
	billToCode__lbl: ".Bill To",
	billToCode__tlp: ".Account code",
	billToId__lbl: ".Bill To(ID)",
	billToType__lbl: ".Type",
	billToType__tlp: ".Type",
	canBeCompleted__lbl: "",
	canCancel__lbl: ".Can Cancel a bid",
	code__lbl: ".Bid code",
	code__tlp: ".Contract No",
	companyId__lbl: ".Company Id",
	contVolUnitCode__lbl: ".Unit",
	contVolUnitCode__tlp: ".contract volume unit",
	contVolUnitId__lbl: ".Contract Volume Unit(ID)",
	contactId__lbl: ".Contact(ID)",
	contactName__lbl: ".Buyer",
	contactName__tlp: ".Buyer",
	counterpartyReference__lbl: ".Counterparty",
	counterpartyReference__tlp: ".Counterparty",
	countryId__lbl: ".Country ID",
	countryId__tlp: ".Country ID",
	countryName__lbl: ".Country Name",
	countryName__tlp: ".Country Name",
	cpRefId__lbl: ".Customer(Ref-ID)",
	creditTerms__lbl: ".Credit Terms",
	creditTerms__tlp: ".Credit terms",
	currencyCode__lbl: ".Currency Code",
	currencyID__lbl: ".Currency ID",
	customerCode__lbl: ".Customer",
	customerCode__tlp: ".Customer",
	customerField__lbl: "",
	customerId__lbl: ".Customer(ID)",
	customerName__lbl: ".Customer name",
	customerName__tlp: ".Customer name",
	daysFrequency__lbl: ".days",
	days__lbl: ". days",
	dealType__lbl: ".Deal type",
	dealType__tlp: ".Deal type",
	eventType__lbl: ".Event type",
	eventType__tlp: ".Event type",
	events__lbl: ".events",
	events__tlp: ".events",
	exchangeRateOffset__lbl: ".Period",
	exchangeRateOffset__tlp: ".Period",
	exportType__lbl: ".Export Type",
	financialSourceCode__lbl: ".Financial Source Code",
	financialSourceCode__tlp: ".Code",
	financialSourceId__lbl: ".Financial Source Id",
	flightServiceType__lbl: ".Operational type",
	flightServiceType__tlp: ".Operational type",
	forex__lbl: ".Forex",
	forex__tlp: ".Using ForEX of",
	formTitle__lbl: ".Bid Location",
	formTitle__tlp: ".Bid Location",
	generatedContractId__lbl: ".Generated Contract Id",
	hasTotalVolume__lbl: ".Has total volume",
	hasTotalVolume__tlp: ".Has total volume",
	holderCode__lbl: ".Holder",
	holderCode__tlp: ".Account code",
	holderContactId__lbl: ".Contact(ID)",
	holderContactId__tlp: ".Points to an existing contact from the holder",
	holderContactName__lbl: ".Full name",
	holderContactName__tlp: ".Full name",
	holderId__lbl: ".Holder(ID)",
	holderName__lbl: ".Holder Name",
	holderName__tlp: ".Name",
	iataServiceLevel__lbl: ".IATA service level",
	iataServiceLevel__tlp: ".IATA service level",
	invFreq__lbl: ".Inv Freq",
	invType__lbl: ".Inv Type",
	invalidBidIds__lbl: ".Invalid Bid Ids",
	invoiceFreqNumber__lbl: ".Invoice frequency number",
	invoiceFreqNumber__tlp: ".Invoice frequency number",
	invoiceFreq__lbl: ".Invoice frequency",
	invoiceFreq__tlp: ".Invoice frequency",
	invoicePayableBy__lbl: ".Payable by",
	invoicePayableBy__tlp: ".Payable by",
	invoiceType__lbl: ".Invoice type",
	invoiceType__tlp: ".Invoice type",
	iplCode__lbl: ".IPL Agent",
	iplCode__tlp: ".ipl agent",
	iplId__lbl: ".Into Plane Agent(ID)",
	isContract__lbl: ".Is contract?",
	isContract__tlp: ".Is contract?",
	isCustomer__lbl: ".Is customer?",
	isCustomer__tlp: ".Is customer?",
	isSpot__lbl: ".Term/Spot",
	isSpot__tlp: ".Term/Spot",
	labelField__lbl: "",
	limitedTo__lbl: ".Flight type",
	limitedTo__tlp: ".Flight type",
	lineDelim__lbl: ". - ",
	locAirportName__lbl: ".Location",
	locAirportName__tlp: ".Location",
	locCode__lbl: ".Location Code",
	locCode__tlp: ".Location Code",
	locId__lbl: ".Location(ID)",
	marked__lbl: ".Generated bid",
	newRevisionDescription__lbl: ".New Revision Description",
	newRevisionSelection__lbl: ".New Revision Selection",
	newVersionSelection__lbl: ".New Version Selection",
	offeredVolume__lbl: ".Offered volume",
	offeredVolume__tlp: ".Offered volume",
	parentGroupBillCode__lbl: ".Parent",
	parentGroupBillCode__tlp: ".Account code",
	parentGroupBillId__lbl: ".Parent(ID)",
	parentGroupCode__lbl: ".Parent",
	parentGroupCode__tlp: ".Account code",
	parentGroupId__lbl: ".Parent(ID)",
	paymentComment__lbl: ".Payment comment",
	paymentComment__tlp: ".Payment comment",
	paymentRefDay__lbl: ".Reference to",
	paymentRefDay__tlp: ".Reference to",
	paymentTerms__lbl: ".Payment Terms",
	paymentTerms__tlp: ".Payment terms",
	percentCoveredAwarded__lbl: "",
	percentCoveredLabel__lbl: ".% of tender volume",
	percentCoveredOffered__lbl: "",
	percent__lbl: ".%",
	periodApprovalStatus__lbl: ".Period approval status",
	periodApprovalStatus__tlp: ".Period approval status",
	period__lbl: ".Period",
	period__tlp: ".Period",
	previewFile__lbl: ".Preview File",
	product__lbl: ".Product",
	product__tlp: ".Product",
	quantityType__lbl: ".Quantity type",
	quantityType__tlp: ".Quantity type",
	referenceTo__lbl: ".Reference To",
	requiredVolumeTitle__lbl: "",
	resBuyerCode__lbl: ".Responsible Buyer",
	resBuyerId__lbl: ".Responsible Buyer(ID)",
	resBuyerName__lbl: ".Account manager",
	resBuyerName__tlp: ".Account manager",
	resaleRefCode__lbl: ".Resale reference",
	resaleRefCode__tlp: ".Contract No",
	resaleRefId__lbl: ".Resale Ref(ID)",
	reviewFirstParam__lbl: ".Review Date(s)",
	reviewFirstParam__tlp: ".Review Date I",
	reviewNotification__lbl: ".Notify",
	reviewNotification__tlp: ".Notify",
	reviewPeriod__lbl: ".Review period",
	reviewPeriod__tlp: ".Review period",
	reviewSecondParam__lbl: ".Review Second Param",
	reviewSecondParam__tlp: ".Review Date II",
	riskHolderCode__lbl: ".Risk Holder",
	riskHolderCode__tlp: ".Account code",
	riskHolderId__lbl: ".Risk Holder(ID)",
	riskHolderType__lbl: ".Type",
	riskHolderType__tlp: ".Type",
	scope__lbl: ".Scope",
	scope__tlp: ".Scope",
	selectedAttachments__lbl: ".Selected Attachments",
	separator__lbl: ".<hr style='height:1px; border:none; color:#D0D0D0; background-color:#D0D0D0;'>",
	settCurrCode__lbl: ".Currency",
	settCurrCode__tlp: ".settlement currency",
	settCurrId__lbl: ".Settlement Curr(ID)",
	settUnitCode__lbl: ".Settlement unit",
	settUnitCode__tlp: ".settlement unit",
	settUnitId__lbl: ".Settlement Unit(ID)",
	settlementDecimals__lbl: ".Decimals",
	settlementDecimals__tlp: ".Decimals",
	space__lbl: ". ",
	status__lbl: ".Status",
	status__tlp: ".Status",
	subType__lbl: ".Delivery",
	subType__tlp: ".Delivery",
	submitForApprovalConcurencyCheckFail__lbl: ".Submit For Approval Concurency Check Fail",
	submitForApprovalInvalidBids__lbl: ".Submit For Approval Invalid Bids",
	submitForApprovalSuccesfullySubmitted__lbl: ".Submit For Approval Succesfully Submitted",
	subsidiaryCode__lbl: ".Subsidiary",
	tax__lbl: ".Tax type",
	tax__tlp: ".Tax type",
	tenderCode__lbl: ".Bid Tender Identification",
	tenderCode__tlp: ".Identifying code of tender",
	tenderHolderCode__lbl: ".Tender Issuer",
	tenderHolderCode__tlp: ".Account code",
	tenderHolderId__lbl: ".Holder(ID)",
	tenderHolderId__tlp: ".Identification of the tender holder",
	tenderLocationId__lbl: ".Tender Location Id",
	tenderName__lbl: ".Tender Name",
	tenderName__tlp: ".Name of tender",
	tenderSource__lbl: ".Source",
	tenderSource__tlp: ".Indicates if the tender was imported or captured manually",
	tenderStatus__lbl: ".Tender Status",
	tenderStatus__tlp: ".Current tender status",
	tenderVersion__lbl: ".Tender Version",
	tenderVersion__tlp: ".Version of the tender",
	type__lbl: ".Type",
	type__tlp: ".Type",
	unit__lbl: ".Unit",
	validFrom__lbl: ".Valid from",
	validFrom__tlp: ".Valid from",
	validTo__lbl: ".Valid to",
	validTo__tlp: ".Valid to",
	vat__lbl: ".VAT applicability",
	vat__tlp: ".VAT applicability",
	volumeShare__lbl: ".Share percentage",
	volumeShare__tlp: ".Share percentage",
	volumeTolerance__lbl: ".Tolerance",
	volumeTolerance__tlp: ".Tolerance"
});
