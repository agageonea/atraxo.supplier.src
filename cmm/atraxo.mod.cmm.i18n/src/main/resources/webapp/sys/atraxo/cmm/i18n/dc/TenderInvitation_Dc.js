
Ext.define("atraxo.cmm.i18n.dc.TenderInvitation_Dc$Edit", {
	biddingPeriodFromLabel__lbl: ".Bidding from",
	biddingPeriodFrom__lbl: ".Bidding from",
	biddingPeriodToLabel__lbl: ".Bidding to",
	biddingPeriodTo__lbl: ".Bidding to",
	commentLabel__lbl: ".Comments",
	comment__lbl: ".Comments",
	contactLabel__lbl: ".Contact",
	emailLabel__lbl: ".E-mail",
	email__lbl: ".E-mail",
	phoneLabel__lbl: ".Phone",
	phone__lbl: ".Phone",
	tenderHolderCodeLabel__lbl: ".Issuer",
	tenderTypeLabel__lbl: ".Type",
	tenderType__lbl: ".Type"
});

Ext.define("atraxo.cmm.i18n.dc.TenderInvitation_Dc$Header", {
	biddingPeriodFromLabel__lbl: ".Bidding from",
	biddingPeriodToLabel__lbl: ".Bidding to",
	biddingStatus__lbl: ".Bidding status",
	commentLabel__lbl: ".Comments",
	contactLabel__lbl: ".Contact",
	emailLabel__lbl: ".E-mail",
	noOfroundsToLabel__lbl: ".Number of rounds",
	phoneLabel__lbl: ".Phone",
	publishedOn__lbl: ".Published on",
	status__lbl: ".Status",
	tenderHolderCodeLabel__lbl: ".Issuer",
	tenderTypeLabel__lbl: ".Type",
	totalVolume__lbl: ".Total volume",
	version__lbl: ".Version"
});

Ext.define("atraxo.cmm.i18n.dc.TenderInvitation_Dc$List", {
	biddingPeriodFrom__lbl: ".Bidding period from",
	biddingPeriodTo__lbl: ".Bidding ends",
	biddingStatus__lbl: ".Bidding status",
	canceledOn__lbl: ".Canceled on",
	code__lbl: ".Code",
	comments__lbl: ".Comments",
	contact__lbl: ".Contact Person",
	email__lbl: ".Email",
	holderName__lbl: ".Issuer",
	msgId__lbl: ".Message id",
	name__lbl: ".Name",
	noIfRounds__lbl: ".Number of rounds",
	phone__lbl: ".Phone",
	publishedOn__lbl: ".Published on",
	schemaVersion__lbl: ".Schema version",
	source__lbl: ".Source",
	subsidiaryCode__lbl: ".Subsidiary",
	tenderStatus__lbl: ".Tender status",
	tenderVersion__lbl: ".Tender Version",
	totalVolume__lbl: ".Volume",
	transmissionStatus__lbl: ".Transmission status",
	type__lbl: ".Type",
	updatedOn__lbl: ".Updated on",
	volumeOffer__lbl: ".Volume offer",
	volumeUnit__lbl: ".Unit"
});

Ext.define("atraxo.cmm.i18n.dc.TenderInvitation_Dc$New", {
	agreementFromLabel__lbl: ".Agreement from",
	agreementToLabel__lbl: ".to",
	biddingPeriodFromLabel__lbl: ".Bidding from",
	biddingPeriodToLabel__lbl: ".to",
	commentsLabel__lbl: ".Comments",
	contactLabel__lbl: ".Contact person",
	emailLabel__lbl: ".Contact e-mail",
	phoneLabel__lbl: ".Contact phone",
	tenderHolderCodeLabel__lbl: ".Tender issuer",
	tenderNameLabel__lbl: ".Tender name",
	tenderStatusLabel__lbl: ".Tender status",
	tenderTypeLabel__lbl: ".Tender type",
	tenderVersionLabel__lbl: ".Version",
	typeLabel__lbl: ".Tender type"
});

Ext.define("atraxo.cmm.i18n.dc.TenderInvitation_Dc$wizardHeader", {
	phase1__lbl: ".General information",
	phase2__lbl: ".Locations",
	phase3__lbl: ".Agreement conditions",
	phase4__lbl: ".Airlines"
});
