
Ext.define("atraxo.cmm.i18n.dc.TenderBid_Dc$CopyBidForm", {
	anotherBidSelectionLabel__lbl: ".Create a new bid for another location tender by preserving the prices and payment terms from source bid",
	newVersionSelectionLabel__lbl: ".Copy as new bid version of the same location tender"
});

Ext.define("atraxo.cmm.i18n.dc.TenderBid_Dc$CreditTerms", {
	bankGuaranteesLabel__lbl: ".Bank guarantees",
	creditTermsLabel__lbl: ".Credit terms",
	paymentFreqLabel__lbl: ".Payment frequency",
	prepaidAmountLabel__lbl: ".Amount",
	prepaidDaysLabel__lbl: ".Prepaid days",
	prepayFirstDeliveryDateLabel__lbl: ".First delivery date: payment date plus "
});

Ext.define("atraxo.cmm.i18n.dc.TenderBid_Dc$Export", {
	exportTypeLabel__lbl: ".Select export type"
});

Ext.define("atraxo.cmm.i18n.dc.TenderBid_Dc$Filter", {
	code__lbl: ".Bid code",
	country__lbl: ".Country"
});

Ext.define("atraxo.cmm.i18n.dc.TenderBid_Dc$Header", {
	astmSpecLabel__lbl: ".ASTM specification",
	bidPackageIdentifierLabel__lbl: ".Package Identifier",
	bidRevisionLabel__lbl: ".Revision",
	bidValidLabel__lbl: ".Bid validity",
	bidVersionLabel__lbl: ".Version",
	contractValidLabel__lbl: ".Contract validity",
	flightServiceTypeLabel__lbl: ".Operational type",
	holderContactLabel__lbl: ".Issuer contact",
	holder__lbl: ".Holder",
	iataServiceLevelLabel__lbl: ".IATA service level",
	operatingHoursLabel__lbl: ".Operating hours",
	refuelerLabel__lbl: ".Refueler",
	requiredVolume__lbl: ".Required Volume",
	responsibleBuyerLabel__lbl: ".Bidder contact",
	taxLabel__lbl: ".Tax type",
	titleTransferLabel__lbl: ".Title transfer"
});

Ext.define("atraxo.cmm.i18n.dc.TenderBid_Dc$PaymentTerms", {
	avgMethodNameLabel__lbl: ".Averaging method",
	avgMethodName__lbl: ".Averaging method",
	exchangeRate__lbl: ".<span style='font-weight:bold !important'>Exchange rate for price conversions</span>",
	financialSourceCodeLabel__lbl: ".Financial source",
	financialSourceCode__lbl: ".Financial source",
	invoiceFreqLabel__lbl: ".Invoice frequency",
	invoiceTypeLabel__lbl: ".Invoice type",
	paymentRefDayLabel__lbl: ".Reference day",
	paymentTermsLabel__lbl: ".Payment terms",
	paymentTypeLabel__lbl: ".Payment method",
	periodLabel__lbl: ".Offset",
	settCurrLabel__lbl: ".Payment currency",
	settUnitLabel__lbl: ".Pricing unit",
	vatLabel__lbl: ".VAT applicability"
});

Ext.define("atraxo.cmm.i18n.dc.TenderBid_Dc$TabList", {
	airlinesList__lbl: ".Airlines",
	bidAcceptAwardTransmissionStatus__lbl: ".Accept Award Transmission status",
	bidApprovalStatus__lbl: ".Approval Status",
	bidCancelTransmissionStatus__lbl: ".Bid Cancel Transmission status",
	bidCode__lbl: ".Code",
	bidDeclineAwardTransmissionStatus__lbl: ".Decline Award Transmission status",
	bidLocBiddingStatus__lbl: ".Location Bidding Status",
	bidPackageIdentifier__lbl: ".Package Identifier",
	bidRevision__lbl: ".Bid revision",
	bidRound__lbl: ".Bid round",
	bidStatus__lbl: ".Bid status",
	bidTransmissionStatus__lbl: ".Transmission status",
	bidValidTo__lbl: ".Bid deadline",
	bidVersion__lbl: ".Bid version",
	contractType__lbl: ".Type",
	countryName__lbl: ".Country",
	deliveryPoint__lbl: ".Delivery",
	flightServiceType__lbl: ".Operational type",
	invoiceFreq__lbl: ".Invoice frequency",
	locCode__lbl: ".Location code",
	paymentTerms__lbl: ".Payment terms",
	settlementCurrency__lbl: ".Payment currency",
	tenderHolderCode__lbl: ".Tender issuer code",
	tenderName__lbl: ".Tender name",
	tenderStatus__lbl: ".Tender status"
});

Ext.define("atraxo.cmm.i18n.dc.TenderBid_Dc$Volumes", {
	awardedVolumeLabel__lbl: ".Awarded volume",
	offeredVolumeLabel__lbl: ".Offered volume ",
	periodLabel__lbl: ".Volume period",
	quantityTypeLabel__lbl: ".Volume measurement",
	toleranceLabel__lbl: ".Volume tolerance"
});
