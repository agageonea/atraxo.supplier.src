
Ext.define("atraxo.cmm.i18n.dc.ShipTo_Dc$EditList", {
	contractPeriod__lbl: ".Period",
	customerName__lbl: ".Customer name",
	settlementUnitCode__lbl: ".Unit",
	shipTo__lbl: ".Ship to"
});

Ext.define("atraxo.cmm.i18n.dc.ShipTo_Dc$EditListPurchase", {
	contractPeriod__lbl: ".Period",
	customerName__lbl: ".Customer name",
	settlementUnitCode__lbl: ".Unit",
	shipTo__lbl: ".Ship to"
});

Ext.define("atraxo.cmm.i18n.dc.ShipTo_Dc$Filter", {
	customerName__lbl: ".Customer name",
	customer__lbl: ".Ship to"
});
