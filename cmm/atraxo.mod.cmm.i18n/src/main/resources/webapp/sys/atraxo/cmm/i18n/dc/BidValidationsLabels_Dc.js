
Ext.define("atraxo.cmm.i18n.dc.BidValidationsLabels_Dc$BidValidationsLabel", {
	concurencyCheckFailedBidsLabel__lbl: ".%s bids out of existing <b>%s</b> have failed the concurency check, please reload.",
	succesfullySubmitedBidsLabel__lbl: ".Successfully submitted <b>%s</b> bids out of existing <b>%s</b> .",
	validationErrorsBidsLabel__lbl: ".<b>%s</b> bids have failed the check and cannot be submitted. <br/> Please check the reason of failure below. Correct the bid and try again to submit."
});
