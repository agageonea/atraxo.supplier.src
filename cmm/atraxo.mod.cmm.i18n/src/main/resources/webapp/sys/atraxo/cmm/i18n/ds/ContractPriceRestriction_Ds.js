Ext.define("atraxo.cmm.i18n.ds.ContractPriceRestriction_Ds", {
	contractPriceCtgry__lbl: ".Contract Price Category(ID)",
	isEnabled__lbl: ".Is Enabled",
	operator__lbl: ".Operator",
	operator__tlp: ".Operator",
	restrictionType__lbl: ".Restriction type",
	restrictionType__tlp: ".Restriction type",
	unitCode__lbl: ".Unit",
	unitCode__tlp: ".Code",
	unit__lbl: ".Unit(ID)",
	value__lbl: ".Value",
	value__tlp: ".Value"
});
