
Ext.define("atraxo.cmm.i18n.dc.ExpectedFeesAndTaxes_Dc$List", {
	expectFeeCode__lbl: ".IATA code",
	expectFeeName__lbl: ".IATA name",
	expectFeeType__lbl: ".Type"
});
