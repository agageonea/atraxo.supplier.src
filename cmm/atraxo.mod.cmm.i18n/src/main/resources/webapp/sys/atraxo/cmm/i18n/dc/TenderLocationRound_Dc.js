
Ext.define("atraxo.cmm.i18n.dc.TenderLocationRound_Dc$List", {
	active__lbl: ".Active Round",
	bidOpening__lbl: ".Opening date",
	biddingFrom__lbl: ".From",
	biddingTo__lbl: ".To",
	roundNo__lbl: ".Round#"
});
