
Ext.define("atraxo.cmm.i18n.dc.ContractPriceCategoryGrouping_Dc$List", {
	contractAmount__lbl: ".Contract amount",
	convertedPrice__lbl: ".Contract price",
	mainCategory__lbl: ".Main category",
	name__lbl: ".Name",
	priceCategory__lbl: ".Price category",
	price__lbl: ".Contract original price"
});
