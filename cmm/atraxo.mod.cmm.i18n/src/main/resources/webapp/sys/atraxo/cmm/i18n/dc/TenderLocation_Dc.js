
Ext.define("atraxo.cmm.i18n.dc.TenderLocation_Dc$AssignList", {
	locationCode__lbl: ".Location code",
	locationName__lbl: ".Name"
});

Ext.define("atraxo.cmm.i18n.dc.TenderLocation_Dc$CaptureBid", {
	moreInfo1__lbl: ".If selected will replace information comming from existing bids",
	moreInfo2__lbl: ".Enabled if per fuel receiver method was selected",
	useExistingBidsLabel__lbl: ".Prefil with data from existing bids",
	useFuelReceiverMasterAgrementLabel__lbl: ".Use the fuel receiver's master agreement",
	useTenderHolderMasterAgrementLabel__lbl: ".Use existing master agreements for payment terms"
});

Ext.define("atraxo.cmm.i18n.dc.TenderLocation_Dc$Edit", {
	agreementFrom__lbl: ".Contract From",
	agreementTo__lbl: ".Contract To",
	location__lbl: ".Location",
	period__lbl: ".Period",
	product__lbl: ".Product",
	taxType__lbl: ".Tax type",
	volumeUnit__lbl: ".Unit",
	volume__lbl: ".Volume/period"
});

Ext.define("atraxo.cmm.i18n.dc.TenderLocation_Dc$Filter", {
	agreementFrom__lbl: ".Contract From",
	agreementTo__lbl: ".Contract To",
	biddingStatus__lbl: ".Bidding status",
	country__lbl: ".Country",
	deliveryPoint__lbl: ".Delivery point",
	flightServiceType__lbl: ".Operational Type",
	isValid__lbl: ".Valid?",
	location__lbl: ".Location",
	period__lbl: ".Period",
	product__lbl: ".Product",
	taxType__lbl: ".Tax type",
	volumeUnit__lbl: ".Unit",
	volume__lbl: ".Volume/period"
});

Ext.define("atraxo.cmm.i18n.dc.TenderLocation_Dc$Header", {
	ASTMFuelSpecLabel__lbl: ".ASTM Fuel specification",
	adHocLoc__lbl: ".Ad-hoc location",
	bidOpeningDateLabel__lbl: ".Bidding opening date",
	bidPeriodLabel__lbl: ".Bidding period",
	bidRoundLabel__lbl: ".Bidding Round",
	biddingRound__lbl: ".Round",
	contractPeriodLabel__lbl: ".Contract period",
	contractToLabel__lbl: ".Contract to",
	deliveryPointLabel__lbl: ".Delivery point",
	flightServiceType__lbl: ".Operational type",
	fuelProduct__lbl: ".Product",
	iataServiceLevelLabel__lbl: ".IATA Service level",
	locationBiddingStatus__lbl: ".Bidding status",
	operatingHoursLabel__lbl: ".Operating hours",
	packageIdentifierLabel__lbl: ".Package Identifier",
	period__lbl: ".Period",
	productLabel__lbl: ".Product",
	refuelerLabel__lbl: ".Refueler",
	taxTypeLabel__lbl: ".Tax type",
	titleTransferLabel__lbl: ".Title transfer"
});

Ext.define("atraxo.cmm.i18n.dc.TenderLocation_Dc$List", {
	adHoc__lbl: ".Ad-hoc",
	agreementFrom__lbl: ".Contract From",
	agreementTo__lbl: ".Contract To",
	bidOpeningDate__lbl: ".Bid opening date",
	bidPeriodFrom__lbl: ".Bid round from",
	bidPeriodTo__lbl: ".Bid round to",
	biddingRound__lbl: ".Bidding round",
	biddingStatus__lbl: ".Bidding status",
	country__lbl: ".Country",
	deliveryPoint__lbl: ".Delivery point",
	flightServiceType__lbl: ".Operational Type",
	isValid__lbl: ".Valid?",
	location__lbl: ".Location",
	packageIdentifier__lbl: ".Package identifier",
	period__lbl: ".Period",
	product__lbl: ".Product",
	taxType__lbl: ".Tax type",
	transmissionStatus__lbl: ".Transmission status",
	volumeOffer__lbl: ".Volume offer",
	volumeUnit__lbl: ".Unit",
	volume__lbl: ".Volume/period"
});

Ext.define("atraxo.cmm.i18n.dc.TenderLocation_Dc$New", {
	adHocLabel__lbl: ".Ad-hoc",
	adHoc__lbl: ".Ad-hoc",
	agreementFromLabel__lbl: ".Contract from",
	agreementFrom__lbl: ".Contract From",
	agreementToLabel__lbl: ".Contract to",
	agreementTo__lbl: ".Contract To",
	locationBiddingStatusLabel__lbl: ".Bidding status",
	locationBiddingStatus__lbl: ".Bidding status",
	locationLabel__lbl: ".Location",
	periodLabel__lbl: ".Period",
	period__lbl: ".Period",
	productLabel__lbl: ".Product",
	product__lbl: ".Product",
	rateLabel__lbl: ".Rate",
	volumeLabel__lbl: ".Volume",
	volumeUnit__lbl: ".Unit",
	volume__lbl: ".Volume/period"
});

Ext.define("atraxo.cmm.i18n.dc.TenderLocation_Dc$PaymentConditions", {
	averagingMethodLabel__lbl: ".Averaging method",
	bankGuaranteeLabel__lbl: ".Bank guarantees",
	creditTermsLabel__lbl: ".Credit terms",
	days__lbl: ".days",
	finanacialSourceLabel__lbl: ".Financial source",
	formTitlePayment__lbl: ".<b>Exchange rate for price conversions</b>",
	invoiceFreqLabel__lbl: ".Invoice frequency",
	invoiceTypeLabel__lbl: ".Invoice type",
	offsetLabel__lbl: ".Offset",
	paymentAmountLabel__lbl: ".Amount",
	paymentCurrLabel__lbl: ".Payment currency",
	paymentDaysLabel__lbl: ".First delivery date, payment date plus",
	paymentFreqLabel__lbl: ".Payment frequency",
	paymentMethodLabel__lbl: ".Payment method",
	paymentTermsLabel__lbl: ".Payment terms",
	preapaidDaysLabel__lbl: ".Prepaid days",
	pricingUnitLabel__lbl: ".Pricing unit",
	referenceDayLabel__lbl: ".Reference day"
});

Ext.define("atraxo.cmm.i18n.dc.TenderLocation_Dc$ProductPricing", {
	averagingMethodLabel__lbl: ".Averaging method",
	convertFromLabel__lbl: ".from",
	convertToLabel__lbl: ".Convert to",
	densityLabel__lbl: ".Equivalent density",
	factorLabel__lbl: ".with",
	indexConv__lbl: ".<b>Index conversion</b>",
	indexConversion__lbl: ".<b>Index conversion</b>",
	indexProviderLabel__lbl: ".Index provider",
	marketCurrencyLabel__lbl: ".Currency",
	marketUnitLabel__lbl: ".Unit",
	offsetLabel__lbl: ".Offset",
	operatorLabel__lbl: ".by",
	pricingBaseLabel__lbl: ".Pricing base",
	rateFromLabel__lbl: ".Rate",
	sourceNameLabel__lbl: ".Description",
	sourceTypeLabel__lbl: ".Source",
	timeSerieCodeLabel__lbl: ".Index code",
	timeSerieDescLabel__lbl: ".Description",
	timeSerieLabel__lbl: ".Index"
});

Ext.define("atraxo.cmm.i18n.dc.TenderLocation_Dc$SelectList", {
	biddingstatus__lbl: ".Bidding status",
	location__lbl: ".Location"
});

Ext.define("atraxo.cmm.i18n.dc.TenderLocation_Dc$biddingPeriodForm", {
	bidPeriodFrom__lbl: ".Bidding from",
	bidPeriodTo__lbl: ".Bidding ends"
});

Ext.define("atraxo.cmm.i18n.dc.TenderLocation_Dc$contractPeriodForm", {
	agreementFrom__lbl: ".Contract from",
	agreementTo__lbl: ".Contract to"
});
