
Ext.define("atraxo.cmm.i18n.dc.OpenTender_Dc$SelectList", {
	biddingStatus__lbl: ".Bidding status",
	code__lbl: ".Tender code",
	issuer__lbl: ".Issuer",
	name__lbl: ".Tender name",
	tenderStatus__lbl: ".Tender status"
});
