Ext.define("atraxo.cmm.i18n.ds.TenderLocationRound_Ds", {
	bidOpening__lbl: ".Bid Opening",
	bidRoundActive__lbl: ".Is bid active",
	biddingFrom__lbl: ".Binding period from",
	biddingTo__lbl: ".Bidding period to",
	roundNo__lbl: ".Round No",
	tenderLocationId__lbl: ".Tender Location(ID)"
});
