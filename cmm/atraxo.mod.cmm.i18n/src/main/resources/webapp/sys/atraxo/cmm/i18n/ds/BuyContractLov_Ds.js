Ext.define("atraxo.cmm.i18n.ds.BuyContractLov_Ds", {
	code__lbl: ".Contract No",
	code__tlp: ".Contract No",
	dealType__lbl: ".Deal type",
	dealType__tlp: ".Deal type",
	id__lbl: ".Id",
	scope__lbl: ".Scope",
	scope__tlp: ".Scope",
	status__lbl: ".Status",
	status__tlp: ".Status",
	subType__lbl: ".Delivery",
	subType__tlp: ".Delivery",
	type__lbl: ".Type",
	type__tlp: ".Type"
});
