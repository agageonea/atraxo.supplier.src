
Ext.define("atraxo.cmm.i18n.dc.ContractPriceCategory_Dc$BidList", {
	avgMethodIndicatorName__lbl: ".Averaging method",
	financialSourceName__lbl: ".Financial source",
	iataPriceCategoryCode__lbl: ".IATA Code",
	iataPriceCategoryUse__lbl: ".Type",
	mainCategory__lbl: ".Main category",
	name__lbl: ".Name",
	priceCategory__lbl: ".Price category",
	price__lbl: ".Price",
	restrictionsParameter__lbl: ".Restrictions"
});

Ext.define("atraxo.cmm.i18n.dc.ContractPriceCategory_Dc$DetailsList", {
	iataPriceCategoryCode__lbl: ".IATA Code",
	iataPriceCategoryUse__lbl: ".Type",
	name__lbl: ".Name",
	priceCategory__lbl: ".Price category",
	price__lbl: ".Price",
	restrictionsParameter__lbl: ".Restrictions"
});

Ext.define("atraxo.cmm.i18n.dc.ContractPriceCategory_Dc$EditGrid", {
	assignPriceComponentsLabel__lbl: "....",
	assignPriceComponents__lbl: "....",
	avgMethodIndicatorName__lbl: ".Averaging method",
	avgMethodNameLabel__lbl: ".Averaging method",
	btnToggle__lbl: "....",
	commentsLabel__lbl: ".Comments",
	comments__lbl: ".Comments",
	continousLabel__lbl: ".Continuous?",
	continous__lbl: ".Continuous?",
	defaultLabel__lbl: ".Include in total price?",
	default__lbl: ".Include in total price?",
	exchangeRateOffset__lbl: ".Period",
	financialSourceCodeLabel__lbl: ".Financial source",
	financialSourceCode__lbl: ".Financial source",
	includeAveragePriceLabel__lbl: ".Include in average price?",
	nameLabel__lbl: ".Name*",
	percentageOfLabel__lbl: ".Percentage of",
	periodLabel__lbl: ".Period",
	popoverLabel__lbl: ".Using ForEX of*",
	priceCategoryLabel__lbl: ".Price category*",
	quantityTypeLabel__lbl: ".Quantity type",
	restrictionLabel__lbl: ".Apply restrictions",
	showAssignmentLabel__lbl: "....",
	showAssignment__lbl: "....",
	titleLabel__lbl: ".<span style='font-weight:bold'>Convert into settlement currency using</span>",
	vatLabel__lbl: ".VAT applicability*"
});

Ext.define("atraxo.cmm.i18n.dc.ContractPriceCategory_Dc$Filter", {
	avgMethodIndicatorName__lbl: ".Averaging method",
	financialSourceName__lbl: ".Financial source"
});

Ext.define("atraxo.cmm.i18n.dc.ContractPriceCategory_Dc$FormUpdatePrice", {
	customer__lbl: ".Customer"
});

Ext.define("atraxo.cmm.i18n.dc.ContractPriceCategory_Dc$List", {
	avgMethodIndicatorName__lbl: ".Averaging method",
	financialSourceName__lbl: ".Financial source",
	iataPriceCategoryCode__lbl: ".IATA Code",
	iataPriceCategoryUse__lbl: ".Type",
	mainCategory__lbl: ".Main category",
	name__lbl: ".Name",
	priceCategory__lbl: ".Price category",
	price__lbl: ".Price",
	restrictionsParameter__lbl: ".Restrictions"
});

Ext.define("atraxo.cmm.i18n.dc.ContractPriceCategory_Dc$NewGrid", {
	avgMethodName__lbl: ".Averaging method",
	comments__lbl: ".Comments",
	continous__lbl: ".Continuous?",
	financialSourceCode__lbl: ".Financial source",
	period__lbl: ".Period",
	quantityType__lbl: ".Quantity type",
	showAssignment__lbl: "....",
	vat__lbl: ".VAT"
});

Ext.define("atraxo.cmm.i18n.dc.ContractPriceCategory_Dc$Totals", {
	total__lbl: ".Total converted price:"
});

Ext.define("atraxo.cmm.i18n.dc.ContractPriceCategory_Dc$financialSourceAvgMethodPeriod", {
	avgMethodIndicatorName__lbl: ".Averaging method",
	exchangeRateOffset__lbl: ".Period",
	financialSourceCode__lbl: ".Financial source"
});
