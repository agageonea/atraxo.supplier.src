
Ext.define("atraxo.cmm.i18n.dc.PriceUpdateCategoriesAdd_Dc$AddList", {
	contract__lbl: ".Contract",
	counterparty__lbl: ".Counterparty",
	currency__lbl: ".Currency",
	holder__lbl: ".Holder",
	location__lbl: ".Location",
	price__lbl: ".Current price",
	selected__lbl: ". ",
	unit__lbl: ".Unit"
});
