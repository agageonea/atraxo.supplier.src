
Ext.define("atraxo.cmm.i18n.dc.ShipToReadOnly_Dc$EditList", {
	contractPeriod__lbl: ".Period",
	customerName__lbl: ".Customer name",
	settlementUnitCode__lbl: ".Unit",
	shipTo__lbl: ".Ship to"
});
