
Ext.define("atraxo.cmm.i18n.dc.BidAirlineVols_Dc$EditList", {
	awardedVolume__lbl: ".Awarded volume",
	customerName__lbl: ".Customer name",
	customer__lbl: ".Airline",
	offeredVolume__lbl: ".Offered volume",
	percentCovered__lbl: ".Percentage covered",
	settlementUnitCode__lbl: ".Unit",
	tenderBidVolume__lbl: ".Required volume/period"
});

Ext.define("atraxo.cmm.i18n.dc.BidAirlineVols_Dc$Filter", {
	actualVolume__lbl: ".Offered volume",
	allocatedVolume__lbl: ".Required volume/period",
	customerName__lbl: ".Customer name",
	customer__lbl: ".Airline"
});
