Ext.define("atraxo.cmm.i18n.ds.ContractTemplate_Ds", {
	contractType__lbl: ".Contract Type",
	contractType__tlp: ".Contract Type",
	deliverySubtype__lbl: ".Delivery/Subtype",
	deliverySubtype__tlp: ".Delivery/Subtype",
	dialog__lbl: ".Dialog",
	dialog__tlp: ".Dialog",
	scope__lbl: ".Scope",
	scope__tlp: ".Scope"
});
