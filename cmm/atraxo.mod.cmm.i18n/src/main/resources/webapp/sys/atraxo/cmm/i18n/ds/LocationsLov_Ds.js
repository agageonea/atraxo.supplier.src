Ext.define("atraxo.cmm.i18n.ds.LocationsLov_Ds", {
	areaID__lbl: ".Area ID",
	areaName__lbl: ".Areas(Name)",
	areaName__tlp: ".Name",
	code__lbl: ".Code",
	code__tlp: ".Code",
	name__lbl: ".Name",
	name__tlp: ".Name",
	type__lbl: ".Type",
	type__tlp: ".Type"
});
