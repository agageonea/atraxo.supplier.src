
Ext.define("atraxo.cmm.i18n.dc.ContractPriceComponentClassic_Dc$Edit", {
	price__lbl: ".New price value"
});

Ext.define("atraxo.cmm.i18n.dc.ContractPriceComponentClassic_Dc$SimpleList", {
	currency__lbl: ".Currency",
	unit__lbl: ".Unit",
	validFrom__lbl: ".Valid from",
	validTo__lbl: ".Valid to",
	value__lbl: ".Value"
});
