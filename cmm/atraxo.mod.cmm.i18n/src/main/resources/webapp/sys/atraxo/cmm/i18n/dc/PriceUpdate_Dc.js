
Ext.define("atraxo.cmm.i18n.dc.PriceUpdate_Dc$ApprovalNote", {
	remarks__lbl: ".Add your note to be sent with the request to approvers"
});

Ext.define("atraxo.cmm.i18n.dc.PriceUpdate_Dc$priceUpdateFirstStep", {
	locationCodeLabel__lbl: ".Indicate location where price should be changed",
	priceCategoryLabel__lbl: ".Indicate price category whose price should be changed"
});

Ext.define("atraxo.cmm.i18n.dc.PriceUpdate_Dc$priceUpdateFourthStep", {
	enterPriceLabel__lbl: ".Enter the new price for [price_category]",
	enterReasonLabel__lbl: ".Enter reason for changing the price",
	enterStartingDateLabel__lbl: ".Enter starting date for the new price"
});

Ext.define("atraxo.cmm.i18n.dc.PriceUpdate_Dc$priceUpdateSecondStep", {
	billToLabel__lbl: ".Bill to",
	contractForLabel__lbl: ".Contract for",
	contractForValues__lbl: ".Contract type",
	contractHolderLabel__lbl: ".Contract holder",
	contractStatusLabel__lbl: ".Contract status",
	contractStatusValues__lbl: ".Contract status",
	contractTypeLabel__lbl: ".Contract type",
	contractTypeValues__lbl: ".Contract for",
	counterPartyLabel__lbl: ".Counterparty",
	deliveryPointLabel__lbl: ".Delivery point",
	deliveryPointValues__lbl: ".Delivery point",
	effectiveLabel__lbl: ".Effective",
	expiredLabel__lbl: ".Expired",
	indicateLabel__lbl: ".Indicate criteria for contracts selection",
	intoPlaneLabel__lbl: ".Into plane",
	intoStorageLabel__lbl: ".Into storage",
	productLabel__lbl: ".Product",
	purchaseLabel__lbl: ".Purchase",
	salesLabel__lbl: ".Sales",
	serviceLabel__lbl: ".Service",
	shipToLabel__lbl: ".Ship-to"
});

Ext.define("atraxo.cmm.i18n.dc.PriceUpdate_Dc$priceUpdateSteps", {
	phase1__lbl: ".Select price category",
	phase2__lbl: ".Set conditions",
	phase3__lbl: ".Select contracts",
	phase4__lbl: ".Set new price"
});
