
Ext.define("atraxo.cmm.i18n.dc.ContractCustomerWizard_Dc$NewContractStep1", {
	customerName__lbl: ".Customer"
});

Ext.define("atraxo.cmm.i18n.dc.ContractCustomerWizard_Dc$NewContractStep2", {
	contractedVolumeLabel__lbl: ".Contracted volume",
	eventType__lbl: ".Event type",
	periodLabel__lbl: ".Contract period",
	periodSeparator__lbl: "./",
	period__lbl: ".Contract period",
	volumePeriodSeparator__lbl: "./"
});

Ext.define("atraxo.cmm.i18n.dc.ContractCustomerWizard_Dc$NewContractStep3", {
	bankGuaranteeLabel__lbl: ".Bank guarantees",
	btnTogglePaymentTerms__lbl: "....",
	creditTermsLabel__lbl: ".Credit terms",
	daysLabel__lbl: ".days",
	firstDeliveryLabel__lbl: ".First delivery date: payment date plus",
	paymentTermsLabel__lbl: ".Payment terms"
});

Ext.define("atraxo.cmm.i18n.dc.ContractCustomerWizard_Dc$NewContractStep4", {
	averageMethodName__lbl: ".Averaging method",
	btnToggle__lbl: "....",
	financialSourceAvgMethodPeriod__lbl: ".Using ForEX of*",
	financialSourceCode__lbl: ".Financial source",
	invoiceType__lbl: ".Invoice type",
	settlementCurrCode__lbl: ".Invoice currency",
	useExchangeRatesLabel__lbl: ".Use exchange rates provided by",
	vat__lbl: ".VAT applicability"
});

Ext.define("atraxo.cmm.i18n.dc.ContractCustomerWizard_Dc$NewContractStep5", {
	benchmarkPeriodLabel__lbl: ". period",
	btnToggleBenchmark__lbl: "....",
	btnToggleConversionFactor__lbl: "....",
	commaLabel__lbl: "., ",
	conversionFactorLabel__lbl: ".Conversion factor*",
	conversionFromLabel__lbl: ".Convert from",
	conversionToLabel__lbl: ".to",
	daysLabel__lbl: ".days",
	decimalPrecisionLabel__lbl: ".decimals precision",
	differentialLabel__lbl: ".Differential",
	initialPrice__lbl: ".Price value",
	multiplyLabel__lbl: ".by multiplying quotation with",
	priceCategoryName__lbl: ".Pricing base",
	priceName__lbl: ".Price name",
	priceValueLabel__lbl: ".Price value",
	quotAvgMethodName__lbl: ".Averaging method",
	quotationLabel__lbl: ".Benchmark",
	secondCommaLabel__lbl: "., ",
	usingLabel__lbl: ".using"
});

Ext.define("atraxo.cmm.i18n.dc.ContractCustomerWizard_Dc$benchmark", {
	averagingMethodLabel__lbl: ".Averaging method",
	benchmarProviderkLabel__lbl: ".Benchmark provider",
	benchmarkLabel__lbl: ".Benchmark",
	exchangeRateOffsetLabel__lbl: ".Pricing period",
	priceValueTypeLabel__lbl: ".Price value type",
	quotAvgMethodName__lbl: ".Averaging method",
	quotFinancialSourceCode__lbl: ".Financial source",
	warning__lbl: ".There is no benchmark in the system to meet the above condition"
});

Ext.define("atraxo.cmm.i18n.dc.ContractCustomerWizard_Dc$conversionFactor", {
	benchmarkLabel__lbl: ".From benchmark unit",
	conversionFactorLabel__lbl: ".Conversion factor",
	conversionOperatorLabel__lbl: ".Conversion operator",
	conversionPreciosionLabel__lbl: ".Conversion precision",
	decimalsLabel__lbl: ".decimals"
});

Ext.define("atraxo.cmm.i18n.dc.ContractCustomerWizard_Dc$financialSourceAvgMethodPeriod", {
	averageMethodName__lbl: ".Averaging method",
	financialSourceCode__lbl: ".Financial source"
});

Ext.define("atraxo.cmm.i18n.dc.ContractCustomerWizard_Dc$paymentTermsRefDay", {
	daysLabel__lbl: ".days",
	paymentTermsLabel__lbl: ".Payment terms*"
});

Ext.define("atraxo.cmm.i18n.dc.ContractCustomerWizard_Dc$wizardHeader", {
	phase1__lbl: ".Contract parties",
	phase2__lbl: ".General terms",
	phase3__lbl: ".Payment terms",
	phase4__lbl: ".Invoicing",
	phase5__lbl: ".Pricing"
});
