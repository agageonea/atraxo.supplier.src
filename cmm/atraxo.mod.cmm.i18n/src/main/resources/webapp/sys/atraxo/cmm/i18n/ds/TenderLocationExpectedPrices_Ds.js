Ext.define("atraxo.cmm.i18n.ds.TenderLocationExpectedPrices_Ds", {
	iataPriceCategoryCode__lbl: ".Iata Price Category",
	iataPriceCategoryCode__tlp: ".IATA code",
	iataPriceCategoryId__lbl: ".Iata Price Category(ID)",
	iataPriceCategoryName__lbl: ".Iata Price Category(Name)",
	iataPriceCategoryName__tlp: ".Name",
	iataPriceCategoryType__lbl: ".Use",
	iataPriceCategoryType__tlp: ".Use",
	tenderLocationId__lbl: ".Tender Location(ID)"
});
