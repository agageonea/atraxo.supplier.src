/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.contracts.model;

import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.QuantityType;
import atraxo.cmm.domain.impl.cmm_type.VatApplicability;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.fmbas_type.ValueType;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = PricingBase.class, sort = {@SortField(field = PricingBase_Ds.F_VALIDFROM, desc = true)})
@RefLookups({
		@RefLookup(refId = PricingBase_Ds.F_CONTRACTID, namedQuery = Contract.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PricingBase_Ds.F_CONTRACTCODE)}),
		@RefLookup(refId = PricingBase_Ds.F_QUOTID),
		@RefLookup(refId = PricingBase_Ds.F_CONVUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PricingBase_Ds.F_CONVUNITCODE)}),
		@RefLookup(refId = PricingBase_Ds.F_AVGMTID),
		@RefLookup(refId = PricingBase_Ds.F_PRICECATID, namedQuery = PriceCategory.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = PricingBase_Ds.F_PRICECATNAME)})})
public class PricingBase_Ds extends AbstractDs_Ds<PricingBase> {

	public static final String ALIAS = "cmm_PricingBase_Ds";

	public static final String F_CONVFACTOR = "convFactor";
	public static final String F_USED = "used";
	public static final String F_DENSITY = "density";
	public static final String F_DENSITY1 = "density1";
	public static final String F_DENSITY2 = "density2";
	public static final String F_CONTRACTID = "contractId";
	public static final String F_CONTRACTCODE = "contractCode";
	public static final String F_QUOTTIMESERIESID = "quotTimeSeriesId";
	public static final String F_QUOTTIMESERIESDESCRIPTION = "quotTimeSeriesDescription";
	public static final String F_CONTRACTSTATUS = "contractStatus";
	public static final String F_CONTRACTVALIDFROM = "contractValidFrom";
	public static final String F_CONTRACTVALIDTO = "contractValidTo";
	public static final String F_QUOTID = "quotId";
	public static final String F_QUOTNAME = "quotName";
	public static final String F_CALCVAL = "calcVal";
	public static final String F_QUOTVALUE = "quotValue";
	public static final String F_QUOTUNITID = "quotUnitId";
	public static final String F_QUOTUNITCODE = "quotUnitCode";
	public static final String F_QUOTUNITTYPEIND = "quotUnitTypeInd";
	public static final String F_CONVUNITID = "convUnitId";
	public static final String F_CONVUNITCODE = "convUnitCode";
	public static final String F_CONVUNITTYPEIND = "convUnitTypeInd";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_QUOTATIONOFFSET = "quotationOffset";
	public static final String F_OPERATOR = "operator";
	public static final String F_FACTOR = "factor";
	public static final String F_DECIMALS = "decimals";
	public static final String F_DESCRIPTION = "description";
	public static final String F_INITIALPRICE = "initialPrice";
	public static final String F_PCCURR = "pcCurr";
	public static final String F_PCUNIT = "pcUnit";
	public static final String F_QUANTITYTYPE = "quantityType";
	public static final String F_CONTINOUS = "continous";
	public static final String F_DEFAULTPRICECTGY = "defaultPriceCtgy";
	public static final String F_RESTRICTION = "restriction";
	public static final String F_EXCHANGERATEOFFSET = "exchangeRateOffset";
	public static final String F_INITIALFINANCIALSOURCEID = "initialFinancialSourceId";
	public static final String F_INITIALAVERAGEMETHODID = "initialAverageMethodId";
	public static final String F_PRICECATEGORYIDSJSON = "priceCategoryIdsJson";
	public static final String F_PRICEVALUETYPE = "priceValueType";
	public static final String F_PRICECATID = "priceCatId";
	public static final String F_PRICECATNAME = "priceCatName";
	public static final String F_PRICECATTYPE = "priceCatType";
	public static final String F_PRICECATPRICEPER = "priceCatPricePer";
	public static final String F_QUOTAVGMETHODID = "quotAvgMethodId";
	public static final String F_QUOTAVGMETHODCODE = "quotAvgMethodCode";
	public static final String F_QUOTAVGMETHODNAME = "quotAvgMethodName";
	public static final String F_AVGMTID = "avgMTId";
	public static final String F_AVGMTCODE = "avgMTCode";
	public static final String F_AVGMTNAME = "avgMTName";
	public static final String F_CALCULATEDDENSITY = "calculatedDensity";
	public static final String F_NAME = "name";
	public static final String F_INCLUDEINAVERAGE = "includeInAverage";
	public static final String F_VAT = "vat";
	public static final String F_COMMENTS = "comments";
	public static final String F_FOREX = "forex";
	public static final String F_SUPPID = "suppId";
	public static final String F_SUPPCODE = "suppCode";
	public static final String F_LOCID = "locId";
	public static final String F_LOCCODE = "locCode";
	public static final String F_CURRENCYID = "currencyId";
	public static final String F_CURRENCYCODE = "currencyCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_MAINCATEGORYCODE = "mainCategoryCode";
	public static final String F_FINANCIALSOURCEID = "financialSourceId";
	public static final String F_FINANCIALSOURCENAME = "financialSourceName";
	public static final String F_FINANCIALSOURCECODE = "financialSourceCode";
	public static final String F_AVGMETHODINDICATORDEFMETH = "avgMethodIndicatorDefMeth";
	public static final String F_AVGMETHODINDICATORNAME = "avgMethodIndicatorName";
	public static final String F_AVGMTHDID = "avgMthdId";
	public static final String F_PCOMPPROVISIONAL = "pcompProvisional";
	public static final String F_PCOMPWITHHOLD = "pcompWithHold";
	public static final String F_PCCURRCODE = "pcCurrCode";
	public static final String F_PCUNITCODE = "pcUnitCode";
	public static final String F_QUOTFINANCIALSOURCEID = "quotFinancialSourceId";
	public static final String F_QUOTFINANCIALSOURCECODE = "quotFinancialSourceCode";
	public static final String F_VALUE = "value";
	public static final String F_FORMTITLE = "formTitle";
	public static final String F_PRICE = "price";
	public static final String F_CONVERTTITLE = "convertTitle";
	public static final String F_CONVERTEDPRICE = "convertedPrice";
	public static final String F_USED1 = "used1";
	public static final String F_INITIALPRICETITLE = "initialPriceTitle";
	public static final String F_SLASH = "slash";
	public static final String F_PERCENTAGEOF = "percentageOf";
	public static final String F_LABELFIELD = "labelField";
	public static final String F_DUMMYFIELD = "dummyField";
	public static final String F_PRICECOMPONENTS = "priceComponents";
	public static final String F_BENCHMARKPROVIDER = "benchmarkProvider";
	public static final String F_CONVERSIONFACTORVALIDATIONMSG = "conversionFactorValidationMsg";

	@DsField(fetch = false)
	private String convFactor;

	@DsField(fetch = false)
	private String used;

	@DsField(fetch = false)
	private BigDecimal density;

	@DsField(fetch = false)
	private String density1;

	@DsField(fetch = false)
	private String density2;

	@DsField(join = "left", path = "contract.id")
	private Integer contractId;

	@DsField(join = "left", path = "contract.code")
	private String contractCode;

	@DsField(join = "left", path = "quotation.timeseries.id")
	private Integer quotTimeSeriesId;

	@DsField(join = "left", path = "quotation.timeseries.description")
	private String quotTimeSeriesDescription;

	@DsField(join = "left", path = "contract.status")
	private ContractStatus contractStatus;

	@DsField(join = "left", path = "contract.validFrom")
	private Date contractValidFrom;

	@DsField(join = "left", path = "contract.validTo")
	private Date contractValidTo;

	@DsField(join = "left", path = "quotation.id")
	private Integer quotId;

	@DsField(join = "left", path = "quotation.name")
	private String quotName;

	@DsField(join = "left", fetch = false, path = "quotation.calcVal")
	private String calcVal;

	@DsField(join = "left", fetch = false, path = "quotation.calcVal")
	private String quotValue;

	@DsField(join = "left", path = "quotation.unit.id")
	private Integer quotUnitId;

	@DsField(join = "left", path = "quotation.unit.code")
	private String quotUnitCode;

	@DsField(join = "left", path = "quotation.unit.unittypeInd")
	private UnitType quotUnitTypeInd;

	@DsField(join = "left", path = "convUnit.id")
	private Integer convUnitId;

	@DsField(join = "left", path = "convUnit.code")
	private String convUnitCode;

	@DsField(join = "left", path = "convUnit.unittypeInd")
	private UnitType convUnitTypeInd;

	@DsField(jpqlFilter = " e.validTo >= :validFrom ")
	private Date validFrom;

	@DsField(jpqlFilter = " e.validFrom <= :validTo ")
	private Date validTo;

	@DsField
	private MasterAgreementsPeriod quotationOffset;

	@DsField
	private Operator operator;

	@DsField
	private BigDecimal factor;

	@DsField
	private Integer decimals;

	@DsField
	private String description;

	@DsField(fetch = false)
	private BigDecimal initialPrice;

	@DsField(fetch = false, path = "initialCurrId")
	private Integer pcCurr;

	@DsField(fetch = false, path = "initialUnitId")
	private Integer pcUnit;

	@DsField(fetch = false)
	private QuantityType quantityType;

	@DsField(fetch = false)
	private Boolean continous;

	@DsField(fetch = false, path = "defauftPC")
	private Boolean defaultPriceCtgy;

	@DsField(fetch = false)
	private Boolean restriction;

	@DsField(fetch = false)
	private MasterAgreementsPeriod exchangeRateOffset;

	@DsField(fetch = false, path = "financialSourceId")
	private Integer initialFinancialSourceId;

	@DsField(fetch = false, path = "averageMethodId")
	private Integer initialAverageMethodId;

	@DsField(fetch = false)
	private String priceCategoryIdsJson;

	@DsField(join = "left", path = "quotation.valueType")
	private ValueType priceValueType;

	@DsField(join = "left", path = "priceCat.id")
	private Integer priceCatId;

	@DsField(join = "left", path = "priceCat.name")
	private String priceCatName;

	@DsField(join = "left", path = "priceCat.type")
	private PriceType priceCatType;

	@DsField(join = "left", path = "priceCat.pricePer")
	private PriceInd priceCatPricePer;

	@DsField(join = "left", path = "quotation.avgMethodIndicator.id")
	private Integer quotAvgMethodId;

	@DsField(join = "left", path = "quotation.avgMethodIndicator.code")
	private String quotAvgMethodCode;

	@DsField(join = "left", path = "quotation.avgMethodIndicator.name")
	private String quotAvgMethodName;

	@DsField(join = "left", path = "avgMethod.id")
	private Integer avgMTId;

	@DsField(join = "left", path = "avgMethod.code")
	private String avgMTCode;

	@DsField(join = "left", path = "avgMethod.name")
	private String avgMTName;

	@DsField(fetch = false)
	private BigDecimal calculatedDensity;

	@DsField(join = "left", path = "contractPriceCategories.name")
	private String name;

	@DsField(join = "left", path = "contractPriceCategories.includeInAverage")
	private Boolean includeInAverage;

	@DsField(fetch = false)
	private VatApplicability vat;

	@DsField(join = "left", path = "contractPriceCategories.comments")
	private String comments;

	@DsField(join = "left", fetch = false, path = "contractPriceCategories.forex")
	private String forex;

	@DsField(join = "left", path = "contract.supplier.id")
	private Integer suppId;

	@DsField(join = "left", path = "contract.supplier.code")
	private String suppCode;

	@DsField(join = "left", path = "contract.location.id")
	private Integer locId;

	@DsField(join = "left", path = "contract.location.code")
	private String locCode;

	@DsField(join = "left", path = "contract.settlementCurr.id")
	private Integer currencyId;

	@DsField(join = "left", path = "contract.settlementCurr.code")
	private String currencyCode;

	@DsField(join = "left", path = "contract.settlementUnit.id")
	private Integer unitId;

	@DsField(join = "left", path = "contract.settlementUnit.code")
	private String unitCode;

	@DsField(join = "left", path = "contractPriceCategories.priceCategory.mainCategory.code")
	private String mainCategoryCode;

	@DsField(join = "left", path = "contractPriceCategories.financialSource.id")
	private Integer financialSourceId;

	@DsField(join = "left", path = "contractPriceCategories.financialSource.name")
	private String financialSourceName;

	@DsField(join = "left", path = "contractPriceCategories.financialSource.code")
	private String financialSourceCode;

	@DsField(join = "left", path = "contractPriceCategories.averageMethod.defaultMethod")
	private Boolean avgMethodIndicatorDefMeth;

	@DsField(join = "left", path = "contractPriceCategories.averageMethod.name")
	private String avgMethodIndicatorName;

	@DsField(join = "left", path = "contractPriceCategories.averageMethod.id")
	private Integer avgMthdId;

	@DsField(join = "left", path = "contractPriceCategories.priceComponents.provisional")
	private Boolean pcompProvisional;

	@DsField(join = "left", path = "contractPriceCategories.priceComponents.withHold")
	private Boolean pcompWithHold;

	@DsField(join = "left", path = "contractPriceCategories.priceComponents.currency.code")
	private String pcCurrCode;

	@DsField(join = "left", path = "contractPriceCategories.priceComponents.unit.code")
	private String pcUnitCode;

	@DsField(join = "left", path = "quotation.timeseries.financialSource.id")
	private Integer quotFinancialSourceId;

	@DsField(join = "left", path = "quotation.timeseries.financialSource.code")
	private String quotFinancialSourceCode;

	@DsField(fetch = false)
	private BigDecimal value;

	@DsField(fetch = false)
	private String formTitle;

	@DsField(fetch = false)
	private BigDecimal price;

	@DsField(fetch = false)
	private String convertTitle;

	@DsField(fetch = false)
	private BigDecimal convertedPrice;

	@DsField(fetch = false)
	private Boolean used1;

	@DsField(fetch = false)
	private String initialPriceTitle;

	@DsField(fetch = false)
	private String slash;

	@DsField(fetch = false)
	private String percentageOf;

	@DsField(fetch = false)
	private String labelField;

	@DsField(fetch = false)
	private String dummyField;

	@DsField(fetch = false)
	private String priceComponents;

	@DsField(fetch = false)
	private String benchmarkProvider;

	@DsField(fetch = false)
	private String conversionFactorValidationMsg;

	/**
	 * Default constructor
	 */
	public PricingBase_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public PricingBase_Ds(PricingBase e) {
		super(e);
	}

	public String getConvFactor() {
		return this.convFactor;
	}

	public void setConvFactor(String convFactor) {
		this.convFactor = convFactor;
	}

	public String getUsed() {
		return this.used;
	}

	public void setUsed(String used) {
		this.used = used;
	}

	public BigDecimal getDensity() {
		return this.density;
	}

	public void setDensity(BigDecimal density) {
		this.density = density;
	}

	public String getDensity1() {
		return this.density1;
	}

	public void setDensity1(String density1) {
		this.density1 = density1;
	}

	public String getDensity2() {
		return this.density2;
	}

	public void setDensity2(String density2) {
		this.density2 = density2;
	}

	public Integer getContractId() {
		return this.contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public String getContractCode() {
		return this.contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public Integer getQuotTimeSeriesId() {
		return this.quotTimeSeriesId;
	}

	public void setQuotTimeSeriesId(Integer quotTimeSeriesId) {
		this.quotTimeSeriesId = quotTimeSeriesId;
	}

	public String getQuotTimeSeriesDescription() {
		return this.quotTimeSeriesDescription;
	}

	public void setQuotTimeSeriesDescription(String quotTimeSeriesDescription) {
		this.quotTimeSeriesDescription = quotTimeSeriesDescription;
	}

	public ContractStatus getContractStatus() {
		return this.contractStatus;
	}

	public void setContractStatus(ContractStatus contractStatus) {
		this.contractStatus = contractStatus;
	}

	public Date getContractValidFrom() {
		return this.contractValidFrom;
	}

	public void setContractValidFrom(Date contractValidFrom) {
		this.contractValidFrom = contractValidFrom;
	}

	public Date getContractValidTo() {
		return this.contractValidTo;
	}

	public void setContractValidTo(Date contractValidTo) {
		this.contractValidTo = contractValidTo;
	}

	public Integer getQuotId() {
		return this.quotId;
	}

	public void setQuotId(Integer quotId) {
		this.quotId = quotId;
	}

	public String getQuotName() {
		return this.quotName;
	}

	public void setQuotName(String quotName) {
		this.quotName = quotName;
	}

	public String getCalcVal() {
		return this.calcVal;
	}

	public void setCalcVal(String calcVal) {
		this.calcVal = calcVal;
	}

	public String getQuotValue() {
		return this.quotValue;
	}

	public void setQuotValue(String quotValue) {
		this.quotValue = quotValue;
	}

	public Integer getQuotUnitId() {
		return this.quotUnitId;
	}

	public void setQuotUnitId(Integer quotUnitId) {
		this.quotUnitId = quotUnitId;
	}

	public String getQuotUnitCode() {
		return this.quotUnitCode;
	}

	public void setQuotUnitCode(String quotUnitCode) {
		this.quotUnitCode = quotUnitCode;
	}

	public UnitType getQuotUnitTypeInd() {
		return this.quotUnitTypeInd;
	}

	public void setQuotUnitTypeInd(UnitType quotUnitTypeInd) {
		this.quotUnitTypeInd = quotUnitTypeInd;
	}

	public Integer getConvUnitId() {
		return this.convUnitId;
	}

	public void setConvUnitId(Integer convUnitId) {
		this.convUnitId = convUnitId;
	}

	public String getConvUnitCode() {
		return this.convUnitCode;
	}

	public void setConvUnitCode(String convUnitCode) {
		this.convUnitCode = convUnitCode;
	}

	public UnitType getConvUnitTypeInd() {
		return this.convUnitTypeInd;
	}

	public void setConvUnitTypeInd(UnitType convUnitTypeInd) {
		this.convUnitTypeInd = convUnitTypeInd;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public MasterAgreementsPeriod getQuotationOffset() {
		return this.quotationOffset;
	}

	public void setQuotationOffset(MasterAgreementsPeriod quotationOffset) {
		this.quotationOffset = quotationOffset;
	}

	public Operator getOperator() {
		return this.operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public BigDecimal getFactor() {
		return this.factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

	public Integer getDecimals() {
		return this.decimals;
	}

	public void setDecimals(Integer decimals) {
		this.decimals = decimals;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getInitialPrice() {
		return this.initialPrice;
	}

	public void setInitialPrice(BigDecimal initialPrice) {
		this.initialPrice = initialPrice;
	}

	public Integer getPcCurr() {
		return this.pcCurr;
	}

	public void setPcCurr(Integer pcCurr) {
		this.pcCurr = pcCurr;
	}

	public Integer getPcUnit() {
		return this.pcUnit;
	}

	public void setPcUnit(Integer pcUnit) {
		this.pcUnit = pcUnit;
	}

	public QuantityType getQuantityType() {
		return this.quantityType;
	}

	public void setQuantityType(QuantityType quantityType) {
		this.quantityType = quantityType;
	}

	public Boolean getContinous() {
		return this.continous;
	}

	public void setContinous(Boolean continous) {
		this.continous = continous;
	}

	public Boolean getDefaultPriceCtgy() {
		return this.defaultPriceCtgy;
	}

	public void setDefaultPriceCtgy(Boolean defaultPriceCtgy) {
		this.defaultPriceCtgy = defaultPriceCtgy;
	}

	public Boolean getRestriction() {
		return this.restriction;
	}

	public void setRestriction(Boolean restriction) {
		this.restriction = restriction;
	}

	public MasterAgreementsPeriod getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(MasterAgreementsPeriod exchangeRateOffset) {
		this.exchangeRateOffset = exchangeRateOffset;
	}

	public Integer getInitialFinancialSourceId() {
		return this.initialFinancialSourceId;
	}

	public void setInitialFinancialSourceId(Integer initialFinancialSourceId) {
		this.initialFinancialSourceId = initialFinancialSourceId;
	}

	public Integer getInitialAverageMethodId() {
		return this.initialAverageMethodId;
	}

	public void setInitialAverageMethodId(Integer initialAverageMethodId) {
		this.initialAverageMethodId = initialAverageMethodId;
	}

	public String getPriceCategoryIdsJson() {
		return this.priceCategoryIdsJson;
	}

	public void setPriceCategoryIdsJson(String priceCategoryIdsJson) {
		this.priceCategoryIdsJson = priceCategoryIdsJson;
	}

	public ValueType getPriceValueType() {
		return this.priceValueType;
	}

	public void setPriceValueType(ValueType priceValueType) {
		this.priceValueType = priceValueType;
	}

	public Integer getPriceCatId() {
		return this.priceCatId;
	}

	public void setPriceCatId(Integer priceCatId) {
		this.priceCatId = priceCatId;
	}

	public String getPriceCatName() {
		return this.priceCatName;
	}

	public void setPriceCatName(String priceCatName) {
		this.priceCatName = priceCatName;
	}

	public PriceType getPriceCatType() {
		return this.priceCatType;
	}

	public void setPriceCatType(PriceType priceCatType) {
		this.priceCatType = priceCatType;
	}

	public PriceInd getPriceCatPricePer() {
		return this.priceCatPricePer;
	}

	public void setPriceCatPricePer(PriceInd priceCatPricePer) {
		this.priceCatPricePer = priceCatPricePer;
	}

	public Integer getQuotAvgMethodId() {
		return this.quotAvgMethodId;
	}

	public void setQuotAvgMethodId(Integer quotAvgMethodId) {
		this.quotAvgMethodId = quotAvgMethodId;
	}

	public String getQuotAvgMethodCode() {
		return this.quotAvgMethodCode;
	}

	public void setQuotAvgMethodCode(String quotAvgMethodCode) {
		this.quotAvgMethodCode = quotAvgMethodCode;
	}

	public String getQuotAvgMethodName() {
		return this.quotAvgMethodName;
	}

	public void setQuotAvgMethodName(String quotAvgMethodName) {
		this.quotAvgMethodName = quotAvgMethodName;
	}

	public Integer getAvgMTId() {
		return this.avgMTId;
	}

	public void setAvgMTId(Integer avgMTId) {
		this.avgMTId = avgMTId;
	}

	public String getAvgMTCode() {
		return this.avgMTCode;
	}

	public void setAvgMTCode(String avgMTCode) {
		this.avgMTCode = avgMTCode;
	}

	public String getAvgMTName() {
		return this.avgMTName;
	}

	public void setAvgMTName(String avgMTName) {
		this.avgMTName = avgMTName;
	}

	public BigDecimal getCalculatedDensity() {
		return this.calculatedDensity;
	}

	public void setCalculatedDensity(BigDecimal calculatedDensity) {
		this.calculatedDensity = calculatedDensity;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIncludeInAverage() {
		return this.includeInAverage;
	}

	public void setIncludeInAverage(Boolean includeInAverage) {
		this.includeInAverage = includeInAverage;
	}

	public VatApplicability getVat() {
		return this.vat;
	}

	public void setVat(VatApplicability vat) {
		this.vat = vat;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getForex() {
		return this.forex;
	}

	public void setForex(String forex) {
		this.forex = forex;
	}

	public Integer getSuppId() {
		return this.suppId;
	}

	public void setSuppId(Integer suppId) {
		this.suppId = suppId;
	}

	public String getSuppCode() {
		return this.suppCode;
	}

	public void setSuppCode(String suppCode) {
		this.suppCode = suppCode;
	}

	public Integer getLocId() {
		return this.locId;
	}

	public void setLocId(Integer locId) {
		this.locId = locId;
	}

	public String getLocCode() {
		return this.locCode;
	}

	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}

	public Integer getCurrencyId() {
		return this.currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getMainCategoryCode() {
		return this.mainCategoryCode;
	}

	public void setMainCategoryCode(String mainCategoryCode) {
		this.mainCategoryCode = mainCategoryCode;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceName() {
		return this.financialSourceName;
	}

	public void setFinancialSourceName(String financialSourceName) {
		this.financialSourceName = financialSourceName;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}

	public Boolean getAvgMethodIndicatorDefMeth() {
		return this.avgMethodIndicatorDefMeth;
	}

	public void setAvgMethodIndicatorDefMeth(Boolean avgMethodIndicatorDefMeth) {
		this.avgMethodIndicatorDefMeth = avgMethodIndicatorDefMeth;
	}

	public String getAvgMethodIndicatorName() {
		return this.avgMethodIndicatorName;
	}

	public void setAvgMethodIndicatorName(String avgMethodIndicatorName) {
		this.avgMethodIndicatorName = avgMethodIndicatorName;
	}

	public Integer getAvgMthdId() {
		return this.avgMthdId;
	}

	public void setAvgMthdId(Integer avgMthdId) {
		this.avgMthdId = avgMthdId;
	}

	public Boolean getPcompProvisional() {
		return this.pcompProvisional;
	}

	public void setPcompProvisional(Boolean pcompProvisional) {
		this.pcompProvisional = pcompProvisional;
	}

	public Boolean getPcompWithHold() {
		return this.pcompWithHold;
	}

	public void setPcompWithHold(Boolean pcompWithHold) {
		this.pcompWithHold = pcompWithHold;
	}

	public String getPcCurrCode() {
		return this.pcCurrCode;
	}

	public void setPcCurrCode(String pcCurrCode) {
		this.pcCurrCode = pcCurrCode;
	}

	public String getPcUnitCode() {
		return this.pcUnitCode;
	}

	public void setPcUnitCode(String pcUnitCode) {
		this.pcUnitCode = pcUnitCode;
	}

	public Integer getQuotFinancialSourceId() {
		return this.quotFinancialSourceId;
	}

	public void setQuotFinancialSourceId(Integer quotFinancialSourceId) {
		this.quotFinancialSourceId = quotFinancialSourceId;
	}

	public String getQuotFinancialSourceCode() {
		return this.quotFinancialSourceCode;
	}

	public void setQuotFinancialSourceCode(String quotFinancialSourceCode) {
		this.quotFinancialSourceCode = quotFinancialSourceCode;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getFormTitle() {
		return this.formTitle;
	}

	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getConvertTitle() {
		return this.convertTitle;
	}

	public void setConvertTitle(String convertTitle) {
		this.convertTitle = convertTitle;
	}

	public BigDecimal getConvertedPrice() {
		return this.convertedPrice;
	}

	public void setConvertedPrice(BigDecimal convertedPrice) {
		this.convertedPrice = convertedPrice;
	}

	public Boolean getUsed1() {
		return this.used1;
	}

	public void setUsed1(Boolean used1) {
		this.used1 = used1;
	}

	public String getInitialPriceTitle() {
		return this.initialPriceTitle;
	}

	public void setInitialPriceTitle(String initialPriceTitle) {
		this.initialPriceTitle = initialPriceTitle;
	}

	public String getSlash() {
		return this.slash;
	}

	public void setSlash(String slash) {
		this.slash = slash;
	}

	public String getPercentageOf() {
		return this.percentageOf;
	}

	public void setPercentageOf(String percentageOf) {
		this.percentageOf = percentageOf;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}

	public String getDummyField() {
		return this.dummyField;
	}

	public void setDummyField(String dummyField) {
		this.dummyField = dummyField;
	}

	public String getPriceComponents() {
		return this.priceComponents;
	}

	public void setPriceComponents(String priceComponents) {
		this.priceComponents = priceComponents;
	}

	public String getBenchmarkProvider() {
		return this.benchmarkProvider;
	}

	public void setBenchmarkProvider(String benchmarkProvider) {
		this.benchmarkProvider = benchmarkProvider;
	}

	public String getConversionFactorValidationMsg() {
		return this.conversionFactorValidationMsg;
	}

	public void setConversionFactorValidationMsg(
			String conversionFactorValidationMsg) {
		this.conversionFactorValidationMsg = conversionFactorValidationMsg;
	}
}
