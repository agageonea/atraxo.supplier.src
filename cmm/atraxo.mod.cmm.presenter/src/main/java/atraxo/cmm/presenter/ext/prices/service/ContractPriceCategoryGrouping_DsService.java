/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.prices.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.ext.exceptions.NoPriceComponentsException;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.presenter.impl.prices.model.ContractPriceCategoryGrouping_Ds;
import atraxo.cmm.presenter.impl.prices.model.ContractPriceCategoryGrouping_DsParam;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ContractPriceCategoryGrouping_DsService extends
		AbstractEntityDsService<ContractPriceCategoryGrouping_Ds, ContractPriceCategoryGrouping_Ds, ContractPriceCategoryGrouping_DsParam, ContractPriceCategory>
		implements IDsService<ContractPriceCategoryGrouping_Ds, ContractPriceCategoryGrouping_Ds, ContractPriceCategoryGrouping_DsParam> {

	private static final Logger LOG = LoggerFactory.getLogger(ContractPriceCategoryGrouping_DsService.class);

	@Override
	protected void postFind(
			IQueryBuilder<ContractPriceCategoryGrouping_Ds, ContractPriceCategoryGrouping_Ds, ContractPriceCategoryGrouping_DsParam> builder,
			List<ContractPriceCategoryGrouping_Ds> result) throws Exception {

		super.postFind(builder, result);
		IContractPriceCategoryService service = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);

		ContractPriceCategoryGrouping_DsParam params = builder.getParams();
		Map<String, ContractPriceCategoryGrouping_Ds> usedPriceCategories = new HashMap<>();
		Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH);
		if (params.getPriceDate() != null && params.getPriceDate().before(date)) {
			date = params.getPriceDate();
		}
		for (ContractPriceCategoryGrouping_Ds priceCategoryDs : result) {
			try {
				this.calculateConvertedPrice(service, params, priceCategoryDs, date);
				this.markPriceCategories(usedPriceCategories, priceCategoryDs);
			} catch (NoPriceComponentsException e) {
				LOG.info("No price component exists, will continue as designed with empty/null values!", e);
				priceCategoryDs.setConvertedPrice(BigDecimal.ZERO);
				priceCategoryDs.setUnitCode("");
				priceCategoryDs.setUnitId(null);
				priceCategoryDs.setCurrencyCode("");
				priceCategoryDs.setCurrencyId(null);
			}
		}
	}

	private void markPriceCategories(Map<String, ContractPriceCategoryGrouping_Ds> usedPriceCategories,
			ContractPriceCategoryGrouping_Ds priceCategoryDs) {
		ContractPriceCategory priceCategory = priceCategoryDs._getEntity_();
		PriceInd priceCategoryPer = priceCategory.getPriceCategory().getPricePer();
		if (!PriceInd._EVENT_.equals(priceCategoryPer)) {
			if (!usedPriceCategories.keySet().contains(priceCategory.getPriceCategory().getName())) {
				usedPriceCategories.put(priceCategory.getPriceCategory().getName(), priceCategoryDs);
				priceCategoryDs.setUsed(true);
			} else if (priceCategory.getDefaultPriceCtgy()) {
				ContractPriceCategoryGrouping_Ds ds = usedPriceCategories.get(priceCategory.getPriceCategory().getName());
				ds.setUsed(false);
				usedPriceCategories.put(priceCategory.getPriceCategory().getName(), priceCategoryDs);
				priceCategoryDs.setUsed(true);
			}
		}
	}

	private void calculateConvertedPrice(IContractPriceCategoryService service, ContractPriceCategoryGrouping_DsParam params,
			ContractPriceCategoryGrouping_Ds priceCategoryDs, Date date) throws BusinessException {
		ContractPriceCategory priceCategory = service.findById(priceCategoryDs.getId());
		if (date.before(priceCategoryDs.getContractValidFrom())) {
			date = priceCategoryDs.getContractValidFrom();
		}
		if (date.after(priceCategoryDs.getContractValidTo())) {
			date = priceCategoryDs.getContractValidTo();
		}
		ContractPriceComponent priceComponent = service.getPriceComponent(priceCategory, date);
		this.checkAndUpdateParams(params, priceComponent.getContrPriceCtgry().getContract());
		if (PriceInd._COMPOSITE_.equals(priceCategoryDs.getPriceCtgryPricePer())) {
			BigDecimal compositePrice = service.getPriceInCurrencyUnit(priceCategory, params.getSettlementUnitCode(),
					params.getSettlementCurrencyCode(), date);
			priceComponent.setPrice(compositePrice);
		}
		this.setContractPriceCtgry(priceComponent, priceCategoryDs);

		BigDecimal convertedPrice = BigDecimal.ZERO;
		try {
			service.getPriceInCurrencyUnit(priceCategory, params.getSettlementUnitCode(), params.getSettlementCurrencyCode(), date);
		} catch (NoPriceComponentsException e) {
			LOG.info("Price component not found for contract " + priceCategory.getContract().getCode() + ", for contract price category "
					+ priceCategory.getName() + " on " + date, e);

		}
		priceCategoryDs.setConvertedPrice(convertedPrice);
	}

	/**
	 * Check if unit and currency parameters are set, otherwise set them with contract settlement currency and unit.
	 *
	 * @param params
	 * @param contract
	 */
	private void checkAndUpdateParams(ContractPriceCategoryGrouping_DsParam params, Contract contract) {
		if (StringUtils.isEmpty(params.getSettlementCurrencyCode())) {
			params.setSettlementCurrencyCode(contract.getSettlementCurr().getCode());
		}
		if (StringUtils.isEmpty(params.getSettlementUnitCode())) {
			params.setSettlementUnitCode(contract.getSettlementUnit().getCode());
		}
	}

	/**
	 * @param cpComponent
	 * @param cpc
	 */
	private void setContractPriceCtgry(ContractPriceComponent cpComponent, ContractPriceCategoryGrouping_Ds cpc) {
		if (cpComponent.getPrice() != null) {
			cpc.setPrice(cpComponent.getPrice());
		}
		if (cpComponent.getValidFrom() != null) {
			cpc.setValidFrom(cpComponent.getValidFrom());
		}
		if (cpComponent.getValidTo() != null) {
			cpc.setValidTo(cpComponent.getValidTo());
		}
		if (cpComponent.getCurrency() != null && cpComponent.getCurrency().getCode() != null) {
			cpc.setCurrencyCode(cpComponent.getCurrency().getCode());
		}
		if (cpComponent.getCurrency() != null && cpComponent.getCurrency().getId() != null) {
			cpc.setCurrencyId(cpComponent.getCurrency().getId());
		}
		if (cpComponent.getUnit() != null && cpComponent.getUnit().getCode() != null) {
			cpc.setUnitCode(cpComponent.getUnit().getCode());
		}
		if (cpComponent.getUnit() != null && cpComponent.getUnit().getId() != null) {
			cpc.setUnitId(cpComponent.getUnit().getId());
		}
	}

}
