/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.contracts.model;

import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractScope;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.Period;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.QuantityType;
import atraxo.cmm.domain.impl.cmm_type.VatApplicability;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceType;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.fmbas_type.ValueType;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Contract.class)
@RefLookups({
		@RefLookup(refId = ContractCustomerWizard_Ds.F_HOLDERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = ContractCustomerWizard_Ds.F_HOLDERCODE)}),
		@RefLookup(refId = ContractCustomerWizard_Ds.F_CUSTOMERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = ContractCustomerWizard_Ds.F_CUSTOMERCODE)}),
		@RefLookup(refId = ContractCustomerWizard_Ds.F_BILLTOID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = ContractCustomerWizard_Ds.F_BILLTOCODE)}),
		@RefLookup(refId = ContractCustomerWizard_Ds.F_RISKHOLDERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = ContractCustomerWizard_Ds.F_RISKHOLDERCODE)}),
		@RefLookup(refId = ContractCustomerWizard_Ds.F_LOCATIONID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = ContractCustomerWizard_Ds.F_LOCATIONCODE)}),
		@RefLookup(refId = ContractCustomerWizard_Ds.F_CONTRACTVOLUMEUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = ContractCustomerWizard_Ds.F_CONTRACTVOLUMEUNITCODE)}),
		@RefLookup(refId = ContractCustomerWizard_Ds.F_SETTLEMENTCURRID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = ContractCustomerWizard_Ds.F_SETTLEMENTCURRCODE)}),
		@RefLookup(refId = ContractCustomerWizard_Ds.F_SETTLEMENTUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = ContractCustomerWizard_Ds.F_SETTLEMENTUNITCODE)}),
		@RefLookup(refId = ContractCustomerWizard_Ds.F_FINANCIALSOURCEID, namedQuery = FinancialSources.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = ContractCustomerWizard_Ds.F_FINANCIALSOURCECODE)}),
		@RefLookup(refId = ContractCustomerWizard_Ds.F_AVERAGEMETHODID)})
public class ContractCustomerWizard_Ds
		extends
			AbstractSubsidiaryDs_Ds<Contract> {

	public static final String ALIAS = "cmm_ContractCustomerWizard_Ds";

	public static final String F_TYPE = "type";
	public static final String F_SUBTYPE = "subType";
	public static final String F_SCOPE = "scope";
	public static final String F_DEALTYPE = "dealType";
	public static final String F_ISBLUEPRINT = "isBlueprint";
	public static final String F_PERIODAPPROVALSTATUS = "periodApprovalStatus";
	public static final String F_PRICEAPPROVALSTATUS = "priceApprovalStatus";
	public static final String F_SHIPTOAPPROVALSTATUS = "shipToApprovalStatus";
	public static final String F_APPROVALSTATUS = "approvalStatus";
	public static final String F_HOLDERID = "holderId";
	public static final String F_HOLDERCODE = "holderCode";
	public static final String F_HOLDERNAME = "holderName";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_CUSTOMERNAME = "customerName";
	public static final String F_CUSTOMERPARENTID = "customerParentId";
	public static final String F_CUSTOMERPARENTCODE = "customerParentCode";
	public static final String F_BILLTOID = "billToId";
	public static final String F_BILLTOCODE = "billToCode";
	public static final String F_BILLTONAME = "billToName";
	public static final String F_RISKHOLDERID = "riskHolderId";
	public static final String F_RISKHOLDERCODE = "riskHolderCode";
	public static final String F_RISKHOLDERNAME = "riskHolderName";
	public static final String F_LOCATIONID = "locationId";
	public static final String F_LOCATIONCODE = "locationCode";
	public static final String F_LOCATIONNAME = "locationName";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_PRODUCT = "product";
	public static final String F_EVENTTYPE = "eventType";
	public static final String F_AWARDEDVOLUME = "awardedVolume";
	public static final String F_CONTRACTVOLUMEUNITID = "contractVolumeUnitId";
	public static final String F_CONTRACTVOLUMEUNITCODE = "contractVolumeUnitCode";
	public static final String F_CONTRACTVOLUMEUNITNAME = "contractVolumeUnitName";
	public static final String F_QUANTITYTYPE = "quantityType";
	public static final String F_PERIOD = "period";
	public static final String F_PAYMENTTERMS = "paymentTerms";
	public static final String F_PAYMENTREFDAY = "paymentRefDay";
	public static final String F_CREDITTERMS = "creditTerms";
	public static final String F_BANKGUARANTEE = "bankGuarantee";
	public static final String F_PREPAIDDAYS = "prepaidDays";
	public static final String F_PREPAIEDAMOUNT = "prepaiedAmount";
	public static final String F_PAYMENTFREQ = "paymentFreq";
	public static final String F_PREPAYFIRSTDELIVERYDATE = "prepayFirstDeliveryDate";
	public static final String F_SETTLEMENTCURRID = "settlementCurrId";
	public static final String F_SETTLEMENTCURRCODE = "settlementCurrCode";
	public static final String F_SETTLEMENTCURRNAME = "settlementCurrName";
	public static final String F_SETTLEMENTUNITID = "settlementUnitId";
	public static final String F_SETTLEMENTUNITCODE = "settlementUnitCode";
	public static final String F_SETTLEMENTUNITNAME = "settlementUnitName";
	public static final String F_INVOICEFREQ = "invoiceFreq";
	public static final String F_FINANCIALSOURCEID = "financialSourceId";
	public static final String F_FINANCIALSOURCECODE = "financialSourceCode";
	public static final String F_FINANCIALSOURCENAME = "financialSourceName";
	public static final String F_AVERAGEMETHODID = "averageMethodId";
	public static final String F_AVERAGEMETHODCODE = "averageMethodCode";
	public static final String F_AVERAGEMETHODNAME = "averageMethodName";
	public static final String F_EXCHANGERATEOFFSET = "exchangeRateOffset";
	public static final String F_VAT = "vat";
	public static final String F_INVOICETYPE = "invoiceType";
	public static final String F_PRICECATEGORYID = "priceCategoryId";
	public static final String F_PRICECATEGORYNAME = "priceCategoryName";
	public static final String F_PRICENAME = "priceName";
	public static final String F_QUOTATIONID = "quotationId";
	public static final String F_QUOTATIONNAME = "quotationName";
	public static final String F_QUOTATIONOFFSET = "quotationOffset";
	public static final String F_INITIALPRICE = "initialPrice";
	public static final String F_INITIALCURID = "initialCurId";
	public static final String F_INITIALUNITID = "initialUnitId";
	public static final String F_INITIALFINANCIALSOURCEID = "initialFinancialSourceId";
	public static final String F_FACTOR = "factor";
	public static final String F_DECIMALS = "decimals";
	public static final String F_OPERATOR = "operator";
	public static final String F_QUOTTIMESERIESID = "quotTimeSeriesId";
	public static final String F_QUOTTIMESERIESDESCRIPTION = "quotTimeSeriesDescription";
	public static final String F_QUOTAVGMETHODID = "quotAvgMethodId";
	public static final String F_QUOTAVGMETHODCODE = "quotAvgMethodCode";
	public static final String F_QUOTAVGMETHODNAME = "quotAvgMethodName";
	public static final String F_QUOTFINANCIALSOURCEID = "quotFinancialSourceId";
	public static final String F_QUOTFINANCIALSOURCECODE = "quotFinancialSourceCode";
	public static final String F_AVGMETHODINDICATORDEFMETH = "avgMethodIndicatorDefMeth";
	public static final String F_AVGMETHODINDICATORNAME = "avgMethodIndicatorName";
	public static final String F_AVGMTHDID = "avgMthdId";
	public static final String F_CALCVAL = "calcVal";
	public static final String F_QUOTVALUE = "quotValue";
	public static final String F_PRICEVALUETYPE = "priceValueType";
	public static final String F_QUOTUNITID = "quotUnitId";
	public static final String F_QUOTUNITCODE = "quotUnitCode";
	public static final String F_QUOTUNITTYPEIND = "quotUnitTypeInd";
	public static final String F_CONVUNITID = "convUnitId";
	public static final String F_CONVUNITCODE = "convUnitCode";
	public static final String F_CONVUNITTYPEIND = "convUnitTypeInd";
	public static final String F_LABELFIELD = "labelField";
	public static final String F_INITIALCURRENCY = "initialCurrency";
	public static final String F_INITIALUNIT = "initialUnit";
	public static final String F_BENCHMARKPROVIDER = "benchmarkProvider";
	public static final String F_CONVERSIONFACTORVALIDATIONMSG = "conversionFactorValidationMsg";
	public static final String F_CALCULATEDDENSITY = "calculatedDensity";

	@DsField
	private ContractType type;

	@DsField
	private ContractSubType subType;

	@DsField
	private ContractScope scope;

	@DsField
	private DealType dealType;

	@DsField
	private Boolean isBlueprint;

	@DsField
	private BidApprovalStatus periodApprovalStatus;

	@DsField
	private BidApprovalStatus priceApprovalStatus;

	@DsField
	private BidApprovalStatus shipToApprovalStatus;

	@DsField(path = "bidApprovalStatus")
	private BidApprovalStatus approvalStatus;

	@DsField(join = "left", path = "holder.id")
	private Integer holderId;

	@DsField(join = "left", path = "holder.code")
	private String holderCode;

	@DsField(join = "left", path = "holder.name")
	private String holderName;

	@DsField(join = "left", path = "customer.id")
	private Integer customerId;

	@DsField(join = "left", path = "customer.code")
	private String customerCode;

	@DsField(join = "left", path = "customer.name")
	private String customerName;

	@DsField(join = "left", path = "customer.parent.id")
	private Integer customerParentId;

	@DsField(join = "left", path = "customer.parent.code")
	private String customerParentCode;

	@DsField(join = "left", path = "billTo.id")
	private Integer billToId;

	@DsField(join = "left", path = "billTo.code")
	private String billToCode;

	@DsField(join = "left", path = "billTo.name")
	private String billToName;

	@DsField(join = "left", path = "riskHolder.id")
	private Integer riskHolderId;

	@DsField(join = "left", path = "riskHolder.code")
	private String riskHolderCode;

	@DsField(join = "left", path = "riskHolder.name")
	private String riskHolderName;

	@DsField(join = "left", path = "location.id")
	private Integer locationId;

	@DsField(join = "left", path = "location.code")
	private String locationCode;

	@DsField(join = "left", path = "location.name")
	private String locationName;

	@DsField(jpqlFilter = " e.validTo >= :validFrom ")
	private Date validFrom;

	@DsField(jpqlFilter = " e.validFrom <= :validTo ")
	private Date validTo;

	@DsField
	private Product product;

	@DsField(path = "limitedTo")
	private FlightTypeIndicator eventType;

	@DsField
	private BigDecimal awardedVolume;

	@DsField(join = "left", path = "contractVolumeUnit.id")
	private Integer contractVolumeUnitId;

	@DsField(join = "left", path = "contractVolumeUnit.code")
	private String contractVolumeUnitCode;

	@DsField(join = "left", path = "contractVolumeUnit.name")
	private String contractVolumeUnitName;

	@DsField
	private QuantityType quantityType;

	@DsField
	private Period period;

	@DsField
	private Integer paymentTerms;

	@DsField
	private PaymentDay paymentRefDay;

	@DsField
	private CreditTerm creditTerms;

	@DsField(path = "bidBankGuarantee")
	private Boolean bankGuarantee;

	@DsField(path = "bidPrepaidDays")
	private Integer prepaidDays;

	@DsField(path = "bidPrepaidAmount")
	private BigDecimal prepaiedAmount;

	@DsField(path = "bidPayementFreq")
	private InvoiceFreq paymentFreq;

	@DsField(path = "bidPrepayFirstDeliveryDate")
	private Integer prepayFirstDeliveryDate;

	@DsField(join = "left", path = "settlementCurr.id")
	private Integer settlementCurrId;

	@DsField(join = "left", path = "settlementCurr.code")
	private String settlementCurrCode;

	@DsField(join = "left", path = "settlementCurr.name")
	private String settlementCurrName;

	@DsField(join = "left", path = "settlementUnit.id")
	private Integer settlementUnitId;

	@DsField(join = "left", path = "settlementUnit.code")
	private String settlementUnitCode;

	@DsField(join = "left", path = "settlementUnit.name")
	private String settlementUnitName;

	@DsField
	private InvoiceFreq invoiceFreq;

	@DsField(join = "left", path = "financialSource.id")
	private Integer financialSourceId;

	@DsField(join = "left", path = "financialSource.code")
	private String financialSourceCode;

	@DsField(join = "left", path = "financialSource.name")
	private String financialSourceName;

	@DsField(join = "left", path = "averageMethod.id")
	private Integer averageMethodId;

	@DsField(join = "left", path = "averageMethod.code")
	private String averageMethodCode;

	@DsField(join = "left", path = "averageMethod.name")
	private String averageMethodName;

	@DsField
	private MasterAgreementsPeriod exchangeRateOffset;

	@DsField
	private VatApplicability vat;

	@DsField
	private InvoiceType invoiceType;

	@DsField(join = "left", path = "pricingBases.priceCat.id")
	private Integer priceCategoryId;

	@DsField(join = "left", path = "pricingBases.priceCat.name")
	private String priceCategoryName;

	@DsField(join = "left", path = "pricingBases.description")
	private String priceName;

	@DsField(join = "left", path = "pricingBases.quotation.id")
	private Integer quotationId;

	@DsField(join = "left", path = "pricingBases.quotation.name")
	private String quotationName;

	@DsField(join = "left", path = "pricingBases.quotationOffset")
	private MasterAgreementsPeriod quotationOffset;

	@DsField(join = "left", fetch = false, path = "pricingBases.initialPrice")
	private BigDecimal initialPrice;

	@DsField(join = "left", fetch = false, path = "pricingBases.initialCurrId")
	private Integer initialCurId;

	@DsField(join = "left", fetch = false, path = "pricingBases.initialUnitId")
	private Integer initialUnitId;

	@DsField(join = "left", fetch = false, path = "pricingBases.financialSourceId")
	private Integer initialFinancialSourceId;

	@DsField(join = "left", path = "pricingBases.factor")
	private BigDecimal factor;

	@DsField(join = "left", path = "pricingBases.decimals")
	private Integer decimals;

	@DsField(join = "left", path = "pricingBases.operator")
	private Operator operator;

	@DsField(join = "left", path = "pricingBases.quotation.timeseries.id")
	private Integer quotTimeSeriesId;

	@DsField(join = "left", path = "pricingBases.quotation.timeseries.description")
	private String quotTimeSeriesDescription;

	@DsField(join = "left", path = "pricingBases.quotation.avgMethodIndicator.id")
	private Integer quotAvgMethodId;

	@DsField(join = "left", path = "pricingBases.quotation.avgMethodIndicator.code")
	private String quotAvgMethodCode;

	@DsField(join = "left", path = "pricingBases.quotation.avgMethodIndicator.name")
	private String quotAvgMethodName;

	@DsField(join = "left", path = "pricingBases.quotation.timeseries.financialSource.id")
	private Integer quotFinancialSourceId;

	@DsField(join = "left", path = "pricingBases.quotation.timeseries.financialSource.code")
	private String quotFinancialSourceCode;

	@DsField(join = "left", path = "pricingBases.contractPriceCategories.averageMethod.defaultMethod")
	private Boolean avgMethodIndicatorDefMeth;

	@DsField(join = "left", path = "pricingBases.contractPriceCategories.averageMethod.name")
	private String avgMethodIndicatorName;

	@DsField(join = "left", path = "pricingBases.contractPriceCategories.averageMethod.id")
	private Integer avgMthdId;

	@DsField(join = "left", fetch = false, path = "pricingBases.quotation.calcVal")
	private String calcVal;

	@DsField(join = "left", fetch = false, path = "pricingBases.quotation.calcVal")
	private String quotValue;

	@DsField(join = "left", path = "pricingBases.quotation.valueType")
	private ValueType priceValueType;

	@DsField(join = "left", path = "pricingBases.quotation.unit.id")
	private Integer quotUnitId;

	@DsField(join = "left", path = "pricingBases.quotation.unit.code")
	private String quotUnitCode;

	@DsField(join = "left", path = "pricingBases.quotation.unit.unittypeInd")
	private UnitType quotUnitTypeInd;

	@DsField(join = "left", path = "pricingBases.convUnit.id")
	private Integer convUnitId;

	@DsField(join = "left", path = "pricingBases.convUnit.code")
	private String convUnitCode;

	@DsField(join = "left", path = "pricingBases.convUnit.unittypeInd")
	private UnitType convUnitTypeInd;

	@DsField(fetch = false)
	private String labelField;

	@DsField(fetch = false)
	private String initialCurrency;

	@DsField(fetch = false)
	private String initialUnit;

	@DsField(fetch = false)
	private String benchmarkProvider;

	@DsField(fetch = false)
	private String conversionFactorValidationMsg;

	@DsField(fetch = false)
	private BigDecimal calculatedDensity;

	/**
	 * Default constructor
	 */
	public ContractCustomerWizard_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ContractCustomerWizard_Ds(Contract e) {
		super(e);
	}

	public ContractType getType() {
		return this.type;
	}

	public void setType(ContractType type) {
		this.type = type;
	}

	public ContractSubType getSubType() {
		return this.subType;
	}

	public void setSubType(ContractSubType subType) {
		this.subType = subType;
	}

	public ContractScope getScope() {
		return this.scope;
	}

	public void setScope(ContractScope scope) {
		this.scope = scope;
	}

	public DealType getDealType() {
		return this.dealType;
	}

	public void setDealType(DealType dealType) {
		this.dealType = dealType;
	}

	public Boolean getIsBlueprint() {
		return this.isBlueprint;
	}

	public void setIsBlueprint(Boolean isBlueprint) {
		this.isBlueprint = isBlueprint;
	}

	public BidApprovalStatus getPeriodApprovalStatus() {
		return this.periodApprovalStatus;
	}

	public void setPeriodApprovalStatus(BidApprovalStatus periodApprovalStatus) {
		this.periodApprovalStatus = periodApprovalStatus;
	}

	public BidApprovalStatus getPriceApprovalStatus() {
		return this.priceApprovalStatus;
	}

	public void setPriceApprovalStatus(BidApprovalStatus priceApprovalStatus) {
		this.priceApprovalStatus = priceApprovalStatus;
	}

	public BidApprovalStatus getShipToApprovalStatus() {
		return this.shipToApprovalStatus;
	}

	public void setShipToApprovalStatus(BidApprovalStatus shipToApprovalStatus) {
		this.shipToApprovalStatus = shipToApprovalStatus;
	}

	public BidApprovalStatus getApprovalStatus() {
		return this.approvalStatus;
	}

	public void setApprovalStatus(BidApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public Integer getHolderId() {
		return this.holderId;
	}

	public void setHolderId(Integer holderId) {
		this.holderId = holderId;
	}

	public String getHolderCode() {
		return this.holderCode;
	}

	public void setHolderCode(String holderCode) {
		this.holderCode = holderCode;
	}

	public String getHolderName() {
		return this.holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Integer getCustomerParentId() {
		return this.customerParentId;
	}

	public void setCustomerParentId(Integer customerParentId) {
		this.customerParentId = customerParentId;
	}

	public String getCustomerParentCode() {
		return this.customerParentCode;
	}

	public void setCustomerParentCode(String customerParentCode) {
		this.customerParentCode = customerParentCode;
	}

	public Integer getBillToId() {
		return this.billToId;
	}

	public void setBillToId(Integer billToId) {
		this.billToId = billToId;
	}

	public String getBillToCode() {
		return this.billToCode;
	}

	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}

	public String getBillToName() {
		return this.billToName;
	}

	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}

	public Integer getRiskHolderId() {
		return this.riskHolderId;
	}

	public void setRiskHolderId(Integer riskHolderId) {
		this.riskHolderId = riskHolderId;
	}

	public String getRiskHolderCode() {
		return this.riskHolderCode;
	}

	public void setRiskHolderCode(String riskHolderCode) {
		this.riskHolderCode = riskHolderCode;
	}

	public String getRiskHolderName() {
		return this.riskHolderName;
	}

	public void setRiskHolderName(String riskHolderName) {
		this.riskHolderName = riskHolderName;
	}

	public Integer getLocationId() {
		return this.locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getLocationCode() {
		return this.locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getLocationName() {
		return this.locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public FlightTypeIndicator getEventType() {
		return this.eventType;
	}

	public void setEventType(FlightTypeIndicator eventType) {
		this.eventType = eventType;
	}

	public BigDecimal getAwardedVolume() {
		return this.awardedVolume;
	}

	public void setAwardedVolume(BigDecimal awardedVolume) {
		this.awardedVolume = awardedVolume;
	}

	public Integer getContractVolumeUnitId() {
		return this.contractVolumeUnitId;
	}

	public void setContractVolumeUnitId(Integer contractVolumeUnitId) {
		this.contractVolumeUnitId = contractVolumeUnitId;
	}

	public String getContractVolumeUnitCode() {
		return this.contractVolumeUnitCode;
	}

	public void setContractVolumeUnitCode(String contractVolumeUnitCode) {
		this.contractVolumeUnitCode = contractVolumeUnitCode;
	}

	public String getContractVolumeUnitName() {
		return this.contractVolumeUnitName;
	}

	public void setContractVolumeUnitName(String contractVolumeUnitName) {
		this.contractVolumeUnitName = contractVolumeUnitName;
	}

	public QuantityType getQuantityType() {
		return this.quantityType;
	}

	public void setQuantityType(QuantityType quantityType) {
		this.quantityType = quantityType;
	}

	public Period getPeriod() {
		return this.period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public PaymentDay getPaymentRefDay() {
		return this.paymentRefDay;
	}

	public void setPaymentRefDay(PaymentDay paymentRefDay) {
		this.paymentRefDay = paymentRefDay;
	}

	public CreditTerm getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(CreditTerm creditTerms) {
		this.creditTerms = creditTerms;
	}

	public Boolean getBankGuarantee() {
		return this.bankGuarantee;
	}

	public void setBankGuarantee(Boolean bankGuarantee) {
		this.bankGuarantee = bankGuarantee;
	}

	public Integer getPrepaidDays() {
		return this.prepaidDays;
	}

	public void setPrepaidDays(Integer prepaidDays) {
		this.prepaidDays = prepaidDays;
	}

	public BigDecimal getPrepaiedAmount() {
		return this.prepaiedAmount;
	}

	public void setPrepaiedAmount(BigDecimal prepaiedAmount) {
		this.prepaiedAmount = prepaiedAmount;
	}

	public InvoiceFreq getPaymentFreq() {
		return this.paymentFreq;
	}

	public void setPaymentFreq(InvoiceFreq paymentFreq) {
		this.paymentFreq = paymentFreq;
	}

	public Integer getPrepayFirstDeliveryDate() {
		return this.prepayFirstDeliveryDate;
	}

	public void setPrepayFirstDeliveryDate(Integer prepayFirstDeliveryDate) {
		this.prepayFirstDeliveryDate = prepayFirstDeliveryDate;
	}

	public Integer getSettlementCurrId() {
		return this.settlementCurrId;
	}

	public void setSettlementCurrId(Integer settlementCurrId) {
		this.settlementCurrId = settlementCurrId;
	}

	public String getSettlementCurrCode() {
		return this.settlementCurrCode;
	}

	public void setSettlementCurrCode(String settlementCurrCode) {
		this.settlementCurrCode = settlementCurrCode;
	}

	public String getSettlementCurrName() {
		return this.settlementCurrName;
	}

	public void setSettlementCurrName(String settlementCurrName) {
		this.settlementCurrName = settlementCurrName;
	}

	public Integer getSettlementUnitId() {
		return this.settlementUnitId;
	}

	public void setSettlementUnitId(Integer settlementUnitId) {
		this.settlementUnitId = settlementUnitId;
	}

	public String getSettlementUnitCode() {
		return this.settlementUnitCode;
	}

	public void setSettlementUnitCode(String settlementUnitCode) {
		this.settlementUnitCode = settlementUnitCode;
	}

	public String getSettlementUnitName() {
		return this.settlementUnitName;
	}

	public void setSettlementUnitName(String settlementUnitName) {
		this.settlementUnitName = settlementUnitName;
	}

	public InvoiceFreq getInvoiceFreq() {
		return this.invoiceFreq;
	}

	public void setInvoiceFreq(InvoiceFreq invoiceFreq) {
		this.invoiceFreq = invoiceFreq;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}

	public String getFinancialSourceName() {
		return this.financialSourceName;
	}

	public void setFinancialSourceName(String financialSourceName) {
		this.financialSourceName = financialSourceName;
	}

	public Integer getAverageMethodId() {
		return this.averageMethodId;
	}

	public void setAverageMethodId(Integer averageMethodId) {
		this.averageMethodId = averageMethodId;
	}

	public String getAverageMethodCode() {
		return this.averageMethodCode;
	}

	public void setAverageMethodCode(String averageMethodCode) {
		this.averageMethodCode = averageMethodCode;
	}

	public String getAverageMethodName() {
		return this.averageMethodName;
	}

	public void setAverageMethodName(String averageMethodName) {
		this.averageMethodName = averageMethodName;
	}

	public MasterAgreementsPeriod getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(MasterAgreementsPeriod exchangeRateOffset) {
		this.exchangeRateOffset = exchangeRateOffset;
	}

	public VatApplicability getVat() {
		return this.vat;
	}

	public void setVat(VatApplicability vat) {
		this.vat = vat;
	}

	public InvoiceType getInvoiceType() {
		return this.invoiceType;
	}

	public void setInvoiceType(InvoiceType invoiceType) {
		this.invoiceType = invoiceType;
	}

	public Integer getPriceCategoryId() {
		return this.priceCategoryId;
	}

	public void setPriceCategoryId(Integer priceCategoryId) {
		this.priceCategoryId = priceCategoryId;
	}

	public String getPriceCategoryName() {
		return this.priceCategoryName;
	}

	public void setPriceCategoryName(String priceCategoryName) {
		this.priceCategoryName = priceCategoryName;
	}

	public String getPriceName() {
		return this.priceName;
	}

	public void setPriceName(String priceName) {
		this.priceName = priceName;
	}

	public Integer getQuotationId() {
		return this.quotationId;
	}

	public void setQuotationId(Integer quotationId) {
		this.quotationId = quotationId;
	}

	public String getQuotationName() {
		return this.quotationName;
	}

	public void setQuotationName(String quotationName) {
		this.quotationName = quotationName;
	}

	public MasterAgreementsPeriod getQuotationOffset() {
		return this.quotationOffset;
	}

	public void setQuotationOffset(MasterAgreementsPeriod quotationOffset) {
		this.quotationOffset = quotationOffset;
	}

	public BigDecimal getInitialPrice() {
		return this.initialPrice;
	}

	public void setInitialPrice(BigDecimal initialPrice) {
		this.initialPrice = initialPrice;
	}

	public Integer getInitialCurId() {
		return this.initialCurId;
	}

	public void setInitialCurId(Integer initialCurId) {
		this.initialCurId = initialCurId;
	}

	public Integer getInitialUnitId() {
		return this.initialUnitId;
	}

	public void setInitialUnitId(Integer initialUnitId) {
		this.initialUnitId = initialUnitId;
	}

	public Integer getInitialFinancialSourceId() {
		return this.initialFinancialSourceId;
	}

	public void setInitialFinancialSourceId(Integer initialFinancialSourceId) {
		this.initialFinancialSourceId = initialFinancialSourceId;
	}

	public BigDecimal getFactor() {
		return this.factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

	public Integer getDecimals() {
		return this.decimals;
	}

	public void setDecimals(Integer decimals) {
		this.decimals = decimals;
	}

	public Operator getOperator() {
		return this.operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public Integer getQuotTimeSeriesId() {
		return this.quotTimeSeriesId;
	}

	public void setQuotTimeSeriesId(Integer quotTimeSeriesId) {
		this.quotTimeSeriesId = quotTimeSeriesId;
	}

	public String getQuotTimeSeriesDescription() {
		return this.quotTimeSeriesDescription;
	}

	public void setQuotTimeSeriesDescription(String quotTimeSeriesDescription) {
		this.quotTimeSeriesDescription = quotTimeSeriesDescription;
	}

	public Integer getQuotAvgMethodId() {
		return this.quotAvgMethodId;
	}

	public void setQuotAvgMethodId(Integer quotAvgMethodId) {
		this.quotAvgMethodId = quotAvgMethodId;
	}

	public String getQuotAvgMethodCode() {
		return this.quotAvgMethodCode;
	}

	public void setQuotAvgMethodCode(String quotAvgMethodCode) {
		this.quotAvgMethodCode = quotAvgMethodCode;
	}

	public String getQuotAvgMethodName() {
		return this.quotAvgMethodName;
	}

	public void setQuotAvgMethodName(String quotAvgMethodName) {
		this.quotAvgMethodName = quotAvgMethodName;
	}

	public Integer getQuotFinancialSourceId() {
		return this.quotFinancialSourceId;
	}

	public void setQuotFinancialSourceId(Integer quotFinancialSourceId) {
		this.quotFinancialSourceId = quotFinancialSourceId;
	}

	public String getQuotFinancialSourceCode() {
		return this.quotFinancialSourceCode;
	}

	public void setQuotFinancialSourceCode(String quotFinancialSourceCode) {
		this.quotFinancialSourceCode = quotFinancialSourceCode;
	}

	public Boolean getAvgMethodIndicatorDefMeth() {
		return this.avgMethodIndicatorDefMeth;
	}

	public void setAvgMethodIndicatorDefMeth(Boolean avgMethodIndicatorDefMeth) {
		this.avgMethodIndicatorDefMeth = avgMethodIndicatorDefMeth;
	}

	public String getAvgMethodIndicatorName() {
		return this.avgMethodIndicatorName;
	}

	public void setAvgMethodIndicatorName(String avgMethodIndicatorName) {
		this.avgMethodIndicatorName = avgMethodIndicatorName;
	}

	public Integer getAvgMthdId() {
		return this.avgMthdId;
	}

	public void setAvgMthdId(Integer avgMthdId) {
		this.avgMthdId = avgMthdId;
	}

	public String getCalcVal() {
		return this.calcVal;
	}

	public void setCalcVal(String calcVal) {
		this.calcVal = calcVal;
	}

	public String getQuotValue() {
		return this.quotValue;
	}

	public void setQuotValue(String quotValue) {
		this.quotValue = quotValue;
	}

	public ValueType getPriceValueType() {
		return this.priceValueType;
	}

	public void setPriceValueType(ValueType priceValueType) {
		this.priceValueType = priceValueType;
	}

	public Integer getQuotUnitId() {
		return this.quotUnitId;
	}

	public void setQuotUnitId(Integer quotUnitId) {
		this.quotUnitId = quotUnitId;
	}

	public String getQuotUnitCode() {
		return this.quotUnitCode;
	}

	public void setQuotUnitCode(String quotUnitCode) {
		this.quotUnitCode = quotUnitCode;
	}

	public UnitType getQuotUnitTypeInd() {
		return this.quotUnitTypeInd;
	}

	public void setQuotUnitTypeInd(UnitType quotUnitTypeInd) {
		this.quotUnitTypeInd = quotUnitTypeInd;
	}

	public Integer getConvUnitId() {
		return this.convUnitId;
	}

	public void setConvUnitId(Integer convUnitId) {
		this.convUnitId = convUnitId;
	}

	public String getConvUnitCode() {
		return this.convUnitCode;
	}

	public void setConvUnitCode(String convUnitCode) {
		this.convUnitCode = convUnitCode;
	}

	public UnitType getConvUnitTypeInd() {
		return this.convUnitTypeInd;
	}

	public void setConvUnitTypeInd(UnitType convUnitTypeInd) {
		this.convUnitTypeInd = convUnitTypeInd;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}

	public String getInitialCurrency() {
		return this.initialCurrency;
	}

	public void setInitialCurrency(String initialCurrency) {
		this.initialCurrency = initialCurrency;
	}

	public String getInitialUnit() {
		return this.initialUnit;
	}

	public void setInitialUnit(String initialUnit) {
		this.initialUnit = initialUnit;
	}

	public String getBenchmarkProvider() {
		return this.benchmarkProvider;
	}

	public void setBenchmarkProvider(String benchmarkProvider) {
		this.benchmarkProvider = benchmarkProvider;
	}

	public String getConversionFactorValidationMsg() {
		return this.conversionFactorValidationMsg;
	}

	public void setConversionFactorValidationMsg(
			String conversionFactorValidationMsg) {
		this.conversionFactorValidationMsg = conversionFactorValidationMsg;
	}

	public BigDecimal getCalculatedDensity() {
		return this.calculatedDensity;
	}

	public void setCalculatedDensity(BigDecimal calculatedDensity) {
		this.calculatedDensity = calculatedDensity;
	}
}
