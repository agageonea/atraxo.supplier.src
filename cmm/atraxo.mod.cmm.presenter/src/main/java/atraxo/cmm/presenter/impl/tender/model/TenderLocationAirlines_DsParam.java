/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.tender.model;

/**
 * Generated code. Do not modify in this file.
 */
public class TenderLocationAirlines_DsParam {

	public static final String f_bidVersion = "bidVersion";
	public static final String f_captureBidFromHolderMA = "captureBidFromHolderMA";
	public static final String f_captureBidFromReceiverMA = "captureBidFromReceiverMA";
	public static final String f_captureBidFromExistingBids = "captureBidFromExistingBids";

	private String bidVersion;

	private Boolean captureBidFromHolderMA;

	private Boolean captureBidFromReceiverMA;

	private Boolean captureBidFromExistingBids;

	public String getBidVersion() {
		return this.bidVersion;
	}

	public void setBidVersion(String bidVersion) {
		this.bidVersion = bidVersion;
	}

	public Boolean getCaptureBidFromHolderMA() {
		return this.captureBidFromHolderMA;
	}

	public void setCaptureBidFromHolderMA(Boolean captureBidFromHolderMA) {
		this.captureBidFromHolderMA = captureBidFromHolderMA;
	}

	public Boolean getCaptureBidFromReceiverMA() {
		return this.captureBidFromReceiverMA;
	}

	public void setCaptureBidFromReceiverMA(Boolean captureBidFromReceiverMA) {
		this.captureBidFromReceiverMA = captureBidFromReceiverMA;
	}

	public Boolean getCaptureBidFromExistingBids() {
		return this.captureBidFromExistingBids;
	}

	public void setCaptureBidFromExistingBids(Boolean captureBidFromExistingBids) {
		this.captureBidFromExistingBids = captureBidFromExistingBids;
	}
}
