/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.shipTo.qb;

import atraxo.cmm.presenter.impl.shipTo.model.ShipToCustomerLov_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class ShipToCustomerLov_DsQb
		extends
			QueryBuilderWithJpql<ShipToCustomerLov_Ds, ShipToCustomerLov_Ds, Object> {

	@Override
	public void setFilter(ShipToCustomerLov_Ds filter) {
		filter.setIsCustomer(true);
		super.setFilter(filter);
	}
}
