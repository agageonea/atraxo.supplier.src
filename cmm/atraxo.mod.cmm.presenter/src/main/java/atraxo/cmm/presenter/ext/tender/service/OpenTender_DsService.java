/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.tender.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.tender.ITenderLocationService;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.presenter.impl.tender.model.OpenTender_Ds;
import atraxo.cmm.presenter.impl.tender.model.OpenTender_DsParam;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service OpenTender_DsService
 */
public class OpenTender_DsService extends AbstractEntityDsService<OpenTender_Ds, OpenTender_Ds, OpenTender_DsParam, Tender>
		implements IDsService<OpenTender_Ds, OpenTender_Ds, OpenTender_DsParam> {

	@Autowired
	private ITenderLocationService locationService;

	@Override
	protected void postFind(IQueryBuilder<OpenTender_Ds, OpenTender_Ds, OpenTender_DsParam> builder, List<OpenTender_Ds> result) throws Exception {
		super.postFind(builder, result);

		OpenTender_DsParam params = builder.getParams();

		List<OpenTender_Ds> filteredDs = new ArrayList<>();
		for (OpenTender_Ds ds : result) {
			List<TenderLocation> locations = this.locationService.findByTenderId(ds.getId());
			List<TenderLocation> openLocations = locations.stream()
					.filter(l -> !BiddingStatus._FINISHED_.equals(l.getLocationBiddingStatus()) && !l.getId().equals(params.getSelectedLocation()))
					.collect(Collectors.toList());

			if (openLocations.size() == 0) {
				filteredDs.add(ds);
			}
		}

		result.removeAll(filteredDs);
	}

}
