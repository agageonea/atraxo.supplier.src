/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.priceUpdate.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdateCategories;
import atraxo.cmm.presenter.impl.priceUpdate.model.PriceUpdateCategories_Ds;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service PriceUpdateCategories_DsService
 */
public class PriceUpdateCategories_DsService
		extends AbstractEntityDsService<PriceUpdateCategories_Ds, PriceUpdateCategories_Ds, Object, PriceUpdateCategories>
		implements IDsService<PriceUpdateCategories_Ds, PriceUpdateCategories_Ds, Object> {

	@Override
	@Transactional
	public void deleteByIds(List<Object> ids) throws Exception {
		List<PriceUpdateCategories> priceUpdatesCategories = this.getEntityService().findByIds(ids);
		for (PriceUpdateCategories element : priceUpdatesCategories) {
			if (element.getPriceUpdate() == null && !((element.getPriceUpdate().getApprovalStatus() == BidApprovalStatus._NEW_
					|| element.getPriceUpdate().getApprovalStatus() == BidApprovalStatus._REJECTED_) && !element.getPriceUpdate().getPublished())) {
				throw new BusinessException(CmmErrorCode.PRICEUPDATECATEGORY_DELETE_CONCURENCY_ACTION_ERROR,
						CmmErrorCode.PRICEUPDATECATEGORY_DELETE_CONCURENCY_ACTION_ERROR.getErrMsg());
			}
		}
		this.getEntityService().delete(priceUpdatesCategories);
	}

}
