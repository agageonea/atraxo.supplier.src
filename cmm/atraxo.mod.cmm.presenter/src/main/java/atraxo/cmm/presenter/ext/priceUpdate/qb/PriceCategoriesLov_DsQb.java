/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.cmm.presenter.ext.priceUpdate.qb;

import org.apache.commons.lang.StringUtils;

import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.presenter.impl.priceUpdate.model.PriceCategoriesLov_Ds;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

/**
 * Custom Query Builder PriceCategoriesLov_DsQb
 */
public class PriceCategoriesLov_DsQb extends QueryBuilderWithJpql<PriceCategoriesLov_Ds, Object, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();

		this.addFilterCondition(
				" e.name != 'Index' AND e.id IN (SELECT e1.priceCategory.id  FROM ContractPriceCategory e1 WHERE e1.clientId = :clientId  AND e1.contract.status IN (:effective, :expired) AND e1.contract.isBlueprint = false AND e1.contract.subsidiaryId IN ('"
						+ StringUtils.join(Session.user.get().getProfile().getOrganizationIds(), "','") + "'))");

		this.addCustomFilterItem("effective", ContractStatus._EFFECTIVE_);
		this.addCustomFilterItem("expired", ContractStatus._EXPIRED_);

		this.addFilterCondition(" e.pricePer!= :price_per ");
		this.addCustomFilterItem("price_per", PriceInd._COMPOSITE_);

	}
}
