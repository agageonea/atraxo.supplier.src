/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.contracts.model;

/**
 * Generated code. Do not modify in this file.
 */
public class Contract_DsParam {

	public static final String f_generatedContractId = "generatedContractId";
	public static final String f_generatedContractCode = "generatedContractCode";
	public static final String f_userIsNotAllowedToView = "userIsNotAllowedToView";
	public static final String f_companyId = "companyId";
	public static final String f_remarks = "remarks";
	public static final String f_submitForApprovalDescription = "submitForApprovalDescription";
	public static final String f_submitForApprovalResult = "submitForApprovalResult";
	public static final String f_selectedAttachments = "selectedAttachments";
	public static final String f_approvalNote = "approvalNote";

	private Integer generatedContractId;

	private String generatedContractCode;

	private Boolean userIsNotAllowedToView;

	private Integer companyId;

	private String remarks;

	private String submitForApprovalDescription;

	private Boolean submitForApprovalResult;

	private String selectedAttachments;

	private String approvalNote;

	public Integer getGeneratedContractId() {
		return this.generatedContractId;
	}

	public void setGeneratedContractId(Integer generatedContractId) {
		this.generatedContractId = generatedContractId;
	}

	public String getGeneratedContractCode() {
		return this.generatedContractCode;
	}

	public void setGeneratedContractCode(String generatedContractCode) {
		this.generatedContractCode = generatedContractCode;
	}

	public Boolean getUserIsNotAllowedToView() {
		return this.userIsNotAllowedToView;
	}

	public void setUserIsNotAllowedToView(Boolean userIsNotAllowedToView) {
		this.userIsNotAllowedToView = userIsNotAllowedToView;
	}

	public Integer getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getSubmitForApprovalDescription() {
		return this.submitForApprovalDescription;
	}

	public void setSubmitForApprovalDescription(
			String submitForApprovalDescription) {
		this.submitForApprovalDescription = submitForApprovalDescription;
	}

	public Boolean getSubmitForApprovalResult() {
		return this.submitForApprovalResult;
	}

	public void setSubmitForApprovalResult(Boolean submitForApprovalResult) {
		this.submitForApprovalResult = submitForApprovalResult;
	}

	public String getSelectedAttachments() {
		return this.selectedAttachments;
	}

	public void setSelectedAttachments(String selectedAttachments) {
		this.selectedAttachments = selectedAttachments;
	}

	public String getApprovalNote() {
		return this.approvalNote;
	}

	public void setApprovalNote(String approvalNote) {
		this.approvalNote = approvalNote;
	}
}
