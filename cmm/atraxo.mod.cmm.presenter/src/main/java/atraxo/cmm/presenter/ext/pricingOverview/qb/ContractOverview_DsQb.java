/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.cmm.presenter.ext.pricingOverview.qb;

import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.presenter.impl.pricingOverview.model.ContractOverview_Ds;
import atraxo.cmm.presenter.impl.pricingOverview.model.ContractOverview_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class ContractOverview_DsQb extends QueryBuilderWithJpql<ContractOverview_Ds, Object, ContractOverview_DsParam> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();

		this.addFilterCondition("e.dealType = :dealType and (e.status = :activeStatus or e.status = :effectiveStatus)");

		this.addCustomFilterItem("dealType", DealType._BUY_);
		this.addCustomFilterItem("activeStatus", ContractStatus._ACTIVE_);
		this.addCustomFilterItem("effectiveStatus", ContractStatus._EFFECTIVE_);

	}
}
