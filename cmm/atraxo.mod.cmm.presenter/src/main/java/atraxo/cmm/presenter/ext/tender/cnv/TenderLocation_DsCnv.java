/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.tender.cnv;

import javax.persistence.EntityManager;

import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.presenter.impl.tender.model.TenderLocation_Ds;
import seava.j4e.api.action.result.IDsConverter;
import seava.j4e.presenter.converter.AbstractDsConverter;

/**
 * Custom converter TenderLocation_DsCnv
 */
public class TenderLocation_DsCnv extends AbstractDsConverter<TenderLocation_Ds, TenderLocation>
		implements IDsConverter<TenderLocation_Ds, TenderLocation> {

	@Override
	public void modelToEntity(TenderLocation_Ds m, TenderLocation e, boolean isInsert, EntityManager em) throws Exception {
		// TODO Auto-generated method stub
		super.modelToEntity(m, e, isInsert, em);
		if (e.getTransmissionStatus() == null) {
			e.setTransmissionStatus(TransmissionStatus._EMPTY_);
		}
		if (e.getIsValid() == null) {
			e.setIsValid(true);
		}
	}
}
