/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.prices.asgn.qb;

import java.util.List;

import atraxo.cmm.presenter.impl.prices.asgn.model.PriceCtgryAssign_Asgn;
import seava.j4e.api.action.query.IFilterRule;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

/**
 * Custom Query Builder Left PriceCtgryAssign_AsgnQb
 */
public class PriceCtgryAssign_AsgnQbLeft extends QueryBuilderWithJpql<PriceCtgryAssign_Asgn, PriceCtgryAssign_Asgn, Object> {

	/*
	 * Check the rule regarding percentage and composite, so there will not create a loop at the assign
	 */
	@Override
	@SuppressWarnings("unchecked")
	protected void beforeBuildWhere() throws Exception {

		for (IFilterRule rule : this.filterRules) {
			if (rule.getFieldName().equals("id")) {

				List<Integer> percentageParents = this.getEntityManager()
						.createNativeQuery(
								"select PRICE_CATGRPRY_ASSIGN_ID from bas_contract_price_ctgry_assign where PRICE_CATEGORY_ID = " + rule.getValue1())
						.getResultList();

				if (!percentageParents.isEmpty()) {
					this.addFilterCondition("e.id not in (" + percentageParents.toString().replace("[", "").replace("]", "") + ")");
				}
			}
		}
		super.beforeBuildWhere();
	}
}
