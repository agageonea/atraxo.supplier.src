package atraxo.cmm.presenter.ext.contracts.delegate;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.ext.bid.IBidService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.utils.NewBidOption;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.presenter.impl.contracts.model.TenderBid_Ds;
import atraxo.cmm.presenter.impl.contracts.model.TenderBid_DsParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class GenerateContract_Pd extends AbstractPresenterDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenerateContract_Pd.class);

	public void generateContract(TenderBid_Ds ds, TenderBid_DsParam params) throws Exception {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Contract contract = contractService.findByBusiness(ds.getId());
		if (BidStatus._AWARD_ACCEPTED_.equals(contract.getBidStatus())) {
			Integer newContractId = contractService.generateSaleContractFromBid(contract);
			params.setGeneratedContractId(newContractId);
		} else {
			throw new BusinessException(CmmErrorCode.BID_STATUS_NOT_ACCEPTED, CmmErrorCode.BID_STATUS_NOT_ACCEPTED.getErrMsg());
		}
	}

	public void copySupplierBid(TenderBid_Ds ds, TenderBid_DsParam params) throws Exception {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		IBidService bidService = (IBidService) this.getApplicationContext().getBean("bidService");
		Contract contract = contractService.findByBusiness(ds.getId());
		TenderLocation tenderLocation = contract.getBidTenderLocation();
		if (params.getNewVersionSelection()) {
			Integer newContractId = bidService.copySupplierBid(contract, tenderLocation.getId(), NewBidOption.NEW_VERSION.name(), null);
			params.setGeneratedContractId(newContractId);
		} else if (params.getAnotherBidSelection()) {
			Integer newContractId = bidService.copySupplierBid(contract, params.getTenderLocationId(), NewBidOption.NEW_LOCATION.name(), null);
			params.setGeneratedContractId(newContractId);
		} else if (params.getNewRevisionSelection()) {
			// check statuses
			boolean statusesAreOk = this.checkStatusesForNewRevisionBid(contract);
			if (statusesAreOk) {
				// check if last revision for bid is transmitted
				boolean lastRevisionTransmitted = this.checkLastTransmittedBidRevision(contract);
				if (lastRevisionTransmitted) {
					Integer newContractId = bidService.copySupplierBid(contract, tenderLocation.getId(), NewBidOption.NEW_REVISION.name(),
							params.getNewRevisionDescription());
					params.setGeneratedContractId(newContractId);
				} else {
					throw new BusinessException(CmmErrorCode.NEW_BID_REVISION_LAST_REVISION_NOT_OK,
							CmmErrorCode.NEW_BID_REVISION_LAST_REVISION_NOT_OK.getErrMsg());
				}
			} else {
				throw new BusinessException(CmmErrorCode.NEW_BID_REVISION_CONCURRENCY_NOT_OK,
						CmmErrorCode.NEW_BID_REVISION_CONCURRENCY_NOT_OK.getErrMsg());
			}
		} else {
			LOGGER.error("Unknown option from UI to backend when calling copy bid !");
			throw new BusinessException(CmmErrorCode.BID_OPTION_UNKNOWN, CmmErrorCode.BID_OPTION_UNKNOWN.getErrMsg());
		}
	}

	/**
	 * Checks if last available revision for a location bid was transmitted or not.
	 *
	 * @param bid
	 * @return
	 */
	private boolean checkLastTransmittedBidRevision(Contract bid) {
		Contract lastRevisionBid = bid;
		for (Contract lBid : (List<Contract>) bid.getBidTenderLocation().getBids()) {
			if (lBid.getBidVersion() != null && lBid.getBidRevision() != null) {
				if (lBid.getBidVersion().equals(bid.getBidVersion()) && lBid.getBidRevision() > lastRevisionBid.getBidRevision()) {
					lastRevisionBid = lBid;
				}
			}
		}
		return lastRevisionBid.getBidTransmissionStatus().equals(TransmissionStatus._TRANSMITTED_)
				|| lastRevisionBid.getBidTransmissionStatus().equals(TransmissionStatus._RECEIVED_);
	}

	/**
	 * Cheks if the conditions for creating a new bid revision are met;
	 *
	 * @param bid
	 * @return
	 */
	private boolean checkStatusesForNewRevisionBid(Contract bid) {
		boolean bidStatusOk = !bid.getBidStatus().equals(BidStatus._AWARDED_) && !bid.getBidStatus().equals(BidStatus._DECLINED_)
				&& !bid.getBidStatus().equals(BidStatus._AWARD_ACCEPTED_);
		boolean transmissionStatusOk = bid.getBidTransmissionStatus().equals(TransmissionStatus._TRANSMITTED_)
				|| bid.getBidTransmissionStatus().equals(TransmissionStatus._RECEIVED_);
		return bidStatusOk && transmissionStatusOk;
	}
}