/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.contracts.model;

import atraxo.cmm.domain.impl.cmm_type.ExportTypeBid;
/**
 * Generated code. Do not modify in this file.
 */
public class TenderBid_DsParam {

	public static final String f_currencyID = "currencyID";
	public static final String f_currencyCode = "currencyCode";
	public static final String f_period = "period";
	public static final String f_referenceTo = "referenceTo";
	public static final String f_averageMethodId = "averageMethodId";
	public static final String f_averageMethodName = "averageMethodName";
	public static final String f_financialSourceId = "financialSourceId";
	public static final String f_financialSourceCode = "financialSourceCode";
	public static final String f_invFreq = "invFreq";
	public static final String f_invType = "invType";
	public static final String f_creditTerms = "creditTerms";
	public static final String f_forex = "forex";
	public static final String f_paymentTerms = "paymentTerms";
	public static final String f_companyId = "companyId";
	public static final String f_generatedContractId = "generatedContractId";
	public static final String f_approvalNote = "approvalNote";
	public static final String f_exportType = "exportType";
	public static final String f_previewFile = "previewFile";
	public static final String f_submitForApprovalSuccesfullySubmitted = "submitForApprovalSuccesfullySubmitted";
	public static final String f_submitForApprovalConcurencyCheckFail = "submitForApprovalConcurencyCheckFail";
	public static final String f_submitForApprovalInvalidBids = "submitForApprovalInvalidBids";
	public static final String f_selectedAttachments = "selectedAttachments";
	public static final String f_newVersionSelection = "newVersionSelection";
	public static final String f_anotherBidSelection = "anotherBidSelection";
	public static final String f_newRevisionSelection = "newRevisionSelection";
	public static final String f_newRevisionDescription = "newRevisionDescription";
	public static final String f_bidCancelReason = "bidCancelReason";
	public static final String f_bidDeclineAwardReason = "bidDeclineAwardReason";
	public static final String f_tenderLocationId = "tenderLocationId";
	public static final String f_invalidBidIds = "invalidBidIds";

	private Integer currencyID;

	private String currencyCode;

	private String period;

	private String referenceTo;

	private Integer averageMethodId;

	private String averageMethodName;

	private Integer financialSourceId;

	private String financialSourceCode;

	private String invFreq;

	private String invType;

	private String creditTerms;

	private String forex;

	private Integer paymentTerms;

	private Integer companyId;

	private Integer generatedContractId;

	private String approvalNote;

	private ExportTypeBid exportType;

	private String previewFile;

	private String submitForApprovalSuccesfullySubmitted;

	private String submitForApprovalConcurencyCheckFail;

	private String submitForApprovalInvalidBids;

	private String selectedAttachments;

	private Boolean newVersionSelection;

	private Boolean anotherBidSelection;

	private Boolean newRevisionSelection;

	private String newRevisionDescription;

	private String bidCancelReason;

	private String bidDeclineAwardReason;

	private Integer tenderLocationId;

	private String invalidBidIds;

	public Integer getCurrencyID() {
		return this.currencyID;
	}

	public void setCurrencyID(Integer currencyID) {
		this.currencyID = currencyID;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getPeriod() {
		return this.period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getReferenceTo() {
		return this.referenceTo;
	}

	public void setReferenceTo(String referenceTo) {
		this.referenceTo = referenceTo;
	}

	public Integer getAverageMethodId() {
		return this.averageMethodId;
	}

	public void setAverageMethodId(Integer averageMethodId) {
		this.averageMethodId = averageMethodId;
	}

	public String getAverageMethodName() {
		return this.averageMethodName;
	}

	public void setAverageMethodName(String averageMethodName) {
		this.averageMethodName = averageMethodName;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}

	public String getInvFreq() {
		return this.invFreq;
	}

	public void setInvFreq(String invFreq) {
		this.invFreq = invFreq;
	}

	public String getInvType() {
		return this.invType;
	}

	public void setInvType(String invType) {
		this.invType = invType;
	}

	public String getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	public String getForex() {
		return this.forex;
	}

	public void setForex(String forex) {
		this.forex = forex;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public Integer getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Integer getGeneratedContractId() {
		return this.generatedContractId;
	}

	public void setGeneratedContractId(Integer generatedContractId) {
		this.generatedContractId = generatedContractId;
	}

	public String getApprovalNote() {
		return this.approvalNote;
	}

	public void setApprovalNote(String approvalNote) {
		this.approvalNote = approvalNote;
	}

	public ExportTypeBid getExportType() {
		return this.exportType;
	}

	public void setExportType(ExportTypeBid exportType) {
		this.exportType = exportType;
	}

	public String getPreviewFile() {
		return this.previewFile;
	}

	public void setPreviewFile(String previewFile) {
		this.previewFile = previewFile;
	}

	public String getSubmitForApprovalSuccesfullySubmitted() {
		return this.submitForApprovalSuccesfullySubmitted;
	}

	public void setSubmitForApprovalSuccesfullySubmitted(
			String submitForApprovalSuccesfullySubmitted) {
		this.submitForApprovalSuccesfullySubmitted = submitForApprovalSuccesfullySubmitted;
	}

	public String getSubmitForApprovalConcurencyCheckFail() {
		return this.submitForApprovalConcurencyCheckFail;
	}

	public void setSubmitForApprovalConcurencyCheckFail(
			String submitForApprovalConcurencyCheckFail) {
		this.submitForApprovalConcurencyCheckFail = submitForApprovalConcurencyCheckFail;
	}

	public String getSubmitForApprovalInvalidBids() {
		return this.submitForApprovalInvalidBids;
	}

	public void setSubmitForApprovalInvalidBids(
			String submitForApprovalInvalidBids) {
		this.submitForApprovalInvalidBids = submitForApprovalInvalidBids;
	}

	public String getSelectedAttachments() {
		return this.selectedAttachments;
	}

	public void setSelectedAttachments(String selectedAttachments) {
		this.selectedAttachments = selectedAttachments;
	}

	public Boolean getNewVersionSelection() {
		return this.newVersionSelection;
	}

	public void setNewVersionSelection(Boolean newVersionSelection) {
		this.newVersionSelection = newVersionSelection;
	}

	public Boolean getAnotherBidSelection() {
		return this.anotherBidSelection;
	}

	public void setAnotherBidSelection(Boolean anotherBidSelection) {
		this.anotherBidSelection = anotherBidSelection;
	}

	public Boolean getNewRevisionSelection() {
		return this.newRevisionSelection;
	}

	public void setNewRevisionSelection(Boolean newRevisionSelection) {
		this.newRevisionSelection = newRevisionSelection;
	}

	public String getNewRevisionDescription() {
		return this.newRevisionDescription;
	}

	public void setNewRevisionDescription(String newRevisionDescription) {
		this.newRevisionDescription = newRevisionDescription;
	}

	public String getBidCancelReason() {
		return this.bidCancelReason;
	}

	public void setBidCancelReason(String bidCancelReason) {
		this.bidCancelReason = bidCancelReason;
	}

	public String getBidDeclineAwardReason() {
		return this.bidDeclineAwardReason;
	}

	public void setBidDeclineAwardReason(String bidDeclineAwardReason) {
		this.bidDeclineAwardReason = bidDeclineAwardReason;
	}

	public Integer getTenderLocationId() {
		return this.tenderLocationId;
	}

	public void setTenderLocationId(Integer tenderLocationId) {
		this.tenderLocationId = tenderLocationId;
	}

	public String getInvalidBidIds() {
		return this.invalidBidIds;
	}

	public void setInvalidBidIds(String invalidBidIds) {
		this.invalidBidIds = invalidBidIds;
	}
}
