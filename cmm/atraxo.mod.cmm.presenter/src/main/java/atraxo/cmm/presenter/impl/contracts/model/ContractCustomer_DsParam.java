/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.contracts.model;

/**
 * Generated code. Do not modify in this file.
 */
public class ContractCustomer_DsParam {

	public static final String f_currencyID = "currencyID";
	public static final String f_currencyCode = "currencyCode";
	public static final String f_period = "period";
	public static final String f_referenceTo = "referenceTo";
	public static final String f_averageMethodId = "averageMethodId";
	public static final String f_averageMethodName = "averageMethodName";
	public static final String f_financialSourceId = "financialSourceId";
	public static final String f_financialSourceCode = "financialSourceCode";
	public static final String f_invFreq = "invFreq";
	public static final String f_invType = "invType";
	public static final String f_creditTerms = "creditTerms";
	public static final String f_forex = "forex";
	public static final String f_paymentTerms = "paymentTerms";
	public static final String f_companyId = "companyId";
	public static final String f_generatedContractId = "generatedContractId";
	public static final String f_generatedContractCode = "generatedContractCode";
	public static final String f_userIsNotAllowedToView = "userIsNotAllowedToView";
	public static final String f_remarks = "remarks";
	public static final String f_submitForApprovalDescription = "submitForApprovalDescription";
	public static final String f_submitForApprovalResult = "submitForApprovalResult";
	public static final String f_selectedAttachments = "selectedAttachments";
	public static final String f_approvalNote = "approvalNote";
	public static final String f_contractChangeType = "contractChangeType";

	private Integer currencyID;

	private String currencyCode;

	private String period;

	private String referenceTo;

	private Integer averageMethodId;

	private String averageMethodName;

	private Integer financialSourceId;

	private String financialSourceCode;

	private String invFreq;

	private String invType;

	private String creditTerms;

	private String forex;

	private Integer paymentTerms;

	private Integer companyId;

	private Integer generatedContractId;

	private String generatedContractCode;

	private Boolean userIsNotAllowedToView;

	private String remarks;

	private String submitForApprovalDescription;

	private Boolean submitForApprovalResult;

	private String selectedAttachments;

	private String approvalNote;

	private String contractChangeType;

	public Integer getCurrencyID() {
		return this.currencyID;
	}

	public void setCurrencyID(Integer currencyID) {
		this.currencyID = currencyID;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getPeriod() {
		return this.period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getReferenceTo() {
		return this.referenceTo;
	}

	public void setReferenceTo(String referenceTo) {
		this.referenceTo = referenceTo;
	}

	public Integer getAverageMethodId() {
		return this.averageMethodId;
	}

	public void setAverageMethodId(Integer averageMethodId) {
		this.averageMethodId = averageMethodId;
	}

	public String getAverageMethodName() {
		return this.averageMethodName;
	}

	public void setAverageMethodName(String averageMethodName) {
		this.averageMethodName = averageMethodName;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}

	public String getInvFreq() {
		return this.invFreq;
	}

	public void setInvFreq(String invFreq) {
		this.invFreq = invFreq;
	}

	public String getInvType() {
		return this.invType;
	}

	public void setInvType(String invType) {
		this.invType = invType;
	}

	public String getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	public String getForex() {
		return this.forex;
	}

	public void setForex(String forex) {
		this.forex = forex;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public Integer getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Integer getGeneratedContractId() {
		return this.generatedContractId;
	}

	public void setGeneratedContractId(Integer generatedContractId) {
		this.generatedContractId = generatedContractId;
	}

	public String getGeneratedContractCode() {
		return this.generatedContractCode;
	}

	public void setGeneratedContractCode(String generatedContractCode) {
		this.generatedContractCode = generatedContractCode;
	}

	public Boolean getUserIsNotAllowedToView() {
		return this.userIsNotAllowedToView;
	}

	public void setUserIsNotAllowedToView(Boolean userIsNotAllowedToView) {
		this.userIsNotAllowedToView = userIsNotAllowedToView;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getSubmitForApprovalDescription() {
		return this.submitForApprovalDescription;
	}

	public void setSubmitForApprovalDescription(
			String submitForApprovalDescription) {
		this.submitForApprovalDescription = submitForApprovalDescription;
	}

	public Boolean getSubmitForApprovalResult() {
		return this.submitForApprovalResult;
	}

	public void setSubmitForApprovalResult(Boolean submitForApprovalResult) {
		this.submitForApprovalResult = submitForApprovalResult;
	}

	public String getSelectedAttachments() {
		return this.selectedAttachments;
	}

	public void setSelectedAttachments(String selectedAttachments) {
		this.selectedAttachments = selectedAttachments;
	}

	public String getApprovalNote() {
		return this.approvalNote;
	}

	public void setApprovalNote(String approvalNote) {
		this.approvalNote = approvalNote;
	}

	public String getContractChangeType() {
		return this.contractChangeType;
	}

	public void setContractChangeType(String contractChangeType) {
		this.contractChangeType = contractChangeType;
	}
}
