/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.cmm.presenter.ext.prices.qb;

import java.util.List;

import atraxo.cmm.presenter.impl.prices.model.ContractPriceCategoryAssign_Ds;
import atraxo.cmm.presenter.impl.prices.model.ContractPriceCategoryAssign_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

/**
 * Custom Query Builder ContractPriceCategoryAssign_DsQb
 */
public class ContractPriceCategoryAssign_DsQb
		extends QueryBuilderWithJpql<ContractPriceCategoryAssign_Ds, Object, ContractPriceCategoryAssign_DsParam> {

	/*
	 * Check the rule regarding percentage and composite, so there will not create a loop at the assign
	 */
	@Override
	@SuppressWarnings("unchecked")
	protected void beforeBuildWhere() throws Exception {
		Integer compositeId = this.params.getMainPriceCategoryId();
		if (compositeId != null) {
			List<Integer> compositeParents = this.getEntityManager()
					.createNativeQuery(
							"select PRICE_CATGRPRY_ASSIGN_ID from bas_contract_price_ctgry_assign where PRICE_CATEGORY_ID = " + compositeId)
					.getResultList();

			if (!compositeParents.isEmpty()) {
				this.addFilterCondition("e.id not in (" + compositeParents.toString().replace("[", "").replace("]", "") + ")");
			}
		}
		super.beforeBuildWhere();
	}
}
