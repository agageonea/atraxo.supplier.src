/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.pricingOverview.model;

/**
 * Generated code. Do not modify in this file.
 */
public class ContractOverview_DsParam {

	public static final String f_p_currency = "p_currency";
	public static final String f_p_unit = "p_unit";

	private String p_currency;

	private String p_unit;

	public String getP_currency() {
		return this.p_currency;
	}

	public void setP_currency(String p_currency) {
		this.p_currency = p_currency;
	}

	public String getP_unit() {
		return this.p_unit;
	}

	public void setP_unit(String p_unit) {
		this.p_unit = p_unit;
	}
}
