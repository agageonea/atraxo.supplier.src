package atraxo.cmm.presenter.ext.contracts.delegate;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.ext.bid.IBidService;
import atraxo.cmm.business.ext.bid.BidService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.presenter.impl.contracts.model.ContractCustomer_Ds;
import atraxo.cmm.presenter.impl.contracts.model.ContractCustomer_DsParam;
import atraxo.cmm.presenter.impl.contracts.model.Contract_Ds;
import atraxo.cmm.presenter.impl.contracts.model.Contract_DsParam;
import atraxo.cmm.presenter.impl.contracts.model.TenderBid_Ds;
import atraxo.cmm.presenter.impl.contracts.model.TenderBid_DsParam;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.suppliers.ISuppliersService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class CopyContract_Pd extends AbstractPresenterDelegate {

	public void copySaleContract(ContractCustomer_Ds contractDS, ContractCustomer_DsParam params) throws Exception {
		ICustomerService customerService = (ICustomerService) this.findEntityService(Customer.class);
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Customer customer = customerService.findById(params.getCompanyId());
		Contract contract = contractService.findById(contractDS.getId());
		Integer contractId = contractService.copyAndSaveSaleContract(contract, customer);
		params.setGeneratedContractId(contractId);
	}

	public void createBlueprintContract(ContractCustomer_Ds contractDS, ContractCustomer_DsParam params) throws Exception {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Contract contract = contractService.findById(contractDS.getId());
		if (contract.getReadOnly()) {
			throw new BusinessException(CmmErrorCode.CONTRACT_READ_ONLY, CmmErrorCode.CONTRACT_READ_ONLY.getErrMsg());
		}
		Integer contractId = contractService.getBlueprintSaleContract(contract).getId();
		params.setGeneratedContractId(contractId);
	}

	public void copyBuyContract(Contract_Ds contractDS, Contract_DsParam params) throws Exception {
		ISuppliersService supplierService = (ISuppliersService) this.findEntityService(Suppliers.class);
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Suppliers supplier = supplierService.findById(params.getCompanyId());
		Contract contract = contractService.findById(contractDS.getId());
		Integer contractId = contractService.copyAndSaveBuyContract(contract, supplier);
		params.setGeneratedContractId(contractId);
	}

	public void copyBid(TenderBid_Ds tenderBidDS, TenderBid_DsParam params) throws Exception {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		IBidService bidService = this.getApplicationContext().getBean(BidService.class);
		Contract contract = contractService.findById(tenderBidDS.getId());
		Integer contractId = bidService.copyBid(contract);
		params.setGeneratedContractId(contractId);
	}

}
