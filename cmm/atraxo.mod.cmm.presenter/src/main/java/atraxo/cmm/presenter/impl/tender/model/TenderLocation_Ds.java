/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.tender.model;

import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.ImportBidFileType;
import atraxo.cmm.domain.impl.cmm_type.PaymentChoise;
import atraxo.cmm.domain.impl.cmm_type.PaymentType;
import atraxo.cmm.domain.impl.cmm_type.Period;
import atraxo.cmm.domain.impl.cmm_type.PricingType;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.SourceType;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.cmm_type.TenderSource;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.cmm_type.YesNo;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.DataProvider;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceType;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.fmbas_type.TenderVolumeOffer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = TenderLocation.class, sort = {@SortField(field = TenderLocation_Ds.F_LOCATIONCODE)})
@RefLookups({
		@RefLookup(refId = TenderLocation_Ds.F_LOCATIONID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderLocation_Ds.F_LOCATIONCODE)}),
		@RefLookup(refId = TenderLocation_Ds.F_TENDERID, namedQuery = Tender.NQ_FIND_BY_CODE_PRIMITIVE, params = {
				@Param(name = "code", field = TenderLocation_Ds.F_TENDERCODE),
				@Param(name = "tenderVersion", field = TenderLocation_Ds.F_TENDERVERSION),
				@Param(name = "holderId", field = TenderLocation_Ds.F_TENDERHOLDERID)}),
		@RefLookup(refId = TenderLocation_Ds.F_VOLUMEUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderLocation_Ds.F_VOLUMEUNITCODE)}),
		@RefLookup(refId = TenderLocation_Ds.F_DENSITYWEIGHTUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderLocation_Ds.F_DENSITYWEIGHTUNITCODE)}),
		@RefLookup(refId = TenderLocation_Ds.F_DENSITYVOLUMEUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderLocation_Ds.F_DENSITYVOLUMEUNITCODE)}),
		@RefLookup(refId = TenderLocation_Ds.F_DENSITYCONVFROMUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderLocation_Ds.F_DENSITYCONVFROMUNITCODE)}),
		@RefLookup(refId = TenderLocation_Ds.F_DENSITYCONVTOUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderLocation_Ds.F_DENSITYCONVTOUNITCODE)}),
		@RefLookup(refId = TenderLocation_Ds.F_MARKETCURRENCYID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderLocation_Ds.F_MARKETCURRENCYCODE)}),
		@RefLookup(refId = TenderLocation_Ds.F_MARKETPRICINGUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderLocation_Ds.F_MARKETPRICINGUNITCODE)}),
		@RefLookup(refId = TenderLocation_Ds.F_FINANCIALSOURCEID, namedQuery = FinancialSources.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderLocation_Ds.F_FINANCIALSOURCECODE)}),
		@RefLookup(refId = TenderLocation_Ds.F_INDEXPROVIDERID, namedQuery = TimeSerie.NQ_FIND_BY_NAME, params = {@Param(name = "serieName", field = TenderLocation_Ds.F_INDEXPROVIDERNAME)}),
		@RefLookup(refId = TenderLocation_Ds.F_INDEXAVERAGINGMETHODID, namedQuery = AverageMethod.NQ_FIND_BY_NAME, params = {@Param(name = "code", field = TenderLocation_Ds.F_INDEXAVERAGINGMETHODNAME)}),
		@RefLookup(refId = TenderLocation_Ds.F_AVERAGINGMETHODID, namedQuery = AverageMethod.NQ_FIND_BY_NAME, params = {@Param(name = "code", field = TenderLocation_Ds.F_AVERAGINGMETHODNAME)}),
		@RefLookup(refId = TenderLocation_Ds.F_SETTLEMENTCURRENCYID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderLocation_Ds.F_SETTLEMENTCURRENCYCODE)}),
		@RefLookup(refId = TenderLocation_Ds.F_SETTLEMENTUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderLocation_Ds.F_SETTLEMENTUNITCODE)})})
public class TenderLocation_Ds extends AbstractDs_Ds<TenderLocation> {

	public static final String ALIAS = "cmm_TenderLocation_Ds";

	public static final String F_LOCATIONID = "locationId";
	public static final String F_LOCATIONCODE = "locationCode";
	public static final String F_LOCATIONNAME = "locationName";
	public static final String F_COUNTRYID = "countryId";
	public static final String F_COUNTRYNAME = "countryName";
	public static final String F_TENDERID = "tenderId";
	public static final String F_TENDERCODE = "tenderCode";
	public static final String F_TENDERVERSION = "tenderVersion";
	public static final String F_TENDERSTATUS = "tenderStatus";
	public static final String F_BIDDINGPERIODFROM = "biddingPeriodFrom";
	public static final String F_BIDDINGPERIODTO = "biddingPeriodTo";
	public static final String F_TENDERSOURCE = "tenderSource";
	public static final String F_TENDERHOLDERID = "tenderHolderId";
	public static final String F_TENDERHOLDERCODE = "tenderHolderCode";
	public static final String F_VOLUMEUNITID = "volumeUnitId";
	public static final String F_VOLUMEUNITCODE = "volumeUnitCode";
	public static final String F_DENSITYWEIGHTUNITID = "densityWeightUnitId";
	public static final String F_DENSITYWEIGHTUNITCODE = "densityWeightUnitCode";
	public static final String F_DENSITYWEIGHTUNITNAME = "densityWeightUnitName";
	public static final String F_DENSITYVOLUMEUNITID = "densityVolumeUnitId";
	public static final String F_DENSITYVOLUMEUNITCODE = "densityVolumeUnitCode";
	public static final String F_DENSITYCONVFROMUNITID = "densityConvFromUnitId";
	public static final String F_DENSITYCONVFROMUNITCODE = "densityConvFromUnitCode";
	public static final String F_DENSITYCONVTOUNITID = "densityConvToUnitId";
	public static final String F_DENSITYCONVTOUNITCODE = "densityConvToUnitCode";
	public static final String F_MARKETCURRENCYID = "marketCurrencyId";
	public static final String F_MARKETCURRENCYCODE = "marketCurrencyCode";
	public static final String F_MARKETPRICINGUNITID = "marketPricingUnitId";
	public static final String F_MARKETPRICINGUNITCODE = "marketPricingUnitCode";
	public static final String F_FINANCIALSOURCEID = "financialSourceId";
	public static final String F_FINANCIALSOURCECODE = "financialSourceCode";
	public static final String F_AVERAGINGMETHODID = "averagingMethodId";
	public static final String F_AVERAGINGMETHODCODE = "averagingMethodCode";
	public static final String F_AVERAGINGMETHODNAME = "averagingMethodName";
	public static final String F_INDEXPROVIDERID = "indexProviderId";
	public static final String F_INDEXPROVIDERNAME = "indexProviderName";
	public static final String F_INDEXPROVIDERDESC = "indexProviderDesc";
	public static final String F_INDEXPROVIDERCODE = "indexProviderCode";
	public static final String F_INDEXPROVIDERFROMUNITID = "indexProviderFromUnitId";
	public static final String F_INDEXPROVIDERTOUNITID = "indexProviderToUnitId";
	public static final String F_INDEXPROVIDERFACTOR = "indexProviderFactor";
	public static final String F_INDEXPROVIDERDECIMALS = "indexProviderDecimals";
	public static final String F_INDEXAVERAGINGMETHODID = "indexAveragingMethodId";
	public static final String F_INDEXAVERAGINGMETHODCODE = "indexAveragingMethodCode";
	public static final String F_INDEXAVERAGINGMETHODNAME = "indexAveragingMethodName";
	public static final String F_SETTLEMENTCURRENCYID = "settlementCurrencyId";
	public static final String F_SETTLEMENTCURRENCYCODE = "settlementCurrencyCode";
	public static final String F_SETTLEMENTUNITID = "settlementUnitId";
	public static final String F_SETTLEMENTUNITCODE = "settlementUnitCode";
	public static final String F_SETTLEMENTUNITNAME = "settlementUnitName";
	public static final String F_LOCATIONBIDDINGSTATUS = "locationBiddingStatus";
	public static final String F_ADHOCLOC = "adHocLoc";
	public static final String F_AGREEMENTFROM = "agreementFrom";
	public static final String F_AGREEMENTTO = "agreementTo";
	public static final String F_VOLUME = "volume";
	public static final String F_HASTOTALVOLUME = "hasTotalVolume";
	public static final String F_PERIOD = "period";
	public static final String F_FUELPRODUCT = "fuelProduct";
	public static final String F_TAXTYPE = "taxType";
	public static final String F_ASTMSPECIFICATION = "astmSpecification";
	public static final String F_IATASERVICELEVEL = "iataServiceLevel";
	public static final String F_FLIGHTSERVICETYPE = "flightServiceType";
	public static final String F_DELIVERYPOINT = "deliveryPoint";
	public static final String F_OPERATINGHOURS = "operatingHours";
	public static final String F_REFUELERCODE = "refuelerCode";
	public static final String F_TITLETRANSFER = "titleTransfer";
	public static final String F_FUELDENSITY = "fuelDensity";
	public static final String F_OPERATOR = "operator";
	public static final String F_CONVERSIONFACTOR = "conversionFactor";
	public static final String F_COMMENTS = "comments";
	public static final String F_PAYMENTTERMS = "paymentTerms";
	public static final String F_PAYMENTREFDATE = "paymentRefDate";
	public static final String F_INVOICEFREQUENCY = "invoiceFrequency";
	public static final String F_INVOICETYPE = "invoiceType";
	public static final String F_NUMBEROFDAYSPREPAID = "numberOfDaysPrepaid";
	public static final String F_PREPAYMENTDAYOFFSET = "prepaymentDayOffset";
	public static final String F_PREPAYMENTFREQUENCY = "prepaymentFrequency";
	public static final String F_PREPAYMENTAMOUNT = "prepaymentAmount";
	public static final String F_PAYMENTMETHOD = "paymentMethod";
	public static final String F_DEPOSITREQUIRED = "depositRequired";
	public static final String F_EXCHANGERATEOFFSET = "exchangeRateOffset";
	public static final String F_DATAPROVIDER = "dataProvider";
	public static final String F_INDEXAVERAGINGPERIOD = "indexAveragingPeriod";
	public static final String F_RATE = "rate";
	public static final String F_SOURCETYPE = "sourceType";
	public static final String F_SOURCENAME = "sourceName";
	public static final String F_CREDITTERMS = "creditTerms";
	public static final String F_PRICINGBASE = "pricingBase";
	public static final String F_PACKAGEIDENTIFIER = "packageIdentifier";
	public static final String F_TRANSMISSIONSTATUS = "transmissionStatus";
	public static final String F_ISVALID = "isValid";
	public static final String F_VOLUMEOFFER = "volumeOffer";
	public static final String F_CANBEDECLINED = "canBeDeclined";
	public static final String F_ROUND = "round";
	public static final String F_BIDPERIODFROM = "bidPeriodFrom";
	public static final String F_BIDPERIODTO = "bidPeriodTo";
	public static final String F_BIDOPENINGDATE = "bidOpeningDate";
	public static final String F_LABELFIELD = "labelField";
	public static final String F_FORMTITLE = "formTitle";
	public static final String F_FILEPATH = "filePath";
	public static final String F_IMPORTBIDFILETYPE = "importBidFileType";

	@DsField(join = "left", path = "location.id")
	private Integer locationId;

	@DsField(join = "left", path = "location.code")
	private String locationCode;

	@DsField(join = "left", path = "location.name")
	private String locationName;

	@DsField(join = "left", path = "location.country.id")
	private Integer countryId;

	@DsField(join = "left", path = "location.country.name")
	private String countryName;

	@DsField(join = "left", path = "tender.id")
	private Integer tenderId;

	@DsField(join = "left", path = "tender.code")
	private String tenderCode;

	@DsField(join = "left", path = "tender.version")
	private Long tenderVersion;

	@DsField(join = "left", path = "tender.status")
	private TenderStatus tenderStatus;

	@DsField(join = "left", path = "tender.biddingPeriodFrom")
	private Date biddingPeriodFrom;

	@DsField(join = "left", path = "tender.biddingPeriodTo")
	private Date biddingPeriodTo;

	@DsField(join = "left", path = "tender.source")
	private TenderSource tenderSource;

	@DsField(join = "left", path = "tender.holder.id")
	private Integer tenderHolderId;

	@DsField(join = "left", path = "tender.holder.code")
	private String tenderHolderCode;

	@DsField(join = "left", path = "volumeUnit.id")
	private Integer volumeUnitId;

	@DsField(join = "left", path = "volumeUnit.code")
	private String volumeUnitCode;

	@DsField(join = "left", path = "densityWeightUnit.id")
	private Integer densityWeightUnitId;

	@DsField(join = "left", path = "densityWeightUnit.code")
	private String densityWeightUnitCode;

	@DsField(join = "left", path = "densityWeightUnit.name")
	private String densityWeightUnitName;

	@DsField(join = "left", path = "densityVolumeUnit.id")
	private Integer densityVolumeUnitId;

	@DsField(join = "left", path = "densityVolumeUnit.code")
	private String densityVolumeUnitCode;

	@DsField(join = "left", path = "densityConvFromUnit.id")
	private Integer densityConvFromUnitId;

	@DsField(join = "left", path = "densityConvFromUnit.code")
	private String densityConvFromUnitCode;

	@DsField(join = "left", path = "densityConvToUnit.id")
	private Integer densityConvToUnitId;

	@DsField(join = "left", path = "densityConvToUnit.code")
	private String densityConvToUnitCode;

	@DsField(join = "left", path = "marketCurrency.id")
	private Integer marketCurrencyId;

	@DsField(join = "left", path = "marketCurrency.code")
	private String marketCurrencyCode;

	@DsField(join = "left", path = "marketPricingUnit.id")
	private Integer marketPricingUnitId;

	@DsField(join = "left", path = "marketPricingUnit.code")
	private String marketPricingUnitCode;

	@DsField(join = "left", path = "financialSource.id")
	private Integer financialSourceId;

	@DsField(join = "left", path = "financialSource.code")
	private String financialSourceCode;

	@DsField(join = "left", path = "averagingMethod.id")
	private Integer averagingMethodId;

	@DsField(join = "left", path = "averagingMethod.code")
	private String averagingMethodCode;

	@DsField(join = "left", path = "averagingMethod.name")
	private String averagingMethodName;

	@DsField(join = "left", path = "indexProvider.id")
	private Integer indexProviderId;

	@DsField(join = "left", path = "indexProvider.serieName")
	private String indexProviderName;

	@DsField(join = "left", path = "indexProvider.description")
	private String indexProviderDesc;

	@DsField(join = "left", path = "indexProvider.externalSerieName")
	private String indexProviderCode;

	@DsField(join = "left", path = "indexProvider.unitId.id")
	private Integer indexProviderFromUnitId;

	@DsField(join = "left", path = "indexProvider.unit2Id.id")
	private Integer indexProviderToUnitId;

	@DsField(join = "left", path = "indexProvider.factor")
	private BigDecimal indexProviderFactor;

	@DsField(join = "left", path = "indexProvider.decimals")
	private Integer indexProviderDecimals;

	@DsField(join = "left", path = "indexAveragingMethod.id")
	private Integer indexAveragingMethodId;

	@DsField(join = "left", path = "indexAveragingMethod.code")
	private String indexAveragingMethodCode;

	@DsField(join = "left", path = "indexAveragingMethod.name")
	private String indexAveragingMethodName;

	@DsField(join = "left", path = "settlementCurrency.id")
	private Integer settlementCurrencyId;

	@DsField(join = "left", path = "settlementCurrency.code")
	private String settlementCurrencyCode;

	@DsField(join = "left", path = "settlementUnit.id")
	private Integer settlementUnitId;

	@DsField(join = "left", path = "settlementUnit.code")
	private String settlementUnitCode;

	@DsField(join = "left", path = "settlementUnit.name")
	private String settlementUnitName;

	@DsField
	private BiddingStatus locationBiddingStatus;

	@DsField
	private Boolean adHocLoc;

	@DsField
	private Date agreementFrom;

	@DsField
	private Date agreementTo;

	@DsField
	private BigDecimal volume;

	@DsField
	private Boolean hasTotalVolume;

	@DsField(path = "periodType")
	private Period period;

	@DsField
	private Product fuelProduct;

	@DsField
	private TaxType taxType;

	@DsField
	private String astmSpecification;

	@DsField
	private Integer iataServiceLevel;

	@DsField
	private FlightServiceType flightServiceType;

	@DsField
	private ContractSubType deliveryPoint;

	@DsField
	private String operatingHours;

	@DsField
	private String refuelerCode;

	@DsField
	private String titleTransfer;

	@DsField
	private BigDecimal fuelDensity;

	@DsField
	private Operator operator;

	@DsField
	private BigDecimal conversionFactor;

	@DsField
	private String comments;

	@DsField
	private Long paymentTerms;

	@DsField
	private PaymentDay paymentRefDate;

	@DsField
	private InvoiceFreq invoiceFrequency;

	@DsField
	private InvoiceType invoiceType;

	@DsField
	private Long numberOfDaysPrepaid;

	@DsField
	private Long prepaymentDayOffset;

	@DsField
	private InvoiceFreq prepaymentFrequency;

	@DsField
	private BigDecimal prepaymentAmount;

	@DsField
	private PaymentType paymentMethod;

	@DsField
	private YesNo depositRequired;

	@DsField
	private MasterAgreementsPeriod exchangeRateOffset;

	@DsField
	private DataProvider dataProvider;

	@DsField
	private MasterAgreementsPeriod indexAveragingPeriod;

	@DsField
	private BigDecimal rate;

	@DsField
	private SourceType sourceType;

	@DsField
	private String sourceName;

	@DsField
	private PaymentChoise creditTerms;

	@DsField
	private PricingType pricingBase;

	@DsField
	private String packageIdentifier;

	@DsField
	private TransmissionStatus transmissionStatus;

	@DsField
	private Boolean isValid;

	@DsField
	private TenderVolumeOffer volumeOffer;

	@DsField(fetch = false)
	private Boolean canBeDeclined;

	@DsField(fetch = false)
	private Integer round;

	@DsField(fetch = false)
	private Date bidPeriodFrom;

	@DsField(fetch = false)
	private Date bidPeriodTo;

	@DsField(fetch = false)
	private Date bidOpeningDate;

	@DsField(fetch = false)
	private String labelField;

	@DsField(fetch = false)
	private String formTitle;

	@DsField(fetch = false)
	private String filePath;

	@DsField(fetch = false)
	private ImportBidFileType importBidFileType;

	/**
	 * Default constructor
	 */
	public TenderLocation_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public TenderLocation_Ds(TenderLocation e) {
		super(e);
	}

	public Integer getLocationId() {
		return this.locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getLocationCode() {
		return this.locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getLocationName() {
		return this.locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public Integer getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Integer getTenderId() {
		return this.tenderId;
	}

	public void setTenderId(Integer tenderId) {
		this.tenderId = tenderId;
	}

	public String getTenderCode() {
		return this.tenderCode;
	}

	public void setTenderCode(String tenderCode) {
		this.tenderCode = tenderCode;
	}

	public Long getTenderVersion() {
		return this.tenderVersion;
	}

	public void setTenderVersion(Long tenderVersion) {
		this.tenderVersion = tenderVersion;
	}

	public TenderStatus getTenderStatus() {
		return this.tenderStatus;
	}

	public void setTenderStatus(TenderStatus tenderStatus) {
		this.tenderStatus = tenderStatus;
	}

	public Date getBiddingPeriodFrom() {
		return this.biddingPeriodFrom;
	}

	public void setBiddingPeriodFrom(Date biddingPeriodFrom) {
		this.biddingPeriodFrom = biddingPeriodFrom;
	}

	public Date getBiddingPeriodTo() {
		return this.biddingPeriodTo;
	}

	public void setBiddingPeriodTo(Date biddingPeriodTo) {
		this.biddingPeriodTo = biddingPeriodTo;
	}

	public TenderSource getTenderSource() {
		return this.tenderSource;
	}

	public void setTenderSource(TenderSource tenderSource) {
		this.tenderSource = tenderSource;
	}

	public Integer getTenderHolderId() {
		return this.tenderHolderId;
	}

	public void setTenderHolderId(Integer tenderHolderId) {
		this.tenderHolderId = tenderHolderId;
	}

	public String getTenderHolderCode() {
		return this.tenderHolderCode;
	}

	public void setTenderHolderCode(String tenderHolderCode) {
		this.tenderHolderCode = tenderHolderCode;
	}

	public Integer getVolumeUnitId() {
		return this.volumeUnitId;
	}

	public void setVolumeUnitId(Integer volumeUnitId) {
		this.volumeUnitId = volumeUnitId;
	}

	public String getVolumeUnitCode() {
		return this.volumeUnitCode;
	}

	public void setVolumeUnitCode(String volumeUnitCode) {
		this.volumeUnitCode = volumeUnitCode;
	}

	public Integer getDensityWeightUnitId() {
		return this.densityWeightUnitId;
	}

	public void setDensityWeightUnitId(Integer densityWeightUnitId) {
		this.densityWeightUnitId = densityWeightUnitId;
	}

	public String getDensityWeightUnitCode() {
		return this.densityWeightUnitCode;
	}

	public void setDensityWeightUnitCode(String densityWeightUnitCode) {
		this.densityWeightUnitCode = densityWeightUnitCode;
	}

	public String getDensityWeightUnitName() {
		return this.densityWeightUnitName;
	}

	public void setDensityWeightUnitName(String densityWeightUnitName) {
		this.densityWeightUnitName = densityWeightUnitName;
	}

	public Integer getDensityVolumeUnitId() {
		return this.densityVolumeUnitId;
	}

	public void setDensityVolumeUnitId(Integer densityVolumeUnitId) {
		this.densityVolumeUnitId = densityVolumeUnitId;
	}

	public String getDensityVolumeUnitCode() {
		return this.densityVolumeUnitCode;
	}

	public void setDensityVolumeUnitCode(String densityVolumeUnitCode) {
		this.densityVolumeUnitCode = densityVolumeUnitCode;
	}

	public Integer getDensityConvFromUnitId() {
		return this.densityConvFromUnitId;
	}

	public void setDensityConvFromUnitId(Integer densityConvFromUnitId) {
		this.densityConvFromUnitId = densityConvFromUnitId;
	}

	public String getDensityConvFromUnitCode() {
		return this.densityConvFromUnitCode;
	}

	public void setDensityConvFromUnitCode(String densityConvFromUnitCode) {
		this.densityConvFromUnitCode = densityConvFromUnitCode;
	}

	public Integer getDensityConvToUnitId() {
		return this.densityConvToUnitId;
	}

	public void setDensityConvToUnitId(Integer densityConvToUnitId) {
		this.densityConvToUnitId = densityConvToUnitId;
	}

	public String getDensityConvToUnitCode() {
		return this.densityConvToUnitCode;
	}

	public void setDensityConvToUnitCode(String densityConvToUnitCode) {
		this.densityConvToUnitCode = densityConvToUnitCode;
	}

	public Integer getMarketCurrencyId() {
		return this.marketCurrencyId;
	}

	public void setMarketCurrencyId(Integer marketCurrencyId) {
		this.marketCurrencyId = marketCurrencyId;
	}

	public String getMarketCurrencyCode() {
		return this.marketCurrencyCode;
	}

	public void setMarketCurrencyCode(String marketCurrencyCode) {
		this.marketCurrencyCode = marketCurrencyCode;
	}

	public Integer getMarketPricingUnitId() {
		return this.marketPricingUnitId;
	}

	public void setMarketPricingUnitId(Integer marketPricingUnitId) {
		this.marketPricingUnitId = marketPricingUnitId;
	}

	public String getMarketPricingUnitCode() {
		return this.marketPricingUnitCode;
	}

	public void setMarketPricingUnitCode(String marketPricingUnitCode) {
		this.marketPricingUnitCode = marketPricingUnitCode;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}

	public Integer getAveragingMethodId() {
		return this.averagingMethodId;
	}

	public void setAveragingMethodId(Integer averagingMethodId) {
		this.averagingMethodId = averagingMethodId;
	}

	public String getAveragingMethodCode() {
		return this.averagingMethodCode;
	}

	public void setAveragingMethodCode(String averagingMethodCode) {
		this.averagingMethodCode = averagingMethodCode;
	}

	public String getAveragingMethodName() {
		return this.averagingMethodName;
	}

	public void setAveragingMethodName(String averagingMethodName) {
		this.averagingMethodName = averagingMethodName;
	}

	public Integer getIndexProviderId() {
		return this.indexProviderId;
	}

	public void setIndexProviderId(Integer indexProviderId) {
		this.indexProviderId = indexProviderId;
	}

	public String getIndexProviderName() {
		return this.indexProviderName;
	}

	public void setIndexProviderName(String indexProviderName) {
		this.indexProviderName = indexProviderName;
	}

	public String getIndexProviderDesc() {
		return this.indexProviderDesc;
	}

	public void setIndexProviderDesc(String indexProviderDesc) {
		this.indexProviderDesc = indexProviderDesc;
	}

	public String getIndexProviderCode() {
		return this.indexProviderCode;
	}

	public void setIndexProviderCode(String indexProviderCode) {
		this.indexProviderCode = indexProviderCode;
	}

	public Integer getIndexProviderFromUnitId() {
		return this.indexProviderFromUnitId;
	}

	public void setIndexProviderFromUnitId(Integer indexProviderFromUnitId) {
		this.indexProviderFromUnitId = indexProviderFromUnitId;
	}

	public Integer getIndexProviderToUnitId() {
		return this.indexProviderToUnitId;
	}

	public void setIndexProviderToUnitId(Integer indexProviderToUnitId) {
		this.indexProviderToUnitId = indexProviderToUnitId;
	}

	public BigDecimal getIndexProviderFactor() {
		return this.indexProviderFactor;
	}

	public void setIndexProviderFactor(BigDecimal indexProviderFactor) {
		this.indexProviderFactor = indexProviderFactor;
	}

	public Integer getIndexProviderDecimals() {
		return this.indexProviderDecimals;
	}

	public void setIndexProviderDecimals(Integer indexProviderDecimals) {
		this.indexProviderDecimals = indexProviderDecimals;
	}

	public Integer getIndexAveragingMethodId() {
		return this.indexAveragingMethodId;
	}

	public void setIndexAveragingMethodId(Integer indexAveragingMethodId) {
		this.indexAveragingMethodId = indexAveragingMethodId;
	}

	public String getIndexAveragingMethodCode() {
		return this.indexAveragingMethodCode;
	}

	public void setIndexAveragingMethodCode(String indexAveragingMethodCode) {
		this.indexAveragingMethodCode = indexAveragingMethodCode;
	}

	public String getIndexAveragingMethodName() {
		return this.indexAveragingMethodName;
	}

	public void setIndexAveragingMethodName(String indexAveragingMethodName) {
		this.indexAveragingMethodName = indexAveragingMethodName;
	}

	public Integer getSettlementCurrencyId() {
		return this.settlementCurrencyId;
	}

	public void setSettlementCurrencyId(Integer settlementCurrencyId) {
		this.settlementCurrencyId = settlementCurrencyId;
	}

	public String getSettlementCurrencyCode() {
		return this.settlementCurrencyCode;
	}

	public void setSettlementCurrencyCode(String settlementCurrencyCode) {
		this.settlementCurrencyCode = settlementCurrencyCode;
	}

	public Integer getSettlementUnitId() {
		return this.settlementUnitId;
	}

	public void setSettlementUnitId(Integer settlementUnitId) {
		this.settlementUnitId = settlementUnitId;
	}

	public String getSettlementUnitCode() {
		return this.settlementUnitCode;
	}

	public void setSettlementUnitCode(String settlementUnitCode) {
		this.settlementUnitCode = settlementUnitCode;
	}

	public String getSettlementUnitName() {
		return this.settlementUnitName;
	}

	public void setSettlementUnitName(String settlementUnitName) {
		this.settlementUnitName = settlementUnitName;
	}

	public BiddingStatus getLocationBiddingStatus() {
		return this.locationBiddingStatus;
	}

	public void setLocationBiddingStatus(BiddingStatus locationBiddingStatus) {
		this.locationBiddingStatus = locationBiddingStatus;
	}

	public Boolean getAdHocLoc() {
		return this.adHocLoc;
	}

	public void setAdHocLoc(Boolean adHocLoc) {
		this.adHocLoc = adHocLoc;
	}

	public Date getAgreementFrom() {
		return this.agreementFrom;
	}

	public void setAgreementFrom(Date agreementFrom) {
		this.agreementFrom = agreementFrom;
	}

	public Date getAgreementTo() {
		return this.agreementTo;
	}

	public void setAgreementTo(Date agreementTo) {
		this.agreementTo = agreementTo;
	}

	public BigDecimal getVolume() {
		return this.volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public Boolean getHasTotalVolume() {
		return this.hasTotalVolume;
	}

	public void setHasTotalVolume(Boolean hasTotalVolume) {
		this.hasTotalVolume = hasTotalVolume;
	}

	public Period getPeriod() {
		return this.period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public Product getFuelProduct() {
		return this.fuelProduct;
	}

	public void setFuelProduct(Product fuelProduct) {
		this.fuelProduct = fuelProduct;
	}

	public TaxType getTaxType() {
		return this.taxType;
	}

	public void setTaxType(TaxType taxType) {
		this.taxType = taxType;
	}

	public String getAstmSpecification() {
		return this.astmSpecification;
	}

	public void setAstmSpecification(String astmSpecification) {
		this.astmSpecification = astmSpecification;
	}

	public Integer getIataServiceLevel() {
		return this.iataServiceLevel;
	}

	public void setIataServiceLevel(Integer iataServiceLevel) {
		this.iataServiceLevel = iataServiceLevel;
	}

	public FlightServiceType getFlightServiceType() {
		return this.flightServiceType;
	}

	public void setFlightServiceType(FlightServiceType flightServiceType) {
		this.flightServiceType = flightServiceType;
	}

	public ContractSubType getDeliveryPoint() {
		return this.deliveryPoint;
	}

	public void setDeliveryPoint(ContractSubType deliveryPoint) {
		this.deliveryPoint = deliveryPoint;
	}

	public String getOperatingHours() {
		return this.operatingHours;
	}

	public void setOperatingHours(String operatingHours) {
		this.operatingHours = operatingHours;
	}

	public String getRefuelerCode() {
		return this.refuelerCode;
	}

	public void setRefuelerCode(String refuelerCode) {
		this.refuelerCode = refuelerCode;
	}

	public String getTitleTransfer() {
		return this.titleTransfer;
	}

	public void setTitleTransfer(String titleTransfer) {
		this.titleTransfer = titleTransfer;
	}

	public BigDecimal getFuelDensity() {
		return this.fuelDensity;
	}

	public void setFuelDensity(BigDecimal fuelDensity) {
		this.fuelDensity = fuelDensity;
	}

	public Operator getOperator() {
		return this.operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public BigDecimal getConversionFactor() {
		return this.conversionFactor;
	}

	public void setConversionFactor(BigDecimal conversionFactor) {
		this.conversionFactor = conversionFactor;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Long getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Long paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public PaymentDay getPaymentRefDate() {
		return this.paymentRefDate;
	}

	public void setPaymentRefDate(PaymentDay paymentRefDate) {
		this.paymentRefDate = paymentRefDate;
	}

	public InvoiceFreq getInvoiceFrequency() {
		return this.invoiceFrequency;
	}

	public void setInvoiceFrequency(InvoiceFreq invoiceFrequency) {
		this.invoiceFrequency = invoiceFrequency;
	}

	public InvoiceType getInvoiceType() {
		return this.invoiceType;
	}

	public void setInvoiceType(InvoiceType invoiceType) {
		this.invoiceType = invoiceType;
	}

	public Long getNumberOfDaysPrepaid() {
		return this.numberOfDaysPrepaid;
	}

	public void setNumberOfDaysPrepaid(Long numberOfDaysPrepaid) {
		this.numberOfDaysPrepaid = numberOfDaysPrepaid;
	}

	public Long getPrepaymentDayOffset() {
		return this.prepaymentDayOffset;
	}

	public void setPrepaymentDayOffset(Long prepaymentDayOffset) {
		this.prepaymentDayOffset = prepaymentDayOffset;
	}

	public InvoiceFreq getPrepaymentFrequency() {
		return this.prepaymentFrequency;
	}

	public void setPrepaymentFrequency(InvoiceFreq prepaymentFrequency) {
		this.prepaymentFrequency = prepaymentFrequency;
	}

	public BigDecimal getPrepaymentAmount() {
		return this.prepaymentAmount;
	}

	public void setPrepaymentAmount(BigDecimal prepaymentAmount) {
		this.prepaymentAmount = prepaymentAmount;
	}

	public PaymentType getPaymentMethod() {
		return this.paymentMethod;
	}

	public void setPaymentMethod(PaymentType paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public YesNo getDepositRequired() {
		return this.depositRequired;
	}

	public void setDepositRequired(YesNo depositRequired) {
		this.depositRequired = depositRequired;
	}

	public MasterAgreementsPeriod getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(MasterAgreementsPeriod exchangeRateOffset) {
		this.exchangeRateOffset = exchangeRateOffset;
	}

	public DataProvider getDataProvider() {
		return this.dataProvider;
	}

	public void setDataProvider(DataProvider dataProvider) {
		this.dataProvider = dataProvider;
	}

	public MasterAgreementsPeriod getIndexAveragingPeriod() {
		return this.indexAveragingPeriod;
	}

	public void setIndexAveragingPeriod(
			MasterAgreementsPeriod indexAveragingPeriod) {
		this.indexAveragingPeriod = indexAveragingPeriod;
	}

	public BigDecimal getRate() {
		return this.rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public SourceType getSourceType() {
		return this.sourceType;
	}

	public void setSourceType(SourceType sourceType) {
		this.sourceType = sourceType;
	}

	public String getSourceName() {
		return this.sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public PaymentChoise getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(PaymentChoise creditTerms) {
		this.creditTerms = creditTerms;
	}

	public PricingType getPricingBase() {
		return this.pricingBase;
	}

	public void setPricingBase(PricingType pricingBase) {
		this.pricingBase = pricingBase;
	}

	public String getPackageIdentifier() {
		return this.packageIdentifier;
	}

	public void setPackageIdentifier(String packageIdentifier) {
		this.packageIdentifier = packageIdentifier;
	}

	public TransmissionStatus getTransmissionStatus() {
		return this.transmissionStatus;
	}

	public void setTransmissionStatus(TransmissionStatus transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public Boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}

	public TenderVolumeOffer getVolumeOffer() {
		return this.volumeOffer;
	}

	public void setVolumeOffer(TenderVolumeOffer volumeOffer) {
		this.volumeOffer = volumeOffer;
	}

	public Boolean getCanBeDeclined() {
		return this.canBeDeclined;
	}

	public void setCanBeDeclined(Boolean canBeDeclined) {
		this.canBeDeclined = canBeDeclined;
	}

	public Integer getRound() {
		return this.round;
	}

	public void setRound(Integer round) {
		this.round = round;
	}

	public Date getBidPeriodFrom() {
		return this.bidPeriodFrom;
	}

	public void setBidPeriodFrom(Date bidPeriodFrom) {
		this.bidPeriodFrom = bidPeriodFrom;
	}

	public Date getBidPeriodTo() {
		return this.bidPeriodTo;
	}

	public void setBidPeriodTo(Date bidPeriodTo) {
		this.bidPeriodTo = bidPeriodTo;
	}

	public Date getBidOpeningDate() {
		return this.bidOpeningDate;
	}

	public void setBidOpeningDate(Date bidOpeningDate) {
		this.bidOpeningDate = bidOpeningDate;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}

	public String getFormTitle() {
		return this.formTitle;
	}

	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}

	public String getFilePath() {
		return this.filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public ImportBidFileType getImportBidFileType() {
		return this.importBidFileType;
	}

	public void setImportBidFileType(ImportBidFileType importBidFileType) {
		this.importBidFileType = importBidFileType;
	}
}
