/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.prices.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.ext.prices.builder.IComponentPriceBuilder;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.ext.ContractPriceComponentCheckResult;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.presenter.impl.prices.model.ContractPriceComponent_Ds;
import atraxo.fmbas.domain.impl.fmbas_type.AverageMethodCode;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ContractPriceComponent_DsService
		extends AbstractEntityDsService<ContractPriceComponent_Ds, ContractPriceComponent_Ds, Object, ContractPriceComponent>
		implements IDsService<ContractPriceComponent_Ds, ContractPriceComponent_Ds, Object> {

	@Autowired
	private IContractPriceCategoryService categorySrv;
	@Autowired
	private IComponentPriceBuilder priceBuilder;

	@Override
	protected void preUpdateAfterEntity(ContractPriceComponent_Ds ds, ContractPriceComponent e, Object params) throws Exception {
		super.preUpdateAfterEntity(ds, e, params);
		this.checkPeriod(e);
		this.checkOverlapping(e);
		this.priceBuilder.extendsToMargins(e);
		this.priceBuilder.createConvertedPrices(e);
		this.priceBuilder.updateNeibours(e, false);
	}

	@Override
	protected void onInsert(List<ContractPriceComponent_Ds> list, List<ContractPriceComponent> entities, Object params) throws Exception {
		this.updateContractPriceCategories(entities);
	}

	private void updateContractPriceCategories(List<ContractPriceComponent> entities) throws BusinessException {
		Set<Integer> categoryIds = new HashSet<>();
		List<ContractPriceCategory> categories = new ArrayList<>();
		for (ContractPriceComponent entity : entities) {
			if (categoryIds.add(entity.getContrPriceCtgry().getId())) {
				categories.add(entity.getContrPriceCtgry());
				if (!entity.getContrPriceCtgry().getPriceComponents().contains(entity)) {
					entity.getContrPriceCtgry().addToPriceComponents(entity);
				}
			}
		}
		this.categorySrv.update(categories);
	}

	@Override
	protected void onUpdate(List<ContractPriceComponent_Ds> list, List<ContractPriceComponent> entities, Object params) throws Exception {
		this.updateContractPriceCategories(entities);
	}

	@Override
	protected void preInsert(ContractPriceComponent_Ds ds, ContractPriceComponent e, Object params) throws Exception {
		super.preInsert(ds, e, params);
		this.checkPeriod(e);
		this.checkOverlapping(e);
		this.priceBuilder.extendsToMargins(e);
		this.priceBuilder.createConvertedPrices(e);
		this.priceBuilder.updateNeibours(e, true);

	}

	private void checkPeriod(ContractPriceComponent e) throws BusinessException {
		if (e.getValidFrom().after(e.getValidTo())) { // checks if validFrom is smaller than validTo to stop the user from entering "reverse" dates
			throw new BusinessException(CmmErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
					CmmErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO.getErrMsg());
		}
		if (e.getValidFrom().before(e.getContrPriceCtgry().getContract().getValidFrom())
				|| e.getValidTo().after(e.getContrPriceCtgry().getContract().getValidTo())) { // checks if the validFrom/validTo entered is within the
																								 // bounds of the contract period
			throw new BusinessException(CmmErrorCode.PB_VALIDITY, CmmErrorCode.PB_VALIDITY.getErrMsg());
		}
		if (e.getValidFrom().before(e.getContrPriceCtgry().getPricingBases().getValidFrom())
				|| e.getValidTo().after(e.getContrPriceCtgry().getPricingBases().getValidTo())) { // checks if the validFrom/validTo entered is
																									 // withing the bounds of the contract price
																									 // category
			throw new BusinessException(CmmErrorCode.INVALID_PRICECOMPONENT_DATE, CmmErrorCode.INVALID_PRICECOMPONENT_DATE.getErrMsg());
		}
	}

	/**
	 * Check component overlapping for not continuous price category components.
	 *
	 * @param e
	 * @throws BusinessException
	 */
	private void checkOverlapping(ContractPriceComponent e) throws BusinessException {
		List<ContractPriceComponent> list = new ArrayList<>();
		if (e.getContrPriceCtgry().getPriceComponents() != null) {
			list.addAll(e.getContrPriceCtgry().getPriceComponents());
		}
		if (e.getId() != null) {
			Iterator<ContractPriceComponent> iter = list.iterator();
			while (iter.hasNext()) {
				if (iter.next().getId().equals(e.getId())) {
					iter.remove();
				}
			}
		}

		list.sort((o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()));
		for (ContractPriceComponent c : list) {
			if (e.getValidFrom().equals(c.getValidFrom()) && (e.getValidTo().equals(c.getValidTo()))) {
				throw new BusinessException(CmmErrorCode.PB_OVERLAPING, CmmErrorCode.PB_OVERLAPING.getErrMsg());
			}
			if (!e.getContrPriceCtgry().getContinous()) {
				if ((e.getValidFrom().compareTo(c.getValidFrom()) >= 0 && e.getValidFrom().compareTo(c.getValidTo()) <= 0)
						|| (e.getValidTo().compareTo(c.getValidFrom()) >= 0 && e.getValidTo().compareTo(c.getValidTo()) <= 0)) {
					throw new BusinessException(CmmErrorCode.PB_OVERLAPING, CmmErrorCode.PB_OVERLAPING.getErrMsg());
				} else if ((c.getValidFrom().compareTo(e.getValidFrom()) >= 0 && c.getValidFrom().compareTo(e.getValidTo()) <= 0)
						|| (c.getValidTo().compareTo(e.getValidFrom()) >= 0 && c.getValidTo().compareTo(e.getValidTo()) <= 0)) {
					throw new BusinessException(CmmErrorCode.PB_OVERLAPING, CmmErrorCode.PB_OVERLAPING.getErrMsg());
				}
			}
		}

	}

	@Override
	protected void postInsert(List<ContractPriceComponent_Ds> list, Object params) throws Exception {
		this.collectToleranceResult(list);
	}

	@Override
	protected void postUpdate(List<ContractPriceComponent_Ds> list, Object params) throws Exception {
		this.collectToleranceResult(list);
	}

	protected void collectToleranceResult(List<ContractPriceComponent_Ds> list) {
		for (ContractPriceComponent_Ds ds : list) {
			ds.setToleranceMessage(ContractPriceComponentCheckResult.resultMap.get().remove(ds.getId()));
		}
	}

	@Override
	protected void postFind(IQueryBuilder<ContractPriceComponent_Ds, ContractPriceComponent_Ds, Object> builder,
			List<ContractPriceComponent_Ds> result) throws Exception {
		super.postFind(builder, result);
		for (ContractPriceComponent_Ds ds : result) {
			ContractPriceComponent cpc = ds._getEntity_();
			if (cpc.getContrPriceCtgry().getPricingBases().getQuotation() != null && (AverageMethodCode._WC_.getName()
					.equalsIgnoreCase(cpc.getContrPriceCtgry().getPricingBases().getQuotation().getAvgMethodIndicator().getCode())
					|| AverageMethodCode._WT_.getName()
							.equalsIgnoreCase(cpc.getContrPriceCtgry().getPricingBases().getQuotation().getAvgMethodIndicator().getCode()))) {
				cpc.setValidFrom(this.addADay(cpc.getValidFrom()));
				cpc.setValidTo(this.addADay(cpc.getValidTo()));
			}
		}
	}

	private Date addADay(Date date) {
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_YEAR, 1);
		return cal.getTime();
	}
}
