/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.contracts.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.exceptions.SiblingContractNotFoundException;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.cmm.presenter.impl.contracts.model.Contract_Ds;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class Contract_DsService extends AbstractEntityDsService<Contract_Ds, Contract_Ds, Object, Contract>
		implements IDsService<Contract_Ds, Contract_Ds, Object> {

	private static final Logger LOGGER = LoggerFactory.getLogger(Contract_DsService.class);

	@Autowired
	private IWorkflowInstanceEntityService workflowInstanceEntityService;
	@Autowired
	private IWorkflowBpmManager workflowBpmManager;
	@Autowired
	private IContractService contractService;

	@Override
	protected void postFind(IQueryBuilder<Contract_Ds, Contract_Ds, Object> builder, List<Contract_Ds> result) throws Exception {
		super.postFind(builder, result);
		for (Contract_Ds ds : result) {
			this.setUnitDestination(ds);
			Contract contract = this.contractService.findById(ds.getId());
			StringBuilder sb = new StringBuilder();
			if (contract.getShipTo() != null) {
				for (ShipTo st : contract.getShipTo()) {
					sb.append(st.getCustomer().getName());
					sb.append(", ");
				}
				if (!contract.getShipTo().isEmpty()) {
					sb.setLength(sb.length() - 2);
				}
			}
			try {
				Contract resaleContract = this.contractService.findSiblingContract(contract);
				ds.setResaleContract(resaleContract.getCode());
			} catch (SiblingContractNotFoundException e) {
				LOGGER.info("NO sibling contract found, will set this as a NON RESALE contract!", e);
				ds.setResaleContract(null);
			}
			ds.setShipToList(sb.toString());

			// set can be completed (check for tasks only if status is Awaiting approval in order to not make a search for tasks every time)
			ds.setCanBeCompleted(false);
			if (ds.getApprovalStatus().equals(BidApprovalStatus._AWAITING_APPROVAL_)) {
				try {
					WorkflowInstanceEntity wie = this.workflowInstanceEntityService.findByBusinessKey(
							WorkflowNames.PURCHASE_CONTRACT_APPROVAL.getWorkflowName(), ds.getId(), ds._getEntity_().getClass().getSimpleName());
					ds.setCanBeCompleted(this.workflowBpmManager.canComplete(wie.getWorkflowInstance().getId()));
				} catch (Exception e) {
					// do nothing
					LOGGER.warn("Could not retrieve a WorkflowInstanceEntity for workflow PURCHASE_CONTRACT_APPROVAL, will do nothing !", e);
				}
			}
		}
	}

	private void setUnitDestination(Contract_Ds ds) {
		Locations locations = ds._getEntity_().getLocation();
		String str = "";
		if (locations != null) {
			str += str.matches("") ? locations.getCode() : " , " + locations.getCode();
		}
		ds.setUnit(ds.getSettCurrCode() + "/" + ds.getSettUnitCode());
		ds.setDest(str);
	}

	/**
	 * The percentage of approved customers with sales contracts. Used for KPI.
	 *
	 * @return
	 * @throws Exception
	 */
	public BigDecimal getTermBusinessRation() throws Exception {
		HashMap<String, Object> params = new HashMap<>();
		params.put("dealType", DealType._SELL_);
		List<Contract> list = this.contractService.findEntitiesByAttributes(params);
		Set<Integer> customerIds = new HashSet<>();
		for (Contract c : list) {
			customerIds.add(c.getCustomer().getId());
		}
		params = new HashMap<>();
		params.put("status", CustomerStatus._ACTIVE_);
		params.put("isCustomer", true);
		Set<Integer> appCustomerIds = new HashSet<>();
		List<Customer> customers = this.contractService.findEntitiesByAttributes(Customer.class, params);
		for (Customer customer : customers) {
			if (customerIds.contains(customer.getId())) {
				appCustomerIds.add(customer.getId());
			}
		}
		int nApprovedCustomers = customers.size();
		return nApprovedCustomers == 0 ? BigDecimal.ZERO : new BigDecimal((appCustomerIds.size() * 100) / nApprovedCustomers).setScale(2);
	}

	@Override
	protected void preDelete(List<Object> ids) throws Exception {
		super.preDelete(ids);
		for (Object id : ids) {
			List<Contract> refContracts = this.contractService.findByResaleRefId((Integer) id);
			for (Contract contract : refContracts) {
				if (ContractStatus._EFFECTIVE_.equals(contract.getStatus()) || ContractStatus._EXPIRED_.equals(contract.getStatus())) {
					throw new BusinessException(CmmErrorCode.EFFECTIVE_EXPIRED_GENERATED_CONTRACTS,
							CmmErrorCode.EFFECTIVE_EXPIRED_GENERATED_CONTRACTS.getErrMsg());
				}
			}
		}
	}

	// @Override
	// protected void preUpdateAfterEntity(Contract_Ds ds, Contract e, Object params) throws Exception {
	// super.preUpdateAfterEntity(ds, e, params);
	// this.contractService.checkContractConditions(e);
	// for (PricingBase pb : e.getPricingBases()) {
	// this.priceBuilder.extendsToMargins(pb);
	// this.priceBuilder.buildPricingBases(pb);
	// this.priceBuilder.updateNeighbours(pb);
	// }
	// this.contractService.update(e);
	// }
}
