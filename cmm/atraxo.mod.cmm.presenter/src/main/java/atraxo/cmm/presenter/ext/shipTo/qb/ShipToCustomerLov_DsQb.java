/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.cmm.presenter.ext.shipTo.qb;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.presenter.impl.shipTo.model.ShipToCustomerLov_Ds;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

/**
 * Custom Query Builder ShipToCustomerLov_DsQb
 */
public class ShipToCustomerLov_DsQb extends QueryBuilderWithJpql<ShipToCustomerLov_Ds, ShipToCustomerLov_Ds, Object> {

	private final static Logger LOG = LoggerFactory.getLogger(ShipToCustomerLov_DsQb.class);

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();
		String clientId = Session.user.get().getClientId();
		Object object = true;
		boolean restricted = false;
		try {
			object = this.getEntityManager()
					.createNativeQuery(
							"select e.value from BAS_SYSTEM_PARAMETERS e where e.clientid ='" + clientId + "' and e.code='RESTRICTBLOCKED'")
					.getSingleResult();
			restricted = Boolean.parseBoolean(object.toString());
		} catch (NoResultException | NonUniqueResultException e) {
			LOG.warn("The RESTRICTBLACKLISTED system parameter is missing or has more then one defined: " + e.getMessage(), e);
		}
		String query = ":status1" + (restricted ? "" : ", :status2");
		this.addFilterCondition("e1.status in (" + query + ")");
		this.addCustomFilterItem("status1", CustomerStatus._ACTIVE_);
		if (!restricted) {
			this.addCustomFilterItem("status2", CustomerStatus._BLOCKED_);
		}
	}

}
