/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.priceUpdate.model;

/**
 * Generated code. Do not modify in this file.
 */
public class LocationsLov_DsParam {

	public static final String f_areaID = "areaID";

	private Integer areaID;

	public Integer getAreaID() {
		return this.areaID;
	}

	public void setAreaID(Integer areaID) {
		this.areaID = areaID;
	}
}
