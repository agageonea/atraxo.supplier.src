package atraxo.cmm.presenter.ext.prices.delegate;

import java.util.ArrayList;
import java.util.List;

import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.presenter.impl.prices.model.ContractPriceCategory_Ds;
import atraxo.cmm.presenter.impl.prices.model.ContractPriceCategory_DsParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class ContractPriceCtgryOrder_Pd extends AbstractPresenterDelegate {

	private IContractPriceCategoryService cpcService;

	public void moveUp(ContractPriceCategory_Ds cpc, ContractPriceCategory_DsParam params) throws Exception {
		this.cpcService = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
		ContractPriceCategory currentCpc = this.cpcService.findById(cpc.getId());
		List<ContractPriceCategory> priceCategoryList = this.getAllSiblingPriceCategories(currentCpc, 1);
		this.changeOrder(currentCpc, priceCategoryList);
	}

	public void moveDown(ContractPriceCategory_Ds cpc, ContractPriceCategory_DsParam params) throws Exception {
		this.cpcService = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
		ContractPriceCategory currentCpc = this.cpcService.findById(cpc.getId());
		List<ContractPriceCategory> priceCategoryList = this.getAllSiblingPriceCategories(currentCpc, -1);
		this.changeOrder(currentCpc, priceCategoryList);
	}

	private void changeOrder(ContractPriceCategory currentCpc, List<ContractPriceCategory> priceCategoryList) throws BusinessException {
		Integer currentPossition = priceCategoryList.indexOf(currentCpc);
		if (currentPossition == 0) {
			return;
		}
		ContractPriceCategory otherCpc = priceCategoryList.get(currentPossition - 1);
		Integer aux = otherCpc.getOrderIndex();
		otherCpc.setOrderIndex(currentCpc.getOrderIndex());
		currentCpc.setOrderIndex(aux);
		currentCpc.setOrderChanged(true);
		otherCpc.setOrderChanged(true);
		List<ContractPriceCategory> toUpdateList = new ArrayList<>();
		toUpdateList.add(otherCpc);
		toUpdateList.add(currentCpc);
		this.cpcService.update(toUpdateList);

	}

	private List<ContractPriceCategory> getAllSiblingPriceCategories(ContractPriceCategory cpc, Integer order) {
		Contract contract = cpc.getContract();
		List<ContractPriceCategory> allSiblingPriceCategories = new ArrayList<>(contract.getPriceCategories());
		allSiblingPriceCategories.sort((c1, c2) -> order * (c1.getOrderIndex().compareTo(c2.getOrderIndex())));
		return allSiblingPriceCategories;
	}
}
