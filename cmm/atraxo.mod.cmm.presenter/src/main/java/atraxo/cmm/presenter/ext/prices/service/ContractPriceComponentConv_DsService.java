/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.prices.service;

import java.util.List;

import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.cmm.presenter.impl.prices.model.ContractPriceComponentConv_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ContractPriceComponentConv_DsService
		extends AbstractEntityDsService<ContractPriceComponentConv_Ds, ContractPriceComponentConv_Ds, Object, ContractPriceComponentConv>
		implements IDsService<ContractPriceComponentConv_Ds, ContractPriceComponentConv_Ds, Object> {

	private static final String SEPARATOR = "/";

	@Override
	protected void postFind(IQueryBuilder<ContractPriceComponentConv_Ds, ContractPriceComponentConv_Ds, Object> builder,
			List<ContractPriceComponentConv_Ds> result) throws Exception {
		super.postFind(builder, result);
		StringBuilder sb = new StringBuilder();
		for (ContractPriceComponentConv_Ds ds : result) {
			sb.delete(0, sb.length());
			sb.append(ds.getEquivalentCurrencyCode());
			sb.append(SEPARATOR);
			sb.append(ds.getEquivalentUnitCode());
			ds.setEquivalentCurrencyUnitCode(sb.toString());
		}
	}
}
