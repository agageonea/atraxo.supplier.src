/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.contracts.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.shipTo.IShipToService;
import atraxo.cmm.business.api.tender.ITenderLocationService;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import atraxo.cmm.presenter.impl.contracts.model.TenderBid_Ds;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.api.session.IOrganization;
import seava.j4e.api.session.Session;
import seava.j4e.commons.utils.BigDecimalFormater;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service TenderBid_DsService
 */
public class TenderBid_DsService extends AbstractEntityDsService<TenderBid_Ds, TenderBid_Ds, Object, Contract>
		implements IDsService<TenderBid_Ds, TenderBid_Ds, Object> {

	private static final Logger LOGG = LoggerFactory.getLogger(TenderBid_DsService.class);

	@Autowired
	private ISystemParameterService systemParamsService;
	@Autowired
	private IWorkflowInstanceEntityService workflowInstanceEntityService;
	@Autowired
	private ITenderLocationService tenderLocationService;
	@Autowired
	private IWorkflowBpmManager workflowBpmManager;
	@Autowired
	private IShipToService shipToService;
	@Autowired
	private ICancelBidService cancelBidService;

	@Override
	protected void postFind(IQueryBuilder<TenderBid_Ds, TenderBid_Ds, Object> builder, List<TenderBid_Ds> result) throws Exception {
		super.postFind(builder, result);
		this.getRequiredVolume(result);
		this.calculatePercentage(result);
		this.getTenderBidRoundFromLocation(result);
		Calendar cal = GregorianCalendar.getInstance();
		cal.add(Calendar.HOUR, -2);
		Date date = cal.getTime();
		for (TenderBid_Ds ds : result) {
			this.setAirlinesColumn(ds);
			if (ds.getCreatedAt().after(date)) {
				ds.setMarked(true);
			} else {
				ds.setMarked(false);
			}
			ds.setCanBeCompleted(false);
			ds.setCanCancel(this.cancelBidService.canCancelBids(Arrays.asList(ds)));
			if (ds.getBidApprovalStatus().equals(BidApprovalStatus._AWAITING_APPROVAL_)) {
				try {
					WorkflowInstanceEntity wie = this.workflowInstanceEntityService.findByBusinessKey(WorkflowNames.BID_APPROVAL.getWorkflowName(),
							ds.getId(), ds._getEntity_().getClass().getSimpleName());
					ds.setCanBeCompleted(this.workflowBpmManager.canComplete(wie.getWorkflowInstance().getId()));
				} catch (Exception e) {
					LOGG.info("No Bid Workflow Tasks assigned to " + Session.user.get().getLoginName(), e);
				}
			}
		}
	}

	private void setAirlinesColumn(TenderBid_Ds ds) throws Exception {
		IContractService contr = (IContractService) this.findEntityService(Contract.class);
		Contract contract = contr.findById(ds.getId());
		StringBuilder sb = new StringBuilder();
		if (contract.getShipTo() != null) {
			for (ShipTo st : contract.getShipTo()) {
				sb.append(st.getCustomer().getName());
				sb.append(", ");
			}
			if (!contract.getShipTo().isEmpty()) {
				sb.setLength(sb.length() - 2);
			}
		}
		ds.setAirlinesList(sb.toString());
	}

	/**
	 * SONE-6285 - On postFind method, the Tender Bid Version is directly taken from the linked Active TenderLocationRound
	 *
	 * @param result
	 */
	private void getTenderBidRoundFromLocation(List<TenderBid_Ds> result) {
		for (TenderBid_Ds ds : result) {
			try {
				for (TenderLocationRound locationRound : this.tenderLocationService.findById(ds.getBidLocId()).getTenderLocationRounds()) {
					if (locationRound.getActive()) {
						ds.setBidRound(locationRound.getRoundNo());
						break;
					}
				}
			} catch (Exception e) {
				LOGG.warn("Could not locate the bidding round for this tender bid : " + ds.getCode(), e);
			}
		}
	}

	protected void calculatePercentage(List<TenderBid_Ds> result) throws Exception {
		IUnitService unitConverter = (IUnitService) this.findEntityService(Unit.class);
		ISystemParameterService systemParameterService = (ISystemParameterService) this.getServiceLocator().findEntityService(SystemParameter.class);

		for (TenderBid_Ds ds : result) {
			try {
				Double density = Double.parseDouble(systemParameterService.getDensity());
				Unit fromUnit = unitConverter.findByCode(ds.getSettUnitCode());
				Unit toUnit = unitConverter.findByCode(ds.getBidLocVolUnitCode());

				BigDecimal convertedconvertedBidLocVolumeOffered = BigDecimal.ZERO;
				if (ds.getOfferedVolume() != null && ds.getBidLocVolume().compareTo(BigDecimal.ZERO) > 0) {
					convertedconvertedBidLocVolumeOffered = unitConverter.convert(fromUnit, toUnit, ds.getOfferedVolume(), density);
					convertedconvertedBidLocVolumeOffered = convertedconvertedBidLocVolumeOffered.multiply(new BigDecimal(100))
							.divide(ds.getBidLocVolume(), systemParameterService.getDecimalsForPercentage(), RoundingMode.HALF_UP);
				}
				ds.setPercentCoveredOffered(convertedconvertedBidLocVolumeOffered);

				BigDecimal convertedconvertedBidLocVolumeAwarded = BigDecimal.ZERO;
				if (ds.getAwardedVolume() != null && ds.getBidLocVolume().compareTo(BigDecimal.ZERO) > 0) {
					convertedconvertedBidLocVolumeAwarded = unitConverter.convert(fromUnit, toUnit, ds.getAwardedVolume(), density);
					convertedconvertedBidLocVolumeAwarded = convertedconvertedBidLocVolumeAwarded.multiply(new BigDecimal(100))
							.divide(ds.getBidLocVolume(), systemParameterService.getDecimalsForPercentage(), RoundingMode.HALF_UP);
				}
				ds.setPercentCoveredAwarded(convertedconvertedBidLocVolumeAwarded);

				// set subsidiary code
				IOrganization org = Session.user.get().getProfile().getOrganizations().stream().filter(o -> ds.getSubsidiaryId().equals(o.getId()))
						.findFirst().orElse(null);

				if (org != null) {
					ds.setSubsidiaryCode(org.getCode());
				}

			} catch (Exception e) {
				LOGG.warn("Coul not calculate volume percentage for bid : " + ds.getCode(), e);
			}
		}
	}

	protected void getRequiredVolume(List<TenderBid_Ds> result) throws BusinessException {

		for (TenderBid_Ds ds : result) {
			BigDecimal total = BigDecimal.ZERO;
			List<ShipTo> shipTos = this.shipToService.findByContractId(ds.getId());
			for (ShipTo shipTo : shipTos) {
				if (shipTo.getTenderBidVolume() != null) {
					total = total.add(shipTo.getTenderBidVolume());
				}
			}
			if (total.equals(BigDecimal.ZERO)) {
				total = ds.getBidLocVolume();
			}

			String formatedVolume = BigDecimalFormater.dynamicNumberFormat(total, this.systemParamsService.getDecimalsForUnit());
			StringBuilder requiredVolume = new StringBuilder();
			requiredVolume.append(formatedVolume).append(" ").append(ds.getBidLocVolUnitCode());
			ds.setRequiredVolumeTitle(requiredVolume.toString());
		}

	}

}
