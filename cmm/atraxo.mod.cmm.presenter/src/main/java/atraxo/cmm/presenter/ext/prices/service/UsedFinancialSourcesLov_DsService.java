/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.prices.service;

import java.util.Comparator;
import java.util.List;

import atraxo.cmm.presenter.impl.prices.model.UsedFinancialSourcesLov_Ds;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service UsedFinancialSourcesLov_DsService
 */
public class UsedFinancialSourcesLov_DsService
		extends AbstractEntityDsService<UsedFinancialSourcesLov_Ds, UsedFinancialSourcesLov_Ds, Object, FinancialSources>
		implements IDsService<UsedFinancialSourcesLov_Ds, UsedFinancialSourcesLov_Ds, Object> {

	/**
	 * Compare financial sources based on code but: - PLATTS is first - ARGUS is second if PLATTS not exists otherwise is first - OPIS is third if
	 * ARGUS and PLATTS exists, is second if ARGUS xor PLATTS, is first if none of ARGUS or PLATTS exists.
	 *
	 * @author zspeter
	 */
	private final class SpecialFinancialSourcesComparator implements Comparator<UsedFinancialSourcesLov_Ds> {
		@Override
		public int compare(UsedFinancialSourcesLov_Ds o1, UsedFinancialSourcesLov_Ds o2) {
			int compareTo = o1.getCardinal().compareTo(o2.getCardinal());
			if (compareTo == 0) {
				compareTo = o1.getCode().compareTo(o2.getCode());
			}
			return compareTo;
		}
	}

	@Override
	protected void postFind(IQueryBuilder<UsedFinancialSourcesLov_Ds, UsedFinancialSourcesLov_Ds, Object> builder,
			List<UsedFinancialSourcesLov_Ds> result) throws Exception {
		super.postFind(builder, result);
		result.stream().forEach(UsedFinancialSourcesLov_DsService::populateCardinal);
		result.sort(new SpecialFinancialSourcesComparator());
	}

	private static void populateCardinal(UsedFinancialSourcesLov_Ds o1) {
		if ("PLATTS".equalsIgnoreCase(o1.getCode())) {
			o1.setCardinal(1);
		} else if ("ARGUS".equalsIgnoreCase(o1.getCode())) {
			o1.setCardinal(2);
		} else if ("OPIS".equalsIgnoreCase(o1.getCode())) {
			o1.setCardinal(3);
		} else {
			o1.setCardinal(4);
		}
	}
}
