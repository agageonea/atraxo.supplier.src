/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.prices.model;

import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ContractPriceComponentConv.class, sort = {@SortField(field = ContractPriceComponentConv_Ds.F_VALIDFROM, desc = true)})
@RefLookups({@RefLookup(refId = ContractPriceComponentConv_Ds.F_CONTRACTPRICECOMPONENT)})
public class ContractPriceComponentConv_Ds
		extends
			AbstractDs_Ds<ContractPriceComponentConv> {

	public static final String ALIAS = "cmm_ContractPriceComponentConv_Ds";

	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_PRICE = "price";
	public static final String F_CURRENCYUNITCD = "currencyUnitCd";
	public static final String F_EQUIVALENT = "equivalent";
	public static final String F_EXCHANGERATE = "exchangeRate";
	public static final String F_EXCHANGERATECURRNECYCDS = "exchangeRateCurrnecyCDS";
	public static final String F_OFDATE = "ofDate";
	public static final String F_PROVISIONAL = "provisional";
	public static final String F_WHLDPAY = "whldPay";
	public static final String F_CONTRACTPRICECOMPONENT = "contractPriceComponent";
	public static final String F_CONTRACTPRICECTGRY = "contractPriceCtgry";
	public static final String F_EQUIVALENTUNITID = "equivalentUnitId";
	public static final String F_EQUIVALENTUNITCODE = "equivalentUnitCode";
	public static final String F_EQUIVALENTCURRENCYID = "equivalentCurrencyId";
	public static final String F_EQUIVALENTCURRENCYCODE = "equivalentCurrencyCode";
	public static final String F_EQUIVALENTCURRENCYUNITCODE = "equivalentCurrencyUnitCode";

	@DsField(jpqlFilter = " e.validTo >= :validFrom ")
	private Date validFrom;

	@DsField(jpqlFilter = " e.validFrom <= :validTo ")
	private Date validTo;

	@DsField
	private BigDecimal price;

	@DsField
	private String currencyUnitCd;

	@DsField
	private BigDecimal equivalent;

	@DsField
	private BigDecimal exchangeRate;

	@DsField
	private String exchangeRateCurrnecyCDS;

	@DsField
	private Date ofDate;

	@DsField
	private Boolean provisional;

	@DsField
	private Boolean whldPay;

	@DsField(join = "left", path = "contractPriceComponent.id")
	private Integer contractPriceComponent;

	@DsField(join = "left", path = "contractPriceComponent.contrPriceCtgry.id")
	private Integer contractPriceCtgry;

	@DsField(join = "left", path = "equivalentUnit.id")
	private Integer equivalentUnitId;

	@DsField(join = "left", path = "equivalentUnit.code")
	private String equivalentUnitCode;

	@DsField(join = "left", path = "equivalentCurrency.id")
	private Integer equivalentCurrencyId;

	@DsField(join = "left", path = "equivalentCurrency.code")
	private String equivalentCurrencyCode;

	@DsField(fetch = false)
	private String equivalentCurrencyUnitCode;

	/**
	 * Default constructor
	 */
	public ContractPriceComponentConv_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ContractPriceComponentConv_Ds(ContractPriceComponentConv e) {
		super(e);
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getCurrencyUnitCd() {
		return this.currencyUnitCd;
	}

	public void setCurrencyUnitCd(String currencyUnitCd) {
		this.currencyUnitCd = currencyUnitCd;
	}

	public BigDecimal getEquivalent() {
		return this.equivalent;
	}

	public void setEquivalent(BigDecimal equivalent) {
		this.equivalent = equivalent;
	}

	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getExchangeRateCurrnecyCDS() {
		return this.exchangeRateCurrnecyCDS;
	}

	public void setExchangeRateCurrnecyCDS(String exchangeRateCurrnecyCDS) {
		this.exchangeRateCurrnecyCDS = exchangeRateCurrnecyCDS;
	}

	public Date getOfDate() {
		return this.ofDate;
	}

	public void setOfDate(Date ofDate) {
		this.ofDate = ofDate;
	}

	public Boolean getProvisional() {
		return this.provisional;
	}

	public void setProvisional(Boolean provisional) {
		this.provisional = provisional;
	}

	public Boolean getWhldPay() {
		return this.whldPay;
	}

	public void setWhldPay(Boolean whldPay) {
		this.whldPay = whldPay;
	}

	public Integer getContractPriceComponent() {
		return this.contractPriceComponent;
	}

	public void setContractPriceComponent(Integer contractPriceComponent) {
		this.contractPriceComponent = contractPriceComponent;
	}

	public Integer getContractPriceCtgry() {
		return this.contractPriceCtgry;
	}

	public void setContractPriceCtgry(Integer contractPriceCtgry) {
		this.contractPriceCtgry = contractPriceCtgry;
	}

	public Integer getEquivalentUnitId() {
		return this.equivalentUnitId;
	}

	public void setEquivalentUnitId(Integer equivalentUnitId) {
		this.equivalentUnitId = equivalentUnitId;
	}

	public String getEquivalentUnitCode() {
		return this.equivalentUnitCode;
	}

	public void setEquivalentUnitCode(String equivalentUnitCode) {
		this.equivalentUnitCode = equivalentUnitCode;
	}

	public Integer getEquivalentCurrencyId() {
		return this.equivalentCurrencyId;
	}

	public void setEquivalentCurrencyId(Integer equivalentCurrencyId) {
		this.equivalentCurrencyId = equivalentCurrencyId;
	}

	public String getEquivalentCurrencyCode() {
		return this.equivalentCurrencyCode;
	}

	public void setEquivalentCurrencyCode(String equivalentCurrencyCode) {
		this.equivalentCurrencyCode = equivalentCurrencyCode;
	}

	public String getEquivalentCurrencyUnitCode() {
		return this.equivalentCurrencyUnitCode;
	}

	public void setEquivalentCurrencyUnitCode(String equivalentCurrencyUnitCode) {
		this.equivalentCurrencyUnitCode = equivalentCurrencyUnitCode;
	}
}
