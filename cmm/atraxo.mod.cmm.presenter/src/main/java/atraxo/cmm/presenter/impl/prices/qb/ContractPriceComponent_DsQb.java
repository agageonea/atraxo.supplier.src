/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.prices.qb;

import atraxo.cmm.presenter.impl.prices.model.ContractPriceComponent_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class ContractPriceComponent_DsQb
		extends
			QueryBuilderWithJpql<ContractPriceComponent_Ds, ContractPriceComponent_Ds, Object> {
}
