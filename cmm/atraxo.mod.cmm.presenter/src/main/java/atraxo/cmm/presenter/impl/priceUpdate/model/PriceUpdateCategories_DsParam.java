/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.priceUpdate.model;

/**
 * Generated code. Do not modify in this file.
 */
public class PriceUpdateCategories_DsParam {

	public static final String f_selectedAll = "selectedAll";

	private Boolean selectedAll;

	public Boolean getSelectedAll() {
		return this.selectedAll;
	}

	public void setSelectedAll(Boolean selectedAll) {
		this.selectedAll = selectedAll;
	}
}
