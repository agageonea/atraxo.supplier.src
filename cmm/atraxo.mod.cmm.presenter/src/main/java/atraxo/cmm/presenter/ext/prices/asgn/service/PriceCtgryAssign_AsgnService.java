/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.prices.asgn.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import atraxo.cmm.business.api.contracts.IContractChangeService;
import atraxo.cmm.business.api.contracts.IMarkedContractService;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.ext.contracts.service.ContractChangeUtil;
import atraxo.cmm.domain.impl.contracts.MarkedContract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.presenter.impl.prices.asgn.model.PriceCtgryAssign_Asgn;
import atraxo.fmbas.domain.impl.fmbas_type.ContractChangeType;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.service.presenter.IAsgnService;
import seava.j4e.presenter.service.asgn.AbstractAsgnService;

/**
 * Custom Service PriceCtgryAssign_AsgnService
 */
public class PriceCtgryAssign_AsgnService extends AbstractAsgnService<PriceCtgryAssign_Asgn, PriceCtgryAssign_Asgn, Object, ContractPriceCategory>
		implements IAsgnService<PriceCtgryAssign_Asgn, PriceCtgryAssign_Asgn, Object> {

	private static final Logger LOG = LoggerFactory.getLogger(PriceCtgryAssign_AsgnService.class);

	@Autowired
	IMarkedContractService markedContractService;
	@Autowired
	IContractPriceCategoryService contractPriceCategoryService;
	@Autowired
	IContractChangeService contractChangeService;

	@Override
	@Transactional
	public void save(String selectionId, String objectId) throws Exception {
		super.save(selectionId, objectId);

		Integer cpcId = Integer.parseInt(objectId);
		ContractPriceCategory cpc = this.contractPriceCategoryService.findById(cpcId);
		Integer contractId = cpc.getContract().getId();
		MarkedContract mc = new MarkedContract();
		mc.setCompute(true);
		mc.setContractId(contractId);

		try {
			this.markedContractService.findByContractId(contractId);
			LOG.warn("Contract already marked or cannot mark it.");
		} catch (ApplicationException e) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(e.getErrorCode())) {
				throw e;
			}
			LOG.warn("Could not find contract for id " + contractId + "! Will insert this new Marked Contract !", e);
			this.markedContractService.insert(mc);
		}

		if (ContractChangeUtil.checkBlueprintProductChanges(cpc.getContract())) {
			ContractPriceCategory oldPriceCategory = this.contractPriceCategoryService.findById(cpc.getId());
			this.contractChangeService.addNewChange(cpc.getContract(), ContractChangeType._PRICE_,
					ContractChangeUtil.buildMessageForPriceCategoryUpdate(oldPriceCategory, cpc));
		}
	}

}
