/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.pricingOverview.model;

import atraxo.cmm.domain.impl.cmm_type.ContractScope;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Contract.class, sort = {@SortField(field = ContractOverview_Ds.F_LOCCODE)})
@RefLookups({@RefLookup(refId = ContractOverview_Ds.F_LOCID),
		@RefLookup(refId = ContractOverview_Ds.F_COUNTERPARTYID),
		@RefLookup(refId = ContractOverview_Ds.F_SETTCURRID),
		@RefLookup(refId = ContractOverview_Ds.F_SETTUNITID),
		@RefLookup(refId = ContractOverview_Ds.F_RESBUYERID),
		@RefLookup(refId = ContractOverview_Ds.F_CONTACTID),
		@RefLookup(refId = ContractOverview_Ds.F_IPLID)})
public class ContractOverview_Ds extends AbstractSubsidiaryDs_Ds<Contract> {

	public static final String ALIAS = "cmm_ContractOverview_Ds";

	public static final String F_LOCID = "locId";
	public static final String F_LOCCODE = "locCode";
	public static final String F_COUNTERPARTYID = "counterPartyId";
	public static final String F_COUNTERPARTYCODE = "counterPartyCode";
	public static final String F_SETTCURRID = "settCurrId";
	public static final String F_SETTCURRCODE = "settCurrCode";
	public static final String F_SETTUNITID = "settUnitId";
	public static final String F_SETTUNITCODE = "settUnitCode";
	public static final String F_PRICE = "price";
	public static final String F_IPLID = "iplId";
	public static final String F_IPLCODE = "iplCode";
	public static final String F_RESBUYERID = "resBuyerId";
	public static final String F_RESBUYERNAME = "resBuyerName";
	public static final String F_CONTACTID = "contactId";
	public static final String F_CONTACTNAME = "contactName";
	public static final String F_DEALTYPE = "dealType";
	public static final String F_CODE = "code";
	public static final String F_TYPE = "type";
	public static final String F_SUBTYPE = "subType";
	public static final String F_SCOPE = "scope";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_PAYMENTTERMS = "paymentTerms";
	public static final String F_CREDITTERMS = "creditTerms";
	public static final String F_STATUS = "status";
	public static final String F_LIMITEDTO = "limitedTo";
	public static final String F_PRODUCT = "product";
	public static final String F_TAX = "tax";

	@DsField(join = "left", path = "location.id")
	private Integer locId;

	@DsField(join = "left", path = "location.code")
	private String locCode;

	@DsField(join = "left", path = "supplier.id")
	private Integer counterPartyId;

	@DsField(join = "left", path = "supplier.code")
	private String counterPartyCode;

	@DsField(join = "left", path = "settlementCurr.id")
	private Integer settCurrId;

	@DsField(join = "left", path = "settlementCurr.code")
	private String settCurrCode;

	@DsField(join = "left", path = "settlementUnit.id")
	private Integer settUnitId;

	@DsField(join = "left", path = "settlementUnit.code")
	private String settUnitCode;

	@DsField(fetch = false)
	private BigDecimal price;

	@DsField(join = "left", path = "intoPlaneAgent.id")
	private Integer iplId;

	@DsField(join = "left", path = "intoPlaneAgent.code")
	private String iplCode;

	@DsField(join = "left", path = "responsibleBuyer.id")
	private String resBuyerId;

	@DsField(join = "left", path = "responsibleBuyer.name")
	private String resBuyerName;

	@DsField(join = "left", path = "contact.id")
	private Integer contactId;

	@DsField(join = "left", fetch = false, path = "contact.fullNameField")
	private String contactName;

	@DsField
	private DealType dealType;

	@DsField
	private String code;

	@DsField
	private ContractType type;

	@DsField
	private ContractSubType subType;

	@DsField
	private ContractScope scope;

	@DsField(jpqlFilter = " e.validTo >= :validFrom ")
	private Date validFrom;

	@DsField(jpqlFilter = " e.validFrom <= :validTo ")
	private Date validTo;

	@DsField
	private Integer paymentTerms;

	@DsField
	private CreditTerm creditTerms;

	@DsField
	private ContractStatus status;

	@DsField
	private FlightTypeIndicator limitedTo;

	@DsField
	private Product product;

	@DsField
	private TaxType tax;

	/**
	 * Default constructor
	 */
	public ContractOverview_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ContractOverview_Ds(Contract e) {
		super(e);
	}

	public Integer getLocId() {
		return this.locId;
	}

	public void setLocId(Integer locId) {
		this.locId = locId;
	}

	public String getLocCode() {
		return this.locCode;
	}

	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}

	public Integer getCounterPartyId() {
		return this.counterPartyId;
	}

	public void setCounterPartyId(Integer counterPartyId) {
		this.counterPartyId = counterPartyId;
	}

	public String getCounterPartyCode() {
		return this.counterPartyCode;
	}

	public void setCounterPartyCode(String counterPartyCode) {
		this.counterPartyCode = counterPartyCode;
	}

	public Integer getSettCurrId() {
		return this.settCurrId;
	}

	public void setSettCurrId(Integer settCurrId) {
		this.settCurrId = settCurrId;
	}

	public String getSettCurrCode() {
		return this.settCurrCode;
	}

	public void setSettCurrCode(String settCurrCode) {
		this.settCurrCode = settCurrCode;
	}

	public Integer getSettUnitId() {
		return this.settUnitId;
	}

	public void setSettUnitId(Integer settUnitId) {
		this.settUnitId = settUnitId;
	}

	public String getSettUnitCode() {
		return this.settUnitCode;
	}

	public void setSettUnitCode(String settUnitCode) {
		this.settUnitCode = settUnitCode;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getIplId() {
		return this.iplId;
	}

	public void setIplId(Integer iplId) {
		this.iplId = iplId;
	}

	public String getIplCode() {
		return this.iplCode;
	}

	public void setIplCode(String iplCode) {
		this.iplCode = iplCode;
	}

	public String getResBuyerId() {
		return this.resBuyerId;
	}

	public void setResBuyerId(String resBuyerId) {
		this.resBuyerId = resBuyerId;
	}

	public String getResBuyerName() {
		return this.resBuyerName;
	}

	public void setResBuyerName(String resBuyerName) {
		this.resBuyerName = resBuyerName;
	}

	public Integer getContactId() {
		return this.contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}

	public String getContactName() {
		return this.contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public DealType getDealType() {
		return this.dealType;
	}

	public void setDealType(DealType dealType) {
		this.dealType = dealType;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ContractType getType() {
		return this.type;
	}

	public void setType(ContractType type) {
		this.type = type;
	}

	public ContractSubType getSubType() {
		return this.subType;
	}

	public void setSubType(ContractSubType subType) {
		this.subType = subType;
	}

	public ContractScope getScope() {
		return this.scope;
	}

	public void setScope(ContractScope scope) {
		this.scope = scope;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public CreditTerm getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(CreditTerm creditTerms) {
		this.creditTerms = creditTerms;
	}

	public ContractStatus getStatus() {
		return this.status;
	}

	public void setStatus(ContractStatus status) {
		this.status = status;
	}

	public FlightTypeIndicator getLimitedTo() {
		return this.limitedTo;
	}

	public void setLimitedTo(FlightTypeIndicator limitedTo) {
		this.limitedTo = limitedTo;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public TaxType getTax() {
		return this.tax;
	}

	public void setTax(TaxType tax) {
		this.tax = tax;
	}
}
