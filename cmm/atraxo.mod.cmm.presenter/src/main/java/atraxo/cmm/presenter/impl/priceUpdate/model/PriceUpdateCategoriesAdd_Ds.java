/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.priceUpdate.model;

import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdateCategories;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import java.math.BigDecimal;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = PriceUpdateCategories.class, jpqlWhere = "e.selected = false")
@RefLookups({
		@RefLookup(refId = PriceUpdateCategoriesAdd_Ds.F_PRICEUPDATEID),
		@RefLookup(refId = PriceUpdateCategoriesAdd_Ds.F_LOCATIONID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceUpdateCategoriesAdd_Ds.F_LOCATIONCODE)}),
		@RefLookup(refId = PriceUpdateCategoriesAdd_Ds.F_CONTRACTHOLDERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceUpdateCategoriesAdd_Ds.F_CONTRACTHOLDERCODE)}),
		@RefLookup(refId = PriceUpdateCategoriesAdd_Ds.F_COUNTERPARTYID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceUpdateCategoriesAdd_Ds.F_COUNTERPARTYCODE)}),
		@RefLookup(refId = PriceUpdateCategoriesAdd_Ds.F_CURRENCYID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceUpdateCategoriesAdd_Ds.F_CURRENCYCODE)}),
		@RefLookup(refId = PriceUpdateCategoriesAdd_Ds.F_UNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceUpdateCategoriesAdd_Ds.F_UNITCODE)})})
public class PriceUpdateCategoriesAdd_Ds
		extends
			AbstractSubsidiaryDs_Ds<PriceUpdateCategories> {

	public static final String ALIAS = "cmm_PriceUpdateCategoriesAdd_Ds";

	public static final String F_PRICEUPDATEID = "priceUpdateId";
	public static final String F_PRICECATEGORYID = "priceCategoryId";
	public static final String F_PRICECATEGORYNAME = "priceCategoryName";
	public static final String F_LOCATIONID = "locationId";
	public static final String F_LOCATIONCODE = "locationCode";
	public static final String F_CONTRACTHOLDERID = "contractHolderId";
	public static final String F_CONTRACTHOLDERCODE = "contractHolderCode";
	public static final String F_COUNTERPARTYID = "counterpartyId";
	public static final String F_COUNTERPARTYCODE = "counterpartyCode";
	public static final String F_CURRENCYID = "currencyId";
	public static final String F_CURRENCYCODE = "currencyCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_CONTRACT = "contract";
	public static final String F_PRICE = "price";
	public static final String F_DEALTYPE = "dealType";
	public static final String F_PRICECATNAME = "priceCatName";
	public static final String F_CONTRACTPRICECATID = "contractPriceCatId";
	public static final String F_SELECTED = "selected";
	public static final String F_LABELFIELD = "labelField";

	@DsField(join = "left", path = "priceUpdate.id")
	private Integer priceUpdateId;

	@DsField(join = "left", path = "priceUpdate.priceCategory.id")
	private Integer priceCategoryId;

	@DsField(join = "left", path = "priceUpdate.priceCategory.name")
	private String priceCategoryName;

	@DsField(join = "left", path = "location.id")
	private Integer locationId;

	@DsField(join = "left", path = "location.code")
	private String locationCode;

	@DsField(join = "left", path = "contractHolder.id")
	private Integer contractHolderId;

	@DsField(join = "left", path = "contractHolder.code")
	private String contractHolderCode;

	@DsField(join = "left", path = "counterparty.id")
	private Integer counterpartyId;

	@DsField(join = "left", path = "counterparty.code")
	private String counterpartyCode;

	@DsField(join = "left", path = "currency.id")
	private Integer currencyId;

	@DsField(join = "left", path = "currency.code")
	private String currencyCode;

	@DsField(join = "left", path = "unit.id")
	private Integer unitId;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	@DsField
	private String contract;

	@DsField
	private BigDecimal price;

	@DsField
	private DealType dealType;

	@DsField
	private String priceCatName;

	@DsField
	private Integer contractPriceCatId;

	@DsField
	private Boolean selected;

	@DsField(fetch = false)
	private String labelField;

	/**
	 * Default constructor
	 */
	public PriceUpdateCategoriesAdd_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public PriceUpdateCategoriesAdd_Ds(PriceUpdateCategories e) {
		super(e);
	}

	public Integer getPriceUpdateId() {
		return this.priceUpdateId;
	}

	public void setPriceUpdateId(Integer priceUpdateId) {
		this.priceUpdateId = priceUpdateId;
	}

	public Integer getPriceCategoryId() {
		return this.priceCategoryId;
	}

	public void setPriceCategoryId(Integer priceCategoryId) {
		this.priceCategoryId = priceCategoryId;
	}

	public String getPriceCategoryName() {
		return this.priceCategoryName;
	}

	public void setPriceCategoryName(String priceCategoryName) {
		this.priceCategoryName = priceCategoryName;
	}

	public Integer getLocationId() {
		return this.locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getLocationCode() {
		return this.locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public Integer getContractHolderId() {
		return this.contractHolderId;
	}

	public void setContractHolderId(Integer contractHolderId) {
		this.contractHolderId = contractHolderId;
	}

	public String getContractHolderCode() {
		return this.contractHolderCode;
	}

	public void setContractHolderCode(String contractHolderCode) {
		this.contractHolderCode = contractHolderCode;
	}

	public Integer getCounterpartyId() {
		return this.counterpartyId;
	}

	public void setCounterpartyId(Integer counterpartyId) {
		this.counterpartyId = counterpartyId;
	}

	public String getCounterpartyCode() {
		return this.counterpartyCode;
	}

	public void setCounterpartyCode(String counterpartyCode) {
		this.counterpartyCode = counterpartyCode;
	}

	public Integer getCurrencyId() {
		return this.currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getContract() {
		return this.contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public DealType getDealType() {
		return this.dealType;
	}

	public void setDealType(DealType dealType) {
		this.dealType = dealType;
	}

	public String getPriceCatName() {
		return this.priceCatName;
	}

	public void setPriceCatName(String priceCatName) {
		this.priceCatName = priceCatName;
	}

	public Integer getContractPriceCatId() {
		return this.contractPriceCatId;
	}

	public void setContractPriceCatId(Integer contractPriceCatId) {
		this.contractPriceCatId = contractPriceCatId;
	}

	public Boolean getSelected() {
		return this.selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}
}
