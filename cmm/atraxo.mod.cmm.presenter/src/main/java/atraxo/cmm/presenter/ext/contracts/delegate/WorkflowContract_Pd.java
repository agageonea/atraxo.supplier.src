package atraxo.cmm.presenter.ext.contracts.delegate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.api.contracts.IContractChangeService;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ext.contracts.service.ContractChangeUtil;
import atraxo.cmm.business.ext.contracts.service.ContractUtil;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.presenter.impl.contracts.model.ContractCustomer_Ds;
import atraxo.cmm.presenter.impl.contracts.model.ContractCustomer_DsParam;
import atraxo.cmm.presenter.impl.contracts.model.Contract_Ds;
import atraxo.cmm.presenter.impl.contracts.model.Contract_DsParam;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.ext.bpm.WorkflowStartResult;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.business.ext.bpm.WorkflowMsgConstants;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.exceptions.workflow.ClaimedWorkflowException;
import atraxo.fmbas.business.ext.util.StringUtil;
import atraxo.fmbas.domain.impl.fmbas_type.ContractChangeType;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.exeptions.J4EPresenterErrorCodes;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author vhojda
 */
public class WorkflowContract_Pd extends AbstractPresenterDelegate {

	private static final String WORKFLOW_BPM_MANAGER = "workflowBpmManager";

	private static final String RESULTS_OF_WORKFLOWS = "Results of workflows:";

	private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowContract_Pd.class);

	/**
	 * Approves current purchase contract workflow task
	 *
	 * @param ds
	 * @param param
	 * @throws Exception
	 */
	public void approvePurchaseContract(Contract_Ds ds, Contract_DsParam param) throws Exception {
		this.completeCurrentTask(ds.getId(), true, param.getApprovalNote(), false);
	}

	/**
	 * Rejects current purchase contract workflow task
	 *
	 * @param ds
	 * @param param
	 * @throws Exception
	 */
	public void rejectPurchaseContract(Contract_Ds ds, Contract_DsParam param) throws Exception {
		this.completeCurrentTask(ds.getId(), false, param.getApprovalNote(), false);
	}

	/**
	 * Approves current sale contract workflow task
	 *
	 * @param ds
	 * @param param
	 * @throws Exception
	 */
	public void approveSaleContract(ContractCustomer_Ds ds, ContractCustomer_DsParam param) throws Exception {
		this.completeCurrentTask(ds.getId(), true, param.getApprovalNote(), true);
	}

	/**
	 * @param ds
	 * @param params
	 * @throws Exception
	 */
	public void approveContractChanges(ContractCustomer_Ds ds, ContractCustomer_DsParam params) throws Exception {
		this.completeCurrentTask(ds.getId(), true, params);
	}

	/**
	 * @param ds
	 * @param params
	 * @throws Exception
	 */
	public void rejectContractChanges(ContractCustomer_Ds ds, ContractCustomer_DsParam params) throws Exception {
		this.completeCurrentTask(ds.getId(), false, params);
	}

	/**
	 * Rejects current sale contract workflow task
	 *
	 * @param ds
	 * @param param
	 * @throws Exception
	 */
	public void rejectSaleContract(ContractCustomer_Ds ds, ContractCustomer_DsParam param) throws Exception {
		this.completeCurrentTask(ds.getId(), false, param.getApprovalNote(), true);
	}

	/**
	 * @param id
	 * @param accept
	 * @param params
	 * @throws Exception
	 */
	private void completeCurrentTask(Integer id, boolean accept, ContractCustomer_DsParam params) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START completeCurrentTask()");
		}

		// check concurrency for system parameter
		ISystemParameterService paramService = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		if (!paramService.getSellContractApprovalWorkflow()) {
			throw new BusinessException(CmmErrorCode.SELL_CONTRACT_WORKFLOW_NO_SYS_PARAM,
					CmmErrorCode.SELL_CONTRACT_WORKFLOW_NO_SYS_PARAM.getErrMsg());
		}

		// get the type of change
		ContractChangeType type;
		try {
			type = ContractChangeType.getByName(params.getContractChangeType());
		} catch (Exception e2) {
			LOGGER.error("Could not extract the contract change type !", e2);
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, "Could not extract the contract change type !", e2);
		}

		// get the contract
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Contract contract = contractService.findById(id);
		if (contract == null) {
			throw new BusinessException(J4EPresenterErrorCodes.ENTITY_NOT_EXISTS, J4EPresenterErrorCodes.ENTITY_NOT_EXISTS.getErrMsg());
		}
		// check concurrency from GUI
		if (type.equals(ContractChangeType._SHIPTO_) && !contract.getShipToApprovalStatus().equals(BidApprovalStatus._AWAITING_APPROVAL_)) {
			throw new BusinessException(CmmErrorCode.SHIPTO_WORKFLOW_APPROVED, CmmErrorCode.SHIPTO_WORKFLOW_APPROVED.getErrMsg());
		} else if (type.equals(ContractChangeType._PERIOD_) && !contract.getPeriodApprovalStatus().equals(BidApprovalStatus._AWAITING_APPROVAL_)) {
			throw new BusinessException(CmmErrorCode.CONTRACT_WORKFLOW_APPROVED, CmmErrorCode.CONTRACT_WORKFLOW_APPROVED.getErrMsg());
		} else if (type.equals(ContractChangeType._PRICE_) && !contract.getPriceApprovalStatus().equals(BidApprovalStatus._AWAITING_APPROVAL_)) {
			throw new BusinessException(CmmErrorCode.PRICE_UPDATE_WORKFLOW_APPROVED, CmmErrorCode.PRICE_UPDATE_WORKFLOW_APPROVED.getErrMsg());
		}

		// check instances
		IWorkflowInstanceEntityService workflowInstanceEntityService = (IWorkflowInstanceEntityService) this
				.findEntityService(WorkflowInstanceEntity.class);

		IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean(WORKFLOW_BPM_MANAGER);

		WorkflowNames wkf = null;
		if (type.equals(ContractChangeType._SHIPTO_)) {
			wkf = WorkflowNames.SALE_CONTRACT_SHIPTO_UPDATE_APPROVAL;
		} else if (type.equals(ContractChangeType._PERIOD_)) {
			wkf = WorkflowNames.SALE_CONTRACT_PERIOD_UPDATE_APPROVAL;
		} else if (type.equals(ContractChangeType._PRICE_)) {
			wkf = WorkflowNames.SALE_CONTRACT_PRICE_CATEGORY_APPROVAL;
		}

		if (wkf != null) {
			WorkflowInstanceEntity workflowInstanceEntity = workflowInstanceEntityService.findByBusinessKey(wkf.getWorkflowName(), contract.getId(),
					contract.getClass().getSimpleName());
			try {
				workflowBpmManager.claimCompleteTask(workflowInstanceEntity.getWorkflowInstance().getId(), accept, params.getApprovalNote());
			} catch (ClaimedWorkflowException e1) {
				throw e1;
			} catch (Exception e) {
				workflowBpmManager.terminateWorkflow(workflowInstanceEntity.getWorkflowInstance().getId(),
						CmmErrorCode.CONTRACT_WORKFLOW_IDENTIFICATION_ERROR.getErrMsg() + ":" + e.getMessage());
				throw new BusinessException(CmmErrorCode.BID_WORKFLOW_IDENTIFICATION_ERROR,
						CmmErrorCode.CONTRACT_WORKFLOW_IDENTIFICATION_ERROR.getErrMsg() + ":" + e.getMessage(), e);
			}
		} else {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, "Invalid value for ContractChangeType, no workflow !");
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END completeCurrentTask()");
		}
	}

	/**
	 * Completes the current task associated with the contract
	 *
	 * @param contractId
	 * @param accept
	 * @param note
	 * @param sale
	 * @throws Exception
	 */
	private void completeCurrentTask(Integer contractId, boolean accept, String note, boolean sale) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START completeCurrentTask()");
		}

		IContractService contractService = this.getApplicationContext().getBean(IContractService.class);
		IWorkflowInstanceEntityService workflowInstanceEntityService = this.getApplicationContext().getBean(IWorkflowInstanceEntityService.class);

		// get the contract
		Contract contract = contractService.findById(contractId);

		// check concurency from GUI
		if (!contract.getStatus().equals(ContractStatus._DRAFT_) || !contract.getBidApprovalStatus().equals(BidApprovalStatus._AWAITING_APPROVAL_)) {
			throw new BusinessException(CmmErrorCode.CONTRACT_WORKFLOW_APPROVED, CmmErrorCode.CONTRACT_WORKFLOW_APPROVED.getErrMsg());
		}

		IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean(WORKFLOW_BPM_MANAGER);

		WorkflowInstanceEntity workflowInstanceEntity = workflowInstanceEntityService.findByBusinessKey(
				sale ? WorkflowNames.SELL_CONTRACT_APPROVAL.getWorkflowName() : WorkflowNames.PURCHASE_CONTRACT_APPROVAL.getWorkflowName(),
				contract.getId(), contract.getClass().getSimpleName());
		try {
			workflowBpmManager.claimCompleteTask(workflowInstanceEntity.getWorkflowInstance().getId(), accept, note);
		} catch (ClaimedWorkflowException e1) {
			throw e1;
		} catch (Exception e) {
			workflowBpmManager.terminateWorkflow(workflowInstanceEntity.getWorkflowInstance().getId(),
					CmmErrorCode.CONTRACT_WORKFLOW_IDENTIFICATION_ERROR.getErrMsg() + ":" + e.getMessage());
			throw new BusinessException(CmmErrorCode.CONTRACT_WORKFLOW_IDENTIFICATION_ERROR,
					CmmErrorCode.CONTRACT_WORKFLOW_IDENTIFICATION_ERROR.getErrMsg() + ":" + e.getMessage(), e);
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END completeCurrentTask()");
		}
	}

	/**
	 * @param ds
	 * @param params
	 * @throws Exception
	 */
	public void submitForApprovalSaleContract(ContractCustomer_Ds ds, ContractCustomer_DsParam params) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START submitForApprovalSaleContract()");
		}

		ContractDsParam contractDsParam = new ContractDsParam(params);
		this.submitForApproval(ds.getId(), contractDsParam, true);

		params.setSubmitForApprovalResult(contractDsParam.getSubmitForApprovalResult());
		params.setSubmitForApprovalDescription(contractDsParam.getSubmitForApprovalDescription());

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END submitForApprovalSaleContract()");
		}
	}

	/**
	 * @param ds
	 * @param params
	 * @throws Exception
	 */
	public void submitForApprovalPurchaseContract(Contract_Ds ds, Contract_DsParam params) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START submitForApprovalPurchaseContract()");
		}

		ContractDsParam contractDsParam = new ContractDsParam(params);
		this.submitForApproval(ds.getId(), contractDsParam, false);

		params.setSubmitForApprovalResult(contractDsParam.getSubmitForApprovalResult());
		params.setSubmitForApprovalDescription(contractDsParam.getSubmitForApprovalDescription());

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END submitForApprovalPurchaseContract()");
		}
	}

	/**
	 * Submits for approval
	 *
	 * @param dsId
	 * @param params
	 * @param sellContract
	 * @throws Exception
	 */
	private void submitForApproval(Integer dsId, ContractDsParam params, boolean sellContract) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START submitForApproval()");
		}
		// assume false, ( the operation didn't complete ok )
		params.setSubmitForApprovalResult(false);

		// first, check the 'Use sell/purchase contract approval workflow' system parameter
		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		boolean useWorkflow = sellContract ? paramSrv.getSellContractApprovalWorkflow() : paramSrv.getPurchaseContractApprovalWorkflow();
		if (!useWorkflow) {
			params.setSubmitForApprovalDescription(sellContract ? CmmErrorCode.SELL_CONTRACT_WORKFLOW_NO_SYS_PARAM.getErrMsg()
					: CmmErrorCode.PURCHASE_CONTRACT_WORKFLOW_NO_SYS_PARAM.getErrMsg());
			return;
		}

		// if ok, proceed forward and retrieve the contract object from DB
		IContractService contractService = this.getApplicationContext().getBean(IContractService.class);
		Contract contract = null;
		try {
			contract = contractService.findByBusiness(dsId);
		} catch (Exception e) {
			LOGGER.warn("WARNING:could not locate Contract for ID " + dsId, e);
			params.setSubmitForApprovalDescription(String.format(CmmErrorCode.CONTRACT_WORKFLOW_ENTITY_NOT_EXISTING.getErrMsg(), dsId.toString()));
		}

		// if object exists, proceed
		if (contract != null) {
			// check if contract has awarded volume set SONE-5908
			if (contract.getAwardedVolume() != null) {
				// check for concurrency (maybe object changed in the meantime)
				if (contract.getStatus().equals(ContractStatus._DRAFT_)) {
					if (ContractUtil.alreadyApprovedOrInApprovalProcess(contract)) {
						params.setSubmitForApprovalDescription(CmmErrorCode.CONTRACT_WORKFLOW_CONCURRENCY_NOT_START_ERROR.getErrMsg());
					} else {
						this.submitForApprovalNewContract(contract, params, sellContract);
					}
				} else {
					this.submitForApprovalExistingContract(contract, params);
				}
			} else {
				params.setSubmitForApprovalDescription(CmmErrorCode.CONTRACT_WORKFLOW_AWARDED_VOLUME_NOT_SET.getErrMsg());
			}
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END submitForApproval()");
		}
	}

	/**
	 * Submits for approval a new contract in draft form.
	 *
	 * @param contract
	 * @param params
	 * @param sellContract
	 * @throws BusinessException
	 */
	private void submitForApprovalNewContract(Contract contract, ContractDsParam params, boolean sellContract) throws BusinessException {
		IContractService contractService = this.getApplicationContext().getBean(IContractService.class);

		contractService.checkIfCanSendForAppoval(contract);

		IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean(WORKFLOW_BPM_MANAGER);

		// submit for approval for the entity(change status, fill in History)
		contractService.submitForApproval(contract, BidApprovalStatus._AWAITING_APPROVAL_, WorkflowMsgConstants.MSG_STARTED_FOR_APPROVAL_OK,
				params.getApprovalNote());

		// start workflow after adding workflow parameters
		Map<String, Object> vars = this.getWorkflowVariablesMap(params, contract);
		WorkflowStartResult result = workflowBpmManager.startWorkflow(
				sellContract ? WorkflowNames.SELL_CONTRACT_APPROVAL.getWorkflowName() : WorkflowNames.PURCHASE_CONTRACT_APPROVAL.getWorkflowName(),
				vars, contract.getSubsidiaryId(), contract);
		if (result.isStarted()) {
			params.setSubmitForApprovalResult(true);
		} else {
			params.setSubmitForApprovalDescription(CmmErrorCode.CONTRACT_WORKFLOW_NOT_START_REASON.getErrMsg() + result.getReason());

			// submit for approval for the entity(change status, fill in History)
			contractService.submitForApproval(contractService.findById(contract.getId()), BidApprovalStatus._REJECTED_,
					WorkflowMsgConstants.MSG_STARTED_FOR_APPROVAL_NOT_OK, result.getReason());
		}
	}

	/**
	 * Submits for approval an existing contract.
	 *
	 * @param contract
	 * @param params
	 * @throws BusinessException
	 */
	private void submitForApprovalExistingContract(Contract contract, ContractDsParam params) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START submitForApprovalExistingContract()");
		}

		List<String> workflowResultsMessages = new ArrayList<>();

		boolean periodWkfOk = this.startSubmitForApproval(contract, params, ContractChangeType._PERIOD_, workflowResultsMessages);
		boolean shipToWkfOk = this.startSubmitForApproval(contract, params, ContractChangeType._SHIPTO_, workflowResultsMessages);
		boolean priceWkfOk = this.startSubmitForApproval(contract, params, ContractChangeType._PRICE_, workflowResultsMessages);

		// fill in the result
		if (!workflowResultsMessages.isEmpty()) {
			params.setSubmitForApprovalDescription(RESULTS_OF_WORKFLOWS + StringUtil.unorderedList(workflowResultsMessages.toArray()));
		}
		params.setSubmitForApprovalResult(periodWkfOk && shipToWkfOk && priceWkfOk);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END submitForApprovalExistingContract()");
		}
	}

	/**
	 * @param contract
	 * @param params
	 * @param changeType
	 * @param workflowResultsMessages
	 * @return
	 * @throws BusinessException
	 */
	private boolean startSubmitForApproval(Contract contract, ContractDsParam params, ContractChangeType changeType,
			List<String> workflowResultsMessages) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START startSubmitForApproval()");
		}

		boolean wkfOk = false;

		IContractChangeService contractChangeService = this.getApplicationContext().getBean(IContractChangeService.class);

		boolean periodChanged = !contractChangeService.getContractChanges(contract, changeType).isEmpty();
		if (periodChanged) {
			try {
				WorkflowStartResult resultWkf;

				if (changeType.equals(ContractChangeType._PERIOD_)) {
					resultWkf = this.startSubmitForApprovalSaleContractPeriod(contract, params);
				} else if (changeType.equals(ContractChangeType._PRICE_)) {
					resultWkf = this.startSubmitForApprovalSaleContractPrice(contract, params);
				} else if (changeType.equals(ContractChangeType._SHIPTO_)) {
					resultWkf = this.startSubmitForApprovalSaleContractShipTo(contract, params);
				} else {
					resultWkf = new WorkflowStartResult();
				}

				if (resultWkf.isStarted()) {
					wkfOk = true;
				} else {
					workflowResultsMessages.add(this.formatResultMessage(changeType.getName(), resultWkf.getReason()));
				}
			} catch (BusinessException e) {
				LOGGER.info("GOT known BusinessException, will set the result as NOT OK !", e);
				workflowResultsMessages.add(this.formatResultMessage(changeType.getName(), e.getMessage()));
			}
		} else {
			wkfOk = true;
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END startSubmitForApproval()");
		}
		return wkfOk;
	}

	/**
	 * @param contract
	 * @param params
	 * @return
	 * @throws BusinessException
	 */
	private WorkflowStartResult startSubmitForApprovalSaleContractPeriod(Contract contract, ContractDsParam params) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START startSubmitForApprovalSaleContractPeriod()");
		}

		IContractService contractService = this.getApplicationContext().getBean(IContractService.class);

		if (!this.isPeriodValid(contract.getValidFrom(), contract.getValidTo())) {
			throw new BusinessException(CmmErrorCode.PERIOD_WKF_INVALID, CmmErrorCode.PERIOD_WKF_INVALID.getErrMsg());
		}

		// submit for approval for the entity(change status, fill in History)
		contractService.submitForApprovalPeriod(contractService.findById(contract.getId()), BidApprovalStatus._AWAITING_APPROVAL_,
				WorkflowMsgConstants.MSG_STARTED_FOR_PERIOD_APPROVAL_OK, params.getApprovalNote());

		// start workflow after adding workflow parameters
		Map<String, Object> vars = this.getWorkflowVariablesMap(params, contract, ContractChangeType._PERIOD_);

		IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean(WORKFLOW_BPM_MANAGER);
		WorkflowStartResult result = workflowBpmManager.startWorkflow(WorkflowNames.SALE_CONTRACT_PERIOD_UPDATE_APPROVAL.getWorkflowName(), vars,
				contract.getSubsidiaryId(), contract);

		if (result.isStarted()) {
			// everything is OK, so set the result as success
			result.setStarted(true);

		} else {
			result.setReason(CmmErrorCode.CONTRACT_WORKFLOW_NOT_START_REASON.getErrMsg() + result.getReason());
			// submit for approval for the entity(change status, fill in History)
			contractService.submitForApprovalPeriod(contractService.findById(contract.getId()), BidApprovalStatus._REJECTED_,
					WorkflowMsgConstants.MSG_STARTED_FOR_PERIOD_APPROVAL_NOT_OK, result.getReason());
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END startSubmitForApprovalSaleContractPeriod()");
		}

		return result;
	}

	/**
	 * @param contract
	 * @param params
	 * @return
	 * @throws BusinessException
	 */
	private WorkflowStartResult startSubmitForApprovalSaleContractShipTo(Contract contract, ContractDsParam params) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START startSubmitForApprovalSaleContractShipTo()");
		}
		IContractService contractService = this.getApplicationContext().getBean(IContractService.class);

		// start workflow
		IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean(WORKFLOW_BPM_MANAGER);

		// submit for approval for the entity(change status, fill in History)
		contractService.submitForApprovalShipTo(contractService.findById(contract.getId()), BidApprovalStatus._AWAITING_APPROVAL_,
				WorkflowMsgConstants.MSG_STARTED_FOR_SHIP_TO_APPROVAL_OK, params.getApprovalNote());

		// start workflow after adding workflow parameters
		Map<String, Object> vars = this.getWorkflowVariablesMap(params, contract, ContractChangeType._SHIPTO_);
		WorkflowStartResult result = workflowBpmManager.startWorkflow(WorkflowNames.SALE_CONTRACT_SHIPTO_UPDATE_APPROVAL.getWorkflowName(), vars,
				contract.getSubsidiaryId(), contract);

		if (result.isStarted()) {
			// everything is OK, so set the result as success
			result.setStarted(true);

		} else {
			result.setReason(CmmErrorCode.SHIPTO_WORKFLOW_NOT_START_REASON.getErrMsg() + result.getReason());

			// submit for approval for the entity(change status, fill in History)
			contractService.submitForApprovalShipTo(contractService.findById(contract.getId()), BidApprovalStatus._REJECTED_,
					WorkflowMsgConstants.MSG_STARTED_FOR_SHIP_TO_APPROVAL_NOT_OK, result.getReason());
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END startSubmitForApprovalSaleContractShipTo()");
		}

		return result;
	}

	/**
	 * @param contract
	 * @param params
	 * @throws BusinessException
	 */
	private WorkflowStartResult startSubmitForApprovalSaleContractPrice(Contract contract, ContractDsParam params) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START startSubmitForApprovalSaleContractPrice()");
		}
		IContractService contractService = this.getApplicationContext().getBean(IContractService.class);
		contractService.checkIfCanSendForAppoval(contract);

		IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean(WORKFLOW_BPM_MANAGER);

		// submit for approval for the entity(change status, fill in History)
		contractService.submitForApprovalPrice(contractService.findById(contract.getId()), BidApprovalStatus._AWAITING_APPROVAL_,
				WorkflowMsgConstants.MSG_STARTED_FOR_PRICE_APPROVAL_OK, params.getApprovalNote());

		// start workflow after adding workflow parameters
		Map<String, Object> vars = this.getWorkflowVariablesMap(params, contract, ContractChangeType._PRICE_);
		WorkflowStartResult result = workflowBpmManager.startWorkflow(WorkflowNames.SALE_CONTRACT_PRICE_CATEGORY_APPROVAL.getWorkflowName(), vars,
				contract.getSubsidiaryId(), contract);
		if (result.isStarted()) {
			// everything is OK, so set the result as success
			result.setStarted(true);
		} else {
			result.setReason(CmmErrorCode.CONTRACT_WORKFLOW_NOT_START_REASON.getErrMsg() + result.getReason());

			// submit for approval for the entity(change status, fill in History)
			contractService.submitForApprovalPrice(contractService.findById(contract.getId()), BidApprovalStatus._REJECTED_,
					WorkflowMsgConstants.MSG_STARTED_FOR_PRICE_APPROVAL_NOT_OK, result.getReason());
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END startSubmitForApprovalSaleContractPrice()");
		}

		return result;
	}

	/**
	 * @param msg
	 * @param reason
	 * @return
	 */
	private String formatResultMessage(String msg, String reason) {
		StringBuilder sb = new StringBuilder();
		sb.append("<b>");
		sb.append(msg);
		sb.append("</b>");
		sb.append(" : <span style='color:red'>");
		sb.append("Failure");
		sb.append(" : </span>");
		sb.append(reason);
		return sb.toString();
	}

	/**
	 * @param validFrom
	 * @param validTo
	 * @return
	 */
	private boolean isPeriodValid(Date validFrom, Date validTo) {
		return validTo.compareTo(validFrom) >= 0;
	}

	/**
	 * @param params
	 * @param contract
	 * @return
	 */
	private Map<String, Object> getWorkflowVariablesMap(ContractDsParam params, Contract contract) {
		Map<String, Object> vars = new HashMap<>();

		String description = "";

		if (contract.getDealType().equals(DealType._BUY_)) {
			description = "Purchase Contract " + contract.getCode() + " for "
					+ (contract.getLocation() != null ? contract.getLocation().getName() : "") + " with "
					+ (contract.getSupplier() != null ? contract.getSupplier().getName() : "") + " awaiting your approval";
		} else if (contract.getDealType().equals(DealType._SELL_)) {
			description = "Sales Contract " + contract.getCode() + " for " + (contract.getLocation() != null ? contract.getLocation().getName() : "")
					+ " with " + (contract.getCustomer() != null ? contract.getCustomer().getName() : "") + " awaiting your approval";
		}

		vars.put(WorkflowVariablesConstants.VAR_WKF_ENTITY_DESCRIPTION, description);
		vars.put(WorkflowVariablesConstants.CONTRACT_ID_VAR, contract.getId() + "");
		vars.put(WorkflowVariablesConstants.APPROVE_NOTE, params.getApprovalNote());
		vars.put(WorkflowVariablesConstants.FULLNAME_REQUESTER_VAR, this.getFullNameOfRequester());

		String attachmentsArray = params.getSelectedAttachments();
		if (!StringUtils.isEmpty(attachmentsArray)) {
			String attachTrimmed = attachmentsArray.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "").trim();
			if (!StringUtils.isEmpty(attachTrimmed)) {
				String[] attachmentIdentifiers = attachTrimmed.split(",");
				vars.put(WorkflowVariablesConstants.VAR_WKF_EMAIL_ATTACHMENTS, Arrays.asList(attachmentIdentifiers));
			}
		}
		return vars;
	}

	/**
	 * @param params
	 * @param contract
	 * @param changeType
	 * @return
	 */
	private Map<String, Object> getWorkflowVariablesMap(ContractDsParam params, Contract contract, ContractChangeType changeType) {
		Map<String, Object> vars = new HashMap<>();

		String description = "";

		String contractCode = ContractChangeUtil.checkContractCode(contract.getCode());
		String contractLocation = contract.getLocation() != null ? contract.getLocation().getName() : "";
		String customerName = contract.getCustomer() != null ? contract.getCustomer().getName() : "";

		if (changeType.equals(ContractChangeType._SHIPTO_)) {
			description = String.format(CmmErrorCode.WKF_CONTRACT_CHANGE_SHIP_TO_SUBJECT.getErrMsg(), contractCode, contractLocation, customerName);
		} else if (changeType.equals(ContractChangeType._PERIOD_)) {
			description = String.format(CmmErrorCode.WKF_CONTRACT_CHANGE_PERIOD_SUBJECT.getErrMsg(), contractCode, contractLocation, customerName);
		} else if (changeType.equals(ContractChangeType._PRICE_)) {
			description = String.format(CmmErrorCode.WKF_CONTRACT_CHANGE_PRICE_SUBJECT.getErrMsg(), contractCode, contractLocation, customerName);
		}

		vars.put(WorkflowVariablesConstants.VAR_WKF_ENTITY_DESCRIPTION, description);
		vars.put(WorkflowVariablesConstants.CONTRACT_ID_VAR, contract.getId() + "");
		vars.put(WorkflowVariablesConstants.APPROVE_NOTE, params.getApprovalNote());
		vars.put(WorkflowVariablesConstants.FULLNAME_REQUESTER_VAR, this.getFullNameOfRequester());

		String attachmentsArray = params.getSelectedAttachments();
		if (!StringUtils.isEmpty(attachmentsArray)) {
			String attachTrimmed = attachmentsArray.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "").trim();
			if (!StringUtils.isEmpty(attachTrimmed)) {
				String[] attachmentIdentifiers = attachTrimmed.split(",");
				vars.put(WorkflowVariablesConstants.VAR_WKF_EMAIL_ATTACHMENTS, Arrays.asList(attachmentIdentifiers));
			}
		}
		return vars;
	}

	/**
	 * @return
	 */
	private String getFullNameOfRequester() {
		IUserSuppService userService = this.getApplicationContext().getBean(IUserSuppService.class);
		UserSupp user = userService.findByLogin(Session.user.get().getLoginName());
		return user.getFirstName() + " " + user.getLastName();
	}

	/**
	 * Private class for wrapping over <code>Contract_DsParam</code> and also <code>ContractCustomer_DsParam</code>
	 *
	 * @author vhojda
	 */
	private class ContractDsParam {
		public String submitForApprovalDescription;
		public Boolean submitForApprovalResult;
		public String selectedAttachments;
		public String approvalNote;

		public ContractDsParam(Contract_DsParam params) {
			this.approvalNote = params.getApprovalNote();
			this.selectedAttachments = params.getSelectedAttachments();
		}

		public ContractDsParam(ContractCustomer_DsParam params) {
			this.approvalNote = params.getApprovalNote();
			this.selectedAttachments = params.getSelectedAttachments();
		}

		public String getSubmitForApprovalDescription() {
			return this.submitForApprovalDescription;
		}

		public void setSubmitForApprovalDescription(String submitForApprovalDescription) {
			this.submitForApprovalDescription = submitForApprovalDescription;
		}

		public Boolean getSubmitForApprovalResult() {
			return this.submitForApprovalResult;
		}

		public void setSubmitForApprovalResult(Boolean submitForApprovalResult) {
			this.submitForApprovalResult = submitForApprovalResult;
		}

		public String getSelectedAttachments() {
			return this.selectedAttachments;
		}

		@SuppressWarnings("unused")
		public void setSelectedAttachments(String selectedAttachments) {
			this.selectedAttachments = selectedAttachments;
		}

		public String getApprovalNote() {
			return this.approvalNote;
		}

		@SuppressWarnings("unused")
		public void setApprovalNote(String approvalNote) {
			this.approvalNote = approvalNote;
		}

	}

}
