/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.pricingOverview.qb;

import atraxo.cmm.presenter.impl.pricingOverview.model.ContractOverview_Ds;
import atraxo.cmm.presenter.impl.pricingOverview.model.ContractOverview_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class ContractOverview_DsQb
		extends
			QueryBuilderWithJpql<ContractOverview_Ds, ContractOverview_Ds, ContractOverview_DsParam> {
}
