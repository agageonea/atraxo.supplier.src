/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.prices.service;

import atraxo.cmm.domain.impl.cmm_type.RestrictionTypes;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.cmm.presenter.impl.prices.model.ContractPriceRestriction_Ds;
import atraxo.cmm.presenter.impl.prices.model.ContractPriceRestriction_DsParam;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ContractPriceRestriction_DsService extends
		AbstractEntityDsService<ContractPriceRestriction_Ds, ContractPriceRestriction_Ds, ContractPriceRestriction_DsParam, ContractPriceRestriction>
		implements IDsService<ContractPriceRestriction_Ds, ContractPriceRestriction_Ds, ContractPriceRestriction_DsParam> {

	@Override
	protected void preInsert(ContractPriceRestriction_Ds ds, ContractPriceRestriction e, ContractPriceRestriction_DsParam params) throws Exception {
		super.preInsert(ds, e, params);
		if (ds.getRestrictionType().equals(RestrictionTypes._TIME_OF_OPERATION_)) {
			IUnitService unitsSrv = (IUnitService) this.findEntityService(Unit.class);
			Unit unit = unitsSrv.findByCode("HR");
			ds.setUnitCode("HR");
			ds.setUnit(unit.getId());
		}
	}

	@Override
	protected void preUpdate(ContractPriceRestriction_Ds ds, ContractPriceRestriction_DsParam params) throws Exception {
		super.preUpdate(ds, params);
		if (ds.getRestrictionType().equals(RestrictionTypes._TIME_OF_OPERATION_)) {
			IUnitService unitsSrv = (IUnitService) this.findEntityService(Unit.class);
			Unit unit = unitsSrv.findByCode("HR");
			ds.setUnitCode("HR");
			ds.setUnit(unit.getId());
		}
	}

}
