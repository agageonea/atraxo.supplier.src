/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.tender.model;

import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = TenderLocationRound.class)
@RefLookups({@RefLookup(refId = TenderLocationRound_Ds.F_TENDERLOCATIONID)})
public class TenderLocationRound_Ds extends AbstractDs_Ds<TenderLocationRound> {

	public static final String ALIAS = "cmm_TenderLocationRound_Ds";

	public static final String F_BIDDINGFROM = "biddingFrom";
	public static final String F_BIDDINGTO = "biddingTo";
	public static final String F_ROUNDNO = "roundNo";
	public static final String F_BIDOPENING = "bidOpening";
	public static final String F_BIDROUNDACTIVE = "bidRoundActive";
	public static final String F_TENDERLOCATIONID = "tenderLocationId";

	@DsField
	private Date biddingFrom;

	@DsField
	private Date biddingTo;

	@DsField
	private Integer roundNo;

	@DsField
	private Date bidOpening;

	@DsField(path = "active")
	private Boolean bidRoundActive;

	@DsField(join = "left", path = "tenderLocation.id")
	private Integer tenderLocationId;

	/**
	 * Default constructor
	 */
	public TenderLocationRound_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public TenderLocationRound_Ds(TenderLocationRound e) {
		super(e);
	}

	public Date getBiddingFrom() {
		return this.biddingFrom;
	}

	public void setBiddingFrom(Date biddingFrom) {
		this.biddingFrom = biddingFrom;
	}

	public Date getBiddingTo() {
		return this.biddingTo;
	}

	public void setBiddingTo(Date biddingTo) {
		this.biddingTo = biddingTo;
	}

	public Integer getRoundNo() {
		return this.roundNo;
	}

	public void setRoundNo(Integer roundNo) {
		this.roundNo = roundNo;
	}

	public Date getBidOpening() {
		return this.bidOpening;
	}

	public void setBidOpening(Date bidOpening) {
		this.bidOpening = bidOpening;
	}

	public Boolean getBidRoundActive() {
		return this.bidRoundActive;
	}

	public void setBidRoundActive(Boolean bidRoundActive) {
		this.bidRoundActive = bidRoundActive;
	}

	public Integer getTenderLocationId() {
		return this.tenderLocationId;
	}

	public void setTenderLocationId(Integer tenderLocationId) {
		this.tenderLocationId = tenderLocationId;
	}
}
