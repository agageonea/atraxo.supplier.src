/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.tender.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.tender.ITenderService;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.presenter.impl.tender.model.Tender_Ds;
import atraxo.cmm.presenter.impl.tender.model.Tender_DsParam;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.history.ChangeHistory;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.api.session.IOrganization;
import seava.j4e.api.session.Session;
import seava.j4e.commons.utils.BigDecimalFormater;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service Tender_DsService
 *
 * @param <T>
 */
public class Tender_DsService extends AbstractEntityDsService<Tender_Ds, Tender_Ds, Tender_DsParam, Tender>
		implements IDsService<Tender_Ds, Tender_Ds, Tender_DsParam> {

	private static final Logger LOG = LoggerFactory.getLogger(Tender_DsService.class);

	@Autowired
	private ISystemParameterService systemParamsService;

	@Autowired
	private ICustomerService customerService;

	@Override
	protected void postInsertAfterModel(Tender_Ds ds, Tender e, Tender_DsParam params) throws Exception {
		super.postInsertAfterModel(ds, e, params);

		IChangeHistoryService historyService = (IChangeHistoryService) this.findEntityService(ChangeHistory.class);

		ChangeHistory history = new ChangeHistory();
		history.setObjectType(Tender.class.getSimpleName());
		history.setObjectId(e.getId());
		history.setObjectValue(e.getSource().getName());
		history.setRemarks("New Tender Invitation");

		historyService.insert(history);
	}

	@Override
	protected void postFind(IQueryBuilder<Tender_Ds, Tender_Ds, Tender_DsParam> builder, List<Tender_Ds> result) throws Exception {
		super.postFind(builder, result);
		this.getTotalVolume(result);
	}

	protected void getTotalVolume(List<Tender_Ds> result) throws Exception {
		ITenderService service = (ITenderService) this.findEntityService(Tender.class);

		for (Tender_Ds ds : result) {
			Tender tender = ds._getEntity_();
			if (tender != null) {
				try {
					BigDecimal volume = service.getTotalVolume(tender);
					this.setTotalVolume(ds, volume);
				} catch (BusinessException e) {
					if (LOG.isDebugEnabled()) {
						LOG.debug(e.getMessage(), e);
					}
				}
			}
		}
	}

	private void setTotalVolume(Tender_Ds ds, BigDecimal volume) throws BusinessException {
		String formatedVolume = BigDecimalFormater.dynamicNumberFormat(volume, this.systemParamsService.getDecimalsForUnit());

		StringBuilder total = new StringBuilder();
		total.append(formatedVolume);

		Map<String, Object> subsidiaryParams = new HashMap<>();
		subsidiaryParams.put("refid", Session.user.get().getClient().getActiveSubsidiaryId());

		String volumeUnit;
		Customer subsidiary = this.customerService.findEntityByAttributes(subsidiaryParams);
		if (subsidiary.getDefaultVolumeUnit() != null) {
			volumeUnit = subsidiary.getDefaultVolumeUnit().getCode();
			total.append(" ").append(subsidiary.getDefaultVolumeUnit().getCode());
		} else {
			volumeUnit = this.systemParamsService.getSysVol();
			total.append(" ").append(this.systemParamsService.getSysVol());
		}

		ds.setTotalVolume(volume);
		ds.setVolumeUnit(volumeUnit);
		ds.setTotalVolumeHeader(total.toString());

		// set subsidiary code
		IOrganization org = Session.user.get().getProfile().getOrganizations().stream().filter(o -> ds.getSubsidiaryId().equals(o.getId()))
				.findFirst().orElse(null);

		if (org != null) {
			ds.setSubsidiaryCode(org.getCode());
		}
	}
}
