/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.prices.model;

import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = AverageMethod.class, jpqlWhere = "e.id IN (SELECT q.avgMethodIndicator.id FROM Quotation q)", sort = {@SortField(field = UsedAverageMethodsLov_Ds.F_NAME)})
public class UsedAverageMethodsLov_Ds extends AbstractLov_Ds<AverageMethod> {

	public static final String ALIAS = "cmm_UsedAverageMethodsLov_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";

	@DsField
	private String code;

	@DsField
	private String name;

	/**
	 * Default constructor
	 */
	public UsedAverageMethodsLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public UsedAverageMethodsLov_Ds(AverageMethod e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
