/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.contracts.qb;

import atraxo.cmm.presenter.impl.contracts.model.PricingBaseGrouping_Ds;
import atraxo.cmm.presenter.impl.contracts.model.PricingBaseGrouping_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class PricingBaseGrouping_DsQb
		extends
			QueryBuilderWithJpql<PricingBaseGrouping_Ds, PricingBaseGrouping_Ds, PricingBaseGrouping_DsParam> {
}
