/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.contracts.model;

import atraxo.fmbas.domain.impl.fmbas_type.ValidationMessageSeverity;
import atraxo.fmbas.domain.impl.validationMessage.ValidationMessage;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ValidationMessage.class, sort = {@SortField(field = ValidationMessageBid_Ds.F_CREATEDAT)})
public class ValidationMessageBid_Ds extends AbstractDs_Ds<ValidationMessage> {

	public static final String ALIAS = "cmm_ValidationMessageBid_Ds";

	public static final String F_OBJECTID = "objectId";
	public static final String F_OBJECTTYPE = "objectType";
	public static final String F_MESSAGE = "message";
	public static final String F_SEVERITY = "severity";
	public static final String F_LOCATION = "location";
	public static final String F_TENDERNAME = "tenderName";
	public static final String F_BIDVERSION = "bidVersion";
	public static final String F_BIDREVISION = "bidRevision";
	public static final String F_SUCCESFULLYSUBMITEDBIDSLABEL = "succesfullySubmitedBidsLabel";
	public static final String F_CONCURENCYCHECKFAILEDBIDSLABEL = "concurencyCheckFailedBidsLabel";
	public static final String F_VALIDATIONERRORSBIDSLABEL = "validationErrorsBidsLabel";

	@DsField
	private String objectId;

	@DsField
	private String objectType;

	@DsField
	private String message;

	@DsField
	private ValidationMessageSeverity severity;

	@DsField(fetch = false)
	private String location;

	@DsField(fetch = false)
	private String tenderName;

	@DsField(fetch = false)
	private String bidVersion;

	@DsField(fetch = false)
	private Integer bidRevision;

	@DsField(fetch = false)
	private String succesfullySubmitedBidsLabel;

	@DsField(fetch = false)
	private String concurencyCheckFailedBidsLabel;

	@DsField(fetch = false)
	private String validationErrorsBidsLabel;

	/**
	 * Default constructor
	 */
	public ValidationMessageBid_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ValidationMessageBid_Ds(ValidationMessage e) {
		super(e);
	}

	public String getObjectId() {
		return this.objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ValidationMessageSeverity getSeverity() {
		return this.severity;
	}

	public void setSeverity(ValidationMessageSeverity severity) {
		this.severity = severity;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTenderName() {
		return this.tenderName;
	}

	public void setTenderName(String tenderName) {
		this.tenderName = tenderName;
	}

	public String getBidVersion() {
		return this.bidVersion;
	}

	public void setBidVersion(String bidVersion) {
		this.bidVersion = bidVersion;
	}

	public Integer getBidRevision() {
		return this.bidRevision;
	}

	public void setBidRevision(Integer bidRevision) {
		this.bidRevision = bidRevision;
	}

	public String getSuccesfullySubmitedBidsLabel() {
		return this.succesfullySubmitedBidsLabel;
	}

	public void setSuccesfullySubmitedBidsLabel(
			String succesfullySubmitedBidsLabel) {
		this.succesfullySubmitedBidsLabel = succesfullySubmitedBidsLabel;
	}

	public String getConcurencyCheckFailedBidsLabel() {
		return this.concurencyCheckFailedBidsLabel;
	}

	public void setConcurencyCheckFailedBidsLabel(
			String concurencyCheckFailedBidsLabel) {
		this.concurencyCheckFailedBidsLabel = concurencyCheckFailedBidsLabel;
	}

	public String getValidationErrorsBidsLabel() {
		return this.validationErrorsBidsLabel;
	}

	public void setValidationErrorsBidsLabel(String validationErrorsBidsLabel) {
		this.validationErrorsBidsLabel = validationErrorsBidsLabel;
	}
}
