/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.tender.service;

import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.presenter.impl.tender.model.OpenTenderLocation_Ds;
import atraxo.cmm.presenter.impl.tender.model.OpenTenderLocation_DsParam;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service OpenTenderLocation_DsService
 */
public class OpenTenderLocation_DsService
		extends
			AbstractEntityDsService<OpenTenderLocation_Ds, OpenTenderLocation_Ds, OpenTenderLocation_DsParam, TenderLocation>
		implements
			IDsService<OpenTenderLocation_Ds, OpenTenderLocation_Ds, OpenTenderLocation_DsParam> {

	// Implement me ...

}
