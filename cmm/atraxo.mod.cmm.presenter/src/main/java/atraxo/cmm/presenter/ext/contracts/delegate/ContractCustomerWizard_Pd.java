package atraxo.cmm.presenter.ext.contracts.delegate;

import java.util.Date;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.presenter.ext.contracts.service.IConversionFactorValidator;
import atraxo.cmm.presenter.impl.contracts.model.ContractCustomerWizard_Ds;
import atraxo.cmm.presenter.impl.contracts.model.ContractCustomerWizard_DsParam;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.masteragreements.MasterAgreement;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author zspeter
 */
public class ContractCustomerWizard_Pd extends AbstractPresenterDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContractCustomerWizard_Pd.class);

	public void changeUnit(ContractCustomerWizard_Ds ds) throws Exception {
		IConversionFactorValidator srv = (IConversionFactorValidator) this.getApplicationContext().getBean("conversionFactorValidator");
		srv.validateContractCustomerWizard(ds);
	}

	/**
	 * @param contract
	 * @param param
	 * @throws Exception
	 */
	public void setDefaulPaymentTerms(ContractCustomerWizard_Ds contract, ContractCustomerWizard_DsParam param) throws Exception {
		ICustomerService custService = (ICustomerService) this.findEntityService(Customer.class);
		try {
			MasterAgreement ma = custService.getValidMasterAgreement(custService.findById(contract.getCustomerId()),
					GregorianCalendar.getInstance().getTime());

			if (ma.getCurrency() != null) {
				param.setCurrencyID(ma.getCurrency().getId());
				param.setCurrencyCode(ma.getCurrency().getCode());
			}
			param.setPeriod(ma.getPeriod().getName());
			if (ma.getAverageMethod() != null) {
				param.setAverageMethodId(ma.getAverageMethod().getId());
				param.setAverageMethodName(ma.getAverageMethod().getName());
			}
			if (ma.getFinancialsource() != null) {
				param.setFinancialSourceId(ma.getFinancialsource().getId());
				param.setFinancialSourceCode(ma.getFinancialsource().getCode());
			}
			if (ma.getFinancialsource() != null && ma.getAverageMethod() != null) {
				param.setForex(ma.getFinancialsource().getCode() + "/" + ma.getAverageMethod().getName());
			}
			param.setInvFreq(ma.getInvoiceFrequency().getName());
			param.setInvType(ma.getInvoiceType().getName());
			param.setPaymentTerms(ma.getPaymentTerms());
			param.setReferenceTo(ma.getReferenceTo().getName());

			CreditLines cl = custService.getValidCreditLine(custService.findById(contract.getCustomerId()), new Date());
			if (cl != null && cl.getCreditTerm().getName() != null) {
				param.setCreditTerms(cl.getCreditTerm().getName());
			}

		} catch (BusinessException e) {
			// do nothing credit line does not exists
			LOGGER.warn("Got an exception when setting the default payment terms, will continue since the credit line does not exist!", e);
		}

	}
}
