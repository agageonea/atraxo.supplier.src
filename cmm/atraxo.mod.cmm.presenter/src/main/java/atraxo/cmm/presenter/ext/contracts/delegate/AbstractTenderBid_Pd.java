/**
 *
 */
package atraxo.cmm.presenter.ext.contracts.delegate;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.presenter.impl.contracts.model.TenderBid_Ds;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author zspeter
 */
public class AbstractTenderBid_Pd extends AbstractPresenterDelegate {

	protected static final String BEAN_HISTORY_FACADE = "externalInterfaceHistoryFacade";
	protected static final String BEAN_MESSAGE_HISTORY_FACADE = "externalInterfaceMessageHistoryFacade";
	protected static final String BID_SERVICE = "bidService";

	protected String getBidIdentificationMessage(TenderBid_Ds bid, String error) {
		StringBuilder bidIdentification = new StringBuilder(this.getBidIdentificationMessage(bid));
		bidIdentification.append("<br/>");
		bidIdentification.append("<b>Error:</b> ").append(error);
		return bidIdentification.toString();
	}

	protected String getBidIdentificationMessage(TenderBid_Ds bid) {
		StringBuilder bidIdentification = new StringBuilder();

		bidIdentification.append("Tender Name: ").append(bid.getTenderName()).append(" - ");
		bidIdentification.append("Tender Issuer Code: ").append(bid.getHolderCode()).append(" - ");
		bidIdentification.append("Location Code: ").append(bid.getLocCode()).append(" - ");
		bidIdentification.append("Bid Version: ").append(bid.getBidVersion());

		return bidIdentification.toString();
	}

	protected String getBidIdentificationMessage(Contract bid, String error) {
		StringBuilder bidIdentification = new StringBuilder(this.getBidIdentificationMessage(bid));
		bidIdentification.append("<br/>");
		bidIdentification.append("<b>Error:</b> ").append(error);
		return bidIdentification.toString();
	}

	protected String getBidIdentificationMessage(Contract bid) {
		StringBuilder bidIdentification = new StringBuilder();

		bidIdentification.append("Tender Name: ").append(bid.getBidTenderLocation().getTender().getName()).append(" - ");
		bidIdentification.append("Tender Issuer Code: ").append(bid.getHolder().getCode()).append(" - ");
		bidIdentification.append("Location Code: ").append(bid.getBidTenderLocation().getLocation().getCode()).append(" - ");
		bidIdentification.append("Bid Version: ").append(bid.getBidVersion());

		return bidIdentification.toString();
	}

}
