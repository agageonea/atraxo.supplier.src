package atraxo.cmm.presenter.ext.shipTo.delegate;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.presenter.impl.shipTo.model.ShipTo_Ds;
import atraxo.cmm.presenter.impl.shipTo.model.ShipTo_DsParam;
import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.domain.impl.history.ChangeHistory;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class ShipTo_Pd extends AbstractPresenterDelegate {

	/**
	 * @param shipTo
	 * @param params
	 * @throws Exception
	 */
	public void saveInHistory(ShipTo_Ds shipTo, ShipTo_DsParam params) throws Exception {
		IChangeHistoryService changeHistoryService = (IChangeHistoryService) this.findEntityService(ChangeHistory.class);
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Contract contract = contractService.findByBusiness(shipTo.getContractId());

		changeHistoryService.saveChangeHistory(contract, "Volume adjustment for customer " + shipTo.getCustomerCode(), params.getRemarks());
	}

}
