package atraxo.cmm.presenter.ext.contracts.delegate;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.presenter.impl.contracts.model.ContractCustomer_Ds;
import atraxo.cmm.presenter.impl.contracts.model.ContractCustomer_DsParam;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.masteragreements.MasterAgreement;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author zspeter
 */
public class ContractCustomerBlueprint_Pd extends AbstractPresenterDelegate {

	private final static Logger LOGGER = LoggerFactory.getLogger(ContractCustomerBlueprint_Pd.class);

	/**
	 * If status is "draft" set the status to active.
	 *
	 * @param contract
	 * @throws Exception
	 */
	public void setActive(ContractCustomer_Ds contractDs) throws Exception {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Contract contract = contractService.findById(contractDs.getId());
		if (ContractStatus._DRAFT_.equals(contract.getStatus())) {
			contractService.setActive(contract);
		} else {
			throw new BusinessException(CmmErrorCode.CONTRACT_RPC_ACTION_ERROR,
					String.format(CmmErrorCode.CONTRACT_RPC_ACTION_ERROR.getErrMsg(), contract.getCode()));
		}
	}

	/**
	 * If current date is in period set status to effective else set status to expired.
	 *
	 * @param contract
	 * @throws Exception
	 */
	public void updateStatus(ContractCustomer_Ds contract) throws Exception {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Contract contractBis = contractService.findById(contract.getId());
		if (!ContractStatus._DRAFT_.equals(contractBis.getStatus())) {
			contractService.updateStatus(contractBis);
		} else {
			throw new BusinessException(CmmErrorCode.CONTRACT_RPC_ACTION_ERROR,
					String.format(CmmErrorCode.CONTRACT_RPC_ACTION_ERROR.getErrMsg(), contract.getCode()));
		}
	}

	/**
	 * Remove the time part from the date.
	 *
	 * @param date
	 * @return
	 */
	public Date removeTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/**
	 * If contract status is not equal with expired change status to draft.
	 *
	 * @param contract
	 * @throws Exception
	 */
	public void resetStatus(ContractCustomer_Ds contract, ContractCustomer_DsParam param) throws Exception {

		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Contract contractB = contractService.findById(contract.getId());
		String remark = param.getRemarks();
		if (!ContractStatus._DRAFT_.equals(contractB.getStatus())) {
			contractService.reset(contractB, remark);
		} else {
			throw new BusinessException(CmmErrorCode.CONTRACT_RPC_ACTION_ERROR,
					String.format(CmmErrorCode.CONTRACT_RPC_ACTION_ERROR.getErrMsg(), contract.getCode()));
		}
	}

	/**
	 * @param contract
	 * @param param
	 * @throws Exception
	 */
	public void setDefaulPaymentTerms(ContractCustomer_Ds contract, ContractCustomer_DsParam param) throws Exception {
		ICustomerService custService = (ICustomerService) this.findEntityService(Customer.class);
		try {
			MasterAgreement ma = custService.getValidMasterAgreement(custService.findById(contract.getCustomerId()),
					GregorianCalendar.getInstance().getTime());

			if (ma.getCurrency() != null) {
				param.setCurrencyID(ma.getCurrency().getId());
				param.setCurrencyCode(ma.getCurrency().getCode());
			}
			param.setPeriod(ma.getPeriod().getName());
			param.setReferenceTo(ma.getReferenceTo().getName());
			if (ma.getAverageMethod() != null) {
				param.setAverageMethodId(ma.getAverageMethod().getId());
				param.setAverageMethodName(ma.getAverageMethod().getName());
			}
			if (ma.getFinancialsource() != null) {
				param.setFinancialSourceId(ma.getFinancialsource().getId());
				param.setFinancialSourceCode(ma.getFinancialsource().getCode());
			}
			if (ma.getFinancialsource() != null && ma.getAverageMethod() != null) {
				param.setForex(ma.getFinancialsource().getCode() + "/" + ma.getAverageMethod().getName());
			}
			param.setInvFreq(ma.getInvoiceFrequency().getName());
			param.setInvType(ma.getInvoiceType().getName());

			CreditLines cl = custService.getValidCreditLine(custService.findById(contract.getCustomerId()), new Date());
			if (cl != null) {
				param.setCreditTerms(cl.getCreditTerm().getName());
			}
			param.setPaymentTerms(ma.getPaymentTerms());
		} catch (BusinessException e) {
			// do nothing credit line does not exists
			LOGGER.warn("Got an exception when setting the default payment terms, will continue since the credit line does not exist!", e);
		}
	}

}
