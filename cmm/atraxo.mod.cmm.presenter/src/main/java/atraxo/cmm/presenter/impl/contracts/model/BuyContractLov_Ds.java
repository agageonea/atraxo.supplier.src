/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.contracts.model;

import atraxo.cmm.domain.impl.cmm_type.ContractScope;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;
import seava.j4e.api.model.IModelWithId;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Contract.class, sort = {@SortField(field = BuyContractLov_Ds.F_CODE)})
public class BuyContractLov_Ds extends AbstractSubsidiaryLov_Ds<Contract>
		implements
			IModelWithId<Integer> {

	public static final String ALIAS = "cmm_BuyContractLov_Ds";

	public static final String F_CODE = "code";
	public static final String F_ID = "id";
	public static final String F_DEALTYPE = "dealType";
	public static final String F_TYPE = "type";
	public static final String F_SUBTYPE = "subType";
	public static final String F_SCOPE = "scope";
	public static final String F_STATUS = "status";

	@DsField
	private String code;

	@DsField
	private Integer id;

	@DsField
	private DealType dealType;

	@DsField
	private ContractType type;

	@DsField
	private ContractSubType subType;

	@DsField
	private ContractScope scope;

	@DsField
	private ContractStatus status;

	/**
	 * Default constructor
	 */
	public BuyContractLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public BuyContractLov_Ds(Contract e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DealType getDealType() {
		return this.dealType;
	}

	public void setDealType(DealType dealType) {
		this.dealType = dealType;
	}

	public ContractType getType() {
		return this.type;
	}

	public void setType(ContractType type) {
		this.type = type;
	}

	public ContractSubType getSubType() {
		return this.subType;
	}

	public void setSubType(ContractSubType subType) {
		this.subType = subType;
	}

	public ContractScope getScope() {
		return this.scope;
	}

	public void setScope(ContractScope scope) {
		this.scope = scope;
	}

	public ContractStatus getStatus() {
		return this.status;
	}

	public void setStatus(ContractStatus status) {
		this.status = status;
	}
}
