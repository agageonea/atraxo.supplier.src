/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.tender.model;

import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.TenderSource;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Tender.class, sort = {@SortField(field = OpenTender_Ds.F_PUBLISHEDON, desc = true)})
@RefLookups({@RefLookup(refId = OpenTender_Ds.F_HOLDERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = OpenTender_Ds.F_HOLDERCODE)})})
public class OpenTender_Ds extends AbstractSubsidiaryDs_Ds<Tender> {

	public static final String ALIAS = "cmm_OpenTender_Ds";

	public static final String F_HOLDERID = "holderId";
	public static final String F_HOLDERCODE = "holderCode";
	public static final String F_HOLDERNAME = "holderName";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_TENDERVERSION = "tenderVersion";
	public static final String F_BIDDINGPERIODFROM = "biddingPeriodFrom";
	public static final String F_BIDDINGPERIODTO = "biddingPeriodTo";
	public static final String F_STATUS = "status";
	public static final String F_BIDDINGSTATUS = "biddingStatus";
	public static final String F_PUBLISHEDON = "publishedOn";
	public static final String F_SOURCE = "source";
	public static final String F_CLOSED = "closed";

	@DsField(join = "left", path = "holder.id")
	private Integer holderId;

	@DsField(join = "left", path = "holder.code")
	private String holderCode;

	@DsField(join = "left", path = "holder.name")
	private String holderName;

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private Long tenderVersion;

	@DsField
	private Date biddingPeriodFrom;

	@DsField
	private Date biddingPeriodTo;

	@DsField
	private TenderStatus status;

	@DsField
	private BiddingStatus biddingStatus;

	@DsField
	private Date publishedOn;

	@DsField
	private TenderSource source;

	@DsField
	private Boolean closed;

	/**
	 * Default constructor
	 */
	public OpenTender_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public OpenTender_Ds(Tender e) {
		super(e);
	}

	public Integer getHolderId() {
		return this.holderId;
	}

	public void setHolderId(Integer holderId) {
		this.holderId = holderId;
	}

	public String getHolderCode() {
		return this.holderCode;
	}

	public void setHolderCode(String holderCode) {
		this.holderCode = holderCode;
	}

	public String getHolderName() {
		return this.holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getTenderVersion() {
		return this.tenderVersion;
	}

	public void setTenderVersion(Long tenderVersion) {
		this.tenderVersion = tenderVersion;
	}

	public Date getBiddingPeriodFrom() {
		return this.biddingPeriodFrom;
	}

	public void setBiddingPeriodFrom(Date biddingPeriodFrom) {
		this.biddingPeriodFrom = biddingPeriodFrom;
	}

	public Date getBiddingPeriodTo() {
		return this.biddingPeriodTo;
	}

	public void setBiddingPeriodTo(Date biddingPeriodTo) {
		this.biddingPeriodTo = biddingPeriodTo;
	}

	public TenderStatus getStatus() {
		return this.status;
	}

	public void setStatus(TenderStatus status) {
		this.status = status;
	}

	public BiddingStatus getBiddingStatus() {
		return this.biddingStatus;
	}

	public void setBiddingStatus(BiddingStatus biddingStatus) {
		this.biddingStatus = biddingStatus;
	}

	public Date getPublishedOn() {
		return this.publishedOn;
	}

	public void setPublishedOn(Date publishedOn) {
		this.publishedOn = publishedOn;
	}

	public TenderSource getSource() {
		return this.source;
	}

	public void setSource(TenderSource source) {
		this.source = source;
	}

	public Boolean getClosed() {
		return this.closed;
	}

	public void setClosed(Boolean closed) {
		this.closed = closed;
	}
}
