package atraxo.cmm.presenter.ext.prices.service.validator;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IPricingBaseService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.utils.AverageMethodCodeFrequency;
import atraxo.cmm.business.ext.utils.AverageMethodCodeFrequencyPeriodCalculator;
import atraxo.cmm.business.ext.utils.DatePeriod;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.presenter.impl.contracts.model.PricingBase_Ds;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterBaseService;

/**
 * @author abolindu
 */
public class QuotationValidator extends AbstractPresenterBaseService {

	@Autowired
	private IPricingBaseService pbService;

	/**
	 * Check the needed condition before adding a new PricingBase regarding Quotation. Financial Source is also checked.
	 *
	 * @param ds
	 * @param pricingBase
	 * @throws BusinessException
	 */
	public void validateQuotation(PricingBase_Ds ds, PricingBase pricingBase) throws BusinessException {
		this.checkPricingBasisQuotation(pricingBase);
		this.checkFinancialSourceAvereagingmethod(ds);
		this.checkQuotationPeriod(pricingBase);
	}

	/**
	 * Check the needed condition before adding a new PricingBase regarding Quotation.
	 *
	 * @param pricingBase
	 * @throws BusinessException
	 */
	public void validateQuotation(PricingBase pricingBase) throws BusinessException {
		this.checkPricingBasisQuotation(pricingBase);
		this.checkQuotationPeriod(pricingBase);
	}

	/**
	 * Check if the mandatory field are filled.
	 *
	 * @param pricingBase - PricingBase
	 * @throws BusinessException
	 */
	private void checkPricingBasisQuotation(PricingBase pricingBase) throws BusinessException {
		if (this.pbService.isIndex(pricingBase)) {
			if (pricingBase.getQuotation() == null) {
				throw new BusinessException(CmmErrorCode.PB_QUOTATION_REQUIERED, CmmErrorCode.PB_QUOTATION_REQUIERED.getErrMsg());
			}
			if (pricingBase.getQuotationOffset() == null || MasterAgreementsPeriod._EMPTY_.equals(pricingBase.getQuotationOffset())) {
				throw new BusinessException(CmmErrorCode.PB_PERIOD_REQUIRED, CmmErrorCode.PB_PERIOD_REQUIRED.getErrMsg());
			}
			if (Operator._EMPTY_.equals(pricingBase.getOperator())) {
				throw new BusinessException(CmmErrorCode.PB_OPERATOR_REQUIRED, CmmErrorCode.PB_OPERATOR_REQUIRED.getErrMsg());
			}
			if (pricingBase.getFactor() == null) {
				throw new BusinessException(CmmErrorCode.PB_FACTOR_REQUIRED, CmmErrorCode.PB_FACTOR_REQUIRED.getErrMsg());
			}
			if (pricingBase.getConvUnit() == null) {
				throw new BusinessException(CmmErrorCode.PB_TO_REQUIRED, CmmErrorCode.PB_TO_REQUIRED.getErrMsg());
			}
		}
	}

	/**
	 * @param ds
	 * @throws BusinessException
	 */
	private void checkFinancialSourceAvereagingmethod(PricingBase_Ds ds) throws BusinessException {
		if ((ds.getQuotId() == null && "".equals(ds.getAvgMethodIndicatorName())) || (ds.getQuotId() != null && "".equals(ds.getQuotAvgMethodId()))) {
			throw new BusinessException(CmmErrorCode.AVERAGE_METHOD_IS_MISSING, CmmErrorCode.AVERAGE_METHOD_IS_MISSING.getErrMsg());
		}
		if (ds.getQuotId() == null && "".equals(ds.getFinancialSourceCode())) {
			throw new BusinessException(CmmErrorCode.FINCANCIAL_SOURCE_IS_MISSING, CmmErrorCode.FINCANCIAL_SOURCE_IS_MISSING.getErrMsg());
		}
	}

	/**
	 * Verify if on the pricing bases period's start is quotation value. If no throw an exception.
	 *
	 * @param pb - Pricing bases
	 * @throws BusinessException
	 */
	private void checkQuotationPeriod(PricingBase pb) throws BusinessException {
		List<QuotationValue> list = new ArrayList<>(pb.getQuotation().getQuotationValues());

		Date quotationValidFrom = null;

		if (!CollectionUtils.isEmpty(list)) {
			list.sort((o1, o2) -> o1.getValidFromDate().compareTo(o2.getValidToDate()));
			quotationValidFrom = list.get(0).getValidFromDate();
		}

		if (quotationValidFrom != null) {

			// see if the new date period start date is included in the quotation time series items
			AverageMethodCodeFrequencyPeriodCalculator calc = new AverageMethodCodeFrequencyPeriodCalculator(
					AverageMethodCodeFrequency.getByAverageMethod(pb.getQuotation().getAvgMethodIndicator()), pb.getValidFrom(),
					pb.getQuotationOffset().getDelta());
			DatePeriod dp = calc.calculatePeriod();
			Date offsetDate = dp.getStartDate().getTime();

			if (quotationValidFrom.compareTo(offsetDate) > 0) {
				DateFormat sdf = DateFormat.getDateInstance(DateFormat.MEDIUM, java.util.Locale.getDefault());
				throw new BusinessException(CmmErrorCode.QUOTATION_VALUES_NOT_FOUND,
						String.format(CmmErrorCode.QUOTATION_VALUES_NOT_FOUND.getErrMsg(), pb.getQuotation().getCalcVal(), sdf.format(offsetDate)));
			}
		} else {
			throw new BusinessException(CmmErrorCode.QUOTATION_INVALID_DATA,
					String.format(CmmErrorCode.QUOTATION_INVALID_DATA.getErrMsg(), "Valid From"));
		}

	}

}
