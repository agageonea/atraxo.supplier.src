/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.priceUpdate.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Customer.class, jpqlWhere = "(e.isSupplier = true or e.isCustomer = true)", sort = {@SortField(field = CounterPartyLov_Ds.F_CODE)})
public class CounterPartyLov_Ds extends AbstractLov_Ds<Customer> {

	public static final String ALIAS = "cmm_CounterPartyLov_Ds";

	public static final String F_NAME = "name";
	public static final String F_CODE = "code";

	@DsField
	private String name;

	@DsField
	private String code;

	/**
	 * Default constructor
	 */
	public CounterPartyLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public CounterPartyLov_Ds(Customer e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
