/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.contracts.model;

import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractScope;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.EventType;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.InvoicePayableBy;
import atraxo.cmm.domain.impl.cmm_type.IsSpot;
import atraxo.cmm.domain.impl.cmm_type.PaymentType;
import atraxo.cmm.domain.impl.cmm_type.Period;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.QuantityType;
import atraxo.cmm.domain.impl.cmm_type.ReviewPeriod;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.cmm_type.TenderSource;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.cmm_type.VatApplicability;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerType;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceType;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Contract.class, jpqlWhere = "e.isContract = false ", sort = {@SortField(field = TenderBid_Ds.F_CREATEDAT, desc = true)})
@RefLookups({
		@RefLookup(refId = TenderBid_Ds.F_LOCID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderBid_Ds.F_LOCCODE)}),
		@RefLookup(refId = TenderBid_Ds.F_HOLDERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderBid_Ds.F_HOLDERCODE)}),
		@RefLookup(refId = TenderBid_Ds.F_CUSTOMERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderBid_Ds.F_CUSTOMERCODE)}),
		@RefLookup(refId = TenderBid_Ds.F_IPLID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderBid_Ds.F_IPLCODE)}),
		@RefLookup(refId = TenderBid_Ds.F_RESBUYERID, namedQuery = UserSupp.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderBid_Ds.F_RESBUYERCODE)}),
		@RefLookup(refId = TenderBid_Ds.F_CONTACTID, namedQuery = Contacts.NQ_FIND_BY_ID, params = {@Param(name = "id", field = TenderBid_Ds.F_CONTACTNAME)}),
		@RefLookup(refId = TenderBid_Ds.F_CONTVOLUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderBid_Ds.F_CONTVOLUNITCODE)}),
		@RefLookup(refId = TenderBid_Ds.F_RESALEREFID, namedQuery = Contract.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderBid_Ds.F_RESALEREFCODE)}),
		@RefLookup(refId = TenderBid_Ds.F_SETTCURRID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderBid_Ds.F_SETTCURRCODE)}),
		@RefLookup(refId = TenderBid_Ds.F_SETTUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderBid_Ds.F_SETTUNITCODE)}),
		@RefLookup(refId = TenderBid_Ds.F_AVGMETHODID, namedQuery = AverageMethod.NQ_FIND_BY_NAME, params = {@Param(name = "code", field = TenderBid_Ds.F_AVGMETHODNAME)}),
		@RefLookup(refId = TenderBid_Ds.F_FINANCIALSOURCEID),
		@RefLookup(refId = TenderBid_Ds.F_RISKHOLDERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderBid_Ds.F_RISKHOLDERCODE)}),
		@RefLookup(refId = TenderBid_Ds.F_BILLTOID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderBid_Ds.F_BILLTOID)}),
		@RefLookup(refId = TenderBid_Ds.F_BIDIDENTID, namedQuery = Tender.NQ_FIND_BY_CODE_PRIMITIVE, params = {
				@Param(name = "code", field = TenderBid_Ds.F_TENDERCODE),
				@Param(name = "tenderVersion", field = TenderBid_Ds.F_TENDERVERSION),
				@Param(name = "holderId", field = TenderBid_Ds.F_TENDERHOLDERID)}),
		@RefLookup(refId = TenderBid_Ds.F_BIDLOCID, namedQuery = TenderLocation.NQ_FIND_BY_CODE_PRIMITIVE, params = {
				@Param(name = "locationId", field = TenderBid_Ds.F_BIDIDENTID),
				@Param(name = "tenderId", field = TenderBid_Ds.F_BIDLOCLOCATIONIDID)})})
public class TenderBid_Ds extends AbstractSubsidiaryDs_Ds<Contract> {

	public static final String ALIAS = "cmm_TenderBid_Ds";

	public static final String F_DAYSFREQUENCY = "daysFrequency";
	public static final String F_SEPARATOR = "separator";
	public static final String F_EVENTS = "events";
	public static final String F_DAYS = "days";
	public static final String F_LOCID = "locId";
	public static final String F_LOCCODE = "locCode";
	public static final String F_LOCAIRPORTNAME = "locAirportName";
	public static final String F_COUNTRYID = "countryId";
	public static final String F_COUNTRYNAME = "countryName";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_CUSTOMERNAME = "customerName";
	public static final String F_CPREFID = "cpRefId";
	public static final String F_ISCUSTOMER = "isCustomer";
	public static final String F_HOLDERID = "holderId";
	public static final String F_HOLDERCODE = "holderCode";
	public static final String F_HOLDERNAME = "holderName";
	public static final String F_IPLID = "iplId";
	public static final String F_IPLCODE = "iplCode";
	public static final String F_RESBUYERID = "resBuyerId";
	public static final String F_RESBUYERNAME = "resBuyerName";
	public static final String F_RESBUYERCODE = "resBuyerCode";
	public static final String F_CONTACTID = "contactId";
	public static final String F_CONTACTNAME = "contactName";
	public static final String F_CONTVOLUNITID = "contVolUnitId";
	public static final String F_CONTVOLUNITCODE = "contVolUnitCode";
	public static final String F_RESALEREFID = "resaleRefId";
	public static final String F_RESALEREFCODE = "resaleRefCode";
	public static final String F_SETTCURRID = "settCurrId";
	public static final String F_SETTCURRCODE = "settCurrCode";
	public static final String F_SETTUNITID = "settUnitId";
	public static final String F_SETTUNITCODE = "settUnitCode";
	public static final String F_FINANCIALSOURCEID = "financialSourceId";
	public static final String F_FINANCIALSOURCECODE = "financialSourceCode";
	public static final String F_AVGMETHODID = "avgMethodId";
	public static final String F_AVGMETHODNAME = "avgMethodName";
	public static final String F_RISKHOLDERID = "riskHolderId";
	public static final String F_RISKHOLDERCODE = "riskHolderCode";
	public static final String F_RISKHOLDERTYPE = "riskHolderType";
	public static final String F_PARENTGROUPID = "parentGroupId";
	public static final String F_PARENTGROUPCODE = "parentGroupCode";
	public static final String F_BILLTOID = "billToId";
	public static final String F_BILLTOCODE = "billToCode";
	public static final String F_BILLTOTYPE = "billToType";
	public static final String F_PARENTGROUPBILLID = "parentGroupBillId";
	public static final String F_PARENTGROUPBILLCODE = "parentGroupBillCode";
	public static final String F_BIDIDENTID = "bidIdentId";
	public static final String F_TENDERNAME = "tenderName";
	public static final String F_TENDERCODE = "tenderCode";
	public static final String F_TENDERVERSION = "tenderVersion";
	public static final String F_TENDERSTATUS = "tenderStatus";
	public static final String F_TENDERHOLDERID = "tenderHolderId";
	public static final String F_TENDERHOLDERCODE = "tenderHolderCode";
	public static final String F_HOLDERCONTACTID = "holderContactId";
	public static final String F_HOLDERCONTACTNAME = "holderContactName";
	public static final String F_BIDLOCID = "bidLocId";
	public static final String F_BIDLOCVOLUME = "bidLocVolume";
	public static final String F_BIDLOCBIDDINGSTATUS = "bidLocBiddingStatus";
	public static final String F_BIDLOCLOCATIONIDID = "bidLocLocationIdId";
	public static final String F_BIDLOCVOLUNITID = "bidLocVolUnitId";
	public static final String F_BIDLOCVOLUNITCODE = "bidLocVolUnitCode";
	public static final String F_TENDERSOURCE = "tenderSource";
	public static final String F_UNIT = "unit";
	public static final String F_CODE = "code";
	public static final String F_DEALTYPE = "dealType";
	public static final String F_TYPE = "type";
	public static final String F_FLIGHTSERVICETYPE = "flightServiceType";
	public static final String F_SUBTYPE = "subType";
	public static final String F_SCOPE = "scope";
	public static final String F_ISSPOT = "isSpot";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_STATUS = "status";
	public static final String F_LIMITEDTO = "limitedTo";
	public static final String F_PRODUCT = "product";
	public static final String F_IATASERVICELEVEL = "iataServiceLevel";
	public static final String F_TAX = "tax";
	public static final String F_QUANTITYTYPE = "quantityType";
	public static final String F_PERIOD = "period";
	public static final String F_VOLUMETOLERANCE = "volumeTolerance";
	public static final String F_VOLUMESHARE = "volumeShare";
	public static final String F_COUNTERPARTYREFERENCE = "counterpartyReference";
	public static final String F_SETTLEMENTDECIMALS = "settlementDecimals";
	public static final String F_EXCHANGERATEOFFSET = "exchangeRateOffset";
	public static final String F_PAYMENTTERMS = "paymentTerms";
	public static final String F_PAYMENTREFDAY = "paymentRefDay";
	public static final String F_INVOICEFREQ = "invoiceFreq";
	public static final String F_INVOICEFREQNUMBER = "invoiceFreqNumber";
	public static final String F_CREDITTERMS = "creditTerms";
	public static final String F_INVOICETYPE = "invoiceType";
	public static final String F_VAT = "vat";
	public static final String F_REVIEWPERIOD = "reviewPeriod";
	public static final String F_REVIEWFIRSTPARAM = "reviewFirstParam";
	public static final String F_REVIEWSECONDPARAM = "reviewSecondParam";
	public static final String F_REVIEWNOTIFICATION = "reviewNotification";
	public static final String F_PAYMENTCOMMENT = "paymentComment";
	public static final String F_EVENTTYPE = "eventType";
	public static final String F_INVOICEPAYABLEBY = "invoicePayableBy";
	public static final String F_AWARDEDVOLUME = "awardedVolume";
	public static final String F_OFFEREDVOLUME = "offeredVolume";
	public static final String F_FOREX = "forex";
	public static final String F_FORMTITLE = "formTitle";
	public static final String F_PERCENT = "percent";
	public static final String F_PERCENTCOVEREDAWARDED = "percentCoveredAwarded";
	public static final String F_PERCENTCOVEREDOFFERED = "percentCoveredOffered";
	public static final String F_PERCENTCOVEREDLABEL = "percentCoveredLabel";
	public static final String F_REQUIREDVOLUMETITLE = "requiredVolumeTitle";
	public static final String F_SPACE = "space";
	public static final String F_LINEDELIM = "lineDelim";
	public static final String F_LABELFIELD = "labelField";
	public static final String F_CUSTOMERFIELD = "customerField";
	public static final String F_CANBECOMPLETED = "canBeCompleted";
	public static final String F_AIRLINESLIST = "airlinesList";
	public static final String F_HASTOTALVOLUME = "hasTotalVolume";
	public static final String F_ISCONTRACT = "isContract";
	public static final String F_BIDSTATUS = "bidStatus";
	public static final String F_BIDTRANSMISSIONSTATUS = "bidTransmissionStatus";
	public static final String F_BIDCANCELTRANSMITIONSTATUS = "bidCancelTransmitionStatus";
	public static final String F_BIDACCEPTAWARDTRANSMISSIONSTATUS = "bidAcceptAwardTransmissionStatus";
	public static final String F_BIDDECLINEAWARDTRANSMISSIONSTATUS = "bidDeclineAwardTransmissionStatus";
	public static final String F_BIDAPPROVALSTATUS = "bidApprovalStatus";
	public static final String F_BIDREVISION = "bidRevision";
	public static final String F_BIDVERSION = "bidVersion";
	public static final String F_BIDVALIDFROM = "bidValidFrom";
	public static final String F_BIDVALIDTO = "bidValidTo";
	public static final String F_BIDVOLUMEPERAIRLINE = "bidVolumePerAirline";
	public static final String F_BIDPREPAIDDAYS = "bidPrepaidDays";
	public static final String F_BIDPAYEMENTFREQ = "bidPayementFreq";
	public static final String F_BIDPREPAYFIRSTDELIVERYDATE = "bidPrepayFirstDeliveryDate";
	public static final String F_BIDBANKGUARANTEE = "bidBankGuarantee";
	public static final String F_BIDOPERATINGHOURS = "bidOperatingHours";
	public static final String F_BIDTITLETRANSFER = "bidTitleTransfer";
	public static final String F_BIDASTMSPECIFICATION = "bidAstmSpecification";
	public static final String F_BIDPAYMENTTYPE = "bidPaymentType";
	public static final String F_BIDPREPAIDAMOUNT = "bidPrepaidAmount";
	public static final String F_PERIODAPPROVALSTATUS = "periodApprovalStatus";
	public static final String F_BIDPACKAGEIDENTIFIER = "bidPackageIdentifier";
	public static final String F_SUBSIDIARYCODE = "subsidiaryCode";
	public static final String F_BIDROUND = "bidRound";
	public static final String F_MARKED = "marked";
	public static final String F_CANCANCEL = "canCancel";

	@DsField(fetch = false)
	private String daysFrequency;

	@DsField(fetch = false)
	private String separator;

	@DsField(fetch = false)
	private String events;

	@DsField(fetch = false)
	private String days;

	@DsField(join = "left", path = "location.id")
	private Integer locId;

	@DsField(join = "left", path = "location.code")
	private String locCode;

	@DsField(join = "left", path = "location.name")
	private String locAirportName;

	@DsField(join = "left", path = "location.country.id")
	private Integer countryId;

	@DsField(join = "left", path = "location.country.name")
	private String countryName;

	@DsField(join = "left", path = "customer.id")
	private Integer customerId;

	@DsField(join = "left", path = "customer.code")
	private String customerCode;

	@DsField(join = "left", path = "customer.name")
	private String customerName;

	@DsField(join = "left", path = "customer.refid")
	private String cpRefId;

	@DsField(join = "left", path = "customer.isCustomer")
	private Boolean isCustomer;

	@DsField(join = "left", path = "holder.id")
	private Integer holderId;

	@DsField(join = "left", path = "holder.code")
	private String holderCode;

	@DsField(join = "left", path = "holder.name")
	private String holderName;

	@DsField(join = "left", path = "intoPlaneAgent.id")
	private Integer iplId;

	@DsField(join = "left", path = "intoPlaneAgent.code")
	private String iplCode;

	@DsField(join = "left", path = "responsibleBuyer.id")
	private String resBuyerId;

	@DsField(join = "left", path = "responsibleBuyer.name")
	private String resBuyerName;

	@DsField(join = "left", path = "responsibleBuyer.code")
	private String resBuyerCode;

	@DsField(join = "left", path = "contact.id")
	private Integer contactId;

	@DsField(join = "left", fetch = false, path = "contact.fullNameField")
	private String contactName;

	@DsField(join = "left", path = "contractVolumeUnit.id")
	private Integer contVolUnitId;

	@DsField(join = "left", path = "contractVolumeUnit.code")
	private String contVolUnitCode;

	@DsField(join = "left", path = "resaleRef.id")
	private Integer resaleRefId;

	@DsField(join = "left", path = "resaleRef.code")
	private String resaleRefCode;

	@DsField(join = "left", path = "settlementCurr.id")
	private Integer settCurrId;

	@DsField(join = "left", path = "settlementCurr.code")
	private String settCurrCode;

	@DsField(join = "left", path = "settlementUnit.id")
	private Integer settUnitId;

	@DsField(join = "left", path = "settlementUnit.code")
	private String settUnitCode;

	@DsField(join = "left", path = "financialSource.id")
	private Integer financialSourceId;

	@DsField(join = "left", path = "financialSource.code")
	private String financialSourceCode;

	@DsField(join = "left", path = "averageMethod.id")
	private Integer avgMethodId;

	@DsField(join = "left", path = "averageMethod.name")
	private String avgMethodName;

	@DsField(join = "left", path = "riskHolder.id")
	private Integer riskHolderId;

	@DsField(join = "left", path = "riskHolder.code")
	private String riskHolderCode;

	@DsField(join = "left", path = "riskHolder.type")
	private CustomerType riskHolderType;

	@DsField(join = "left", path = "riskHolder.parent.id")
	private Integer parentGroupId;

	@DsField(join = "left", path = "riskHolder.parent.code")
	private String parentGroupCode;

	@DsField(join = "left", path = "billTo.id")
	private Integer billToId;

	@DsField(join = "left", path = "billTo.code")
	private String billToCode;

	@DsField(join = "left", path = "billTo.type")
	private CustomerType billToType;

	@DsField(join = "left", path = "billTo.parent.id")
	private Integer parentGroupBillId;

	@DsField(join = "left", path = "billTo.parent.code")
	private String parentGroupBillCode;

	@DsField(join = "left", path = "bidTenderIdentification.id")
	private Integer bidIdentId;

	@DsField(join = "left", path = "bidTenderIdentification.name")
	private String tenderName;

	@DsField(join = "left", path = "bidTenderIdentification.code")
	private String tenderCode;

	@DsField(join = "left", path = "bidTenderIdentification.tenderVersion")
	private Long tenderVersion;

	@DsField(join = "left", path = "bidTenderIdentification.status")
	private TenderStatus tenderStatus;

	@DsField(join = "left", path = "bidTenderIdentification.holder.id")
	private Integer tenderHolderId;

	@DsField(join = "left", path = "bidTenderIdentification.holder.code")
	private String tenderHolderCode;

	@DsField(join = "left", path = "bidTenderIdentification.contact.id")
	private Integer holderContactId;

	@DsField(join = "left", fetch = false, path = "bidTenderIdentification.contact.fullNameField")
	private String holderContactName;

	@DsField(join = "left", path = "bidTenderLocation.id")
	private Integer bidLocId;

	@DsField(join = "left", path = "bidTenderLocation.volume")
	private BigDecimal bidLocVolume;

	@DsField(join = "left", path = "bidTenderLocation.locationBiddingStatus")
	private BiddingStatus bidLocBiddingStatus;

	@DsField(join = "left", path = "bidTenderLocation.location.id")
	private Integer bidLocLocationIdId;

	@DsField(join = "left", path = "bidTenderLocation.volumeUnit.id")
	private Integer bidLocVolUnitId;

	@DsField(join = "left", path = "bidTenderLocation.volumeUnit.code")
	private String bidLocVolUnitCode;

	@DsField(join = "left", path = "bidTenderIdentification.source")
	private TenderSource tenderSource;

	@DsField(fetch = false)
	private String unit;

	@DsField
	private String code;

	@DsField
	private DealType dealType;

	@DsField
	private ContractType type;

	@DsField
	private FlightServiceType flightServiceType;

	@DsField
	private ContractSubType subType;

	@DsField
	private ContractScope scope;

	@DsField
	private IsSpot isSpot;

	@DsField(jpqlFilter = " e.validTo >= :validFrom ")
	private Date validFrom;

	@DsField(jpqlFilter = " e.validFrom <= :validTo ")
	private Date validTo;

	@DsField
	private ContractStatus status;

	@DsField
	private FlightTypeIndicator limitedTo;

	@DsField
	private Product product;

	@DsField
	private Integer iataServiceLevel;

	@DsField
	private TaxType tax;

	@DsField
	private QuantityType quantityType;

	@DsField
	private Period period;

	@DsField
	private Integer volumeTolerance;

	@DsField
	private Integer volumeShare;

	@DsField
	private String counterpartyReference;

	@DsField
	private Integer settlementDecimals;

	@DsField
	private MasterAgreementsPeriod exchangeRateOffset;

	@DsField
	private Integer paymentTerms;

	@DsField
	private PaymentDay paymentRefDay;

	@DsField
	private InvoiceFreq invoiceFreq;

	@DsField
	private Integer invoiceFreqNumber;

	@DsField
	private CreditTerm creditTerms;

	@DsField
	private InvoiceType invoiceType;

	@DsField
	private VatApplicability vat;

	@DsField
	private ReviewPeriod reviewPeriod;

	@DsField
	private String reviewFirstParam;

	@DsField
	private String reviewSecondParam;

	@DsField
	private Integer reviewNotification;

	@DsField
	private String paymentComment;

	@DsField
	private EventType eventType;

	@DsField
	private InvoicePayableBy invoicePayableBy;

	@DsField
	private BigDecimal awardedVolume;

	@DsField
	private BigDecimal offeredVolume;

	@DsField(fetch = false)
	private String forex;

	@DsField(fetch = false)
	private String formTitle;

	@DsField(fetch = false)
	private String percent;

	@DsField(fetch = false)
	private BigDecimal percentCoveredAwarded;

	@DsField(fetch = false)
	private BigDecimal percentCoveredOffered;

	@DsField(fetch = false)
	private String percentCoveredLabel;

	@DsField(fetch = false)
	private String requiredVolumeTitle;

	@DsField(fetch = false)
	private String space;

	@DsField(fetch = false)
	private String lineDelim;

	@DsField(fetch = false)
	private String labelField;

	@DsField(fetch = false)
	private String customerField;

	@DsField(fetch = false)
	private Boolean canBeCompleted;

	@DsField(fetch = false)
	private String airlinesList;

	@DsField(path = "bidHasTotalVolume")
	private Boolean hasTotalVolume;

	@DsField
	private Boolean isContract;

	@DsField
	private BidStatus bidStatus;

	@DsField
	private TransmissionStatus bidTransmissionStatus;

	@DsField
	private TransmissionStatus bidCancelTransmitionStatus;

	@DsField
	private TransmissionStatus bidAcceptAwardTransmissionStatus;

	@DsField
	private TransmissionStatus bidDeclineAwardTransmissionStatus;

	@DsField
	private BidApprovalStatus bidApprovalStatus;

	@DsField
	private Integer bidRevision;

	@DsField
	private String bidVersion;

	@DsField
	private Date bidValidFrom;

	@DsField
	private Date bidValidTo;

	@DsField
	private Boolean bidVolumePerAirline;

	@DsField
	private Integer bidPrepaidDays;

	@DsField
	private InvoiceFreq bidPayementFreq;

	@DsField
	private Integer bidPrepayFirstDeliveryDate;

	@DsField
	private Boolean bidBankGuarantee;

	@DsField
	private String bidOperatingHours;

	@DsField
	private String bidTitleTransfer;

	@DsField
	private String bidAstmSpecification;

	@DsField
	private PaymentType bidPaymentType;

	@DsField
	private BigDecimal bidPrepaidAmount;

	@DsField
	private BidApprovalStatus periodApprovalStatus;

	@DsField
	private String bidPackageIdentifier;

	@DsField(fetch = false)
	private String subsidiaryCode;

	@DsField(fetch = false)
	private Integer bidRound;

	@DsField(fetch = false)
	private Boolean marked;

	@DsField(fetch = false)
	private Boolean canCancel;

	/**
	 * Default constructor
	 */
	public TenderBid_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public TenderBid_Ds(Contract e) {
		super(e);
	}

	public String getDaysFrequency() {
		return this.daysFrequency;
	}

	public void setDaysFrequency(String daysFrequency) {
		this.daysFrequency = daysFrequency;
	}

	public String getSeparator() {
		return this.separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public String getEvents() {
		return this.events;
	}

	public void setEvents(String events) {
		this.events = events;
	}

	public String getDays() {
		return this.days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public Integer getLocId() {
		return this.locId;
	}

	public void setLocId(Integer locId) {
		this.locId = locId;
	}

	public String getLocCode() {
		return this.locCode;
	}

	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}

	public String getLocAirportName() {
		return this.locAirportName;
	}

	public void setLocAirportName(String locAirportName) {
		this.locAirportName = locAirportName;
	}

	public Integer getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCpRefId() {
		return this.cpRefId;
	}

	public void setCpRefId(String cpRefId) {
		this.cpRefId = cpRefId;
	}

	public Boolean getIsCustomer() {
		return this.isCustomer;
	}

	public void setIsCustomer(Boolean isCustomer) {
		this.isCustomer = isCustomer;
	}

	public Integer getHolderId() {
		return this.holderId;
	}

	public void setHolderId(Integer holderId) {
		this.holderId = holderId;
	}

	public String getHolderCode() {
		return this.holderCode;
	}

	public void setHolderCode(String holderCode) {
		this.holderCode = holderCode;
	}

	public String getHolderName() {
		return this.holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public Integer getIplId() {
		return this.iplId;
	}

	public void setIplId(Integer iplId) {
		this.iplId = iplId;
	}

	public String getIplCode() {
		return this.iplCode;
	}

	public void setIplCode(String iplCode) {
		this.iplCode = iplCode;
	}

	public String getResBuyerId() {
		return this.resBuyerId;
	}

	public void setResBuyerId(String resBuyerId) {
		this.resBuyerId = resBuyerId;
	}

	public String getResBuyerName() {
		return this.resBuyerName;
	}

	public void setResBuyerName(String resBuyerName) {
		this.resBuyerName = resBuyerName;
	}

	public String getResBuyerCode() {
		return this.resBuyerCode;
	}

	public void setResBuyerCode(String resBuyerCode) {
		this.resBuyerCode = resBuyerCode;
	}

	public Integer getContactId() {
		return this.contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}

	public String getContactName() {
		return this.contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public Integer getContVolUnitId() {
		return this.contVolUnitId;
	}

	public void setContVolUnitId(Integer contVolUnitId) {
		this.contVolUnitId = contVolUnitId;
	}

	public String getContVolUnitCode() {
		return this.contVolUnitCode;
	}

	public void setContVolUnitCode(String contVolUnitCode) {
		this.contVolUnitCode = contVolUnitCode;
	}

	public Integer getResaleRefId() {
		return this.resaleRefId;
	}

	public void setResaleRefId(Integer resaleRefId) {
		this.resaleRefId = resaleRefId;
	}

	public String getResaleRefCode() {
		return this.resaleRefCode;
	}

	public void setResaleRefCode(String resaleRefCode) {
		this.resaleRefCode = resaleRefCode;
	}

	public Integer getSettCurrId() {
		return this.settCurrId;
	}

	public void setSettCurrId(Integer settCurrId) {
		this.settCurrId = settCurrId;
	}

	public String getSettCurrCode() {
		return this.settCurrCode;
	}

	public void setSettCurrCode(String settCurrCode) {
		this.settCurrCode = settCurrCode;
	}

	public Integer getSettUnitId() {
		return this.settUnitId;
	}

	public void setSettUnitId(Integer settUnitId) {
		this.settUnitId = settUnitId;
	}

	public String getSettUnitCode() {
		return this.settUnitCode;
	}

	public void setSettUnitCode(String settUnitCode) {
		this.settUnitCode = settUnitCode;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}

	public Integer getAvgMethodId() {
		return this.avgMethodId;
	}

	public void setAvgMethodId(Integer avgMethodId) {
		this.avgMethodId = avgMethodId;
	}

	public String getAvgMethodName() {
		return this.avgMethodName;
	}

	public void setAvgMethodName(String avgMethodName) {
		this.avgMethodName = avgMethodName;
	}

	public Integer getRiskHolderId() {
		return this.riskHolderId;
	}

	public void setRiskHolderId(Integer riskHolderId) {
		this.riskHolderId = riskHolderId;
	}

	public String getRiskHolderCode() {
		return this.riskHolderCode;
	}

	public void setRiskHolderCode(String riskHolderCode) {
		this.riskHolderCode = riskHolderCode;
	}

	public CustomerType getRiskHolderType() {
		return this.riskHolderType;
	}

	public void setRiskHolderType(CustomerType riskHolderType) {
		this.riskHolderType = riskHolderType;
	}

	public Integer getParentGroupId() {
		return this.parentGroupId;
	}

	public void setParentGroupId(Integer parentGroupId) {
		this.parentGroupId = parentGroupId;
	}

	public String getParentGroupCode() {
		return this.parentGroupCode;
	}

	public void setParentGroupCode(String parentGroupCode) {
		this.parentGroupCode = parentGroupCode;
	}

	public Integer getBillToId() {
		return this.billToId;
	}

	public void setBillToId(Integer billToId) {
		this.billToId = billToId;
	}

	public String getBillToCode() {
		return this.billToCode;
	}

	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}

	public CustomerType getBillToType() {
		return this.billToType;
	}

	public void setBillToType(CustomerType billToType) {
		this.billToType = billToType;
	}

	public Integer getParentGroupBillId() {
		return this.parentGroupBillId;
	}

	public void setParentGroupBillId(Integer parentGroupBillId) {
		this.parentGroupBillId = parentGroupBillId;
	}

	public String getParentGroupBillCode() {
		return this.parentGroupBillCode;
	}

	public void setParentGroupBillCode(String parentGroupBillCode) {
		this.parentGroupBillCode = parentGroupBillCode;
	}

	public Integer getBidIdentId() {
		return this.bidIdentId;
	}

	public void setBidIdentId(Integer bidIdentId) {
		this.bidIdentId = bidIdentId;
	}

	public String getTenderName() {
		return this.tenderName;
	}

	public void setTenderName(String tenderName) {
		this.tenderName = tenderName;
	}

	public String getTenderCode() {
		return this.tenderCode;
	}

	public void setTenderCode(String tenderCode) {
		this.tenderCode = tenderCode;
	}

	public Long getTenderVersion() {
		return this.tenderVersion;
	}

	public void setTenderVersion(Long tenderVersion) {
		this.tenderVersion = tenderVersion;
	}

	public TenderStatus getTenderStatus() {
		return this.tenderStatus;
	}

	public void setTenderStatus(TenderStatus tenderStatus) {
		this.tenderStatus = tenderStatus;
	}

	public Integer getTenderHolderId() {
		return this.tenderHolderId;
	}

	public void setTenderHolderId(Integer tenderHolderId) {
		this.tenderHolderId = tenderHolderId;
	}

	public String getTenderHolderCode() {
		return this.tenderHolderCode;
	}

	public void setTenderHolderCode(String tenderHolderCode) {
		this.tenderHolderCode = tenderHolderCode;
	}

	public Integer getHolderContactId() {
		return this.holderContactId;
	}

	public void setHolderContactId(Integer holderContactId) {
		this.holderContactId = holderContactId;
	}

	public String getHolderContactName() {
		return this.holderContactName;
	}

	public void setHolderContactName(String holderContactName) {
		this.holderContactName = holderContactName;
	}

	public Integer getBidLocId() {
		return this.bidLocId;
	}

	public void setBidLocId(Integer bidLocId) {
		this.bidLocId = bidLocId;
	}

	public BigDecimal getBidLocVolume() {
		return this.bidLocVolume;
	}

	public void setBidLocVolume(BigDecimal bidLocVolume) {
		this.bidLocVolume = bidLocVolume;
	}

	public BiddingStatus getBidLocBiddingStatus() {
		return this.bidLocBiddingStatus;
	}

	public void setBidLocBiddingStatus(BiddingStatus bidLocBiddingStatus) {
		this.bidLocBiddingStatus = bidLocBiddingStatus;
	}

	public Integer getBidLocLocationIdId() {
		return this.bidLocLocationIdId;
	}

	public void setBidLocLocationIdId(Integer bidLocLocationIdId) {
		this.bidLocLocationIdId = bidLocLocationIdId;
	}

	public Integer getBidLocVolUnitId() {
		return this.bidLocVolUnitId;
	}

	public void setBidLocVolUnitId(Integer bidLocVolUnitId) {
		this.bidLocVolUnitId = bidLocVolUnitId;
	}

	public String getBidLocVolUnitCode() {
		return this.bidLocVolUnitCode;
	}

	public void setBidLocVolUnitCode(String bidLocVolUnitCode) {
		this.bidLocVolUnitCode = bidLocVolUnitCode;
	}

	public TenderSource getTenderSource() {
		return this.tenderSource;
	}

	public void setTenderSource(TenderSource tenderSource) {
		this.tenderSource = tenderSource;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public DealType getDealType() {
		return this.dealType;
	}

	public void setDealType(DealType dealType) {
		this.dealType = dealType;
	}

	public ContractType getType() {
		return this.type;
	}

	public void setType(ContractType type) {
		this.type = type;
	}

	public FlightServiceType getFlightServiceType() {
		return this.flightServiceType;
	}

	public void setFlightServiceType(FlightServiceType flightServiceType) {
		this.flightServiceType = flightServiceType;
	}

	public ContractSubType getSubType() {
		return this.subType;
	}

	public void setSubType(ContractSubType subType) {
		this.subType = subType;
	}

	public ContractScope getScope() {
		return this.scope;
	}

	public void setScope(ContractScope scope) {
		this.scope = scope;
	}

	public IsSpot getIsSpot() {
		return this.isSpot;
	}

	public void setIsSpot(IsSpot isSpot) {
		this.isSpot = isSpot;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public ContractStatus getStatus() {
		return this.status;
	}

	public void setStatus(ContractStatus status) {
		this.status = status;
	}

	public FlightTypeIndicator getLimitedTo() {
		return this.limitedTo;
	}

	public void setLimitedTo(FlightTypeIndicator limitedTo) {
		this.limitedTo = limitedTo;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getIataServiceLevel() {
		return this.iataServiceLevel;
	}

	public void setIataServiceLevel(Integer iataServiceLevel) {
		this.iataServiceLevel = iataServiceLevel;
	}

	public TaxType getTax() {
		return this.tax;
	}

	public void setTax(TaxType tax) {
		this.tax = tax;
	}

	public QuantityType getQuantityType() {
		return this.quantityType;
	}

	public void setQuantityType(QuantityType quantityType) {
		this.quantityType = quantityType;
	}

	public Period getPeriod() {
		return this.period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public Integer getVolumeTolerance() {
		return this.volumeTolerance;
	}

	public void setVolumeTolerance(Integer volumeTolerance) {
		this.volumeTolerance = volumeTolerance;
	}

	public Integer getVolumeShare() {
		return this.volumeShare;
	}

	public void setVolumeShare(Integer volumeShare) {
		this.volumeShare = volumeShare;
	}

	public String getCounterpartyReference() {
		return this.counterpartyReference;
	}

	public void setCounterpartyReference(String counterpartyReference) {
		this.counterpartyReference = counterpartyReference;
	}

	public Integer getSettlementDecimals() {
		return this.settlementDecimals;
	}

	public void setSettlementDecimals(Integer settlementDecimals) {
		this.settlementDecimals = settlementDecimals;
	}

	public MasterAgreementsPeriod getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(MasterAgreementsPeriod exchangeRateOffset) {
		this.exchangeRateOffset = exchangeRateOffset;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public PaymentDay getPaymentRefDay() {
		return this.paymentRefDay;
	}

	public void setPaymentRefDay(PaymentDay paymentRefDay) {
		this.paymentRefDay = paymentRefDay;
	}

	public InvoiceFreq getInvoiceFreq() {
		return this.invoiceFreq;
	}

	public void setInvoiceFreq(InvoiceFreq invoiceFreq) {
		this.invoiceFreq = invoiceFreq;
	}

	public Integer getInvoiceFreqNumber() {
		return this.invoiceFreqNumber;
	}

	public void setInvoiceFreqNumber(Integer invoiceFreqNumber) {
		this.invoiceFreqNumber = invoiceFreqNumber;
	}

	public CreditTerm getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(CreditTerm creditTerms) {
		this.creditTerms = creditTerms;
	}

	public InvoiceType getInvoiceType() {
		return this.invoiceType;
	}

	public void setInvoiceType(InvoiceType invoiceType) {
		this.invoiceType = invoiceType;
	}

	public VatApplicability getVat() {
		return this.vat;
	}

	public void setVat(VatApplicability vat) {
		this.vat = vat;
	}

	public ReviewPeriod getReviewPeriod() {
		return this.reviewPeriod;
	}

	public void setReviewPeriod(ReviewPeriod reviewPeriod) {
		this.reviewPeriod = reviewPeriod;
	}

	public String getReviewFirstParam() {
		return this.reviewFirstParam;
	}

	public void setReviewFirstParam(String reviewFirstParam) {
		this.reviewFirstParam = reviewFirstParam;
	}

	public String getReviewSecondParam() {
		return this.reviewSecondParam;
	}

	public void setReviewSecondParam(String reviewSecondParam) {
		this.reviewSecondParam = reviewSecondParam;
	}

	public Integer getReviewNotification() {
		return this.reviewNotification;
	}

	public void setReviewNotification(Integer reviewNotification) {
		this.reviewNotification = reviewNotification;
	}

	public String getPaymentComment() {
		return this.paymentComment;
	}

	public void setPaymentComment(String paymentComment) {
		this.paymentComment = paymentComment;
	}

	public EventType getEventType() {
		return this.eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public InvoicePayableBy getInvoicePayableBy() {
		return this.invoicePayableBy;
	}

	public void setInvoicePayableBy(InvoicePayableBy invoicePayableBy) {
		this.invoicePayableBy = invoicePayableBy;
	}

	public BigDecimal getAwardedVolume() {
		return this.awardedVolume;
	}

	public void setAwardedVolume(BigDecimal awardedVolume) {
		this.awardedVolume = awardedVolume;
	}

	public BigDecimal getOfferedVolume() {
		return this.offeredVolume;
	}

	public void setOfferedVolume(BigDecimal offeredVolume) {
		this.offeredVolume = offeredVolume;
	}

	public String getForex() {
		return this.forex;
	}

	public void setForex(String forex) {
		this.forex = forex;
	}

	public String getFormTitle() {
		return this.formTitle;
	}

	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}

	public String getPercent() {
		return this.percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	public BigDecimal getPercentCoveredAwarded() {
		return this.percentCoveredAwarded;
	}

	public void setPercentCoveredAwarded(BigDecimal percentCoveredAwarded) {
		this.percentCoveredAwarded = percentCoveredAwarded;
	}

	public BigDecimal getPercentCoveredOffered() {
		return this.percentCoveredOffered;
	}

	public void setPercentCoveredOffered(BigDecimal percentCoveredOffered) {
		this.percentCoveredOffered = percentCoveredOffered;
	}

	public String getPercentCoveredLabel() {
		return this.percentCoveredLabel;
	}

	public void setPercentCoveredLabel(String percentCoveredLabel) {
		this.percentCoveredLabel = percentCoveredLabel;
	}

	public String getRequiredVolumeTitle() {
		return this.requiredVolumeTitle;
	}

	public void setRequiredVolumeTitle(String requiredVolumeTitle) {
		this.requiredVolumeTitle = requiredVolumeTitle;
	}

	public String getSpace() {
		return this.space;
	}

	public void setSpace(String space) {
		this.space = space;
	}

	public String getLineDelim() {
		return this.lineDelim;
	}

	public void setLineDelim(String lineDelim) {
		this.lineDelim = lineDelim;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}

	public String getCustomerField() {
		return this.customerField;
	}

	public void setCustomerField(String customerField) {
		this.customerField = customerField;
	}

	public Boolean getCanBeCompleted() {
		return this.canBeCompleted;
	}

	public void setCanBeCompleted(Boolean canBeCompleted) {
		this.canBeCompleted = canBeCompleted;
	}

	public String getAirlinesList() {
		return this.airlinesList;
	}

	public void setAirlinesList(String airlinesList) {
		this.airlinesList = airlinesList;
	}

	public Boolean getHasTotalVolume() {
		return this.hasTotalVolume;
	}

	public void setHasTotalVolume(Boolean hasTotalVolume) {
		this.hasTotalVolume = hasTotalVolume;
	}

	public Boolean getIsContract() {
		return this.isContract;
	}

	public void setIsContract(Boolean isContract) {
		this.isContract = isContract;
	}

	public BidStatus getBidStatus() {
		return this.bidStatus;
	}

	public void setBidStatus(BidStatus bidStatus) {
		this.bidStatus = bidStatus;
	}

	public TransmissionStatus getBidTransmissionStatus() {
		return this.bidTransmissionStatus;
	}

	public void setBidTransmissionStatus(
			TransmissionStatus bidTransmissionStatus) {
		this.bidTransmissionStatus = bidTransmissionStatus;
	}

	public TransmissionStatus getBidCancelTransmitionStatus() {
		return this.bidCancelTransmitionStatus;
	}

	public void setBidCancelTransmitionStatus(
			TransmissionStatus bidCancelTransmitionStatus) {
		this.bidCancelTransmitionStatus = bidCancelTransmitionStatus;
	}

	public TransmissionStatus getBidAcceptAwardTransmissionStatus() {
		return this.bidAcceptAwardTransmissionStatus;
	}

	public void setBidAcceptAwardTransmissionStatus(
			TransmissionStatus bidAcceptAwardTransmissionStatus) {
		this.bidAcceptAwardTransmissionStatus = bidAcceptAwardTransmissionStatus;
	}

	public TransmissionStatus getBidDeclineAwardTransmissionStatus() {
		return this.bidDeclineAwardTransmissionStatus;
	}

	public void setBidDeclineAwardTransmissionStatus(
			TransmissionStatus bidDeclineAwardTransmissionStatus) {
		this.bidDeclineAwardTransmissionStatus = bidDeclineAwardTransmissionStatus;
	}

	public BidApprovalStatus getBidApprovalStatus() {
		return this.bidApprovalStatus;
	}

	public void setBidApprovalStatus(BidApprovalStatus bidApprovalStatus) {
		this.bidApprovalStatus = bidApprovalStatus;
	}

	public Integer getBidRevision() {
		return this.bidRevision;
	}

	public void setBidRevision(Integer bidRevision) {
		this.bidRevision = bidRevision;
	}

	public String getBidVersion() {
		return this.bidVersion;
	}

	public void setBidVersion(String bidVersion) {
		this.bidVersion = bidVersion;
	}

	public Date getBidValidFrom() {
		return this.bidValidFrom;
	}

	public void setBidValidFrom(Date bidValidFrom) {
		this.bidValidFrom = bidValidFrom;
	}

	public Date getBidValidTo() {
		return this.bidValidTo;
	}

	public void setBidValidTo(Date bidValidTo) {
		this.bidValidTo = bidValidTo;
	}

	public Boolean getBidVolumePerAirline() {
		return this.bidVolumePerAirline;
	}

	public void setBidVolumePerAirline(Boolean bidVolumePerAirline) {
		this.bidVolumePerAirline = bidVolumePerAirline;
	}

	public Integer getBidPrepaidDays() {
		return this.bidPrepaidDays;
	}

	public void setBidPrepaidDays(Integer bidPrepaidDays) {
		this.bidPrepaidDays = bidPrepaidDays;
	}

	public InvoiceFreq getBidPayementFreq() {
		return this.bidPayementFreq;
	}

	public void setBidPayementFreq(InvoiceFreq bidPayementFreq) {
		this.bidPayementFreq = bidPayementFreq;
	}

	public Integer getBidPrepayFirstDeliveryDate() {
		return this.bidPrepayFirstDeliveryDate;
	}

	public void setBidPrepayFirstDeliveryDate(Integer bidPrepayFirstDeliveryDate) {
		this.bidPrepayFirstDeliveryDate = bidPrepayFirstDeliveryDate;
	}

	public Boolean getBidBankGuarantee() {
		return this.bidBankGuarantee;
	}

	public void setBidBankGuarantee(Boolean bidBankGuarantee) {
		this.bidBankGuarantee = bidBankGuarantee;
	}

	public String getBidOperatingHours() {
		return this.bidOperatingHours;
	}

	public void setBidOperatingHours(String bidOperatingHours) {
		this.bidOperatingHours = bidOperatingHours;
	}

	public String getBidTitleTransfer() {
		return this.bidTitleTransfer;
	}

	public void setBidTitleTransfer(String bidTitleTransfer) {
		this.bidTitleTransfer = bidTitleTransfer;
	}

	public String getBidAstmSpecification() {
		return this.bidAstmSpecification;
	}

	public void setBidAstmSpecification(String bidAstmSpecification) {
		this.bidAstmSpecification = bidAstmSpecification;
	}

	public PaymentType getBidPaymentType() {
		return this.bidPaymentType;
	}

	public void setBidPaymentType(PaymentType bidPaymentType) {
		this.bidPaymentType = bidPaymentType;
	}

	public BigDecimal getBidPrepaidAmount() {
		return this.bidPrepaidAmount;
	}

	public void setBidPrepaidAmount(BigDecimal bidPrepaidAmount) {
		this.bidPrepaidAmount = bidPrepaidAmount;
	}

	public BidApprovalStatus getPeriodApprovalStatus() {
		return this.periodApprovalStatus;
	}

	public void setPeriodApprovalStatus(BidApprovalStatus periodApprovalStatus) {
		this.periodApprovalStatus = periodApprovalStatus;
	}

	public String getBidPackageIdentifier() {
		return this.bidPackageIdentifier;
	}

	public void setBidPackageIdentifier(String bidPackageIdentifier) {
		this.bidPackageIdentifier = bidPackageIdentifier;
	}

	public String getSubsidiaryCode() {
		return this.subsidiaryCode;
	}

	public void setSubsidiaryCode(String subsidiaryCode) {
		this.subsidiaryCode = subsidiaryCode;
	}

	public Integer getBidRound() {
		return this.bidRound;
	}

	public void setBidRound(Integer bidRound) {
		this.bidRound = bidRound;
	}

	public Boolean getMarked() {
		return this.marked;
	}

	public void setMarked(Boolean marked) {
		this.marked = marked;
	}

	public Boolean getCanCancel() {
		return this.canCancel;
	}

	public void setCanCancel(Boolean canCancel) {
		this.canCancel = canCancel;
	}
}
