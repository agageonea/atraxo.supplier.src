/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.prices.model;

import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FinancialSources.class, jpqlWhere = "e.id IN (SELECT t.financialSource.id FROM TimeSerie t JOIN Quotation q WHERE t.id = q.timeseries.id)", sort = {@SortField(field = UsedFinancialSourcesLov_Ds.F_NAME)})
public class UsedFinancialSourcesLov_Ds
		extends
			AbstractLov_Ds<FinancialSources> {

	public static final String ALIAS = "cmm_UsedFinancialSourcesLov_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_CARDINAL = "cardinal";

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField(fetch = false)
	private Integer cardinal;

	/**
	 * Default constructor
	 */
	public UsedFinancialSourcesLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public UsedFinancialSourcesLov_Ds(FinancialSources e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCardinal() {
		return this.cardinal;
	}

	public void setCardinal(Integer cardinal) {
		this.cardinal = cardinal;
	}
}
