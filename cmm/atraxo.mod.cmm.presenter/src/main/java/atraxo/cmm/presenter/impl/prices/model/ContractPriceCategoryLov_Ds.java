/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.prices.model;

import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ContractPriceCategory.class)
public class ContractPriceCategoryLov_Ds
		extends
			AbstractLov_Ds<ContractPriceCategory> {

	public static final String ALIAS = "cmm_ContractPriceCategoryLov_Ds";

	public static final String F_CONTID = "contId";
	public static final String F_CONTCODE = "contCode";
	public static final String F_NAME = "name";

	@DsField(join = "left", path = "contract.id")
	private Integer contId;

	@DsField(join = "left", path = "contract.code")
	private String contCode;

	@DsField
	private String name;

	/**
	 * Default constructor
	 */
	public ContractPriceCategoryLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ContractPriceCategoryLov_Ds(ContractPriceCategory e) {
		super(e);
	}

	public Integer getContId() {
		return this.contId;
	}

	public void setContId(Integer contId) {
		this.contId = contId;
	}

	public String getContCode() {
		return this.contCode;
	}

	public void setContCode(String contCode) {
		this.contCode = contCode;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
