/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.priceUpdate.model;

/**
 * Generated code. Do not modify in this file.
 */
public class PriceUpdateCategoriesList_DsParam {

	public static final String f_parentPriceUpdateId = "parentPriceUpdateId";

	private String parentPriceUpdateId;

	public String getParentPriceUpdateId() {
		return this.parentPriceUpdateId;
	}

	public void setParentPriceUpdateId(String parentPriceUpdateId) {
		this.parentPriceUpdateId = parentPriceUpdateId;
	}
}
