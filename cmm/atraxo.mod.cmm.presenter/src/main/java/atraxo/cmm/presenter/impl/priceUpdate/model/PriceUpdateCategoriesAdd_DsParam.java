/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.priceUpdate.model;

/**
 * Generated code. Do not modify in this file.
 */
public class PriceUpdateCategoriesAdd_DsParam {

	public static final String f_parentPriceUpdateRefId = "parentPriceUpdateRefId";

	private String parentPriceUpdateRefId;

	public String getParentPriceUpdateRefId() {
		return this.parentPriceUpdateRefId;
	}

	public void setParentPriceUpdateRefId(String parentPriceUpdateRefId) {
		this.parentPriceUpdateRefId = parentPriceUpdateRefId;
	}
}
