/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.tender.model;

import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.Period;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = TenderLocationAirlines.class)
@RefLookups({
		@RefLookup(refId = TenderLocationAirlines_Ds.F_TENDERLOCATIONID),
		@RefLookup(refId = TenderLocationAirlines_Ds.F_AIRLINEID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderLocationAirlines_Ds.F_AIRLINECODE)}),
		@RefLookup(refId = TenderLocationAirlines_Ds.F_UNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderLocationAirlines_Ds.F_UNITCODE)})})
public class TenderLocationAirlines_Ds
		extends
			AbstractDs_Ds<TenderLocationAirlines> {

	public static final String ALIAS = "cmm_TenderLocationAirlines_Ds";

	public static final String F_TENDERSTATUS = "tenderStatus";
	public static final String F_TENDERLOCATIONID = "tenderLocationId";
	public static final String F_AIRLINEID = "airlineId";
	public static final String F_AIRLINECODE = "airlineCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_VOLUME = "volume";
	public static final String F_VOLUMEPERIOD = "volumePeriod";
	public static final String F_BIDDINGSTATUS = "biddingStatus";
	public static final String F_NOTES = "notes";
	public static final String F_ISVALID = "isValid";

	@DsField(join = "left", path = "tenderLocation.tender.status")
	private TenderStatus tenderStatus;

	@DsField(join = "left", path = "tenderLocation.id")
	private Integer tenderLocationId;

	@DsField(join = "left", path = "airline.id")
	private Integer airlineId;

	@DsField(join = "left", path = "airline.code")
	private String airlineCode;

	@DsField(join = "left", path = "unit.id")
	private Integer unitId;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	@DsField
	private BigDecimal volume;

	@DsField
	private Period volumePeriod;

	@DsField
	private BiddingStatus biddingStatus;

	@DsField
	private String notes;

	@DsField
	private Boolean isValid;

	/**
	 * Default constructor
	 */
	public TenderLocationAirlines_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public TenderLocationAirlines_Ds(TenderLocationAirlines e) {
		super(e);
	}

	public TenderStatus getTenderStatus() {
		return this.tenderStatus;
	}

	public void setTenderStatus(TenderStatus tenderStatus) {
		this.tenderStatus = tenderStatus;
	}

	public Integer getTenderLocationId() {
		return this.tenderLocationId;
	}

	public void setTenderLocationId(Integer tenderLocationId) {
		this.tenderLocationId = tenderLocationId;
	}

	public Integer getAirlineId() {
		return this.airlineId;
	}

	public void setAirlineId(Integer airlineId) {
		this.airlineId = airlineId;
	}

	public String getAirlineCode() {
		return this.airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public BigDecimal getVolume() {
		return this.volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public Period getVolumePeriod() {
		return this.volumePeriod;
	}

	public void setVolumePeriod(Period volumePeriod) {
		this.volumePeriod = volumePeriod;
	}

	public BiddingStatus getBiddingStatus() {
		return this.biddingStatus;
	}

	public void setBiddingStatus(BiddingStatus biddingStatus) {
		this.biddingStatus = biddingStatus;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getIsValid() {
		return this.isValid;
	}

	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}
}
