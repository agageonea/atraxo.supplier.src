/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.contracts.cnv;

import javax.persistence.EntityManager;

import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.EventType;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.InvoicePayableBy;
import atraxo.cmm.domain.impl.cmm_type.IsSpot;
import atraxo.cmm.domain.impl.cmm_type.PaymentType;
import atraxo.cmm.domain.impl.cmm_type.ReviewPeriod;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.cmm.presenter.impl.contracts.model.ContractCustomerWizard_Ds;
import atraxo.fmbas.business.api.categories.IPriceCategoryService;
import atraxo.fmbas.business.api.quotation.IQuotationService;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import seava.j4e.api.action.result.IDsConverter;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.converter.AbstractDsConverter;

/**
 * Custom converter ContractCustomerWizard_DsCnv
 */
public class ContractCustomerWizard_DsCnv extends AbstractDsConverter<ContractCustomerWizard_Ds, Contract>
		implements IDsConverter<ContractCustomerWizard_Ds, Contract> {

	@Override
	protected void modelToEntityAttributes(ContractCustomerWizard_Ds m, Contract e, boolean isInsert, EntityManager em) throws Exception {
		super.modelToEntityAttributes(m, e, isInsert, em);
		if (e.getBidApprovalStatus() == null) {
			e.setBidApprovalStatus(BidApprovalStatus._EMPTY_);
		}
		if (e.getBidPaymentType() == null) {
			e.setBidPaymentType(PaymentType._EMPTY_);
		}
		if (e.getBidStatus() == null) {
			e.setBidStatus(BidStatus._EMPTY_);
		}
		if (e.getBidTransmissionStatus() == null) {
			e.setBidTransmissionStatus(TransmissionStatus._EMPTY_);
		}
		if (e.getBidAcceptAwardTransmissionStatus() == null) {
			e.setBidAcceptAwardTransmissionStatus(TransmissionStatus._EMPTY_);
		}
		if (e.getBidDeclineAwardTransmissionStatus() == null) {
			e.setBidDeclineAwardTransmissionStatus(TransmissionStatus._EMPTY_);
		}
		if (e.getInvoicePayableBy() == null) {
			e.setInvoicePayableBy(InvoicePayableBy._EMPTY_);
		}
		if (e.getIsSpot() == null) {
			e.setIsSpot(IsSpot._TERM_);
		}
		if (e.getLimitedTo() == null) {
			e.setLimitedTo(FlightTypeIndicator._UNSPECIFIED_);
		}
		if (e.getEventType() == null) {
			e.setEventType(EventType._EMPTY_);
		}
		if (e.getPeriodApprovalStatus() == null) {
			e.setPeriodApprovalStatus(BidApprovalStatus._EMPTY_);
		}
		if (e.getReviewPeriod() == null) {
			e.setReviewPeriod(ReviewPeriod._EMPTY_);
		}
		if (e.getStatus() == null) {
			e.setStatus(ContractStatus._DRAFT_);
		}
		if (e.getTax() == null) {
			e.setTax(TaxType._UNSPECIFIED_);
		}
		if (e.getReadOnly() == null) {
			e.setReadOnly(false);
		}
		if (e.getIsContract() == null) {
			e.setIsContract(true);
		}
		if (e.getFlightServiceType() == null) {
			e.setFlightServiceType(FlightServiceType._EMPTY_);
		}
		if (e.getBidCancelTransmitionStatus() == null) {
			e.setBidCancelTransmitionStatus(TransmissionStatus._EMPTY_);
		}
	}

	@Override
	protected void modelToEntityReferences(ContractCustomerWizard_Ds m, Contract e, boolean isInsert, EntityManager em) throws Exception {
		super.modelToEntityReferences(m, e, isInsert, em);

		if (isInsert) {
			this.buildPricingBases(m, e);
			this.buidShipTo(m, e);
		}
	}

	private void buidShipTo(ContractCustomerWizard_Ds m, Contract e) {
		if (CollectionUtils.isEmpty(e.getShipTo())) {
			ShipTo to = new ShipTo();
			to.setAllocatedVolume(e.getAwardedVolume());
			to.setCustomer(e.getCustomer());
			to.setValidFrom(e.getValidFrom());
			to.setValidTo(e.getValidTo());
			e.addToShipTo(to);
		}

	}

	private void buildPricingBases(ContractCustomerWizard_Ds m, Contract e) throws Exception {
		IPriceCategoryService srv = (IPriceCategoryService) this.findEntityService(PriceCategory.class);
		PriceCategory pc = null;
		if (m.getPriceCategoryId() != null) {
			pc = srv.findById(m.getPriceCategoryId());
		} else if (m.getPriceCategoryName() != null) {
			pc = srv.findbyName(m.getPriceCategoryName());
		}
		if (pc != null && pc.getType().equals(PriceType._PRODUCT_) && pc.getPricePer().equals(PriceInd._VOLUME_)) {
			if (pc.getIsIndexBased()) {
				PriceCategory diff = srv.findbyName("Differential");
				this.createIndexPrice(m, e, pc, diff);
			} else {
				this.createPricingBase(m, e, pc);
			}
		} else {
			throw new BusinessException(CmmErrorCode.INVALID_PRICE_CATEGORY, CmmErrorCode.INVALID_PRICE_CATEGORY.getErrMsg());
		}

	}

	/**
	 * Create an index and a differential type pricing base and add it to contract.
	 *
	 * @param m
	 * @param e
	 * @param index
	 * @param diff
	 * @throws Exception
	 */
	private void createIndexPrice(ContractCustomerWizard_Ds m, Contract e, PriceCategory index, PriceCategory diff) throws Exception {
		PricingBase indexPb = new PricingBase();
		indexPb.setPriceCat(index);
		indexPb.setDescription(m.getPriceName());
		indexPb.setQuantityType(m.getQuantityType());
		indexPb.setContinous(true);
		Quotation q = this.getQuotation(m);
		indexPb.setAvgMethod(q.getAvgMethodIndicator());
		indexPb.setAverageMethodId(e.getAverageMethod().getId());
		indexPb.setQuotation(q);
		indexPb.setQuotationOffset(m.getQuotationOffset());
		indexPb.setOperator(q.getTimeseries().getConvFctr());
		indexPb.setFactor(q.getTimeseries().getArithmOper());
		indexPb.setDecimals(q.getTimeseries().getDecimals());
		indexPb.setConvUnit(q.getTimeseries().getUnit2Id());
		indexPb.setVat(e.getVat());
		indexPb.setValidFrom(e.getValidFrom());
		indexPb.setValidTo(e.getValidTo());
		e.addToPricingBases(indexPb);

		this.createPricingBase(m, e, diff);
	}

	/**
	 * Create and add a pricing base to contract
	 *
	 * @param m
	 * @param e
	 * @param pc
	 */
	private void createPricingBase(ContractCustomerWizard_Ds m, Contract e, PriceCategory pc) {
		PricingBase pb = new PricingBase();
		pb.setPriceCat(pc);
		pb.setContinous(true);
		pb.setDescription(pc.getName());
		pb.setInitialUnitId(m.getInitialUnitId());
		pb.setInitialCurrId(m.getInitialCurId());
		pb.setInitialPrice(m.getInitialPrice());
		pb.setValidFrom(e.getValidFrom());
		pb.setValidTo(e.getValidTo());
		pb.setOperator(Operator._EMPTY_);
		pb.setQuotationOffset(MasterAgreementsPeriod._EMPTY_);
		pb.setVat(m.getVat());
		pb.setQuantityType(m.getQuantityType());
		pb.setAverageMethodId(e.getAverageMethod().getId());
		e.addToPricingBases(pb);
	}

	/**
	 * @param m
	 * @return
	 * @throws Exception
	 */
	private Quotation getQuotation(ContractCustomerWizard_Ds m) throws Exception {
		IQuotationService srv = (IQuotationService) this.findEntityService(Quotation.class);
		if (m.getQuotationId() != null) {
			return srv.findById(m.getQuotationId());
		}
		throw new BusinessException(CmmErrorCode.QUOTATION_NOT_FOUND_BY_ID, CmmErrorCode.QUOTATION_NOT_FOUND_BY_ID.getErrMsg());
	}
}
