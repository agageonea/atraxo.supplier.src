/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.priceUpdate.model;

/**
 * Generated code. Do not modify in this file.
 */
public class PriceUpdate_DsParam {

	public static final String f_contractStatusEffective = "contractStatusEffective";
	public static final String f_contractStatusExpired = "contractStatusExpired";
	public static final String f_contractTypeSales = "contractTypeSales";
	public static final String f_contractTypePurchase = "contractTypePurchase";
	public static final String f_contractForProduct = "contractForProduct";
	public static final String f_contractForService = "contractForService";
	public static final String f_deliveryPointIntoPlane = "deliveryPointIntoPlane";
	public static final String f_deliveryPointIntoStorage = "deliveryPointIntoStorage";
	public static final String f_submitForApprovalDescription = "submitForApprovalDescription";
	public static final String f_submitForApprovalResult = "submitForApprovalResult";
	public static final String f_selectedCategories = "selectedCategories";
	public static final String f_selectedAttachments = "selectedAttachments";
	public static final String f_approvalNote = "approvalNote";
	public static final String f_selectedAll = "selectedAll";
	public static final String f_unSelectedCategories = "unSelectedCategories";
	public static final String f_filter = "filter";
	public static final String f_standardFilter = "standardFilter";

	private Boolean contractStatusEffective;

	private Boolean contractStatusExpired;

	private Boolean contractTypeSales;

	private Boolean contractTypePurchase;

	private Boolean contractForProduct;

	private Boolean contractForService;

	private Boolean deliveryPointIntoPlane;

	private Boolean deliveryPointIntoStorage;

	private String submitForApprovalDescription;

	private Boolean submitForApprovalResult;

	private String selectedCategories;

	private String selectedAttachments;

	private String approvalNote;

	private Boolean selectedAll;

	private String unSelectedCategories;

	private String filter;

	private String standardFilter;

	public Boolean getContractStatusEffective() {
		return this.contractStatusEffective;
	}

	public void setContractStatusEffective(Boolean contractStatusEffective) {
		this.contractStatusEffective = contractStatusEffective;
	}

	public Boolean getContractStatusExpired() {
		return this.contractStatusExpired;
	}

	public void setContractStatusExpired(Boolean contractStatusExpired) {
		this.contractStatusExpired = contractStatusExpired;
	}

	public Boolean getContractTypeSales() {
		return this.contractTypeSales;
	}

	public void setContractTypeSales(Boolean contractTypeSales) {
		this.contractTypeSales = contractTypeSales;
	}

	public Boolean getContractTypePurchase() {
		return this.contractTypePurchase;
	}

	public void setContractTypePurchase(Boolean contractTypePurchase) {
		this.contractTypePurchase = contractTypePurchase;
	}

	public Boolean getContractForProduct() {
		return this.contractForProduct;
	}

	public void setContractForProduct(Boolean contractForProduct) {
		this.contractForProduct = contractForProduct;
	}

	public Boolean getContractForService() {
		return this.contractForService;
	}

	public void setContractForService(Boolean contractForService) {
		this.contractForService = contractForService;
	}

	public Boolean getDeliveryPointIntoPlane() {
		return this.deliveryPointIntoPlane;
	}

	public void setDeliveryPointIntoPlane(Boolean deliveryPointIntoPlane) {
		this.deliveryPointIntoPlane = deliveryPointIntoPlane;
	}

	public Boolean getDeliveryPointIntoStorage() {
		return this.deliveryPointIntoStorage;
	}

	public void setDeliveryPointIntoStorage(Boolean deliveryPointIntoStorage) {
		this.deliveryPointIntoStorage = deliveryPointIntoStorage;
	}

	public String getSubmitForApprovalDescription() {
		return this.submitForApprovalDescription;
	}

	public void setSubmitForApprovalDescription(
			String submitForApprovalDescription) {
		this.submitForApprovalDescription = submitForApprovalDescription;
	}

	public Boolean getSubmitForApprovalResult() {
		return this.submitForApprovalResult;
	}

	public void setSubmitForApprovalResult(Boolean submitForApprovalResult) {
		this.submitForApprovalResult = submitForApprovalResult;
	}

	public String getSelectedCategories() {
		return this.selectedCategories;
	}

	public void setSelectedCategories(String selectedCategories) {
		this.selectedCategories = selectedCategories;
	}

	public String getSelectedAttachments() {
		return this.selectedAttachments;
	}

	public void setSelectedAttachments(String selectedAttachments) {
		this.selectedAttachments = selectedAttachments;
	}

	public String getApprovalNote() {
		return this.approvalNote;
	}

	public void setApprovalNote(String approvalNote) {
		this.approvalNote = approvalNote;
	}

	public Boolean getSelectedAll() {
		return this.selectedAll;
	}

	public void setSelectedAll(Boolean selectedAll) {
		this.selectedAll = selectedAll;
	}

	public String getUnSelectedCategories() {
		return this.unSelectedCategories;
	}

	public void setUnSelectedCategories(String unSelectedCategories) {
		this.unSelectedCategories = unSelectedCategories;
	}

	public String getFilter() {
		return this.filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public String getStandardFilter() {
		return this.standardFilter;
	}

	public void setStandardFilter(String standardFilter) {
		this.standardFilter = standardFilter;
	}
}
