/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.tender.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.tender.ITenderLocationRoundService;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.TenderSource;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import atraxo.cmm.presenter.impl.tender.model.TenderLocation_Ds;
import atraxo.cmm.presenter.impl.tender.model.TenderLocation_DsParam;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service TenderLocation_DsService
 */
public class TenderLocation_DsService extends AbstractEntityDsService<TenderLocation_Ds, TenderLocation_Ds, TenderLocation_DsParam, TenderLocation>
		implements IDsService<TenderLocation_Ds, TenderLocation_Ds, TenderLocation_DsParam> {

	@Autowired
	private ITenderLocationRoundService tenderLocationRoundService;

	@Override
	protected void postFind(IQueryBuilder<TenderLocation_Ds, TenderLocation_Ds, TenderLocation_DsParam> builder, List<TenderLocation_Ds> result)
			throws Exception {

		super.postFind(builder, result);

		for (TenderLocation_Ds ds : result) {
			ds.setCanBeDeclined(this.canDeclineBid(ds));

			List<TenderLocationRound> rounds = this.tenderLocationRoundService.findByTenderLocationId(ds.getId());
			for (TenderLocationRound round : rounds) {
				if (round.getActive() != null && round.getActive()) {
					ds.setRound(round.getRoundNo());
					ds.setBidPeriodFrom(round.getBiddingFrom());
					ds.setBidPeriodTo(round.getBiddingTo());
					ds.setBidOpeningDate(round.getBidOpening());
					break;
				}
			}
		}
	}

	private boolean canDeclineBid(TenderLocation_Ds ds) {
		boolean isImportedAndNew = TenderSource._IMPORTED_.equals(ds.getTenderSource()) && BiddingStatus._NEW_.equals(ds.getLocationBiddingStatus());
		boolean isNotTransmited = TransmissionStatus._NEW_.equals(ds.getTransmissionStatus())
				|| TransmissionStatus._FAILED_.equals(ds.getTransmissionStatus());

		if (isImportedAndNew && isNotTransmited) {
			return true;
		}

		return false;

	}

}
