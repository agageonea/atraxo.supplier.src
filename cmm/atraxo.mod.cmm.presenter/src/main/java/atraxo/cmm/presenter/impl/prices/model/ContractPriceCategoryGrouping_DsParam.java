/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.prices.model;

import java.math.BigDecimal;
import java.util.Date;
/**
 * Generated code. Do not modify in this file.
 */
public class ContractPriceCategoryGrouping_DsParam {

	public static final String f_isDefault = "isDefault";
	public static final String f_settlementCurrencyCode = "settlementCurrencyCode";
	public static final String f_settlementCurrencyId = "settlementCurrencyId";
	public static final String f_settlementUnitCode = "settlementUnitCode";
	public static final String f_priceDate = "priceDate";
	public static final String f_total = "total";

	private Boolean isDefault;

	private String settlementCurrencyCode;

	private Integer settlementCurrencyId;

	private String settlementUnitCode;

	private Date priceDate;

	private BigDecimal total;

	public Boolean getIsDefault() {
		return this.isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public String getSettlementCurrencyCode() {
		return this.settlementCurrencyCode;
	}

	public void setSettlementCurrencyCode(String settlementCurrencyCode) {
		this.settlementCurrencyCode = settlementCurrencyCode;
	}

	public Integer getSettlementCurrencyId() {
		return this.settlementCurrencyId;
	}

	public void setSettlementCurrencyId(Integer settlementCurrencyId) {
		this.settlementCurrencyId = settlementCurrencyId;
	}

	public String getSettlementUnitCode() {
		return this.settlementUnitCode;
	}

	public void setSettlementUnitCode(String settlementUnitCode) {
		this.settlementUnitCode = settlementUnitCode;
	}

	public Date getPriceDate() {
		return this.priceDate;
	}

	public void setPriceDate(Date priceDate) {
		this.priceDate = priceDate;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}
}
