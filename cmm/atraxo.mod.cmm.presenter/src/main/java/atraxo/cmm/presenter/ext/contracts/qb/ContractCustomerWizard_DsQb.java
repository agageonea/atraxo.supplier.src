/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.cmm.presenter.ext.contracts.qb;

import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.presenter.impl.contracts.model.ContractCustomerWizard_Ds;

public class ContractCustomerWizard_DsQb extends
QueryBuilderWithJpql<ContractCustomerWizard_Ds, ContractCustomerWizard_Ds, ContractCustomerWizard_Ds> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();
		this.addFilterCondition("e.dealType = :dealType");
		this.addCustomFilterItem("dealType", DealType._SELL_);
	}
}