/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.contracts.model;

import atraxo.cmm.domain.impl.contracts.ContractTemplate;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ContractTemplate.class)
public class ContractTemplate_Ds extends AbstractDs_Ds<ContractTemplate> {

	public static final String ALIAS = "cmm_ContractTemplate_Ds";

	public static final String F_CONTRACTTYPE = "contractType";
	public static final String F_DELIVERYSUBTYPE = "deliverySubtype";
	public static final String F_SCOPE = "scope";
	public static final String F_DIALOG = "dialog";

	@DsField
	private String contractType;

	@DsField
	private String deliverySubtype;

	@DsField
	private String scope;

	@DsField
	private String dialog;

	/**
	 * Default constructor
	 */
	public ContractTemplate_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ContractTemplate_Ds(ContractTemplate e) {
		super(e);
	}

	public String getContractType() {
		return this.contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getDeliverySubtype() {
		return this.deliverySubtype;
	}

	public void setDeliverySubtype(String deliverySubtype) {
		this.deliverySubtype = deliverySubtype;
	}

	public String getScope() {
		return this.scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getDialog() {
		return this.dialog;
	}

	public void setDialog(String dialog) {
		this.dialog = dialog;
	}
}
