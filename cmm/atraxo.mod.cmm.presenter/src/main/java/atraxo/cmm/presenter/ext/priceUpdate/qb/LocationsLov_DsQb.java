/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.cmm.presenter.ext.priceUpdate.qb;

import atraxo.cmm.presenter.impl.priceUpdate.model.LocationsLov_Ds;
import atraxo.cmm.presenter.impl.priceUpdate.model.LocationsLov_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

/**
 * Custom Query Builder LocationsLov_DsQb
 */
public class LocationsLov_DsQb extends QueryBuilderWithJpql<LocationsLov_Ds, LocationsLov_Ds, LocationsLov_DsParam> {


	@Override
	protected void beforeBuildWhere() throws Exception {

		this.addFilterCondition(" e1.id = :areaId ");
		this.addCustomFilterItem("areaId", this.params.getAreaID());
	super.beforeBuildWhere();

	}
}
