/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.shipTo.model;

import java.math.BigDecimal;
/**
 * Generated code. Do not modify in this file.
 */
public class ShipTo_DsParam {

	public static final String f_contractStatus = "contractStatus";
	public static final String f_contractDealType = "contractDealType";
	public static final String f_approvalStatus = "approvalStatus";
	public static final String f_tenderSource = "tenderSource";
	public static final String f_remarks = "remarks";
	public static final String f_unitParameter = "unitParameter";
	public static final String f_totalVolume = "totalVolume";

	private String contractStatus;

	private String contractDealType;

	private String approvalStatus;

	private String tenderSource;

	private String remarks;

	private String unitParameter;

	private BigDecimal totalVolume;

	public String getContractStatus() {
		return this.contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

	public String getContractDealType() {
		return this.contractDealType;
	}

	public void setContractDealType(String contractDealType) {
		this.contractDealType = contractDealType;
	}

	public String getApprovalStatus() {
		return this.approvalStatus;
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public String getTenderSource() {
		return this.tenderSource;
	}

	public void setTenderSource(String tenderSource) {
		this.tenderSource = tenderSource;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getUnitParameter() {
		return this.unitParameter;
	}

	public void setUnitParameter(String unitParameter) {
		this.unitParameter = unitParameter;
	}

	public BigDecimal getTotalVolume() {
		return this.totalVolume;
	}

	public void setTotalVolume(BigDecimal totalVolume) {
		this.totalVolume = totalVolume;
	}
}
