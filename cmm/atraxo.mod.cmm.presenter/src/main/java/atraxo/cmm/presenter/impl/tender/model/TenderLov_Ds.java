/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.tender.model;

import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Tender.class)
public class TenderLov_Ds extends AbstractSubsidiaryLov_Ds<Tender> {

	public static final String ALIAS = "cmm_TenderLov_Ds";

	public static final String F_HOLDERID = "holderId";
	public static final String F_HOLDERCODE = "holderCode";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_VERSION = "version";

	@DsField(join = "left", path = "holder.id")
	private Integer holderId;

	@DsField(join = "left", path = "holder.code")
	private String holderCode;

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private Long version;

	/**
	 * Default constructor
	 */
	public TenderLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public TenderLov_Ds(Tender e) {
		super(e);
	}

	public Integer getHolderId() {
		return this.holderId;
	}

	public void setHolderId(Integer holderId) {
		this.holderId = holderId;
	}

	public String getHolderCode() {
		return this.holderCode;
	}

	public void setHolderCode(String holderCode) {
		this.holderCode = holderCode;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
}
