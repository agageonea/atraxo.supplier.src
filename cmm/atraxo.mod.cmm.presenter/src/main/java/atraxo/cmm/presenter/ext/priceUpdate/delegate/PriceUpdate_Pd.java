package atraxo.cmm.presenter.ext.priceUpdate.delegate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.priceUpdate.IPriceUpdateService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdate;
import atraxo.cmm.presenter.impl.priceUpdate.model.PriceUpdateCategories_Ds;
import atraxo.cmm.presenter.impl.priceUpdate.model.PriceUpdate_Ds;
import atraxo.cmm.presenter.impl.priceUpdate.model.PriceUpdate_DsParam;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.ext.bpm.WorkflowStartResult;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.business.ext.bpm.WorkflowMsgConstants;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.exceptions.workflow.ClaimedWorkflowException;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.action.query.IFilterRule;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.action.result.IDsMarshaller;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author vhojda
 */
public class PriceUpdate_Pd extends AbstractPresenterDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(PriceUpdate_Pd.class);

	/**
	 * @param ds
	 * @throws Exception
	 */
	public void approve(PriceUpdate_Ds ds, PriceUpdate_DsParam param) throws Exception {
		this.complete(ds, true, param.getApprovalNote());
	}

	/**
	 * @param ds
	 * @throws Exception
	 */
	public void reject(PriceUpdate_Ds ds, PriceUpdate_DsParam param) throws Exception {
		this.complete(ds, false, param.getApprovalNote());
	}

	/**
	 * @param ds
	 * @throws Exception
	 */
	public void verify(PriceUpdate_Ds ds, PriceUpdate_DsParam params) throws Exception {
		if (!params.getSelectedAll()) {
			return;
		}
		List<PriceUpdateCategories_Ds> filteredPriceUpdates = this.find(params);

		List<String> list = new ArrayList<>();
		if (StringUtils.isNotEmpty(params.getUnSelectedCategories())) {
			String[] arr = params.getUnSelectedCategories().split(",");
			list = Arrays.asList(arr);
		}
		Set<String> currencies = new HashSet<>();
		for (PriceUpdateCategories_Ds pu : filteredPriceUpdates) {
			if (pu.getId() == null || !list.contains(pu.getId().toString())) {
				currencies.add(pu.getCurrencyCode());
			}
		}
		if (currencies.size() > 1) {
			String[] array = currencies.stream().toArray(String[]::new);
			throw new BusinessException(CmmErrorCode.INVALID_CONTRACT_FOR_PRICE_UPDATE,
					String.format(CmmErrorCode.INVALID_CONTRACT_FOR_PRICE_UPDATE.getErrMsg(), StringUtils.join(array, ",")));
		}

	}

	private List<PriceUpdateCategories_Ds> find(PriceUpdate_DsParam params) throws Exception {
		IDsService<PriceUpdateCategories_Ds, Object, Object> service = this.findDsService(PriceUpdateCategories_Ds.class);
		IDsMarshaller<PriceUpdateCategories_Ds, Object, Object> marshaller = service.createMarshaller(IDsMarshaller.JSON);

		Object stdFilter = marshaller.readFilterFromString(params.getStandardFilter());
		IQueryBuilder<PriceUpdateCategories_Ds, Object, Object> builder = service.createQueryBuilder().addFilter(stdFilter);
		List<IFilterRule> filterRules = marshaller.readFilterRules(params.getFilter());
		if (!CollectionUtils.isEmpty(filterRules)) {
			builder.addFilterRules(filterRules);
		}
		List<PriceUpdateCategories_Ds> filteredPriceUpdates = service.find(builder);
		return filteredPriceUpdates;
	}

	/**
	 * @param dsList
	 * @param params
	 * @throws Exception
	 */
	public void submitForApproval(PriceUpdate_Ds ds, PriceUpdate_DsParam params) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START submitForApproval()");
		}

		// assume false, ( the operation didn't complete ok )
		params.setSubmitForApprovalResult(false);

		// first, check the 'Use price update publishing approval workflow' system parameter (SYSPRICEUPDATEAPPROVAL)
		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		boolean useWorkflow = paramSrv.getPriceUpdateApprovalWorkflow();
		if (!useWorkflow) {
			params.setSubmitForApprovalDescription(CmmErrorCode.PRICE_UPDATE_WORKFLOW_NO_SYS_PARAM.getErrMsg());
			return;
		}

		// if ok, proceed forward and retrieve the price update object from DB
		IPriceUpdateService priceUpdateService = this.getApplicationContext().getBean(IPriceUpdateService.class);
		PriceUpdate priceUpdate = null;
		try {
			priceUpdate = priceUpdateService.findByBusiness(ds.getId());
		} catch (Exception e) {
			LOGGER.warn("WARNING:could not locate PriceUpdate for ID " + ds.getId(), e);
			params.setSubmitForApprovalDescription(
					String.format(CmmErrorCode.PRICE_UPDATE_WORKFLOW_ENTITY_NOT_EXISTING.getErrMsg(), ds.getId().toString()));
		}

		// if object exists, proceed
		if (priceUpdate != null) {

			// check for concurrency (maybe object changed in the meantime)
			if (this.canSubmitForApproval(priceUpdate)) {

				IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean("workflowBpmManager");

				// submit for approval for the entity(change status, fill in History)
				priceUpdateService.submitForApproval(priceUpdate, BidApprovalStatus._AWAITING_APPROVAL_,
						WorkflowMsgConstants.MSG_STARTED_FOR_APPROVAL_OK, params.getApprovalNote());

				// start workflow after adding workflow parameters
				Map<String, Object> vars = this.getWorkflowVariablesMap(params, priceUpdate);
				WorkflowStartResult result = workflowBpmManager.startWorkflow(WorkflowNames.PRICE_UPDATE_APPROVAL.getWorkflowName(), vars,
						priceUpdate.getSubsidiaryId(), priceUpdate);
				if (result.isStarted()) {
					params.setSubmitForApprovalResult(true);
				} else {
					params.setSubmitForApprovalDescription(CmmErrorCode.PRICE_UPDATE_WORKFLOW_NOT_START_REASON.getErrMsg() + result.getReason());

					// submit for approval for the entity(change status, fill in History)
					priceUpdateService.submitForApproval(priceUpdateService.findById(priceUpdate.getId()), BidApprovalStatus._REJECTED_,
							WorkflowMsgConstants.MSG_STARTED_FOR_APPROVAL_NOT_OK, result.getReason());
				}

			} else {
				params.setSubmitForApprovalDescription(CmmErrorCode.PRICE_UPDATE_WORKFLOW_CONCURRENCY_NOT_START_ERROR.getErrMsg());
			}

		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END submitForApproval()");
		}
	}

	/**
	 * @param params
	 * @param priceUpdate
	 * @return
	 */
	private Map<String, Object> getWorkflowVariablesMap(PriceUpdate_DsParam params, PriceUpdate priceUpdate) {
		Map<String, Object> vars = new HashMap<>();
		vars.put(WorkflowVariablesConstants.VAR_WKF_ENTITY_DESCRIPTION, "Price update approval request for "
				+ priceUpdate.getPriceCategory().getName() + (priceUpdate.getLocation() != null ? " at " + priceUpdate.getLocation().getName() : ""));
		vars.put(WorkflowVariablesConstants.PRICE_UPDATE_ID_VAR, priceUpdate.getId() + "");
		vars.put(WorkflowVariablesConstants.APPROVE_NOTE, params.getApprovalNote());
		vars.put(WorkflowVariablesConstants.FULLNAME_REQUESTER_VAR, this.getFullNameOfRequester());

		String attachmentsArray = params.getSelectedAttachments();
		if (!StringUtils.isEmpty(attachmentsArray)) {
			String attachTrimmed = attachmentsArray.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "").trim();
			if (!StringUtils.isEmpty(attachTrimmed)) {
				String[] attachmentIdentifiers = attachTrimmed.split(",");
				vars.put(WorkflowVariablesConstants.VAR_WKF_EMAIL_ATTACHMENTS, Arrays.asList(attachmentIdentifiers));
			}
		}
		return vars;
	}

	/**
	 * Re-check the conditions that allows a user to submit for approval a price update
	 *
	 * @param outInvoice
	 * @return
	 */
	private boolean canSubmitForApproval(PriceUpdate price) {
		boolean canSubmit = false;
		if (!price.getPublished()
				&& (price.getApprovalStatus().equals(BidApprovalStatus._NEW_) || price.getApprovalStatus().equals(BidApprovalStatus._REJECTED_))) {
			canSubmit = true;
		}
		return canSubmit;
	}

	/**
	 * @return
	 */
	private String getFullNameOfRequester() {

		IUserSuppService userService = this.getApplicationContext().getBean(IUserSuppService.class);

		UserSupp user = userService.findByLogin(Session.user.get().getLoginName());
		return user.getFirstName() + " " + user.getLastName();
	}

	/**
	 * @param ds
	 * @param accept
	 * @param note
	 * @throws Exception
	 */
	private void complete(PriceUpdate_Ds ds, boolean accept, String note) throws Exception {
		IPriceUpdateService puSrv = (IPriceUpdateService) this.findEntityService(PriceUpdate.class);
		IWorkflowInstanceEntityService wieSrc = (IWorkflowInstanceEntityService) this.findEntityService(WorkflowInstanceEntity.class);
		PriceUpdate e = puSrv.findById(ds.getId());
		if (BidApprovalStatus._APPROVED_.equals(e.getApprovalStatus()) || BidApprovalStatus._REJECTED_.equals(e.getApprovalStatus())) {
			throw new BusinessException(CmmErrorCode.PRICE_UPDATE_WORKFLOW_APPROVED, CmmErrorCode.PRICE_UPDATE_WORKFLOW_APPROVED.getErrMsg());
		}
		IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean("workflowBpmManager");
		WorkflowInstanceEntity wie = wieSrc.findByBusinessKey(WorkflowNames.PRICE_UPDATE_APPROVAL.getWorkflowName(), e.getId(),
				e.getClass().getSimpleName());
		try {
			workflowBpmManager.claimCompleteTask(wie.getWorkflowInstance().getId(), accept, note);
		} catch (ClaimedWorkflowException e1) {
			throw e1;
		} catch (Exception ex) {
			workflowBpmManager.terminateWorkflow(wie.getWorkflowInstance().getId(),
					CmmErrorCode.PRICE_UPDATE_WORKFLOW_ERROR.getErrMsg() + ":" + ex.getMessage());
			throw new BusinessException(CmmErrorCode.PRICE_UPDATE_WORKFLOW_ERROR,
					CmmErrorCode.PRICE_UPDATE_WORKFLOW_ERROR.getErrMsg() + ":" + ex.getMessage(), ex);
		}
	}
}
