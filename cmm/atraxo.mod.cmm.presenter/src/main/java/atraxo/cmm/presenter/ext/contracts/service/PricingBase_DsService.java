/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.contracts.service;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IPricingBaseService;
import atraxo.cmm.business.api.ext.contracts.IPriceBuilder;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.presenter.ext.prices.service.validator.PricingBaseValidator;
import atraxo.cmm.presenter.ext.prices.service.validator.QuotationValidator;
import atraxo.cmm.presenter.impl.contracts.model.PricingBase_Ds;
import atraxo.cmm.presenter.impl.contracts.model.PricingBase_DsParam;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class PricingBase_DsService extends AbstractEntityDsService<PricingBase_Ds, PricingBase_Ds, PricingBase_DsParam, PricingBase>
		implements IDsService<PricingBase_Ds, PricingBase_Ds, PricingBase_DsParam> {

	@Autowired
	private IPricingBaseService pbService;
	@Autowired
	private PricingBaseValidator validatorSrv;
	@Autowired
	private QuotationValidator quotationValidatorSrv;
	@Autowired
	private IPriceBuilder builderService;

	@Override
	protected void preUpdateAfterEntity(PricingBase_Ds ds, PricingBase e, PricingBase_DsParam params) throws Exception {
		super.preUpdateAfterEntity(ds, e, params);
		if (this.pbService.isIndex(e)) {
			this.quotationValidatorSrv.validateQuotation(ds, e);
		}
		this.validatorSrv.validatePricingBaseOnUpdate(e);
	}

	@Override
	protected void preInsert(PricingBase_Ds ds, PricingBase e, PricingBase_DsParam params) throws Exception {
		super.preInsert(ds, e, params);
		if (this.pbService.isIndex(e)) {
			this.quotationValidatorSrv.validateQuotation(ds, e);
		}
		this.validatorSrv.validatePricingBaseOnInsert(ds, e);
		this.builderService.extendsNotContinous(e);
		this.builderService.extendsToMargins(e);
		e.setOrderIndex(e.getContract().getPriceCategories().size() + 1);
		this.builderService.buildPricingBases(e);
	}
}
