/**
 *
 */
package atraxo.cmm.presenter.ext.contracts.delegate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.ext.datahub.IDataHubClientRequestService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ws.tender.client.DataHubSoapClient;
import atraxo.cmm.business.ws.tender.transformer.TenderBidTransformer;
import atraxo.cmm.domain.ext.tender.DataHubRequestType;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.TenderSource;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.cmm.presenter.impl.contracts.model.TenderBid_Ds;
import atraxo.cmm.presenter.impl.contracts.model.TenderBid_DsParam;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.ext.bpm.WorkflowStartResult;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.notes.INotesService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.api.validationMessage.IValidationMessageService;
import atraxo.fmbas.business.ext.bpm.WorkflowMsgConstants;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.business.ext.util.LambdaUtil;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.notes.Notes;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.validationMessage.ValidationMessage;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBid;

/**
 * @author zspeter
 */
public class SubmitTenderBid_Pd extends AbstractTenderBid_Pd {

	private static final String BEAN_BID_TRANSFORMER = "tenderBidTransformer";

	private static final Logger LOGGER = LoggerFactory.getLogger(SubmitTenderBid_Pd.class);

	/**
	 * Submits the bid for approval using the workflow process
	 *
	 * @param dsList
	 * @param params
	 * @throws Exception
	 */
	public void submitForApproval(List<TenderBid_Ds> dsList, TenderBid_DsParam params) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START submitForApproval()");
		}

		INotesService notesService = (INotesService) this.findEntityService(Notes.class);
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean("workflowBpmManager");

		List<Notes> notes = new ArrayList<>();
		List<String> dbSyncValidation = new ArrayList<>();
		List<String> dbWorkflowValidation = new ArrayList<>();

		List<Contract> bids = new ArrayList<>();
		for (TenderBid_Ds ds : dsList) {
			Contract bid = contractService.findById(ds.getId());
			// Check for concurency

			if (bid == null || !this.canSubmitForApproval(bid)) {
				dbSyncValidation.add(this.getBidIdentificationMessage(ds));
			} else {
				bids.add(bid);
			}
		}

		Collection<String> invalidBidIds = this.getInvalidBidIds(bids);
		bids = bids.stream().filter(bid -> !invalidBidIds.contains(bid.getRefid())).collect(Collectors.toList());
		params.setInvalidBidIds(invalidBidIds.stream().collect(Collectors.joining(",")));

		for (Contract bid : bids) {

			// submit for approval for the entity(change status, fill in History)
			contractService.submitForApproval(bid, BidApprovalStatus._AWAITING_APPROVAL_, WorkflowMsgConstants.MSG_STARTED_FOR_APPROVAL_OK,
					params.getApprovalNote());

			// start workflow after adding workflow parameters
			Map<String, Object> vars = this.getWorkflowVariablesMap(params, bid);
			WorkflowStartResult result = workflowBpmManager.startWorkflow(WorkflowNames.BID_APPROVAL.getWorkflowName(), vars, bid.getSubsidiaryId(),
					bid);
			if (!result.isStarted()) {
				dbWorkflowValidation.add(this.getBidIdentificationMessage(bid, result.getReason()));
				// submit for approval for the entity(change status, fill in History)
				contractService.submitForApproval(contractService.findById(bid.getId()), BidApprovalStatus._REJECTED_,
						WorkflowMsgConstants.MSG_STARTED_FOR_APPROVAL_NOT_OK, result.getReason());
			}
			// Add note to bid
			Notes note = new Notes();
			note.setObjectId(bid.getId());
			note.setObjectType(Contract.class.getSimpleName());
			note.setNotes(params.getApprovalNote());
			notes.add(note);
		}

		notesService.insert(notes);

		// fill in the result
		this.setupUILabelsMessages(dsList.size(), dbSyncValidation.size(), invalidBidIds.size(), params);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END submitForApproval()");
		}
	}

	private void setupUILabelsMessages(int totalBids, int bidsFailedConcurency, int invalidBidsNumber, TenderBid_DsParam params) {
		String succesfullySubmitedLabelMessage = String.format(CmmErrorCode.BID_APPROVAL_WORKFLOW_RESULT_SUBMIT.getErrMsg(),
				totalBids - bidsFailedConcurency - invalidBidsNumber, Integer.toString(totalBids));
		params.setSubmitForApprovalSuccesfullySubmitted(succesfullySubmitedLabelMessage);

		if (bidsFailedConcurency != 0) {
			String concurencyCheckFailedBidsLabelMessage = String.format(CmmErrorCode.BID_APPROVAL_WORKFLOW_RESULT_CONCURENCY_CHECK.getErrMsg(),
					bidsFailedConcurency, Integer.toString(totalBids));
			params.setSubmitForApprovalConcurencyCheckFail(concurencyCheckFailedBidsLabelMessage);
		}

		if (invalidBidsNumber != 0) {
			String invalidBidsLabelMessage = String.format(CmmErrorCode.BID_APPROVAL_WORKFLOW_RESULT_INVALID_BIDS.getErrMsg(), invalidBidsNumber);
			params.setSubmitForApprovalInvalidBids(invalidBidsLabelMessage);
		}
	}

	/**
	 * Submits all the selected bids
	 *
	 * @param tenderBidDsList
	 * @throws Exception
	 */
	public void submitSelected(List<TenderBid_Ds> tenderBidDsList, TenderBid_DsParam param) throws Exception {
		int totalNumberOfBids = tenderBidDsList.size();

		IContractService contractService = (IContractService) this.findEntityService(Contract.class);

		List<String> dbSyncValidation = new ArrayList<>();
		List<Contract> bids = new ArrayList<>();

		for (TenderBid_Ds ds : tenderBidDsList) {
			Contract bid = contractService.findById(ds.getId());
			// Check for concurency
			if (bid == null || !this.canSubmitSelectedToIssuer(bid)) {
				dbSyncValidation.add(this.getBidIdentificationMessage(ds));
			} else {
				bids.add(bid);
			}
		}

		Collection<String> invalidBidIds = this.getInvalidBidIds(bids);
		bids = bids.stream().filter(bid -> !invalidBidIds.contains(bid.getRefid())).collect(Collectors.toList());
		param.setInvalidBidIds(invalidBidIds.stream().collect(Collectors.joining(",")));

		// Submit bids to DataHub
		this.submitBids(bids);

		this.setupUILabelsMessages(totalNumberOfBids, dbSyncValidation.size(), invalidBidIds.size(), param);
	}

	/**
	 * Submits all the failed transmission bids.
	 *
	 * @param contract
	 * @throws Exception
	 */
	public void submitFailed(TenderBid_Ds tenderBidDs, TenderBid_DsParam param) throws Exception {
		// Check if interface exists and is enabled
		this.checkInterface(ExtInterfaceNames.FUEL_TENDER_BID_INTERFACE.getValue());

		Map<String, Object> params = new HashMap<>();
		params.put("bidStatus", BidStatus._DRAFT_);
		params.put("bidTransmissionStatus", TransmissionStatus._FAILED_);

		// filter bids list
		List<Contract> bids = this.getBidsForSubmit(params);
		int invalidBids = 0;
		int totalNumberOfBids = bids.size();
		if (!CollectionUtils.isEmpty(bids)) {
			Collection<String> ids = this.getInvalidBidIds(bids);
			param.setInvalidBidIds(ids.stream().collect(Collectors.joining(",")));
			bids = bids.stream().filter(bid -> !ids.contains(bid.getRefid())).collect(Collectors.toList());
			invalidBids = ids.size();
		}
		// Submit bids to DataHub
		this.submitBids(bids);

		this.setupUILabelsMessages(totalNumberOfBids, 0, invalidBids, param);
	}

	/**
	 * Submits all the selected bids.
	 *
	 * @param contract
	 * @throws Exception
	 */
	public void submitAll(TenderBid_Ds tenderBidDs, TenderBid_DsParam param) throws Exception {
		// Check if interface exists and is enabled
		this.checkInterface(ExtInterfaceNames.FUEL_TENDER_BID_INTERFACE.getValue());

		ISystemParameterService paramService = (ISystemParameterService) this.findEntityService(SystemParameter.class);

		Map<String, Object> params = new HashMap<>();
		params.put("bidStatus", LambdaUtil.filterEnum(BidStatus.class, a -> BidStatus._DRAFT_.equals(a) || BidStatus._SUBMITTED_.equals(a)));
		params.put("bidTransmissionStatus",
				LambdaUtil.filterEnum(TransmissionStatus.class, a -> TransmissionStatus._NEW_.equals(a) || TransmissionStatus._FAILED_.equals(a)));
		if (paramService.getBidApprovalWorkflow()) {
			params.put("bidApprovalStatus", BidApprovalStatus._APPROVED_);
		}

		// filter bids list
		List<Contract> bids = this.getBidsForSubmit(params);

		Collection<String> ids = this.getInvalidBidIds(bids);
		int bidsNumber = bids.size();
		param.setInvalidBidIds(ids.stream().collect(Collectors.joining(",")));
		bids = bids.stream().filter(bid -> !ids.contains(bid.getRefid())).collect(Collectors.toList());
		// Submit bids to DataHub
		this.submitBids(bids);

		this.setupUILabelsMessages(bidsNumber, 0, ids.size(), param);
	}

	private Collection<String> getInvalidBidIds(List<Contract> bids) throws Exception {
		IValidationMessageService service = (IValidationMessageService) this.findEntityService(ValidationMessage.class);
		return service.getInvalidObjectIds(Contract.class.getSimpleName(), bids);
	}

	private List<Contract> getBidsForSubmit(Map<String, Object> params) throws Exception {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		List<Contract> bids = contractService.findEntitiesByAttributes(params);
		bids = bids.stream()
				.filter(bid -> TenderSource._IMPORTED_.equals(bid.getBidTenderIdentification().getSource())
						&& !TenderStatus._CANCELED_.equals(bid.getBidTenderIdentification().getStatus())
						&& !BiddingStatus._FINISHED_.equals(bid.getBidTenderLocation().getLocationBiddingStatus()) && this.isValid(bid))
				.collect(Collectors.toList());
		return bids;
	}

	/**
	 * Checks if TenderLocation or FuelReceivers from TenderLocation are still valid
	 *
	 * @param bid
	 * @return
	 */
	private boolean isValid(Contract bid) {
		boolean isValid = true;

		TenderLocation location = bid.getBidTenderLocation();
		if (!location.getIsValid()) {
			isValid = false;
		} else {
			List<TenderLocationAirlines> airlinesList = new ArrayList<>();
			for (TenderLocationAirlines airline : location.getTenderLocationAirlines()) {
				if (!airline.getIsValid()) {
					airlinesList.add(airline);
				}
			}

			if (airlinesList.size() == location.getTenderLocationAirlines().size()) {
				isValid = false;
			}
		}
		return isValid;
	}

	private void checkInterface(String interfaceName) throws Exception {
		try {
			IExternalInterfaceService interfaceService = (IExternalInterfaceService) this.findEntityService(ExternalInterface.class);
			ExternalInterface extInterface = interfaceService.findByName(interfaceName);

			if (!extInterface.getEnabled()) {
				throw new BusinessException(BusinessErrorCode.OUT_INTERFACE_DISABLED_WITH_DETAILS, String.format(
						BusinessErrorCode.OUT_INTERFACE_DISABLED_WITH_DETAILS.getErrMsg(), ExtInterfaceNames.FUEL_TENDER_BID_INTERFACE.getValue()));
			}
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw nre;
			}
			LOGGER.warn("Could not find interface " + interfaceName, nre);

			throw new BusinessException(BusinessErrorCode.INTERFACE_NO_INTERFACE_WITH_NAME_AND_CLIENT_IDENTIFIER,
					String.format(BusinessErrorCode.INTERFACE_NO_INTERFACE_WITH_NAME_AND_CLIENT_IDENTIFIER.getErrMsg(),
							ExtInterfaceNames.FUEL_TENDER_BID_INTERFACE.getValue(), Session.user.get().getClientCode()));
		}
	}

	/**
	 * @param params
	 * @param bid
	 * @return
	 */
	private Map<String, Object> getWorkflowVariablesMap(TenderBid_DsParam params, Contract bid) {
		Map<String, Object> vars = new HashMap<>();
		vars.put(WorkflowVariablesConstants.VAR_WKF_ENTITY_DESCRIPTION,
				"Bid for " + bid.getBidTenderIdentification().getHolder().getName() + " / " + bid.getBidTenderLocation().getLocation().getName());
		vars.put(WorkflowVariablesConstants.BID_ID_VAR, bid.getId().toString());
		vars.put(WorkflowVariablesConstants.APPROVE_NOTE, params.getApprovalNote());
		vars.put(WorkflowVariablesConstants.FULLNAME_REQUESTER_VAR, this.getFullNameOfRequester());

		String attachmentsArray = params.getSelectedAttachments();
		if (!StringUtils.isEmpty(attachmentsArray)) {
			String attachTrimmed = attachmentsArray.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "").trim();
			if (!StringUtils.isEmpty(attachTrimmed)) {
				String[] attachmentIdentifiers = attachTrimmed.split(",");
				vars.put(WorkflowVariablesConstants.VAR_WKF_EMAIL_ATTACHMENTS, Arrays.asList(attachmentIdentifiers));
			}
		}
		return vars;
	}

	/**
	 * Re-check the conditions that allows a user to submit for approval a bid
	 *
	 * @param bid
	 * @return
	 */
	private boolean canSubmitForApproval(Contract bid) {
		boolean canSubmit = true;
		if (BidStatus._DRAFT_.equals(bid.getBidStatus())) {
			if (!bid.getBidApprovalStatus().equals(BidApprovalStatus._NEW_) && !bid.getBidApprovalStatus().equals(BidApprovalStatus._REJECTED_)) {
				canSubmit = false;
			}
		} else {
			canSubmit = false;
		}
		return canSubmit;
	}

	private boolean canSubmitSelectedToIssuer(Contract bid) throws Exception {
		ISystemParameterService systemParamsService = (ISystemParameterService) this.findEntityService(SystemParameter.class);

		boolean canSubmit = false;
		boolean isBidDraft = BidStatus._DRAFT_.equals(bid.getBidStatus());
		boolean isNotTransmited = TransmissionStatus._NEW_.equals(bid.getBidTransmissionStatus())
				|| TransmissionStatus._FAILED_.equals(bid.getBidTransmissionStatus());
		boolean isTenderNew = TenderStatus._NEW_.equals(bid.getBidTenderIdentification().getStatus())
				|| TenderStatus._UPDATED_.equals(bid.getBidTenderIdentification().getStatus());
		boolean isBiddingNotFinished = !BiddingStatus._FINISHED_.equals(bid.getBidTenderLocation().getLocationBiddingStatus());

		if (isBidDraft && isNotTransmited && isTenderNew && isBiddingNotFinished) {
			canSubmit = true;
			if (systemParamsService.getBidApprovalWorkflow() && !BidApprovalStatus._APPROVED_.equals(bid.getBidApprovalStatus())) {
				canSubmit = false;
			}
		}

		return canSubmit;
	}

	/**
	 * @return
	 */
	private String getFullNameOfRequester() {
		IUserSuppService userService = this.getApplicationContext().getBean(IUserSuppService.class);
		UserSupp user = userService.findByLogin(Session.user.get().getLoginName());

		return user.getFirstName() + " " + user.getLastName();

	}

	private void submitBids(List<Contract> bids) throws Exception {
		if (!bids.isEmpty()) {
			IExternalInterfaceService interfaceService = (IExternalInterfaceService) this.findEntityService(ExternalInterface.class);
			ExternalInterface externalInterface = interfaceService.findByName(ExtInterfaceNames.FUEL_TENDER_BID_INTERFACE.getValue());
			ExtInterfaceParameters extInterfaceParams = new ExtInterfaceParameters(externalInterface.getParams());

			TenderBidTransformer transformer = (TenderBidTransformer) this.getApplicationContext().getBean(BEAN_BID_TRANSFORMER);
			ExternalInterfaceHistoryFacade historyFacade = (ExternalInterfaceHistoryFacade) this.getApplicationContext().getBean(BEAN_HISTORY_FACADE);
			ExternalInterfaceMessageHistoryFacade messageHistoryFacade = (ExternalInterfaceMessageHistoryFacade) this.getApplicationContext()
					.getBean(BEAN_MESSAGE_HISTORY_FACADE);

			if (!externalInterface.getEnabled()) {
				throw new BusinessException(BusinessErrorCode.OUT_INTERFACE_DISABLED_WITH_DETAILS, String.format(
						BusinessErrorCode.OUT_INTERFACE_DISABLED_WITH_DETAILS.getErrMsg(), ExtInterfaceNames.FUEL_TENDER_BID_INTERFACE.getValue()));
			}

			// Initiate bids transmission
			IContractService contractService = (IContractService) this.findEntityService(Contract.class);

			// Update bids for tranmission initated
			for (Contract bid : bids) {
				bid.setBidTransmissionStatus(TransmissionStatus._IN_PROGRESS_);
			}
			contractService.updateWithoutBusinessLogic(bids);

			// Prepare params for datahub soap client
			@SuppressWarnings("unchecked")
			IDataHubClientRequestService<Contract, FuelTenderBid> bidService = (IDataHubClientRequestService<Contract, FuelTenderBid>) this
					.getApplicationContext().getBean(BID_SERVICE);
			DataHubSoapClient<Contract, FuelTenderBid> dataHubSoapClient = new DataHubSoapClient<>(externalInterface, extInterfaceParams, bids,
					historyFacade, messageHistoryFacade, transformer, FuelTenderBid.class, DataHubRequestType.SINGLE_REQUEST);
			dataHubSoapClient.setRequestService(bidService);
			dataHubSoapClient.start(1);
		}
	}

}
