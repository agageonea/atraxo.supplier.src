/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.tender.model;

/**
 * Generated code. Do not modify in this file.
 */
public class OpenTender_DsParam {

	public static final String f_selectedLocation = "selectedLocation";

	private Integer selectedLocation;

	public Integer getSelectedLocation() {
		return this.selectedLocation;
	}

	public void setSelectedLocation(Integer selectedLocation) {
		this.selectedLocation = selectedLocation;
	}
}
