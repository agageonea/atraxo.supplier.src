/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.shipTo.model;

import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.Period;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ShipTo.class)
@RefLookups({
		@RefLookup(refId = ShipTo_Ds.F_CONTRACTID, namedQuery = Contract.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = ShipTo_Ds.F_CONTRACTCODE)}),
		@RefLookup(refId = ShipTo_Ds.F_CUSTOMERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = ShipTo_Ds.F_CUSTOMERCODE)})})
public class ShipTo_Ds extends AbstractDs_Ds<ShipTo> {

	public static final String ALIAS = "cmm_ShipTo_Ds";

	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_ALLOCATEDVOLUME = "allocatedVolume";
	public static final String F_ADJUSTEDVOLUME = "adjustedVolume";
	public static final String F_ACTUALVOLUME = "actualVolume";
	public static final String F_OFFEREDVOLUME = "offeredVolume";
	public static final String F_TENDERBIDVOLUME = "tenderBidVolume";
	public static final String F_CONTRACTID = "contractId";
	public static final String F_CONTRACTCODE = "contractCode";
	public static final String F_CONTRACTPERIOD = "contractPeriod";
	public static final String F_SHIPTOAPPROVALSTATUS = "shipToApprovalStatus";
	public static final String F_BIDSTATUS = "bidStatus";
	public static final String F_SETTLEMENTUNITCODE = "settlementUnitCode";
	public static final String F_SETTLEMENTUNITID = "settlementUnitId";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_CUSTOMERNAME = "customerName";
	public static final String F_PERCENTCOVERED = "percentCovered";

	@DsField
	private Date validFrom;

	@DsField
	private Date validTo;

	@DsField
	private BigDecimal allocatedVolume;

	@DsField
	private BigDecimal adjustedVolume;

	@DsField
	private BigDecimal actualVolume;

	@DsField
	private BigDecimal offeredVolume;

	@DsField
	private BigDecimal tenderBidVolume;

	@DsField(join = "left", path = "contract.id")
	private Integer contractId;

	@DsField(join = "left", path = "contract.code")
	private String contractCode;

	@DsField(join = "left", path = "contract.period")
	private Period contractPeriod;

	@DsField(join = "left", path = "contract.shipToApprovalStatus")
	private BidApprovalStatus shipToApprovalStatus;

	@DsField(join = "left", path = "contract.bidStatus")
	private BidStatus bidStatus;

	@DsField(join = "left", path = "contract.settlementUnit.code")
	private String settlementUnitCode;

	@DsField(join = "left", path = "contract.settlementUnit.id")
	private Integer settlementUnitId;

	@DsField(join = "left", path = "customer.id")
	private Integer customerId;

	@DsField(join = "left", path = "customer.code")
	private String customerCode;

	@DsField(join = "left", path = "customer.name")
	private String customerName;

	@DsField(fetch = false)
	private BigDecimal percentCovered;

	/**
	 * Default constructor
	 */
	public ShipTo_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ShipTo_Ds(ShipTo e) {
		super(e);
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public BigDecimal getAllocatedVolume() {
		return this.allocatedVolume;
	}

	public void setAllocatedVolume(BigDecimal allocatedVolume) {
		this.allocatedVolume = allocatedVolume;
	}

	public BigDecimal getAdjustedVolume() {
		return this.adjustedVolume;
	}

	public void setAdjustedVolume(BigDecimal adjustedVolume) {
		this.adjustedVolume = adjustedVolume;
	}

	public BigDecimal getActualVolume() {
		return this.actualVolume;
	}

	public void setActualVolume(BigDecimal actualVolume) {
		this.actualVolume = actualVolume;
	}

	public BigDecimal getOfferedVolume() {
		return this.offeredVolume;
	}

	public void setOfferedVolume(BigDecimal offeredVolume) {
		this.offeredVolume = offeredVolume;
	}

	public BigDecimal getTenderBidVolume() {
		return this.tenderBidVolume;
	}

	public void setTenderBidVolume(BigDecimal tenderBidVolume) {
		this.tenderBidVolume = tenderBidVolume;
	}

	public Integer getContractId() {
		return this.contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public String getContractCode() {
		return this.contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public Period getContractPeriod() {
		return this.contractPeriod;
	}

	public void setContractPeriod(Period contractPeriod) {
		this.contractPeriod = contractPeriod;
	}

	public BidApprovalStatus getShipToApprovalStatus() {
		return this.shipToApprovalStatus;
	}

	public void setShipToApprovalStatus(BidApprovalStatus shipToApprovalStatus) {
		this.shipToApprovalStatus = shipToApprovalStatus;
	}

	public BidStatus getBidStatus() {
		return this.bidStatus;
	}

	public void setBidStatus(BidStatus bidStatus) {
		this.bidStatus = bidStatus;
	}

	public String getSettlementUnitCode() {
		return this.settlementUnitCode;
	}

	public void setSettlementUnitCode(String settlementUnitCode) {
		this.settlementUnitCode = settlementUnitCode;
	}

	public Integer getSettlementUnitId() {
		return this.settlementUnitId;
	}

	public void setSettlementUnitId(Integer settlementUnitId) {
		this.settlementUnitId = settlementUnitId;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public BigDecimal getPercentCovered() {
		return this.percentCovered;
	}

	public void setPercentCovered(BigDecimal percentCovered) {
		this.percentCovered = percentCovered;
	}
}
