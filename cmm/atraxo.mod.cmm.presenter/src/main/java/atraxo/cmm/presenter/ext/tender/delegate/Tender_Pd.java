package atraxo.cmm.presenter.ext.tender.delegate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.tender.ITenderLocationService;
import atraxo.cmm.business.api.tender.ITenderService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.TenderSource;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.presenter.impl.tender.model.Tender_Ds;
import atraxo.cmm.presenter.impl.tender.model.Tender_DsParam;
import atraxo.fmbas.business.ext.util.StringUtil;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author mbotorogea
 */
public class Tender_Pd extends AbstractPresenterDelegate {

	public void publish(List<Tender_Ds> tenderDs) throws Exception {
		StringBuilder validationMessages = new StringBuilder();

		List<String> dbSyncValidation = new ArrayList<>();
		List<String> noLocationValidation = new ArrayList<>();
		List<String> volumeValidation = new ArrayList<>();

		ITenderService tenderService = (ITenderService) this.findEntityService(Tender.class);
		ITenderLocationService tenderLocationService = (ITenderLocationService) this.findEntityService(TenderLocation.class);

		for (Tender_Ds ds : tenderDs) {
			Tender tender = tenderService.findById(ds.getId());

			Boolean volumeValidationError = Boolean.FALSE;

			// Check for concurency
			if (tender == null || !TenderStatus._DRAFT_.equals(tender.getStatus())) {
				dbSyncValidation.add(ds.getCode());
				continue;
			}

			// Check for locations
			if (CollectionUtils.isEmpty(tenderLocationService.findByTender(tender))) {
				noLocationValidation.add(tender.getCode());
				continue;
			}

			// Check for tender volume
			if (tenderService.getTotalVolume(tender).compareTo(BigDecimal.ZERO) == 0) {
				volumeValidationError = Boolean.TRUE;
				volumeValidation.add(tender.getCode());
			}

			// Check for tender locations volume
			for (TenderLocation location : tender.getTenderLocation()) {
				if (location.getVolume() == null || location.getVolume().compareTo(BigDecimal.ZERO) == 0) {
					volumeValidationError = Boolean.TRUE;
					volumeValidation.add(tender.getCode() + " - " + location.getLocation().getCode());
				}
			}

			if (volumeValidationError) {
				continue;
			}

			tenderService.publish(tender);
		}

		if (!noLocationValidation.isEmpty()) {
			validationMessages
					.append(String.format(CmmErrorCode.TENDER_NO_LOCATION.getErrMsg(), StringUtil.unorderedList(noLocationValidation.toArray())));
		}

		if (!volumeValidation.isEmpty()) {
			validationMessages.append(String.format(CmmErrorCode.TENDER_NO_VOLUME.getErrMsg(), StringUtil.unorderedList(volumeValidation.toArray())));
		}

		if (!dbSyncValidation.isEmpty()) {
			validationMessages
					.append(String.format(CmmErrorCode.TENDER_RPC_ACTION_ERROR.getErrMsg(), StringUtil.unorderedList(dbSyncValidation.toArray())));
		}

		if (validationMessages.length() > 0) {
			throw new BusinessException(CmmErrorCode.VALIDATION_ERRORS, validationMessages.toString());
		}

	}

	/**
	 * @param tenderDs
	 * @param params
	 * @throws Exception
	 */
	public void unpublish(List<Tender_Ds> tenderDs, Tender_DsParam params) throws Exception {
		StringBuilder validationMessages = new StringBuilder();

		List<String> dbSyncValidation = new ArrayList<>();
		List<String> submitedBidsValidation = new ArrayList<>();

		ITenderService tenderService = (ITenderService) this.findEntityService(Tender.class);
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);

		for (Tender_Ds ds : tenderDs) {
			Tender tender = tenderService.findById(ds.getId());

			// Check for concurency
			if (tender == null || !TenderStatus._NEW_.equals(tender.getStatus())) {
				dbSyncValidation.add(ds.getCode());
				continue;
			}

			Map<String, Object> awardedParams = new HashMap<>();
			awardedParams.put("bidTenderIdentification", tender);
			awardedParams.put("bidStatus", BidStatus._AWARDED_);
			List<Contract> awardedBids = contractService.findEntitiesByAttributes(awardedParams);

			Map<String, Object> acceptedParams = new HashMap<>();
			acceptedParams.put("bidTenderIdentification", tender);
			acceptedParams.put("bidStatus", BidStatus._AWARD_ACCEPTED_);
			List<Contract> acceptedBids = contractService.findEntitiesByAttributes(acceptedParams);

			if (!awardedBids.isEmpty() || !acceptedBids.isEmpty()) {
				submitedBidsValidation.add(tender.getCode());
				continue;
			}

			tenderService.unpublish(tender, params.getRemarks());
		}

		if (!submitedBidsValidation.isEmpty()) {
			validationMessages.append(
					String.format(CmmErrorCode.TENDER_WITH_SUBMITED_BIDS.getErrMsg(), StringUtil.unorderedList(submitedBidsValidation.toArray())));
		}

		if (!dbSyncValidation.isEmpty()) {
			validationMessages
					.append(String.format(CmmErrorCode.TENDER_RPC_ACTION_ERROR.getErrMsg(), StringUtil.unorderedList(dbSyncValidation.toArray())));
		}

		if (validationMessages.length() > 0) {
			throw new BusinessException(CmmErrorCode.VALIDATION_ERRORS, validationMessages.toString());
		}

	}

	public void markUnread(Tender_Ds tenderDs, Tender_DsParam params) throws Exception {
		ITenderService srv = (ITenderService) this.findEntityService(Tender.class);
		Tender tender = srv.findById(tenderDs.getId());
		if (!tender.getSource().equals(TenderSource._CAPTURED_) && tender.getIsRead() != Boolean.FALSE) {
			tender.setIsRead(false);
			srv.update(tender);
		}
	}

	public void markRead(Tender_Ds tenderDs, Tender_DsParam params) throws Exception {
		ITenderService srv = (ITenderService) this.findEntityService(Tender.class);
		Tender tender = srv.findById(tenderDs.getId());
		if (!tender.getSource().equals(TenderSource._CAPTURED_) && tender.getIsRead() != Boolean.TRUE) {
			srv.markRead(tender);
		}
	}
}
