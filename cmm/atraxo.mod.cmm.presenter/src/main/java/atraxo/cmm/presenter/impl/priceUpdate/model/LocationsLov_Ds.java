/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.priceUpdate.model;

import atraxo.fmbas.domain.impl.fmbas_type.GeoType;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Locations.class, sort = {@SortField(field = LocationsLov_Ds.F_NAME)})
public class LocationsLov_Ds extends AbstractLov_Ds<Locations> {

	public static final String ALIAS = "cmm_LocationsLov_Ds";

	public static final String F_NAME = "name";
	public static final String F_AREANAME = "areaName";
	public static final String F_CODE = "code";
	public static final String F_TYPE = "type";

	@DsField
	private String name;

	@DsField(join = "left", path = "areas.name")
	private String areaName;

	@DsField
	private String code;

	@DsField(fetch = false)
	private GeoType type;

	/**
	 * Default constructor
	 */
	public LocationsLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public LocationsLov_Ds(Locations e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAreaName() {
		return this.areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public GeoType getType() {
		return this.type;
	}

	public void setType(GeoType type) {
		this.type = type;
	}
}
