package atraxo.cmm.presenter.ext.prices.service.validator;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IPricingBaseService;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.exceptions.InvalidUpdatePricingBasePeriod;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.presenter.impl.contracts.model.ContractCustomerWizard_Ds;
import atraxo.cmm.presenter.impl.contracts.model.PricingBase_Ds;
import atraxo.fmbas.business.api.categories.IPriceCategoryService;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterBaseService;

/**
 * @author abolindu
 */
public class PricingBaseValidator extends AbstractPresenterBaseService {

	@Autowired
	private IContractPriceCategoryService cpcService;
	@Autowired
	private IPricingBaseService pbServie;
	@Autowired
	private IPriceCategoryService pcService;

	/**
	 * @param pricingBaseDs
	 * @param pricingBase
	 * @throws BusinessException
	 */
	public void validatePricingBaseOnInsert(PricingBase_Ds pricingBaseDs, PricingBase pricingBase) throws BusinessException {
		this.validatePricingBaseOnInsert(null, pricingBaseDs, pricingBase);
	}

	/**
	 * @param wizardDs
	 * @param pricingBase
	 * @throws BusinessException
	 */
	public void validatePricingBaseOnInsert(ContractCustomerWizard_Ds wizardDs, PricingBase pricingBase) throws BusinessException {
		this.validatePricingBaseOnInsert(wizardDs, null, pricingBase);
	}

	/**
	 * Check the needed conditions before adding a new PricingBase
	 *
	 * @param ds
	 * @param pricingBase
	 * @throws BusinessException
	 */
	private void validatePricingBaseOnInsert(ContractCustomerWizard_Ds wizardDs, PricingBase_Ds pricingBaseDs, PricingBase pricingBase)
			throws BusinessException {
		this.checkReadOnly(pricingBase);
		this.extendPricingBase(pricingBase);
		this.checkPriceCategory(wizardDs, pricingBaseDs, pricingBase);
		this.checkIfFitsContractPeriod(pricingBase);
		this.checkProductPriceOverlapping(pricingBase.getContract().getPricingBases(), pricingBase);
	}

	/**
	 * Check the needed conditions at update of PricingBase
	 *
	 * @param pricingBase
	 * @throws Exception
	 */
	public void validatePricingBaseOnUpdate(PricingBase pricingBase) throws Exception {
		this.checkReadOnly(pricingBase);
		this.setFieldsFromContractPriceCategory(pricingBase);
		this.checkIfFitsContractPeriod(pricingBase);
		this.checkNewPeriod(pricingBase);
		this.checkProductPriceOverlapping(pricingBase.getContract().getPricingBases(), pricingBase);
		this.extendPricingBase(pricingBase);
	}

	/**
	 * If the contract would be read-only there should not exist the posibility to add prices
	 *
	 * @param e
	 * @throws BusinessException
	 */
	private void checkReadOnly(PricingBase e) throws BusinessException {
		if (e.getContract().getReadOnly()) {
			throw new BusinessException(CmmErrorCode.CONTRACT_READ_ONLY, CmmErrorCode.CONTRACT_READ_ONLY.getErrMsg());
		}
	}

	/**
	 * Extend pricing base period to contract period.
	 *
	 * @param e - {@link PricingBase}.
	 * @param callUpdate - {@link Boolean}. If true the method call the update function, otherwise not.
	 * @throws BusinessException
	 */
	private void extendPricingBase(PricingBase e) throws BusinessException {
		if (e.getContinous()) {
			boolean extend = true;
			Contract contract = e.getContract();
			for (PricingBase pb : contract.getPricingBases()) {
				if (pb.getId() == null) {
					// pb is e
					continue;
				}
				if (pb.getPriceCat().getId().equals(e.getPriceCat().getId())) {
					extend = false;
				} else {
					if (e.getPriceCat().getType().equals(PriceType._PRODUCT_) && pb.getPriceCat().getType().equals(PriceType._PRODUCT_)) {
						extend = false;
					}
				}
			}
			if (extend) {
				e.setValidFrom(contract.getValidFrom());
				e.setValidTo(contract.getValidTo());
			}
		}
	}

	/**
	 * Check if PricingBase has PriceCategory set, if not try to find by name and set it.
	 *
	 * @param wizardDs
	 * @param pricingBaseDs
	 * @param e
	 */
	private void checkPriceCategory(ContractCustomerWizard_Ds wizardDs, PricingBase_Ds pricingBaseDs, PricingBase e) {
		if (e.getPriceCat() == null) {
			PriceCategory pc = null;
			if (wizardDs != null) {
				pc = this.pcService.findByName(wizardDs.getPriceCategoryName());
			} else if (pricingBaseDs != null) {
				pc = this.pcService.findByName(pricingBaseDs.getPriceCatName());
			}
			e.setPriceCat(pc);
		}
	}

	/**
	 * Check if the pricing base period is in the contract price period. If no throw a error message.
	 *
	 * @param e
	 * @throws BusinessException
	 */
	private void checkIfFitsContractPeriod(PricingBase e) throws BusinessException {
		Contract c = e.getContract();
		if (c == null) {
			throw new BusinessException(CmmErrorCode.REFRESH_CONTRACT_LIST, CmmErrorCode.REFRESH_CONTRACT_LIST.getErrMsg());
		}
		if (e.getValidFrom() != null && c.getValidFrom().after(e.getValidFrom())) {
			throw new BusinessException(CmmErrorCode.PB_VALIDITY, CmmErrorCode.PB_VALIDITY.getErrMsg());
		}
		if (e.getValidTo() != null && c.getValidTo().before(e.getValidTo())) {
			throw new BusinessException(CmmErrorCode.PB_VALIDITY, CmmErrorCode.PB_VALIDITY.getErrMsg());
		}
	}

	/**
	 * Two Product PricingBases can not have the same period
	 *
	 * @param pricingBases
	 * @param pricingBase
	 * @throws BusinessException
	 */
	private void checkProductPriceOverlapping(Collection<PricingBase> pricingBases, PricingBase pricingBase) throws BusinessException {
		if (!PriceType._PRODUCT_.equals(pricingBase.getPriceCat().getType())
				|| PriceInd._COMPOSITE_.equals(pricingBase.getPriceCat().getPricePer())) {
			// price is not PRODUCT
			return;
		}
		for (PricingBase pb : pricingBases) {
			if (!PriceType._PRODUCT_.equals(pb.getPriceCat().getType()) || PriceInd._COMPOSITE_.equals(pb.getPriceCat().getPricePer())) {
				// price is not PRODUCT
				continue;
			}
			if (!pricingBase.equals(pb) && pb.getValidFrom().equals(pricingBase.getValidFrom()) && pb.getValidTo().equals(pricingBase.getValidTo())) {
				throw new BusinessException(CmmErrorCode.PB_PRICE_OVERLAPING, CmmErrorCode.PB_PRICE_OVERLAPING.getErrMsg());
			}
		}
	}

	/**
	 * @param e
	 */
	private void setFieldsFromContractPriceCategory(PricingBase pricingBases) {
		List<ContractPriceCategory> list = this.cpcService.findByPricingBases(pricingBases);
		if (!list.isEmpty()) {
			ContractPriceCategory contractPriceCategory = list.get(0);
			pricingBases.setVat(contractPriceCategory.getVat());
			pricingBases.setQuantityType(contractPriceCategory.getQuantityType());
			pricingBases.setExchangeRateOffset(contractPriceCategory.getExchangeRateOffset());
			pricingBases.setAverageMethodId(contractPriceCategory.getAverageMethod().getId());
			pricingBases.setFinancialSourceId(contractPriceCategory.getFinancialSource().getId());
		}
	}

	/**
	 * @param pricingBases
	 * @throws Exception
	 */
	private void checkNewPeriod(PricingBase pricingBases) throws Exception {
		PricingBase oldPricingBases = this.pbServie.findById(pricingBases.getId());
		if (pricingBases.getValidFrom().compareTo(oldPricingBases.getValidTo()) >= 0) {
			throw new InvalidUpdatePricingBasePeriod();
		}
		if (pricingBases.getValidTo().compareTo(oldPricingBases.getValidFrom()) <= 0) {
			throw new InvalidUpdatePricingBasePeriod();
		}
	}
}
