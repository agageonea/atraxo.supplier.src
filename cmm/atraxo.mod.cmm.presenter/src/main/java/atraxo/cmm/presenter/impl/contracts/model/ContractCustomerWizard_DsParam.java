/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.contracts.model;

/**
 * Generated code. Do not modify in this file.
 */
public class ContractCustomerWizard_DsParam {

	public static final String f_currencyID = "currencyID";
	public static final String f_currencyCode = "currencyCode";
	public static final String f_period = "period";
	public static final String f_averageMethodId = "averageMethodId";
	public static final String f_averageMethodName = "averageMethodName";
	public static final String f_financialSourceId = "financialSourceId";
	public static final String f_financialSourceCode = "financialSourceCode";
	public static final String f_invFreq = "invFreq";
	public static final String f_invType = "invType";
	public static final String f_creditTerms = "creditTerms";
	public static final String f_forex = "forex";
	public static final String f_paymentTerms = "paymentTerms";
	public static final String f_referenceTo = "referenceTo";

	private Integer currencyID;

	private String currencyCode;

	private String period;

	private Integer averageMethodId;

	private String averageMethodName;

	private Integer financialSourceId;

	private String financialSourceCode;

	private String invFreq;

	private String invType;

	private String creditTerms;

	private String forex;

	private Integer paymentTerms;

	private String referenceTo;

	public Integer getCurrencyID() {
		return this.currencyID;
	}

	public void setCurrencyID(Integer currencyID) {
		this.currencyID = currencyID;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getPeriod() {
		return this.period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public Integer getAverageMethodId() {
		return this.averageMethodId;
	}

	public void setAverageMethodId(Integer averageMethodId) {
		this.averageMethodId = averageMethodId;
	}

	public String getAverageMethodName() {
		return this.averageMethodName;
	}

	public void setAverageMethodName(String averageMethodName) {
		this.averageMethodName = averageMethodName;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}

	public String getInvFreq() {
		return this.invFreq;
	}

	public void setInvFreq(String invFreq) {
		this.invFreq = invFreq;
	}

	public String getInvType() {
		return this.invType;
	}

	public void setInvType(String invType) {
		this.invType = invType;
	}

	public String getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	public String getForex() {
		return this.forex;
	}

	public void setForex(String forex) {
		this.forex = forex;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getReferenceTo() {
		return this.referenceTo;
	}

	public void setReferenceTo(String referenceTo) {
		this.referenceTo = referenceTo;
	}
}
