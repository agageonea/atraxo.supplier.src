/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.cmm.presenter.ext.tender.qb;

import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.presenter.impl.tender.model.OpenTenderLocation_Ds;
import atraxo.cmm.presenter.impl.tender.model.OpenTenderLocation_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

/**
 * Custom Query Builder OpenTenderLocation_DsQb
 */
public class OpenTenderLocation_DsQb extends QueryBuilderWithJpql<OpenTenderLocation_Ds, Object, OpenTenderLocation_DsParam> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();

		this.addFilterCondition("e.id != :selectedLocation and e.locationBiddingStatus != :biddingStatus and e.isValid = 1");

		this.addCustomFilterItem("biddingStatus", BiddingStatus._FINISHED_);
		this.addCustomFilterItem("selectedLocation", this.params.getSelectedLocation());
	}

}
