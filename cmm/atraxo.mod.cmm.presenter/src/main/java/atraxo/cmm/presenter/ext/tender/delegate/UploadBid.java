package atraxo.cmm.presenter.ext.tender.delegate;

import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import atraxo.cmm.business.api.ext.tender.ITenderBidConverterService;
import atraxo.cmm.business.api.tender.ITenderLocationService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ws.tender.transformer.TenderBidTransformer;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.business.ext.util.StringUtil;
import seava.j4e.api.descriptor.IUploadedFileDescriptor;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.IFileUploadService;
import seava.j4e.iata.fuelplus.iata.tender.Bid;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBid;
import seava.j4e.iata.fuelplus.iata.tender.LocationHeader;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author vhojda
 */
public class UploadBid extends AbstractPresenterDelegate implements IFileUploadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UploadBid.class);

	private static final String RET_RESPONSE_MSG = "msg";

	private static final String XSD_IATA_IATA_FUEL_TENDER_XSD = "/xsd/iata/IATA_Fuel_Tender.xsd";

	private static final String MEDIA_TYPE_TEXT_XML = "text/xml";
	private static final String MEDIA_TYPE_XLS = "application/vnd.ms-excel";
	private static final String MEDIA_TYPE_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

	private static final String KEY_NAME = "name";
	private static final String KEY_ID = "id";
	private static final String KEY_LOCATION_ID = "locationId";
	private static final String KEY_LOCATION_CODE = "locationCode";

	private static final String BEAN_BID_TRANSFORMER = "tenderBidTransformer";

	private List<String> paramNames;

	public UploadBid() {
		super();
		this.paramNames = new ArrayList<>();
		this.paramNames.add(KEY_NAME);
		this.paramNames.add(KEY_ID);
		this.paramNames.add(KEY_LOCATION_ID);
		this.paramNames.add(KEY_LOCATION_CODE);
	}

	@Override
	public List<String> getParamNames() {
		return this.paramNames;
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.api.service.IFileUploadService#execute(seava.j4e.api.descriptor.IUploadedFileDescriptor, java.io.InputStream, java.util.Map)
	 */
	@Override
	public Map<String, Object> execute(IUploadedFileDescriptor fileDescriptor, InputStream inputStream, Map<String, String> params) throws Exception {

		Map<String, Object> result = new HashMap<>();

		ITenderBidConverterService converter = (ITenderBidConverterService) this.getApplicationContext().getBean("bidConverterService");

		if (inputStream != null && inputStream.available() != 0) {

			// extract tender location if sent from UI
			TenderLocation location = this.getTenderLocation(params);

			String xmlString;
			String contentType = fileDescriptor.getContentType();
			if (contentType.equals(MEDIA_TYPE_TEXT_XML)) {
				try {
					xmlString = StringUtil.getStringFromInputStream(inputStream);
				} catch (Exception e) {
					throw new BusinessException(CmmErrorCode.IMPORT_BID_IMPORT_XML_CAN_NOT_PROCESS,
							CmmErrorCode.IMPORT_BID_IMPORT_XML_CAN_NOT_PROCESS.getErrMsg(), e);
				} finally {
					inputStream.close();
				}
			} else if (contentType.equals(MEDIA_TYPE_XLS) || contentType.equals(MEDIA_TYPE_XLSX)) {
				byte[] bytes = null;
				try {
					bytes = IOUtils.toByteArray(inputStream);
				} catch (Exception e) {
					throw new BusinessException(CmmErrorCode.IMPORT_BID_XLS_CAN_NOT_PROCESS, CmmErrorCode.IMPORT_BID_XLS_CAN_NOT_PROCESS.getErrMsg(),
							e);
				} finally {
					inputStream.close();
				}

				byte[] xmlFile = converter.convert(bytes, fileDescriptor.getOriginalName());
				xmlString = new String(xmlFile);
			} else {
				throw new BusinessException(CmmErrorCode.IMPORT_BID_UNKNOWN_APPLICATION_TYPE,
						CmmErrorCode.IMPORT_BID_UNKNOWN_APPLICATION_TYPE.getErrMsg());
			}

			Collection<Contract> importedBids = this.processBids(xmlString, location);
			result.put(RET_RESPONSE_MSG, String.format(CmmErrorCode.IMPORT_BID_OK.getErrMsg(), importedBids.size()));
		} else {
			throw new BusinessException(CmmErrorCode.IMPORT_BID_COMMUNICATION_ERROR, CmmErrorCode.IMPORT_BID_COMMUNICATION_ERROR.getErrMsg());
		}

		return result;
	}

	/**
	 * @param xmlString
	 * @param location
	 * @throws BusinessException
	 * @throws JAXBException
	 */
	private Collection<Contract> processBids(String xmlString, TenderLocation location) throws BusinessException, JAXBException {
		// validate XML via xsd
		this.validateXML(xmlString);

		// extract FuelTenderBid
		FuelTenderBid fuelTenderBid = this.parseXML(xmlString);

		// check location (if the case)
		if (location != null) {
			this.checkBidLocations(fuelTenderBid, location);
		}

		// extract the contracts/bids
		TenderBidTransformer tenderBidTransformer = (TenderBidTransformer) this.getApplicationContext().getBean(BEAN_BID_TRANSFORMER);
		return tenderBidTransformer.transformDTOToModels(fuelTenderBid);
	}

	/**
	 * @param fuelTenderBid
	 * @param location
	 * @throws BusinessException
	 */
	private void checkBidLocations(FuelTenderBid fuelTenderBid, TenderLocation location) throws BusinessException {
		for (Bid bid : fuelTenderBid.getBids().getBid()) {
			LocationHeader locationHeader = bid.getBidIdentification().getLocationHeader();

			String flightServiceType = locationHeader.getFlightServiceType().value();
			if (!location.getFlightServiceType().getCode().equals(flightServiceType)) {
				throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_CHECK_LOC_DIFFERENT_FLIGHT_SERVICE_TYPE,
						String.format(CmmErrorCode.IMPORT_BID_XML_CHECK_LOC_DIFFERENT_FLIGHT_SERVICE_TYPE.getErrMsg(),
								bid.getBidIdentification().getBidderCode(), flightServiceType, location.getFlightServiceType().getCode()));
			}

			String productTaxType = locationHeader.getProductTaxType().value();
			if (!location.getTaxType().getIataCode().equals(productTaxType)) {
				throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_CHECK_LOC_DIFFERENT_TAX_TYPE,
						String.format(CmmErrorCode.IMPORT_BID_XML_CHECK_LOC_DIFFERENT_TAX_TYPE.getErrMsg(),
								bid.getBidIdentification().getBidderCode(), productTaxType, location.getTaxType().getIataCode()));
			}

			String fuelProductName = locationHeader.getFuelProduct().value();
			if (!location.getFuelProduct().getIataName().equals(fuelProductName)) {
				throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_CHECK_LOC_DIFFERENT_FUEL_PRODUCT,
						String.format(CmmErrorCode.IMPORT_BID_XML_CHECK_LOC_DIFFERENT_FUEL_PRODUCT.getErrMsg(),
								bid.getBidIdentification().getBidderCode(), fuelProductName, location.getFuelProduct().getIataName()));
			}

			String locationCodeIATA = locationHeader.getLocationCodeIATA();
			if (!location.getLocation().getIataCode().equals(locationCodeIATA)) {
				throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_CHECK_LOC_DIFFERENT_LOCATION,
						String.format(CmmErrorCode.IMPORT_BID_XML_CHECK_LOC_DIFFERENT_LOCATION.getErrMsg(),
								bid.getBidIdentification().getBidderCode(), locationCodeIATA, location.getLocation().getIataCode()));
			}

			String locationCodeICAO = locationHeader.getLocationCodeICAO();
			if (!location.getLocation().getIcaoCode().equals(locationCodeICAO)) {
				throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_CHECK_LOC_DIFFERENT_ICAO_CODE,
						String.format(CmmErrorCode.IMPORT_BID_XML_CHECK_LOC_DIFFERENT_ICAO_CODE.getErrMsg(),
								bid.getBidIdentification().getBidderCode(), locationCodeICAO, location.getLocation().getIcaoCode()));
			}
		}
	}

	/**
	 * @param params
	 * @return
	 * @throws BusinessException
	 */
	private TenderLocation getTenderLocation(Map<String, String> params) throws BusinessException {
		TenderLocation tenderLocation = null;
		try {
			String tenderLocationId = params.get(KEY_ID);
			if (!StringUtils.isEmpty(tenderLocationId)) {
				Integer id = Integer.parseInt(tenderLocationId);
				ITenderLocationService tenderLocationService = (ITenderLocationService) this.findEntityService(TenderLocation.class);
				tenderLocation = tenderLocationService.findById(id);
			}
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_EXTRACT_TENDER_LOCATION.getErrMsg(), e.getLocalizedMessage()), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_EXTRACT_TENDER_LOCATION,
					String.format(CmmErrorCode.IMPORT_BID_EXTRACT_TENDER_LOCATION.getErrMsg(), e.getLocalizedMessage()));
		}
		return tenderLocation;
	}

	/**
	 * @param xmlString
	 * @return
	 * @throws BusinessException
	 */
	private FuelTenderBid parseXML(String xmlString) throws BusinessException {
		FuelTenderBid ftb = null;
		try {
			StringReader reader = new StringReader(xmlString);
			JAXBContext jaxbContext = JAXBContext.newInstance(FuelTenderBid.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			ftb = (FuelTenderBid) unmarshaller.unmarshal(reader);
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_PARSE_ERROR.getErrMsg(), e.getLocalizedMessage()), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_PARSE_ERROR,
					String.format(CmmErrorCode.IMPORT_BID_XML_PARSE_ERROR.getErrMsg(), e.getLocalizedMessage()));
		}
		return ftb;
	}

	/**
	 * @param xmlString
	 * @throws BusinessException
	 */
	private void validateXML(String xmlString) throws BusinessException {
		try {
			StringReader reader = new StringReader(xmlString);
			SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = factory.newSchema(ClassPathResource.class.getResource(XSD_IATA_IATA_FUEL_TENDER_XSD));
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(reader));
		} catch (Exception e) {
			LOGGER.warn(String.format(CmmErrorCode.IMPORT_BID_XML_VALIDATE_ERROR.getErrMsg(), e.getLocalizedMessage()), e);
			throw new BusinessException(CmmErrorCode.IMPORT_BID_XML_VALIDATE_ERROR,
					String.format(CmmErrorCode.IMPORT_BID_XML_VALIDATE_ERROR.getErrMsg(), e.getLocalizedMessage()));
		}
	}

}
