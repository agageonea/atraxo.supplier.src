/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.tender.model;

/**
 * Generated code. Do not modify in this file.
 */
public class Tender_DsParam {

	public static final String f_remarks = "remarks";

	private String remarks;

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
