/**
 *
 */
package atraxo.cmm.presenter.ext.contracts.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.ext.datahub.IDataHubClientRequestService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ws.tender.client.DataHubSoapClient;
import atraxo.cmm.business.ws.tender.transformer.TenderBidCancelTransformer;
import atraxo.cmm.domain.ext.tender.DataHubRequestType;
import atraxo.cmm.domain.impl.cmm_type.TenderSource;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.presenter.impl.contracts.model.TenderBid_Ds;
import atraxo.cmm.presenter.impl.contracts.model.TenderBid_DsParam;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.business.ext.util.StringUtil;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderCancelBid;
import seava.j4e.presenter.service.AbstractPresenterBaseService;

/**
 * Service contains business for bid cancellation.
 *
 * @author zspeter
 */
public class CancelBidService extends AbstractPresenterBaseService implements ICancelBidService {

	@Autowired
	private IContractService contractService;
	@Autowired
	private IExternalInterfaceService interfaceService;
	@Autowired
	@Qualifier("cancelBidService")
	private IDataHubClientRequestService<Contract, FuelTenderCancelBid> bidService;
	@Autowired
	private TenderBidCancelTransformer transformer;
	@Autowired
	private ExternalInterfaceHistoryFacade historyFacade;
	@Autowired
	private ExternalInterfaceMessageHistoryFacade messageHistoryFacade;

	/*
	 * (non-Javadoc)
	 * @see atraxo.cmm.presenter.ext.contracts.service.ICancelBidService#canCancelBids(java.util.List)
	 */
	@Override
	public boolean canCancelBids(List<TenderBid_Ds> tenderBidDsList) {
		boolean flag = true;
		for (TenderBid_Ds ds : tenderBidDsList) {
			Contract bid = this.contractService.findById(ds.getId());
			flag = this.canCancel(bid);
			if (!flag) {
				break;
			}
		}
		return flag;
	}

	@Override
	public void cancelBid(List<TenderBid_Ds> tenderBidDsList, TenderBid_DsParam params) throws BusinessException {
		List<Contract> capturedTenderBids = new ArrayList<>();
		List<Contract> importedTenderBids = new ArrayList<>();

		List<String> messageList = this.separateBids(tenderBidDsList, capturedTenderBids, importedTenderBids);

		this.updateCapturedBids(capturedTenderBids, params.getBidCancelReason());
		this.submitImportedBids(importedTenderBids, params.getBidCancelReason());

		StringBuilder validationMessages = new StringBuilder();
		if (!messageList.isEmpty()) {
			validationMessages.append(String.format(CmmErrorCode.BID_RPC_ACTION_ERROR.getErrMsg(), StringUtil.unorderedList(messageList.toArray())));
		}

		if (validationMessages.length() > 0) {
			throw new BusinessException(CmmErrorCode.VALIDATION_ERRORS, validationMessages.toString());
		}

	}

	/**
	 * Separate bids by source into a captured and imported list.
	 *
	 * @param tenderBidDsList
	 * @param capturedTenderBids
	 * @param importedTenderBids
	 * @return
	 */
	private List<String> separateBids(List<TenderBid_Ds> tenderBidDsList, List<Contract> capturedTenderBids, List<Contract> importedTenderBids) {
		List<String> messageList = new ArrayList<>();
		for (TenderBid_Ds ds : tenderBidDsList) {
			Contract bid = this.contractService.findById(ds.getId());

			if (bid == null || !this.canCancel(bid)) {
				messageList.add(this.getBidIdentificationMessage(ds));
				continue;
			}

			// For Captured Invitations just update bid status
			if (TenderSource._CAPTURED_.equals(bid.getBidTenderIdentification().getSource())) {
				capturedTenderBids.add(bid);
			}

			// For Imported Invitations send request to DataHub
			if (TenderSource._IMPORTED_.equals(bid.getBidTenderIdentification().getSource())) {
				importedTenderBids.add(bid);
			}
		}
		return messageList;
	}

	/**
	 * Update Captured bids.
	 *
	 * @param bids
	 * @param reasonMessage
	 * @throws BusinessException
	 */
	private void updateCapturedBids(List<Contract> bids, String reasonMessage) throws BusinessException {
		// Update bids for captured tenders
		if (!bids.isEmpty()) {
			this.contractService.cancelTenderBid(bids, reasonMessage);
		}
	}

	/**
	 * Submit imported bids.
	 *
	 * @param bids
	 * @param reasonMessage
	 * @throws BusinessException
	 */
	private void submitImportedBids(List<Contract> bids, String reasonMessage) throws BusinessException {
		if (bids.isEmpty()) {
			return;
		}
		ExternalInterface externalInterface = this.interfaceService.findByName(ExtInterfaceNames.FUEL_TENDER_BID_CANCEL_INTERFACE.getValue());
		ExtInterfaceParameters extInterfaceParams = new ExtInterfaceParameters(externalInterface.getParams());

		if (!externalInterface.getEnabled()) {
			throw new BusinessException(BusinessErrorCode.OUT_INTERFACE_DISABLED_WITH_DETAILS,
					String.format(BusinessErrorCode.OUT_INTERFACE_DISABLED_WITH_DETAILS.getErrMsg(),
							ExtInterfaceNames.FUEL_TENDER_BID_CANCEL_INTERFACE.getValue()));
		}

		// Update bids for tranmission initated
		for (Contract bid : bids) {
			bid.setBidCancelTransmitionStatus(TransmissionStatus._IN_PROGRESS_);
		}
		this.contractService.updateWithoutBusinessLogic(bids);

		// Prepare params for datahub soap client
		DataHubSoapClient<Contract, FuelTenderCancelBid> dataHubSoapClient = new DataHubSoapClient<>(externalInterface, extInterfaceParams, bids,
				this.historyFacade, this.messageHistoryFacade, this.transformer, FuelTenderCancelBid.class, DataHubRequestType.SINGLE_REQUEST);
		dataHubSoapClient.setRequestService(this.bidService);
		dataHubSoapClient.setReason(reasonMessage);
		dataHubSoapClient.start(1);

	}

	/**
	 * Check a bid if it can be canceled.
	 *
	 * @param bid
	 * @return
	 */
	private boolean canCancel(Contract bid) {
		boolean canCancel = false;
		switch (bid.getBidStatus()) {
		case _DRAFT_:
		case _SUBMITTED_:
			canCancel = TransmissionStatus._RECEIVED_.equals(bid.getBidTransmissionStatus())
					&& (TransmissionStatus._NEW_.equals(bid.getBidCancelTransmitionStatus())
							|| TransmissionStatus._FAILED_.equals(bid.getBidCancelTransmitionStatus()));
			break;
		case _CANCELLED_:
			if (TransmissionStatus._NEW_.equals(bid.getBidCancelTransmitionStatus())
					|| TransmissionStatus._FAILED_.equals(bid.getBidCancelTransmitionStatus())) {
				canCancel = true;
			}
			break;
		case _AWARD_ACCEPTED_:
		case _AWARD_DECLINED_:
		case _AWARDED_:
		case _DECLINED_:
		default:
			// do nothing
			break;
		}
		return canCancel;
	}

	/**
	 * @param bid
	 * @return
	 */
	private String getBidIdentificationMessage(TenderBid_Ds bid) {
		StringBuilder bidIdentification = new StringBuilder();

		bidIdentification.append("Tender Name: ").append(bid.getTenderName()).append(" - ");
		bidIdentification.append("Tender Issuer Code: ").append(bid.getHolderCode()).append(" - ");
		bidIdentification.append("Location Code: ").append(bid.getLocCode()).append(" - ");
		bidIdentification.append("Bid Version: ").append(bid.getBidVersion());

		return bidIdentification.toString();
	}
}
