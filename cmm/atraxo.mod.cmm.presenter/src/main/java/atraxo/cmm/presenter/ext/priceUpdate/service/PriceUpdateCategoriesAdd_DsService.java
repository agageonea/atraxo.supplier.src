/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.priceUpdate.service;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.priceUpdate.IPriceUpdateService;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdateCategories;
import atraxo.cmm.presenter.impl.priceUpdate.model.PriceUpdateCategoriesAdd_Ds;
import atraxo.cmm.presenter.impl.priceUpdate.model.PriceUpdateCategoriesAdd_DsParam;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service PriceUpdateCategoriesAdd_DsService
 */
public class PriceUpdateCategoriesAdd_DsService
		extends AbstractEntityDsService<PriceUpdateCategoriesAdd_Ds, PriceUpdateCategoriesAdd_Ds, PriceUpdateCategoriesAdd_DsParam, PriceUpdateCategories>
		implements IDsService<PriceUpdateCategoriesAdd_Ds, PriceUpdateCategoriesAdd_Ds, PriceUpdateCategoriesAdd_DsParam> {

	@Autowired
	private IPriceUpdateService srv;

	@Override
	protected void preFind(IQueryBuilder<PriceUpdateCategoriesAdd_Ds, PriceUpdateCategoriesAdd_Ds, PriceUpdateCategoriesAdd_DsParam> builder) throws Exception {
		super.preFind(builder);
		this.srv.refresh(builder.getParams().getParentPriceUpdateRefId());
	}




}
