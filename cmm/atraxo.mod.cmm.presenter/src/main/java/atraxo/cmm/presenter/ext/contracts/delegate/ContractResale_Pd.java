package atraxo.cmm.presenter.ext.contracts.delegate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.exceptions.SiblingContractNotFoundException;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.presenter.impl.contracts.model.Contract_Ds;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author apetho
 */
public class ContractResale_Pd extends AbstractPresenterDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContractResale_Pd.class);

	public void deleteResaleContract(Contract_Ds contractDs) throws Exception {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Contract contract = contractService.findById(contractDs.getId());
		if (!ContractStatus._DRAFT_.equals(contract.getStatus())) {
			throw new BusinessException(CmmErrorCode.DELETE_INTERNAL_RESALE_CONTRACT_MUST_BE_DRAFT,
					CmmErrorCode.DELETE_INTERNAL_RESALE_CONTRACT_MUST_BE_DRAFT.getErrMsg());
		}
		try {
			Contract resaleContract = contractService.findSiblingContract(contract);
			contractService.deleteInternalResale(resaleContract);
		} catch (SiblingContractNotFoundException e) {
			LOGGER.error("No Sibling contract found.", e);
			throw new BusinessException(CmmErrorCode.INTERNAL_RESALE_CONTRACT_NOT_FOUND, CmmErrorCode.INTERNAL_RESALE_CONTRACT_NOT_FOUND.getErrMsg());
		}
	}

}
