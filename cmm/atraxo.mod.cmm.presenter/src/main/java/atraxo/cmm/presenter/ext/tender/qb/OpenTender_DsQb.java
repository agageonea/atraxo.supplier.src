/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.cmm.presenter.ext.tender.qb;

import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.presenter.impl.tender.model.OpenTender_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

/**
 * Custom Query Builder OpenTender_DsQb
 */
public class OpenTender_DsQb extends QueryBuilderWithJpql<OpenTender_Ds, Object, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();

		this.addFilterCondition("e.status = :status and e.biddingStatus != :biddingStatus");

		this.addCustomFilterItem("status", TenderStatus._NEW_);
		this.addCustomFilterItem("biddingStatus", BiddingStatus._FINISHED_);
	}

}
