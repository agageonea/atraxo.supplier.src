package atraxo.cmm.presenter.ext.prices.delegate;

import java.util.List;

import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.presenter.impl.prices.model.ContractPriceCategory_Ds;
import atraxo.cmm.presenter.impl.prices.model.ContractPriceCategory_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class ContractPriceCtgryDefault_Pd extends AbstractPresenterDelegate {

	/**
	 * check if exists other ContractPriceCategory with default field checked.
	 *
	 * @param cpc
	 * @param params
	 * @throws Exception
	 */
	public void checkDefault(ContractPriceCategory_Ds cpc, ContractPriceCategory_DsParam params) throws Exception {
		if (cpc.getDefaultPriceCtgy()) {
			IContractPriceCategoryService service = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
			List<ContractPriceCategory> cList = service.getDefaultContractPriceCategories(cpc.getPriceCategory(), cpc.getContId());
			for (ContractPriceCategory c : cList) {
				if (cpc.getId() == null || !c.getId().equals(cpc.getId())) {
					params.setIsDefault(true);
					this.setDefault(cpc);
					return;
				}
			}
		}
		params.setIsDefault(false);
	}

	public void setDefault(ContractPriceCategory_Ds cpc) throws Exception {
		IContractPriceCategoryService service = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
		List<ContractPriceCategory> cList = service.getDefaultContractPriceCategories(cpc.getPriceCategory(), cpc.getContId());
		for (ContractPriceCategory c : cList) {
			if (cpc.getId() == null || !c.getId().equals(cpc.getId())) {
				c.setDefaultPriceCtgy(false);
			}
		}
	}
}
