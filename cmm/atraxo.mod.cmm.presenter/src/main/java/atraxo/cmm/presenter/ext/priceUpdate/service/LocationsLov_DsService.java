/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.priceUpdate.service;

import atraxo.cmm.presenter.impl.priceUpdate.model.LocationsLov_Ds;
import atraxo.cmm.presenter.impl.priceUpdate.model.LocationsLov_DsParam;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service LocationsLov_DsService
 */
public class LocationsLov_DsService extends AbstractEntityDsService<LocationsLov_Ds, LocationsLov_Ds, LocationsLov_DsParam, Locations>
		implements IDsService<LocationsLov_Ds, LocationsLov_Ds, LocationsLov_DsParam> {

	@Override
	protected void preFind(IQueryBuilder<LocationsLov_Ds, LocationsLov_Ds, LocationsLov_DsParam> builder) throws Exception {
		ICustomerService srv = (ICustomerService) this.findEntityService(Customer.class);
		Customer customer = srv.findByRefid(Session.user.get().getClient().getActiveSubsidiaryId());
		LocationsLov_DsParam param = new LocationsLov_DsParam();
		param.setAreaID(customer.getAssignedArea().getId());
		builder.addParams(param);
		super.preFind(builder);
	}
}
