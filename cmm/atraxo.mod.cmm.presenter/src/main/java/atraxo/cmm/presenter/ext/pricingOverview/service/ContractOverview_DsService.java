/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.pricingOverview.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ext.exceptions.FuelPriceNotExistsException;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.presenter.impl.pricingOverview.model.ContractOverview_Ds;
import atraxo.cmm.presenter.impl.pricingOverview.model.ContractOverview_DsParam;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ContractOverview_DsService extends AbstractEntityDsService<ContractOverview_Ds, ContractOverview_Ds, ContractOverview_DsParam, Contract>
		implements IDsService<ContractOverview_Ds, ContractOverview_Ds, ContractOverview_DsParam> {

	private static final Logger LOG = LoggerFactory.getLogger(ContractOverview_DsService.class);

	@Override
	protected void postFind(IQueryBuilder<ContractOverview_Ds, ContractOverview_Ds, ContractOverview_DsParam> builder,
			List<ContractOverview_Ds> result) throws Exception {
		super.postFind(builder, result);
		String currencyCode = builder.getParams().getP_currency();
		String unitCode = builder.getParams().getP_unit();
		IContractService service = (IContractService) this.findEntityService(Contract.class);
		for (Iterator<ContractOverview_Ds> iterator = result.iterator(); iterator.hasNext();) {
			ContractOverview_Ds co = iterator.next();
			if (!ContractStatus._ACTIVE_.equals(co.getStatus()) && !ContractStatus._EFFECTIVE_.equals(co.getStatus())) {
				iterator.remove();
				continue;
			}
			if (!StringUtils.isEmpty(currencyCode)) {
				co.setSettCurrCode(currencyCode);
			}
			if (!StringUtils.isEmpty(unitCode)) {
				co.setSettUnitCode(unitCode);
			}
			Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH);
			try {
				co.setPrice(service.getPrice(co.getId(), co.getSettUnitCode(), co.getSettCurrCode(), date));
			} catch (FuelPriceNotExistsException e) {
				LOG.warn(e.getMessage(), e);
				co.setPrice(BigDecimal.ZERO);
			}

		}
	}

}
