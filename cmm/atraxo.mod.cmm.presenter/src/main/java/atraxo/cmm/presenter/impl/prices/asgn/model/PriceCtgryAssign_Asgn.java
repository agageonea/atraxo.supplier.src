/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.prices.asgn.model;

import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.presenter.model.AbstractAsgnModel;

@Ds(entity = ContractPriceCategory.class)
public class PriceCtgryAssign_Asgn
		extends
			AbstractAsgnModel<ContractPriceCategory> {

	public static final String ALIAS = "cmm_PriceCtgryAssign_Asgn";

	public static final String f_id = "id";
	public static final String f_idContract = "idContract";
	public static final String f_name = "name";

	@DsField(path = "id")
	private Integer id;

	@DsField(path = "contract.id")
	private Integer idContract;

	@DsField(path = "name")
	private String name;

	public PriceCtgryAssign_Asgn() {
	}

	public PriceCtgryAssign_Asgn(ContractPriceCategory e) {
		super();
		this.id = e.getId();
		this.idContract = e.getContract().getId();
		this.name = e.getName();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdContract() {
		return this.idContract;
	}

	public void setIdContract(Integer idContract) {
		this.idContract = idContract;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
