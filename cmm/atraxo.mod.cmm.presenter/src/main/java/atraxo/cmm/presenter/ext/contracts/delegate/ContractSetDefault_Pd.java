package atraxo.cmm.presenter.ext.contracts.delegate;

import java.util.Calendar;
import java.util.Date;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.presenter.impl.contracts.model.Contract_Ds;
import atraxo.cmm.presenter.impl.contracts.model.Contract_DsParam;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.domain.impl.customer.Customer;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class ContractSetDefault_Pd extends AbstractPresenterDelegate {

	/**
	 * If status is "draft" set the status to active.
	 *
	 * @param contract
	 * @throws Exception
	 */
	public void setActive(Contract_Ds contractDs) throws Exception {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Contract contract = contractService.findById(contractDs.getId());
		if (ContractStatus._DRAFT_.equals(contract.getStatus()) && !contract.getReadOnly()) {
			contractService.setActive(contract);
		} else {
			throw new BusinessException(CmmErrorCode.CONTRACT_RPC_ACTION_ERROR,
					String.format(CmmErrorCode.CONTRACT_RPC_ACTION_ERROR.getErrMsg(), contract.getCode()));
		}
	}

	/**
	 * If current date is in period set status to effective else set status to expired.
	 *
	 * @param contract
	 * @throws Exception
	 */

	public void updateStatus(Contract_Ds contract) throws Exception {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Contract contractBis = contractService.findById(contract.getId());
		if (!ContractStatus._DRAFT_.equals(contractBis.getStatus()) && !contract.getReadOnly()) {
			contractService.updateStatus(contractBis);
		} else {
			throw new BusinessException(CmmErrorCode.CONTRACT_RPC_ACTION_ERROR,
					String.format(CmmErrorCode.CONTRACT_RPC_ACTION_ERROR.getErrMsg(), contract.getCode()));
		}
	}

	/**
	 * Remove the time part from the date.
	 *
	 * @param date
	 * @return
	 */
	public Date removeTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/**
	 * If contract status is not equal with expired change status to draft.
	 *
	 * @param contract
	 * @param params
	 * @throws Exception
	 */
	public void resetStatus(Contract_Ds contract, Contract_DsParam params) throws Exception {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Contract contractBis = contractService.findById(contract.getId());
		if (!ContractStatus._DRAFT_.equals(contractBis.getStatus()) && !contract.getReadOnly()) {
			contractService.reset(contractBis, params.getRemarks());
		} else {
			throw new BusinessException(CmmErrorCode.CONTRACT_RPC_ACTION_ERROR,
					String.format(CmmErrorCode.CONTRACT_RPC_ACTION_ERROR.getErrMsg(), contract.getCode()));
		}
	}

	/**
	 * @param contractDs
	 * @param params
	 * @throws Exception
	 */
	public void generateResellerContract(Contract_Ds contractDs, Contract_DsParam params) throws Exception {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		ICustomerService custService = (ICustomerService) this.findEntityService(Customer.class);
		Customer customer;
		Contract contract;
		try {
			if (contractDs.getCustomerId() == null) {
				throw new BusinessException(CmmErrorCode.INVALID_CUSTOMER, String.format(CmmErrorCode.INVALID_CUSTOMER.getErrMsg()));
			}
			customer = custService.findById(contractDs.getCustomerId());
			if (customer == null) {
				throw new BusinessException(CmmErrorCode.INVALID_CUSTOMER, String.format(CmmErrorCode.INVALID_CUSTOMER.getErrMsg()));
			}
			contract = contractService.findById(contractDs.getId());
			if (contract == null) {
				throw new BusinessException(CmmErrorCode.NO_CONTRACTS, String.format(CmmErrorCode.NO_CONTRACTS.getErrMsg()));

			}
		} catch (ApplicationException e) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(e.getErrorCode())) {
				throw e;
			}
			throw new BusinessException(CmmErrorCode.INVALID_CUSTOMER, String.format(CmmErrorCode.INVALID_CUSTOMER.getErrMsg()), e);
		}

		Contract generatedContract = contractService.generateResaleContract(contract, customer);
		params.setGeneratedContractId(generatedContract.getId());
		params.setGeneratedContractCode(generatedContract.getCode());
	}

	/**
	 * @param contractDs
	 * @throws Exception
	 */
	public void generateInternalResellContract(Contract_Ds contractDs, Contract_DsParam params) throws Exception {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Contract contract = contractService.findById(contractDs.getId());
		this.checkCounterParties(contract);
		Contract generatedContract = contractService.generateInternalResaleContract(contract);
		params.setGeneratedContractId(generatedContract.getId());
		params.setGeneratedContractCode(generatedContract.getCode());
	}

	private void checkCounterParties(Contract contract) throws BusinessException {
		if (contract.getSupplier().getIsSubsidiary() == null || !contract.getSupplier().getIsSubsidiary()
				|| contract.getHolder().getIsSubsidiary() == null || !contract.getHolder().getIsSubsidiary()) {
			throw new BusinessException(CmmErrorCode.INVALID_SUBSIDIARIES, CmmErrorCode.INVALID_SUBSIDIARIES.getErrMsg());
		}

	}

}
