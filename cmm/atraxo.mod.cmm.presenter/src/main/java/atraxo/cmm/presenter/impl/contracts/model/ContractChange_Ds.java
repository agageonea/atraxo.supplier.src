/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.contracts.model;

import atraxo.cmm.domain.impl.contracts.ContractChange;
import atraxo.fmbas.domain.impl.fmbas_type.ContractChangeType;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ContractChange.class)
public class ContractChange_Ds extends AbstractDs_Ds<ContractChange> {

	public static final String ALIAS = "cmm_ContractChange_Ds";

	public static final String F_CONTRACTID = "contractId";
	public static final String F_TYPE = "type";
	public static final String F_MESSAGE = "message";
	public static final String F_CREATEDAT = "createdAt";

	@DsField
	private Integer contractId;

	@DsField
	private ContractChangeType type;

	@DsField
	private String message;

	@DsField
	private Date createdAt;

	/**
	 * Default constructor
	 */
	public ContractChange_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ContractChange_Ds(ContractChange e) {
		super(e);
	}

	public Integer getContractId() {
		return this.contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public ContractChangeType getType() {
		return this.type;
	}

	public void setType(ContractChangeType type) {
		this.type = type;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
}
