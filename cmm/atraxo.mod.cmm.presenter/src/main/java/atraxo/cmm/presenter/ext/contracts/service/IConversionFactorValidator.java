/**
 *
 */
package atraxo.cmm.presenter.ext.contracts.service;

import atraxo.cmm.presenter.impl.contracts.model.ContractCustomerWizard_Ds;
import atraxo.cmm.presenter.impl.contracts.model.PricingBase_Ds;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public interface IConversionFactorValidator {

	/**
	 * Verify conversion factor on PricingBase.
	 *
	 * @param ds
	 * @throws BusinessException
	 */
	void validatePricingBase(PricingBase_Ds ds) throws BusinessException;

	/**
	 * Verify conversion factor on ContractCustomerWizard
	 *
	 * @param ds
	 */
	void validateContractCustomerWizard(ContractCustomerWizard_Ds ds) throws BusinessException;
}
