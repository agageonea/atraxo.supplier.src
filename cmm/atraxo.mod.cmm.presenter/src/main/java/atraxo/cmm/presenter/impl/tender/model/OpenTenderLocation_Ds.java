/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.tender.model;

import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = TenderLocation.class, sort = {@SortField(field = OpenTenderLocation_Ds.F_LOCATIONCODE)})
@RefLookups({
		@RefLookup(refId = OpenTenderLocation_Ds.F_LOCATIONID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = OpenTenderLocation_Ds.F_LOCATIONCODE)}),
		@RefLookup(refId = OpenTenderLocation_Ds.F_TENDERID, namedQuery = Tender.NQ_FIND_BY_CODE_PRIMITIVE, params = {
				@Param(name = "code", field = OpenTenderLocation_Ds.F_TENDERCODE),
				@Param(name = "tenderVersion", field = OpenTenderLocation_Ds.F_TENDERVERSION),
				@Param(name = "holderId", field = OpenTenderLocation_Ds.F_TENDERHOLDERID)})})
public class OpenTenderLocation_Ds extends AbstractDs_Ds<TenderLocation> {

	public static final String ALIAS = "cmm_OpenTenderLocation_Ds";

	public static final String F_TENDERID = "tenderId";
	public static final String F_TENDERCODE = "tenderCode";
	public static final String F_TENDERVERSION = "tenderVersion";
	public static final String F_TENDERSTATUS = "tenderStatus";
	public static final String F_LOCATIONID = "locationId";
	public static final String F_LOCATIONCODE = "locationCode";
	public static final String F_LOCATIONNAME = "locationName";
	public static final String F_TENDERHOLDERID = "tenderHolderId";
	public static final String F_TENDERHOLDERCODE = "tenderHolderCode";
	public static final String F_FLIGHTSERVICETYPE = "flightServiceType";
	public static final String F_TAXTYPE = "taxType";
	public static final String F_FUELPRODUCT = "fuelProduct";
	public static final String F_DELIVERYPOINT = "deliveryPoint";
	public static final String F_LOCATIONBIDDINGSTATUS = "locationBiddingStatus";
	public static final String F_ROUND = "round";

	@DsField(join = "left", path = "tender.id")
	private Integer tenderId;

	@DsField(join = "left", path = "tender.code")
	private String tenderCode;

	@DsField(join = "left", path = "tender.version")
	private Long tenderVersion;

	@DsField(join = "left", path = "tender.status")
	private TenderStatus tenderStatus;

	@DsField(join = "left", path = "location.id")
	private Integer locationId;

	@DsField(join = "left", path = "location.code")
	private String locationCode;

	@DsField(join = "left", path = "location.name")
	private String locationName;

	@DsField(join = "left", path = "tender.holder.id")
	private Integer tenderHolderId;

	@DsField(join = "left", path = "tender.holder.code")
	private String tenderHolderCode;

	@DsField
	private FlightServiceType flightServiceType;

	@DsField
	private TaxType taxType;

	@DsField
	private Product fuelProduct;

	@DsField
	private ContractSubType deliveryPoint;

	@DsField
	private BiddingStatus locationBiddingStatus;

	@DsField(fetch = false)
	private Integer round;

	/**
	 * Default constructor
	 */
	public OpenTenderLocation_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public OpenTenderLocation_Ds(TenderLocation e) {
		super(e);
	}

	public Integer getTenderId() {
		return this.tenderId;
	}

	public void setTenderId(Integer tenderId) {
		this.tenderId = tenderId;
	}

	public String getTenderCode() {
		return this.tenderCode;
	}

	public void setTenderCode(String tenderCode) {
		this.tenderCode = tenderCode;
	}

	public Long getTenderVersion() {
		return this.tenderVersion;
	}

	public void setTenderVersion(Long tenderVersion) {
		this.tenderVersion = tenderVersion;
	}

	public TenderStatus getTenderStatus() {
		return this.tenderStatus;
	}

	public void setTenderStatus(TenderStatus tenderStatus) {
		this.tenderStatus = tenderStatus;
	}

	public Integer getLocationId() {
		return this.locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getLocationCode() {
		return this.locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getLocationName() {
		return this.locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public Integer getTenderHolderId() {
		return this.tenderHolderId;
	}

	public void setTenderHolderId(Integer tenderHolderId) {
		this.tenderHolderId = tenderHolderId;
	}

	public String getTenderHolderCode() {
		return this.tenderHolderCode;
	}

	public void setTenderHolderCode(String tenderHolderCode) {
		this.tenderHolderCode = tenderHolderCode;
	}

	public FlightServiceType getFlightServiceType() {
		return this.flightServiceType;
	}

	public void setFlightServiceType(FlightServiceType flightServiceType) {
		this.flightServiceType = flightServiceType;
	}

	public TaxType getTaxType() {
		return this.taxType;
	}

	public void setTaxType(TaxType taxType) {
		this.taxType = taxType;
	}

	public Product getFuelProduct() {
		return this.fuelProduct;
	}

	public void setFuelProduct(Product fuelProduct) {
		this.fuelProduct = fuelProduct;
	}

	public ContractSubType getDeliveryPoint() {
		return this.deliveryPoint;
	}

	public void setDeliveryPoint(ContractSubType deliveryPoint) {
		this.deliveryPoint = deliveryPoint;
	}

	public BiddingStatus getLocationBiddingStatus() {
		return this.locationBiddingStatus;
	}

	public void setLocationBiddingStatus(BiddingStatus locationBiddingStatus) {
		this.locationBiddingStatus = locationBiddingStatus;
	}

	public Integer getRound() {
		return this.round;
	}

	public void setRound(Integer round) {
		this.round = round;
	}
}
