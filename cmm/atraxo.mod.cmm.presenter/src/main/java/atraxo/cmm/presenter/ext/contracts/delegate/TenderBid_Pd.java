package atraxo.cmm.presenter.ext.contracts.delegate;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.ext.bid.IBidService;
import atraxo.cmm.business.api.ext.bid.IIataBidService;
import atraxo.cmm.business.api.ext.datahub.IDataHubClientRequestService;
import atraxo.cmm.business.ext.bid.AcceptAwardService;
import atraxo.cmm.business.ext.bid.DeclineAwardService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.mailMerge.delegate.EntityDtoDelegate;
import atraxo.cmm.business.ws.tender.client.DataHubSoapClient;
import atraxo.cmm.business.ws.tender.transformer.TenderBidAcceptAwardTransformer;
import atraxo.cmm.business.ws.tender.transformer.TenderBidDeclineAwardTransformer;
import atraxo.cmm.business.ws.tender.transformer.TenderBidTransformer;
import atraxo.cmm.domain.ext.tender.DataHubRequestType;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.ExportTypeBid;
import atraxo.cmm.domain.impl.cmm_type.TenderSource;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.presenter.ext.contracts.service.ICancelBidService;
import atraxo.cmm.presenter.impl.contracts.model.TenderBid_Ds;
import atraxo.cmm.presenter.impl.contracts.model.TenderBid_DsParam;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.templates.ITemplateService;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.workflow.ClaimedWorkflowException;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.business.ext.mailMerge.service.MailMerge_Service;
import atraxo.fmbas.business.ext.util.StringUtil;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.masteragreements.MasterAgreement;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.templates.Template;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcceptAward;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBid;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderDeclineAward;

/**
 * @author zspeter
 */
public class TenderBid_Pd extends AbstractTenderBid_Pd {

	private static final Logger LOGGER = LoggerFactory.getLogger(TenderBid_Pd.class);
	private static final String BEAN_BID_ACCEPT_AWARD_TRANSFORMER = "tenderBidAcceptAwardTransformer";
	private static final String BEAN_BID_DECLINE_AWARD_TRANSFORMER = "tenderBidDeclineAwardTransformer";

	private boolean canAwardDecline(Contract bid) throws Exception {
		ISystemParameterService systemParamsService = (ISystemParameterService) this.findEntityService(SystemParameter.class);

		boolean canSubmit = false;

		if (BidStatus._DRAFT_.equals(bid.getBidStatus())) {
			canSubmit = true;

			if (systemParamsService.getBidApprovalWorkflow() && !BidApprovalStatus._APPROVED_.equals(bid.getBidApprovalStatus())) {
				canSubmit = false;
			}

			if (BiddingStatus._FINISHED_.equals(bid.getBidTenderLocation().getLocationBiddingStatus())) {
				canSubmit = false;
			}
		}

		return canSubmit;
	}

	/**
	 * @param contract
	 * @param param
	 * @throws Exception
	 */
	public void setDefaulPaymentTerms(TenderBid_Ds contract, TenderBid_DsParam param) throws Exception {
		ICustomerService custService = (ICustomerService) this.findEntityService(Customer.class);
		MasterAgreement ma = custService.getValidMasterAgreement(custService.findById(contract.getCustomerId()),
				GregorianCalendar.getInstance().getTime());
		if (ma != null) {
			if (ma.getCurrency() != null) {
				param.setCurrencyID(ma.getCurrency().getId());
				param.setCurrencyCode(ma.getCurrency().getCode());
			}
			param.setPeriod(ma.getPeriod().getName());
			param.setReferenceTo(ma.getReferenceTo().getName());
			if (ma.getAverageMethod() != null) {
				param.setAverageMethodId(ma.getAverageMethod().getId());
				param.setAverageMethodName(ma.getAverageMethod().getName());
			}
			if (ma.getFinancialsource() != null) {
				param.setFinancialSourceId(ma.getFinancialsource().getId());
				param.setFinancialSourceCode(ma.getFinancialsource().getCode());
			}
			if (ma.getFinancialsource() != null && ma.getAverageMethod() != null) {
				param.setForex(ma.getFinancialsource().getCode() + "/" + ma.getAverageMethod().getName());
			}
			param.setInvFreq(ma.getInvoiceFrequency().getName());
			param.setInvType(ma.getInvoiceType().getName());
			try {
				CreditLines cl = custService.getValidCreditLine(custService.findById(contract.getCustomerId()), new Date());
				if (cl != null) {
					param.setCreditTerms(cl.getCreditTerm().getName());
				}
			} catch (BusinessException e) {
				LOGGER.warn("Could not set default payment terms for bid : " + contract.getCode(), e);
			}
			param.setPaymentTerms(ma.getPaymentTerms());
		}
	}

	public void acceptAward(TenderBid_Ds tenderBidDs) throws Exception {
		List<String> dbSyncValidation = new ArrayList<>();

		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Contract bid = contractService.findById(tenderBidDs.getId());

		boolean bidAwarded = bid != null && BidStatus._AWARDED_.equals(bid.getBidStatus());
		boolean bidTransmitted = bid != null && (TransmissionStatus._NEW_.equals(bid.getBidAcceptAwardTransmissionStatus())
				|| TransmissionStatus._FAILED_.equals(bid.getBidAcceptAwardTransmissionStatus()));

		// Cehck for Concurency
		if (!bidAwarded || (TenderSource._IMPORTED_.equals(bid.getBidTenderIdentification().getSource()) && !bidTransmitted)) {
			dbSyncValidation.add(this.getBidIdentificationMessage(tenderBidDs));
			throw new BusinessException(CmmErrorCode.VALIDATION_ERRORS,
					String.format(CmmErrorCode.BID_RPC_ACTION_ERROR.getErrMsg(), StringUtil.unorderedList(dbSyncValidation.toArray())));
		}

		// For Captured Invitations just update bid status
		if (TenderSource._CAPTURED_.equals(bid.getBidTenderIdentification().getSource())) {
			contractService.acceptAwardTenderBid(bid);
		}

		// For Imported Invitations send request to DataHub
		if (TenderSource._IMPORTED_.equals(bid.getBidTenderIdentification().getSource())) {
			bid.setBidAcceptAwardTransmissionStatus(TransmissionStatus._IN_PROGRESS_);
			contractService.updateWithoutBusinessLogic(Arrays.asList(bid));

			IExternalInterfaceService interfaceService = (IExternalInterfaceService) this.findEntityService(ExternalInterface.class);
			ExternalInterface externalInterface = interfaceService.findByName(ExtInterfaceNames.FUEL_TENDER_ACEEPT_AWARD.getValue());
			ExtInterfaceParameters externalInterfaceParams = new ExtInterfaceParameters(externalInterface.getParams());

			IDataHubClientRequestService<Contract, FuelTenderAcceptAward> bidService = this.getApplicationContext().getBean(AcceptAwardService.class);
			TenderBidAcceptAwardTransformer transformer = (TenderBidAcceptAwardTransformer) this.getApplicationContext()
					.getBean(BEAN_BID_ACCEPT_AWARD_TRANSFORMER);
			ExternalInterfaceHistoryFacade historyFacade = (ExternalInterfaceHistoryFacade) this.getApplicationContext().getBean(BEAN_HISTORY_FACADE);
			ExternalInterfaceMessageHistoryFacade messageHistoryFacade = (ExternalInterfaceMessageHistoryFacade) this.getApplicationContext()
					.getBean(BEAN_MESSAGE_HISTORY_FACADE);

			if (!externalInterface.getEnabled()) {
				throw new BusinessException(BusinessErrorCode.OUT_INTERFACE_DISABLED_WITH_DETAILS, String.format(
						BusinessErrorCode.OUT_INTERFACE_DISABLED_WITH_DETAILS.getErrMsg(), ExtInterfaceNames.FUEL_TENDER_ACEEPT_AWARD.getValue()));
			}

			// Prepare params for datahub soap client
			DataHubSoapClient<Contract, FuelTenderAcceptAward> dataHubSoapClient = new DataHubSoapClient<>(externalInterface, externalInterfaceParams,
					Arrays.asList(bid), historyFacade, messageHistoryFacade, transformer, FuelTenderAcceptAward.class,
					DataHubRequestType.SINGLE_REQUEST);
			dataHubSoapClient.setRequestService(bidService);
			dataHubSoapClient.start(1);
		}
	}

	public void declineAward(TenderBid_Ds tenderBidDs, TenderBid_DsParam params) throws Exception {
		List<String> dbSyncValidation = new ArrayList<>();

		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Contract bid = contractService.findById(tenderBidDs.getId());

		boolean bidAwarded = bid != null && BidStatus._AWARDED_.equals(bid.getBidStatus());
		boolean bidTransmitted = bid != null && (TransmissionStatus._NEW_.equals(bid.getBidDeclineAwardTransmissionStatus())
				|| TransmissionStatus._FAILED_.equals(bid.getBidDeclineAwardTransmissionStatus()));

		// Cehck for Concurency
		if (!bidAwarded || (TenderSource._IMPORTED_.equals(bid.getBidTenderIdentification().getSource()) && !bidTransmitted)) {
			dbSyncValidation.add(this.getBidIdentificationMessage(tenderBidDs));
			throw new BusinessException(CmmErrorCode.VALIDATION_ERRORS,
					String.format(CmmErrorCode.BID_RPC_ACTION_ERROR.getErrMsg(), StringUtil.unorderedList(dbSyncValidation.toArray())));
		}

		// For Captured Invitations just update bid status
		if (TenderSource._CAPTURED_.equals(bid.getBidTenderIdentification().getSource())) {
			contractService.declineAwardTenderBid(bid, params.getBidDeclineAwardReason());
		}

		// For Imported Invitations send request to DataHub
		if (TenderSource._IMPORTED_.equals(bid.getBidTenderIdentification().getSource())) {
			bid.setBidDeclineAwardTransmissionStatus(TransmissionStatus._IN_PROGRESS_);
			contractService.updateWithoutBusinessLogic(Arrays.asList(bid));

			IExternalInterfaceService interfaceService = (IExternalInterfaceService) this.findEntityService(ExternalInterface.class);
			ExternalInterface externalInterface = interfaceService.findByName(ExtInterfaceNames.FUEL_TENDER_DECLINE_AWARD.getValue());
			ExtInterfaceParameters externalInterfaceParams = new ExtInterfaceParameters(externalInterface.getParams());

			IDataHubClientRequestService<Contract, FuelTenderDeclineAward> bidService = this.getApplicationContext()
					.getBean(DeclineAwardService.class);
			TenderBidDeclineAwardTransformer transformer = (TenderBidDeclineAwardTransformer) this.getApplicationContext()
					.getBean(BEAN_BID_DECLINE_AWARD_TRANSFORMER);
			ExternalInterfaceHistoryFacade historyFacade = (ExternalInterfaceHistoryFacade) this.getApplicationContext().getBean(BEAN_HISTORY_FACADE);
			ExternalInterfaceMessageHistoryFacade messageHistoryFacade = (ExternalInterfaceMessageHistoryFacade) this.getApplicationContext()
					.getBean(BEAN_MESSAGE_HISTORY_FACADE);

			if (!externalInterface.getEnabled()) {
				throw new BusinessException(BusinessErrorCode.OUT_INTERFACE_DISABLED_WITH_DETAILS, String.format(
						BusinessErrorCode.OUT_INTERFACE_DISABLED_WITH_DETAILS.getErrMsg(), ExtInterfaceNames.FUEL_TENDER_DECLINE_AWARD.getValue()));
			}

			// Prepare params for datahub soap client
			DataHubSoapClient<Contract, FuelTenderDeclineAward> dataHubSoapClient = new DataHubSoapClient<>(externalInterface,
					externalInterfaceParams, Arrays.asList(bid), historyFacade, messageHistoryFacade, transformer, FuelTenderDeclineAward.class,
					DataHubRequestType.SINGLE_REQUEST);
			dataHubSoapClient.setRequestService(bidService);
			dataHubSoapClient.setReason(params.getBidDeclineAwardReason());
			dataHubSoapClient.start(1);
		}
	}

	/**
	 * Awards all the selected bids
	 *
	 * @param tenderBidDsList
	 * @throws Exception
	 */
	public void awardBid(List<TenderBid_Ds> tenderBidDsList) throws Exception {
		StringBuilder validationMessages = new StringBuilder();

		List<Contract> tenderBids = new ArrayList<>();
		List<String> dbSyncValidation = new ArrayList<>();

		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		IBidService bidService = (IBidService) this.getApplicationContext().getBean(BID_SERVICE);

		for (TenderBid_Ds tenderBidDs : tenderBidDsList) {
			Contract bid = contractService.findById(tenderBidDs.getId());

			// Check for concurency
			if (bid == null || !this.canAwardDecline(bid)) {
				dbSyncValidation.add(this.getBidIdentificationMessage(tenderBidDs));
				continue;
			}

			tenderBids.add(bid);
		}

		bidService.awardTenderBid(tenderBids);

		if (!dbSyncValidation.isEmpty()) {
			validationMessages
					.append(String.format(CmmErrorCode.BID_RPC_ACTION_ERROR.getErrMsg(), StringUtil.unorderedList(dbSyncValidation.toArray())));
		}

		if (validationMessages.length() > 0) {
			throw new BusinessException(CmmErrorCode.VALIDATION_ERRORS, validationMessages.toString());
		}

	}

	/**
	 * Declines all the selected bids
	 *
	 * @param tenderBidDsList
	 * @throws Exception
	 */
	public void declineBid(List<TenderBid_Ds> tenderBidDsList) throws Exception {
		StringBuilder validationMessages = new StringBuilder();

		List<Contract> tenderBids = new ArrayList<>();
		List<String> dbSyncValidation = new ArrayList<>();

		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		for (TenderBid_Ds tenderBidDs : tenderBidDsList) {
			Contract bid = contractService.findById(tenderBidDs.getId());

			// Check for concurency
			if (bid == null || !this.canAwardDecline(bid)) {
				dbSyncValidation.add(this.getBidIdentificationMessage(tenderBidDs));
				continue;
			}

			tenderBids.add(bid);
		}

		IBidService bidService = (IBidService) this.getApplicationContext().getBean(BID_SERVICE);
		bidService.declineTenderBid(tenderBids);

		if (!dbSyncValidation.isEmpty()) {
			validationMessages
					.append(String.format(CmmErrorCode.BID_RPC_ACTION_ERROR.getErrMsg(), StringUtil.unorderedList(dbSyncValidation.toArray())));
		}

		if (validationMessages.length() > 0) {
			throw new BusinessException(CmmErrorCode.VALIDATION_ERRORS, validationMessages.toString());
		}

	}

	/**
	 * Cancel submitted bids
	 *
	 * @param tenderBidDsList
	 * @throws Exception
	 */
	public void cancelBid(List<TenderBid_Ds> tenderBidDsList, TenderBid_DsParam params) throws BusinessException {
		ICancelBidService srv = (ICancelBidService) this.getApplicationContext().getBean("cancelBid");
		srv.cancelBid(tenderBidDsList, params);
	}

	/**
	 * Exports a Bid locally using Mailmerge
	 *
	 * @param tenderBidDs
	 * @param params
	 * @throws Exception
	 */
	public void exportBid(TenderBid_Ds tenderBidDs, TenderBid_DsParam params) throws Exception {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		ITemplateService templateService = (ITemplateService) this.findEntityService(Template.class);
		IIataBidService iataSrv = this.getApplicationContext().getBean(IIataBidService.class);
		TenderBidTransformer bidTransformer = this.getApplicationContext().getBean(TenderBidTransformer.class);

		EntityDtoDelegate dtoDelegate = this.getApplicationContext().getBean(EntityDtoDelegate.class);
		MailMerge_Service mailMergeService = this.getApplicationContext().getBean(MailMerge_Service.class);

		Contract bid = contractService.findById(tenderBidDs.getId());

		String json = null;
		String fileFullName = null;
		String fileNameWtihExtension = null;
		String fileName = this.generateFileName(bid);
		String filePath = Session.user.get().getWorkspace().getTempPath() + File.separator + fileName;

		switch (params.getExportType()) {
		case _PDF_:
			fileFullName = new StringBuilder(filePath).append(".").append(ExportTypeBid._PDF_.getName()).toString();
			fileNameWtihExtension = new StringBuilder(fileName).append(".").append(ExportTypeBid._PDF_.getName()).toString();
			json = dtoDelegate.generateExportData(bid, ExportTypeBid._PDF_.getName());
			break;

		case _XLSX_:
			fileFullName = new StringBuilder(filePath).append(".").append(ExportTypeBid._XLSX_.getName()).toString();
			fileNameWtihExtension = new StringBuilder(fileName).append(".").append(ExportTypeBid._XLSX_.getName()).toString();
			json = dtoDelegate.generateExportData(bid, ExportTypeBid._XLSX_.getName());
			break;

		case _XML_:
			fileFullName = new StringBuilder(filePath).append(".").append(ExportTypeBid._XML_.getName()).toString();
			fileNameWtihExtension = new StringBuilder(fileName).append(".").append(ExportTypeBid._XML_.getName()).toString();
			try (FileOutputStream fos = new FileOutputStream(fileFullName)) {
				FuelTenderBid fuelTenderBid = bidTransformer.transformModelToDTO(bid, ThreadLocalRandom.current().nextLong(1, 10000000000L));
				iataSrv.generateIataXml(fuelTenderBid, fos);
				fos.flush();
			}
			break;
		default:
			break;
		}

		File file = new File(fileFullName);
		if (json != null) {
			byte[] data = mailMergeService.generateDocumentFromTemplate(fileFullName, json, templateService.getTemplateForExport());
			FileUtils.writeByteArrayToFile(file.getAbsoluteFile(), data);
		}
		params.setPreviewFile(fileNameWtihExtension);
	}

	private String generateFileName(Contract bid) {
		char separator = '_';
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss");
		String date = formatter.format(new Date());
		return new StringBuilder(bid.getHolder().getCode()).append(separator).append(bid.getBidTenderIdentification().getCode()).append(separator)
				.append(bid.getLocation().getCode()).append(separator).append(bid.getVersion()).append(separator).append(date).toString();
	}

	/**
	 * Approves current tender bid workflow task
	 *
	 * @param ds
	 * @param param
	 * @throws Exception
	 */
	public void approveTenderBid(TenderBid_Ds tenderBidDs, TenderBid_DsParam params) throws Exception {
		ISystemParameterService paramService = (ISystemParameterService) this.findEntityService(SystemParameter.class);

		// check concurency for system parameter
		if (!paramService.getBidApprovalWorkflow()) {
			throw new BusinessException(CmmErrorCode.BID_RPC_ACTION_ERROR,
					String.format(CmmErrorCode.BID_RPC_ACTION_ERROR.getErrMsg(), this.getBidIdentificationMessage(tenderBidDs)));
		}

		this.completeCurrentTask(tenderBidDs.getId(), true, params.getApprovalNote());
	}

	/**
	 * Rejects current tender bid workflow task
	 *
	 * @param ds
	 * @param param
	 * @throws Exception
	 */
	public void rejectTenderBid(TenderBid_Ds tenderBidDs, TenderBid_DsParam params) throws Exception {
		ISystemParameterService paramService = (ISystemParameterService) this.findEntityService(SystemParameter.class);

		// check concurency for system parameter
		if (!paramService.getBidApprovalWorkflow()) {
			throw new BusinessException(CmmErrorCode.BID_RPC_ACTION_ERROR,
					String.format(CmmErrorCode.BID_RPC_ACTION_ERROR.getErrMsg(), this.getBidIdentificationMessage(tenderBidDs)));
		}

		this.completeCurrentTask(tenderBidDs.getId(), false, params.getApprovalNote());
	}

	/**
	 * Completes the current task associated with the contract
	 *
	 * @param tenderBidId
	 * @param accept
	 * @param note
	 * @throws Exception
	 */
	private void completeCurrentTask(Integer tenderBidId, boolean accept, String note) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START completeCurrentTask()");
		}

		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		IWorkflowInstanceEntityService workflowInstanceEntityService = (IWorkflowInstanceEntityService) this
				.findEntityService(WorkflowInstanceEntity.class);

		// get the tender bid
		Contract tenderBid = contractService.findById(tenderBidId);

		// check concurency from GUI
		if (!BidStatus._DRAFT_.equals(tenderBid.getBidStatus()) || !tenderBid.getBidApprovalStatus().equals(BidApprovalStatus._AWAITING_APPROVAL_)) {
			throw new BusinessException(CmmErrorCode.BID_WORKFLOW_APPROVED, CmmErrorCode.BID_WORKFLOW_APPROVED.getErrMsg());
		}

		IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean("workflowBpmManager");

		WorkflowInstanceEntity workflowInstanceEntity = workflowInstanceEntityService.findByBusinessKey(WorkflowNames.BID_APPROVAL.getWorkflowName(),
				tenderBid.getId(), tenderBid.getClass().getSimpleName());
		try {
			workflowBpmManager.claimCompleteTask(workflowInstanceEntity.getWorkflowInstance().getId(), accept, note);
		} catch (ClaimedWorkflowException e1) {
			throw e1;
		} catch (Exception e) {
			workflowBpmManager.terminateWorkflow(workflowInstanceEntity.getWorkflowInstance().getId(),
					CmmErrorCode.BID_WORKFLOW_IDENTIFICATION_ERROR.getErrMsg() + ":" + e.getMessage());
			throw new BusinessException(CmmErrorCode.BID_WORKFLOW_IDENTIFICATION_ERROR,
					CmmErrorCode.BID_WORKFLOW_IDENTIFICATION_ERROR.getErrMsg() + ":" + e.getMessage(), e);
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END completeCurrentTask()");
		}
	}
}
