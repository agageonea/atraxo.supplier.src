package atraxo.cmm.presenter.ext.tender.delegate;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.ext.bid.IBidVersionService;
import atraxo.cmm.business.api.ext.datahub.IDataHubClientRequestService;
import atraxo.cmm.business.api.ext.tender.IBidGeneratorService;
import atraxo.cmm.business.api.tender.ITenderLocationService;
import atraxo.cmm.business.ext.bid.BidGeneratorService;
import atraxo.cmm.business.ext.bid.BidVersionService;
import atraxo.cmm.business.ext.bid.NoBidService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.tender.TenderDefaultConstants;
import atraxo.cmm.business.ws.tender.client.DataHubSoapClient;
import atraxo.cmm.business.ws.tender.transformer.TenderNoBidTransformer;
import atraxo.cmm.domain.ext.tender.DataHubRequestType;
import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.cmm.presenter.impl.tender.model.TenderLocation_Ds;
import atraxo.cmm.presenter.impl.tender.model.TenderLocation_DsParam;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.business.ext.util.StringUtil;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderNoBid;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author zspeter
 */
public class TenderLocation_Pd extends AbstractPresenterDelegate {

	private static final String BEAN_NO_BID_TRANSFORMER = "tenderNoBidTransformer";
	private static final String BEAN_HISTORY_FACADE = "externalInterfaceHistoryFacade";
	private static final String BEAN_MESSAGE_HISTORY_FACADE = "externalInterfaceMessageHistoryFacade";

	/**
	 * Generate bid from given tender location
	 *
	 * @param tenderLocationDs
	 * @param param
	 * @throws Exception
	 */
	public void generateBidFromLocation(TenderLocation_Ds tenderLocationDs, TenderLocation_DsParam param) throws Exception {
		IBidGeneratorService bidGeneratorService = this.getApplicationContext().getBean(BidGeneratorService.class);
		ITenderLocationService locationService = (ITenderLocationService) this.findEntityService(TenderLocation.class);
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);

		TenderLocation location = locationService.findById(tenderLocationDs.getId());
		if (!location.getIsValid()) {
			throw new BusinessException(CmmErrorCode.TENDER_LOCATION_NOT_VALID, CmmErrorCode.TENDER_LOCATION_NOT_VALID.getErrMsg());
		}

		Map<String, Object> bidGenerationParams = this.extractTenderLocationParams(param);

		Integer bidId = bidGeneratorService.generateBidFromLocation(tenderLocationDs.getId(), bidGenerationParams);
		Contract generatedBid = contractService.findById(bidId);
		param.setGeneratedBidId(bidId);
		param.setGeneratedBidCode(generatedBid.getCode());

	}

	/**
	 * Generate bid from given tender location
	 *
	 * @param tenderLocationDs
	 * @param param
	 * @throws Exception
	 */
	public void generateBidPerReceiver(TenderLocation_Ds tenderLocationDs, TenderLocation_DsParam param) throws Exception {
		IBidGeneratorService bidGeneratorService = this.getApplicationContext().getBean(BidGeneratorService.class);
		ITenderLocationService locationService = (ITenderLocationService) this.findEntityService(TenderLocation.class);

		TenderLocation location = locationService.findById(tenderLocationDs.getId());
		if (!location.getIsValid()) {
			throw new BusinessException(CmmErrorCode.TENDER_LOCATION_NOT_VALID, CmmErrorCode.TENDER_LOCATION_NOT_VALID.getErrMsg());
		}

		Map<String, Object> bidGenerationParams = this.extractTenderLocationParams(param);
		TenderLocationAirlines[] airlines = location.getTenderLocationAirlines()
				.toArray(new TenderLocationAirlines[location.getTenderLocationAirlines().size()]);

		Integer bidId = bidGeneratorService.generateBidFromLocation(tenderLocationDs.getId(), bidGenerationParams, airlines);
		param.setGeneratedBidId(bidId);
	}

	/**
	 * Decline bids for the selected locations
	 *
	 * @param tenderLocationDsList
	 * @throws Exception
	 */
	public void declineSelectedLocationsBid(List<TenderLocation_Ds> tenderLocationDsList) throws Exception {
		ITenderLocationService tenderLocationService = (ITenderLocationService) this.findEntityService(TenderLocation.class);

		List<String> dbSyncValidation = new ArrayList<>();
		StringBuilder validationMessages = new StringBuilder();

		List<TenderLocation> tenderLocations = new ArrayList<>();
		for (TenderLocation_Ds ds : tenderLocationDsList) {
			TenderLocation tenderLocation = tenderLocationService.findByBusiness(ds.getId());

			boolean isTransmited = TransmissionStatus._TRANSMITTED_.equals(tenderLocation.getTransmissionStatus())
					&& TransmissionStatus._RECEIVED_.equals(tenderLocation.getTransmissionStatus());

			if (!BiddingStatus._NEW_.equals(tenderLocation.getLocationBiddingStatus()) || isTransmited) {
				dbSyncValidation.add(ds.getLocationCode());
				continue;
			}

			tenderLocations.add(tenderLocation);
		}

		// Submit to DataHub
		this.submitTenderLocationsNoBid(tenderLocations);

		if (!dbSyncValidation.isEmpty()) {
			validationMessages.append(
					String.format(CmmErrorCode.TENDER_LOCATION_RPC_ACTION_ERROR.getErrMsg(), StringUtil.unorderedList(dbSyncValidation.toArray())));
		}

		if (validationMessages.length() > 0) {
			throw new BusinessException(CmmErrorCode.VALIDATION_ERRORS, validationMessages.toString());
		}
	}

	/**
	 * Decline bid for all not operated customer locations. All the locations not in the business area of the company.
	 *
	 * @param tenderLocationDs
	 * @throws Exception
	 */
	public void declineNotOperatedLocationsBid(TenderLocation_Ds tenderLocationDs, TenderLocation_DsParam param) throws Exception {
		ITenderLocationService tenderLocationService = (ITenderLocationService) this.findEntityService(TenderLocation.class);
		ICustomerService customerService = (ICustomerService) this.findEntityService(Customer.class);

		List<String> submitedRequests = new ArrayList<>();
		StringBuilder responseDescription = new StringBuilder();

		// Get current customer (active subsidiary)
		Map<String, Object> subsidiaryParams = new HashMap<>();
		subsidiaryParams.put("refid", Session.user.get().getClient().getActiveSubsidiaryId());
		Customer subsidiary = customerService.findEntityByAttributes(subsidiaryParams);

		// Get all the tender locations
		List<TenderLocation> tenderLocations = tenderLocationService.findByTenderId(tenderLocationDs.getTenderId());

		// Find all not operated location
		List<TenderLocation> noBidLocations = new ArrayList<>();
		for (TenderLocation tenderLocation : tenderLocations) {
			if (!subsidiary.getAssignedArea().getLocations().contains(tenderLocation.getLocation())) {
				boolean isNotTransmited = TransmissionStatus._NEW_.equals(tenderLocation.getTransmissionStatus())
						|| TransmissionStatus._FAILED_.equals(tenderLocation.getTransmissionStatus());

				if (BiddingStatus._NEW_.equals(tenderLocation.getLocationBiddingStatus()) && isNotTransmited) {
					noBidLocations.add(tenderLocation);
					submitedRequests.add(tenderLocation.getLocation().getCode() + " - " + tenderLocation.getLocation().getName());
				}
			}
		}

		// Submit to DataHub
		this.submitTenderLocationsNoBid(noBidLocations);

		// Display the results to the interface
		if (noBidLocations.isEmpty()) {
			responseDescription.append(CmmErrorCode.SUBMITED_TENDER_LOCATIONS_NO_BID_EMPTY.getErrMsg());
		} else {
			responseDescription.append(
					String.format(CmmErrorCode.SUBMITED_TENDER_LOCATIONS_NO_BID.getErrMsg(), StringUtil.unorderedList(submitedRequests.toArray())));
		}

		param.setSubmitToDataHubResponseResult(true);
		param.setSubmitToDataHubResponseDescription(responseDescription.toString());
	}

	/**
	 * Finds the bids for the given tender location
	 *
	 * @param tenderLocationDs
	 * @param param
	 * @throws Exception
	 */
	public void findBidForLocation(TenderLocation_Ds tenderLocationDs, TenderLocation_DsParam param) throws Exception {
		ITenderLocationService tenderLocationService = (ITenderLocationService) this.findEntityService(TenderLocation.class);
		TenderLocation tenderLocation = tenderLocationService.findById(tenderLocationDs.getId());

		// Check for concurency
		if (TenderStatus._DRAFT_.equals(tenderLocation.getTender().getStatus())
				|| TenderStatus._CANCELED_.equals(tenderLocation.getTender().getStatus())
				|| TenderStatus._EXPIRED_.equals(tenderLocation.getTender().getStatus())
				|| BiddingStatus._FINISHED_.equals(tenderLocation.getLocationBiddingStatus())) {

			throw new BusinessException(CmmErrorCode.TENDER_LOCATION_RPC_ACTION_ERROR, String.format(
					CmmErrorCode.TENDER_LOCATION_RPC_ACTION_ERROR.getErrMsg(), StringUtil.unorderedList(tenderLocation.getLocation().getCode())));
		}

		IContractService contractService = (IContractService) this.findEntityService(Contract.class);

		// Find bids by Tender and Location
		Map<String, Object> params = new HashMap<>();
		params.put("bidTenderIdentification", tenderLocation.getTender());
		params.put("location", tenderLocation.getLocation());
		List<Contract> bids = contractService.findEntitiesByAttributes(params);

		// Calculate the new version based on the existing bids
		IBidVersionService bidService = this.getApplicationContext().getBean(BidVersionService.class);
		String bidVersion = bidService.calculateBidVersion(bids);
		param.setBidVersion(bidVersion);
		param.setExistingBids(!CollectionUtils.isEmpty(bids));
	}

	private void submitTenderLocationsNoBid(List<TenderLocation> tenderLocations) throws Exception {
		if (!tenderLocations.isEmpty()) {
			IExternalInterfaceService interfaceService = (IExternalInterfaceService) this.findEntityService(ExternalInterface.class);
			ExternalInterface externalInterface = interfaceService.findByName(ExtInterfaceNames.FUEL_TENDER_NO_BID_INTERFACE.getValue());
			ExtInterfaceParameters extInterfaceParams = new ExtInterfaceParameters(externalInterface.getParams());

			ITenderLocationService tenderLocationService = (ITenderLocationService) this.findEntityService(TenderLocation.class);
			IDataHubClientRequestService<TenderLocation, FuelTenderNoBid> requestService = this.getApplicationContext().getBean(NoBidService.class);
			ExternalInterfaceHistoryFacade historyFacade = (ExternalInterfaceHistoryFacade) this.getApplicationContext().getBean(BEAN_HISTORY_FACADE);
			ExternalInterfaceMessageHistoryFacade messageHistoryFacade = (ExternalInterfaceMessageHistoryFacade) this.getApplicationContext()
					.getBean(BEAN_MESSAGE_HISTORY_FACADE);
			TenderNoBidTransformer transformer = (TenderNoBidTransformer) this.getApplicationContext().getBean(BEAN_NO_BID_TRANSFORMER);

			if (!externalInterface.getEnabled()) {
				throw new BusinessException(BusinessErrorCode.OUT_INTERFACE_DISABLED_WITH_DETAILS,
						String.format(BusinessErrorCode.OUT_INTERFACE_DISABLED_WITH_DETAILS.getErrMsg(),
								ExtInterfaceNames.FUEL_TENDER_NO_BID_INTERFACE.getValue()));
			}

			// Update tender locations bidding status
			tenderLocations.forEach(l -> l.setTransmissionStatus(TransmissionStatus._IN_PROGRESS_));
			tenderLocationService.update(tenderLocations);

			// Prepare params for datahub soap client
			DataHubSoapClient<TenderLocation, FuelTenderNoBid> dataHubSoapClient = new DataHubSoapClient<>(externalInterface, extInterfaceParams,
					tenderLocations, historyFacade, messageHistoryFacade, transformer, FuelTenderNoBid.class, DataHubRequestType.BULK_REQUESTS);
			dataHubSoapClient.setRequestService(requestService);
			dataHubSoapClient.start(1);
		}
	}

	public void downloadBidForm(TenderLocation_Ds ds, TenderLocation_DsParam param) throws Exception {
		ITenderLocationService tenderLocationService = (ITenderLocationService) this.findEntityService(TenderLocation.class);
		byte[] fileBytes = tenderLocationService.generateBidForm(ds.getId());
		String path = Session.user.get().getWorkspace().getTempPath();
		String fileName = "Bid capture form" + ".xlsx";
		File file = new File(path + File.separator + fileName);
		FileUtils.writeByteArrayToFile(file.getAbsoluteFile(), fileBytes);
		param.setFormFile(fileName);
	}

	private Map<String, Object> extractTenderLocationParams(TenderLocation_DsParam params) {
		Map<String, Object> generateBidParams = new HashMap<>();

		generateBidParams.put(TenderDefaultConstants.PARAM_CAPTURE_BID_VERSION, params.getBidVersion());
		generateBidParams.put(TenderDefaultConstants.PARAM_CAPTURE_BID_FROM_HOLDER_MA, params.getCaptureBidFromHolderMA());
		generateBidParams.put(TenderDefaultConstants.PARAM_CAPTURE_BID_FROM_RECEIVER_MA, params.getCaptureBidFromReceiverMA());
		generateBidParams.put(TenderDefaultConstants.PARAM_CAPTURE_BID_FROM_EXISTING_DATA, params.getCaptureBidFromExistingBids());

		return generateBidParams;
	}

}
