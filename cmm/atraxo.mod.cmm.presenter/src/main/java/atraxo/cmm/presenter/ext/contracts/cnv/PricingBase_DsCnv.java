/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.contracts.cnv;

import javax.persistence.EntityManager;

import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.presenter.impl.contracts.model.PricingBase_Ds;
import seava.j4e.api.action.result.IDsConverter;
import seava.j4e.presenter.converter.AbstractDsConverter;

/**
 * Custom converter PricingBase_DsCnv
 */
public class PricingBase_DsCnv extends AbstractDsConverter<PricingBase_Ds, PricingBase> implements IDsConverter<PricingBase_Ds, PricingBase> {

	@Override
	public void modelToEntity(PricingBase_Ds m, PricingBase e, boolean isInsert, EntityManager em) throws Exception {
		super.modelToEntity(m, e, isInsert, em);

		e.setAverageMethodId(m.getAvgMthdId());
		e.setFinancialSourceId(m.getFinancialSourceId());
	}

}
