/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.contracts.cnv;

import javax.persistence.EntityManager;

import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.presenter.impl.contracts.model.Contract_Ds;
import seava.j4e.api.action.result.IDsConverter;
import seava.j4e.presenter.converter.AbstractDsConverter;

/**
 * Custom converter Contract_DsCnv
 */
public class Contract_DsCnv extends AbstractDsConverter<Contract_Ds, Contract> implements IDsConverter<Contract_Ds, Contract> {

	@Override
	public void modelToEntity(Contract_Ds m, Contract e, boolean isInsert, EntityManager em) throws Exception {
		super.modelToEntity(m, e, isInsert, em);
		if (e.getPeriodApprovalStatus() == null) {
			e.setPeriodApprovalStatus(BidApprovalStatus._EMPTY_);
		}
		if (e.getShipToApprovalStatus() == null) {
			e.setShipToApprovalStatus(BidApprovalStatus._EMPTY_);
		}
		if (e.getPriceApprovalStatus() == null) {
			e.setPriceApprovalStatus(BidApprovalStatus._EMPTY_);
		}
		if (e.getIsBlueprint() == null) {
			e.setIsBlueprint(false);
		}
		if (e.getBidAcceptAwardTransmissionStatus() == null) {
			e.setBidAcceptAwardTransmissionStatus(TransmissionStatus._EMPTY_);
		}
		if (e.getBidDeclineAwardTransmissionStatus() == null) {
			e.setBidDeclineAwardTransmissionStatus(TransmissionStatus._EMPTY_);
		}
		if (e.getFlightServiceType() == null) {
			e.setFlightServiceType(FlightServiceType._EMPTY_);
		}
		if (e.getBidCancelTransmitionStatus() == null) {
			e.setBidCancelTransmitionStatus(TransmissionStatus._EMPTY_);
		}

	}
}
