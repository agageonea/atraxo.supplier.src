/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.tender.model;

import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.TenderSource;
import atraxo.cmm.domain.impl.cmm_type.TenderStatus;
import atraxo.cmm.domain.impl.cmm_type.TenderType;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.TenderVolumeOffer;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Tender.class, sort = {@SortField(field = Tender_Ds.F_PUBLISHEDON, desc = true)})
@RefLookups({
		@RefLookup(refId = Tender_Ds.F_HOLDERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Tender_Ds.F_HOLDERCODE)}),
		@RefLookup(refId = Tender_Ds.F_CONTACTID)})
public class Tender_Ds extends AbstractSubsidiaryDs_Ds<Tender> {

	public static final String ALIAS = "cmm_Tender_Ds";

	public static final String F_HOLDERID = "holderId";
	public static final String F_HOLDERCODE = "holderCode";
	public static final String F_HOLDERNAME = "holderName";
	public static final String F_CONTACTID = "contactId";
	public static final String F_CONTACTNAME = "contactName";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_TENDERVERSION = "tenderVersion";
	public static final String F_TYPE = "type";
	public static final String F_CONTACTPERSON = "contactPerson";
	public static final String F_EMAIL = "email";
	public static final String F_PHONE = "phone";
	public static final String F_BIDDINGPERIODFROM = "biddingPeriodFrom";
	public static final String F_BIDDINGPERIODTO = "biddingPeriodTo";
	public static final String F_COMMENTS = "comments";
	public static final String F_STATUS = "status";
	public static final String F_BIDDINGSTATUS = "biddingStatus";
	public static final String F_TRANSMISSIONSTATUS = "transmissionStatus";
	public static final String F_SOURCE = "source";
	public static final String F_CANCELEDON = "canceledOn";
	public static final String F_UPDATEDON = "updatedOn";
	public static final String F_PUBLISHEDON = "publishedOn";
	public static final String F_CLOSED = "closed";
	public static final String F_MSGID = "msgId";
	public static final String F_NOOFROUNDS = "noOfRounds";
	public static final String F_SCHEMAVERSION = "schemaVersion";
	public static final String F_VOLUMEOFFER = "volumeOffer";
	public static final String F_READ = "read";
	public static final String F_LABELFIELD = "labelField";
	public static final String F_VOLUMEUNIT = "volumeUnit";
	public static final String F_TOTALVOLUME = "totalVolume";
	public static final String F_TOTALVOLUMEHEADER = "totalVolumeHeader";
	public static final String F_AGREEMENTFROM = "agreementFrom";
	public static final String F_AGREEMENTTO = "agreementTo";
	public static final String F_FORMTITLE = "formTitle";
	public static final String F_SUBSIDIARYCODE = "subsidiaryCode";

	@DsField(join = "left", path = "holder.id")
	private Integer holderId;

	@DsField(join = "left", path = "holder.code")
	private String holderCode;

	@DsField(join = "left", path = "holder.name")
	private String holderName;

	@DsField(join = "left", path = "contact.id")
	private Integer contactId;

	@DsField(join = "left", fetch = false, path = "contact.fullNameField")
	private String contactName;

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private Long tenderVersion;

	@DsField
	private TenderType type;

	@DsField
	private String contactPerson;

	@DsField
	private String email;

	@DsField
	private String phone;

	@DsField
	private Date biddingPeriodFrom;

	@DsField
	private Date biddingPeriodTo;

	@DsField
	private String comments;

	@DsField
	private TenderStatus status;

	@DsField
	private BiddingStatus biddingStatus;

	@DsField
	private TransmissionStatus transmissionStatus;

	@DsField
	private TenderSource source;

	@DsField
	private Date canceledOn;

	@DsField
	private Date updatedOn;

	@DsField
	private Date publishedOn;

	@DsField
	private Boolean closed;

	@DsField
	private Long msgId;

	@DsField
	private Integer noOfRounds;

	@DsField
	private String schemaVersion;

	@DsField
	private TenderVolumeOffer volumeOffer;

	@DsField(path = "isRead")
	private Boolean read;

	@DsField(fetch = false)
	private String labelField;

	@DsField(fetch = false)
	private String volumeUnit;

	@DsField(fetch = false)
	private BigDecimal totalVolume;

	@DsField(fetch = false)
	private String totalVolumeHeader;

	@DsField(fetch = false)
	private Date agreementFrom;

	@DsField(fetch = false)
	private Date agreementTo;

	@DsField(fetch = false)
	private String formTitle;

	@DsField(fetch = false)
	private String subsidiaryCode;

	/**
	 * Default constructor
	 */
	public Tender_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Tender_Ds(Tender e) {
		super(e);
	}

	public Integer getHolderId() {
		return this.holderId;
	}

	public void setHolderId(Integer holderId) {
		this.holderId = holderId;
	}

	public String getHolderCode() {
		return this.holderCode;
	}

	public void setHolderCode(String holderCode) {
		this.holderCode = holderCode;
	}

	public String getHolderName() {
		return this.holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public Integer getContactId() {
		return this.contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}

	public String getContactName() {
		return this.contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getTenderVersion() {
		return this.tenderVersion;
	}

	public void setTenderVersion(Long tenderVersion) {
		this.tenderVersion = tenderVersion;
	}

	public TenderType getType() {
		return this.type;
	}

	public void setType(TenderType type) {
		this.type = type;
	}

	public String getContactPerson() {
		return this.contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getBiddingPeriodFrom() {
		return this.biddingPeriodFrom;
	}

	public void setBiddingPeriodFrom(Date biddingPeriodFrom) {
		this.biddingPeriodFrom = biddingPeriodFrom;
	}

	public Date getBiddingPeriodTo() {
		return this.biddingPeriodTo;
	}

	public void setBiddingPeriodTo(Date biddingPeriodTo) {
		this.biddingPeriodTo = biddingPeriodTo;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public TenderStatus getStatus() {
		return this.status;
	}

	public void setStatus(TenderStatus status) {
		this.status = status;
	}

	public BiddingStatus getBiddingStatus() {
		return this.biddingStatus;
	}

	public void setBiddingStatus(BiddingStatus biddingStatus) {
		this.biddingStatus = biddingStatus;
	}

	public TransmissionStatus getTransmissionStatus() {
		return this.transmissionStatus;
	}

	public void setTransmissionStatus(TransmissionStatus transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public TenderSource getSource() {
		return this.source;
	}

	public void setSource(TenderSource source) {
		this.source = source;
	}

	public Date getCanceledOn() {
		return this.canceledOn;
	}

	public void setCanceledOn(Date canceledOn) {
		this.canceledOn = canceledOn;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Date getPublishedOn() {
		return this.publishedOn;
	}

	public void setPublishedOn(Date publishedOn) {
		this.publishedOn = publishedOn;
	}

	public Boolean getClosed() {
		return this.closed;
	}

	public void setClosed(Boolean closed) {
		this.closed = closed;
	}

	public Long getMsgId() {
		return this.msgId;
	}

	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}

	public Integer getNoOfRounds() {
		return this.noOfRounds;
	}

	public void setNoOfRounds(Integer noOfRounds) {
		this.noOfRounds = noOfRounds;
	}

	public String getSchemaVersion() {
		return this.schemaVersion;
	}

	public void setSchemaVersion(String schemaVersion) {
		this.schemaVersion = schemaVersion;
	}

	public TenderVolumeOffer getVolumeOffer() {
		return this.volumeOffer;
	}

	public void setVolumeOffer(TenderVolumeOffer volumeOffer) {
		this.volumeOffer = volumeOffer;
	}

	public Boolean getRead() {
		return this.read;
	}

	public void setRead(Boolean read) {
		this.read = read;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}

	public String getVolumeUnit() {
		return this.volumeUnit;
	}

	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}

	public BigDecimal getTotalVolume() {
		return this.totalVolume;
	}

	public void setTotalVolume(BigDecimal totalVolume) {
		this.totalVolume = totalVolume;
	}

	public String getTotalVolumeHeader() {
		return this.totalVolumeHeader;
	}

	public void setTotalVolumeHeader(String totalVolumeHeader) {
		this.totalVolumeHeader = totalVolumeHeader;
	}

	public Date getAgreementFrom() {
		return this.agreementFrom;
	}

	public void setAgreementFrom(Date agreementFrom) {
		this.agreementFrom = agreementFrom;
	}

	public Date getAgreementTo() {
		return this.agreementTo;
	}

	public void setAgreementTo(Date agreementTo) {
		this.agreementTo = agreementTo;
	}

	public String getFormTitle() {
		return this.formTitle;
	}

	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}

	public String getSubsidiaryCode() {
		return this.subsidiaryCode;
	}

	public void setSubsidiaryCode(String subsidiaryCode) {
		this.subsidiaryCode = subsidiaryCode;
	}
}
