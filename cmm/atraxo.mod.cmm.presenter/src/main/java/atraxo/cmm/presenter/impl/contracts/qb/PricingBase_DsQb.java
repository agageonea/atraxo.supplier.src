/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.contracts.qb;

import atraxo.cmm.presenter.impl.contracts.model.PricingBase_Ds;
import atraxo.cmm.presenter.impl.contracts.model.PricingBase_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class PricingBase_DsQb
		extends
			QueryBuilderWithJpql<PricingBase_Ds, PricingBase_Ds, PricingBase_DsParam> {
}
