/**
 *
 */
package atraxo.cmm.presenter.ext.contracts.service;

import java.math.BigDecimal;
import java.math.MathContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IPricingBaseService;
import atraxo.cmm.presenter.impl.contracts.model.ContractCustomerWizard_Ds;
import atraxo.cmm.presenter.impl.contracts.model.PricingBase_Ds;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.tolerance.IToleranceService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.tolerance.ToleranceResult;
import atraxo.fmbas.business.ext.tolerance.ToleranceVerifier;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public class ConversionFactorValidator implements IConversionFactorValidator {

	@Autowired
	private IPricingBaseService pricingBaseService;
	@Autowired
	private ISystemParameterService systemParameterService;
	@Autowired
	private IUnitService unitService;
	@Autowired
	private IToleranceService toleranceService;

	/*
	 * (non-Javadoc)
	 * @see
	 * atraxo.cmm.presenter.ext.contracts.service.IConversionFactorService#verifyPricingBase(atraxo.cmm.presenter.impl.contracts.model.PricingBase_Ds)
	 */
	@Override
	public void validatePricingBase(PricingBase_Ds ds) throws BusinessException {
		InputDs input = new InputDs(ds.getFactor(), ds.getOperator(), ds.getQuotUnitId(), ds.getConvUnitId(), ds.getConvUnitTypeInd());
		Result result = this.validate(input);
		ds.setCalculatedDensity(result.getDensity());
		ds.setConversionFactorValidationMsg(result.getMessage());

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * atraxo.cmm.presenter.ext.contracts.service.IConversionFactorService#verifyContractCustomerWizard(atraxo.cmm.presenter.impl.contracts.model.
	 * ContractCustomerWizard_Ds)
	 */
	@Override
	public void validateContractCustomerWizard(ContractCustomerWizard_Ds ds) throws BusinessException {
		InputDs input = new InputDs(ds.getFactor(), ds.getOperator(), ds.getQuotUnitId(), ds.getConvUnitId(), ds.getConvUnitTypeInd());
		Result result = this.validate(input);
		ds.setCalculatedDensity(result.getDensity());
		ds.setConversionFactorValidationMsg(result.getMessage());

	}

	private ToleranceResult checkTolerance(BigDecimal amount, BigDecimal calcAmount, Tolerance tolerance) throws BusinessException {

		ToleranceVerifier verifier = new ToleranceVerifier(null, null, null);
		return verifier.checkTolerance(amount, calcAmount, tolerance);

	}

	private Result validate(InputDs ds) throws BusinessException {

		BigDecimal value = ds.getFactor();
		if (Operator._DIVIDE_.equals(ds.getOperator())) {
			value = BigDecimal.ONE.divide(value, MathContext.DECIMAL64);
		}

		Unit sysMass = this.unitService.findByCode(this.systemParameterService.getSysWeight());
		Unit sysVol = this.unitService.findByCode(this.systemParameterService.getSysVol());
		BigDecimal density = new BigDecimal(this.systemParameterService.getDensity());

		BigDecimal b1 = this.pricingBaseService.convertUnit(ds.getQuotationUnitId(), sysMass.getId(), value, density);
		BigDecimal b2 = this.pricingBaseService.convertUnit(ds.getConversionUnitId(), sysVol.getId(), new BigDecimal(1), density);
		BigDecimal b3 = this.pricingBaseService.convertUnit(ds.getConversionUnitId(), sysMass.getId(), new BigDecimal(1), density);
		BigDecimal b4 = this.pricingBaseService.convertUnit(ds.getQuotationUnitId(), sysVol.getId(), value, density);
		BigDecimal calcval;
		if (UnitType._MASS_.equals(ds.getUnitType())) {
			calcval = b1.divide(b2, MathContext.DECIMAL64);
		} else {
			calcval = b3.divide(b4, MathContext.DECIMAL64);
		}
		Result result;
		Tolerance tolerance = this.toleranceService.getConversionFactorTolerance();

		ToleranceResult toleranceResult = this.checkTolerance(BigDecimal.valueOf(Double.parseDouble(this.systemParameterService.getDensity())),
				calcval, tolerance);
		switch (toleranceResult.getType()) {
		case NOT_WITHIN:
		case MARGIN:
			result = new Result(calcval,
					String.format(BusinessErrorCode.CONVERSION_FACTOR_VALIDATION.getErrMsg(), tolerance.getRelativeDevDown().intValue()));
			break;
		default:
			result = new Result(calcval, StringUtils.EMPTY);
			break;
		}
		return result;

	}

	private class InputDs {
		private final BigDecimal factor;
		private final Operator operator;
		private final int quotationUnitId;
		private final int conversionUnitId;
		private final UnitType unitType;

		public InputDs(BigDecimal factor, Operator operator, int quotationUnitId, int conversionUnitId, UnitType unitType) {
			super();
			this.factor = factor;
			this.operator = operator;
			this.quotationUnitId = quotationUnitId;
			this.conversionUnitId = conversionUnitId;
			this.unitType = unitType;
		}

		public BigDecimal getFactor() {
			return this.factor;
		}

		public Operator getOperator() {
			return this.operator;
		}

		public int getQuotationUnitId() {
			return this.quotationUnitId;
		}

		public int getConversionUnitId() {
			return this.conversionUnitId;
		}

		public UnitType getUnitType() {
			return this.unitType;
		}

	}

	private class Result {
		private final BigDecimal density;
		private final String message;

		public Result(BigDecimal density, String message) {
			super();
			this.density = density;
			this.message = message;
		}

		public BigDecimal getDensity() {
			return this.density;
		}

		public String getMessage() {
			return this.message;
		}

	}
}
