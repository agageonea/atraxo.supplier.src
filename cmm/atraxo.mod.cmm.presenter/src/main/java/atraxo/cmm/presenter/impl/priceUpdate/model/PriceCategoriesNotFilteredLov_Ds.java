/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.priceUpdate.model;

import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = PriceCategory.class, sort = {@SortField(field = PriceCategoriesNotFilteredLov_Ds.F_NAME)})
public class PriceCategoriesNotFilteredLov_Ds
		extends
			AbstractLov_Ds<PriceCategory> {

	public static final String ALIAS = "cmm_PriceCategoriesNotFilteredLov_Ds";

	public static final String F_NAME = "name";
	public static final String F_TYPE = "type";
	public static final String F_PRICEPER = "pricePer";
	public static final String F_ACTIVE = "active";

	@DsField
	private String name;

	@DsField
	private PriceType type;

	@DsField
	private PriceInd pricePer;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public PriceCategoriesNotFilteredLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public PriceCategoriesNotFilteredLov_Ds(PriceCategory e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PriceType getType() {
		return this.type;
	}

	public void setType(PriceType type) {
		this.type = type;
	}

	public PriceInd getPricePer() {
		return this.pricePer;
	}

	public void setPricePer(PriceInd pricePer) {
		this.pricePer = pricePer;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
