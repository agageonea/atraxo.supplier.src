/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.cmm.presenter.ext.contracts.qb;

import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.presenter.impl.contracts.model.ContractCustomer_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class ContractCustomer_DsQb extends QueryBuilderWithJpql<ContractCustomer_Ds, ContractCustomer_Ds, ContractCustomer_Ds> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();
		ContractCustomer_Ds f = this.getFilter();
		if (f.getId() == null) {
			this.addFilterCondition("e.dealType = :dealType");
			this.addFilterCondition("e.isBlueprint = false");
			this.addCustomFilterItem("dealType", DealType._SELL_);
		}
	}
}
