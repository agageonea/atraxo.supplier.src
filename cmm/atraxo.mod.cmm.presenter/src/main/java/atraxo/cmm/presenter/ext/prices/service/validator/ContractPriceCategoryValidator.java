package atraxo.cmm.presenter.ext.prices.service.validator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterBaseService;

/**
 * @author zspeter
 */
public class ContractPriceCategoryValidator extends AbstractPresenterBaseService {

	@Autowired
	private IContractPriceCategoryService srv;

	/**
	 * @param e
	 * @throws BusinessException
	 */
	public void checkRestrictions(ContractPriceCategory e) throws BusinessException {
		if (!e.getRestriction() && e.getPricingBases() == null) {
			Map<String, Object> params = new HashMap<>();
			params.put("priceCategory", e.getPriceCategory());
			params.put("contract", e.getContract());
			List<ContractPriceCategory> list = this.srv.findEntitiesByAttributes(ContractPriceCategory.class, params);
			for (ContractPriceCategory cpc : list) {
				if (!cpc.getId().equals(e.getId()) && !cpc.getRestriction()) {
					throw new BusinessException(CmmErrorCode.PC_SAME, CmmErrorCode.PC_SAME.getErrMsg());
				}
			}
		}
	}

}
