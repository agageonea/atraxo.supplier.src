/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.prices.model;

import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ContractPriceComponent.class, sort = {@SortField(field = ContractPriceComponent_Ds.F_VALIDFROM, desc = true)})
@RefLookups({@RefLookup(refId = ContractPriceComponent_Ds.F_CONTRACTPRICECAT),
		@RefLookup(refId = ContractPriceComponent_Ds.F_CURRENCY),
		@RefLookup(refId = ContractPriceComponent_Ds.F_UNIT)})
public class ContractPriceComponent_Ds
		extends
			AbstractDs_Ds<ContractPriceComponent> {

	public static final String ALIAS = "cmm_ContractPriceComponent_Ds";

	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_PROVISIONAL = "provisional";
	public static final String F_PRICE = "price";
	public static final String F_WITHHOLD = "withHold";
	public static final String F_TOLERANCEMESSAGE = "toleranceMessage";
	public static final String F_CONTRACTPRICECAT = "contractPriceCat";
	public static final String F_CONTRACTPRICECATNAME = "contractPriceCatName";
	public static final String F_PRICECATEGORYID = "priceCategoryId";
	public static final String F_PRICECTGRYNAME = "priceCtgryName";
	public static final String F_PRICECTGRYPRICEPER = "priceCtgryPricePer";
	public static final String F_PRICINGBASESID = "pricingBasesId";
	public static final String F_CURRENCY = "currency";
	public static final String F_CURRENCYCODE = "currencyCode";
	public static final String F_UNIT = "unit";
	public static final String F_UNITCODE = "unitCode";

	@DsField(jpqlFilter = "e.validTo >= :validFrom ")
	private Date validFrom;

	@DsField(jpqlFilter = "e.validFrom <= :validTo ")
	private Date validTo;

	@DsField
	private Boolean provisional;

	@DsField
	private BigDecimal price;

	@DsField
	private Boolean withHold;

	@DsField(fetch = false)
	private String toleranceMessage;

	@DsField(join = "left", path = "contrPriceCtgry.id")
	private Integer contractPriceCat;

	@DsField(join = "left", path = "contrPriceCtgry.name")
	private String contractPriceCatName;

	@DsField(join = "left", path = "contrPriceCtgry.priceCategory.id")
	private Integer priceCategoryId;

	@DsField(join = "left", path = "contrPriceCtgry.priceCategory.name")
	private String priceCtgryName;

	@DsField(join = "left", path = "contrPriceCtgry.priceCategory.pricePer")
	private PriceInd priceCtgryPricePer;

	@DsField(join = "left", path = "contrPriceCtgry.pricingBases.id")
	private Integer pricingBasesId;

	@DsField(join = "left", path = "currency.id")
	private Integer currency;

	@DsField(join = "left", path = "currency.code")
	private String currencyCode;

	@DsField(join = "left", path = "unit.id")
	private Integer unit;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	/**
	 * Default constructor
	 */
	public ContractPriceComponent_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ContractPriceComponent_Ds(ContractPriceComponent e) {
		super(e);
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Boolean getProvisional() {
		return this.provisional;
	}

	public void setProvisional(Boolean provisional) {
		this.provisional = provisional;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Boolean getWithHold() {
		return this.withHold;
	}

	public void setWithHold(Boolean withHold) {
		this.withHold = withHold;
	}

	public String getToleranceMessage() {
		return this.toleranceMessage;
	}

	public void setToleranceMessage(String toleranceMessage) {
		this.toleranceMessage = toleranceMessage;
	}

	public Integer getContractPriceCat() {
		return this.contractPriceCat;
	}

	public void setContractPriceCat(Integer contractPriceCat) {
		this.contractPriceCat = contractPriceCat;
	}

	public String getContractPriceCatName() {
		return this.contractPriceCatName;
	}

	public void setContractPriceCatName(String contractPriceCatName) {
		this.contractPriceCatName = contractPriceCatName;
	}

	public Integer getPriceCategoryId() {
		return this.priceCategoryId;
	}

	public void setPriceCategoryId(Integer priceCategoryId) {
		this.priceCategoryId = priceCategoryId;
	}

	public String getPriceCtgryName() {
		return this.priceCtgryName;
	}

	public void setPriceCtgryName(String priceCtgryName) {
		this.priceCtgryName = priceCtgryName;
	}

	public PriceInd getPriceCtgryPricePer() {
		return this.priceCtgryPricePer;
	}

	public void setPriceCtgryPricePer(PriceInd priceCtgryPricePer) {
		this.priceCtgryPricePer = priceCtgryPricePer;
	}

	public Integer getPricingBasesId() {
		return this.pricingBasesId;
	}

	public void setPricingBasesId(Integer pricingBasesId) {
		this.pricingBasesId = pricingBasesId;
	}

	public Integer getCurrency() {
		return this.currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Integer getUnit() {
		return this.unit;
	}

	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}
}
