/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.tender.model;

/**
 * Generated code. Do not modify in this file.
 */
public class TenderLocation_DsParam {

	public static final String f_bidVersion = "bidVersion";
	public static final String f_existingBids = "existingBids";
	public static final String f_generatedBidId = "generatedBidId";
	public static final String f_generatedBidCode = "generatedBidCode";
	public static final String f_captureBidFromLocation = "captureBidFromLocation";
	public static final String f_selectReceivers = "selectReceivers";
	public static final String f_captureBidFromHolderMA = "captureBidFromHolderMA";
	public static final String f_captureBidFromReceiverMA = "captureBidFromReceiverMA";
	public static final String f_captureBidFromExistingBids = "captureBidFromExistingBids";
	public static final String f_submitToDataHubResponseResult = "submitToDataHubResponseResult";
	public static final String f_submitToDataHubResponseDescription = "submitToDataHubResponseDescription";
	public static final String f_formFile = "formFile";

	private String bidVersion;

	private Boolean existingBids;

	private Integer generatedBidId;

	private String generatedBidCode;

	private Boolean captureBidFromLocation;

	private Boolean selectReceivers;

	private Boolean captureBidFromHolderMA;

	private Boolean captureBidFromReceiverMA;

	private Boolean captureBidFromExistingBids;

	private Boolean submitToDataHubResponseResult;

	private String submitToDataHubResponseDescription;

	private String formFile;

	public String getBidVersion() {
		return this.bidVersion;
	}

	public void setBidVersion(String bidVersion) {
		this.bidVersion = bidVersion;
	}

	public Boolean getExistingBids() {
		return this.existingBids;
	}

	public void setExistingBids(Boolean existingBids) {
		this.existingBids = existingBids;
	}

	public Integer getGeneratedBidId() {
		return this.generatedBidId;
	}

	public void setGeneratedBidId(Integer generatedBidId) {
		this.generatedBidId = generatedBidId;
	}

	public String getGeneratedBidCode() {
		return this.generatedBidCode;
	}

	public void setGeneratedBidCode(String generatedBidCode) {
		this.generatedBidCode = generatedBidCode;
	}

	public Boolean getCaptureBidFromLocation() {
		return this.captureBidFromLocation;
	}

	public void setCaptureBidFromLocation(Boolean captureBidFromLocation) {
		this.captureBidFromLocation = captureBidFromLocation;
	}

	public Boolean getSelectReceivers() {
		return this.selectReceivers;
	}

	public void setSelectReceivers(Boolean selectReceivers) {
		this.selectReceivers = selectReceivers;
	}

	public Boolean getCaptureBidFromHolderMA() {
		return this.captureBidFromHolderMA;
	}

	public void setCaptureBidFromHolderMA(Boolean captureBidFromHolderMA) {
		this.captureBidFromHolderMA = captureBidFromHolderMA;
	}

	public Boolean getCaptureBidFromReceiverMA() {
		return this.captureBidFromReceiverMA;
	}

	public void setCaptureBidFromReceiverMA(Boolean captureBidFromReceiverMA) {
		this.captureBidFromReceiverMA = captureBidFromReceiverMA;
	}

	public Boolean getCaptureBidFromExistingBids() {
		return this.captureBidFromExistingBids;
	}

	public void setCaptureBidFromExistingBids(Boolean captureBidFromExistingBids) {
		this.captureBidFromExistingBids = captureBidFromExistingBids;
	}

	public Boolean getSubmitToDataHubResponseResult() {
		return this.submitToDataHubResponseResult;
	}

	public void setSubmitToDataHubResponseResult(
			Boolean submitToDataHubResponseResult) {
		this.submitToDataHubResponseResult = submitToDataHubResponseResult;
	}

	public String getSubmitToDataHubResponseDescription() {
		return this.submitToDataHubResponseDescription;
	}

	public void setSubmitToDataHubResponseDescription(
			String submitToDataHubResponseDescription) {
		this.submitToDataHubResponseDescription = submitToDataHubResponseDescription;
	}

	public String getFormFile() {
		return this.formFile;
	}

	public void setFormFile(String formFile) {
		this.formFile = formFile;
	}
}
