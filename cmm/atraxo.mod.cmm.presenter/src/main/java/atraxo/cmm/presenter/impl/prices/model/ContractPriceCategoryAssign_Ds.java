/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.prices.model;

import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.QuantityType;
import atraxo.cmm.domain.impl.cmm_type.VatApplicability;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.fmbas_type.Use;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ContractPriceCategory.class)
@RefLookups({@RefLookup(refId = ContractPriceCategoryAssign_Ds.F_CONTRACT),
		@RefLookup(refId = ContractPriceCategoryAssign_Ds.F_PRICECATEGORY),
		@RefLookup(refId = ContractPriceCategoryAssign_Ds.F_FINANCIALSOURCEID),
		@RefLookup(refId = ContractPriceCategoryAssign_Ds.F_AVGMTHDID)})
public class ContractPriceCategoryAssign_Ds
		extends
			AbstractDs_Ds<ContractPriceCategory> {

	public static final String ALIAS = "cmm_ContractPriceCategoryAssign_Ds";

	public static final String F_CONTID = "contId";
	public static final String F_CONTCODE = "contCode";
	public static final String F_CONTSTATUS = "contStatus";
	public static final String F_READONLY = "readOnly";
	public static final String F_BIDAPPROVALSTATUS = "bidApprovalStatus";
	public static final String F_SUPPID = "suppId";
	public static final String F_SUPPCODE = "suppCode";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_LOCID = "locId";
	public static final String F_LOCCODE = "locCode";
	public static final String F_FORMTITLE = "formTitle";
	public static final String F_NAME = "name";
	public static final String F_CONTINOUS = "continous";
	public static final String F_INCLUDEINAVERAGE = "includeInAverage";
	public static final String F_QUANTITYTYPE = "quantityType";
	public static final String F_EXCHANGERATEOFFSET = "exchangeRateOffset";
	public static final String F_RESTRICTION = "restriction";
	public static final String F_VAT = "vat";
	public static final String F_DEFAULTPRICECTGY = "defaultPriceCtgy";
	public static final String F_COMMENTS = "comments";
	public static final String F_FOREX = "forex";
	public static final String F_CALCULATEINDICATOR = "calculateIndicator";
	public static final String F_PRICECATEGORYIDSJSON = "priceCategoryIdsJson";
	public static final String F_CONTRACT = "contract";
	public static final String F_CONTRACTVALIDFROM = "contractValidFrom";
	public static final String F_CONTRACTVALIDTO = "contractValidTo";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_VALUE = "value";
	public static final String F_CURRENCYID = "currencyId";
	public static final String F_CURRENCYCODE = "currencyCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_PRICECATEGORY = "priceCategory";
	public static final String F_PRICECTGRYNAME = "priceCtgryName";
	public static final String F_PRICECTGRYPRICEPER = "priceCtgryPricePer";
	public static final String F_IATAPRICECATCODE = "iataPriceCatCode";
	public static final String F_IATAPRICECATUSE = "iataPriceCatUse";
	public static final String F_MAINCATEGORYCODE = "mainCategoryCode";
	public static final String F_FINANCIALSOURCEID = "financialSourceId";
	public static final String F_FINANCIALSOURCENAME = "financialSourceName";
	public static final String F_FINANCIALSOURCECODE = "financialSourceCode";
	public static final String F_AVGMETHODINDICATORDEFMETH = "avgMethodIndicatorDefMeth";
	public static final String F_AVGMETHODINDICATORNAME = "avgMethodIndicatorName";
	public static final String F_AVGMTHDID = "avgMthdId";
	public static final String F_PRICE = "price";
	public static final String F_PRICINGBASEID = "pricingBaseId";
	public static final String F_CONVERTTITLE = "convertTitle";
	public static final String F_CONVERTEDPRICE = "convertedPrice";
	public static final String F_USED = "used";
	public static final String F_SLASH = "slash";
	public static final String F_PERCENTAGEOF = "percentageOf";
	public static final String F_LABELFIELD = "labelField";

	@DsField(join = "left", path = "contract.id")
	private Integer contId;

	@DsField(join = "left", path = "contract.code")
	private String contCode;

	@DsField(join = "left", path = "contract.status")
	private ContractStatus contStatus;

	@DsField(join = "left", path = "contract.readOnly")
	private Boolean readOnly;

	@DsField(join = "left", path = "contract.bidApprovalStatus")
	private BidApprovalStatus bidApprovalStatus;

	@DsField(join = "left", path = "contract.supplier.id")
	private Integer suppId;

	@DsField(join = "left", path = "contract.supplier.code")
	private String suppCode;

	@DsField(join = "left", path = "contract.customer.id")
	private Integer customerId;

	@DsField(join = "left", path = "contract.customer.code")
	private String customerCode;

	@DsField(join = "left", path = "contract.location.id")
	private Integer locId;

	@DsField(join = "left", path = "contract.location.code")
	private String locCode;

	@DsField(fetch = false)
	private String formTitle;

	@DsField
	private String name;

	@DsField
	private Boolean continous;

	@DsField
	private Boolean includeInAverage;

	@DsField
	private QuantityType quantityType;

	@DsField
	private MasterAgreementsPeriod exchangeRateOffset;

	@DsField
	private Boolean restriction;

	@DsField
	private VatApplicability vat;

	@DsField
	private Boolean defaultPriceCtgy;

	@DsField
	private String comments;

	@DsField(fetch = false)
	private String forex;

	@DsField
	private CalculateIndicator calculateIndicator;

	@DsField(fetch = false)
	private String priceCategoryIdsJson;

	@DsField(join = "left", path = "contract.id")
	private Integer contract;

	@DsField(join = "left", path = "contract.validFrom")
	private Date contractValidFrom;

	@DsField(join = "left", path = "contract.validTo")
	private Date contractValidTo;

	@DsField(fetch = false)
	private Date validFrom;

	@DsField(fetch = false)
	private Date validTo;

	@DsField(fetch = false)
	private BigDecimal value;

	@DsField(join = "left", path = "contract.settlementCurr.id")
	private Integer currencyId;

	@DsField(join = "left", path = "contract.settlementCurr.code")
	private String currencyCode;

	@DsField(join = "left", path = "contract.settlementUnit.id")
	private Integer unitId;

	@DsField(join = "left", path = "contract.settlementUnit.code")
	private String unitCode;

	@DsField(join = "left", path = "priceCategory.id")
	private Integer priceCategory;

	@DsField(join = "left", path = "priceCategory.name")
	private String priceCtgryName;

	@DsField(join = "left", path = "priceCategory.pricePer")
	private PriceInd priceCtgryPricePer;

	@DsField(join = "left", path = "priceCategory.iata.code")
	private String iataPriceCatCode;

	@DsField(join = "left", path = "priceCategory.iata.use")
	private Use iataPriceCatUse;

	@DsField(join = "left", path = "priceCategory.mainCategory.code")
	private String mainCategoryCode;

	@DsField(join = "left", path = "financialSource.id")
	private Integer financialSourceId;

	@DsField(join = "left", path = "financialSource.name")
	private String financialSourceName;

	@DsField(join = "left", path = "financialSource.code")
	private String financialSourceCode;

	@DsField(join = "left", path = "averageMethod.defaultMethod")
	private Boolean avgMethodIndicatorDefMeth;

	@DsField(join = "left", path = "averageMethod.name")
	private String avgMethodIndicatorName;

	@DsField(join = "left", path = "averageMethod.id")
	private Integer avgMthdId;

	@DsField(fetch = false)
	private BigDecimal price;

	@DsField(join = "left", path = "pricingBases.id")
	private Integer pricingBaseId;

	@DsField(fetch = false)
	private String convertTitle;

	@DsField(fetch = false)
	private BigDecimal convertedPrice;

	@DsField(fetch = false)
	private Boolean used;

	@DsField(fetch = false)
	private String slash;

	@DsField(fetch = false)
	private String percentageOf;

	@DsField(fetch = false)
	private String labelField;

	/**
	 * Default constructor
	 */
	public ContractPriceCategoryAssign_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ContractPriceCategoryAssign_Ds(ContractPriceCategory e) {
		super(e);
	}

	public Integer getContId() {
		return this.contId;
	}

	public void setContId(Integer contId) {
		this.contId = contId;
	}

	public String getContCode() {
		return this.contCode;
	}

	public void setContCode(String contCode) {
		this.contCode = contCode;
	}

	public ContractStatus getContStatus() {
		return this.contStatus;
	}

	public void setContStatus(ContractStatus contStatus) {
		this.contStatus = contStatus;
	}

	public Boolean getReadOnly() {
		return this.readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	public BidApprovalStatus getBidApprovalStatus() {
		return this.bidApprovalStatus;
	}

	public void setBidApprovalStatus(BidApprovalStatus bidApprovalStatus) {
		this.bidApprovalStatus = bidApprovalStatus;
	}

	public Integer getSuppId() {
		return this.suppId;
	}

	public void setSuppId(Integer suppId) {
		this.suppId = suppId;
	}

	public String getSuppCode() {
		return this.suppCode;
	}

	public void setSuppCode(String suppCode) {
		this.suppCode = suppCode;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public Integer getLocId() {
		return this.locId;
	}

	public void setLocId(Integer locId) {
		this.locId = locId;
	}

	public String getLocCode() {
		return this.locCode;
	}

	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}

	public String getFormTitle() {
		return this.formTitle;
	}

	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getContinous() {
		return this.continous;
	}

	public void setContinous(Boolean continous) {
		this.continous = continous;
	}

	public Boolean getIncludeInAverage() {
		return this.includeInAverage;
	}

	public void setIncludeInAverage(Boolean includeInAverage) {
		this.includeInAverage = includeInAverage;
	}

	public QuantityType getQuantityType() {
		return this.quantityType;
	}

	public void setQuantityType(QuantityType quantityType) {
		this.quantityType = quantityType;
	}

	public MasterAgreementsPeriod getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(MasterAgreementsPeriod exchangeRateOffset) {
		this.exchangeRateOffset = exchangeRateOffset;
	}

	public Boolean getRestriction() {
		return this.restriction;
	}

	public void setRestriction(Boolean restriction) {
		this.restriction = restriction;
	}

	public VatApplicability getVat() {
		return this.vat;
	}

	public void setVat(VatApplicability vat) {
		this.vat = vat;
	}

	public Boolean getDefaultPriceCtgy() {
		return this.defaultPriceCtgy;
	}

	public void setDefaultPriceCtgy(Boolean defaultPriceCtgy) {
		this.defaultPriceCtgy = defaultPriceCtgy;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getForex() {
		return this.forex;
	}

	public void setForex(String forex) {
		this.forex = forex;
	}

	public CalculateIndicator getCalculateIndicator() {
		return this.calculateIndicator;
	}

	public void setCalculateIndicator(CalculateIndicator calculateIndicator) {
		this.calculateIndicator = calculateIndicator;
	}

	public String getPriceCategoryIdsJson() {
		return this.priceCategoryIdsJson;
	}

	public void setPriceCategoryIdsJson(String priceCategoryIdsJson) {
		this.priceCategoryIdsJson = priceCategoryIdsJson;
	}

	public Integer getContract() {
		return this.contract;
	}

	public void setContract(Integer contract) {
		this.contract = contract;
	}

	public Date getContractValidFrom() {
		return this.contractValidFrom;
	}

	public void setContractValidFrom(Date contractValidFrom) {
		this.contractValidFrom = contractValidFrom;
	}

	public Date getContractValidTo() {
		return this.contractValidTo;
	}

	public void setContractValidTo(Date contractValidTo) {
		this.contractValidTo = contractValidTo;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public Integer getCurrencyId() {
		return this.currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public Integer getPriceCategory() {
		return this.priceCategory;
	}

	public void setPriceCategory(Integer priceCategory) {
		this.priceCategory = priceCategory;
	}

	public String getPriceCtgryName() {
		return this.priceCtgryName;
	}

	public void setPriceCtgryName(String priceCtgryName) {
		this.priceCtgryName = priceCtgryName;
	}

	public PriceInd getPriceCtgryPricePer() {
		return this.priceCtgryPricePer;
	}

	public void setPriceCtgryPricePer(PriceInd priceCtgryPricePer) {
		this.priceCtgryPricePer = priceCtgryPricePer;
	}

	public String getIataPriceCatCode() {
		return this.iataPriceCatCode;
	}

	public void setIataPriceCatCode(String iataPriceCatCode) {
		this.iataPriceCatCode = iataPriceCatCode;
	}

	public Use getIataPriceCatUse() {
		return this.iataPriceCatUse;
	}

	public void setIataPriceCatUse(Use iataPriceCatUse) {
		this.iataPriceCatUse = iataPriceCatUse;
	}

	public String getMainCategoryCode() {
		return this.mainCategoryCode;
	}

	public void setMainCategoryCode(String mainCategoryCode) {
		this.mainCategoryCode = mainCategoryCode;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceName() {
		return this.financialSourceName;
	}

	public void setFinancialSourceName(String financialSourceName) {
		this.financialSourceName = financialSourceName;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}

	public Boolean getAvgMethodIndicatorDefMeth() {
		return this.avgMethodIndicatorDefMeth;
	}

	public void setAvgMethodIndicatorDefMeth(Boolean avgMethodIndicatorDefMeth) {
		this.avgMethodIndicatorDefMeth = avgMethodIndicatorDefMeth;
	}

	public String getAvgMethodIndicatorName() {
		return this.avgMethodIndicatorName;
	}

	public void setAvgMethodIndicatorName(String avgMethodIndicatorName) {
		this.avgMethodIndicatorName = avgMethodIndicatorName;
	}

	public Integer getAvgMthdId() {
		return this.avgMthdId;
	}

	public void setAvgMthdId(Integer avgMthdId) {
		this.avgMthdId = avgMthdId;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getPricingBaseId() {
		return this.pricingBaseId;
	}

	public void setPricingBaseId(Integer pricingBaseId) {
		this.pricingBaseId = pricingBaseId;
	}

	public String getConvertTitle() {
		return this.convertTitle;
	}

	public void setConvertTitle(String convertTitle) {
		this.convertTitle = convertTitle;
	}

	public BigDecimal getConvertedPrice() {
		return this.convertedPrice;
	}

	public void setConvertedPrice(BigDecimal convertedPrice) {
		this.convertedPrice = convertedPrice;
	}

	public Boolean getUsed() {
		return this.used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}

	public String getSlash() {
		return this.slash;
	}

	public void setSlash(String slash) {
		this.slash = slash;
	}

	public String getPercentageOf() {
		return this.percentageOf;
	}

	public void setPercentageOf(String percentageOf) {
		this.percentageOf = percentageOf;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}
}
