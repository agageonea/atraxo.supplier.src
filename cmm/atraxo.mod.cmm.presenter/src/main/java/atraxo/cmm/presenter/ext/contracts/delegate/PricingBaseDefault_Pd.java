package atraxo.cmm.presenter.ext.contracts.delegate;

import java.util.List;

import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.presenter.ext.contracts.service.IConversionFactorValidator;
import atraxo.cmm.presenter.impl.contracts.model.PricingBase_Ds;
import atraxo.cmm.presenter.impl.contracts.model.PricingBase_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class PricingBaseDefault_Pd extends AbstractPresenterDelegate {

	/**
	 * @param ds - {@link PricingBase_Ds} @throws Exception @throws
	 */
	public void changeUnit(PricingBase_Ds ds) throws Exception {
		IConversionFactorValidator srv = (IConversionFactorValidator) this.getApplicationContext().getBean("conversionFactorValidator");
		srv.validatePricingBase(ds);
	}

	/**
	 * check if exists other ContractPriceCategory with default field checked.
	 *
	 * @param pbDs
	 * @param params
	 * @throws Exception
	 */
	public void checkDefault(PricingBase_Ds pbDs, PricingBase_DsParam params) throws Exception {
		if (pbDs.getDefaultPriceCtgy()) {
			IContractPriceCategoryService service = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
			List<ContractPriceCategory> cList = service.getDefaultContractPriceCategories(pbDs.getPriceCatId(), pbDs.getContractId());
			if (cList != null && !cList.isEmpty()) {
				params.setIsDefault(true);
				this.setDefault(pbDs);
				return;
			}
		}
		params.setIsDefault(false);
	}

	public void setDefault(PricingBase_Ds pbDs) throws Exception {
		IContractPriceCategoryService service = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
		List<ContractPriceCategory> cList = service.getDefaultContractPriceCategories(pbDs.getPriceCatId(), pbDs.getContractId());
		for (ContractPriceCategory c : cList) {
			c.setDefaultPriceCtgy(false);
		}
		service.update(cList);
	}

}
