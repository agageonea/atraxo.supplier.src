/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.cmm.presenter.ext.contracts.qb;

import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.presenter.impl.contracts.model.Contract_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class Contract_DsQb extends QueryBuilderWithJpql<Contract_Ds, Contract_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();
		this.addFilterCondition("e.dealType = :dealType");
		this.addCustomFilterItem("dealType", DealType._BUY_);
	}

}
