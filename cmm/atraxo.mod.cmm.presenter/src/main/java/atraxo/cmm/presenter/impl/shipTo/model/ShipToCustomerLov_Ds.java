/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.shipTo.model;

import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerType;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ShipTo.class, sort = {@SortField(field = ShipToCustomerLov_Ds.F_CODE)})
public class ShipToCustomerLov_Ds extends AbstractLov_Ds<ShipTo> {

	public static final String ALIAS = "cmm_ShipToCustomerLov_Ds";

	public static final String F_CUSTID = "custId";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_TYPE = "type";
	public static final String F_REFID = "refid";
	public static final String F_STATUS = "status";
	public static final String F_IATACODE = "iataCode";
	public static final String F_ISCUSTOMER = "isCustomer";
	public static final String F_ISTHIRDPARTY = "isThirdParty";
	public static final String F_CONTRACTID = "contractId";

	@DsField(join = "left", path = "customer.id")
	private Integer custId;

	@DsField(join = "left", path = "customer.code")
	private String code;

	@DsField(join = "left", path = "customer.name")
	private String name;

	@DsField(join = "left", path = "customer.type")
	private CustomerType type;

	@DsField(join = "left", path = "customer.refid")
	private String refid;

	@DsField(join = "left", path = "customer.status")
	private CustomerStatus status;

	@DsField(join = "left", path = "customer.iataCode")
	private String iataCode;

	@DsField(join = "left", path = "customer.isCustomer")
	private Boolean isCustomer;

	@DsField(join = "left", path = "customer.isThirdParty")
	private Boolean isThirdParty;

	@DsField(join = "left", path = "contract.id")
	private Integer contractId;

	/**
	 * Default constructor
	 */
	public ShipToCustomerLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ShipToCustomerLov_Ds(ShipTo e) {
		super(e);
	}

	public Integer getCustId() {
		return this.custId;
	}

	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CustomerType getType() {
		return this.type;
	}

	public void setType(CustomerType type) {
		this.type = type;
	}

	public String getRefid() {
		return this.refid;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}

	public CustomerStatus getStatus() {
		return this.status;
	}

	public void setStatus(CustomerStatus status) {
		this.status = status;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public Boolean getIsCustomer() {
		return this.isCustomer;
	}

	public void setIsCustomer(Boolean isCustomer) {
		this.isCustomer = isCustomer;
	}

	public Boolean getIsThirdParty() {
		return this.isThirdParty;
	}

	public void setIsThirdParty(Boolean isThirdParty) {
		this.isThirdParty = isThirdParty;
	}

	public Integer getContractId() {
		return this.contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}
}
