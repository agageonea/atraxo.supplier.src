/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.shipTo.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.cmm.presenter.impl.shipTo.model.ShipTo_Ds;
import atraxo.cmm.presenter.impl.shipTo.model.ShipTo_DsParam;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ShipTo_DsService extends AbstractEntityDsService<ShipTo_Ds, ShipTo_Ds, ShipTo_DsParam, ShipTo>
		implements IDsService<ShipTo_Ds, ShipTo_Ds, ShipTo_DsParam> {

	final static Logger LOG = LoggerFactory.getLogger(ShipTo_DsService.class);

	@Autowired
	private IContractService contractService;
	@Autowired
	protected IWorkflowBpmManager workflowBpmManager;

	@Override
	protected void postSummaries(IQueryBuilder<ShipTo_Ds, ShipTo_Ds, ShipTo_DsParam> builder, List<ShipTo_Ds> findResult, Map<String, Object> sum)
			throws Exception {
		super.postSummaries(builder, findResult, sum);
		BigDecimal allocatedVolume = BigDecimal.ZERO;
		BigDecimal adjustedVolume = BigDecimal.ZERO;
		BigDecimal actualVolume = BigDecimal.ZERO;
		String fromUnitCode = "";
		for (ShipTo_Ds ds : findResult) {
			allocatedVolume = allocatedVolume.add(ds.getAllocatedVolume() == null ? BigDecimal.ZERO : ds.getAllocatedVolume());
			adjustedVolume = adjustedVolume.add(ds.getAdjustedVolume() == null ? BigDecimal.ZERO : ds.getAdjustedVolume());
			actualVolume = actualVolume.add(ds.getActualVolume() == null ? BigDecimal.ZERO : ds.getActualVolume());
			fromUnitCode = ds.getSettlementUnitCode();
		}
		ShipTo_DsParam params = builder.getParams();
		if (!"".equals(fromUnitCode) && !"".equals(params.getUnitParameter())) {
			IUnitService unitConverter = (IUnitService) this.findEntityService(Unit.class);
			ISystemParameterService systemParameterService = (ISystemParameterService) this.getServiceLocator()
					.findEntityService(SystemParameter.class);
			Double density = Double.parseDouble(systemParameterService.getDensity());
			Unit toUnit = unitConverter.findByCode(params.getUnitParameter());
			Unit fromUnit = unitConverter.findByCode(fromUnitCode);
			allocatedVolume = unitConverter.convert(fromUnit, toUnit, allocatedVolume, density);
			adjustedVolume = unitConverter.convert(fromUnit, toUnit, adjustedVolume, density);
			actualVolume = unitConverter.convert(fromUnit, toUnit, actualVolume, density);
		}
		sum.put("allocatedVolume", allocatedVolume);
		sum.put("adjustedVolume", adjustedVolume);
		sum.put("actualVolume", actualVolume);
	}

	@Override
	protected void postFind(IQueryBuilder<ShipTo_Ds, ShipTo_Ds, ShipTo_DsParam> builder, List<ShipTo_Ds> result) throws Exception {
		super.postFind(builder, result);

		if (!result.isEmpty()) {
			Contract c = this.contractService.findById(result.get(0).getContractId());
			if (!c.getIsContract()) {
				this.calculatePercentage(result);
			}
		}
	}

	protected void calculatePercentage(List<ShipTo_Ds> result) throws Exception {
		ISystemParameterService systemParameterService = (ISystemParameterService) this.getServiceLocator().findEntityService(SystemParameter.class);

		for (ShipTo_Ds ds : result) {
			BigDecimal percent = BigDecimal.ZERO;
			if (ds.getAllocatedVolume() != null && ds.getTenderBidVolume().compareTo(BigDecimal.ZERO) > 0) {
				percent = ds.getAllocatedVolume().multiply(new BigDecimal(100)).divide(ds.getTenderBidVolume(),
						systemParameterService.getDecimalsForPercentage(), RoundingMode.HALF_UP);
			} else if (ds.getOfferedVolume() != null && ds.getTenderBidVolume().compareTo(BigDecimal.ZERO) > 0) {
				percent = ds.getOfferedVolume().multiply(new BigDecimal(100)).divide(ds.getTenderBidVolume(),
						systemParameterService.getDecimalsForPercentage(), RoundingMode.HALF_UP);
			}

			ds.setPercentCovered(percent);
		}
	}

	@Override
	protected void onInsert(List<ShipTo_Ds> list, List<ShipTo> entities, ShipTo_DsParam params) throws Exception {
		if (DealType._SELL_.equals(DealType.getByName(params.getContractDealType()))) {
			this.checkPeriod(list);
			this.checkShipTo(list);
		}
		super.onInsert(list, entities, params);
	}

	@Override
	protected void preUpdate(List<ShipTo_Ds> list, ShipTo_DsParam params) throws Exception {
		super.preUpdate(list, params);
		this.checkPeriod(list);
		this.checkShipTo(list);
	}

	private void checkShipTo(List<ShipTo_Ds> dss) throws BusinessException {
		for (ShipTo_Ds ds : dss) {
			if (BidApprovalStatus._AWAITING_APPROVAL_.equals(ds.getShipToApprovalStatus())) {
				throw new BusinessException(CmmErrorCode.SHIPTO_AWAITING_APPROVAL_CANOT_EDIT,
						CmmErrorCode.SHIPTO_AWAITING_APPROVAL_CANOT_EDIT.getErrMsg());
			}
		}
	}

	private void checkPeriod(List<ShipTo_Ds> list) throws BusinessException {
		for (ShipTo_Ds ds : list) {
			if (ds.getValidFrom().compareTo(ds.getValidTo()) > 1) {
				throw new BusinessException(CmmErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
						CmmErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO.getErrMsg());
			}
		}
	}
}
