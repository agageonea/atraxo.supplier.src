/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.pricingOverview.service;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.ext.exceptions.NoPriceComponentsException;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.presenter.impl.pricingOverview.model.ContractPriceOverview_Ds;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ContractPriceOverview_DsService
		extends AbstractEntityDsService<ContractPriceOverview_Ds, ContractPriceOverview_Ds, Object, ContractPriceCategory>
		implements IDsService<ContractPriceOverview_Ds, ContractPriceOverview_Ds, Object> {

	private static final Logger LOG = LoggerFactory.getLogger(ContractPriceOverview_DsService.class);

	@Override
	protected void postFind(IQueryBuilder<ContractPriceOverview_Ds, ContractPriceOverview_Ds, Object> builder, List<ContractPriceOverview_Ds> result)
			throws Exception {
		super.postFind(builder, result);
		IContractPriceCategoryService service = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
		Map<String, ContractPriceOverview_Ds> usedPriceCategories = new HashMap<>();
		for (ContractPriceOverview_Ds priceCategoryDs : result) {
			ContractPriceCategory priceCategory = priceCategoryDs._getEntity_();
			Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH);
			if (date.before(priceCategoryDs.getContractValidFrom())) {
				date = priceCategoryDs.getContractValidFrom();
			}
			if (date.after(priceCategoryDs.getContractValidTo())) {
				date = priceCategoryDs.getContractValidTo();
			}
			try {
				ContractPriceComponent priceComponent = service.getPriceComponent(priceCategory, date);
				if (priceComponent != null) {
					this.setContractPriceCtgry(priceComponent, priceCategoryDs);
				}
				this.markPriceCategories(usedPriceCategories, priceCategoryDs);
			} catch (NoPriceComponentsException e) {
				LOG.info("No price component found for contract :" + priceCategory.getContract().getCode() + " on " + date, e);
			}
		}

	}

	private void markPriceCategories(Map<String, ContractPriceOverview_Ds> usedPriceCategories, ContractPriceOverview_Ds priceCategoryDs) {
		ContractPriceCategory priceCategory = priceCategoryDs._getEntity_();
		PriceInd priceCategoryPer = priceCategory.getPriceCategory().getPricePer();
		if (!PriceInd._EVENT_.equals(priceCategoryPer)) {
			if (!usedPriceCategories.keySet().contains(priceCategory.getPriceCategory().getName())) {
				usedPriceCategories.put(priceCategory.getPriceCategory().getName(), priceCategoryDs);
				priceCategoryDs.setUsed(true);
			} else if (priceCategory.getDefaultPriceCtgy()) {
				ContractPriceOverview_Ds ds = usedPriceCategories.get(priceCategory.getPriceCategory().getName());
				ds.setUsed(false);
				usedPriceCategories.put(priceCategory.getPriceCategory().getName(), priceCategoryDs);
				priceCategoryDs.setUsed(true);
			}
		}
	}

	/**
	 * @param cpComponent
	 * @param cpc
	 */
	private void setContractPriceCtgry(ContractPriceComponent cpComponent, ContractPriceOverview_Ds cpc) {
		if (cpComponent.getPrice() != null) {
			cpc.setPrice(cpComponent.getPrice());
		}
		if (cpComponent.getValidFrom() != null) {
			cpc.setValidFrom(cpComponent.getValidFrom());
		}
		if (cpComponent.getValidTo() != null) {
			cpc.setValidTo(cpComponent.getValidTo());
		}
		if (cpComponent.getCurrency() != null && cpComponent.getCurrency().getCode() != null) {
			cpc.setCurrencyCode(cpComponent.getCurrency().getCode());
		}
		if (cpComponent.getCurrency() != null && cpComponent.getCurrency().getId() != null) {
			cpc.setCurrencyId(cpComponent.getCurrency().getId());
		}
		if (cpComponent.getUnit() != null && cpComponent.getUnit().getCode() != null) {
			cpc.setUnitCode(cpComponent.getUnit().getCode());
		}
		if (cpComponent.getUnit() != null && cpComponent.getUnit().getId() != null) {
			cpc.setUnitId(cpComponent.getUnit().getId());
		}
	}

}
