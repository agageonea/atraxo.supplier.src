/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.contracts.model;

import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BidStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractScope;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.EventType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.InvoicePayableBy;
import atraxo.cmm.domain.impl.cmm_type.IsSpot;
import atraxo.cmm.domain.impl.cmm_type.PaymentType;
import atraxo.cmm.domain.impl.cmm_type.Period;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.QuantityType;
import atraxo.cmm.domain.impl.cmm_type.ReviewPeriod;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.cmm_type.TransmissionStatus;
import atraxo.cmm.domain.impl.cmm_type.VatApplicability;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceType;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Contract.class, sort = {@SortField(field = Contract_Ds.F_LOCCODE)})
@RefLookups({
		@RefLookup(refId = Contract_Ds.F_LOCID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Contract_Ds.F_LOCCODE)}),
		@RefLookup(refId = Contract_Ds.F_HOLDERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Contract_Ds.F_HOLDERCODE)}),
		@RefLookup(refId = Contract_Ds.F_SUPPLIERID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Contract_Ds.F_SUPPLIERCODE)}),
		@RefLookup(refId = Contract_Ds.F_IPLID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Contract_Ds.F_IPLCODE)}),
		@RefLookup(refId = Contract_Ds.F_RESBUYERID, namedQuery = UserSupp.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Contract_Ds.F_RESBUYERNAME)}),
		@RefLookup(refId = Contract_Ds.F_CONTACTID, namedQuery = Contacts.NQ_FIND_BY_ID, params = {@Param(name = "id", field = Contract_Ds.F_CONTACTNAME)}),
		@RefLookup(refId = Contract_Ds.F_CONTVOLUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Contract_Ds.F_CONTVOLUNITCODE)}),
		@RefLookup(refId = Contract_Ds.F_RESALEREFID, namedQuery = Contract.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Contract_Ds.F_RESALEREFCODE)}),
		@RefLookup(refId = Contract_Ds.F_SETTCURRID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Contract_Ds.F_SETTCURRCODE)}),
		@RefLookup(refId = Contract_Ds.F_SETTUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Contract_Ds.F_SETTUNITCODE)}),
		@RefLookup(refId = Contract_Ds.F_AVGMETHODID, namedQuery = AverageMethod.NQ_FIND_BY_NAME, params = {@Param(name = "code", field = Contract_Ds.F_AVGMETHODNAME)}),
		@RefLookup(refId = Contract_Ds.F_FINANCIALSOURCEID)})
public class Contract_Ds extends AbstractSubsidiaryDs_Ds<Contract> {

	public static final String ALIAS = "cmm_Contract_Ds";

	public static final String F_DEST = "dest";
	public static final String F_SETTLEMENT = "settlement";
	public static final String F_INVOICING = "invoicing";
	public static final String F_PRICE = "price";
	public static final String F_SEPARATOR = "separator";
	public static final String F_EVENTS = "events";
	public static final String F_DAYS = "days";
	public static final String F_DAYSFREQUENCY = "daysFrequency";
	public static final String F_LOCID = "locId";
	public static final String F_LOCCODE = "locCode";
	public static final String F_HOLDERID = "holderId";
	public static final String F_HOLDERCODE = "holderCode";
	public static final String F_SUPPLIERID = "supplierId";
	public static final String F_CPREFID = "cpRefId";
	public static final String F_CPENTITYALIAS = "cpEntityAlias";
	public static final String F_SUPPLIERCODE = "supplierCode";
	public static final String F_ISSUPPLIER = "isSupplier";
	public static final String F_SUPPLIERNAME = "supplierName";
	public static final String F_ISTHIRDPARTY = "isThirdParty";
	public static final String F_IPLID = "iplId";
	public static final String F_IPLCODE = "iplCode";
	public static final String F_RESBUYERID = "resBuyerId";
	public static final String F_RESBUYERNAME = "resBuyerName";
	public static final String F_CONTACTID = "contactId";
	public static final String F_CONTACTNAME = "contactName";
	public static final String F_CONTVOLUNITID = "contVolUnitId";
	public static final String F_CONTVOLUNITCODE = "contVolUnitCode";
	public static final String F_RESALEREFID = "resaleRefId";
	public static final String F_RESALEREFCODE = "resaleRefCode";
	public static final String F_SETTCURRID = "settCurrId";
	public static final String F_SETTCURRCODE = "settCurrCode";
	public static final String F_SETTUNITID = "settUnitId";
	public static final String F_SETTUNITCODE = "settUnitCode";
	public static final String F_FINANCIALSOURCEID = "financialSourceId";
	public static final String F_FINANCIALSOURCECODE = "financialSourceCode";
	public static final String F_AVGMETHODID = "avgMethodId";
	public static final String F_AVGMETHODNAME = "avgMethodName";
	public static final String F_UNIT = "unit";
	public static final String F_CODE = "code";
	public static final String F_DEALTYPE = "dealType";
	public static final String F_TYPE = "type";
	public static final String F_SUBTYPE = "subType";
	public static final String F_SCOPE = "scope";
	public static final String F_ISSPOT = "isSpot";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_STATUS = "status";
	public static final String F_LIMITEDTO = "limitedTo";
	public static final String F_PRODUCT = "product";
	public static final String F_IATASERVICELEVEL = "iataServiceLevel";
	public static final String F_TAX = "tax";
	public static final String F_QUANTITYTYPE = "quantityType";
	public static final String F_PERIOD = "period";
	public static final String F_VOLUMETOLERANCE = "volumeTolerance";
	public static final String F_VOLUMESHARE = "volumeShare";
	public static final String F_COUNTERPARTYREFERENCE = "counterpartyReference";
	public static final String F_SETTLEMENTDECIMALS = "settlementDecimals";
	public static final String F_EXCHANGERATEOFFSET = "exchangeRateOffset";
	public static final String F_PAYMENTTERMS = "paymentTerms";
	public static final String F_PAYMENTREFDAY = "paymentRefDay";
	public static final String F_INVOICEFREQ = "invoiceFreq";
	public static final String F_INVOICEFREQNUMBER = "invoiceFreqNumber";
	public static final String F_CREDITTERMS = "creditTerms";
	public static final String F_INVOICETYPE = "invoiceType";
	public static final String F_INVOICEPAYABLEBY = "invoicePayableBy";
	public static final String F_VAT = "vat";
	public static final String F_REVIEWPERIOD = "reviewPeriod";
	public static final String F_REVIEWFIRSTPARAM = "reviewFirstParam";
	public static final String F_REVIEWSECONDPARAM = "reviewSecondParam";
	public static final String F_REVIEWNOTIFICATION = "reviewNotification";
	public static final String F_PAYMENTCOMMENT = "paymentComment";
	public static final String F_EVENTTYPE = "eventType";
	public static final String F_AWARDEDVOLUME = "awardedVolume";
	public static final String F_OFFEREDVOLUME = "offeredVolume";
	public static final String F_FOREX = "forex";
	public static final String F_HARDCOPY = "hardCopy";
	public static final String F_BIDSTATUS = "bidStatus";
	public static final String F_BIDTRANSMISSIONSTATUS = "bidTransmissionStatus";
	public static final String F_APPROVALSTATUS = "approvalStatus";
	public static final String F_BIDPAYEMENTFREQ = "bidPayementFreq";
	public static final String F_BIDPAYMENTTYPE = "bidPaymentType";
	public static final String F_ISCONTRACT = "isContract";
	public static final String F_READONLY = "readOnly";
	public static final String F_PERIODAPPROVALSTATUS = "periodApprovalStatus";
	public static final String F_FORMTITLE = "formTitle";
	public static final String F_PERCENT = "percent";
	public static final String F_LABELFIELD = "labelField";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_SUPPLIERFIELD = "supplierField";
	public static final String F_SHIPTOLIST = "shipToList";
	public static final String F_RESALECONTRACT = "resaleContract";
	public static final String F_CANBECOMPLETED = "canBeCompleted";

	@DsField(fetch = false)
	private String dest;

	@DsField(fetch = false)
	private String settlement;

	@DsField(fetch = false)
	private String invoicing;

	@DsField(fetch = false)
	private String price;

	@DsField(fetch = false)
	private String separator;

	@DsField(fetch = false)
	private String events;

	@DsField(fetch = false)
	private String days;

	@DsField(fetch = false)
	private String daysFrequency;

	@DsField(join = "left", path = "location.id")
	private Integer locId;

	@DsField(join = "left", path = "location.code")
	private String locCode;

	@DsField(join = "left", path = "holder.id")
	private Integer holderId;

	@DsField(join = "left", path = "holder.code")
	private String holderCode;

	@DsField(join = "left", path = "supplier.id")
	private Integer supplierId;

	@DsField(join = "left", path = "supplier.refid")
	private String cpRefId;

	@DsField(join = "left", fetch = false, path = "supplier.entityAlias")
	private String cpEntityAlias;

	@DsField(join = "left", path = "supplier.code")
	private String supplierCode;

	@DsField(join = "left", path = "supplier.isSupplier")
	private Boolean isSupplier;

	@DsField(join = "left", path = "supplier.name")
	private String supplierName;

	@DsField(join = "left", path = "supplier.isThirdParty")
	private Boolean isThirdParty;

	@DsField(join = "left", path = "intoPlaneAgent.id")
	private Integer iplId;

	@DsField(join = "left", path = "intoPlaneAgent.code")
	private String iplCode;

	@DsField(join = "left", path = "responsibleBuyer.id")
	private String resBuyerId;

	@DsField(join = "left", path = "responsibleBuyer.name")
	private String resBuyerName;

	@DsField(join = "left", path = "contact.id")
	private Integer contactId;

	@DsField(join = "left", fetch = false, path = "contact.fullNameField")
	private String contactName;

	@DsField(join = "left", path = "contractVolumeUnit.id")
	private Integer contVolUnitId;

	@DsField(join = "left", path = "contractVolumeUnit.code")
	private String contVolUnitCode;

	@DsField(join = "left", path = "resaleRef.id")
	private Integer resaleRefId;

	@DsField(join = "left", path = "resaleRef.code")
	private String resaleRefCode;

	@DsField(join = "left", path = "settlementCurr.id")
	private Integer settCurrId;

	@DsField(join = "left", path = "settlementCurr.code")
	private String settCurrCode;

	@DsField(join = "left", path = "settlementUnit.id")
	private Integer settUnitId;

	@DsField(join = "left", path = "settlementUnit.code")
	private String settUnitCode;

	@DsField(join = "left", path = "financialSource.id")
	private Integer financialSourceId;

	@DsField(join = "left", path = "financialSource.code")
	private String financialSourceCode;

	@DsField(join = "left", path = "averageMethod.id")
	private Integer avgMethodId;

	@DsField(join = "left", path = "averageMethod.name")
	private String avgMethodName;

	@DsField(fetch = false)
	private String unit;

	@DsField
	private String code;

	@DsField
	private DealType dealType;

	@DsField
	private ContractType type;

	@DsField
	private ContractSubType subType;

	@DsField
	private ContractScope scope;

	@DsField
	private IsSpot isSpot;

	@DsField(jpqlFilter = " e.validTo >= :validFrom ")
	private Date validFrom;

	@DsField(jpqlFilter = " e.validFrom <= :validTo ")
	private Date validTo;

	@DsField
	private ContractStatus status;

	@DsField
	private FlightTypeIndicator limitedTo;

	@DsField
	private Product product;

	@DsField
	private Integer iataServiceLevel;

	@DsField
	private TaxType tax;

	@DsField
	private QuantityType quantityType;

	@DsField
	private Period period;

	@DsField
	private Integer volumeTolerance;

	@DsField
	private Integer volumeShare;

	@DsField
	private String counterpartyReference;

	@DsField
	private Integer settlementDecimals;

	@DsField
	private MasterAgreementsPeriod exchangeRateOffset;

	@DsField
	private Integer paymentTerms;

	@DsField
	private PaymentDay paymentRefDay;

	@DsField
	private InvoiceFreq invoiceFreq;

	@DsField
	private Integer invoiceFreqNumber;

	@DsField
	private CreditTerm creditTerms;

	@DsField
	private InvoiceType invoiceType;

	@DsField
	private InvoicePayableBy invoicePayableBy;

	@DsField
	private VatApplicability vat;

	@DsField
	private ReviewPeriod reviewPeriod;

	@DsField
	private String reviewFirstParam;

	@DsField
	private String reviewSecondParam;

	@DsField
	private Integer reviewNotification;

	@DsField
	private String paymentComment;

	@DsField
	private EventType eventType;

	@DsField
	private BigDecimal awardedVolume;

	@DsField
	private BigDecimal offeredVolume;

	@DsField(fetch = false)
	private String forex;

	@DsField
	private Boolean hardCopy;

	@DsField
	private BidStatus bidStatus;

	@DsField
	private TransmissionStatus bidTransmissionStatus;

	@DsField(path = "bidApprovalStatus")
	private BidApprovalStatus approvalStatus;

	@DsField
	private InvoiceFreq bidPayementFreq;

	@DsField
	private PaymentType bidPaymentType;

	@DsField
	private Boolean isContract;

	@DsField
	private Boolean readOnly;

	@DsField
	private BidApprovalStatus periodApprovalStatus;

	@DsField(fetch = false)
	private String formTitle;

	@DsField(fetch = false)
	private String percent;

	@DsField(fetch = false)
	private String labelField;

	@DsField(fetch = false)
	private Integer customerId;

	@DsField(fetch = false)
	private String customerCode;

	@DsField(fetch = false)
	private String supplierField;

	@DsField(fetch = false)
	private String shipToList;

	@DsField(fetch = false)
	private String resaleContract;

	@DsField(fetch = false)
	private Boolean canBeCompleted;

	/**
	 * Default constructor
	 */
	public Contract_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Contract_Ds(Contract e) {
		super(e);
	}

	public String getDest() {
		return this.dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public String getSettlement() {
		return this.settlement;
	}

	public void setSettlement(String settlement) {
		this.settlement = settlement;
	}

	public String getInvoicing() {
		return this.invoicing;
	}

	public void setInvoicing(String invoicing) {
		this.invoicing = invoicing;
	}

	public String getPrice() {
		return this.price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getSeparator() {
		return this.separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public String getEvents() {
		return this.events;
	}

	public void setEvents(String events) {
		this.events = events;
	}

	public String getDays() {
		return this.days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public String getDaysFrequency() {
		return this.daysFrequency;
	}

	public void setDaysFrequency(String daysFrequency) {
		this.daysFrequency = daysFrequency;
	}

	public Integer getLocId() {
		return this.locId;
	}

	public void setLocId(Integer locId) {
		this.locId = locId;
	}

	public String getLocCode() {
		return this.locCode;
	}

	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}

	public Integer getHolderId() {
		return this.holderId;
	}

	public void setHolderId(Integer holderId) {
		this.holderId = holderId;
	}

	public String getHolderCode() {
		return this.holderCode;
	}

	public void setHolderCode(String holderCode) {
		this.holderCode = holderCode;
	}

	public Integer getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	public String getCpRefId() {
		return this.cpRefId;
	}

	public void setCpRefId(String cpRefId) {
		this.cpRefId = cpRefId;
	}

	public String getCpEntityAlias() {
		return this.cpEntityAlias;
	}

	public void setCpEntityAlias(String cpEntityAlias) {
		this.cpEntityAlias = cpEntityAlias;
	}

	public String getSupplierCode() {
		return this.supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public Boolean getIsSupplier() {
		return this.isSupplier;
	}

	public void setIsSupplier(Boolean isSupplier) {
		this.isSupplier = isSupplier;
	}

	public String getSupplierName() {
		return this.supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public Boolean getIsThirdParty() {
		return this.isThirdParty;
	}

	public void setIsThirdParty(Boolean isThirdParty) {
		this.isThirdParty = isThirdParty;
	}

	public Integer getIplId() {
		return this.iplId;
	}

	public void setIplId(Integer iplId) {
		this.iplId = iplId;
	}

	public String getIplCode() {
		return this.iplCode;
	}

	public void setIplCode(String iplCode) {
		this.iplCode = iplCode;
	}

	public String getResBuyerId() {
		return this.resBuyerId;
	}

	public void setResBuyerId(String resBuyerId) {
		this.resBuyerId = resBuyerId;
	}

	public String getResBuyerName() {
		return this.resBuyerName;
	}

	public void setResBuyerName(String resBuyerName) {
		this.resBuyerName = resBuyerName;
	}

	public Integer getContactId() {
		return this.contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}

	public String getContactName() {
		return this.contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public Integer getContVolUnitId() {
		return this.contVolUnitId;
	}

	public void setContVolUnitId(Integer contVolUnitId) {
		this.contVolUnitId = contVolUnitId;
	}

	public String getContVolUnitCode() {
		return this.contVolUnitCode;
	}

	public void setContVolUnitCode(String contVolUnitCode) {
		this.contVolUnitCode = contVolUnitCode;
	}

	public Integer getResaleRefId() {
		return this.resaleRefId;
	}

	public void setResaleRefId(Integer resaleRefId) {
		this.resaleRefId = resaleRefId;
	}

	public String getResaleRefCode() {
		return this.resaleRefCode;
	}

	public void setResaleRefCode(String resaleRefCode) {
		this.resaleRefCode = resaleRefCode;
	}

	public Integer getSettCurrId() {
		return this.settCurrId;
	}

	public void setSettCurrId(Integer settCurrId) {
		this.settCurrId = settCurrId;
	}

	public String getSettCurrCode() {
		return this.settCurrCode;
	}

	public void setSettCurrCode(String settCurrCode) {
		this.settCurrCode = settCurrCode;
	}

	public Integer getSettUnitId() {
		return this.settUnitId;
	}

	public void setSettUnitId(Integer settUnitId) {
		this.settUnitId = settUnitId;
	}

	public String getSettUnitCode() {
		return this.settUnitCode;
	}

	public void setSettUnitCode(String settUnitCode) {
		this.settUnitCode = settUnitCode;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}

	public Integer getAvgMethodId() {
		return this.avgMethodId;
	}

	public void setAvgMethodId(Integer avgMethodId) {
		this.avgMethodId = avgMethodId;
	}

	public String getAvgMethodName() {
		return this.avgMethodName;
	}

	public void setAvgMethodName(String avgMethodName) {
		this.avgMethodName = avgMethodName;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public DealType getDealType() {
		return this.dealType;
	}

	public void setDealType(DealType dealType) {
		this.dealType = dealType;
	}

	public ContractType getType() {
		return this.type;
	}

	public void setType(ContractType type) {
		this.type = type;
	}

	public ContractSubType getSubType() {
		return this.subType;
	}

	public void setSubType(ContractSubType subType) {
		this.subType = subType;
	}

	public ContractScope getScope() {
		return this.scope;
	}

	public void setScope(ContractScope scope) {
		this.scope = scope;
	}

	public IsSpot getIsSpot() {
		return this.isSpot;
	}

	public void setIsSpot(IsSpot isSpot) {
		this.isSpot = isSpot;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public ContractStatus getStatus() {
		return this.status;
	}

	public void setStatus(ContractStatus status) {
		this.status = status;
	}

	public FlightTypeIndicator getLimitedTo() {
		return this.limitedTo;
	}

	public void setLimitedTo(FlightTypeIndicator limitedTo) {
		this.limitedTo = limitedTo;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getIataServiceLevel() {
		return this.iataServiceLevel;
	}

	public void setIataServiceLevel(Integer iataServiceLevel) {
		this.iataServiceLevel = iataServiceLevel;
	}

	public TaxType getTax() {
		return this.tax;
	}

	public void setTax(TaxType tax) {
		this.tax = tax;
	}

	public QuantityType getQuantityType() {
		return this.quantityType;
	}

	public void setQuantityType(QuantityType quantityType) {
		this.quantityType = quantityType;
	}

	public Period getPeriod() {
		return this.period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public Integer getVolumeTolerance() {
		return this.volumeTolerance;
	}

	public void setVolumeTolerance(Integer volumeTolerance) {
		this.volumeTolerance = volumeTolerance;
	}

	public Integer getVolumeShare() {
		return this.volumeShare;
	}

	public void setVolumeShare(Integer volumeShare) {
		this.volumeShare = volumeShare;
	}

	public String getCounterpartyReference() {
		return this.counterpartyReference;
	}

	public void setCounterpartyReference(String counterpartyReference) {
		this.counterpartyReference = counterpartyReference;
	}

	public Integer getSettlementDecimals() {
		return this.settlementDecimals;
	}

	public void setSettlementDecimals(Integer settlementDecimals) {
		this.settlementDecimals = settlementDecimals;
	}

	public MasterAgreementsPeriod getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(MasterAgreementsPeriod exchangeRateOffset) {
		this.exchangeRateOffset = exchangeRateOffset;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public PaymentDay getPaymentRefDay() {
		return this.paymentRefDay;
	}

	public void setPaymentRefDay(PaymentDay paymentRefDay) {
		this.paymentRefDay = paymentRefDay;
	}

	public InvoiceFreq getInvoiceFreq() {
		return this.invoiceFreq;
	}

	public void setInvoiceFreq(InvoiceFreq invoiceFreq) {
		this.invoiceFreq = invoiceFreq;
	}

	public Integer getInvoiceFreqNumber() {
		return this.invoiceFreqNumber;
	}

	public void setInvoiceFreqNumber(Integer invoiceFreqNumber) {
		this.invoiceFreqNumber = invoiceFreqNumber;
	}

	public CreditTerm getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(CreditTerm creditTerms) {
		this.creditTerms = creditTerms;
	}

	public InvoiceType getInvoiceType() {
		return this.invoiceType;
	}

	public void setInvoiceType(InvoiceType invoiceType) {
		this.invoiceType = invoiceType;
	}

	public InvoicePayableBy getInvoicePayableBy() {
		return this.invoicePayableBy;
	}

	public void setInvoicePayableBy(InvoicePayableBy invoicePayableBy) {
		this.invoicePayableBy = invoicePayableBy;
	}

	public VatApplicability getVat() {
		return this.vat;
	}

	public void setVat(VatApplicability vat) {
		this.vat = vat;
	}

	public ReviewPeriod getReviewPeriod() {
		return this.reviewPeriod;
	}

	public void setReviewPeriod(ReviewPeriod reviewPeriod) {
		this.reviewPeriod = reviewPeriod;
	}

	public String getReviewFirstParam() {
		return this.reviewFirstParam;
	}

	public void setReviewFirstParam(String reviewFirstParam) {
		this.reviewFirstParam = reviewFirstParam;
	}

	public String getReviewSecondParam() {
		return this.reviewSecondParam;
	}

	public void setReviewSecondParam(String reviewSecondParam) {
		this.reviewSecondParam = reviewSecondParam;
	}

	public Integer getReviewNotification() {
		return this.reviewNotification;
	}

	public void setReviewNotification(Integer reviewNotification) {
		this.reviewNotification = reviewNotification;
	}

	public String getPaymentComment() {
		return this.paymentComment;
	}

	public void setPaymentComment(String paymentComment) {
		this.paymentComment = paymentComment;
	}

	public EventType getEventType() {
		return this.eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public BigDecimal getAwardedVolume() {
		return this.awardedVolume;
	}

	public void setAwardedVolume(BigDecimal awardedVolume) {
		this.awardedVolume = awardedVolume;
	}

	public BigDecimal getOfferedVolume() {
		return this.offeredVolume;
	}

	public void setOfferedVolume(BigDecimal offeredVolume) {
		this.offeredVolume = offeredVolume;
	}

	public String getForex() {
		return this.forex;
	}

	public void setForex(String forex) {
		this.forex = forex;
	}

	public Boolean getHardCopy() {
		return this.hardCopy;
	}

	public void setHardCopy(Boolean hardCopy) {
		this.hardCopy = hardCopy;
	}

	public BidStatus getBidStatus() {
		return this.bidStatus;
	}

	public void setBidStatus(BidStatus bidStatus) {
		this.bidStatus = bidStatus;
	}

	public TransmissionStatus getBidTransmissionStatus() {
		return this.bidTransmissionStatus;
	}

	public void setBidTransmissionStatus(
			TransmissionStatus bidTransmissionStatus) {
		this.bidTransmissionStatus = bidTransmissionStatus;
	}

	public BidApprovalStatus getApprovalStatus() {
		return this.approvalStatus;
	}

	public void setApprovalStatus(BidApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public InvoiceFreq getBidPayementFreq() {
		return this.bidPayementFreq;
	}

	public void setBidPayementFreq(InvoiceFreq bidPayementFreq) {
		this.bidPayementFreq = bidPayementFreq;
	}

	public PaymentType getBidPaymentType() {
		return this.bidPaymentType;
	}

	public void setBidPaymentType(PaymentType bidPaymentType) {
		this.bidPaymentType = bidPaymentType;
	}

	public Boolean getIsContract() {
		return this.isContract;
	}

	public void setIsContract(Boolean isContract) {
		this.isContract = isContract;
	}

	public Boolean getReadOnly() {
		return this.readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	public BidApprovalStatus getPeriodApprovalStatus() {
		return this.periodApprovalStatus;
	}

	public void setPeriodApprovalStatus(BidApprovalStatus periodApprovalStatus) {
		this.periodApprovalStatus = periodApprovalStatus;
	}

	public String getFormTitle() {
		return this.formTitle;
	}

	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}

	public String getPercent() {
		return this.percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getSupplierField() {
		return this.supplierField;
	}

	public void setSupplierField(String supplierField) {
		this.supplierField = supplierField;
	}

	public String getShipToList() {
		return this.shipToList;
	}

	public void setShipToList(String shipToList) {
		this.shipToList = shipToList;
	}

	public String getResaleContract() {
		return this.resaleContract;
	}

	public void setResaleContract(String resaleContract) {
		this.resaleContract = resaleContract;
	}

	public Boolean getCanBeCompleted() {
		return this.canBeCompleted;
	}

	public void setCanBeCompleted(Boolean canBeCompleted) {
		this.canBeCompleted = canBeCompleted;
	}
}
