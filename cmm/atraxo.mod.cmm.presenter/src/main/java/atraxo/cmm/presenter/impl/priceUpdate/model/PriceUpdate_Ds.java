/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.priceUpdate.model;

import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdate;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = PriceUpdate.class, sort = {@SortField(field = PriceUpdate_Ds.F_PRICECATEGORYNAME)})
@RefLookups({
		@RefLookup(refId = PriceUpdate_Ds.F_PRICECATEGORYID, namedQuery = PriceCategory.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = PriceUpdate_Ds.F_PRICECATEGORYNAME)}),
		@RefLookup(refId = PriceUpdate_Ds.F_CURRENCYID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceUpdate_Ds.F_CURRENCYCODE)}),
		@RefLookup(refId = PriceUpdate_Ds.F_UNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceUpdate_Ds.F_UNITCODE)}),
		@RefLookup(refId = PriceUpdate_Ds.F_PUBLISHEDBYID, namedQuery = UserSupp.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceUpdate_Ds.F_PUBLISHEDBYCODE)}),
		@RefLookup(refId = PriceUpdate_Ds.F_SUBMITTEDBYID, namedQuery = UserSupp.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceUpdate_Ds.F_SUBMITTEDBYCODE)}),
		@RefLookup(refId = PriceUpdate_Ds.F_APPROVEDBYID, namedQuery = UserSupp.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceUpdate_Ds.F_APPROVEDBYCODE)}),
		@RefLookup(refId = PriceUpdate_Ds.F_LOCATIONID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceUpdate_Ds.F_LOCATIONCODE)}),
		@RefLookup(refId = PriceUpdate_Ds.F_CONTRACTHOLDERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceUpdate_Ds.F_CONTRACTHOLDERCODE)}),
		@RefLookup(refId = PriceUpdate_Ds.F_COUNTERPARTYID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceUpdate_Ds.F_COUNTERPARTYCODE)}),
		@RefLookup(refId = PriceUpdate_Ds.F_BILLTOID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceUpdate_Ds.F_BILLTOCODE)}),
		@RefLookup(refId = PriceUpdate_Ds.F_SHIPTOID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceUpdate_Ds.F_SHIPTOCODE)})})
public class PriceUpdate_Ds extends AbstractSubsidiaryDs_Ds<PriceUpdate> {

	public static final String ALIAS = "cmm_PriceUpdate_Ds";

	public static final String F_PRICECATEGORYID = "priceCategoryId";
	public static final String F_PRICECATEGORYNAME = "priceCategoryName";
	public static final String F_CURRENCYID = "currencyId";
	public static final String F_CURRENCYCODE = "currencyCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_PUBLISHEDBYID = "publishedById";
	public static final String F_PUBLISHEDBYCODE = "publishedByCode";
	public static final String F_SUBMITTEDBYID = "submittedById";
	public static final String F_SUBMITTEDBYCODE = "submittedByCode";
	public static final String F_APPROVEDBYID = "approvedById";
	public static final String F_APPROVEDBYCODE = "approvedByCode";
	public static final String F_LOCATIONID = "locationId";
	public static final String F_LOCATIONCODE = "locationCode";
	public static final String F_CONTRACTHOLDERID = "contractHolderId";
	public static final String F_CONTRACTHOLDERCODE = "contractHolderCode";
	public static final String F_COUNTERPARTYID = "counterpartyId";
	public static final String F_COUNTERPARTYCODE = "counterpartyCode";
	public static final String F_BILLTOID = "billToId";
	public static final String F_BILLTOCODE = "billToCode";
	public static final String F_SHIPTOID = "shipToId";
	public static final String F_SHIPTOCODE = "shipToCode";
	public static final String F_REASON = "reason";
	public static final String F_NEWPRICE = "newPrice";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_APPROVALSTATUS = "approvalStatus";
	public static final String F_PUBLISHED = "published";
	public static final String F_PUBLISHEDON = "publishedOn";
	public static final String F_SUBMITTEDON = "submittedOn";
	public static final String F_APPROVEDON = "approvedOn";
	public static final String F_DEALTYPE = "dealType";
	public static final String F_CONTRACTSTATUS = "contractStatus";
	public static final String F_CONTRACTTYPE = "contractType";
	public static final String F_DELIVERYPOINT = "deliveryPoint";
	public static final String F_LABELFIELD = "labelField";
	public static final String F_VISIBLEFLAG = "visibleFlag";

	@DsField(join = "left", path = "priceCategory.id")
	private Integer priceCategoryId;

	@DsField(join = "left", path = "priceCategory.name")
	private String priceCategoryName;

	@DsField(join = "left", path = "currency.id")
	private Integer currencyId;

	@DsField(join = "left", path = "currency.code")
	private String currencyCode;

	@DsField(join = "left", path = "unit.id")
	private Integer unitId;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	@DsField(join = "left", path = "publishedBy.id")
	private String publishedById;

	@DsField(join = "left", path = "publishedBy.code")
	private String publishedByCode;

	@DsField(join = "left", path = "submittedBy.id")
	private String submittedById;

	@DsField(join = "left", path = "submittedBy.code")
	private String submittedByCode;

	@DsField(join = "left", path = "approvedBy.id")
	private String approvedById;

	@DsField(join = "left", path = "approvedBy.code")
	private String approvedByCode;

	@DsField(join = "left", path = "location.id")
	private Integer locationId;

	@DsField(join = "left", path = "location.code")
	private String locationCode;

	@DsField(join = "left", path = "contractHolder.id")
	private Integer contractHolderId;

	@DsField(join = "left", path = "contractHolder.code")
	private String contractHolderCode;

	@DsField(join = "left", path = "counterparty.id")
	private Integer counterpartyId;

	@DsField(join = "left", path = "counterparty.code")
	private String counterpartyCode;

	@DsField(join = "left", path = "billTo.id")
	private Integer billToId;

	@DsField(join = "left", path = "billTo.code")
	private String billToCode;

	@DsField(join = "left", path = "shipTo.id")
	private Integer shipToId;

	@DsField(join = "left", path = "shipTo.code")
	private String shipToCode;

	@DsField
	private String reason;

	@DsField
	private BigDecimal newPrice;

	@DsField
	private Date validFrom;

	@DsField
	private BidApprovalStatus approvalStatus;

	@DsField
	private Boolean published;

	@DsField
	private Date publishedOn;

	@DsField
	private Date submittedOn;

	@DsField
	private Date approvedOn;

	@DsField
	private String dealType;

	@DsField
	private String contractStatus;

	@DsField
	private String contractType;

	@DsField
	private String deliveryPoint;

	@DsField(fetch = false)
	private String labelField;

	@DsField(fetch = false)
	private Boolean visibleFlag;

	/**
	 * Default constructor
	 */
	public PriceUpdate_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public PriceUpdate_Ds(PriceUpdate e) {
		super(e);
	}

	public Integer getPriceCategoryId() {
		return this.priceCategoryId;
	}

	public void setPriceCategoryId(Integer priceCategoryId) {
		this.priceCategoryId = priceCategoryId;
	}

	public String getPriceCategoryName() {
		return this.priceCategoryName;
	}

	public void setPriceCategoryName(String priceCategoryName) {
		this.priceCategoryName = priceCategoryName;
	}

	public Integer getCurrencyId() {
		return this.currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getPublishedById() {
		return this.publishedById;
	}

	public void setPublishedById(String publishedById) {
		this.publishedById = publishedById;
	}

	public String getPublishedByCode() {
		return this.publishedByCode;
	}

	public void setPublishedByCode(String publishedByCode) {
		this.publishedByCode = publishedByCode;
	}

	public String getSubmittedById() {
		return this.submittedById;
	}

	public void setSubmittedById(String submittedById) {
		this.submittedById = submittedById;
	}

	public String getSubmittedByCode() {
		return this.submittedByCode;
	}

	public void setSubmittedByCode(String submittedByCode) {
		this.submittedByCode = submittedByCode;
	}

	public String getApprovedById() {
		return this.approvedById;
	}

	public void setApprovedById(String approvedById) {
		this.approvedById = approvedById;
	}

	public String getApprovedByCode() {
		return this.approvedByCode;
	}

	public void setApprovedByCode(String approvedByCode) {
		this.approvedByCode = approvedByCode;
	}

	public Integer getLocationId() {
		return this.locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getLocationCode() {
		return this.locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public Integer getContractHolderId() {
		return this.contractHolderId;
	}

	public void setContractHolderId(Integer contractHolderId) {
		this.contractHolderId = contractHolderId;
	}

	public String getContractHolderCode() {
		return this.contractHolderCode;
	}

	public void setContractHolderCode(String contractHolderCode) {
		this.contractHolderCode = contractHolderCode;
	}

	public Integer getCounterpartyId() {
		return this.counterpartyId;
	}

	public void setCounterpartyId(Integer counterpartyId) {
		this.counterpartyId = counterpartyId;
	}

	public String getCounterpartyCode() {
		return this.counterpartyCode;
	}

	public void setCounterpartyCode(String counterpartyCode) {
		this.counterpartyCode = counterpartyCode;
	}

	public Integer getBillToId() {
		return this.billToId;
	}

	public void setBillToId(Integer billToId) {
		this.billToId = billToId;
	}

	public String getBillToCode() {
		return this.billToCode;
	}

	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}

	public Integer getShipToId() {
		return this.shipToId;
	}

	public void setShipToId(Integer shipToId) {
		this.shipToId = shipToId;
	}

	public String getShipToCode() {
		return this.shipToCode;
	}

	public void setShipToCode(String shipToCode) {
		this.shipToCode = shipToCode;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getNewPrice() {
		return this.newPrice;
	}

	public void setNewPrice(BigDecimal newPrice) {
		this.newPrice = newPrice;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public BidApprovalStatus getApprovalStatus() {
		return this.approvalStatus;
	}

	public void setApprovalStatus(BidApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public Boolean getPublished() {
		return this.published;
	}

	public void setPublished(Boolean published) {
		this.published = published;
	}

	public Date getPublishedOn() {
		return this.publishedOn;
	}

	public void setPublishedOn(Date publishedOn) {
		this.publishedOn = publishedOn;
	}

	public Date getSubmittedOn() {
		return this.submittedOn;
	}

	public void setSubmittedOn(Date submittedOn) {
		this.submittedOn = submittedOn;
	}

	public Date getApprovedOn() {
		return this.approvedOn;
	}

	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}

	public String getDealType() {
		return this.dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getContractStatus() {
		return this.contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

	public String getContractType() {
		return this.contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getDeliveryPoint() {
		return this.deliveryPoint;
	}

	public void setDeliveryPoint(String deliveryPoint) {
		this.deliveryPoint = deliveryPoint;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}

	public Boolean getVisibleFlag() {
		return this.visibleFlag;
	}

	public void setVisibleFlag(Boolean visibleFlag) {
		this.visibleFlag = visibleFlag;
	}
}
