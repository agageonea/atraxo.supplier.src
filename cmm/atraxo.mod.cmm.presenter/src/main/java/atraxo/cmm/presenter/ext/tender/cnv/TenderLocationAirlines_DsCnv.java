/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.tender.cnv;

import javax.persistence.EntityManager;

import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.cmm.presenter.impl.tender.model.TenderLocationAirlines_Ds;
import seava.j4e.api.action.result.IDsConverter;
import seava.j4e.presenter.converter.AbstractDsConverter;

/**
 * Custom converter TenderLocationAirlines_DsCnv
 */
public class TenderLocationAirlines_DsCnv extends AbstractDsConverter<TenderLocationAirlines_Ds, TenderLocationAirlines>
		implements IDsConverter<TenderLocationAirlines_Ds, TenderLocationAirlines> {

	@Override
	public void modelToEntity(TenderLocationAirlines_Ds m, TenderLocationAirlines e, boolean isInsert, EntityManager em) throws Exception {
		super.modelToEntity(m, e, isInsert, em);
		if (e.getIsValid() == null) {
			e.setIsValid(true);
		}
	}
}
