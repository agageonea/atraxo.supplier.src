/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.contracts.service;

import java.util.List;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.presenter.impl.contracts.model.ValidationMessageBid_Ds;
import atraxo.fmbas.domain.impl.validationMessage.ValidationMessage;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service ValidationMessageBid_DsService
 */
public class ValidationMessageBid_DsService
		extends AbstractEntityDsService<ValidationMessageBid_Ds, ValidationMessageBid_Ds, Object, ValidationMessage>
		implements IDsService<ValidationMessageBid_Ds, ValidationMessageBid_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<ValidationMessageBid_Ds, ValidationMessageBid_Ds, Object> builder, List<ValidationMessageBid_Ds> result)
			throws Exception {

		super.postFind(builder, result);

		IContractService contractService = (IContractService) this.findEntityService(Contract.class);

		for (ValidationMessageBid_Ds validationMessage : result) {

			Contract contract = contractService.findByRefid(validationMessage.getObjectId());
			validationMessage.setLocation(contract.getLocation().getCode());
			validationMessage.setTenderName(contract.getBidTenderIdentification().getName());
			validationMessage.setBidVersion(contract.getBidVersion());
			validationMessage.setBidRevision(contract.getBidRevision());

		}
	}

}
