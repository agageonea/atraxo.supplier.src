/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.contracts.qb;

import atraxo.cmm.presenter.impl.contracts.model.ContractCustomer_Ds;
import atraxo.cmm.presenter.impl.contracts.model.ContractCustomer_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class ContractCustomer_DsQb
		extends
			QueryBuilderWithJpql<ContractCustomer_Ds, ContractCustomer_Ds, ContractCustomer_DsParam> {
}
