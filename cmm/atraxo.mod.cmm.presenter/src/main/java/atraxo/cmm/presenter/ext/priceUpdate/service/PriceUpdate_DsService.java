/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.priceUpdate.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.cmm.business.api.priceUpdate.IPriceUpdateService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdate;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdateCategories;
import atraxo.cmm.presenter.impl.priceUpdate.model.PriceUpdateCategories_Ds;
import atraxo.cmm.presenter.impl.priceUpdate.model.PriceUpdate_Ds;
import atraxo.cmm.presenter.impl.priceUpdate.model.PriceUpdate_DsParam;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.action.query.IFilterRule;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.action.result.IDsMarshaller;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service PriceUpdate_DsService
 */
public class PriceUpdate_DsService extends AbstractEntityDsService<PriceUpdate_Ds, PriceUpdate_Ds, PriceUpdate_DsParam, PriceUpdate>
		implements IDsService<PriceUpdate_Ds, PriceUpdate_Ds, PriceUpdate_DsParam> {

	private static final Logger LOG = LoggerFactory.getLogger(PriceUpdate_DsService.class);

	@Autowired
	private IWorkflowInstanceEntityService wieSrv;
	@Autowired
	private IWorkflowBpmManager workflowBpmManager;

	@Override
	@Transactional
	public void deleteByIds(List<Object> ids) throws Exception {
		List<PriceUpdate> priceUpdates = this.getEntityService().findByIds(ids);
		for (PriceUpdate element : priceUpdates) {
			if (!((element.getApprovalStatus() == BidApprovalStatus._NEW_ || element.getApprovalStatus() == BidApprovalStatus._REJECTED_)
					&& !element.getPublished())) {
				throw new BusinessException(CmmErrorCode.PRICEUPDATE_DELETE_ACTION_ERROR, CmmErrorCode.PRICEUPDATE_DELETE_ACTION_ERROR.getErrMsg());
			}
		}
		this.getEntityService().delete(priceUpdates);
	}

	@Override
	protected void preUpdateAfterEntity(PriceUpdate_Ds ds, PriceUpdate e, PriceUpdate_DsParam params) throws Exception {
		super.preUpdateAfterEntity(ds, e, params);
		IPriceUpdateService srv = (IPriceUpdateService) this.getEntityService();
		srv.refresh(e);
	}

	@Override
	protected void postUpdateAfterModel(PriceUpdate_Ds ds, PriceUpdate e, PriceUpdate_DsParam params) throws Exception {
		super.postUpdateAfterModel(ds, e, params);
		List<PriceUpdateCategories> entities = this.findByParams(e, params);
		this.select(entities, e, params, params.getSelectedAll());
		this.getEntityService().update(e);
	}

	/**
	 * @param entities
	 * @param e
	 * @param params
	 * @param selectAll
	 */
	private void select(List<PriceUpdateCategories> entities, PriceUpdate e, PriceUpdate_DsParam params, boolean selectAll) {

		List<String> list = new ArrayList<>();
		if (selectAll) {
			String unSelectedCategories = params.getUnSelectedCategories();
			if (!StringUtils.isEmpty(unSelectedCategories)) {
				if (unSelectedCategories.startsWith("[")) {
					String[] arr = unSelectedCategories.substring(1, unSelectedCategories.length() - 1).split(",");
					list = Arrays.asList(arr);
				} else {
					String[] arr = unSelectedCategories.split(",");
					list = Arrays.asList(arr);
				}
			}
		} else {
			String selectedCategories = params.getSelectedCategories();
			if (!StringUtils.isEmpty(selectedCategories)) {
				if (selectedCategories.startsWith("[")) {
					String[] arr = selectedCategories.substring(1, selectedCategories.length() - 1).split(",");
					list = Arrays.asList(arr);
				} else {
					String[] arr = selectedCategories.split(",");
					list = Arrays.asList(arr);
				}
			}
		}
		e.getPriceUpdateCategories().clear();
		for (PriceUpdateCategories puc : entities) {
			if (puc.getId() != null && list.contains(puc.getId().toString())) {
				puc.setSelected(!selectAll);
			} else {
				puc.setSelected(selectAll);
			}
			puc.setPriceUpdate(e);
			e.addToPriceUpdateCategories(puc);
		}
	}

	@Override
	protected void postFind(IQueryBuilder<PriceUpdate_Ds, PriceUpdate_Ds, PriceUpdate_DsParam> builder, List<PriceUpdate_Ds> result)
			throws Exception {
		super.postFind(builder, result);
		for (PriceUpdate_Ds ds : result) {
			ds.setVisibleFlag(false);
			if (ds.getApprovalStatus().equals(BidApprovalStatus._AWAITING_APPROVAL_)) {
				try {
					WorkflowInstanceEntity wie = this.wieSrv.findByBusinessKey(WorkflowNames.PRICE_UPDATE_APPROVAL.getWorkflowName(), ds.getId(),
							ds._getEntity_().getClass().getSimpleName());
					ds.setVisibleFlag(this.workflowBpmManager.canComplete(wie.getWorkflowInstance().getId()));
				} catch (Exception e) {
					// do nothing
					LOG.warn("Could not retrieve a WorkflowInstanceEntity for workflow PRICE_UPDATE_APPROVAL, will do nothing !", e);
				}
			}

		}
	}

	private List<PriceUpdateCategories> findByParams(PriceUpdate e, PriceUpdate_DsParam params) throws Exception {
		IDsService<PriceUpdateCategories_Ds, Object, Object> service = this.findDsService(PriceUpdateCategories_Ds.class);
		IDsMarshaller<PriceUpdateCategories_Ds, Object, Object> marshaller = service.createMarshaller(IDsMarshaller.JSON);
		Object stdFilter;
		if (StringUtils.isEmpty(params.getStandardFilter())) {
			PriceUpdateCategories_Ds dsFilter = new PriceUpdateCategories_Ds();
			dsFilter.setPriceUpdateId(e.getId());
			stdFilter = dsFilter;
		} else {
			stdFilter = marshaller.readFilterFromString(params.getStandardFilter());
		}
		IQueryBuilder<PriceUpdateCategories_Ds, Object, Object> builder = service.createQueryBuilder().addFilter(stdFilter);
		if (StringUtils.isNotEmpty(params.getFilter())) {
			List<IFilterRule> filterRules = marshaller.readFilterRules(params.getFilter());
			if (!CollectionUtils.isEmpty(filterRules)) {
				builder.addFilterRules(filterRules);
			}
		}

		QueryBuilderWithJpql<PriceUpdateCategories_Ds, Object, Object> bld = (QueryBuilderWithJpql<PriceUpdateCategories_Ds, Object, Object>) builder;

		return bld.createQuery(PriceUpdateCategories.class).setFirstResult(bld.getResultStart()).setMaxResults(bld.getResultSize()).getResultList();

	}
}
