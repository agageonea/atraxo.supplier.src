/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.tender.model;

import atraxo.cmm.domain.impl.tender.TenderLocationExpectedPrice;
import atraxo.fmbas.domain.impl.categories.IataPC;
import atraxo.fmbas.domain.impl.fmbas_type.Use;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = TenderLocationExpectedPrice.class)
@RefLookups({
		@RefLookup(refId = TenderLocationExpectedPrices_Ds.F_TENDERLOCATIONID),
		@RefLookup(refId = TenderLocationExpectedPrices_Ds.F_IATAPRICECATEGORYID, namedQuery = IataPC.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TenderLocationExpectedPrices_Ds.F_IATAPRICECATEGORYCODE)})})
public class TenderLocationExpectedPrices_Ds
		extends
			AbstractDs_Ds<TenderLocationExpectedPrice> {

	public static final String ALIAS = "cmm_TenderLocationExpectedPrices_Ds";

	public static final String F_IATAPRICECATEGORYID = "iataPriceCategoryId";
	public static final String F_IATAPRICECATEGORYCODE = "iataPriceCategoryCode";
	public static final String F_IATAPRICECATEGORYNAME = "iataPriceCategoryName";
	public static final String F_IATAPRICECATEGORYTYPE = "iataPriceCategoryType";
	public static final String F_TENDERLOCATIONID = "tenderLocationId";

	@DsField(join = "left", path = "iataPriceCategory.id")
	private Integer iataPriceCategoryId;

	@DsField(join = "left", path = "iataPriceCategory.code")
	private String iataPriceCategoryCode;

	@DsField(join = "left", path = "iataPriceCategory.name")
	private String iataPriceCategoryName;

	@DsField(join = "left", path = "iataPriceCategory.use")
	private Use iataPriceCategoryType;

	@DsField(join = "left", path = "tenderLocation.id")
	private Integer tenderLocationId;

	/**
	 * Default constructor
	 */
	public TenderLocationExpectedPrices_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public TenderLocationExpectedPrices_Ds(TenderLocationExpectedPrice e) {
		super(e);
	}

	public Integer getIataPriceCategoryId() {
		return this.iataPriceCategoryId;
	}

	public void setIataPriceCategoryId(Integer iataPriceCategoryId) {
		this.iataPriceCategoryId = iataPriceCategoryId;
	}

	public String getIataPriceCategoryCode() {
		return this.iataPriceCategoryCode;
	}

	public void setIataPriceCategoryCode(String iataPriceCategoryCode) {
		this.iataPriceCategoryCode = iataPriceCategoryCode;
	}

	public String getIataPriceCategoryName() {
		return this.iataPriceCategoryName;
	}

	public void setIataPriceCategoryName(String iataPriceCategoryName) {
		this.iataPriceCategoryName = iataPriceCategoryName;
	}

	public Use getIataPriceCategoryType() {
		return this.iataPriceCategoryType;
	}

	public void setIataPriceCategoryType(Use iataPriceCategoryType) {
		this.iataPriceCategoryType = iataPriceCategoryType;
	}

	public Integer getTenderLocationId() {
		return this.tenderLocationId;
	}

	public void setTenderLocationId(Integer tenderLocationId) {
		this.tenderLocationId = tenderLocationId;
	}
}
