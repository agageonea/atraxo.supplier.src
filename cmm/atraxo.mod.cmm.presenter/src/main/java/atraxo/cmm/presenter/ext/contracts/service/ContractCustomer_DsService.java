/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.contracts.service;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import atraxo.cmm.business.api.contracts.IContractChangeService;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ext.contracts.service.ContractChangeUtil;
import atraxo.cmm.business.ext.contracts.service.ContractUtil;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.ContractChange;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.cmm.presenter.impl.contracts.model.ContractCustomer_Ds;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.domain.impl.fmbas_type.ContractChangeType;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceType;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ContractCustomer_DsService extends AbstractEntityDsService<ContractCustomer_Ds, ContractCustomer_Ds, Object, Contract>
		implements IDsService<ContractCustomer_Ds, ContractCustomer_Ds, Object> {

	private static final String ORIGINAL = "Original";

	private static final String TEMPORARY_VERSION = "Temporary Version";

	private static final Logger LOGG = LoggerFactory.getLogger(ContractCustomer_DsService.class);

	@Autowired
	private IWorkflowInstanceEntityService workflowInstanceEntityService;

	@Autowired
	private IWorkflowBpmManager workflowBpmManager;

	@Autowired
	private ISystemParameterService systemParameterService;

	@Autowired
	private IContractChangeService contractChangeService;

	@Autowired
	private IContractService contractService;

	@Override
	protected void postFind(IQueryBuilder<ContractCustomer_Ds, ContractCustomer_Ds, Object> builder, List<ContractCustomer_Ds> result)
			throws Exception {
		super.postFind(builder, result);

		boolean useWorkflow = this.systemParameterService.getSellContractApprovalWorkflow();

		for (ContractCustomer_Ds ds : result) {
			Contract contract = this.contractService.findById(ds.getId());
			Locations locations = ds._getEntity_().getLocation();
			String str = "";
			if (locations != null) {
				str += str.matches("") ? locations.getCode() : " , " + locations.getCode();
			}
			ds.setUnit(ds.getSettCurrCode() + "/" + ds.getSettUnitCode());
			ds.setDest(str);

			// Check if the contract has active modifications
			Contract blueprint = null;
			boolean hasChanges = false;
			boolean hasWkfChanges = false;

			ds.setContractType(ORIGINAL);

			if (contract.getBlueprintOriginalContractReference() != null) {
				blueprint = this.contractService.getBlueprintSaleContract(contract);
				hasChanges = !this.contractChangeService.getContractChanges(blueprint).isEmpty();
				hasWkfChanges = !this.contractChangeService.getWorkflowContractChanges(blueprint).isEmpty();
				ds.setPeriodApprovalStatus(blueprint.getPeriodApprovalStatus());
				ds.setPriceApprovalStatus(blueprint.getPriceApprovalStatus());
				ds.setShipToApprovalStatus(blueprint.getShipToApprovalStatus());

				if (ds.getIsBlueprint()) {
					ds.setContractType(TEMPORARY_VERSION);
				}

			}

			// Build the string to display all shipTo's
			ds.setShipToList(this.buildShipToList(contract));

			// Set display code for original and blueprint
			ds.setDisplayCode(ContractChangeUtil.checkContractCode(ds.getCode()));

			// Set can be submitted
			ds.setCanBeSubmitted(this.checkIfCanBeSubmited(contract, blueprint, hasWkfChanges, useWorkflow));

			// Set can be published
			ds.setCanBePublished(this.checkIfCanBePublished(blueprint, hasChanges, useWorkflow));

			// set can be completed (check for tasks only if status is Awaiting approval in order to not make a search for tasks every time)
			ds.setCanBeCompleted(this.canWorkflowBeCompleted(ds, ds.getApprovalStatus(), WorkflowNames.SELL_CONTRACT_APPROVAL));
			ds.setCanPeriodBeCompleted(
					this.canWorkflowBeCompleted(ds, ds.getPeriodApprovalStatus(), WorkflowNames.SALE_CONTRACT_PERIOD_UPDATE_APPROVAL));
			ds.setCanShipToBeCompleted(
					this.canWorkflowBeCompleted(ds, ds.getShipToApprovalStatus(), WorkflowNames.SALE_CONTRACT_SHIPTO_UPDATE_APPROVAL));
			ds.setCanPriceBeCompleted(
					this.canWorkflowBeCompleted(ds, ds.getPriceApprovalStatus(), WorkflowNames.SALE_CONTRACT_PRICE_CATEGORY_APPROVAL));

			// set the allert message in case there are changes and workflow is in progress
			ds.setPeriodAlertMessage(this.getAlertChanges(contract, ContractChangeType._PERIOD_, useWorkflow));
			ds.setShipToAlertMessage(this.getAlertChanges(contract, ContractChangeType._SHIPTO_, useWorkflow));
			ds.setPriceAlertMessage(this.getAlertChanges(contract, ContractChangeType._PRICE_, useWorkflow));

		}
	}

	@Override
	protected void preInsert(ContractCustomer_Ds ds, Contract e, Object params) throws Exception {
		super.preInsert(ds, e, params);
		this.checkRPTDesign(ds, e);
	}

	@Override
	protected void preUpdateAfterEntity(ContractCustomer_Ds ds, Contract e, Object params) throws Exception {
		super.preUpdateAfterEntity(ds, e, params);
		this.checkRPTDesign(ds, e);
	}

	private void checkRPTDesign(ContractCustomer_Ds ds, Contract e) throws Exception {
		if (InvoiceType._PDF_FORMAT_.equals(ds.getInvoiceType())) {
			if (StringUtils.isEmpty(ds.getInvoiceTemplateName())) {
				throw new BusinessException(CmmErrorCode.RPTDESIGN_IS_MISSING, CmmErrorCode.RPTDESIGN_IS_MISSING.getErrMsg());
			}
		} else {
			e.setInvoiceTemplate(null);
		}
	}

	/**
	 * @param ds
	 * @param approvalStatus
	 * @param workflowName
	 * @return
	 */
	private Boolean canWorkflowBeCompleted(ContractCustomer_Ds ds, BidApprovalStatus approvalStatus, WorkflowNames workflowName) {
		Boolean canBeCompleted = false;
		if (BidApprovalStatus._AWAITING_APPROVAL_.equals(approvalStatus)) {
			try {
				WorkflowInstanceEntity wie = this.workflowInstanceEntityService.findByBusinessKey(workflowName.getWorkflowName(), ds.getId(),
						ds._getEntity_().getClass().getSimpleName());
				canBeCompleted = this.workflowBpmManager.canComplete(wie.getWorkflowInstance().getId());
			} catch (Exception e) {
				LOGG.info("Could not find any workflow instance for " + ds.getCode(), e);
			}
		}
		return canBeCompleted;
	}

	/**
	 * @param contract
	 * @param type
	 * @param useWorkflow
	 * @return
	 * @throws BusinessException
	 */
	private String getAlertChanges(Contract contract, ContractChangeType type, boolean useWorkflow) throws BusinessException {
		String result = "";
		List<ContractChange> contractChanges = this.contractChangeService.getContractChanges(contract, type);
		if (!CollectionUtils.isEmpty(contractChanges)) {
			if (type.equals(ContractChangeType._PERIOD_)) {
				result = (useWorkflow ? CmmErrorCode.PERIOD_AWAITING_APPROVAL.getErrMsg() : CmmErrorCode.PERIOD_AWAITING_PUBLISH.getErrMsg())
						+ contractChanges.get(contractChanges.size() - 1).getMessage();
			} else if (type.equals(ContractChangeType._SHIPTO_)) {
				result = useWorkflow ? CmmErrorCode.SHIPTO_AWAITING_APPROVAL.getErrMsg() : CmmErrorCode.SHIPTO_AWAITING_PUBLISH.getErrMsg();
			} else if (type.equals(ContractChangeType._PRICE_)) {
				result = useWorkflow ? CmmErrorCode.PRICE_AWAITING_APPROVAL.getErrMsg() : CmmErrorCode.PRICE_AWAITING_PUBLISH.getErrMsg();
			}
		}
		return result;
	}

	/**
	 * Build the string to display all shipTo's
	 *
	 * @param ds
	 * @param contract
	 */
	private String buildShipToList(Contract contract) {
		StringBuilder sb = new StringBuilder();
		if (!contract.getShipTo().isEmpty()) {
			for (ShipTo st : contract.getShipTo()) {
				sb.append(st.getCustomer().getName());
				sb.append(", ");
			}
			if (!contract.getShipTo().isEmpty()) {
				sb.setLength(sb.length() - 2);
			}
		}

		return sb.toString();
	}

	/**
	 * Check if the contract can be submited for approval, based on all the contract related workflows
	 *
	 * @param contract
	 * @param hasChanges
	 * @param useWorkflow
	 * @throws BusinessException
	 */
	private boolean checkIfCanBeSubmited(Contract contract, Contract blueprint, boolean hasChanges, boolean useWorkflow) throws BusinessException {
		boolean canBeSubmited = false;
		if (useWorkflow) {
			if (contract.getStatus().equals(ContractStatus._DRAFT_)) {
				canBeSubmited = !ContractUtil.alreadyApprovedOrInApprovalProcess(contract);
			} else {
				if (contract.getBlueprintOriginalContractReference() != null) {
					if (ContractUtil.alreadyInApprovalProcess(blueprint)) {
						canBeSubmited = false;
					} else {
						canBeSubmited = hasChanges;
					}
				} else {
					canBeSubmited = false;
				}
			}
		}
		return canBeSubmited;
	}

	/**
	 * Check if the contract can be published, based on all the blueprint modifications
	 *
	 * @param blueprint
	 * @param hasChanges
	 * @param useWorkflow
	 * @throws BusinessException
	 */
	private boolean checkIfCanBePublished(Contract blueprint, boolean hasChanges, boolean useWorkflow) throws BusinessException {
		boolean canBePublished = false;
		if (blueprint != null) {
			canBePublished = blueprint.getVersion() > 1 || hasChanges;
			if (useWorkflow && ContractUtil.alreadyInApprovalProcess(blueprint)) {
				canBePublished = false;
			}
		}

		return canBePublished;
	}

}
