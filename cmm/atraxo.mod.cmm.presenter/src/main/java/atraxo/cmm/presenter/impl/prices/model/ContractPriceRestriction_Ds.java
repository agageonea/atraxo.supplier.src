/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.impl.prices.model;

import atraxo.cmm.domain.impl.cmm_type.Operators;
import atraxo.cmm.domain.impl.cmm_type.RestrictionTypes;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ContractPriceRestriction.class)
@RefLookups({
		@RefLookup(refId = ContractPriceRestriction_Ds.F_CONTRACTPRICECTGRY),
		@RefLookup(refId = ContractPriceRestriction_Ds.F_UNIT)})
public class ContractPriceRestriction_Ds
		extends
			AbstractDs_Ds<ContractPriceRestriction> {

	public static final String ALIAS = "cmm_ContractPriceRestriction_Ds";

	public static final String F_RESTRICTIONTYPE = "restrictionType";
	public static final String F_OPERATOR = "operator";
	public static final String F_VALUE = "value";
	public static final String F_CONTRACTPRICECTGRY = "contractPriceCtgry";
	public static final String F_UNIT = "unit";
	public static final String F_UNITCODE = "unitCode";

	@DsField
	private RestrictionTypes restrictionType;

	@DsField
	private Operators operator;

	@DsField
	private String value;

	@DsField(join = "left", path = "contractPriceCategory.id")
	private Integer contractPriceCtgry;

	@DsField(join = "left", path = "unit.id")
	private Integer unit;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	/**
	 * Default constructor
	 */
	public ContractPriceRestriction_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ContractPriceRestriction_Ds(ContractPriceRestriction e) {
		super(e);
	}

	public RestrictionTypes getRestrictionType() {
		return this.restrictionType;
	}

	public void setRestrictionType(RestrictionTypes restrictionType) {
		this.restrictionType = restrictionType;
	}

	public Operators getOperator() {
		return this.operator;
	}

	public void setOperator(Operators operator) {
		this.operator = operator;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getContractPriceCtgry() {
		return this.contractPriceCtgry;
	}

	public void setContractPriceCtgry(Integer contractPriceCtgry) {
		this.contractPriceCtgry = contractPriceCtgry;
	}

	public Integer getUnit() {
		return this.unit;
	}

	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}
}
