/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.contracts.service;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IPricingBaseService;
import atraxo.cmm.business.api.ext.contracts.IPriceBuilder;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.presenter.ext.prices.service.validator.PricingBaseValidator;
import atraxo.cmm.presenter.ext.prices.service.validator.QuotationValidator;
import atraxo.cmm.presenter.impl.contracts.model.ContractCustomerWizard_Ds;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service ContractCustomerWizard_DsService
 */
public class ContractCustomerWizard_DsService extends AbstractEntityDsService<ContractCustomerWizard_Ds, ContractCustomerWizard_Ds, Object, Contract>
		implements IDsService<ContractCustomerWizard_Ds, ContractCustomerWizard_Ds, Object> {

	@Autowired
	private IPriceBuilder builderSrv;
	@Autowired
	private IPricingBaseService pbServie;
	@Autowired
	private PricingBaseValidator pbValidatorSrv;
	@Autowired
	private QuotationValidator quotationValidatorSrv;

	@Override
	protected void preInsert(ContractCustomerWizard_Ds ds, Contract e, Object params) throws Exception {
		super.preInsert(ds, e, params);
		int counter = 1;
		for (PricingBase pb : e.getPricingBases()) {
			if (this.pbServie.isIndex(pb)) {
				this.quotationValidatorSrv.validateQuotation(pb);
			}
			this.pbValidatorSrv.validatePricingBaseOnInsert(ds, pb);
			pb.setOrderIndex(counter);
			counter++;
			this.builderSrv.buildPricingBases(pb);
		}
	}
}
