package atraxo.cmm.presenter.ext.tender.delegate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import atraxo.cmm.business.api.ext.tender.IBidGeneratorService;
import atraxo.cmm.business.api.tender.ITenderLocationAirlinesService;
import atraxo.cmm.business.ext.bid.BidGeneratorService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.tender.TenderDefaultConstants;
import atraxo.cmm.business.ext.utils.BidVersionCalculator;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.cmm.presenter.impl.tender.model.TenderLocationAirlines_Ds;
import atraxo.cmm.presenter.impl.tender.model.TenderLocationAirlines_DsParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author zspeter
 */
public class TenderLocationAirlines_Pd extends AbstractPresenterDelegate {

	/**
	 * Generate bid from given tender location
	 *
	 * @param tenderLocationDs
	 * @param param
	 * @throws Exception
	 */
	public void generateBidPerReceivers(List<TenderLocationAirlines_Ds> tenderLocationAirlinesDs, TenderLocationAirlines_DsParam param)
			throws Exception {
		IBidGeneratorService bidGeneratorService = this.getApplicationContext().getBean(BidGeneratorService.class);
		ITenderLocationAirlinesService locationAirlineService = (ITenderLocationAirlinesService) this.findEntityService(TenderLocationAirlines.class);

		String bidVersion = !StringUtils.isEmpty(param.getBidVersion()) ? param.getBidVersion() : BidGeneratorService.INITIAL_BID_VERSION;

		BidVersionCalculator calc = new BidVersionCalculator();
		for (TenderLocationAirlines_Ds ds : tenderLocationAirlinesDs) {
			Map<String, Object> params = this.extractTenderLocationParams(param, bidVersion);

			TenderLocationAirlines locationAirline = locationAirlineService.findById(ds.getId());
			if (!locationAirline.getIsValid()) {
				throw new BusinessException(CmmErrorCode.FUEL_RECEIVER_NOT_VALID,
						String.format(CmmErrorCode.FUEL_RECEIVER_NOT_VALID.getErrMsg(), locationAirline.getAirline().getCode()));
			}

			bidGeneratorService.generateBidFromLocation(ds.getTenderLocationId(), params, locationAirline);

			// calculate next version
			bidVersion = calc.calculateNewVersion(Arrays.asList(bidVersion));
		}
	}

	private Map<String, Object> extractTenderLocationParams(TenderLocationAirlines_DsParam params, String nextBidVersion) {
		Map<String, Object> generateBidParams = new HashMap<>();

		generateBidParams.put(TenderDefaultConstants.PARAM_CAPTURE_BID_VERSION, nextBidVersion);
		generateBidParams.put(TenderDefaultConstants.PARAM_CAPTURE_BID_FROM_HOLDER_MA, params.getCaptureBidFromHolderMA());
		generateBidParams.put(TenderDefaultConstants.PARAM_CAPTURE_BID_FROM_RECEIVER_MA, params.getCaptureBidFromReceiverMA());
		generateBidParams.put(TenderDefaultConstants.PARAM_CAPTURE_BID_FROM_EXISTING_DATA, params.getCaptureBidFromExistingBids());

		return generateBidParams;
	}

}
