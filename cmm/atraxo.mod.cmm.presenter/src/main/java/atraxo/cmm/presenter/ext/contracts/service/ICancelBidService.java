/**
 *
 */
package atraxo.cmm.presenter.ext.contracts.service;

import java.util.List;

import atraxo.cmm.presenter.impl.contracts.model.TenderBid_Ds;
import atraxo.cmm.presenter.impl.contracts.model.TenderBid_DsParam;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Interface define methods for bids cancellation.
 *
 * @author zspeter
 */
public interface ICancelBidService {

	/**
	 * Cancel one ore more bids.
	 *
	 * @param tenderBidDsList list of bids that are marked for cancellation.
	 * @param params Input parameters may contain the reason for cancellation.
	 * @throws BusinessException In case of any business error occurs or one or more bids cannot be canceled.
	 */
	void cancelBid(List<TenderBid_Ds> tenderBidDsList, TenderBid_DsParam params) throws BusinessException;

	/**
	 * @param tenderBidDsList
	 * @return true in case that all bids can be canceled, false otherwise.
	 */
	boolean canCancelBids(List<TenderBid_Ds> tenderBidDsList);
}
