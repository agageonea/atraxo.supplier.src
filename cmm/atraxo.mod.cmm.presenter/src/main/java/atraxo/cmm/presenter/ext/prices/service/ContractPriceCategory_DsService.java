/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.presenter.ext.prices.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.ext.contracts.IPriceBuilder;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.ext.contracts.service.ContractChangeUtil;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.exceptions.FuelPriceNotExistsException;
import atraxo.cmm.business.ext.exceptions.NoPriceComponentsException;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.presenter.ext.prices.service.validator.ContractPriceCategoryValidator;
import atraxo.cmm.presenter.impl.prices.model.ContractPriceCategory_Ds;
import atraxo.cmm.presenter.impl.prices.model.ContractPriceCategory_DsParam;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ContractPriceCategory_DsService
		extends AbstractEntityDsService<ContractPriceCategory_Ds, ContractPriceCategory_Ds, ContractPriceCategory_DsParam, ContractPriceCategory>
		implements IDsService<ContractPriceCategory_Ds, ContractPriceCategory_Ds, ContractPriceCategory_DsParam> {

	private static final Logger LOG = LoggerFactory.getLogger(ContractPriceCategory_DsService.class);

	@Autowired
	private IPriceBuilder priceBuilder;
	@Autowired
	private ContractPriceCategoryValidator validator;
	@Autowired
	private IContractService contractService;
	@Autowired
	private IContractPriceCategoryService cpcService;

	@Override
	public void deleteByIds(List<Object> ids) throws Exception {
		try {
			super.deleteByIds(ids);
		} catch (PersistenceException e) {
			LOG.warn("Contract price category delete:", e);
			throw new BusinessException(CmmErrorCode.PB_CANNOT_EXTEND, CmmErrorCode.PB_CANNOT_EXTEND.getErrMsg(), e.getCause());
		}
	}

	@Override
	protected void postFind(IQueryBuilder<ContractPriceCategory_Ds, ContractPriceCategory_Ds, ContractPriceCategory_DsParam> builder,
			List<ContractPriceCategory_Ds> result) throws Exception {
		ContractPriceCategory_DsParam params = builder.getParams();
		Map<String, ContractPriceCategory_Ds> usedPriceCategories = new HashMap<>();
		Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH);
		if (params.getPriceDate() != null) {
			date = params.getPriceDate();
		}
		for (ContractPriceCategory_Ds priceCategoryDs : result) {
			try {
				this.calculateConvertedPrice(this.cpcService, params, priceCategoryDs, date);
				this.markPriceCategories(usedPriceCategories, priceCategoryDs);
			} catch (NoPriceComponentsException e) {
				LOG.warn("No contract price component:", e);
				priceCategoryDs.setConvertedPrice(BigDecimal.ZERO);
				priceCategoryDs.setUnitCode("");
				priceCategoryDs.setUnitId(null);
				priceCategoryDs.setCurrencyCode("");
				priceCategoryDs.setCurrencyId(null);
			}
			priceCategoryDs.setContCode(ContractChangeUtil.checkContractCode(priceCategoryDs.getContCode()));
		}

		IContractPriceCategoryService pcService = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
		for (ContractPriceCategory_Ds pc : result) {
			String str = this.getChildren(pc.getId(), pcService);
			pc.setPercentageOf(str);
		}
	}

	@Override
	protected void postUpdate(List<ContractPriceCategory_Ds> list, ContractPriceCategory_DsParam params) throws Exception {
		IContractPriceCategoryService pcService = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
		for (ContractPriceCategory_Ds pc : list) {
			String str = this.getChildren(pc.getId(), pcService);
			pc.setPercentageOf(str);
		}
	}

	/**
	 * Check if unit and currency parameters are set, otherwise set them with contract settlement currency and unit.
	 *
	 * @param params
	 * @param contract
	 */
	private void checkAndUpdateParams(ContractPriceCategory_DsParam params, Contract contract) {
		if (StringUtils.isEmpty(params.getSettlementCurrencyCode())) {
			params.setSettlementCurrencyCode(contract.getSettlementCurr().getCode());
		}
		if (StringUtils.isEmpty(params.getSettlementUnitCode())) {
			params.setSettlementUnitCode(contract.getSettlementUnit().getCode());
		}
	}

	private void calculateConvertedPrice(IContractPriceCategoryService service, ContractPriceCategory_DsParam params,
			ContractPriceCategory_Ds priceCategoryDs, Date date) throws BusinessException {
		ContractPriceCategory priceCategory = service.findById(priceCategoryDs.getId());
		if (date.before(priceCategoryDs.getContractValidFrom())) {
			date = priceCategoryDs.getContractValidFrom();
		}
		if (date.after(priceCategoryDs.getContractValidTo())) {
			date = priceCategoryDs.getContractValidTo();
		}
		this.checkAndUpdateParams(params, priceCategory.getPricingBases().getContract());
		ContractPriceComponent priceComponent = service.getPriceComponent(priceCategory, date);

		if (PriceInd._COMPOSITE_.equals(priceCategoryDs.getPriceCtgryPricePer())) {
			BigDecimal compositePrice = service.getPriceInCurrencyUnit(priceCategory, priceCategory.getContract().getSettlementUnit().getCode(),
					priceCategory.getContract().getSettlementCurr().getCode(), date);
			priceComponent.setPrice(compositePrice);
		}

		this.setContractPriceCtgry(priceComponent, priceCategoryDs);
	}

	private void markPriceCategories(Map<String, ContractPriceCategory_Ds> usedPriceCategories, ContractPriceCategory_Ds priceCategoryDs) {
		ContractPriceCategory priceCategory = priceCategoryDs._getEntity_();
		PriceInd priceCategoryPer = priceCategory.getPriceCategory().getPricePer();
		if (!PriceInd._EVENT_.equals(priceCategoryPer)) {
			if (!usedPriceCategories.keySet().contains(priceCategory.getPriceCategory().getName())) {
				usedPriceCategories.put(priceCategory.getPriceCategory().getName(), priceCategoryDs);
				priceCategoryDs.setUsed(true);
			} else if (priceCategory.getDefaultPriceCtgy()) {
				usedPriceCategories.get(priceCategory.getPriceCategory().getName()).setUsed(false);
				usedPriceCategories.put(priceCategory.getPriceCategory().getName(), priceCategoryDs);
				priceCategoryDs.setUsed(true);
			}
		}
	}

	@Override
	protected void postSummaries(IQueryBuilder<ContractPriceCategory_Ds, ContractPriceCategory_Ds, ContractPriceCategory_DsParam> builder,
			List<ContractPriceCategory_Ds> findResult, Map<String, Object> sum) throws Exception {
		super.postSummaries(builder, findResult, sum);

		ContractPriceCategory_DsParam params = builder.getParams();
		BigDecimal total = BigDecimal.ZERO;
		Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH);
		if (params.getPriceDate() != null) {
			date = params.getPriceDate();
		}
		if (!findResult.isEmpty()) {
			try {
				ContractPriceCategory_Ds ds = findResult.get(0);
				Integer contractId = ds.getContract();
				total = this.contractService.getPrice(contractId, params.getSettlementUnitCode(), params.getSettlementCurrencyCode(), date);
			} catch (FuelPriceNotExistsException e) {
				LOG.warn(e.getMessage(), e);
			}
		}

		params.setTotal(total);
	}

	/**
	 * @param cpComponent
	 * @param cpc
	 */
	private void setContractPriceCtgry(ContractPriceComponent cpComponent, ContractPriceCategory_Ds cpc) {
		if (cpComponent.getPrice() != null) {
			cpc.setPrice(cpComponent.getPrice());
		}
		if (cpComponent.getValidFrom() != null) {
			cpc.setValidFrom(cpComponent.getValidFrom());
		}
		if (cpComponent.getValidTo() != null) {
			cpc.setValidTo(cpComponent.getValidTo());
		}
		if (cpComponent.getCurrency() != null && cpComponent.getCurrency().getCode() != null) {
			cpc.setCurrencyCode(cpComponent.getCurrency().getCode());
		}
		if (cpComponent.getCurrency() != null && cpComponent.getCurrency().getId() != null) {
			cpc.setCurrencyId(cpComponent.getCurrency().getId());
		}
		if (cpComponent.getUnit() != null && cpComponent.getUnit().getCode() != null) {
			cpc.setUnitCode(cpComponent.getUnit().getCode());
		}
		if (cpComponent.getUnit() != null && cpComponent.getUnit().getId() != null) {
			cpc.setUnitId(cpComponent.getUnit().getId());
		}
	}

	private String getChildren(Integer id, IContractPriceCategoryService pcService) {
		List<ContractPriceCategory> pcList = pcService.findByChildPriceCategoryId(id);
		StringBuilder sb = new StringBuilder();
		for (ContractPriceCategory pc : pcList) {
			sb.append(",").append(pc.getName());
		}
		if (sb.length() != 0) {
			sb = sb.replace(0, 1, "");
		}
		return sb.toString();
	}

	@Override
	protected void preInsert(List<ContractPriceCategory_Ds> list, ContractPriceCategory_DsParam params) throws Exception {
		for (ContractPriceCategory_Ds pc : list) {
			if (!PriceInd._PERCENT_.equals(pc.getPriceCtgryPricePer())) {
				pc.setPercentageOf("");
			}
		}
	}

	@Override
	protected void preInsert(ContractPriceCategory_Ds ds, ContractPriceCategory e, ContractPriceCategory_DsParam params) throws Exception {
		super.preInsert(ds, e, params);
		this.checkFinancialSourceAvereagingmethod(ds);
	}

	@Override
	protected void preUpdate(List<ContractPriceCategory_Ds> list, ContractPriceCategory_DsParam params) throws Exception {
		for (ContractPriceCategory_Ds pc : list) {
			if (!PriceInd._PERCENT_.equals(pc.getPriceCtgryPricePer())) {
				pc.setPercentageOf("");
			}
			this.checkFinancialSourceAvereagingmethod(pc);
		}
	}

	@Override
	protected void preUpdateAfterEntity(ContractPriceCategory_Ds ds, ContractPriceCategory e, ContractPriceCategory_DsParam params) throws Exception {
		super.preUpdateAfterEntity(ds, e, params);
		this.validator.checkRestrictions(e);
		this.priceBuilder.updateChilds(e);
		if (e.getPriceCategory().getPricePer().equals(PriceInd._COMPOSITE_)) {
			this.priceBuilder.assignParentPriceCategory(e.getPriceCategoryIdsJson(), e);
			this.priceBuilder.updateParentPriceCategories(e, CalculateIndicator._CALCULATE_ONLY_);
		}
	}

	private void checkFinancialSourceAvereagingmethod(ContractPriceCategory_Ds ds) throws BusinessException {
		if ("".equals(ds.getAvgMethodIndicatorName())) {
			throw new BusinessException(CmmErrorCode.AVERAGE_METHOD_IS_MISSING, CmmErrorCode.AVERAGE_METHOD_IS_MISSING.getErrMsg());
		}
		if (StringUtils.isEmpty(ds.getFinancialSourceCode())) {
			throw new BusinessException(CmmErrorCode.FINCANCIAL_SOURCE_IS_MISSING, CmmErrorCode.FINCANCIAL_SOURCE_IS_MISSING.getErrMsg());
		}
	}

}
