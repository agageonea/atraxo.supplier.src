/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdateCategoriesInfinite_Dc", {
	extend: "e4e.dc.AbstractDc",
	_infiniteScroll_: true,
	paramModel: atraxo.cmm.ui.extjs.ds.PriceUpdateCategories_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.PriceUpdateCategories_Ds
});

/* ================= GRID: NewList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdateCategoriesInfinite_Dc$NewList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_PriceUpdateCategoriesInfinite_Dc$NewList",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"priceCategory", dataIndex:"priceCategoryName", width:50,  flex:1})
		.addTextColumn({ name:"location", dataIndex:"locationCode", width:50,  flex:1})
		.addTextColumn({ name:"contract", dataIndex:"contract", width:50,  flex:1})
		.addTextColumn({ name:"holder", dataIndex:"contractHolderCode", width:50,  flex:1})
		.addTextColumn({ name:"counterparty", dataIndex:"counterpartyCode", width:50,  flex:1})
		.addNumberColumn({ name:"price", dataIndex:"price", sysDec:"dec_crncy",  flex:1})
		.addTextColumn({ name:"currency", dataIndex:"currencyCode", width:50,  flex:1})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:50,  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: GridLabel ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdateCategoriesInfinite_Dc$GridLabel", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PriceUpdateCategoriesInfinite_Dc$GridLabel",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"priceCategoryLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, cls:"sone-flat-label-field"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:650})
		.addPanel({ name:"col1", width:650, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["priceCategoryLabel"]);
	}
});
