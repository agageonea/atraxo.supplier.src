/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.BidValidations_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.cmm.ui.extjs.ds.ValidationMessageBid_Ds
});

/* ================= GRID: BidValidations ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.BidValidations_Dc$BidValidations", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_BidValidations_Dc$BidValidations",
	_noExport_: true,
	_noPaginator_: true,
	selType: null,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"bidLocation", dataIndex:"location", width:100})
		.addTextColumn({ name:"tenderName", dataIndex:"tenderName", width:130})
		.addTextColumn({ name:"bidVersion", dataIndex:"bidVersion", width:95})
		.addNumberColumn({ name:"bidRevision", dataIndex:"bidRevision", width:95})
		.addTextColumn({ name:"message", dataIndex:"message", width:248})
		.addTextColumn({ name:"objectId", dataIndex:"objectId", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
