/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.LocationsLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_LocationsLov_Ds"
	},
	
	
	fields: [
		{name:"name", type:"string"},
		{name:"areaName", type:"string"},
		{name:"code", type:"string"},
		{name:"type", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.LocationsLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"name", type:"string"},
		{name:"areaName", type:"string"},
		{name:"code", type:"string"},
		{name:"type", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.cmm.ui.extjs.ds.LocationsLov_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"areaID", type:"int", allowNull:true}
	]
});
