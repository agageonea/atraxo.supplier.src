/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.BuyContractLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_BuyContractLov_Ds"
	},
	
	
	fields: [
		{name:"code", type:"string"},
		{name:"id", type:"int", allowNull:true},
		{name:"dealType", type:"string"},
		{name:"type", type:"string"},
		{name:"subType", type:"string"},
		{name:"scope", type:"string"},
		{name:"status", type:"string"},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.BuyContractLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"code", type:"string"},
		{name:"id", type:"int", allowNull:true},
		{name:"dealType", type:"string"},
		{name:"type", type:"string"},
		{name:"subType", type:"string"},
		{name:"scope", type:"string"},
		{name:"status", type:"string"},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
