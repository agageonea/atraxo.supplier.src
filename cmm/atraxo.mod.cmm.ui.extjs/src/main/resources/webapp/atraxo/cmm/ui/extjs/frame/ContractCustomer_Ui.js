/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.frame.ContractCustomer_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.ContractCustomer_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("contractList", Ext.create(atraxo.cmm.ui.extjs.dc.ContractCustomer_Dc,{}))
		.addDc("contractEdit", Ext.create(atraxo.cmm.ui.extjs.dc.ContractCustomer_Dc,{}))
		.addDc("template", Ext.create(atraxo.cmm.ui.extjs.dc.Template_Dc,{ readOnly:true}))
		.addDc("contractChange", Ext.create(atraxo.cmm.ui.extjs.dc.ContractChange_Dc,{ readOnly:true}))
		.addDc("attachment", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("pricingBase", Ext.create(atraxo.cmm.ui.extjs.dc.PricingBase_Dc,{}))
		.addDc("contractPriceCtgry", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceCategory_Dc,{}))
		.addDc("contractPriceCtgryAssign", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceCategoryAssign_Dc,{ _disableAutoSelection_:true}))
		.addDc("contractPriceCmpn", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceComponent_Dc,{}))
		.addDc("contractPriceCmpnClassic", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceComponentClassic_Dc,{}))
		.addDc("contractPriceRestr", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceRestriction_Dc,{multiEdit: true}))
		.addDc("contractPriceCmpnConv", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceComponentConv_Dc,{}))
		.addDc("shipTo", Ext.create(atraxo.cmm.ui.extjs.dc.ShipTo_Dc,{multiEdit: true}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("attachmentPop", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("newContract", Ext.create(atraxo.cmm.ui.extjs.dc.ContractCustomerWizard_Dc,{}))
		.linkDc("contractEdit", "contractList",{fields:[
					{childField:"id", parentField:"id"}]})
				.linkDc("contractChange", "contractEdit",{fetchMode:"auto",fields:[
					{childField:"contractId", parentField:"id"}]})
				.linkDc("attachment", "contractEdit",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}]})
				.linkDc("pricingBase", "contractEdit",{fetchMode:"auto",fields:[
					{childField:"contractId", parentField:"id"}, {childParam:"settlementCurrencyId", parentField:"settCurrId"}]})
				.linkDc("contractPriceCtgry", "contractEdit",{fetchMode:"auto",reloadChildrenOnParentUpdate:true,fields:[
					{childField:"contract", parentField:"id"}, {childParam:"settlementCurrencyId", parentField:"settCurrId"}, {childParam:"settlementCurrencyCode", parentField:"settCurrCode"}, {childParam:"settlementUnitCode", parentField:"settUnitCode"}, {childField:"bidApprovalStatus", parentField:"approvalStatus"}]})
				.linkDc("contractPriceCtgryAssign", "contractEdit",{fetchMode:"auto",fields:[
					{childField:"contract", parentField:"id"}, {childParam:"settlementCurrencyId", parentField:"settCurrId"}, {childParam:"settlementCurrencyCode", parentField:"settCurrCode"}, {childParam:"settlementUnitCode", parentField:"settUnitCode"}]})
				.linkDc("contractPriceCmpn", "contractPriceCtgry",{fetchMode:"auto",reloadChildrenOnParentUpdate:true,fields:[
					{childField:"contractPriceCat", parentField:"id"}, {childField:"priceCtgryName", parentField:"priceCtgryName"}, {childField:"priceCtgryPricePer", parentField:"priceCtgryPricePer", noFilter:true}]})
				.linkDc("contractPriceCmpnClassic", "contractPriceCtgry",{fetchMode:"auto",reloadChildrenOnParentUpdate:true,fields:[
					{childField:"contractPriceCat", parentField:"id"}, {childField:"priceCtgryName", parentField:"priceCtgryName"}, {childField:"priceCtgryPricePer", parentField:"priceCtgryPricePer", noFilter:true}]})
				.linkDc("contractPriceRestr", "contractPriceCtgry",{fetchMode:"auto",fields:[
					{childField:"contractPriceCtgry", parentField:"id"}, {childParam:"isEnabled", parentField:"restriction"}]})
				.linkDc("contractPriceCmpnConv", "contractPriceCtgry",{fetchMode:"auto",fields:[
					{childField:"contractPriceCtgry", parentField:"id"}]})
				.linkDc("shipTo", "contractEdit",{fetchMode:"auto",fields:[
					{childField:"contractId", parentField:"id"}, {childParam:"contractStatus", parentField:"status"}]})
				.linkDc("history", "contractEdit",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("attachmentPop", "contractEdit",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}, {childField:"categType", value:"upload"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"moveUpButton",glyph:fp_asc.up_glyph.glyph, disabled:false, handler: this.onMoveUpButton,stateManager:[{ name:"record_is_clean", dc:"contractPriceCtgry"}, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isNotReadOnly());} }], scope:this})
		.addButton({name:"moveDownButton",glyph:fp_asc.down_glyph.glyph, disabled:false, handler: this.onMoveDownButton,stateManager:[{ name:"record_is_clean", dc:"contractPriceCtgry"}, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isNotReadOnly());} }], scope:this})
		.addButton({name:"btnCopyInList",glyph:fp_asc.copy_glyph.glyph,iconCls: fp_asc.copy_glyph.css, disabled:true, handler: this.onBtnCopyInList,stateManager:[{ name:"selected_one_clean", dc:"contractList"}], scope:this})
		.addButton({name:"btnDeleteContract",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteContract,stateManager:[{ name:"selected_one_clean", dc:"contractList", and: function() {return (this._canDeleteContract_());} }], scope:this})
		.addButton({name:"btnNewContract",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, isBtnContainer:true,stateManager:[{ name:"no_record_or_record_is_clean", dc:"newContract"}], scope:this})
		.addButton({name:"btnProductIntoPlane", disabled:false,  _btnContainerName_:"btnNewContract", _isDefaultHandler_:true, handler: this.onBtnProductIntoPlane, scope:this})
		.addButton({name:"btnProductExHydrant", disabled:false,  _btnContainerName_:"btnNewContract", handler: this.onBtnProductExHydrant, scope:this})
		.addButton({name:"btnFuelingServices", disabled:false,  _btnContainerName_:"btnNewContract", handler: this.onBtnFuelingServices, scope:this})
		.addButton({name:"btnProductIntoStorage", disabled:false,  _btnContainerName_:"btnNewContract", handler: this.onBtnProductIntoStorage, scope:this})
		.addButton({name:"btnDiscardStep1",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false,  style:"margin-left:24px", handler: this.onBtnDiscardStep1, scope:this})
		.addButton({name:"btnContinueStep1",glyph:fp_asc.next_glyph.glyph,  disabled:true, handler: this.onBtnContinueStep1, scope:this})
		.addButton({name:"btnBackStep2",glyph:fp_asc.prev_glyph.glyph, disabled:false,  hidden:true, style:"margin-left:24px", handler: this.onBtnBackStep2, scope:this})
		.addButton({name:"btnDiscardStep2",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false,  hidden:true, handler: this.onBtnDiscardStep2, scope:this})
		.addButton({name:"btnContinueStep2",glyph:fp_asc.next_glyph.glyph,  hidden:true, disabled:true, handler: this.onBtnContinueStep2, scope:this})
		.addButton({name:"btnContinueStep3",glyph:fp_asc.next_glyph.glyph,  hidden:true, disabled:true, handler: this.onBtnContinueStep3, scope:this})
		.addButton({name:"btnDiscardStep3",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false,  hidden:true, handler: this.onBtnDiscardStep3, scope:this})
		.addButton({name:"btnBackStep3",glyph:fp_asc.prev_glyph.glyph, disabled:false,  hidden:true, style:"margin-left:24px", handler: this.onBtnBackStep3, scope:this})
		.addButton({name:"btnContinueStep4",glyph:fp_asc.next_glyph.glyph,  hidden:true, disabled:true, handler: this.onBtnContinueStep4, scope:this})
		.addButton({name:"btnDiscardStep4",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false,  hidden:true, handler: this.onBtnDiscardStep4, scope:this})
		.addButton({name:"btnBackStep4",glyph:fp_asc.prev_glyph.glyph, disabled:false,  hidden:true, style:"margin-left:24px", handler: this.onBtnBackStep4, scope:this})
		.addButton({name:"btnContinueStep5",glyph:fp_asc.save_glyph_black.glyph,  hidden:true, disabled:true, handler: this.onBtnContinueStep5, scope:this})
		.addButton({name:"btnDiscardStep5",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false,  hidden:true, handler: this.onBtnDiscardStep5, scope:this})
		.addButton({name:"btnBackStep5",glyph:fp_asc.prev_glyph.glyph, disabled:false,  hidden:true, style:"margin-left:24px", handler: this.onBtnBackStep5, scope:this})
		.addButton({name:"btnSetActiveList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSetActiveList,stateManager:[{ name:"selected_one", dc:"contractList", and: function() {return (this.canActivateContract('contractList'));} }], scope:this, visible: !this.useWorkflowForApproval() })
		.addButton({name:"btnSetActiveEdit",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSetActiveEdit,stateManager:[{ name:"selected_one_clean", dc:"contractEdit", and: function() {return (this.canActivateContract('contractEdit'));} }, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return ( (this.isDraft() || this.isOriginal()) && !this.useWorkflowForApproval());} }], scope:this})
		.addButton({name:"btnBackContractPriceComponentCustom",glyph:fp_asc.back_glyph.glyph,iconCls: fp_asc.back_glyph.css, disabled:false, handler: this.onBtnBackContractPriceComponentCustom, scope:this})
		.addButton({name:"btnBackFromPriceCategory",glyph:fp_asc.back_glyph.glyph,iconCls: fp_asc.back_glyph.css, disabled:false, handler: this.onBtnBackFromPriceCategory, scope:this})
		.addButton({name:"savePriceCtgry",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onSavePriceCtgry,stateManager:[{ name:"record_is_dirty", dc:"contractPriceCtgry"}], scope:this})
		.addButton({name:"btnUpdateStatus",glyph:fp_asc.commit_glyph.glyph,iconCls: fp_asc.commit_glyph.css, disabled:true, handler: this.onBtnUpdateStatus,stateManager:[{ name:"selected_one", dc:"contractList", and: function(dc) {return ((dc.record.data.status==='Active' || dc.record.data.status==='Effective' || dc.record.data.status==='Expired') && dc.record.data.readOnly===false);} }], scope:this})
		.addButton({name:"btnApprove",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApprove,stateManager:[{ name:"selected_one_clean", dc:"contractEdit", and: function(dc) {return (this.canApprove(dc) && dc.record.data.readOnly === false);} }, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.useWorkflowForApproval() && this.isDraft());} }], scope:this})
		.addButton({name:"btnReject",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnReject,stateManager:[{ name:"selected_one_clean", dc:"contractEdit", and: function(dc) {return (this.canApprove(dc) && dc.record.data.readOnly === false);} }, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.useWorkflowForApproval() && this.isDraft());} }], scope:this})
		.addButton({name:"btnSubmitForApproval",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitForApproval,stateManager:[{ name:"selected_one_clean", dc:"contractEdit", and: function(dc) {return (this.canSubmitForApproval(dc) && dc.record.data.readOnly === false);} }, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.useWorkflowForApproval() && (this.isDraft() || this.isBlueprint()));} }], scope:this})
		.addButton({name:"btnSaveCloseApprovalWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApprovalWdw, scope:this})
		.addButton({name:"btnCancelApprovalWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApprovalWdw, scope:this})
		.addButton({name:"btnSaveCloseApproveWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApproveWdw, scope:this})
		.addButton({name:"btnSaveCloseRejectWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRejectWdw, scope:this})
		.addButton({name:"btnCloseContractChangeWdw", disabled:false, handler: this.onBtnCloseContractChangeWdw, scope:this})
		.addButton({name:"btnCancelApproveWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApproveWdw, scope:this})
		.addButton({name:"btnCancelRejectWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRejectWdw, scope:this})
		.addButton({name:"btnResetStatus",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnResetStatus,stateManager:[{ name:"selected_one", dc:"contractList", and: function(dc) {return ((dc.record.data.status==='Active' || dc.record.data.status==='Effective' || dc.record.data.status==='Expired') && (dc.record.data.readOnly===false || Ext.isEmpty(dc.record.data.readOnly)) && !dc.record.data.blueprintOriginalContractReferenceId);} }], scope:this})
		.addButton({name:"btnResetStatusEdit",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnResetStatusEdit,stateManager:[{ name:"selected_one", dc:"contractEdit", and: function(dc) {return ((dc.record.data.status==='Active' || dc.record.data.status==='Effective' || dc.record.data.status==='Expired') && dc.record.data.readOnly===false && !dc.record.data.blueprintOriginalContractReferenceId);} }, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isOriginal());} }], scope:this})
		.addButton({name:"btnContinue",glyph:fp_asc.continue_glyph.glyph,iconCls: fp_asc.continue_glyph.css, disabled:true, handler: this.onBtnContinue,stateManager:[{ name:"selected_one_clean", dc:"template"}], scope:this})
		.addButton({name:"Cancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onCancel, scope:this})
		.addButton({name:"CancelPb",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onCancelPb, scope:this})
		.addButton({name:"CancelPbEditFormula",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onCancelPbEditFormula, scope:this})
		.addButton({name:"btnSaveClosePriceWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false,  style:"margin-left:5px", handler: this.onBtnSaveClosePriceWdw, scope:this})
		.addButton({name:"btnSaveClosePriceEditFormulaWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveClosePriceEditFormulaWdw, scope:this})
		.addButton({name:"btnSaveClosePriceCategoryWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveClosePriceCategoryWdw, scope:this})
		.addButton({name:"btnSaveNewPriceCategoryWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewPriceCategoryWdw, scope:this})
		.addButton({name:"btnSaveNewCompositePrice",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewCompositePrice, scope:this})
		.addButton({name:"btnSaveCloseCompositePrice",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseCompositePrice, scope:this})
		.addButton({name:"btnSaveEditCompositePrice",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveEditCompositePrice, scope:this})
		.addButton({name:"btnCancelCompositePrice",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelCompositePrice, scope:this})
		.addButton({name:"btnSaveEditPriceCategoryWdw",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnSaveEditPriceCategoryWdw, scope:this})
		.addButton({name:"btnClosePriceFixWdw",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onBtnClosePriceFixWdw, scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"WdwHelp1",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onWdwHelp1, scope:this})
		.addButton({name:"WdwHelp2",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onWdwHelp2, scope:this})
		.addButton({name:"btnEditContractPriceCtgry",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnEditContractPriceCtgry,stateManager:[{ name:"selected_one", dc:"contractPriceCtgry", and: function(dc) {return (dc.record.data.readOnly===false);} }, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return ((this.isDraft() || this.isBlueprint()) && this.isNotReadOnly());} }], scope:this})
		.addButton({name:"btnViewDefinitionContractPriceCtgry",glyph:fp_asc.eye_glyph.glyph, disabled:true, handler: this.onBtnViewDefinitionContractPriceCtgry,stateManager:[{ name:"selected_one", dc:"contractPriceCtgry"}, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isOriginal() || !this.isNotReadOnly());} }], scope:this})
		.addButton({name:"btnViewFormulaPrice",glyph:fp_asc.eye_glyph.glyph, disabled:true, handler: this.onBtnViewFormulaPrice,stateManager:[{ name:"selected_one", dc:"contractPriceCtgry", and: function() {return (this._checkIfFormulaPrice_());} }, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isOriginal() || !this.isNotReadOnly());} }], scope:this})
		.addButton({name:"btnDeleteContractPriceCtgry",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteContractPriceCtgry,stateManager:[{ name:"selected_one", dc:"contractPriceCtgry", and: function(dc) {return (this._checkPricingBaseId_() && dc.record.data.readOnly === false && this._checkIfContractAwaitingApproval_());} }, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return ((this.isDraft() || this.isBlueprint()) && this.isNotReadOnly());} }], scope:this})
		.addButton({name:"btnDeleteContractPriceComponent",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteContractPriceComponent,stateManager:[{ name:"selected_one", dc:"contractPriceCmpn", and: function() {return (( this.isDraft() || this.isBlueprint() ) && this._checkPriceCtgryName_() && this._checkIfContractAwaitingApproval_() && this._checkContractReadOnly_());} }], scope:this})
		.addButton({name:"btnNewContractPriceComponent",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewContractPriceComponent,stateManager:[{ name:"no_record_or_record_is_clean", dc:"contractPriceCmpn", and: function() {return (( this.isDraft() || this.isBlueprint() ) && this._checkPriceCtgryPricePer_() && this._checkIfContractAwaitingApproval_() && this._checkContractReadOnly_() && this._checkPercent_());} }], scope:this})
		.addButton({name:"btnEditPriceComponent",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnEditPriceComponent,stateManager:[{ name:"selected_one", dc:"contractPriceCmpn", and: function() {return (( this.isDraft() || this.isBlueprint() ) && this._checkIfContractAwaitingApproval_() && this._checkContractReadOnly_());} }], scope:this})
		.addButton({name:"btnEditPriceComponentBlueprint",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnEditPriceComponentBlueprint,stateManager:[{ name:"selected_one", dc:"contractPriceCmpn", and: function() {return (this._checkContractReadOnly_());} }], scope:this})
		.addButton({name:"btnDeleteContractPricingBase",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteContractPricingBase,stateManager:[{ name:"selected_one", dc:"pricingBase", and: function(dc) {return (dc.store.totalCount!==1 );} }], scope:this})
		.addButton({name:"btnUpdatePrice",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnUpdatePrice,stateManager:[{ name:"selected_one", dc:"contractPriceCtgry"}, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return ((this.isDraft() || this.isBlueprint()) && this.isNotReadOnly());} }], scope:this})
		.addButton({name:"btnReviewPrice",glyph:fp_asc.eye_glyph.glyph, disabled:true, handler: this.onBtnReviewPrice,stateManager:[{ name:"selected_one", dc:"contractPriceCtgry"}, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isOriginal() || !this.isNotReadOnly());} }], scope:this})
		.addButton({name:"btnEditFormulaPrice",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnEditFormulaPrice,stateManager:[{ name:"selected_one", dc:"contractPriceCtgry", and: function() {return (this._checkIfFormulaPrice_());} }, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return ((this.isDraft() || this.isBlueprint()) && this.isNotReadOnly());} }], scope:this})
		.addButton({name:"btnPriceCompSave",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnPriceCompSave, scope:this})
		.addButton({name:"btnPriceCompCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnPriceCompCancel, scope:this})
		.addButton({name:"btnNewWithMenu",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, isBtnContainer:true,stateManager:[{ name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return ((this.isDraft() || this.isBlueprint()) && this.isNotReadOnly());} }], scope:this})
		.addButton({name:"btnNewFormulaPrice",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:true,  _btnContainerName_:"btnNewWithMenu", _isDefaultHandler_:true, handler: this.onBtnNewFormulaPrice,stateManager:[{ name:"selected_one", dc:"contractEdit", and: function() {return (this._enableIfConditions_());} }], scope:this})
		.addButton({name:"btnNewFixPrice",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:true,  _btnContainerName_:"btnNewWithMenu", handler: this.onBtnNewFixPrice,stateManager:[{ name:"selected_one", dc:"contractEdit", and: function() {return (this._enableIfConditions_());} }], scope:this})
		.addButton({name:"btnNewDutyFeeTax",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:true,  _btnContainerName_:"btnNewWithMenu", handler: this.onBtnNewDutyFeeTax,stateManager:[{ name:"selected_one", dc:"contractEdit", and: function() {return (this._enableIfConditions_());} }], scope:this})
		.addButton({name:"btnNewCompositePrice",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:true,  _btnContainerName_:"btnNewWithMenu", handler: this.onBtnNewCompositePrice,stateManager:[{ name:"selected_one", dc:"contractEdit", and: function() {return (this._enableIfConditions_());} }], scope:this})
		.addButton({name:"btnContinueNewSalesContract",glyph:fp_asc.rollforward_glyph.glyph,iconCls: fp_asc.rollforward_glyph.css,  disabled:true, handler: this.onBtnContinueNewSalesContract, scope:this})
		.addButton({name:"btnContinueCopySalesContract",glyph:fp_asc.rollforward_glyph.glyph,iconCls: fp_asc.rollforward_glyph.css,  disabled:true, handler: this.onBtnContinueCopySalesContract, scope:this})
		.addButton({name:"btnCancelNewSalesContract",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelNewSalesContract, scope:this})
		.addButton({name:"btnCancelCopySalesContract",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelCopySalesContract, scope:this})
		.addButton({name:"btnDiscardBlueprint",glyph:fp_asc.thrash_glyph.glyph, disabled:true, handler: this.onBtnDiscardBlueprint,stateManager:[{ name:"selected_one_clean", dc:"contractEdit"}, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isBlueprint());} }], scope:this})
		.addButton({name:"btnPublishBlueprint",glyph:fp_asc.publish_glyph.glyph, disabled:true, handler: this.onBtnPublishBlueprint,stateManager:[{ name:"selected_one_clean", dc:"contractEdit", and: function() {return (this.canBePublished());} }, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isBlueprint());} }], scope:this})
		.addButton({name:"btnCancelWithMenu",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"shipTo", and: function(dc) {return (this.fnCancelEnabled(dc));} }, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isBlueprint() || this.isDraft());} }], scope:this})
		.addButton({name:"btnCancelSelected",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", handler: this.onBtnCancelSelected, scope:this})
		.addButton({name:"btnCancelAll",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", _isDefaultHandler_:true, handler: this.onBtnCancelAll, scope:this})
		.addButton({name:"btnSaveCloseRemarkWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRemarkWdw, scope:this})
		.addButton({name:"btnSaveCloseResetRemarkWdwList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseResetRemarkWdwList, scope:this})
		.addButton({name:"btnSaveCloseResetRemarkWdwEdit",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseResetRemarkWdwEdit, scope:this})
		.addButton({name:"btnCancelResetRemarkWdwEdit",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelResetRemarkWdwEdit, scope:this})
		.addButton({name:"btnCancelResetRemarkWdwList",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelResetRemarkWdwList, scope:this})
		.addButton({name:"btnCancelRemarkWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRemarkWdw, scope:this})
		.addButton({name:"btnAssignPriceComponents",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnAssignPriceComponents, scope:this})
		.addButton({name:"btnCancelAssignPriceComponents",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelAssignPriceComponents, scope:this})
		.addButton({name:"btnNewRestriction",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewRestriction,stateManager:[{ name:"no_record_or_record_is_clean", dc:"contractPriceRestr", and: function() {return (this._applyRestrictionState_());} }], scope:this})
		.addButton({name:"btnSaveRestriction",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSaveRestriction,stateManager:[{ name:"selected_one_dirty", dc:"contractPriceRestr", and: function() {return (this._applyRestrictionState_());} }], scope:this})
		.addButton({name:"btnCancelRestriction",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnCancelRestriction,stateManager:[{ name:"selected_one_dirty", dc:"contractPriceRestr", and: function() {return (this._applyRestrictionState_());} }], scope:this})
		.addButton({name:"btnDeleteRestriction",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteRestriction,stateManager:[{ name:"selected_one_clean", dc:"contractPriceRestr", and: function() {return (this._applyRestrictionState_());} }], scope:this})
		.addButton({name:"btnDeleteShipTo",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onBtnDeleteShipTo,stateManager:[{ name:"no_record_or_record_is_clean", dc:"shipTo", and: function(dc) {return (this._enableIfConditions_() && (dc.readOnly === false || dc.canDoNew === true));} }, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isBlueprint() || this.isDraft());} }], scope:this})
		.addButton({name:"btnNewShipTo",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewShipTo,stateManager:[{ name:"no_record_or_record_is_clean", dc:"shipTo", and: function(dc) {return (this._enableIfConditions_() && (dc.readOnly === false || dc.canDoNew === true) );} }, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isBlueprint() || this.isDraft());} }], scope:this})
		.addButton({name:"btnSaveShipTo",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSaveShipTo,stateManager:[{ name:"selected_one_dirty", dc:"shipTo"}, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isBlueprint() || this.isDraft());} }], scope:this})
		.addButton({name:"btnViewOriginalBlueprint",glyph:fp_asc.eye_glyph.glyph, disabled:true, handler: this.onBtnViewOriginalBlueprint,stateManager:[{ name:"selected_one_clean", dc:"contractEdit", and: function(dc) {return (dc.record.data.isBlueprint);} }, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isBlueprint());} }], scope:this})
		.addButton({name:"btnEditDraftContract",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnEditDraftContract,stateManager:[{ name:"selected_one_clean", dc:"contractEdit", and: function(dc) {return (dc.record.data.readOnly===false);} }, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isOriginal());} }], scope:this})
		.addButton({name:"btnUpdateStatusViewMode",glyph:fp_asc.commit_glyph.glyph,iconCls: fp_asc.commit_glyph.css, disabled:true, handler: this.onBtnUpdateStatusViewMode,stateManager:[{ name:"selected_one", dc:"contractEdit", and: function(dc) {return ((dc.record.data.status==='Active' || dc.record.data.status==='Effective' || dc.record.data.status==='Expired') && dc.record.data.readOnly===false);} }, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isOriginal());} }], scope:this})
		.addButton({name:"btnViewContractList",glyph:fp_asc.eye_glyph.glyph, disabled:true, handler: this.onBtnViewContractList,stateManager:[{ name:"selected_one_clean", dc:"contractList", and: function(dc) {return (!(dc.record.data.status === __CMM_TYPE__.ContractStatus._DRAFT_) || dc.record.data.readOnly===true);} }], scope:this})
		.addButton({name:"btnEditContractList",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnEditContractList,stateManager:[{ name:"selected_one_clean", dc:"contractList", and: function(dc) {return (dc.record.data.readOnly===false);} }], scope:this})
		.addButton({name:"btnCustomSave",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnCustomSave,stateManager:[{ name:"record_is_dirty", dc:"contractEdit"}, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isDraft() || this.isBlueprint());} }], scope:this})
		.addButton({name:"btnCustomCancel",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnCustomCancel,stateManager:[{ name:"record_is_dirty", dc:"contractEdit"}, { name:"dc_in_any_state", visibleRule: true, dc:"contractEdit", and: function() {return (this.isDraft() || this.isBlueprint());} }], scope:this})
		.addDcGridView("contractList", {name:"SalesContract", xtype:"cmm_ContractCustomer_Dc$TabList",  viewConfig:{getRowClass: function (record) {
		                return !Ext.isEmpty(record.get('blueprintOriginalContractReferenceId')) ? 'sone-row-pending-approval' : ''; }}, listeners:{itemdblclick: {scope: this, fn:function(el, record, item) {this._showEdit_(el, record, item)}}}})
		.addDcFilterFormView("contractList", {name:"SalesContractFilter", xtype:"cmm_ContractCustomer_Dc$Filter"})
		.addDcFormView("contractEdit", {name:"Header", xtype:"cmm_ContractCustomer_Dc$Header", _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcGridView("template", {name:"TempList", xtype:"cmm_Template_Dc$List",  split:false})
		.addDcGridView("template", {name:"TempListWdw", xtype:"cmm_Template_Dc$List",  split:false})
		.addDcFilterFormView("pricingBase", {name:"PbFilter", xtype:"cmm_PricingBase_Dc$Filter"})
		.addDcGridView("contractChange", {name:"ContractChangeList", xtype:"cmm_ContractChange_Dc$List"})
		.addDcFormView("contractEdit", {name:"PriceAlert", height:45, xtype:"cmm_ContractCustomer_Dc$PriceAlert",  split:false, cls:"sone-alert-panel-container", hidden:true})
		.addDcFormView("contractEdit", {name:"ShipToAlert", height:45, xtype:"cmm_ContractCustomer_Dc$ShipToAlert",  split:false, cls:"sone-alert-panel-container", hidden:true})
		.addDcFormView("contractEdit", {name:"PeriodAlert", height:45, xtype:"cmm_ContractCustomer_Dc$PeriodAlert",  split:false, cls:"sone-alert-panel-container", hidden:true})
		.addDcFormView("pricingBase", {name:"PbEdit", xtype:"cmm_PricingBase_Dc$NewFormulaPrice"})
		.addDcFormView("pricingBase", {name:"PbEditFormula", xtype:"cmm_PricingBase_Dc$EditFormulaPrice"})
		.addDcFormView("pricingBase", {name:"priceFixNew", xtype:"cmm_PricingBase_Dc$NewPCSellContract"})
		.addDcFormView("pricingBase", {name:"compositePriceNew", xtype:"cmm_PricingBase_Dc$NewCompositePrice"})
		.addDcGridView("attachment", {name:"attachmentList", _hasTitle_:true, xtype:"ad_Attachment_Dc$List"})
		.addDcGridView("attachment", {name:"attachmentListBlueprint", _hasTitle_:true, xtype:"ad_Attachment_Dc$List"})
		.addDcFormView("attachment", {name:"attachmentEdit", xtype:"ad_Attachment_Dc$New"})
		.addDcFilterFormView("contractPriceCtgry", {name:"priceCtgryFilter", xtype:"cmm_ContractPriceCategory_Dc$Filter"})
		.addDcGridView("contractPriceCtgry", {name:"priceCtgryList", xtype:"cmm_ContractPriceCategory_Dc$List",  listeners:{itemdblclick: {scope: this, fn:function(el, record, item) {this._showPriceEdit_(el, record, item)}}}})
		.addDcGridView("contractPriceCtgry", {name:"priceCtgryListBlueprint", xtype:"cmm_ContractPriceCategory_Dc$List"})
		.addDcGridView("contractPriceCtgryAssign", {name:"priceCtgryAssignList", xtype:"cmm_ContractPriceCategoryAssign_Dc$AssignList",  listeners:{ afterrender: { scope: this, fn: function(list) { this._excludeComposite_(list)}}}})
		.addDcFormView("contractPriceCtgry", {name:"priceCtgryEditPopUp", xtype:"cmm_ContractPriceCategory_Dc$NewGrid"})
		.addDcFormView("contractPriceCtgry", {name:"priceCtgryEdit", xtype:"cmm_ContractPriceCategory_Dc$EditGrid"})
		.addDcFilterFormView("contractPriceCmpn", {name:"priceCompFilter", xtype:"cmm_ContractPriceComponent_Dc$Filter"})
		.addDcGridView("contractPriceCmpnClassic", {name:"priceCompList", xtype:"cmm_ContractPriceComponentClassic_Dc$SimpleList",  split:false, listeners:{ rowdblclick: { scope: this, fn: function(list) { this._setPriceCmpReadOnlyOrNot_()}}}})
		.addDcGridView("contractPriceCmpn", {name:"priceCompListBlueprint", xtype:"cmm_ContractPriceComponent_Dc$SimpleList",  split:false})
		.addDcFormView("contractPriceCmpnClassic", {name:"priceCompEdit", xtype:"cmm_ContractPriceComponentClassic_Dc$Edit"})
		.addDcEditGridView("contractPriceRestr", {name:"priceRestr", xtype:"cmm_ContractPriceRestriction_Dc$List", frame:true,  listeners:{ beforeedit: { scope: this, fn: function(editor, ctx) { this._setPriceRestrictionEditorType_(editor, ctx, this._get_('priceRestr'))}}}})
		.addPanel({name:"priceRestrWrapper", layout:"fit"})
		.addDcFormView("contractPriceCtgry", {name:"UpdatePriceHeader", xtype:"cmm_ContractPriceCategory_Dc$FormUpdatePrice",  cls:"form-large-space", split:false})
		.addDcFormView("contractPriceCtgry", {name:"UpdatePriceHeaderBlueprint", xtype:"cmm_ContractPriceCategory_Dc$FormUpdatePrice",  cls:"form-large-space", split:false})
		.addDcFormView("contractEdit", {name:"paymentTerms", _hasTitle_:true, xtype:"cmm_ContractCustomer_Dc$PaymentTerms", _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcFormView("contractEdit", {name:"volumes", _hasTitle_:true, xtype:"cmm_ContractCustomer_Dc$Volumes"})
		.addDcFilterFormView("contractPriceCmpnConv", {name:"priceComponentConvFilter", xtype:"cmm_ContractPriceComponentConv_Dc$Filter"})
		.addDcGridView("contractPriceCmpnConv", {name:"priceComponentConv", _hasTitle_:true, height:300, xtype:"cmm_ContractPriceComponentConv_Dc$List"})
		.addDcGridView("contractPriceCmpnConv", {name:"priceComponentConvBlueprint", _hasTitle_:true, height:300, xtype:"cmm_ContractPriceComponentConv_Dc$List"})
		.addDcFormView("contractPriceCtgry", {name:"totals", xtype:"cmm_ContractPriceCategory_Dc$Totals",  split:false, cls:"sone-total-panel", listeners:{afterrender: {scope: this, fn: function(view) {var grid = this._get_('priceCtgryList'); grid._totalPanelId_ = view.getId(); }}}})
		.addDcFormView("contractPriceCtgry", {name:"totalsBlueprint", xtype:"cmm_ContractPriceCategory_Dc$Totals",  split:false, cls:"sone-total-panel", listeners:{afterrender: {scope: this, fn: function(view) {var grid = this._get_('priceCtgryList'); grid._totalPanelId_ = view.getId(); }}}})
		.addDcFormView("contractList", {name:"selectCustomer", height:80, xtype:"cmm_ContractCustomer_Dc$selectCustomer",  split:false})
		.addDcFormView("contractList", {name:"copyCustomer", xtype:"cmm_ContractCustomer_Dc$copyCustomer",  split:false})
		.addDcFilterFormView("shipTo", {name:"ShipToFilter", xtype:"cmm_ShipTo_Dc$Filter"})
		.addDcEditGridView("shipTo", {name:"ShipToEditList", _hasTitle_:true, xtype:"cmm_ShipTo_Dc$EditList", frame:true, _summaryDefined_:true})
		.addDcEditGridView("shipTo", {name:"ShipToEditListBlueprint", _hasTitle_:true, xtype:"cmm_ShipTo_Dc$EditList", frame:true, _summaryDefined_:true,  viewConfig:{getRowClass: function (record) {
		                return _SYSTEMPARAMETERS_.syssalescontractapproval === 'true' && 
						!Ext.isEmpty(record.get('changes')) ? 'sone-row-pending-approval' : ''; }}})
		.addDcGridView("history", {name:"History", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcGridView("history", {name:"HistoryBlueprint", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("shipTo", {name:"remark", xtype:"cmm_ShipTo_Dc$EditRemark"})
		.addDcFormView("contractList", {name:"remarkResetList", xtype:"cmm_ContractCustomer_Dc$Remark"})
		.addDcFormView("contractEdit", {name:"remarkResetEdit", xtype:"cmm_ContractCustomer_Dc$Remark"})
		.addDcFormView("contractEdit", {name:"approvalNote", xtype:"cmm_ContractCustomer_Dc$ApprovalNote",  split:false})
		.addDcFormView("contractEdit", {name:"approveNote", xtype:"cmm_ContractCustomer_Dc$ApprovalNote"})
		.addDcFormView("contractEdit", {name:"rejectNote", xtype:"cmm_ContractCustomer_Dc$ApprovalNote"})
		.addDcFormView("newContract", {name:"wizardHeader", height:130, xtype:"cmm_ContractCustomerWizard_Dc$wizardHeader"})
		.addDcFormView("newContract", {name:"newContractStep1", xtype:"cmm_ContractCustomerWizard_Dc$NewContractStep1",  _firstFocusItemName_:"customerName"})
		.addDcFormView("newContract", {name:"newContractStep2", xtype:"cmm_ContractCustomerWizard_Dc$NewContractStep2"})
		.addDcFormView("newContract", {name:"newContractStep3", xtype:"cmm_ContractCustomerWizard_Dc$NewContractStep3"})
		.addDcFormView("newContract", {name:"newContractStep4", xtype:"cmm_ContractCustomerWizard_Dc$NewContractStep4"})
		.addDcFormView("newContract", {name:"newContractStep5", xtype:"cmm_ContractCustomerWizard_Dc$NewContractStep5"})
		.addDcGridView("attachmentPop", {name:"documentsAssignList", _hasTitle_:true, height:300, xtype:"ad_Attachment_Dc$attachmentAssignList",  cls:"sone-custom-panel-title"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas4", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas4Blueprint", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"newSalesContract", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"panelAssignAttachments", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"prices", _hasTitle_:true})
		.addPanel({name:"pricesBlueprint", _hasTitle_:true})
		.addWindow({name:"wdwRemarks", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("remark")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRemarkWdw"), this._elems_.get("btnCancelRemarkWdw")]}]})
		.addWindow({name:"wdwNewSalesContract", _hasTitle_:true, width:420, height:360, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("newSalesContract")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnContinueNewSalesContract"), this._elems_.get("btnCancelNewSalesContract")]}]})
		.addWindow({name:"wdwCopySalesContract", _hasTitle_:true, width:260, height:170, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("copyCustomer")],  listeners:{ show:{fn:function(win) { win.down('form')._isVisible_ = true;}, scope:this}, close:{fn:function(win) {win.down('form')._isVisible_ = false;}, scope:this} }, closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnContinueCopySalesContract"), this._elems_.get("btnCancelCopySalesContract")]}]})
		.addWindow({name:"wdwPricing", _hasTitle_:true, width:600, height:340, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("PbEdit")],  listeners:{ show:{fn:function(win) { win.down('form')._isVisible_ = true;}, scope:this}, close:{fn:function(win) {this._onWdwwClose_(win);}, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveClosePriceWdw"), this._elems_.get("CancelPb")]}]})
		.addWindow({name:"wdwPricingEditFormula", _hasTitle_:true, width:600, height:340, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("PbEditFormula")],  listeners:{ show:{fn:function(win) {win.down('form')._isVisible_ = true;}, scope:this}, close:{fn:function(win) {this._onWdwwClose_(win);}, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveClosePriceEditFormulaWdw"), this._elems_.get("CancelPbEditFormula")]}]})
		.addWindow({name:"wdwTemp", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("TempListWdw")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnContinue"), this._elems_.get("Cancel")]}]})
		.addWindow({name:"priceFixWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("priceFixNew")],  listeners:{ show:{fn:function(win) {win.down('form')._isVisible_ = true;}, scope:this}, close:{fn:function(win) {this._onWdwwClose_(win);}, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewPriceCategoryWdw"), this._elems_.get("btnSaveClosePriceCategoryWdw"), this._elems_.get("btnSaveEditPriceCategoryWdw"), this._elems_.get("btnClosePriceFixWdw")]}]})
		.addWindow({name:"compositePriceWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("compositePriceNew")],  listeners:{ show:{fn:function(win) {win.down('form')._isVisible_ = true;}, scope:this}, close:{fn:function(win) {this._onWdwwClose_(win);}, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewCompositePrice"), this._elems_.get("btnSaveCloseCompositePrice"), this._elems_.get("btnSaveEditCompositePrice"), this._elems_.get("btnCancelCompositePrice")]}]})
		.addWindow({name:"priceCategoryAsgnWdw", _hasTitle_:true, width:800, height:400, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("priceCtgryAssignList")],  listeners:{ show:{fn:function() {this._setMainPriceCategory_(); this._getDc_('contractPriceCtgryAssign').doQuery({markSelected: true})}, scope:this}, close: {fn: function() {this._get_('priceCtgryAssignList').getSelectionModel().deselectAll(); this._clearMainPriceCategory_()}, scope: this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnAssignPriceComponents"), this._elems_.get("btnCancelAssignPriceComponents")]}]})
		.addWindow({name:"priceCompWdw", width:400, height:200, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("priceCompEdit")],  listeners:{ close:{fn:this.onPriceCompWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnPriceCompSave"), this._elems_.get("btnPriceCompCancel")]}]})
		.addWindow({name:"wdwRemarkResetList", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("remarkResetList")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseResetRemarkWdwList"), this._elems_.get("btnCancelResetRemarkWdwList")]}]})
		.addWindow({name:"wdwRemarkResetEdit", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("remarkResetEdit")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseResetRemarkWdwEdit"), this._elems_.get("btnCancelResetRemarkWdwEdit")]}]})
		.addWindow({name:"wdwApprovalNote", _hasTitle_:true, width:600, height:530, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("panelAssignAttachments")],  closable:true, listeners:{ show:{fn:this.onApprovalWdwShow, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseApprovalWdw"), this._elems_.get("btnCancelApprovalWdw")]}]})
		.addWindow({name:"wdwApproveNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("approveNote")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseApproveWdw"), this._elems_.get("btnCancelApproveWdw")]}]})
		.addWindow({name:"wdwRejectNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("rejectNote")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRejectWdw"), this._elems_.get("btnCancelRejectWdw")]}]})
		.addWindow({name:"wdwContractChange", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("ContractChangeList")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCloseContractChangeWdw")]}]})
		.addWizardWindow({name:"wdwNewContract", _hasTitle_:true, width:700, height:550,_labelPanelName_:"wizardHeader"
			,_stepPanels_:["newContractStep1","newContractStep2","newContractStep3","newContractStep4","newContractStep5"]
			,  closable:true, listeners:{close:{fn:function(win) {this._onNewContractWdwdClose_();}, scope:this}}, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnDiscardStep1"), this._elems_.get("btnContinueStep1"), this._elems_.get("btnBackStep2"), this._elems_.get("btnDiscardStep2"), this._elems_.get("btnContinueStep2"), this._elems_.get("btnBackStep3"), this._elems_.get("btnDiscardStep3"), this._elems_.get("btnContinueStep3"), this._elems_.get("btnBackStep4"), this._elems_.get("btnDiscardStep4"), this._elems_.get("btnContinueStep4"), this._elems_.get("btnBackStep5"), this._elems_.get("btnDiscardStep5"), this._elems_.get("btnContinueStep5")]}]})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas2", dcName:"contractEdit"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("priceRestrWrapper", ["priceRestr"])
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["SalesContract"], ["center"])
		.addChildrenTo("canvas2", ["PriceAlert", "PeriodAlert", "ShipToAlert", "Header", "detailsTab"])
		.addChildrenTo("canvas3", ["priceCtgryEdit", "priceRestrWrapper"], ["north", "center"])
		.addChildrenTo("canvas4", ["UpdatePriceHeader", "priceCompList", "priceComponentConv"], ["north", "center", "south"])
		.addChildrenTo("canvas4Blueprint", ["UpdatePriceHeaderBlueprint", "priceCompListBlueprint", "priceComponentConvBlueprint"], ["north", "center", "south"])
		.addChildrenTo("newSalesContract", ["selectCustomer", "TempList"], ["north", "center"])
		.addChildrenTo("detailsTab", ["prices"])
		.addChildrenTo("panelAssignAttachments", ["approvalNote", "documentsAssignList"], ["north", "center"])
		.addChildrenTo("prices", ["priceCtgryList", "totals"])
		.addChildrenTo("pricesBlueprint", ["priceCtgryListBlueprint", "totalsBlueprint"])
		.addToolbarTo("priceCtgryList", "tlbPriceCategoryList")
		.addToolbarTo("UpdatePriceHeader", "tlbPriceComponentList")
		.addToolbarTo("canvas2", "tlbHeader")
		.addToolbarTo("priceCtgryEdit", "tlbPriceCategoryDetails")
		.addToolbarTo("attachmentList", "tlbAttachments")
		.addToolbarTo("priceRestrWrapper", "tlbPriceCategoryRestr")
		.addToolbarTo("ShipToEditList", "tlbShipTo")
		.addToolbarTo("SalesContract", "tblSalesContractsList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2", "canvas3", "canvas4", "canvas4Blueprint"])
		.addChildrenTo2("detailsTab", ["paymentTerms", "volumes", "ShipToEditList", "attachmentList", "History"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbPriceCategoryList", {dc: "contractPriceCtgry"})
			.addButtons([this._elems_.get("btnNewWithMenu") ])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewFormulaPrice"),this._elems_.get("btnNewFixPrice"),this._elems_.get("btnNewDutyFeeTax"),this._elems_.get("btnNewCompositePrice"),this._elems_.get("btnEditContractPriceCtgry"),this._elems_.get("btnDeleteContractPriceCtgry"),this._elems_.get("btnEditFormulaPrice"),this._elems_.get("btnUpdatePrice"),this._elems_.get("btnViewDefinitionContractPriceCtgry"),this._elems_.get("btnViewFormulaPrice"),this._elems_.get("btnReviewPrice"),this._elems_.get("moveUpButton"),this._elems_.get("moveDownButton")])
			.addReports()
		.end()
		.beginToolbar("tlbPriceComponentList", {dc: "contractPriceCmpnClassic"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnBackFromPriceCategory"),this._elems_.get("btnNewContractPriceComponent"),this._elems_.get("btnEditPriceComponent"),this._elems_.get("btnDeleteContractPriceComponent")])
			.addReports()
		.end()
		.beginToolbar("tlbHeader", {dc: "contractEdit"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas1"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCustomSave"),this._elems_.get("btnCustomCancel"),this._elems_.get("btnEditDraftContract"),this._elems_.get("btnDiscardBlueprint"),this._elems_.get("btnViewOriginalBlueprint"),this._elems_.get("btnPublishBlueprint"),this._elems_.get("btnUpdateStatusViewMode"),this._elems_.get("btnSubmitForApproval"),this._elems_.get("btnApprove"),this._elems_.get("btnReject"),this._elems_.get("btnSetActiveEdit"),this._elems_.get("btnResetStatusEdit"),this._elems_.get("WdwHelp1")])
			.addReports()
		.end()
		.beginToolbar("tlbPriceCategoryDetails", {dc: "contractPriceCtgry"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas2"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("savePriceCtgry")])
			.addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addPrevRec({glyph:fp_asc.prev_glyph.glyph})
			.addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbAttachments", {dc: "attachment"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbPriceCategoryRestr", {dc: "contractPriceRestr"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewRestriction"),this._elems_.get("btnSaveRestriction"),this._elems_.get("btnCancelRestriction"),this._elems_.get("btnDeleteRestriction")])
			.addReports()
		.end()
		.beginToolbar("tlbShipTo", {dc: "shipTo"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewShipTo"),this._elems_.get("btnSaveShipTo"),this._elems_.get("btnDeleteShipTo"),this._elems_.get("btnCancelWithMenu"),this._elems_.get("btnCancelSelected"),this._elems_.get("btnCancelAll"),this._elems_.get("btnDeleteShipTo")])
			.addReports()
		.end()
		.beginToolbar("tblSalesContractsList", {dc: "contractList"})
			.addButtons([this._elems_.get("btnNewContract") ])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnViewContractList"),this._elems_.get("btnEditContractList"),this._elems_.get("btnCopyInList"),this._elems_.get("btnProductIntoPlane"),this._elems_.get("btnProductExHydrant"),this._elems_.get("btnFuelingServices"),this._elems_.get("btnProductIntoStorage"),this._elems_.get("btnDeleteContract"),this._elems_.get("btnCopyInList"),this._elems_.get("btnSetActiveList"),this._elems_.get("btnUpdateStatus"),this._elems_.get("btnResetStatus"),this._elems_.get("WdwHelp2")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button moveUpButton
	 */
	,onMoveUpButton: function() {
		var successFn = function() {
			this._getDc_("contractPriceCtgry").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"moveUp",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("contractPriceCtgry").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button moveDownButton
	 */
	,onMoveDownButton: function() {
		var successFn = function() {
			this._getDc_("contractPriceCtgry").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"moveDown",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("contractPriceCtgry").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCopyInList
	 */
	,onBtnCopyInList: function() {
		this._getWindow_("wdwCopySalesContract").show();
	}
	
	/**
	 * On-Click handler for button btnDeleteContract
	 */
	,onBtnDeleteContract: function() {
		this._getDc_("contractList").doDelete();
	}
	
	/**
	 * On-Click handler for button btnProductIntoPlane
	 */
	,onBtnProductIntoPlane: function() {
		this._createNewContract_(__CMM_TYPE__.ContractType._PRODUCT_,__CMM_TYPE__.ContractSubType._INTO_PLANE_,__CMM_TYPE__.ContractScope._INTO_PLANE_)
	}
	
	/**
	 * On-Click handler for button btnProductExHydrant
	 */
	,onBtnProductExHydrant: function() {
		this._createNewContract_(__CMM_TYPE__.ContractType._PRODUCT_,__CMM_TYPE__.ContractSubType._EX_HYDRANT_,__CMM_TYPE__.ContractScope._INTO_PLANE_)
	}
	
	/**
	 * On-Click handler for button btnFuelingServices
	 */
	,onBtnFuelingServices: function() {
		this._createNewContract_(__CMM_TYPE__.ContractType._FUELING_SERVICE_,__CMM_TYPE__.ContractSubType._NA_,__CMM_TYPE__.ContractScope._INTO_PLANE_)
	}
	
	/**
	 * On-Click handler for button btnProductIntoStorage
	 */
	,onBtnProductIntoStorage: function() {
		this._createNewContract_(__CMM_TYPE__.ContractType._PRODUCT_,__CMM_TYPE__.ContractSubType._INTO_STORAGE_,__CMM_TYPE__.ContractScope._INTO_PLANE_)
	}
	
	/**
	 * On-Click handler for button btnDiscardStep1
	 */
	,onBtnDiscardStep1: function() {
		this._cancelCapture_();
	}
	
	/**
	 * On-Click handler for button btnContinueStep1
	 */
	,onBtnContinueStep1: function() {
		this._goToStep2BtnHandler_();
	}
	
	/**
	 * On-Click handler for button btnBackStep2
	 */
	,onBtnBackStep2: function() {
		this._backToStep1_();
	}
	
	/**
	 * On-Click handler for button btnDiscardStep2
	 */
	,onBtnDiscardStep2: function() {
		this._cancelCapture_();
	}
	
	/**
	 * On-Click handler for button btnContinueStep2
	 */
	,onBtnContinueStep2: function() {
		this._goToStep3BtnHandler_();
	}
	
	/**
	 * On-Click handler for button btnContinueStep3
	 */
	,onBtnContinueStep3: function() {
		this._goToStep4BtnHandler_();
	}
	
	/**
	 * On-Click handler for button btnDiscardStep3
	 */
	,onBtnDiscardStep3: function() {
		this._cancelCapture_();
	}
	
	/**
	 * On-Click handler for button btnBackStep3
	 */
	,onBtnBackStep3: function() {
		this._backToStep2_();
	}
	
	/**
	 * On-Click handler for button btnContinueStep4
	 */
	,onBtnContinueStep4: function() {
		this._goToStep5BtnHandler_();
	}
	
	/**
	 * On-Click handler for button btnDiscardStep4
	 */
	,onBtnDiscardStep4: function() {
		this._cancelCapture_();
	}
	
	/**
	 * On-Click handler for button btnBackStep4
	 */
	,onBtnBackStep4: function() {
		this._backToStep3_();
	}
	
	/**
	 * On-Click handler for button btnContinueStep5
	 */
	,onBtnContinueStep5: function() {
		this._createContract_();
	}
	
	/**
	 * On-Click handler for button btnDiscardStep5
	 */
	,onBtnDiscardStep5: function() {
		this._cancelCapture_();
	}
	
	/**
	 * On-Click handler for button btnBackStep5
	 */
	,onBtnBackStep5: function() {
		this._backToStep4_();
	}
	
	/**
	 * On-Click handler for button btnSetActiveList
	 */
	,onBtnSetActiveList: function() {
		Ext.Msg.show({
				       title : "Note",
				       msg : Main.translate("applicationMsg", "settingContractActive__lbl"),
				       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
				       fn : function(btnId) {
				              if( btnId === "yes" ){
				                     this.setActiveYesList();
		              }
			       },
			       scope : this
					});
	}
	
	/**
	 * On-Click handler for button btnSetActiveEdit
	 */
	,onBtnSetActiveEdit: function() {
		Ext.Msg.show({
					       title : "Note",
					       msg : Main.translate("applicationMsg", "settingContractActive__lbl"),
					       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
					       fn : function(btnId) {
					       	if( btnId === "yes" ) {
					       		this.setActiveYesEdit();
			                }		
					       },
					       scope : this
						});
	}
	
	/**
	 * On-Click handler for button btnBackContractPriceComponentCustom
	 */
	,onBtnBackContractPriceComponentCustom: function() {
		this._showStackedViewElement_("main", "canvas2")
	}
	
	/**
	 * On-Click handler for button btnBackFromPriceCategory
	 */
	,onBtnBackFromPriceCategory: function() {
		this._showStackedViewElement_("main", "canvas2", this._whenBackFronPriceUpdate_())
	}
	
	/**
	 * On-Click handler for button savePriceCtgry
	 */
	,onSavePriceCtgry: function() {
		this.saveContractPriceCtgry(this._getDc_("contractPriceCtgry"));
	}
	
	/**
	 * On-Click handler for button btnUpdateStatus
	 */
	,onBtnUpdateStatus: function() {
		var successFn = function() {
			this._getDc_("contractList").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"updateStatus",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("contractList").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnApprove
	 */
	,onBtnApprove: function() {
		this.openApproveWindow();
	}
	
	/**
	 * On-Click handler for button btnReject
	 */
	,onBtnReject: function() {
		this.openRejectWindow();
	}
	
	/**
	 * On-Click handler for button btnSubmitForApproval
	 */
	,onBtnSubmitForApproval: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApprovalWdw
	 */
	,onBtnSaveCloseApprovalWdw: function() {
		this.submitForApprovalRpc()
	}
	
	/**
	 * On-Click handler for button btnCancelApprovalWdw
	 */
	,onBtnCancelApprovalWdw: function() {
		this._getWindow_("wdwApprovalNote").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApproveWdw
	 */
	,onBtnSaveCloseApproveWdw: function() {
		this.approveRpc()
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRejectWdw
	 */
	,onBtnSaveCloseRejectWdw: function() {
		this.rejectRpc()
	}
	
	/**
	 * On-Click handler for button btnCloseContractChangeWdw
	 */
	,onBtnCloseContractChangeWdw: function() {
		this._getWindow_("wdwContractChange").close();
	}
	
	/**
	 * On-Click handler for button btnCancelApproveWdw
	 */
	,onBtnCancelApproveWdw: function() {
		this._getWindow_("wdwApproveNote").close();
		this._getDc_("contractEdit").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCancelRejectWdw
	 */
	,onBtnCancelRejectWdw: function() {
		this._getWindow_("wdwRejectNote").close();
		this._getDc_("contractEdit").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnResetStatus
	 */
	,onBtnResetStatus: function() {
		this._getWindow_("wdwRemarkResetList").show();
		
						var remarksField = this._get_("remarkResetList")._get_("remarks");
						var saveButton = this._get_("btnSaveCloseResetRemarkWdwList");
						var cancelButton = this._get_("btnCancelResetRemarkWdwList");
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(false);
						remarksField.setReadOnly(false);
		
						remarksField.on("change", function(field) {
							if (field.value.length > 0) {
								saveButton.setDisabled(false);						
							}	
							else {
								saveButton.setDisabled(true);						
							}	
						});
	}
	
	/**
	 * On-Click handler for button btnResetStatusEdit
	 */
	,onBtnResetStatusEdit: function() {
		this._getWindow_("wdwRemarkResetEdit").show();
		
						var remarksField = this._get_("remarkResetEdit")._get_("remarks");
						var saveButton = this._get_("btnSaveCloseResetRemarkWdwEdit");
						var cancelButton = this._get_("btnCancelResetRemarkWdwEdit");
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(false);
						remarksField.setReadOnly(false);
		
						remarksField.on("change", function(field) {
							if (field.value.length > 0) {
								saveButton.setDisabled(false);						
							}	
							else {
								saveButton.setDisabled(true);						
							}	
						});
	}
	
	/**
	 * On-Click handler for button btnContinue
	 */
	,onBtnContinue: function() {
		this._getWindow_("wdwTemp").close();
		this.Cont();
	}
	
	/**
	 * On-Click handler for button Cancel
	 */
	,onCancel: function() {
		this._getDc_("contractEdit").doCancel();
		this._getWindow_("wdwTemp").close();
	}
	
	/**
	 * On-Click handler for button CancelPb
	 */
	,onCancelPb: function() {
		this._getWindow_("wdwPricing").close();
	}
	
	/**
	 * On-Click handler for button CancelPbEditFormula
	 */
	,onCancelPbEditFormula: function() {
		this._getWindow_("wdwPricingEditFormula").close();
	}
	
	/**
	 * On-Click handler for button btnSaveClosePriceWdw
	 */
	,onBtnSaveClosePriceWdw: function() {
		this.saveClosePriceBase();
	}
	
	/**
	 * On-Click handler for button btnSaveClosePriceEditFormulaWdw
	 */
	,onBtnSaveClosePriceEditFormulaWdw: function() {
		this.saveClosePriceEditFormulaBase();
	}
	
	/**
	 * On-Click handler for button btnSaveClosePriceCategoryWdw
	 */
	,onBtnSaveClosePriceCategoryWdw: function() {
		this.saveClosePriceCategory();
	}
	
	/**
	 * On-Click handler for button btnSaveNewPriceCategoryWdw
	 */
	,onBtnSaveNewPriceCategoryWdw: function() {
		this.saveNewPriceCategory();
	}
	
	/**
	 * On-Click handler for button btnSaveNewCompositePrice
	 */
	,onBtnSaveNewCompositePrice: function() {
		this.saveNewPriceCategory();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseCompositePrice
	 */
	,onBtnSaveCloseCompositePrice: function() {
		this.saveCloseCompositePrice();
	}
	
	/**
	 * On-Click handler for button btnSaveEditCompositePrice
	 */
	,onBtnSaveEditCompositePrice: function() {
		this.saveEditCompositePrice();
	}
	
	/**
	 * On-Click handler for button btnCancelCompositePrice
	 */
	,onBtnCancelCompositePrice: function() {
		this._getWindow_("compositePriceWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveEditPriceCategoryWdw
	 */
	,onBtnSaveEditPriceCategoryWdw: function() {
		this.saveEditPriceCategory();
	}
	
	/**
	 * On-Click handler for button btnClosePriceFixWdw
	 */
	,onBtnClosePriceFixWdw: function() {
		this._getWindow_("priceFixWdw").close();
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button WdwHelp1
	 */
	,onWdwHelp1: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button WdwHelp2
	 */
	,onWdwHelp2: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnEditContractPriceCtgry
	 */
	,onBtnEditContractPriceCtgry: function() {
		this.editContractPriceCtgry()
	}
	
	/**
	 * On-Click handler for button btnViewDefinitionContractPriceCtgry
	 */
	,onBtnViewDefinitionContractPriceCtgry: function() {
		this.viewContractPriceCtgry()
	}
	
	/**
	 * On-Click handler for button btnViewFormulaPrice
	 */
	,onBtnViewFormulaPrice: function() {
		this._viewFormulaPrice_();
	}
	
	/**
	 * On-Click handler for button btnDeleteContractPriceCtgry
	 */
	,onBtnDeleteContractPriceCtgry: function() {
		this._getDc_("contractPriceCtgry").doDelete();
	}
	
	/**
	 * On-Click handler for button btnDeleteContractPriceComponent
	 */
	,onBtnDeleteContractPriceComponent: function() {
		this._getDc_("contractPriceCmpnClassic").doDelete();
	}
	
	/**
	 * On-Click handler for button btnNewContractPriceComponent
	 */
	,onBtnNewContractPriceComponent: function() {
		this._getDc_("contractPriceCmpnClassic").doNew();
	}
	
	/**
	 * On-Click handler for button btnEditPriceComponent
	 */
	,onBtnEditPriceComponent: function() {
		this._getDc_("contractPriceCmpnClassic").doEditIn();
	}
	
	/**
	 * On-Click handler for button btnEditPriceComponentBlueprint
	 */
	,onBtnEditPriceComponentBlueprint: function() {
		this._getDc_("contractPriceCmpnClassic").doEditIn()
	}
	
	/**
	 * On-Click handler for button btnDeleteContractPricingBase
	 */
	,onBtnDeleteContractPricingBase: function() {
		this._getDc_("pricingBase").doDelete();
	}
	
	/**
	 * On-Click handler for button btnUpdatePrice
	 */
	,onBtnUpdatePrice: function() {
		this._showStackedViewElement_("main", "canvas4", this._onViewCanvas4_()); 
	}
	
	/**
	 * On-Click handler for button btnReviewPrice
	 */
	,onBtnReviewPrice: function() {
		this._showStackedViewElement_("main", "canvas4", this._onViewCanvas4_()); 
	}
	
	/**
	 * On-Click handler for button btnEditFormulaPrice
	 */
	,onBtnEditFormulaPrice: function() {
		this._editFormulaPrice_();
	}
	
	/**
	 * On-Click handler for button btnPriceCompSave
	 */
	,onBtnPriceCompSave: function() {
		this.saveClosePriceComp();
	}
	
	/**
	 * On-Click handler for button btnPriceCompCancel
	 */
	,onBtnPriceCompCancel: function() {
		this.onPriceCompWdwClose();
		this._getWindow_("priceCompWdw").close();
	}
	
	/**
	 * On-Click handler for button btnNewFormulaPrice
	 */
	,onBtnNewFormulaPrice: function() {
		this._setDefaultPb();
	}
	
	/**
	 * On-Click handler for button btnNewFixPrice
	 */
	,onBtnNewFixPrice: function() {
		this._setDefaultFix();
	}
	
	/**
	 * On-Click handler for button btnNewDutyFeeTax
	 */
	,onBtnNewDutyFeeTax: function() {
		this._setDefaultDFT();
	}
	
	/**
	 * On-Click handler for button btnNewCompositePrice
	 */
	,onBtnNewCompositePrice: function() {
		this._setDefaultComposite();
	}
	
	/**
	 * On-Click handler for button btnContinueNewSalesContract
	 */
	,onBtnContinueNewSalesContract: function() {
		this._continueNewSalesContract_();
	}
	
	/**
	 * On-Click handler for button btnContinueCopySalesContract
	 */
	,onBtnContinueCopySalesContract: function() {
		this._continueCopySalesContract_();
	}
	
	/**
	 * On-Click handler for button btnCancelNewSalesContract
	 */
	,onBtnCancelNewSalesContract: function() {
		this._cancelNewSalesContract_();
	}
	
	/**
	 * On-Click handler for button btnCancelCopySalesContract
	 */
	,onBtnCancelCopySalesContract: function() {
		this._cancelCopySalesContract_();
	}
	
	/**
	 * On-Click handler for button btnDiscardBlueprint
	 */
	,onBtnDiscardBlueprint: function() {
		var successFn = function() {
			this._showStackedViewElement_("main", "canvas1");
			this._getDc_("contractList").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"discardBlueprint",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("contractEdit").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnPublishBlueprint
	 */
	,onBtnPublishBlueprint: function() {
		this.publishBlueprint();
	}
	
	/**
	 * On-Click handler for button btnCancelSelected
	 */
	,onBtnCancelSelected: function() {
		this.fnCancelSelected();
	}
	
	/**
	 * On-Click handler for button btnCancelAll
	 */
	,onBtnCancelAll: function() {
		this.fnCancelAll();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRemarkWdw
	 */
	,onBtnSaveCloseRemarkWdw: function() {
		var successFn = function(dc) {
			this._getWindow_("wdwRemarks").close();
			this._getDc_("shipTo").doReloadPage();
			this._getDc_("history").doReloadPage();
			var dcST = this._getDc_("shipTo"); 
								dcST.setParamValue("remarks",""); 
								dcST._volumeChanged_ = null;
		};
		var o={
			name:"saveInHistory",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("shipTo").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnSaveCloseResetRemarkWdwList
	 */
	,onBtnSaveCloseResetRemarkWdwList: function() {
		this.resetYesList();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseResetRemarkWdwEdit
	 */
	,onBtnSaveCloseResetRemarkWdwEdit: function() {
		this.resetYesEdit();
	}
	
	/**
	 * On-Click handler for button btnCancelResetRemarkWdwEdit
	 */
	,onBtnCancelResetRemarkWdwEdit: function() {
		this._getWindow_("wdwRemarkResetList").close();
		var dc = this._getDc_("contractList"); 
							dc.setParamValue("remarks",""); 
	}
	
	/**
	 * On-Click handler for button btnCancelResetRemarkWdwList
	 */
	,onBtnCancelResetRemarkWdwList: function() {
		this._getWindow_("wdwRemarkResetList").close();
		var dc = this._getDc_("contractList"); 
							dc.setParamValue("remarks",""); 
	}
	
	/**
	 * On-Click handler for button btnCancelRemarkWdw
	 */
	,onBtnCancelRemarkWdw: function() {
		this._getWindow_("wdwRemarks").close();
	}
	
	/**
	 * On-Click handler for button btnAssignPriceComponents
	 */
	,onBtnAssignPriceComponents: function() {
		this._assignPriceComponents_();
	}
	
	/**
	 * On-Click handler for button btnCancelAssignPriceComponents
	 */
	,onBtnCancelAssignPriceComponents: function() {
		this._getDc_("contractPriceCtgryAssign").doCancel();
		this._getWindow_("priceCategoryAsgnWdw").close();
	}
	
	/**
	 * On-Click handler for button btnNewRestriction
	 */
	,onBtnNewRestriction: function() {
		this._getDc_("contractPriceRestr").doNew();
	}
	
	/**
	 * On-Click handler for button btnSaveRestriction
	 */
	,onBtnSaveRestriction: function() {
		this._getDc_("contractPriceRestr").doSave();
	}
	
	/**
	 * On-Click handler for button btnCancelRestriction
	 */
	,onBtnCancelRestriction: function() {
		this._getDc_("contractPriceRestr").doCancel();
	}
	
	/**
	 * On-Click handler for button btnDeleteRestriction
	 */
	,onBtnDeleteRestriction: function() {
		this._getDc_("contractPriceRestr").doDelete();
	}
	
	/**
	 * On-Click handler for button btnDeleteShipTo
	 */
	,onBtnDeleteShipTo: function() {
		this._getDc_("shipTo").doDelete();
	}
	
	/**
	 * On-Click handler for button btnNewShipTo
	 */
	,onBtnNewShipTo: function() {
		this._getDc_("shipTo").doNew();
	}
	
	/**
	 * On-Click handler for button btnSaveShipTo
	 */
	,onBtnSaveShipTo: function() {
		this._getDc_("shipTo").doSave();
	}
	
	/**
	 * On-Click handler for button btnViewOriginalBlueprint
	 */
	,onBtnViewOriginalBlueprint: function() {
		this.openOriginalContract()
	}
	
	/**
	 * On-Click handler for button btnEditDraftContract
	 */
	,onBtnEditDraftContract: function() {
		var successFn = function(dc,response) {
			
									dc.setReadOnly(false);
									var params = JSON.parse(response.responseText).params; 
								 	var generatedContractId = params.generatedContractId;
								 	dc.setFilterValue("id",generatedContractId);
								 	dc.doQuery();
								 	this.makeAttachmentsReadOnly( false );
									this._enableDisableFieldsIfViewContract_(true);
		};
		var failureFn = function(dc,response) {
			
									Main.rpcFailure(response);
		};
		var o={
			name:"createBlueprintContract",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("contractEdit").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnUpdateStatusViewMode
	 */
	,onBtnUpdateStatusViewMode: function() {
		var successFn = function() {
			this._getDc_("contractEdit").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"updateStatus",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("contractEdit").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnViewContractList
	 */
	,onBtnViewContractList: function() {
		this.openForView()
	}
	
	/**
	 * On-Click handler for button btnEditContractList
	 */
	,onBtnEditContractList: function() {
		this.openContractForEdit()
	}
	
	/**
	 * On-Click handler for button btnCustomSave
	 */
	,onBtnCustomSave: function() {
		this._getDc_("contractEdit").doSave();
	}
	
	/**
	 * On-Click handler for button btnCustomCancel
	 */
	,onBtnCustomCancel: function() {
		this._getDc_("contractEdit").doCancel();
	}
	
	,_continueCopySalesContract_: function() {	
		var successFn = function(dc,response) {
			 var dc = this._getDc_("contractEdit");
								var params = JSON.parse(response.responseText).params;
								var generatedContractId = params.generatedContractId;
								this._getWindow_("wdwCopySalesContract").close();
								this._showStackedViewElement_("main", "canvas2", this._afterCopyContract_(dc, generatedContractId));
		};
		var o={
			name:"copySaleContract",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("contractList").doRpcData(o);
	}
	
	,_cancelNewSalesContract_: function() {	
		this._getDc_("contractList").doCancel();;
		this._getWindow_("wdwNewSalesContract").close();
	}
	
	,_cancelCopySalesContract_: function() {	
		this._getDc_("contractList").doCancel();;
		this._getWindow_("wdwCopySalesContract").close();
	}
	
	,setDefaultValues: function() {	
		var successFn = function(dc) {
			
								var dc = this._getDc_("contractEdit");
								var view = this._get_("paymentTerms");
								var params = dc.params;
								var record = dc.getRecord();
								var avgMethodField = view._get_("avgMethodName");
			
								if (record) {
			
									var status = record.get("status");
			
									record.beginEdit();
									record.set("settCurrId",params.get("currencyID"));
									record.set("settCurrCode",params.get("currencyCode"));
									record.set("exchangeRateOffset",params.get("period"));
									record.set("paymentRefDay",params.get("referenceTo"));
									record.set("avgMethodId",params.get("averageMethodId"));
									record.set("avgMethodName",params.get("averageMethodName"));
			//						record.set("financialSourceId",params.get("financialSourceId"));
			//						record.set("financialSourceCode",params.get("financialSourceCode"));
									record.set("invoiceFreq",params.get("invFreq"));
									record.set("invoiceType",params.get("invType"));
									record.set("creditTerms",params.get("creditTerms"));
									record.set("forex",params.get("forex"));
									record.set("paymentTerms",params.get("paymentTerms"));
									record.set("holderCode",parent._ACTIVESUBSIDIARY_.code);
									record.endEdit();
			
									if (!Ext.isEmpty(params.get("financialSourceCode")) && (status !== "Effective" && status !== "Expired" && status !== "Active")) {
										avgMethodField._enable_();
									}
								}					
		};
		var o={
			name:"setDefaulPaymentTerms",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("contractEdit").doRpcData(o);
	}
	
	,assignPrice: function() {	
		this._showAsgnWindow_("atraxo.cmm.ui.extjs.asgn.PriceCtgryAssign_Asgn$Ui" ,{dc: "contractPriceCtgry", objectIdField: "id",leftAdvFilter: [{"fieldName":"idContract","value1":this._getDc_("contractPriceCtgry").getRecord().get("contract"),"operation":"="},{"fieldName":"id","value1":this._getDc_("contractPriceCtgry").getRecord().get("id"),"operation":"<>"}],rightAdvFilter: [{"fieldName":"idContract","value1":this._getDc_("contractPriceCtgry").getRecord().get("contract"),"operation":"="},{"fieldName":"id","value1":this._getDc_("contractPriceCtgry").getRecord().get("id"),"operation":"<>"}]
		,listeners: {close: {scope: this, fn: function() { this._getDc_("contractPriceCtgry").doReloadRecord();
		}} }});
	}
	
	,showCompositePriceWdw: function() {	
		this._getWindow_("compositePriceWdw").show();
	}
	
	,onShowWdw: function() {	
		this._getWindow_("wdwTemp").show();;
		this._getDc_("template").doQuery();
	}
	
	,showWdwPricing: function() {	
		this._getWindow_("wdwPricing").show();
	}
	
	,setActiveYesList: function() {	
		var successFn = function() {
			this._getDc_("contractList").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"setActive",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("contractList").doRpcData(o);
	}
	
	,setActiveYesEdit: function() {	
		var successFn = function() {
			this._getDc_("contractEdit").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"setActive",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("contractEdit").doRpcData(o);
	}
	
	,resetYesList: function() {	
		var successFn = function() {
			this._getDc_("contractList").doReloadPage();
			this._getWindow_("wdwRemarkResetList").close();
		};
		var failureFn = function(dc,response) {
			this._getWindow_("wdwRemarkResetList").close();
			Main.rpcFailure(response);
		};
		var o={
			name:"resetStatus",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("contractList").doRpcData(o);
	}
	
	,resetYesEdit: function() {	
		var successFn = function() {
			this._getDc_("contractEdit").doReloadRecord();
			this._getWindow_("wdwRemarkResetEdit").close();
		};
		var failureFn = function(dc,response) {
			this._getWindow_("wdwRemarkResetEdit").close();
			Main.rpcFailure(response);
		};
		var o={
			name:"resetStatus",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("contractEdit").doRpcData(o);
	}
	
	,priceCtgrySetDefault: function() {	
		var successFn = function(dc) {
			
			                        var dc = this._getDc_("contractPriceCtgry");
			                        var isDefault = dc.params.get("isDefault");
			                        if(isDefault){
			                            Ext.Msg.show({
			                               title : "Warning",
										   msg: Main.translate("applicationMsg", "priceCatWarning__lbl"),
			                               buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
			                               fn : function(btnId) {
			                                      if( btnId === "yes" ){
			                                            dc.params.set("isDefault",false);
			                                            this.setAndSaveNewDefaultPriceCtgry();
			                                        }
			                               },
			                               scope : this
			                            });
			                        }else{
			                            this.setAndSaveNewDefaultPriceCtgry();
			                        }
			                        
		};
		var o={
			name:"checkDefault",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("contractPriceCtgry").doRpcData(o);
	}
	
	,setAndSaveNewDefaultPriceCtgry: function() {	
		var successFn = function() {
			this._getDc_("contractPriceCtgry").doSave();
			this._getDc_("contractEdit").doReloadPage();
		};
		var o={
			name:"setDefault",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("contractPriceCtgry").doRpcData(o);
	}
	
	,_onViewCanvas4_: function() {
		
						this._disableDeleteIfIndex_();
						var priceCmp = this._getDc_("contractPriceCmpnClassic");
						var contract = this._getDc_("contractEdit");
						var r = contract.getRecord();
		//				if (r) {
		//					if (!Ext.isEmpty(r.get("resaleRefCode"))) {						
		//						priceCmp.setReadOnly(true);
		//					}	
		//					else {
		//						priceCmp.setReadOnly(false);
		//					}			
		//				}
	}
	
	,_whenBackFronPriceUpdate_: function() {
		
						var d = this._getDc_("contractPriceCtgry");
						d.canDoDelete = true;
						d.doReloadRecord();
	}
	
	,_when_called_for_details_edit: function(params) {
		
						var dc = this._getDc_("contractList");
						dc.doClearAllFilters();
						dc.setFilterValue("code", params.contractCode);				
						dc.doQuery({showEdit:true});                   
	}
	
	,_goToStep2BtnHandler_: function() {
		
						var f = function(){ 
							var btn = this._get_("btnContinueStep1");
							if( !btn.isDisabled() ){
								this._goToStep2_();
							}
						};
						Ext.defer(f,200,this);
	}
	
	,_backToStep1_: function() {
		
		
						var w = this._getWindow_("wdwNewContract");
						var ctx = this;
						var showElems = ["btnDiscardStep1", "btnContinueStep1"];
						var hideElems = ["btnBackStep2", "btnDiscardStep2", "btnContinueStep2", "btnBackStep3", "btnDiscardStep3", "btnContinueStep3"];
		
						Ext.each(hideElems, function(e) {
							ctx._get_(e).hide();
						}, this);
		
						Ext.each(showElems, function(e) {
							ctx._get_(e).show();
						}, this);
		
						w.setStep(1);
	}
	
	,_backToStep2_: function() {
		
		
						var w = this._getWindow_("wdwNewContract");
						var ctx = this;
						var showElems = ["btnBackStep2", "btnDiscardStep2", "btnContinueStep2"];
						var hideElems = ["btnBackStep3", "btnDiscardStep3", "btnContinueStep3"];
		
						Ext.each(hideElems, function(e) {
							ctx._get_(e).hide();
						}, this);
		
						Ext.each(showElems, function(e) {
							ctx._get_(e).show();
						}, this);
		
						w.setStep(2);
	}
	
	,_backToStep3_: function() {
		
		
						var w = this._getWindow_("wdwNewContract");
						var ctx = this;
						var showElems = ["btnBackStep3", "btnDiscardStep3", "btnContinueStep3"];
						var hideElems = ["btnBackStep2", "btnDiscardStep2", "btnContinueStep2", "btnBackStep4", "btnDiscardStep4", "btnContinueStep4"];
		
						Ext.each(hideElems, function(e) {
							ctx._get_(e).hide();
						}, this);
		
						Ext.each(showElems, function(e) {
							ctx._get_(e).show();
						}, this);
		
						w.setStep(3);
	}
	
	,_backToStep4_: function() {
		
		
						var w = this._getWindow_("wdwNewContract");
						var ctx = this;
						var showElems = ["btnBackStep4", "btnDiscardStep4", "btnContinueStep4"];
						var hideElems = ["btnBackStep2", "btnDiscardStep2", "btnContinueStep2", "btnBackStep3", "btnDiscardStep3", "btnContinueStep3", "btnBackStep5", "btnDiscardStep5", "btnContinueStep5"];
		
						Ext.each(hideElems, function(e) {
							ctx._get_(e).hide();
						}, this);
		
						Ext.each(showElems, function(e) {
							ctx._get_(e).show();
						}, this);
		
						w.setStep(4);
	}
	
	,_goToStep4BtnHandler_: function() {
		
						var f = function(){
							var btn = this._get_("btnContinueStep3");
							if( !btn.isDisabled() ){
								this._goToStep4_();
							}
						};
						Ext.defer(f,200,this);
	}
	
	,_goToStep5BtnHandler_: function() {
		
						var f = function(){
							var btn = this._get_("btnContinueStep4");
							if( !btn.isDisabled() ){
								this._goToStep5_();
							}
						};
						Ext.defer(f,200,this);
	}
	
	,_goToStep3BtnHandler_: function() {
		
						var f = function(){
							var btn = this._get_("btnContinueStep2");
							if( !btn.isDisabled() ){
								this._goToStep3_();
							}
						};
						Ext.defer(f,200,this);
	}
	
	,_createContract_: function() {
		
						var newContract = this._getDc_("newContract");
						var contract = this._getDc_("contractList");
						var w = this._getWindow_("wdwNewContract");
						newContract.doSave({closeWindow: true});
	}
	
	,_cancelCapture_: function() {
		
						var dc = this._getDc_("newContract");
						var w = this._getWindow_("wdwNewContract");
						dc.doCancel();
						w.close();
	}
	
	,_assignPriceComponents_: function() {
		
						var grid = this._get_("priceCtgryAssignList");
						var priceCatDc = this._getDc_("contractPriceCtgry");
						var pricingBase = this._getDc_("pricingBase");
						var s = grid.getSelectionModel().getSelection();
						var win = this._getWindow_("priceCategoryAsgnWdw");
						var r = pricingBase.getRecord();
						var priceCatRecord = priceCatDc.getRecord();
						var selected = [];
						var selectedNames = [];
		
						Ext.each(s, function (item) {
						  selectedNames.push(item.data.name);
						  selected.push({id:item.data.id});
						});
		
						if (r) {
							r.set("priceCategoryIdsJson",JSON.stringify(selected));
							r.set("priceComponents",selectedNames.join(","));
						}
						if (priceCatRecord && win._isInEditForm_ === true) {
							priceCatRecord.set("priceCategoryIdsJson",JSON.stringify(selected));
							priceCatRecord.set("percentageOf",selectedNames.join(","));
						}
		
						win.close(); 
	}
	
	,_continueNewSalesContract_: function() {
		
					var doWork = function(){
						var btn = this._get_("btnContinueNewSalesContract");
						if( btn.isDisabled() ){
							// function is executed with a delay => check again the button status
							return;
						}
		
						var tempList = this._get_("TempList");
			
						var selection = tempList.getSelectionModel().getSelection();
						var contractType = selection[0].data.contractType;
						var deliverySubType = selection[0].data.deliverySubtype;
						var scope = selection[0].data.scope;
						
						var dc = this._getDc_("contractList");
						var record = dc.getRecord();
		
						var counterPartyId = record.get("resBuyerId");
						var counterPartyCode = record.get("resBuyerCode");
		
						var customerId = record.get("customerId");
						var customerCode = record.get("customerCode");
						var customerParentId = record.get("customerParentId");
						
		
						var params = {
							type: contractType,
							subType: deliverySubType,
							scope: scope,
							counterPartyId: counterPartyId,
							counterPartyCode: counterPartyCode,
							customerId: customerId,
							customerCode: customerCode,
							parentId : customerParentId
						}
		
						this._when_do_new_form_list(params);
						this._getWindow_("wdwNewSalesContract").close();
					};
					// execute function with a delay to be sure that the events on the customer LOV is executed
					Ext.defer(doWork, 100, this);
	}
	
	,_editFormulaPrice_: function() {
		
		                var pricingBase = this._getDc_("pricingBase");
		                var pbStore = pricingBase.store;
		 
		                var contractPriceCtgry = this._getDc_("contractPriceCtgry");
		                var pcRecord = contractPriceCtgry.getRecord();
		                var pbRecord = pricingBase.getRecord();
		                var pricingBaseId ="";
		 
		                if (pcRecord && pbRecord) {
		                    pricingBase._changeState_ = 2;
		                    pricingBaseId = pcRecord.get("pricingBaseId");
		                    pbStore.load({
		                        scope: this,
		                        callback: function(records, operation, success) {
		                            var index = pbStore.find("id", pricingBaseId);
		                            
		                            pricingBase.setRecord(index);
									var window = this._getWindow_("wdwPricingEditFormula");
				                    window.show(undefined, function() {
										pricingBase.fireEvent("itsTimeToCalculateDensity", this, {updateRecords: false});
									}, this);                            
		                        }
		                    });                    
		                }       
	}
	
	,_viewFormulaPrice_: function() {
		
						this._editFormulaPrice_();
		
		 				var pricingBase = this._getDc_("pricingBase");
						pricingBase.setReadOnly(true);
	}
	
	,_setDefaultPb: function() {
		
		                var dc = this._getDc_("pricingBase");
		                dc.doCancel(); // SONE-2144
						dc.canDoNew = true;
		                dc.doNew({setIndexPrice: true});
		                
	}
	
	,_setDefaultFix: function() {
		
		                var dc = this._getDc_("pricingBase");
		                dc._step_ = 1;
		                dc.doCancel(); // SONE-2144
						dc.canDoNew = true;
		                dc.doNew({setFixPrice: true}); 
	}
	
	,_setDefaultDFT: function() {
		
		                var dc = this._getDc_("pricingBase");
		                dc._step_ = 2;
		                dc.doCancel(); // SONE-2144
						dc.canDoNew = true;
		                dc.doNew({setDftPrice: true}); 
	}
	
	,_setDefaultComposite: function() {
		
		                var dc = this._getDc_("pricingBase");
		                dc._step_ = 3;
		                dc.doCancel(); // SONE-2144
						dc.canDoNew = true;
		                dc.doNew({setCompositePrice: true}); 
	}
	
	,Cont: function() {
		
		                var tempList = this._get_("TempList");
		 
		                var selection = tempList.getSelectionModel().getSelection();
		                var contractType = selection[0].data.contractType;
		                var deliverySubType = selection[0].data.deliverySubtype;
		                var scope = selection[0].data.scope;
		 
		                var contractDc = this._getDc_("contractEdit");
		                var record = contractDc.getRecord();
		 
		                if (record) {
		                    record.beginEdit();
		 
		                    record.set("type", contractType);
		                    record.set("subType", deliverySubType);
		                    record.set("scope", scope);
		                    record.endEdit();
		                }
		                 
		                contractDc.doEditIn(); 
	}
	
	,saveNewPriceCategory: function() {
		                
		                this.savePriceBases("New"); 
	}
	
	,saveClosePriceCategory: function() {
		              
		                this.savePriceBases("Close"); 
	}
	
	,saveEditPriceCategory: function() {
		               
		                this.savePriceBases("Edit"); 
	}
	
	,saveEditCompositePrice: function() {
		               
		                this.savePriceBases("EditComposite"); 
	}
	
	,saveClosePriceBase: function() {
		
		                var pb = this._getDc_("pricingBase");
		                pb.doSave({doCloseNewIndexPriceWindowAfterSave: true });
	}
	
	,saveCloseCompositePrice: function() {
		
		                var pb = this._getDc_("pricingBase");
		                pb.doSave({doCloseCompositePriceAfterSave: true });
	}
	
	,saveClosePriceEditFormulaBase: function() {
		
		                var pb = this._getDc_("pricingBase");
		                pb.doSave({doCloseEditFormulaWindowAfterSave: true });
	}
	
	,saveClosePriceComp: function() {
		
		                var pb = this._getDc_("contractPriceCmpnClassic");
		                pb.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveClosePriceComp1: function() {
		
		                var pb = this._getDc_("contractPriceCmpn");
		                pb.doSave({doCloseNewWindowAfterSavee: true });
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/CustomerContracts.html";
					Main.downloadFile(url, false);
	}
	
	,onPriceCompWdwClose: function() {
		
		                var dc = this._getDc_("contractPriceCmpnClassic");
		 
		                if(dc.isDirty()){
							dc._suspendEditOut_ = true;
		                    dc.doCancel();
		                }
	}
	
	,getDensity: function(callValidate,updateRecords,dc,formName) {
		
		
						var successFn = function() {	
							var ctx = this;
							var f = function() {
								if (Ext.isEmpty(formName)){
									return;
								}			
								var view = ctx._get_(formName);
								if (!Ext.isEmpty(view)) {
									var field = view._get_("conversionFactor");
									var editorForm = field._getFormInstance_();
									if( editorForm ){
										editorForm._validateInput_();
									}
								}
							}
							Ext.defer(f,300,this);		   
		                    
						};
						var failureFn = function() {					
			                this.setDefaultDensity(callValidate,updateRecords,dc,formName);
							 
						};
						var o={
							name:"changeUnit",
							showWorking : false,
							callbacks:{
								successFn: successFn,
								successScope: this,
								failureFn: failureFn,
								failureScope: this
							},
							modal:false
						};
						dc.doRpcData(o);
	}
	
	,openApproveWindow: function(calledFromPanelButton,contractChangeType) {
		
						var label = "Please provide an approval reason.";
						var title = "Approve";
						var w = this._getWindow_("wdwApproveNote");
						if (calledFromPanelButton && contractChangeType) {
							w._calledFromPanelButton_ = calledFromPanelButton;
							w._contractChangeType_ = contractChangeType;
						}
						this.setupSubmitForApprovalWindow("wdwApproveNote","approveNote","btnSaveCloseApproveWdw","btnCancelApproveWdw" , label , title);				
	}
	
	,openRejectWindow: function(calledFromPanelButton,contractChangeType) {
		
						var label = "Please provide a reason for rejection.";
						var title = "Reject";
						var w = this._getWindow_("wdwRejectNote");
						if (calledFromPanelButton && contractChangeType) {
							w._calledFromPanelButton_ = calledFromPanelButton;
							w._contractChangeType_ = contractChangeType;
						}
						this.setupSubmitForApprovalWindow("wdwRejectNote","rejectNote","btnSaveCloseRejectWdw","btnCancelRejectWdw" , label , title);				
	}
	
	,openSubmitForApprovalWindow: function() {
		
						var label = "Add your note to be sent to your request to the approvers";
						var title = "Submit for approval";
						this.setupSubmitForApprovalWindow("wdwApprovalNote","approvalNote","btnSaveCloseApprovalWdw","btnCancelApprovalWdw" , label , title);				
	}
	
	,fnCancelSelected: function() {
		
						var dc = this._getDc_("shipTo");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAll: function() {
		
						var dc = this._getDc_("shipTo");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,canBePublished: function() {
		
						var dc = this._getDc_("contractEdit");
						var r = dc.getRecord();
						var result = false;
						if (r) {
							var canBePublished = r.get("canBePublished");
							var isBlueprint = r.get("isBlueprint");
							if ( isBlueprint && canBePublished ) {
								result = true;
							}
						}
						return result;
	}
	
	,publishBlueprint: function() {
		
						var dc = this._getDc_("contractEdit");
						if(this.useWorkflowForApproval() && this.canSubmitForApproval(dc)){
							this._showConfirmPublishWithChanges();
						}else{
							this.publishBlueprintRpc(); 
						}
	}
	
	,useWorkflowForApproval: function() {
		
						return _SYSTEMPARAMETERS_.syssalescontractapproval === "true";
	}
});
