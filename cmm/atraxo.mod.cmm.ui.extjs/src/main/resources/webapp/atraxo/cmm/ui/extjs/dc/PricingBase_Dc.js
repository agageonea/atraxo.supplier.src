/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.PricingBase_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.PricingBase_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.PricingBase_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.cmm.ui.extjs.dc.PricingBase_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.cmm_PricingBase_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"priceCat", dataIndex:"priceCatName", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.PriceCategoriesLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "priceCatId"} ]}})
			.addDateField({name:"validFrom", dataIndex:"validFrom"})
			.addDateField({name:"validTo", dataIndex:"validTo"})
			.addCombo({ xtype:"combo", name:"period", dataIndex:"quotationOffset", store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_]})
			.addLov({name:"quotation", dataIndex:"quotName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.QuotNameLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "quotId"} ]}})
			.addLov({name:"avgMthdName", dataIndex:"avgMTName", _enableFn_: function(dc, rec) { return !Ext.isEmpty(this._get_("quotation").getValue()); } , maxLength:50,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.QuotAvgNameLov_Lov", selectOnFocus:true, maxLength:50,
					retFieldMapping: [{lovField:"avgId", dsField: "avgMTId"} ],
					filterFieldMapping: [{lovField:"name", dsField: "quotName"} ]}})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PricingBase_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_PricingBase_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"priceCatName", dataIndex:"priceCatName", width:100})
		.addDateColumn({ name:"validFrom", dataIndex:"validFrom", _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"validTo", _mask_: Masks.DATE})
		.addTextColumn({ name:"quotation", dataIndex:"quotAvgMethodName", width:200})
		.addTextColumn({ name:"period", dataIndex:"quotationOffset", width:70})
		.addTextColumn({ name:"description", dataIndex:"description", width:200})
		.addNumberColumn({ name:"decimals", dataIndex:"decimals", hidden:true, width:100})
		.addTextColumn({ name:"quotUnitCode", dataIndex:"quotUnitCode", hidden:true, width:100})
		.addTextColumn({ name:"conversionUnitCode", dataIndex:"convUnitCode", hidden:true, width:100})
		.addTextColumn({ name:"operator", dataIndex:"operator", hidden:true, width:100})
		.addNumberColumn({ name:"factor", dataIndex:"factor", hidden:true, width:100,  decimals:4})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_setQuotation_: function(val,meta,record,rowIndex) {
		
						var quotation = record.get("quotName");
						var averageMethodName = record.get("avgMTName");
						var result = quotation+" / "+averageMethodName;
						return result;
	}
});

/* ================= EDIT FORM: financialSourceAvgMethodPeriodPurchase ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PricingBase_Dc$financialSourceAvgMethodPeriodPurchase", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PricingBase_Dc$financialSourceAvgMethodPeriodPurchase",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", _enableFn_: function(dc, rec) { return !Ext.isEmpty(dc.getParams().get('settlementCurrencyCode')); ; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !='Percent'; } , allowBlank:false, xtype:"fmbas_ExchangeRateToFinancialSourceAndAvgFilteredLov_Lov", maxLength:25, labelAlign:"top",
			retFieldMapping: [{lovField:"financialSourceId", dsField: "initialFinancialSourceId"} ,{lovField:"financialSourceId", dsField: "financialSourceId"} ],
			filterFieldMapping: [{lovField:"currencyId", dsParam: "settlementCurrencyId"} ],listeners:{
			fpchange:{scope:this, fn:this._enableAvgMethods_},
			expand:{scope:this, fn:this._filterDistinct_}
		}})
		.addLov({name:"avgMethodIndicatorName", bind:"{d.avgMethodIndicatorName}", dataIndex:"avgMethodIndicatorName", _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Percent'; } , allowBlank:false, xtype:"fmbas_ExchangeRateToAvgMthdFilteredLov_Lov", maxLength:50, labelAlign:"top",
			retFieldMapping: [{lovField:"avgMthdId", dsField: "initialAverageMethodId"} ,{lovField:"avgMthdId", dsField: "avgMthdId"} ],
			filterFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"}, {lovField:"currencyId", dsParam: "settlementCurrencyId"} ],listeners:{
			expand:{scope:this, fn:this._filterDistinctAvg_}
		}})
		.addCombo({ xtype:"combo", name:"exchangeRateOffset", bind:"{d.exchangeRateOffset}", dataIndex:"exchangeRateOffset", _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Percent'; } , store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_], labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:170, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["financialSourceCode", "avgMethodIndicatorName", "exchangeRateOffset"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableAvgMethods_: function(el,newVal,oldVal) {
		
		
						if(el._fpRawValue_ != el._onFocusVal_) {
							var avgMethodName = this._get_("avgMethodName");
							var elVal = el.getValue(); 
							if (!Ext.isEmpty(elVal)) {
								avgMethodName.setReadOnly(false);
							}
							else {
								avgMethodName.setReadOnly(true);
							}
							avgMethodName.setValue("");
						}
	},
	
	_filterDistinct_: function(el) {
		
						var store = el.store;
		
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("financialSourceName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });
						}, this);
	},
	
	_filterDistinctAvg_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("avgMthdName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	}
});

/* ================= EDIT FORM: periodFromToPurchase ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PricingBase_Dc$periodFromToPurchase", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PricingBase_Dc$periodFromToPurchase",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index'; } , allowBlank:false, labelAlign:"top"})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index'; } , allowBlank:false, labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:170, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["validFrom", "validTo"]);
	}
});

/* ================= EDIT FORM: NewPCBuyContract ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PricingBase_Dc$NewPCBuyContract", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PricingBase_Dc$NewPCBuyContract",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btnToggleForEx", scope: this, handler: this._toggleForEx_, text: "...", style:"margin-left:3px"})
		.addButton({name:"btnTogglePeriod", scope: this, handler: this._toggle_, text: "...", style:"margin-left:3px"})
		.addTextField({ name:"name", bind:"{d.description}", dataIndex:"description", allowBlank:false, width:280, maxLength:100})
		.addLov({name:"priceCatName", bind:"{d.priceCatName}", dataIndex:"priceCatName", _enableFn_: function(dc, rec) { return dc.record.data.priceCatName != 'Fixed';; } , allowBlank:false, width:280, xtype:"fmbas_PriceCategoriesLov_Lov", maxLength:32, pageSize:0,
			retFieldMapping: [{lovField:"id", dsField: "priceCatId"} ,{lovField:"pricePer", dsField: "priceCatPricePer"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ],listeners:{
			expand:{scope:this, fn:this._filterLovValues_},
			fpchange:{scope:this, fn:this._setEaIfEvent_}
		}})
		.addToggleField({ name:"continous", bind:"{d.continous}", dataIndex:"continous", _enableFn_: function(dc, rec) { return dc.record.data.priceCatName !== 'Fixed';; } , labelWidth:120, style:"margin-left:170px"})
		.addCombo({ xtype:"combo", name:"quantityType", bind:"{d.quantityType}", dataIndex:"quantityType", _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !== 'Percent'; } , width:280, store:[ __CMM_TYPE__.QuantityType._NET_VOLUME_, __CMM_TYPE__.QuantityType._GROSS_VOLUME_]})
		.addCombo({ xtype:"combo", name:"vat", bind:"{d.vat}", dataIndex:"vat", allowBlank:false, width:280, store:[ __CMM_TYPE__.VatApplicability._ALL_EVENTS_, __CMM_TYPE__.VatApplicability._DOMESTIC_EVENTS_, __CMM_TYPE__.VatApplicability._INTERNATIONAL_EVENTS_, __CMM_TYPE__.VatApplicability._NOT_APPLICABLE_]})
		.addDisplayFieldText({ name:"title", bind:"{d.convertTitle}", dataIndex:"convertTitle", _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !== 'Percent'; } , maxLength:100, labelWidth:240})
		.addToggleField({ name:"default", bind:"{d.defaultPriceCtgy}", dataIndex:"defaultPriceCtgy", labelWidth:160, style:"margin-left:130px"})
		.addTextField({ name:"comments", bind:"{d.comments}", dataIndex:"comments", width:520, maxLength:255})
		.addToggleField({ name:"newRestriction", bind:"{d.restriction}", dataIndex:"restriction", labelWidth:120, style:"margin-left:170px"})
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Percent'; } , allowBlank:false, xtype:"fmbas_ExchangeRateToFinancialSourceAndAvgFilteredLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"financialSourceId", dsField: "initialFinancialSourceId"} ,{lovField:"financialSourceId", dsField: "financialSourceId"} ],
			filterFieldMapping: [{lovField:"currencyId", dsParam: "settlementCurrencyId"} ]})
		.addLov({name:"avgMethodIndicatorName", bind:"{d.avgMethodIndicatorName}", dataIndex:"avgMethodIndicatorName", _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Percent'; } , allowBlank:false, xtype:"fmbas_ExchangeRateToAvgMthdFilteredLov_Lov", maxLength:50,
			retFieldMapping: [{lovField:"avgMthdId", dsField: "initialAverageMethodId"} ,{lovField:"avgMthdId", dsField: "avgMthdId"} ],
			filterFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"}, {lovField:"currencyId", dsParam: "settlementCurrencyId"} ]})
		.addCombo({ xtype:"combo", name:"exchangeRateOffset", bind:"{d.exchangeRateOffset}", dataIndex:"exchangeRateOffset", _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Percent'; } , store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_]})
		.addPopoverTextField({name:"financialSourceAvgMethodPeriod", width:415, hPos:"r", vPos:"t", labelWidth:120, fieldsList:["financialSourceCode","avgMethodIndicatorName","exchangeRateOffset"], formatList:[], editorForm:"cmm_PricingBase_Dc$financialSourceAvgMethodPeriod", fieldsValueSeparator:" / "})
		.addNumberField({name:"price", bind:"{d.initialPrice}", dataIndex:"initialPrice", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !=='Index'; } , allowBlank:false, width:215, maxLength:19, decimals:_SYSTEMPARAMETERS_.dec_crncy, labelWidth:120})
		.addLov({name:"currency", bind:"{d.pcCurrCode}", dataIndex:"pcCurrCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !=='Index'; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Percent'; } , allowBlank:false, width:60, noLabel: true, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3,
			retFieldMapping: [{lovField:"id", dsField: "pcCurr"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addHiddenField({ name:"currHidden", bind:"{d.pcCurrCode}", dataIndex:"pcCurrCode", _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer ==='Percent'; } , width:60, noLabel: true})
		.addLov({name:"unit", bind:"{d.pcUnitCode}", dataIndex:"pcUnitCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !=='Index'; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Event' && dc.record.data.priceCatPricePer !=='Percent'; } , allowBlank:false, width:60, noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "pcUnit"} ]})
		.addLov({name:"unitEvent", bind:"{d.pcUnitCode}", dataIndex:"pcUnitCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !=='Index'; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer ==='Event'; } , allowBlank:false, width:60, noLabel: true, xtype:"fmbas_MeasurmentUnitsEventLov_Lov", maxLength:2, pageSize:0,
			retFieldMapping: [{lovField:"id", dsField: "pcUnit"} ]})
		.addLov({name:"unitPercent", bind:"{d.pcUnitCode}", dataIndex:"pcUnitCode", noEdit:true , _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer ==='Percent'; } , allowBlank:false, width:60, noLabel: true, xtype:"fmbas_UnitsLov_Lov", maxLength:2, pageSize:0,
			retFieldMapping: [{lovField:"id", dsField: "pcUnit"} ]})
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !=='Index'; } , allowBlank:false, width:220})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !=='Index'; } , allowBlank:false, width:150, labelWidth:30})
		.addPopoverTextField({name:"periodFromTo", hPos:"r", vPos:"b", dataType:"date", labelWidth:120, fieldsList:["validFrom","validTo"], formatList:[], editorForm:"cmm_PricingBase_Dc$periodFromToPurchase", fieldsValueSeparator:" - "})
		.addToggleField({ name:"provisional", bind:"{d.pcompProvisional}", dataIndex:"pcompProvisional", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !=='Index'; } , labelWidth:120, style:"margin-left:115px"})
		.addToggleField({ name:"withHold", bind:"{d.pcompWithHold}", dataIndex:"pcompWithHold", labelWidth:120, style:"margin-left:113px"})
		.addDisplayFieldText({ name:"initialPrice", bind:"{d.initialPriceTitle}", dataIndex:"initialPriceTitle", maxLength:32})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("priceCatName"),this._getConfig_("continous")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("name"),this._getConfig_("default")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("vat"),this._getConfig_("newRestriction")]})
		.add({name:"row4", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("quantityType")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("title")]})
		.add({name:"row6", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("financialSourceAvgMethodPeriod"),this._getConfig_("btnToggleForEx")]})
		.add({name:"priceRow", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("price"),this._getConfig_("currency"),this._getConfig_("unit"),this._getConfig_("unitEvent"),this._getConfig_("unitPercent"),this._getConfig_("currHidden")]})
		.add({name:"priceRow1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("periodFromTo"),this._getConfig_("btnTogglePeriod")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:700, layout:"anchor"})
		.addPanel({ name:"col2", width:700, layout:"anchor"})
		.addPanel({ name:"col3", width:700, layout:"anchor"})
		.addPanel({ name:"col4", width:700, layout:"anchor"})
		.addPanel({ name:"col5", width:700, layout:"anchor"})
		.addPanel({ name:"col6", width:700, layout:"anchor"})
		.addPanel({ name:"col7", width:300, layout:"anchor"})
		.addPanel({ name:"col8", width:700, layout:"anchor"})
		.addPanel({ name:"col9", width:700, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8", "col9"])
		.addChildrenTo("col1", ["row1"])
		.addChildrenTo("col2", ["row2"])
		.addChildrenTo("col3", ["row3"])
		.addChildrenTo("col4", ["row4"])
		.addChildrenTo("col5", ["row5"])
		.addChildrenTo("col6", ["row6"])
		.addChildrenTo("col7", ["initialPrice"])
		.addChildrenTo("col8", ["priceRow"])
		.addChildrenTo("col9", ["priceRow1"]);
	},
	/* ==================== Business functions ==================== */
	
	_toggleForEx_: function() {
		
						var f = this._get_("financialSourceAvgMethodPeriod");
						f._showEditor_();
	},
	
	_toggle_: function() {
		
						var f = this._get_("periodFromTo");
						f._showEditor_();
	},
	
	_setEaIfEvent_: function(el) {
		
						var dc= this._controller_;
						var r = dc.getRecord();
						var frame = dc.getFrame();
		
						if(el._fpRawValue_ != el._onFocusVal_) {
							if (r) {
			
								var desc = r.get("description");
								var name = r.get("priceCatName");
								
								if (Ext.isEmpty(desc) || (desc!=name)) {
									r.beginEdit();
									r.set("description",name);
									r.endEdit();
								}
			
								var priceCatPricePer = r.get("priceCatPricePer");
								var pcUnitCodeVal = r.get("pcUnitCode");
			
								if (priceCatPricePer == __FMBAS_TYPE__.PriceInd._EVENT_) {
									frame._setDefaultLovValue_("priceFixNew", "unitEvent", "EA", "id", "pcUnit");
								}
								else {
									if (pcUnitCodeVal == "EA") {
										r.beginEdit();
										r.set("pcUnitCode",null);
										r.set("pcUnit",null);
										r.endEdit();
									}
								}
								if (priceCatPricePer == __FMBAS_TYPE__.PriceInd._PERCENT_) {
									Main.info(Main.translate("applicationMsg", "percentInfo__lbl"));
									frame._setDefaultLovValue_("priceFixNew", "unitPercent", "%", "id", "pcUnit");
								}
								else {
									if (pcUnitCodeVal == "%") {
										r.beginEdit();
										r.set("pcUnitCode",null);
										r.set("pcUnit",null);
										r.endEdit();
									}
								}
							}
						}
	},
	
	_filterLovValues_: function(el) {
		
						var s = el.store;
						s.filter(function(r) {
							var value = r.get("type");
							return (value == "Differential" || value == "Into Plane Fee" || value == "Other fee" || value == "Tax");
						});
	},
	
	_shouldValidate_: function() {
		
						return this._isVisible_;
	},
	
	_filterDistinct_: function(el) {
		
						var store = el.store;
		
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("financialSourceName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });
						}, this);
	},
	
	_filterDistinctAvg_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("avgMthdName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	},
	
	_enableAvgMethods_: function(el,newVal,oldVal) {
		
		
						if(el._fpRawValue_ != el._onFocusVal_) {
							var avgMethodName = this._get_("avgMethodName");
							var elVal = el.getValue(); 
							if (!Ext.isEmpty(elVal)) {
								avgMethodName.setReadOnly(false);
							}
							else {
								avgMethodName.setReadOnly(true);
							}
							avgMethodName.setValue("");
						}
	},
	
	_enableIfFinancialSource_: function() {
		
						
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = false;
		
						if (record) {
							var financialSourceName = record.get("financialSourceName");
							if (!Ext.isEmpty(financialSourceName)) {
								result = true;
							}
						}
						return result;
	},
	
	_setReadOnlyRestriction_: function() {
		
						var dc = this._controller_;
						var record = dc.getRecord();
						if (record) {
							var mainCategoryCode = record.get("mainCategoryCode");
							return mainCategoryCode != __FMBAS_TYPE__.PriceType._PRODUCT_;			
						}
						return false;
	}
});

/* ================= EDIT FORM: NewCompositePrice ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PricingBase_Dc$NewCompositePrice", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PricingBase_Dc$NewCompositePrice",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"showAssignment", scope: this, handler: this._assignPriceCategory_, text: "..."})
		.addTextField({ name:"name", bind:"{d.description}", dataIndex:"description", allowBlank:false, width:235, noLabel: true, maxLength:100})
		.addLov({name:"priceCatName", bind:"{d.priceCatName}", dataIndex:"priceCatName", allowBlank:false, width:235, noLabel: true, xtype:"fmbas_PriceCategoriesLov_Lov", maxLength:32, pageSize:0,
			retFieldMapping: [{lovField:"id", dsField: "priceCatId"} ,{lovField:"pricePer", dsField: "priceCatPricePer"} ],
			filterFieldMapping: [{lovField:"active", value: "true"}, {lovField:"pricePer", value: "Composite"} ]})
		.addToggleField({ name:"continous", bind:"{d.continous}", dataIndex:"continous", allowBlank:false, noLabel: true})
		.addCombo({ xtype:"combo", name:"vat", bind:"{d.vat}", dataIndex:"vat", allowBlank:false, width:235, noLabel: true, store:[ __CMM_TYPE__.VatApplicability._ALL_EVENTS_, __CMM_TYPE__.VatApplicability._DOMESTIC_EVENTS_, __CMM_TYPE__.VatApplicability._INTERNATIONAL_EVENTS_, __CMM_TYPE__.VatApplicability._NOT_APPLICABLE_]})
		.addToggleField({ name:"default", bind:"{d.defaultPriceCtgy}", dataIndex:"defaultPriceCtgy", noLabel: true})
		.addToggleField({ name:"newRestriction", bind:"{d.restriction}", dataIndex:"restriction", noEdit:true , noLabel: true})
		.addNumberField({name:"price", bind:"{d.initialPrice}", dataIndex:"initialPrice", noEdit:true , width:100, noLabel: true, maxLength:19, decimals:_SYSTEMPARAMETERS_.dec_crncy})
		.addLov({name:"currency", bind:"{d.pcCurrCode}", dataIndex:"pcCurrCode", noEdit:true , width:67, noLabel: true, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3,
			retFieldMapping: [{lovField:"id", dsField: "pcCurr"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addLov({name:"unit", bind:"{d.pcUnitCode}", dataIndex:"pcUnitCode", noEdit:true , width:67, noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "pcUnit"} ]})
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", allowBlank:false, width:100, noLabel: true})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", allowBlank:false, width:100, noLabel: true})
		.addBooleanField({ name:"provisional", bind:"{d.pcompProvisional}", dataIndex:"pcompProvisional", noLabel: true})
		.addBooleanField({ name:"withHold", bind:"{d.pcompWithHold}", dataIndex:"pcompWithHold", _visibleFn_:this._showIfPurchaseContract_, noLabel: true})
		.addTextArea({ name:"priceComponents", bind:"{d.priceComponents}", dataIndex:"priceComponents", noEdit:true , width:235, noLabel: true})
		.addDisplayFieldText({ name:"emptyField1", bind:"{d.dummyField}", dataIndex:"dummyField", noLabel: true, maxLength:32, style:"width:70px"})
		.addDisplayFieldText({ name:"emptyField2", bind:"{d.dummyField}", dataIndex:"dummyField", noLabel: true, maxLength:32, style:"width:70px"})
		.addDisplayFieldText({ name:"emptyField3", bind:"{d.dummyField}", dataIndex:"dummyField", noLabel: true, maxLength:32, style:"width:70px"})
		.addDisplayFieldText({ name:"emptyField4", bind:"{d.dummyField}", dataIndex:"dummyField", noLabel: true, maxLength:32, style:"width:70px", colspan:3})
		.addDisplayFieldText({ name:"emptyField5", bind:"{d.dummyField}", dataIndex:"dummyField", noLabel: true, maxLength:32, style:"width:70px"})
		.addDisplayFieldText({ name:"emptyField6", bind:"{d.dummyField}", dataIndex:"dummyField", noLabel: true, maxLength:32, style:"width:70px", colspan:3})
		.addDisplayFieldText({ name:"initialPrice", bind:"{d.initialPriceTitle}", dataIndex:"initialPriceTitle", maxLength:32, colspan:5})
		.addDisplayFieldText({ name:"nameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"priceCatNameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"vatLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"continousLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"defaultLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"newRestrictionLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"provisionalLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"priceLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"validFromLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"validToLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-left:10px"})
		.addDisplayFieldText({ name:"withHoldLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_:this._showIfPurchaseContract_, maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"priceComponentsLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.add({name:"priceCurrUnit", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("price"),this._getConfig_("currency"),this._getConfig_("unit")]})
		.add({name:"validFromValidTo", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("validFrom"),this._getConfig_("validToLabel"),this._getConfig_("validTo")]})
		.add({name:"priceCompAsgn", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("priceComponents"),this._getConfig_("showAssignment")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  style:"padding:10px"})
		.addPanel({ name:"tableContainer1", layout: {type:"table"}})
		.addPanel({ name:"table1", layout: {type:"table", columns:5}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["tableContainer1"])
		.addChildrenTo("tableContainer1", ["table1"])
		.addChildrenTo("table1", ["priceCatNameLabel", "priceCatName", "emptyField1", "continousLabel", "continous", "nameLabel", "name", "emptyField2", "defaultLabel", "default", "vatLabel", "vat", "emptyField3", "newRestrictionLabel", "newRestriction", "priceComponentsLabel", "priceCompAsgn", "emptyField6", "initialPrice", "priceLabel", "priceCurrUnit", "emptyField4", "validFromLabel", "validFromValidTo", "emptyField5"]);
	},
	/* ==================== Business functions ==================== */
	
	_showIfPurchaseContract_: function() {
		
						var dc = this._controller_;
						var dcContext = dc.dcContext;
						var parentDc = dcContext.parentDc;
						var dsName = parentDc.dsName;
						var result = true;
		
						if (dsName != "cmm_Contract_Ds") {
							result = false;
						}
		
						return result;
		
	},
	
	_shouldValidate_: function() {
		
						return this._isVisible_;
	},
	
	_assignPriceCategory_: function() {
		
						var dc = this._controller_;
						var frame = dc.getFrame();
						var win = frame._getWindow_("priceCategoryAsgnWdw");	
						win._isInEditForm_ = false;			
						win.show();
		
	}
});

/* ================= EDIT FORM: financialSourceAvgMethodPeriod ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PricingBase_Dc$financialSourceAvgMethodPeriod", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PricingBase_Dc$financialSourceAvgMethodPeriod",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", _enableFn_: function(dc, rec) { return dc.getParams().get('settlementCurrencyCode') != null; ; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Percent'; } , allowBlank:false, xtype:"fmbas_ExchangeRateToFinancialSourceAndAvgFilteredLov_Lov", maxLength:25, labelAlign:"top",
			retFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"} ],
			filterFieldMapping: [{lovField:"currencyId", dsParam: "settlementCurrencyId"} ],listeners:{
			fpchange:{scope:this, fn:this._enableAvgMethods_},
			expand:{scope:this, fn:this._filterDistinct_}
		}})
		.addLov({name:"avgMethodIndicatorName", bind:"{d.avgMethodIndicatorName}", dataIndex:"avgMethodIndicatorName", _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Percent'; } , allowBlank:false, xtype:"fmbas_ExchangeRateToAvgMthdFilteredLov_Lov", maxLength:50, labelAlign:"top",
			retFieldMapping: [{lovField:"avgMthdId", dsField: "avgMthdId"} ],
			filterFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"}, {lovField:"currencyId", dsParam: "settlementCurrencyId"} ],listeners:{
			expand:{scope:this, fn:this._filterDistinctAvg_}
		}})
		.addCombo({ xtype:"combo", name:"exchangeRateOffset", bind:"{d.exchangeRateOffset}", dataIndex:"exchangeRateOffset", _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Percent'; } , store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_], labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:170, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["financialSourceCode", "avgMethodIndicatorName", "exchangeRateOffset"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableAvgMethods_: function(el,newVal,oldVal) {
		
		
						if(el._fpRawValue_ != el._onFocusVal_) {
							var avgMethodName = this._get_("avgMethodIndicatorName");
							var elVal = el.getValue(); 
							if (!Ext.isEmpty(elVal)) {
								avgMethodName.setReadOnly(false);
							}
							else {
								avgMethodName.setReadOnly(true);
							}
							avgMethodName.setValue("");
						}
	},
	
	_filterDistinctAvg_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("avgMthdName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	},
	
	_filterDistinct_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("financialSourceName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });
						}, this);
	}
});

/* ================= EDIT FORM: NewPCSellContract ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PricingBase_Dc$NewPCSellContract", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PricingBase_Dc$NewPCSellContract",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btnToggleForEx", scope: this, handler: this._toggleForEx_, text: "...", _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Percent'; } , style:"margin-left:3px"})
		.addButton({name:"btnTogglePeriod", scope: this, handler: this._toggle_, text: "...", style:"margin-left:3px"})
		.addTextField({ name:"name", bind:"{d.description}", dataIndex:"description", allowBlank:false, width:280, maxLength:100})
		.addLov({name:"priceCatName", bind:"{d.priceCatName}", dataIndex:"priceCatName", _enableFn_: function(dc, rec) { return dc.record.data.priceCatName != 'Fixed';; } , allowBlank:false, width:280, xtype:"fmbas_PriceCategoriesLov_Lov", maxLength:32, pageSize:0,
			retFieldMapping: [{lovField:"id", dsField: "priceCatId"} ,{lovField:"pricePer", dsField: "priceCatPricePer"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ],listeners:{
			expand:{scope:this, fn:this._filterLovValues_},
			fpchange:{scope:this, fn:this._setEaIfEvent_},
			change:{scope:this, fn:this._hideForexIfPercent_}
		}})
		.addHiddenField({ name:"priceCatPricePer", bind:"{d.priceCatPricePer}", dataIndex:"priceCatPricePer"})
		.addToggleField({ name:"continous", bind:"{d.continous}", dataIndex:"continous", _enableFn_: function(dc, rec) { return dc.record.data.priceCatName !== 'Fixed';; } , labelWidth:120, style:"margin-left:170px"})
		.addCombo({ xtype:"combo", name:"quantityType", bind:"{d.quantityType}", dataIndex:"quantityType", _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Percent'; } , width:280, store:[ __CMM_TYPE__.QuantityType._NET_VOLUME_, __CMM_TYPE__.QuantityType._GROSS_VOLUME_]})
		.addCombo({ xtype:"combo", name:"vat", bind:"{d.vat}", dataIndex:"vat", allowBlank:false, width:280, store:[ __CMM_TYPE__.VatApplicability._ALL_EVENTS_, __CMM_TYPE__.VatApplicability._DOMESTIC_EVENTS_, __CMM_TYPE__.VatApplicability._INTERNATIONAL_EVENTS_, __CMM_TYPE__.VatApplicability._NOT_APPLICABLE_]})
		.addDisplayFieldText({ name:"title", bind:"{d.convertTitle}", dataIndex:"convertTitle", _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Percent'; } , maxLength:100, labelWidth:240})
		.addToggleField({ name:"default", bind:"{d.defaultPriceCtgy}", dataIndex:"defaultPriceCtgy", labelWidth:160, style:"margin-left:130px"})
		.addTextField({ name:"comments", bind:"{d.comments}", dataIndex:"comments", width:520, maxLength:255})
		.addToggleField({ name:"newRestriction", bind:"{d.restriction}", dataIndex:"restriction", labelWidth:120, style:"margin-left:170px"})
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", _enableFn_: function(dc, rec) { return !Ext.isEmpty(dc.getParams().get('settlementCurrencyCode'));; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Percent'; } , allowBlank:false, xtype:"fmbas_ExchangeRateToFinancialSourceAndAvgFilteredLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"} ],
			filterFieldMapping: [{lovField:"currencyId", dsParam: "settlementCurrencyId"} ]})
		.addLov({name:"avgMethodIndicatorName", bind:"{d.avgMethodIndicatorName}", dataIndex:"avgMethodIndicatorName", _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Percent'; } , allowBlank:false, xtype:"fmbas_ExchangeRateToAvgMthdFilteredLov_Lov", maxLength:50,
			retFieldMapping: [{lovField:"avgMthdId", dsField: "avgMthdId"} ],
			filterFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"}, {lovField:"currencyId", dsParam: "settlementCurrencyId"} ]})
		.addCombo({ xtype:"combo", name:"exchangeRateOffset", bind:"{d.exchangeRateOffset}", dataIndex:"exchangeRateOffset", _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Percent'; } , store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_]})
		.addPopoverTextField({name:"financialSourceAvgMethodPeriod", width:415, hPos:"r", vPos:"t", labelWidth:120, fieldsList:["financialSourceCode","avgMethodIndicatorName","exchangeRateOffset"], formatList:[], editorForm:"cmm_PricingBase_Dc$financialSourceAvgMethodPeriod", fieldsValueSeparator:" / "})
		.addNumberField({name:"price", bind:"{d.initialPrice}", dataIndex:"initialPrice", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !=='Index'; } , allowBlank:false, width:215, maxLength:19, decimals:_SYSTEMPARAMETERS_.dec_crncy, labelWidth:120})
		.addLov({name:"currency", bind:"{d.pcCurrCode}", dataIndex:"pcCurrCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !=='Index'; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Percent'; } , allowBlank:false, width:60, noLabel: true, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3,
			retFieldMapping: [{lovField:"id", dsField: "pcCurr"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addHiddenField({ name:"currHidden", bind:"{d.pcCurrCode}", dataIndex:"pcCurrCode", _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer ==='Percent'; } , width:60, noLabel: true})
		.addLov({name:"unit", bind:"{d.pcUnitCode}", dataIndex:"pcUnitCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !=='Index'; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer !=='Event' && dc.record.data.priceCatPricePer !=='Percent'; } , allowBlank:false, width:60, noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "pcUnit"} ]})
		.addLov({name:"unitEvent", bind:"{d.pcUnitCode}", dataIndex:"pcUnitCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !=='Index'; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer ==='Event'; } , allowBlank:false, width:60, noLabel: true, xtype:"fmbas_MeasurmentUnitsEventLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "pcUnit"} ]})
		.addLov({name:"unitPercent", bind:"{d.pcUnitCode}", dataIndex:"pcUnitCode", noEdit:true , _visibleFn_: function(dc, rec) { return dc.record.data.priceCatPricePer ==='Percent'; } , allowBlank:false, width:60, noLabel: true, xtype:"fmbas_UnitsLov_Lov", maxLength:2, pageSize:0,
			retFieldMapping: [{lovField:"id", dsField: "pcUnit"} ]})
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", allowBlank:false, width:220})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", allowBlank:false, width:150, labelWidth:30})
		.addPopoverTextField({name:"periodFromTo", hPos:"r", vPos:"b", dataType:"date", labelWidth:120, fieldsList:["validFrom","validTo"], formatList:[], editorForm:"cmm_PricingBase_Dc$periodFromTo", fieldsValueSeparator:" - "})
		.addToggleField({ name:"provisional", bind:"{d.pcompProvisional}", dataIndex:"pcompProvisional", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !=='Index'; } , labelWidth:120, style:"margin-left:115px"})
		.addDisplayFieldText({ name:"initialPrice", bind:"{d.initialPriceTitle}", dataIndex:"initialPriceTitle", maxLength:32})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("priceCatName"),this._getConfig_("continous"),this._getConfig_("priceCatPricePer")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("name"),this._getConfig_("default")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("vat"),this._getConfig_("newRestriction")]})
		.add({name:"row4", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("quantityType")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("title")]})
		.add({name:"row6", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("financialSourceAvgMethodPeriod"),this._getConfig_("btnToggleForEx")]})
		.add({name:"priceRow", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("price"),this._getConfig_("currency"),this._getConfig_("unit"),this._getConfig_("unitEvent"),this._getConfig_("unitPercent"),this._getConfig_("currHidden")]})
		.add({name:"priceRow1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("periodFromTo"),this._getConfig_("btnTogglePeriod")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:700, layout:"anchor"})
		.addPanel({ name:"col2", width:700, layout:"anchor"})
		.addPanel({ name:"col3", width:700, layout:"anchor"})
		.addPanel({ name:"col4", width:700, layout:"anchor"})
		.addPanel({ name:"col5", width:700, layout:"anchor"})
		.addPanel({ name:"col6", width:700, layout:"anchor"})
		.addPanel({ name:"col7", width:300, layout:"anchor"})
		.addPanel({ name:"col8", width:700, layout:"anchor"})
		.addPanel({ name:"col9", width:700, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8", "col9"])
		.addChildrenTo("col1", ["row1"])
		.addChildrenTo("col2", ["row2"])
		.addChildrenTo("col3", ["row3"])
		.addChildrenTo("col4", ["row4"])
		.addChildrenTo("col5", ["row5"])
		.addChildrenTo("col6", ["row6"])
		.addChildrenTo("col7", ["initialPrice"])
		.addChildrenTo("col8", ["priceRow"])
		.addChildrenTo("col9", ["priceRow1"]);
	},
	/* ==================== Business functions ==================== */
	
	_hideForexIfPercent_: function() {
		
						var dc = this._controller_;
						var r = dc.getRecord();
						var field = this._get_("row6");
						if (r) {
							var f = function() {
								var pp = r.get("priceCatPricePer");
								if (pp === "Percent") {
									field.setHidden(true);
								}
								else {
									field.setHidden(false);
								}
							}
							Ext.defer(f,50,this);
							
						}
	},
	
	_toggleForEx_: function() {
		
						var f = this._get_("financialSourceAvgMethodPeriod");
						f._showEditor_();
	},
	
	_toggle_: function() {
		
						var f = this._get_("periodFromTo");
						f._showEditor_();
	},
	
	_setEaIfEvent_: function(el) {
		
						var dc= this._controller_;
		                var r = dc.getRecord();
		                var frame = dc.getFrame();
		 
		                if(el._fpRawValue_ != el._onFocusVal_) {
		
		                    if (r) {
		    
		                        var desc = r.get("description");
		                        var name = r.get("priceCatName");
		                        
		                        if (Ext.isEmpty(desc) || (desc!=name)) {
									
		                            r.beginEdit();
		                            r.set("description",name);
		                            r.endEdit();
		                        }
		    
		                        var priceCatPricePer = r.get("priceCatPricePer");
		                        var pcUnitCodeVal = r.get("pcUnitCode");
		    
		                        if (priceCatPricePer == __FMBAS_TYPE__.PriceInd._EVENT_) {
		                            frame._setDefaultLovValue_("priceFixNew", "unitEvent", "EA", "id", "pcUnit");
		                        }
		                        else {
		                            if (pcUnitCodeVal == "EA") {
		                                r.beginEdit();
		                                r.set("pcUnitCode",null);
		                                r.set("pcUnit",null);
		                                r.endEdit();
		                            }
		                        }
		                        if (priceCatPricePer == __FMBAS_TYPE__.PriceInd._PERCENT_) {
		                            Main.info(Main.translate("applicationMsg", "percentInfo__lbl"));
		                            frame._setDefaultLovValue_("priceFixNew", "unitPercent", "%", "id", "pcUnit");
		                        }
		                        else {
		                            if (pcUnitCodeVal == "%") {
		                                r.beginEdit();
		                                r.set("pcUnitCode",null);
		                                r.set("pcUnit",null);
		                                r.endEdit();
		                            }
		                        }
		                    }
		                }
	},
	
	_shouldValidate_: function() {
		
						return this._isVisible_;
	},
	
	_filterLovValues_: function(el) {
		
						var s = el.store;
						s.filter(function(r) {
							var value = r.get("type");
							var pricePer = r.get("pricePer");
							return ((value == "Differential" || value == "Into Plane Fee" || value == "Other fee" || value == "Tax") && pricePer != "Composite");
						});
	},
	
	_filterDistinct_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("financialSourceName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });
						}, this);
	},
	
	_filterDistinctAvg_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("avgMthdName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	},
	
	_enableAvgMethods_: function(el,newVal,oldVal) {
		
		
						if(el._fpRawValue_ != el._onFocusVal_) {
							var avgMethodName = this._get_("avgMethodName");
							var elVal = el.getValue(); 
							if (!Ext.isEmpty(elVal)) {
								avgMethodName.setReadOnly(false);
							}
							else {
								avgMethodName.setReadOnly(true);
							}
							avgMethodName.setValue("");
						}
	},
	
	_enableIfFinancialSource_: function() {
		
						
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = false;
		
						if (record) {
							var financialSourceName = record.get("financialSourceName");
							if (!Ext.isEmpty(financialSourceName)) {
								result = true;
							}
						}
						return result;
	},
	
	_setReadOnlyRestriction_: function() {
		
						var dc = this._controller_;
						var record = dc.getRecord();
						if (record) {
							var mainCategoryCode = record.get("mainCategoryCode");
							return mainCategoryCode != __FMBAS_TYPE__.PriceType._PRODUCT_;			
						}
						return false;
	}
});

/* ================= EDIT FORM: EditFormulaPrice ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PricingBase_Dc$EditFormulaPrice", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PricingBase_Dc$EditFormulaPrice",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btnTogglePeriod", scope: this, handler: this._toggle_, text: "...", style:"margin-left:3px"})
		.addButton({name:"btnToggleConversionFactor", scope: this, handler: this._toggleConversionfactor_, text: "...", style:"margin-left:3px"})
		.addButton({name:"btnToggleBenchmark", scope: this, handler: this._toggleBenchmark_, text: "...", style:"margin-left:3px"})
		.addTextField({ name:"description", bind:"{d.description}", dataIndex:"description", allowBlank:false, width:210, maxLength:100, labelAlign:"top"})
		.addLov({name:"priceCatName", bind:"{d.priceCatName}", dataIndex:"priceCatName", noEdit:true , _visibleFn_: function(dc, rec) { return false; } , allowBlank:false, xtype:"fmbas_PriceCategoriesLov_Lov", maxLength:32, labelWidth:100, pageSize:0,
			retFieldMapping: [{lovField:"id", dsField: "priceCatId"} ],listeners:{
			change:{scope:this, fn:this._enableDisableFields_},
			load:{scope:this, fn:this._enableDisableFields_}
		}})
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", allowBlank:false, labelWidth:120})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", allowBlank:false, labelWidth:80})
		.addDisplayFieldText({ name:"convFactor", bind:"{d.convFactor}", dataIndex:"convFactor", noEdit:true , maxLength:4000, labelWidth:110})
		.addTextField({ name:"quotUnitCode", bind:"{d.quotUnitCode}", dataIndex:"quotUnitCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index'; } , maxLength:2})
		.addLov({name:"convUnitCode", bind:"{d.convUnitCode}", dataIndex:"convUnitCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index'; } , width:100, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, labelWidth:30,
			retFieldMapping: [{lovField:"id", dsField: "convUnitId"} ,{lovField:"unitTypeInd", dsField: "convUnitTypeInd"} ],listeners:{
			blur:{scope:this, fn:this._calculateDensity_},
			expand:{scope:this, fn:this._filterConvUnit_}
		}})
		.addDisplayFieldText({ name:"conversionFromLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotUnitCode); } , maxLength:100})
		.addDisplayFieldText({ name:"conversionToLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.convUnitCode); } , maxLength:100})
		.addDisplayFieldText({ name:"multiplyLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.operator); } , maxLength:100})
		.addDisplayFieldText({ name:"usingLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.factor); } , maxLength:100})
		.addDisplayFieldText({ name:"decimalPrecisionLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.factor); } , maxLength:100})
		.addPopoverTextField({name:"conversionFactor", width:515, hPos:"r", vPos:"b", labelAlign:"top", fieldsList:["conversionFromLabel","quotUnitCode","conversionToLabel","convUnitCode","multiplyLabel","operator","factor","usingLabel","decimals","decimalPrecisionLabel"], formatList:[], editorForm:"cmm_PricingBase_Dc$conversionFactor", fieldsValueSeparator:" ", allowBlank:false})
		.addPopoverTextField({name:"periodFromTo", noLabel: true, hPos:"r", vPos:"b", dataType:"date", labelAlign:"top", fieldsList:["validFrom","validTo"], formatList:[], editorForm:"cmm_PricingBase_Dc$periodFromTo", fieldsValueSeparator:" - ", allowBlank:false})
		.add({name:"periodOfValidity", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("periodFromTo"),this._getConfig_("btnTogglePeriod")]})
		.addDisplayFieldText({ name:"periodLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left;"})
		.addDisplayFieldText({ name:"conversionFactorLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left;"})
		.add({name:"conversionFactorAndButton", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("conversionFactor"),this._getConfig_("btnToggleConversionFactor")]})
		.add({name:"priceName", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("priceCatName"),this._getConfig_("description")]})
		.add({name:"validityPeriod", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("periodOfValidity")]})
		.addTextField({ name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", maxLength:25})
		.addTextField({ name:"benchmarkProvider", bind:"{d.benchmarkProvider}", dataIndex:"benchmarkProvider", maxLength:64})
		.addTextField({ name:"quotValue", bind:"{d.quotValue}", dataIndex:"quotValue", allowBlank:false, maxLength:255})
		.addTextField({ name:"quotAvgMethodName", bind:"{d.quotAvgMethodName}", dataIndex:"quotAvgMethodName", allowBlank:false, maxLength:50})
		.addTextField({ name:"quotationOffset", bind:"{d.quotationOffset}", dataIndex:"quotationOffset", maxLength:32})
		.addTextField({ name:"quotTimeSeriesDescription", bind:"{d.quotTimeSeriesDescription}", dataIndex:"quotTimeSeriesDescription", maxLength:200})
		.addDisplayFieldText({ name:"commaLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotTimeSeriesDescription); } , maxLength:100})
		.addDisplayFieldText({ name:"secondCommaLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotAvgMethodName); } , maxLength:100})
		.addDisplayFieldText({ name:"benchmarkPeriodLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotationOffset); } , maxLength:100})
		.addPopoverTextField({name:"benchmark", width:515, hPos:"r", vPos:"b", labelAlign:"top", fieldsList:["quotTimeSeriesDescription","commaLabel","quotAvgMethodName","secondCommaLabel","quotationOffset","benchmarkPeriodLabel"], formatList:[], editorForm:"cmm_PricingBase_Dc$benchmark", fieldsValueSeparator:"", allowBlank:false})
		.addDisplayFieldText({ name:"benchmarkLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left;"})
		.add({name:"benchmarkAndButton", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("benchmark"),this._getConfig_("btnToggleBenchmark")]})
		.addCombo({ xtype:"combo", name:"operator", bind:"{d.operator}", dataIndex:"operator", _enableFn_: function(dc, rec) { return this._get_("priceCatName").getValue() === "Index"; } , width:150, store:[ __FMBAS_TYPE__.Operator._MULTIPLY_, __FMBAS_TYPE__.Operator._DIVIDE_], labelWidth:60,listeners:{
			fpchange:{scope:this, fn:this._calculateDensity_}
		}})
		.addNumberField({name:"factor", bind:"{d.factor}", dataIndex:"factor", _enableFn_: function(dc, rec) { return this._get_("priceCatName").getValue() === "Index"; } , width:130, maxLength:19, labelWidth:60, decimals:4,listeners:{
			blur:{scope:this, fn:this._calculateDensity_}
		}})
		.addNumberField({name:"decimals", bind:"{d.decimals}", dataIndex:"decimals", _enableFn_: function(dc, rec) { return this._get_("priceCatName").getValue() === "Index"; } , _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.factor); } , width:150, maxLength:3, labelWidth:100})
		.addTextField({ name:"used", bind:"{d.used}", dataIndex:"used", noEdit:true , width:150, maxLength:4000})
		.addNumberField({name:"density", bind:"{d.density}", dataIndex:"density", noEdit:true , width:150, sysDec:"dec_unit", maxLength:19, labelWidth:60})
		.addTextField({ name:"density1", bind:"{d.density1}", dataIndex:"density1", noEdit:true , width:60, maxLength:4000, labelWidth:1})
		.addTextField({ name:"density2", bind:"{d.density2}", dataIndex:"density2", noEdit:true , width:60, maxLength:4000, labelWidth:1})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"hiddenFields", width:560,  style:"display:none", layout:"anchor"})
		.addPanel({ name:"p1", width:560, layout:"anchor"})
		.addPanel({ name:"p2", width:560, layout:"anchor"})
		.addPanel({ name:"p3", width:560, layout:"anchor"})
		.addPanel({ name:"p4", width:560, layout:"anchor"})
		.addPanel({ name:"p5", width:560, layout:"anchor"})
		.addPanel({ name:"p6", width:560, layout:"anchor"})
		.addPanel({ name:"p7", width:560, layout:"anchor"})
		.addPanel({ name:"p8", width:560,  style:"display:none", layout:"anchor"})
		.addPanel({ name:"p9", width:560,  style:"display:none", layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["hiddenFields", "p1", "p2", "p3", "p4", "p5", "p6", "p7", "p8", "p9"])
		.addChildrenTo("hiddenFields", ["quotAvgMethodName", "conversionFromLabel", "conversionToLabel", "multiplyLabel", "usingLabel", "decimalPrecisionLabel", "commaLabel", "benchmarkPeriodLabel", "secondCommaLabel"])
		.addChildrenTo("p1", ["priceName"])
		.addChildrenTo("p2", ["periodLabel"])
		.addChildrenTo("p3", ["validityPeriod"])
		.addChildrenTo("p4", ["benchmarkLabel"])
		.addChildrenTo("p5", ["benchmarkAndButton"])
		.addChildrenTo("p6", ["conversionFactorLabel"])
		.addChildrenTo("p7", ["conversionFactorAndButton"])
		.addChildrenTo("p8", ["quotUnitCode", "operator", "factor", "convUnitCode", "decimals"])
		.addChildrenTo("p9", ["used", "density", "density1", "density2"]);
	},
	/* ==================== Business functions ==================== */
	
	_toggle_: function() {
		
						var f = this._get_("periodFromTo");
						f._showEditor_();
	},
	
	_toggleBenchmark_: function() {
		
						var f = this._get_("benchmark");
						f._showEditor_();
	},
	
	_toggleConversionfactor_: function() {
		
						var f = this._get_("conversionFactor");
						f._showEditor_();
	},
	
	_endDefine_: function() {
		
						this._changeCounter_ = 0;
	},
	
	_shouldValidate_: function() {
		
						return this._isVisible_;
	},
	
	_calculateDensity_: function() {
		
						var dc = this._controller_;
						var r = dc.getRecord();
						if (r) {
							var factor = r.get("factor");
							if(factor != 0 && factor != null && factor != "0" && factor != ""){
								dc.fireEvent("itsTimeToCalculateDensity", this);
							} 
						}
	},
	
	_enableDisableFields_: function(el) {
		
		
						if (this._controller_._changeState_ != 1) {
							return;
						}
						var rec = this._controller_.getRecord();
						if (!Ext.isEmpty(rec) && this._controller_.getRecordStatus()==="insert") {
							if(!el){
								el = this._get_("priceBasis");
							}
							var decimals = this._get_("decimals");
							var operator = this._get_("operator");
							var quotUnit = this._get_("quotUnitCode");
							var factor = this._get_("factor");
							var convUnit = this._get_("convUnitCode");
							var used = this._get_("used");
							var density = this._get_("density");
							var density1 = this._get_("density1");
							var density2 = this._get_("density2");
			
							if (el.getValue() === "Index") {
								density.setValue("");
								used.setValue("");
							}
							else {				
									density.setValue(_SYSTEMPARAMETERS_.sysdens);							
									density1.setValue(_SYSTEMPARAMETERS_.sysweight);							
									density2.setValue(_SYSTEMPARAMETERS_.sysvol);							
									var fields  = [decimals,operator,quotUnit,factor,convUnit,used];
									for (var i = 0; i < fields.length; i++) {
										fields[i].setValue("");
									}					
							}
						}
						this._applyStates_(rec);				
	},
	
	_filterConvUnit_: function(el) {
		
						var s = el.store;
						s.clearFilter();
						
						var dc = this._controller_;
						var rec = dc.getRecord();
						var quotUnitTypeInd = rec.get("quotUnitTypeInd");
		
						s.filter(function(r) {
							var convUnitTypeInd = r.get("unitTypeInd");
							if ( convUnitTypeInd !== quotUnitTypeInd) {
								return convUnitTypeInd;
							}
						});
	}
});

/* ================= EDIT FORM: periodFromTo ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PricingBase_Dc$periodFromTo", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PricingBase_Dc$periodFromTo",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index'; } , allowBlank:false, labelAlign:"top"})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index'; } , allowBlank:false, labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:170, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["validFrom", "validTo"]);
	}
});

/* ================= EDIT FORM: benchmark ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PricingBase_Dc$benchmark", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PricingBase_Dc$benchmark",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"benchmarProviderkLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left"})
		.addTextField({ name:"benchmarkProvider", bind:"{d.benchmarkProvider}", dataIndex:"benchmarkProvider", noLabel: true, maxLength:64})
		.addLov({name:"quotFinancialSourceCode", bind:"{d.quotFinancialSourceCode}", dataIndex:"quotFinancialSourceCode", noEdit:true , noLabel: true, xtype:"cmm_UsedFinancialSourcesLov_Lov", maxLength:25, hidden:true,
			retFieldMapping: [{lovField:"id", dsField: "initialFinancialSourceId"} ,{lovField:"id", dsField: "financialSourceId"} ,{lovField:"name", dsField: "financialSourceName"} ,{lovField:"code", dsField: "financialSourceCode"} ],listeners:{
			change:{scope:this, fn:this._filterBenchmark_},
			expand:{scope:this, fn:this._filterDistinct_},
			fpchange:{scope:this, fn:this._enableDisableBenchmark_}
		}})
		.addDisplayFieldText({ name:"priceValueTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left"})
		.addRadioField({ xtype:"combo", name:"priceValueType", bind:"{d.priceValueType}", dataIndex:"priceValueType", noLabel: true, _elements_:[ {inputValue:__FMBAS_TYPE__.ValueType._HIGH_PRICE_}, {inputValue:__FMBAS_TYPE__.ValueType._LOW_PRICE_}, {inputValue:__FMBAS_TYPE__.ValueType._MEAN_PRICE_}, {inputValue:__FMBAS_TYPE__.ValueType._USER_CALCULATED_PRICE_}],listeners:{
			change:{scope:this, fn:this._filterBenchmark_}
		}})
		.addDisplayFieldText({ name:"averagingMethodLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left"})
		.addLov({name:"quotAvgMethodName", bind:"{d.quotAvgMethodName}", dataIndex:"quotAvgMethodName", allowBlank:false, width:407, noLabel: true, xtype:"cmm_UsedAverageMethodLov_Lov", maxLength:50,
			retFieldMapping: [{lovField:"id", dsField: "quotAvgMethodId"} ,{lovField:"name", dsField: "avgMethodIndicatorName"} ],listeners:{
			change:{scope:this, fn:this._filterBenchmark_},
			fpchange:{scope:this, fn:this._enableDisableBenchmark_}
		}})
		.addDisplayFieldText({ name:"exchangeRateOffsetLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left"})
		.addRadioField({ xtype:"combo", name:"quotationOffset", bind:"{d.quotationOffset}", dataIndex:"quotationOffset", noLabel: true, _elements_:[ {inputValue:__FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_}, {inputValue:__FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_}, {inputValue:__FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_}, {inputValue:__FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_}]})
		.addDisplayFieldText({ name:"benchmarkLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left"})
		.addLov({name:"quotTimeSeriesDescription", bind:"{d.quotTimeSeriesDescription}", dataIndex:"quotTimeSeriesDescription", allowBlank:false, width:407, noLabel: true, xtype:"fmbas_QuotDescriptionLov_Lov", maxLength:200, listeners:{afterrender: {scope: this, fn: function(el) {this._loadQuotations_(el)}}, fpchange: {scope: this, fn: function(el) {this._onQuotationChange_(el)}}}, _localCache_:true})
		.addDisplayFieldText({ name:"warning", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, cls:"sone-warning-label", hidden:true})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:420})
		.addPanel({ name:"r1", width:420, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["r1"])
		.addChildrenTo("r1", ["benchmarProviderkLabel", "benchmarkProvider", "quotFinancialSourceCode", "priceValueTypeLabel", "priceValueType", "averagingMethodLabel", "quotAvgMethodName", "exchangeRateOffsetLabel", "quotationOffset", "benchmarkLabel", "quotTimeSeriesDescription", "warning"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableDisableBenchmark_: function(el) {
		
						var quotTimeSeriesDescription = this._get_("quotTimeSeriesDescription");
						var quotFinancialSourceCode = this._get_("quotFinancialSourceCode");
						var ctrl = this._controller_;
						var r = ctrl.getRecord();
						var fields = ["quotAvgMethodName","quotFinancialSourceCode"];
						var disable = false;
						var fieldValues = [];
		
						var f = function() {
							for (var i = 0; i< fields.length; i++) {
								if (r && this._get_(fields[i])) {
									fieldValues.push(this._get_(fields[i]).getRawValue());
								}
							}
							if (fieldValues.indexOf(null) > -1 || fieldValues.indexOf("") > -1) {
								disable = true;
							}
							if (disable === true) {
								quotTimeSeriesDescription._disable_();
							}
							else {
								if (ctrl.readOnly === true) {
									quotTimeSeriesDescription._disable_();
									quotFinancialSourceCode._disable_();
								}
								else {
									quotTimeSeriesDescription._enable_();
									quotFinancialSourceCode._enable_();
								}
							}
						}
						Ext.defer(f,300,this);
						
	},
	
	_onQuotationChange_: function(el) {
		
						var selection = el.selection;
						var ctrl = this._controller_;
						var r = ctrl.getRecord();
						if (r && selection) {
							r.beginEdit();
							r.set("quotId",selection.get("id"));
							r.set("quotUnitCode",selection.get("unitCode"));
							r.set("quotUnitId",selection.get("unitId"));
							r.set("convUnitCode",selection.get("unit2Code"));
							r.set("convUnitId",selection.get("unit2Id"));
							r.set("quotUnitTypeInd",selection.get("unitTypeInd"));
							r.set("convUnitTypeInd",selection.get("unit2TypeInd"));
							r.set("quotName",selection.get("name"));
							r.set("quotValue",selection.get("calcVal"));
							r.set("factor",selection.get("arithmOper"));
							r.set("decimals",selection.get("decimals"));
							r.set("operator",selection.get("convFctr"));
							r.set("quotTimeSeriesId",selection.get("id"));
							r.endEdit();
						}
		
	},
	
	_filterDistinct_: function(el) {
		
						var store = el.store;
						var radioItems = this._radioItems_;
						store.on("load", function(store) {
				            store.filterBy(function(record) {
								var name = record.get("name");
								if(radioItems.indexOf(record.get("name")) > -1 ) {return false}else{return true}
				            });	
						}, this);
	},
	
	_filterBenchmark_: function(e,n,o) {
		
						var benchmark = this._get_("quotTimeSeriesDescription");
						var store = benchmark.store;
						var dataIndex = e.dataIndex;
						var ctx = this;
						var resetFn = function() {
							var ctrl = ctx._controller_;
							var r = ctrl.getRecord();
							if (r) {
								r.set("quotTimeSeriesDescription",null);
							}
						}
			
						if (dataIndex === "quotAvgMethodName") {
							dataIndex = "avgMethodIndicatorName";
							if (n !== o && !Ext.isEmpty(o)) {
								resetFn();
							}
						}
						if (dataIndex === "quotFinancialSourceCode") {
							dataIndex = "financialSourceCode";
						}
						var warning = this._get_("warning");
		
						store.filter(dataIndex, n);
						store.load();
						store.on("load", function(s) {
							var count = s.getCount();
							var benchmarkDescription = this._get_("quotTimeSeriesDescription");			
							if (count > 0) {
								warning.hide();
							}
							else {
								warning.show();
								resetFn();
							}					
						}, this);
	},
	
	_loadQuotations_: function(el) {
		
						var dc = this._controller_;
						var s = el.store;
						this._enableDisableBenchmark_();
						s.load();
	},
	
	_setFinancialSource_: function(el,n) {
		
						var financialSourceCode = this._get_("quotFinancialSourceCode");
						var dc = this._controller_;
						var fsStore = financialSourceCode.store;
					    var benchmarkValue = n[el.dataIndex];
						if (benchmarkValue !== "OTHERS") {
							financialSourceCode._disable_();
							financialSourceCode.setHidden(true);
							fsStore.load({
						        callback : function(records, operation, success) {
									financialSourceCode.setValue(benchmarkValue);
						        }
						    });
						}
						else {
							financialSourceCode.setValue(null);
							if (dc.readOnly === true) {
								financialSourceCode._disable_();
							}
							else {
								financialSourceCode._enable_();
							}
							
							financialSourceCode.setHidden(false);
						}
	},
	
	_afterDefineElements_: function() {
		
						var radioItems = [];
						var ctx = this;
						var ctrl = this._controller_;
						this._radioItems_ = [];
						this._getBuilder_().change("benchmarkProvider",{
							xtype: "radiogroup",
							listeners: {
								change: {
									scope: this,
									fn: function(e,n,o) {
										this._setFinancialSource_(e,n);
										this._enableDisableBenchmark_(e,n);
									}
								},
								beforerender: {
									scope: this,
									fn: function(el) {
										var ds = "cmm_UsedFinancialSourcesLov_Ds";
										var counter = 0;
										Ext.Ajax.request({
							            	url: Main.dsAPI(ds, "json").read,
							            	method: "POST",
							            	scope: this,
							            	success: function(response) {
												var data = Ext.getResponseDataInJSON(response).data;
												var dc = this._controller_;
												var r = dc.getRecord();
												Ext.each(data, function(d) {
													var checked = false;										
													if (counter<=2) {
														if (r) {
															var financialSourceCode = r.get("quotFinancialSourceCode");
															if (financialSourceCode === d.code) {
																checked = true;
															}
															if (financialSourceCode !== d.code && Ext.isEmpty(financialSourceCode)) {
																if (counter === 0) {
																	checked = true;
																}														
															}											
														}
														radioItems.push({
															boxLabel: d.code, inputValue: d.code, name: el.dataIndex, checked: checked, disabled: ctrl.readOnly
														});
														this._radioItems_.push(d.name);
													}						
													counter++;
												}, this);
												var othersChecked = true;
												Ext.each(radioItems, function(item) {
													if (item.checked === true) {
														othersChecked = false;
													}
												}, this);
												el.add(radioItems.concat({
													boxLabel: "OTHERS", inputValue: "OTHERS", name: el.dataIndex, checked: othersChecked, disabled: ctrl.readOnly
												}));
												el.fireEvent("itemsadded",el,radioItems.concat({
													boxLabel: "OTHERS", inputValue: "OTHERS", name: el.dataIndex, checked: othersChecked, disabled: ctrl.readOnly
												}));
											}
										});
									}
								},
								itemsadded: {
									scope: this,
									fn: function(el,items) {
										Ext.each(items,function(item) {
											if (item.checked === true) {
												var ctrl = ctx._controller_;
												var r = ctrl.getRecord();
												if (r && item.boxLabel !== "OTHERS") {
													r.set("quotFinancialSourceCode",item.boxLabel);
												}
												if (item.boxLabel === "OTHERS") {
													var financialSourceCode = this._get_("quotFinancialSourceCode");
													financialSourceCode._enable_();
													financialSourceCode.setHidden(false);
												}
											}
										},this);
									}
								}
							}
						});
						
	},
	
	_selectFirstValue_: function(el) {
		
						var f = function() {
							var firstItem = el.items.items[0];
							firstItem.setValue(true);
						}
						Ext.defer(f,300,this);
	}
});

/* ================= EDIT FORM: conversionFactor ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PricingBase_Dc$conversionFactor", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PricingBase_Dc$conversionFactor",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"benchmarkLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left"})
		.addTextField({ name:"quotUnitCode", bind:"{d.quotUnitCode}", dataIndex:"quotUnitCode", noEdit:true , _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index'; } , width:60, noLabel: true, maxLength:2, labelAlign:"top"})
		.addLov({name:"convUnitCode", bind:"{d.convUnitCode}", dataIndex:"convUnitCode", _enableFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotTimeSeriesDescription); } , allowBlank:false, width:60, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, labelAlign:"top",
			retFieldMapping: [{lovField:"id", dsField: "convUnitId"} ,{lovField:"unitTypeInd", dsField: "convUnitTypeInd"} ],listeners:{
			expand:{scope:this, fn:this._filterConvUnit_},
			blur:{scope:this, fn:function() {this._calculateDensity_(true)}}
		}})
		.addDisplayFieldText({ name:"conversionOperatorLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left;"})
		.addRadioField({ xtype:"combo", name:"operator", bind:"{d.operator}", dataIndex:"operator", _enableFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotTimeSeriesDescription); } , noLabel: true, _elements_:[ {inputValue:__FMBAS_TYPE__.Operator._MULTIPLY_}, {inputValue:__FMBAS_TYPE__.Operator._DIVIDE_}], listeners:{change: {scope: this, fn: function(el) {this._calculateDensity_(true);}}}})
		.addDisplayFieldText({ name:"conversionFactorLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left;"})
		.addNumberField({name:"factor", bind:"{d.factor}", dataIndex:"factor", _enableFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotTimeSeriesDescription); } , allowBlank:false, width:60, noLabel: true, maxLength:19, decimals:4,listeners:{
			blur:{scope:this, fn:function() {this._calculateDensity_(true)}}
		}})
		.addDisplayFieldText({ name:"conversionPreciosionLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left;"})
		.addDisplayFieldText({ name:"decimalsLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left", style:"margin-left:5px"})
		.addNumberField({name:"decimals", bind:"{d.decimals}", dataIndex:"decimals", _enableFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotTimeSeriesDescription); } , allowBlank:false, width:60, noLabel: true, cls:"number-with-spinner", minValue:0, maxValue:6, maxLength:0, listeners:{keydown: { scope: this, fn: function(field, e) { this._restrictInput_(field, e) }}}, enableKeyEvents:true})
		.add({name:"decimalsAndLabel", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("decimals"),this._getConfig_("decimalsLabel")]})
		.addDisplayFieldText({ name:"warning", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, cls:"sone-warning-label", hidden:true})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:220})
		.addPanel({ name:"r1", width:220, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["r1"])
		.addChildrenTo("r1", ["benchmarkLabel", "quotUnitCode", "convUnitCode", "conversionOperatorLabel", "operator", "conversionFactorLabel", "factor", "warning", "conversionPreciosionLabel", "decimalsAndLabel"]);
	},
	/* ==================== Business functions ==================== */
	
	_selectFirstValue_: function(el) {
		
						var f = function() {
							var firstItem = el.items.items[0];
							firstItem.setValue(true);
						}
						Ext.defer(f,300,this);
	},
	
	_restrictInput_: function(field,e) {
		
						var key = e.browserEvent.key;
						var keys = ["0","1","2","3","4","5","6","Backspace","ArrowLeft","ArrowRight","Tab"];
				        if (keys.indexOf(key) < 0) {
				            e.stopEvent();
				        }
	},
	
	_validateInput_: function() {
			
						var dc = this._controller_;
						var r = dc.getRecord();
						var field = this._get_("warning");
						if (r) {
							var conversionFactorValidationMsg = r.get("conversionFactorValidationMsg");
							if (!Ext.isEmpty(conversionFactorValidationMsg)) {
								field.setFieldLabel(conversionFactorValidationMsg);
								field.show();
							}
							else {						
								field.setFieldLabel(null);
								field.hide();
							}
						}
	},
	
	_calculateDensity_: function(validateInput) {
		
						var dc = this._controller_;
						var r = dc.getRecord();
						if (r) {
							var formName;
							if (Ext.isEmpty(r.get("id"))) {
								formName = "PbEdit";
							} else {
								formName = "PbEditFormula";
							}
							var factor = r.get("factor");
							if(factor != 0 && factor != null && factor != "0" && factor != ""){
								if (validateInput) {
									dc.fireEvent("itsTimeToCalculateDensity", this, {validateInput: true,formName: formName});
								}
								else {
									dc.fireEvent("itsTimeToCalculateDensity", this, {formName: formName});
								}						
							}
						}
	},
	
	_filterConvUnit_: function(el) {
		
						var s = el.store;
						s.clearFilter();
						
						var dc = this._controller_;
						var rec = dc.getRecord();
						var quotUnitTypeInd = rec.get("quotUnitTypeInd");
		
						s.filter(function(r) {
							var convUnitTypeInd = r.get("unitTypeInd");
							if ( convUnitTypeInd !== quotUnitTypeInd) {
								return convUnitTypeInd;
							}
						});
	}
});

/* ================= EDIT FORM: NewFormulaPrice ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PricingBase_Dc$NewFormulaPrice", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PricingBase_Dc$NewFormulaPrice",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btnTogglePeriod", scope: this, handler: this._toggle_, text: "...", style:"margin-left:3px"})
		.addButton({name:"btnToggleConversionFactor", scope: this, handler: this._toggleConversionfactor_, text: "...", style:"margin-left:3px"})
		.addButton({name:"btnToggleBenchmark", scope: this, handler: this._toggleBenchmark_, text: "...", style:"margin-left:3px"})
		.addTextField({ name:"description", bind:"{d.description}", dataIndex:"description", allowBlank:false, width:210, maxLength:100, labelAlign:"top"})
		.addLov({name:"priceCatName", bind:"{d.priceCatName}", dataIndex:"priceCatName", noEdit:true , _visibleFn_: function(dc, rec) { return false; } , allowBlank:false, xtype:"fmbas_PriceCategoriesLov_Lov", maxLength:32, labelWidth:100, pageSize:0,
			retFieldMapping: [{lovField:"id", dsField: "priceCatId"} ],listeners:{
			change:{scope:this, fn:this._enableDisableFields_},
			load:{scope:this, fn:this._enableDisableFields_}
		}})
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", allowBlank:false, labelWidth:120})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", allowBlank:false, labelWidth:80})
		.addDisplayFieldText({ name:"convFactor", bind:"{d.convFactor}", dataIndex:"convFactor", noEdit:true , maxLength:4000, labelWidth:110})
		.addTextField({ name:"quotUnitCode", bind:"{d.quotUnitCode}", dataIndex:"quotUnitCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index'; } , maxLength:2})
		.addLov({name:"convUnitCode", bind:"{d.convUnitCode}", dataIndex:"convUnitCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index'; } , width:100, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, labelWidth:30,
			retFieldMapping: [{lovField:"id", dsField: "convUnitId"} ,{lovField:"unitTypeInd", dsField: "convUnitTypeInd"} ],listeners:{
			blur:{scope:this, fn:this._calculateDensity_},
			expand:{scope:this, fn:this._filterConvUnit_}
		}})
		.addDisplayFieldText({ name:"conversionFromLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotUnitCode); } , maxLength:100})
		.addDisplayFieldText({ name:"conversionToLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.convUnitCode); } , maxLength:100})
		.addDisplayFieldText({ name:"multiplyLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.operator); } , maxLength:100})
		.addDisplayFieldText({ name:"usingLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.factor); } , maxLength:100})
		.addDisplayFieldText({ name:"decimalPrecisionLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.factor); } , maxLength:100})
		.addPopoverTextField({name:"conversionFactor", width:515, hPos:"r", vPos:"b", labelAlign:"top", fieldsList:["conversionFromLabel","quotUnitCode","conversionToLabel","convUnitCode","multiplyLabel","operator","factor","usingLabel","decimals","decimalPrecisionLabel"], formatList:[], editorForm:"cmm_PricingBase_Dc$conversionFactor", fieldsValueSeparator:" ", allowBlank:false})
		.addPopoverTextField({name:"periodFromTo", noLabel: true, hPos:"r", vPos:"b", dataType:"date", labelAlign:"top", fieldsList:["validFrom","validTo"], formatList:[], editorForm:"cmm_PricingBase_Dc$periodFromTo", fieldsValueSeparator:" - ", allowBlank:false})
		.add({name:"periodOfValidity", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("periodFromTo"),this._getConfig_("btnTogglePeriod")]})
		.addDisplayFieldText({ name:"periodLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left;"})
		.addDisplayFieldText({ name:"conversionFactorLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left;"})
		.add({name:"conversionFactorAndButton", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("conversionFactor"),this._getConfig_("btnToggleConversionFactor")]})
		.add({name:"priceName", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("priceCatName"),this._getConfig_("description")]})
		.add({name:"validityPeriod", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("periodOfValidity")]})
		.addTextField({ name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", maxLength:25})
		.addTextField({ name:"benchmarkProvider", bind:"{d.benchmarkProvider}", dataIndex:"benchmarkProvider", maxLength:64})
		.addTextField({ name:"quotValue", bind:"{d.quotValue}", dataIndex:"quotValue", allowBlank:false, maxLength:255})
		.addTextField({ name:"quotAvgMethodName", bind:"{d.quotAvgMethodName}", dataIndex:"quotAvgMethodName", allowBlank:false, maxLength:50})
		.addTextField({ name:"quotationOffset", bind:"{d.quotationOffset}", dataIndex:"quotationOffset", maxLength:32})
		.addTextField({ name:"quotTimeSeriesDescription", bind:"{d.quotTimeSeriesDescription}", dataIndex:"quotTimeSeriesDescription", maxLength:200})
		.addDisplayFieldText({ name:"commaLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotTimeSeriesDescription); } , maxLength:100})
		.addDisplayFieldText({ name:"secondCommaLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotAvgMethodName); } , maxLength:100})
		.addDisplayFieldText({ name:"benchmarkPeriodLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotationOffset); } , maxLength:100})
		.addPopoverTextField({name:"benchmark", width:515, hPos:"r", vPos:"b", labelAlign:"top", fieldsList:["quotTimeSeriesDescription","commaLabel","quotAvgMethodName","secondCommaLabel","quotationOffset","benchmarkPeriodLabel"], formatList:[], editorForm:"cmm_PricingBase_Dc$benchmark", fieldsValueSeparator:"", allowBlank:false})
		.addDisplayFieldText({ name:"benchmarkLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left;"})
		.add({name:"benchmarkAndButton", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("benchmark"),this._getConfig_("btnToggleBenchmark")]})
		.addCombo({ xtype:"combo", name:"operator", bind:"{d.operator}", dataIndex:"operator", _enableFn_: function(dc, rec) { return this._get_("priceCatName").getValue() === "Index"; } , width:150, store:[ __FMBAS_TYPE__.Operator._MULTIPLY_, __FMBAS_TYPE__.Operator._DIVIDE_], labelWidth:60,listeners:{
			fpchange:{scope:this, fn:this._calculateDensity_}
		}})
		.addNumberField({name:"factor", bind:"{d.factor}", dataIndex:"factor", _enableFn_: function(dc, rec) { return this._get_("priceCatName").getValue() === "Index"; } , width:130, maxLength:19, labelWidth:60, decimals:4,listeners:{
			blur:{scope:this, fn:this._calculateDensity_}
		}})
		.addNumberField({name:"decimals", bind:"{d.decimals}", dataIndex:"decimals", _enableFn_: function(dc, rec) { return this._get_("priceCatName").getValue() === "Index"; } , _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.factor); } , width:150, maxLength:3, labelWidth:100})
		.addTextField({ name:"used", bind:"{d.used}", dataIndex:"used", noEdit:true , width:150, maxLength:4000})
		.addNumberField({name:"density", bind:"{d.density}", dataIndex:"density", noEdit:true , width:150, sysDec:"dec_unit", maxLength:19, labelWidth:60})
		.addTextField({ name:"density1", bind:"{d.density1}", dataIndex:"density1", noEdit:true , width:60, maxLength:4000, labelWidth:1})
		.addTextField({ name:"density2", bind:"{d.density2}", dataIndex:"density2", noEdit:true , width:60, maxLength:4000, labelWidth:1})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"hiddenFields", width:560,  style:"display:none", layout:"anchor"})
		.addPanel({ name:"p1", width:560, layout:"anchor"})
		.addPanel({ name:"p2", width:560, layout:"anchor"})
		.addPanel({ name:"p3", width:560, layout:"anchor"})
		.addPanel({ name:"p4", width:560, layout:"anchor"})
		.addPanel({ name:"p5", width:560, layout:"anchor"})
		.addPanel({ name:"p6", width:560, layout:"anchor"})
		.addPanel({ name:"p7", width:560, layout:"anchor"})
		.addPanel({ name:"p8", width:560,  style:"display:none", layout:"anchor"})
		.addPanel({ name:"p9", width:560,  style:"display:none", layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["hiddenFields", "p1", "p2", "p3", "p4", "p5", "p6", "p7", "p8", "p9"])
		.addChildrenTo("hiddenFields", ["quotAvgMethodName", "conversionFromLabel", "conversionToLabel", "multiplyLabel", "usingLabel", "decimalPrecisionLabel", "commaLabel", "benchmarkPeriodLabel", "secondCommaLabel"])
		.addChildrenTo("p1", ["priceName"])
		.addChildrenTo("p2", ["periodLabel"])
		.addChildrenTo("p3", ["validityPeriod"])
		.addChildrenTo("p4", ["benchmarkLabel"])
		.addChildrenTo("p5", ["benchmarkAndButton"])
		.addChildrenTo("p6", ["conversionFactorLabel"])
		.addChildrenTo("p7", ["conversionFactorAndButton"])
		.addChildrenTo("p8", ["quotUnitCode", "operator", "factor", "convUnitCode", "decimals"])
		.addChildrenTo("p9", ["used", "density", "density1", "density2"]);
	},
	/* ==================== Business functions ==================== */
	
	_toggle_: function() {
		
						var f = this._get_("periodFromTo");
						f._showEditor_();
	},
	
	_toggleBenchmark_: function() {
		
						var f = this._get_("benchmark");
						f._showEditor_();
	},
	
	_toggleConversionfactor_: function() {
		
						var f = this._get_("conversionFactor");
						f._showEditor_();
	},
	
	_endDefine_: function() {
		
						this._changeCounter_ = 0;
	},
	
	_shouldValidate_: function() {
		
						return this._isVisible_;
	},
	
	_calculateDensity_: function() {
		
						var dc = this._controller_;
						var r = dc.getRecord();
						if (r) {
							var factor = r.get("factor");
							if(factor != 0 && factor != null && factor != "0" && factor != ""){
								dc.fireEvent("itsTimeToCalculateDensity", this);
							} 
						}
	},
	
	_enableDisableFields_: function(el) {
		
		
						if (this._controller_._changeState_ != 1) {
							return;
						}
						var rec = this._controller_.getRecord();
						if (!Ext.isEmpty(rec) && this._controller_.getRecordStatus()==="insert") {
							if(!el){
								el = this._get_("priceBasis");
							}
							var decimals = this._get_("decimals");
							var operator = this._get_("operator");
							var quotUnit = this._get_("quotUnitCode");
							var factor = this._get_("factor");
							var convUnit = this._get_("convUnitCode");
							var used = this._get_("used");
							var density = this._get_("density");
							var density1 = this._get_("density1");
							var density2 = this._get_("density2");
			
							if (el.getValue() === "Index") {
								density.setValue("");
								used.setValue("");
							}
							else {				
									density.setValue(_SYSTEMPARAMETERS_.sysdens);							
									density1.setValue(_SYSTEMPARAMETERS_.sysweight);							
									density2.setValue(_SYSTEMPARAMETERS_.sysvol);							
									var fields  = [decimals,operator,quotUnit,factor,convUnit,used];
									for (var i = 0; i < fields.length; i++) {
										fields[i].setValue("");
									}					
							}
						}
						this._applyStates_(rec);
	},
	
	_filterConvUnit_: function(el) {
		
						var s = el.store;
						s.clearFilter();
						
						var dc = this._controller_;
						var rec = dc.getRecord();
						var quotUnitTypeInd = rec.get("quotUnitTypeInd");
		
						s.filter(function(r) {
							var convUnitTypeInd = r.get("unitTypeInd");
							if ( convUnitTypeInd !== quotUnitTypeInd) {
								return convUnitTypeInd;
							}
						});
	}
});
