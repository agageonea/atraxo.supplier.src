/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.TenderLocationRound_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_TenderLocationRound_Ds"
	},
	
	
	fields: [
		{name:"biddingFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"biddingTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"roundNo", type:"int", allowNull:true},
		{name:"bidOpening", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"bidRoundActive", type:"boolean"},
		{name:"tenderLocationId", type:"int", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.TenderLocationRound_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"biddingFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"biddingTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"roundNo", type:"int", allowNull:true},
		{name:"bidOpening", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"bidRoundActive", type:"boolean", allowNull:true},
		{name:"tenderLocationId", type:"int", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
