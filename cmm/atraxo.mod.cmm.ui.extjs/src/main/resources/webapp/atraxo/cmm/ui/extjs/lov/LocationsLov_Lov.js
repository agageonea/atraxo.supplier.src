/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.lov.LocationsLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.cmm_LocationsLov_Lov",
	displayField: "code", 
	_columns_: ["code", "name"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{code}, {name}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	paramModel: atraxo.cmm.ui.extjs.ds.LocationsLov_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.LocationsLov_Ds
});
