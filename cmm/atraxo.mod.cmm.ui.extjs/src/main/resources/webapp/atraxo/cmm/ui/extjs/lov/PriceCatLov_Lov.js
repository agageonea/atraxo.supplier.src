/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.lov.PriceCatLov_Lov", {
	extend: "e4e.lov.LovAsgn",
	alias: "widget.cmm_PriceCatLov_Lov",
	displayField: "name", 
	_columns_: ["name"],
	delimiter: ",",
	listConfig: {
		getInnerTpl: function() {
			return '<span>{id}, {name}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.cmm.ui.extjs.ds.ContractPriceCategoryLov_Ds
});
