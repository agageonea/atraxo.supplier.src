/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdateCategories_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.PriceUpdateCategoriesList_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.PriceUpdateCategoriesList_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdateCategories_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.cmm_PriceUpdateCategories_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"priceCategory", dataIndex:"priceCategoryName", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.PriceCategoriesLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"name", dsField: "priceCategoryName"} ]}})
			.addLov({name:"location", dataIndex:"locationCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsAreasLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"code", dsField: "locationCode"} ]}})
			.addTextField({ name:"contract", dataIndex:"contract", maxLength:32})
			.addCombo({ xtype:"combo", name:"dealType", dataIndex:"dealType", store:[ __CMM_TYPE__.DealType._BUY_, __CMM_TYPE__.DealType._SELL_]})
			.addLov({name:"holder", dataIndex:"contractHolderCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SubsidiaryLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"code", dsField: "contractHolderCode"} ]}})
			.addLov({name:"counterparty", dataIndex:"counterpartyCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"code", dsField: "counterpartyCode"} ]}})
			.addNumberField({name:"price", dataIndex:"price", sysDec:"dec_crncy", maxLength:19})
			.addLov({name:"currency", dataIndex:"currencyCode", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"code", dsField: "currencyCode"} ]}})
			.addLov({name:"unit", dataIndex:"unitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MeasurmentUnitsLov_Lov", selectOnFocus:true, maxLength:2,
					retFieldMapping: [{lovField:"code", dsField: "unitCode"} ]}})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdateCategories_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_PriceUpdateCategories_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"priceCategory", dataIndex:"priceCategoryName", width:180})
		.addTextColumn({ name:"location", dataIndex:"locationCode", width:80})
		.addTextColumn({ name:"contract", dataIndex:"contract", width:80})
		.addTextColumn({ name:"dealType", dataIndex:"dealType", width:100})
		.addTextColumn({ name:"holder", dataIndex:"contractHolderCode", width:110})
		.addTextColumn({ name:"counterparty", dataIndex:"counterpartyCode", width:100})
		.addNumberColumn({ name:"price", dataIndex:"price", width:100, sysDec:"dec_crncy"})
		.addTextColumn({ name:"currency", dataIndex:"currencyCode", width:80})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:80})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
