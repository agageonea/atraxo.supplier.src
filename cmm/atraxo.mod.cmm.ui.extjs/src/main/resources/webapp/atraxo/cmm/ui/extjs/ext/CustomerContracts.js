
Ext.override(atraxo.fmbas.ui.extjs.frame.Customers_Ui, {
	
	_defineDcs_ : function() {
		this.callParent(arguments);
		// our stuff
		this._getBuilder_()
		.addDc("contract", Ext.create(atraxo.cmm.ui.extjs.dc.ContractCustomer_Dc, {}))
		.addDc("template", Ext.create(atraxo.cmm.ui.extjs.dc.Template_Dc,{ readOnly:true}))
		.linkDc("contract", "customer", {fetchMode : "auto",fields : [{childField : "customerId",parentField : "id"}]})
	},
	
	_defineElements_ : function() {
		this.callParent(arguments);

		this._getBuilder_()
			.addButton({name:"buttonOpenHelp", glyph:"xf059@FontAwesome", iconCls:"glyph-yellow", disabled:false, handler: this.onHelpWdw, scope:this})	
			.addButton({name:"buttonOpenContractHelp", glyph:"xf059@FontAwesome", iconCls:"glyph-yellow", disabled:false, handler: this.onContractHelpWdw, scope:this})	
			.addButton({name:"btnCancelCustomer", glyph:"xf05e@FontAwesome", iconCls:"glyph-red", disabled:false, handler: this.onCancel, scope:this})
			.addButton({name:"btnContinueCustomer", glyph:"xf064@FontAwesome", iconCls:"glyph-green", disabled:false, handler: this.onContinue,
					stateManager:{ name:"selected_one", dc:"template" }, scope:this})
			.addDcGridView("template", {name:"TempListCustomer", xtype:"cmm_Template_Dc$List"})
			.addDcGridView("contract", {name:"contractsList", _hasTitle_:true, xtype:"cmm_ContractCustomer_Dc$TabList"})
			.addDcFormView("contract", {name:"remarkReset", xtype:"cmm_ContractCustomer_Dc$Remark"})
			.addDcFilterFormView("contract", {name:"contractsFilter", xtype:"cmm_ContractCustomer_Dc$Filter"})			
			.addWindow({name:"wdwTempCustomer", _hasTitle_:true, width:420, height:360, closeAction:'hide', resizable:true, layout:"fit", modal:true,
				items:[this._elems_.get("TempListCustomer")],  closable:false, 
						dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
							items:[ this._elems_.get("btnContinueCustomer"), this._elems_.get("btnCancelCustomer")]}]})		
		
	},
	
	_afterDefineElements_ : function() {
		this.callParent(arguments);

		this._getBuilder_()
		.change("contractsList", {
			title: "Sales Contracts"
		})
		.change("buttonOpenHelp", {
			text: "Help",
			tooltip: "Open the contextual documentation"
		})		
		.change("buttonOpenContractHelp", {
			text: "Help",
			tooltip: "Open the contextual documentation"
		})
		.change("wdwTempCustomer", {
			title: "Select contract template",
			tooltip: "Reset"
		})
		.change("btnContinueCustomer", {
			text: "Continue",
			tooltip: "Continue with the definition of the contract header"
		})		
		.change("btnCancelCustomer", {
			text: "Cancel",
			tooltip: "Cancel all changes"
		})
		
	},
	
	_linkElements_: function() {
		this.callParent(arguments);
		this._getBuilder_()
		.addChildrenTo("detailsTab", ["contractsList"], null, 5)
		.addToolbarTo("contractsList", "tlbCustomerContract");
	},
	
	_defineToolbars_: function() {
		
		this.callParent(arguments);
		this._getBuilder_()
		.beginToolbar("tlbCustomerContract", {dc: "contract"})
			.addEdit({glyph:"xf040@FontAwesome",iconCls:"glyph-green"})
			.addDelete({glyph:"xf00d@FontAwesome",iconCls:"glyph-red"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("buttonOpenContractHelp") ])
			.addReports()
		.end();
		
	},
	
	_afterOnReady_ :  function() {
		//this._configureViewsAndFilters_();
		var dc = this._getDc_("contract");
		dc.on("onEditIn", function (dc) {
		    this.doEditContract();			
		}, this);
	},
	
	doEditContract: function() {
		
		var bundle = "atraxo.mod.cmm";
		var frame = "atraxo.cmm.ui.extjs.frame.ContractCustomer_Ui";
		var dc = this._getDc_("contract");
		var record = dc.getRecord();
		var contractId = record.get("id");
		var contractCode = record.get("code");
		
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				contractCode: contractCode
			},
			callback: function (params) {
				this._when_called_for_details_(params);
			}
		});
		
	},		
		
	onHelpWdw: function() {
		var url = Main.urlHelp+"/CustomerAccounts.html";
		window.open( url, "SONE_Help");
	},
	
	onContractHelpWdw: function() {		
		var url = Main.urlHelp+"/CustomerContracts.html";
		window.open( url, "SONE_Help");
	},
	
	_afterAfterDefineDcs_: function() {
		var contract = this._getDc_("contract");
		contract.on("afterDoNew", function (dc) {
			this.onShowContractWdw();
			contract.doCancel();
			return false;
				}, this);
	},
	
	_when_called_from_appMenu_ : function() {
		var detailsTab = this._get_("detailsTab");
		var contractsList = this._get_("contractsList");
		var dc = this._getDc_("contract");
		this._showStackedViewElement_("main", "canvas2");
		this.setActiveTabByTitle(detailsTab,contractsList);
		this.onShowContractWdw();
	},
	
	onShowContractWdw: function() {	
		this._getWindow_("wdwTempCustomer").show();
		this._getDc_("template").doQuery();
	},
	
	onCancel: function() {
		this._getDc_("contract").doCancel();
		this._getWindow_("wdwTempCustomer").close();
	},
	
	onContinue: function() {
		this._getWindow_("wdwTempCustomer").close();
		this.Cont();
	},
	
	Cont: function() {
		
		var tempList = this._get_("TempListCustomer");
	
		var selection = tempList.getSelectionModel().getSelection();
		var contractType = selection[0].data.contractType;
		var deliverySubType = selection[0].data.deliverySubtype;
		var scope = selection[0].data.scope;
		
		var bundle = "atraxo.mod.cmm";
		var frame = "atraxo.cmm.ui.extjs.frame.ContractCustomer_Ui";
		var dc = this._getDc_("contract");
		var record = dc.getRecord();
		
		var supplierRecord = this._getDc_("customer").getRecord();
		var counterPartyId = supplierRecord.get("id");
		var counterPartyCode = supplierRecord.get("code");
		var parentId = supplierRecord.get("parentId");
		
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				type: contractType,
				subType: deliverySubType,
				scope: scope,
				counterPartyId: counterPartyId,
				counterPartyCode: counterPartyCode,
				parentId: parentId
			},
			callback: function (params) {
				this._when_do_new_(params);
			}
		});

	}
});
