/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.TenderBid_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.TenderBid_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.TenderBid_Ds
});

/* ================= EDIT FORM: Export ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderBid_Dc$Export", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderBid_Dc$Export",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addCombo({ xtype:"combo", name:"exportType", bind:"{p.exportType}", paramIndex:"exportType", noLabel: true, store:[ __CMM_TYPE__.ExportTypeBid._PDF_, __CMM_TYPE__.ExportTypeBid._XML_, __CMM_TYPE__.ExportTypeBid._XLSX_],listeners:{
			change:{scope:this, fn:this._enableOkBtn_}
		}})
		.addDisplayFieldText({ name:"exportTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"row", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["row"])
		.addChildrenTo("row", ["exportTypeLabel", "exportType"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableOkBtn_: function() {
		
					var ctrl = this._controller_;
					var frame = ctrl.getFrame();
					var btn = frame._get_("btnOkExport");
					var exportType = this._get_("exportType").getValue();
					if(Ext.isEmpty(exportType)){
						btn.disable();
					} else {
						btn.enable();
					}
	}
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.cmm.ui.extjs.dc.TenderBid_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.cmm_TenderBid_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"tenderName", dataIndex:"tenderName", maxLength:100})
			.addTextField({ name:"code", dataIndex:"code", maxLength:32})
			.addLov({name:"tenderHolderCode", dataIndex:"tenderHolderCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeExtendedLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "tenderHolderId"} ]}})
			.addLov({name:"locCode", dataIndex:"locCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "locId"} ]}})
			.addLov({name:"country", dataIndex:"countryName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CountryNameLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "countryId"} ]}})
			.addDateField({name:"bidValidTo", dataIndex:"bidValidTo"})
			.addCombo({ xtype:"combo", name:"type", dataIndex:"type", store:[ __CMM_TYPE__.ContractType._PRODUCT_, __CMM_TYPE__.ContractType._FUELING_SERVICE_, __CMM_TYPE__.ContractType._GENERAL_SERVICE_, __CMM_TYPE__.ContractType._INSPECTION_, __CMM_TYPE__.ContractType._STORAGE_, __CMM_TYPE__.ContractType._CSO_]})
			.addCombo({ xtype:"combo", name:"flightServiceType", dataIndex:"flightServiceType", store:[ __CMM_TYPE__.FlightServiceType._AD_HOC_, __CMM_TYPE__.FlightServiceType._SCHEDULED_, __CMM_TYPE__.FlightServiceType._GENERAL_AVIATION_]})
			.addCombo({ xtype:"combo", name:"subType", dataIndex:"subType", store:[ __CMM_TYPE__.ContractSubType._NA_, __CMM_TYPE__.ContractSubType._INTO_PLANE_, __CMM_TYPE__.ContractSubType._EX_HYDRANT_, __CMM_TYPE__.ContractSubType._AIRLINE_SUPPLY_, __CMM_TYPE__.ContractSubType._INTO_STORAGE_, __CMM_TYPE__.ContractSubType._SERVICE_]})
			.addCombo({ xtype:"combo", name:"tenderStatus", dataIndex:"tenderStatus", store:[ __CMM_TYPE__.TenderStatus._DRAFT_, __CMM_TYPE__.TenderStatus._NEW_, __CMM_TYPE__.TenderStatus._CANCELED_, __CMM_TYPE__.TenderStatus._UPDATED_, __CMM_TYPE__.TenderStatus._EXPIRED_]})
			.addNumberField({name:"paymentTerms", dataIndex:"paymentTerms", maxLength:4})
			.addCombo({ xtype:"combo", name:"invoiceFreq", dataIndex:"invoiceFreq", store:[ __FMBAS_TYPE__.InvoiceFreq._WEEKLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIMONTHLY_, __FMBAS_TYPE__.InvoiceFreq._MONTHLY_, __FMBAS_TYPE__.InvoiceFreq._QUARTERLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._ANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._PER_DELIVERY_, __FMBAS_TYPE__.InvoiceFreq._DAILY_, __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_]})
			.addLov({name:"settlementCurrency", dataIndex:"settCurrCode", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"id", dsField: "settCurrId"} ],
					filterFieldMapping: [{lovField:"active", value: "true"} ]}})
			.addNumberField({name:"bidRevision", dataIndex:"bidRevision", maxLength:8})
			.addTextField({ name:"bidVersion", dataIndex:"bidVersion", maxLength:2})
			.addCombo({ xtype:"combo", name:"bidStatus", dataIndex:"bidStatus", store:[ __CMM_TYPE__.BidStatus._DRAFT_, __CMM_TYPE__.BidStatus._SUBMITTED_, __CMM_TYPE__.BidStatus._AWARDED_, __CMM_TYPE__.BidStatus._DECLINED_, __CMM_TYPE__.BidStatus._CANCELLED_, __CMM_TYPE__.BidStatus._AWARD_ACCEPTED_, __CMM_TYPE__.BidStatus._AWARD_DECLINED_]})
			.addCombo({ xtype:"combo", name:"approvalStatus", dataIndex:"bidApprovalStatus", store:[ __CMM_TYPE__.BidApprovalStatus._NEW_, __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_, __CMM_TYPE__.BidApprovalStatus._APPROVED_, __CMM_TYPE__.BidApprovalStatus._REJECTED_]})
			.addCombo({ xtype:"combo", name:"transmissionStatus", dataIndex:"bidTransmissionStatus", store:[ __CMM_TYPE__.TransmissionStatus._NEW_, __CMM_TYPE__.TransmissionStatus._IN_PROGRESS_, __CMM_TYPE__.TransmissionStatus._TRANSMITTED_, __CMM_TYPE__.TransmissionStatus._FAILED_, __CMM_TYPE__.TransmissionStatus._RECEIVED_, __CMM_TYPE__.TransmissionStatus._READ_, __CMM_TYPE__.TransmissionStatus._PROCESSED_]})
			.addCombo({ xtype:"combo", name:"bidCancelTransmissionStatus", dataIndex:"bidCancelTransmitionStatus", store:[ __CMM_TYPE__.TransmissionStatus._NEW_, __CMM_TYPE__.TransmissionStatus._IN_PROGRESS_, __CMM_TYPE__.TransmissionStatus._TRANSMITTED_, __CMM_TYPE__.TransmissionStatus._FAILED_, __CMM_TYPE__.TransmissionStatus._RECEIVED_, __CMM_TYPE__.TransmissionStatus._READ_, __CMM_TYPE__.TransmissionStatus._PROCESSED_]})
			.addCombo({ xtype:"combo", name:"bidAcceptAwardTransmissionStatus", dataIndex:"bidAcceptAwardTransmissionStatus", store:[ __CMM_TYPE__.TransmissionStatus._NEW_, __CMM_TYPE__.TransmissionStatus._IN_PROGRESS_, __CMM_TYPE__.TransmissionStatus._TRANSMITTED_, __CMM_TYPE__.TransmissionStatus._FAILED_, __CMM_TYPE__.TransmissionStatus._RECEIVED_, __CMM_TYPE__.TransmissionStatus._READ_, __CMM_TYPE__.TransmissionStatus._PROCESSED_]})
			.addTextField({ name:"bidPackageIdentifier", dataIndex:"bidPackageIdentifier", maxLength:100})
		;
	}

});

/* ================= GRID: TabList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderBid_Dc$TabList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_TenderBid_Dc$TabList",
	_noExport_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addBooleanColumn({ name:"marked", dataIndex:"marked", width:50,  renderer:function(val, meta, record, rowIndex) { return this._setCheckStatusColor_(val, meta, record); }})
		.addTextColumn({ name:"bidStatus", dataIndex:"bidStatus", width:140,  draggable:false, hideable:false, renderer:function(value, metaData) {return this._badgifyColumn_(value); }})
		.addTextColumn({ name:"tenderName", dataIndex:"tenderName", width:200})
		.addTextColumn({ name:"tenderHolderCode", dataIndex:"tenderHolderCode", width:140})
		.addTextColumn({ name:"locCode", dataIndex:"locCode", width:110})
		.addTextColumn({ name:"airlinesList", dataIndex:"airlinesList", width:120})
		.addTextColumn({ name:"countryName", dataIndex:"countryName", width:200})
		.addDateColumn({ name:"bidValidTo", dataIndex:"bidValidTo", width:140, _mask_: Masks.DATETIME})
		.addTextColumn({ name:"contractType", dataIndex:"type", hidden:true, width:90})
		.addTextColumn({ name:"flightServiceType", dataIndex:"flightServiceType", width:140})
		.addTextColumn({ name:"deliveryPoint", dataIndex:"subType", width:110})
		.addTextColumn({ name:"tenderStatus", dataIndex:"tenderStatus", hidden:true, width:110})
		.addTextColumn({ name:"bidLocBiddingStatus", dataIndex:"bidLocBiddingStatus", hidden:true, width:150})
		.addNumberColumn({ name:"paymentTerms", dataIndex:"paymentTerms", width:130})
		.addTextColumn({ name:"invoiceFreq", dataIndex:"invoiceFreq", width:140})
		.addTextColumn({ name:"settlementCurrency", dataIndex:"settCurrCode", width:140})
		.addNumberColumn({ name:"bidRound", dataIndex:"bidRound", width:100})
		.addNumberColumn({ name:"bidRevision", dataIndex:"bidRevision", width:100})
		.addTextColumn({ name:"bidVersion", dataIndex:"bidVersion", width:100})
		.addTextColumn({ name:"bidApprovalStatus", dataIndex:"bidApprovalStatus", width:140})
		.addTextColumn({ name:"bidTransmissionStatus", dataIndex:"bidTransmissionStatus", hidden:true, width:150})
		.addTextColumn({ name:"bidCancelTransmissionStatus", dataIndex:"bidCancelTransmitionStatus", hidden:true, width:200})
		.addTextColumn({ name:"bidAcceptAwardTransmissionStatus", dataIndex:"bidAcceptAwardTransmissionStatus", hidden:true, width:220})
		.addTextColumn({ name:"bidDeclineAwardTransmissionStatus", dataIndex:"bidDeclineAwardTransmissionStatus", hidden:true, width:220})
		.addTextColumn({ name:"bidPackageIdentifier", dataIndex:"bidPackageIdentifier", hidden:true, width:200})
		.addTextColumn({ name:"bidCode", dataIndex:"code", hidden:true, width:120})
		.addTextColumn({ name:"subsidiaryCode", dataIndex:"subsidiaryCode", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_badgifyColumn_: function(value) {
		
						var badgeCls = "sone-badge-cyan";
						if (value === __CMM_TYPE__.BidStatus._DRAFT_) {
							badgeCls = "sone-badge-default";
						}
						else if (value === __CMM_TYPE__.BidStatus._AWARDED_) {
							badgeCls = "sone-badge-yellow";
						}
						else if (value === __CMM_TYPE__.BidStatus._SUBMITTED_) {
							badgeCls = "sone-badge-blue";
						}				
						else if (value === __CMM_TYPE__.BidStatus._DECLINED_) {
							badgeCls = "sone-badge-red";
						}				
						else if (value === __CMM_TYPE__.BidStatus._AWARD_ACCEPTED_) {
							badgeCls = "sone-badge-green";
						}
						else if (value === __CMM_TYPE__.BidStatus._AWARD_DECLINED_) {
							badgeCls = "sone-badge-red";
						}
						else if (value === __CMM_TYPE__.BidStatus._CANCELLED_) {
							badgeCls = "sone-badge-red";
						}
						var badge = "<div class='sone-badge "+badgeCls+"'>"+value+"</div>";
						return badge;
	},
	
	_setCheckStatusColor_: function(val,meta,record) {
		
						var icon;
						if (val) {
							icon = "<div style='width:100%; text-align:center'><i class='fa fa-circle' style='color:#f65a84'></i></div>";
						}
						else {
							icon = "<div style='width:100%; text-align:center'><i class='fa fa-circle' style='color:#FFFFFF'></i></div>";
						}
						return icon;
	},
	
	_afterDefineElements_: function() {
		
						if(_SYSTEMPARAMETERS_.sysbidapproval === "false"){
							this._columns_.getByKey("bidApprovalStatus").hidden = true ;
						}
	},
	
	_afterInitComponent_: function() {
		
						var headerCt = this.getView().getHeaderCt();
						this._buildChildGrid_({
							childDc: "contractPriceCtgry",
							gridCls: atraxo.cmm.ui.extjs.dc.ContractPriceCategory_Dc$DetailsList,
							filterField: "id",
							applyFilter : false
						});	
		
	},
	
	_endDefine_: function() {
		
						this.plugins = [].concat(this.plugins?this.plugins:[],[
							{
								ptype: "gridfilters"
							},{
								ptype: "rowexpanderplus",
								pluginId : "rowexpanderplus",
								rowBodyTpl : new Ext.XTemplate(
									"<div id='rec-{id}'></div>"
								)
							}
						]);
						this.selType = "rowmodel"
	}
});

/* ================= EDIT FORM: bidValidityPeriodForm ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderBid_Dc$bidValidityPeriodForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderBid_Dc$bidValidityPeriodForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDateField({name:"bidValidFrom", bind:"{d.bidValidFrom}", dataIndex:"bidValidFrom", _enableFn_: this._enableIfStatus_, allowBlank:false, width:110, labelAlign:"top"})
		.addDateField({name:"bidValidTo", bind:"{d.bidValidTo}", dataIndex:"bidValidTo", _enableFn_: this._enableIfStatus_, allowBlank:false, width:110, labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:170, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["bidValidFrom", "bidValidTo"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableIfStatus_: function() {
		
						var dc 		= this._controller_;
						var record 	= dc.getRecord();
						var status 	= record.get("bidStatus");			
		
						return status === __CMM_TYPE__.BidStatus._DRAFT_;
	}
});

/* ================= EDIT FORM: contractValidityPeriodForm ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderBid_Dc$contractValidityPeriodForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderBid_Dc$contractValidityPeriodForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", _enableFn_: this._enableIfStatus_, allowBlank:false, width:110, labelAlign:"top"})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", _enableFn_: this._enableIfStatus_, allowBlank:false, width:110, labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:170, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["validFrom", "validTo"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableIfStatus_: function() {
		
						var dc 		= this._controller_;
						var record 	= dc.getRecord();
						var status 	= record.get("bidStatus");			
		
						return status === __CMM_TYPE__.BidStatus._DRAFT_;
	}
});

/* ================= EDIT FORM: Header ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderBid_Dc$Header", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderBid_Dc$Header",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"bidLineDelim", bind:"{d.lineDelim}", dataIndex:"lineDelim", maxLength:3, style:"padding-left:3px"})
		.addDisplayFieldText({ name:"contractLineDelim", bind:"{d.lineDelim}", dataIndex:"lineDelim", maxLength:3, style:"padding-left:3px"})
		.addDateField({name:"bidValidFrom", bind:"{d.bidValidFrom}", dataIndex:"bidValidFrom", _enableFn_: this._enableIfStatus_, allowBlank:false, width:110, noLabel: true})
		.addDateField({name:"bidValidTo", bind:"{d.bidValidTo}", dataIndex:"bidValidTo", _enableFn_: this._enableIfStatus_, allowBlank:false, width:110, noLabel: true})
		.addPopoverTextField({name:"bidValidityPeriod", hPos:"r", vPos:"b", fieldsList:["bidValidFrom","bidValidTo"], formatList:[], editorForm:"cmm_TenderBid_Dc$bidValidityPeriodForm", fieldsValueSeparator:" - "})
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", _enableFn_: this._enableIfStatus_, allowBlank:false, width:110, noLabel: true})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", _enableFn_: this._enableIfStatus_, allowBlank:false, width:110, noLabel: true})
		.addPopoverTextField({name:"contractValidityPeriod", hPos:"r", vPos:"b", fieldsList:["validFrom","validTo"], formatList:[], editorForm:"cmm_TenderBid_Dc$contractValidityPeriodForm", fieldsValueSeparator:" - "})
		.addNumberField({name:"iataServiceLevel", bind:"{d.iataServiceLevel}", dataIndex:"iataServiceLevel", _enableFn_: this._enableIfStatus_, width:45, noLabel: true, maxLength:1, cls:"number-with-spinner", maxValue:4, minValue:1})
		.addCombo({ xtype:"combo", name:"flightServiceType", bind:"{d.flightServiceType}", dataIndex:"flightServiceType", noEdit:true , width:120, noLabel: true, store:[ __CMM_TYPE__.FlightServiceType._AD_HOC_, __CMM_TYPE__.FlightServiceType._SCHEDULED_, __CMM_TYPE__.FlightServiceType._GENERAL_AVIATION_]})
		.addCombo({ xtype:"combo", name:"tax", bind:"{d.tax}", dataIndex:"tax", noEdit:true , width:120, noLabel: true, store:[ __CMM_TYPE__.TaxType._UNSPECIFIED_, __CMM_TYPE__.TaxType._BONDED_, __CMM_TYPE__.TaxType._DOMESTIC_, __CMM_TYPE__.TaxType._FOREIGN_TRADE_ZONE_, __CMM_TYPE__.TaxType._DUTY_FREE_, __CMM_TYPE__.TaxType._DUTY_PAID_, __CMM_TYPE__.TaxType._OTHER_]})
		.addTextField({ name:"bidPackageIdentifier", bind:"{d.bidPackageIdentifier}", dataIndex:"bidPackageIdentifier", _enableFn_: this._enableIfStatus_, noLabel: true, maxLength:100})
		.addLov({name:"refueler", bind:"{d.iplCode}", dataIndex:"iplCode", _enableFn_: this._enableIfStatus_, width:100, noLabel: true, xtype:"fmbas_SupplierCodeLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "iplId"} ]})
		.addLov({name:"contact", bind:"{d.contactName}", dataIndex:"contactName", _enableFn_: this._enableIfStatus_, noLabel: true, xtype:"fmbas_ContactsLov_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "contactId"} ],
			filterFieldMapping: [{lovField:"objectId", dsField: "customerId"}, {lovField:"objectType", value: "Customer"} ]})
		.addLov({name:"responsibleBuyer", bind:"{d.resBuyerName}", dataIndex:"resBuyerName", noLabel: true, xtype:"fmbas_UserNameLov_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "resBuyerId"} ,{lovField:"userCode", dsField: "resBuyerCode"} ]})
		.addTextField({ name:"bidTitleTransfer", bind:"{d.bidTitleTransfer}", dataIndex:"bidTitleTransfer", _enableFn_: this._enableIfStatus_, width:150, noLabel: true, maxLength:100})
		.addTextField({ name:"bidOperatingHours", bind:"{d.bidOperatingHours}", dataIndex:"bidOperatingHours", _enableFn_: this._enableIfStatus_, width:150, noLabel: true, maxLength:100})
		.addTextField({ name:"bidAstmSpecification", bind:"{d.bidAstmSpecification}", dataIndex:"bidAstmSpecification", _enableFn_: this._enableIfStatus_, allowBlank:false, width:150, noLabel: true, maxLength:20})
		.addTextField({ name:"bidVersion", bind:"{d.bidVersion}", dataIndex:"bidVersion", noEdit:true , width:45, noLabel: true, maxLength:2})
		.addNumberField({name:"bidRevision", bind:"{d.bidRevision}", dataIndex:"bidRevision", noEdit:true , width:45, noLabel: true, maxLength:8})
		.addDisplayFieldText({ name:"bidValidLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"contractValidLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"taxLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"flightServiceTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"bidPackageIdentifierLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"refuelerLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"holderContactLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"responsibleBuyerLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding-left:30px; float:right"})
		.addDisplayFieldText({ name:"titleTransferLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding-left:30px; float:right"})
		.addDisplayFieldText({ name:"operatingHoursLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding-left:30px; float:right"})
		.addDisplayFieldText({ name:"astmSpecLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding-left:30px; float:right"})
		.addDisplayFieldText({ name:"iataServiceLevelLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding-left:30px; float:right"})
		.addDisplayFieldText({ name:"bidVersionLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"bidRevisionLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"formTitle", bind:"{d.formTitle}", dataIndex:"formTitle", noEdit:true , maxLength:16})
		.addDisplayFieldText({ name:"holder", bind:"{d.customerCode}", dataIndex:"customerCode", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"type", bind:"{d.type}", dataIndex:"type", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"delivery", bind:"{d.subType}", dataIndex:"subType", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"productDisplay", bind:"{d.product}", dataIndex:"product", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"requiredVolume", bind:"{d.requiredVolumeTitle}", dataIndex:"requiredVolumeTitle", noEdit:true , maxLength:25})
		.addDisplayFieldText({ name:"bidLocVolUnitCode", bind:"{d.bidLocVolUnitCode}", dataIndex:"bidLocVolUnitCode", noEdit:true , noLabel: true, maxLength:2})
		.addDisplayFieldText({ name:"periodDisplay", bind:"{d.period}", dataIndex:"period", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"bidStatus", bind:"{d.bidStatus}", dataIndex:"bidStatus", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"space", bind:"{d.space}", dataIndex:"space", noLabel: true, maxLength:1, xtype:"percent", style:"padding-left:3px"})
		.add({name:"row", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("formTitle")]})
		/* =========== containers =========== */
		.addPanel({ name:"c1", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c2", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c3", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c4", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c5", width:120, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c6", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c7", width:300, defaults: { labelAlign:"top", cls:"sone-highlight-kpi"}, layout:"anchor"})
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"titleAndKpi",  style:"margin-bottom:10px;", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"title", width:300,  cls:"sone-title-panel", layout:"anchor"})
		.addPanel({ name:"p2",  cls:"sone-kpi-panel", layout: {type:"table"}})
		.addPanel({ name:"table", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3", layout: {type:"table", columns:2}})
		.addPanel({ name:"table4", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("c1", ["holder"])
		.addChildrenTo("c2", ["type"])
		.addChildrenTo("c3", ["delivery"])
		.addChildrenTo("c4", ["productDisplay"])
		.addChildrenTo("c5", ["requiredVolume"])
		.addChildrenTo("c6", ["periodDisplay"])
		.addChildrenTo("c7", ["bidStatus"])
		.addChildrenTo("main", ["titleAndKpi", "table"])
		.addChildrenTo("titleAndKpi", ["title", "p2"])
		.addChildrenTo("title", ["row"])
		.addChildrenTo("p2", ["c1", "c2", "c3", "c4", "c5", "c6", "c7"])
		.addChildrenTo("table", ["table1", "table2", "table3", "table4"])
		.addChildrenTo("table1", ["bidValidLabel", "bidValidityPeriod", "contractValidLabel", "contractValidityPeriod", "taxLabel", "tax", "flightServiceTypeLabel", "flightServiceType"])
		.addChildrenTo("table2", ["bidVersionLabel", "bidVersion", "bidRevisionLabel", "bidRevision", "iataServiceLevelLabel", "iataServiceLevel", "bidPackageIdentifierLabel", "bidPackageIdentifier"])
		.addChildrenTo("table3", ["refuelerLabel", "refueler", "titleTransferLabel", "bidTitleTransfer", "operatingHoursLabel", "bidOperatingHours", "astmSpecLabel", "bidAstmSpecification"])
		.addChildrenTo("table4", ["holderContactLabel", "contact", "responsibleBuyerLabel", "responsibleBuyer"]);
	},
	/* ==================== Business functions ==================== */
	
	_afterDefineElements_: function() {
		
						this._getBuilder_().change("titleAndKpi",{
							layout: {
						        type: "hbox",
						        align: "stretch"
						    },
						    defaults: {
						        flex: 1,
								height: 60
						    }
						});
						
	},
	
	_afterApplyStates_: function() {
		
		                var field = this._controller_.getRecord();
		                if(field){
		                    var locCode = field.get("locCode");
		                    var locAirportName = field.get("locAirportName");
		                    var formTitle = this._get_("formTitle");                
		                    var originalTitleLabel = formTitle.fieldLabel;
							if (formTitle.labelEl) {
								formTitle.labelEl.update("<i class='fa fa-legal'></i> " + originalTitleLabel + " : " + locCode + " , " + locAirportName);
							}
		                }
	},
	
	_enableIfStatus_: function() {
		
						var dc 		= this._controller_;
						var record 	= dc.getRecord();
						var status 	= record.get("bidStatus");			
		
						return status === __CMM_TYPE__.BidStatus._DRAFT_;
	}
});

/* ================= EDIT FORM: Volumes ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderBid_Dc$Volumes", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderBid_Dc$Volumes",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addNumberField({name:"awardedVolume", bind:"{d.awardedVolume}", dataIndex:"awardedVolume", _enableFn_: this._enableAwardedVolume_, width:120, noLabel: true, sysDec:"dec_unit", maxLength:19,listeners:{
			fpchange:{scope:this, fn:this._calculatePercentAwarded_}
		}})
		.addNumberField({name:"offeredVolume", bind:"{d.offeredVolume}", dataIndex:"offeredVolume", _enableFn_: this._enableOfferedVolume_, width:120, noLabel: true, sysDec:"dec_unit", maxLength:19,listeners:{
			fpchange:{scope:this, fn:this._calculatePercentOffered_}
		}})
		.addTextField({ name:"settUnitAwarded", bind:"{d.settUnitCode}", dataIndex:"settUnitCode", noEdit:true , width:40, noLabel: true, maxLength:2})
		.addTextField({ name:"settUnitOffered", bind:"{d.settUnitCode}", dataIndex:"settUnitCode", noEdit:true , width:40, noLabel: true, maxLength:2})
		.addCombo({ xtype:"combo", name:"period", bind:"{d.period}", dataIndex:"period", _enableFn_: this._enableIfStatus_, allowBlank:false, width:130, noLabel: true, store:[ __CMM_TYPE__.Period._CONTRACT_PERIOD_, __CMM_TYPE__.Period._MONTHLY_, __CMM_TYPE__.Period._YEARLY_]})
		.addNumberField({name:"tolerance", bind:"{d.volumeTolerance}", dataIndex:"volumeTolerance", _enableFn_: this._enableIfStatus_, width:60, noLabel: true, sysDec:"dec_prc", maxLength:4,listeners:{
			fpchange:{scope:this, fn:function() {this._validatePercent_(this._get_('tolerance'))}}
		}})
		.addCombo({ xtype:"combo", name:"quantityType", bind:"{d.quantityType}", dataIndex:"quantityType", _enableFn_: this._enableIfStatus_, width:120, noLabel: true, store:[ __CMM_TYPE__.QuantityType._NET_VOLUME_, __CMM_TYPE__.QuantityType._GROSS_VOLUME_]})
		.addDisplayFieldText({ name:"percent", bind:"{d.percent}", dataIndex:"percent", maxLength:1, xtype:"percent", style:"padding-left:3px"})
		.addNumberField({name:"percentCoveredAwarded", bind:"{d.percentCoveredAwarded}", dataIndex:"percentCoveredAwarded", noEdit:true , width:60, noLabel: true, sysDec:"dec_prc", maxLength:19})
		.addNumberField({name:"percentCoveredOffered", bind:"{d.percentCoveredOffered}", dataIndex:"percentCoveredOffered", noEdit:true , width:60, noLabel: true, sysDec:"dec_prc", maxLength:19})
		.addDisplayFieldText({ name:"percentCoveredLabelAwarded", bind:"{d.percentCoveredLabel}", dataIndex:"percentCoveredLabel", noEdit:true , maxLength:100, labelStyle:"right", style:"padding-left:3px"})
		.addDisplayFieldText({ name:"percentCoveredLabelOffered", bind:"{d.percentCoveredLabel}", dataIndex:"percentCoveredLabel", noEdit:true , maxLength:100, labelStyle:"right", style:"padding-left:3px"})
		.addDisplayFieldText({ name:"awardedVolumeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"offeredVolumeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right ; padding-right:7px"})
		.addDisplayFieldText({ name:"periodLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"toleranceLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"quantityTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:10px"})
		.add({name:"awardedVolWithUnit", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("awardedVolumeLabel"),this._getConfig_("awardedVolume"),this._getConfig_("settUnitAwarded"),this._getConfig_("percentCoveredAwarded"),this._getConfig_("percentCoveredLabelAwarded")]})
		.add({name:"offeredVolWithUnit", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("offeredVolumeLabel"),this._getConfig_("offeredVolume"),this._getConfig_("settUnitOffered"),this._getConfig_("percentCoveredOffered"),this._getConfig_("percentCoveredLabelOffered")]})
		.add({name:"toleranceWithPercent", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("tolerance"),this._getConfig_("percent")]})
		.add({name:"quantity", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("quantityTypeLabel"),this._getConfig_("quantityType")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"table"}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", layout: {type:"table", columns:1}})
		.addPanel({ name:"col2", layout: {type:"table", columns:3}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["awardedVolWithUnit", "offeredVolWithUnit"])
		.addChildrenTo("col2", ["periodLabel", "period", "quantity", "toleranceLabel", "toleranceWithPercent"]);
	},
	/* ==================== Business functions ==================== */
	
	_calculatePercentAwarded_: function() {
		
						var result = false;
						var dc = this._controller_;
						var record = dc.getRecord();
						if(record){
							var awardedVolume = record.get("awardedVolume");
		                	var bidLocVolume = record.get("bidLocVolume");
							var settUnitCode = record.get("settUnitCode");
							var bidLocVolUnitCode = record.get("bidLocVolUnitCode");
							var app = getApplication();
							var convBidLocVolume = app.convertUnit(bidLocVolUnitCode,settUnitCode,bidLocVolume);
							var percentage = (awardedVolume / convBidLocVolume) * 100;
		
							//Set format to system decimals
							percentage = Ext.util.Format.number(percentage, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_prc));
							record.set("percentCoveredAwarded", percentage);
						}
	},
	
	_calculatePercentOffered_: function() {
		
						var result = false;
						var dc = this._controller_;
						var record = dc.getRecord();
						if(record){
							var offeredVolume = record.get("offeredVolume");
		                	var bidLocVolume = record.get("bidLocVolume");
							var settUnitCode = record.get("settUnitCode");
							var bidLocVolUnitCode = record.get("bidLocVolUnitCode");
							var app = getApplication();
							var convBidLocVolume = app.convertUnit(bidLocVolUnitCode,settUnitCode,bidLocVolume);
							var percentage = (offeredVolume / convBidLocVolume) * 100;
		
							//Set format to system decimals
							percentage = Ext.util.Format.number(percentage, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_prc));
							record.set("percentCoveredOffered", percentage);
						}
	},
	
	_validatePercent_: function(el) {
		
						var value = el.getValue();
						if (isNaN(value) || value < 0 || value > 100) {
							Main.warning("Value is out of range! The value must be within 0 and 100");
						    el.setValue("");
						}
	},
	
	_enableIfStatus_: function() {
		
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var status = record.get("bidStatus");				
						return status === __CMM_TYPE__.BidStatus._DRAFT_;
	},
	
	_enableAwardedVolume_: function() {
		
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var bidStatus = record.get("bidStatus");
						var approvalStatus = record.get("bidApprovalStatus");
						var hasTotalVolume = record.get("hasTotalVolume");
						var tenderSource = record.get("tenderSource");
		
						if(tenderSource === __CMM_TYPE__.TenderSource._CAPTURED_ && bidStatus === __CMM_TYPE__.BidStatus._DRAFT_ && hasTotalVolume){
							if(_SYSTEMPARAMETERS_.sysbidapproval === "true" && approvalStatus === __CMM_TYPE__.BidApprovalStatus._APPROVED_){
								return true;
							}
		
							return true;
						}
		 
						return false;
	},
	
	_enableOfferedVolume_: function() {
		
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var status = record.get("bidStatus");		
						var hasTotalVolume = record.get("hasTotalVolume");
						return status === __CMM_TYPE__.BidStatus._DRAFT_ && hasTotalVolume;
	}
});

/* ================= EDIT FORM: PaymentTerms ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderBid_Dc$PaymentTerms", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderBid_Dc$PaymentTerms",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"exchangeRate", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, labelWidth:120, labelAlign:"left", colspan:8})
		.addLov({name:"settCurr", bind:"{d.settCurrCode}", dataIndex:"settCurrCode", _enableFn_: this._enableIfStatus_, allowBlank:false, width:60, noLabel: true, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3,
			retFieldMapping: [{lovField:"id", dsField: "settCurrId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ],listeners:{
			fpchange:{scope:this, fn:this._enableFs_}
		}})
		.addLov({name:"settUnit", bind:"{d.settUnitCode}", dataIndex:"settUnitCode", _enableFn_: this._enableIfStatus_, allowBlank:false, width:60, noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "settUnitId"} ]})
		.addCombo({ xtype:"combo", name:"paymentType", bind:"{d.bidPaymentType}", dataIndex:"bidPaymentType", _enableFn_: this._enableIfStatus_, allowBlank:false, width:100, noLabel: true, store:[ __CMM_TYPE__.PaymentType._CONTRACT_, __CMM_TYPE__.PaymentType._FUEL_CARD_, __CMM_TYPE__.PaymentType._CREDIT_CARD_, __CMM_TYPE__.PaymentType._CASH_]})
		.addCombo({ xtype:"combo", name:"vat", bind:"{d.vat}", dataIndex:"vat", _enableFn_: this._enableIfStatus_, allowBlank:false, width:150, noLabel: true, store:[ __CMM_TYPE__.VatApplicability._ALL_EVENTS_, __CMM_TYPE__.VatApplicability._DOMESTIC_EVENTS_, __CMM_TYPE__.VatApplicability._INTERNATIONAL_EVENTS_, __CMM_TYPE__.VatApplicability._NOT_APPLICABLE_]})
		.addNumberField({name:"paymentTerms", bind:"{d.paymentTerms}", dataIndex:"paymentTerms", _enableFn_: this._enableIfStatus_, allowBlank:false, width:60, noLabel: true, maxLength:4})
		.addCombo({ xtype:"combo", name:"paymentRefDay", bind:"{d.paymentRefDay}", dataIndex:"paymentRefDay", _enableFn_: this._enableIfStatus_, allowBlank:false, noLabel: true, store:[ __FMBAS_TYPE__.PaymentDay._INVOICE_DATE_, __FMBAS_TYPE__.PaymentDay._INVOICE_RECEIVING_DATE_, __FMBAS_TYPE__.PaymentDay._LAST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._FIRST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._MIDPOINT_DELIVERY_DATE_]})
		.addCombo({ xtype:"combo", name:"invoiceFreq", bind:"{d.invoiceFreq}", dataIndex:"invoiceFreq", _enableFn_: this._enableIfStatus_, allowBlank:false, width:115, noLabel: true, store:[ __FMBAS_TYPE__.InvoiceFreq._WEEKLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIMONTHLY_, __FMBAS_TYPE__.InvoiceFreq._MONTHLY_, __FMBAS_TYPE__.InvoiceFreq._QUARTERLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._ANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._PER_DELIVERY_, __FMBAS_TYPE__.InvoiceFreq._DAILY_, __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_],listeners:{
			expand:{scope:this, fn:this._filterInvoiceFreq_}
		}})
		.addCombo({ xtype:"combo", name:"invoiceType", bind:"{d.invoiceType}", dataIndex:"invoiceType", _enableFn_: this._enableIfStatus_, allowBlank:false, width:120, noLabel: true, store:[ __FMBAS_TYPE__.InvoiceType._XML_2_0_2_, __FMBAS_TYPE__.InvoiceType._XML_3_0_0_, __FMBAS_TYPE__.InvoiceType._XML_3_0_1_, __FMBAS_TYPE__.InvoiceType._XML_3_1_0_, __FMBAS_TYPE__.InvoiceType._EXCEL_, __FMBAS_TYPE__.InvoiceType._PAPER_, __FMBAS_TYPE__.InvoiceType._EDI_, __FMBAS_TYPE__.InvoiceType._PDF_FORMAT_, __FMBAS_TYPE__.InvoiceType._FISCAL_, __FMBAS_TYPE__.InvoiceType._EXPORT_, __FMBAS_TYPE__.InvoiceType._REGULAR_]})
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", _enableFn_: this._enableIfStatus_, allowBlank:false, width:100, noLabel: true, xtype:"fmbas_ExchangeRateToFinancialSourceAndAvgFilteredLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"} ],
			filterFieldMapping: [{lovField:"currencyId", dsField: "settCurrId"} ],listeners:{
			fpchange:{scope:this, fn:this._enableAvgMethods_},
			expand:{scope:this, fn:this._filterDistinct_}
		}})
		.addLov({name:"avgMethodName", bind:"{d.avgMethodName}", dataIndex:"avgMethodName", _enableFn_: this._enableIfFinancialSource_, allowBlank:false, noLabel: true, xtype:"fmbas_ExchangeRateToAvgMthdFilteredLov_Lov", maxLength:50,
			retFieldMapping: [{lovField:"avgMthdId", dsField: "avgMethodId"} ],
			filterFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"}, {lovField:"currencyId", dsField: "settCurrId"} ],listeners:{
			expand:{scope:this, fn:this._filterDistinctAvg_}
		}})
		.addCombo({ xtype:"combo", name:"period", bind:"{d.exchangeRateOffset}", dataIndex:"exchangeRateOffset", _enableFn_: this._enableIfStatus_, allowBlank:false, width:100, noLabel: true, store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_], labelWidth:150})
		.addDisplayFieldText({ name:"settCurrLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"settUnitLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"paymentTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"vatLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"paymentTermsLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right;"})
		.addDisplayFieldText({ name:"paymentRefDayLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"invoiceFreqLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"invoiceTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"financialSourceCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right;"})
		.addDisplayFieldText({ name:"avgMethodNameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"periodLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"table"}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", layout: {type:"table", columns:8}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["settCurrLabel", "settCurr", "settUnitLabel", "settUnit", "paymentTypeLabel", "paymentType", "vatLabel", "vat", "paymentTermsLabel", "paymentTerms", "paymentRefDayLabel", "paymentRefDay", "invoiceFreqLabel", "invoiceFreq", "invoiceTypeLabel", "invoiceType", "exchangeRate", "financialSourceCodeLabel", "financialSourceCode", "avgMethodNameLabel", "avgMethodName", "periodLabel", "period"]);
	},
	/* ==================== Business functions ==================== */
	
	_filterDistinct_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("financialSourceName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	},
	
	_filterDistinctAvg_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("avgMthdName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	},
	
	_enableFs_: function(el) {
		
						if (el._fpRawValue_ != el._onFocusVal_) {
							if( this._enableIfStatus_() ){
				                var financialSourceCode = this._get_("financialSourceCode");
				                var elVal = el.getValue();
				                if (!Ext.isEmpty(elVal)) {
				                    financialSourceCode.setReadOnly(false);
				                }
				                else {
				                    financialSourceCode.setReadOnly(true);
				                }
				                financialSourceCode.setValue("");
			                }
						}
						
	},
	
	_enableAvgMethods_: function(el) {
		
						if (el._fpRawValue_ != el._onFocusVal_) {
							if( this._enableIfStatus_() ){
								var avgMethodName = this._get_("avgMethodName");
								var elVal = el.getValue(); 
								if (!Ext.isEmpty(elVal)) {
									avgMethodName.setReadOnly(false);
								}
								else {
									avgMethodName.setReadOnly(true);
								}
								avgMethodName.setValue("");
							}
						}
	},
	
	_enableIfStatus_: function() {
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = true;
		
						if (record) {
		
							var bidStatus = record.get("bidStatus");
		
							if (bidStatus != __CMM_TYPE__.BidStatus._DRAFT_) {
								result = false;
							}
						}
		
						return result;
	},
	
	_enableIfFinancialSource_: function() {
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = true;
		
						if (record) {
							var financialSourceCode = record.get("financialSourceCode");
							var bidStatus = record.get("bidStatus");
		
							if (Ext.isEmpty(financialSourceCode) || (bidStatus != __CMM_TYPE__.BidStatus._DRAFT_)) {
								result = false;
							}
						}
		
						return result;
	},
	
	_filterInvoiceFreq_: function(el) {
		
						var s = el.store;
						s.filter(function(r) {
							var value = r.get("field1");
							if ( value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_ && value !== __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_) {
								return value;
							}
						});
	}
});

/* ================= EDIT FORM: CreditTerms ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderBid_Dc$CreditTerms", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderBid_Dc$CreditTerms",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addCombo({ xtype:"combo", name:"creditTerms", bind:"{d.creditTerms}", dataIndex:"creditTerms", _enableFn_: this._enableIfStatus_, allowBlank:false, width:130, noLabel: true, store:[ __FMBAS_TYPE__.CreditTerm._OPEN_CREDIT_, __FMBAS_TYPE__.CreditTerm._CREDIT_LINE_, __FMBAS_TYPE__.CreditTerm._BANK_GUARANTEE_, __FMBAS_TYPE__.CreditTerm._PREPAYMENT_],listeners:{
			change:{scope:this, fn:this._showFields_}
		}})
		.addBooleanField({ name:"bankGuarantees", bind:"{d.bidBankGuarantee}", dataIndex:"bidBankGuarantee", _enableFn_: this._enableIfStatus_, width:80, noLabel: true, colspan:3})
		.addNumberField({name:"prepaidDays", bind:"{d.bidPrepaidDays}", dataIndex:"bidPrepaidDays", _enableFn_: this._enableIfStatus_, width:50, noLabel: true, maxLength:4})
		.addCombo({ xtype:"combo", name:"paymentFreq", bind:"{d.bidPayementFreq}", dataIndex:"bidPayementFreq", _enableFn_: this._enableIfStatus_, width:200, noLabel: true, store:[ __FMBAS_TYPE__.InvoiceFreq._WEEKLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIMONTHLY_, __FMBAS_TYPE__.InvoiceFreq._MONTHLY_, __FMBAS_TYPE__.InvoiceFreq._QUARTERLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._ANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._PER_DELIVERY_, __FMBAS_TYPE__.InvoiceFreq._DAILY_, __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_], cls:"margin-left:5px",listeners:{
			expand:{scope:this, fn:this._filterInvoiceFreq_}
		}})
		.addNumberField({name:"prepaidAmount", bind:"{d.bidPrepaidAmount}", dataIndex:"bidPrepaidAmount", _enableFn_: this._enableIfStatus_, width:100, noLabel: true, sysDec:"dec_amount", maxLength:21})
		.addNumberField({name:"prepayFirstDeliveryDate", bind:"{d.bidPrepayFirstDeliveryDate}", dataIndex:"bidPrepayFirstDeliveryDate", _enableFn_: this._enableIfStatus_, width:40, noLabel: true, maxLength:4})
		.addDisplayFieldText({ name:"days", bind:"{d.days}", dataIndex:"days", maxLength:4000, labelAlign:"left"})
		.addDisplayFieldText({ name:"creditTermsLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"bankGuaranteesLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"prepaidDaysLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right;"})
		.addDisplayFieldText({ name:"paymentFreqLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"prepaidAmountLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"prepayFirstDeliveryDateLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right;"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", layout: {type:"table", columns:6}})
		.addPanel({ name:"prepayFistWithDays", layout: {type:"table", columns:3}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "prepayFistWithDays"])
		.addChildrenTo("col1", ["creditTermsLabel", "creditTerms", "bankGuaranteesLabel", "bankGuarantees", "prepaidDaysLabel", "prepaidDays", "paymentFreqLabel", "paymentFreq", "prepaidAmountLabel", "prepaidAmount"])
		.addChildrenTo("prepayFistWithDays", ["prepayFirstDeliveryDateLabel", "prepayFirstDeliveryDate", "days"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableIfStatus_: function() {
		
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = true;
		
						if (record) {
		
							var bidStatus = record.get("bidStatus");
		
							if (bidStatus != __CMM_TYPE__.BidStatus._DRAFT_) {
								result = false;
							}
						}
		
						return result;
	},
	
	_showFields_: function(field,newValue,oldValue) {
		
						var prepaymentVal = ["prepaidDays" , "prepaidDaysLabel" , "paymentFreq" , "paymentFreqLabel" , 
								"prepaidAmount" , "prepaidAmountLabel" , "prepayFirstDeliveryDate" , "prepayFirstDeliveryDateLabel" , "days"];
						
						var prepaymentRecord = ["marketCurrencyId","marketPricingUnitId"];
		
						if(field){
							if(newValue == "Prepayment"){
								Ext.each(prepaymentVal, function(e) {
									this._get_(e).show();
								}, this);
							}else{
								Ext.each(prepaymentVal, function(e) {
									this._get_(e).hide();
									this._get_(e)._safeSetValue_("");
								}, this);
							}
						}
	},
	
	_filterInvoiceFreq_: function(el) {
		
						var s = el.store;
						s.filter(function(r) {
							var value = r.get("field1");
							if ( value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_ && value !== __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_) {
								return value;
							}
						});
	}
});

/* ================= EDIT FORM: ApprovalNote ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderBid_Dc$ApprovalNote", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderBid_Dc$ApprovalNote",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"approvalNote", bind:"{p.approvalNote}", paramIndex:"approvalNote", width:580, labelAlign:"top", labelStyle:"font-weight:bold", fieldStyle:"margin-top: 5px;"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  layout:"fit"})
		.addPanel({ name:"col1", layout:"fit"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["approvalNote"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var approvalNote = this._get_("approvalNote");
						approvalNote.setRawValue("");
	}
});

/* ================= EDIT FORM: BidRevisionForm ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderBid_Dc$BidRevisionForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderBid_Dc$BidRevisionForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"newRevisionDescription", bind:"{p.newRevisionDescription}", paramIndex:"newRevisionDescription", width:475, noLabel: true, labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"col1", layout: {type:"table"}})
		.addPanel({ name:"main", autoScroll:true});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("col1", ["newRevisionDescription"])
		.addChildrenTo("main", ["col1"]);
	}
});

/* ================= EDIT FORM: BidCancelForm ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderBid_Dc$BidCancelForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderBid_Dc$BidCancelForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"bidCancelReason", bind:"{p.bidCancelReason}", paramIndex:"bidCancelReason", width:475, noLabel: true, labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"col1", layout: {type:"table"}})
		.addPanel({ name:"main", autoScroll:true});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("col1", ["bidCancelReason"])
		.addChildrenTo("main", ["col1"]);
	}
});

/* ================= EDIT FORM: BidDeclineAwardForm ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderBid_Dc$BidDeclineAwardForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderBid_Dc$BidDeclineAwardForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"bidDeclineAwardReason", bind:"{p.bidDeclineAwardReason}", paramIndex:"bidDeclineAwardReason", width:475, noLabel: true, labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"col1", layout: {type:"table"}})
		.addPanel({ name:"main", autoScroll:true});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("col1", ["bidDeclineAwardReason"])
		.addChildrenTo("main", ["col1"]);
	}
});

/* ================= EDIT FORM: CopyBidForm ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderBid_Dc$CopyBidForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderBid_Dc$CopyBidForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addBooleanField({ name:"newVersionSelection", bind:"{p.newVersionSelection}", paramIndex:"newVersionSelection", _visibleFn_: function(dc, rec) { return dc.record.data.bidLocBiddingStatus != __CMM_TYPE__.BiddingStatus._FINISHED_; } , noLabel: true})
		.addBooleanField({ name:"anotherBidSelection", bind:"{p.anotherBidSelection}", paramIndex:"anotherBidSelection", _visibleFn_: function(dc, rec) { return dc.record.data.bidLocBiddingStatus != __CMM_TYPE__.BiddingStatus._FINISHED_; } , noLabel: true})
		.addDisplayFieldText({ name:"newVersionSelectionLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record.data.bidLocBiddingStatus != __CMM_TYPE__.BiddingStatus._FINISHED_; } , maxLength:32})
		.addDisplayFieldText({ name:"anotherBidSelectionLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["newVersionSelection", "newVersionSelectionLabel", "anotherBidSelection", "anotherBidSelectionLabel"]);
	},
	/* ==================== Business functions ==================== */
	
	_afterApplyStates_: function() {
		
		
						var dc = this._controller_;
						var record = dc.getRecord();
		
						var dc = this._controller_;
						var newVersionSelection = this._get_("newVersionSelection");
						var anotherBidSelection = this._get_("anotherBidSelection");
		
						if (record) {
		
							var bidLocBiddingStatus = record.get("bidLocBiddingStatus");
		
							if (bidLocBiddingStatus != "Finished") {
								dc.setParamValue("newVersionSelection",true);
								dc.setParamValue("anotherBidSelection",false);
								dc.fireEvent("dissableTenderListEvent");
							}else{
								dc.setParamValue("newVersionSelection",false);
								dc.setParamValue("anotherBidSelection",true);
								dc.fireEvent("enableTenderListEvent");
							}
						}
						
						newVersionSelection.on("change", function() {				
							if (newVersionSelection.checked == true) {						
								dc.setParamValue("anotherBidSelection",false);
								dc.fireEvent("dissableTenderListEvent");
							} else if (newVersionSelection.checked == false) {
								dc.setParamValue("anotherBidSelection",true);						
							}
						},this);
		
						anotherBidSelection.on("change", function() {
							if (anotherBidSelection.checked == true) {
								dc.setParamValue("newVersionSelection",false);						
								dc.fireEvent("enableTenderListEvent");
							} else if (newVersionSelection.checked == false) {
								dc.setParamValue("newVersionSelection",true);						
							}
						},this);
	}
});
