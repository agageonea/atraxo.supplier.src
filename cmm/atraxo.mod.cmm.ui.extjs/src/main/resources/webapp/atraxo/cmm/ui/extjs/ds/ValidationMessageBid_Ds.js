/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.ValidationMessageBid_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_ValidationMessageBid_Ds"
	},
	
	
	validators: {
		message: [{type: 'presence'}]
	},
	
	fields: [
		{name:"objectId", type:"string"},
		{name:"objectType", type:"string"},
		{name:"message", type:"string"},
		{name:"severity", type:"string"},
		{name:"location", type:"string", noFilter:true, noSort:true},
		{name:"tenderName", type:"string", noFilter:true, noSort:true},
		{name:"bidVersion", type:"string", noFilter:true, noSort:true},
		{name:"bidRevision", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"succesfullySubmitedBidsLabel", type:"string", noFilter:true, noSort:true},
		{name:"concurencyCheckFailedBidsLabel", type:"string", noFilter:true, noSort:true},
		{name:"validationErrorsBidsLabel", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.ValidationMessageBid_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"objectId", type:"string"},
		{name:"objectType", type:"string"},
		{name:"message", type:"string"},
		{name:"severity", type:"string"},
		{name:"location", type:"string", noFilter:true, noSort:true},
		{name:"tenderName", type:"string", noFilter:true, noSort:true},
		{name:"bidVersion", type:"string", noFilter:true, noSort:true},
		{name:"bidRevision", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"succesfullySubmitedBidsLabel", type:"string", noFilter:true, noSort:true},
		{name:"concurencyCheckFailedBidsLabel", type:"string", noFilter:true, noSort:true},
		{name:"validationErrorsBidsLabel", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
