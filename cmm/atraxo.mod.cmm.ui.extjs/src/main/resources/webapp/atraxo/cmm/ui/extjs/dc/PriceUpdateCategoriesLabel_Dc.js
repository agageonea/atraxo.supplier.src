/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdateCategoriesLabel_Dc", {
	extend: "e4e.dc.AbstractDc",
	_infiniteScroll_: true,
	paramModel: atraxo.cmm.ui.extjs.ds.PriceUpdateCategories_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.PriceUpdateCategories_Ds
});

/* ================= EDIT FORM: GridLabel ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdateCategoriesLabel_Dc$GridLabel", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PriceUpdateCategoriesLabel_Dc$GridLabel",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"priceCategoryLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, cls:"sone-flat-label-field"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:650})
		.addPanel({ name:"col1", width:650, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["priceCategoryLabel"]);
	}
});
