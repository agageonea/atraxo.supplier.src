/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.ShipToReadOnly_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.ShipTo_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.ShipTo_Ds
});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ShipToReadOnly_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.cmm_ShipToReadOnly_Dc$EditList",
	_noPaginator_: true,

	/**
	 * 
	 */
	initComponent : function(config) {
		Ext.apply(this, {
			features: [{
				ftype: 'summary',
				remoteRoot: 'summaries',
				dock: 'bottom'
			}]
		});
		this.callParent(arguments);
	},
	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"shipTo", dataIndex:"customerCode", width:150, allowBlank: false, _enableFn_: function(dc, rec, col, fld) { return dc.record.data.id == null; } , xtype:"gridcolumn", 
			editor:{xtype:"fmbas_CustomerCodeLov_Lov", allowBlank:false, maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "customerId"} ,{lovField:"name", dsField: "customerName"} ]}})
		.addLov({name:"customerName", dataIndex:"customerName", width:150, noEdit: true, xtype:"gridcolumn"})
		.addDateColumn({name:"validFrom", dataIndex:"validFrom", width:150, allowBlank: false, _enableFn_: function(dc, rec, col, fld) { return dc.record.data.approvalStatus !== __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_; } , _mask_: Masks.DATE })
		.addDateColumn({name:"validTo", dataIndex:"validTo", width:150, allowBlank: false, _enableFn_: function(dc, rec, col, fld) { return dc.record.data.approvalStatus !== __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_; } , _mask_: Masks.DATE })
		.addNumberColumn({name:"allocatedVolume", dataIndex:"allocatedVolume", width:150, sysDec:"dec_unit",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_unit']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, _enableFn_: this._enableIfDraft_, maxLength:21, align:"right" })
		.addNumberColumn({name:"adjustedVolume", dataIndex:"adjustedVolume", width:150, sysDec:"dec_unit",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_unit']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, _enableFn_: function(dc, rec, col, fld) { return dc.record.data.approvalStatus !== __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_; } , maxLength:21, align:"right",listeners:{
			blur:{scope:this, fn:this._setVolumeChangedFlag_}, buffer: Main.viewConfig.GRID_EDITOR_CHANGE_EVT_BUFFER
		} })
		.addNumberColumn({name:"actualVolume", dataIndex:"actualVolume", width:150, noEdit: true, sysDec:"dec_unit",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_unit']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, maxLength:21, align:"right" })
		.addTextColumn({name:"settlementUnitCode", dataIndex:"settlementUnitCode", width:150, noEdit: true, maxLength:2})
		.addTextColumn({name:"contractPeriod", dataIndex:"contractPeriod", hidden:true, width:150, noEdit: true, maxLength:32})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._unitParamName_ = "unitParameter";
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_canChangeShipTo_: function() {
		
						if  ( _SYSTEMPARAMETERS_.syssalescontractapproval !== "true"){
							return true;
						}
						var dc = this._controller_;
						var rec = dc.getRecord();
						if (rec){
							return __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_ !== rec.get("approvalStatus") ;
						}
						return true;
	},
	
	_afterDefineElements_: function() {
		
						if(_SYSTEMPARAMETERS_.syssalescontractapproval === "true"){
							this._columns_.getByKey("approvalStatus").hidden = false ;
							this._columns_.getByKey("pendingAction").hidden = false ;
							this._columns_.getByKey("changes").hidden = false ;
						}
	},
	
	_setVolumeChangedFlag_: function() {
		
						var dc = this._controller_;
						dc._volumeChanged_ = true;
	},
	
	_enableIfDraft_: function() {
						
						var dc = this._controller_;
						var contractStatus = dc.params.get("contractStatus");				
						return contractStatus === "Draft" && dc.record.data.approvalStatus !== __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_;					
	},
	
	_disableIfDraft_: function() {
		
						var result = true;
						var dc = this._controller_;
						var contractStatus = dc.params.get("contractStatus");
						
						if (contractStatus === "Draft") {
							result = false;
						}
						
						return result;
	}
});
