/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.ContractPriceCategoryGrouping_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_ContractPriceCategoryGrouping_Ds"
	},
	
	
	initRecord: function() {
		this.set("continous", true);
		this.set("includeInAverage", true);
		this.set("vat", "Not applicable");
		this.set("exchangeRateOffset", "Current");
		this.set("defaultPriceCtgy", true);
		this.set("quantityType", "Gross volume");
	},
	
	fields: [
		{name:"contId", type:"int", allowNull:true},
		{name:"contCode", type:"string"},
		{name:"contStatus", type:"string"},
		{name:"suppId", type:"int", allowNull:true, noFilter:true},
		{name:"suppCode", type:"string", noFilter:true},
		{name:"locId", type:"int", allowNull:true},
		{name:"locCode", type:"string", noFilter:true},
		{name:"formTitle", type:"string", noFilter:true, noSort:true},
		{name:"name", type:"string"},
		{name:"continous", type:"boolean"},
		{name:"includeInAverage", type:"boolean"},
		{name:"quantityType", type:"string"},
		{name:"exchangeRateOffset", type:"string"},
		{name:"restriction", type:"boolean"},
		{name:"vat", type:"string"},
		{name:"defaultPriceCtgy", type:"boolean"},
		{name:"comments", type:"string"},
		{name:"forex", type:"string"},
		{name:"contract", type:"int", allowNull:true},
		{name:"contractValidFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"contractValidTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"value", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"priceCategory", type:"int", allowNull:true},
		{name:"priceCtgryName", type:"string"},
		{name:"priceCtgryPricePer", type:"string"},
		{name:"mainCategoryCode", type:"string"},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceName", type:"string"},
		{name:"financialSourceCode", type:"string"},
		{name:"avgMethodIndicatorDefMeth", type:"boolean"},
		{name:"avgMethodIndicatorName", type:"string"},
		{name:"avgMthdId", type:"int", allowNull:true},
		{name:"price", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"pricingBaseId", type:"int", allowNull:true},
		{name:"initialPrice", type:"float", allowNull:true},
		{name:"convertTitle", type:"string", noFilter:true, noSort:true},
		{name:"convertedPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"used", type:"boolean", noFilter:true, noSort:true},
		{name:"slash", type:"string", noFilter:true, noSort:true},
		{name:"percentageOf", type:"string", noFilter:true, noSort:true},
		{name:"contractAmount", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.ContractPriceCategoryGrouping_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"contId", type:"int", allowNull:true},
		{name:"contCode", type:"string"},
		{name:"contStatus", type:"string"},
		{name:"suppId", type:"int", allowNull:true, noFilter:true},
		{name:"suppCode", type:"string", noFilter:true},
		{name:"locId", type:"int", allowNull:true},
		{name:"locCode", type:"string", noFilter:true},
		{name:"formTitle", type:"string", noFilter:true, noSort:true},
		{name:"name", type:"string"},
		{name:"continous", type:"boolean", allowNull:true},
		{name:"includeInAverage", type:"boolean", allowNull:true},
		{name:"quantityType", type:"string"},
		{name:"exchangeRateOffset", type:"string"},
		{name:"restriction", type:"boolean", allowNull:true},
		{name:"vat", type:"string"},
		{name:"defaultPriceCtgy", type:"boolean", allowNull:true},
		{name:"comments", type:"string"},
		{name:"forex", type:"string"},
		{name:"contract", type:"int", allowNull:true},
		{name:"contractValidFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"contractValidTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"value", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"priceCategory", type:"int", allowNull:true},
		{name:"priceCtgryName", type:"string"},
		{name:"priceCtgryPricePer", type:"string"},
		{name:"mainCategoryCode", type:"string"},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceName", type:"string"},
		{name:"financialSourceCode", type:"string"},
		{name:"avgMethodIndicatorDefMeth", type:"boolean", allowNull:true},
		{name:"avgMethodIndicatorName", type:"string"},
		{name:"avgMthdId", type:"int", allowNull:true},
		{name:"price", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"pricingBaseId", type:"int", allowNull:true},
		{name:"initialPrice", type:"float", allowNull:true},
		{name:"convertTitle", type:"string", noFilter:true, noSort:true},
		{name:"convertedPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"used", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"slash", type:"string", noFilter:true, noSort:true},
		{name:"percentageOf", type:"string", noFilter:true, noSort:true},
		{name:"contractAmount", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.cmm.ui.extjs.ds.ContractPriceCategoryGrouping_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"isDefault", type:"boolean"},
		{name:"priceDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"settlementCurrencyCode", type:"string"},
		{name:"settlementCurrencyId", type:"int", allowNull:true},
		{name:"settlementUnitCode", type:"string"},
		{name:"total", type:"float", allowNull:true}
	]
});
