/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.ExpectedFeesAndTaxes_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.cmm.ui.extjs.ds.TenderLocationExpectedPrices_Ds
});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ExpectedFeesAndTaxes_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.cmm_ExpectedFeesAndTaxes_Dc$List",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"expectFeeCode", dataIndex:"iataPriceCategoryCode", width:80, noEdit: true, maxLength:25})
		.addTextColumn({name:"expectFeeName", dataIndex:"iataPriceCategoryName", width:200, noEdit: true, maxLength:100})
		.addTextColumn({name:"expectFeeType", dataIndex:"iataPriceCategoryType", width:75, noEdit: true, maxLength:32})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
