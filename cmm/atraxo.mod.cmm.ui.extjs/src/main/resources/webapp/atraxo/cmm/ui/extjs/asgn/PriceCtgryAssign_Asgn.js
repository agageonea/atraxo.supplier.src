/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

/* model */
Ext.define("atraxo.cmm.ui.extjs.asgn.PriceCtgryAssign_Asgn$Model", {
	extend: 'Ext.data.Model',
	statics: {
		ALIAS: "cmm_PriceCtgryAssign_Asgn"
	},
	fields:  [
		{name:"id",type:"string"},
		{name:"idContract",type:"string"},
		{name:"name",type:"string"}
	]
});

/* lists */
Ext.define( "atraxo.cmm.ui.extjs.asgn.PriceCtgryAssign_Asgn$List", {
	extend: "e4e.asgn.AbstractAsgnGrid",
	alias:[ "widget.cmm_PriceCtgryAssign_Asgn$Left","widget.cmm_PriceCtgryAssign_Asgn$Right" ],
	_defineColumns_: function () {
		this._getBuilder_()
		.addTextColumn({name:"id", dataIndex:"id", hidden:true})
		.addTextColumn({name:"idContract", dataIndex:"idContract", hidden:true})
		.addTextColumn({name:"name", dataIndex:"name", width:120})
	} 
});
	
/* ui-window */
Ext.define("atraxo.cmm.ui.extjs.asgn.PriceCtgryAssign_Asgn$Ui", {
	extend: "e4e.asgn.AbstractAsgnUi",
	width:800,
	height:400,
	title:"Assign price category ",
	_filterFields_: [
		["name"]
	],
	_defaultFilterField_ : "name",

	_defineElements_: function () {
		this._getBuilder_()
			.addLeftGrid({ xtype:"cmm_PriceCtgryAssign_Asgn$Left"})
			.addRightGrid({ xtype:"cmm_PriceCtgryAssign_Asgn$Right"})
	}
});

/* ui-panel */
Ext.define("atraxo.cmm.ui.extjs.asgn.PriceCtgryAssign_Asgn$AsgnPanel", {
	extend: "e4e.asgn.AbstractAsgnPanel",
	alias: "widget.cmm_PriceCtgryAssign_Asgn$AsgnPanel",
	width:800,
	height:400,
	title:"Assign price category ",
	_filterFields_: [
		["name"]
	],
	_defaultFilterField_ : "name",

	_defineElements_: function () {
		this._getBuilder_()
			.addLeftGrid({ xtype:"cmm_PriceCtgryAssign_Asgn$Left"})
			.addRightGrid({ xtype:"cmm_PriceCtgryAssign_Asgn$Right"})
	}
});
