/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.PriceUpdate_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_PriceUpdate_Ds"
	},
	
	
	validators: {
		contractStatus: [{type: 'presence'}],
		dealType: [{type: 'presence'}],
		contractType: [{type: 'presence'}],
		deliveryPoint: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("approvalStatus", "New");
		this.set("dealType", "Sell");
		this.set("contractStatus", "Effective");
		this.set("contractType", "Product");
		this.set("published", false);
		this.set("contractStatus", __CMM_TYPE__.ContractStatus._EFFECTIVE_);
		this.set("dealType", __CMM_TYPE__.DealType._SELL_);
		this.set("contractType", __CMM_TYPE__.ContractType._PRODUCT_);
		this.set("deliveryPoint", __CMM_TYPE__.FuelDelivery._INTO_PLANE_);
	},
	
	fields: [
		{name:"priceCategoryId", type:"int", allowNull:true},
		{name:"priceCategoryName", type:"string"},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"publishedById", type:"string"},
		{name:"publishedByCode", type:"string"},
		{name:"submittedById", type:"string"},
		{name:"submittedByCode", type:"string"},
		{name:"approvedById", type:"string"},
		{name:"approvedByCode", type:"string"},
		{name:"locationId", type:"int", allowNull:true},
		{name:"locationCode", type:"string"},
		{name:"contractHolderId", type:"int", allowNull:true},
		{name:"contractHolderCode", type:"string"},
		{name:"counterpartyId", type:"int", allowNull:true},
		{name:"counterpartyCode", type:"string"},
		{name:"billToId", type:"int", allowNull:true},
		{name:"billToCode", type:"string"},
		{name:"shipToId", type:"int", allowNull:true},
		{name:"shipToCode", type:"string"},
		{name:"reason", type:"string"},
		{name:"newPrice", type:"float", allowNull:true},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"approvalStatus", type:"string"},
		{name:"published", type:"boolean"},
		{name:"publishedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"submittedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"approvedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"dealType", type:"string"},
		{name:"contractStatus", type:"string"},
		{name:"contractType", type:"string"},
		{name:"deliveryPoint", type:"string"},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"visibleFlag", type:"boolean", noFilter:true, noSort:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.PriceUpdate_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"priceCategoryId", type:"int", allowNull:true},
		{name:"priceCategoryName", type:"string"},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"publishedById", type:"string"},
		{name:"publishedByCode", type:"string"},
		{name:"submittedById", type:"string"},
		{name:"submittedByCode", type:"string"},
		{name:"approvedById", type:"string"},
		{name:"approvedByCode", type:"string"},
		{name:"locationId", type:"int", allowNull:true},
		{name:"locationCode", type:"string"},
		{name:"contractHolderId", type:"int", allowNull:true},
		{name:"contractHolderCode", type:"string"},
		{name:"counterpartyId", type:"int", allowNull:true},
		{name:"counterpartyCode", type:"string"},
		{name:"billToId", type:"int", allowNull:true},
		{name:"billToCode", type:"string"},
		{name:"shipToId", type:"int", allowNull:true},
		{name:"shipToCode", type:"string"},
		{name:"reason", type:"string"},
		{name:"newPrice", type:"float", allowNull:true},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"approvalStatus", type:"string"},
		{name:"published", type:"boolean", allowNull:true},
		{name:"publishedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"submittedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"approvedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"dealType", type:"string"},
		{name:"contractStatus", type:"string"},
		{name:"contractType", type:"string"},
		{name:"deliveryPoint", type:"string"},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"visibleFlag", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.cmm.ui.extjs.ds.PriceUpdate_DsParam", {
	extend: 'Ext.data.Model',



	initParam: function() {
		this.set("contractStatusEffective", true);
		this.set("contractTypeSales", true);
		this.set("contractForProduct", true);
		this.set("deliveryPointIntoPlane", true);
	},

	fields: [
		{name:"approvalNote", type:"string"},
		{name:"contractForProduct", type:"boolean"},
		{name:"contractForService", type:"boolean"},
		{name:"contractStatusEffective", type:"boolean"},
		{name:"contractStatusExpired", type:"boolean"},
		{name:"contractTypePurchase", type:"boolean"},
		{name:"contractTypeSales", type:"boolean"},
		{name:"deliveryPointIntoPlane", type:"boolean"},
		{name:"deliveryPointIntoStorage", type:"boolean"},
		{name:"filter", type:"string"},
		{name:"selectedAll", type:"boolean"},
		{name:"selectedAttachments", type:"string"},
		{name:"selectedCategories", type:"string"},
		{name:"standardFilter", type:"string"},
		{name:"submitForApprovalDescription", type:"string"},
		{name:"submitForApprovalResult", type:"boolean"},
		{name:"unSelectedCategories", type:"string"}
	]
});
