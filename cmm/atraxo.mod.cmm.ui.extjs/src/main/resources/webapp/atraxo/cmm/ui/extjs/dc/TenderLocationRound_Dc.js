/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocationRound_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.cmm.ui.extjs.ds.TenderLocationRound_Ds
});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocationRound_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.cmm_TenderLocationRound_Dc$List",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addNumberColumn({name:"roundNo", dataIndex:"roundNo", width:75, _enableFn_: function(dc, rec, col, fld) { return this._enableIfStatus_(); } , maxLength:2, align:"right" })
		.addDateColumn({name:"biddingFrom", dataIndex:"biddingFrom", width:130, _enableFn_: function(dc, rec, col, fld) { return this._enableIfStatus_(); } , _mask_: Masks.DATETIME })
		.addDateColumn({name:"biddingTo", dataIndex:"biddingTo", width:130, _enableFn_: function(dc, rec, col, fld) { return this._enableIfStatus_(); } , _mask_: Masks.DATETIME })
		.addDateColumn({name:"bidOpening", dataIndex:"bidOpening", width:100, _enableFn_: function(dc, rec, col, fld) { return this._enableIfStatus_(); } , _mask_: Masks.DATE })
		.addBooleanColumn({name:"active", dataIndex:"bidRoundActive", width:90, noEdit: true})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_enableIfStatus_: function() {
		
						var dc 		= this._controller_;
						var dcParent = dc._frame_._getDc_("tenderInvitation");
						var r = dcParent.getRecord();
						if( r ){
							if(r.data.status === __CMM_TYPE__.TenderStatus._DRAFT_ ){
								return true;
							}
						}
						return false;
	}
});
