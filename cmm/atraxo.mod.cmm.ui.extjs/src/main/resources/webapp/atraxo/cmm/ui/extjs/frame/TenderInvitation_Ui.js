/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.frame.TenderInvitation_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.TenderInvitation_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("geoLocations", Ext.create(atraxo.fmbas.ui.extjs.dc.Locations_Dc,{}))
		.addDc("tenderInvitation", Ext.create(atraxo.cmm.ui.extjs.dc.TenderInvitation_Dc,{}))
		.addDc("tenderInvitationNew", Ext.create(atraxo.cmm.ui.extjs.dc.TenderInvitation_Dc,{}))
		.addDc("location", Ext.create(atraxo.cmm.ui.extjs.dc.TenderLocation_Dc,{multiEdit: true}))
		.addDc("locationBid", Ext.create(atraxo.cmm.ui.extjs.dc.TenderBid_Dc,{}))
		.addDc("locationAirline", Ext.create(atraxo.cmm.ui.extjs.dc.TenderLocationAirlines_Dc,{multiEdit: true}))
		.addDc("locationNew", Ext.create(atraxo.cmm.ui.extjs.dc.TenderLocation_Dc,{multiEdit: true}))
		.addDc("locationAirlineNew", Ext.create(atraxo.cmm.ui.extjs.dc.TenderLocationAirlines_Dc,{multiEdit: true}))
		.addDc("locationRound", Ext.create(atraxo.cmm.ui.extjs.dc.TenderLocationRound_Dc,{multiEdit: true}))
		.addDc("locationExpectedFeesAndTaxes", Ext.create(atraxo.cmm.ui.extjs.dc.ExpectedFeesAndTaxes_Dc,{multiEdit: true}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("attachment", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.addDc("locationHistory", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("locationNote", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.addDc("importBid", Ext.create(atraxo.cmm.ui.extjs.dc.ImportBid_Dc,{}))
		.addDc("attachmentPop", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("bidValidations", Ext.create(atraxo.cmm.ui.extjs.dc.BidValidations_Dc,{}))
		.addDc("bidValidationsLabel", Ext.create(atraxo.cmm.ui.extjs.dc.BidValidationsLabels_Dc,{}))
		.linkDc("location", "tenderInvitation",{fetchMode:"auto",fields:[
					{childField:"tenderId", parentField:"id"}]})
				.linkDc("locationBid", "location",{fetchMode:"auto",fields:[
					{childField:"bidLocId", parentField:"id"}]})
				.linkDc("locationAirline", "location",{fetchMode:"auto",fields:[
					{childField:"tenderLocationId", parentField:"id"}]})
				.linkDc("locationNew", "tenderInvitationNew",{fetchMode:"auto",fields:[
					{childField:"tenderId", parentField:"id"}]})
				.linkDc("locationAirlineNew", "locationNew",{fetchMode:"auto",fields:[
					{childField:"tenderLocationId", parentField:"id"}]})
				.linkDc("locationRound", "location",{fetchMode:"auto",fields:[
					{childField:"tenderLocationId", parentField:"id"}]})
				.linkDc("locationExpectedFeesAndTaxes", "location",{fetchMode:"auto",fields:[
					{childField:"tenderLocationId", parentField:"id"}]})
				.linkDc("history", "tenderInvitation",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("attachment", "tenderInvitation",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"refid"}, {childField:"targetAlias", parentField:"entityAlias"}]})
				.linkDc("note", "tenderInvitation",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("locationHistory", "location",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("locationNote", "location",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("importBid", "location",{fetchMode:"auto",fields:[
					{childField:"id", parentField:"id"}, {childField:"locationId", parentField:"locationId"}, {childField:"locationCode", parentField:"locationCode"}]})
				.linkDc("attachmentPop", "locationBid",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}, {childField:"categType", value:"upload"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnPublishList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnPublishList,stateManager:[{ name:"selected_not_zero", dc:"tenderInvitation", and: function(dc) {return (this.canPublish(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnPublishEdit",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnPublishEdit,stateManager:[{ name:"selected_one_clean", dc:"tenderInvitation", and: function(dc) {return ((dc.record.data.status===__CMM_TYPE__.TenderStatus._DRAFT_));} }], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnUnpublishList",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnUnpublishList,stateManager:[{ name:"selected_not_zero", dc:"tenderInvitation", and: function(dc) {return (this.canUnpublish(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnUnpublishEdit",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnUnpublishEdit,stateManager:[{ name:"selected_one_clean", dc:"tenderInvitation", and: function(dc) {return (this.canUnpublish(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnSaveCloseUnpublishWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseUnpublishWdw, scope:this})
		.addButton({name:"btnCancelUnpublishWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelUnpublishWdw, scope:this})
		.addButton({name:"btnDeleteTenderInv",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteTenderInv,stateManager:[{ name:"selected_one_clean", dc:"tenderInvitation", and: function(dc) {return ((dc.record.data.status===__CMM_TYPE__.TenderStatus._DRAFT_));} }], scope:this})
		.addButton({name:"btnTIFirstContinue",glyph:fp_asc.next_glyph.glyph, disabled:false, handler: this.onBtnTIFirstContinue, scope:this})
		.addButton({name:"btnTIFinishWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css,  disabled:true, listeners:{boxready: { scope: this, fn: function(btn) { this._stopTabKey_(btn) }}}, handler: this.onBtnTIFinishWdw, scope:this})
		.addButton({name:"btnFinishStep2",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false,  hidden:true, handler: this.onBtnFinishStep2, scope:this})
		.addButton({name:"btnFinishStep3",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false,  hidden:true, handler: this.onBtnFinishStep3, scope:this})
		.addButton({name:"btnFinishStep4",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false,  hidden:true, handler: this.onBtnFinishStep4, scope:this})
		.addButton({name:"btnTISecondContinue",glyph:fp_asc.next_glyph.glyph,  hidden:true, disabled:true, handler: this.onBtnTISecondContinue, scope:this})
		.addButton({name:"btnTIThirdContinue",glyph:fp_asc.next_glyph.glyph, disabled:false,  hidden:true, handler: this.onBtnTIThirdContinue,stateManager:[{ name:"dc_in_any_state", dc:"locationNew", and: function() {return (this._getDc_('locationNew').getRecord() && !Ext.isEmpty(this._getDc_('locationNew').getRecord().get('taxType')) ? true : false);} }], scope:this})
		.addButton({name:"btnViewTenderDetails",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnViewTenderDetails,stateManager:[{ name:"selected_one", dc:"tenderInvitation"}], scope:this})
		.addButton({name:"btnViewTenderLocDetails",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnViewTenderLocDetails,stateManager:[{ name:"selected_one", dc:"location"}], scope:this})
		.addButton({name:"btnCloseNoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseNoteWdw, scope:this})
		.addButton({name:"btnSaveCloseNoteWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseNoteWdw, scope:this})
		.addButton({name:"btnCloseLocationNoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseLocationNoteWdw, scope:this})
		.addButton({name:"btnSaveCloseLocationNoteWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseLocationNoteWdw, scope:this})
		.addButton({name:"btnHelpList",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onBtnHelpList, scope:this})
		.addButton({name:"btnHelpEdit",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onBtnHelpEdit, scope:this})
		.addButton({name:"btnImportWithMenu",glyph:fp_asc.xml_glyph.glyph,iconCls: fp_asc.xml_glyph.css, disabled:false, handler: this.onBtnImportWithMenu, scope:this})
		.addButton({name:"btnListMakeLocationBidMenu",glyph:fp_asc.bid_glyph.glyph,iconCls: fp_asc.bid_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_one_clean", dc:"location"}], scope:this})
		.addButton({name:"btnListMakeLocationBid",glyph:fp_asc.bid_glyph.glyph,iconCls: fp_asc.bid_glyph.css, disabled:true,  _btnContainerName_:"btnListMakeLocationBidMenu", _isDefaultHandler_:true, handler: this.onBtnListMakeLocationBid,stateManager:[{ name:"selected_one_clean", dc:"location", and: function() {return (this.canCreateBidFromTenderLocation());} }], scope:this})
		.addButton({name:"btnListMakePerReceiverLocationBid",glyph:fp_asc.bid_glyph.glyph,iconCls: fp_asc.bid_glyph.css, disabled:true,  _btnContainerName_:"btnListMakeLocationBidMenu", handler: this.onBtnListMakePerReceiverLocationBid,stateManager:[{ name:"selected_one_clean", dc:"location", and: function() {return (this.canCreateBidFromTenderLocation());} }], scope:this})
		.addButton({name:"btnFuelReceiversListMakeLocationBidMenu",glyph:fp_asc.bid_glyph.glyph,iconCls: fp_asc.bid_glyph.css, disabled:false, isBtnContainer:true, scope:this})
		.addButton({name:"btnFuelReceiversListMakeLocationBid",glyph:fp_asc.bid_glyph.glyph,iconCls: fp_asc.bid_glyph.css, disabled:true,  _btnContainerName_:"btnFuelReceiversListMakeLocationBidMenu", _isDefaultHandler_:true, handler: this.onBtnFuelReceiversListMakeLocationBid,stateManager:[{ name:"selected_one_clean", dc:"location", and: function() {return (this.canCreateBidFromFuelReceiversAndLocation());} }], scope:this})
		.addButton({name:"btnFuelReceiversListMakePerReceiverLocationBid",glyph:fp_asc.bid_glyph.glyph,iconCls: fp_asc.bid_glyph.css, disabled:true,  _btnContainerName_:"btnFuelReceiversListMakeLocationBidMenu", handler: this.onBtnFuelReceiversListMakePerReceiverLocationBid,stateManager:[{ name:"selected_not_zero", dc:"locationAirline", and: function() {return (this.canCreateBidFromFuelReceiversAndLocation());} }], scope:this})
		.addButton({name:"btnCaptureBidWdw", disabled:false, handler: this.onBtnCaptureBidWdw, scope:this})
		.addButton({name:"btnCancelCaptureBidWdw", disabled:false, handler: this.onBtnCancelCaptureBidWdw, scope:this})
		.addButton({name:"btnSaveBidCancelWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveBidCancelWdw, scope:this})
		.addButton({name:"btnCancelBidCancelWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelBidCancelWdw, scope:this})
		.addButton({name:"btnDeclineLocationBidWithMenu",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"location"}, { name:"list_not_empty", visibleRule: true, dc:"location", and: function(dc) {return (dc.record && dc.record.data.tenderSource === __CMM_TYPE__.TenderSource._IMPORTED_);} }], scope:this})
		.addButton({name:"btnDeclineSelectedLocationsBid",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true,  _btnContainerName_:"btnDeclineLocationBidWithMenu", _isDefaultHandler_:true, handler: this.onBtnDeclineSelectedLocationsBid,stateManager:[{ name:"selected_not_zero", dc:"location", and: function(dc) {return (this.canDeclineLocationBid(dc));} }], scope:this})
		.addButton({name:"btnDeclineNotOperatedLocationsBid",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true,  _btnContainerName_:"btnDeclineLocationBidWithMenu", handler: this.onBtnDeclineNotOperatedLocationsBid,stateManager:[{ name:"list_not_empty", dc:"location"}], scope:this})
		.addButton({name:"btnExportTenderList",glyph:fp_asc.export_glyph.glyph, disabled:false, handler: this.onBtnExportTenderList, scope:this})
		.addButton({name:"btnExportTenderEdit",glyph:fp_asc.export_glyph.glyph, disabled:false, handler: this.onBtnExportTenderEdit, scope:this})
		.addButton({name:"btnBackToFirstStep",glyph:fp_asc.back_glyph.glyph,iconCls: fp_asc.back_glyph.css, disabled:false,  hidden:true, handler: this.onBtnBackToFirstStep, scope:this})
		.addButton({name:"btnBackToSecondStep",glyph:fp_asc.back_glyph.glyph,iconCls: fp_asc.back_glyph.css, disabled:false,  hidden:true, handler: this.onBtnBackToSecondStep, scope:this})
		.addButton({name:"btnBackToThirdStep",glyph:fp_asc.back_glyph.glyph,iconCls: fp_asc.back_glyph.css, disabled:false,  hidden:true, handler: this.onBtnBackToThirdStep,stateManager:[{ name:"no_record_or_record_is_clean", dc:"locationAirlineNew"}], scope:this})
		.addButton({name:"btnAddGroupMemeber",  disabled:true, handler: this.onBtnAddGroupMemeber, scope:this})
		.addButton({name:"btnNewTenderInv",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewTenderInv, scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnViewBid",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnViewBid,stateManager:[{ name:"selected_one_clean", dc:"locationBid"}], scope:this})
		.addButton({name:"btnDeleteBid",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteBid,stateManager:[{ name:"selected_one_clean", dc:"locationBid", and: function() {return (this.canDeleteBid(this._getDc_('locationBid')));} }], scope:this})
		.addButton({name:"btnSubmitBidForApproval",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitBidForApproval,stateManager:[{ name:"selected_not_zero", dc:"locationBid", and: function(dc) {return (this.canSubmitForApproval(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysbidapproval === 'true' })
		.addButton({name:"btnDownloadBidFormList",glyph:fp_asc.export_glyph.glyph, disabled:true, handler: this.onBtnDownloadBidFormList,stateManager:[{ name:"selected_one_clean", dc:"location"}], scope:this})
		.addButton({name:"btnDownloadBidFormEdit",glyph:fp_asc.export_glyph.glyph, disabled:true, handler: this.onBtnDownloadBidFormEdit,stateManager:[{ name:"selected_one_clean", dc:"location"}], scope:this})
		.addButton({name:"btnNewTenderLocation",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewTenderLocation,stateManager:[{ name:"no_record_or_record_is_clean", dc:"location", and: function() {return (this.canModifyTender(this._getDc_('location')));} }], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnSaveTenderLocation",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSaveTenderLocation,stateManager:[{ name:"record_is_dirty", dc:"location"}], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnCancelTenderLocation",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnCancelTenderLocation,stateManager:[{ name:"record_is_dirty", dc:"location"}], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnDeleteTenderLocation",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onBtnDeleteTenderLocation,stateManager:[{ name:"record_is_clean", dc:"location", and: function() {return (this.canModifyTender(this._getDc_('location')));} }], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnSaveCloseApproveWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApproveWdw, scope:this})
		.addButton({name:"btnCancelApproveWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApproveWdw, scope:this})
		.addButton({name:"btnCancelApprovalWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApprovalWdw, scope:this})
		.addButton({name:"btnSaveCloseApprovalWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApprovalWdw, scope:this})
		.addButton({name:"btnApproveList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApproveList,stateManager:[{ name:"selected_one_clean", dc:"locationBid", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysbidapproval === 'true' })
		.addButton({name:"btnRejectList",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnRejectList,stateManager:[{ name:"selected_one_clean", dc:"locationBid", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysbidapproval === 'true' })
		.addButton({name:"btnSaveCloseRejectWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRejectWdw, scope:this})
		.addButton({name:"btnCancelRejectWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRejectWdw, scope:this})
		.addButton({name:"btnSubmitSelected",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSubmitSelected,stateManager:[{ name:"selected_not_zero", dc:"locationBid", and: function(dc) {return (this.canSubmitSelectedToIssuer(dc));} }], scope:this})
		.addButton({name:"btnAwardBidList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnAwardBidList,stateManager:[{ name:"selected_not_zero", dc:"locationBid", and: function(dc) {return (this.canAwardDecline(dc));} }, { name:"selected_not_zero", visibleRule: true, dc:"locationBid", and: function(dc) {return (dc.record.data.tenderSource===__CMM_TYPE__.TenderSource._CAPTURED_);} }], scope:this})
		.addButton({name:"btnDeclineBidList",iconCls: fp_asc.close_icon.css, disabled:true, handler: this.onBtnDeclineBidList,stateManager:[{ name:"selected_not_zero", dc:"locationBid", and: function(dc) {return (this.canAwardDecline(dc));} }, { name:"selected_not_zero", visibleRule: true, dc:"locationBid", and: function(dc) {return (dc.record.data.tenderSource===__CMM_TYPE__.TenderSource._CAPTURED_);} }], scope:this})
		.addButton({name:"btnCancelBidList",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnCancelBidList,stateManager:[{ name:"selected_not_zero", dc:"locationBid", and: function(dc) {return (this.canCancelBid(dc));} }], scope:this})
		.addButton({name:"btnAcceptAward",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnAcceptAward,stateManager:[{ name:"selected_one_clean", dc:"locationBid", and: function(dc) {return ((dc.record.data.bidStatus===__CMM_TYPE__.BidStatus._AWARDED_));} }], scope:this})
		.addButton({name:"btnCloseBidValidationsWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false,  style:"margin-left: 9px", handler: this.onBtnCloseBidValidationsWdw, scope:this})
		.addButton({name:"btnCloseAndFilterBidValidationsWdw",glyph:fp_asc.filter_glyph.glyph, disabled:false, handler: this.onBtnCloseAndFilterBidValidationsWdw, scope:this})
		.addButton({name:"btnNewTenderNote",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewTenderNote,stateManager:[{ name:"no_record_or_record_is_clean", dc:"note"}], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnEditTenderNote",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnEditTenderNote,stateManager:[{ name:"selected_one_clean", dc:"note"}], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnDeleteTenderNote",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteTenderNote,stateManager:[{ name:"selected_one_clean", dc:"note"}], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnNewTenderLocationNote",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewTenderLocationNote,stateManager:[{ name:"no_record_or_record_is_clean", dc:"locationNote"}], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnEditTenderLocationNote",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnEditTenderLocationNote,stateManager:[{ name:"selected_one_clean", dc:"locationNote"}], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnDeleteTenderLocationNote",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteTenderLocationNote,stateManager:[{ name:"selected_one_clean", dc:"locationNote"}], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnNewFuelReceiver",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewFuelReceiver,stateManager:[{ name:"no_record_or_record_is_clean", dc:"locationAirline", and: function() {return (this.canModifyTender(this._getDc_('locationAirline')));} }], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnSaveFuelReceiver",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSaveFuelReceiver,stateManager:[{ name:"record_is_dirty", dc:"locationAirline"}], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnCancelFuelReceiver",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnCancelFuelReceiver,stateManager:[{ name:"record_is_dirty", dc:"locationAirline"}], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnDeleteFuelReceiver",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteFuelReceiver,stateManager:[{ name:"selected_one_clean", dc:"locationAirline", and: function() {return (this.canModifyTender(this._getDc_('locationAirline')));} }], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnNewRound",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewRound,stateManager:[{ name:"no_record_or_record_is_clean", dc:"locationRound", and: function() {return (this.canModifyTender(this._getDc_('locationRound')));} }], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnSaveRound",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSaveRound,stateManager:[{ name:"record_is_dirty", dc:"locationRound"}], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnCancelRound",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnCancelRound,stateManager:[{ name:"record_is_dirty", dc:"locationRound"}], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnDeleteRound",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteRound,stateManager:[{ name:"selected_one_clean", dc:"locationRound", and: function() {return (this.canModifyTender(this._getDc_('locationRound')));} }], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnMarkUnread",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnMarkUnread,stateManager:[{ name:"selected_one_clean", dc:"tenderInvitation", and: function(dc) {return (dc.record && dc.record.read === true);} }], scope:this})
		.addDcFilterFormView("tenderInvitation", {name:"tiFilter", xtype:"cmm_TenderInvitation_Dc$Filter"})
		.addDcGridView("tenderInvitation", {name:"tiList", xtype:"cmm_TenderInvitation_Dc$List",  viewConfig:{getRowClass: function (record) {
		                return !Ext.isEmpty(record.get('read')) && record.get('read') === false ? 'sone-tender-unread' : ''; }}})
		.addDcFormView("tenderInvitationNew", {name:"tiNew", height:200, xtype:"cmm_TenderInvitation_Dc$New",  listeners:{ validitychange:{fn: function(basic, dirty) {this._checkValidity_(basic, dirty)}, scope:this} }, _firstFocusItemName_:"tenderName"})
		.addDcFormView("tenderInvitation", {name:"tiHeader", xtype:"cmm_TenderInvitation_Dc$Header", _acquireFocusUpdate_: false})
		.addDcFormView("tenderInvitation", {name:"tiRemarks", xtype:"cmm_TenderInvitation_Dc$Remarks"})
		.addDcFilterFormView("location", {name:"tlFilter", xtype:"cmm_TenderLocation_Dc$Filter"})
		.addDcEditGridView("location", {name:"tlList", _hasTitle_:true, xtype:"cmm_TenderLocation_Dc$List", frame:true,  viewConfig:{getRowClass: function (record) {
		                return record.get('locationBiddingStatus') === __CMM_TYPE__.BiddingStatus._NO_BID_ ? 'sone-row-declined-tender-location-bid' : ''; }}, listeners:{itemdblclick: {scope: this, fn:function(el, record, item) {this._showEdit_(el, record, item)}}}})
		.addDcEditGridView("locationNew", {name:"tlEdit", xtype:"cmm_TenderLocation_Dc$Edit", frame:true,  _showViewFilterToolbar_:false, cls:"sone-grid-no-summary", selType:"rowmodel"})
		.addDcGridView("geoLocations", {name:"tiSourceList", height:200, xtype:"fmbas_Locations_Dc$SourceList",  _showViewFilterToolbar_:false, cls:"sone-grid-no-summary", selType:"rowmodel"})
		.addDcGridView("locationNew", {name:"tiAssignList", height:200, xtype:"cmm_TenderLocation_Dc$AssignList",  _showViewFilterToolbar_:false, cls:"sone-grid-no-summary", listeners:{boxready: {scope: this, fn: function(grid) { this._setBtnState_(grid) }}}, selType:"rowmodel"})
		.addDcGridView("locationNew", {name:"tiFourthAssignList", width:250, height:200, xtype:"cmm_TenderLocation_Dc$AssignList",  _showViewFilterToolbar_:false, cls:"sone-grid-no-summary", selType:"rowmodel"})
		.addDcFormView("location", {name:"tlHeader", xtype:"cmm_TenderLocation_Dc$Header"})
		.addDcEditGridView("locationAirline", {name:"tlAList", _hasTitle_:true, xtype:"cmm_TenderLocationAirlines_Dc$List", frame:true})
		.addDcFormView("locationAirline", {name:"tlNoteView", xtype:"cmm_TenderLocationAirlines_Dc$View"})
		.addDcEditGridView("locationAirlineNew", {name:"tlFourthAList", width:535, height:200, xtype:"cmm_TenderLocationAirlines_Dc$FourthStepList", frame:true})
		.addDcFormView("location", {name:"tlPayment", _hasTitle_:true, xtype:"cmm_TenderLocation_Dc$PaymentConditions"})
		.addDcEditGridView("locationExpectedFeesAndTaxes", {name:"tlExpectedFees", _hasTitle_:true, xtype:"cmm_ExpectedFeesAndTaxes_Dc$List", frame:true})
		.addDcEditGridView("locationRound", {name:"tlRound", _hasTitle_:true, xtype:"cmm_TenderLocationRound_Dc$List", frame:true})
		.addDcFormView("location", {name:"tlPricing", _hasTitle_:true, xtype:"cmm_TenderLocation_Dc$ProductPricing"})
		.addDcFormView("note", {name:"NotesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addDcGridView("note", {name:"NotesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcGridView("attachment", {name:"attachmentList", _hasTitle_:true, xtype:"ad_Attachment_Dc$List"})
		.addDcFormView("locationNote", {name:"locationNotesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addDcGridView("locationNote", {name:"locationNotesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcGridView("locationBid", {name:"locationBidsList", _hasTitle_:true, xtype:"cmm_TenderBid_Dc$TabList"})
		.addDcGridView("history", {name:"History", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcGridView("locationHistory", {name:"locationHistoryList", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("tenderInvitationNew", {name:"wizardHeader", height:120, xtype:"cmm_TenderInvitation_Dc$wizardHeader",  _wizardLineMargin_:100})
		.addDcFormView("tenderInvitationNew", {name:"assignButtonsForm", width:50, height:200, xtype:"cmm_TenderInvitation_Dc$AssignButtonsForm"})
		.addDcFormView("importBid", {name:"importBid", xtype:"cmm_ImportBid_Dc$NewWithName"})
		.addDcFormView("locationBid", {name:"approveNote", xtype:"cmm_TenderBid_Dc$ApprovalNote"})
		.addDcFormView("locationBid", {name:"approvalNote", xtype:"cmm_TenderBid_Dc$ApprovalNote",  split:false})
		.addDcGridView("attachmentPop", {name:"documentsAssignList", _hasTitle_:true, height:300, xtype:"ad_Attachment_Dc$attachmentAssignList",  cls:"sone-custom-panel-title"})
		.addDcFormView("locationBid", {name:"rejectNote", xtype:"cmm_TenderBid_Dc$ApprovalNote"})
		.addDcFormView("location", {name:"captureBid", xtype:"cmm_TenderLocation_Dc$CaptureBid"})
		.addDcFormView("locationBid", {name:"bidCancelForm", xtype:"cmm_TenderBid_Dc$BidCancelForm",  split:false})
		.addDcFormView("bidValidationsLabel", {name:"bidValidationsLabel", xtype:"cmm_BidValidationsLabels_Dc$BidValidationsLabel",  split:false})
		.addDcGridView("bidValidations", {name:"bidValidationsList", width:160, xtype:"cmm_BidValidations_Dc$BidValidations"})
		.addPanel({name:"fourthStepPanel", height:200, layout:"hbox", layoutConfig: {padding:"2", align: "stretchmax"}})
		.addPanel({name:"secondStepPanel", width:80, height:200, layout:"hbox", layoutConfig: {padding:"2", align: "stretchmax"}})
		.addWizardWindow({name:"tenderInvitationWdw", _hasTitle_:true, width:820, height:400,_labelPanelName_:"wizardHeader"
			,_stepPanels_:["tiNew","secondStepPanel","tlEdit","fourthStepPanel"]
			,  listeners:{ close:{fn:this.onTIWdwClose, scope:this}, show:{fn:this._jumpToFirstStep_, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnBackToFirstStep"), this._elems_.get("btnTIFinishWdw"), this._elems_.get("btnTIFirstContinue"), this._elems_.get("btnFinishStep2"), this._elems_.get("btnTISecondContinue"), this._elems_.get("btnBackToSecondStep"), this._elems_.get("btnFinishStep3"), this._elems_.get("btnTIThirdContinue"), this._elems_.get("btnBackToThirdStep"), this._elems_.get("btnFinishStep4")]}]})
		.addWindow({name:"noteWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("NotesEdit")],  listeners:{ close:{fn:this.onNoteWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseNoteWdw"), this._elems_.get("btnCloseNoteWdw")]}]})
		.addWindow({name:"locationNoteWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("locationNotesEdit")],  listeners:{ close:{fn:this.onLocationNoteWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseLocationNoteWdw"), this._elems_.get("btnCloseLocationNoteWdw")]}]})
		.addWindow({name:"wdwImportBid", _hasTitle_:true, width:260, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("importBid")],  closable:true, listeners:{ show:{fn:function() { var ctx = getActiveDialog(), 
						form = ctx._get_('importBid'), 
						dc = ctx._getDc_('importBid'),
						w = ctx._getWindow_('wdwImportBid'); 
						form._get_('importBidFileType').setValue(null); 
						dc._doNewWdw_ = w} } }})
		.addPanel({name:"panelAssignAttachments", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"bidValidationsCanvas", layout:{ type: "border" }, defaults:{split:true}})
		.addWindow({name:"wdwBidValidations", _hasTitle_:true, width:700, height:400, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("bidValidationsCanvas")],  closable:true, bodyPadding:"10", 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCloseBidValidationsWdw"), this._elems_.get("btnCloseAndFilterBidValidationsWdw")]}]})
		.addWindow({name:"locationNoteViewWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("tlNoteView")],  listeners:{afterrender: {scope: this, fn: function(w) { this._closeNoteWdwOnMaskClick_(w) }}}})
		.addWindow({name:"wdwApprovalNote", _hasTitle_:true, width:600, height:530, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("panelAssignAttachments")],  closable:true, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCancelApprovalWdw"), this._elems_.get("btnSaveCloseApprovalWdw")]}]})
		.addWindow({name:"wdwRejectNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("rejectNote")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRejectWdw"), this._elems_.get("btnCancelRejectWdw")]}]})
		.addWindow({name:"wdwApproveNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("approveNote")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseApproveWdw"), this._elems_.get("btnCancelApproveWdw")]}]})
		.addWindow({name:"wdwRemarks", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("tiRemarks")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseUnpublishWdw"), this._elems_.get("btnCancelUnpublishWdw")]}]})
		.addWindow({name:"wdwCaptureBid", _hasTitle_:true, width:480, height:260, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("captureBid")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCaptureBidWdw"), this._elems_.get("btnCancelCaptureBidWdw")]}]})
		.addWindow({name:"wdwBidCancel", _hasTitle_:true, width:500, height:170, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("bidCancelForm")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveBidCancelWdw"), this._elems_.get("btnCancelBidCancelWdw")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0,  scrollable:true})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTabCanvas3", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas2", dcName:"tenderInvitation"})
		
		.addTabPanelForUserFields({tabPanelName:"detailsTabCanvas3", containerPanelName:"canvas3", dcName:"location"})
		; 					
		
		this._elems_.get("tiSourceList")["flex"] = 10;
		this._elems_.get("tiAssignList")["flex"] = 10;
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("fourthStepPanel", ["tiFourthAssignList", "tlFourthAList"])
		.addChildrenTo("secondStepPanel", ["tiSourceList", "assignButtonsForm", "tiAssignList"])
		.addChildrenTo("panelAssignAttachments", ["approvalNote", "documentsAssignList"], ["north", "center"])
		.addChildrenTo("bidValidationsCanvas", ["bidValidationsLabel", "bidValidationsList"], ["north", "center"])
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["tiList"], ["center"])
		.addChildrenTo("canvas2", ["tiHeader", "detailsTab"], ["north", "center"])
		.addChildrenTo("detailsTab", ["tlList"])
		.addChildrenTo("canvas3", ["tlHeader", "detailsTabCanvas3"], ["north", "center"])
		.addChildrenTo("detailsTabCanvas3", ["tlAList"])
		.addToolbarTo("tiList", "tlbCmpList")
		.addToolbarTo("tiHeader", "tlbTiEdit")
		.addToolbarTo("tlList", "tlbTlList")
		.addToolbarTo("tlHeader", "tlbTlHeader")
		.addToolbarTo("locationBidsList", "tlblocationBidsList")
		.addToolbarTo("tlAList", "tlbTlaList")
		.addToolbarTo("tlRound", "tlbTlRound")
		.addToolbarTo("NotesList", "tlbNoteList")
		.addToolbarTo("attachmentList", "tlbAttachments")
		.addToolbarTo("locationNotesList", "tlbLocationNoteList")
		.addToolbarTo("tlFourthAList", "tlbTlFourthAList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2", "canvas3"])
		.addChildrenTo2("detailsTab", ["NotesList", "attachmentList", "History"])
		.addChildrenTo2("detailsTabCanvas3", ["tlPricing", "tlPayment", "tlExpectedFees", "tlRound", "locationBidsList", "locationNotesList", "locationHistoryList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbCmpList", {dc: "tenderInvitation"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewTenderInv"),this._elems_.get("btnViewTenderDetails"),this._elems_.get("btnDeleteTenderInv"),this._elems_.get("btnPublishList"),this._elems_.get("btnUnpublishList"),this._elems_.get("btnExportTenderList"),this._elems_.get("btnHelpList")])
			.addReports()
			.addDefaultDblClickBtnAction("btnViewTenderDetails")
		.end()
		.beginToolbar("tlbTiEdit", {dc: "tenderInvitation"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnPublishEdit"),this._elems_.get("btnUnpublishEdit"),this._elems_.get("btnExportTenderEdit"),this._elems_.get("btnHelpEdit")])
			.addReports()
		.end()
		.beginToolbar("tlbTlList", {dc: "location"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewTenderLocation"),this._elems_.get("btnSaveTenderLocation"),this._elems_.get("btnCancelTenderLocation"),this._elems_.get("btnViewTenderLocDetails"),this._elems_.get("btnDeleteTenderLocation"),this._elems_.get("btnListMakeLocationBidMenu"),this._elems_.get("btnListMakeLocationBid"),this._elems_.get("btnListMakePerReceiverLocationBid"),this._elems_.get("btnDeclineLocationBidWithMenu"),this._elems_.get("btnDeclineSelectedLocationsBid"),this._elems_.get("btnDeclineNotOperatedLocationsBid"),this._elems_.get("btnDownloadBidFormList")])
			.addReports()
			.addDefaultDblClickBtnAction("btnViewTenderLocDetails")
		.end()
		.beginToolbar("tlbTlHeader", {dc: "location"})
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas2"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlblocationBidsList", {dc: "locationBid"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnViewBid"),this._elems_.get("btnDeleteBid"),this._elems_.get("btnSubmitBidForApproval"),this._elems_.get("btnApproveList"),this._elems_.get("btnRejectList"),this._elems_.get("btnSubmitSelected"),this._elems_.get("btnAwardBidList"),this._elems_.get("btnDeclineBidList"),this._elems_.get("btnCancelBidList"),this._elems_.get("btnAcceptAward"),this._elems_.get("btnDownloadBidFormEdit"),this._elems_.get("btnImportWithMenu")])
			.addReports()
		.end()
		.beginToolbar("tlbTlaList", {dc: "locationAirline"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewFuelReceiver"),this._elems_.get("btnSaveFuelReceiver"),this._elems_.get("btnCancelFuelReceiver"),this._elems_.get("btnDeleteFuelReceiver"),this._elems_.get("btnFuelReceiversListMakeLocationBidMenu"),this._elems_.get("btnFuelReceiversListMakeLocationBid"),this._elems_.get("btnFuelReceiversListMakePerReceiverLocationBid")])
			.addReports()
		.end()
		.beginToolbar("tlbTlRound", {dc: "locationRound"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewRound"),this._elems_.get("btnSaveRound"),this._elems_.get("btnCancelRound"),this._elems_.get("btnDeleteRound")])
			.addReports()
		.end()
		.beginToolbar("tlbNoteList", {dc: "note"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewTenderNote"),this._elems_.get("btnEditTenderNote"),this._elems_.get("btnDeleteTenderNote")])
			.addReports()
		.end()
		.beginToolbar("tlbAttachments", {dc: "attachment"})
			.addReports()
		.end()
		.beginToolbar("tlbLocationNoteList", {dc: "locationNote"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewTenderLocationNote"),this._elems_.get("btnEditTenderLocationNote"),this._elems_.get("btnDeleteTenderLocationNote")])
			.addReports()
		.end()
		.beginToolbar("tlbTlFourthAList", {dc: "locationAirlineNew"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancelSelected({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnPublishList
	 */
	,onBtnPublishList: function() {
		var successFn = function() {
			this._getDc_("tenderInvitation").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"publish",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("tenderInvitation").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnPublishEdit
	 */
	,onBtnPublishEdit: function() {
		var successFn = function() {
			this._getDc_("tenderInvitation").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"publish",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("tenderInvitation").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnUnpublishList
	 */
	,onBtnUnpublishList: function() {
		this.openUnpublishWindow();
	}
	
	/**
	 * On-Click handler for button btnUnpublishEdit
	 */
	,onBtnUnpublishEdit: function() {
		this.openUnpublishWindow();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseUnpublishWdw
	 */
	,onBtnSaveCloseUnpublishWdw: function() {
		var successFn = function() {
			this._getWindow_("wdwRemarks").close();
			this._getDc_("tenderInvitation").doReloadPage();
		};
		var failureFn = function(dc,response) {
			this._getWindow_("wdwRemarks").close();
			Main.rpcFailure(response);
		};
		var o={
			name:"unpublish",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("tenderInvitation").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnCancelUnpublishWdw
	 */
	,onBtnCancelUnpublishWdw: function() {
		this._getWindow_("wdwRemarks").close();
	}
	
	/**
	 * On-Click handler for button btnDeleteTenderInv
	 */
	,onBtnDeleteTenderInv: function() {
		this._getDc_("tenderInvitation").doDelete();
	}
	
	/**
	 * On-Click handler for button btnTIFirstContinue
	 */
	,onBtnTIFirstContinue: function() {
		this._jumpToSecondStep_();
	}
	
	/**
	 * On-Click handler for button btnTIFinishWdw
	 */
	,onBtnTIFinishWdw: function() {
		this._saveFirstStep_();
	}
	
	/**
	 * On-Click handler for button btnFinishStep2
	 */
	,onBtnFinishStep2: function() {
		this._saveSecondStep_();
	}
	
	/**
	 * On-Click handler for button btnFinishStep3
	 */
	,onBtnFinishStep3: function() {
		this._saveThirdStep_();
	}
	
	/**
	 * On-Click handler for button btnFinishStep4
	 */
	,onBtnFinishStep4: function() {
		this._saveFourthStep_();
	}
	
	/**
	 * On-Click handler for button btnTISecondContinue
	 */
	,onBtnTISecondContinue: function() {
		this._jumpToThirdStep_();
	}
	
	/**
	 * On-Click handler for button btnTIThirdContinue
	 */
	,onBtnTIThirdContinue: function() {
		this._jumpToFourthStep_();
	}
	
	/**
	 * On-Click handler for button btnViewTenderDetails
	 */
	,onBtnViewTenderDetails: function() {
		this._showStackedViewElement_('main', 'canvas2',this._get_('tiList')._markTender_('markRead',Main.translate('applicationMsg','tenderMarkedAsRead__lbl'),true,true));
	}
	
	/**
	 * On-Click handler for button btnViewTenderLocDetails
	 */
	,onBtnViewTenderLocDetails: function() {
		this._getDc_("location").doEditIn();
		this._showStackedViewElement_('main', 'canvas3');
	}
	
	/**
	 * On-Click handler for button btnCloseNoteWdw
	 */
	,onBtnCloseNoteWdw: function() {
		this._getDc_("note").doCancel();
		this._getWindow_("noteWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseNoteWdw
	 */
	,onBtnSaveCloseNoteWdw: function() {
		this.saveCloseNote();
		this._getDc_("note").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCloseLocationNoteWdw
	 */
	,onBtnCloseLocationNoteWdw: function() {
		this._getDc_("locationNote").doCancel();
		this._getWindow_("locationNoteWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseLocationNoteWdw
	 */
	,onBtnSaveCloseLocationNoteWdw: function() {
		this.saveCloseLocationNote();
		this._getDc_("locationNote").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnHelpList
	 */
	,onBtnHelpList: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnHelpEdit
	 */
	,onBtnHelpEdit: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnImportWithMenu
	 */
	,onBtnImportWithMenu: function() {
		this._getWindow_("wdwImportBid").show();
	}
	
	/**
	 * On-Click handler for button btnListMakeLocationBid
	 */
	,onBtnListMakeLocationBid: function() {
		this.openCaptureBidWindow(true, false)
	}
	
	/**
	 * On-Click handler for button btnListMakePerReceiverLocationBid
	 */
	,onBtnListMakePerReceiverLocationBid: function() {
		this.openCaptureBidWindow(false, false)
	}
	
	/**
	 * On-Click handler for button btnFuelReceiversListMakeLocationBid
	 */
	,onBtnFuelReceiversListMakeLocationBid: function() {
		this.openCaptureBidWindow(true, false)
	}
	
	/**
	 * On-Click handler for button btnFuelReceiversListMakePerReceiverLocationBid
	 */
	,onBtnFuelReceiversListMakePerReceiverLocationBid: function() {
		this.openCaptureBidWindow(false,true)
	}
	
	/**
	 * On-Click handler for button btnCaptureBidWdw
	 */
	,onBtnCaptureBidWdw: function() {
		this.captureBid();
		this._getWindow_("wdwCaptureBid").close();
	}
	
	/**
	 * On-Click handler for button btnCancelCaptureBidWdw
	 */
	,onBtnCancelCaptureBidWdw: function() {
		this._getWindow_("wdwCaptureBid").close();
	}
	
	/**
	 * On-Click handler for button btnSaveBidCancelWdw
	 */
	,onBtnSaveBidCancelWdw: function() {
		var successFn = function() {
			this._applyStateAllButtons_();
			this._getWindow_("wdwBidCancel").close();
			this._getDc_("locationBid").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"cancelBid",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("locationBid").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnCancelBidCancelWdw
	 */
	,onBtnCancelBidCancelWdw: function() {
		this._getWindow_("wdwBidCancel").close();
		this._getDc_("locationBid").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnDeclineSelectedLocationsBid
	 */
	,onBtnDeclineSelectedLocationsBid: function() {
		Ext.Msg.show({
		                       title : "Warning",
							   msg: Main.translate("applicationMsg", "declineBidsWarning_lbl"),
		                       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
		                       fn : function(btnId) {
		                              if( btnId === "yes" ){
		                                    this.declineSelectedLocationsBid();
		                              }
		                       },
		                       scope : this
		                    });
	}
	
	/**
	 * On-Click handler for button btnDeclineNotOperatedLocationsBid
	 */
	,onBtnDeclineNotOperatedLocationsBid: function() {
		Ext.Msg.show({
		                       title : "Warning",
							   msg: Main.translate("applicationMsg", "declineBidsWarning_lbl"),
		                       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
		                       fn : function(btnId) {
		                              if( btnId === "yes" ){
		                                    this.declineNotOperatedLocationsBidRpc();
		                              }
		                       },
		                       scope : this
		                    });
		this._getDc_("location").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnExportTenderList
	 */
	,onBtnExportTenderList: function() {
		this._exportTender_();
	}
	
	/**
	 * On-Click handler for button btnExportTenderEdit
	 */
	,onBtnExportTenderEdit: function() {
		this._exportTender_();
	}
	
	/**
	 * On-Click handler for button btnBackToFirstStep
	 */
	,onBtnBackToFirstStep: function() {
		this._jumpToFirstStep_();
	}
	
	/**
	 * On-Click handler for button btnBackToSecondStep
	 */
	,onBtnBackToSecondStep: function() {
		this._jumpToSecondStep_();
	}
	
	/**
	 * On-Click handler for button btnBackToThirdStep
	 */
	,onBtnBackToThirdStep: function() {
		this._jumpToThirdStep_();
	}
	
	/**
	 * On-Click handler for button btnAddGroupMemeber
	 */
	,onBtnAddGroupMemeber: function() {
	}
	
	/**
	 * On-Click handler for button btnNewTenderInv
	 */
	,onBtnNewTenderInv: function() {
		this._getDc_("tenderInvitationNew").doNew();
		this._getWindow_("tenderInvitationWdw").show();
	}
	
	/**
	 * On-Click handler for button btnViewBid
	 */
	,onBtnViewBid: function() {
		this._showBidDetails_();
	}
	
	/**
	 * On-Click handler for button btnDeleteBid
	 */
	,onBtnDeleteBid: function() {
		this._getDc_("locationBid").doDelete();
	}
	
	/**
	 * On-Click handler for button btnSubmitBidForApproval
	 */
	,onBtnSubmitBidForApproval: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnDownloadBidFormList
	 */
	,onBtnDownloadBidFormList: function() {
		var successFn = function(dc) {
			
								var dc = this._getDc_("location");
				                var fn = dc.params.get("formFile");
				                var r = window.open(Main.urlDownload + "/" + fn,"Form");
				                r.focus();   
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"downloadBidForm",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("location").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnDownloadBidFormEdit
	 */
	,onBtnDownloadBidFormEdit: function() {
		var successFn = function(dc) {
			
								var dc = this._getDc_("location");
				                var fn = dc.params.get("formFile");
				                var r = window.open(Main.urlDownload + "/" + fn,"Form");
				                r.focus();   
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"downloadBidForm",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("location").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnNewTenderLocation
	 */
	,onBtnNewTenderLocation: function() {
		this._getDc_("location").doNew();
	}
	
	/**
	 * On-Click handler for button btnSaveTenderLocation
	 */
	,onBtnSaveTenderLocation: function() {
		this._getDc_("location").doSave();
	}
	
	/**
	 * On-Click handler for button btnCancelTenderLocation
	 */
	,onBtnCancelTenderLocation: function() {
		this._getDc_("location").doCancel();
	}
	
	/**
	 * On-Click handler for button btnDeleteTenderLocation
	 */
	,onBtnDeleteTenderLocation: function() {
		this._getDc_("location").doDelete();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApproveWdw
	 */
	,onBtnSaveCloseApproveWdw: function() {
		this.approveRpc()
	}
	
	/**
	 * On-Click handler for button btnCancelApproveWdw
	 */
	,onBtnCancelApproveWdw: function() {
		this._getWindow_("wdwApproveNote").close();
		this._getDc_("locationBid").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCancelApprovalWdw
	 */
	,onBtnCancelApprovalWdw: function() {
		this._getWindow_("wdwApprovalNote").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApprovalWdw
	 */
	,onBtnSaveCloseApprovalWdw: function() {
		this.submitForApprovalRpc()
	}
	
	/**
	 * On-Click handler for button btnApproveList
	 */
	,onBtnApproveList: function() {
		this.openApproveWindow();
	}
	
	/**
	 * On-Click handler for button btnRejectList
	 */
	,onBtnRejectList: function() {
		this.openRejectWindow();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRejectWdw
	 */
	,onBtnSaveCloseRejectWdw: function() {
		this.rejectRpc()
	}
	
	/**
	 * On-Click handler for button btnCancelRejectWdw
	 */
	,onBtnCancelRejectWdw: function() {
		this._getWindow_("wdwRejectNote").close();
		this._getDc_("locationBid").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSubmitSelected
	 */
	,onBtnSubmitSelected: function() {
		var successFn = function(dc,response) {
			this.initFeedback(response)
			this._getDc_("locationBid").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"submitSelected",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("locationBid").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnAwardBidList
	 */
	,onBtnAwardBidList: function() {
		var successFn = function() {
			this._applyStateAllButtons_();
			this._getDc_("locationBid").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"awardBid",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("locationBid").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnDeclineBidList
	 */
	,onBtnDeclineBidList: function() {
		var successFn = function() {
			this._applyStateAllButtons_();
			this._getDc_("locationBid").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"declineBid",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("locationBid").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnCancelBidList
	 */
	,onBtnCancelBidList: function() {
		this.openCancelBidWindow();
	}
	
	/**
	 * On-Click handler for button btnAcceptAward
	 */
	,onBtnAcceptAward: function() {
		var successFn = function() {
			this._getDc_("locationBid").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"acceptAward",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("locationBid").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCloseBidValidationsWdw
	 */
	,onBtnCloseBidValidationsWdw: function() {
		this._getWindow_("wdwBidValidations").close();
	}
	
	/**
	 * On-Click handler for button btnCloseAndFilterBidValidationsWdw
	 */
	,onBtnCloseAndFilterBidValidationsWdw: function() {
		this.viewFailedBids();
	}
	
	/**
	 * On-Click handler for button btnNewTenderNote
	 */
	,onBtnNewTenderNote: function() {
		this._getDc_("note").doNew();
	}
	
	/**
	 * On-Click handler for button btnEditTenderNote
	 */
	,onBtnEditTenderNote: function() {
		this._getDc_("note").doEditIn();
	}
	
	/**
	 * On-Click handler for button btnDeleteTenderNote
	 */
	,onBtnDeleteTenderNote: function() {
		this._getDc_("note").doDelete();
	}
	
	/**
	 * On-Click handler for button btnNewTenderLocationNote
	 */
	,onBtnNewTenderLocationNote: function() {
		this._getDc_("locationNote").doNew();
	}
	
	/**
	 * On-Click handler for button btnEditTenderLocationNote
	 */
	,onBtnEditTenderLocationNote: function() {
		this._showEdit_();
	}
	
	/**
	 * On-Click handler for button btnDeleteTenderLocationNote
	 */
	,onBtnDeleteTenderLocationNote: function() {
		this._getDc_("locationNote").doDelete();
	}
	
	/**
	 * On-Click handler for button btnNewFuelReceiver
	 */
	,onBtnNewFuelReceiver: function() {
		this._getDc_("locationAirline").doNew();
	}
	
	/**
	 * On-Click handler for button btnSaveFuelReceiver
	 */
	,onBtnSaveFuelReceiver: function() {
		this._getDc_("locationAirline").doSave();
	}
	
	/**
	 * On-Click handler for button btnCancelFuelReceiver
	 */
	,onBtnCancelFuelReceiver: function() {
		this._getDc_("locationAirline").doCancel();
	}
	
	/**
	 * On-Click handler for button btnDeleteFuelReceiver
	 */
	,onBtnDeleteFuelReceiver: function() {
		this._getDc_("locationAirline").doDelete();
	}
	
	/**
	 * On-Click handler for button btnNewRound
	 */
	,onBtnNewRound: function() {
		this._getDc_("locationRound").doNew();
	}
	
	/**
	 * On-Click handler for button btnSaveRound
	 */
	,onBtnSaveRound: function() {
		this._getDc_("locationRound").doSave();
	}
	
	/**
	 * On-Click handler for button btnCancelRound
	 */
	,onBtnCancelRound: function() {
		this._getDc_("locationRound").doCancel();
	}
	
	/**
	 * On-Click handler for button btnDeleteRound
	 */
	,onBtnDeleteRound: function() {
		this._getDc_("locationRound").doDelete();
	}
	
	/**
	 * On-Click handler for button btnMarkUnread
	 */
	,onBtnMarkUnread: function() {
		var o={
			name:"markUnread",
			modal:true
		};
		this._getDc_("tenderInvitation").doRpcData(o);
	}
	
	,generateBidPerReceiver: function() {	
		var successFn = function() {
			
							var ctx = this;
							var f = function() {
								var successFn = function() {
									var bundle = "atraxo.mod.cmm";
									var frame = "atraxo.cmm.ui.extjs.frame.TenderBid_Ui";
									getApplication().showFrame(frame,{
										url:Main.buildUiPath(bundle, frame, false),
										params: {
											generatedBidCode: ctx._getDc_("location").getParamValue("generatedBidCode")
										},
										callback: function (params) {
											this._after_bid_was_generated_(params);
										}
									});
								};
								var o={
									name:"generateBidPerReceiver",
									callbacks:{
										successFn: successFn,
										successScope: this
									},
									modal:true
								};
								ctx._getDc_("location").doRpcData(o);
							}
			
							var existingBids = this._getDc_("location").getParamValue("existingBids");
							var bidVersion = this._getDc_("location").getParamValue("bidVersion");
			
			                if(existingBids){
			                    Ext.Msg.show({
			                       title : "Warning",
								   msg: Main.translate("applicationMsg", "existingBidsWarningPerReceiver__lbl") + " " + bidVersion + "?",
			                       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
			                       fn : function(btnId) {
			                              if( btnId === "yes" ){
			                                    f();
			                              }
			                       },
			                       scope : this
			                    });
							} else {
			                    f();
			                }
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"findBidForLocation",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("location").doRpcData(o);
	}
	
	,generateBidPerReceivers: function() {	
		var successFn = function() {
			
								var ctx = this;
								var locationAirline = this._getDc_("locationAirline");
								var location = this._getDc_("location");
								
								var f = function() {
									var successFn = function() {
										var bundle = "atraxo.mod.cmm";
										var frame = "atraxo.cmm.ui.extjs.frame.TenderBid_Ui";
										getApplication().showFrame(frame,{
											url:Main.buildUiPath(bundle, frame, false),
											callback: function (params) {
												this._after_bid_was_generated_from_receivers_()
											}
										});
									};
									var o={
										name:"generateBidPerReceivers",
										callbacks:{
											successFn: successFn,
											successScope: this
										},
										modal:true
									};
									locationAirline.doRpcDataList(o);
								}
			
								var existingBids = this._getDc_("location").getParamValue("existingBids");
								var bidVersion = this._getDc_("location").getParamValue("bidVersion");
								locationAirline.setParamValue("bidVersion",location.getParamValue("bidVersion"));
								
				                if(existingBids){
				                    Ext.Msg.show({
				                       title : "Warning",
									   msg: Main.translate("applicationMsg", "existingBidsWarningPerReceiver__lbl") + " " + bidVersion + "?",
				                       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
				                       fn : function(btnId) {
				                              if( btnId === "yes" ){
				                                    f();
													location.doReloadPage();
				                              }
				                       },	                       scope : this
				                    });
								} else {
				                    f();
									location.doReloadPage();
				                }
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"findBidForLocation",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("location").doRpcData(o);
	}
	
	,generateBid: function() {	
		var successFn = function(dc) {
			
			                    var dc = this._getDc_("location");
								var bidVersion = this._getDc_("location").getParamValue("bidVersion");
								var existingBids = this._getDc_("location").getParamValue("existingBids");
			
			                    if(existingBids){
			                        Ext.Msg.show({
			                           title : "Warning",
									   msg: Main.translate("applicationMsg", "existingBidsWarning__lbl") + " " + bidVersion + "?",
			                           buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
			                           fn : function(btnId) {
			                                  if( btnId === "yes" ){
			                                        this.generateBidFromLocation();
													dc.doReloadPage();
			                                    }
			                           },
			                           scope : this
			                        });
								}else{
			                        this.generateBidFromLocation();
									dc.doReloadPage();
			                    }
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"findBidForLocation",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("location").doRpcData(o);
	}
	
	,generateBidFromLocation: function() {	
		var successFn = function() {
				
								var bundle = "atraxo.mod.cmm";
								var frame = "atraxo.cmm.ui.extjs.frame.TenderBid_Ui";
								getApplication().showFrame(frame,{
									url:Main.buildUiPath(bundle, frame, false),
									params: {
										generatedBidCode: this._getDc_("location").getParamValue("generatedBidCode")
									},
									callback: function (params) {
										this._after_bid_was_generated_(params)
									}
								});
		};
		var o={
			name:"generateBidFromLocation",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("location").doRpcData(o);
	}
	
	,showNoteWdw: function() {	
		this._getWindow_("noteWdw").show();
	}
	
	,onNoteWdwClose: function() {	
		this._getDc_("note").doCancel();
	}
	
	,showLocationNoteWdw: function() {	
		this._getWindow_("locationNoteWdw").show();
	}
	
	,onLocationNoteWdwClose: function() {	
		this._getDc_("locationNote").doCancel();
	}
	
	,declineSelectedLocationsBid: function() {	
		var successFn = function() {
			this._getDc_("location").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"declineSelectedLocationsBid",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("location").doRpcDataList(o);
	}
	
	,canSubmitSelectedToIssuer: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if( record.data.tenderSource ==__CMM_TYPE__.TenderSource._IMPORTED_){
								if((record.data.tenderStatus ==__CMM_TYPE__.TenderStatus._NEW_ || record.data.tenderStatus ==__CMM_TYPE__.TenderStatus._UPDATED_) && 
								   record.data.bidLocBiddingStatus != __CMM_TYPE__.BiddingStatus._FINISHED_ &&
								   record.data.bidStatus == __CMM_TYPE__.BidStatus._DRAFT_  &&
								   record.data.bidTransmissionStatus == __CMM_TYPE__.TransmissionStatus._NEW_){
								   
								   if(_SYSTEMPARAMETERS_.sysbidapproval === "true" && record.data.bidApprovalStatus != __CMM_TYPE__.BidApprovalStatus._APPROVED_){
										return false;
								   }
								}else{
									return false;
								}
							}else{
								return false;
							}
							
						}
		
						return true;
	}
	
	,_onShow_from_externalInterfaceMessageHistory_: function(params) {
			
						var dc = this._getDc_("tenderInvitation");
						dc.setFilterValue("id", params.objectId);
						dc.doQuery({queryFromExternalInterface: true});
	}
	
	,canAwardDecline: function(dc) {
		 
						var selectedRecords = dc.selectedRecords;				
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.tenderSource == __CMM_TYPE__.TenderSource._CAPTURED_ && record.data.bidLocBiddingStatus != __CMM_TYPE__.BiddingStatus._FINISHED_){
								if(record.data.bidStatus == __CMM_TYPE__.BidStatus._DRAFT_){
									if(_SYSTEMPARAMETERS_.sysbidapproval === "true" && record.data.bidApprovalStatus != __CMM_TYPE__.BidApprovalStatus._APPROVED_){
										return false;
									}
								}else{
									return false;
								}
							}else{
								return false;
							}
						}
						return true;
	}
	
	,canCancelBid: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
		
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if (record.data.canCancel === false) {
								return false;
							}
						}
						return true;
	}
	
	,initFeedback: function(response) {
		
						var responseData = Ext.getResponseDataInJSON(response);
						this.showInvalidBids("wdwBidValidations", responseData.params);	
	}
	
	,canApproveReject: function(dc) {
		
						var r = dc.getRecord();
						if (r) {
							return ( r.data.bidStatus === __CMM_TYPE__.BidStatus._DRAFT_ && r.data.canBeCompleted == true) ;
							//canBeCompleted is set in TenderBid_DsService in postFind
						}
						return false;
	}
	
	,approveRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								//close window
								this._getWindow_("wdwApproveNote").close();
								
								//reload page
								dc.doReloadPage();
							}
						};
		
		                var dc = this._getDc_("locationBid");
		                var t = {
					        name: "approveTenderBid",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcData(t);
	}
	
	,rejectRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								//close window
								this._getWindow_("wdwRejectNote").close();
								
								//reload page
								dc.doReloadPage();
							}
						};
		
		                var dc = this._getDc_("locationBid");
		                var t = {
					        name: "rejectTenderBid",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcData(t);
	}
	
	,openRejectWindow: function() {
		
						var label = "Please provide a reason for rejection.";
						var title = "Reject";
						this.setupNoteWindow("wdwRejectNote","rejectNote","btnSaveCloseRejectWdw","btnCancelRejectWdw" , label , title);				
	}
	
	,openApproveWindow: function() {
		
						var label = "Please provide an approval reason.";
						var title = "Approve";
						this.setupNoteWindow("wdwApproveNote","approveNote","btnSaveCloseApproveWdw","btnCancelApproveWdw" , label , title);				
	}
	
	,submitForApprovalRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								var responseDescription = responseData.params.submitForApprovalDescription;							
								var responseResult = responseData.params.submitForApprovalResult;
								
								//close window
								this._getWindow_("wdwApprovalNote").close();
		
								//display results
								this.showInvalidBids("wdwBidValidations", responseData.params);	
		
								//reload page
								dc.doReloadPage();
							}
						};
		
						this._sendAttachments_();
		
		                var dc = this._getDc_("locationBid");
		                var t = {
					        name: "submitForApproval",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcDataList(t);
	}
	
	,_sendAttachments_: function() {
		
						var list = this._get_("documentsAssignList");
						var view = list.getView();
						var selectedAttachments = [];
						var dc = this._getDc_("locationBid");
						if (view.getSelectionModel().hasSelection()) {
						   var rows = view.getSelectionModel().getSelection();
						   Ext.each(rows, function(row) {
								selectedAttachments.push(row.data.id);
						   }, this);
						}
						dc.setParamValue("selectedAttachments",JSON.stringify(selectedAttachments));
	}
	
	,openSubmitForApprovalWindow: function() {
		
						var label = "Please provide a note for approval.";
						var title = "Submit for approval";
						this.setupNoteWindow("wdwApprovalNote","approvalNote","btnSaveCloseApprovalWdw","btnCancelApprovalWdw" , label , title);				
	}
	
	,setupNoteWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var approvalNoteField = this._get_(theForm)._get_("approvalNote");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						var ctx = this;
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(true);
						
						approvalNoteField.fieldLabel = label;
						approvalNoteField.setValue("");
						window.title = title;
						
						approvalNoteField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
								cancelButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
								cancelButton.setDisabled(true);
						    }
						});
						
						if ("wdwApprovalNote" == theWindow) {
							var view = this._get_("documentsAssignList");
							view.store.load();
						}
						window.show();
	}
	
	,canSubmitForApproval: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if( record.data.bidLocBiddingStatus != __CMM_TYPE__.BiddingStatus._FINISHED_ && record.data.bidStatus == __CMM_TYPE__.BidStatus._DRAFT_ ){
								if(record.data.bidApprovalStatus != __CMM_TYPE__.BidApprovalStatus._NEW_ && 
							       record.data.bidApprovalStatus != __CMM_TYPE__.BidApprovalStatus._REJECTED_){
								   return false;
								}	
							} else{
								return false;
							}
						}
		
						return true;
	}
	
	,canDeleteBid: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						if (dc) {
							for(var i=0 ; i<selectedRecords.length ; ++i){
								var record = selectedRecords[i];
								
								if( record.data.bidStatus != __CMM_TYPE__.BidStatus._DRAFT_){
									if(record.data.bidStatus == __CMM_TYPE__.BidStatus._DRAFT_){
										if(_SYSTEMPARAMETERS_.sysbidapproval === "true" && 
											(record.data.bidApprovalStatus == __CMM_TYPE__.BidApprovalStatus._APPROVED_ ||
											record.data.bidApprovalStatus == __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_)){
											return false;
										}
									}else if(record.data.bidStatus == __CMM_TYPE__.BidStatus._SUBMITTED_){
										if(record.data.bidTransmissionStatus == __CMM_TYPE__.TransmissionStatus._IN_PROGRESS_ || 
										   record.data.bidTransmissionStatus == __CMM_TYPE__.TransmissionStatus._TRANSMITTED_){
											return false;
										  }
									}else{
										return false;
									}
								}
							}
		
							return true;
						}				
	}
	
	,_showEdit_: function(el,record) {
		
						this._showStackedViewElement_("main", "canvas3");
	}
	
	,_closeNoteWdwOnMaskClick_: function(w) {
		
						w.mon(Ext.getBody(), "click", function(el, e){
				            w.close(w.closeAction);
				        }, w, { delegate: ".x-mask" });
	}
	
	,_openNotesWindow_: function(notes) {
		
						var w = this._getWindow_("locationNoteViewWdw");
						w.show(undefined, function() {
							var f = this._get_("tlNoteView");
							var n = f._get_("note");
							n.setRawValue(notes);
						}, this);
	}
	
	,_stopTabKey_: function(btn) {
		
						var continueBtn = this._get_("btnTIFirstContinue");
						btn.getEl().on("keydown", function(e) {
							if (e.getKey() === 9) {
								continueBtn.focus();
								e.stopEvent();
							}
						}, this);
	}
	
	,_setBtnState_: function(grid) {
		
		
						var s = grid.getStore();
						var b = this._get_("btnTISecondContinue");
						var b2 = this._get_("btnFinishStep2");
		
						s.on("load", function() {
							var count = s.getTotalCount();
							if (count > 0) {
								b._enable_();
								b2._enable_();
							}
							else {
								b._disable_();
								b2._disable_();
							}
						});
	}
	
	,canPublish: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.status != __CMM_TYPE__.TenderStatus._DRAFT_){
								return false;			
							}
						}
						return true;
	}
	
	,canUnpublish: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.source!=__CMM_TYPE__.TenderSource._CAPTURED_ || record.data.status != __CMM_TYPE__.TenderStatus._NEW_){
								return false;	
							}
						}
						return true;
	}
	
	,canModifyTender: function(dc) {
		
						var dcParent = dc._frame_._getDc_("tenderInvitation");
						var r = dcParent.getRecord();
						if( r ){
							if(r.data.status === __CMM_TYPE__.TenderStatus._DRAFT_ ){
								return true;
							}
						}
						return false;
	}
	
	,canCreateBidFromTenderLocation: function() {
		
		  				var result = false;
		
						var tenderStatus = null;
						var bidLocBiddingStatus = null;
		
		                var dc1 = this._getDc_("tenderInvitation");
		                var r1 = dc1.getRecord();
		               	if (r1) {
							tenderStatus = r1.get("status");					
						}
		
		 				var dc2 = this._getDc_("location");
						var r2 = dc2.getRecord();
		               	if (r2) {
							bidLocBiddingStatus = r2.get("locationBiddingStatus");
							locationValidity = r2.get("isValid");
						}
		
		                if ((tenderStatus == __CMM_TYPE__.TenderStatus._NEW_ || tenderStatus == __CMM_TYPE__.TenderStatus._UPDATED_) && 
								(bidLocBiddingStatus != __CMM_TYPE__.BiddingStatus._FINISHED_) && 
								(locationValidity === true)) {
							result = true;
						}
						return result;
	}
	
	,canCreateBidFromFuelReceiversAndLocation: function() {
		
		  				var result = false;
		
						var tenderStatus = null;
						var bidLocBiddingStatus = null;
		
						var locationValidity = true;
						var fuelReceiverValidity = true;
		
		                var dc1 = this._getDc_("tenderInvitation");
		                var r1 = dc1.getRecord();
		               	if (r1) {
							tenderStatus = r1.get("status");					
						}
		
		 				var dc2 = this._getDc_("location");
						var r2 = dc2.getRecord();
		
		               	if (r2) {
							bidLocBiddingStatus = r2.get("locationBiddingStatus");
							locationValidity = r2.get("isValid");
						}
		
						var dc3 = this._getDc_("locationAirline");
						var recs = dc3.selectedRecords;
						Ext.each(recs, function(r) {
							var isValid = r.get("isValid");
							if (isValid === false) {
								fuelReceiverValidity = false;
							}
						}, this);
		
		                if ((tenderStatus == __CMM_TYPE__.TenderStatus._NEW_ || tenderStatus == __CMM_TYPE__.TenderStatus._UPDATED_) && 
								(bidLocBiddingStatus != __CMM_TYPE__.BiddingStatus._FINISHED_) && 
								(locationValidity === true) && (fuelReceiverValidity === true)) {
							result = true;
						}
						return result;
	}
	
	,canDeclineLocationBid: function(dc) {
		
						var r = dc.getRecord();
						if (r) {
							//canBeDeclined is set in TenderLocation_DsService in postFind
							return r.data.canBeDeclined == true;
						}
						return false;
	}
	
	,_applyStateAllToolbars_: function(dc) {
		
						this._setComponentsToolbarState_(dc, "tlAList");
	}
	
	,_setComponentsToolbarState_: function(dc,view) {
		
						var dcView = this._get_(view);
						if(dcView){
							var items = dcView.dockedItems.items;
							var record = dc.getRecord();
							var toolbar = "";
							for (var i = 0; i < items.length; i++) {
								if (items[i].dock == "top" && items[i].xtype == "toolbar") {
									toolbar = items[i];
								}
							}
							if (!Ext.isEmpty(toolbar)) {
								if (record) {
									var tenderStatus = record.get("tenderStatus");
									var hasTotalVolume = record.get("hasTotalVolume");
		
									if (tenderStatus == "Draft" && hasTotalVolume == false) {
										this.disabledTlbFlag = false;
									}
									else{
										this.disabledTlbFlag = true;
									}
								}
							}
						}
	}
	
	,openUnpublishWindow: function() {
		
						var label = "Please provide a note.";
						var title = "Unpublish";
						this.setupRemarksWindow("wdwRemarks","tiRemarks","btnSaveCloseUnpublishWdw","btnCancelUnpublishWdw" , label , title);				
	}
	
	,_checkParam_: function(ctx) {
		
						var dc = ctx._getDc_("location");
						var flag = dc.getParamValue("captureBidFromHolderMA");
						var view = ctx._get_("captureBid");
						var field = view._get_("useFuelReceiverMasterAgrement");
						if (flag === true) {
							field._disable_();
						}
						else {
							field._enable_();
						}
	}
	
	,openCaptureBidWindow: function(captureBidFromLocation,selectReceivers) {
		
						var dc = this._getDc_("location");
		
						if(captureBidFromLocation){
							dc.setParamValue("captureBidFromHolderMA",true);
							dc.setParamValue("captureBidFromReceiverMA",false);
							dc.setParamValue("captureBidFromExistingBids",true);				
						}else{
							dc.setParamValue("captureBidFromHolderMA",false);
							dc.setParamValue("captureBidFromReceiverMA",true);
							dc.setParamValue("captureBidFromExistingBids",true);
						}
						dc.setParamValue("captureBidFromLocation",captureBidFromLocation);
						dc.setParamValue("selectReceivers",selectReceivers);
		
						this.setupCaptureBidWindow("wdwCaptureBid","captureBid","btnCaptureBidWdw","btnCancelCaptureBidWdw", this._checkParam_, this);
	}
	
	,openCancelBidWindow: function() {
		
						var label = "Please provide a reason for cancelling the bid.";
						var title = "Cancel Bid";
						this.setupCancelBidWindow("wdwBidCancel","bidCancelForm","btnSaveBidCancelWdw","btnCancelBidCancelWdw" , label , title);				
	}
	
	,_showBidDetails_: function() {
		
					var dc = this._getDc_("locationBid");
					var rec = dc.getRecord();
					var code = rec.get("code");
					var bundle = "atraxo.mod.cmm";
					var frame = "atraxo.cmm.ui.extjs.frame.TenderBid_Ui";
		
					getApplication().showFrame(frame,{
						url:Main.buildUiPath(bundle, frame, false),
						params: {
							generatedBidCode: code
						},
						callback: function (params) {
							this._after_bid_was_generated_(params)
						}
					});
	}
	
	,declineNotOperatedLocationsBidRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								var responseResult = responseData.params.submitToDataHubResponseResult;
								var responseDescription = responseData.params.submitToDataHubResponseDescription;							
								
								//display results
								if (responseResult == true) {
									Main.info(responseDescription); 
								}
								else {
									Main.error(responseDescription);
								}
		
								//reload page
								dc.doReloadPage();
							}
						};
		
		                var dc = this._getDc_("location");
		                var t = {
					        name: "declineNotOperatedLocationsBid",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcData(t);
	}
	
	,_setDefaultLovValue_: function(viewName,fieldToGet,valueToSet,fieldToReturn,fielToReturnValueIn,opt) {
		
		 
		                // -------------- Function config --------------
		 
		                var dcView = this._get_(viewName);
		 
		                // -------------- End function config --------------
		 
		                var field = dcView._get_(fieldToGet);
		                var valueField = field.valueField;
		                var record = dcView._controller_.getRecord();
		 
		                var store = field.store;			
		
		                store.load({
		                    callback: function(rec) {
								if (fieldToReturn instanceof Array) {
									var valueToReturn = [];
								}
								else {
									var valueToReturn = "";
								}                        
		 
		                        for (var i=0 ; i < rec.length; i++) {
		                            var data = rec[i].data;
		                            for (var key in data) {
		                                if (data[valueField] == valueToSet) {
		
											if (fieldToReturn instanceof Array) {
												for (var x = 0; x<fieldToReturn.length; x++) {
													valueToReturn.push(data[fieldToReturn[x]]);
												}
											}
											else {
												valueToReturn = data[fieldToReturn];
											}
		                                    break;									
		                                }
		                            }
		                        }
		
		                        if (record) {
		                            record.beginEdit();
		 
		                            record.set(field.dataIndex, valueToSet);
									if (fielToReturnValueIn instanceof Array) {
										for (var z = 0; z<fielToReturnValueIn.length; z++) {
											record.set(fielToReturnValueIn[z], valueToReturn[z]);
										}
									}
									else {
										record.set(fielToReturnValueIn, valueToReturn);
									}
		                            
		                            if (opt) {
		                                for (var key in opt) {
		                                    record.set(key, opt[key]); 
		                                }
		                            }
		                            record.endEdit();
		                        }
		                    }
		                },this);
	}
	
	,setupRemarksWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var remarksField = this._get_(theForm)._get_("remarks");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						var ctx = this;
						
						saveButton.setDisabled(true);
						
						remarksField.fieldLabel = label;
						remarksField.setValue("");
						window.title = title;
						
						remarksField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
						    }
						});
		
						window.show();
	}
	
	,setupCaptureBidWindow: function(theWindow,theForm,saveBtn,cancelBtn,callBackFn,ctx) {
		
						var window = this._getWindow_(theWindow);
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						var ctx = this;
		
						window.show();
						if (callBackFn && ctx) {
							callBackFn(ctx);
						}
	}
	
	,setupCancelBidWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var reasonField = this._get_(theForm)._get_("bidCancelReason");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						var ctx = this;
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(true);
						
						reasonField.fieldLabel = label;
						reasonField.setValue("");
						window.title = title;
						
						reasonField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
								cancelButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
								cancelButton.setDisabled(true);
						    }
						});
		
						window.setHeight(200);
						window.show();
	}
	
	,_saveFourthStep_: function() {
		
		
						var locationAirline = this._getDc_("locationAirlineNew");
						var tenderInvitationNew = this._getDc_("tenderInvitationNew");
						var tenderInvitation = this._getDc_("tenderInvitation");
						var r = locationAirline.getRecord();
						var ctx = this;
		
						var editFn = function() {
							var window = ctx._getWindow_("tenderInvitationWdw");
							window.close();
							tenderInvitation.doClearAllFilters();
							
							tenderInvitation.setFilterValue("id",ctx._createdTenderInvitationId_);
							ctx._showStackedViewElement_("main", "canvas2", tenderInvitation.doQuery());
							ctx._resetWizard_();
						}
						
						if (r) {
							if (r.dirty === true) {
								locationAirline.doSave({saveAndEdit: true,tiId: ctx._createdTenderInvitationId_});
								ctx._resetWizard_();
							}
							else {
								editFn();
							}
						}
						else {
							editFn();
						}
	}
	
	,_jumpToFourthStep_: function() {
		
						
						var w = this._getWindow_("tenderInvitationWdw");
						var locationAirline = this._getDc_("locationAirlineNew");
						var location = this._getDc_("locationNew");
		
						var hideElems = ["btnBackToSecondStep","btnFinishStep3","btnTIThirdContinue"];
						var showElems = ["btnBackToThirdStep", "btnFinishStep4"];
		
						// Change the panel from the center region
		
						Ext.each(hideElems, function(e) {
							this._get_(e).hide();
						}, this);
		
						Ext.each(showElems, function(e) {
							this._get_(e).show();
						}, this);
						
						var r = location.getRecord();
						r.set("flightServiceType","Scheduled");
						location.doSave();
						locationAirline.doReloadPage();
						w.setStep(4);
	}
	
	,_saveThirdStep_: function() {
		
		
						var location = this._getDc_("locationNew");
						var originalTi = this._getDc_("tenderInvitation");
						var r = location.getRecord();
						if (r && r.dirty === true) {
							r.set("flightServiceType","Scheduled");
							location.doSave({saveAndEdit: true});
						}
						else {
							var window = this._getWindow_("tenderInvitationWdw");
							window.close();
							originalTi.doClearAllFilters();
							originalTi.setFilterValue("id",this._createdTenderInvitationId_);
							this._showStackedViewElement_("main", "canvas2", originalTi.doQuery());
						}
		
						var hideElems = ["btnBackToSecondStep","btnFinishStep3","btnTIThirdContinue"];
						Ext.each(hideElems, function(e) {
							this._get_(e).hide();
						}, this);
						this._assignedLocationsList_ = [];
	}
	
	,_saveSecondStep_: function() {
		
						var tenderInvitation = this._getDc_("tenderInvitationNew");
						var originalTi = this._getDc_("tenderInvitation");
						var window = this._getWindow_("tenderInvitationWdw");	
		
						originalTi.doClearAllFilters();		
						originalTi.setFilterValue("id",this._createdTenderInvitationId_);				
						window.close();		
						this._showStackedViewElement_("main", "canvas2", originalTi.doQuery());
	}
	
	,_saveFirstStep_: function() {
		
						var tenderInvitationNew = this._getDc_("tenderInvitationNew");
						var tenderInvitation = this._getDc_("tenderInvitation");
						var r = tenderInvitationNew.getRecord();
						var w = this._getWindow_("tenderInvitationWdw");
						if (r) {
							if (r.dirty === true) {
								tenderInvitationNew.doSave({saveAndEdit: true});
							}
							else {
								w.close();
								if (!Ext.isEmpty(this._createdTenderInvitationId_)) {
									tenderInvitation.setFilterValue("id", this._createdTenderInvitationId_);
									this._showStackedViewElement_("main", "canvas2", tenderInvitation.doQuery());
								}
							}
						}
	}
	
	,_jumpToThirdStep_: function() {
		
						
						var w = this._getWindow_("tenderInvitationWdw");
						var hideElems = ["btnBackToFirstStep","btnFinishStep2","btnTISecondContinue","btnBackToThirdStep", "btnFinishStep4"];
						var showElems = ["btnBackToSecondStep","btnFinishStep3","btnTIThirdContinue"];
						var tiSourceListSearchField = Ext.getCmp(this._tiSourceListSearchFieldId_);
						var tiAssignListSearchFieldId = Ext.getCmp(this._tiAssignListSearchFieldId_);
						var location = this._getDc_("locationNew");
		
						tiSourceListSearchField.setValue("");
		
						// Change the panel from the center region
		
						Ext.each(hideElems, function(e) {
							this._get_(e).hide();
						}, this);
		
						Ext.each(showElems, function(e) {
							this._get_(e).show();
						}, this);
		
		//				location.advancedFilter = null;
						location.doReloadPage();
						w.setStep(3);
	}
	
	,_jumpToFirstStep_: function() {
		
						
						var w = this._getWindow_("tenderInvitationWdw");
						var hideElems = ["btnBackToFirstStep","btnFinishStep2","btnTISecondContinue","btnBackToThirdStep", "btnFinishStep4"];
						var showElems = ["btnTIFinishWdw","btnTIFirstContinue"];
		
						var form = this._get_("tiNew");
						var agreementFrom = form._get_("agreementFrom");
						var agreementTo = form._get_("agreementTo");
		
						// Change the panel from the center region
		
						Ext.each(hideElems, function(e) {
							this._get_(e).hide();
						}, this);
		
						Ext.each(showElems, function(e) {
							this._get_(e).show();
						}, this);
						
						this._getDc_("geoLocations").store.removeAll();
						
						var ctx = this;
						var f = function() {
							var btn = ctx._get_("btnTIFinishWdw");
							var continueBtn = ctx._get_("btnTIFirstContinue");
							var form = this._get_("tiNew").getForm();
							if (form.wasValid === false) {
								btn.disable();
								continueBtn.disable();
							}
							if (!Ext.isEmpty(ctx._agreementFrom_)) {					
								agreementFrom.setValue(new Date(ctx._agreementFrom_));
							}
							else {
								agreementFrom.setValue("");
							}
							if (!Ext.isEmpty(ctx._agreementTo_)) {
								agreementTo.setValue(new Date(ctx._agreementTo_));
							}
							else {
								agreementTo.setValue("");
							}	
							w.setStep(1);		
						}
						Ext.defer(f, 50, this);
	}
	
	,_jumpToSecondStep_: function() {
		
						
						var w = this._getWindow_("tenderInvitationWdw");
						var hideElems = ["btnTIFinishWdw","btnTIFirstContinue","btnBackToSecondStep","btnFinishStep3","btnTIThirdContinue"];
						var showElems = ["btnBackToFirstStep","btnFinishStep2","btnTISecondContinue"];
						var tenderInvitation = this._getDc_("tenderInvitationNew");
		
						// Change the panel from the center region
		
						Ext.each(hideElems, function(e) {
							this._get_(e).hide();
						}, this);
		
						Ext.each(showElems, function(e) {
							this._get_(e).show();
						}, this);
		
						tenderInvitation.doSave({storeResponse: true});
						w.setStep(2);
	}
	
	,_checkValidity_: function(form,valid) {
		
						var btn = this._get_("btnTIFinishWdw");
						var continueBtn = this._get_("btnTIFirstContinue");
						if (valid == true) {
							btn.enable();
							continueBtn.enable();
						}
						else {
							btn.disable();
							continueBtn.disable();
						}
	}
	
	,_injectSearchField_: function(id,gridName,fieldWidth,showResetBtn) {
		
						return { tbar: [{ 
								xtype: "textfield",
								id: id,
							    width: fieldWidth ? fieldWidth : 200,
							    emptyText: "Search for location code or name",
							    listeners: {
							        change: {
							            scope: this,
										buffer: 1000,
							            fn: function(field, newVal) {
		
							                var grid = this._get_(gridName);
							                
							                if (newVal && newVal != "*") {
												var af = [];
												var dateFilter = [];
												var dc = grid._controller_;
		
												for(i=0;i<grid.columns.length;i++){
													var operation ="like";
													var value1 = "%"+newVal+"%";
		
													if (grid.columns[i].dataIndex == "deliveryDate") {
		
														// If the value is a valid date in the following format: dd.mm.yyyy
		
														var splitValue = newVal.split(".");
														var formatedValue;
														if (splitValue.length == 3) {
															formatedValue = new Date(splitValue[2], splitValue[1] - 1, splitValue[0]);		
														}
														else {
															formatedValue = null;
														}
														operation = "=";
														value1 = formatedValue;
													}
		
													var o = {
														"id":null,
														"fieldName":grid.columns[i].dataIndex,
														"operation":operation,
														"value1": value1,
														"groupOp":"OR1"
													};
													if (grid.columns[i].hidden == false) {
														af.push(o);
													}
													
												}
												dc.advancedFilter = af;
												dc.doQuery();
							                }
							            }
							        }
							    }
							},{
								glyph: "xf021@FontAwesome",
		                        xtype: "button",
								hidden: (showResetBtn && showResetBtn === true) ? false : true,
								listeners: {
									click: {
										scope: this,
										fn: function() {
											var searchFieldId = Ext.getCmp(id);
											var grid = this._get_(gridName);
											var dc = grid._controller_;
											searchFieldId.setValue("");
		
											dc.advancedFilter = null;
											dc.doQuery();
										}
									}
								}}
							]}
	}
	
	,_setEmptyForEnum_: function(obj) {
		
						var fields = ["taxType","operator","paymentRefDate","invoiceFrequency",
						"invoiceType","prepaymentFrequency","paymentMethod","depositRequired","exchangeRateOffset","dataProvider","indexAveragingPeriod","sourceType",
						"sourceType","creditTerms","pricingBase"];
						Ext.each(fields, function(f) {
							for (var key in obj) {
								obj[f] = "";
							}
						}, this);
						return obj;
	}
	
	,_assignSelectedRecords_: function() {
		
						var grid = this._get_("tiSourceList");
						var selectedRecords = grid.getView().getSelectionModel().getSelection();
						var assignDc = this._getDc_("locationNew");
						var dsName = assignDc.dsName;
						var data = [];
		
						//find the current volume unit to use
						var currentVolumeUnit = null;
						var sessionVolumeUnit = getApplication().getSession().getUser().volumeUnitCode;
						if( sessionVolumeUnit != null ){
							currentVolumeUnit = sessionVolumeUnit;
						}else{
							if( _ACTIVESUBSIDIARY_.volumeUnitCode != ""){
								currentVolumeUnit = _ACTIVESUBSIDIARY_.volumeUnitCode
							}else{
								currentVolumeUnit = _SYSTEMPARAMETERS_.sysvol
							}
						}
		
						//populate ui with default data
						Ext.each(selectedRecords, function(selection) {
							var selData = selection.data;
							for (var key in selData) {
		
								// Default data
								selData["tenderId"] = this._createdTenderInvitationId_;
								selData["locationId"] = selData["id"];	
								selData["round"] = 1;
								selData["hasTotalVolume"] = "true";
								selData["agreementFrom"] = this._agreementFrom_;
								selData["agreementTo"] = this._agreementTo_;
								selData["locationBiddingStatus"] = __CMM_TYPE__.BiddingStatus._NEW_;
								selData["period"] = __CMM_TYPE__.Period._CONTRACT_PERIOD_;
								selData["deliveryPoint"] = __CMM_TYPE__.ContractSubType._INTO_PLANE_;
								selData["fuelProduct"] = __CMM_TYPE__.Product._JET_A1_;
								selData["volumeUnitCode"] = currentVolumeUnit;
								selData["flightServiceType"] = __CMM_TYPE__.FlightServiceType._SCHEDULED_;
								selData["volumeOffer"] = __FMBAS_TYPE__.TenderVolumeOffer._UNDEFINED_;
							}
							this._setEmptyForEnum_(selData);
							data.push(selData);
						}, this);
		
						Ext.Ajax.request({
				            url: Main.dsAPI(dsName, "json").create,
				            method: "POST",
				            params: {
				                data: Ext.JSON.encode(data)
				            },
				            success: function(response) {
								this._buildAssignedLocationsList_(response);
								assignDc.doReloadPage();
								this._filterSourceLocationsList_();
				            },
				            scope: this
				        });
	}
	
	,_assignAllRecords_: function() {
		
		
						var grid = this._get_("tiSourceList");
						var store = grid.store.getRange();
						var assignDc = this._getDc_("locationNew");
						var dsName = assignDc.dsName;
						var data = [];
		
						Ext.each(store, function(selection) {
							var selData = selection.data;
							for (var key in selData) {
		
								// Default data
		
								selData["tenderId"] = this._createdTenderInvitationId_;
								selData["locationId"] = selData["id"];	
								selData["round"] = 1;						
								selData["agreementFrom"] = this._agreementFrom_;
								selData["agreementTo"] = this._agreementTo_;
								selData["locationBiddingStatus"] = __CMM_TYPE__.BiddingStatus._NEW_;
								selData["period"] = __CMM_TYPE__.Period._CONTRACT_PERIOD_;
								selData["deliveryPoint"] = __CMM_TYPE__.ContractSubType._INTO_PLANE_;
								selData["fuelProduct"] = __CMM_TYPE__.Product._JET_A1_;
		
							}
							this._setEmptyForEnum_(selData);
							data.push(selData);
						}, this);
		
						Ext.Ajax.request({
				            url: Main.dsAPI(dsName, "json").create,
				            method: "POST",
				            params: {
				                data: Ext.JSON.encode(data)
				            },
				            success: function(response) {
								this._buildAssignedLocationsList_(response);
								assignDc.doReloadPage();
								this._filterSourceLocationsList_();
				            },
				            scope: this
				        });
	}
	
	,_refreshSourceList_: function(list) {
		
						var toRemove = [];				
						Ext.each(list, function(l) {
							var listData = l.data;
							toRemove.push(listData.locationCode);
						});
						this._assignedLocationsList_ = this._assignedLocationsList_.filter( function( el ) {
							return toRemove.indexOf( el ) < 0;
						});
						this._filterSourceLocationsList_();
	}
	
	,_deAssignSelectedRecords_: function() {
		
		
						var grid = this._get_("tiAssignList");
						var assignDc = grid._controller_;
						var selectedRecords = grid.getView().getSelectionModel().getSelection();
						assignDc.commands.doDelete.confirmByUser = false;
						assignDc.doDelete({refreshSourceList: true, selectedRecords: selectedRecords});
	}
	
	,_deAssignAllRecords_: function() {
		
		
						var grid = this._get_("tiAssignList");
						var assignDc = grid._controller_;
						var selectedRecords = grid.store.getRange();
		
						var ids = [];			
						Ext.each(selectedRecords, function(n) {
							var o = {
								id : n.data.id
							};
							ids.push(o);
						}, this);
			
						Ext.Ajax.request({
				            url: Main.dsAPI(assignDc.dsName, "json").destroy,
				            method: "POST",
				            scope: this,
				            params: {
				              data: JSON.stringify(ids)
				            },
				            success: function(response, opts) {
				            	assignDc.doReloadPage();
								this._refreshSourceList_(selectedRecords);
				            }
						});
		
						assignDc.commands.doDelete.confirmByUser = false;
						assignDc.doDelete({refreshSourceList: true, selectedRecords: selectedRecords});
	}
	
	,_filterSourceLocationsList_: function() {
		
						var sourceGrid = this._get_("tiSourceList");
						var store = sourceGrid.store;
						store.clearFilter();
						store.filterBy(function (record) {
							if (this._assignedLocationsList_.indexOf(record.get("code")) === -1) {
								return record;
							}					
		                }, this);							
						store.load();
	}
	
	,_buildAssignedLocationsList_: function(response) {
		
		
		        		var res = Ext.getResponseDataInJSON(response).data;				
						var data = [];
						Ext.each(res, function(r) {
							data.push(r.locationCode);
						}, this);
						this._assignedLocationsList_ = data.concat(this._assignedLocationsList_);
	}
	
	,_afterDefineElements_: function() {
		
		
						var builder = this._getBuilder_();
						this._wdwNorthPanelId_ = Ext.id();	
						this._wdwCenterPanelId_ = Ext.id();
						this._tiSourceListSearchFieldId_ = Ext.id();
						this._tiAssignListSearchFieldId_ = Ext.id();
						this._createdTenderInvitationId_ = null;
						this._createdTenderHolderCode_ = null;
						this._assignedLocationsList_ = null;
						this._agreementFrom_ = null;
						this._agreementTo_ = null;
						this._secondStepPanelId_ = Ext.id();
						this._fourthStepPanelId_ = Ext.id();
						this._tiFourthAssignListSearchFieldId_ = Ext.id();				
						this.disabledTlbFlag = false;
		
						builder.change("tiSourceList",this._injectSearchField_(this._tiSourceListSearchFieldId_, "tiSourceList"));
						builder.change("tiAssignList",this._injectSearchField_(this._tiAssignListSearchFieldId_, "tiAssignList"));
						builder.change("tiFourthAssignList",this._injectSearchField_(this._tiFourthAssignListSearchFieldId_, "tiFourthAssignList", 185, true));
	}
	
	,captureBid: function() {
		
						var location = this._getDc_("location");
						var captureBidFromLocation = location.getParamValue("captureBidFromLocation");
						if(captureBidFromLocation){
							this.generateBid();
						}else{
							var selectReceivers = this._getDc_("location").getParamValue("selectReceivers");
							if (selectReceivers) {
								this.generateBidPerReceivers();
							}
							else{
								this.generateBidPerReceiver();
							}
						}
	}
	
	,_exportTender_: function() {
		
						var mainGrid = this._get_("tiList"); 
						mainGrid._doReport_();
	}
	
	,openHelp: function() {
		
						var url = Main.urlHelp+"/TenderInvitations.html";
						window.open( url, "SONE_Help");
	}
	
	,_resetWizard_: function() {
		
						this._createdTenderInvitationId_ = null;
						this._assignedLocationsList_ = [];
						this._agreementFrom_ = null;
						this._agreementTo_ = null;
	}
	
	,_afterDefineDcs_: function() {
		
						var tenderInvitation = this._getDc_("tenderInvitation");
						var tenderInvitationNew = this._getDc_("tenderInvitationNew");
						var location = this._getDc_("location");
						var locationNew = this._getDc_("locationNew");
						var airline = this._getDc_("locationAirline");
						var locationAirline = this._getDc_("locationAirlineNew");
		
						locationAirline.on("afterDoNew", function (dc, ajaxResponse) {
							var r = dc.getRecord();
							var locationDc = this._getDc_("locationNew");
							var locationRecord = locationDc.getRecord();
							if (r && locationRecord) {						
								r.beginEdit();
								r.set("airlineCode", this._createdTenderHolderCode_);
								r.set("unitCode", locationRecord.get("volumeUnitCode"));
								r.set("volume", locationRecord.get("volume"));
								r.set("volumePeriod", locationRecord.get("period"));
								r.endEdit();
							}
						}, this);
		
						location.on("afterDoEditIn", function (dc) {
							this._applyStateAllToolbars_(dc);
						}, this);
		
						locationAirline.on("afterDoSaveSuccess", function (dc, ajaxResponse) {
							var fuelReceivers = this._getDc_("locationAirline");
							var saveAndEditFlag = ajaxResponse.options.options.saveAndEdit;
							var tenderInvId = ajaxResponse.options.options.tiId;
							if (saveAndEditFlag && saveAndEditFlag === true) {
								var window = this._getWindow_("tenderInvitationWdw");
								window.close();
								tenderInvitation.doClearAllFilters();
								tenderInvitation.setFilterValue("id",tenderInvId);
								tenderInvitation.doQuery();
								this._showStackedViewElement_("main", "canvas2");
								fuelReceivers.doReloadPage();
							}
							this._applyStateAllButtons_();
						}, this);
		
						locationAirline.on("afterDoDeleteSuccess", function (dc, ajaxResponse) {
							this._applyStateAllButtons_(); 
						}, this);
		
						locationAirline.on("afterDoCancel", function (dc, ajaxResponse) {
							this._applyStateAllButtons_();
						}, this);
		
						tenderInvitationNew.on("afterDoSaveSuccess", function (dc, ajaxResponse) {	
				
							var storeResponseFlag = ajaxResponse.options.options.storeResponse;
							var saveAndEditFlag = ajaxResponse.options.options.saveAndEdit;
							var res = Ext.getResponseDataInJSON(ajaxResponse.response).data[0];
							var tenderInvitation = this._getDc_("tenderInvitation");
		
							if (storeResponseFlag && storeResponseFlag === true) {			
					
								this._createdTenderInvitationId_ = res.id;
								this._agreementFrom_ = res.agreementFrom;
								this._agreementTo_ = res.agreementTo;
							}
		
							if (saveAndEditFlag && saveAndEditFlag === true) {
		
								var tenderCode = res.code;
								var window = this._getWindow_("tenderInvitationWdw");
								window.close();			
								tenderInvitation.doClearAllFilters();
								tenderInvitation.setFilterValue("code",tenderCode);
								this._showStackedViewElement_("main", "canvas2", tenderInvitation.doQuery());
		
							}
							this._createdTenderHolderCode_ = res.holderCode; 
						}, this);		
		
						locationNew.on("afterDoSaveSuccess", function (dc, ajaxResponse) {
							var window = this._getWindow_("tenderInvitationWdw");
							var saveAndEditFlag = ajaxResponse.options.options.saveAndEdit;
							if (saveAndEditFlag && saveAndEditFlag === true) {
								window.close();
								tenderInvitation.doClearAllFilters();
								tenderInvitation.setFilterValue("id",this._createdTenderInvitationId_);
								this._showStackedViewElement_("main", "canvas2", tenderInvitation.doQuery());
							}
							tenderInvitationNew.doReloadRecord();
						}, this);
		
						location.on("afterDoSaveSuccess", function (dc, ajaxResponse) {
							tenderInvitation.doReloadRecord();
						}, this);		
		
						locationNew.on("afterDoDeleteSuccess", function (dc, ajaxResult) {	
							if (ajaxResult.options.options.refreshSourceList === true) {
								var selectedRecords = ajaxResult.options.options.selectedRecords;
								this._refreshSourceList_(selectedRecords);
							}
							tenderInvitation.doReloadRecord();
						}, this);
		
						location.on("onEditOut", function(dc) { 
							tenderInvitation.doReloadRecord();
						}, this);
		
						airline.on("afterDoSaveSuccess", function () {
							location.doReloadRecord();
						},this);
		
						airline.on("afterDoDeleteSuccess", function (dc) {	
							location.doReloadRecord();
						}, this);
		
						//********* Note **********//
		
						var note = this._getDc_("note");
			
						note.on("afterDoNew", function (dc) {
						    this.showNoteWdw();
						}, this);
			
						note.on("OnEditIn", function(dc) {
						   this.showNoteWdw();
						}, this);
						
						note.on("afterDoSaveSuccess", function(dc, ajaxResult) {
						    if (ajaxResult.options.options.doNewAfterSave === true) {
						        dc.doNew();
						    } else if (ajaxResult.options.options.doCloseNoteWindowAfterSave === true) {
						        this._getWindow_("noteWdw").close();
						    }
		dc.doReloadRecord();
						}, this);
		
						//********* Location Note **********//
		
						var locationNote = this._getDc_("locationNote");
			
						locationNote.on("afterDoNew", function (dc) {
						    this.showLocationNoteWdw();
						}, this);
			
						locationNote.on("OnEditIn", function(dc) {
						   this.showLocationNoteWdw();
						}, this);
						
						locationNote.on("afterDoSaveSuccess", function(dc, ajaxResult) {
						    if (ajaxResult.options.options.doNewAfterSave === true) {
						        dc.doNew();
						    } else if (ajaxResult.options.options.doCloseNoteWindowAfterSave === true) {
						        this._getWindow_("locationNoteWdw").close();
						    }
		dc.doReloadRecord();
						}, this);
	}
	
	,saveCloseNote: function() {
		
					var note = this._getDc_("note");
					note.doSave({doCloseNoteWindowAfterSave: true });
	}
	
	,saveCloseLocationNote: function() {
		
					var locationNote = this._getDc_("locationNote");
					locationNote.doSave({doCloseNoteWindowAfterSave: true });
	}
	
	,togglePricingType: function(dcName,show) {
		
						var dc = this._getDc_(dcName);
						if(show){	
							if(!(dc.getRecord())){
								dc.doNew();
							}
						}else{
							if(dc.getRecord()){
								dc.doDelete();
							}
						}
	}
	
	,togglePaymentChoise: function(dcName,show) {
		
						var dc = this._getDc_(dcName);
						if(show){
							if(!(dc.getRecord())){
								dc.doNew();
								dc.setEditMode();
							}else{
								dc.doSave();
							}
						}else{
							if(dc.getRecord()){
								dc.doDelete();
							}
						}
	}
	
	,fnCancelSelected: function() {
		
						var dc = this._getDc_("tenderInvitation");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAll: function() {
		
						var dc = this._getDc_("tenderInvitation");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isCancelEnabled(dc);
	}
	
	,onTIWdwClose: function() {
		
						var dcNew = this._getDc_("tenderInvitationNew");
						var dc = this._getDc_("tenderInvitation");
						dcNew.clearData();		
						dcNew.doCancel();
						this._agreementTo_ = null;
						this._agreementFrom_ = null;
	}
	
	,showInvalidBids: function(theWindow,params) {
		
						//variables
						var window = this._getWindow_(theWindow);
						var form  = this._get_("bidValidationsLabel");
						var succesfullySubmittedField = form._get_("succesfullySubmitedBidsLabel");
						var concurencyCheckFailedBidsField = form._get_("concurencyCheckFailedBidsLabel");
						var validationErrorsBidsField = form._get_("validationErrorsBidsLabel");
						var validationsList  = this._get_("bidValidationsList");
						var filterBidsButton = this._get_("btnCloseAndFilterBidValidationsWdw");
						var allBidsSubmittedSuccesfully = true;
		
						//reset windows
						concurencyCheckFailedBidsField.show();
						validationErrorsBidsField.show();
						validationsList.show();
						filterBidsButton.show();
						window.height = 400;
						window.width = 700;
						
						succesfullySubmittedField.setFieldLabel(params.submitForApprovalSuccesfullySubmitted);
		
						if(params.submitForApprovalConcurencyCheckFail) {
							concurencyCheckFailedBidsField.setFieldLabel(params.submitForApprovalConcurencyCheckFail);
							allBidsSubmittedSuccesfully = false;
						} else {
							concurencyCheckFailedBidsField.hide();
						}
		
						if(params.submitForApprovalInvalidBids) {
							validationErrorsBidsField.setFieldLabel(params.submitForApprovalInvalidBids);
							allBidsSubmittedSuccesfully = false;
						} else {
							validationErrorsBidsField.hide();
						}
		
						if (allBidsSubmittedSuccesfully) {
							validationsList.hide();
							filterBidsButton.hide();
							window.height = 150;
							window.width = 330;
						}
		
		
						var dc = this._getDc_("bidValidations");
						dc.doClearAllFilters();
						var af = [];
						var o = {
							"fieldName":"objectId",
							"id":null,
							"operation":"in",
							"value1": params.invalidBidIds
							};
						af.push(o);
						dc.advancedFilter = af;
						dc.doQuery();
		
						window.show();
	}
	
	,viewFailedBids: function() {
		
						var win = this._getWindow_("wdwBidValidations");
						win.close();
		
						var frame = "atraxo.cmm.ui.extjs.frame.TenderBid_Ui";
						var bundle = "atraxo.mod.cmm";
						var validationDc = this._getDc_("bidValidations");
		
						var o = {
							"fieldName":"refid",
							"id":null,
							"operation":"in",
							"value1": validationDc.advancedFilter[0].value1
							};
						
						getApplication().showFrame(frame,{
								url:Main.buildUiPath(bundle, frame, false),
								params: {
									advancedFilter: o
								},
								callback: function (params) {
									this._onShow_from_TenderInvitation_(params)
								}
							});
	}
});
