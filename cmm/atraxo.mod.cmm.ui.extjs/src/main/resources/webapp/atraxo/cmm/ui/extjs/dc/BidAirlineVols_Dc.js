/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.BidAirlineVols_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.ShipTo_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.ShipTo_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.cmm.ui.extjs.dc.BidAirlineVols_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.cmm_BidAirlineVols_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"customer", dataIndex:"customerCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeExtendedLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "customerId"} ]}})
			.addLov({name:"customerName", dataIndex:"customerName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "customerId"} ]}})
			.addNumberField({name:"allocatedVolume", dataIndex:"allocatedVolume", sysDec:"dec_unit", maxLength:21})
			.addNumberField({name:"actualVolume", dataIndex:"actualVolume", sysDec:"dec_unit", maxLength:21})
		;
	}

});

/* ================= EDIT FORM: EditRemark ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.BidAirlineVols_Dc$EditRemark", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_BidAirlineVols_Dc$EditRemark",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.remarks}", paramIndex:"remarks", labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  layout:"fit"})
		.addPanel({ name:"remarkCol1", layout:"fit"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["remarkCol1"])
		.addChildrenTo("remarkCol1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
						remarks.setReadOnly(false);
						return true;
	}
});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.BidAirlineVols_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.cmm_BidAirlineVols_Dc$EditList",
	_noPaginator_: true,

	/**
	 * 
	 */
	initComponent : function(config) {
		Ext.apply(this, {
			features: [{
				ftype: 'summary',
				remoteRoot: 'summaries',
				dock: 'bottom'
			}]
		});
		this.callParent(arguments);
	},
	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"customer", dataIndex:"customerCode", width:150, _enableFn_: this._enableAirlineEdit_, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_CustomerCodeExtendedLov_Lov", maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "customerId"} ,{lovField:"name", dsField: "customerName"} ]}})
		.addLov({name:"customerName", dataIndex:"customerName", width:250, noEdit: true, xtype:"gridcolumn"})
		.addNumberColumn({name:"tenderBidVolume", dataIndex:"tenderBidVolume", width:180, noEdit: true, sysDec:"dec_unit",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_unit']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, maxLength:21, align:"right" })
		.addNumberColumn({name:"offeredVolume", dataIndex:"offeredVolume", width:150, sysDec:"dec_unit",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_unit']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, _enableFn_: this._enableOfferedVolume_, maxLength:21, align:"right",listeners:{
			fpchange:{scope:this, fn:this._calculatePercent_}, buffer: Main.viewConfig.GRID_EDITOR_CHANGE_EVT_BUFFER
		} })
		.addNumberColumn({name:"awardedVolume", dataIndex:"allocatedVolume", width:150, sysDec:"dec_unit",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_unit']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, _enableFn_: this._enableAwardedVolume_, maxLength:21, align:"right",listeners:{
			fpchange:{scope:this, fn:this._calculatePercent_}, buffer: Main.viewConfig.GRID_EDITOR_CHANGE_EVT_BUFFER
		} })
		.addTextColumn({name:"settlementUnitCode", dataIndex:"settlementUnitCode", width:60, noEdit: true, maxLength:2})
		.addNumberColumn({name:"percentCovered", dataIndex:"percentCovered", width:150, noEdit: true, sysDec:"dec_prc", maxLength:19, align:"right",  renderer:function(val, meta, record, rowIndex) { return this._addPercent_(val, meta, record, rowIndex); } })
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._unitParamName_ = "unitParameter";
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_addPercent_: function(value,meta,record,rowIndex) {
		
							return value + " % ";
	},
	
	_enableAwardedVolume_: function() {
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var bidStatus = dc.params.get("contractStatus");
						var approvalStatus = dc.params.get("approvalStatus");
						var tenderSource = dc.params.get("tenderSource");
		
						if(tenderSource == __CMM_TYPE__.TenderSource._CAPTURED_ && bidStatus == __CMM_TYPE__.BidStatus._DRAFT_){
							if(_SYSTEMPARAMETERS_.sysbidapproval === "true" && approvalStatus == __CMM_TYPE__.BidApprovalStatus._APPROVED_){
								return true;
							}
		
							return true;
						}
		
						return false;
	},
	
	_enableOfferedVolume_: function() {
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var status = dc.params.get("contractStatus");
						return (status == __CMM_TYPE__.BidStatus._DRAFT_);
	},
	
	_enableAirlineEdit_: function() {
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var status = dc.params.get("contractStatus");		
						return (status == __CMM_TYPE__.BidStatus._DRAFT_);
	},
	
	_calculatePercent_: function() {
		
						var dc = this._controller_;
						var record = dc.getRecord();
		                var tenderBidVolume = record.get("tenderBidVolume");
						var offeredVolume = record.get("offeredVolume");
						var awardedVolume = record.get("allocatedVolume");
						if(tenderBidVolume != null &&  tenderBidVolume != 0){
							var percentage = 0;
							if (awardedVolume != null && awardedVolume >= 0 ) {
								percentage = (awardedVolume / tenderBidVolume) * 100;
							}
							else if (offeredVolume != null && offeredVolume >= 0) {
								percentage = (offeredVolume / tenderBidVolume) * 100;
							}
							
							//Set format to system decimals
							percentage = Ext.util.Format.number(percentage, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_prc));
							record.set("percentCovered", percentage);
						}
						
	}
});
