/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.ShipTo_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_ShipTo_Ds"
	},
	
	
	validators: {
		customerCode: [{type: 'presence'}],
		validFrom: [{type: 'presence'}],
		validTo: [{type: 'presence'}]
	},
	
	fields: [
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"allocatedVolume", type:"float", allowNull:true},
		{name:"adjustedVolume", type:"float", allowNull:true},
		{name:"actualVolume", type:"float", allowNull:true},
		{name:"offeredVolume", type:"float", allowNull:true},
		{name:"tenderBidVolume", type:"float", allowNull:true},
		{name:"contractId", type:"int", allowNull:true},
		{name:"contractCode", type:"string"},
		{name:"contractPeriod", type:"string"},
		{name:"shipToApprovalStatus", type:"string"},
		{name:"bidStatus", type:"string"},
		{name:"settlementUnitCode", type:"string"},
		{name:"settlementUnitId", type:"int", allowNull:true},
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerCode", type:"string"},
		{name:"customerName", type:"string"},
		{name:"percentCovered", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.ShipTo_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"allocatedVolume", type:"float", allowNull:true},
		{name:"adjustedVolume", type:"float", allowNull:true},
		{name:"actualVolume", type:"float", allowNull:true},
		{name:"offeredVolume", type:"float", allowNull:true},
		{name:"tenderBidVolume", type:"float", allowNull:true},
		{name:"contractId", type:"int", allowNull:true},
		{name:"contractCode", type:"string"},
		{name:"contractPeriod", type:"string"},
		{name:"shipToApprovalStatus", type:"string"},
		{name:"bidStatus", type:"string"},
		{name:"settlementUnitCode", type:"string"},
		{name:"settlementUnitId", type:"int", allowNull:true},
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerCode", type:"string"},
		{name:"customerName", type:"string"},
		{name:"percentCovered", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.cmm.ui.extjs.ds.ShipTo_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"approvalStatus", type:"string"},
		{name:"contractDealType", type:"string"},
		{name:"contractStatus", type:"string"},
		{name:"remarks", type:"string"},
		{name:"tenderSource", type:"string"},
		{name:"totalVolume", type:"float", allowNull:true},
		{name:"unitParameter", type:"string"}
	]
});
