/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.ContractPriceCategory_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_ContractPriceCategory_Ds"
	},
	
	
	initRecord: function() {
		this.set("continous", true);
		this.set("includeInAverage", true);
		this.set("vat", "Not applicable");
		this.set("exchangeRateOffset", "Current");
		this.set("defaultPriceCtgy", true);
		this.set("quantityType", "Gross volume");
		this.set("calculateIndicator", "Included");
	},
	
	fields: [
		{name:"contId", type:"int", allowNull:true, noSort:true},
		{name:"contCode", type:"string", noSort:true},
		{name:"contStatus", type:"string", noSort:true},
		{name:"readOnly", type:"boolean", noSort:true},
		{name:"bidApprovalStatus", type:"string", noSort:true},
		{name:"isContract", type:"boolean", noSort:true},
		{name:"suppId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"suppCode", type:"string", noFilter:true, noSort:true},
		{name:"customerId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"customerCode", type:"string", noFilter:true, noSort:true},
		{name:"locId", type:"int", allowNull:true, noSort:true},
		{name:"locCode", type:"string", noFilter:true, noSort:true},
		{name:"formTitle", type:"string", noFilter:true, noSort:true},
		{name:"name", type:"string", noSort:true},
		{name:"continous", type:"boolean", noSort:true},
		{name:"includeInAverage", type:"boolean", noSort:true},
		{name:"quantityType", type:"string", noSort:true},
		{name:"exchangeRateOffset", type:"string", noSort:true},
		{name:"restriction", type:"boolean", noSort:true},
		{name:"vat", type:"string", noSort:true},
		{name:"defaultPriceCtgy", type:"boolean", noSort:true},
		{name:"comments", type:"string", noSort:true},
		{name:"forex", type:"string", noSort:true},
		{name:"calculateIndicator", type:"string", noSort:true},
		{name:"priceCategoryIdsJson", type:"string", noSort:true},
		{name:"contract", type:"int", allowNull:true, noSort:true},
		{name:"contractValidFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"contractValidTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"order", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"value", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"currencyId", type:"int", allowNull:true, noSort:true},
		{name:"currencyCode", type:"string", noSort:true},
		{name:"unitId", type:"int", allowNull:true, noSort:true},
		{name:"unitCode", type:"string", noSort:true},
		{name:"priceCategory", type:"int", allowNull:true, noSort:true},
		{name:"priceCtgryName", type:"string", noSort:true},
		{name:"priceCtgryPricePer", type:"string", noSort:true},
		{name:"iataPriceCatCode", type:"string", noSort:true},
		{name:"iataPriceCatUse", type:"string", noSort:true},
		{name:"mainCategoryCode", type:"string", noSort:true},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceName", type:"string", noSort:true},
		{name:"financialSourceCode", type:"string", noSort:true},
		{name:"avgMethodIndicatorDefMeth", type:"boolean", noSort:true},
		{name:"avgMethodIndicatorName", type:"string", noSort:true},
		{name:"avgMthdId", type:"int", allowNull:true, noSort:true},
		{name:"price", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"pricingBaseId", type:"int", allowNull:true, noSort:true},
		{name:"convertTitle", type:"string", noFilter:true, noSort:true},
		{name:"convertedPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"used", type:"boolean", noFilter:true, noSort:true},
		{name:"slash", type:"string", noFilter:true, noSort:true},
		{name:"percentageOf", type:"string", noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.ContractPriceCategory_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"contId", type:"int", allowNull:true, noSort:true},
		{name:"contCode", type:"string", noSort:true},
		{name:"contStatus", type:"string", noSort:true},
		{name:"readOnly", type:"boolean", allowNull:true, noSort:true},
		{name:"bidApprovalStatus", type:"string", noSort:true},
		{name:"isContract", type:"boolean", allowNull:true, noSort:true},
		{name:"suppId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"suppCode", type:"string", noFilter:true, noSort:true},
		{name:"customerId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"customerCode", type:"string", noFilter:true, noSort:true},
		{name:"locId", type:"int", allowNull:true, noSort:true},
		{name:"locCode", type:"string", noFilter:true, noSort:true},
		{name:"formTitle", type:"string", noFilter:true, noSort:true},
		{name:"name", type:"string", noSort:true},
		{name:"continous", type:"boolean", allowNull:true, noSort:true},
		{name:"includeInAverage", type:"boolean", allowNull:true, noSort:true},
		{name:"quantityType", type:"string", noSort:true},
		{name:"exchangeRateOffset", type:"string", noSort:true},
		{name:"restriction", type:"boolean", allowNull:true, noSort:true},
		{name:"vat", type:"string", noSort:true},
		{name:"defaultPriceCtgy", type:"boolean", allowNull:true, noSort:true},
		{name:"comments", type:"string", noSort:true},
		{name:"forex", type:"string", noSort:true},
		{name:"calculateIndicator", type:"string", noSort:true},
		{name:"priceCategoryIdsJson", type:"string", noSort:true},
		{name:"contract", type:"int", allowNull:true, noSort:true},
		{name:"contractValidFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"contractValidTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"order", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"value", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"currencyId", type:"int", allowNull:true, noSort:true},
		{name:"currencyCode", type:"string", noSort:true},
		{name:"unitId", type:"int", allowNull:true, noSort:true},
		{name:"unitCode", type:"string", noSort:true},
		{name:"priceCategory", type:"int", allowNull:true, noSort:true},
		{name:"priceCtgryName", type:"string", noSort:true},
		{name:"priceCtgryPricePer", type:"string", noSort:true},
		{name:"iataPriceCatCode", type:"string", noSort:true},
		{name:"iataPriceCatUse", type:"string", noSort:true},
		{name:"mainCategoryCode", type:"string", noSort:true},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceName", type:"string", noSort:true},
		{name:"financialSourceCode", type:"string", noSort:true},
		{name:"avgMethodIndicatorDefMeth", type:"boolean", allowNull:true, noSort:true},
		{name:"avgMethodIndicatorName", type:"string", noSort:true},
		{name:"avgMthdId", type:"int", allowNull:true, noSort:true},
		{name:"price", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"pricingBaseId", type:"int", allowNull:true, noSort:true},
		{name:"convertTitle", type:"string", noFilter:true, noSort:true},
		{name:"convertedPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"used", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"slash", type:"string", noFilter:true, noSort:true},
		{name:"percentageOf", type:"string", noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.cmm.ui.extjs.ds.ContractPriceCategory_DsParam", {
	extend: 'Ext.data.Model',


	validators: {
		settlementCurrencyCode: [{type: 'presence'}],
		settlementUnitCode: [{type: 'presence'}]
	},


	initParam: function() {
		this.set("changeOrder", false);
	},

	fields: [
		{name:"changeOrder", type:"boolean"},
		{name:"isDefault", type:"boolean"},
		{name:"priceDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"settlementCurrencyCode", type:"string"},
		{name:"settlementCurrencyId", type:"int", allowNull:true},
		{name:"settlementUnitCode", type:"string"},
		{name:"total", type:"float", allowNull:true}
	]
});
