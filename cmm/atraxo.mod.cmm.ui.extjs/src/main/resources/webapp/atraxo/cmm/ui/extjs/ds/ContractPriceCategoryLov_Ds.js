/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.ContractPriceCategoryLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_ContractPriceCategoryLov_Ds"
	},
	
	
	fields: [
		{name:"contId", type:"int", allowNull:true},
		{name:"contCode", type:"string"},
		{name:"name", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.ContractPriceCategoryLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"contId", type:"int", allowNull:true},
		{name:"contCode", type:"string"},
		{name:"name", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
