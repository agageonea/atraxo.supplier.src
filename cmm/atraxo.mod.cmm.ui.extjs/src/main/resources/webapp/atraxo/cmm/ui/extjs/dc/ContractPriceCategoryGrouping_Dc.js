/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceCategoryGrouping_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.ContractPriceCategoryGrouping_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.ContractPriceCategoryGrouping_Ds
});

/* ================= GRID: List ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceCategoryGrouping_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_ContractPriceCategoryGrouping_Dc$List",
	_noPaginator_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:160,  flex:1})
		.addTextColumn({ name:"priceCategory", dataIndex:"priceCtgryName", width:160,  flex:1})
		.addTextColumn({ name:"mainCategory", dataIndex:"mainCategoryCode", width:160,  flex:1})
		.addNumberColumn({ name:"price", dataIndex:"price", width:160, decimals:6,  renderer:function(val, meta, record, rowIndex) { return this._markUtilization_(val, meta, record); }, flex:1})
		.addTextColumn({ name:"currency", dataIndex:"currencyCode", width:80,  flex:1})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:60,  flex:1})
		.addNumberColumn({ name:"convertedPrice", dataIndex:"convertedPrice", width:120, decimals:6,  flex:1})
		.addNumberColumn({ name:"contractAmount", dataIndex:"contractAmount", width:120, decimals:6,  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_markUtilization_: function(value,meta,record) {
		
						var used = record.get("used");
		
						if (!Ext.isEmpty(value)) {
							if(used === true) {
					        	meta.style = "font-weight:bold !important";
						    } 
							return Ext.util.Format.number(value,Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_crncy));
						}				
	},
	
	_afterInitComponent_: function() {
		
		
						var dc = this._controller_;
						var headerCt = this.getView().getHeaderCt();
						var convertedPriceColumn = headerCt.down("[dataIndex=convertedPrice]");
						var contractAmountColumn = headerCt.down("[dataIndex=contractAmount]");
		
						dc.on("afterDoQuerySuccess", function() {
							var params = dc.getParams();
							var settCrncy = params.get("settlementCurrencyCode");
							if (!Ext.isEmpty(settCrncy)) {
								convertedPriceColumn.setText(convertedPriceColumn.initialConfig.header+" ("+settCrncy+")");
								contractAmountColumn.setText(contractAmountColumn.initialConfig.header+" ("+settCrncy+")");
							}
							else {
								convertedPriceColumn.setText(convertedPriceColumn.initialConfig.header);
								contractAmountColumn.setText(contractAmountColumn.initialConfig.header);
							}
		
						}, this);
	}
});
