/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.BidValidationsLabels_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.cmm.ui.extjs.ds.ValidationMessageBid_Ds
});

/* ================= EDIT FORM: BidValidationsLabel ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.BidValidationsLabels_Dc$BidValidationsLabel", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_BidValidationsLabels_Dc$BidValidationsLabel",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"succesfullySubmitedBidsLabel", bind:"{d.succesfullySubmitedBidsLabel}", dataIndex:"succesfullySubmitedBidsLabel", maxLength:255, cls:"sone-field-label-left"})
		.addDisplayFieldText({ name:"concurencyCheckFailedBidsLabel", bind:"{d.concurencyCheckFailedBidsLabel}", dataIndex:"concurencyCheckFailedBidsLabel", maxLength:255, cls:"sone-field-label-left"})
		.addDisplayFieldText({ name:"validationErrorsBidsLabel", bind:"{d.validationErrorsBidsLabel}", dataIndex:"validationErrorsBidsLabel", maxLength:255, cls:"sone-field-label-left"})
		/* =========== containers =========== */
		.addPanel({ name:"col1", layout: {type:"table"}})
		.addPanel({ name:"col2", layout: {type:"table"}})
		.addPanel({ name:"col3", layout: {type:"table"}})
		.addPanel({ name:"main", autoScroll:true});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("col1", ["succesfullySubmitedBidsLabel"])
		.addChildrenTo("col2", ["concurencyCheckFailedBidsLabel"])
		.addChildrenTo("col3", ["validationErrorsBidsLabel"])
		.addChildrenTo("main", ["col1", "col2", "col3"]);
	}
});
