/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.OpenTenderLocation_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_OpenTenderLocation_Ds"
	},
	
	
	fields: [
		{name:"tenderId", type:"int", allowNull:true},
		{name:"tenderCode", type:"string"},
		{name:"tenderVersion", type:"int", allowNull:true},
		{name:"tenderStatus", type:"string"},
		{name:"locationId", type:"int", allowNull:true},
		{name:"locationCode", type:"string"},
		{name:"locationName", type:"string"},
		{name:"tenderHolderId", type:"int", allowNull:true},
		{name:"tenderHolderCode", type:"string"},
		{name:"flightServiceType", type:"string"},
		{name:"taxType", type:"string"},
		{name:"fuelProduct", type:"string"},
		{name:"deliveryPoint", type:"string"},
		{name:"locationBiddingStatus", type:"string"},
		{name:"round", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.OpenTenderLocation_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"tenderId", type:"int", allowNull:true},
		{name:"tenderCode", type:"string"},
		{name:"tenderVersion", type:"int", allowNull:true},
		{name:"tenderStatus", type:"string"},
		{name:"locationId", type:"int", allowNull:true},
		{name:"locationCode", type:"string"},
		{name:"locationName", type:"string"},
		{name:"tenderHolderId", type:"int", allowNull:true},
		{name:"tenderHolderCode", type:"string"},
		{name:"flightServiceType", type:"string"},
		{name:"taxType", type:"string"},
		{name:"fuelProduct", type:"string"},
		{name:"deliveryPoint", type:"string"},
		{name:"locationBiddingStatus", type:"string"},
		{name:"round", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.cmm.ui.extjs.ds.OpenTenderLocation_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"selectedLocation", type:"int", allowNull:true}
	]
});
