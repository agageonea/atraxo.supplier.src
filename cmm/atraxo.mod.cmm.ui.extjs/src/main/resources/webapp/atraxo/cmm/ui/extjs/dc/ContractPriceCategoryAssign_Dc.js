/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceCategoryAssign_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.ContractPriceCategoryAssign_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.ContractPriceCategoryAssign_Ds
});

/* ================= GRID: AssignList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceCategoryAssign_Dc$AssignList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_ContractPriceCategoryAssign_Dc$AssignList",
	_noPaginator_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:120,  flex:1})
		.addTextColumn({ name:"priceCategory", dataIndex:"priceCtgryName", width:50,  flex:1})
		.addTextColumn({ name:"mainCategory", dataIndex:"mainCategoryCode", width:50,  flex:1})
		.addTextColumn({ name:"currency", dataIndex:"currencyCode", width:50,  flex:1})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:50,  flex:1})
		.addDateColumn({ name:"validFrom", dataIndex:"validFrom", _mask_: Masks.DATE,  flex:1})
		.addDateColumn({ name:"validTo", dataIndex:"validTo", _mask_: Masks.DATE,  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
