/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceComponentConv_Dc", {
	extend: "e4e.dc.AbstractDc",
	_infiniteScroll_: true,
	recordModel: atraxo.cmm.ui.extjs.ds.ContractPriceComponentConv_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceComponentConv_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.cmm_ContractPriceComponentConv_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateField({name:"validFrom", dataIndex:"validFrom"})
			.addDateField({name:"validTo", dataIndex:"validTo"})
			.addDateField({name:"ofDate", dataIndex:"ofDate"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceComponentConv_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_ContractPriceComponentConv_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"validFrom", dataIndex:"validFrom", width:100, _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"validTo", width:100, _mask_: Masks.DATE})
		.addNumberColumn({ name:"value", dataIndex:"price", width:100,  decimals:_SYSTEMPARAMETERS_.dec_crncy})
		.addTextColumn({ name:"currencyunitCd", dataIndex:"currencyUnitCd", width:100})
		.addNumberColumn({ name:"equivalent", dataIndex:"equivalent", width:100,  decimals:_SYSTEMPARAMETERS_.dec_crncy})
		.addTextColumn({ name:"equiv_crncy_unit_cd", dataIndex:"equivalentCurrencyUnitCode", width:150})
		.addNumberColumn({ name:"exchangeRate", dataIndex:"exchangeRate", width:100,  decimals:_SYSTEMPARAMETERS_.dec_xr})
		.addTextColumn({ name:"exchangeRateCurrnecyCDS", dataIndex:"exchangeRateCurrnecyCDS", width:150})
		.addDateColumn({ name:"ofDate", dataIndex:"ofDate", width:150, _mask_: Masks.DATE})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
