/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdateCategoriesAdd_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.PriceUpdateCategoriesAdd_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.PriceUpdateCategoriesAdd_Ds
});

/* ================= EDIT-GRID: AddList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdateCategoriesAdd_Dc$AddList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.cmm_PriceUpdateCategoriesAdd_Dc$AddList",
	_noExport_: true,
	_noPaginator_: true,
	selType: null,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addBooleanColumn({name:"selected", dataIndex:"selected", width:40})
		.addTextColumn({name:"location", dataIndex:"locationCode", width:50, noEdit: true, maxLength:25,  flex:1})
		.addTextColumn({name:"contract", dataIndex:"contract", width:50, noEdit: true, maxLength:32,  flex:1})
		.addTextColumn({name:"holder", dataIndex:"contractHolderCode", width:50, noEdit: true, maxLength:32,  flex:1})
		.addTextColumn({name:"counterparty", dataIndex:"counterpartyCode", width:50, noEdit: true, maxLength:32,  flex:1})
		.addNumberColumn({name:"price", dataIndex:"price", noEdit: true, sysDec:"dec_crncy", maxLength:19, align:"right",  flex:1 })
		.addTextColumn({name:"currency", dataIndex:"currencyCode", width:50, noEdit: true, maxLength:3,  flex:1})
		.addTextColumn({name:"unit", dataIndex:"unitCode", width:50, noEdit: true, maxLength:2,  flex:1})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
