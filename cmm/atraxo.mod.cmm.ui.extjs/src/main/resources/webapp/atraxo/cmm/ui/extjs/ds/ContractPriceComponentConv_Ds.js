/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.ContractPriceComponentConv_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_ContractPriceComponentConv_Ds"
	},
	
	
	fields: [
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"price", type:"float", allowNull:true},
		{name:"currencyUnitCd", type:"string"},
		{name:"equivalent", type:"float", allowNull:true},
		{name:"exchangeRate", type:"float", allowNull:true},
		{name:"exchangeRateCurrnecyCDS", type:"string"},
		{name:"ofDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"provisional", type:"boolean"},
		{name:"whldPay", type:"boolean"},
		{name:"contractPriceComponent", type:"int", allowNull:true},
		{name:"contractPriceCtgry", type:"int", allowNull:true},
		{name:"equivalentUnitId", type:"int", allowNull:true},
		{name:"equivalentUnitCode", type:"string"},
		{name:"equivalentCurrencyId", type:"int", allowNull:true},
		{name:"equivalentCurrencyCode", type:"string"},
		{name:"equivalentCurrencyUnitCode", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.ContractPriceComponentConv_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"price", type:"float", allowNull:true},
		{name:"currencyUnitCd", type:"string"},
		{name:"equivalent", type:"float", allowNull:true},
		{name:"exchangeRate", type:"float", allowNull:true},
		{name:"exchangeRateCurrnecyCDS", type:"string"},
		{name:"ofDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"provisional", type:"boolean", allowNull:true},
		{name:"whldPay", type:"boolean", allowNull:true},
		{name:"contractPriceComponent", type:"int", allowNull:true},
		{name:"contractPriceCtgry", type:"int", allowNull:true},
		{name:"equivalentUnitId", type:"int", allowNull:true},
		{name:"equivalentUnitCode", type:"string"},
		{name:"equivalentCurrencyId", type:"int", allowNull:true},
		{name:"equivalentCurrencyCode", type:"string"},
		{name:"equivalentCurrencyUnitCode", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
