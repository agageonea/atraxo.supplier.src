/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.frame.TenderBid_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.TenderBid_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("tenderBid", Ext.create(atraxo.cmm.ui.extjs.dc.TenderBid_Dc,{}))
		.addDc("pricingBase", Ext.create(atraxo.cmm.ui.extjs.dc.PricingBase_Dc,{}))
		.addDc("contractPriceCtgry", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceCategory_Dc,{}))
		.addDc("contractPriceCtgryAssign", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceCategoryAssign_Dc,{}))
		.addDc("contractPriceCmpn", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceComponentClassic_Dc,{}))
		.addDc("contractPriceRestr", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceRestriction_Dc,{multiEdit: true}))
		.addDc("contractPriceCmpnConv", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceComponentConv_Dc,{}))
		.addDc("bidAirlineVols", Ext.create(atraxo.cmm.ui.extjs.dc.BidAirlineVols_Dc,{multiEdit: true}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("attachment", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("attachmentPop", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("importBid", Ext.create(atraxo.cmm.ui.extjs.dc.ImportBid_Dc,{}))
		.addDc("bidValidations", Ext.create(atraxo.cmm.ui.extjs.dc.BidValidations_Dc,{}))
		.addDc("bidValidationsLabel", Ext.create(atraxo.cmm.ui.extjs.dc.BidValidationsLabels_Dc,{}))
		.addDc("tenderInviation", Ext.create(atraxo.cmm.ui.extjs.dc.OpenTender_Dc,{}))
		.addDc("tenderLocation", Ext.create(atraxo.cmm.ui.extjs.dc.OpenTenderLocation_Dc,{}))
		.linkDc("pricingBase", "tenderBid",{fetchMode:"auto",fields:[
					{childField:"contractId", parentField:"id"}, {childParam:"settlementCurrencyId", parentField:"settCurrId"}]})
				.linkDc("contractPriceCtgry", "tenderBid",{fetchMode:"auto",reloadChildrenOnParentUpdate:true,fields:[
					{childField:"contract", parentField:"id"}, {childParam:"settlementCurrencyId", parentField:"settCurrId"}, {childParam:"settlementCurrencyCode", parentField:"settCurrCode"}, {childParam:"settlementUnitCode", parentField:"settUnitCode"}]})
				.linkDc("contractPriceCtgryAssign", "tenderBid",{fetchMode:"auto",fields:[
					{childField:"contract", parentField:"id"}, {childParam:"settlementCurrencyId", parentField:"settCurrId"}, {childParam:"settlementCurrencyCode", parentField:"settCurrCode"}, {childParam:"settlementUnitCode", parentField:"settUnitCode"}, {childField:"priceCtgryPricePer", value:"Volume"}]})
				.linkDc("contractPriceCmpn", "contractPriceCtgry",{fetchMode:"auto",reloadChildrenOnParentUpdate:true,fields:[
					{childField:"contractPriceCat", parentField:"id"}, {childField:"priceCtgryName", parentField:"priceCtgryName"}, {childField:"priceCtgryPricePer", parentField:"priceCtgryPricePer", noFilter:true}]})
				.linkDc("contractPriceRestr", "contractPriceCtgry",{fetchMode:"auto",fields:[
					{childField:"contractPriceCtgry", parentField:"id"}, {childParam:"isEnabled", parentField:"restriction"}]})
				.linkDc("contractPriceCmpnConv", "contractPriceCtgry",{fetchMode:"auto",fields:[
					{childField:"contractPriceCtgry", parentField:"id"}]})
				.linkDc("bidAirlineVols", "tenderBid",{fetchMode:"auto",fields:[
					{childField:"contractId", parentField:"id"}, {childParam:"contractStatus", parentField:"bidStatus"}, {childParam:"approvalStatus", parentField:"bidApprovalStatus"}, {childParam:"tenderSource", parentField:"tenderSource"}]})
				.linkDc("note", "tenderBid",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("history", "tenderBid",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("attachment", "tenderBid",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}]})
				.linkDc("attachmentPop", "tenderBid",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}, {childField:"categType", value:"upload"}]})
				.linkDc("tenderLocation", "tenderInviation",{fetchMode:"auto",fields:[
					{childField:"tenderId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnEditBid",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnEditBid,stateManager:[{ name:"selected_one_clean", dc:"tenderBid"}], scope:this})
		.addButton({name:"btnDeleteBid",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteBid,stateManager:[{ name:"selected_one_clean", dc:"tenderBid", and: function() {return (this.canDeleteBid());} }], scope:this})
		.addButton({name:"btnAcceptAward",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnAcceptAward,stateManager:[{ name:"selected_one_clean", dc:"tenderBid", and: function(dc) {return (this.canAcceptAward(dc));} }], scope:this})
		.addButton({name:"btnAcceptAwardEdit",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnAcceptAwardEdit,stateManager:[{ name:"selected_one_clean", dc:"tenderBid", and: function(dc) {return (this.canAcceptAward(dc));} }], scope:this})
		.addButton({name:"btnDeclineAward",iconCls: fp_asc.close_icon.css, disabled:true, handler: this.onBtnDeclineAward,stateManager:[{ name:"selected_one_clean", dc:"tenderBid", and: function(dc) {return (this.canDeclineAward(dc));} }], scope:this})
		.addButton({name:"btnDeclineAwardEdit",iconCls: fp_asc.close_icon.css, disabled:true, handler: this.onBtnDeclineAwardEdit,stateManager:[{ name:"selected_one_clean", dc:"tenderBid", and: function(dc) {return (this.canDeclineAward(dc));} }], scope:this})
		.addButton({name:"btnSaveDeclineAwardWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveDeclineAwardWdw, scope:this})
		.addButton({name:"btnCancelDeclineAwardWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelDeclineAwardWdw, scope:this})
		.addButton({name:"btnSubmitForApproval",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitForApproval,stateManager:[{ name:"selected_not_zero", dc:"tenderBid", and: function(dc) {return (this.canSubmitForApproval(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysbidapproval === 'true' })
		.addButton({name:"btnSubmitForApprovalEdit",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitForApprovalEdit,stateManager:[{ name:"selected_one_clean", dc:"tenderBid", and: function(dc) {return (this.canSubmitForApproval(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysbidapproval === 'true' })
		.addButton({name:"btnApprove",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApprove,stateManager:[{ name:"selected_one_clean", dc:"tenderBid", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysbidapproval === 'true' })
		.addButton({name:"btnApproveList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApproveList,stateManager:[{ name:"selected_one_clean", dc:"tenderBid", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysbidapproval === 'true' })
		.addButton({name:"btnReject",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnReject,stateManager:[{ name:"selected_one_clean", dc:"tenderBid", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysbidapproval === 'true' })
		.addButton({name:"btnRejectList",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnRejectList,stateManager:[{ name:"selected_one_clean", dc:"tenderBid", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysbidapproval === 'true' })
		.addButton({name:"btnSaveCloseApprovalWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApprovalWdw, scope:this})
		.addButton({name:"btnCancelApprovalWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApprovalWdw, scope:this})
		.addButton({name:"btnSaveCloseApproveWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApproveWdw, scope:this})
		.addButton({name:"btnSaveCloseRejectWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRejectWdw, scope:this})
		.addButton({name:"btnCancelApproveWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApproveWdw, scope:this})
		.addButton({name:"btnCancelRejectWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRejectWdw, scope:this})
		.addButton({name:"btnSubmitWithMenu",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"tenderBid"}], scope:this})
		.addButton({name:"btnSubmitSelected",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true,  _btnContainerName_:"btnSubmitWithMenu", _isDefaultHandler_:true, handler: this.onBtnSubmitSelected,stateManager:[{ name:"selected_not_zero", dc:"tenderBid", and: function(dc) {return (this.canSubmitSelectedToIssuer(dc));} }], scope:this})
		.addButton({name:"btnSubmitFailed",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false,  _btnContainerName_:"btnSubmitWithMenu", handler: this.onBtnSubmitFailed, scope:this})
		.addButton({name:"btnSubmitAll",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false,  _btnContainerName_:"btnSubmitWithMenu", handler: this.onBtnSubmitAll, scope:this})
		.addButton({name:"btnExportList",glyph:fp_asc.export_glyph.glyph, disabled:false, handler: this.onBtnExportList, scope:this})
		.addButton({name:"savePriceCtgry",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onSavePriceCtgry,stateManager:[{ name:"record_is_dirty", dc:"contractPriceCtgry"}], scope:this})
		.addButton({name:"CancelPb",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onCancelPb, scope:this})
		.addButton({name:"CancelPbEditFormula",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onCancelPbEditFormula, scope:this})
		.addButton({name:"btnSaveClosePriceWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveClosePriceWdw, scope:this})
		.addButton({name:"btnSaveClosePriceEditFormulaWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveClosePriceEditFormulaWdw, scope:this})
		.addButton({name:"btnSaveClosePriceCategoryWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveClosePriceCategoryWdw, scope:this})
		.addButton({name:"btnSaveNewPriceCategoryWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewPriceCategoryWdw, scope:this})
		.addButton({name:"btnSaveNewCompositePrice",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewCompositePrice, scope:this})
		.addButton({name:"btnSaveCloseCompositePrice",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseCompositePrice, scope:this})
		.addButton({name:"btnSaveEditCompositePrice",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveEditCompositePrice, scope:this})
		.addButton({name:"btnCancelCompositePrice",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelCompositePrice, scope:this})
		.addButton({name:"btnSaveEditPriceCategoryWdw",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnSaveEditPriceCategoryWdw, scope:this})
		.addButton({name:"btnClosePriceFixWdw",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onBtnClosePriceFixWdw, scope:this})
		.addButton({name:"btnHelpEdit",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onBtnHelpEdit, scope:this})
		.addButton({name:"btnHelpList",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onBtnHelpList, scope:this})
		.addButton({name:"btnDeleteContractPriceCtgry",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteContractPriceCtgry,stateManager:[{ name:"selected_one", dc:"contractPriceCtgry", and: function() {return (this._checkPricingBaseId_() && this.canEditBid());} }], scope:this})
		.addButton({name:"btnEditPrice",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnEditPrice,stateManager:[{ name:"selected_one", dc:"contractPriceCtgry", and: function() {return (this._checkPricingBaseId_());} }], scope:this})
		.addButton({name:"btnDeleteContractPriceComponent",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteContractPriceComponent,stateManager:[{ name:"selected_one", dc:"contractPriceCmpn", and: function() {return (this._checkPriceCtgryName_() && this.canEditBid());} }], scope:this})
		.addButton({name:"btnNewContractPriceComponent",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:true, handler: this.onBtnNewContractPriceComponent,stateManager:[{ name:"selected_one", dc:"contractPriceCmpn", and: function() {return (this._checkPriceCtgryPricePer_() && this.canEditBid());} }], scope:this})
		.addButton({name:"btnDeleteContractPricingBase",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteContractPricingBase,stateManager:[{ name:"selected_one", dc:"pricingBase", and: function(dc) {return (dc.store.totalCount!==1 );} }], scope:this})
		.addButton({name:"btnUpdatePrice",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnUpdatePrice,stateManager:[{ name:"selected_one", dc:"contractPriceCtgry"}], scope:this})
		.addButton({name:"btnEditFormulaPrice",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnEditFormulaPrice,stateManager:[{ name:"selected_one", dc:"contractPriceCtgry", and: function() {return (this._checkIfFormulaPriceAndNoDraft_());} }], scope:this})
		.addButton({name:"btnPriceCompSave",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnPriceCompSave, scope:this})
		.addButton({name:"btnPriceCompCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnPriceCompCancel, scope:this})
		.addButton({name:"btnExportEdit",glyph:fp_asc.export_glyph.glyph, disabled:false, handler: this.onBtnExportEdit, scope:this})
		.addButton({name:"btnAwardBidList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnAwardBidList,stateManager:[{ name:"selected_not_zero", dc:"tenderBid", and: function(dc) {return (this.canAwardDecline(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnAwardBidEditForm",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnAwardBidEditForm,stateManager:[{ name:"selected_not_zero", dc:"tenderBid", and: function(dc) {return (this.canAwardDecline(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnDeclineBidList",iconCls: fp_asc.close_icon.css, disabled:true, handler: this.onBtnDeclineBidList,stateManager:[{ name:"selected_not_zero", dc:"tenderBid", and: function(dc) {return (this.canAwardDecline(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnDeclineBidEditForm",iconCls: fp_asc.close_icon.css, disabled:true, handler: this.onBtnDeclineBidEditForm,stateManager:[{ name:"selected_not_zero", dc:"tenderBid", and: function(dc) {return (this.canAwardDecline(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.etenderclient !== 'true' })
		.addButton({name:"btnCancelBidList",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnCancelBidList,stateManager:[{ name:"selected_not_zero", dc:"tenderBid", and: function(dc) {return (this.canCancelBid(dc));} }], scope:this})
		.addButton({name:"btnCancelBidEditForm",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnCancelBidEditForm,stateManager:[{ name:"selected_not_zero", dc:"tenderBid", and: function(dc) {return (this.canCancelBid(dc));} }], scope:this})
		.addButton({name:"btnNewWithMenu",iconCls: fp_asc.new_icon.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_one_clean", dc:"tenderBid", and: function() {return (this.canEditBid());} }], scope:this})
		.addButton({name:"btnNewFormulaPrice",iconCls: fp_asc.new_icon.css, disabled:false,  _btnContainerName_:"btnNewWithMenu", _isDefaultHandler_:true, handler: this.onBtnNewFormulaPrice, scope:this})
		.addButton({name:"btnNewFixPrice",iconCls: fp_asc.new_icon.css, disabled:false,  _btnContainerName_:"btnNewWithMenu", handler: this.onBtnNewFixPrice, scope:this})
		.addButton({name:"btnNewDutyFeeTax",iconCls: fp_asc.new_icon.css, disabled:false,  _btnContainerName_:"btnNewWithMenu", handler: this.onBtnNewDutyFeeTax, scope:this})
		.addButton({name:"btnNewCompositePrice",iconCls: fp_asc.new_icon.css, disabled:false,  _btnContainerName_:"btnNewWithMenu", handler: this.onBtnNewCompositePrice, scope:this})
		.addButton({name:"btnNewNote",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewNote,stateManager:[{ name:"no_record_or_record_is_clean", dc:"note", and: function() {return (this.canEditBid());} }], scope:this})
		.addButton({name:"btnEditNote",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnEditNote,stateManager:[{ name:"selected_one_clean", dc:"note", and: function() {return (this.canEditBid());} }], scope:this})
		.addButton({name:"btnDeleteNote",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteNote,stateManager:[{ name:"selected_one_clean", dc:"note", and: function() {return (this.canEditBid());} }], scope:this})
		.addButton({name:"btnNewAttachment",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewAttachment,stateManager:[{ name:"no_record_or_record_is_clean", dc:"attachment", and: function() {return (this.canEditBid());} }], scope:this})
		.addButton({name:"btnDeleteAttachment",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteAttachment,stateManager:[{ name:"selected_one_clean", dc:"attachment", and: function() {return (this.canEditBid());} }], scope:this})
		.addButton({name:"btnCancelWithMenu",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"bidAirlineVols", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", handler: this.onBtnCancelSelected, scope:this})
		.addButton({name:"btnCancelAll",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", _isDefaultHandler_:true, handler: this.onBtnCancelAll, scope:this})
		.addButton({name:"btnCloseNoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseNoteWdw, scope:this})
		.addButton({name:"btnSaveCloseNoteWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseNoteWdw, scope:this})
		.addButton({name:"btnSaveCloseRemarkWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRemarkWdw, scope:this})
		.addButton({name:"btnCancelRemarkWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRemarkWdw, scope:this})
		.addButton({name:"btnAssignPriceComponents",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnAssignPriceComponents, scope:this})
		.addButton({name:"btnCancelAssignPriceComponents",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelAssignPriceComponents, scope:this})
		.addButton({name:"btnNewRestriction",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewRestriction,stateManager:[{ name:"no_record_or_record_is_clean", dc:"contractPriceRestr", and: function() {return (this._applyRestrictionState_() && this.canEditBid());} }], scope:this})
		.addButton({name:"btnSaveRestriction",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSaveRestriction,stateManager:[{ name:"selected_one_dirty", dc:"contractPriceRestr", and: function() {return (this._applyRestrictionState_() && this.canEditBid());} }], scope:this})
		.addButton({name:"btnCancelRestriction",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnCancelRestriction,stateManager:[{ name:"selected_one_dirty", dc:"contractPriceRestr", and: function() {return (this._applyRestrictionState_() && this.canEditBid());} }], scope:this})
		.addButton({name:"btnDeleteRestriction",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteRestriction,stateManager:[{ name:"selected_one_clean", dc:"contractPriceRestr", and: function() {return (this._applyRestrictionState_() && this.canEditBid());} }], scope:this})
		.addButton({name:"btnGenerateSaleContract",glyph:fp_asc.copy_glyph.glyph,iconCls: fp_asc.copy_glyph.css, disabled:true, handler: this.onBtnGenerateSaleContract,stateManager:[{ name:"selected_one_clean", dc:"tenderBid", and: function(dc) {return (dc.record.data.bidStatus === __CMM_TYPE__.BidStatus._AWARD_ACCEPTED_);} }], scope:this})
		.addButton({name:"btnGenerateSaleContractEdit",glyph:fp_asc.copy_glyph.glyph,iconCls: fp_asc.copy_glyph.css, disabled:true, handler: this.onBtnGenerateSaleContractEdit,stateManager:[{ name:"selected_one_clean", dc:"tenderBid", and: function(dc) {return (dc.record.data.bidStatus === __CMM_TYPE__.BidStatus._AWARD_ACCEPTED_);} }], scope:this})
		.addButton({name:"btnCopyBidList",glyph:fp_asc.copy_glyph.glyph,iconCls: fp_asc.copy_glyph.css, disabled:true, handler: this.onBtnCopyBidList,stateManager:[{ name:"selected_one_clean", dc:"tenderBid"}], scope:this})
		.addButton({name:"btnNewBidRevision",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnNewBidRevision,stateManager:[{ name:"selected_one_clean", dc:"tenderBid", and: function() {return (this.canCreateNewRevisionBid());} }], scope:this})
		.addButton({name:"btnCopyBidEdit",glyph:fp_asc.copy_glyph.glyph,iconCls: fp_asc.copy_glyph.css, disabled:true, handler: this.onBtnCopyBidEdit,stateManager:[{ name:"selected_one_clean", dc:"tenderBid"}], scope:this})
		.addButton({name:"btnCopy",glyph:fp_asc.copy_glyph.glyph,iconCls: fp_asc.copy_glyph.css, disabled:false, handler: this.onBtnCopy, scope:this})
		.addButton({name:"btnSaveBidRevision",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveBidRevision, scope:this})
		.addButton({name:"btnSaveBidCancelWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveBidCancelWdw, scope:this})
		.addButton({name:"btnCancelBidCancelWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelBidCancelWdw, scope:this})
		.addButton({name:"btnCancelCopy",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelCopy, scope:this})
		.addButton({name:"btnCancelBidRevision",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelBidRevision, scope:this})
		.addButton({name:"btnImportWithMenu",glyph:fp_asc.xml_glyph.glyph,iconCls: fp_asc.xml_glyph.css, disabled:false, handler: this.onBtnImportWithMenu, scope:this})
		.addButton({name:"btnExportBidList",glyph:fp_asc.export_glyph.glyph, disabled:true, handler: this.onBtnExportBidList,stateManager:[{ name:"selected_one", dc:"tenderBid", and: function(dc) {return (this.canExportSingle(dc));} }], scope:this})
		.addButton({name:"btnExportBidEdit",glyph:fp_asc.export_glyph.glyph, disabled:true, handler: this.onBtnExportBidEdit,stateManager:[{ name:"selected_one", dc:"tenderBid", and: function(dc) {return (this.canExportSingle(dc));} }], scope:this})
		.addButton({name:"btnOkExport",glyph:fp_asc.send_glyph.glyph,iconCls: fp_asc.send_glyph.css,  disabled:true, handler: this.onBtnOkExport, scope:this})
		.addButton({name:"btnCancelExport",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelExport, scope:this})
		.addButton({name:"btnDeleteShipTo",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteShipTo,stateManager:[{ name:"selected_one", dc:"bidAirlineVols", and: function() {return (this.canEditBid());} }], scope:this})
		.addButton({name:"btnSaveVols",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSaveVols,stateManager:[{ name:"record_is_dirty", dc:"bidAirlineVols", and: function() {return (this.canEditBid());} }], scope:this})
		.addButton({name:"btnCloseBidValidationsWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false,  style:"margin-left: 9px", handler: this.onBtnCloseBidValidationsWdw, scope:this})
		.addButton({name:"btnCloseAndFilterBidValidationsWdw",glyph:fp_asc.filter_glyph.glyph, disabled:false, handler: this.onBtnCloseAndFilterBidValidationsWdw, scope:this})
		.addDcGridView("tenderBid", {name:"BidsList", xtype:"cmm_TenderBid_Dc$TabList",  listeners:{itemdblclick: {scope: this, fn:function(el, record, item) {this._showEdit_()}}}})
		.addDcFilterFormView("tenderBid", {name:"SalesContractFilter", xtype:"cmm_TenderBid_Dc$Filter"})
		.addDcFormView("tenderBid", {name:"Header", xtype:"cmm_TenderBid_Dc$Header",  split:false, _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcFilterFormView("pricingBase", {name:"PbFilter", xtype:"cmm_PricingBase_Dc$Filter"})
		.addDcFormView("pricingBase", {name:"PbEdit", xtype:"cmm_PricingBase_Dc$NewFormulaPrice"})
		.addDcFormView("pricingBase", {name:"PbEditFormula", xtype:"cmm_PricingBase_Dc$EditFormulaPrice"})
		.addDcFormView("pricingBase", {name:"priceFixNew", xtype:"cmm_PricingBase_Dc$NewPCSellContract"})
		.addDcFormView("pricingBase", {name:"compositePriceNew", xtype:"cmm_PricingBase_Dc$NewCompositePrice"})
		.addDcFilterFormView("contractPriceCtgry", {name:"priceCtgryFilter", xtype:"cmm_ContractPriceCategory_Dc$Filter"})
		.addDcGridView("contractPriceCtgry", {name:"priceCtgryList", xtype:"cmm_ContractPriceCategory_Dc$BidList",  listeners:{itemdblclick: {scope: this, fn:function(el, record, item) {this._showPriceEdit_(el, record, item)}}}})
		.addDcGridView("contractPriceCtgryAssign", {name:"priceCtgryAssignList", xtype:"cmm_ContractPriceCategoryAssign_Dc$AssignList",  listeners:{ afterrender: { scope: this, fn: function(list) { this._excludeComposite_(list)}}}})
		.addDcFormView("contractPriceCtgry", {name:"priceCtgryEditPopUp", xtype:"cmm_ContractPriceCategory_Dc$NewGrid"})
		.addDcFormView("contractPriceCtgry", {name:"priceCtgryEdit", xtype:"cmm_ContractPriceCategory_Dc$EditGrid"})
		.addDcGridView("contractPriceCmpn", {name:"priceCompList", xtype:"cmm_ContractPriceComponentClassic_Dc$SimpleList"})
		.addDcFormView("contractPriceCmpn", {name:"priceCompEdit", xtype:"cmm_ContractPriceComponentClassic_Dc$Edit"})
		.addDcEditGridView("contractPriceRestr", {name:"priceRestr", xtype:"cmm_ContractPriceRestriction_Dc$List", frame:true,  listeners:{ beforeedit: { scope: this, fn: function(editor, ctx) { this._setPriceRestrictionEditorType_(editor, ctx, this._get_('priceRestr'))}}}})
		.addPanel({name:"priceRestrWrapper", layout:"fit"})
		.addDcFormView("contractPriceCtgry", {name:"UpdatePriceHeader", xtype:"cmm_ContractPriceCategory_Dc$FormUpdatePrice",  cls:"form-large-space"})
		.addDcFormView("tenderBid", {name:"paymentTerms", _hasTitle_:true, xtype:"cmm_TenderBid_Dc$PaymentTerms", _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcFormView("tenderBid", {name:"creditTerms", _hasTitle_:true, xtype:"cmm_TenderBid_Dc$CreditTerms", _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcFormView("tenderBid", {name:"volumes", xtype:"cmm_TenderBid_Dc$Volumes",  split:false})
		.addDcFilterFormView("contractPriceCmpnConv", {name:"priceComponentConvFilter", xtype:"cmm_ContractPriceComponentConv_Dc$Filter"})
		.addDcGridView("contractPriceCmpnConv", {name:"priceComponentConv", _hasTitle_:true, height:300, xtype:"cmm_ContractPriceComponentConv_Dc$List"})
		.addDcFormView("contractPriceCtgry", {name:"totals", xtype:"cmm_ContractPriceCategory_Dc$Totals",  split:false, cls:"sone-total-panel", listeners:{afterrender: {scope: this, fn: function(view) {var grid = this._get_('priceCtgryList'); grid._totalPanelId_ = view.getId(); }}}})
		.addDcEditGridView("bidAirlineVols", {name:"ShipToEditList", xtype:"cmm_BidAirlineVols_Dc$EditList", frame:true, _summaryDefined_:true})
		.addDcGridView("note", {name:"NotesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcFormView("note", {name:"NotesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addDcGridView("attachment", {name:"attachmentList", _hasTitle_:true, xtype:"ad_Attachment_Dc$List"})
		.addDcFormView("attachment", {name:"attachmentEdit", xtype:"ad_Attachment_Dc$New"})
		.addDcGridView("history", {name:"History", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("bidAirlineVols", {name:"remark", xtype:"cmm_BidAirlineVols_Dc$EditRemark"})
		.addDcFormView("tenderBid", {name:"BidExportForm", xtype:"cmm_TenderBid_Dc$Export"})
		.addDcFormView("tenderBid", {name:"approvalNote", xtype:"cmm_TenderBid_Dc$ApprovalNote",  split:false})
		.addDcFormView("importBid", {name:"importBid", xtype:"cmm_ImportBid_Dc$NewWithName"})
		.addDcGridView("attachmentPop", {name:"documentsAssignList", _hasTitle_:true, height:300, xtype:"ad_Attachment_Dc$attachmentAssignList",  cls:"sone-custom-panel-title"})
		.addDcFormView("tenderBid", {name:"approveNote", xtype:"cmm_TenderBid_Dc$ApprovalNote"})
		.addDcFormView("tenderBid", {name:"rejectNote", xtype:"cmm_TenderBid_Dc$ApprovalNote"})
		.addDcGridView("tenderInviation", {name:"tenderSelectList", _hasTitle_:true, width:160, xtype:"cmm_OpenTender_Dc$SelectList",  split:false})
		.addDcGridView("tenderLocation", {name:"tenderLocationList", _hasTitle_:true, width:590, xtype:"cmm_OpenTenderLocation_Dc$SelectList",  split:false})
		.addDcFormView("tenderBid", {name:"bidRevisionForm", xtype:"cmm_TenderBid_Dc$BidRevisionForm",  split:false})
		.addDcFormView("tenderBid", {name:"bidCancelForm", xtype:"cmm_TenderBid_Dc$BidCancelForm",  split:false})
		.addDcFormView("tenderBid", {name:"bidDeclineAwardForm", xtype:"cmm_TenderBid_Dc$BidDeclineAwardForm",  split:false})
		.addDcFormView("tenderBid", {name:"bidCopyTypeSelection", xtype:"cmm_TenderBid_Dc$CopyBidForm",  split:false})
		.addDcFormView("bidValidationsLabel", {name:"bidValidationsLabel", xtype:"cmm_BidValidationsLabels_Dc$BidValidationsLabel",  split:false})
		.addDcGridView("bidValidations", {name:"bidValidationsList", width:160, xtype:"cmm_BidValidations_Dc$BidValidations"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas4", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"panelAssignAttachments", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"volPanel", _hasTitle_:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"prices", _hasTitle_:true, layout:{ type: "border" }, defaults:{split:true}})
		.addWindow({name:"wdwRemarks", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("remark")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRemarkWdw"), this._elems_.get("btnCancelRemarkWdw")]}]})
		.addWindow({name:"wdwApprovalNote", _hasTitle_:true, width:600, height:530, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("panelAssignAttachments")],  closable:true, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCancelApprovalWdw"), this._elems_.get("btnSaveCloseApprovalWdw")]}]})
		.addWindow({name:"wdwImportBid", _hasTitle_:true, width:260, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("importBid")],  closable:true, listeners:{ show:{fn:function() { var ctx = getActiveDialog(), 
						form = ctx._get_('importBid'), 
						dc = ctx._getDc_('importBid'),
						w = ctx._getWindow_('wdwImportBid'); 
						form._get_('importBidFileType').setValue(null); 
						dc.doNew();
						dc._doNewWdw_ = w} } }})
		.addWindow({name:"wdwPricing", _hasTitle_:true, width:600, height:340, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("PbEdit")],  listeners:{ show:{fn:function(win) { win.down('form')._isVisible_ = true;}, scope:this}, close:{fn:function(win) {this._onWdwwClose_(win);}, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveClosePriceWdw"), this._elems_.get("CancelPb")]}]})
		.addWindow({name:"wdwPricingEditFormula", _hasTitle_:true, width:600, height:340, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("PbEditFormula")],  listeners:{ show:{fn:function(win) {win.down('form')._isVisible_ = true;}, scope:this}, close:{fn:function(win) {this._onWdwwClose_(win);}, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveClosePriceEditFormulaWdw"), this._elems_.get("CancelPbEditFormula")]}]})
		.addWindow({name:"priceFixWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("priceFixNew")],  listeners:{ show:{fn:function(win) {win.down('form')._isVisible_ = true;}, scope:this}, close:{fn:function(win) {this._onWdwwClose_(win);}, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewPriceCategoryWdw"), this._elems_.get("btnSaveClosePriceCategoryWdw"), this._elems_.get("btnSaveEditPriceCategoryWdw"), this._elems_.get("btnClosePriceFixWdw")]}]})
		.addWindow({name:"compositePriceWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("compositePriceNew")],  listeners:{ show:{fn:function(win) {win.down('form')._isVisible_ = true;}, scope:this}, close:{fn:function(win) {this._onWdwwClose_(win);}, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewCompositePrice"), this._elems_.get("btnSaveCloseCompositePrice"), this._elems_.get("btnSaveEditCompositePrice"), this._elems_.get("btnCancelCompositePrice")]}]})
		.addWindow({name:"priceCategoryAsgnWdw", _hasTitle_:true, width:800, height:400, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("priceCtgryAssignList")],  listeners:{ show:{fn:function() {this._getDc_('contractPriceCtgryAssign').doReloadPage(); this._markSelectedPriceCategories_()}, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnAssignPriceComponents"), this._elems_.get("btnCancelAssignPriceComponents")]}]})
		.addWindow({name:"priceCompWdw", width:400, height:280, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("priceCompEdit")],  listeners:{ close:{fn:this.onPriceCompWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnPriceCompSave"), this._elems_.get("btnPriceCompCancel")]}]})
		.addWindow({name:"noteWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("NotesEdit")],  listeners:{ close:{fn:this.onNoteWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseNoteWdw"), this._elems_.get("btnCloseNoteWdw")]}]})
		.addWindow({name:"exportBidWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("BidExportForm")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnOkExport"), this._elems_.get("btnCancelExport")]}]})
		.addWindow({name:"wdwApproveNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("approveNote")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseApproveWdw"), this._elems_.get("btnCancelApproveWdw")]}]})
		.addWindow({name:"wdwRejectNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("rejectNote")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRejectWdw"), this._elems_.get("btnCancelRejectWdw")]}]})
		.addPanel({name:"canvas5", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addWindow({name:"wdwCopyLocationBid", _hasTitle_:true, width:900, height:500, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("canvas5")],  listeners:{ close:{fn:this.onCopyLocationBidWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCopy"), this._elems_.get("btnCancelCopy")]}]})
		.addWindow({name:"wdwNewBidRevision", _hasTitle_:true, width:500, height:170, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("bidRevisionForm")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveBidRevision"), this._elems_.get("btnCancelBidRevision")]}]})
		.addWindow({name:"wdwBidCancel", _hasTitle_:true, width:500, height:170, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("bidCancelForm")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveBidCancelWdw"), this._elems_.get("btnCancelBidCancelWdw")]}]})
		.addWindow({name:"wdwDeclineAward", _hasTitle_:true, width:500, height:170, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("bidDeclineAwardForm")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveDeclineAwardWdw"), this._elems_.get("btnCancelDeclineAwardWdw")]}]})
		.addPanel({name:"bidValidationsCanvas", layout:{ type: "border" }, defaults:{split:true}})
		.addWindow({name:"wdwBidValidations", _hasTitle_:true, width:700, height:400, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("bidValidationsCanvas")],  closable:true, bodyPadding:"10", 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCloseBidValidationsWdw"), this._elems_.get("btnCloseAndFilterBidValidationsWdw")]}]})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas2", dcName:"tenderBid"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("priceRestrWrapper", ["priceRestr"])
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["BidsList"], ["center"])
		.addChildrenTo("canvas2", ["Header", "detailsTab"], ["north", "center"])
		.addChildrenTo("canvas3", ["priceCtgryEdit", "priceRestrWrapper"], ["north", "center"])
		.addChildrenTo("canvas4", ["UpdatePriceHeader", "priceCompList", "priceComponentConv"], ["north", "center", "south"])
		.addChildrenTo("detailsTab", ["volPanel"])
		.addChildrenTo("panelAssignAttachments", ["approvalNote", "documentsAssignList"], ["north", "center"])
		.addChildrenTo("volPanel", ["volumes", "ShipToEditList"], ["north", "center"])
		.addChildrenTo("prices", ["priceCtgryList", "totals"], ["center", "south"])
		.addChildrenTo("canvas5", ["tenderSelectList", "tenderLocationList", "bidCopyTypeSelection"], ["center", "east", "north"])
		.addChildrenTo("bidValidationsCanvas", ["bidValidationsLabel", "bidValidationsList"], ["north", "center"])
		.addToolbarTo("BidsList", "tlbBidsList")
		.addToolbarTo("Header", "tlbHeader")
		.addToolbarTo("priceCtgryList", "tlbPriceCategoryList")
		.addToolbarTo("UpdatePriceHeader", "tlbPriceComponentList")
		.addToolbarTo("priceCtgryEdit", "tlbPriceCategoryDetails")
		.addToolbarTo("priceRestrWrapper", "tlbPriceCategoryRestr")
		.addToolbarTo("ShipToEditList", "tlbShipTo")
		.addToolbarTo("NotesList", "tlbNoteList")
		.addToolbarTo("attachmentList", "tlbAttachments");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2", "canvas3", "canvas4"])
		.addChildrenTo2("detailsTab", ["paymentTerms", "creditTerms", "prices", "NotesList", "attachmentList", "History"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbBidsList", {dc: "tenderBid"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnEditBid"),this._elems_.get("btnDeleteBid"),this._elems_.get("btnSubmitForApproval"),this._elems_.get("btnApproveList"),this._elems_.get("btnRejectList"),this._elems_.get("btnSubmitWithMenu"),this._elems_.get("btnSubmitSelected"),this._elems_.get("btnSubmitFailed"),this._elems_.get("btnSubmitAll"),this._elems_.get("btnAwardBidList"),this._elems_.get("btnDeclineBidList"),this._elems_.get("btnCancelBidList"),this._elems_.get("btnAcceptAward"),this._elems_.get("btnDeclineAward"),this._elems_.get("btnGenerateSaleContract"),this._elems_.get("btnNewBidRevision"),this._elems_.get("btnCopyBidList"),this._elems_.get("btnImportWithMenu"),this._elems_.get("btnExportBidList"),this._elems_.get("btnExportList"),this._elems_.get("btnHelpList")])
			.addReports()
		.end()
		.beginToolbar("tlbHeader", {dc: "tenderBid"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas1"}).addSaveInChild({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnSubmitForApprovalEdit"),this._elems_.get("btnApprove"),this._elems_.get("btnReject"),this._elems_.get("btnAwardBidEditForm"),this._elems_.get("btnDeclineBidEditForm"),this._elems_.get("btnCancelBidEditForm"),this._elems_.get("btnAcceptAwardEdit"),this._elems_.get("btnDeclineAwardEdit"),this._elems_.get("btnGenerateSaleContractEdit"),this._elems_.get("btnCopyBidEdit"),this._elems_.get("btnExportBidEdit"),this._elems_.get("btnHelpEdit")])
			.addReports()
		.end()
		.beginToolbar("tlbPriceCategoryList", {dc: "contractPriceCtgry"})
			.addButtons([this._elems_.get("btnNewWithMenu") ])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnEditPrice"),this._elems_.get("btnDeleteContractPriceCtgry"),this._elems_.get("btnUpdatePrice"),this._elems_.get("btnEditFormulaPrice"),this._elems_.get("btnNewFormulaPrice"),this._elems_.get("btnNewFixPrice"),this._elems_.get("btnNewDutyFeeTax"),this._elems_.get("btnNewCompositePrice")])
			.addReports()
		.end()
		.beginToolbar("tlbPriceComponentList", {dc: "contractPriceCmpn"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas2"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewContractPriceComponent")])
			.addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas4"})
			.addButton(this._elems_.get('btnDeleteContractPriceComponent'))
			.addReports()
		.end()
		.beginToolbar("tlbPriceCategoryDetails", {dc: "contractPriceCtgry"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,inContainer:"main",showView:"canvas2"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("savePriceCtgry")])
			.addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addPrevRec({glyph:fp_asc.prev_glyph.glyph})
			.addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbPriceCategoryRestr", {dc: "contractPriceRestr"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewRestriction"),this._elems_.get("btnSaveRestriction"),this._elems_.get("btnCancelRestriction"),this._elems_.get("btnDeleteRestriction")])
			.addReports()
		.end()
		.beginToolbar("tlbShipTo", {dc: "bidAirlineVols"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnSaveVols"),this._elems_.get("btnDeleteShipTo"),this._elems_.get("btnCancelWithMenu"),this._elems_.get("btnCancelSelected"),this._elems_.get("btnCancelAll")])
			.addReports()
		.end()
		.beginToolbar("tlbNoteList", {dc: "note"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewNote"),this._elems_.get("btnEditNote"),this._elems_.get("btnDeleteNote")])
			.addReports()
		.end()
		.beginToolbar("tlbAttachments", {dc: "attachment"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewAttachment"),this._elems_.get("btnDeleteAttachment")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnEditBid
	 */
	,onBtnEditBid: function() {
		this._showStackedViewElement_('main', 'canvas2', this._setPriceCategoryAndPriceBaseState_());
	}
	
	/**
	 * On-Click handler for button btnDeleteBid
	 */
	,onBtnDeleteBid: function() {
		this._getDc_("tenderBid").doDelete();
	}
	
	/**
	 * On-Click handler for button btnAcceptAward
	 */
	,onBtnAcceptAward: function() {
		var successFn = function() {
			this._getDc_("tenderBid").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"acceptAward",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnAcceptAwardEdit
	 */
	,onBtnAcceptAwardEdit: function() {
		var successFn = function() {
			this._getDc_("tenderBid").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"acceptAward",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnDeclineAward
	 */
	,onBtnDeclineAward: function() {
		this.openDeclineAwardWindow();
	}
	
	/**
	 * On-Click handler for button btnDeclineAwardEdit
	 */
	,onBtnDeclineAwardEdit: function() {
		this.openDeclineAwardWindow();
	}
	
	/**
	 * On-Click handler for button btnSaveDeclineAwardWdw
	 */
	,onBtnSaveDeclineAwardWdw: function() {
		var successFn = function() {
			this._applyStateAllButtons_();
			this._getWindow_("wdwDeclineAward").close();
			this._getDc_("tenderBid").doReloadPage();
		};
		var failureFn = function(dc,response) {
			this._getWindow_("wdwDeclineAward").close();
			Main.rpcFailure(response);
		};
		var o={
			name:"declineAward",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCancelDeclineAwardWdw
	 */
	,onBtnCancelDeclineAwardWdw: function() {
		this._getWindow_("wdwDeclineAward").close();
		this._getDc_("tenderBid").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSubmitForApproval
	 */
	,onBtnSubmitForApproval: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnSubmitForApprovalEdit
	 */
	,onBtnSubmitForApprovalEdit: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnApprove
	 */
	,onBtnApprove: function() {
		this.openApproveWindow();
	}
	
	/**
	 * On-Click handler for button btnApproveList
	 */
	,onBtnApproveList: function() {
		this.openApproveWindow();
	}
	
	/**
	 * On-Click handler for button btnReject
	 */
	,onBtnReject: function() {
		this.openRejectWindow();
	}
	
	/**
	 * On-Click handler for button btnRejectList
	 */
	,onBtnRejectList: function() {
		this.openRejectWindow();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApprovalWdw
	 */
	,onBtnSaveCloseApprovalWdw: function() {
		this.submitForApprovalRpc()
	}
	
	/**
	 * On-Click handler for button btnCancelApprovalWdw
	 */
	,onBtnCancelApprovalWdw: function() {
		this._getWindow_("wdwApprovalNote").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApproveWdw
	 */
	,onBtnSaveCloseApproveWdw: function() {
		this.approveRpc()
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRejectWdw
	 */
	,onBtnSaveCloseRejectWdw: function() {
		this.rejectRpc()
	}
	
	/**
	 * On-Click handler for button btnCancelApproveWdw
	 */
	,onBtnCancelApproveWdw: function() {
		this._getWindow_("wdwApproveNote").close();
		this._getDc_("tenderBid").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCancelRejectWdw
	 */
	,onBtnCancelRejectWdw: function() {
		this._getWindow_("wdwRejectNote").close();
		this._getDc_("tenderBid").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSubmitSelected
	 */
	,onBtnSubmitSelected: function() {
		var successFn = function(dc,response) {
			this.initFeedback(response)
			this._getDc_("tenderBid").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"submitSelected",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnSubmitFailed
	 */
	,onBtnSubmitFailed: function() {
		var successFn = function(dc,response) {
			this.initFeedback(response)
			this._getDc_("tenderBid").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"submitFailed",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnSubmitAll
	 */
	,onBtnSubmitAll: function() {
		var successFn = function(dc,response) {
			this.initFeedback(response)
			this._getDc_("tenderBid").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"submitAll",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnExportList
	 */
	,onBtnExportList: function() {
		this._export_();
	}
	
	/**
	 * On-Click handler for button savePriceCtgry
	 */
	,onSavePriceCtgry: function() {
		this.saveContractPriceCtgry(this._getDc_("contractPriceCtgry"));
	}
	
	/**
	 * On-Click handler for button CancelPb
	 */
	,onCancelPb: function() {
		this._cancelIndexPrice_();
	}
	
	/**
	 * On-Click handler for button CancelPbEditFormula
	 */
	,onCancelPbEditFormula: function() {
		this._cancelEditIndexPrice_();
	}
	
	/**
	 * On-Click handler for button btnSaveClosePriceWdw
	 */
	,onBtnSaveClosePriceWdw: function() {
		this.saveClosePriceBase();
	}
	
	/**
	 * On-Click handler for button btnSaveClosePriceEditFormulaWdw
	 */
	,onBtnSaveClosePriceEditFormulaWdw: function() {
		this.saveClosePriceEditFormulaBase();
	}
	
	/**
	 * On-Click handler for button btnSaveClosePriceCategoryWdw
	 */
	,onBtnSaveClosePriceCategoryWdw: function() {
		this.saveClosePriceCategory();
	}
	
	/**
	 * On-Click handler for button btnSaveNewPriceCategoryWdw
	 */
	,onBtnSaveNewPriceCategoryWdw: function() {
		this.saveNewPriceCategory();
	}
	
	/**
	 * On-Click handler for button btnSaveNewCompositePrice
	 */
	,onBtnSaveNewCompositePrice: function() {
		this.saveNewPriceCategory();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseCompositePrice
	 */
	,onBtnSaveCloseCompositePrice: function() {
		this.saveCloseCompositePrice();
	}
	
	/**
	 * On-Click handler for button btnSaveEditCompositePrice
	 */
	,onBtnSaveEditCompositePrice: function() {
		this.saveEditCompositePrice();
	}
	
	/**
	 * On-Click handler for button btnCancelCompositePrice
	 */
	,onBtnCancelCompositePrice: function() {
		this._cancelCompositePrice_();
	}
	
	/**
	 * On-Click handler for button btnSaveEditPriceCategoryWdw
	 */
	,onBtnSaveEditPriceCategoryWdw: function() {
		this.saveEditPriceCategory();
	}
	
	/**
	 * On-Click handler for button btnClosePriceFixWdw
	 */
	,onBtnClosePriceFixWdw: function() {
		this._cancelPriceFix_();
	}
	
	/**
	 * On-Click handler for button btnHelpEdit
	 */
	,onBtnHelpEdit: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnHelpList
	 */
	,onBtnHelpList: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnDeleteContractPriceCtgry
	 */
	,onBtnDeleteContractPriceCtgry: function() {
		this.deleteContractPriceCtgry();
	}
	
	/**
	 * On-Click handler for button btnEditPrice
	 */
	,onBtnEditPrice: function() {
		this.editContractPriceCtgry();
	}
	
	/**
	 * On-Click handler for button btnDeleteContractPriceComponent
	 */
	,onBtnDeleteContractPriceComponent: function() {
		this.deleteContractPriceComponent();
	}
	
	/**
	 * On-Click handler for button btnNewContractPriceComponent
	 */
	,onBtnNewContractPriceComponent: function() {
		this._getDc_("contractPriceCmpn").doNew();
	}
	
	/**
	 * On-Click handler for button btnDeleteContractPricingBase
	 */
	,onBtnDeleteContractPricingBase: function() {
		this.deleteContractPricingBases();
	}
	
	/**
	 * On-Click handler for button btnUpdatePrice
	 */
	,onBtnUpdatePrice: function() {
		this.updateContractPriceCtgry();
	}
	
	/**
	 * On-Click handler for button btnEditFormulaPrice
	 */
	,onBtnEditFormulaPrice: function() {
		this._editFormulaPrice_();
	}
	
	/**
	 * On-Click handler for button btnPriceCompSave
	 */
	,onBtnPriceCompSave: function() {
		this.saveClosePriceComp();
	}
	
	/**
	 * On-Click handler for button btnPriceCompCancel
	 */
	,onBtnPriceCompCancel: function() {
		this.onPriceCompWdwClose();
		this._getWindow_("priceCompWdw").close();
	}
	
	/**
	 * On-Click handler for button btnExportEdit
	 */
	,onBtnExportEdit: function() {
		this._export_();
	}
	
	/**
	 * On-Click handler for button btnAwardBidList
	 */
	,onBtnAwardBidList: function() {
		var successFn = function() {
			this._applyStateAllButtons_();
			this._getDc_("tenderBid").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"awardBid",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnAwardBidEditForm
	 */
	,onBtnAwardBidEditForm: function() {
		var successFn = function() {
			this._applyStateAllButtons_();
			this._getDc_("tenderBid").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"awardBid",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnDeclineBidList
	 */
	,onBtnDeclineBidList: function() {
		var successFn = function() {
			this._applyStateAllButtons_();
			this._getDc_("tenderBid").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"declineBid",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnDeclineBidEditForm
	 */
	,onBtnDeclineBidEditForm: function() {
		var successFn = function() {
			this._applyStateAllButtons_();
			this._getDc_("tenderBid").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"declineBid",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnCancelBidList
	 */
	,onBtnCancelBidList: function() {
		this.openCancelBidWindow();
	}
	
	/**
	 * On-Click handler for button btnCancelBidEditForm
	 */
	,onBtnCancelBidEditForm: function() {
		this.openCancelBidWindow();
	}
	
	/**
	 * On-Click handler for button btnNewFormulaPrice
	 */
	,onBtnNewFormulaPrice: function() {
		this._setDefaultPb();
	}
	
	/**
	 * On-Click handler for button btnNewFixPrice
	 */
	,onBtnNewFixPrice: function() {
		this._setDefaultFix();
	}
	
	/**
	 * On-Click handler for button btnNewDutyFeeTax
	 */
	,onBtnNewDutyFeeTax: function() {
		this._setDefaultDFT();
	}
	
	/**
	 * On-Click handler for button btnNewCompositePrice
	 */
	,onBtnNewCompositePrice: function() {
		this._setDefaultComposite();
	}
	
	/**
	 * On-Click handler for button btnNewNote
	 */
	,onBtnNewNote: function() {
		this._getDc_("note").doNew();
	}
	
	/**
	 * On-Click handler for button btnEditNote
	 */
	,onBtnEditNote: function() {
		this._getDc_("note").doEditIn();
	}
	
	/**
	 * On-Click handler for button btnDeleteNote
	 */
	,onBtnDeleteNote: function() {
		this._getDc_("note").doDelete();
	}
	
	/**
	 * On-Click handler for button btnNewAttachment
	 */
	,onBtnNewAttachment: function() {
		this._getDc_("attachment").doNew();
	}
	
	/**
	 * On-Click handler for button btnDeleteAttachment
	 */
	,onBtnDeleteAttachment: function() {
		this._getDc_("attachment").doDelete();
	}
	
	/**
	 * On-Click handler for button btnCancelSelected
	 */
	,onBtnCancelSelected: function() {
		this.fnCancelSelected();
	}
	
	/**
	 * On-Click handler for button btnCancelAll
	 */
	,onBtnCancelAll: function() {
		this.fnCancelAll();
	}
	
	/**
	 * On-Click handler for button btnCloseNoteWdw
	 */
	,onBtnCloseNoteWdw: function() {
		this._getDc_("note").doCancel();
		this._getWindow_("noteWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseNoteWdw
	 */
	,onBtnSaveCloseNoteWdw: function() {
		this.saveCloseNote();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRemarkWdw
	 */
	,onBtnSaveCloseRemarkWdw: function() {
		var successFn = function(dc) {
			this._getWindow_("wdwRemarks").close();
			this._getDc_("bidAirlineVols").doReloadPage();
			this._getDc_("history").doReloadPage();
			var dcST = this._getDc_("bidAirlineVols"); 
								dcST.setParamValue("remarks",""); 
								dcST._volumeChanged_ = null;
		};
		var o={
			name:"saveInHistory",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("bidAirlineVols").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCancelRemarkWdw
	 */
	,onBtnCancelRemarkWdw: function() {
		this._getWindow_("wdwRemarks").close();
	}
	
	/**
	 * On-Click handler for button btnAssignPriceComponents
	 */
	,onBtnAssignPriceComponents: function() {
		this._assignPriceComponents_();
	}
	
	/**
	 * On-Click handler for button btnCancelAssignPriceComponents
	 */
	,onBtnCancelAssignPriceComponents: function() {
		this._cancelAssignPriceComponents_();
	}
	
	/**
	 * On-Click handler for button btnNewRestriction
	 */
	,onBtnNewRestriction: function() {
		this._getDc_("contractPriceRestr").doNew();
	}
	
	/**
	 * On-Click handler for button btnSaveRestriction
	 */
	,onBtnSaveRestriction: function() {
		this._getDc_("contractPriceRestr").doSave();
	}
	
	/**
	 * On-Click handler for button btnCancelRestriction
	 */
	,onBtnCancelRestriction: function() {
		this._getDc_("contractPriceRestr").doCancel();
	}
	
	/**
	 * On-Click handler for button btnDeleteRestriction
	 */
	,onBtnDeleteRestriction: function() {
		this._getDc_("contractPriceRestr").doDelete();
	}
	
	/**
	 * On-Click handler for button btnGenerateSaleContract
	 */
	,onBtnGenerateSaleContract: function() {
		var successFn = function(dc,response) {
			this._getDc_("history").doReloadPage();
			this._showSaleContractDetails_(dc,response)
		};
		var o={
			name:"generateContract",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnGenerateSaleContractEdit
	 */
	,onBtnGenerateSaleContractEdit: function() {
		var successFn = function(dc,response) {
			this._getDc_("history").doReloadPage();
			this._showSaleContractDetails_(dc,response)
		};
		var o={
			name:"generateContract",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCopyBidList
	 */
	,onBtnCopyBidList: function() {
		this._getWindow_("wdwCopyLocationBid").show();
		var tenderInvitationDc = this._getDc_("tenderInviation");
					 var tenderLocationDc = this._getDc_("tenderLocation");
					 var bidDc = this._getDc_("tenderBid");
					 
					 tenderInvitationDc.setParamValue("selectedLocation",bidDc.record.data.bidLocId);
					 tenderLocationDc.setParamValue("selectedLocation",bidDc.record.data.bidLocId);
					 tenderInvitationDc.doQuery();
	}
	
	/**
	 * On-Click handler for button btnNewBidRevision
	 */
	,onBtnNewBidRevision: function() {
		this._getWindow_("wdwNewBidRevision").show();
	}
	
	/**
	 * On-Click handler for button btnCopyBidEdit
	 */
	,onBtnCopyBidEdit: function() {
		this._getWindow_("wdwCopyLocationBid").show();
		var tenderInvitationDc = this._getDc_("tenderInviation");
					 var tenderLocationDc = this._getDc_("tenderLocation");
					 var bidDc = this._getDc_("tenderBid");
					 
					 tenderInvitationDc.setParamValue("selectedLocation",bidDc.record.data.bidLocId);
					 tenderLocationDc.setParamValue("selectedLocation",bidDc.record.data.bidLocId);
					 tenderInvitationDc.doQuery();
	}
	
	/**
	 * On-Click handler for button btnCopy
	 */
	,onBtnCopy: function() {
		
						var tenderLocationDc = this._getDc_("tenderLocation");
						var bidDc = this._getDc_("tenderBid");
						var tenderLocationDc = this._getDc_("tenderLocation");
						if(tenderLocationDc && tenderLocationDc.record && tenderLocationDc.record.data && tenderLocationDc.record.data.id){
							bidDc.setParamValue("tenderLocationId",tenderLocationDc.record.data.id);
						}
		var successFn = function(dc,response) {
			this._getWindow_("wdwCopyLocationBid").close();
			dc.doCancel();
									dc.doClearAllFilters();					
				        		    var res = Ext.getResponseDataInJSON(response).params;
									dc.setFilterValue("id", res.generatedContractId);	
									this._showStackedViewElement_("main", "canvas2", dc.doQuery());
		};
		var o={
			name:"copySupplierBid",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnSaveBidRevision
	 */
	,onBtnSaveBidRevision: function() {
		
						var tenderLocationDc = this._getDc_("tenderLocation");
						var bidDc = this._getDc_("tenderBid");
						var tenderLocationDc = this._getDc_("tenderLocation");
						bidDc.setParamValue("anotherBidSelection",false);
						bidDc.setParamValue("newVersionSelection",false);
						bidDc.setParamValue("newRevisionSelection",true);
						if(tenderLocationDc && tenderLocationDc.record && tenderLocationDc.record.data && tenderLocationDc.record.data.id){
							bidDc.setParamValue("tenderLocationId",tenderLocationDc.record.data.id);					
						}
		var successFn = function(dc,response) {
			this._getWindow_("wdwNewBidRevision").close();
			
									dc.doCancel();
									dc.doClearAllFilters();					
				        		    var res = Ext.getResponseDataInJSON(response).params;
									dc.setFilterValue("id", res.generatedContractId);	
									this._showStackedViewElement_("main", "canvas2", dc.doQuery());
		};
		var o={
			name:"copySupplierBid",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnSaveBidCancelWdw
	 */
	,onBtnSaveBidCancelWdw: function() {
		var successFn = function() {
			this._applyStateAllButtons_();
			this._getWindow_("wdwBidCancel").close();
			this._getDc_("tenderBid").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"cancelBid",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnCancelBidCancelWdw
	 */
	,onBtnCancelBidCancelWdw: function() {
		this._getWindow_("wdwBidCancel").close();
		this._getDc_("tenderBid").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCancelCopy
	 */
	,onBtnCancelCopy: function() {
		this._getWindow_("wdwCopyLocationBid").close();
	}
	
	/**
	 * On-Click handler for button btnCancelBidRevision
	 */
	,onBtnCancelBidRevision: function() {
		this._getWindow_("wdwNewBidRevision").close();
		this._getDc_("tenderBid").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnImportWithMenu
	 */
	,onBtnImportWithMenu: function() {
		this._getWindow_("wdwImportBid").show();
	}
	
	/**
	 * On-Click handler for button btnExportBidList
	 */
	,onBtnExportBidList: function() {
		this._getWindow_("exportBidWdw").show();
	}
	
	/**
	 * On-Click handler for button btnExportBidEdit
	 */
	,onBtnExportBidEdit: function() {
		this._getWindow_("exportBidWdw").show();
	}
	
	/**
	 * On-Click handler for button btnOkExport
	 */
	,onBtnOkExport: function() {
		var successFn = function() {
			this.showTemplateCopy();
		};
		var o={
			name:"exportBid",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcData(o);
		this._getWindow_("exportBidWdw").close();
	}
	
	/**
	 * On-Click handler for button btnCancelExport
	 */
	,onBtnCancelExport: function() {
		this._getWindow_("exportBidWdw").close();
		this._getDc_("tenderBid").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnDeleteShipTo
	 */
	,onBtnDeleteShipTo: function() {
		this._getDc_("bidAirlineVols").doDelete();
	}
	
	/**
	 * On-Click handler for button btnSaveVols
	 */
	,onBtnSaveVols: function() {
		this.saveVols();
	}
	
	/**
	 * On-Click handler for button btnCloseBidValidationsWdw
	 */
	,onBtnCloseBidValidationsWdw: function() {
		this._getWindow_("wdwBidValidations").close();
	}
	
	/**
	 * On-Click handler for button btnCloseAndFilterBidValidationsWdw
	 */
	,onBtnCloseAndFilterBidValidationsWdw: function() {
		this.viewFailedBids();
	}
	
	,setDefaultValues: function() {	
		var successFn = function(dc) {
			
								var dc = this._getDc_("tenderBid");
								var view = this._get_("paymentTerms");
								var params = dc.params;
								var record = dc.getRecord();
								var avgMethodField = view._get_("avgMethodName");
			
								if (record) {
			
									var status = record.get("status");
			
									record.beginEdit();
									record.set("settCurrId",params.get("currencyID"));
									record.set("settCurrCode",params.get("currencyCode"));
									record.set("exchangeRateOffset",params.get("period"));
									record.set("paymentRefDay",params.get("referenceTo"));
									record.set("avgMethodId",params.get("averageMethodId"));
									record.set("avgMethodName",params.get("averageMethodName"));
									record.set("financialSourceId",params.get("financialSourceId"));
									record.set("financialSourceCode",params.get("financialSourceCode"));
									record.set("invoiceFreq",params.get("invFreq"));
									record.set("invoiceType",params.get("invType"));
									record.set("creditTerms",params.get("creditTerms"));
									record.set("forex",params.get("forex"));
									record.set("paymentTerms",params.get("paymentTerms"));
									record.set("holderCode",parent._ACTIVESUBSIDIARY_.code);
									record.endEdit();
			
									if (!Ext.isEmpty(params.get("financialSourceCode")) && (status !== "Effective" && status !== "Expired" && status !== "Active")) {
										avgMethodField._enable_();
									}
								}					
		};
		var o={
			name:"setDefaulPaymentTerms",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("tenderBid").doRpcData(o);
	}
	
	,assignPrice: function() {	
		this._showAsgnWindow_("atraxo.cmm.ui.extjs.asgn.PriceCtgryAssign_Asgn$Ui" ,{dc: "contractPriceCtgry", objectIdField: "id",leftAdvFilter: [{"fieldName":"idContract","value1":this._getDc_("contractPriceCtgry").getRecord().get("contract"),"operation":"="},{"fieldName":"id","value1":this._getDc_("contractPriceCtgry").getRecord().get("id"),"operation":"<>"}],rightAdvFilter: [{"fieldName":"idContract","value1":this._getDc_("contractPriceCtgry").getRecord().get("contract"),"operation":"="},{"fieldName":"id","value1":this._getDc_("contractPriceCtgry").getRecord().get("id"),"operation":"<>"}]
		,listeners: {close: {scope: this, fn: function() { this._getDc_("contractPriceCtgry").doReloadRecord();
		}} }});
	}
	
	,showWdwPricing: function() {	
		this._getWindow_("wdwPricing").show();
	}
	
	,onWdwTempClose: function() {	
		this._getDc_("tenderBid").doCancel();
	}
	
	,priceCtgrySetDefault: function() {	
		var successFn = function(dc) {
			
			                        var dc = this._getDc_("contractPriceCtgry");
			                        var isDefault = dc.params.get("isDefault");
			                        if(isDefault){
			                            Ext.Msg.show({
			                               title : "Warning",
										   msg: Main.translate("applicationMsg", "priceCatWarning__lbl"),
			                               buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
			                               fn : function(btnId) {
			                                      if( btnId === "yes" ){
			                                            dc.params.set("isDefault",false);
			                                            this.setAndSaveNewDefaultPriceCtgry();
			                                        }
			                               },
			                               scope : this
			                            });
			                        }else{
			                            this.setAndSaveNewDefaultPriceCtgry();
			                        }                        
		};
		var o={
			name:"checkDefault",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("contractPriceCtgry").doRpcData(o);
	}
	
	,setAndSaveNewDefaultPriceCtgry: function() {	
		var successFn = function() {
			
			                        var cpc = this._getDc_("contractPriceCtgry");
			                        cpc.doSave();
		};
		var o={
			name:"setDefault",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("contractPriceCtgry").doRpcData(o);
	}
	
	,showNoteWdw: function() {	
		this._getWindow_("noteWdw").show();
	}
	
	,onNoteWdwClose: function() {	
		this._getDc_("note").doCancel();
	}
	
	,viewFailedBids: function() {
		
						var dc = this._getDc_("tenderBid");
						var validationDc = this._getDc_("bidValidations");
		
						dc.doClearAllFilters();
						var af = [];
						var o = {
							"fieldName":"refid",
							"id":null,
							"operation":"in",
							"value1": validationDc.advancedFilter[0].value1
							};
						af.push(o);
						dc.advancedFilter = af;
						dc.doQuery();
						var win = this._getWindow_("wdwBidValidations");
						win.close();
		
	}
	
	,saveVols: function() {
		
						var tenderBid = this._getDc_("tenderBid");
						var vols = this._getDc_("bidAirlineVols");
						var r = tenderBid.getRecord();
						if (r.dirty === true) {
							Ext.Msg.show({
					       		title : "Warning",
					       		msg : Main.translate("applicationMsg","saveVolWarning_lbl"),
					       		buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
					       		fn : function(btnId) {
					            	if( btnId == "yes" ){
					                     vols.doSave();
										 tenderBid.doCancel();
			              			}
			       				},
			       				scope : this
							});
						}
						else {
							vols.doSave();
						}
	}
	
	,_showEdit_: function() {
		
						this._showStackedViewElement_("main", "canvas2");
	}
	
	,onCopyLocationBidWdwClose: function() {
		
						var dc = this._getDc_("tenderBid");
						var dcRight = this._getDc_("tenderLocation");
						var sf = Ext.getCmp(this._tiSourceLocationListSearchFieldId_);
						sf.setValue("");
						dcRight.advancedFilter = null;
						dcRight.doQuery();
						dc.doReloadPage();
	}
	
	,_onShow_from_externalInterfaceMessageHistory_: function(params) {
			
						var dc = this._getDc_("tenderBid");
						dc.setFilterValue("id", params.objectId);
						dc.doQuery({queryFromExternalInterface: true});
	}
	
	,_onShow_from_TenderInvitation_: function(params) {
			
						var dc = this._getDc_("tenderBid");
						var validationDc = this._getDc_("bidValidations");
		
						dc.doClearAllFilters();
						var af = [];
						var o = params.advancedFilter;
						af.push(o);
						dc.advancedFilter = af;
						dc.doQuery();
	}
	
	,_when_called_from_dshboard_: function(id) {
		
						var dc = this._getDc_("tenderBid");
						dc.doCancel();
						dc.doClearAllFilters();
						dc.setFilterValue("id", id);				
						dc.doQuery({showEdit: true}); 
	}
	
	,initFeedback: function(response) {
		
						var responseData = Ext.getResponseDataInJSON(response);
						this.showInvalidBids("wdwBidValidations", responseData.params);	
	}
	
	,canDeleteBid: function() {
		
						var dc = this._getDc_("tenderBid");
						var selectedRecords = dc.selectedRecords;
						if (dc) {
							for(var i=0 ; i<selectedRecords.length ; ++i){
								var record = selectedRecords[i];
								
								if(record.data.bidStatus == __CMM_TYPE__.BidStatus._DRAFT_){
									if(_SYSTEMPARAMETERS_.sysbidapproval === "true" && 
										(record.data.bidApprovalStatus == __CMM_TYPE__.BidApprovalStatus._APPROVED_ ||
										record.data.bidApprovalStatus == __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_)){
										return false;
									}
								}else if(record.data.bidStatus == __CMM_TYPE__.BidStatus._SUBMITTED_){
									if(record.data.bidTransmissionStatus == __CMM_TYPE__.TransmissionStatus._IN_PROGRESS_ || 
									   record.data.bidTransmissionStatus == __CMM_TYPE__.TransmissionStatus._TRANSMITTED_){
										return false;
									  }
								}else{
									return false;
								}
							}
		
							return true;
						}				
	}
	
	,canAwardDecline: function(dc) {
		 
						var selectedRecords = dc.selectedRecords;				
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.tenderSource == __CMM_TYPE__.TenderSource._CAPTURED_ && record.data.bidLocBiddingStatus != __CMM_TYPE__.BiddingStatus._FINISHED_){
								if(record.data.bidStatus == __CMM_TYPE__.BidStatus._DRAFT_){
									if(_SYSTEMPARAMETERS_.sysbidapproval === "true" && record.data.bidApprovalStatus != __CMM_TYPE__.BidApprovalStatus._APPROVED_){
										return false;
									}
								}else{
									return false;
								}
							}else{
								return false;
							}
						}
						return true;
	}
	
	,canAcceptAward: function(dc) {
		 
						var selectedRecords = dc.selectedRecords;				
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.bidStatus !== __CMM_TYPE__.BidStatus._AWARDED_){
								return false;
							}else if(record.data.tenderSource === __CMM_TYPE__.TenderSource._IMPORTED_ &&
									 (record.data.bidAcceptAwardTransmissionStatus !== __CMM_TYPE__.TransmissionStatus._NEW_ && 
									 record.data.bidAcceptAwardTransmissionStatus !== __CMM_TYPE__.TransmissionStatus._FAILED_)){
								return false;
							}
						}
						return true;
	}
	
	,canDeclineAward: function(dc) {
		 
						var selectedRecords = dc.selectedRecords;				
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.bidStatus !== __CMM_TYPE__.BidStatus._AWARDED_){
								return false;
							}else if(record.data.tenderSource === __CMM_TYPE__.TenderSource._IMPORTED_ &&
									 (record.data.bidDeclineAwardTransmissionStatus !== __CMM_TYPE__.TransmissionStatus._NEW_ &&
									 record.data.bidDeclineAwardTransmissionStatus !== __CMM_TYPE__.TransmissionStatus._FAILED_)){
								return false;
							}
						}
						return true;
	}
	
	,canCancelBid: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
		
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if (record.data.canCancel === false) {
								return false;
							}
						}
						return true;
	}
	
	,canSubmitForApproval: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if( record.data.bidLocBiddingStatus != __CMM_TYPE__.BiddingStatus._FINISHED_ && record.data.bidStatus == __CMM_TYPE__.BidStatus._DRAFT_ ){
								if(record.data.bidApprovalStatus != __CMM_TYPE__.BidApprovalStatus._NEW_ && 
							       record.data.bidApprovalStatus != __CMM_TYPE__.BidApprovalStatus._REJECTED_){
								   return false;
								}	
							} else{
								return false;
							}
						}
		
						return true;
	}
	
	,canApproveReject: function(dc) {
		
						var r = dc.getRecord();
						if (r) {
							return ( r.data.bidStatus === __CMM_TYPE__.BidStatus._DRAFT_ && r.data.canBeCompleted == true) ;
							//canBeCompleted is set in TenderBid_DsService in postFind
						}
						return false;
	}
	
	,canSubmitToIssuer: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
		
						var tenderSourceIsImported = function(record) {
							return record.data.tenderSource == __CMM_TYPE__.TenderSource._IMPORTED_;
						}
		
						var tenderStatusIsNew = function(record) {
							return record.data.tenderStatus == __CMM_TYPE__.TenderStatus._NEW_;
						}
		
						var locationBiddingStatusIsNotFinished = function(record) {
							return record.data.bidLocBiddingStatus != __CMM_TYPE__.BiddingStatus._FINISHED_;
						}
		
						var bidStatusIsDraftOrSubmitted = function(record) {
							return record.data.bidStatus == __CMM_TYPE__.BidStatus._DRAFT_ || record.data.bidStatus == __CMM_TYPE__.BidStatus._SUBMITTED_;
						}
		
						var bidTransmissionStatusIsNewOrFailed = function(record) {
							return record.data.bidTransmissionStatus == __CMM_TYPE__.TransmissionStatus._NEW_ || record.data.bidTransmissionStatus == __CMM_TYPE__.TransmissionStatus._FAILED_;
						}
		
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if( tenderSourceIsImported(record) ) {
								if( tenderStatusIsNew(record) && locationBiddingStatusIsNotFinished(record) && bidStatusIsDraftOrSubmitted(record) && bidTransmissionStatusIsNewOrFailed(record) ){
								   if(_SYSTEMPARAMETERS_.sysbidapproval === "true" && record.data.bidApprovalStatus != __CMM_TYPE__.BidApprovalStatus._APPROVED_){
										return false;
								   }
								}else{
									return false;
								}
							}else{
								return false;
							}
							
						}
		
						return true;
	}
	
	,canSubmitSelectedToIssuer: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if( record.data.tenderSource ==__CMM_TYPE__.TenderSource._IMPORTED_){
								if((record.data.tenderStatus ==__CMM_TYPE__.TenderStatus._NEW_ || record.data.tenderStatus ==__CMM_TYPE__.TenderStatus._UPDATED_) && 
								   record.data.bidLocBiddingStatus != __CMM_TYPE__.BiddingStatus._FINISHED_ &&
								   record.data.bidStatus == __CMM_TYPE__.BidStatus._DRAFT_  &&
								   record.data.bidTransmissionStatus == __CMM_TYPE__.TransmissionStatus._NEW_){
								   
								   if(_SYSTEMPARAMETERS_.sysbidapproval === "true" && record.data.bidApprovalStatus != __CMM_TYPE__.BidApprovalStatus._APPROVED_){
										return false;
								   }
								}else{
									return false;
								}
							}else{
								return false;
							}
							
						}
		
						return true;
	}
	
	,showTemplateCopy: function() {
		
		                var dc = this._getDc_("tenderBid");
		                var fn = dc.getParamValue("previewFile");
		                var r = window.open(Main.urlDownload + "/" + fn,"Preview");
		                r.focus();  
	}
	
	,_markSelectedPriceCategories_: function() {
		
						
						var view = this._get_("priceCtgryAssignList");
						var form = this._get_("priceCtgryEdit");
						var percentageOf = form._get_("percentageOf");
						var win = this._getWindow_("priceCategoryAsgnWdw");
		
						var f = function() {
		
							if (win._isInEditForm_ === true) {	
								view.store.on("load", function(s) {	
									if (!Ext.isEmpty(percentageOf.getRawValue())) {	
										var percentageOfArray = percentageOf.getRawValue().split(",");
										s.each(function(r) {
											if (percentageOfArray.indexOf(r.get("name")) !== -1 ) {
												view.getSelectionModel().select(s.indexOf(r), true);
											}
										}, this);
									}
								}, this);
							}
							else {
								view.store.on("load", function() {	
									view.getSelectionModel().select(0, false);
								}, this);
							}
		
						}
		
						Ext.defer(f, 250, this);
	}
	
	,_assignPriceComponents_: function() {
		
		
						var grid = this._get_("priceCtgryAssignList");
						var priceCatDc = this._getDc_("contractPriceCtgry");
						var pricingBase = this._getDc_("pricingBase");
						var s = grid.getSelectionModel().getSelection();
						var win = this._getWindow_("priceCategoryAsgnWdw");
						var r = pricingBase.getRecord();
						var priceCatRecord = priceCatDc.getRecord();
						var selected = [];
						var selectedNames = [];
		
						Ext.each(s, function (item) {
						  selectedNames.push(item.data.priceCtgryName);
						  selected.push({id:item.data.id});
						});
		
						if (r) {
							r.set("priceCategoryIdsJson",JSON.stringify(selected));
							r.set("priceComponents",selectedNames.join(","));
						}
						if (priceCatRecord && win._isInEditForm_ === true) {
							priceCatRecord.set("priceCategoryIdsJson",JSON.stringify(selected));
							priceCatRecord.set("percentageOf",selectedNames.join(","));
						}
		
						win.close();
	}
	
	,_cancelAssignPriceComponents_: function() {
		
		
						var grid = this._get_("priceCtgryAssignList");
						var dc = grid._controller_;
						var win = this._getWindow_("priceCategoryAsgnWdw");
						dc.doCancel();
						win.close();
	}
	
	,_excludeComposite_: function(list) {
		
						var s = list.store;
						s.filterBy(function (record) {
				           if (record.get("priceCtgryPricePer") !== __FMBAS_TYPE__.PriceInd._COMPOSITE_) return record;
				        });
	}
	
	,_setPriceRestrictionEditorType_: function(editor,ctx,view) {
		
					var fieldIndex = ctx.field;
					var col = ctx.column;
					var cfg = {};
					var textEditor = {xtype:"textfield", maxLength:32};	
					var numberEditor = {xtype:"numberfield", maxLength:32};	
					var storeData = [];
					var timeFormat = "H:i";
		
					if (fieldIndex === "value") {
						var r = ctx.record;
						if (r) {
							var restrictionType = r.get("restrictionType");
							if (!Ext.isEmpty(restrictionType)) {
		
								//////////// Aircraft type ////////////
		
								if (restrictionType === __CMM_TYPE__.RestrictionTypes._AIRCRAFT_TYPE_) {
									cfg = {
										_dcView_ : view, 
										xtype:"fmbas_AcTypesLov_Lov", 
										maxLength:32, 
										typeAhead: false,
										remoteFilter: true,
										remoteSort: true,
										forceSelection: true,
										retFieldMapping: [{
											lovField:"code", 
											dsField: "value"}
										]
									};
								}
		
								//////////// Fueling type ////////////
		
								else if (restrictionType === __CMM_TYPE__.RestrictionTypes._FUELING_TYPE_) {
									for (var key in __CMM_TYPE__.FuelTicketFuelingOperation) {
										storeData.push(__CMM_TYPE__.FuelTicketFuelingOperation[key]);
									}
									cfg = {
									    xtype: "combo",
										store: storeData,
										forceSelection: true
									};
								}
		
								//////////// Transport type ////////////
		
								else if (restrictionType === __CMM_TYPE__.RestrictionTypes._TRANSPORT_TYPE_ ) {
									for (var key in __CMM_TYPE__.FuelingType) {
										storeData.push(__CMM_TYPE__.FuelingType[key]);
									}
									cfg = {
									    xtype: "combo",
									    store: storeData,
										forceSelection: true
									};
								}
		
								//////////// Registration number ////////////
		
								else if (restrictionType === __CMM_TYPE__.RestrictionTypes._REGISTRATION_NUMBER_ ) {							
									cfg = {xtype:"textfield", maxLength:5, enforceMaxLength : true};
								}
		
								//////////// Operation type ////////////
		
								else if (restrictionType === __CMM_TYPE__.RestrictionTypes._OPERATION_TYPE_ ) {
									for (var key in __OPS_TYPE__.OperationType) {
										storeData.push(__OPS_TYPE__.OperationType[key]);
									}
									cfg = {
									    xtype: "combo",
									    store: storeData,
										forceSelection: true
									};
								}
		
								//////////// Ship to ////////////
		
								else if (restrictionType === __CMM_TYPE__.RestrictionTypes._SHIP_TO_) {
									var tenderBid = this._getDc_("tenderBid");
		                            var tenderRecord = tenderBid.getRecord();
		                            var contractId;
									if (tenderRecord) {
		                            	contractId = tenderRecord.get("id");
		                            }
									cfg = {
										_dcView_ : view, 
										xtype:"cmm_ShipToCustomerCodeLov_Lov", 
										maxLength:32,
										typeAhead: false,
										remoteFilter: true,
										remoteSort: true,
										forceSelection: true,
										filterFieldMapping: [
											{lovField:"isCustomer", value: true}, 
		                    				{lovField:"contractId", value: contractId}
										],
										retFieldMapping: [{
											lovField:"code", 
											dsField: "value"
										}],								
									};
								}
		
								//////////// Time of operation ////////////
		
								else if (restrictionType === __CMM_TYPE__.RestrictionTypes._TIME_OF_OPERATION_ ) {
									cfg = {
		                                xtype: "combo",
		                                selectOnFocus: true,
		                                queryMode: "local",
		                                store: ["00:00","00:15","00:30","00:45","01:00","01:15","01:30","01:45","02:00","02:15","02:30","02:45","03:00","03:15","03:30","03:45","04:00","04:15","04:30","04:45","05:00","05:15","05:30","05:45","06:00","06:15","06:30","06:45","07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45","10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"]
		                            };
								}
		
								//////////// Flight type ////////////
		
								else if (restrictionType === __CMM_TYPE__.RestrictionTypes._FLIGHT_TYPE_  ) {
									for (var key in __CMM_TYPE__.FlightTypeIndicator) {
										storeData.push(__CMM_TYPE__.FlightTypeIndicator[key]);
									}
									cfg = {
									    xtype: "combo",
									    store: storeData,
										forceSelection: true
									};
								}
		
								//////////// Volume ////////////
		
								else if (restrictionType === __CMM_TYPE__.RestrictionTypes._VOLUME_  ) {
									cfg = {xtype:"numberfield", minValue:0};
								}
		
								//////////// END CASE ////////////
		
								else {
									cfg = textEditor;
								}
		
								col.setEditor(cfg);
		
							}
						}
					}
		
					// Clear the value on restriction type change
		
					if (fieldIndex === "restrictionType") {
						var ed = col.getEditor();
						ed.on("change", function() {
							var record = view._controller_.getRecord();
							record.set("value", "");
							record.set("unit", null);
							record.set("unitCode", "");
						}, this);
					}
	}
	
	,openSubmitForApprovalWindow: function() {
		
						var label = "Please provide a note for approval.";
						var title = "Submit for approval";
						this.setupNoteWindow("wdwApprovalNote","approvalNote","btnSaveCloseApprovalWdw","btnCancelApprovalWdw" , label , title);				
	}
	
	,openApproveWindow: function() {
		
						var label = "Please provide an approval reason.";
						var title = "Approve";
						this.setupNoteWindow("wdwApproveNote","approveNote","btnSaveCloseApproveWdw","btnCancelApproveWdw" , label , title);				
	}
	
	,openRejectWindow: function() {
		
						var label = "Please provide a reason for rejection.";
						var title = "Reject";
						this.setupNoteWindow("wdwRejectNote","rejectNote","btnSaveCloseRejectWdw","btnCancelRejectWdw" , label , title);				
	}
	
	,setupNoteWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var approvalNoteField = this._get_(theForm)._get_("approvalNote");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						var ctx = this;
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(true);
						
						approvalNoteField.fieldLabel = label;
						approvalNoteField.setValue("");
						window.title = title;
						
						approvalNoteField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
								cancelButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
								cancelButton.setDisabled(true);
						    }
						});
						
						if ("wdwApprovalNote" == theWindow) {
							var view = this._get_("documentsAssignList");
							view.store.load();
						}
						window.show();
	}
	
	,openCancelBidWindow: function() {
		
						var label = "Please provide a reason for cancelling the bid.";
						var title = "Cancel Bid";
						this.setupCancelBidWindow("wdwBidCancel","bidCancelForm","btnSaveBidCancelWdw","btnCancelBidCancelWdw" , label , title);				
	}
	
	,openDeclineAwardWindow: function() {
		
						var label = "Please provide a reason for declining the bid award.";
						var title = "Decline Bid Award";
						this.setupDeclineAwardWindow("wdwDeclineAward","bidDeclineAwardForm","btnSaveDeclineAwardWdw","btnCancelDeclineAwardWdw" , label , title);				
	}
	
	,setupCancelBidWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var reasonField = this._get_(theForm)._get_("bidCancelReason");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						var ctx = this;
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(true);
						
						reasonField.fieldLabel = label;
						reasonField.setValue("");
						window.title = title;
						
						reasonField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
								cancelButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
								cancelButton.setDisabled(true);
						    }
						});
		
						window.setHeight(200);
						window.show();
	}
	
	,setupDeclineAwardWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var reasonField = this._get_(theForm)._get_("bidDeclineAwardReason");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						var ctx = this;
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(true);
						
						reasonField.fieldLabel = label;
						reasonField.setValue("");
						window.title = title;
						
						reasonField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
								cancelButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
								cancelButton.setDisabled(true);
						    }
						});
		
						window.setHeight(200);
						window.show();
	}
	
	,submitForApprovalRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								var responseDescription = responseData.params.submitForApprovalDescription;							
								var responseResult = responseData.params.submitForApprovalResult;
								
								//close window
								this._getWindow_("wdwApprovalNote").close();
								
								//display results
								this.showInvalidBids("wdwBidValidations", responseData.params);	
		
								//reload page
								dc.doReloadPage();
							}
						};
		
						this._sendAttachments_();
		
		                var dc = this._getDc_("tenderBid");
		                var t = {
					        name: "submitForApproval",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcDataList(t);
	}
	
	,approveRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								//close window
								this._getWindow_("wdwApproveNote").close();
								
								//reload page
								dc.doReloadPage();
							}
						};
		
		                var dc = this._getDc_("tenderBid");
		                var t = {
					        name: "approveTenderBid",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcData(t);
	}
	
	,rejectRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								//close window
								this._getWindow_("wdwRejectNote").close();
								
								//reload page
								dc.doReloadPage();
							}
						};
		
		                var dc = this._getDc_("tenderBid");
		                var t = {
					        name: "rejectTenderBid",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcData(t);
	}
	
	,_sendAttachments_: function() {
		
						var list = this._get_("documentsAssignList");
						var view = list.getView();
						var selectedAttachments = [];
						var dc = this._getDc_("tenderBid");
						if (view.getSelectionModel().hasSelection()) {
						   var rows = view.getSelectionModel().getSelection();
						   Ext.each(rows, function(row) {
								selectedAttachments.push(row.data.id);
						   }, this);
						}
						dc.setParamValue("selectedAttachments",JSON.stringify(selectedAttachments));
	}
	
	,_onWdwwClose_: function(win) {
		
						var f = function(win) {
							var dc = this._getDc_("pricingBase");
							dc.doCancel(); 
							win.down("form")._isVisible_ = false; 
							dc._changeState_ = 0; 
						};
						Ext.defer(f, 300, this, [win]);
	}
	
	,_cancelPriceFix_: function() {
		
						var wdw = this._getWindow_("priceFixWdw");
						wdw.close();
	}
	
	,_cancelCompositePrice_: function() {
		
						var wdw = this._getWindow_("compositePriceWdw");
						wdw.close();
	}
	
	,_cancelIndexPrice_: function() {
		
		                var wdw = this._getWindow_("wdwPricing");
		                wdw.close();
	}
	
	,_cancelEditIndexPrice_: function() {
		
		                var wdw = this._getWindow_("wdwPricingEditFormula");
		                wdw.close();                
	}
	
	,_editFormulaPrice_: function() {
		
		                var pricingBase = this._getDc_("pricingBase");
		                var pbStore = pricingBase.store;
		 
		                var contractPriceCtgry = this._getDc_("contractPriceCtgry");
		                var pcRecord = contractPriceCtgry.getRecord();
		                var pbRecord = pricingBase.getRecord();
		                var pricingBaseId ="";
		 
		                if (pcRecord && pbRecord) {
		                    pricingBase._changeState_ = 2;
		                    pricingBaseId = pcRecord.get("pricingBaseId");
		                    pbStore.load({
		                        scope: this,
		                        callback: function(records, operation, success) {
		                            var index = pbStore.find("id", pricingBaseId); 
		                            pricingBase.setRecord(index);
									var window = this._getWindow_("wdwPricingEditFormula");
				                    window.show();                            
		                        }
		                    });                    
		                }         
	}
	
	,_disableDeleteIfIndex_: function() {
		
		                var dc = this._getDc_("contractPriceCtgry");
		                var rec = dc.getRecord();
		 
		                if (rec) {
		                    var priceCtgryName = rec.get("priceCtgryName");
							var priceCtgryPricePer = rec.get("priceCtgryPricePer");
		                    if (priceCtgryName === "Index") {
		                        dc.canDoDelete = false;
		                    } else {
		                        dc.canDoDelete = true;
		                    }
		                }
		 
		                dc.doReloadPage();
	}
	
	,_checkPriceCtgryName_: function() {
		
		                var dc = this._getDc_("contractPriceCtgry");
		                var rec = dc.getRecord();
		                var result = false;
		 
		                if (rec) {
							var priceCtgryName = rec.get("priceCtgryName");
							var priceCtgryPricePer = rec.get("priceCtgryPricePer");
							if (priceCtgryName !== "Index" && priceCtgryPricePer !== "Composite") {
								result = true;
							}					
						}
						return result;
	}
	
	,_checkPriceCtgryPricePer_: function() {
		
						var dc = this._getDc_("contractPriceCtgry");
						var rec = dc.getRecord();
						var result = false;
		
						if (rec) {
							var priceCtgryPricePer = rec.get("priceCtgryPricePer");
							if (priceCtgryPricePer !== "Composite") {
								result = true;
							}					
						}
						return result;
	}
	
	,_checkIfFormulaPriceAndNoDraft_: function() {
		
		                var dc = this._getDc_("contractPriceCtgry");
						var tenderBid = this._getDc_("tenderBid");
						var pricingBase = this._getDc_("pricingBase");
						
		                var rec = dc.getRecord();
						var bRec = tenderBid.getRecord();
		                var result = true;
		
		                if (rec && bRec) {
		                    var priceCtgryName = rec.get("priceCtgryName");
							var bidStatus = bRec.get("bidStatus");
		                    if (priceCtgryName === "Index") {
								if(bidStatus !== __CMM_TYPE__.BidStatus._DRAFT_){
									pricingBase.setReadOnly(true);
								}else{
									pricingBase.setReadOnly(false);
								}
		                    }else{
								pricingBase.setReadOnly(false);
								result = false;
							}
		                }
			
		                return result;
	}
	
	,_canAddPrice: function() {
		
						var dc = this._getDc_("tenderBid");
		                var rec = dc.getRecord();
		               	if (rec) {
							var avgMethodId = rec.get("avgMethodId");
							if(avgMethodId != null){
								return true;
							} else {
								Main.warning(Main.translate("applicationMsg","cannotAddPriceWarning__lbl"));
								return false;
							}			
						}
						
	}
	
	,_setDefaultPb: function() {
		
		 				if(this._canAddPrice()){
			                var dc = this._getDc_("pricingBase");
			                dc.doCancel(); // SONE-2144
							dc.canDoNew = true; // SONE-6893
			                dc.doNew({setIndexPrice: true});
						}                 
	}
	
	,_setDefaultFix: function() {
		
		 				if(this._canAddPrice()){
			                var dc = this._getDc_("pricingBase");
			                dc._step_ = 1;
			                dc.doCancel(); // SONE-2144
							dc.canDoNew = true; // SONE-6893
			                dc.doNew({setFixPrice: true});
		 				}
	}
	
	,_setDefaultDFT: function() {
		
		 				if(this._canAddPrice()){
			                var dc = this._getDc_("pricingBase");
			                dc._step_ = 2;
			                dc.doCancel(); // SONE-2144
							dc.canDoNew = true; // SONE-6893
			                dc.doNew({setDftPrice: true});
						} 
	}
	
	,_setDefaultComposite: function() {
		
		 				if(this._canAddPrice()){
			                var dc = this._getDc_("pricingBase");
			                dc._step_ = 3;
			                dc.doCancel(); // SONE-2144
							dc.canDoNew = true; // SONE-6893
			                dc.doNew({setCompositePrice: true});
						} 
	}
	
	,_checkPricingBaseId_: function() {
		
		 
		                var dc = this._getDc_("tenderBid");
		                var rec = dc.getRecord();
		                var result = false;
		 
		                if (rec) {
		                    var pricingBaseId = rec.get("pricingBaseId");
		                    if (Ext.isEmpty(pricingBaseId)) {
		                        result = true;
		                    }
		                }
		                return result;
	}
	
	,_filterEditAfterQuery_: function(params,callBackFn,ctx) {
		
						if( Ext.Ajax.isLoading() ){
							Ext.defer(this._filterEditAfterQuery_,250,this,[params, callBackFn, ctx]);
						} else {
							callBackFn(ctx);
						}
	}
	
	,_after_bid_was_generated_: function(params) {
		
						this._filterEditAfterQuery_(params,function(ctx) {
							var dc = ctx._getDc_("tenderBid");
							dc.doCancel();
							dc.doClearAllFilters();
							if (params.generatedBidId !== -1) {
								dc.setFilterValue("code", params.generatedBidCode);	
								dc.on("afterDoQuerySuccess", function(_dc){
								    _dc.doEditIn();
								}, this, {single : true});
								dc.doQuery();
							}
						}, this);        
	}
	
	,_after_bid_was_generated_from_receivers_: function() {
		
						var ctx = this;
						var dc = ctx._getDc_("tenderBid");
						dc.doCancel();
						dc.doClearAllFilters();
						dc.doQuery();
	}
	
	,_when_called_for_details_: function(params) {
		
		                this._filterEditAfterQuery_(params,function(ctx) {
							var dc = ctx._getDc_("tenderBid");
							dc.doClearAllFilters();
							dc.setFilterValue("code", params.contractCode);				
							dc.doQuery({showFilteredEdit: true});
						}, this);  
	}
	
	,_when_called_from_appMenu_: function(params) {
		
						var dc = this._getDc_("tenderBid");
						dc.doCancel();
						dc.doQuery({doNew: true});
	}
	
	,_when_do_new_: function(params) {
		
		                var dc = this._getDc_("tenderBid");
		                
		                var detailsTab = this._get_("detailsTab");
		                var paymentTerms = this._get_("paymentTerms");
		                detailsTab.setActiveTab(paymentTerms);
		 
		                this._showStackedViewElement_("main", "canvas2", dc.fireEvent("adjustDatePicker"));
						this.setContractTemplate(params);
	}
	
	,_when_do_new_form_list: function(params) {
		
		 				var dc = this._getDc_("tenderBid");
		                var detailsTab = this._get_("detailsTab");
		                var paymentTerms = this._get_("paymentTerms");
		                detailsTab.setActiveTab(paymentTerms);
		                this._showStackedViewElement_("main", "canvas2", dc.fireEvent("adjustDatePicker"));
						this.setSecondContractTemplate(params);
						
						var rec = dc.getRecord();
						if (rec) {
							var customerCode = rec.get("customerCode");
					
							this._setDefaultLovValue_("paymentTerms", "riskHolder", customerCode, ["id","type","parentGroupId"], ["riskHolderId","riskHolderType","parentGroupId"]);
							this._setDefaultLovValue_("paymentTerms", "billTo", customerCode, ["id","type"], ["billToId","billToType"]);
						}
	}
	
	,_afterViewAndFilters_: function() {
		
						var priceList = this._get_("priceCtgryList");
						var tenderBid = this._getDc_("tenderBid");
						var viewFilterToolbar = Ext.getCmp(priceList._viewFilterToolbarId_);
		
						tenderBid.on("recordChange", function() {
							var bidLists = this._get_("BidsList");
							var expander = bidLists.getPlugin("rowexpanderplus");
							expander.collapseLastRow();
						}, this);
		
						if (viewFilterToolbar) {
		
							var datePickerCmp = Ext.getCmp(viewFilterToolbar._datePickerId_);
			
							var adjustDate = function() {
								var r = tenderBid.getRecord();
								if (r) {
									var validFrom = r.get("validFrom");
									var validTo = r.get("validTo");
									var datePickerValue = datePickerCmp.getValue();
			
									if (datePickerValue < validFrom) {
										datePickerCmp.setValue(validFrom);
										Main.warning("The selected date is out of the bid period!");
									}
									else if (datePickerValue > validTo) {
										datePickerCmp.setValue(validTo);
										Main.warning("The selected date is out of the bid period!");
									}
								}
							}
			
							var adjustDateIfExpired = function() {
			
								var r = tenderBid.getRecord();
								if (r) {
									var status = r.get("status");
									var validTo = r.get("validTo");
									var currentDate = new Date();
			
									if (status  === "Expired") {
										datePickerCmp.setValue(validTo);
									}
									else {
										datePickerCmp.setValue(currentDate);
									}
								}
							}
			
							datePickerCmp.on("blur", function() {
								adjustDate();
							}, this);
			
							tenderBid.on("onAfterEditIn", function() {
								adjustDateIfExpired();
							}, this);
			
							tenderBid.on("adjustDatePicker", function() {
								adjustDateIfExpired();
							}, this);
						}				
	}
	
	,_setPriceCategoryAndPriceBaseState_: function() {
		
						var contractPriceCtgry = this._getDc_("contractPriceCtgry");
						var pricingBase = this._getDc_("pricingBase");
						var contractPriceRestr = this._getDc_("contractPriceRestr");
						var tenderBid = this._getDc_("tenderBid");
						
		                var rec = contractPriceCtgry.getRecord();
						var bRec = tenderBid.getRecord();
		 
		                if (rec && bRec) {
		                    var priceCtgryName = rec.get("priceCtgryName");
							var bidStatus = bRec.get("bidStatus");
							if(bidStatus !== __CMM_TYPE__.BidStatus._DRAFT_){
								contractPriceCtgry.setReadOnly(true);
								contractPriceRestr.setReadOnly(true);
								pricingBase.setReadOnly(true);
							}else{
								contractPriceCtgry.setReadOnly(false);
								contractPriceRestr.setReadOnly(false);
								pricingBase.setReadOnly(false);
							}
		                }
	}
	
	,setContractTemplate: function(params) {
		
						var dc = this._getDc_("tenderBid");
						dc.doQuery();
						dc.on("afterDoQuerySuccess", function() {
		
							dc.doNew({newFromCustomers: true});
							var record = dc.getRecord();
		
							if (record) {
								record.beginEdit();
								record.set("type", params.type);
								record.set("subType", params.subType);
								record.set("scope", params.scope);
								record.set("customerId", params.counterPartyId);
								record.set("customerCode", params.counterPartyCode);
								record.endEdit();
		
								this._setDefaultLovValue_("paymentTerms", "riskHolder", params.counterPartyCode, ["id","type","parentGroupId"], ["riskHolderId","riskHolderType","parentGroupId"]);
							}
		
							this.setDefaultValues();
						}, this);
	}
	
	,setSecondContractTemplate: function(params) {
		
						var dc = this._getDc_("tenderBid");
						var record = dc.getRecord();
							if (record) {
								record.beginEdit();
								record.set("type", params.type);
								record.set("subType", params.subType);
								record.set("scope", params.scope);
		
								record.set("resBuyerId", params.counterPartyId);
								record.set("resBuyerCode", params.counterPartyCode);
		
								record.set("customerId", params.customerId);
								record.set("customerCode", params.customerCode);
		
								record.endEdit();
							}
							if (Ext.isEmpty(dc._isCopyMode_)) {
								this.setDefaultValues();
							}
	}
	
	,setContractValue: function() {
		
							var header = this._get_("Header");
							var PbEdit = this._get_("PbEdit");
		
							var headerRecord =  header._controller_.getRecord();
							var headerCode = headerRecord.get("code");
							
							var pbRecord = PbEdit._controller_.getRecord();
							
							pbRecord.set("contractCode",headerCode);
							var pricingBaseDc = this._getDc_("pricingBase");
							var recNumber = pricingBaseDc.store.totalCount;
							if(recNumber === 0){
								pricingBaseDc.getRecord().set("validFrom", this._getDc_("tenderBid").getRecord().get("validFrom"));
							}
							pricingBaseDc.getRecord().set("validTo", this._getDc_("tenderBid").getRecord().get("validTo"));
							if(pbRecord.get("pricingMethod") === "Index"){
								PbEdit.fireEvent("itsTimeToCalculateDensity", PbEdit);
							}else{
								this.setDefaultDensity();
							}
	}
	
	,setPriceFields: function() {
		
		                    var pricingBase = this._getDc_("pricingBase");
		                    var contractRecord = this._getDc_("tenderBid").getRecord();
		                    var priceRecord = pricingBase.getRecord();
		 
		                    if (contractRecord && priceRecord) {
		 
		                        var financialSourceId = contractRecord.get("financialSourceId");
		                        var financialSourceCode = contractRecord.get("financialSourceCode");
		                        var avgMethodId = contractRecord.get("avgMethodId");
		                        var avgMethodName = contractRecord.get("avgMethodName");
		                        var period = contractRecord.get("exchangeRateOffset");
		                        var vat = contractRecord.get("vat");
		                        var pcCurr = contractRecord.get("settCurrId");
		                        var pcCurrCode = contractRecord.get("settCurrCode");
		                        var pcUnit = contractRecord.get("settUnitId");
		                        var pcUnitCode = contractRecord.get("settUnitCode");
		                        var validFrom = contractRecord.get("validFrom");
		                        var validTo = contractRecord.get("validTo");
		 
		                        priceRecord.beginEdit();
		 
		                        priceRecord.set("financialSourceId", financialSourceId);
		                        priceRecord.set("financialSourceCode", financialSourceCode);
		                        priceRecord.set("avgMethodIndicatorName", avgMethodName);
		                        priceRecord.set("avgMthdId", avgMethodId);
		                        priceRecord.set("exchangeRateOffset", period);
		                        priceRecord.set("vat", vat);
		                        priceRecord.set("pcCurr", pcCurr);
		                        priceRecord.set("pcCurrCode", pcCurrCode);
		                        priceRecord.set("pcUnit", pcUnit);
		                        priceRecord.set("pcUnitCode", pcUnitCode);
		                        priceRecord.set("validFrom", validFrom);
		                        priceRecord.set("validTo", validTo);
		                        priceRecord.endEdit();
		                    }
	}
	
	,_setSubsidiary_: function() {
		
						var ds = "fmbas_Subsidiary_Ds";
						var dc = this._getDc_("tenderBid");
						var r = dc.getRecord();
		
						Ext.Ajax.request({
			            	url: Main.dsAPI(ds, "json").read,
			            	method: "POST",
			            	scope: this,
			            	success: function(response) {
		
								var data = Ext.getResponseDataInJSON(response).data;
								var len = data.length;
								if (len === 1 && r) {
									var id = data[0].id;
									var code = data[0].code;
									r.set("holderId", id);
									r.set("holderCode", code);
								}
							}
						});
	}
	
	,_afterCopyContract_: function(dc,generatedContractId) {
		
						var detailsTab = this._get_("detailsTab");
						var paymentTerms = this._get_("paymentTerms");
						detailsTab.setActiveTab(paymentTerms);
						dc.doClearAllFilters();
						dc.setFilterValue("id", generatedContractId);
						dc.doQuery();
	}
	
	,_applyRestrictionState_: function() {
		
						var contractPriceCtgry = this._getDc_("contractPriceCtgry");
						var r = contractPriceCtgry.getRecord();
						var result = false;
					
						if (r) {
							var restriction = r.get("restriction");
							if (restriction === true) {
								result = true;
							}
						}
						return result;
	}
	
	,_afterDefineElements_: function() {
		
						var builder = this._getBuilder_();
						this._tiSourceTenderListSearchFieldId_ = Ext.id();
						this._tiSourceLocationListSearchFieldId_ = Ext.id();
						builder.change("tenderSelectList",this._injectSearchField_(this._tiSourceTenderListSearchFieldId_, "tenderSelectList"));
						builder.change("tenderLocationList",this._injectSearchField_(this._tiSourceLocationListSearchFieldId_, "tenderLocationList"));				
	}
	
	,_afterDefineDcs_: function() {
		
		                var tenderBid = this._getDc_("tenderBid");
						var shipToList = this._getDc_("bidAirlineVols");
		                var pricingBase = this._getDc_("pricingBase");
		                var contractPriceCtgry =  this._getDc_("contractPriceCtgry");
		                var contractPriceCmpn = this._getDc_("contractPriceCmpn");
						var bidAirlineVols = this._getDc_("bidAirlineVols");
						var contractPriceRestr = this._getDc_("contractPriceRestr");
						var tenderInviation = this._getDc_("tenderInviation");
						var tenderLocation = this._getDc_("tenderLocation");
		
						contractPriceCtgry.on("onAssignPriceCategory", function() { 
			                this.assignPrice();
			            } , this );
		
						contractPriceCtgry.on("onEditOut", function(dc) { 
			                dc.doReloadRecord();
			            } , this );
		
		                tenderBid.on("afterDoSaveSuccess", function (dc) {
		                    dc.doReloadRecord();
		
		                    this._applyStateAllButtons_();
							
							shipToList.doReloadPage();
		                    pricingBase.doReloadPage();
							contractPriceCtgry.doReloadPage();
		
							dc._isCopyMode_ = null;
		                }, this);
		
						contractPriceRestr.on("afterDoSaveSuccess", function () {
		        	        this._applyStateAllButtons_();
		        	    }, this);
		
						tenderBid.beforeDoSave = function () {
		
		                    var r = tenderBid.getRecord();
							var validTo = r.get("validTo");
							var validFrom = r.get("validFrom");
		
							if (validFrom > validTo) {
								Main.warning(Main.translate("applicationMsg","validFromValidToWarning__lbl"));
								return false;
							}
		
		                };
		
						tenderBid.on("afterDoCancel", function (dc, ajaxResult) {
							if (ajaxResult.reloadPage === true) {
								dc.doReloadPage({filterGeneratedContract : true, generatedContractId : ajaxResult.generatedContractId});
							}
		                    
		                }, this);
		
						tenderBid.on("afterDoReloadPageSuccess", function (dc, ajaxResult) {
							if (ajaxResult.options.filterGeneratedContract === true) {
								this._showStackedViewElement_("main", "canvas2", this._afterCopyContract_(dc, ajaxResult.options.generatedContractId));
							}
		                }, this);
		
						tenderBid.on("afterDoEditIn", function(dc) {
							var r = dc.getRecord();
							if (r) {		
								var hasTotalVolume = r.get("hasTotalVolume");
								var list = this._get_("ShipToEditList");					
								list.setVisible(!hasTotalVolume);
							}
						}, this);
		
						tenderBid.on("beforeDoDelete", function (dc) {
							var r = dc.getRecord();
							if (r) {
								var resaleRefId = r.get("resaleRefId");
								if (!Ext.isEmpty(resaleRefId)) {
									dc._controller_.commands.doDelete.confirmMessageBody = Main.translate("msg", "dc_confirm_delete_if_reference");
								}
								else {
									dc._controller_.commands.doDelete.confirmMessageBody = Main.translate("msg", "dc_confirm_delete_selection");
								}
							}
		                    
		                }, this);
		
						tenderBid.on("afterDoQuerySuccess", function(dc, ajaxResult) {
								if (ajaxResult.options.doNew === true) {
									this._showStackedViewElement_("main", "canvas1", dc.doNew());
								}
								if (ajaxResult.options.showFilteredEdit === true) {
									if (this._get_("main").hidden === true) {
										var f = function() {
											this._showStackedViewElement_("main", "canvas2", this._get_("main").show());
										}
										Ext.defer(f, 800, this);
									}
									else {
										this._showStackedViewElement_("main", "canvas2");
									}
								}
								if (ajaxResult.options.showEdit == true) {
									this._showStackedViewElement_("main", "canvas2");
								}
								
						}, this);
		
						contractPriceCtgry.on("afterDoSaveSuccess", function(dc) { 
		 
							dc.doReloadPage();
		                    var contractPriceRestrDC = this._getDc_("contractPriceRestr");
		                    var rec = dc.getRecord();
		 
		                    if (rec) {
		                        var restriction = rec.get("restriction");
		                        contractPriceRestrDC.setParamValue("isEnabled", restriction);
		                        contractPriceRestrDC.doReloadPage();
		                    }  
							tenderBid.doReloadRecord(); 
							              
		                }, this);
		
						contractPriceCtgry.on("afterDoQuerySuccess", function(dc, ajaxResult) { 
							if (ajaxResult.options.showFilteredEdit === true) {
								this._showStackedViewElement_("main", "canvas3", dc.noReset = false); // SONE-2733
							}
						}, this);
		
						contractPriceCtgry.on("onEditOut", function(dc) {
							var priceCtgryList = this._get_("priceCtgryList");
							var resetButton = Ext.getCmp(priceCtgryList._resetButtonId_);
							resetButton._resetGrid_();
							dc.doReloadPage();
						}, this); 
		
						tenderBid.on("onEditOut", function (dc) {
							var priceCtgryList = this._get_("priceCtgryList");
							var resetButton = Ext.getCmp(priceCtgryList._resetButtonId_);
							if(resetButton) {
								resetButton._resetGrid_();
							}
							pricingBase.doReloadRecord();
							var bidLists = this._get_("BidsList");
							var expander = bidLists.getPlugin("rowexpanderplus");
							expander.collapseLastRow();
		                }, this);
		 
		                contractPriceCtgry.on("afterDoDeleteSuccess", function() {
		                    pricingBase.doReloadPage(); //important
		 					tenderBid.doReloadRecord(); // SONE-2295					
		                } , this );
		 
		                pricingBase.on("itsTimeToCalculateDensity", function (dc,params) {
		                    var record = dc.getRecord();
					        var formName;
				            if (params && !Ext.isEmpty(params.formName)) {
				            	formName = params.formName;
				            }
		                    if (record) {
		                        var quotation = record.get("quotName");
		                        var quotUnitTypeInd = record.get("quotUnitTypeInd");
		                        var convUnitTypeInd = record.get("convUnitTypeInd");
		                        var convUnitCode = record.get("convUnitCode");
		                        if(!Ext.isEmpty(quotation) && quotUnitTypeInd !== convUnitTypeInd && !Ext.isEmpty(convUnitCode)){
		                            this.getDensity(pricingBase,formName);
		                        }
		                    }
		                }, this, {buffer: 200});
		 
		                pricingBase.on("OnEditIn", function (dc) {
		                   this.showWdwPricing();
		                                        
		                    var pbRecord = dc.getRecord();
		                    if(pbRecord.get("pricingMethod") === "Index"){
		                        dc.fireEvent("itsTimeToCalculateDensity", dc);
		                    }else{
		                        this.setDefaultDensity();
		                    } 
		                }, this);
		 
		                contractPriceCmpn.on("OnEditIn", function (dc) {
		                    this.showWdwPriceComp1();
		                }, this);
		
		                contractPriceCmpn.on("OnEditOut", function (dc) {
		                    contractPriceCtgry.doReloadPage();
		                    contractPriceCtgry.canDoDelete = true;
		                }, this);
		 
		                pricingBase.on("afterDoNew", function (dc, ajaxResult) {
		
		                    if (ajaxResult.setIndexPrice === true) {
		                        dc._changeState_ = 1;
								var newRec = dc.getRecord();                    	
								if (newRec) { 
		                        	newRec.beginEdit();                    
									newRec.set("mainCategoryCode", __FMBAS_TYPE__.PriceType._PRODUCT_);                        
		                        	newRec.endEdit();
		                    	}
		                        this.showPriceIndexWdw("PbEdit","priceCatName","Index","id","priceCatId");
		                    } 
		                    if (ajaxResult.setFixPrice === true) {
		 
		                        var opt = {
		                            description: "Fix price"
		                        }
		                        dc._changeState_ = 1;
								var newRec = dc.getRecord();                    	
								if (newRec) { 
		                        	newRec.beginEdit();                    
									newRec.set("mainCategoryCode", __FMBAS_TYPE__.PriceType._PRODUCT_);                        
		                        	newRec.endEdit();
		                    	}
		                        this.showPriceFixWdw("priceFixNew","priceCatName","Fixed","id","priceCatId", opt);
		                    } 
		                    if (ajaxResult.setDftPrice === true) {
		 
		                        var opt = {
		                            description: "Into Plane Service for low volume"
		                        }
		                        dc._changeState_ = 1;
		                        this.showPriceFixWdw("priceFixNew","priceCatName","","id","priceCatId", opt);
		                    }
							if (ajaxResult.setCompositePrice === true) {
		 
		                        var opt = {
		                            description: "Composite Price"
		                        }
		                        dc._changeState_ = 1;
		                        this.showCompositePriceWdw();
		                    }
		 
		                    this.setPriceFields();
		                    this._getDc_("pricingBase").getRecord().set("exchangeRateOffset", this._getDc_("tenderBid").getRecord().get("exchangeRateOffset"));
		 
		                }, this);
		 
		                pricingBase.on("afterDoDeleteSuccess", function () {
		                    contractPriceCtgry.doReloadPage();
							tenderBid.doReloadPage();
		                }, this);
		 
		                pricingBase.on("afterDoSaveSuccess", function (dc, ajaxResult) {
		 
		                    if (ajaxResult.options.options.doNewAfterSave === true){
		 
		                        if(dc._step_ === 1) {
		                             this._setDefaultFix();
		                        } else if (dc._step_ === 2 ) {
		                            this._setDefaultDFT();
		                        } else if (dc._step_ === 3) {
									 this._setDefaultComposite();
								}
		
		                        tenderBid.doReloadRecord();
		                        contractPriceCtgry.doReloadPage();
		                    } 
		                    if (ajaxResult.options.options.doCloseNewIndexPriceWindowAfterSave === true) {
		                        this._getWindow_("wdwPricing").close();
		                        tenderBid.doReloadRecord();
		                        contractPriceCtgry.doReloadPage();
		                    }
							if (ajaxResult.options.options.doCloseCompositePriceAfterSave === true) {
		                        this._getWindow_("compositePriceWdw").close();
		                        tenderBid.doReloadRecord();
		                        contractPriceCtgry.doReloadPage();
		                    }
		                    if (ajaxResult.options.options.doCloseNewWindowAfterSave === true){
		                        this._getWindow_("priceFixWdw").close();
		                        tenderBid.doReloadRecord();
		                        contractPriceCtgry.doReloadPage();
		                    } 
		                    if (ajaxResult.options.options.doCloseEditFormulaWindowAfterSave === true) {
		                        this._getWindow_("wdwPricingEditFormula").close();
		 
		                        tenderBid.doReloadRecord();
		                        contractPriceCtgry.doReloadPage();
		                    }
		 
		                    // Dan: SONE-2108 bug fixes
		 
		                    if(ajaxResult.options.options.doCloseEditAfterSave === true){
								this._getWindow_("priceFixWdw").close();
								var jsonData = Ext.getResponseDataInJSON(ajaxResult.response);
								var data = jsonData.data[0];
								var lastRecordId = data.id;
								contractPriceCtgry.doClearAllFilters();
								contractPriceCtgry.setFilterValue("pricingBaseId", lastRecordId);
								contractPriceCtgry.doQuery({showFilteredEdit: true});
							}
		 
		                    // End Dan
		
							if(ajaxResult.options.options.doCloseEditCompositeAfterSave === true){
		
								this._getWindow_("compositePriceWdw").close();
								var jd = Ext.getResponseDataInJSON(ajaxResult.response);
								var d = jd.data[0];
								var lastRecId = d.id;
		
								contractPriceCtgry.doClearAllFilters();
								contractPriceCtgry.setFilterValue("pricingBaseId", lastRecId);
								contractPriceCtgry.doQuery({showFilteredEdit: true});
							}
		                }, this);
		
						bidAirlineVols.on("afterDoNew", function (dc){
							var newRec = dc.getRecord();
							var tenderBid = this._getDc_("tenderBid");
		                    var contractRecord = tenderBid.getRecord();
							if (newRec && contractRecord) { 
		                        newRec.beginEdit(); 
		                        newRec.set("customerId", contractRecord.get("customerId"));
		                        newRec.set("customerCode", contractRecord.get("customerCode"));
								newRec.set("customerName", contractRecord.get("customerName"));
								newRec.set("validFrom", contractRecord.get("validFrom"));
		                        newRec.set("validTo", contractRecord.get("validTo"));
								dc.params.set("contractStatus",contractRecord.get("bidStatus"));
								dc.params.set("approvalStatus",contractRecord.get("bidApprovalStatus"));
								dc.params.set("tenderSource",contractRecord.get("tenderSource"));
		                        newRec.endEdit();
		                    }
						},this);
		
						bidAirlineVols.on("afterDoSaveSuccess", function (dc){					
							this._applyStateAllButtons_();
							this._getDc_("tenderBid").doReloadPage();                   					
						},this);
		
						bidAirlineVols.on("afterDoReloadPageSuccess", function (dc){
							var tenderBid = this._getDc_("tenderBid");
							tenderBid.doReloadRecord();				
						},this);
		
						bidAirlineVols.on("afterDoDeleteFailure", function() {
							tenderBid.doCancel();
							tenderBid.doReloadRecord();
						}, this);
		
						contractPriceCmpn.on("afterDoNew", function (dc){
		                    this.showWdwPriceComp();
		                    var number = dc.store.totalCount;
		                    var rec = dc.getRecord();
		                    var tenderBid = this._getDc_("tenderBid");
		                    var contractRecord = tenderBid.getRecord();
		                    
		                    if(number === 0){
		                        if (rec && contractRecord) {
		                            rec.set("validFrom", contractRecord.get("validFrom"));
		                        }
		                    }
		                    var dst = dc.getRecord();
		                    var src = this._getDc_("tenderBid").getRecord();
		 
		                    if (dst && src) {
		
								var priceCtgryPricePer = dst.get("priceCtgryPricePer");
		 
		                        dst.beginEdit();
		                        dst.set("validTo", src.get("validTo"));
		                        dst.set("currency", src.get("settCurrId"));             
		                        dst.set("currencyCode", src.get("settCurrCode"));
		
		                        if (priceCtgryPricePer !== __FMBAS_TYPE__.PriceInd._EVENT_) {
		                            dst.set("unit", src.get("settUnitId"));
		                            dst.set("unitCode", src.get("settUnitCode"));
		                        }
		                        else {
		                            this._setDefaultLovValue_("priceCompEdit", "unitEvent", "EA", "id", "unit");
		                        }
		                        if (priceCtgryPricePer !== __FMBAS_TYPE__.PriceInd._PERCENT_) {
		                            dst.set("unit", src.get("settUnitId"));
		                            dst.set("unitCode", src.get("settUnitCode"));
		                        }
		                        else {
		                            this._setDefaultLovValue_("priceCompEdit", "unitPercent", "%", "id", "unit");
		                        }
		                        dst.endEdit();
		                    }
		                }, this );
		 
		                
		                contractPriceCmpn.on("afterDoSaveSuccess", function (dc, ajaxResult) {
		                    var rec = dc.getRecord();
		                    if (rec) {
		                        var message = rec.get("toleranceMessage");
		                        if(!Ext.isEmpty(message)){
		                            Main.warning(message);
		                        }
		                    }                   
		                    
		                    if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
		                        this._getWindow_("priceCompWdw").close();
		                    }
		                    if (ajaxResult.options.options.doCloseNewWindowAfterSavee === true) {
		                        this._getWindow_("priceCompWdwEdit").close();
		                    }
							this._get_("priceCompList").store.load();
							this._get_("priceComponentConv").store.load();
		                }, this);
		
						pricingBase.mon(pricingBase.store, "update", function() {
								var view = this._get_("priceFixNew");
								if(view){
									var record = view._controller_.getRecord();
									view._applyStates_(record);
								}
						}, this);
		
		 				//********* Note **********//
		
						var note = this._getDc_("note");
			
						note.on("afterDoNew", function (dc) {
						    this.showNoteWdw();
						}, this);
			
						note.on("OnEditIn", function(dc) {
						   this._getWindow_("noteWdw").show();
						}, this);
						
						note.on("afterDoSaveSuccess", function(dc, ajaxResult) {
						    if (ajaxResult.options.options.doNewAfterSave === true) {
						        dc.doNew();
						    } else if (ajaxResult.options.options.doCloseNoteWindowAfterSave === true) {
						        this._getWindow_("noteWdw").close();
						    }
							dc.doReloadPage();
						}, this);
		
						tenderBid.on("dissableTenderListEvent", function(dc) {
							this._get_("tenderSelectList").setVisible(false);
							this._get_("tenderLocationList").setVisible(false);	
							this._getWindow_("wdwCopyLocationBid").setHeight(200);
						}, this);
		
						tenderBid.on("enableTenderListEvent", function(dc) {
							this._get_("tenderSelectList").setVisible(true);
							this._get_("tenderLocationList").setVisible(true);	
							this._getWindow_("wdwCopyLocationBid").setHeight(500);
						}, this);
		
	}
	
	,saveNewPriceCategory: function() {
		                
		                this.savePriceBases("New"); 
	}
	
	,saveClosePriceCategory: function() {
		              
		                this.savePriceBases("Close"); 
	}
	
	,saveEditPriceCategory: function() {
		               
		                this.savePriceBases("Edit"); 
	}
	
	,saveEditCompositePrice: function() {
		               
		                this.savePriceBases("EditComposite"); 
	}
	
	,saveClosePriceBase: function() {
		
		                var pb = this._getDc_("pricingBase");
		                pb.doSave({doCloseNewIndexPriceWindowAfterSave: true });
	}
	
	,saveCloseCompositePrice: function() {
		
		                var pb = this._getDc_("pricingBase");
		                pb.doSave({doCloseCompositePriceAfterSave: true });
	}
	
	,saveClosePriceEditFormulaBase: function() {
		
		                var pb = this._getDc_("pricingBase");
		                pb.doSave({doCloseEditFormulaWindowAfterSave: true });
	}
	
	,saveClosePriceComp: function() {
		
		                var pb = this._getDc_("contractPriceCmpn");
		                pb.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveClosePriceComp1: function() {
		
		                var pb = this._getDc_("contractPriceCmpn");
		                pb.doSave({doCloseNewWindowAfterSavee: true });
	}
	
	,showPriceIndexWdw: function(viewName,fieldToGet,valueToSet,fieldToReturn,fielToReturnValueIn) {
		
		                var window = this._getWindow_("wdwPricing");
		                window.show();
						
		                this._setDefaultLovValue_(viewName, fieldToGet, valueToSet, fieldToReturn, fielToReturnValueIn);
	}
	
	,showPriceFixWdw: function(viewName,fieldToGet,valueToSet,fieldToReturn,fielToReturnValueIn,opt) {
		
		                var window = this._getWindow_("priceFixWdw");
		                window.show();
		
						if (!Ext.isEmpty(valueToSet)) {
							this._setDefaultLovValue_(viewName, fieldToGet, valueToSet, fieldToReturn, fielToReturnValueIn, opt);
						}                
	}
	
	,showCompositePriceWdw: function() {
		
		                var window = this._getWindow_("compositePriceWdw");
		                window.show();
	}
	
	,_export_: function() {
		
						var mainGrid = this._get_("BidsList"); 
						mainGrid._doReport_();
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/BidsManagement.html";
					window.open( url, "SONE_Help");
	}
	
	,showWdwPriceComp: function() {
		
		            var win = this._getWindow_("priceCompWdw");
		            win.show(undefined, function() {
		                var dc = this._getDc_("contractPriceCmpn");
		                var rec = dc.getRecord();
		                if (rec) {
		                    var field = rec.get("contractPriceCatName");
		                    win.setTitle("Set a new price for "+field);
		                }
		                
		            },this);
	}
	
	,showWdwPriceComp1: function() {
		
		            var win = this._getWindow_("priceCompWdw");
		            win.show(undefined, function() {
		                var dc = this._getDc_("contractPriceCmpn");
		                var rec = dc.getRecord();
		                if (rec) {
		                    var field = rec.get("contractPriceCatName");
		                    win.setTitle("Change price for "+field);
		                }
		            },this);
	}
	
	,onWdwPricingShow: function() {
		
		                this._get_("PbEdit")._enableDisableFields_(null);
	}
	
	,onPriceCompWdwClose: function() {
		
		                var dc = this._getDc_("contractPriceCmpn");
		 
		                if(dc.isDirty()){
		                    dc.doCancel();
		                }
	}
	
	,getDensity: function(dc,formName) {
		
		
						var successFn = function() {	
							if (Ext.isEmpty(formName)){
								return;
							}			
							var view = this._get_(formName);
							var field = view._get_("conversionFactor");
							var editorForm = field._getFormInstance_();
							if( editorForm ){
								editorForm._validateInput_();
							}
						};
						var o={
							name:"changeUnit",
							showWorking : false,
							callbacks:{
								successFn: successFn,
								successScope: this,
								failureScope: this
							},
							modal:false
						};
						dc.doRpcData(o);
	}
	
	,canExportSingle: function(dc) {
		
						if(dc.record.data.bidStatus != __CMM_TYPE__.BidStatus._DRAFT_){
							if(_SYSTEMPARAMETERS_.sysbidapproval === "true" && dc.record.data.bidApprovalStatus != __CMM_TYPE__.BidApprovalStatus._APPROVED_){
								canExport = false;
							}
						}
		
						return true;
	}
	
	,deleteContractPriceCtgry: function() {
		
						this._getDc_("contractPriceCtgry").doDelete();
	}
	
	,deleteContractPriceComponent: function() {
		
						this._getDc_("contractPriceCmpn").doDelete();
	}
	
	,editContractPriceCtgry: function() {
		
						var contractPriceCtgry = this._getDc_("contractPriceCtgry");
						var contractPriceRestr = this._getDc_("contractPriceRestr");
						var tenderBid = this._getDc_("tenderBid");
						
		                var rec = contractPriceCtgry.getRecord();
						var bRec = tenderBid.getRecord();
		                if (rec && bRec) {
		                    var priceCtgryName = rec.get("priceCtgryName");
							var bidStatus = bRec.get("bidStatus");
		
							if(bidStatus !== __CMM_TYPE__.BidStatus._DRAFT_){
								contractPriceCtgry.setReadOnly(true);
								contractPriceRestr.setReadOnly(true);
							}else{
								contractPriceCtgry.setReadOnly(false);
								contractPriceRestr.setReadOnly(false);
							}
		                }
						this._showStackedViewElement_("main", "canvas3");
	}
	
	,updateContractPriceCtgry: function() {
		
						var dc = this._getDc_("contractPriceCmpn");
						var tenderBid = this._getDc_("tenderBid");
						
		                var rec = dc.getRecord();
						var bRec = tenderBid.getRecord();
		 
		                if (rec && bRec) {
		                    var priceCtgryName = rec.get("priceCtgryName");
							var bidStatus = bRec.get("bidStatus");
							if(bidStatus !== __CMM_TYPE__.BidStatus._DRAFT_){
								dc.setReadOnly(true);
							}else{
								dc.setReadOnly(false);
							}
		                }
		
						this._showStackedViewElement_("main", "canvas4", this._disableDeleteIfIndex_());
	}
	
	,deleteContractPricingBases: function() {
		
		                this._getDc_("pricingBase").doDelete();
	}
	
	,_setDefaultLovValue_: function(viewName,fieldToGet,valueToSet,fieldToReturn,fielToReturnValueIn,opt) {
		
		 
		                // -------------- Function config --------------
		 
		                var dcView = this._get_(viewName);
		 
		                // -------------- End function config --------------
		 
		                var field = dcView._get_(fieldToGet);
		                var valueField = field.valueField;
		                var record = dcView._controller_.getRecord();
		 
		                var store = field.store;
		
						
		
		                store.load({
		                    callback: function(rec) {
		                        var i = 0, l = rec.length;
								var valueToReturn = "";
		
								if (fieldToReturn instanceof Array) {
									valueToReturn = [];
								}                        
		 
		                        for (i ; i < l; i++) {
		                            var data = rec[i].data;
		                            for (var key in data) {
		                                if (data[valueField] == valueToSet) {
		
											if (fieldToReturn instanceof Array) {
												for (var x = 0; x<fieldToReturn.length; x++) {
													valueToReturn.push(data[fieldToReturn[x]]);
												}
											}
											else {
												valueToReturn = data[fieldToReturn];
											}
		                                    break;
											
		                                }
		                            }
		                        }
		
		                        if (record) {
		                            record.beginEdit();
		 
		                            record.set(field.dataIndex, valueToSet);
									if (fielToReturnValueIn instanceof Array) {
										for (var z = 0; z<fielToReturnValueIn.length; z++) {
											record.set(fielToReturnValueIn[z], valueToReturn[z]);
										}
									}
									else {
										record.set(fielToReturnValueIn, valueToReturn);
									}
		                            
		                            if (opt) {
		                                for (var key in opt) {
		                                    record.set(key, opt[key]); 
		                                }
		                            }
		                            record.endEdit();
		                        }
		                    }
		                },this);
	}
	
	,saveContractPriceCtgry: function(dc) {
		
		                if(dc.getRecord().get("defaultPriceCtgy")){
		                    this.priceCtgrySetDefault();
		                } else {
		                    dc.doSave();
		                }
	}
	
	,savePriceBases: function(command) {
		 
		                var dc = this._getDc_("pricingBase");
		                var r = dc.getRecord();
		                if (r) {
		                    if(r.get("defaultPriceCtgy")){
		                        this.priceBasesCPCSetDefault(command);
		                    } else {
		                        this.savePbWithParameter(command);
		                    }
		                }                
	}
	
	,priceBasesCPCSetDefault: function(command) {
		
		                var successFn = function(dc,response,serviceName,specs) {           
		                    this.pricingBaseSaveDefaultQueston (command);   
		                };
		                var o={
		                    name:"checkDefault",
		                    callbacks:{
		                        successFn: successFn,
		                        successScope: this
		                    },
		                    modal:true
		                };
		                this._getDc_("pricingBase").doRpcData(o);
	}
	
	,pricingBaseSaveDefaultQueston: function(command) {
		
		                var dc = this._getDc_("pricingBase");
		                        var isDefault = dc.params.get("isDefault");
		                        if(isDefault){
		                            Ext.Msg.show({
		                               title : "Warning",
		                               msg : Main.translate("applicationMsg", "pricingBaseSaveDefaultQueston__lbl"),
		                               buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
		                               fn : function(btnId) {
		                                    if( btnId === "yes" ){
		                                            dc.params.set("isDefault",false);
		                                            this.savePbWithParameter(command);
		                                    }
		                               },
		                               scope : this
		                            });
		                        }else{
		                            this.savePbWithParameter(command);
		                        }
	}
	
	,savePbWithParameter: function(command) {
		
		
		                var dc = this._getDc_("pricingBase");
		                if(command === "New"){
		                    dc.doSave({doNewAfterSave: true });
		                } else if(command === "Close"){
		                    dc.doSave({doCloseNewWindowAfterSave: true });
		                } else if( command === "Edit"){
		                    dc.doSave({doCloseEditAfterSave: true });
		                } else if( command === "EditComposite"){
		                    dc.doSave({doCloseEditCompositeAfterSave: true });
		                }
	}
	
	,canEditBid: function() {
		
						var dc = this._getDc_("tenderBid");
						var result = false;
						if(dc){
							var record = dc.getRecord();
							if (record) {
								var bidStatus = record.get("bidStatus");
								if (bidStatus == __CMM_TYPE__.BidStatus._DRAFT_ ){
									result = true;
								}
							}
						}
						return result;
	}
	
	,fnCancelSelected: function() {
		
						var dc = this._getDc_("bidAirlineVols");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAll: function() {
		
						var dc = this._getDc_("bidAirlineVols");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isCancelEnabled(dc);
	}
	
	,_finish: function() {
		
						var dc = this._getDc_("tenderBid");		
	}
	
	,saveCloseNote: function() {
		
					var note = this._getDc_("note");
					note.doSave({doCloseNoteWindowAfterSave: true });
	}
	
	,_showSaleContractDetails_: function(dc,response) {
		
		                           var bundle = "atraxo.mod.cmm";
		                           var frame = "atraxo.cmm.ui.extjs.frame.ContractCustomer_Ui";                            
		                                   
		                           if (getApplication().getFrameInstance(frame)) {
		                                   var mainPanel = getApplication().getFrameInstance(frame)._get_("main");
		                                   mainPanel.hide();
		                           }
		   
		                           getApplication().showFrame(frame,{
		                                   url:Main.buildUiPath(bundle, frame, false),
		                                   params: {
		                                           saleContractId: this._getDc_("tenderBid").getParamValue("generatedContractId")
		                                   },
		                                   callback: function (params) {
		                                           this._after_contract_was_generated_(params)
		                                   }
		                           });
		                           generateWindow.close();
	}
	
	,_injectSearchField_: function(id,gridName,fieldWidth,showResetBtn) {
		
						return { tbar: [{ 
								xtype: "textfield",
								id: id,
							    width: fieldWidth ? fieldWidth : 200,
							    emptyText: "Search for location code or name",
							    listeners: {
							        change: {
							            scope: this,
										buffer: 1000,
							            fn: function(field, newVal) {
		
							                var grid = this._get_(gridName);
							                
							                if (newVal && newVal != "*") {
												var af = [];
												var dateFilter = [];
												var dc = grid._controller_;
		
												for(i=0;i<grid.columns.length;i++){												
													var operation ="like";
													if(!grid.columns[i].dataIndex.includes("status") && !grid.columns[i].dataIndex.includes("Status") && !grid.columns[i].dataIndex.includes("round") && !grid.columns[i].dataIndex.includes("fuelProduct") && !grid.columns[i].dataIndex.includes("deliveryPoint") && !grid.columns[i].dataIndex.includes("taxType") && !grid.columns[i].dataIndex.includes("flightServiceType") ){
														var value1 = "%"+newVal+"%";	
														var o = {
															"id":null,
															"fieldName":grid.columns[i].dataIndex,
															"operation":operation,
															"value1": value1,
															"groupOp":"OR1"
														};
														if (grid.columns[i].hidden == false) {
															af.push(o);
														}
													}//tenderStatus
													if(grid.columns[i].dataIndex.includes("tenderStatus") && !grid.columns[i].dataIndex.includes("round")){
														var o = {
															"id":null,
															"fieldName":grid.columns[i].dataIndex,
															"operation":"=",
															"value1": "New",
															"groupOp":"OR1"
														};
														if (grid.columns[i].hidden == false) {
															af.push(o);
														}
													}											
												}
												dc.advancedFilter = af;
												dc.doQuery();
							                }
							            }
							        }
							    }
							},{
								glyph: "xf021@FontAwesome",
		                        xtype: "button",
								hidden: false,
								listeners: {
									click: {
										scope: this,
										fn: function() {
											var searchFieldId = Ext.getCmp(id);
											var grid = this._get_(gridName);
											var dc = grid._controller_;
											searchFieldId.setValue("");
		
											dc.advancedFilter = null;
											dc.doQuery();
										}
									}
								}}]}
	}
	
	,canCreateNewRevisionBid: function() {
		
						var dc = this._get_("Header");
						if(dc){
							var record = dc.getRecord();
							if (record) {
								var bidStatus = record.get("bidStatus");
								var transmissionStatus = record.get("bidTransmissionStatus");
		
								var bidStatusOk = ( bidStatus != __CMM_TYPE__.BidStatus._AWARDED_ && bidStatus != __CMM_TYPE__.BidStatus._DECLINED_
									&& bidStatus != __CMM_TYPE__.BidStatus._AWARD_ACCEPTED_ );
								var transmissionStatusOk = ( transmissionStatus === __CMM_TYPE__.TransmissionStatus._TRANSMITTED_ 
									|| transmissionStatus === __CMM_TYPE__.TransmissionStatus._RECEIVED_ );
								
								return bidStatusOk && transmissionStatusOk;
							}
						}
						return false;
	}
	
	,_showPriceEdit_: function(el,record) {
		
						var r = record.get("readOnly");
						if (r === false) {
							this.editContractPriceCtgry()
						}
	}
	
	,showInvalidBids: function(theWindow,params) {
		
						//variables
						var window = this._getWindow_(theWindow);
						var form  = this._get_("bidValidationsLabel");
						var succesfullySubmittedField = form._get_("succesfullySubmitedBidsLabel");
						var concurencyCheckFailedBidsField = form._get_("concurencyCheckFailedBidsLabel");
						var validationErrorsBidsField = form._get_("validationErrorsBidsLabel");
						var validationsList  = this._get_("bidValidationsList");
						var filterBidsButton = this._get_("btnCloseAndFilterBidValidationsWdw");
						var allBidsSubmittedSuccesfully = true;
		
						//reset windows
						concurencyCheckFailedBidsField.show();
						validationErrorsBidsField.show();
						validationsList.show();
						filterBidsButton.show();
						window.height = 400;
						window.width = 700;
						
						succesfullySubmittedField.setFieldLabel(params.submitForApprovalSuccesfullySubmitted);
		
						if(params.submitForApprovalConcurencyCheckFail) {
							concurencyCheckFailedBidsField.setFieldLabel(params.submitForApprovalConcurencyCheckFail);
							allBidsSubmittedSuccesfully = false;
						} else {
							concurencyCheckFailedBidsField.hide();
						}
		
						if(params.submitForApprovalInvalidBids) {
							validationErrorsBidsField.setFieldLabel(params.submitForApprovalInvalidBids);
							allBidsSubmittedSuccesfully = false;
						} else {
							validationErrorsBidsField.hide();
						}
		
						if (allBidsSubmittedSuccesfully) {
							validationsList.hide();
							filterBidsButton.hide();
							window.height = 150;
							window.width = 330;
						}
		
		
						var dc = this._getDc_("bidValidations");
						dc.doClearAllFilters();
						var af = [];
						var o = {
							"fieldName":"objectId",
							"id":null,
							"operation":"in",
							"value1": params.invalidBidIds
							};
						af.push(o);
						dc.advancedFilter = af;
						dc.doQuery();
		
						window.show();
	}
});
