Ext.override(atraxo.cmm.ui.extjs.frame.ContractCustomer_Ui, {

	_reloadContractEdit_ : null,
	
	_afterShowStackedViewElement_ : function(svn, idx) {
		if( idx === "canvas2" && this._reloadContractEdit_ === true ){
			this._reloadContractEdit_ = false;
			this._getDc_("contractEdit").doReloadPage();
		} else if( idx === "canvas1" ){
			this._getDc_("contractEdit").setParentCheckDisabled(false);
		}
	},
	
	_when_called_from_dshboard_ : function(id) {
		var dc = this._getDc_("contractList");
		dc.doCancel();
		dc.doClearAllFilters();
		dc.setFilterValue("id", id);				
		dc.doQuery({showEdit: true}); 
	},
	
	_setMainPriceCategory_ : function() {
			var dc = this._getDc_("contractPriceCtgryAssign");
			var dc2 = this._getDc_("contractPriceCtgry");
			var r = dc2.getRecord();
			if (r) {			
				dc.setParamValue("mainPriceCategoryId", r.get("id"));
			}
	},
	
	_clearMainPriceCategory_ : function() {
		var dc = this._getDc_("contractPriceCtgryAssign");
		dc.setParamValue("mainPriceCategoryId", null);
	},
	
	_enableIfConditions_ : function() {
		if (this.useWorkflowForApproval() === false) {
			return true;
		}
		else {
			if (this._checkIfContractAwaitingApproval_() === false || this._checkContractReadOnly_() === false) {
				return false;
			}
			else {
				return true;
			}
		}
	},
	
	_showEdit_ : function(el, record) {
		var s = record.get("status");
		var r = record.get("readOnly");
	
		if (s !== __CMM_TYPE__.ContractStatus._DRAFT_ || r === true) {
			this.openForView();
		}
		else if (r === false) {
			this.openContractForEdit()
		}
	},
		
	_showPriceEdit_ : function(el, record) {
		var r = record.get("readOnly");
		if (r === false) {
			this.editContractPriceCtgry()
		}
	},
	
	_goToStep2_ : function() {
		var w = this._getWindow_("wdwNewContract");
	
		var view = this._get_("newContractStep2");
		var awardedVolume = view._get_("awardedVolume");
		var dc = this._getDc_("newContract");
		var r = dc.getRecord();
		if (r) {
			awardedVolume.setValue(r.get("awardedVolume"));
		}
		else {
			awardedVolume.setValue(null);
		}
		
		view._enableContinue_();
	
		var ctx = this;
		var hideElems = ["btnDiscardStep1", "btnContinueStep1"];
		var showElems = ["btnBackStep2", "btnDiscardStep2", "btnContinueStep2"];
	
		Ext.each(hideElems, function(e) {
			ctx._get_(e).hide();
		}, this);
	
		Ext.each(showElems, function(e) {
			ctx._get_(e).show();
		}, this);				
	
		w.nextStep();
	},
	
	
	_goToStep4_ : function() {

		var w = this._getWindow_("wdwNewContract");
		var ctx = this;					
	
		var view = this._get_("newContractStep4");
		view._enableContinue_();
	
		var showElems = ["btnBackStep4", "btnDiscardStep4", "btnContinueStep4"];
		var hideElems = ["btnDiscardStep1", "btnContinueStep1","btnBackStep2", "btnDiscardStep2", "btnContinueStep2","btnBackStep3", "btnDiscardStep3", "btnContinueStep3"];
	
		Ext.each(hideElems, function(e) {
			ctx._get_(e).hide();
		}, this);
	
		Ext.each(showElems, function(e) {
			ctx._get_(e).show();
		}, this);		
	
		w.nextStep();
	},

	_goToStep5_ : function() {

		var w = this._getWindow_("wdwNewContract");
		var ctx = this;	
	
		var view = this._get_("newContractStep5");
		view._enableContinue_();
	
		var showElems = ["btnBackStep5", "btnDiscardStep5", "btnContinueStep5"];
		var hideElems = ["btnDiscardStep1", "btnContinueStep1","btnBackStep2", "btnDiscardStep2", "btnContinueStep2","btnBackStep3", "btnDiscardStep3", "btnContinueStep3","btnBackStep4", "btnDiscardStep4", "btnContinueStep4"];
	
		Ext.each(hideElems, function(e) {
			ctx._get_(e).hide();
		}, this);
	
		Ext.each(showElems, function(e) {
			ctx._get_(e).show();
		}, this);		
	
		w.nextStep();
	},
	
	_goToStep3_ : function() {

		var w = this._getWindow_("wdwNewContract");
		var ctx = this;	
	
		var view = this._get_("newContractStep3");
		view._enableContinue_();
	
		var showElems = ["btnBackStep3", "btnDiscardStep3", "btnContinueStep3"];
		var hideElems = ["btnDiscardStep1", "btnContinueStep1","btnBackStep2", "btnDiscardStep2", "btnContinueStep2"];
	
		Ext.each(hideElems, function(e) {
			ctx._get_(e).hide();
		}, this);
	
		Ext.each(showElems, function(e) {
			ctx._get_(e).show();
		}, this);
	
		w.nextStep();	
	},
	
	_goToStep1_ : function( finished ) {
		var w = this._getWindow_("wdwNewContract");
		var ctx = this;				
		var hideElems = ["btnBackStep2", "btnDiscardStep2", "btnContinueStep2","btnBackStep3", "btnDiscardStep3", "btnContinueStep3","btnBackStep4", "btnDiscardStep4", "btnContinueStep4","btnBackStep5", "btnDiscardStep5", "btnContinueStep5"];
		var showElems = ["btnDiscardStep1", "btnContinueStep1"];
	
		Ext.each(hideElems, function(e) {
			ctx._get_(e).hide();
		}, this);
	
		Ext.each(showElems, function(e) {
			ctx._get_(e).show();
		}, this);
	
		if (!finished) {
			w.prevStep();
		}
		
	},
	
	_onNewContractWdwdClose_ : function() {
		var dc = this._getDc_("newContract");
		dc.doCancel();
		dc.clearEditMode();
		this._goToStep1_(true);
	},
	
	_createNewContract_ : function( type , subType , scope ) {
		var dc = this._getDc_("newContract");
		dc.doNew({
			contractTemplate: {
				type : type,
				subType : subType,
				scope : scope,
				dealType : __CMM_TYPE__.DealType._SELL_
			}
		});
	},
	
	_checkPercent_ : function() {
		var dc = this._getDc_("contractPriceCtgry");
		var r = dc.getRecord();
		var result = true;
		if (r) {
			var priceCtgryName = r.get("priceCtgryName");
			if (priceCtgryName === __CMM_TYPE__.PricingType._INDEX_) {
				result = false;
			}
		}
		return result;
	},
	
	_onShow_from_externalInterfaceMessageHistory_ : function( params ) {	
		var dc = this._getDc_("contractEdit");
		dc.setFilterValue("id", params.objectId);
		dc.doQuery({queryFromExternalInterface: true});
	},
	
	_markSelectedPriceCategories_ : function() {
		var view = this._get_("priceCtgryAssignList");
		var form = this._get_("priceCtgryEdit");
		var percentageOf = form._get_("percentageOf");
		var win = this._getWindow_("priceCategoryAsgnWdw");
	
		var f = function() {
			if (win._isInEditForm_ === true) {	
				view.store.on("load", function(s) {	
					if (!Ext.isEmpty(percentageOf.getRawValue())) {	
						var percentageOfArray = percentageOf.getRawValue().split(",");
						s.each(function(r) {
							if (percentageOfArray.indexOf(r.get("name")) !== -1 ) {
								view.getSelectionModel().select(s.indexOf(r), true);
							}
						}, this);
					}
				}, this);
			}
			else {
				view.getSelectionModel().deselectAll();
			}
		}
		Ext.defer(f, 250, this);
	},
	
	_excludeComposite_ : function( list ) {
		var s = list.store;
		s.filterBy(function (record) {
	       if (record.get("priceCtgryPricePer") !== __FMBAS_TYPE__.PriceInd._COMPOSITE_ && record.get("priceCtgryPricePer") !== __FMBAS_TYPE__.PriceInd._EVENT_) return record;
	    });
		
	},
	
	_checkIfContractAwaitingApproval_ : function() {
		var dc = this._getDc_("contractEdit");
		var rec = dc.getRecord();
		if (rec) {
			var status = dc.getRecord().get("approvalStatus");
			var statusPrice = dc.getRecord().get("priceApprovalStatus");
			var statusShipTo = dc.getRecord().get("shipToApprovalStatus");
			var statusPeriod = dc.getRecord().get("periodApprovalStatus");
			if ( status === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_ 
				|| statusPrice === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_ 
					|| statusShipTo === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_ 
						|| statusPeriod === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_ ) {
				return false;
			}		
		}
		return true;
	},
	
	_checkContractReadOnly_ : function() {
		var dc = this._getDc_("contractEdit");
		var rec = dc.getRecord();
		if (rec) {
			var readOnly = rec.get("readOnly");
			if (readOnly === true) {
				return false;
			}					
		}
		return true;
	},
	
	_onWdwwClose_ : function( win ) {
		var f = function(win) {
			var dc = this._getDc_("pricingBase");
			dc.doCancel(); 
			win.down("form")._isVisible_ = false; 
			dc._changeState_ = 0; 
		};
		Ext.defer(f, 300, this, [win]);
	},	
	
	// Dan: bug fix: index prices can be deleted
	_disableDeleteIfIndex_ : function() {
        var dc = this._getDc_("contractPriceCtgry");
        var rec = dc.getRecord();

        if (rec) {
            var priceCtgryName = rec.get("priceCtgryName");
            if (priceCtgryName === "Index") {
                dc.canDoDelete = false;
            } else {
                dc.canDoDelete = true;
            }
        }

        dc.doReloadPage();
		this._applyStateAllButtons_();

	},
	
	_checkPriceCtgryName_ : function() {
	    var dc = this._getDc_("contractPriceCtgry");
	    var rec = dc.getRecord();
	    var result = false;
	
	    if (rec) {
			var priceCtgryName = rec.get("priceCtgryName");
			var priceCtgryPricePer = rec.get("priceCtgryPricePer");
			if (priceCtgryName !== "Index" && priceCtgryPricePer !== "Composite") {
				result = true;
			}					
		}
		return result;
	},
	
	_checkPriceCtgryPricePer_ : function() {
		var dc = this._getDc_("contractPriceCtgry");
		var rec = dc.getRecord();
		
		if (rec) {
			var priceCtgryPricePer = rec.get("priceCtgryPricePer");
			if (priceCtgryPricePer !== "Composite") {
				return true;
			}					
		}
		return false;
	},
	
	_checkIfFormulaPrice_ : function() {
	    var dc = this._getDc_("contractPriceCtgry");
	    var rec = dc.getRecord();
	    var result = true;
	
	    if (rec) {
	        var priceCtgryName = rec.get("priceCtgryName");
	        if (priceCtgryName !== "Index") {
	            result = false;
	        }
	    }
	    return result;
	},
	
	_checkPricingBaseId_ : function() {
	    var dc = this._getDc_("contractEdit");
	    var rec = dc.getRecord();
	    var result = false;
	
	    if (rec) {
	        var pricingBaseId = rec.get("pricingBaseId");
	        if (Ext.isEmpty(pricingBaseId)) {
	            result = true;
	        }
	    }
	    return result;
	},

	// SONE-3452: Start Data not displayed when editing a sales contract
	_filterEditAfterQuery_ : function( params , callBackFn , ctx ) {
		if( Ext.Ajax.isLoading() ){
			Ext.defer(this._filterEditAfterQuery_,250,this,[params, callBackFn, ctx]);
		} else {
			var dc = ctx._getDc_("contractEdit");
			dc.doCancel();
			dc.doClearAllFilters();
			dc.setFilterValue("id", params.saleContractId);
			dc.doQuery({showFilteredEdit: true});
		}
	},

	// SONE-3452: End Data not displayed when editing a sales contract
	_after_contract_was_generated_ : function( params ) {
		var dc = this._getDc_("contractEdit");
		var priceWindows = ["priceFixWdw","wdwPricing","compositePriceWdw"];
		Ext.each(priceWindows, function(w) {
			this._getWindow_(w).close();
		}, this);
		this.openForView(dc,params.saleContractId,params.internalResale);
	},

	_when_called_for_details_ : function( params ) {
        this._filterEditAfterQuery_(params,function(ctx) {
			var dc = ctx._getDc_("contractEdit");
			dc.doClearAllFilters();
			dc.setFilterValue("code", params.contractCode);				
			dc.doQuery({showFilteredEdit: true});
		}, this);  
	},

	_when_called_for_details_edit : function( params ) {
		var dc = this._getDc_("contractList");
			dc.doClearAllFilters();
			dc.setFilterValue("code", params.contractCode);				
			dc.doQuery({openForView:true});                   
	},

	_when_called_from_appMenu_ : function() {
		var dc = this._getDc_("contractEdit");
		dc.doCancel();
		dc.doQuery({doNew: true});
	},

	_when_do_new_ : function( params ) {
		var dc = this._getDc_("contractEdit");
            
           // Dan: SONE-1632
           var detailsTab = this._get_("detailsTab");
           var paymentTerms = this._get_("paymentTerms");
               detailsTab.setActiveTab(paymentTerms);
 
               this._showStackedViewElement_("main", "canvas2", dc.fireEvent("adjustDatePicker"));
		dc._isNewFromCustomers_ = true;
		this.setContractTemplate(params);
	},

	_when_do_new_form_list : function( params ) {
		var dc = this._getDc_("contractEdit");
        var detailsTab = this._get_("detailsTab");
        var paymentTerms = this._get_("paymentTerms");
        detailsTab.setActiveTab(paymentTerms);
        this._showStackedViewElement_("main", "canvas2", dc.fireEvent("adjustDatePicker"));
		this.setSecondContractTemplate(params);
		
		var rec = dc.getRecord();
		if (rec) {
			var customerCode = rec.get("customerCode");
			this._setDefaultLovValue_("paymentTerms", "riskHolder", customerCode, ["id","type","parentGroupId"], ["riskHolderId","riskHolderType","parentGroupId"]);
			this._setDefaultLovValue_("paymentTerms", "billTo", customerCode, ["id","type"], ["billToId","billToType"]);
		}
	},
	
	setContractTemplate : function( params ) {
		var dc = this._getDc_("contractEdit");
		dc.doQuery();
		dc.on("afterDoQuerySuccess", function() {

			if (dc._isNewFromCustomers_ === true) {
				// Dan: SONE-2114:  waith for the doQuery() action to finish than call doNew()

				dc.doNew({newFromCustomers: true});
				var record = dc.getRecord();

				if (record) {
					record.beginEdit();
					record.set("type", params.type);
					record.set("subType", params.subType);
					record.set("scope", params.scope);
					record.set("customerId", params.counterPartyId);
					record.set("customerCode", params.counterPartyCode);
					record.set("customerParentId", params.parentId);
					record.endEdit();

					this._setDefaultLovValue_("paymentTerms", "riskHolder", params.counterPartyCode, ["id","type","parentGroupId"], ["riskHolderId","riskHolderType","parentGroupId"]);
 					this._setDefaultLovValue_("paymentTerms", "billTo", params.counterPartyCode, ["id","type"], ["billToId","billToType"]);
				}

				this.setDefaultValues();					}					
		}, this);	
	},
	
	setSecondContractTemplate : function( params ) {
		var dc = this._getDc_("contractEdit");					
		var record = dc.getRecord();
			if (record) {
				record.beginEdit();
				record.set("type", params.type);
				record.set("subType", params.subType);
				record.set("scope", params.scope);
	
				record.set("resBuyerId", params.counterPartyId);
				record.set("resBuyerCode", params.counterPartyCode);
	
				record.set("customerId", params.customerId);
				record.set("customerCode", params.customerCode);
				record.set("customerParentId", params.parentId);
	
				record.endEdit();
			}
			if (Ext.isEmpty(dc._isCopyMode_)) {
				this.setDefaultValues();
			}					
	},
	
	setNewDefaultValues : function() {
		var dc = this._getDc_("newContract");
		var r = dc.getRecord();
		var ds = "cmm_ContractCustomerWizard_Ds";
		Ext.Ajax.request({
	    	url: Main.dsAPI(ds, "json").service,
	    	method: "POST",
	    	scope: this,
	    	params : {
	    		rpcName: "setDefaulPaymentTerms",
	    		rpcType: "data",
				data : Ext.encode({
					customerId: r ? r.get("customerId") : null
				})
			},
	    	success: function(response) {
				var params = Ext.getResponseDataInJSON(response).params;
				if (r) {
					r.beginEdit();
						r.set("paymentTerms",params.paymentTerms);
						r.set("paymentRefDay",params.referenceTo);
						if (!Ext.isEmpty(params.creditTerms)) {
							r.set("creditTerms",params.creditTerms);
						}
						if (!Ext.isEmpty(params.currencyID)) {
							r.set("settlementCurrId",params.currencyID);
							r.set("settlementCurrCode",params.currencyCode);
						}
						r.set("invoiceFreq",params.invFreq);
						r.set("averageMethodId",params.averageMethodId);
						r.set("averageMethodName",params.averageMethodName);
						r.set("financialSourceId",params.financialSourceId);
						r.set("financialSourceCode",params.financialSourceCode);
						r.set("exchangeRateOffset",params.period);
					r.endEdit();						
				}
			}
	    		
		});
	},
	
	setContractValue : function() {
			var header = this._get_("Header");
			var PbEdit = this._get_("PbEdit");
	
			var headerRecord =  header._controller_.getRecord();
			var headerCode = headerRecord.get("code");
			
			var pbRecord = PbEdit._controller_.getRecord();
			
			pbRecord.set("contractCode",headerCode);
			var pricingBaseDc = this._getDc_("pricingBase");
			var recNumber = pricingBaseDc.store.totalCount;
			if(recNumber === 0){
				pricingBaseDc.getRecord().set("validFrom", this._getDc_("contractEdit").getRecord().get("validFrom"));
			}
			pricingBaseDc.getRecord().set("validTo", this._getDc_("contractEdit").getRecord().get("validTo"));
			if(pbRecord.get("pricingMethod") === "Index"){
				PbEdit.fireEvent("itsTimeToCalculateDensity", PbEdit);
			}else{
				this.setDefaultDensity(false, false, pricingBaseDc);
			}
	},
	
	setPriceFields : function() {
	        // TODO set price fields
	        var pricingBase = this._getDc_("pricingBase");
	        var contractRecord = this._getDc_("contractEdit").getRecord();
	        var priceRecord = pricingBase.getRecord();
	
	        if (contractRecord && priceRecord) {
	
	            var financialSourceId = contractRecord.get("financialSourceId");
	            var financialSourceCode = contractRecord.get("financialSourceCode");
	            var avgMethodId = contractRecord.get("avgMethodId");
	            var avgMethodName = contractRecord.get("avgMethodName");
	            var period = contractRecord.get("exchangeRateOffset");
	            var vat = contractRecord.get("vat");
	            var pcCurr = contractRecord.get("settCurrId");
	            var pcCurrCode = contractRecord.get("settCurrCode");
	            var pcUnit = contractRecord.get("settUnitId");
	            var pcUnitCode = contractRecord.get("settUnitCode");
	            var validFrom = contractRecord.get("validFrom");
	            var validTo = contractRecord.get("validTo");
	
	            priceRecord.beginEdit();
	
//	            priceRecord.set("financialSourceId", financialSourceId);
//	            priceRecord.set("financialSourceCode", financialSourceCode);
	            priceRecord.set("avgMethodIndicatorName", avgMethodName);
	            priceRecord.set("avgMthdId", avgMethodId);
	            priceRecord.set("exchangeRateOffset", period);
	            priceRecord.set("vat", vat);
	            priceRecord.set("pcCurr", pcCurr);
	            priceRecord.set("pcCurrCode", pcCurrCode);
	            priceRecord.set("pcUnit", pcUnit);
	            priceRecord.set("pcUnitCode", pcUnitCode);
	            priceRecord.set("validFrom", validFrom);
	            priceRecord.set("validTo", validTo);
	            priceRecord.endEdit();
	        } 
	},
	
	_setSubsidiary_ : function() {
		var ds = "fmbas_Subsidiary_Ds";
		var dc = this._getDc_("contractEdit");
		var r = dc.getRecord();
	
		Ext.Ajax.request({
	    	url: Main.dsAPI(ds, "json").read,
	    	method: "POST",
	    	scope: this,
	    	success: function(response) {
	
				var data = Ext.getResponseDataInJSON(response).data;
				var len = data.length;
				if (len === 1 && r) {
					var id = data[0].id;
					var code = data[0].code;
					r.set("holderId", id);
					r.set("holderCode", code);
				}
			}
		});
	},
	
	_enableDisableChildrenDcs_ : function(flag) {
		var childrenDcs = ["contractEdit","pricingBase","contractPriceCtgry","shipTo","contractPriceCmpn","contractPriceRestr","contractPriceCmpnConv","contractPriceCmpnClassic"];
		Ext.each(childrenDcs, function(dc) {
			this._getDc_(dc).setReadOnly(flag);
		}, this);
	},
	
	_enableDisableFieldsIfViewContract_ : function(flag) {
		var ctx =  this;
		var paymentTerms = ctx._get_("paymentTerms");
		var header = ctx._get_("Header");
		var paymentTermsFields = ["invoiceType","invoiceTemplate"];
		var headerFields = ["hardCopy","contacts","respBuyer"];
		Ext.each(paymentTermsFields, function(pField) {
			var thePaymentfield = paymentTerms._get_(pField);
			thePaymentfield._readOnlyOnView_ = flag;
		},this);
		Ext.each(headerFields, function(hField) {
			var theHeaderField = header._get_(hField);
			theHeaderField._readOnlyOnView_ = flag;
		},this);
	},
	
	openForView : function(context,generatedContractId,internalResale) {
		var contract = this._getDc_("contractList");
		var contractEdit = this._getDc_("contractEdit");
		var dcs
		if (context) {
			contract = context;
		}
		var r = contract.getRecord();
		var ctx = this;
		var afterViewFn = function() {
			ctx._disableDeleteIfIndex_();
		}
		
		if (r && !generatedContractId) {
			var contractId = r.get("id");
			var readOnly = r.get("readOnly");
			// List of children dcs that have to be disabled if the contract can't be edited
			

			// Start logic inherited from previous function	
			
			this.makeAttachmentsReadOnly(true);
			contractEdit.setParentCheckDisabled(true);
			contractEdit.setFilterValue("id",contractId);
			contractEdit.doQuery();
			this._showStackedViewElement_("main", "canvas2", afterViewFn());	
			
			// End logic inherited from the previous function
			
			// A contract which has the readOnly flag set to true is an internal resale contract
			// A contract is not editable if it's part of an internal resale
			
			if (readOnly === true) {
				this._enableDisableChildrenDcs_(true);
			}
			
		}
		
		// Start logic inherited from previous function
		
		if (generatedContractId && context) {
			context.setFilterValue("id",generatedContractId);
			context.setParentCheckDisabled(true);
			var f = function() {
				context.doQuery({showFilteredEdit: true, internalResale: true});
			}
			Ext.defer(f,800,this);
		}
		this._enableDisableFieldsIfViewContract_(false);
		
		// End logic inherited from the previous function
		
	},
		
	_setPriceCmpReadOnlyOrNot_ : function() {
		var dc = this._getDc_("contractPriceCmpnClassic");
		if ((this.isDraft() || this.isBlueprint()) && this._checkIfContractAwaitingApproval_() && this._checkContractReadOnly_()) {
			dc.setReadOnly(false);
		}
		else {
			dc.setReadOnly(true);
		}
	},
	
	_checkReadOnly_ : function(isInternalResale) {
		var contract = this._getDc_("contractEdit");
		var record = contract.getRecord();
		if(record) {
			var readOnly = record.get("readOnly");
			if(readOnly === true) {
				this._enableDisableChildrenDcs_(true);
			}
			else {
				this._enableDisableChildrenDcs_(false);
			}
		}
	},
	
	_refreshContractReadOnly_ : function() {
		var contract = this._getDc_("contractEdit");
		var record = contract.getRecord();
		if(!record) {
			return;
		}
		var isDraft = record.get("status") === __CMM_TYPE__.ContractStatus._DRAFT_; 
		var readonly = true;		
		if (isDraft){
			var awaitingApproval = record.get("approvalStatus") === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_;
			readonly = awaitingApproval;
		}
		else{
			var isBlueprint = record.get("isBlueprint");
			if (!isBlueprint) {
				readonly = true;
			}
			else {
				var priceAwaitingApproval = record.get("priceApprovalStatus") === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_;
				var periodAwaitingApproval = record.get("periodApprovalStatus") === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_;
				var shipToAwaitingApproval = record.get("shipToApprovalStatus") === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_;
				readonly = priceAwaitingApproval || periodAwaitingApproval || shipToAwaitingApproval;
			}
		}
		this._enableDisableChildrenDcs_(readonly);
	},
	
	_afterCopyContract_ : function( dc , generatedContractId ) {
		var contractList = this._getDc_("contractList");
		contractList.doCancel();
		contractList.doClearAllFilters();
		contractList.setFilterValue("id", generatedContractId);
		contractList.doQuery({showEdit:true});
		var detailsTab = this._get_("detailsTab");
		var paymentTerms = this._get_("paymentTerms");
		detailsTab.setActiveTab(paymentTerms);
	},
	
	_applyRestrictionState_ : function() {
		var contractPriceCtgry = this._getDc_("contractPriceCtgry");
		var r = contractPriceCtgry.getRecord();
		var result = false;
	
		if (r) {
			var restriction = r.get("restriction");
			if (restriction == true) {
				result	= true;
			}
		}
		return result;
	},
	
	_afterLinkElementsPhase2_ : function() {
		var contract = this._getDc_("contractEdit");
		this._onContractRecordChange_(contract);
	},
	
	_onContractRecordChange_ : function( contract ) {
	    var r = contract.getRecord();
		if (r) {
			var view = this._get_("Header");
			if( r && view ) {
				var customer = view._get_("customer");
				if(customer && customer.inputEl){
					var status = r.get("status");
	
					// Start SONE-3918
					
					var customerName = r.get("customerName");
					var toolTip = Ext.getCmp(this.customerCodeTooltipId);
	
					if (toolTip) {
						toolTip.destroy();
						}
	
					customer.inputEl.dom.removeAttribute("data-qtip");
	
					Ext.create("Ext.tip.ToolTip", {
						id: this.customerCodeTooltipId,
				        target: customer.getEl(),
				        html: customerName
				    });
	
					// End SONE-3918
	
					var btnDeleteContract = this._get_("btnDeleteContract");
					if (status === __CMM_TYPE__.ContractStatus._EFFECTIVE_ || status === __CMM_TYPE__.ContractStatus._EXPIRED_) {
						contract.canDoDelete = false;
						btnDeleteContract._disable_();
						contract.updateDcState();	
					}
					else {
						contract.canDoDelete = true;
						btnDeleteContract._enable_();
						contract.updateDcState();	
					}
				}
			}
		}
	},
	
	_canDeleteContract_ : function() {
		var contract = this._getDc_("contractList");
	    var r = contract.getRecord();
		var result = true;
		if (r) {
			var status = r.get("status");
			var readOnly = r.get("readOnly");
			if (status === __CMM_TYPE__.ContractStatus._EFFECTIVE_ || status === __CMM_TYPE__.ContractStatus._EXPIRED_ || readOnly === true) {
				result = false;	
			}
			else {
				result = true;	
			}
		}
		return result;
	},
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	_showHidePeriodAlert_ : function() {
		var p = this._get_("PeriodAlert");
		var dc = this._getDc_("contractEdit");
		var r = dc.getRecord();
	
		if (r && p) {
			var periodAlertMessage = r.get("periodAlertMessage");
			if (!Ext.isEmpty(periodAlertMessage)) {
				p.show();
			}
			else {
				p.hide();
			}
		}
	},
	
	_showHideShipToAlert_ : function() {
		var p = this._get_("ShipToAlert");
		var dc = this._getDc_("contractEdit");
		var r = dc.getRecord();
	
		if (r && p) {
			var shipToAlertMessage = r.get("shipToAlertMessage");
			if (!Ext.isEmpty(shipToAlertMessage)) {
				p.show();
			}
			else {
				p.hide();
			}
		}
	},
	
	_showHidePriceAlert_ : function() {
		var p = this._get_("PriceAlert");
		var dc = this._getDc_("contractEdit");
		var r = dc.getRecord();
	
		if (r && p) {
			var priceAlertMessage = r.get("priceAlertMessage");
			if (!Ext.isEmpty(priceAlertMessage)) {
				p.show();
			}
			else {
				p.hide();
			}
		}
	},
	
	showPriceIndexWdw : function( viewName , fieldToGet , valueToSet , fieldToReturn , fielToReturnValueIn ) {
	    var window = this._getWindow_("wdwPricing");
	    window.show();
		
	    this._setDefaultLovValue_(viewName, fieldToGet, valueToSet, fieldToReturn, fielToReturnValueIn);
	},
	
	showPriceFixWdw : function( viewName , fieldToGet , valueToSet , fieldToReturn , fielToReturnValueIn , opt) {
	    var window = this._getWindow_("priceFixWdw");
	    window.show();
	
		if (!Ext.isEmpty(valueToSet)) {
			this._setDefaultLovValue_(viewName, fieldToGet, valueToSet, fieldToReturn, fielToReturnValueIn, opt);
		}
	    
	},
	
	showWdwPriceComp : function() {
	    var win = this._getWindow_("priceCompWdw");
	    win.show(undefined, function() {
	        var dc = this._getDc_("contractPriceCmpnClassic");
	        var rec = dc.getRecord();
	        if (rec) {
	            var field = rec.get("contractPriceCatName");
	            win.setTitle("Set a new price for "+field);
	        }
	        
	    },this);
	},
	
	showWdwPriceComp1 : function() {
	    var win = this._getWindow_("priceCompWdw");
	    win.show(undefined, function() {
	        var dc = this._getDc_("contractPriceCmpn");
	        var rec = dc.getRecord();
	        if (rec) {
	            var field = rec.get("contractPriceCatName");
	            win.setTitle("Change price for "+field);
	        }
	    },this);
	},
	
	onWdwPricingShow : function() {
	    this._get_("PbEdit")._enableDisableFields_(null);
	},
	
	setDefaultDensity : function(callValidate,updateRecords,dc,formName) {
		var view = this._get_(formName);
		if (Ext.isEmpty(view)) {
			return;
		}
		
		var field = view._get_("conversionFactor");
		var editorForm = field._getFormInstance_();
        editorForm._validateInput_();
	},
	
	_setDefaultLovValue_ : function( viewName , fieldToGet , valueToSet , fieldToReturn , fielToReturnValueIn ,opt )  {
 
            	// -------------- Function config --------------
 
                var dcView = this._get_(viewName);
 
                // -------------- End function config --------------
 
                var field = dcView._get_(fieldToGet);
                var valueField = field.valueField;
                var record = dcView._controller_.getRecord();
 
                var store = field.store;				

                store.load({
                    callback: function(rec) {
                        var i = 0, l = rec.length;
						var valueToReturn = "";

						if (fieldToReturn instanceof Array) {
							valueToReturn = [];
						}                        
 
                        for (i ; i < l; i++) {
                            var data = rec[i].data;
                            for (var key in data) {
                                if (data[valueField] == valueToSet) {

									if (fieldToReturn instanceof Array) {
										for (var x = 0; x<fieldToReturn.length; x++) {
											valueToReturn.push(data[fieldToReturn[x]]);
										}
									}
									else {
										valueToReturn = data[fieldToReturn];
									}
                                    break;									
                                }
                            }
                        }

                        if (record) {
                            record.beginEdit();
 
                            record.set(field.dataIndex, valueToSet);
							if (fielToReturnValueIn instanceof Array) {
								for (var z = 0; z<fielToReturnValueIn.length; z++) {
									record.set(fielToReturnValueIn[z], valueToReturn[z]);
								}
							}
							else {
								record.set(fielToReturnValueIn, valueToReturn);
							}
                            
                            if (opt) {
                                for (var key in opt) {
                                    record.set(key, opt[key]); 
                                }
                            }
                            record.endEdit();
                        }
                    }
                },this);
	},
	
	saveContractPriceCtgry : function( dc ) {
	    if(dc.getRecord().get("defaultPriceCtgy")){
	        this.priceCtgrySetDefault();
	    } else {
	        dc.doSave();
	    }
	},
	
	/* ****************** Can submit for Approval ********************************************** */
	canSubmitForApproval : function( dc ) {
		var r = dc.getRecord();
		if (r) {
			return r.data.canBeSubmitted;
		}
		return false;
	},
	
	canApprove : function( dc ) {
		var r = dc.getRecord();
		if (r) {
			return r.data.status === __CMM_TYPE__.ContractStatus._DRAFT_ && r.data.approvalStatus === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_ && r.data.canBeCompleted == true;
		}
		return false;
	},
	
	setupSubmitForApprovalWindow : function( theWindow , theForm , saveBtn , cancelBtn , label , title ) {
		var window = this._getWindow_(theWindow);
		var approvalNoteField = this._get_(theForm)._get_("approvalNote");
		var saveButton = this._get_(saveBtn);
		var cancelButton = this._get_(cancelBtn);
		
		saveButton.setDisabled(true);
		
		approvalNoteField.fieldLabel = label;
		approvalNoteField.setValue("");
		window.title = title;
		
		approvalNoteField.on("change", function(field) {
		    if (field.value.length > 0) {
		        saveButton.setDisabled(false);
				cancelButton.setDisabled(false);
		    } else {
		        saveButton.setDisabled(true);
				cancelButton.setDisabled(true);
		    }
		});
	
		if ("wdwApprovalNote" === theWindow) {
			var view = this._get_("documentsAssignList");
			view.store.load();	
			view.setSelection("");
		}
	
		window.show();
	},
	
	submitForApprovalRpc : function() {
		var callbackFn = function(dc,response) {
			var responseData = Ext.getResponseDataInJSON(response);
			if (responseData && responseData.params) {
				var responseDescription = responseData.params.submitForApprovalDescription;							
				var responseResult = responseData.params.submitForApprovalResult;
				
				//close window
				this._getWindow_("wdwApprovalNote").close();
				
				//display results (only if error)
				if (responseResult == false) {
					Main.error(responseDescription);
					//reload page
					dc.doReloadPage();
				} else {
					//reload page
					dc.doReloadPage({openForView: true, context: dc});
				}
			}
		};
		var callbackFailFn = function(dc,response) {
					Main.rpcFailure(response);
					//reload page
					dc.doReloadPage();
		};
	
		var dc = this.getActiveItemIndex() === 0 ? this._getDc_("contractList") : this._getDc_("contractEdit");
		this._sendAttachments_("documentsAssignList", dc);
	    var t = {
	        name: "submitForApproval",
			modal: true,
	        callbacks: {
	            successFn: callbackFn,
	            successScope: this,
	            failureFn: callbackFailFn,
	            failureScope: this
	        }
	    };
	    dc.doRpcData(t);
	},
	
	_sendAttachments_ : function( docAssignList , dc )  {
		var list = this._get_(docAssignList);
		var view = list.getView();
		var selectedAttachments = [];
		if (view.getSelectionModel().hasSelection()) {
		   var rows = view.getSelectionModel().getSelection();
		   Ext.each(rows, function(row) {
				selectedAttachments.push(row.data.id);
		   }, this);
		}
		dc.setParamValue("selectedAttachments",JSON.stringify(selectedAttachments));
	},
	
	/* ****************** Contract price category defaul question ********************************************** */
	savePriceBases : function( command ) {
	    var dc = this._getDc_("pricingBase");
	    var r = dc.getRecord();
	    if (r) {
	        if(r.get("defaultPriceCtgy")){
	            this.priceBasesCPCSetDefault(command);
	        } else {
	            this.savePbWithParameter(command);
	        }
	    }
	    
	},
	
	priceBasesCPCSetDefault : function( command ) {
	    var successFn = function() {           
	        this.pricingBaseSaveDefaultQueston (command);   
	    };
	    var o={
	        name:"checkDefault",
	        callbacks:{
	            successFn: successFn,
	            successScope: this
	        },
	        modal:true
	    };
	    this._getDc_("pricingBase").doRpcData(o);
	},
	
	pricingBaseSaveDefaultQueston : function( command ) {
	    var dc = this._getDc_("pricingBase");
	            var isDefault = dc.params.get("isDefault");
	            if(isDefault){
	                Ext.Msg.show({
	                   title : "Warning",
	                   msg : Main.translate("applicationMsg", "pricingBaseSaveDefaultQueston__lbl"),
	                   buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
	                   fn : function(btnId) {
	                        if( btnId === "yes" ){
	                                dc.params.set("isDefault",false);
	                                this.savePbWithParameter(command);
	                        }
	                   },
	                   scope : this
	                });
	            }else{
	                this.savePbWithParameter(command);
	            }
	},
	
	savePbWithParameter : function( command ) {
	    var dc = this._getDc_("pricingBase");
	    if(command === "New"){
	        dc.doSave({doNewAfterSave: true });
	    } else if(command === "Close"){
	        dc.doSave({doCloseNewWindowAfterSave: true });
	    } else if( command === "Edit"){
	        dc.doSave({doCloseEditAfterSave: true });
	    } else if( command === "EditComposite"){
	        dc.doSave({doCloseEditCompositeAfterSave: true });
	    }
	},
	
	fnCancelEnabled : function( dc ) {
		return e4e.dc.DcActionsStateManager.isCancelEnabled(dc);
	},
	
	/* **********************************approve/reject buttons rpc ******************************************** */
	approveRpc : function() {
		var dc = this.getActiveItemIndex() === 0 ? this._getDc_("contractList") : this._getDc_("contractEdit");
		var r = dc.getRecord();
		var rpcName = "approve";
		var w = this._getWindow_("wdwApproveNote");
						
		if (r && w._calledFromPanelButton_ === true) {
			var isBlueprint = r.get("isBlueprint");
			if (isBlueprint === true) {
				rpcName = "approveContractChanges";
			}
			dc.setParamValue("contractChangeType",w._contractChangeType_);
		}

		var callbackFn = function(dc,response) {
			var responseData = Ext.getResponseDataInJSON(response);
			if (responseData && responseData.params) {
				//close window
				w.close();
				
				//reload page
				dc.doReloadPage({checkRecordLength: true});
			}
		};
        
        var t = {
	        name: rpcName,
			modal: true,
	        callbacks: {
	            successFn: callbackFn,
	            successScope: this,
	            failureFn: callbackFn,
	            failureScope: this
	        }
	    };
	    dc.doRpcData(t);
	},
	
	rejectRpc : function() {
		var dc = this.getActiveItemIndex() === 0 ? this._getDc_("contractList") : this._getDc_("contractEdit");
		var r = dc.getRecord();
		var rpcName = "reject";
		var w = this._getWindow_("wdwRejectNote");			
		if (r && w._calledFromPanelButton_ === true) {
			var isBlueprint = r.get("isBlueprint");
			if (isBlueprint === true) {
				rpcName = "rejectContractChanges";
			}
			dc.setParamValue("contractChangeType",w._contractChangeType_);
		}
	
		var callbackFn = function(dc,response) {
			var responseData = Ext.getResponseDataInJSON(response);
			if (responseData && responseData.params) {
				//close window
				w.close();
				
				//reload page
				dc.doReloadPage();
			}
		};
	
	    var t = {
	        name: rpcName,
			modal: true,
	        callbacks: {
	            successFn: callbackFn,
	            successScope: this,
	            failureFn: callbackFn,
	            failureScope: this
	        }
	    };
	    dc.doRpcData(t);
	},

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BLUEPRINT CONTRACT STORY METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	isBlueprint : function() {
		var dc = this._getDc_("contractEdit");
		var r = dc.getRecord();
		var result = false;
		if (r) {
			var status = r.get("status");
			var isBlueprint = r.get("isBlueprint");
			if ( status !== __CMM_TYPE__.ContractStatus._DRAFT_ && isBlueprint ) {
				result = true;
			}
		}
		return result;
	},
	
	isNotReadOnly : function() {
		var dc = this._getDc_("contractEdit");
		var r = dc.getRecord();
		var result = false;
		if (r) {
			var readOnly = r.get("readOnly");
			if ( readOnly !== true ) {
				result = true;
			}
		}
		return result;
	},
	
	isDraft : function() {
		var dc = this._getDc_("contractEdit");
		var r = dc.getRecord();
		var result = false;
		if (r) {
			var status = r.get("status");
			if ( status === __CMM_TYPE__.ContractStatus._DRAFT_ ) {
				result = true;
			}
		}
		return result;
	},

	isOriginal : function() {
		var dc = this._getDc_("contractEdit");
		var r = dc.getRecord();
		var result = false;
		if (r) {
			var status = r.get("status");
			var isBlueprint = r.get("isBlueprint");
			if ( status !== __CMM_TYPE__.ContractStatus._DRAFT_ && !isBlueprint ) {
				result = true;
			}
		}
		return result;
	},
	
	_showConfirmPublishWithChanges : function() {
		Ext.Msg.show({
		       title : "Publish Changes",
		       msg : Main.translate("applicationMsg", "saleContractPublishCahnges__lbl"),
		       icon : Ext.MessageBox.WARNING,
		       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
		       fn : function(btnId) {
		              if( btnId === "yes" ){
		                  this.publishBlueprintRpc();
	          		  }
		       },
		       scope : this
		});
	},
	
	publishBlueprintRpc : function() {
		var dcEdit = this._getDc_("contractEdit");
	
		var successFn = function() {
			this._showStackedViewElement_("main", "canvas2");
			dcEdit.doReloadPage({checkRecordLength: true});
		}
	
		var callbackFailFn = function(dc,response) {
			Main.rpcFailure(response);
			dcEdit.doReloadPage({checkRecordLength: true});
		}
		
		var o={
			name:"publishBlueprint",
			modal:true,
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: callbackFailFn,
		        failureScope: this
			},
		};
	
		dcEdit.doRpcData(o);
	},
	
	openContractForEdit : function() {
		var dc = this._getDc_("contractList");
		var r = dc.getRecord();
		if ( r ) {
			var dcEdit = this._getDc_("contractEdit");
			if ( r.get("status") !== __CMM_TYPE__.ContractStatus._DRAFT_ ) {
				var successFn = function(dc, response) {
					var params = Ext.getResponseDataInJSON(response).params; 
					var generatedContractId = params.generatedContractId;
					dcEdit.setFilterValue("id",generatedContractId);
					dcEdit.doQuery();
					this._showStackedViewElement_("main", "canvas2");
					this.makeAttachmentsReadOnly( false );
				}
				var o={
					name:"createBlueprintContract",
					modal:true,
					callbacks:{
						successFn: successFn,
						successScope: this
					}
				};
				dc.doRpcData(o);
	
			} else {
				if ( r.get("approvalStatus") === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_ ) {
					this.openForView();
				} else {
					dcEdit.doQuery();
					this._showStackedViewElement_("main", "canvas2");	
					this.makeAttachmentsReadOnly( false );
				}
			}
		}	

		this._enableDisableFieldsIfViewContract_(true);
	},
	
	publishBlueprint : function() {
		var dc = this._getDc_("contractEdit");
		if(this.useWorkflowForApproval() && this.canSubmitForApproval(dc)){
			this._showConfirmPublishWithChanges();
		}else{
			this.publishBlueprintRpc();
		}
	},
	
	makeAttachmentsReadOnly : function( flag ) {
			var attachmentList = this._getDc_("attachment");
			attachmentList.setReadOnly(flag);
	},
	
	openOriginalContract : function() {
		var dc = this._getDc_("contractEdit");
		dc.setReadOnly(true);
		var r = dc.getRecord();
			if (r) {
				var origContractId = r.get("blueprintOriginalContractReferenceId");
				dc.setFilterValue("id",origContractId);
				dc.doQuery();
				this.makeAttachmentsReadOnly(true);
			}

		this._enableDisableFieldsIfViewContract_(false);
	},
	
	editContractPriceCtgry : function() {
		var dc = this._getDc_("contractEdit");
		var r = dc.getRecord();
			if (r) {
				this._showStackedViewElement_("main", "canvas3");		
			}
	},
	
	viewContractPriceCtgry : function() {
		var dc = this._getDc_("contractEdit");
		var r = dc.getRecord();
			if (r) {
				this._showStackedViewElement_("main", "canvas3");	
				var contractPriceCtgry = this._getDc_("contractPriceCtgry");
				contractPriceCtgry.setReadOnly(true);	
			}
	},
	
	canActivateContract : function(targetedDC) {
		var dc = this._getDc_(targetedDC);
		var rec = dc.getRecord();
		var result = true;
		if (rec) {
			if ( rec.data.status !== __CMM_TYPE__.ContractStatus._DRAFT_ ) {
				result = false;
			}
			if (rec.data.readOnly === true) {
				result = false;
			}
		}
		return result;
	},
	
	_afterDefineElements_ : function(){
		var f = this._getElementConfig_("copyCustomer");
		var f1 = this._getElementConfig_("approveNote");
		var f2 = this._getElementConfig_("approvalNote");
		var f3 = this._getElementConfig_("rejectNote");
		
		if( f ){
			f._shouldDisableWhenDcIsReadOnly_ = false;
		}
		if( f1 ){
			f1._shouldDisableWhenDcIsReadOnly_ = false;
		}
		if( f2 ){
			f2._shouldDisableWhenDcIsReadOnly_ = false;
		}
		if( f3 ){
			f3._shouldDisableWhenDcIsReadOnly_ = false;
		}
	
		// Dan: manual override to fix the grid panels that were not stretching to the full height of the window
	
		this._getBuilder_().change("canvas2",{
			layout: { type: "vbox", align: "stretch"}
		});
		this._getBuilder_().change("detailsTab",{
			flex: 1
		});
		this._getBuilder_().change("prices",{
			layout: { type: "vbox", align: "stretch"}
		});
		this._getBuilder_().change("priceCtgryList",{
			flex: 1
		});
	},
	
	//Dan: SONE-2494 Enhance price category restrictions selection
	_setPriceRestrictionEditorType_ : function( editor , ctx , view ) {
		var fieldIndex = ctx.field;
		var col = ctx.column;
		var cfg;
		var textEditor = {xtype:"textfield", maxLength:32};	
		var storeData = [];
		
		if (fieldIndex === "value") {
			var r = ctx.record;
			if (r) {
				var restrictionType = r.get("restrictionType");
				if (!Ext.isEmpty(restrictionType)) {
		
					//////////// Aircraft type ////////////
		
					if (restrictionType === __CMM_TYPE__.RestrictionTypes._AIRCRAFT_TYPE_) {
						cfg = {
							_dcView_ : view, 
							xtype:"fmbas_AcTypesLov_Lov", 
							maxLength:32, 
							typeAhead: false,
							remoteFilter: true,
							remoteSort: true,
							forceSelection: true,
							retFieldMapping: [{
								lovField:"code", 
								dsField: "value"}
							]
						};
					}
		
					//////////// Fueling type ////////////
		
					else if (restrictionType === __CMM_TYPE__.RestrictionTypes._FUELING_TYPE_) {
						for (var key in __CMM_TYPE__.FuelTicketFuelingOperation) {
							storeData.push(__CMM_TYPE__.FuelTicketFuelingOperation[key]);
						}
						cfg = {
						    xtype: "combo",
							store: storeData,
							forceSelection: true
						};
					}
		
					//////////// Transport type ////////////
		
					else if (restrictionType === __CMM_TYPE__.RestrictionTypes._TRANSPORT_TYPE_ ) {
						for (var key in __CMM_TYPE__.FuelingType) {
							storeData.push(__CMM_TYPE__.FuelingType[key]);
						}
						cfg = {
						    xtype: "combo",
						    store: storeData,
							forceSelection: true
						};
					}
		
					//////////// Registration number ////////////
		
					else if (restrictionType === __CMM_TYPE__.RestrictionTypes._REGISTRATION_NUMBER_ ) {							
						cfg = {xtype:"textfield", maxLength:5, enforceMaxLength : true};
					}
		
					//////////// Operation type ////////////
		
					else if (restrictionType === __CMM_TYPE__.RestrictionTypes._OPERATION_TYPE_ ) {
						for (var key in __OPS_TYPE__.OperationType) {
							storeData.push(__OPS_TYPE__.OperationType[key]);
						}
						cfg = {
						    xtype: "combo",
						    store: storeData,
							forceSelection: true
						};
					}
		
					//////////// Ship to ////////////
		
					else if (restrictionType === __CMM_TYPE__.RestrictionTypes._SHIP_TO_) {
						var contractDc = this._getDc_("contractEdit");
						var contractRecord = contractDc.getRecord();
						var contractId;
						if(contractRecord){
							contractId = contractRecord.get("id");
						}
						cfg = {
							_dcView_ : view, 
							xtype:"cmm_ShipToCustomerCodeLov_Lov", 
							maxLength:32,
							typeAhead: false,
							remoteFilter: true,
							remoteSort: true,
							forceSelection: true,
							filterFieldMapping: [
								{lovField:"isCustomer", value: true}, 
				                {lovField:"contractId", value: contractId}
							],
							retFieldMapping: [{
								lovField:"code", 
								dsField: "value"
							}],								
						};
					}
		
					//////////// Time of operation ////////////
		
					else if (restrictionType === __CMM_TYPE__.RestrictionTypes._TIME_OF_OPERATION_ ) {
						cfg = {
		                    xtype: "combo",
		                    selectOnFocus: true,
		                    queryMode: "local",
		                    store: ["00:00","00:15","00:30","00:45","01:00","01:15","01:30","01:45","02:00","02:15","02:30","02:45","03:00","03:15","03:30","03:45","04:00","04:15","04:30","04:45","05:00","05:15","05:30","05:45","06:00","06:15","06:30","06:45","07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45","10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"]
		                };
					}
		
					//////////// Flight type ////////////
		
					else if (restrictionType === __CMM_TYPE__.RestrictionTypes._FLIGHT_TYPE_  ) {
						for (var key in __CMM_TYPE__.FlightTypeIndicator) {
							storeData.push(__CMM_TYPE__.FlightTypeIndicator[key]);
						}
						cfg = {
						    xtype: "combo",
						    store: storeData,
							forceSelection: true
						};
					}
		
					//////////// Volume ////////////
					else if (restrictionType === __CMM_TYPE__.RestrictionTypes._VOLUME_  ) {
						cfg = {xtype:"numberfield", minValue:0};
					}
		
					//////////// END CASE ////////////
					else {
						cfg = textEditor;
					}
		
					col.setEditor(cfg);
				}
			}
		}
		
		// Clear the value on restriction type change
		
		if (fieldIndex === "restrictionType") {
			var ed = col.getEditor();
			ed.on("change", function() {
				var record = view._controller_.getRecord();
				record.set("value", "");
				record.set("unit", null);
				record.set("unitCode", "");
			}, this);
		}
	},

	_afterViewAndFilters_ : function(){
		// Dan: SONE-1554: Contract Management: the following error message appears when updating the contract\'s validity period 
		var priceList = this._get_("priceCtgryList");
		var contract = this._getDc_("contractList");
		if(Ext.getCmp(priceList)){
			var viewFilterToolbar = Ext.getCmp(priceList._viewFilterToolbarId_);
		}
	
		if (viewFilterToolbar) {
	
			var datePickerCmp = Ext.getCmp(viewFilterToolbar._datePickerId_);
	
			// Dan: SONE-2159: Purchase contracts: issues with price review date
	
			var adjustDate = function() {
				var r = contract.getRecord();
				if (r) {
					var validFrom = r.get("validFrom");
					var validTo = r.get("validTo");
					var datePickerValue = datePickerCmp.getValue();
	
					if (datePickerValue < validFrom) {
						datePickerCmp.setValue(validFrom);
						Main.warning("The selected date is out of the contract period!");
					}
					else if (datePickerValue > validTo) {
						datePickerCmp.setValue(validTo);
						Main.warning("The selected date is out of the contract period!");
					}
				}
			}
	
			var adjustDateIfExpired = function() {
	
				var r = contract.getRecord();
				if (r) {
					var status = r.get("status");
					var validTo = r.get("validTo");
					var currentDate = new Date();
	
					if (status  === "Expired") {
						datePickerCmp.setValue(validTo);
					}
					else {
						datePickerCmp.setValue(currentDate);
					}
				}
			}
	
			datePickerCmp.on("blur", function() {
				adjustDate();
			}, this);
	
			contract.on("onAfterEditIn", function() {
				adjustDateIfExpired();
	
			}, this);
	
			contract.on("adjustDatePicker", function() {
				adjustDateIfExpired();
			}, this);
		}
	},
	
	_afterDefineDcs_ : function() {

	    var contractEdit = this._getDc_("contractEdit");
		var contractList = this._getDc_("contractList");
		var newContract = this._getDc_("newContract");
	    var pricingBase = this._getDc_("pricingBase");
	    var contractPriceCtgry =  this._getDc_("contractPriceCtgry");
	    var contractPriceCmpn = this._getDc_("contractPriceCmpn");
		var contractPriceCmpnClassic = this._getDc_("contractPriceCmpnClassic");
		var contractPriceRestr = this._getDc_("contractPriceRestr");
		var contractPriceCtgryAssign = this._getDc_("contractPriceCtgryAssign");
		var shipTo = this._getDc_("shipTo");
		var attachment = this._getDc_("attachment");
		this.customerCodeTooltipId = Ext.id();
	
		contractEdit.on("afterDoSaveFailure", function() {
			return false;
		}, this);
	
		contractPriceCtgryAssign.on("afterDoQuerySuccess", function(dc,ajax) { 
	
	        var markSelected = ajax.options.markSelected;
			var view = this._get_("priceCtgryAssignList");
			var form = this._get_("priceCtgryEdit");
			if (form) {
				var percentageOf = form._get_("percentageOf");
				var win = this._getWindow_("priceCategoryAsgnWdw");
				var s = dc.store;
	
				if (markSelected && markSelected === true) {	
					if (win._isInEditForm_ === true) {	
						if (!Ext.isEmpty(percentageOf.getRawValue())) {	
							var percentageOfArray = percentageOf.getRawValue().split(",");
							s.each(function(r) {
								if (percentageOfArray.indexOf(r.get("name")) !== -1 ) {
									view.getSelectionModel().select(s.indexOf(r), true);
								}
							}, this);
						}
					}
					else {
						view.getSelectionModel().deselectAll();
					}
				}
			}
	    } , this );
	
		contractEdit.on("onEditOut", function(dc) {
			var contract = this._getDc_("contractEdit");
			var pricingBase = this._getDc_("pricingBase");
			var contractPriceCtgry = this._getDc_("contractPriceCtgry");
			var shipTo = this._getDc_("shipTo");
			var contractList = this._getDc_("contractList");
			
			contract.setReadOnly(false);
			pricingBase.setReadOnly(false);
			shipTo.setReadOnly(false);
			contractPriceCtgry.setReadOnly(false);
			contractPriceCtgry.canDoEditIn = true;
	        contractPriceCtgry.canDoDelete = true;
	
			dc.doReloadPage();
			contractList.doCancel();
			contractList.doReloadPage();
	
		}, this);
	
		contractPriceCtgry.on("onAssignPriceCategory", function() { 
	        this.assignPrice();
	        this._setMainPriceCategory_();
	    } , this );
		
		contractPriceCtgry.on("onEditOut", function() { 
			contractEdit.doReloadRecord();
	    } , this );
	
	    contractEdit.on("afterDoSaveSuccess", function (dc) {
	        this._applyStateAllButtons_();
	        pricingBase.doReloadPage();
			dc._isCopyMode_ = null;
			dc._isNewFromCustomers_ = null;
			this._checkReadOnly_();
			contractEdit.doReloadPage();
	    }, this);
	
		contractPriceRestr.on("afterDoSaveSuccess", function () {
	        this._applyStateAllButtons_();
	    }, this);
	
		contractEdit.on("beforeDoSave", function () {
			var dc = this._getDc_("contractEdit");
	    	var r = dc.getRecord();
	
			if (r) {
				var validTo = r.get("validTo");
				var validFrom = r.get("validFrom");
	
				if (validFrom > validTo) {
					Main.warning(Main.translate("applicationMsg","validFromValidToWarning__lbl"));
				}
				var holderCode = r.get("holderCode");
				var customerCode = r.get("customerCode");
				if(holderCode == customerCode){
					Main.warning(Main.translate("applicationMsg","sameCompaniesWarning__lbl"));
				}
			}
	    }, this);
	
		contractEdit.on("afterDoReloadPageSuccess", function (dc, ajaxResult) {
			var openForView = ajaxResult.options.openForView;
			if (ajaxResult.options.filterGeneratedContract === true) {
				this._showStackedViewElement_("main", "canvas2", this._afterCopyContract_(dc, ajaxResult.options.generatedContractId));
			}
			if (openForView === true) {
				var context = ajaxResult.options.context;
				this.openForView(context);
			}	
			if (ajaxResult.options.checkRecordLength === true) {
				var totalCount = Ext.getResponseDataInJSON(ajaxResult.response).totalCount;
				if (totalCount === 0) {
					dc.doEditOut();
				}
			}
	    }, this);
	
		contractList.on("afterDoReloadPageSuccess", function (dc, ajaxResult) {
			var openForView = ajaxResult.options.openForView;
			if (openForView === true) {
				var context = ajaxResult.options.context;
				this.openForView(context);
			}
			if (ajaxResult.options.checkRecordLength === true) {
				var totalCount = Ext.getResponseDataInJSON(ajaxResult.response).totalCount;
				if (totalCount === 0) {
					dc.doEditOut();
				}
			}
	    }, this);
	
		contractList.on("afterDoQuerySuccess", function (dc, ajaxResult) {
			var showEdit = ajaxResult.options.showEdit;
			var openForView = ajaxResult.options.openForView;
			if (showEdit === true) {
				this.openContractForEdit();
			}
			if (openForView === true) {
				this.openForView();
			}
			
	    }, this);
	
		contractEdit.on("beforeDoDelete", function (dc) {
			var r = dc.getRecord();
			if (r) {
				var resaleRefId = r.get("resaleRefId");
				if (!Ext.isEmpty(resaleRefId)) {
					dc._controller_.commands.doDelete.confirmMessageBody = Main.translate("msg", "dc_confirm_delete_if_reference");
				}
				else {
					dc._controller_.commands.doDelete.confirmMessageBody = Main.translate("msg", "dc_confirm_delete_selection");
				}
			}                    
	    }, this);
	
		contractEdit.on("recordChange", function (obj) {
			this._onContractRecordChange_(obj.dc);
	
			this._showHidePeriodAlert_();
			this._showHideShipToAlert_();
			this._showHidePriceAlert_();
	
			this._refreshContractReadOnly_();
	    }, this);

		contractEdit.on("recordReload", function(obj) {
			this._onContractRecordChange_(obj.dc);
			
			this._showHidePeriodAlert_();
			this._showHideShipToAlert_();
			this._showHidePriceAlert_();
	
			this._refreshContractReadOnly_();
	    }, this);
	
	    // Dan: onEditIn, set the first tab as active to fix the focus problem
	
		contractEdit.on("onEditIn", function () {
			var ctx = this;
			var fn = function() {
				var detailsTab = ctx._get_("detailsTab");
				var prices = ctx._get_("prices");
				detailsTab.setActiveTab(prices);
				ctx._checkReadOnly_();
			}
			this._showStackedViewElement_("main", "canvas2", fn());					
		}, this);
		
		contractList.on("onEditIn", function () {
			this._checkReadOnly_();	
		}, this);
	
		contractEdit.on("disableContinueBtn", function() {
			var btn = this._get_("btnContinueNewSalesContract");
			btn.setDisabled(true);
		}, this);
	
		contractEdit.on("enableContinueBtn", function(dc, val) {
			var btn = this._get_("btnContinueNewSalesContract");
			btn.setDisabled(!val);
		}, this);
	
		contractList.on("enableContinueCopyBtn", function() {
			var btn = this._get_("btnContinueCopySalesContract");
			btn.setDisabled(false);
		}, this);
	
		contractList.on("afterDoNew", function (dc, ajaxRequest) {
	        var newFromCustomers = ajaxRequest.newFromCustomers;
	        if (!newFromCustomers) {
				var window = this._getWindow_("wdwNewSalesContract");
	            window.show(undefined, function() {
	                var templateDc = this._getDc_("template");
					templateDc.doQuery();
	            }, this);       
	        }
	
			this._setSubsidiary_();
	
			var view = this._get_("selectCustomer");
			if (view) {
				var field = view._get_("customerName");
				field.setValue(null);
			}                                       
	     }, this);
	
		newContract.on("afterDoNew", function (dc, ajaxRequest) {	
	        var window = this._getWindow_("wdwNewContract");
			var contractTemplate = ajaxRequest.contractTemplate;
			var r = dc.getRecord();
	
	        window.show(undefined,function() {
				if (r) {
					r.beginEdit();
					r.set("type", contractTemplate.type);
					r.set("subType", contractTemplate.subType);
					r.set("scope", contractTemplate.scope);
					r.set("dealType", contractTemplate.dealType);
					r.endEdit();
				}
			}, this);
	
			window.setStep(1);
	
	     }, this);
	
		newContract.on("afterDoSaveSuccess", function (dc, ajaxRequest) {
	        var window = this._getWindow_("wdwNewContract");
			var contractList = this._getDc_("contractList");
	
			if (ajaxRequest.options.options.closeWindow === true) {	
	
				var data = Ext.getResponseDataInJSON(ajaxRequest.response).data[0];
				var contractId = data.id;					
	
				var salesContract = this._get_("SalesContract");
				var resetButton = Ext.getCmp(salesContract._resetButtonId_);
				window.close();
				resetButton._resetGrid_({showCreatedContract: true, contractId : contractId});
	
			}
			contractList.doReloadPage();
			contractList.setFilterValue("id", data.id);
	     }, this);
		newContract.on("itsTimeToCalculateDensity", function (dc,params) {
	        var formName;
            if (params && !Ext.isEmpty(params.formName)) {
            	formName = params.formName;
            }
	        var record = newContract.getRecord();
	        if (record) {
	            var quotation = record.get("quotationName");
	            var quotUnitTypeInd = record.get("quotUnitTypeInd");
	            var convUnitTypeInd = record.get("convUnitTypeInd");
	            var convUnitCode = record.get("convUnitCode");
	            var callValidate = false;
	            var updateRecords = true;
	            if(!Ext.isEmpty(quotation) && quotUnitTypeInd !== convUnitTypeInd && !Ext.isEmpty(convUnitCode)){	            	
		            if (params && params.validateInput && params.validateInput === true) {
		            	callValidate = true;
		            }
		            if (params && params.updateRecords && params.updateRecords === false) {
		            	updateRecords = false;
		            }
	                this.getDensity(callValidate,updateRecords,newContract,formName);
	            } else{
	                this.setDefaultDensity(callValidate,updateRecords,newContract,formName);
	            }
	        }			
		}, this);
	
		contractList.on("afterViewFiltersReset", function (dc, ajaxRequest) {
			if (ajaxRequest.showCreatedContract === true) {
				var contract = this._getDc_("contractEdit");					
	            var window = this._getWindow_("wdwNewContract");
				contract.setFilterValue("id", ajaxRequest.contractId);
				contract.doQuery({showCreatedContract: true});
				
			}
	     }, this);
		
		shipTo.on("afterDoQuerySuccess", function (dc, ajaxRequest) {
			this._checkReadOnly_();
			this._applyStateAllButtons_();
	     }, this);
	
		contractEdit.on("afterDoQuerySuccess", function(dc, ajaxResult) {
				if (ajaxResult.options.doNew === true) {
					this._showStackedViewElement_("main", "canvas1", dc.doNew());
				}
				if (ajaxResult.options.showFilteredEdit === true) {
					if (this._get_("main").hidden === true) {						
						this._get_("main").show();
						this._showStackedViewElement_("main", "canvas2",this._checkReadOnly_(ajaxResult.options.internalResale));
						;
					}
					else {
						this._showStackedViewElement_("main", "canvas2",this._checkReadOnly_(ajaxResult.options.internalResale));
					}
				}
				if(ajaxResult.options.queryFromExternalInterface === true) {
					if (ajaxResult.records.length !== 0) {
						this._showStackedViewElement_("main", "canvas1");
					} else {
						Main.info(Main.translate("applicationMsg", "externalInterfaceViewNoObj__lbl"));
						return;
					}
				}
				if(ajaxResult.options.showCreatedContract === true) {
					this._showStackedViewElement_("main", "canvas2");
					var detailsTab = this._get_("detailsTab");
	                var prices = this._get_("prices");
	                detailsTab.setActiveTab(prices);
				}
		}, this);
		contractPriceCtgry.on("afterDoSaveSuccess", function(dc) { 
	        var contractPriceRestrDC = this._getDc_("contractPriceRestr");
			var contractEdit = this._getDc_("contractEdit");
	        var rec = dc.getRecord();
	        if (rec) {
	            var restriction = rec.get("restriction");
	            contractPriceRestrDC.setParamValue("isEnabled", restriction);
	            contractPriceRestrDC.doReloadPage();
	        }
			pricingBase.doCancel();
			contractEdit.doReloadRecord();
	    }, this);

		contractPriceCtgry.on("afterDoQuerySuccess", function(dc, ajaxResult) { 
			if (ajaxResult.options.showFilteredEdit === true) {
				this._showStackedViewElement_("main", "canvas3", dc.noReset = false); // SONE-2733
			}
		}, this);
	
		contractPriceCtgry.on("onEditOut", function(dc) {
			var priceCtgryList = this._get_("priceCtgryList");
			var resetButton = Ext.getCmp(priceCtgryList._resetButtonId_);
			resetButton._resetGrid_();
			dc.doReloadPage();
		}, this); 
	
	    contractPriceCtgry.on("afterDoDeleteSuccess", function() {
	        pricingBase.doReloadPage();
			contractEdit.doReloadPage();
	    } , this );
	

	    shipTo.on("afterDoDeleteSuccess", function() {
	        shipTo.doReloadPage();
				contractEdit.doReloadRecord();
			contractEdit.doReloadPage();
	    } , this );
	
	    pricingBase.on("itsTimeToCalculateDensity", function (dc,params) {
	        var record = pricingBase.getRecord();
	        var formName;
            if (params && !Ext.isEmpty(params.formName)) {
            	formName = params.formName;
            }

	        if (record) {
	            var quotation = record.get("quotName");
	            var quotUnitTypeInd = record.get("quotUnitTypeInd");
	            var convUnitTypeInd = record.get("convUnitTypeInd");
	            var convUnitCode = record.get("convUnitCode");
	            var callValidate = false;
	            var updateRecords = true;
	            if(!Ext.isEmpty(quotation) && quotUnitTypeInd !== convUnitTypeInd && !Ext.isEmpty(convUnitCode)){	            	
		            if (params && params.validateInput && params.validateInput === true) {
		            	callValidate = true;
		            }
		            if (params && params.updateRecords && params.updateRecords === false) {
		            	updateRecords = false;
		            }
	                this.getDensity(callValidate,updateRecords,pricingBase,formName);
	            } else{
	                this.setDefaultDensity(callValidate,updateRecords,pricingBase,formName);
	            }
	        }
	    }, this);
	
	    pricingBase.on("OnEditIn", function (dc) {
	       this.showWdwPricing();
	                            
	        var pbRecord = dc.getRecord();
	        if(pbRecord.get("pricingMethod") === "Index"){
	            dc.fireEvent("itsTimeToCalculateDensity", dc);
	        }else{
	            this.setDefaultDensity(false, false, dc);
	        }
	
	    }, this);
		
//		pricingBase.on("afterDoSaveFailure", function (){
//			var w = this._getWindow_("wdwPricing");
//			w.hide();
//		},this);
	
	    contractPriceCmpnClassic.on("OnEditIn", function () {
	        this.showWdwPriceComp1();
	    }, this);
	
		contractEdit.on("afterDoSaveFailure", function (dc) {
	       dc.doCancel();
	    }, this);
	
	    contractPriceCmpn.on("OnEditOut", function () {
	        contractPriceCtgry.doReloadPage();
	        contractPriceCtgry.canDoDelete = true;
	    }, this);
	
	    pricingBase.on("afterDoNew", function (dc, ajaxResult) {
	    	var newRec;
	    	var opt;
	        if (ajaxResult.setIndexPrice === true) {
	            dc._changeState_ = 1;
				newRec = dc.getRecord();                    	
				if (newRec) { 
	            	newRec.beginEdit();                    
					newRec.set("mainCategoryCode", __FMBAS_TYPE__.PriceType._PRODUCT_);                        
	            	newRec.endEdit();
	        	}
	            this.showPriceIndexWdw("PbEdit","priceCatName","Index","id","priceCatId");
	        } 
	        if (ajaxResult.setFixPrice === true) {
	            opt = {
	                description: "Fixed"
	            }
	            dc._changeState_ = 1;
				newRec = dc.getRecord();                    	
				if (newRec) { 
	            	newRec.beginEdit();                    
					newRec.set("mainCategoryCode", __FMBAS_TYPE__.PriceType._PRODUCT_);                        
	            	newRec.endEdit();
	        	}
	            this.showPriceFixWdw("priceFixNew","priceCatName","Fixed","id","priceCatId", opt);
	
	        } 
	        if (ajaxResult.setDftPrice === true) {
	            opt = {
	                description: "Into Plane Service for low volume"
	            }
	            dc._changeState_ = 1;
	            this.showPriceFixWdw("priceFixNew","priceCatName","","id","priceCatId", opt);
	        }
			if (ajaxResult.setCompositePrice === true) {
	            dc._changeState_ = 1;
	            this.showCompositePriceWdw();
	        }
	
	        this.setPriceFields();
	        this._getDc_("pricingBase").getRecord().set("exchangeRateOffset", this._getDc_("contractEdit").getRecord().get("exchangeRateOffset"));
	    }, this);
	
	    pricingBase.on("afterDoDeleteSuccess", function () {
	        contractPriceCtgry.doReloadPage();
			contract.doReloadPage();
	    }, this);
	
	    pricingBase.on("afterDoSaveSuccess", function (dc, ajaxResult) {
	        if (ajaxResult.options.options.doNewAfterSave === true){
	
	            if(dc._step_ === 1) {
	                 this._setDefaultFix();
	            } else if (dc._step_ === 2 ) {
	                this._setDefaultDFT();
	            } else if (dc._step_ === 3) {
					 this._setDefaultComposite();
				}
	
	            contractEdit.doReloadRecord();
	            contractPriceCtgry.doReloadPage();
	        } 
	        if (ajaxResult.options.options.doCloseNewIndexPriceWindowAfterSave === true) {
	            this._getWindow_("wdwPricing").close();
	            contractEdit.doReloadRecord();
	            contractPriceCtgry.doReloadPage();
	        }
			if (ajaxResult.options.options.doCloseCompositePriceAfterSave === true) {
	            this._getWindow_("compositePriceWdw").close();
	            contractEdit.doReloadRecord();
	            contractPriceCtgry.doReloadPage();
	        }
	        if (ajaxResult.options.options.doCloseNewWindowAfterSave === true){
	            this._getWindow_("priceFixWdw").close();
	            contractEdit.doReloadRecord();
	            contractPriceCtgry.doReloadPage();
	        } 
	        if (ajaxResult.options.options.doCloseEditFormulaWindowAfterSave === true) {
	            this._getWindow_("wdwPricingEditFormula").close();
	
	            contractEdit.doReloadRecord();
	            contractPriceCtgry.doReloadPage();
	        }
	
	        // Dan: SONE-2108 bug fixes
	
	        if(ajaxResult.options.options.doCloseEditAfterSave === true){
				this._getWindow_("priceFixWdw").close();
				var jsonData = Ext.getResponseDataInJSON(ajaxResult.response);
				var data = jsonData.data[0];
				var lastRecordId = data.id;
				contractPriceCtgry.doClearAllFilters();
				contractPriceCtgry.setFilterValue("pricingBaseId", lastRecordId);
				contractPriceCtgry.doQuery({showFilteredEdit: true});
			}
	
	        // End Dan
	
			if(ajaxResult.options.options.doCloseEditCompositeAfterSave === true){
	
				this._getWindow_("compositePriceWdw").close();
				var jd = Ext.getResponseDataInJSON(ajaxResult.response);
				var d = jd.data[0];
				var lastRecId = d.id;
	
				contractPriceCtgry.doClearAllFilters();
				contractPriceCtgry.setFilterValue("pricingBaseId", lastRecId);
				contractPriceCtgry.doQuery({showFilteredEdit: true});
			}
			
			contractEdit.doReloadPage();
	    }, this);
	
		shipTo.on("afterDoNew", function (dc){
			var newRec = dc.getRecord();
			var contract = this._getDc_("contractEdit");
	        var contractRecord = contract.getRecord();
			if (newRec && contractRecord) { 
	            newRec.beginEdit(); 
				newRec.set("validFrom", contractRecord.get("validFrom"));
	            newRec.set("validTo", contractRecord.get("validTo"));
				dc.params.set("contractStatus",contractRecord.get("status"));
				dc.params.set("contractDealType",contractRecord.get("dealType"));
	            newRec.endEdit();
	        }
		},this);
	
		shipTo.on("afterDoSaveSuccess", function (dc){
			if (_SYSTEMPARAMETERS_.syssalescontractapproval === "false" && dc._volumeChanged_ === true) {
				var win = this._getWindow_("wdwRemarks");
				win.show();
			}
			dc.doReloadPage();
			contractEdit.doReloadPage();
		},this);
	
		shipTo.on("afterDoSaveFailure", function (){
			shipTo.doCancel();
			shipTo.doReloadPage();
		},this);
	
		shipTo.on("afterDoDeleteFailure", function() {
			contract.doCancel();
			contract.doReloadRecord();
		}, this);
	
		contractPriceCmpn.on("afterDoNew", function (dc){
	        this.showWdwPriceComp();
	        var number = dc.store.totalCount;
	        var rec = dc.getRecord();
	        var contract = this._getDc_("contractEdit");
	        var contractRecord = contract.getRecord();
	        
	        if(number === 0 && rec && contractRecord) {
                rec.set("validFrom", contractRecord.get("validFrom"));
	        }
	        var dst = dc.getRecord();
	        var src = contractRecord;
	
	        if (dst && src) {
	
				var priceCtgryPricePer = dst.get("priceCtgryPricePer");
	
	            dst.beginEdit();
	            dst.set("validTo", src.get("validTo"));
	            dst.set("currency", src.get("settCurrId"));             
	            dst.set("currencyCode", src.get("settCurrCode"));
	
	            if (priceCtgryPricePer !== __FMBAS_TYPE__.PriceInd._EVENT_) {
	                dst.set("unit", src.get("settUnitId"));
	                dst.set("unitCode", src.get("settUnitCode"));
	            }
	            else {
	                this._setDefaultLovValue_("priceCompEdit", "unitEvent", "EA", "id", "unit");
	            }
	            if (priceCtgryPricePer !== __FMBAS_TYPE__.PriceInd._PERCENT_) {
	                dst.set("unit", src.get("settUnitId"));
	                dst.set("unitCode", src.get("settUnitCode"));
	            }
	            else {
	                this._setDefaultLovValue_("priceCompEdit", "unitPercent", "%", "id", "unit");
	            }
	            dst.endEdit();
	        }
	
	    }, this ); 
	
		contractPriceCmpnClassic.on("afterDoNew", function (dc){
	        this.showWdwPriceComp();
	        var number = dc.store.totalCount;
	        var rec = dc.getRecord();
	        var contract = this._getDc_("contractEdit");
	        var contractRecord = contract.getRecord();
	        
	        if(number === 0 && rec && contractRecord) {
                rec.set("validFrom", contractRecord.get("validFrom"));
	        }
	        var dst = dc.getRecord();
	        var src = contractRecord;
	
	        if (dst && src) {
	
				var priceCtgryPricePer = dst.get("priceCtgryPricePer");
	
	            dst.beginEdit();
	            dst.set("validTo", src.get("validTo"));
	            dst.set("currency", src.get("settCurrId"));             
	            dst.set("currencyCode", src.get("settCurrCode"));
	
	            if (priceCtgryPricePer !== __FMBAS_TYPE__.PriceInd._EVENT_) {
	                dst.set("unit", src.get("settUnitId"));
	                dst.set("unitCode", src.get("settUnitCode"));
	            }
	            else {
	                this._setDefaultLovValue_("priceCompEdit", "unitEvent", "EA", "id", "unit");
	            }
	            if (priceCtgryPricePer !== __FMBAS_TYPE__.PriceInd._PERCENT_) {
	                dst.set("unit", src.get("settUnitId"));
	                dst.set("unitCode", src.get("settUnitCode"));
	            }
	            else {
	                this._setDefaultLovValue_("priceCompEdit", "unitPercent", "%", "id", "unit");
	            }
	            dst.endEdit();
	        }
	
	    }, this ); 
	    
	    contractPriceCmpn.on("afterDoSaveSuccess", function (dc, ajaxResult) {
	        var rec = dc.getRecord();
	        if (rec) {
	            var message = rec.get("toleranceMessage");
	            if(!Ext.isEmpty(message)){
	                Main.warning(message);
	            }
	        }                   
	        
	        if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
	            this._getWindow_("priceCompWdw").close();
	        }
	        if (ajaxResult.options.options.doCloseNewWindowAfterSavee === true) {
	            this._getWindow_("priceCompWdwEdit").close();
	        }
			this._get_("priceCompList").store.load();
			this._get_("priceComponentConv").store.load();
	    }, this);
	
		contractPriceCmpnClassic.on("afterDoSaveSuccess", function (dc, ajaxResult) {
	        if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
	            this._getWindow_("priceCompWdw").close();
	        }
			this._get_("priceCompList").store.load();
			this._get_("priceComponentConv").store.load();
			this._getDc_("contractPriceCtgry").doReloadRecord();
			this._reloadContractEdit_ = true;
	    }, this);
	
		contractPriceCmpnClassic.on("afterDoDeleteSuccess", function () {
			this._get_("priceCompList").store.load();
			this._get_("priceComponentConv").store.load();
			this._reloadContractEdit_ = true;
	    }, this);
	
		pricingBase.mon(pricingBase.store, "update", function() {
				var view = this._get_("priceFixNew");
				if(view){
					var record = view._controller_.getRecord();
					view._applyStates_(record);
				}
		}, this);
	
		attachment.on("afterAttachmentWindowClose", function (){
			contractEdit.doReloadPage();
		},this);
	
		attachment.on("afterDoDeleteSuccess", function (){
			contractEdit.doReloadPage();
		},this);
	}

});
