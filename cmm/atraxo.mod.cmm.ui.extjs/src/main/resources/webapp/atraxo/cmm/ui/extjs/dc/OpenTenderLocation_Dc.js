/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.OpenTenderLocation_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.OpenTenderLocation_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.OpenTenderLocation_Ds
});

/* ================= GRID: SelectList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.OpenTenderLocation_Dc$SelectList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_OpenTenderLocation_Dc$SelectList",
	_noExport_: true,
	_noPaginator_: true,
	selType: null,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"locationName", width:120})
		.addTextColumn({ name:"flightServiceType", dataIndex:"flightServiceType", width:110})
		.addTextColumn({ name:"fuelProduct", dataIndex:"fuelProduct", width:90})
		.addTextColumn({ name:"deliveryPoint", dataIndex:"deliveryPoint", width:100})
		.addTextColumn({ name:"taxType", dataIndex:"taxType", width:140})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
