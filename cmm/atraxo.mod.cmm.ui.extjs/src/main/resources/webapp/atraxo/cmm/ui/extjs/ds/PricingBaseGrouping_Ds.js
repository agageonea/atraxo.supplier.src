/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.PricingBaseGrouping_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_PricingBaseGrouping_Ds"
	},
	
	
	initRecord: function() {
		this.set("decimals", 4);
		this.set("continous", true);
		this.set("includeInAverage", true);
		this.set("vat", "Not applicable");
		this.set("exchangeRateOffset", "Current");
		this.set("defaultPriceCtgy", true);
		this.set("quantityType", "Gross volume");
	},
	
	fields: [
		{name:"convFactor", type:"string", noFilter:true, noSort:true},
		{name:"used", type:"string", noFilter:true, noSort:true},
		{name:"density", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"density1", type:"string", noFilter:true, noSort:true},
		{name:"density2", type:"string", noFilter:true, noSort:true},
		{name:"contractId", type:"int", allowNull:true, noFilter:true},
		{name:"contractCode", type:"string"},
		{name:"contractStatus", type:"string"},
		{name:"contractValidFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"contractValidTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"quotId", type:"int", allowNull:true},
		{name:"quotName", type:"string"},
		{name:"calcVal", type:"string"},
		{name:"quotUnitId", type:"int", allowNull:true},
		{name:"quotUnitCode", type:"string"},
		{name:"quotUnitTypeInd", type:"string"},
		{name:"convUnitId", type:"int", allowNull:true},
		{name:"convUnitCode", type:"string"},
		{name:"convUnitTypeInd", type:"string"},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"quotationOffset", type:"string"},
		{name:"operator", type:"string"},
		{name:"factor", type:"float", allowNull:true},
		{name:"decimals", type:"int", allowNull:true},
		{name:"description", type:"string"},
		{name:"initialPrice", type:"float", allowNull:true},
		{name:"pcCurr", type:"int", allowNull:true},
		{name:"pcUnit", type:"int", allowNull:true},
		{name:"quantityType", type:"string"},
		{name:"continous", type:"boolean"},
		{name:"defaultPriceCtgy", type:"boolean"},
		{name:"restriction", type:"boolean"},
		{name:"exchangeRateOffset", type:"string"},
		{name:"initialFinancialSourceId", type:"int", allowNull:true},
		{name:"initialAverageMethodId", type:"int", allowNull:true},
		{name:"priceCatId", type:"int", allowNull:true},
		{name:"priceCatName", type:"string"},
		{name:"priceCatType", type:"string"},
		{name:"priceCatPricePer", type:"string"},
		{name:"quotAvgMethodId", type:"int", allowNull:true},
		{name:"quotAvgMethodCode", type:"string"},
		{name:"quotAvgMethodName", type:"string"},
		{name:"avgMTId", type:"int", allowNull:true},
		{name:"avgMTCode", type:"string"},
		{name:"avgMTName", type:"string"},
		{name:"calculatedDensity", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"name", type:"string"},
		{name:"includeInAverage", type:"boolean"},
		{name:"vat", type:"string"},
		{name:"comments", type:"string"},
		{name:"forex", type:"string"},
		{name:"suppId", type:"int", allowNull:true, noFilter:true},
		{name:"suppCode", type:"string", noFilter:true},
		{name:"locId", type:"int", allowNull:true},
		{name:"locCode", type:"string", noFilter:true},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"mainCategoryCode", type:"string"},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceName", type:"string"},
		{name:"financialSourceCode", type:"string"},
		{name:"avgMethodIndicatorDefMeth", type:"boolean"},
		{name:"avgMethodIndicatorName", type:"string"},
		{name:"avgMthdId", type:"int", allowNull:true},
		{name:"pcompProvisional", type:"boolean"},
		{name:"pcompWithHold", type:"boolean"},
		{name:"pcCurrCode", type:"string"},
		{name:"pcUnitCode", type:"string"},
		{name:"value", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"formTitle", type:"string", noFilter:true, noSort:true},
		{name:"price", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"convertTitle", type:"string", noFilter:true, noSort:true},
		{name:"convertedPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"used1", type:"boolean", noFilter:true, noSort:true},
		{name:"initialPriceTitle", type:"string", noFilter:true, noSort:true},
		{name:"slash", type:"string", noFilter:true, noSort:true},
		{name:"percentageOf", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.PricingBaseGrouping_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"convFactor", type:"string", noFilter:true, noSort:true},
		{name:"used", type:"string", noFilter:true, noSort:true},
		{name:"density", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"density1", type:"string", noFilter:true, noSort:true},
		{name:"density2", type:"string", noFilter:true, noSort:true},
		{name:"contractId", type:"int", allowNull:true, noFilter:true},
		{name:"contractCode", type:"string"},
		{name:"contractStatus", type:"string"},
		{name:"contractValidFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"contractValidTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"quotId", type:"int", allowNull:true},
		{name:"quotName", type:"string"},
		{name:"calcVal", type:"string"},
		{name:"quotUnitId", type:"int", allowNull:true},
		{name:"quotUnitCode", type:"string"},
		{name:"quotUnitTypeInd", type:"string"},
		{name:"convUnitId", type:"int", allowNull:true},
		{name:"convUnitCode", type:"string"},
		{name:"convUnitTypeInd", type:"string"},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"quotationOffset", type:"string"},
		{name:"operator", type:"string"},
		{name:"factor", type:"float", allowNull:true},
		{name:"decimals", type:"int", allowNull:true},
		{name:"description", type:"string"},
		{name:"initialPrice", type:"float", allowNull:true},
		{name:"pcCurr", type:"int", allowNull:true},
		{name:"pcUnit", type:"int", allowNull:true},
		{name:"quantityType", type:"string"},
		{name:"continous", type:"boolean", allowNull:true},
		{name:"defaultPriceCtgy", type:"boolean", allowNull:true},
		{name:"restriction", type:"boolean", allowNull:true},
		{name:"exchangeRateOffset", type:"string"},
		{name:"initialFinancialSourceId", type:"int", allowNull:true},
		{name:"initialAverageMethodId", type:"int", allowNull:true},
		{name:"priceCatId", type:"int", allowNull:true},
		{name:"priceCatName", type:"string"},
		{name:"priceCatType", type:"string"},
		{name:"priceCatPricePer", type:"string"},
		{name:"quotAvgMethodId", type:"int", allowNull:true},
		{name:"quotAvgMethodCode", type:"string"},
		{name:"quotAvgMethodName", type:"string"},
		{name:"avgMTId", type:"int", allowNull:true},
		{name:"avgMTCode", type:"string"},
		{name:"avgMTName", type:"string"},
		{name:"calculatedDensity", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"name", type:"string"},
		{name:"includeInAverage", type:"boolean", allowNull:true},
		{name:"vat", type:"string"},
		{name:"comments", type:"string"},
		{name:"forex", type:"string"},
		{name:"suppId", type:"int", allowNull:true, noFilter:true},
		{name:"suppCode", type:"string", noFilter:true},
		{name:"locId", type:"int", allowNull:true},
		{name:"locCode", type:"string", noFilter:true},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"mainCategoryCode", type:"string"},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceName", type:"string"},
		{name:"financialSourceCode", type:"string"},
		{name:"avgMethodIndicatorDefMeth", type:"boolean", allowNull:true},
		{name:"avgMethodIndicatorName", type:"string"},
		{name:"avgMthdId", type:"int", allowNull:true},
		{name:"pcompProvisional", type:"boolean", allowNull:true},
		{name:"pcompWithHold", type:"boolean", allowNull:true},
		{name:"pcCurrCode", type:"string"},
		{name:"pcUnitCode", type:"string"},
		{name:"value", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"formTitle", type:"string", noFilter:true, noSort:true},
		{name:"price", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"convertTitle", type:"string", noFilter:true, noSort:true},
		{name:"convertedPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"used1", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"initialPriceTitle", type:"string", noFilter:true, noSort:true},
		{name:"slash", type:"string", noFilter:true, noSort:true},
		{name:"percentageOf", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.cmm.ui.extjs.ds.PricingBaseGrouping_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"isDefault", type:"boolean"},
		{name:"priceDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"quotation", type:"string"},
		{name:"settlementCurrencyCode", type:"string"},
		{name:"settlementCurrencyId", type:"int", allowNull:true},
		{name:"settlementUnitCode", type:"string"},
		{name:"total", type:"float", allowNull:true}
	]
});
