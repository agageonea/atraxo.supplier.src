/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
__CMM_TYPE__ = {
	DealType : {
		_BUY_ : "Buy" , 
		_SELL_ : "Sell" 
	},
	ContractType : {
		_PRODUCT_ : "Product" , 
		_FUELING_SERVICE_ : "Fueling Service" , 
		_GENERAL_SERVICE_ : "General Service" , 
		_INSPECTION_ : "Inspection" , 
		_STORAGE_ : "Storage" , 
		_CSO_ : "CSO" 
	},
	ContractSubType : {
		_NA_ : "Unspecified" , 
		_INTO_PLANE_ : "Into plane" , 
		_EX_HYDRANT_ : "Ex-Hydrant" , 
		_AIRLINE_SUPPLY_ : "Airline Supply" , 
		_INTO_STORAGE_ : "Into Storage" , 
		_SERVICE_ : "Service" 
	},
	FuelingType : {
		_RE_FUELER_ : "Re-Fueler" , 
		_HYDRANT_ : "Hydrant" 
	},
	FuelTicketFuelingOperation : {
		_UPLIFT_ : "Uplift" , 
		_PRE_FUEL_ : "Pre-fuel" , 
		_RE_FUEL_ : "Re-fuel" , 
		_DE_FUEL_ : "De-fuel" , 
		_FUEL_DUMP_ : "Fuel dump" , 
		_DE_FUEL_TO_DRUM_ : "De-fuel to drum" , 
		_FUEL_INTO_DRUM_ : "Fuel into drum" , 
		_FUEL_INTO_TRUCK_ : "Fuel into truck" 
	},
	ContractScope : {
		_INTO_PLANE_ : "Into plane" , 
		_NON_INTO_PLANE_ : "Non into plane" 
	},
	IsSpot : {
		_TERM_ : "Term" , 
		_SPOT_ : "Spot" 
	},
	ContractStatus : {
		_DRAFT_ : "Draft" , 
		_ACTIVE_ : "Active" , 
		_EFFECTIVE_ : "Effective" , 
		_EXPIRED_ : "Expired" 
	},
	FlightTypeIndicator : {
		_UNSPECIFIED_ : "Unspecified" , 
		_DOMESTIC_ : "Domestic" , 
		_INTERNATIONAL_ : "International" 
	},
	Product : {
		_JET_A_ : "Jet-A" , 
		_JET_A1_ : "Jet-A1" , 
		_TS_1_ : "TS-1" , 
		_F_34_ : "F-34" , 
		_JP_8_ : "JP-8" , 
		_RP_4_ : "RP-4" , 
		_T1_ : "T1" , 
		_A1_BIO_ : "A1-BIO" , 
		_CAT_2_ : "Cat 2" , 
		_AVGAS_ : "AVGAS" , 
		_AG_1_HL_ : "AG-1-HL" , 
		_AG_80_ : "AG-80" , 
		_JA_1_A_ : "JA-1-A" , 
		_JAA_ : "Jaa" , 
		_JP_5_ : "JP-5" , 
		_NON_ : "NON" , 
		_OTH_ : "OTH" 
	},
	TaxType : {
		_UNSPECIFIED_ : "Unspecified" , 
		_BONDED_ : "Bonded" , 
		_DOMESTIC_ : "Domestic" , 
		_FOREIGN_TRADE_ZONE_ : "Foreign trade zone" , 
		_DUTY_FREE_ : "Duty free" , 
		_DUTY_PAID_ : "Duty paid" , 
		_OTHER_ : "Other" 
	},
	QuantityType : {
		_NET_VOLUME_ : "Net volume" , 
		_GROSS_VOLUME_ : "Gross volume" 
	},
	Period : {
		_CONTRACT_PERIOD_ : "Contract period" , 
		_MONTHLY_ : "Monthly" , 
		_YEARLY_ : "Yearly" 
	},
	PricingMethod : {
		_INDEX_ : "Index" , 
		_FIXED_ : "Fixed" 
	},
	InvoicePayableBy : {
		_HEADQUARTER_ : "Headquarter" , 
		_LOCAL_OFFICE_ : "Local office" 
	},
	VatApplicability : {
		_ALL_EVENTS_ : "All events" , 
		_DOMESTIC_EVENTS_ : "Domestic events" , 
		_INTERNATIONAL_EVENTS_ : "International events" , 
		_NOT_APPLICABLE_ : "Not applicable" 
	},
	ReviewPeriod : {
		_NO_ : "No" , 
		_WEEKLY_ : "Weekly" , 
		_SEMIMONTHLY_ : "Semimonthly" , 
		_MONTHLY_ : "Monthly" , 
		_QUARTERLY_ : "Quarterly" , 
		_SEMIANNUALLY_ : "Semiannually" , 
		_ANNUALLY_ : "Annually" 
	},
	EventType : {
		_TRANSPORT_ : "Transport" , 
		_PERIOD_ : "Period" , 
		_MOVEMENT_ : "Movement" , 
		_CSO_EVENT_ : "CSO event" 
	},
	YesNo : {
		_True_ : "Yes" , 
		_False_ : "No" 
	},
	Operators : {
		_EQUAL_ : "=" , 
		_LOWER_ : "<" , 
		_HIGHER_ : ">" , 
		_LOWER_EQ_ : "<=" , 
		_HIGHER_EQ_ : ">=" , 
		_IN_ : "IN" , 
		_NOT_EQUAL_ : "<>" 
	},
	RestrictionTypes : {
		_VOLUME_ : "Volume" , 
		_AIRCRAFT_TYPE_ : "Aircraft type" , 
		_FUELING_TYPE_ : "Fueling type" , 
		_TRANSPORT_TYPE_ : "Transport type" , 
		_FLIGHT_TYPE_ : "Flight type" , 
		_REGISTRATION_NUMBER_ : "Registration number" , 
		_TIME_OF_OPERATION_ : "Time of operation" , 
		_SHIP_TO_ : "Ship to" 
	},
	BillStatus : {
		_NOT_BILLED_ : "Not billed" , 
		_DRAFT_ : "Draft" , 
		_CHECK_FAILED_ : "Check failed" , 
		_CHECK_PASSED_ : "Check passed" , 
		_NEEDS_RECHECK_ : "Needs recheck" , 
		_AWAITING_APPROVAL_ : "Awaiting approval" , 
		_AWAITING_PAYMENT_ : "Awaiting payment" , 
		_PAID_ : "Paid" , 
		_NO_CONTRACT_ : "No Contract" , 
		_CREDITED_ : "Credited" 
	},
	CalculateIndicator : {
		_INCLUDED_ : "Included" , 
		_CALCULATE_ONLY_ : "Calculate only" 
	},
	IATAServiceLevel : {
		_IATA_SERVICE_LEVEL_1_ : "IATA Service level 1" , 
		_IATA_SERVICE_LEVEL_2_ : "IATA Service level 2" , 
		_IATA_SERVICE_LEVEL_3_ : "IATA Service level 3" , 
		_IATA_SERVICE_LEVEL_4_ : "IATA Service level 4" 
	},
	FuelDelivery : {
		_EX_HIDRANT_ : "Ex hidrant" , 
		_INTO_PLANE_ : "Into Plane" , 
		_INTO_STORAGE_ : "Into Storage" , 
		_FUELING_SERVICE_ : "Fueling Service" 
	},
	BusinessType : {
		_SUPPLIER_ : "Supplier" , 
		_AIRLINE_ : "Airline" 
	},
	PricingType : {
		_INDEX_ : "Index" , 
		_MARKET_ : "Market" 
	},
	PaymentChoise : {
		_OPEN_CREDIT_ : "Open credit" , 
		_PREPAYMENT_ : "Prepayment" 
	},
	TenderType : {
		_PRODUCT_ : "Product" , 
		_FUELING_SERVICE_ : "Fueling Service" 
	},
	TenderStatus : {
		_DRAFT_ : "Draft" , 
		_NEW_ : "New" , 
		_CANCELED_ : "Canceled" , 
		_UPDATED_ : "Updated" , 
		_EXPIRED_ : "Expired" 
	},
	BiddingStatus : {
		_NEW_ : "New" , 
		_IN_NEGOTIATION_ : "In negotiation" , 
		_FINISHED_ : "Finished" , 
		_NO_BID_ : "No bid" 
	},
	BidStatus : {
		_DRAFT_ : "Draft" , 
		_SUBMITTED_ : "Submitted" , 
		_AWARDED_ : "Awarded" , 
		_DECLINED_ : "Declined" , 
		_CANCELLED_ : "Cancelled" , 
		_AWARD_ACCEPTED_ : "Award accepted" , 
		_AWARD_DECLINED_ : "Award declined" 
	},
	TransmissionStatus : {
		_NEW_ : "New" , 
		_IN_PROGRESS_ : "In progress" , 
		_TRANSMITTED_ : "Transmitted" , 
		_FAILED_ : "Failed" , 
		_RECEIVED_ : "Received" , 
		_READ_ : "Read" , 
		_PROCESSED_ : "Processed" 
	},
	BidApprovalStatus : {
		_NEW_ : "New" , 
		_AWAITING_APPROVAL_ : "Awaiting approval" , 
		_APPROVED_ : "Approved" , 
		_REJECTED_ : "Rejected" 
	},
	TenderSource : {
		_CAPTURED_ : "Captured" , 
		_IMPORTED_ : "Imported" 
	},
	SourceType : {
		_EX_REFINERY_ : "Ex-Refinery" , 
		_GOVERNMENT_ : "Government" 
	},
	PaymentType : {
		_CONTRACT_ : "Contract" , 
		_FUEL_CARD_ : "Fuel Card" , 
		_CREDIT_CARD_ : "Credit Card" , 
		_CASH_ : "Cash" 
	},
	ExportTypeBid : {
		_PDF_ : "PDF" , 
		_XML_ : "XML" , 
		_XLSX_ : "XLSX" 
	},
	FlightServiceType : {
		_AD_HOC_ : "Ad-hoc" , 
		_SCHEDULED_ : "Scheduled" , 
		_GENERAL_AVIATION_ : "General Aviation" 
	},
	ProductIATA : {
		_ADT_ : "ADT" , 
		_ADTV_ : "ADTV" , 
		_AG100_ : "AG100" , 
		_AG1HL_ : "AG1HL" , 
		_AG80_ : "AG80" , 
		_AHF_ : "AHF" , 
		_AIF_ : "AIF" , 
		_APT_ : "APT" , 
		_ATT_ : "ATT" , 
		_CCF_ : "CCF" , 
		_CHN_ : "CHN" , 
		_CNF_ : "CNF" , 
		_CNT_ : "CNT" , 
		_COM_ : "COM" , 
		_CON_ : "CON" , 
		_CSF_ : "CSF" , 
		_CUF_ : "CUF" , 
		_DEF_ : "DEF" , 
		_DEH_ : "DEH" , 
		_DIF_ : "DIF" , 
		_DIS_ : "DIS" , 
		_DMF_ : "DMF" , 
		_ENV_ : "ENV" , 
		_EWF_ : "EWF" , 
		_FLF_ : "FLF" , 
		_FMJ_ : "FMJ" , 
		_FRF_ : "FRF" , 
		_GAF_ : "GAF" , 
		_GSF_ : "GSF" , 
		_HNG_ : "HNG" , 
		_HUF_ : "HUF" , 
		_HYD_ : "HYD" , 
		_INF_ : "INF" , 
		_INS_ : "INS" , 
		_ITP_ : "ITP" , 
		_JA1A_ : "JA1A" , 
		_JAA_ : "JAA" , 
		_JETA_ : "JETA" , 
		_JETA1_ : "JETA1" , 
		_JP4_ : "JP4" , 
		_JP5_ : "JP5" , 
		_JP8_ : "JP8" , 
		_JSF_ : "JSF" , 
		_LVF_ : "LVF" , 
		_MVF_ : "MVF" , 
		_NFF_ : "NFF" , 
		_NON_ : "NON" , 
		_OFF_ : "OFF" , 
		_OTH_ : "OTH" , 
		_OVT_ : "OVT" , 
		_OWF_ : "OWF" , 
		_PLF_ : "PLF" , 
		_REB_ : "REB" , 
		_STR_ : "STR" , 
		_SYS_ : "SYS" , 
		_THP_ : "THP" , 
		_TPF_ : "TPF" , 
		_TRF_ : "TRF" , 
		_TS1_ : "TS1" 
	},
	PendingAction : {
		_ADD_ : "Add" , 
		_UPDATE_ : "Update" , 
		_DELETE_ : "Delete" 
	},
	ImportBidFileType : {
		_XLS_ : "XLS" , 
		_XML_ : "XML" 
	},
	PriceValueType : {
		_HIGH_PRICE_ : "High price" , 
		_LOW_PRICE_ : "Low price" , 
		_MEAN_PRICE_ : "Mean price" , 
		_USER_CALCULATED_PRICE_ : "User calculated price" 
	}
}
