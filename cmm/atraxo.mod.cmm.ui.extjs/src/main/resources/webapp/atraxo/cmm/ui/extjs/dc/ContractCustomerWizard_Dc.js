/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.ContractCustomerWizard_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.ContractCustomerWizard_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.ContractCustomerWizard_Ds
});

/* ================= EDIT FORM: wizardHeader ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractCustomerWizard_Dc$wizardHeader", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractCustomerWizard_Dc$wizardHeader",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLabel({ name:"phase1"})
		.addLabel({ name:"phase2"})
		.addLabel({ name:"phase3"})
		.addLabel({ name:"phase4"})
		.addLabel({ name:"phase5"})
		/* =========== containers =========== */
		.addPanel({name:"main"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addWizardLabels("main", ["phase1", "phase2", "phase3", "phase4", "phase5"]);
	}
});

/* ================= EDIT FORM: NewContractStep1 ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractCustomerWizard_Dc$NewContractStep1", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractCustomerWizard_Dc$NewContractStep1",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"customerName", bind:"{d.customerName}", dataIndex:"customerName", width:200, xtype:"fmbas_CustomerCodeExtendedLov_Lov", maxLength:100, labelAlign:"top",
			retFieldMapping: [{lovField:"id", dsField: "customerId"} ,{lovField:"id", dsField: "billToId"} ,{lovField:"id", dsField: "riskHolderId"} ,{lovField:"code", dsField: "customerCode"} ,{lovField:"code", dsField: "billToCode"} ,{lovField:"code", dsField: "riskHolderCode"} ],listeners:{
			change:{scope:this, fn:this._enableContinue_},
			fpchange:{scope:this, fn:this._setDefaultValues_}
		}})
		.addLov({name:"contractHolder", bind:"{d.holderName}", dataIndex:"holderName", width:200, xtype:"fmbas_UserSubsidiaryNameLov_Lov", maxLength:100, labelAlign:"top",
			retFieldMapping: [{lovField:"subsidiaryId", dsField: "holderId"} ,{lovField:"code", dsField: "holderCode"} ],listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addLov({name:"billToCode", bind:"{d.billToCode}", dataIndex:"billToCode", width:200, xtype:"fmbas_CustomerGroupLov_Lov", maxLength:32, labelAlign:"top", pageSize:0,
			retFieldMapping: [{lovField:"id", dsField: "billToId"} ],
			filterFieldMapping: [{lovField:"id", dsField: "customerId"}, {lovField:"parentGroupId", dsField: "customerParentId"} ],listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addLov({name:"riskHolderCode", bind:"{d.riskHolderCode}", dataIndex:"riskHolderCode", width:200, xtype:"fmbas_CustomerGroupLov_Lov", maxLength:32, labelAlign:"top", pageSize:0,
			retFieldMapping: [{lovField:"id", dsField: "riskHolderId"} ],
			filterFieldMapping: [{lovField:"id", dsField: "customerId"}, {lovField:"parentGroupId", dsField: "customerParentId"} ],listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  style:"padding: 0px 20px"})
		.addPanel({ name:"col1", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["contractHolder", "customerName", "billToCode", "riskHolderCode"]);
	},
	/* ==================== Business functions ==================== */
	
	_setDefaultValues_: function(cmp,newV,oldV) {
		
						if( !Ext.isEmpty(newV) ){
							var ctx = this;
							var f = function() {
								var ctrl = this._controller_;
								var frame = ctrl.getFrame();
								frame.setNewDefaultValues();
							}
							Ext.defer(f,500,this);
						}
	},
	
	_enableContinue_: function() {
		
		
						var dc = this._controller_;
						var frame= dc.getFrame();
						var btn = frame._get_("btnContinueStep1");
						var ctx = this;
						var r = dc.getRecord();
						
						var f = function() {
		
							if (!r) {
								return;
							}
							var field = ["customerName","holderName","billToCode","riskHolderCode"];
							var dirtyFields = [];
		
							for (var i = 0; i < field.length; i++) {
								if (Ext.isEmpty(r.get(field[i]))) {							
									dirtyFields.push(field[i]);
								}
								else {
									var index = dirtyFields.indexOf(field[i]);
									if (index > -1) {
									    dirtyFields.splice(index, 1);
									}
								}					
							}
							if (dirtyFields.length > 0) {
								btn._disable_();
							}
							else {
								btn._enable_();
							}
						}
						Ext.defer(f, 50, this);
						
	}
});

/* ================= EDIT FORM: NewContractStep2 ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractCustomerWizard_Dc$NewContractStep2", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractCustomerWizard_Dc$NewContractStep2",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"locationCode", bind:"{d.locationCode}", dataIndex:"locationCode", width:260, xtype:"fmbas_LocationsLov_Lov", maxLength:25, labelAlign:"top",
			retFieldMapping: [{lovField:"id", dsField: "locationId"} ,{lovField:"code", dsField: "locationCode"} ],
			filterFieldMapping: [{lovField:"isAirport", value: "true"} ],listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addDateField({name:"contractValidFrom", bind:"{d.validFrom}", dataIndex:"validFrom", width:120, noLabel: true,listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addDateField({name:"contractValidTo", bind:"{d.validTo}", dataIndex:"validTo", width:120, noLabel: true,listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.add({name:"period", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("contractValidFrom"),this._getConfig_("contractValidTo")]})
		.addCombo({ xtype:"combo", name:"product", bind:"{d.product}", dataIndex:"product", width:185, store:[ __CMM_TYPE__.Product._JET_A_, __CMM_TYPE__.Product._JET_A1_, __CMM_TYPE__.Product._TS_1_, __CMM_TYPE__.Product._F_34_, __CMM_TYPE__.Product._JP_8_, __CMM_TYPE__.Product._RP_4_, __CMM_TYPE__.Product._T1_, __CMM_TYPE__.Product._A1_BIO_, __CMM_TYPE__.Product._CAT_2_, __CMM_TYPE__.Product._AVGAS_, __CMM_TYPE__.Product._AG_1_HL_, __CMM_TYPE__.Product._AG_80_, __CMM_TYPE__.Product._JA_1_A_, __CMM_TYPE__.Product._JAA_, __CMM_TYPE__.Product._JP_5_, __CMM_TYPE__.Product._NON_, __CMM_TYPE__.Product._OTH_], labelAlign:"top",listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addDisplayFieldText({ name:"periodSeparator", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding: 0px 0px 0px 5px"})
		.addDisplayFieldText({ name:"periodLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelAlign:"left", style:"float: left"})
		.addCombo({ xtype:"combo", name:"eventType", bind:"{d.eventType}", dataIndex:"eventType", width:185, store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_], labelAlign:"top",listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addDisplayFieldText({ name:"contractedVolumeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelAlign:"left", style:"float: left; white-space: nowrap", labelStyle:"width:300px"})
		.addNumberField({name:"awardedVolume", bind:"{d.awardedVolume}", dataIndex:"awardedVolume", width:100, noLabel: true, maxLength:19, style:"margin-right:5px", decimals:2,listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addLov({name:"contractVolumeUnitCode", bind:"{d.contractVolumeUnitCode}", dataIndex:"contractVolumeUnitCode", width:80, noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "settlementUnitId"} ,{lovField:"code", dsField: "initialUnit"} ],listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addCombo({ xtype:"combo", name:"quantityType", bind:"{d.quantityType}", dataIndex:"quantityType", width:130, noLabel: true, store:[ __CMM_TYPE__.QuantityType._NET_VOLUME_, __CMM_TYPE__.QuantityType._GROSS_VOLUME_], style:"margin-left:5px",listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addCombo({ xtype:"combo", name:"contractPeriod", bind:"{d.period}", dataIndex:"period", width:130, noLabel: true, store:[ __CMM_TYPE__.Period._CONTRACT_PERIOD_, __CMM_TYPE__.Period._MONTHLY_, __CMM_TYPE__.Period._YEARLY_],listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addDisplayFieldText({ name:"volumePeriodSeparator", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding: 0px 0px 0px 5px"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  style:"padding: 0px 20px"})
		.addPanel({ name:"row1", width:300, layout:"anchor"})
		.addPanel({ name:"row2", width:300, layout:"anchor"})
		.addPanel({ name:"row3", layout: {type:"table"}})
		.addPanel({ name:"row4", width:300, layout:"anchor"})
		.addPanel({ name:"row5", width:300, layout:"anchor"})
		.addPanel({ name:"row6", layout: {type:"table"}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["row1", "row2", "row3", "row4", "row5", "row6"])
		.addChildrenTo("row1", ["locationCode"])
		.addChildrenTo("row2", ["periodLabel"])
		.addChildrenTo("row3", ["contractValidFrom", "periodSeparator", "contractValidTo"])
		.addChildrenTo("row4", ["product", "eventType"])
		.addChildrenTo("row5", ["contractedVolumeLabel"])
		.addChildrenTo("row6", ["awardedVolume", "contractVolumeUnitCode", "quantityType", "volumePeriodSeparator", "contractPeriod"]);
	},
	/* ==================== Business functions ==================== */
	
	_checkPeriod_: function() {
		
		
						var dc = this._controller_;
						var r = dc.getRecord();
						if (r) {
							var validFrom = r.get("validFrom");
							var validTo = r.get("validTo");
							if (!Ext.isEmpty(validFrom) && !Ext.isEmpty(validTo)) {
								if (validFrom > validTo) {
									Main.warning("Valid from cannot be greater than Valid to!");
									r.beginEdit();
										r.set("validFrom",null);
										r.set("validTo",null);
									r.endEdit();
								}	
							}
						}
							
	},
	
	_enableContinue_: function(el) {
		
		
						var dc = this._controller_;
						var frame= dc.getFrame();
						var btn = frame._get_("btnContinueStep2");
						var ctx = this;
						var r = dc.getRecord();
						
						var f = function() {
							var field = ["locationCode","contractValidFrom","contractValidTo","product","eventType","awardedVolume","contractVolumeUnitCode","quantityType","contractPeriod"];
							var dirtyFields = [];
		
							if (el && (el.dataIndex === "validFrom" || el.dataIndex === "validTo")) {	
								this._checkPeriod_();
							}
		
							for (var i = 0; i < field.length; i++) {
								if (Ext.isEmpty(ctx._get_(field[i]).getValue())) {							
									dirtyFields.push(field[i]);
								}
								else {
									var index = dirtyFields.indexOf(field[i]);
									if (index > -1) {
									    dirtyFields.splice(index, 1);
									}
								}					
							}
							if (dirtyFields.length > 0) {
								btn._disable_();
							}
							else {
								btn._enable_();
							}
						}
						Ext.defer(f, 50, this);
						
	}
});

/* ================= EDIT FORM: paymentTermsRefDay ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractCustomerWizard_Dc$paymentTermsRefDay", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractCustomerWizard_Dc$paymentTermsRefDay",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"paymentTermsLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelAlign:"left"})
		.addDisplayFieldText({ name:"daysLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelAlign:"left", style:"margin-left:5px"})
		.addNumberField({name:"paymentTerms", bind:"{d.paymentTerms}", dataIndex:"paymentTerms", width:120, noLabel: true, maxLength:4, cls:"number-with-spinner", minValue:0, maxValue:99999,listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addCombo({ xtype:"combo", name:"paymentRefDay", bind:"{d.paymentRefDay}", dataIndex:"paymentRefDay", store:[ __FMBAS_TYPE__.PaymentDay._INVOICE_DATE_, __FMBAS_TYPE__.PaymentDay._INVOICE_RECEIVING_DATE_, __FMBAS_TYPE__.PaymentDay._LAST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._FIRST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._MIDPOINT_DELIVERY_DATE_], labelAlign:"top",listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.add({name:"termsDays", xtype: "fieldcontainer", layout: {type:"hbox"}, noLabel: true, items: [this._getConfig_("paymentTerms"),this._getConfig_("daysLabel")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:170, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["paymentTermsLabel", "termsDays", "paymentRefDay"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableContinue_: function(el) {
		
		
						var dc = this._controller_;
						var frame= dc.getFrame();
						var btn = frame._get_("btnContinueStep3");
						var ctx = this;
						var r = dc.getRecord();
						
						var f = function() {
							if (!r) {
								return;
							}
		
							var field = ["paymentTerms","paymentRefDay","creditTerms","prepaidDays","prepaiedAmount","paymentFreq","prepayFirstDeliveryDate","creditTerms"];
							var dirtyFields = [];
		
							if (el && el.dataIndex === "creditTerms") {	
								this._showHideFields_();
							}
							if (r.get("creditTerms") !== __FMBAS_TYPE__.CreditTerm._PREPAYMENT_) {
								field = ["paymentTerms","paymentRefDay","creditTerms"];
							}
		
							for (var i = 0; i < field.length; i++) {
								if (Ext.isEmpty(r.get(field[i]))) {							
									dirtyFields.push(field[i]);
								}
								else {
									var index = dirtyFields.indexOf(field[i]);
									if (index > -1) {
									    dirtyFields.splice(index, 1);
									}
								}					
							}
							if (dirtyFields.length > 0) {
								btn._disable_();
							}
							else {
								btn._enable_();
							}
						}
						Ext.defer(f, 50, this);
						
	}
});

/* ================= EDIT FORM: NewContractStep3 ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractCustomerWizard_Dc$NewContractStep3", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractCustomerWizard_Dc$NewContractStep3",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btnTogglePaymentTerms", scope: this, handler: this._togglePaymentTerms_, text: "...", style:"margin-left:3px"})
		.addNumberField({name:"paymentTerms", bind:"{d.paymentTerms}", dataIndex:"paymentTerms", maxLength:4})
		.addCombo({ xtype:"combo", name:"paymentRefDay", bind:"{d.paymentRefDay}", dataIndex:"paymentRefDay", width:280, store:[ __FMBAS_TYPE__.PaymentDay._INVOICE_DATE_, __FMBAS_TYPE__.PaymentDay._INVOICE_RECEIVING_DATE_, __FMBAS_TYPE__.PaymentDay._LAST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._FIRST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._MIDPOINT_DELIVERY_DATE_]})
		.addPopoverTextField({name:"paymentTermRefDay", width:250, noLabel: true, hPos:"r", vPos:"b", labelWidth:120, _skipMask_:['paymentTerms'], fieldsList:["paymentTerms","paymentRefDay"], formatList:[], editorForm:"cmm_ContractCustomerWizard_Dc$paymentTermsRefDay", fieldsValueSeparator:" days from "})
		.add({name:"paymentTermsAndButton", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("paymentTermRefDay"),this._getConfig_("btnTogglePaymentTerms")]})
		.addDisplayFieldText({ name:"paymentTermsLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelAlign:"left", style:"float: left; white-space: nowrap", labelStyle:"width:300px"})
		.addDisplayFieldText({ name:"bankGuaranteeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float: right; white-space: nowrap; padding-left:80px"})
		.addDisplayFieldText({ name:"creditTermsLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelAlign:"left", style:"float: left; white-space: nowrap", labelStyle:"width:300px"})
		.addCombo({ xtype:"combo", name:"creditTerms", bind:"{d.creditTerms}", dataIndex:"creditTerms", noLabel: true, store:[ __FMBAS_TYPE__.CreditTerm._OPEN_CREDIT_, __FMBAS_TYPE__.CreditTerm._CREDIT_LINE_, __FMBAS_TYPE__.CreditTerm._BANK_GUARANTEE_, __FMBAS_TYPE__.CreditTerm._PREPAYMENT_], labelAlign:"top",listeners:{
			change:{scope:this, fn:this._enableContinue_},
			focus:{scope:this, fn:function() {this._excludeBankGuarantee_()}}
		}})
		.addToggleField({ name:"bankGuarantee", bind:"{d.bankGuarantee}", dataIndex:"bankGuarantee", style:"padding-left:80px"})
		.addNumberField({name:"prepaidDays", bind:"{d.prepaidDays}", dataIndex:"prepaidDays", maxLength:4, labelAlign:"top", colspan:2, cls:"number-with-spinner", minValue:0, maxValue:99999,listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addNumberField({name:"prepaiedAmount", bind:"{d.prepaiedAmount}", dataIndex:"prepaiedAmount", maxLength:21, labelAlign:"top", decimals:2,listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addCombo({ xtype:"combo", name:"paymentFreq", bind:"{d.paymentFreq}", dataIndex:"paymentFreq", store:[ __FMBAS_TYPE__.InvoiceFreq._WEEKLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIMONTHLY_, __FMBAS_TYPE__.InvoiceFreq._MONTHLY_, __FMBAS_TYPE__.InvoiceFreq._QUARTERLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._ANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._PER_DELIVERY_, __FMBAS_TYPE__.InvoiceFreq._DAILY_, __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_], labelAlign:"top", style:"padding-left:80px",listeners:{
			expand:{scope:this, fn:this._filterInvoiceFreq_},
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addNumberField({name:"prepayFirstDeliveryDate", bind:"{d.prepayFirstDeliveryDate}", dataIndex:"prepayFirstDeliveryDate", width:60, noLabel: true, maxLength:4, cls:"number-with-spinner", minValue:0, maxValue:99999,listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addDisplayFieldText({ name:"firstDeliveryLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelAlign:"left", style:"float: left; white-space: nowrap", labelStyle:"width:300px"})
		.addDisplayFieldText({ name:"daysLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"margin-left: 5px"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  style:"padding: 0px 20px"})
		.addPanel({ name:"row1", width:300, layout:"anchor"})
		.addPanel({ name:"row2", width:300, layout:"anchor"})
		.addPanel({ name:"row3", layout: {type:"table", columns:2}})
		.addPanel({ name:"row4",  style:"margin-top: 20px", layout: {type:"table"}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["row1", "row2", "row3", "row4"])
		.addChildrenTo("row1", ["paymentTermsLabel", "paymentTermsAndButton"])
		.addChildrenTo("row2", ["creditTermsLabel"])
		.addChildrenTo("row3", ["creditTerms", "bankGuarantee", "prepaidDays", "prepaiedAmount", "paymentFreq"])
		.addChildrenTo("row4", ["firstDeliveryLabel", "prepayFirstDeliveryDate", "daysLabel"]);
	},
	/* ==================== Business functions ==================== */
	
	_excludeBankGuarantee_: function() {
		
		
						var dc = this._controller_;
						var store = this._get_("creditTerms").getStore();
		
						store.filterBy(function (record) {
		                	if (record.get("field1") !== __FMBAS_TYPE__.CreditType._BANK_GUARANTEE_) return record;
		                });
	},
	
	_enableContinue_: function(el,newVal,oldVal) {
		
		
						var dc = this._controller_;
						var frame= dc.getFrame();
						var btn = frame._get_("btnContinueStep3");
						var ctx = this;
						var r = dc.getRecord();
						
						var f = function() {
							if (!r) {
								return;
							}
		
							var field = ["paymentTerms","paymentRefDay","creditTerms","prepaidDays","prepaiedAmount","paymentFreq","prepayFirstDeliveryDate","creditTerms"];
							var dirtyFields = [];
		
							if (el && el.dataIndex === "creditTerms") {	
								this._showHideFields_();
							}
							if (r.get("creditTerms") !== __FMBAS_TYPE__.CreditTerm._PREPAYMENT_) {
								field = ["paymentTerms","paymentRefDay","creditTerms"];
							}
		
							for (var i = 0; i < field.length; i++) {
								if (Ext.isEmpty(r.get(field[i]))) {							
									dirtyFields.push(field[i]);
								}
								else {
									var index = dirtyFields.indexOf(field[i]);
									if (index > -1) {
									    dirtyFields.splice(index, 1);
									}
								}					
							}
							if (dirtyFields.length > 0) {
								btn._disable_();
							}
							else {
								btn._enable_();
							}
						}
						Ext.defer(f, 50, this);
						
	},
	
	_showHideFields_: function() {
		
						var el = this._get_("creditTerms");
						var val = el.getValue();
						var elems = ["prepaidDays","prepaiedAmount","paymentFreq","firstDeliveryLabel", "prepayFirstDeliveryDate", "daysLabel"];
						var ctx = this;
		
						if (!Ext.isEmpty(val)) {
							Ext.each(elems, function(e) {
								if (val === __FMBAS_TYPE__.CreditTerm._OPEN_CREDIT_ || val === __FMBAS_TYPE__.CreditTerm._CREDIT_LINE_) {
									ctx._get_(e).hide();
								}
								else {
									ctx._get_(e).show();
								}
							}, this);
						}
						else {
							Ext.each(elems, function(e) {
								ctx._get_(e).hide();
							}, this);
						}
						
	},
	
	_togglePaymentTerms_: function() {
		
						var f = this._get_("paymentTermRefDay");
						f._showEditor_();
	},
	
	_filterInvoiceFreq_: function(el) {
		
						var s = el.store;
						s.filter(function(r) {
							var value = r.get("field1");
							if ( value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_ && value !== __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_ && 
								value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_ && value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_ ) {
								return value;
							}
						});
	}
});

/* ================= EDIT FORM: financialSourceAvgMethodPeriod ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractCustomerWizard_Dc$financialSourceAvgMethodPeriod", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractCustomerWizard_Dc$financialSourceAvgMethodPeriod",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", _enableFn_: this._enableIfCurrency_, xtype:"fmbas_ExchangeRateToFinancialSourceAndAvgFilteredLov_Lov", maxLength:25, labelAlign:"top",
			retFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"} ],
			filterFieldMapping: [{lovField:"currencyId", dsField: "settlementCurrId"} ],listeners:{
			fpchange:{scope:this, fn:this._enableAvgMethods_},
			expand:{scope:this, fn:this._filterDistinct_},
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addLov({name:"averageMethodName", bind:"{d.averageMethodName}", dataIndex:"averageMethodName", _enableFn_: this._enableIfFinancialSource_, xtype:"fmbas_ExchangeRateToAvgMthdFilteredLov_Lov", maxLength:50, labelAlign:"top",
			retFieldMapping: [{lovField:"avgMthdId", dsField: "averageMethodId"} ],
			filterFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"}, {lovField:"currencyId", dsField: "settlementCurrId"} ],listeners:{
			expand:{scope:this, fn:this._filterDistinctAvg_},
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addCombo({ xtype:"combo", name:"exchangeRateOffset", bind:"{d.exchangeRateOffset}", dataIndex:"exchangeRateOffset", _enableFn_: this._enableIfStatus_, store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_], labelAlign:"top",listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:170, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["financialSourceCode", "averageMethodName", "exchangeRateOffset"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableContinue_: function(el) {
		
		
						var dc = this._controller_;
						var frame= dc.getFrame();
						var btn = frame._get_("btnContinueStep4");
						var ctx = this;
						var r = dc.getRecord();
						
						var f = function() {
							if (!r) {
								return;
							}
							var field = ["settlementCurrCode","invoiceFreq","financialSourceCode","averageMethodName","exchangeRateOffset","vat","invoiceType"];
							var dirtyFields = [];
							for (var i = 0; i < field.length; i++) {
								if (Ext.isEmpty(r.get(field[i]))) {							
									dirtyFields.push(field[i]);
								}
								else {
									var index = dirtyFields.indexOf(field[i]);
									if (index > -1) {
									    dirtyFields.splice(index, 1);
									}
								}					
							}
							if (dirtyFields.length > 0) {
								btn._disable_();
							}
							else {
								btn._enable_();
							}
						}
						Ext.defer(f, 50, this);
						
	},
	
	_filterDistinctAvg_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {			
				                var name = record.get("avgMthdName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	},
	
	_filterDistinct_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("financialSourceName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	},
	
	_enableAvgMethods_: function(el) {
		
		
						if (el._fpRawValue_ != el._onFocusVal_) {
							if( this._enableIfStatus_() ){
								var avgMethodName = this._get_("averageMethodName");
								var elVal = el.getValue(); 
								if (!Ext.isEmpty(elVal)) {
									avgMethodName.setReadOnly(false);
								}
								else {
									avgMethodName.setReadOnly(true);
								}
								avgMethodName.setValue("");
							}
						}
	},
	
	_enableIfStatus_: function() {
		
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = true;
		
						if (record) {
		
							var status = record.get("status");
		
							if (status == "Effective" || status == "Expired" || status == "Active") {
								result = false;
							}
						}
		
						return result;
	},
	
	_enableIfCurrency_: function() {
		
						
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = false;
		
						if (record) {
							var currencyCode = record.get("settlementCurrCode");
							var status = record.get("status");
							if (!Ext.isEmpty(currencyCode) && (status != "Effective" && status != "Expired" && status != "Active")) {
								result = true;
							}
						}
		
						return result;
		
	},
	
	_enableIfFinancialSource_: function() {
		
						
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = false;
		
						if (record) {
							var financialSourceCode = record.get("financialSourceCode");
							var status = record.get("status");
							if (!Ext.isEmpty(financialSourceCode) && (status != "Effective" && status != "Expired" && status != "Active")) {
								result = true;
							}
						}
		
						return result;
		
	}
});

/* ================= EDIT FORM: NewContractStep4 ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractCustomerWizard_Dc$NewContractStep4", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractCustomerWizard_Dc$NewContractStep4",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btnToggle", scope: this, handler: this._toggle_, text: "...", style:"margin-left:3px"})
		.addLov({name:"settlementCurrCode", bind:"{d.settlementCurrCode}", dataIndex:"settlementCurrCode", allowBlank:false, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelAlign:"top",
			retFieldMapping: [{lovField:"id", dsField: "settlementCurrId"} ,{lovField:"code", dsField: "initialCurrency"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ],listeners:{
			change:{scope:this, fn:this._enableContinue_},
			fpchange:{scope:this, fn:this._enableFs_}
		}})
		.addCombo({ xtype:"combo", name:"invoiceFreq", bind:"{d.invoiceFreq}", dataIndex:"invoiceFreq", store:[ __FMBAS_TYPE__.InvoiceFreq._WEEKLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIMONTHLY_, __FMBAS_TYPE__.InvoiceFreq._MONTHLY_, __FMBAS_TYPE__.InvoiceFreq._QUARTERLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._ANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._PER_DELIVERY_, __FMBAS_TYPE__.InvoiceFreq._DAILY_, __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_], labelAlign:"top",listeners:{
			expand:{scope:this, fn:this._filterInvoiceFreq_},
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addDisplayFieldText({ name:"useExchangeRatesLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelAlign:"left", style:"float: left; white-space: nowrap", labelStyle:"width:300px"})
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", width:280, xtype:"fmbas_ExchangeRateToFinancialSourceAndAvgFilteredLov_Lov", maxLength:25, labelWidth:120,
			retFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"} ],
			filterFieldMapping: [{lovField:"currencyId", dsField: "settlementCurrId"} ]})
		.addLov({name:"averageMethodName", bind:"{d.averageMethodName}", dataIndex:"averageMethodName", width:280, xtype:"fmbas_ExchangeRateToAvgMthdFilteredLov_Lov", maxLength:50,
			retFieldMapping: [{lovField:"avgMthdId", dsField: "averageMethodId"} ],
			filterFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"}, {lovField:"currencyId", dsField: "settlementCurrId"} ]})
		.addCombo({ xtype:"combo", name:"exchangeRateOffset", bind:"{d.exchangeRateOffset}", dataIndex:"exchangeRateOffset", width:280, store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_], labelWidth:120})
		.addPopoverTextField({name:"financialSourceAvgMethodPeriod", width:300, noLabel: true, hPos:"r", vPos:"t", fieldsList:["financialSourceCode","averageMethodName","exchangeRateOffset"], formatList:[], editorForm:"cmm_ContractCustomerWizard_Dc$financialSourceAvgMethodPeriod", fieldsValueSeparator:" / "})
		.add({name:"exchangeAndToggle", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("financialSourceAvgMethodPeriod"),this._getConfig_("btnToggle")]})
		.addCombo({ xtype:"combo", name:"vat", bind:"{d.vat}", dataIndex:"vat", store:[ __CMM_TYPE__.VatApplicability._ALL_EVENTS_, __CMM_TYPE__.VatApplicability._DOMESTIC_EVENTS_, __CMM_TYPE__.VatApplicability._INTERNATIONAL_EVENTS_, __CMM_TYPE__.VatApplicability._NOT_APPLICABLE_], labelAlign:"top",listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addCombo({ xtype:"combo", name:"invoiceType", bind:"{d.invoiceType}", dataIndex:"invoiceType", store:[ __FMBAS_TYPE__.InvoiceType._XML_2_0_2_, __FMBAS_TYPE__.InvoiceType._XML_3_0_0_, __FMBAS_TYPE__.InvoiceType._XML_3_0_1_, __FMBAS_TYPE__.InvoiceType._XML_3_1_0_, __FMBAS_TYPE__.InvoiceType._EXCEL_, __FMBAS_TYPE__.InvoiceType._PAPER_, __FMBAS_TYPE__.InvoiceType._EDI_, __FMBAS_TYPE__.InvoiceType._PDF_FORMAT_, __FMBAS_TYPE__.InvoiceType._FISCAL_, __FMBAS_TYPE__.InvoiceType._EXPORT_, __FMBAS_TYPE__.InvoiceType._REGULAR_], labelAlign:"top",listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:500,  style:"padding: 0px 20px"})
		.addPanel({ name:"row1", width:500, layout:"anchor"})
		.addPanel({ name:"row2", width:500, layout:"anchor"})
		.addPanel({ name:"row3", width:500, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["row1", "row2", "row3"])
		.addChildrenTo("row1", ["settlementCurrCode", "invoiceFreq"])
		.addChildrenTo("row2", ["useExchangeRatesLabel", "exchangeAndToggle"])
		.addChildrenTo("row3", ["vat", "invoiceType"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableFs_: function(el) {
		
		
						if (el._fpRawValue_ != el._onFocusVal_) {
							var ctrl = this._controller_;
							var r = ctrl.getRecord();
							if (r) {
								r.beginEdit();
								r.set("financialSourceCode",null);
								r.set("averageMethodName",null);
								r.endEdit();
							}	                
						}
	},
	
	_toggle_: function() {
		
						var f = this._get_("financialSourceAvgMethodPeriod");
						f._showEditor_();
	},
	
	_enableContinue_: function(el) {
		
		
						var dc = this._controller_;
						var frame= dc.getFrame();
						var btn = frame._get_("btnContinueStep4");
						var ctx = this;
						var r = dc.getRecord();
						
						var f = function() {
		
							if (el && el.dataIndex === "settlementCurrCode") {	
								var f = function() {
									el.doQuery(el.getValue(),true);
								}
								Ext.defer(f,500,this);												
							}
		
							if (!r) {
								return;
							}
							var field = ["settlementCurrCode","invoiceFreq","financialSourceCode","averageMethodName","exchangeRateOffset","vat","invoiceType"];
							var dirtyFields = [];
		
							for (var i = 0; i < field.length; i++) {
								if (Ext.isEmpty(r.get(field[i]))) {							
									dirtyFields.push(field[i]);
								}
								else {
									var index = dirtyFields.indexOf(field[i]);
									if (index > -1) {
									    dirtyFields.splice(index, 1);
									}
								}					
							}
							if (dirtyFields.length > 0) {
								btn._disable_();
							}
							else {
								btn._enable_();
							}
						}
						Ext.defer(f, 50, this);
	},
	
	_filterInvoiceFreq_: function(el) {
		
						var s = el.store;
						s.filter(function(r) {
							var value = r.get("field1");
							if ( value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_ && value !== __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_ && 
								value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_ && value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_ ) {
								return value;
							}
						});
	}
});

/* ================= EDIT FORM: benchmark ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractCustomerWizard_Dc$benchmark", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractCustomerWizard_Dc$benchmark",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"benchmarProviderkLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left"})
		.addTextField({ name:"benchmarkProvider", bind:"{d.benchmarkProvider}", dataIndex:"benchmarkProvider", noLabel: true, maxLength:64})
		.addLov({name:"quotFinancialSourceCode", bind:"{d.quotFinancialSourceCode}", dataIndex:"quotFinancialSourceCode", noEdit:true , noLabel: true, xtype:"cmm_UsedFinancialSourcesLov_Lov", maxLength:25, hidden:true,
			retFieldMapping: [{lovField:"id", dsField: "initialFinancialSourceId"} ,{lovField:"id", dsField: "financialSourceId"} ,{lovField:"name", dsField: "financialSourceName"} ,{lovField:"code", dsField: "financialSourceCode"} ],listeners:{
			change:{scope:this, fn:this._filterBenchmark_},
			expand:{scope:this, fn:this._filterDistinct_},
			fpchange:{scope:this, fn:this._enableDisableBenchmark_}
		}})
		.addDisplayFieldText({ name:"priceValueTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left"})
		.addRadioField({ xtype:"combo", name:"priceValueType", bind:"{d.priceValueType}", dataIndex:"priceValueType", noLabel: true, _elements_:[ {inputValue:__FMBAS_TYPE__.ValueType._HIGH_PRICE_}, {inputValue:__FMBAS_TYPE__.ValueType._LOW_PRICE_}, {inputValue:__FMBAS_TYPE__.ValueType._MEAN_PRICE_}, {inputValue:__FMBAS_TYPE__.ValueType._USER_CALCULATED_PRICE_}],listeners:{
			change:{scope:this, fn:this._filterBenchmark_}
		}})
		.addDisplayFieldText({ name:"averagingMethodLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left"})
		.addLov({name:"quotAvgMethodName", bind:"{d.quotAvgMethodName}", dataIndex:"quotAvgMethodName", allowBlank:false, width:407, noLabel: true, xtype:"cmm_UsedAverageMethodLov_Lov", maxLength:50,
			retFieldMapping: [{lovField:"id", dsField: "quotAvgMethodId"} ,{lovField:"name", dsField: "avgMethodIndicatorName"} ],listeners:{
			change:{scope:this, fn:this._filterBenchmark_},
			fpchange:{scope:this, fn:this._enableDisableBenchmark_}
		}})
		.addDisplayFieldText({ name:"exchangeRateOffsetLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left"})
		.addRadioField({ xtype:"combo", name:"quotationOffset", bind:"{d.quotationOffset}", dataIndex:"quotationOffset", noLabel: true, _elements_:[ {inputValue:__FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_}, {inputValue:__FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_}, {inputValue:__FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_}, {inputValue:__FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_}]})
		.addDisplayFieldText({ name:"benchmarkLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left"})
		.addLov({name:"quotTimeSeriesDescription", bind:"{d.quotTimeSeriesDescription}", dataIndex:"quotTimeSeriesDescription", allowBlank:false, width:407, noLabel: true, xtype:"fmbas_QuotDescriptionLov_Lov", maxLength:200, listeners:{afterrender: {scope: this, fn: function(el) {this._loadQuotations_(el)}}, fpchange: {scope: this, fn: function(el) {this._onQuotationChange_(el)}}}, _localCache_:true})
		.addDisplayFieldText({ name:"warning", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, cls:"sone-warning-label", hidden:true})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:420})
		.addPanel({ name:"r1", width:420, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["r1"])
		.addChildrenTo("r1", ["benchmarProviderkLabel", "benchmarkProvider", "quotFinancialSourceCode", "priceValueTypeLabel", "priceValueType", "averagingMethodLabel", "quotAvgMethodName", "exchangeRateOffsetLabel", "quotationOffset", "benchmarkLabel", "quotTimeSeriesDescription", "warning"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableDisableBenchmark_: function(el) {
		
						var quotTimeSeriesDescription = this._get_("quotTimeSeriesDescription");
						var quotFinancialSourceCode = this._get_("quotFinancialSourceCode");
						var ctrl = this._controller_;
						var r = ctrl.getRecord();
						var fields = ["quotAvgMethodName","quotFinancialSourceCode"];
						var disable = false;
						var fieldValues = [];
		
						var f = function() {
							for (var i = 0; i< fields.length; i++) {
								if (r && this._get_(fields[i])) {
									fieldValues.push(this._get_(fields[i]).getRawValue());
								}
							}
							if (fieldValues.indexOf(null) > -1 || fieldValues.indexOf("") > -1) {
								disable = true;
							}
							if (disable === true) {
								quotTimeSeriesDescription._disable_();
							}
							else {
								if (ctrl.readOnly === true) {
									quotTimeSeriesDescription._disable_();
									quotFinancialSourceCode._disable_();
								}
								else {
									quotTimeSeriesDescription._enable_();
									quotFinancialSourceCode._enable_();
								}
							}
						}
						Ext.defer(f,300,this);
						
	},
	
	_onQuotationChange_: function(el) {
		
						var selection = el.selection;
						var ctrl = this._controller_;
						var r = ctrl.getRecord();
		
						if (r && selection) {
							r.beginEdit();
							r.set("quotationId",selection.get("id"));
							r.set("quotUnitCode",selection.get("unitCode"));
							r.set("quotUnitId",selection.get("unitId"));
							r.set("convUnitCode",selection.get("unit2Code"));
							r.set("convUnitId",selection.get("unit2Id"));
		
							r.set("quotUnitTypeInd",selection.get("unitTypeInd"));
							r.set("convUnitTypeInd",selection.get("unit2TypeInd"));
							r.set("quotationName",selection.get("name"));
							r.set("quotValue",selection.get("calcVal"));
							r.set("factor",selection.get("arithmOper"));
							r.set("decimals",selection.get("decimals"));
							r.set("operator",selection.get("convFctr"));
							r.set("quotTimeSeriesId",selection.get("id"));
							r.endEdit();
						}
		
	},
	
	_filterDistinct_: function(el) {
		
						var store = el.store;
						var radioItems = this._radioItems_;
						store.on("load", function(store) {
				            store.filterBy(function(record) {
								var name = record.get("name");
								if(radioItems.indexOf(record.get("name")) > -1 ) {return false}else{return true}
				            });	
						}, this);
	},
	
	_filterBenchmark_: function(e,n,o) {
		
						var benchmark = this._get_("quotTimeSeriesDescription");
						var store = benchmark.store;
						var dataIndex = e.dataIndex;
						var ctx = this;
						var resetFn = function() {
							var ctrl = ctx._controller_;
							var r = ctrl.getRecord();
							if (r) {
								r.set("quotTimeSeriesDescription",null);
							}
						}	
						if (dataIndex === "quotAvgMethodName") {
							dataIndex = "avgMethodIndicatorName";
							if (n !== o && !Ext.isEmpty(o)) {
								resetFn();
							}
						}
						if (dataIndex === "quotFinancialSourceCode") {
							dataIndex = "financialSourceCode";
						}
						var warning = this._get_("warning");				
						store.filter(dataIndex, n);
						store.load();
						store.on("load", function(s) {
							var count = s.getCount();
							var benchmarkDescription = this._get_("quotTimeSeriesDescription");			
							if (count > 0) {
								warning.hide();
							}
							else {
								warning.show();
								resetFn();
							}
						}, this);	
	},
	
	_loadQuotations_: function(el) {
		
						var dc = this._controller_;
						var s = el.store;
						this._enableDisableBenchmark_();
						s.load();
	},
	
	_setFinancialSource_: function(el,n) {
		
						var financialSourceCode = this._get_("quotFinancialSourceCode");
						var fsStore = financialSourceCode.store;
						var dc = this._controller_;
					    var benchmarkValue = n[el.dataIndex];
						if (benchmarkValue !== "OTHERS") {
							financialSourceCode._disable_();
							financialSourceCode.setHidden(true);
							fsStore.load({
						        callback : function(records, operation, success) {
									financialSourceCode.setValue(benchmarkValue);
						        }
						    });
						}
						else {
							financialSourceCode.setValue(null);
							if (dc.readOnly === true) {
								financialSourceCode._disable_();
							}
							else {
								financialSourceCode._enable_();
							}
							financialSourceCode.setHidden(false);
						}
	},
	
	_afterDefineElements_: function() {
		
						var radioItems = [];
						var ctx = this;
						this._radioItems_ = [];
						var ctrl = this._controller_;
						this._getBuilder_().change("benchmarkProvider",{
							xtype: "radiogroup",
							listeners: {
								change: {
									scope: this,
									fn: function(e,n,o) {
										this._setFinancialSource_(e,n);
										this._enableDisableBenchmark_(e,n);
									}
								},
								beforerender: {
									scope: this,
									fn: function(el) {
										var ds = "cmm_UsedFinancialSourcesLov_Ds";
										var counter = 0;
										Ext.Ajax.request({
							            	url: Main.dsAPI(ds, "json").read,
							            	method: "POST",
							            	scope: this,
							            	success: function(response) {
												var data = Ext.getResponseDataInJSON(response).data;
												var dc = this._controller_;
												var r = dc.getRecord();
												Ext.each(data, function(d) {
													var checked = false;										
													if (counter<=2) {
														if (r) {
															var financialSourceCode = r.get("quotFinancialSourceCode");
															if (financialSourceCode === d.code) {
																checked = true;
															}
															if (financialSourceCode !== d.code && Ext.isEmpty(financialSourceCode)) {
																if (counter === 0) {
																	checked = true;
																}														
															}											
														}
														radioItems.push({
															boxLabel: d.code, inputValue: d.code, name: el.dataIndex, checked: checked, disabled: ctrl.readOnly
														});
														this._radioItems_.push(d.name);
													}						
													counter++;
												}, this);
												var othersChecked = true;
												Ext.each(radioItems, function(item) {
													if (item.checked === true) {
														othersChecked = false;
													}
												}, this);
												el.add(radioItems.concat({
													boxLabel: "OTHERS", inputValue: "OTHERS", name: el.dataIndex, checked: othersChecked, disabled: ctrl.readOnly
												}));
												el.fireEvent("itemsadded",el,radioItems.concat({
													boxLabel: "OTHERS", inputValue: "OTHERS", name: el.dataIndex, checked: othersChecked, disabled: ctrl.readOnly
												}));
											}
										});
									}
								},
								itemsadded: {
									scope: this,
									fn: function(el,items) {
										Ext.each(items,function(item) {
											if (item.checked === true) {
												var ctrl = ctx._controller_;
												var r = ctrl.getRecord();
												if (r && item.boxLabel !== "OTHERS") {
													r.set("quotFinancialSourceCode",item.boxLabel);
												}
												if (item.boxLabel === "OTHERS") {
													var financialSourceCode = this._get_("quotFinancialSourceCode");
													financialSourceCode._enable_();
													financialSourceCode.setHidden(false);
												}
											}
										},this);
									}
								}
							}
						});
						
	},
	
	_selectFirstValue_: function(el) {
		
						var f = function() {
							var firstItem = el.items.items[0];
							firstItem.setValue(true);
						}
						Ext.defer(f,300,this);
	}
});

/* ================= EDIT FORM: conversionFactor ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractCustomerWizard_Dc$conversionFactor", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractCustomerWizard_Dc$conversionFactor",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"benchmarkLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left"})
		.addTextField({ name:"quotUnitCode", bind:"{d.quotUnitCode}", dataIndex:"quotUnitCode", noEdit:true , _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index'; } , width:60, noLabel: true, maxLength:2, labelAlign:"top"})
		.addLov({name:"convUnitCode", bind:"{d.convUnitCode}", dataIndex:"convUnitCode", _enableFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotTimeSeriesDescription); } , allowBlank:false, width:60, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, labelAlign:"top",
			retFieldMapping: [{lovField:"id", dsField: "convUnitId"} ,{lovField:"unitTypeInd", dsField: "convUnitTypeInd"} ],listeners:{
			expand:{scope:this, fn:this._filterConvUnit_},
			blur:{scope:this, fn:function() {this._calculateDensity_(true)}}
		}})
		.addDisplayFieldText({ name:"conversionOperatorLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left;"})
		.addRadioField({ xtype:"combo", name:"operator", bind:"{d.operator}", dataIndex:"operator", _enableFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotTimeSeriesDescription); } , noLabel: true, _elements_:[ {inputValue:__FMBAS_TYPE__.Operator._MULTIPLY_}, {inputValue:__FMBAS_TYPE__.Operator._DIVIDE_}], listeners:{change: {scope: this, fn: function(el) {this._calculateDensity_(true);}}}})
		.addDisplayFieldText({ name:"conversionFactorLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left;"})
		.addNumberField({name:"factor", bind:"{d.factor}", dataIndex:"factor", _enableFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotTimeSeriesDescription); } , allowBlank:false, width:60, noLabel: true, maxLength:19, decimals:4,listeners:{
			blur:{scope:this, fn:function() {this._calculateDensity_(true)}}
		}})
		.addDisplayFieldText({ name:"conversionPreciosionLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left;"})
		.addDisplayFieldText({ name:"decimalsLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left", style:"margin-left:5px"})
		.addNumberField({name:"decimals", bind:"{d.decimals}", dataIndex:"decimals", _enableFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotTimeSeriesDescription); } , allowBlank:false, width:60, noLabel: true, cls:"number-with-spinner", minValue:0, maxValue:6, maxLength:0, listeners:{keydown: { scope: this, fn: function(field, e) { this._restrictInput_(field, e) }}}, enableKeyEvents:true})
		.add({name:"decimalsAndLabel", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("decimals"),this._getConfig_("decimalsLabel")]})
		.addDisplayFieldText({ name:"warning", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, cls:"sone-warning-label", hidden:true})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:220})
		.addPanel({ name:"r1", width:220, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["r1"])
		.addChildrenTo("r1", ["benchmarkLabel", "quotUnitCode", "convUnitCode", "conversionOperatorLabel", "operator", "conversionFactorLabel", "factor", "warning", "conversionPreciosionLabel", "decimalsAndLabel"]);
	},
	/* ==================== Business functions ==================== */
	
	_selectFirstValue_: function(el) {
		
						var f = function() {
							var firstItem = el.items.items[0];
							firstItem.setValue(true);
						}
						Ext.defer(f,300,this);
	},
	
	_restrictInput_: function(field,e) {
		
						var key = e.browserEvent.key;
						var keys = ["0","1","2","3","4","5","6","Backspace","ArrowLeft","ArrowRight","Tab"];
				        if (keys.indexOf(key) < 0) {
				            e.stopEvent();
				        }
	},
	
	_validateInput_: function() {
			
						var dc = this._controller_;
						var r = dc.getRecord();
						var field = this._get_("warning");
						if (r) {
							var conversionFactorValidationMsg = r.get("conversionFactorValidationMsg");
							if (!Ext.isEmpty(conversionFactorValidationMsg)) {
								field.setFieldLabel(conversionFactorValidationMsg);
								field.show();
							}
							else {						
								field.setFieldLabel(null);
								field.hide();
							}
						}
	},
	
	_calculateDensity_: function(validateInput) {
		
						var dc = this._controller_;
						var r = dc.getRecord();
						if (r) {
							var factor = r.get("factor");
							if(factor != 0 && factor != null && factor != "0" && factor != ""){
								if (validateInput) {
									dc.fireEvent("itsTimeToCalculateDensity", this, {validateInput: true, formName: "newContractStep5"});
								}
								else {
									dc.fireEvent("itsTimeToCalculateDensity", this, {formName: "newContractStep5"});
								}						
							}
						}
	},
	
	_filterConvUnit_: function(el) {
		
						var s = el.store;
						s.clearFilter();
						
						var dc = this._controller_;
						var rec = dc.getRecord();
						var quotUnitTypeInd = rec.get("quotUnitTypeInd");
		
						s.filter(function(r) {
							var convUnitTypeInd = r.get("unitTypeInd");
							if ( convUnitTypeInd !== quotUnitTypeInd) {
								return convUnitTypeInd;
							}
						});
	}
});

/* ================= EDIT FORM: NewContractStep5 ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractCustomerWizard_Dc$NewContractStep5", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractCustomerWizard_Dc$NewContractStep5",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btnToggleBenchmark", scope: this, handler: this._toggleBenchmark_, text: "...", style:"margin-left:3px"})
		.addButton({name:"btnToggleConversionFactor", scope: this, handler: this._toggleConversionfactor_, text: "...", style:"margin-left:3px"})
		.addLov({name:"priceCategoryName", bind:"{d.priceCategoryName}", dataIndex:"priceCategoryName", xtype:"cmm_PriceCategoriesNotFilteredLov_Lov", maxLength:32, labelAlign:"top",
			retFieldMapping: [{lovField:"id", dsField: "priceCategoryId"} ,{lovField:"name", dsField: "priceName"} ],
			filterFieldMapping: [{lovField:"type", value: "Product"}, {lovField:"pricePer", value: "Volume"} ],listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addTextField({ name:"priceName", bind:"{d.priceName}", dataIndex:"priceName", maxLength:100, labelAlign:"top",listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addDisplayFieldText({ name:"daysLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelAlign:"left", style:"padding-left:5px"})
		.addDisplayFieldText({ name:"quotationLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelAlign:"left", style:"float: left; white-space: nowrap", labelStyle:"width:300px"})
		.addTextField({ name:"quotTimeSeriesDescription", bind:"{d.quotTimeSeriesDescription}", dataIndex:"quotTimeSeriesDescription", maxLength:200})
		.addDisplayFieldText({ name:"commaLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotTimeSeriesDescription); } , maxLength:100})
		.addTextField({ name:"quotAvgMethodName", bind:"{d.quotAvgMethodName}", dataIndex:"quotAvgMethodName", allowBlank:false, maxLength:50})
		.addDisplayFieldText({ name:"secondCommaLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.avgMethodIndicatorName); } , maxLength:100})
		.addDisplayFieldText({ name:"benchmarkPeriodLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotTimeSeriesDescription); } , maxLength:100})
		.addTextField({ name:"quotationOffset", bind:"{d.quotationOffset}", dataIndex:"quotationOffset", _visibleFn_: function(dc, rec) { return !Ext.isEmpty(dc.record.data.quotTimeSeriesDescription); } , maxLength:32})
		.addDisplayFieldText({ name:"conversionFromLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.quotUnitCode); } , maxLength:100})
		.addTextField({ name:"quotUnitCode", bind:"{d.quotUnitCode}", dataIndex:"quotUnitCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCategoryName !='Index'; } , maxLength:2})
		.addDisplayFieldText({ name:"conversionToLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.convUnitCode); } , maxLength:100})
		.addLov({name:"convUnitCode", bind:"{d.convUnitCode}", dataIndex:"convUnitCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index'; } , width:100, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, labelWidth:30,
			retFieldMapping: [{lovField:"id", dsField: "convUnitId"} ,{lovField:"unitTypeInd", dsField: "convUnitTypeInd"} ]})
		.addDisplayFieldText({ name:"multiplyLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.operator); } , maxLength:100})
		.addCombo({ xtype:"combo", name:"operator", bind:"{d.operator}", dataIndex:"operator", _enableFn_: function(dc, rec) { return this._get_("priceCatName").getValue() === "Index"; } , width:150, store:[ __FMBAS_TYPE__.Operator._MULTIPLY_, __FMBAS_TYPE__.Operator._DIVIDE_], labelWidth:60})
		.addNumberField({name:"factor", bind:"{d.factor}", dataIndex:"factor", _enableFn_: function(dc, rec) { return this._get_("priceCatName").getValue() === "Index"; } , width:130, maxLength:19, labelWidth:60, decimals:4})
		.addDisplayFieldText({ name:"usingLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.decimals); } , maxLength:100})
		.addDisplayFieldText({ name:"decimalPrecisionLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && !Ext.isEmpty(dc.record.data.decimals); } , maxLength:100})
		.addNumberField({name:"decimals", bind:"{d.decimals}", dataIndex:"decimals", _enableFn_: function(dc, rec) { return this._get_("priceCatName").getValue() == "Index"; } , width:150, maxLength:3, labelWidth:100})
		.addPopoverTextField({name:"benchmark", width:485, hPos:"r", vPos:"b", labelAlign:"top",listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}, fieldsList:["quotTimeSeriesDescription","commaLabel","quotAvgMethodName","secondCommaLabel","quotationOffset","benchmarkPeriodLabel"], formatList:[], editorForm:"cmm_ContractCustomerWizard_Dc$benchmark", fieldsValueSeparator:"", allowBlank:false})
		.addPopoverTextField({name:"conversionFactor", width:485, hPos:"r", vPos:"b", labelAlign:"top",listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}, fieldsList:["conversionFromLabel","quotUnitCode","conversionToLabel","convUnitCode","multiplyLabel","operator","factor","usingLabel","decimals","decimalPrecisionLabel"], formatList:[], editorForm:"cmm_ContractCustomerWizard_Dc$conversionFactor", fieldsValueSeparator:" ", allowBlank:false})
		.add({name:"benchmarkAndButton", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("benchmark"),this._getConfig_("btnToggleBenchmark")]})
		.add({name:"conversionFactorAndButton", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("conversionFactor"),this._getConfig_("btnToggleConversionFactor")]})
		.addDisplayFieldText({ name:"conversionFactorLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelStyle:"white-space:nowrap; width: 100%; text-align: left;"})
		.addDisplayFieldText({ name:"differentialLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelAlign:"left", style:"float: left; white-space: nowrap", labelStyle:"width:300px"})
		.addNumberField({name:"initialPrice", bind:"{d.initialPrice}", dataIndex:"initialPrice", noLabel: true, maxLength:19, decimals:_SYSTEMPARAMETERS_.dec_crncy,listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addDisplayFieldText({ name:"priceValueLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, labelAlign:"left", style:"float: left; white-space: nowrap", labelStyle:"width:300px"})
		.addLov({name:"initialCurrency", bind:"{d.initialCurrency}", dataIndex:"initialCurrency", width:80, noLabel: true, xtype:"fmbas_CurrenciesLov_Lov", maxLength:100, style:"margin-left:5px",
			retFieldMapping: [{lovField:"id", dsField: "initialCurId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ],listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addLov({name:"initialUnit", bind:"{d.initialUnit}", dataIndex:"initialUnit", width:60, noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:100, style:"margin-left:5px",
			retFieldMapping: [{lovField:"id", dsField: "initialUnitId"} ],listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  style:"padding: 0px 20px"})
		.addPanel({ name:"hiddenFields", width:560,  style:"display:none", layout:"anchor"})
		.addPanel({ name:"row1", width:535, layout:"anchor"})
		.addPanel({ name:"row2", width:535, layout:"anchor"})
		.addPanel({ name:"row3", width:535, layout:"anchor"})
		.addPanel({ name:"row4", width:535, layout:"anchor"})
		.addPanel({ name:"row6", layout: {type:"table"}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["hiddenFields", "row1", "row2", "row3", "row4", "row6"])
		.addChildrenTo("hiddenFields", ["quotAvgMethodName", "conversionFromLabel", "conversionToLabel", "multiplyLabel", "usingLabel", "decimalPrecisionLabel", "commaLabel", "benchmarkPeriodLabel", "secondCommaLabel"])
		.addChildrenTo("row1", ["priceCategoryName", "priceName"])
		.addChildrenTo("row2", ["quotationLabel", "benchmarkAndButton"])
		.addChildrenTo("row3", ["conversionFactorLabel", "conversionFactorAndButton"])
		.addChildrenTo("row4", ["differentialLabel", "priceValueLabel"])
		.addChildrenTo("row6", ["initialPrice", "initialCurrency", "initialUnit"]);
	},
	/* ==================== Business functions ==================== */
	
	_shouldValidate_: function() {
		
						return this._isVisible_;
	},
	
	_toggleBenchmark_: function() {
		
						var f = this._get_("benchmark");
						f._showEditor_();
	},
	
	_toggleConversionfactor_: function() {
		
						var f = this._get_("conversionFactor");
						f._showEditor_();
	},
	
	_enableContinue_: function(el,newVal,oldVal) {
		
		
						var dc = this._controller_;
						var frame= dc.getFrame();
						var btn = frame._get_("btnContinueStep5");
						var ctx = this;
						var r = dc.getRecord();
						
						var f = function() {
							if (!r) {
								return;
							}
		
							var field = ["priceCategoryName","priceName","quotationName","quotTimeSeriesDescription","quotAvgMethodName","quotationOffset","quotUnitCode","convUnitCode","operator","factor","decimals","initialPrice","initialCurrency","initialUnit"];
							var dirtyFields = [];
							if (el && el.dataIndex === "priceCategoryName") {	
								this._showHideFields_();
							}
							if (r.get("priceCategoryName") === "Fixed") {
								field = ["priceCategoryName","priceName","initialPrice","initialCurrency","initialUnit"];
							}
							
							for (var i = 0; i < field.length; i++) {
								if (Ext.isEmpty(r.get(field[i]))) {							
									dirtyFields.push(field[i]);
								}
								else {
									var index = dirtyFields.indexOf(field[i]);
									if (index > -1) {
									    dirtyFields.splice(index, 1);
									}
								}					
							}
							if (dirtyFields.length > 0) {
								btn._disable_();
							}
							else {
								btn._enable_();
							}
						}
						Ext.defer(f, 50, this);
						
	},
	
	_showHideFields_: function() {
		
		
						var el = this._get_("priceCategoryName");
						var val = el.getValue();
						var elems = ["quotationLabel","benchmarkAndButton","conversionFactorLabel","conversionFactorAndButton", "priceValueLabel"];
						var ctx = this;
		
						if (!Ext.isEmpty(val)) {
							if (val === "Fixed") {
								Ext.each(elems, function(e) {						
									ctx._get_(e).hide();
								}, this);
								ctx._get_("differentialLabel").hide();
								ctx._get_("priceValueLabel").show();
							}
							else if (val === "Index") {
								Ext.each(elems, function(e) {						
									ctx._get_(e).show();
								}, this);
								ctx._get_("differentialLabel").show();
								ctx._get_("priceValueLabel").hide();
							}
						}
						else {
							Ext.each(elems, function(e) {
								ctx._get_(e).hide();
							}, this);
							ctx._get_("differentialLabel").show();
							ctx._get_("priceValueLabel").hide();
						}
	}
});
