/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.TenderLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_TenderLov_Ds"
	},
	
	
	fields: [
		{name:"holderId", type:"int", allowNull:true},
		{name:"holderCode", type:"string"},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"version", type:"int", allowNull:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.TenderLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"holderId", type:"int", allowNull:true},
		{name:"holderCode", type:"string"},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"version", type:"int", allowNull:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
