/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.Template_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.cmm.ui.extjs.ds.ContractTemplate_Ds
});

/* ================= GRID: List ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.Template_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_Template_Dc$List",
	_noPaginator_: true,
	selType: null,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"contractType", dataIndex:"contractType", width:132})
		.addTextColumn({ name:"deliverySubtype", dataIndex:"deliverySubtype", width:132})
		.addTextColumn({ name:"scope", dataIndex:"scope", width:132})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
