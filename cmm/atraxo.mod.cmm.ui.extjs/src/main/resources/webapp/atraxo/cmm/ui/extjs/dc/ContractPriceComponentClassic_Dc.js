/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceComponentClassic_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.cmm.ui.extjs.ds.ContractPriceComponent_Ds
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceComponentClassic_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractPriceComponentClassic_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addNumberField({name:"price", bind:"{d.price}", dataIndex:"price", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index' && dc.record.data.priceCtgryPricePer != 'Composite'; } , allowBlank:false, width:180, sysDec:"dec_crncy", maxLength:19, decimals:_SYSTEMPARAMETERS_.dec_crncy})
		.addLov({name:"currency", bind:"{d.currencyCode}", dataIndex:"currencyCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index' && dc.record.data.priceCtgryPricePer != 'Composite'; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !='Percent'; } , allowBlank:false, width:80, noLabel: true, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3,
			retFieldMapping: [{lovField:"id", dsField: "currency"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addLov({name:"unit", bind:"{d.unitCode}", dataIndex:"unitCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index' && dc.record.data.priceCtgryPricePer != 'Composite'; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !='Event' && dc.record.data.priceCtgryPricePer !='Percent'; } , allowBlank:false, width:60, noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "unit"} ]})
		.addLov({name:"unitEvent", bind:"{d.unitCode}", dataIndex:"unitCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index' && dc.record.data.priceCtgryPricePer != 'Composite'; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer =='Event'; } , allowBlank:false, width:60, noLabel: true, xtype:"fmbas_MeasurmentUnitsEventLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "unit"} ]})
		.addLov({name:"unitPercent", bind:"{d.unitCode}", dataIndex:"unitCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer != 'Composite'; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer =='Percent'; } , allowBlank:false, width:60, noLabel: true, xtype:"fmbas_MeasurmentUnitsPercentLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "unit"} ]})
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index'; } , allowBlank:false, width:220})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index'; } , allowBlank:false, width:220})
		.add({name:"row", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("price"),this._getConfig_("currency"),this._getConfig_("unit"),this._getConfig_("unitEvent"),this._getConfig_("unitPercent")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:380, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["row", "validFrom", "validTo"]);
	}
});

/* ================= GRID: SimpleList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceComponentClassic_Dc$SimpleList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_ContractPriceComponentClassic_Dc$SimpleList",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"validFrom", dataIndex:"validFrom", width:150, _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"validTo", width:150, _mask_: Masks.DATE})
		.addNumberColumn({ name:"value", dataIndex:"price", width:150, sysDec:"dec_crncy",  decimals:_SYSTEMPARAMETERS_.dec_crncy})
		.addTextColumn({ name:"currency", dataIndex:"currencyCode", width:150})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:150})
		.addTextColumn({ name:"contractPriceCatName", dataIndex:"contractPriceCatName", hidden:true, width:100})
		.addTextColumn({ name:"priceCtgryName", dataIndex:"priceCtgryName", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
