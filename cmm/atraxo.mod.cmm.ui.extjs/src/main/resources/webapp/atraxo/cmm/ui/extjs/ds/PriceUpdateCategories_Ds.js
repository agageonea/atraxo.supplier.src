/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.PriceUpdateCategories_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_PriceUpdateCategories_Ds"
	},
	
	
	fields: [
		{name:"priceUpdateId", type:"int", allowNull:true},
		{name:"priceCategoryId", type:"int", allowNull:true},
		{name:"priceCategoryName", type:"string"},
		{name:"locationId", type:"int", allowNull:true},
		{name:"locationCode", type:"string"},
		{name:"contractHolderId", type:"int", allowNull:true},
		{name:"contractHolderCode", type:"string"},
		{name:"counterpartyId", type:"int", allowNull:true},
		{name:"counterpartyCode", type:"string"},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"contract", type:"string"},
		{name:"price", type:"float", allowNull:true},
		{name:"dealType", type:"string"},
		{name:"priceCatName", type:"string"},
		{name:"contractPriceCatId", type:"int", allowNull:true},
		{name:"selected", type:"boolean"},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.PriceUpdateCategories_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"priceUpdateId", type:"int", allowNull:true},
		{name:"priceCategoryId", type:"int", allowNull:true},
		{name:"priceCategoryName", type:"string"},
		{name:"locationId", type:"int", allowNull:true},
		{name:"locationCode", type:"string"},
		{name:"contractHolderId", type:"int", allowNull:true},
		{name:"contractHolderCode", type:"string"},
		{name:"counterpartyId", type:"int", allowNull:true},
		{name:"counterpartyCode", type:"string"},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"contract", type:"string"},
		{name:"price", type:"float", allowNull:true},
		{name:"dealType", type:"string"},
		{name:"priceCatName", type:"string"},
		{name:"contractPriceCatId", type:"int", allowNull:true},
		{name:"selected", type:"boolean", allowNull:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.cmm.ui.extjs.ds.PriceUpdateCategories_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"selectedAll", type:"boolean"}
	]
});
