/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.ContractOverview_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.ContractOverview_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.ContractOverview_Ds
});

/* ================= FILTER: ContractFilter ================= */


Ext.define("atraxo.cmm.ui.extjs.dc.ContractOverview_Dc$ContractFilter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.cmm_ContractOverview_Dc$ContractFilter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"locCode", dataIndex:"locCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"code", dsField: "locCode"} ]}})
			.addTextField({ name:"code", dataIndex:"code", maxLength:32})
			.addLov({name:"counterPartyCode", dataIndex:"counterPartyCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"code", dsField: "counterPartyCode"} ],
					filterFieldMapping: [{lovField:"isSupplier", value: "true"} ]}})
			.addCombo({ xtype:"combo", name:"type", dataIndex:"type", store:[ __CMM_TYPE__.ContractType._PRODUCT_, __CMM_TYPE__.ContractType._FUELING_SERVICE_, __CMM_TYPE__.ContractType._GENERAL_SERVICE_, __CMM_TYPE__.ContractType._INSPECTION_, __CMM_TYPE__.ContractType._STORAGE_, __CMM_TYPE__.ContractType._CSO_]})
			.addCombo({ xtype:"combo", name:"subType", dataIndex:"subType", store:[ __CMM_TYPE__.ContractSubType._NA_, __CMM_TYPE__.ContractSubType._INTO_PLANE_, __CMM_TYPE__.ContractSubType._EX_HYDRANT_, __CMM_TYPE__.ContractSubType._AIRLINE_SUPPLY_, __CMM_TYPE__.ContractSubType._INTO_STORAGE_, __CMM_TYPE__.ContractSubType._SERVICE_]})
			.addCombo({ xtype:"combo", name:"scope", dataIndex:"scope", store:[ __CMM_TYPE__.ContractScope._INTO_PLANE_, __CMM_TYPE__.ContractScope._NON_INTO_PLANE_]})
			.addLov({name:"settCurrCode", dataIndex:"settCurrCode", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"code", dsField: "settCurrCode"} ]}})
			.addLov({name:"settUnitCode", dataIndex:"settUnitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.UnitsLov_Lov", selectOnFocus:true, maxLength:2,
					retFieldMapping: [{lovField:"code", dsField: "settUnitCode"} ]}})
			.addNumberField({name:"paymentTerms", dataIndex:"paymentTerms", maxLength:4})
			.addCombo({ xtype:"combo", name:"creditTerms", dataIndex:"creditTerms", store:[ __FMBAS_TYPE__.CreditTerm._OPEN_CREDIT_, __FMBAS_TYPE__.CreditTerm._CREDIT_LINE_, __FMBAS_TYPE__.CreditTerm._BANK_GUARANTEE_, __FMBAS_TYPE__.CreditTerm._PREPAYMENT_]})
			.addDateField({name:"validFrom", dataIndex:"validFrom"})
			.addDateField({name:"validTo", dataIndex:"validTo"})
		;
	}

});

/* ================= GRID: ContractList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractOverview_Dc$ContractList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_ContractOverview_Dc$ContractList",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"location", dataIndex:"locCode", width:100})
		.addTextColumn({ name:"code", dataIndex:"code", width:100})
		.addTextColumn({ name:"counterPartyCode", dataIndex:"counterPartyCode", width:100})
		.addTextColumn({ name:"type", dataIndex:"type", width:100})
		.addTextColumn({ name:"subType", dataIndex:"subType", width:100})
		.addTextColumn({ name:"scope", dataIndex:"scope", width:100})
		.addNumberColumn({ name:"price", dataIndex:"price", width:100, decimals:6})
		.addTextColumn({ name:"settCurrCode", dataIndex:"settCurrCode", width:80})
		.addTextColumn({ name:"settUnitCode", dataIndex:"settUnitCode", width:70})
		.addNumberColumn({ name:"paymentTerms", dataIndex:"paymentTerms", width:100})
		.addTextColumn({ name:"creditTerms", dataIndex:"creditTerms", width:100})
		.addDateColumn({ name:"validFrom", dataIndex:"validFrom", width:80, _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"validTo", width:80, _mask_: Masks.DATE})
		.addTextColumn({ name:"status", dataIndex:"status", hidden:true, width:70})
		.addTextColumn({ name:"product", dataIndex:"product", hidden:true, width:70})
		.addTextColumn({ name:"limitedTo", dataIndex:"limitedTo", hidden:true, width:80})
		.addTextColumn({ name:"tax", dataIndex:"tax", hidden:true, width:80})
		.addTextColumn({ name:"respBuyer", dataIndex:"resBuyerName", hidden:true, width:100})
		.addTextColumn({ name:"contactName", dataIndex:"contactName", hidden:true, width:100})
		.addTextColumn({ name:"iplAgent", dataIndex:"iplCode", hidden:true, width:100})
		.addTextColumn({ name:"dealType", dataIndex:"dealType", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._currencyParamName_ = "p_currency";
		this._unitParamName_ = "p_unit";
		this._getBuilder_()
		;
	}
});
