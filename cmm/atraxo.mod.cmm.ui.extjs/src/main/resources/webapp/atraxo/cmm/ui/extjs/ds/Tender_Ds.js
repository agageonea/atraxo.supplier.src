/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.Tender_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_Tender_Ds"
	},
	
	
	validators: {
		contactId: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("type", "Product");
		this.set("status", "Draft");
		this.set("biddingStatus", "New");
		this.set("transmissionStatus", "New");
		this.set("source", "Captured");
		this.set("volumeOffer", "Undefined");
		this.set("tenderVersion", 1);
		this.set("closed", true);
		this.set("read", true);
	},
	
	fields: [
		{name:"holderId", type:"int", allowNull:true},
		{name:"holderCode", type:"string"},
		{name:"holderName", type:"string"},
		{name:"contactId", type:"int", allowNull:true},
		{name:"contactName", type:"string"},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"tenderVersion", type:"int", allowNull:true},
		{name:"type", type:"string"},
		{name:"contactPerson", type:"string"},
		{name:"email", type:"string"},
		{name:"phone", type:"string"},
		{name:"biddingPeriodFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"biddingPeriodTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"comments", type:"string"},
		{name:"status", type:"string"},
		{name:"biddingStatus", type:"string"},
		{name:"transmissionStatus", type:"string"},
		{name:"source", type:"string"},
		{name:"canceledOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"updatedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"publishedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"closed", type:"boolean"},
		{name:"msgId", type:"int", allowNull:true},
		{name:"noOfRounds", type:"int", allowNull:true},
		{name:"schemaVersion", type:"string"},
		{name:"volumeOffer", type:"string"},
		{name:"read", type:"boolean"},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"volumeUnit", type:"string", noFilter:true, noSort:true},
		{name:"totalVolume", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"totalVolumeHeader", type:"string", noFilter:true, noSort:true},
		{name:"agreementFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"agreementTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"formTitle", type:"string", noFilter:true, noSort:true},
		{name:"subsidiaryCode", type:"string", noFilter:true, noSort:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.Tender_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"holderId", type:"int", allowNull:true},
		{name:"holderCode", type:"string"},
		{name:"holderName", type:"string"},
		{name:"contactId", type:"int", allowNull:true},
		{name:"contactName", type:"string"},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"tenderVersion", type:"int", allowNull:true},
		{name:"type", type:"string"},
		{name:"contactPerson", type:"string"},
		{name:"email", type:"string"},
		{name:"phone", type:"string"},
		{name:"biddingPeriodFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"biddingPeriodTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"comments", type:"string"},
		{name:"status", type:"string"},
		{name:"biddingStatus", type:"string"},
		{name:"transmissionStatus", type:"string"},
		{name:"source", type:"string"},
		{name:"canceledOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"updatedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"publishedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"closed", type:"boolean", allowNull:true},
		{name:"msgId", type:"int", allowNull:true},
		{name:"noOfRounds", type:"int", allowNull:true},
		{name:"schemaVersion", type:"string"},
		{name:"volumeOffer", type:"string"},
		{name:"read", type:"boolean", allowNull:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"volumeUnit", type:"string", noFilter:true, noSort:true},
		{name:"totalVolume", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"totalVolumeHeader", type:"string", noFilter:true, noSort:true},
		{name:"agreementFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"agreementTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"formTitle", type:"string", noFilter:true, noSort:true},
		{name:"subsidiaryCode", type:"string", noFilter:true, noSort:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.cmm.ui.extjs.ds.Tender_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"remarks", type:"string"}
	]
});
