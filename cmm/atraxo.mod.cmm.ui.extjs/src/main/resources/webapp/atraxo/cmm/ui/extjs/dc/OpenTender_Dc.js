/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.OpenTender_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.OpenTender_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.OpenTender_Ds
});

/* ================= GRID: SelectList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.OpenTender_Dc$SelectList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_OpenTender_Dc$SelectList",
	_noExport_: true,
	_noPaginator_: true,
	selType: null,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", hidden:true, width:100})
		.addTextColumn({ name:"name", dataIndex:"name", width:200})
		.addTextColumn({ name:"issuer", dataIndex:"holderCode", width:80})
		.addTextColumn({ name:"tenderStatus", dataIndex:"status", hidden:true, width:100})
		.addTextColumn({ name:"biddingStatus", dataIndex:"biddingStatus", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
