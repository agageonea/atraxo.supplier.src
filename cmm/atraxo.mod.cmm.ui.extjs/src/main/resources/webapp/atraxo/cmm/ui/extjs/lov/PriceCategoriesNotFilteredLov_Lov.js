/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.lov.PriceCategoriesNotFilteredLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.cmm_PriceCategoriesNotFilteredLov_Lov",
	displayField: "name", 
	_columns_: ["name"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{name}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.cmm.ui.extjs.ds.PriceCategoriesNotFilteredLov_Ds
});
