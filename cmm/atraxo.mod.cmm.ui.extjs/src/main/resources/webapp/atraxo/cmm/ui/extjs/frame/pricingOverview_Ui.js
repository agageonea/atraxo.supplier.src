/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.frame.pricingOverview_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.pricingOverview_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("contract", Ext.create(atraxo.cmm.ui.extjs.dc.ContractOverview_Dc,{multiEdit: true}))
		.addDc("price", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceOverview_Dc,{}))
		.linkDc("price", "contract",{fetchMode:"auto",fields:[
					{childField:"contract", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addDcGridView("contract", {name:"contractList", height:"50%", xtype:"cmm_ContractOverview_Dc$ContractList"})
		.addDcFilterFormView("contract", {name:"contractFilter", xtype:"cmm_ContractOverview_Dc$ContractFilter"})
		.addDcGridView("price", {name:"priceList", xtype:"cmm_ContractPriceOverview_Dc$PriceList"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"grids", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["grids"], ["center"])
		.addChildrenTo("grids", ["contractList", "priceList"], ["north", "center"])
		.addToolbarTo("contractList", "tlbCmpList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbCmpList", {dc: "contract"})
			.addButtons([])
			.addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	,doDetails: function() {	
		var bundle = "atraxo.mod.cmm";
		var frame = "atraxo.cmm.ui.extjs.frame.Contract_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				contractCode: this._getDc_("contract").getRecord().get("code")
			},
			callback: function (params) {
				this._when_called_for_details_(params);
			}
		});
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/PricingOverview.html";
					window.open( url, "_blank");
	}
	
	,_afterDefineDcs_: function() {
		
					this._getDc_("contract").on("onEditIn", function (dc) {
							this.doDetails();
						}, this);
	}
});
