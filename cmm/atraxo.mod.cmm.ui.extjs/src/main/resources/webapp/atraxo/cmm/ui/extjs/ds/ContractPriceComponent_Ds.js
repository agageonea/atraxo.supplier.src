/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.ContractPriceComponent_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_ContractPriceComponent_Ds"
	},
	
	
	fields: [
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"provisional", type:"boolean"},
		{name:"price", type:"float", allowNull:true},
		{name:"withHold", type:"boolean"},
		{name:"toleranceMessage", type:"string"},
		{name:"contractPriceCat", type:"int", allowNull:true},
		{name:"contractPriceCatName", type:"string"},
		{name:"priceCategoryId", type:"int", allowNull:true},
		{name:"priceCtgryName", type:"string"},
		{name:"priceCtgryPricePer", type:"string"},
		{name:"pricingBasesId", type:"int", allowNull:true},
		{name:"currency", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"unit", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.ContractPriceComponent_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"provisional", type:"boolean", allowNull:true},
		{name:"price", type:"float", allowNull:true},
		{name:"withHold", type:"boolean", allowNull:true},
		{name:"toleranceMessage", type:"string"},
		{name:"contractPriceCat", type:"int", allowNull:true},
		{name:"contractPriceCatName", type:"string"},
		{name:"priceCategoryId", type:"int", allowNull:true},
		{name:"priceCtgryName", type:"string"},
		{name:"priceCtgryPricePer", type:"string"},
		{name:"pricingBasesId", type:"int", allowNull:true},
		{name:"currency", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"unit", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
