/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.frame.PriceUpdate_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.PriceUpdate_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("price", Ext.create(atraxo.cmm.ui.extjs.dc.PriceUpdate_Dc,{}))
		.addDc("priceNew", Ext.create(atraxo.cmm.ui.extjs.dc.PriceUpdate_Dc,{ _cmdExecutionTimeout_:-1}))
		.addDc("priceCategory", Ext.create(atraxo.cmm.ui.extjs.dc.PriceUpdateCategories_Dc,{}))
		.addDc("priceCategoryNew", Ext.create(atraxo.cmm.ui.extjs.dc.PriceUpdateCategoriesInfinite_Dc,{}))
		.addDc("priceCategoryLabel", Ext.create(atraxo.cmm.ui.extjs.dc.PriceUpdateCategoriesLabel_Dc,{}))
		.addDc("priceCategoryAdd", Ext.create(atraxo.cmm.ui.extjs.dc.PriceUpdateCategoriesAdd_Dc,{multiEdit: true}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.addDc("attachment", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("attachmentPop", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.linkDc("priceCategory", "price",{fetchMode:"auto",fields:[
					{childField:"priceUpdateId", parentField:"id"}]})
				.linkDc("priceCategoryNew", "priceNew",{fetchMode:"auto",fields:[
					{childField:"priceUpdateId", parentField:"id"}]})
				.linkDc("priceCategoryAdd", "price",{fields:[
					{childField:"priceUpdateId", parentField:"id"}]})
				.linkDc("note", "price",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("attachment", "price",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}]})
				.linkDc("attachmentPop", "price",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}, {childField:"categType", value:"upload"}]})
				.linkDc("history", "price",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"btnCloseNoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseNoteWdw, scope:this})
		.addButton({name:"btnSaveCloseNoteWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseNoteWdw, scope:this})
		.addButton({name:"btnAddPriceCategory",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:true, handler: this.onBtnAddPriceCategory,stateManager:[{ name:"selected_one", dc:"price", and: function() {return (this._checkIfICanDeletePriceUpdate_());} }], scope:this})
		.addButton({name:"btnRemovePriceCategory",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnRemovePriceCategory,stateManager:[{ name:"selected_not_zero", dc:"priceCategory", and: function() {return (this._checkIfICanDeletePriceUpdateAndPrice_());} }], scope:this})
		.addButton({name:"btnClosePriceCategoryWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnClosePriceCategoryWdw, scope:this})
		.addButton({name:"btnCloseAddPriceCategoryWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css,  disabled:true, handler: this.onBtnCloseAddPriceCategoryWdw, scope:this})
		.addButton({name:"btnSubmitForApproval",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitForApproval,stateManager:[{ name:"selected_one", dc:"price", and: function(dc) {return (this.canSubmitForApproval(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnSaveCloseApprovalWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApprovalWdw, scope:this})
		.addButton({name:"btnCancelApprovalWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApprovalWdw, scope:this})
		.addButton({name:"btnCancelApproveWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApproveWdw, scope:this})
		.addButton({name:"btnCancelRejectWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRejectWdw, scope:this})
		.addButton({name:"btnApprovePrice",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApprovePrice,stateManager:[{ name:"selected_one", dc:"price", and: function(dc) {return (this.canApprove(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnRejectPrice",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnRejectPrice,stateManager:[{ name:"selected_one", dc:"price", and: function(dc) {return (this.canApprove(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnPublishPrice",glyph:fp_asc.send_glyph.glyph,iconCls: fp_asc.send_glyph.css, disabled:false, handler: this.onBtnPublishPrice, scope:this})
		.addButton({name:"btnNewPriceUpdate",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewPriceUpdate, scope:this})
		.addButton({name:"btnEditPriceUpdate",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css,  disabled:true, handler: this.onBtnEditPriceUpdate, scope:this})
		.addButton({name:"btnDeletePriceUpdate",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeletePriceUpdate,stateManager:[{ name:"selected_not_zero", dc:"price", and: function(dc) {return (this._checkIfICanDeletePrice_(dc));} }], scope:this})
		.addButton({name:"btnContinueStep1",glyph:fp_asc.next_glyph.glyph,  disabled:true, handler: this.onBtnContinueStep1, scope:this})
		.addButton({name:"btnContinueStep2",glyph:fp_asc.next_glyph.glyph, disabled:false,  hidden:true, handler: this.onBtnContinueStep2, scope:this})
		.addButton({name:"btnContinueStep3",glyph:fp_asc.next_glyph.glyph, disabled:true,  hidden:true, handler: this.onBtnContinueStep3,stateManager:[{ name:"selected_not_zero", dc:"priceCategoryNew"}], scope:this})
		.addButton({name:"btnBackToPriceCategory",glyph:fp_asc.back_glyph.glyph,iconCls: fp_asc.back_glyph.css, disabled:false,  hidden:true, handler: this.onBtnBackToPriceCategory, scope:this})
		.addButton({name:"btnCancelCapturing",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelCapturing, scope:this})
		.addButton({name:"btnCancelCapturingStep2",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false,  hidden:true, handler: this.onBtnCancelCapturingStep2, scope:this})
		.addButton({name:"btnCancelCapturingStep3",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false,  hidden:true, handler: this.onBtnCancelCapturingStep3, scope:this})
		.addButton({name:"btnCancelCapturingStep4",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false,  hidden:true, handler: this.onBtnCancelCapturingStep4, scope:this})
		.addButton({name:"btnBackToCondition",glyph:fp_asc.back_glyph.glyph,iconCls: fp_asc.back_glyph.css, disabled:false,  hidden:true, handler: this.onBtnBackToCondition, scope:this})
		.addButton({name:"btnBackToContracts",glyph:fp_asc.back_glyph.glyph,iconCls: fp_asc.back_glyph.css, disabled:false,  hidden:true, handler: this.onBtnBackToContracts, scope:this})
		.addButton({name:"btnFinish",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false,  hidden:true, handler: this.onBtnFinish, scope:this})
		.addButton({name:"btnSaveCloseApproveWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApproveWdw, scope:this})
		.addButton({name:"btnSaveCloseRejectWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRejectWdw, scope:this})
		.addDcFilterFormView("price", {name:"priceFilter", xtype:"cmm_PriceUpdate_Dc$Filter"})
		.addDcGridView("price", {name:"priceList", height:400, xtype:"cmm_PriceUpdate_Dc$List",  listeners:{selectionchange: {scope: this, fn: function(list, selectedRecord) {this._checkIfMultiplePrice_(list, selectedRecord)}}}})
		.addDcFormView("price", {name:"approvalNote", xtype:"cmm_PriceUpdate_Dc$ApprovalNote",  split:false})
		.addDcFormView("price", {name:"approveNote", xtype:"cmm_PriceUpdate_Dc$ApprovalNote"})
		.addDcFormView("price", {name:"rejectNote", xtype:"cmm_PriceUpdate_Dc$ApprovalNote"})
		.addDcGridView("note", {name:"notesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcFormView("note", {name:"notesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addDcGridView("attachment", {name:"documentsList", _hasTitle_:true, xtype:"ad_Attachment_Dc$List"})
		.addDcGridView("attachmentPop", {name:"documentsAssignList", _hasTitle_:true, height:300, xtype:"ad_Attachment_Dc$attachmentAssignList",  cls:"sone-custom-panel-title"})
		.addDcFormView("attachment", {name:"documentsEdit", xtype:"ad_Attachment_Dc$New"})
		.addDcFormView("history", {name:"historyEdit", xtype:"fmbas_ChangeHistory_Dc$Edit"})
		.addDcGridView("history", {name:"historyList", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFilterFormView("priceCategory", {name:"priceCategoryFilter", xtype:"cmm_PriceUpdateCategories_Dc$Filter"})
		.addDcGridView("priceCategory", {name:"priceCategoryList", _hasTitle_:true, xtype:"cmm_PriceUpdateCategories_Dc$List"})
		.addDcFormView("priceNew", {name:"priceUpdateSteps", height:110, xtype:"cmm_PriceUpdate_Dc$priceUpdateSteps",  _wizardLineMargin_:100})
		.addDcFormView("priceNew", {name:"priceUpdateFirstStep", height:500, xtype:"cmm_PriceUpdate_Dc$priceUpdateFirstStep"})
		.addDcFormView("priceNew", {name:"priceUpdateSecondStep", height:500, xtype:"cmm_PriceUpdate_Dc$priceUpdateSecondStep"})
		.addDcFormView("priceNew", {name:"priceUpdateFourthStep", height:500, xtype:"cmm_PriceUpdate_Dc$priceUpdateFourthStep"})
		.addDcGridView("priceCategoryNew", {name:"contractPriceCategoryList", height:215, xtype:"cmm_PriceUpdateCategoriesInfinite_Dc$NewList",  style:"padding:0px 20px 0px 20px", listeners:{afterrender: {scope: this, fn: function(grid) {this._handleSelect_(grid)}}, selectionchange: {scope: this, fn: function(list, selectedRecord) {this._checkCurrencies_(list, selectedRecord)}}}, selModel:"{mode: 'SINGLE'}"})
		.addDcFormView("priceCategoryLabel", {name:"contractPriceCategoryLabel", xtype:"cmm_PriceUpdateCategoriesLabel_Dc$GridLabel",  style:"padding:20px 20px 0px 20px", split:false})
		.addDcEditGridView("priceCategoryAdd", {name:"addPriceCategoriesList", xtype:"cmm_PriceUpdateCategoriesAdd_Dc$AddList", frame:true,  listeners:{ afterrender:{fn: function(grid) {this._checkValidity_(grid)}, scope:this} }})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"priceUpdateStepThree", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"panelAssignAttachments", layout:{ type: "border" }, defaults:{split:true}})
		.addWindow({name:"wdwApprovalNote", _hasTitle_:true, width:600, height:530, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("panelAssignAttachments")],  closable:true, listeners:{ show:{fn:this.onApprovalWdwShow, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCancelApprovalWdw"), this._elems_.get("btnSaveCloseApprovalWdw")]}]})
		.addWindow({name:"wdwApproveNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("approveNote")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseApproveWdw"), this._elems_.get("btnCancelApproveWdw")]}]})
		.addWindow({name:"wdwRejectNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("rejectNote")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRejectWdw"), this._elems_.get("btnCancelRejectWdw")]}]})
		.addWindow({name:"noteWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("notesEdit")],  listeners:{ close:{fn:this.onNoteWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseNoteWdw"), this._elems_.get("btnCloseNoteWdw")]}]})
		.addWindow({name:"wdwAddPriceCategory", _hasTitle_:true, width:700, height:480, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("addPriceCategoriesList")],  listeners:{ close:{fn:this.onWdwAddPriceCategoryClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnClosePriceCategoryWdw"), this._elems_.get("btnCloseAddPriceCategoryWdw")]}]})
		.addWizardWindow({name:"wdwWizard", _hasTitle_:true, width:700, height:520,_labelPanelName_:"priceUpdateSteps"
			,_stepPanels_:["priceUpdateFirstStep","priceUpdateSecondStep","priceUpdateStepThree","priceUpdateFourthStep"]
			,  listeners:{ close:{fn:this._closeWizard_, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCancelCapturing"), this._elems_.get("btnContinueStep1"), this._elems_.get("btnBackToPriceCategory"), this._elems_.get("btnCancelCapturingStep2"), this._elems_.get("btnContinueStep2"), this._elems_.get("btnBackToCondition"), this._elems_.get("btnCancelCapturingStep3"), this._elems_.get("btnContinueStep3"), this._elems_.get("btnBackToContracts"), this._elems_.get("btnCancelCapturingStep4"), this._elems_.get("btnFinish")]}]})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"main", dcName:"price"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["priceList", "detailsTab"], ["north", "center"])
		.addChildrenTo("priceUpdateStepThree", ["contractPriceCategoryLabel", "contractPriceCategoryList"], ["north", "center"])
		.addChildrenTo("detailsTab", ["priceCategoryList"])
		.addChildrenTo("panelAssignAttachments", ["approvalNote", "documentsAssignList"], ["north", "center"])
		.addToolbarTo("priceList", "tlbPriceList")
		.addToolbarTo("priceCategoryList", "tlbPriceCategoryList")
		.addToolbarTo("documentsList", "tlbAttachments")
		.addToolbarTo("notesList", "tlbNoteList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("detailsTab", ["notesList", "documentsList", "historyList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbPriceList", {dc: "price"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewPriceUpdate"),this._elems_.get("btnEditPriceUpdate"),this._elems_.get("btnDeletePriceUpdate"),this._elems_.get("btnSubmitForApproval"),this._elems_.get("btnApprovePrice"),this._elems_.get("btnRejectPrice"),this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbPriceCategoryList", {dc: "priceCategory"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnAddPriceCategory"),this._elems_.get("btnRemovePriceCategory")])
			.addReports()
		.end()
		.beginToolbar("tlbAttachments", {dc: "attachment"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbNoteList", {dc: "note"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnCloseNoteWdw
	 */
	,onBtnCloseNoteWdw: function() {
		this.onNoteWdwClose();
		this._getWindow_("noteWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseNoteWdw
	 */
	,onBtnSaveCloseNoteWdw: function() {
		this.saveCloseNote();
		this._getDc_("note").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnAddPriceCategory
	 */
	,onBtnAddPriceCategory: function() {
		this.newPriceCategories();
	}
	
	/**
	 * On-Click handler for button btnRemovePriceCategory
	 */
	,onBtnRemovePriceCategory: function() {
		this._removePriceCategory_();
	}
	
	/**
	 * On-Click handler for button btnClosePriceCategoryWdw
	 */
	,onBtnClosePriceCategoryWdw: function() {
		this._getWindow_("wdwAddPriceCategory").close();
	}
	
	/**
	 * On-Click handler for button btnCloseAddPriceCategoryWdw
	 */
	,onBtnCloseAddPriceCategoryWdw: function() {
		this.saveCloseAddPriceCategory();
	}
	
	/**
	 * On-Click handler for button btnSubmitForApproval
	 */
	,onBtnSubmitForApproval: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApprovalWdw
	 */
	,onBtnSaveCloseApprovalWdw: function() {
		this.submitForApprovalRpc()
	}
	
	/**
	 * On-Click handler for button btnCancelApprovalWdw
	 */
	,onBtnCancelApprovalWdw: function() {
		this._getWindow_("wdwApprovalNote").close();
		this._getDc_("price").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCancelApproveWdw
	 */
	,onBtnCancelApproveWdw: function() {
		this._getWindow_("wdwApproveNote").close();
		this._getDc_("price").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCancelRejectWdw
	 */
	,onBtnCancelRejectWdw: function() {
		this._getWindow_("wdwRejectNote").close();
		this._getDc_("price").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnApprovePrice
	 */
	,onBtnApprovePrice: function() {
		this.openApproveWindow();
	}
	
	/**
	 * On-Click handler for button btnRejectPrice
	 */
	,onBtnRejectPrice: function() {
		this.openRejectWindow();
	}
	
	/**
	 * On-Click handler for button btnPublishPrice
	 */
	,onBtnPublishPrice: function() {
	}
	
	/**
	 * On-Click handler for button btnNewPriceUpdate
	 */
	,onBtnNewPriceUpdate: function() {
		this.newPriceUpdate();
	}
	
	/**
	 * On-Click handler for button btnEditPriceUpdate
	 */
	,onBtnEditPriceUpdate: function() {
	}
	
	/**
	 * On-Click handler for button btnDeletePriceUpdate
	 */
	,onBtnDeletePriceUpdate: function() {
		this._getDc_("price").doDelete();
	}
	
	/**
	 * On-Click handler for button btnContinueStep1
	 */
	,onBtnContinueStep1: function() {
		this._continueStep1_();
	}
	
	/**
	 * On-Click handler for button btnContinueStep2
	 */
	,onBtnContinueStep2: function() {
		this._continueStep2_();
	}
	
	/**
	 * On-Click handler for button btnContinueStep3
	 */
	,onBtnContinueStep3: function() {
		this._continueStep3_();
	}
	
	/**
	 * On-Click handler for button btnBackToPriceCategory
	 */
	,onBtnBackToPriceCategory: function() {
		this._backToPriceCategory_();
	}
	
	/**
	 * On-Click handler for button btnCancelCapturing
	 */
	,onBtnCancelCapturing: function() {
		this._getWindow_("wdwWizard").close();
	}
	
	/**
	 * On-Click handler for button btnCancelCapturingStep2
	 */
	,onBtnCancelCapturingStep2: function() {
		this._getWindow_("wdwWizard").close();
	}
	
	/**
	 * On-Click handler for button btnCancelCapturingStep3
	 */
	,onBtnCancelCapturingStep3: function() {
		this._getWindow_("wdwWizard").close();
	}
	
	/**
	 * On-Click handler for button btnCancelCapturingStep4
	 */
	,onBtnCancelCapturingStep4: function() {
		this._getWindow_("wdwWizard").close();
	}
	
	/**
	 * On-Click handler for button btnBackToCondition
	 */
	,onBtnBackToCondition: function() {
		this._backToConditions_();
	}
	
	/**
	 * On-Click handler for button btnBackToContracts
	 */
	,onBtnBackToContracts: function() {
		this._backToContracts_();
	}
	
	/**
	 * On-Click handler for button btnFinish
	 */
	,onBtnFinish: function() {
		this._finishWizard_();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApproveWdw
	 */
	,onBtnSaveCloseApproveWdw: function() {
		this.approveRpc()
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRejectWdw
	 */
	,onBtnSaveCloseRejectWdw: function() {
		this.rejectRpc()
	}
	
	,_when_called_from_dshboard_: function(id) {
		
						var dc = this._getDc_("price");
						dc.doCancel();
						dc.doClearAllFilters();
						dc.setFilterValue("id", id);				
						dc.doQuery(); 
	}
	
	,_checkCurrencies_: function(list,selectedRecord) {
		
						var grid = this._get_("contractPriceCategoryList");
						if (!grid._selectedAll_ || grid._selectedAll_ === false) {
							var i = 0, l = selectedRecord.length, b = this._get_("btnContinueStep3");
							for (i;i<l;i++) {
								if ((selectedRecord[i].get("currencyCode") !== selectedRecord[0].get("currencyCode")) && !Ext.isEmpty(selectedRecord[0].get("currencyCode"))) {
									b._disable_();
									return;
								}
								else {
									b._enable_();
								}
							}
						}
	}
	
	,_handleSelect_: function(grid) {
		
		
						var store = grid.getStore();
						var dc = this._getDc_("priceNew");
						var ctx = this;
		
						// Disable select / deselect on cell click if the clicked cell is not the one containing the checkbox
		
						grid.view.on("beforecellclick", function(view, cell, cellIndex, r, td, rowIndex, e) {
							if (cellIndex > 0) {
								return false;
							}
							if (e.shiftKey === true) {
								return false;
							}	
						});
		
						store.on("load", function() {
							if (grid._selectedAll_ === false) {
								grid.getSelectionModel().deselectAll();
							}
						}, this);
		
						grid.on("headerclick", function() {
							ctx._uncheckAll_();
						});
		
						grid.view.getEl().on("scroll", function() {
							if (grid._selectedAll_ === true) {
		
								var lastRequestStart = store.lastRequestStart;
								var lastRequestEnd = store.lastRequestEnd;
								grid.getSelectionModel().selectRange(lastRequestStart, lastRequestEnd, false);
		
								// Do not select the rows already being in the grid._deselectedIndexes_ array
		
								var l = grid._deselectedIndexes_.length, i = 0;
								if (l > 0) {
									for (i;i<l;i++) {								
										grid.getSelectionModel().deselect(grid._deselectedIndexes_[i]);
									}
								}			
		
							}
						});
		
						grid.on("itemclick", function(view, record, item, index, e) {
		
							if (grid._selectedAll_ === true) {					
		
								// If the index is already in the _deselectedIndexes_ array then remove it, else add it to the array
		
								var x = grid._deselectedIndexes_.indexOf(index);
								var id = record.get("id");
								var y = grid._deselectedIds_.indexOf(id);
		
								if (x > -1) {							
									grid._deselectedIndexes_.splice(x, 1);
								}
								else {
									grid._deselectedIndexes_.push(index);
								}
		
								if (y > -1) {							
									grid._deselectedIds_.splice(y, 1);
								}
								else {
									grid._deselectedIds_.push(id);
								}	
		
								dc.setParamValue("unSelectedCategories",grid._deselectedIds_.toString());
						
							}
		
						});
		
	}
	
	,_checkIfMultiplePrice_: function(list,selection) {
		
						var count = selection.length;
						var b = this._get_("btnRemovePriceCategory");
						if (count > 1 || count === 0) {
							b._disable_();
						}
						else {
							b._enable_();
						}
	}
	
	,_checkIfICanDeletePriceUpdateAndPrice_: function() {
		
		
		                var dc = this._getDc_("price");
		                var rec = dc.getRecord();
		                var result = false;
		 
		                if (rec) {
							var approvalStatus = rec.get("approvalStatus");
							var published = rec.get("published");
							if ((approvalStatus === __CMM_TYPE__.BidApprovalStatus._NEW_ || approvalStatus === __CMM_TYPE__.BidApprovalStatus._REJECTED_) && !published) {
								result = true;
							}					
						}
						return result;
	}
	
	,_checkValidity_: function(grid) {
		
		
						var view = grid.getView();
						var store = grid.store;
						var b = this._get_("btnCloseAddPriceCategoryWdw");
		
			            view.on("itemupdate", function () {
			            	var records = store.getRange();				
							for (var i = 0; i < records.length; i++) {
							    var rec = records[i];
							    if (rec.dirty === true) {
									b._enable_();
									break;
								}
								else {
									b._disable_();
								}
							}
			        	}, grid);
	}
	
	,onApprovalWdwShow: function() {
		
						var dc = this._getDc_("attachment");
						dc.doQuery();
	}
	
	,doDeleteWithCancel: function() {
		
		
						var dc = this._getDc_("priceNew");
						var r = dc.getRecord();
		
						if (r) {
							var rId = r.get("id");
							if(rId == null){
								dc.doCancel();
							} else {
								dc.commands.doDelete.confirmByUser = false ;
								if(dc.isDirty()){
									dc.doCancel();
								}
								dc.doDelete();
							}
						}
	}
	
	,_injectSearchField_: function(id,gridName,emptyLabelText,fieldWidth,showResetBtn,buttonInsideField,extraElems) {
		
		
						var defaultMargin = "0px 8px 0px 0px";
						var marginInside = "0px 8px 0px -42px";
						var buttonInsideCls = "sone-search-btn-inside";
						var defaultCls = "sone-search-btn-outside";
						var btnId = id+"-btn";				
		
						var toolbarItems = [{ 
							xtype: "textfield",
							id: id,
						    width: fieldWidth ? fieldWidth : 200,
						    emptyText: emptyLabelText,
						    listeners: {
						        change: {
						            scope: this,
									buffer: 1000,
						            fn: function(field, newVal) {
		
						                var grid = this._get_(gridName);
										var resetBtn = Ext.getCmp(btnId);							
						                
						                if (newVal && newVal != "*") {
		
											var af = [];
											var dateFilter = [];
											var dc = grid._controller_;
											var s = dc.store;
		
											// Check if the records have been changed and if yes discard the chages
		
											if (s.getNewRecords().length > 0 || s.getUpdatedRecords().length > 0 || s.getRemovedRecords().length > 0) {
												s.rejectChanges();
											}
		
											for(i=0;i<grid.columns.length;i++){
												var operation ="like";
												var value1 = "%"+newVal+"%";
		
												var o = {
													"id":null,
													"fieldName":grid.columns[i].dataIndex,
													"operation":operation,
													"value1": value1,
													"groupOp":"OR1"
												};
												if (grid.columns[i].hidden == false && grid.columns[i].dataIndex !== "price" && grid.columns[i].dataIndex !== "unitCode" && grid.columns[i].dataIndex !== "selected") {
													af.push(o);
												}
												
											}
		
											resetBtn.show();									
											dc.advancedFilter = af;
											dc.doQuery();
						                }
										else {
											resetBtn.hide();
										}
						            }
						        }
						    }
						},{
							glyph: "xf021@FontAwesome",
		                    xtype: "button",
							id : btnId,
							hidden: true,
							margin: buttonInsideField === true ? marginInside : defaultMargin,
							cls: buttonInsideField === true ? buttonInsideCls : defaultCls,
							listeners: {
								click: {
									scope: this,
									fn: function(b) {
										var searchFieldId = Ext.getCmp(id);
										var grid = this._get_(gridName);
										var dc = grid._controller_;
										searchFieldId.setValue("");
										b.hide();
										dc.advancedFilter = null;
										dc.doQuery();
									}
								}
							}
						}];
		
						if (extraElems) {
							var c = toolbarItems.concat(extraElems);
							toolbarItems = c;
						}
		
		
		
						return { tbar: toolbarItems}
	}
	
	,_selectAllRpc_: function(dc,showLastStep) {
		
						var ctx = this;
						var successFn = function(dc) {
							var w = ctx._getElement_("wdwWizard");
							var wizardForm = this._get_("priceUpdateFourthStep");
							var hideElems = ["btnBackToCondition", "btnCancelCapturingStep3", "btnContinueStep3"];
							var showElems = ["btnBackToContracts", "btnCancelCapturingStep4", "btnFinish"];
			
							var contractPriceCategoryList = ctx._get_("contractPriceCategoryList");
							var pcDc = contractPriceCategoryList._controller_;
							var pcRec = pcDc.getRecord();
							var puDc = ctx._getDc_("priceNew");
							var puRec = puDc.getRecord();
			
							ctx._sendSelectedCategories_();
			
							if (pcRec) {
								var currencyCode = pcRec.get("currencyCode");
								puRec.set("currencyCode", currencyCode);
			
								var unit = pcRec.get("unitCode");
								if(unit == "%"){
									puRec.set("unitCode", unit);
									var unitLastStep = wizardForm._get_("unitCode");
									unitLastStep.setReadOnly(true);
								} else if(unit == "EA"){
									puRec.set("unitCode", unit);
									var unitLastStep = wizardForm._get_("unitCode");
									unitLastStep.setReadOnly(true);
								}
							}
			
							// Change the panel from the center region
			
							Ext.each(hideElems, function(e) {
								ctx._get_(e).hide();
							}, this);
			
							Ext.each(showElems, function(e) {
								ctx._get_(e).show();
							}, this);
			
							puDc.setParamValue("filter",JSON.stringify(pcDc.advancedFilter));
							puDc.setParamValue("standardFilter",JSON.stringify(pcDc.getFilter().data));
		
							ctx._updateThirdFormLabel_();
							w.setStep(4);
			
							ctx._setMandatoryStep4_(true);
						};
						var callbacks = null;
						
						if (showLastStep && showLastStep === true) {
							callbacks = {
								successFn: successFn,
								successScope: this
							}
						}
						var o={
							name:"verify",
							modal:true,
							callbacks: callbacks
						};
						dc.doRpcData(o);
	}
	
	,_uncheckAll_: function() {
		
						var grid = this._get_("contractPriceCategoryList");   
		                var ctrl = grid._controller_;
		                var s = ctrl.store; 
		                var priceUpdateDc = this._getDc_("priceNew"); 
		                var viewSize = s.viewSize;
		                if (viewSize > 0) {
		                    grid.getSelectionModel().deselectAll();
		                    grid._selectedAll_ = false;
		                    priceUpdateDc.setParamValue("selectedAll", false);
		                    priceUpdateDc.setParamValue("unSelectedCategories","");
		                    grid._deselectedIds_ = [];
		                    grid._deselectedIndexes_ = [];
		                } 
	}
	
	,_uncheckAddPriceCategories_: function() {
		
						var grid = this._get_("addPriceCategoriesList");
		                var ctrl = grid._controller_;
		                var s = ctrl.store; 								
						grid.getSelectionModel().deselectAll();
						s.rejectChanges();
	}
	
	,onWdwAddPriceCategoryClose: function() {
		
						this._uncheckAddPriceCategories_();
						Ext.getCmp(this._addPriceCategorySearchFieldId_+"-btn").click();
	}
	
	,_endDefine_: function() {
		
						
						var builder = this._getBuilder_();
						this._priceCategorySearchFieldId_ = Ext.id();
						this._addPriceCategorySearchFieldId_ = Ext.id();
						this._documentsSearchFieldId_ = Ext.id();
		
						this._getBuilder_().change("contractPriceCategoryList", { 
		                    _deselectedIndexes_ : [],
		                    _deselectedIds_ : []
		                });
		                var selectDeselectAllButtons = [{
		                    glyph: "xf00c@FontAwesome",
		                    text: Main.translate("priceUpdateBtns", "checkAllBtn__lbl"),
		                    xtype: "button",
		                    listeners: {
		                        click: {
		                            scope: this,
		                            fn: function(b) {
		                                var grid = this._get_("contractPriceCategoryList");   
		                                var priceUpdateDc = this._getDc_("priceNew"); 
		                                var ctrl = grid._controller_;
		                                var s = ctrl.store; 
		                                var viewSize = s.viewSize;  
		                                var lastRequestEnd = s.lastRequestEnd;								
										var b = this._get_("btnContinueStep3");    
		        
		                                if (viewSize > 0) {									
											grid._deselectedIds_ = [];
		                                    grid._deselectedIndexes_ = [];
		                                    grid.getSelectionModel().selectRange(0, lastRequestEnd);
		                                    priceUpdateDc.setParamValue("selectedAll", true);
		                                    grid._selectedAll_ = true;
		                                    priceUpdateDc.setParamValue("filter",JSON.stringify(ctrl.advancedFilter));
		                                    priceUpdateDc.setParamValue("standardFilter",JSON.stringify(ctrl.getFilter().data));
											b._enable_();
		                                    this._selectAllRpc_(priceUpdateDc);
		                                }
		                            }
		                        }
		                    }
		                },{
		                    glyph: "xf00d@FontAwesome",
		                    text: Main.translate("priceUpdateBtns", "unCheckAllBtn__lbl"),
		                    xtype: "button",
		                    listeners: {
		                        click: {
		                            scope: this,
		                            fn: function(b) {								
										var b = this._get_("btnContinueStep3");
										b._disable_();
		                                this._uncheckAll_();                      
		                            }
		                        }
		                    }
		                }];
		
						var selectDeselectAllPriceCategories = [{
		                    glyph: "xf00c@FontAwesome",
		                    text: Main.translate("priceUpdateBtns", "checkAllBtn__lbl"),
		                    xtype: "button",
		                    listeners: {
		                        click: {
		                            scope: this,
		                            fn: function() {
		
		                                var grid = this._get_("addPriceCategoriesList");
		                                var ctrl = grid._controller_;
		                                var s = ctrl.store; 
		                                var totalCount = s.totalCount; 
										var recs = s.data.items;
		        
		                                if (totalCount > 0) {	
		                                    grid.getSelectionModel().selectRange(0, totalCount);
											Ext.each(recs, function(r) {
												r.set("selected", true);
											}, this);
		                                }
		                            }
		                        }
		                    }
		                },{
		                    glyph: "xf00d@FontAwesome",
		                    text: Main.translate("priceUpdateBtns", "unCheckAllBtn__lbl"),
		                    xtype: "button",
		                    listeners: {
		                        click: {
		                            scope: this,
		                            fn: function() {
										this._uncheckAddPriceCategories_();
		                            }
		                        }
		                    }
		                }];
		
						builder.change("contractPriceCategoryList",
							this._injectSearchField_(this._priceCategorySearchFieldId_,"contractPriceCategoryList","Search for contracts", 200, true, true, selectDeselectAllButtons)
						);
		
						builder.change("addPriceCategoriesList",this._injectSearchField_(this._addPriceCategorySearchFieldId_,"addPriceCategoriesList","Search for contracts", 200, true, true, selectDeselectAllPriceCategories));
						builder.change("documentsAssignList",this._injectSearchField_(this._documentsSearchFieldId_, "documentsAssignList","Search for documents", 200, true, false));
	}
	
	,_checkIfICanDeletePrice_: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							var approvalStatus = record.data.approvalStatus;
							var published = record.data.published;
							if(!((approvalStatus === __CMM_TYPE__.BidApprovalStatus._NEW_ || approvalStatus === __CMM_TYPE__.BidApprovalStatus._REJECTED_) && !published)) {
								return false;
							}	
						}
						return true;
	}
	
	,_checkIfICanDeletePriceUpdate_: function() {
		
		
		                var dc = this._getDc_("price");
		                var rec = dc.getRecord();
		                var result = false;
		 
		                if (rec) {
							var approvalStatus = rec.get("approvalStatus");
							var published = rec.get("published");
							if ((approvalStatus === __CMM_TYPE__.BidApprovalStatus._NEW_ || approvalStatus === __CMM_TYPE__.BidApprovalStatus._REJECTED_) && !published) {
								result = true;
							}
							
						}
						return result;
	}
	
	,_removePriceCategory_: function() {
		
						var dc = this._getDc_("priceCategory");
						dc.doDelete();
	}
	
	,_sendSelectedCategories_: function() {
		
						var list = this._get_("contractPriceCategoryList");
						var view = list.getView();
						var selectedPriceCategories = [];
						var dc = this._getDc_("priceNew");
						if (view.getSelectionModel().hasSelection()) {
						   var rows = view.getSelectionModel().getSelection();
						   Ext.each(rows, function(row) {
								selectedPriceCategories.push(row.data.id);
						   }, this);
						}
						dc.setParamValue("selectedCategories",JSON.stringify(selectedPriceCategories));
	}
	
	,_sendAttachments_: function() {
		
						var list = this._get_("documentsAssignList");
						var view = list.getView();
						var selectedAttachments = [];
						var dc = this._getDc_("price");
						if (view.getSelectionModel().hasSelection()) {
						   var rows = view.getSelectionModel().getSelection();
						   Ext.each(rows, function(row) {
								selectedAttachments.push(row.data.id);
						   }, this);
						}
						dc.setParamValue("selectedAttachments",JSON.stringify(selectedAttachments));
	}
	
	,_backToContracts_: function() {
		
		
						var w = this._getElement_("wdwWizard");
						var hideElems = ["btnBackToContracts", "btnCancelCapturingStep4", "btnFinish"];
						var showElems = ["btnBackToCondition", "btnCancelCapturingStep3", "btnContinueStep3"];
		
		
						Ext.each(hideElems, function(e) {
							this._get_(e).hide();
						}, this);
		
						Ext.each(showElems, function(e) {
							this._get_(e).show();
						}, this);	
		
						w.setStep(3);	
						this._getDc_("priceNew").doCancel();
						this._setMandatoryStep4_(false);
		
	}
	
	,_setMandatoryStep4_: function(isMandatory) {
		
		
						var thirdStepForm = this._get_("priceUpdateFourthStep");
		
						var fieldToSet1 = thirdStepForm._get_("newPrice");
						var fieldToSet2 = thirdStepForm._get_("unitCode");
						var fieldToSet3 = thirdStepForm._get_("validFrom");
						var fieldToSet4 = thirdStepForm._get_("reason");
		
						if(thirdStepForm.hidden == false && isMandatory === true){
							fieldToSet1.allowBlank = false ;
							fieldToSet2.allowBlank = false ;
							fieldToSet3.allowBlank = false ;
							fieldToSet4.allowBlank = false ;
						} else {
							fieldToSet1.allowBlank = true ;
							fieldToSet2.allowBlank = true ;
							fieldToSet3.allowBlank = true ;
							fieldToSet4.allowBlank = true ;
						}
	}
	
	,_finishWizard_: function() {
		
						var priceNew = this._getDc_("priceNew");
						this._doNotDelete_ = true;
						priceNew.doSave({closeWindow: true});
	}
	
	,_continueStep3_: function() {
				
						var puDc = this._getDc_("priceNew");	
						this._selectAllRpc_(puDc, true);				
	}
	
	,_backToConditions_: function() {
		
		
						var w = this._getElement_("wdwWizard");
						var hideElems = ["btnBackToCondition", "btnCancelCapturingStep3", "btnContinueStep3"];
						var showElems = ["btnBackToPriceCategory", "btnCancelCapturingStep2", "btnContinueStep2"];
		
						var grid = this._get_("contractPriceCategoryList");
						grid.getSelectionModel().deselectAll();
						grid._selectedAll_ = false;
		
						Ext.each(hideElems, function(e) {
							this._get_(e).hide();
						}, this);
		
						Ext.each(showElems, function(e) {
							this._get_(e).show();
						}, this);	
			
						w.setStep(2);
	}
	
	,_resetTriggerFields_: function() {
		
						var form = this._get_("priceUpdateSecondStep");
		
						var contractStatusEffective = form._get_("contractStatusEffective");
						var contractStatusExpired = form._get_("contractStatusExpired");
						var contractTypeSales = form._get_("contractTypeSales");
						var contractTypePurchase = form._get_("contractTypePurchase");
						var contractForProduct = form._get_("contractForProduct");
						var contractForService = form._get_("contractForService");
						var deliveryPointIntoPlane = form._get_("deliveryPointIntoPlane");
						var deliveryPointIntoStorage = form._get_("deliveryPointIntoStorage");
		
						contractStatusEffective.setValue(true);
						contractStatusExpired.setValue(false);
		
						contractTypeSales.setValue(true);
						contractTypePurchase.setValue(false);
		
						contractForProduct.setValue(true);
						contractForService.setValue(false);
		
						deliveryPointIntoPlane.setValue(true);
						deliveryPointIntoStorage.setValue(false);
		
	}
	
	,_closeWizard_: function() {
		
						if (this._doNotDelete_ === null) {
							this.doDeleteWithCancel();
						}
						this._resetWizard_();
						this._setMandatoryStep4_(false);
						var price = this._getDc_("price");
		
						var priceUpdateDc = this._getDc_("priceNew");
						var grid = this._get_("contractPriceCategoryList");
		
						priceUpdateDc.setParamValue("unSelectedCategories",null);
						priceUpdateDc.setParamValue("filter",null);
						priceUpdateDc.setParamValue("standardFilter",null);
						
						this._uncheckAll_();
		
						price.doReloadPage();
	}
	
	,_resetWizard_: function() {
		
		
						var w = this._getElement_("wdwWizard");
						var hideElems = ["btnBackToPriceCategory","btnCancelCapturingStep2", "btnContinueStep2","btnBackToCondition", "btnCancelCapturingStep3", "btnContinueStep3","btnBackToContracts", "btnCancelCapturingStep4", "btnFinish"];
						var showElems = ["btnCancelCapturing", "btnContinueStep1"];
			
						var priceNew = this._getDc_("priceNew");
						
						priceNew.doCancel();
						priceNew.clearData();		
		
						// Change the panel from the center region
		
						Ext.each(hideElems, function(e) {
							this._get_(e).hide();
						}, this);
		
						Ext.each(showElems, function(e) {
							this._get_(e).show();
						}, this);
		
						this._resetTriggerFields_();
						this._doNotDelete_ = null;
		
						Ext.getCmp(this._priceCategorySearchFieldId_+"-btn").click();
				
						w.setStep(1);
	}
	
	,_backToPriceCategory_: function() {
		
		
						var w = this._getElement_("wdwWizard");
		
						var hideElems = ["btnBackToPriceCategory","btnCancelCapturingStep2", "btnContinueStep2"];
						var showElems = ["btnCancelCapturing", "btnContinueStep1"];
		
						// Change the panel from the center region
		
						Ext.each(hideElems, function(e) {
							this._get_(e).hide();
						}, this);
		
						Ext.each(showElems, function(e) {
							this._get_(e).show();
						}, this);
		
						w.setStep(1);
	}
	
	,_continueStep1_: function() {
		
		
						var w = this._getWindow_("wdwWizard");
		
						var hideElems = ["btnCancelCapturing","btnContinueStep1"];
						var showElems = ["btnBackToPriceCategory", "btnCancelCapturingStep2", "btnContinueStep2"];
		
						// Change the panel from the center region
		
						Ext.each(hideElems, function(e) {
							this._get_(e).hide();
						}, this);
		
						Ext.each(showElems, function(e) {
							this._get_(e).show();
						}, this);
		
						w.setStep(2);
	}
	
	,_continueStep2_: function() {
		
		
						var priceNew = this._getDc_("priceNew");
						var r = priceNew.getRecord();
		
						if (r) {
							if(r.dirty === true){
								priceNew.doSave({doSaveStepTwo:true});
							} else {
								this.showStepThree();
							}
						}
	}
	
	,_updateGridLabel_: function() {
		
		
						var wizardForm = this._get_("priceUpdateFirstStep");
		
						var priceCategoryName = wizardForm._get_("priceCategoryName");
						var priceCategoryValue = priceCategoryName.getValue();
						var contractPriceCategoryLabel = this._get_("contractPriceCategoryLabel");
						var gridLabel = contractPriceCategoryLabel._get_("priceCategoryLabel");
						var originalLabel = gridLabel.fieldLabel;
						var newlabel = originalLabel.replace("[price_category]","<strong>"+priceCategoryValue+"</strong>");
						gridLabel.labelEl.update(newlabel);
		
	}
	
	,_updateThirdFormLabel_: function() {
		
		
						var firstForm = this._get_("priceUpdateFirstStep");
						var priceCategoryName = firstForm._get_("priceCategoryName");
						var priceCategoryValue = priceCategoryName.getValue();
		
						var fourthForm = this._get_("priceUpdateFourthStep");
						var label = fourthForm._get_("enterPriceLabel");
		
						var originalLabel = label.fieldLabel;
						var newlabel = originalLabel.replace("[price_category]","<strong>"+priceCategoryValue+"</strong>");
						label.labelEl.update(newlabel);
		
	}
	
	,newPriceUpdate: function() {
		
						var w = this._getWindow_("wdwWizard");
						var priceNew = this._getDc_("priceNew");
						priceNew.doNew();
						w.show();
						// dupa ce s-a creat obiectul, pot sa am acces la el
						var f = this._getElement_("priceUpdateSteps");
						f.setStep(1);
	}
	
	,newPriceCategories: function() {
		
						var w = this._getWindow_("wdwAddPriceCategory");
						var price = this._getDc_("price");
						var priceCategoryAdd  = this._getDc_("priceCategoryAdd");
						var rec = price.getRecord();
						if(rec) {
							var refId = rec.get("refid");
		 					priceCategoryAdd.setParamValue("parentPriceUpdateRefId",refId);
						}			
						w.show();
						priceCategoryAdd.doCancel();
						priceCategoryAdd.advancedFilter = null;
						priceCategoryAdd.doQuery();
		
	}
	
	,_afterDefineDcs_: function() {
		
					var note = this._getDc_("note");
				    var priceNew = this._getDc_("priceNew");
				    var priceCategoryAdd = this._getDc_("priceCategoryAdd");
					var priceCategoryNew = this._getDc_("priceCategoryNew");
					this._doNotDelete_ = null;
		
					note.on("afterDoQuerySuccess", function (dc) {
						var rec = dc.getRecord();
					    if (rec) {
							var date = rec.get("createdAt");
							var newDate = ("0" + date.getDate()).slice(-2)+"/"+("0" + (date.getMonth() + 1)).slice(-2)+"/"+date.getFullYear();
					    }
					}, this);
		
					priceCategoryNew.on("afterDoQuerySuccess", function (dc) {
						var puDc = this._getDc_("priceNew");
						puDc.setParamValue("filter",JSON.stringify(dc.advancedFilter));
						puDc.setParamValue("standardFilter",JSON.stringify(dc.getFilter().data));
					}, this);
		
					note.on("afterDoNew", function (dc) { 
						dc.setEditMode();
						this._getWindow_("noteWdw").show();
					}, this);
				
					note.on("afterDoEditIn", function (dc) {
						this._getWindow_("noteWdw").show();
					}, this);
					
					note.on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("noteWdw").close();
						}
					}, this);
		
					priceNew.on("afterDoSaveSuccess", function (dc, ajaxResponse) {
			
						if(ajaxResponse.options.options.doSaveStepTwo === true){
							this.showStepThree();
						}
			
						var closeWdwFlag = ajaxResponse.options.options.closeWindow;
						if (closeWdwFlag && closeWdwFlag === true) {
							var window = this._getWindow_("wdwWizard");
							window.close();
						}
						this._applyStateAllButtons_();
		
					}, this);
		
		
					priceCategoryAdd.on("afterDoSaveSuccess", function (dc, ajaxResponse) {
			
						var closeWdwFlag = ajaxResponse.options.options.doCloseNewWindowAfterSave;
						if (closeWdwFlag && closeWdwFlag === true) {
							var window = this._getWindow_("wdwAddPriceCategory");
							window.close();
						}
		
						var priceCategoryListDc = this._getDc_("priceCategory");
						priceCategoryListDc.doReloadPage();
		
					}, this);
		
	}
	
	,showStepThree: function() {
		
		
						var w = this._get_("wdwWizard");
						var dc = this._getDc_("priceCategoryNew");
						var hideElems = ["btnBackToPriceCategory","btnCancelCapturingStep2", "btnContinueStep2"];
						var showElems = ["btnBackToCondition", "btnCancelCapturingStep3", "btnContinueStep3"];
						var price = this._getDc_("price");
			
						Ext.each(hideElems, function(e) {
							this._get_(e).hide();
						}, this);
		
						Ext.each(showElems, function(e) {
							this._get_(e).show();
						}, this);
		
						w.setStep(3);
						this._updateGridLabel_();
						
						price.doReloadPage();
						dc.doQuery();
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/PriceMaintenance.html";
					window.open( url, "SONE_Help");
	}
	
	,onNoteWdwClose: function() {
		
						var dc = this._getDc_("note");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,saveCloseNote: function() {
		
					var note = this._getDc_("note");
					note.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveCloseAddPriceCategory: function() {
		
					var dc = this._getDc_("priceCategoryAdd");
					dc.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,useWorkflowForApproval: function(dc) {
		
						return (_SYSTEMPARAMETERS_.syspriceupdateapproval === "true" || _SYSTEMPARAMETERS_.syspriceupdateapproval === "TRUE") ;
	}
	
	,canSubmitForApproval: function(dc) {
		
						//if sys param "Price Update Workflow" is set to false then the "submit for approval" operation is disabled
						if ( !this.useWorkflowForApproval(dc) ) { 
							return false; 
						}
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if( record.data.published === true || (record.data.approvalStatus != __CMM_TYPE__.BidApprovalStatus._NEW_ && record.data.approvalStatus != __CMM_TYPE__.BidApprovalStatus._REJECTED_) ){
								return false;			
							}
						}
						return true;
	}
	
	,canApprove: function(dc) {
		
						//if sys param "Price Update Workflow" is set to false then the "submit for approval" operation is disabled
						if ( !this.useWorkflowForApproval(dc) ) { 
							return false; 
						}
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if (record.data.published === true || record.data.approvalStatus != __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_ || record.data.visibleFlag !== true){
								return false;			
							}
						}
						return true;
	}
	
	,openSubmitForApprovalWindow: function() {
		
						var label = "Add your note to be sent to your request to the approvers";
						var title = "Submit for approval";
						this.setupPopUpWindow("wdwApprovalNote","approvalNote","btnSaveCloseApprovalWdw","btnCancelApprovalWdw" , label , title);				
	}
	
	,openApproveWindow: function() {
		
						var label = "Please provide an approval reason.";
						var title = "Approve";
						this.setupPopUpWindow("wdwApproveNote","approveNote","btnSaveCloseApproveWdw","btnCancelApproveWdw" , label , title);				
	}
	
	,openRejectWindow: function() {
		
						var label = "Please provide a reason for rejection.";
						var title = "Reject";
						this.setupPopUpWindow("wdwRejectNote","rejectNote","btnSaveCloseRejectWdw","btnCancelRejectWdw" , label , title);				
	}
	
	,setupPopUpWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var remarksField = this._get_(theForm)._get_("remarks");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						var ctx = this;
						
						saveButton.setDisabled(true);
						
						remarksField.fieldLabel = label;
		
						remarksField.setValue("");
						window.title = title;
		
						remarksField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
								cancelButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
						    }
						});
						var priceDc = this._getDc_("price");
		
						//set the default remark as the reason
						if ("wdwApprovalNote" == theWindow) {
							var view = this._get_("documentsAssignList");
							view.store.load();
							var firstSelectedPrice = priceDc.selectedRecords[0];
							if( firstSelectedPrice) {
								remarksField.setValue(firstSelectedPrice.data.reason);
							}
						}	
						window.show();
	}
	
	,submitForApprovalRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								var responseDescription = responseData.params.submitForApprovalDescription;							
								var responseResult = responseData.params.submitForApprovalResult;
								
								//close window
								this._getWindow_("wdwApprovalNote").close();
								
								//display results (only if error)
								if (responseResult == false) {
									Main.error(responseDescription);
								}
								//reload page
								dc.doReloadPage();
							}
						};
						this._sendAttachments_();
		                var dc = this._getDc_("price");
		                var t = {
					        name: "submitForApproval",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcData(t);
	}
	
	,approveRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								//close window
								this._getWindow_("wdwApproveNote").close();
								
								//reload page
								dc.doReloadPage();
							}
						};
		
		                var dc = this._getDc_("price");
		                var t = {
					        name: "approve",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcData(t);
	}
	
	,rejectRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								//close window
								this._getWindow_("wdwRejectNote").close();
								
								//reload page
								dc.doReloadPage();
							}
						};
		
		                var dc = this._getDc_("price");
		                var t = {
					        name: "reject",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcData(t);
	}
});
