/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.ImportBid_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.TenderLocation_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.TenderLocation_Ds
});

/* ================= EDIT FORM: NewWithName ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ImportBid_Dc$NewWithName", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ImportBid_Dc$NewWithName",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btnSelectFile", scope: this, handler: this._selectFile_, text: "Select file"})
		.addButton({name:"btnCancel", scope: this, handler: this._cancel_, text: "Cancel"})
		.addCombo({ xtype:"combo", name:"importBidFileType", bind:"{d.importBidFileType}", dataIndex:"importBidFileType", allowBlank:false, width:235, store:[ __CMM_TYPE__.ImportBidFileType._XLS_, __CMM_TYPE__.ImportBidFileType._XML_], labelAlign:"top",listeners:{
			change:{scope:this, fn:this.setupWindow}
		}})
		.addUploadField({ name:"file", bind:"{d.filePath}", dataIndex:"filePath", style:"display:none", listeners:{change: {scope: this,fn:function(e) {this._save_()}}}})
		.addTextField({ name:"locationCode", bind:"{d.locationCode}", dataIndex:"locationCode", maxLength:25, style:"display:none"})
		.addNumberField({name:"locationId", bind:"{d.locationId}", dataIndex:"locationId", maxLength:11, style:"display:none"})
		.addNumberField({name:"id", bind:"{d.id}", dataIndex:"id", maxLength:11, style:"display:none"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, buttons: [this._getConfig_("btnSelectFile"),this._getConfig_("btnCancel")],  xtype:"panel", buttonAlign:"left"})
		.addPanel({ name:"col", width:235, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col"])
		.addChildrenTo("col", ["importBidFileType", "file", "locationCode", "locationId", "id"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableDisableSelectBtn_: function(el) {
		
						var importBidFileType = this._get_("importBidFileType");
						var selectFileField = this._get_("btnSelectFile");
						var importBidFileTypeValue = importBidFileType.getValue();
						if (!Ext.isEmpty(importBidFileTypeValue)) {
							selectFileField._enable_();
						}
						else {
							selectFileField._disable_();
						}
	},
	
	setupWindow: function(el) {
		
		
						this._enableDisableSelectBtn_(el);
						var f = this._get_("importBidFileType");
						var v = f.getValue();
						var ctrl = this._controller_;
						var f = ctrl.getFrame();
		
						if (v === __CMM_TYPE__.ImportBidFileType._XLS_) {
							ctrl._uploadAcceptedFileTypes_ = "application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
						}
						else if (v === __CMM_TYPE__.ImportBidFileType._XML_) {
							ctrl._uploadAcceptedFileTypes_ = "application/xml";
						}
		
	},
	
	_selectFile_: function() {
		
						var fileUploadField = this._get_("file");
						var ctrl = this._controller_;
						var frame = ctrl.getFrame();
						var importBidDc = frame._getDc_("importBid");
		
						var elemId = fileUploadField.button.fileInputEl.dom.getAttribute("id");
						var elem = document.getElementById(elemId);
		
						if (importBidDc._uploadAcceptedFileTypes_) {
							elem.setAttribute("accept",importBidDc._uploadAcceptedFileTypes_);
						}
					   if(elem && document.createEvent) {
					      var evt = document.createEvent("MouseEvents");
					      evt.initEvent("click", true, false);
					      elem.dispatchEvent(evt);
					   }
	},
	
	_save_: function() {
		 
						var me = this;
						var ctrl = me._controller_;
						var frame = ctrl.getFrame();
						var fileUploadField = this._get_("file");
						var uploadFieldValue = fileUploadField.getValue();
						if (me.isValid() && !Ext.isEmpty(uploadFieldValue)) {
							this._doUploadFile_(frame);
						}
	},
	
	_cancel_: function() {
		
						this.up("window").hide();
						this._controller_.doCancel();
	},
	
	_doUploadFile_: function(ctx) {
		
		
						var window = this._controller_._doNewWdw_;
						var form = window.items.get(0);
						var dc =  ctx._controller_;
						var passedOptions = this._controller_._passedOptions_;
		
						var cfg = {
				                _handler_ : "uploadBid",
				                _succesCallbackScope_ : this._controller_,
								_succesCallbackFn_ : function() {
									this.store.commitChanges();
									this.doCancel();
									this.doReloadPage();
							
									window.close();
								}
				        };
						Ext.apply(window,cfg);
						var formCfg = {
							fileUpload : true
						}
						Ext.apply(form,formCfg);
						if (form.getForm().isValid()) {
							form.getForm().submit(
									{
										url : Main.urlUpload + "/" + window._handler_,
										waitMsg : Main.translate("msg", "uploading"),
										scope : window,
										success : function(a,b) {
											if (passedOptions && !Ext.isEmpty(passedOptions.postDoNewFn)) {
												passedOptions.postDoNewFn(passedOptions.postNewCtrl,passedOptions.postRefreshCtrl1,passedOptions.postRefreshCtrl2);
											}
		
											try {
												Ext.Msg.hide();
											} catch (e) {
												// nothing to do
											}
											if (this._succesCallbackFn_ != null) {
												this._succesCallbackFn_.call(this._succesCallbackScope_ || this);
											}
											
											var responseData = Ext.getResponseDataInJSON(b.response);
											if (responseData && responseData.msg) {
												Main.info(responseData.msg);
												var locationBid = ctx._getDc_("locationBid");
												var tenderBid = ctx._getDc_("tenderBid");
												if (locationBid){
													locationBid.doReloadPage();
												}
												if (tenderBid){
													tenderBid.doReloadPage();
												}
											}
										},
										failure : function(form, action) {
											try {
												Ext.Msg.hide();
											} catch (e) {
												// nothing to do
											}
											if( action.failureType === Ext.form.Action.CLIENT_INVALID ){
												Main.error("INVALID_FORM");
											} else {
												Main.serverMessage(null, action.response);
											}
										}
									});
						} else {
							Main.error("INVALID_FORM");
						}
		
	}
});
