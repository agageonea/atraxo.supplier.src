/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.PricingBaseGrouping_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.PricingBaseGrouping_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.PricingBaseGrouping_Ds
});

/* ================= GRID: List ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PricingBaseGrouping_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_PricingBaseGrouping_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"priceCatType", dataIndex:"priceCatType", width:100})
		.addTextColumn({ name:"priceCatName", dataIndex:"priceCatName", width:100})
		.addTextColumn({ name:"mainCategoryCode", dataIndex:"mainCategoryCode", width:100})
		.addNumberColumn({ name:"initialPrice", dataIndex:"initialPrice", width:100, decimals:6})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
