/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.TenderInvitation_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.Tender_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.Tender_Ds
});

/* ================= EDIT FORM: wizardHeader ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderInvitation_Dc$wizardHeader", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderInvitation_Dc$wizardHeader",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLabel({ name:"phase1"})
		.addLabel({ name:"phase2"})
		.addLabel({ name:"phase3"})
		.addLabel({ name:"phase4"})
		/* =========== containers =========== */
		.addPanel({name:"main"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addWizardLabels("main", ["phase1", "phase2", "phase3", "phase4"]);
	}
});

/* ================= EDIT FORM: AssignButtonsForm ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderInvitation_Dc$AssignButtonsForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderInvitation_Dc$AssignButtonsForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btnAssignSelectedRecords", scope: this, handler: this.callAssignSelectedRecords, text: "", glyph:"xf105@FontAwesome", cls:"sone-flat-button"})
		.addButton({name:"btnDeAssignSelectedRecords", scope: this, handler: this.callDeAssignSelectedRecords, text: "", glyph:"xf104@FontAwesome", cls:"sone-flat-button"})
		.addButton({name:"btnAssignAllRecords", scope: this, handler: this.callAssignAllRecords, text: "", glyph:"xf101@FontAwesome", cls:"sone-flat-button"})
		.addButton({name:"btnDeAssignAllRecords", scope: this, handler: this.callDeAssignAllRecords, text: "", glyph:"xf100@FontAwesome", cls:"sone-flat-button"})
		/* =========== containers =========== */
		.addPanel({ name:"main", width:30,  autoScroll:false})
		.addPanel({ name:"col1", width:30, height: 200, layout: {type: "vbox", align: "center", pack: "center"}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["btnAssignSelectedRecords", "btnDeAssignSelectedRecords", "btnAssignAllRecords", "btnDeAssignAllRecords"]);
	},
	/* ==================== Business functions ==================== */
	
	callAssignSelectedRecords: function() {
		
						var dc = this._controller_;
						var frame = dc.getFrame();
						frame._assignSelectedRecords_();
	},
	
	callDeAssignSelectedRecords: function() {
		
						var dc = this._controller_;
						var frame = dc.getFrame();
						frame._deAssignSelectedRecords_();
	},
	
	callAssignAllRecords: function() {
		
						var dc = this._controller_;
						var frame = dc.getFrame();
						frame._assignAllRecords_();
	},
	
	callDeAssignAllRecords: function() {
		
						var dc = this._controller_;
						var frame = dc.getFrame();
						frame._deAssignAllRecords_();
	}
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.cmm.ui.extjs.dc.TenderInvitation_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.cmm_TenderInvitation_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"tenderCode", dataIndex:"code", maxLength:20,
				editor:{xtype:"atraxo.cmm.ui.extjs.lov.TenderCodeLov_Lov", selectOnFocus:true, maxLength:20,
					retFieldMapping: [{lovField:"name", dsField: "name"} ]}})
			.addTextField({ name:"tenderName", dataIndex:"name", maxLength:100})
			.addLov({name:"tenderHolderCode", dataIndex:"holderCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "holderId"} ]}})
			.addCombo({ xtype:"combo", name:"type", dataIndex:"type", store:[ __CMM_TYPE__.TenderType._PRODUCT_, __CMM_TYPE__.TenderType._FUELING_SERVICE_]})
			.addDateField({name:"biddingEnds", dataIndex:"biddingPeriodTo"})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __CMM_TYPE__.TenderStatus._DRAFT_, __CMM_TYPE__.TenderStatus._NEW_, __CMM_TYPE__.TenderStatus._CANCELED_, __CMM_TYPE__.TenderStatus._UPDATED_, __CMM_TYPE__.TenderStatus._EXPIRED_]})
			.addCombo({ xtype:"combo", name:"bidingStatus", dataIndex:"biddingStatus", store:[ __CMM_TYPE__.BiddingStatus._NEW_, __CMM_TYPE__.BiddingStatus._IN_NEGOTIATION_, __CMM_TYPE__.BiddingStatus._FINISHED_, __CMM_TYPE__.BiddingStatus._NO_BID_]})
			.addBooleanField({ name:"read", dataIndex:"read"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderInvitation_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_TenderInvitation_Dc$List",
	_noExport_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"tenderStatus", dataIndex:"status", width:120,  draggable:false, hideable:false, renderer:function(value, metaData) {return this._badgifyColumn_(value); }})
		.addTextColumn({ name:"code", dataIndex:"code", width:200})
		.addTextColumn({ name:"name", dataIndex:"name", width:200})
		.addTextColumn({ name:"holderName", dataIndex:"holderName", width:200})
		.addTextColumn({ name:"type", dataIndex:"type", hidden:true, width:140})
		.addNumberColumn({ name:"totalVolume", dataIndex:"totalVolume", width:200, sysDec:"dec_unit"})
		.addTextColumn({ name:"volumeUnit", dataIndex:"volumeUnit", width:60})
		.addDateColumn({ name:"biddingPeriodTo", dataIndex:"biddingPeriodTo", width:110, _mask_: Masks.DATE})
		.addTextColumn({ name:"biddingStatus", dataIndex:"biddingStatus", hidden:true, width:110})
		.addNumberColumn({ name:"noIfRounds", dataIndex:"noOfRounds", hidden:true, width:120})
		.addTextColumn({ name:"read", dataIndex:"read", hidden:true, width:100,  renderer:function(value,metaData) {return this._isRead_(value); }})
		.addDateColumn({ name:"biddingPeriodFrom", dataIndex:"biddingPeriodFrom", hidden:true, _mask_: Masks.DATE})
		.addNumberColumn({ name:"tenderVersion", dataIndex:"tenderVersion", hidden:true})
		.addNumberColumn({ name:"msgId", dataIndex:"msgId", hidden:true})
		.addTextColumn({ name:"contact", dataIndex:"contactPerson", hidden:true, width:200})
		.addTextColumn({ name:"email", dataIndex:"email", hidden:true, width:200})
		.addTextColumn({ name:"phone", dataIndex:"phone", hidden:true, width:120})
		.addTextColumn({ name:"comments", dataIndex:"comments", hidden:true, width:200})
		.addTextColumn({ name:"transmissionStatus", dataIndex:"transmissionStatus", hidden:true, width:140})
		.addTextColumn({ name:"source", dataIndex:"source", hidden:true, width:100})
		.addDateColumn({ name:"canceledOn", dataIndex:"canceledOn", hidden:true, _mask_: Masks.DATE})
		.addDateColumn({ name:"updatedOn", dataIndex:"updatedOn", hidden:true, _mask_: Masks.DATE})
		.addDateColumn({ name:"publishedOn", dataIndex:"publishedOn", hidden:true, _mask_: Masks.DATE})
		.addTextColumn({ name:"subsidiaryCode", dataIndex:"subsidiaryCode", hidden:true, width:100})
		.addTextColumn({ name:"schemaVersion", dataIndex:"schemaVersion", hidden:true, width:120})
		.addTextColumn({ name:"volumeOffer", dataIndex:"volumeOffer", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_isRead_: function(value) {
		
						var glyph, cls, text, rpcName, msg, action;
						var id = Ext.id();
						var ctx = this;
						if (value === true) {
							glyph = "xf2b7@FontAwesome";
							cls = "sone-tender-btn-read";
							text = "Read";
							rpcName = "markUnread";
							msg = Main.translate("applicationMsg","tenderMarkedAsUnRead__lbl");
							action = false;
						} else {
							glyph = "xf003@FontAwesome";
							cls = "sone-tender-btn-unread";
							text = "Unread";
							rpcName = "markRead";
							msg = Main.translate("applicationMsg","tenderMarkedAsRead__lbl");
							action = true;
						}
					    Ext.defer(function () {
					        Ext.widget("button", {
					            renderTo: document.getElementById(id),
								glyph: glyph,
								cls: cls,
								text: text,
								style: "padding:0px !important; border:0px !important; background:transparent !important",
					            handler: function () { 
									var f = function() {
										ctx._markTender_(rpcName,msg,action)
									}
									Ext.defer(f,300,ctx);
								}
					        });
					    }, 50);
					    return Ext.String.format("<div id='{0}'></div>", id);
	},
	
	_markTender_: function(rpcName,msg,action,viewClicked) {
		
						var ctrl = this._controller_;
						var frame = ctrl.getFrame();
						var successFn = function(dc) {
							ctrl.doReloadRecord();	
						}
						var o={
							name:rpcName,
							modal:true,
							callbacks:{
								successFn: successFn,
								successScope: this
							}
						};
						var record = ctrl.getRecord();
						if(record) {
							var source = record.get("source");
							if(source === __CMM_TYPE__.TenderSource._IMPORTED_){ 
								ctrl.doRpcData(o);
							}
						}
						
	},
	
	_badgifyColumn_: function(value) {
		
						var badgeCls = "sone-badge-cyan";
						if (value === __CMM_TYPE__.TenderStatus._CANCELED_) {
							badgeCls = "sone-badge-red";
						}
						else if (value === __CMM_TYPE__.TenderStatus._EXPIRED_) {
							badgeCls = "sone-badge-dark";
						}
						else if (value === __CMM_TYPE__.TenderStatus._UPDATED_ ) {
							badgeCls = "sone-badge-yellow";
						}				
						else if (value === __CMM_TYPE__.TenderStatus._DRAFT_) {
							badgeCls = "sone-badge-blue";
						}
						else if (value === __CMM_TYPE__.TenderStatus._NEW_) {
							badgeCls = "sone-badge-green";
						}
						var badge = "<div class='sone-badge "+badgeCls+"'>"+value+"</div>";
						return badge;
	},
	
	_injectVolume_: function() {
		
		
						//find the current volume unit to use
						var currentVolumeUnit = null;
						var sessionVolumeUnit = getApplication().getSession().getUser().volumeUnitCode;
		
						if( sessionVolumeUnit != null ){
							currentVolumeUnit = sessionVolumeUnit;
						}else{
							if( _ACTIVESUBSIDIARY_.volumeUnitCode != ""){
								currentVolumeUnit = _ACTIVESUBSIDIARY_.volumeUnitCode
							}else{
								currentVolumeUnit = _SYSTEMPARAMETERS_.sysvol
							}
						}
		
						var view = this.getView();
						var headerCt = view.getHeaderCt();
		
						var volume = headerCt.down("[dataIndex=totalVolume]");
		                volume.setText(volume.initialConfig.header + " (" + currentVolumeUnit + ")");
		
	}
});

/* ================= EDIT FORM: New ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderInvitation_Dc$New", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderInvitation_Dc$New",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"tenderName", bind:"{d.name}", dataIndex:"name", allowBlank:false, width:140, noLabel: true, maxLength:100})
		.addTextField({ name:"tenderStatus", bind:"{d.status}", dataIndex:"status", width:140, noLabel: true, maxLength:32})
		.addNumberField({name:"holderId", bind:"{d.holderId}", dataIndex:"holderId", allowBlank:false, maxLength:11, style:"display:none !important"})
		.addLov({name:"tenderHolderCode", bind:"{d.holderCode}", dataIndex:"holderCode", allowBlank:false, width:140, noLabel: true, xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "holderId"} ],listeners:{
			fpchange:{scope:this, fn:this._enableDisableContact_}
		}})
		.addNumberField({name:"tenderVersion", bind:"{d.tenderVersion}", dataIndex:"tenderVersion", allowBlank:false, width:140, noLabel: true, maxLength:6, colspan:1})
		.addNumberField({name:"contactId", bind:"{d.contactId}", dataIndex:"contactId", allowBlank:false, maxLength:11, style:"display:none !important"})
		.addLov({name:"contact", bind:"{d.contactPerson}", dataIndex:"contactPerson", noEdit:true , allowBlank:false, width:140, noLabel: true, xtype:"fmbas_ContactsLov_Lov", maxLength:200, colspan:1,
			retFieldMapping: [{lovField:"id", dsField: "contactId"} ,{lovField:"email", dsField: "email"} ,{lovField:"businessPhone", dsField: "phone"} ],
			filterFieldMapping: [{lovField:"objectId", dsField: "holderId"}, {lovField:"objectType", value: "Customer"} ],listeners:{
			fpchange:{scope:this, fn:this._checkFormValidity_}
		}})
		.addTextField({ name:"email", bind:"{d.email}", dataIndex:"email", allowBlank:false, width:140, noLabel: true, maxLength:100})
		.addTextField({ name:"phone", bind:"{d.phone}", dataIndex:"phone", width:140, noLabel: true, maxLength:50})
		.addCombo({ xtype:"combo", name:"tenderType", bind:"{d.type}", dataIndex:"type", allowBlank:false, width:140, noLabel: true, store:[ __CMM_TYPE__.TenderType._PRODUCT_, __CMM_TYPE__.TenderType._FUELING_SERVICE_]})
		.addDateField({name:"biddingPeriodFrom", bind:"{d.biddingPeriodFrom}", dataIndex:"biddingPeriodFrom", allowBlank:false, width:120, noLabel: true})
		.addDateField({name:"biddingPeriodTo", bind:"{d.biddingPeriodTo}", dataIndex:"biddingPeriodTo", allowBlank:false, width:120, noLabel: true, colspan:2})
		.addTextArea({ name:"comments", bind:"{d.comments}", dataIndex:"comments", noLabel: true})
		.addCombo({ xtype:"combo", name:"type", bind:"{d.type}", dataIndex:"type", allowBlank:false, width:140, noLabel: true, store:[ __CMM_TYPE__.TenderType._PRODUCT_, __CMM_TYPE__.TenderType._FUELING_SERVICE_], colspan:3})
		.addDateField({name:"agreementFrom", bind:"{d.agreementFrom}", dataIndex:"agreementFrom", allowBlank:false, width:120, noLabel: true})
		.addDateField({name:"agreementTo", bind:"{d.agreementTo}", dataIndex:"agreementTo", allowBlank:false, width:120, noLabel: true, colspan:2, listeners:{keydown: { scope: this, fn: function(field, e) { this._stopTabKey_(field, e) }}}, enableKeyEvents:true})
		.addDisplayFieldText({ name:"tenderNameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"tenderStatusLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"tenderHolderCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"tenderVersionLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:60px"})
		.addDisplayFieldText({ name:"contactLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"emailLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"phoneLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"tenderTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"biddingPeriodFromLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"biddingPeriodToLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:5px"})
		.addDisplayFieldText({ name:"commentsLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:left"})
		.addDisplayFieldText({ name:"typeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"agreementFromLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"agreementToLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:5px"})
		.add({name:"biddingFromTo", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("biddingPeriodFrom"),this._getConfig_("biddingPeriodToLabel")]})
		.add({name:"agreementFromTo", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("agreementFrom"),this._getConfig_("agreementToLabel")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"table1",  style:"margin-left:10px; margin-right:10px", layout: {type:"table", columns:4}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["table1"])
		.addChildrenTo("table1", ["tenderNameLabel", "tenderName", "tenderVersionLabel", "tenderVersion", "tenderHolderCodeLabel", "tenderHolderCode", "contactLabel", "contact", "typeLabel", "type", "biddingPeriodFromLabel", "biddingFromTo", "biddingPeriodTo", "agreementFromLabel", "agreementFromTo", "agreementTo", "contactId", "holderId"]);
	},
	/* ==================== Business functions ==================== */
	
	_checkFormValidity_: function() {
		
						var ctrl = this._controller_;
						var frame = ctrl.getFrame();
						var finishBtn = frame._get_("btnTIFinishWdw");
						var continueBtn = frame._get_("btnTIFirstContinue");
						var view = frame._get_("tiNew");
						var form = view.getForm();
						
						if (form.isValid() === true) {
							finishBtn._enable_();		
							continueBtn._enable_();	
						}
						else {
							finishBtn._disable_();		
							continueBtn._disable_();
						}
		
	},
	
	_enableDisableContact_: function() {
		
						var el = this._get_("tenderHolderCode");
						var ctrl = this._controller_;
						var frame = ctrl.getFrame();
						var finishBtn = frame._get_("btnTIFinishWdw");
						var continueBtn = frame._get_("btnTIFirstContinue");
		
						var contactField = this._get_("contact");
		
						if (!Ext.isEmpty(el.getRawValue())) {
							contactField._enable_();
							contactField.setValue("");		
							finishBtn._disable_();		
							continueBtn._disable_();		
						}
						else {
							contactField._disable_();
							contactField.setValue("");		
							finishBtn._enable_();		
							continueBtn._enable_();		
						}
	},
	
	_stopTabKey_: function(field,e) {
		
						
						if (e.getKey() === 9) {
							
							var form = field.up("form");
							var formFields = form.getForm().getFields().items;
							var formDirty = false;
							var firstDirtyField = null;
							var dc = this._controller_;
							var finishBtn = dc.getFrame()._getElement_("btnTIFinishWdw");
		
							for (var i =0; i<formFields.length; i++) {
								if (formFields[i].wasValid == false) {
									formDirty = true;
									var fieldId = formFields[i].id;
									var field = Ext.getCmp(fieldId);
									e.stopEvent();
									if (field.dataIndex != "agreementTo") {
										field.focus();
									}
									else { 
										// Dirty hack to solve never-ending story
										field.blur();
										field.focus();
									}					
									break;
								}
							}
							if (formDirty === false) {						
								finishBtn.focus();
								e.stopEvent();
							}
						}
	}
});

/* ================= EDIT FORM: periodForm ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderInvitation_Dc$periodForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderInvitation_Dc$periodForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDateField({name:"biddingPeriodFrom", bind:"{d.biddingPeriodFrom}", dataIndex:"biddingPeriodFrom", _enableFn_: this._enableIfStatus_, labelAlign:"top"})
		.addDateField({name:"biddingPeriodTo", bind:"{d.biddingPeriodTo}", dataIndex:"biddingPeriodTo", _enableFn_: this._enableIfStatus_, labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:170, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["biddingPeriodFrom", "biddingPeriodTo"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableIfStatus_: function() {
		
						var dc 		= this._controller_;
						var record 	= dc.getRecord();
						var status 	= record.get("status");				
		
						return status=="Draft";
	}
});

/* ================= EDIT FORM: Header ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderInvitation_Dc$Header", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderInvitation_Dc$Header",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"tenderHolderName", bind:"{d.holderName}", dataIndex:"holderName", _enableFn_: this._enableIfStatus_, width:200, noLabel: true, xtype:"fmbas_CustomerLov_Lov", maxLength:100,
			retFieldMapping: [{lovField:"id", dsField: "holderId"} ,{lovField:"code", dsField: "holderCode"} ]})
		.addLov({name:"contact", bind:"{d.contactPerson}", dataIndex:"contactPerson", _enableFn_: this._enableIfStatus_, allowBlank:false, noLabel: true, xtype:"fmbas_ContactsLov_Lov", maxLength:200, colspan:1,
			retFieldMapping: [{lovField:"id", dsField: "contactId"} ,{lovField:"email", dsField: "email"} ,{lovField:"businessPhone", dsField: "phone"} ],
			filterFieldMapping: [{lovField:"objectId", dsField: "holderId"}, {lovField:"objectType", value: "Customer"} ]})
		.addTextField({ name:"email", bind:"{d.email}", dataIndex:"email", _enableFn_: this._enableIfStatus_, width:200, noLabel: true, maxLength:100, vtype:"email"})
		.addTextField({ name:"phone", bind:"{d.phone}", dataIndex:"phone", _enableFn_: this._enableIfStatus_, width:100, noLabel: true, maxLength:50})
		.addCombo({ xtype:"combo", name:"tenderType", bind:"{d.type}", dataIndex:"type", _enableFn_: this._enableIfStatus_, width:130, noLabel: true, store:[ __CMM_TYPE__.TenderType._PRODUCT_, __CMM_TYPE__.TenderType._FUELING_SERVICE_]})
		.addDateField({name:"biddingPeriodFrom", bind:"{d.biddingPeriodFrom}", dataIndex:"biddingPeriodFrom"})
		.addDateField({name:"biddingPeriodTo", bind:"{d.biddingPeriodTo}", dataIndex:"biddingPeriodTo"})
		.addPopoverTextField({name:"contractPeriod", hPos:"r", vPos:"b", fieldsList:["biddingPeriodFrom","biddingPeriodTo"], formatList:[], editorForm:"cmm_TenderInvitation_Dc$periodForm", fieldsValueSeparator:" - "})
		.addNumberField({name:"noOfRounds", bind:"{d.noOfRounds}", dataIndex:"noOfRounds", _enableFn_: this._enableIfStatus_, width:40, noLabel: true, maxLength:2})
		.addTextArea({ name:"comment", bind:"{d.comments}", dataIndex:"comments", _enableFn_: this._enableIfStatus_, noLabel: true})
		.addDisplayFieldText({ name:"tenderHolderCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"contactLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"emailLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"phoneLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"tenderTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"biddingPeriodFromLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"biddingPeriodToLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"noOfroundsToLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"commentLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"formTitle", bind:"{d.formTitle}", dataIndex:"formTitle", noEdit:true , maxLength:16})
		.addDisplayFieldText({ name:"totalVolume", bind:"{d.totalVolumeHeader}", dataIndex:"totalVolumeHeader", maxLength:32})
		.addDisplayFieldDate({ name:"publishedOn", bind:"{d.publishedOn}", dataIndex:"publishedOn" })
		.addDisplayFieldText({ name:"status", bind:"{d.status}", dataIndex:"status", maxLength:32})
		.addDisplayFieldText({ name:"biddingStatus", bind:"{d.biddingStatus}", dataIndex:"biddingStatus", maxLength:32})
		.addDisplayFieldNumber({ name:"version", bind:"{d.tenderVersion}", dataIndex:"tenderVersion", maxLength:6 })
		.addDisplayFieldText({ name:"tenderCode", bind:"{d.code}", dataIndex:"code", noLabel: true, maxLength:20})
		.addTextField({ name:"tenderName", bind:"{d.name}", dataIndex:"name", _enableFn_: this._enableIfStatus_, noLabel: true, maxLength:100, cls:"sone-flat-field", style:"padding-top: 2px"})
		.add({name:"row", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("formTitle"),this._getConfig_("tenderName")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"mainTable", layout: {type:"table"}})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left: 40px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3",  style:"margin-left: 40px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table4",  style:"margin-left: 40px", layout: {type:"table", columns:2}})
		.addPanel({ name:"c1", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c2", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c3", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c4", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c5", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"titleAndKpi",  style:"margin-bottom:10px", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"p5",  cls:"sone-kpi-panel", layout: {type:"table"}})
		.addPanel({ name:"title", width:300,  cls:"sone-title-panel", layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["titleAndKpi", "mainTable"])
		.addChildrenTo("mainTable", ["table1", "table2", "table3", "table4"])
		.addChildrenTo("table1", ["tenderHolderCodeLabel", "tenderHolderName", "tenderTypeLabel", "tenderType"])
		.addChildrenTo("table2", ["biddingPeriodFromLabel", "contractPeriod", "noOfroundsToLabel", "noOfRounds"])
		.addChildrenTo("table3", ["emailLabel", "email", "phoneLabel", "phone"])
		.addChildrenTo("table4", ["contactLabel", "contact"])
		.addChildrenTo("c1", ["totalVolume"])
		.addChildrenTo("c2", ["publishedOn"])
		.addChildrenTo("c3", ["status"])
		.addChildrenTo("c4", ["biddingStatus"])
		.addChildrenTo("c5", ["version"])
		.addChildrenTo("titleAndKpi", ["title", "p5"])
		.addChildrenTo("p5", ["c1", "c2", "c3", "c4", "c5"])
		.addChildrenTo("title", ["row"]);
	},
	/* ==================== Business functions ==================== */
	
	_afterDefineElements_: function() {
		
						this._getBuilder_().change("titleAndKpi",{
							layout: {
						        type: "hbox",
						        align: "stretch"
						    },
						    defaults: {
						        flex: 1,
								height: 60
						    }
						});
		
						this._controller_.on("afterDoServiceSuccess", function() {					
							this._applyStates_(this._controller_.getRecord());
						},this);
	},
	
	_afterApplyStates_: function() {
		
						var field = this._controller_.getRecord();
						if(field){
							var tenderCode = field.get("code");
							var formTitle = this._get_("formTitle");
							if (formTitle.labelEl) {
								formTitle.labelEl.update("<i class='fa fa-envelope'></i> "+ tenderCode);
							}
						}
		
						var tName = this._get_("tenderName");
						if (tName.inputEl) {
							var inputElId = tName.inputEl.dom.id;
							var inputWidth = 500;
			
							document.getElementById(inputElId).style.width = inputWidth+"px";
						}
							
	},
	
	_enableIfStatus_: function() {
		
						var dc 		= this._controller_;
						var record 	= dc.getRecord();
						var status 	= record.get("status");				
		
						return status=="Draft";
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderInvitation_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderInvitation_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"tenderHolderName", bind:"{d.holderName}", dataIndex:"holderName", noEdit:true , noLabel: true, xtype:"fmbas_CustomerLov_Lov", maxLength:100,
			retFieldMapping: [{lovField:"id", dsField: "holderId"} ,{lovField:"code", dsField: "holderCode"} ]})
		.addTextField({ name:"contact", bind:"{d.contactPerson}", dataIndex:"contactPerson", noEdit:true , noLabel: true, maxLength:200})
		.addTextField({ name:"email", bind:"{d.email}", dataIndex:"email", noEdit:true , noLabel: true, maxLength:100})
		.addTextField({ name:"phone", bind:"{d.phone}", dataIndex:"phone", noEdit:true , noLabel: true, maxLength:50})
		.addCombo({ xtype:"combo", name:"tenderType", bind:"{d.type}", dataIndex:"type", noEdit:true , noLabel: true, store:[ __CMM_TYPE__.TenderType._PRODUCT_, __CMM_TYPE__.TenderType._FUELING_SERVICE_]})
		.addDateField({name:"biddingPeriodFrom", bind:"{d.biddingPeriodFrom}", dataIndex:"biddingPeriodFrom", noEdit:true , noLabel: true})
		.addDateField({name:"biddingPeriodTo", bind:"{d.biddingPeriodTo}", dataIndex:"biddingPeriodTo", noEdit:true , noLabel: true})
		.addTextArea({ name:"comment", bind:"{d.comments}", dataIndex:"comments", noEdit:true , noLabel: true})
		.addDisplayFieldText({ name:"tenderHolderCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"contactLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"emailLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"phoneLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"tenderTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"biddingPeriodFromLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"biddingPeriodToLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"commentLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  style:"padding-left:18px"})
		.addPanel({ name:"mainTable", layout: {type:"table"}})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left: 40px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3",  style:"margin-left: 40px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table4",  style:"margin-left: 40px", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["mainTable"])
		.addChildrenTo("mainTable", ["table1", "table2", "table3", "table4"])
		.addChildrenTo("table1", ["tenderHolderCodeLabel", "tenderHolderName", "tenderTypeLabel", "tenderType"])
		.addChildrenTo("table2", ["biddingPeriodFromLabel", "biddingPeriodFrom", "biddingPeriodToLabel", "biddingPeriodTo"])
		.addChildrenTo("table3", ["contactLabel", "contact", "emailLabel", "email"])
		.addChildrenTo("table4", ["phoneLabel", "phone"]);
	},
	/* ==================== Business functions ==================== */
	
	_afterDefineElements_: function() {
		
		
						this._controller_.on("afterDoServiceSuccess", function() {					
							this._applyStates_(this._controller_.getRecord());
						},this);
	}
});

/* ================= EDIT FORM: Remarks ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderInvitation_Dc$Remarks", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderInvitation_Dc$Remarks",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.remarks}", paramIndex:"remarks", width:400, labelAlign:"top", fieldStyle:"margin-top: 5px"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:400, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
	}
});
