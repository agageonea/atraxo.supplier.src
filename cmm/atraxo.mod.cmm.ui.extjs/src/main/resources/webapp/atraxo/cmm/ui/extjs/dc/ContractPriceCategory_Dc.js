/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceCategory_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.ContractPriceCategory_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.ContractPriceCategory_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceCategory_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.cmm_ContractPriceCategory_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:100})
			.addLov({name:"priceCtgryName", dataIndex:"priceCtgryName", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.PriceCategoriesLov_Lov", selectOnFocus:true, maxLength:32}})
			.addLov({name:"mainCategoryCode", dataIndex:"mainCategoryCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MainCategoriesLov_Lov", selectOnFocus:true, maxLength:25}})
			.addLov({name:"financialSourceName", dataIndex:"financialSourceName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CodeLov_Lov", selectOnFocus:true, maxLength:100}})
			.addLov({name:"avgMethodIndicatorName", dataIndex:"avgMethodIndicatorName", maxLength:50,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.AvgMethLov_Lov", selectOnFocus:true, maxLength:50}})
			.addCombo({ xtype:"combo", name:"calculateIndicator", dataIndex:"calculateIndicator", store:[ __CMM_TYPE__.CalculateIndicator._INCLUDED_, __CMM_TYPE__.CalculateIndicator._CALCULATE_ONLY_]})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceCategory_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_ContractPriceCategory_Dc$List",
	_noPaginator_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:160})
		.addTextColumn({ name:"priceCategory", dataIndex:"priceCtgryName", width:160})
		.addTextColumn({ name:"mainCategory", dataIndex:"mainCategoryCode", width:160})
		.addTextColumn({ name:"iataPriceCategoryUse", dataIndex:"iataPriceCatUse", width:160})
		.addTextColumn({ name:"iataPriceCategoryCode", dataIndex:"iataPriceCatCode", width:80})
		.addBooleanColumn({ name:"restrictionsParameter", dataIndex:"restriction", width:100})
		.addNumberColumn({ name:"price", dataIndex:"price", width:80, decimals:6,  renderer:function(val, meta, record, rowIndex) { return this._markUtilization_(val, meta, record, rowIndex); }})
		.addTextColumn({ name:"currency", dataIndex:"currencyCode", width:80})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:60})
		.addDateColumn({ name:"validFrom", dataIndex:"validFrom", width:120, _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"validTo", width:120, _mask_: Masks.DATE})
		.addBooleanColumn({ name:"continous", dataIndex:"continous", hidden:true, width:100})
		.addTextColumn({ name:"quantityType", dataIndex:"quantityType", hidden:true, width:100})
		.addTextColumn({ name:"averagingMethodName", dataIndex:"avgMethodIndicatorName", hidden:true, width:150})
		.addTextColumn({ name:"exchangeRateOffset", dataIndex:"exchangeRateOffset", hidden:true, width:120})
		.addTextColumn({ name:"vat", dataIndex:"vat", hidden:true, width:100})
		.addBooleanColumn({ name:"defaulPriceCategory", dataIndex:"defaultPriceCtgy", hidden:true, width:120})
		.addTextColumn({ name:"avgMethodIndicatorName", dataIndex:"avgMethodIndicatorName", hidden:true, width:100})
		.addTextColumn({ name:"financialSourceName", dataIndex:"financialSourceName", hidden:true, width:100})
		.addTextColumn({ name:"calculateIndicator", dataIndex:"calculateIndicator", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._currencyParamName_ = "settlementCurrencyCode";
		this._unitParamName_ = "settlementUnitCode";
		this._dateParamName_ = "priceDate";
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_markUtilization_: function(value,meta,record,rowIndex) {
		
						var used = record.get("used");
		
						if (!Ext.isEmpty(value)) {
							if(used == true) {
					        	meta.style = "font-weight:bold !important";
						    } 
							return Ext.util.Format.number(value,Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_crncy));
						}				
	}
});

/* ================= GRID: BidList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceCategory_Dc$BidList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_ContractPriceCategory_Dc$BidList",
	_noPaginator_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:160})
		.addTextColumn({ name:"priceCategory", dataIndex:"priceCtgryName", width:160})
		.addTextColumn({ name:"mainCategory", dataIndex:"mainCategoryCode", hidden:true, width:160})
		.addTextColumn({ name:"iataPriceCategoryUse", dataIndex:"iataPriceCatUse", width:160})
		.addTextColumn({ name:"iataPriceCategoryCode", dataIndex:"iataPriceCatCode", hidden:true, width:80})
		.addBooleanColumn({ name:"restrictionsParameter", dataIndex:"restriction", width:100})
		.addNumberColumn({ name:"price", dataIndex:"price", width:80, decimals:6,  renderer:function(val, meta, record, rowIndex) { return this._markUtilization_(val, meta, record, rowIndex); }})
		.addTextColumn({ name:"currency", dataIndex:"currencyCode", width:80})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:60})
		.addDateColumn({ name:"validFrom", dataIndex:"validFrom", hidden:true, width:120, _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"validTo", hidden:true, width:120, _mask_: Masks.DATE})
		.addBooleanColumn({ name:"continous", dataIndex:"continous", hidden:true, width:100})
		.addTextColumn({ name:"quantityType", dataIndex:"quantityType", hidden:true, width:100})
		.addTextColumn({ name:"averagingMethodName", dataIndex:"avgMethodIndicatorName", hidden:true, width:150})
		.addTextColumn({ name:"exchangeRateOffset", dataIndex:"exchangeRateOffset", hidden:true, width:120})
		.addTextColumn({ name:"vat", dataIndex:"vat", hidden:true, width:100})
		.addBooleanColumn({ name:"defaulPriceCategory", dataIndex:"defaultPriceCtgy", hidden:true, width:120})
		.addTextColumn({ name:"avgMethodIndicatorName", dataIndex:"avgMethodIndicatorName", hidden:true, width:100})
		.addTextColumn({ name:"financialSourceName", dataIndex:"financialSourceName", hidden:true, width:100})
		.addTextColumn({ name:"calculateIndicator", dataIndex:"calculateIndicator", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._currencyParamName_ = "settlementCurrencyCode";
		this._unitParamName_ = "settlementUnitCode";
		this._dateParamName_ = "priceDate";
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_markUtilization_: function(value,meta,record,rowIndex) {
		
						var used = record.get("used");
		
						if (!Ext.isEmpty(value)) {
							if(used == true) {
					        	meta.style = "font-weight:bold !important";
						    } 
							return Ext.util.Format.number(value,Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_crncy));
						}				
	}
});

/* ================= GRID: DetailsList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceCategory_Dc$DetailsList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_ContractPriceCategory_Dc$DetailsList",
	_noPaginator_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:160,  flex:1})
		.addTextColumn({ name:"priceCategory", dataIndex:"priceCtgryName", width:50,  flex:1})
		.addTextColumn({ name:"iataPriceCategoryUse", dataIndex:"iataPriceCatUse", width:70,  flex:1})
		.addTextColumn({ name:"iataPriceCategoryCode", dataIndex:"iataPriceCatCode", width:50,  flex:1})
		.addBooleanColumn({ name:"restrictionsParameter", dataIndex:"restriction",  flex:1})
		.addNumberColumn({ name:"price", dataIndex:"price", width:80, decimals:6,  flex:1})
		.addTextColumn({ name:"currency", dataIndex:"currencyCode", width:80,  flex:1})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:60,  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: NewGrid ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceCategory_Dc$NewGrid", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractPriceCategory_Dc$NewGrid",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"showAssignment", scope: this, handler: this._doAssign, text: "...", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer === 'Percent' && dc.record.data.readOnly === false; } })
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, width:280, maxLength:100, labelWidth:120})
		.addLov({name:"priceCategory", bind:"{d.priceCtgryName}", dataIndex:"priceCtgryName", _enableFn_: function(dc, rec) { return dc.data.record.priceCtgryName != 'Fixed'; ; } , allowBlank:false, width:280, xtype:"fmbas_PriceCategoriesLov_Lov", maxLength:32, labelWidth:120,
			retFieldMapping: [{lovField:"id", dsField: "priceCategory"} ,{lovField:"name", dsField: "name"} ,{lovField:"pricePer", dsField: "priceCtgryPricePer"} ],listeners:{
			change:{scope:this, fn:this._setReadOnlyLov_}
		}})
		.addBooleanField({ name:"continous", bind:"{d.continous}", dataIndex:"continous", width:300, labelWidth:170})
		.addCombo({ xtype:"combo", name:"quantityType", bind:"{d.quantityType}", dataIndex:"quantityType", width:280, store:[ __CMM_TYPE__.QuantityType._NET_VOLUME_, __CMM_TYPE__.QuantityType._GROSS_VOLUME_], labelWidth:120})
		.addCombo({ xtype:"combo", name:"vat", bind:"{d.vat}", dataIndex:"vat", allowBlank:false, width:280, store:[ __CMM_TYPE__.VatApplicability._ALL_EVENTS_, __CMM_TYPE__.VatApplicability._DOMESTIC_EVENTS_, __CMM_TYPE__.VatApplicability._INTERNATIONAL_EVENTS_, __CMM_TYPE__.VatApplicability._NOT_APPLICABLE_], labelWidth:120})
		.addBooleanField({ name:"includeAveragePrice", bind:"{d.includeInAverage}", dataIndex:"includeInAverage", width:300, labelWidth:170})
		.addDisplayFieldText({ name:"title", bind:"{d.convertTitle}", dataIndex:"convertTitle", maxLength:100, labelWidth:240})
		.addBooleanField({ name:"default", bind:"{d.defaultPriceCtgy}", dataIndex:"defaultPriceCtgy", labelWidth:170})
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", _enableFn_: function(dc, rec) { return dc.getParams().get('settlementCurrencyCode') != null; ; } , allowBlank:false, width:280, xtype:"fmbas_ExchangeRateToFinancialSourceLov_Lov", maxLength:25, labelWidth:120,
			retFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"} ],
			filterFieldMapping: [{lovField:"currencyId", dsField: "currencyId"} ],listeners:{
			fpchange:{scope:this, fn:this._enableAvgMethods_},
			expand:{scope:this, fn:this._filterDistinct_}
		}})
		.addLov({name:"avgMethodName", bind:"{d.avgMethodIndicatorName}", dataIndex:"avgMethodIndicatorName", _enableFn_: this._enableIfFinancialSource_, allowBlank:false, width:280, xtype:"fmbas_ExchangeRateToAvgMthdFilteredLov_Lov", maxLength:50, labelWidth:120,
			retFieldMapping: [{lovField:"avgMthdId", dsField: "avgMthdId"} ,{lovField:"currencyId", dsParam: "settlementCurrencyId"} ],
			filterFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"}, {lovField:"currencyId", dsParam: "settlementCurrencyId"} ]})
		.addCombo({ xtype:"combo", name:"period", bind:"{d.exchangeRateOffset}", dataIndex:"exchangeRateOffset", width:220, store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_], labelWidth:70})
		.addTextField({ name:"comments", bind:"{d.comments}", dataIndex:"comments", width:560, maxLength:255, labelWidth:120})
		.addBooleanField({ name:"newRestriction", bind:"{d.restriction}", dataIndex:"restriction", width:300, labelWidth:120})
		.addTextArea({ name:"percentageOf", bind:"{d.percentageOf}", dataIndex:"percentageOf", noEdit:true , _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer === 'Percent'; } , width:245})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("priceCategory"),this._getConfig_("continous")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("name"),this._getConfig_("includeAveragePrice")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("vat"),this._getConfig_("default")]})
		.add({name:"row4", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("quantityType")]})
		.add({name:"rowAssign", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("percentageOf"),this._getConfig_("showAssignment")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("title")]})
		.add({name:"row6", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("financialSourceCode"),this._getConfig_("avgMethodName"),this._getConfig_("period")]})
		.add({name:"row7", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("comments")]})
		.add({name:"row8", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("newRestriction")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:600, layout:"anchor"})
		.addPanel({ name:"col2", width:600, layout:"anchor"})
		.addPanel({ name:"col3", width:600, layout:"anchor"})
		.addPanel({ name:"col4", width:600, layout:"anchor"})
		.addPanel({ name:"col5", width:600, layout:"anchor"})
		.addPanel({ name:"colAssign", width:600, layout:"anchor"})
		.addPanel({ name:"col6", width:800, layout:"anchor"})
		.addPanel({ name:"col7", width:600, layout:"anchor"})
		.addPanel({ name:"col8", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "colAssign", "col5", "col6", "col7", "col8"])
		.addChildrenTo("col1", ["row1"])
		.addChildrenTo("col2", ["row2"])
		.addChildrenTo("col3", ["row3"])
		.addChildrenTo("col4", ["row4"])
		.addChildrenTo("col5", ["row5"])
		.addChildrenTo("colAssign", ["rowAssign"])
		.addChildrenTo("col6", ["row6"])
		.addChildrenTo("col7", ["row7"])
		.addChildrenTo("col8", ["row8"]);
	},
	/* ==================== Business functions ==================== */
	
	_doAssign: function() {
		
						this._controller_.fireEvent("onAssignPriceCategory", this); 
	},
	
	_setReadOnlyLov_: function() {
		
						var dc = this._controller_;
						var record = dc.getRecord();
						this._applyStates_(record);
	},
	
	_filterDistinct_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("financialSourceName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });
						}, this);
	},
	
	_enableAvgMethods_: function(el) {
		
		
						var avgMethodName = this._get_("avgMethodName");
						var elVal = el.getValue(); 
						if (!Ext.isEmpty(elVal)) {
							avgMethodName.setReadOnly(false);
						}
						else {
							avgMethodName.setReadOnly(true);					
						}
						avgMethodName.setValue("");
	},
	
	_enableIfFinancialSource_: function() {
		
						
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = false;
		
						if (record) {
							var financialSourceName = record.get("financialSourceName");
							if (!Ext.isEmpty(financialSourceName)) {
								result = true;
							}
						}
						return result;
	}
});

/* ================= EDIT FORM: financialSourceAvgMethodPeriod ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceCategory_Dc$financialSourceAvgMethodPeriod", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractPriceCategory_Dc$financialSourceAvgMethodPeriod",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", _enableFn_: this._enableIfStatus_, _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !== 'Percent'; } , allowBlank:false, xtype:"fmbas_ExchangeRateToFinancialSourceAndAvgFilteredLov_Lov", maxLength:25, labelAlign:"top",
			retFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"} ],
			filterFieldMapping: [{lovField:"currencyId", dsField: "currencyId"} ],listeners:{
			fpchange:{scope:this, fn:this._enableAvgMethods_},
			expand:{scope:this, fn:this._filterDistinct_}
		}})
		.addLov({name:"avgMethodIndicatorName", bind:"{d.avgMethodIndicatorName}", dataIndex:"avgMethodIndicatorName", _enableFn_: this._enableIfFinancialSource_, _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !== 'Percent'; } , allowBlank:false, xtype:"fmbas_ExchangeRateToAvgMthdFilteredLov_Lov", maxLength:50, labelAlign:"top",
			retFieldMapping: [{lovField:"avgMthdId", dsField: "avgMthdId"} ,{lovField:"fromCurrencyCode", dsParam: "settlementCurrencyCode"} ],
			filterFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"}, {lovField:"currencyId", dsParam: "settlementCurrencyId"} ],listeners:{
			expand:{scope:this, fn:this._filterDistinctAvg_}
		}})
		.addCombo({ xtype:"combo", name:"exchangeRateOffset", bind:"{d.exchangeRateOffset}", dataIndex:"exchangeRateOffset", _enableFn_: this._enableIfStatus_, store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_], labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:170, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["financialSourceCode", "avgMethodIndicatorName", "exchangeRateOffset"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableAvgMethods_: function(el) {
		
						if (el._fpRawValue_ != el._onFocusVal_) {
							var avgMethodName = this._get_("avgMethodIndicatorName");
							var elVal = el.getValue(); 
							if (!Ext.isEmpty(elVal)) {
								avgMethodName.setReadOnly(false);
							}
							else {
								avgMethodName.setReadOnly(true);
							}
							avgMethodName.setValue("");
						}
	},
	
	_enableIfStatus_: function() {
		
						var ctrl = this._controller_;
						var frame = ctrl.getFrame();
						var dc = frame._getDc_("contractEdit");
						var result = true;
						if (dc) {
							var record = dc.getRecord();
			
							if (record) {
			
								var status = record.get("status");
			
								if (status === "Effective" || status === "Expired" || status === "Active") {
									result = false;
								}
							}
						} else {
							var purchaseDc = frame._getDc_("contract");
							if (purchaseDc) {
								var record = purchaseDc.getRecord();
								if (record) {
				
									var status = record.get("status");
				
									if (status == "Effective" || status == "Expired" || status == "Active") {
										result = false;
									}
								}
							}
						}
						return result;
	},
	
	_filterDistinct_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("financialSourceName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });
						}, this);
	},
	
	_enableIfFinancialSource_: function() {
		
						
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = false;
		
						if (record) {
							var financialSourceName = record.get("financialSourceName");
							if (!Ext.isEmpty(financialSourceName)) {
								result = true;
							}
						}
						return result && this._enableIfStatus_();
	},
	
	_filterDistinctAvg_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("avgMthdName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	}
});

/* ================= EDIT FORM: EditGrid ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceCategory_Dc$EditGrid", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractPriceCategory_Dc$EditGrid",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btnToggle", scope: this, handler: this._toggle_, text: "...", style:"margin-left:3px"})
		.addButton({name:"showAssignment", scope: this, handler: this._doAssign, text: "...", _enableFn_: function(dc, rec) { return this._superCheck_(); } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer === 'Percent'; } })
		.addButton({name:"assignPriceComponents", scope: this, handler: this._doAssignCompositePrice, text: "...", _enableFn_: function(dc, rec) { return this._superCheck_(); } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer === 'Composite'; } })
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, noLabel: true, maxLength:100})
		.addLov({name:"priceCategory", bind:"{d.priceCtgryName}", dataIndex:"priceCtgryName", noEdit:true , allowBlank:false, noLabel: true, xtype:"fmbas_PriceCategoriesLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "priceCategory"} ,{lovField:"pricePer", dsField: "priceCtgryPricePer"} ]})
		.addToggleField({ name:"continous", bind:"{d.continous}", dataIndex:"continous", noEdit:true , noLabel: true})
		.addCombo({ xtype:"combo", name:"quantityType", bind:"{d.quantityType}", dataIndex:"quantityType", _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !== 'Percent'; } , noLabel: true, store:[ __CMM_TYPE__.QuantityType._NET_VOLUME_, __CMM_TYPE__.QuantityType._GROSS_VOLUME_], colspan:3})
		.addCombo({ xtype:"combo", name:"vat", bind:"{d.vat}", dataIndex:"vat", _enableFn_: this._enableIfStatus_, allowBlank:false, noLabel: true, store:[ __CMM_TYPE__.VatApplicability._ALL_EVENTS_, __CMM_TYPE__.VatApplicability._DOMESTIC_EVENTS_, __CMM_TYPE__.VatApplicability._INTERNATIONAL_EVENTS_, __CMM_TYPE__.VatApplicability._NOT_APPLICABLE_]})
		.addBooleanField({ name:"includeAveragePrice", bind:"{d.includeInAverage}", dataIndex:"includeInAverage", noLabel: true})
		.addDisplayFieldText({ name:"title", bind:"{d.convertTitle}", dataIndex:"convertTitle", _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !== 'Percent'; } , noLabel: true, maxLength:100})
		.addToggleField({ name:"default", bind:"{d.defaultPriceCtgy}", dataIndex:"defaultPriceCtgy", noLabel: true})
		.addToggleField({ name:"restriction", bind:"{d.restriction}", dataIndex:"restriction", _enableFn_: this._setReadOnlyRestriction_, noLabel: true})
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !== 'Percent'; } , allowBlank:false, xtype:"fmbas_ExchangeRateToFinancialSourceAndAvgFilteredLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"} ],
			filterFieldMapping: [{lovField:"currencyId", dsField: "currencyId"} ]})
		.addLov({name:"avgMethodIndicatorName", bind:"{d.avgMethodIndicatorName}", dataIndex:"avgMethodIndicatorName", _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !== 'Percent'; } , allowBlank:false, xtype:"fmbas_ExchangeRateToAvgMthdFilteredLov_Lov", maxLength:50,
			retFieldMapping: [{lovField:"avgMthdId", dsField: "avgMthdId"} ,{lovField:"fromCurrencyCode", dsParam: "settlementCurrencyCode"} ],
			filterFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"}, {lovField:"currencyId", dsParam: "settlementCurrencyId"} ]})
		.addCombo({ xtype:"combo", name:"exchangeRateOffset", bind:"{d.exchangeRateOffset}", dataIndex:"exchangeRateOffset", store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_]})
		.addPopoverTextField({name:"financialSourceAvgMethodPeriod", width:255, hPos:"r", vPos:"b", fieldsList:["financialSourceCode","avgMethodIndicatorName","exchangeRateOffset"], formatList:[], editorForm:"cmm_ContractPriceCategory_Dc$financialSourceAvgMethodPeriod", fieldsValueSeparator:" / "})
		.addTextField({ name:"comments", bind:"{d.comments}", dataIndex:"comments", _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !== 'Percent'; } , noLabel: true, maxLength:255, colspan:5})
		.addTextArea({ name:"percentageOf", bind:"{d.percentageOf}", dataIndex:"percentageOf", noEdit:true , _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer === 'Composite' || dc.record.data.priceCtgryPricePer === 'Percent'; } , noLabel: true})
		.addDisplayFieldText({ name:"nameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"priceCategoryLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"vatLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"quantityTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !== 'Percent'; } , maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"continousLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"includeAveragePriceLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"titleLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !== 'Percent'; } , maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"defaultLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"financialSourceCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !== 'Percent'; } , maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"avgMethodNameLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !== 'Percent'; } , maxLength:100, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"periodLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !== 'Percent'; } , maxLength:100, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"commentsLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !== 'Percent'; } , maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"restrictionLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"showAssignmentLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"percentageOfLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer === 'Composite' || dc.record.data.priceCtgryPricePer === 'Percent'; } , maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"assignPriceComponentsLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"popoverLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.add({name:"rowAssign", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("percentageOf"),this._getConfig_("showAssignment"),this._getConfig_("assignPriceComponents")]})
		.add({name:"rowToggle", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("financialSourceAvgMethodPeriod"),this._getConfig_("btnToggle")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"tableContainer1", layout: {type:"table"}})
		.addPanel({ name:"table1", layout: {type:"table", columns:4}})
		.addPanel({ name:"tableContainer2", layout: {type:"table"}})
		.addPanel({ name:"tableContainer3", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["tableContainer1", "tableContainer2", "tableContainer3"])
		.addChildrenTo("tableContainer1", ["table1"])
		.addChildrenTo("table1", ["nameLabel", "name", "continousLabel", "continous", "priceCategoryLabel", "priceCategory", "defaultLabel", "default", "vatLabel", "vat", "restrictionLabel", "restriction", "quantityTypeLabel", "quantityType", "percentageOfLabel", "rowAssign"])
		.addChildrenTo("tableContainer2", ["titleLabel"])
		.addChildrenTo("tableContainer3", ["popoverLabel", "rowToggle", "commentsLabel", "comments"]);
	},
	/* ==================== Business functions ==================== */
	
	_superCheck_: function() {
		
						var dc = this._controller_;
						var frame = dc.getFrame();
						var result = true;
						var r,as,s,b,contract;
						if (frame.xtype === "ContractCustomer_Ui") {
							contract = frame._getDc_("contractEdit");
							r = contract.getRecord();
							if (r) {
								as = r.get("approvalStatus");
								s = r.get("status");
								b = r.get("isBlueprint");
								if ((as === __CMM_TYPE__.BidApprovalStatus._NEW_ && s === __CMM_TYPE__.ContractStatus._DRAFT_) ||
									(as === __CMM_TYPE__.BidApprovalStatus._NEW_ && s === __CMM_TYPE__.ContractStatus._ACTIVE_ && b === true) ||
									(as === __CMM_TYPE__.BidApprovalStatus._REJECTED_ && s === __CMM_TYPE__.ContractStatus._DRAFT_) ||
									(as === __CMM_TYPE__.BidApprovalStatus._REJECTED_ && s === __CMM_TYPE__.ContractStatus._ACTIVE_ && b === true) ||
									(as === __CMM_TYPE__.BidApprovalStatus._REJECTED_ && s === __CMM_TYPE__.ContractStatus._EFFECTIVE_ && b === true) ||
									(as === __CMM_TYPE__.BidApprovalStatus._REJECTED_ && s === __CMM_TYPE__.ContractStatus._EXPIRED_ && b === true) ||
									(as === __CMM_TYPE__.BidApprovalStatus._APPROVED_ && s === __CMM_TYPE__.ContractStatus._ACTIVE_ && b === true) ||
									(as === __CMM_TYPE__.BidApprovalStatus._APPROVED_ && s === __CMM_TYPE__.ContractStatus._EFFECTIVE_ && b === true) ||
									(as === __CMM_TYPE__.BidApprovalStatus._APPROVED_ && s === __CMM_TYPE__.ContractStatus._EXPIRED_ && b === true)) {
		
									result = true;
								}
								if ((as === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_ && s === __CMM_TYPE__.ContractStatus._DRAFT_) ||
									(as === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_ && s === __CMM_TYPE__.ContractStatus._ACTIVE_) ||
									(as === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_ && s === __CMM_TYPE__.ContractStatus._EFFECTIVE_) ||
									(as === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_ && s === __CMM_TYPE__.ContractStatus._EXPIRED_)) {
		
									result = false;
								}
							}
						}
						else if (frame.xtype === "Contract_Ui") {
							contract = frame._getDc_("contract");
							r = contract.getRecord();
							if (r) {
								as = r.get("approvalStatus");
								if (as === __CMM_TYPE__.BidApprovalStatus._NEW_ ||
									as === __CMM_TYPE__.BidApprovalStatus._REJECTED_ ||
									as === __CMM_TYPE__.BidApprovalStatus._APPROVED_) {
		
									result = true;
								}
								if (as === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_) {
		
									result = false;
								}
							}
						}
						return result;
	},
	
	_enableIfStatus_: function() {
		
						var ctrl = this._controller_;
						var frame = ctrl.getFrame();
						var dc = frame._getDc_("contractEdit");
						var result = true;
						if (dc) {
							var record = dc.getRecord();
							if (record) {
			
								var status = record.get("status");
			
								if (status == "Effective" || status == "Expired" || status == "Active") {
									result = false;
								}
							}
						} else {
							var purchaseDc = frame._getDc_("contract");
							if (purchaseDc) {
								var record = purchaseDc.getRecord();
								if (record) {
				
									var status = record.get("status");
				
									if (status == "Effective" || status == "Expired" || status == "Active") {
										result = false;
									}
								}
							}
						}
						return result;
	},
	
	_toggle_: function() {
		
						var f = this._get_("financialSourceAvgMethodPeriod");
						f._showEditor_();
	},
	
	_doAssignCompositePrice: function() {
		
						var dc = this._controller_;
						var frame = dc.getFrame();
						var win = frame._getWindow_("priceCategoryAsgnWdw");
						win._isInEditForm_ = true;
						win.show();
	},
	
	_doAssign: function() {
		
						this._controller_.fireEvent("onAssignPriceCategory", this); 
	},
	
	_isAwaitingApproval: function(record) {
		
						return record.data.bidApprovalStatus === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_;
	},
	
	_setReadOnlyRestriction_: function() {
		
						var dc = this._controller_;
						var record = dc.getRecord();
						if (record) {
							var mainCategoryCode = record.get("mainCategoryCode");
							var pricePer = record.get("priceCtgryPricePer");
							return pricePer != __FMBAS_TYPE__.PriceInd._COMPOSITE_;			
						}
						return false;
	}
});

/* ================= EDIT FORM: FormUpdatePrice ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceCategory_Dc$FormUpdatePrice", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractPriceCategory_Dc$FormUpdatePrice",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"contract", bind:"{d.contCode}", dataIndex:"contCode", maxLength:32})
		.addDisplayFieldText({ name:"customer", bind:"{d.customerCode}", dataIndex:"customerCode", maxLength:32})
		.addDisplayFieldText({ name:"status", bind:"{d.contStatus}", dataIndex:"contStatus", maxLength:32})
		.addDisplayFieldText({ name:"location", bind:"{d.locCode}", dataIndex:"locCode", maxLength:25})
		.addDisplayFieldText({ name:"formTitle", bind:"{d.formTitle}", dataIndex:"formTitle", noEdit:true , maxLength:64})
		.add({name:"row", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("formTitle")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"p2",  cls:"sone-kpi-panel", layout: {type:"table"}})
		.addPanel({ name:"titleAndKpi",  style:"margin-bottom:10px", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"c1", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c2", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c3", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c4", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"title", width:300,  cls:"sone-title-panel", layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["titleAndKpi"])
		.addChildrenTo("p2", ["c1", "c2", "c3", "c4"])
		.addChildrenTo("titleAndKpi", ["title", "p2"])
		.addChildrenTo("c1", ["contract"])
		.addChildrenTo("c2", ["customer"])
		.addChildrenTo("c3", ["status"])
		.addChildrenTo("c4", ["location"])
		.addChildrenTo("title", ["row"]);
	},
	/* ==================== Business functions ==================== */
	
	_afterDefineElements_: function() {
		
						this._getBuilder_().change("titleAndKpi",{
							layout: {
						        type: "hbox",
						        align: "stretch"
						    },
						    defaults: {
						        flex: 1,
								height: 60
						    }
						});
	},
	
	_afterApplyStates_: function() {
		
						var formTitle = this._get_("formTitle");
						if(formTitle && formTitle.labelEl){    
		                    var record = this._controller_.getRecord(); 
		                    var ticketNoValue = "";
		                    if (record !== null){
		                    ticketNoValue = record.get("name");
		                    }
		                    var originalTitleLabel = formTitle.fieldLabel;
		                    formTitle.labelEl.update(originalTitleLabel+" "+ticketNoValue);
		                }
	}
});

/* ================= EDIT FORM: Totals ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceCategory_Dc$Totals", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractPriceCategory_Dc$Totals",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldNumber({ name:"total", bind:"{p.total}", paramIndex:"total", maxLength:19, labelWidth:150, labelAlign:"left", decimals:_SYSTEMPARAMETERS_.dec_crncy, labelStyle:"font-weight:bold; text-transform:uppercase; white-space:nowrap" })
		.addDisplayFieldText({ name:"curr", bind:"{p.settlementCurrencyCode}", paramIndex:"settlementCurrencyCode", maxLength:3, hideLabel:"true", fieldStyle:"padding-left:5px"})
		.addDisplayFieldText({ name:"unit", bind:"{p.settlementUnitCode}", paramIndex:"settlementUnitCode", maxLength:2, hideLabel:"true", fieldStyle:"padding-left:5px"})
		.addDisplayFieldText({ name:"slash", bind:"{d.slash}", dataIndex:"slash", width:10, maxLength:1, labelWidth:10})
		.add({name:"cf", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("total"),this._getConfig_("curr"),this._getConfig_("slash"),this._getConfig_("unit")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"row", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"col", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["row"])
		.addChildrenTo("row", ["col"])
		.addChildrenTo("col", ["cf"]);
	},
	/* ==================== Business functions ==================== */
	
	_afterApplyStates_: function() {
		
		
						this._controller_.on("afterDoQuerySuccess", function(dc) {
		
							var currency = dc.getParamValue("settlementCurrencyCode");
							var unit = dc.getParamValue("settlementUnitCode");
							var record = dc.getRecord();
							
							if (record) {
								var contractUnit = record.get("unitCode");
								var contractCurrency = record.get("currencyCode");
								var curr = this._get_("curr");
								var unit = this._get_("unit");
		
								if (Ext.isEmpty(curr.getValue())) {
									curr.setValue(contractCurrency);
								}
								if (Ext.isEmpty(unit.getValue())) {
									unit.setValue(contractUnit);
								}
							}
						},this);
		
	}
});
