/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.frame.Contract_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Contract_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("contract", Ext.create(atraxo.cmm.ui.extjs.dc.Contract_Dc,{}))
		.addDc("template", Ext.create(atraxo.cmm.ui.extjs.dc.Template_Dc,{ readOnly:true}))
		.addDc("attachment", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("pricingBase", Ext.create(atraxo.cmm.ui.extjs.dc.PricingBase_Dc,{}))
		.addDc("contractPriceCtgry", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceCategory_Dc,{}))
		.addDc("contractPriceCtgryAssign", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceCategoryAssign_Dc,{ _disableAutoSelection_:true}))
		.addDc("contractPriceCmpn", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceComponent_Dc,{}))
		.addDc("contractPriceCmpnClassic", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceComponentClassic_Dc,{}))
		.addDc("contractPriceRestr", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceRestriction_Dc,{multiEdit: true}))
		.addDc("contractPriceCmpnConv", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceComponentConv_Dc,{}))
		.addDc("shipTo", Ext.create(atraxo.cmm.ui.extjs.dc.ShipTo_Dc,{multiEdit: true}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("attachmentPop", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.linkDc("attachment", "contract",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}]})
				.linkDc("pricingBase", "contract",{fetchMode:"auto",fields:[
					{childField:"contractId", parentField:"id"}, {childParam:"settlementCurrencyId", parentField:"settCurrId"}]})
				.linkDc("contractPriceCtgry", "contract",{fetchMode:"auto",reloadChildrenOnParentUpdate:true,fields:[
					{childField:"contract", parentField:"id"}, {childParam:"settlementCurrencyId", parentField:"settCurrId"}, {childParam:"settlementCurrencyCode", parentField:"settCurrCode"}, {childParam:"settlementUnitCode", parentField:"settUnitCode"}, {childField:"bidApprovalStatus", parentField:"approvalStatus", noFilter:true}]})
				.linkDc("contractPriceCtgryAssign", "contract",{fetchMode:"auto",fields:[
					{childField:"contract", parentField:"id"}, {childParam:"settlementCurrencyId", parentField:"settCurrId"}, {childParam:"settlementCurrencyCode", parentField:"settCurrCode"}, {childParam:"settlementUnitCode", parentField:"settUnitCode"}]})
				.linkDc("contractPriceCmpn", "contractPriceCtgry",{fetchMode:"auto",reloadChildrenOnParentUpdate:true,fields:[
					{childField:"contractPriceCat", parentField:"id"}, {childField:"priceCtgryName", parentField:"priceCtgryName"}, {childField:"priceCtgryPricePer", parentField:"priceCtgryPricePer", noFilter:true}]})
				.linkDc("contractPriceCmpnClassic", "contractPriceCtgry",{fetchMode:"auto",reloadChildrenOnParentUpdate:true,fields:[
					{childField:"contractPriceCat", parentField:"id"}, {childField:"priceCtgryName", parentField:"priceCtgryName"}, {childField:"priceCtgryPricePer", parentField:"priceCtgryPricePer", noFilter:true}]})
				.linkDc("contractPriceRestr", "contractPriceCtgry",{fetchMode:"auto",fields:[
					{childField:"contractPriceCtgry", parentField:"id"}, {childParam:"isEnabled", parentField:"restriction"}]})
				.linkDc("contractPriceCmpnConv", "contractPriceCtgry",{fetchMode:"auto",fields:[
					{childField:"contractPriceCtgry", parentField:"id"}]})
				.linkDc("shipTo", "contract",{fetchMode:"auto",fields:[
					{childField:"contractId", parentField:"id"}, {childParam:"contractStatus", parentField:"status"}]})
				.linkDc("history", "contract",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("attachmentPop", "contract",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}, {childField:"categType", value:"upload"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"moveUpButton",glyph:fp_asc.up_glyph.glyph, disabled:false, handler: this.onMoveUpButton,stateManager:[{ name:"record_is_clean", dc:"contractPriceCtgry"}], scope:this})
		.addButton({name:"moveDownButton",glyph:fp_asc.down_glyph.glyph, disabled:false, handler: this.onMoveDownButton,stateManager:[{ name:"record_is_clean", dc:"contractPriceCtgry"}], scope:this})
		.addButton({name:"btnCopyInList",glyph:fp_asc.copy_glyph.glyph,iconCls: fp_asc.copy_glyph.css, disabled:true, handler: this.onBtnCopyInList,stateManager:[{ name:"selected_one_clean", dc:"contract"}], scope:this})
		.addButton({name:"btnSetActive",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSetActive,stateManager:[{ name:"selected_one", dc:"contract", and: function() {return (this.canActivateContract());} }], scope:this, visible: !this.useWorkflowForApproval() })
		.addButton({name:"btnSetActive2",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSetActive2,stateManager:[{ name:"selected_one_clean", dc:"contract", and: function() {return (this.canActivateContract());} }], scope:this, visible: !this.useWorkflowForApproval() })
		.addButton({name:"savePriceCtgry",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onSavePriceCtgry,stateManager:[{ name:"record_is_dirty", dc:"contractPriceCtgry"}], scope:this})
		.addButton({name:"btnBackFromPriceCategory",glyph:fp_asc.back_glyph.glyph,iconCls: fp_asc.back_glyph.css, disabled:false, handler: this.onBtnBackFromPriceCategory, scope:this})
		.addButton({name:"btnUpdateStatus",glyph:fp_asc.commit_glyph.glyph,iconCls: fp_asc.commit_glyph.css, disabled:true, handler: this.onBtnUpdateStatus,stateManager:[{ name:"selected_one", dc:"contract", and: function(dc) {return (dc.record.data.status==='Active' || dc.record.data.status==='Effective' || dc.record.data.status==='Expired');} }], scope:this})
		.addButton({name:"btnResetStatus",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnResetStatus,stateManager:[{ name:"selected_one", dc:"contract", and: function(dc) {return ((dc.record.data.status==='Active' || dc.record.data.status==='Effective' || dc.record.data.status==='Expired'));} }], scope:this})
		.addButton({name:"btnResetStatusEdit",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnResetStatusEdit,stateManager:[{ name:"selected_one", dc:"contract", and: function(dc) {return ((dc.record.data.status==='Active' || dc.record.data.status==='Effective' || dc.record.data.status==='Expired'));} }], scope:this})
		.addButton({name:"btnContinue",glyph:fp_asc.continue_glyph.glyph,iconCls: fp_asc.continue_glyph.css, disabled:true, handler: this.onBtnContinue,stateManager:[{ name:"selected_one_clean", dc:"template"}], scope:this})
		.addButton({name:"Cancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onCancel, scope:this})
		.addButton({name:"CancelPb",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onCancelPb, scope:this})
		.addButton({name:"CancelPbEditFormula",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onCancelPbEditFormula, scope:this})
		.addButton({name:"btnSaveClosePriceWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveClosePriceWdw, scope:this})
		.addButton({name:"btnSaveClosePriceEditFormulaWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveClosePriceEditFormulaWdw, scope:this})
		.addButton({name:"btnSaveClosePriceCategoryWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveClosePriceCategoryWdw, scope:this})
		.addButton({name:"btnSaveNewPriceCategoryWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewPriceCategoryWdw, scope:this})
		.addButton({name:"btnSaveNewCompositePrice",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewCompositePrice, scope:this})
		.addButton({name:"btnSaveCloseCompositePrice",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseCompositePrice, scope:this})
		.addButton({name:"btnSaveEditCompositePrice",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveEditCompositePrice, scope:this})
		.addButton({name:"btnCancelCompositePrice",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelCompositePrice, scope:this})
		.addButton({name:"btnSaveEditPriceCategoryWdw",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnSaveEditPriceCategoryWdw, scope:this})
		.addButton({name:"btnClosePriceFixWdw",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onBtnClosePriceFixWdw, scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"WdwHelp1",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onWdwHelp1, scope:this})
		.addButton({name:"WdwHelp2",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onWdwHelp2, scope:this})
		.addButton({name:"btnDeleteContractPriceCtgry",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteContractPriceCtgry,stateManager:[{ name:"selected_one", dc:"contractPriceCtgry", and: function() {return (this._checkPricingBaseId_() && this._checkIfContractAwaitingApproval_() );} }], scope:this})
		.addButton({name:"btnDeleteContractPriceComponent",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteContractPriceComponent,stateManager:[{ name:"selected_one", dc:"contractPriceCmpn", and: function() {return (this._checkPriceCtgryName_() && this._checkIfContractAwaitingApproval_());} }], scope:this})
		.addButton({name:"btnNewContractPriceComponent",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewContractPriceComponent,stateManager:[{ name:"no_record_or_record_is_clean", dc:"contractPriceCmpn", and: function() {return (this._checkPriceCtgryPricePer_() && this._checkIfContractAwaitingApproval_() && this._checkPercent_());} }], scope:this})
		.addButton({name:"btnDeleteContractPricingBase",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteContractPricingBase,stateManager:[{ name:"selected_one", dc:"pricingBase", and: function(dc) {return (dc.store.totalCount!==1 );} }], scope:this})
		.addButton({name:"btnUpdatePrice",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnUpdatePrice,stateManager:[{ name:"selected_one", dc:"contractPriceCtgry"}], scope:this})
		.addButton({name:"btnEditFormulaPrice",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnEditFormulaPrice,stateManager:[{ name:"selected_one", dc:"contractPriceCtgry", and: function() {return (this._checkIfFormulaPrice_());} }], scope:this})
		.addButton({name:"btnPriceCompSave",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnPriceCompSave, scope:this})
		.addButton({name:"btnPriceCompCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnPriceCompCancel, scope:this})
		.addButton({name:"btnNewWithMenu",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, isBtnContainer:true,stateManager:[{ name:"record_is_clean", dc:"contract", and: function(dc) {return (dc.record.data.approvalStatus !== __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_);} }], scope:this})
		.addButton({name:"btnNewFormulaPrice",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false,  _btnContainerName_:"btnNewWithMenu", _isDefaultHandler_:true, handler: this.onBtnNewFormulaPrice, scope:this})
		.addButton({name:"btnNewFixPrice",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false,  _btnContainerName_:"btnNewWithMenu", handler: this.onBtnNewFixPrice, scope:this})
		.addButton({name:"btnNewDutyFeeTax",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false,  _btnContainerName_:"btnNewWithMenu", handler: this.onBtnNewDutyFeeTax, scope:this})
		.addButton({name:"btnNewCompositePrice",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false,  _btnContainerName_:"btnNewWithMenu", handler: this.onBtnNewCompositePrice, scope:this})
		.addButton({name:"btnContinueNewPurchaseContract",glyph:fp_asc.rollforward_glyph.glyph,iconCls: fp_asc.rollforward_glyph.css,  disabled:true, handler: this.onBtnContinueNewPurchaseContract, scope:this})
		.addButton({name:"btnContinueCopyPurchaseContract",glyph:fp_asc.rollforward_glyph.glyph,iconCls: fp_asc.rollforward_glyph.css,  disabled:true, handler: this.onBtnContinueCopyPurchaseContract, scope:this})
		.addButton({name:"btnCancelNewPurchaseContract",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelNewPurchaseContract, scope:this})
		.addButton({name:"btnCancelCopyPurchaseContract",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelCopyPurchaseContract, scope:this})
		.addButton({name:"btnCancelWithMenu",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"shipTo", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", handler: this.onBtnCancelSelected, scope:this})
		.addButton({name:"btnCancelAll",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", _isDefaultHandler_:true, handler: this.onBtnCancelAll, scope:this})
		.addButton({name:"btnSaveCloseRemarkWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRemarkWdw, scope:this})
		.addButton({name:"btnSaveCloseResetRemarkWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseResetRemarkWdw, scope:this})
		.addButton({name:"btnCancelResetRemarkWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelResetRemarkWdw, scope:this})
		.addButton({name:"btnCancelRemarkWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRemarkWdw, scope:this})
		.addButton({name:"btnAssignPriceComponents",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnAssignPriceComponents, scope:this})
		.addButton({name:"btnCancelAssignPriceComponents",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelAssignPriceComponents, scope:this})
		.addButton({name:"btnGenerateSaleContractMenu",glyph:fp_asc.copy_glyph.glyph,iconCls: fp_asc.copy_glyph.css, disabled:false, isBtnContainer:true, scope:this})
		.addButton({name:"btnGenerateExternalSaleContract", disabled:true, handler: this.onBtnGenerateExternalSaleContract,stateManager:[{ name:"selected_one", dc:"contract", and: function(dc) {return (dc.record.data.status!=='Expired');} }], scope:this})
		.addButton({name:"btnGenerateInternalSaleContract",glyph:fp_asc.copy_glyph.glyph,iconCls: fp_asc.copy_glyph.css, disabled:true,  _btnContainerName_:"btnGenerateSaleContractMenu", handler: this.onBtnGenerateInternalSaleContract,stateManager:[{ name:"selected_one", dc:"contract", and: function(dc) {return (dc.record.data.status === __CMM_TYPE__.ContractStatus._DRAFT_ && ((_SYSTEMPARAMETERS_.syspurchasecontractapproval === 'true' && (dc.record.data.approvalStatus === __CMM_TYPE__.BidApprovalStatus._NEW_ || dc.record.data.approvalStatus === __CMM_TYPE__.BidApprovalStatus._REJECTED_)) || _SYSTEMPARAMETERS_.syspurchasecontractapproval === 'false') && Ext.isEmpty(dc.record.data.resaleContract));} }], scope:this})
		.addButton({name:"btnOpenCustomerWdw",glyph:fp_asc.copy_glyph.glyph,iconCls: fp_asc.copy_glyph.css, disabled:true,  _btnContainerName_:"btnGenerateSaleContractMenu", _isDefaultHandler_:true, handler: this.onBtnOpenCustomerWdw,stateManager:[{ name:"selected_one", dc:"contract", and: function(dc) {return (dc.record.data.status!=='Expired');} }], scope:this})
		.addButton({name:"btnNewRestriction",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewRestriction,stateManager:[{ name:"no_record_or_record_is_clean", dc:"contractPriceRestr", and: function() {return (this._applyRestrictionState_());} }], scope:this})
		.addButton({name:"btnSaveRestriction",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSaveRestriction,stateManager:[{ name:"selected_one_dirty", dc:"contractPriceRestr", and: function() {return (this._applyRestrictionState_());} }], scope:this})
		.addButton({name:"btnCancelRestriction",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnCancelRestriction,stateManager:[{ name:"selected_one_dirty", dc:"contractPriceRestr", and: function() {return (this._applyRestrictionState_());} }], scope:this})
		.addButton({name:"btnDeleteRestriction",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteRestriction,stateManager:[{ name:"selected_one_clean", dc:"contractPriceRestr", and: function() {return (this._applyRestrictionState_());} }], scope:this})
		.addButton({name:"btnApprove",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApprove,stateManager:[{ name:"selected_one_clean", dc:"contract", and: function(dc) {return (this.canApprove(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnApproveList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApproveList,stateManager:[{ name:"selected_one_clean", dc:"contract", and: function(dc) {return (this.canApprove(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnReject",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnReject,stateManager:[{ name:"selected_one_clean", dc:"contract", and: function(dc) {return (this.canApprove(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnRejectList",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnRejectList,stateManager:[{ name:"selected_one_clean", dc:"contract", and: function(dc) {return (this.canApprove(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnSubmitForApproval",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitForApproval,stateManager:[{ name:"selected_one_clean", dc:"contract", and: function(dc) {return (this.canSubmitForApproval(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnSubmitForApprovalList",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitForApprovalList,stateManager:[{ name:"selected_one_clean", dc:"contract", and: function(dc) {return (this.canSubmitForApproval(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnSaveCloseApprovalWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApprovalWdw, scope:this})
		.addButton({name:"btnCancelApprovalWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApprovalWdw, scope:this})
		.addButton({name:"btnSaveCloseApproveWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApproveWdw, scope:this})
		.addButton({name:"btnSaveCloseRejectWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRejectWdw, scope:this})
		.addButton({name:"btnCancelApproveWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApproveWdw, scope:this})
		.addButton({name:"btnCancelRejectWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRejectWdw, scope:this})
		.addButton({name:"btnEditPriceComponent",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnEditPriceComponent,stateManager:[{ name:"selected_one", dc:"contractPriceCmpn", and: function() {return (this._checkIfContractAwaitingApproval_());} }], scope:this})
		.addButton({name:"btnDeleteResale",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true,  hidden:true, handler: this.onBtnDeleteResale,stateManager:[{ name:"selected_one", dc:"contract", and: function(dc) {return (dc.record.data.status === __CMM_TYPE__.ContractStatus._DRAFT_ && ((_SYSTEMPARAMETERS_.syspurchasecontractapproval === 'true' && (dc.record.data.approvalStatus === __CMM_TYPE__.BidApprovalStatus._NEW_ || dc.record.data.approvalStatus === __CMM_TYPE__.BidApprovalStatus._REJECTED_)) || _SYSTEMPARAMETERS_.syspurchasecontractapproval === 'false') && !Ext.isEmpty(dc.record.data.resaleContract));} }], scope:this})
		.addDcGridView("contract", {name:"BuyContract", xtype:"cmm_Contract_Dc$TabList"})
		.addDcFilterFormView("contract", {name:"BuyContractFilter", xtype:"cmm_Contract_Dc$Filter"})
		.addDcFormView("contract", {name:"Header", xtype:"cmm_Contract_Dc$Header", _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcGridView("template", {name:"TempList", xtype:"cmm_Template_Dc$List",  split:false})
		.addDcGridView("template", {name:"TempListWdw", xtype:"cmm_Template_Dc$List",  split:false})
		.addDcFilterFormView("pricingBase", {name:"PbFilter", xtype:"cmm_PricingBase_Dc$Filter"})
		.addDcFormView("pricingBase", {name:"PbEdit", xtype:"cmm_PricingBase_Dc$NewFormulaPrice"})
		.addDcFormView("pricingBase", {name:"PbEditFormula", xtype:"cmm_PricingBase_Dc$EditFormulaPrice"})
		.addDcFormView("pricingBase", {name:"priceFixNew", xtype:"cmm_PricingBase_Dc$NewPCBuyContract"})
		.addDcFormView("pricingBase", {name:"compositePriceNew", xtype:"cmm_PricingBase_Dc$NewCompositePrice"})
		.addDcGridView("attachment", {name:"attachmentList", _hasTitle_:true, xtype:"ad_Attachment_Dc$List"})
		.addDcFormView("attachment", {name:"attachmentEdit", xtype:"ad_Attachment_Dc$New"})
		.addDcFilterFormView("contractPriceCtgry", {name:"priceCtgryFilter", xtype:"cmm_ContractPriceCategory_Dc$Filter"})
		.addDcGridView("contractPriceCtgry", {name:"priceCtgryList", xtype:"cmm_ContractPriceCategory_Dc$List"})
		.addDcGridView("contractPriceCtgryAssign", {name:"priceCtgryAssignList", xtype:"cmm_ContractPriceCategoryAssign_Dc$AssignList",  listeners:{ afterrender: { scope: this, fn: function(list) { this._excludeComposite_(list)}}}})
		.addDcFormView("contractPriceCtgry", {name:"priceCtgryEditPopUp", xtype:"cmm_ContractPriceCategory_Dc$NewGrid"})
		.addDcFormView("contractPriceCtgry", {name:"priceCtgryEdit", xtype:"cmm_ContractPriceCategory_Dc$EditGrid"})
		.addDcFilterFormView("contractPriceCmpn", {name:"priceCompFilter", xtype:"cmm_ContractPriceComponent_Dc$Filter"})
		.addDcGridView("contractPriceCmpnClassic", {name:"priceCompList", xtype:"cmm_ContractPriceComponentClassic_Dc$SimpleList",  split:false})
		.addDcFormView("contractPriceCmpnClassic", {name:"priceCompEdit", xtype:"cmm_ContractPriceComponentClassic_Dc$Edit"})
		.addDcFormView("contract", {name:"CustomerSelect", xtype:"cmm_Contract_Dc$SelectCustomer"})
		.addDcFormView("contract", {name:"copySupplier", xtype:"cmm_Contract_Dc$copySupplier",  split:false})
		.addDcFormView("contract", {name:"approvalNote", xtype:"cmm_Contract_Dc$ApprovalNote",  split:false})
		.addDcFormView("contract", {name:"approveNote", xtype:"cmm_Contract_Dc$ApprovalNote"})
		.addDcFormView("contract", {name:"rejectNote", xtype:"cmm_Contract_Dc$ApprovalNote"})
		.addDcGridView("attachmentPop", {name:"documentsAssignList", _hasTitle_:true, height:300, xtype:"ad_Attachment_Dc$attachmentAssignList",  cls:"sone-custom-panel-title"})
		.addDcEditGridView("contractPriceRestr", {name:"priceRestr", xtype:"cmm_ContractPriceRestriction_Dc$List", frame:true,  listeners:{ beforeedit: { scope: this, fn: function(editor, ctx) { this._setPriceRestrictionEditorType_(editor, ctx, this._get_('priceRestr'))}}}})
		.addPanel({name:"priceRestrWrapper", layout:"fit"})
		.addDcFormView("contractPriceCtgry", {name:"UpdatePriceHeader", xtype:"cmm_ContractPriceCategory_Dc$FormUpdatePrice",  cls:"form-large-space", split:false})
		.addDcFormView("contract", {name:"paymentTerms", _hasTitle_:true, xtype:"cmm_Contract_Dc$PaymentTerms", _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcFormView("contract", {name:"volumes", _hasTitle_:true, xtype:"cmm_Contract_Dc$Volumes"})
		.addDcFilterFormView("contractPriceCmpnConv", {name:"priceComponentConvFilter", xtype:"cmm_ContractPriceComponentConv_Dc$Filter"})
		.addDcGridView("contractPriceCmpnConv", {name:"priceComponentConv", _hasTitle_:true, height:300, xtype:"cmm_ContractPriceComponentConv_Dc$List"})
		.addDcFormView("contractPriceCtgry", {name:"totals", xtype:"cmm_ContractPriceCategory_Dc$Totals",  split:false, cls:"sone-total-panel", listeners:{afterrender: {scope: this, fn: function(view) {var grid = this._get_('priceCtgryList'); grid._totalPanelId_ = view.getId(); }}}})
		.addDcFormView("contract", {name:"selectSupplier", height:80, xtype:"cmm_Contract_Dc$selectSupplier",  split:false})
		.addDcFilterFormView("shipTo", {name:"ShipToFilter", xtype:"cmm_ShipTo_Dc$Filter"})
		.addDcEditGridView("shipTo", {name:"ShipToEditList", _hasTitle_:true, xtype:"cmm_ShipTo_Dc$EditListPurchase", frame:true, _summaryDefined_:true})
		.addDcGridView("history", {name:"History", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("shipTo", {name:"remark", xtype:"cmm_ShipTo_Dc$EditRemark"})
		.addDcFormView("contract", {name:"remarkReset", xtype:"cmm_Contract_Dc$Remark"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas4", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"newPurchaseContract", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"panelAssignAttachments", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"prices", _hasTitle_:true, layout:{ type: "border" }, defaults:{split:true}})
		.addWindow({name:"wdwNewPurchaseContract", _hasTitle_:true, width:420, height:360, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("newPurchaseContract")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnContinueNewPurchaseContract"), this._elems_.get("btnCancelNewPurchaseContract")]}]})
		.addWindow({name:"wdwCopyPurchaseContract", _hasTitle_:true, width:260, height:170, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("copySupplier")],  listeners:{ show:{fn:function(win) { win.down('form')._isVisible_ = true;}, scope:this}, close:{fn:function(win) {win.down('form')._isVisible_ = false;}, scope:this} }, closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnContinueCopyPurchaseContract"), this._elems_.get("btnCancelCopyPurchaseContract")]}]})
		.addWindow({name:"wdwRemarks", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("remark")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRemarkWdw"), this._elems_.get("btnCancelRemarkWdw")]}]})
		.addWindow({name:"wdwPricing", _hasTitle_:true, width:600, height:340, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("PbEdit")],  listeners:{ show:{fn:function(win) { win.down('form')._isVisible_ = true;}, scope:this}, close:{fn:function(win) {this._onWdwwClose_(win);}, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveClosePriceWdw"), this._elems_.get("CancelPb")]}]})
		.addWindow({name:"wdwPricingEditFormula", _hasTitle_:true, width:600, height:340, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("PbEditFormula")],  listeners:{ show:{fn:function(win) {win.down('form')._isVisible_ = true;}, scope:this}, close:{fn:function(win) {this._onWdwwClose_(win);}, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveClosePriceEditFormulaWdw"), this._elems_.get("CancelPbEditFormula")]}]})
		.addWindow({name:"wdwTemp", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("TempListWdw")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnContinue"), this._elems_.get("Cancel")]}]})
		.addWindow({name:"priceFixWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("priceFixNew")],  listeners:{ show:{fn:function(win) {win.down('form')._isVisible_ = true;}, scope:this}, close:{fn:function(win) {this._onWdwwClose_(win);}, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewPriceCategoryWdw"), this._elems_.get("btnSaveClosePriceCategoryWdw"), this._elems_.get("btnSaveEditPriceCategoryWdw"), this._elems_.get("btnClosePriceFixWdw")]}]})
		.addWindow({name:"priceCompWdw", width:400, height:200, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("priceCompEdit")],  listeners:{ close:{fn:this.onPriceCompWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnPriceCompSave"), this._elems_.get("btnPriceCompCancel")]}]})
		.addWindow({name:"compositePriceWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("compositePriceNew")],  listeners:{ show:{fn:function(win) {win.down('form')._isVisible_ = true;}, scope:this}, close:{fn:function(win) {this._onWdwwClose_(win);}, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewCompositePrice"), this._elems_.get("btnSaveCloseCompositePrice"), this._elems_.get("btnSaveEditCompositePrice"), this._elems_.get("btnCancelCompositePrice")]}]})
		.addWindow({name:"priceCategoryAsgnWdw", _hasTitle_:true, width:800, height:400, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("priceCtgryAssignList")],  listeners:{ show:{fn:function() {this._setMainPriceCategory_(); this._getDc_('contractPriceCtgryAssign').doQuery({markSelected: true})}, scope:this}, close: {fn: function() {this._get_('priceCtgryAssignList').getSelectionModel().deselectAll()}, scope: this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnAssignPriceComponents"), this._elems_.get("btnCancelAssignPriceComponents")]}]})
		.addWindow({name:"wdwGenarateSaleContract", _hasTitle_:true, width:290, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("CustomerSelect")],  buttonAlign:"center", listeners:{ show:{fn:function(win) { win.down('form')._isVisible_ = true;}, scope:this}, close:{fn:function(win) {this._cancelContract_(win);}, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnGenerateExternalSaleContract")]}]})
		.addWindow({name:"wdwApprovalNote", _hasTitle_:true, width:600, height:530, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("panelAssignAttachments")],  closable:true, listeners:{ show:{fn:this.onApprovalWdwShow, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCancelApprovalWdw"), this._elems_.get("btnSaveCloseApprovalWdw")]}]})
		.addWindow({name:"wdwApproveNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("approveNote")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseApproveWdw"), this._elems_.get("btnCancelApproveWdw")]}]})
		.addWindow({name:"wdwRejectNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("rejectNote")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRejectWdw"), this._elems_.get("btnCancelRejectWdw")]}]})
		.addWindow({name:"wdwRemarkReset", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("remarkReset")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseResetRemarkWdw"), this._elems_.get("btnCancelResetRemarkWdw")]}]})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas2", dcName:"contract"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("priceRestrWrapper", ["priceRestr"])
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["BuyContract"], ["center"])
		.addChildrenTo("canvas2", ["Header", "detailsTab"], ["north", "center"])
		.addChildrenTo("canvas3", ["priceCtgryEdit", "priceRestrWrapper"], ["north", "center"])
		.addChildrenTo("canvas4", ["UpdatePriceHeader", "priceCompList", "priceComponentConv"], ["north", "center", "south"])
		.addChildrenTo("newPurchaseContract", ["selectSupplier", "TempList"], ["north", "center"])
		.addChildrenTo("detailsTab", ["prices"])
		.addChildrenTo("panelAssignAttachments", ["approvalNote", "documentsAssignList"], ["north", "center"])
		.addChildrenTo("prices", ["priceCtgryList", "totals"], ["center", "south"])
		.addToolbarTo("Header", "tlbHeader")
		.addToolbarTo("priceCtgryList", "tlbPriceCategoryList")
		.addToolbarTo("UpdatePriceHeader", "tlbPriceComponentList")
		.addToolbarTo("priceCtgryEdit", "tlbPriceCategoryDetails")
		.addToolbarTo("priceRestrWrapper", "tlbPriceCategoryRestr")
		.addToolbarTo("attachmentList", "tlbAttachments")
		.addToolbarTo("BuyContract", "tblBuyContract")
		.addToolbarTo("ShipToEditList", "tlbShipTo");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2", "canvas3", "canvas4"])
		.addChildrenTo2("detailsTab", ["paymentTerms", "volumes", "ShipToEditList", "attachmentList", "History"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbHeader", {dc: "contract"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas1"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnSetActive2"),this._elems_.get("btnSubmitForApproval"),this._elems_.get("btnApprove"),this._elems_.get("btnReject"),this._elems_.get("WdwHelp1")])
			.addReports()
		.end()
		.beginToolbar("tlbPriceCategoryList", {dc: "contractPriceCtgry"})
			.addButtons([this._elems_.get("btnNewWithMenu") ])
			.addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas3"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnDeleteContractPriceCtgry"),this._elems_.get("btnUpdatePrice"),this._elems_.get("btnEditFormulaPrice"),this._elems_.get("btnNewFormulaPrice"),this._elems_.get("btnNewFixPrice"),this._elems_.get("btnNewDutyFeeTax"),this._elems_.get("btnNewCompositePrice"),this._elems_.get("moveUpButton"),this._elems_.get("moveDownButton")])
			.addReports()
		.end()
		.beginToolbar("tlbPriceComponentList", {dc: "contractPriceCmpnClassic"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnBackFromPriceCategory"),this._elems_.get("btnNewContractPriceComponent"),this._elems_.get("btnEditPriceComponent"),this._elems_.get("btnDeleteContractPriceComponent")])
			.addReports()
		.end()
		.beginToolbar("tlbPriceCategoryDetails", {dc: "contractPriceCtgry"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas2"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("savePriceCtgry")])
			.addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addPrevRec({glyph:fp_asc.prev_glyph.glyph})
			.addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbPriceCategoryRestr", {dc: "contractPriceRestr"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewRestriction"),this._elems_.get("btnSaveRestriction"),this._elems_.get("btnCancelRestriction"),this._elems_.get("btnDeleteRestriction")])
			.addReports()
		.end()
		.beginToolbar("tlbAttachments", {dc: "attachment"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tblBuyContract", {dc: "contract"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCopyInList"),this._elems_.get("btnSubmitForApprovalList"),this._elems_.get("btnApproveList"),this._elems_.get("btnRejectList"),this._elems_.get("btnSetActive"),this._elems_.get("btnUpdateStatus"),this._elems_.get("btnResetStatus"),this._elems_.get("btnGenerateSaleContractMenu"),this._elems_.get("btnOpenCustomerWdw"),this._elems_.get("btnGenerateInternalSaleContract"),this._elems_.get("btnDeleteResale"),this._elems_.get("WdwHelp2")])
			.addReports()
		.end()
		.beginToolbar("tlbShipTo", {dc: "shipTo"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelWithMenu"),this._elems_.get("btnCancelSelected"),this._elems_.get("btnCancelAll")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button moveUpButton
	 */
	,onMoveUpButton: function() {
		var successFn = function() {
			this._getDc_("contractPriceCtgry").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"moveUp",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("contractPriceCtgry").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button moveDownButton
	 */
	,onMoveDownButton: function() {
		var successFn = function() {
			this._getDc_("contractPriceCtgry").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"moveDown",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("contractPriceCtgry").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCopyInList
	 */
	,onBtnCopyInList: function() {
		this._getWindow_('wdwCopyPurchaseContract').show();
	}
	
	/**
	 * On-Click handler for button btnSetActive
	 */
	,onBtnSetActive: function() {
		var message = Main.translate("applicationMsg", "settingContractActive__lbl");
					var dc = this._getDc_("contract");
					var record = dc.getRecord();
					var resaleContractCode = record.get("resaleContract");
					if(!Ext.isEmpty(resaleContractCode)){ 
						message = message.concat("<br>").concat(Main.translate("applicationMsg", "updateLinkedSaleContract_lbl")).concat(resaleContractCode);
					}
					Ext.Msg.show({
				       title : "Note",
				       msg : message,
				       icon : Ext.MessageBox.QUESTION,
				       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
				       fn : function(btnId) {
				              if( btnId === "yes" ){
				                     this.setActiveYes();
		              }
		       },
		       scope : this
				});
	}
	
	/**
	 * On-Click handler for button btnSetActive2
	 */
	,onBtnSetActive2: function() {
		var message = Main.translate("applicationMsg", "settingContractActive__lbl");
					var dc = this._getDc_("contract");
					var record = dc.getRecord();
					var resaleContractCode = record.get("resaleContract");
					if(!Ext.isEmpty(resaleContractCode)){ 
						message = message.concat("<br>").concat(Main.translate("applicationMsg", "updateLinkedSaleContract_lbl")).concat(resaleContractCode);
					}
					Ext.Msg.show({
				       title : "Note",
				       msg : message,
				       icon : Ext.MessageBox.QUESTION,
				       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
				       fn : function(btnId) {
				              if( btnId === "yes" ){
				                     this.setActiveYes();
		              }
		       },
		       scope : this
				});
	}
	
	/**
	 * On-Click handler for button savePriceCtgry
	 */
	,onSavePriceCtgry: function() {
		this.saveContractPriceCtgry(this._getDc_("contractPriceCtgry"));
	}
	
	/**
	 * On-Click handler for button btnBackFromPriceCategory
	 */
	,onBtnBackFromPriceCategory: function() {
		this._showStackedViewElement_("main", "canvas2", this._whenBackFronPriceUpdate_())
	}
	
	/**
	 * On-Click handler for button btnUpdateStatus
	 */
	,onBtnUpdateStatus: function() {
		var dc = this._getDc_("contract");
					var record = dc.getRecord();
					var resaleContractCode = record.get("resaleContract");
					if(!Ext.isEmpty(resaleContractCode)){
						Ext.Msg.show({
				       title : "Note",
				       msg : Main.translate("applicationMsg", "updateLinkedSaleContract_lbl").concat(resaleContractCode),
				       icon : Ext.MessageBox.QUESTION,
				       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
				       fn : function(btnId) {
				              if( btnId === "yes" ){
				                     this.updateStatus();
		              			}
				       },
				       scope : this
						});	
					} else {
						this.updateStatus();
					}
	}
	
	/**
	 * On-Click handler for button btnResetStatus
	 */
	,onBtnResetStatus: function() {
		this._getWindow_("wdwRemarkReset").show();
		
						var remarksField = this._get_("remarkReset")._get_("remarks");
						var saveButton = this._get_("btnSaveCloseResetRemarkWdw");
						var cancelButton = this._get_("btnCancelResetRemarkWdw");
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(false);
						remarksField.setReadOnly(false);
		
						remarksField.on("change", function(field) {
							if (field.value.length > 0) {
								saveButton.setDisabled(false);						
							}	
							else {
								saveButton.setDisabled(true);						
							}	
						});
	}
	
	/**
	 * On-Click handler for button btnResetStatusEdit
	 */
	,onBtnResetStatusEdit: function() {
		this._getWindow_("wdwRemarkReset").show();
		
						var remarksField = this._get_("remarkReset")._get_("remarks");
						var saveButton = this._get_("btnSaveCloseResetRemarkWdw");
						var cancelButton = this._get_("btnCancelResetRemarkWdw");
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(false);
						remarksField.setReadOnly(false);
		
						remarksField.on("change", function(field) {
							if (field.value.length > 0) {
								saveButton.setDisabled(false);						
							}	
							else {
								saveButton.setDisabled(true);						
							}	
						});
	}
	
	/**
	 * On-Click handler for button btnContinue
	 */
	,onBtnContinue: function() {
		this._getWindow_("wdwTemp").close();
		this.Cont();
	}
	
	/**
	 * On-Click handler for button Cancel
	 */
	,onCancel: function() {
		this._getDc_("contract").doCancel();
		this._getWindow_("wdwTemp").close();
	}
	
	/**
	 * On-Click handler for button CancelPb
	 */
	,onCancelPb: function() {
		this._cancelIndexPrice_();
	}
	
	/**
	 * On-Click handler for button CancelPbEditFormula
	 */
	,onCancelPbEditFormula: function() {
		this._cancelEditIndexPrice_();
	}
	
	/**
	 * On-Click handler for button btnSaveClosePriceWdw
	 */
	,onBtnSaveClosePriceWdw: function() {
		this.saveClosePriceBase();
	}
	
	/**
	 * On-Click handler for button btnSaveClosePriceEditFormulaWdw
	 */
	,onBtnSaveClosePriceEditFormulaWdw: function() {
		this.saveClosePriceEditFormulaBase();
	}
	
	/**
	 * On-Click handler for button btnSaveClosePriceCategoryWdw
	 */
	,onBtnSaveClosePriceCategoryWdw: function() {
		this.saveClosePriceCategory();
	}
	
	/**
	 * On-Click handler for button btnSaveNewPriceCategoryWdw
	 */
	,onBtnSaveNewPriceCategoryWdw: function() {
		this.saveNewPriceCategory();
	}
	
	/**
	 * On-Click handler for button btnSaveNewCompositePrice
	 */
	,onBtnSaveNewCompositePrice: function() {
		this.saveNewPriceCategory();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseCompositePrice
	 */
	,onBtnSaveCloseCompositePrice: function() {
		this.saveCloseCompositePrice();
	}
	
	/**
	 * On-Click handler for button btnSaveEditCompositePrice
	 */
	,onBtnSaveEditCompositePrice: function() {
		this.saveEditCompositePrice();
	}
	
	/**
	 * On-Click handler for button btnCancelCompositePrice
	 */
	,onBtnCancelCompositePrice: function() {
		this._cancelCompositePrice_();
	}
	
	/**
	 * On-Click handler for button btnSaveEditPriceCategoryWdw
	 */
	,onBtnSaveEditPriceCategoryWdw: function() {
		this.saveEditPriceCategory();
	}
	
	/**
	 * On-Click handler for button btnClosePriceFixWdw
	 */
	,onBtnClosePriceFixWdw: function() {
		this._cancelPriceFix_();
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button WdwHelp1
	 */
	,onWdwHelp1: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button WdwHelp2
	 */
	,onWdwHelp2: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnDeleteContractPriceCtgry
	 */
	,onBtnDeleteContractPriceCtgry: function() {
		this.deleteContractPriceCtgry();
	}
	
	/**
	 * On-Click handler for button btnDeleteContractPriceComponent
	 */
	,onBtnDeleteContractPriceComponent: function() {
		this.deleteContractPriceComponent();
	}
	
	/**
	 * On-Click handler for button btnNewContractPriceComponent
	 */
	,onBtnNewContractPriceComponent: function() {
		this._getDc_("contractPriceCmpnClassic").doNew();
	}
	
	/**
	 * On-Click handler for button btnDeleteContractPricingBase
	 */
	,onBtnDeleteContractPricingBase: function() {
		this.deleteContractPricingBases();
	}
	
	/**
	 * On-Click handler for button btnUpdatePrice
	 */
	,onBtnUpdatePrice: function() {
		this._showStackedViewElement_("main", "canvas4", this._disableDeleteIfIndex_()); 
	}
	
	/**
	 * On-Click handler for button btnEditFormulaPrice
	 */
	,onBtnEditFormulaPrice: function() {
		this._editFormulaPrice_();
	}
	
	/**
	 * On-Click handler for button btnPriceCompSave
	 */
	,onBtnPriceCompSave: function() {
		this.saveClosePriceComp();
	}
	
	/**
	 * On-Click handler for button btnPriceCompCancel
	 */
	,onBtnPriceCompCancel: function() {
		this.onPriceCompWdwClose();
		this._getWindow_("priceCompWdw").close();
	}
	
	/**
	 * On-Click handler for button btnNewFormulaPrice
	 */
	,onBtnNewFormulaPrice: function() {
		this._setDefaultPb();
	}
	
	/**
	 * On-Click handler for button btnNewFixPrice
	 */
	,onBtnNewFixPrice: function() {
		this._setDefaultFix();
	}
	
	/**
	 * On-Click handler for button btnNewDutyFeeTax
	 */
	,onBtnNewDutyFeeTax: function() {
		this._setDefaultDFT();
	}
	
	/**
	 * On-Click handler for button btnNewCompositePrice
	 */
	,onBtnNewCompositePrice: function() {
		this._setDefaultComposite();
	}
	
	/**
	 * On-Click handler for button btnContinueNewPurchaseContract
	 */
	,onBtnContinueNewPurchaseContract: function() {
		this._continueNewPurchaseContract_();
	}
	
	/**
	 * On-Click handler for button btnContinueCopyPurchaseContract
	 */
	,onBtnContinueCopyPurchaseContract: function() {
		this._continueCopyPurchaseContract_();
	}
	
	/**
	 * On-Click handler for button btnCancelNewPurchaseContract
	 */
	,onBtnCancelNewPurchaseContract: function() {
		this._cancelNewPurchaseContract_();
	}
	
	/**
	 * On-Click handler for button btnCancelCopyPurchaseContract
	 */
	,onBtnCancelCopyPurchaseContract: function() {
		this._cancelCopyPurchaseContract_();
	}
	
	/**
	 * On-Click handler for button btnCancelSelected
	 */
	,onBtnCancelSelected: function() {
		this.fnCancelSelected();
	}
	
	/**
	 * On-Click handler for button btnCancelAll
	 */
	,onBtnCancelAll: function() {
		this.fnCancelAll();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRemarkWdw
	 */
	,onBtnSaveCloseRemarkWdw: function() {
		var successFn = function(dc) {
			this._getWindow_("wdwRemarks").close();
			this._getDc_("shipTo").doReloadPage();
			this._getDc_("history").doReloadPage();
			var dcShipTo = this._getDc_("shipTo"); 
								dcShipTo.setParamValue("remarks",""); 
								dcShipTo._volumeChanged_ = null
		};
		var o={
			name:"saveInHistory",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("shipTo").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnSaveCloseResetRemarkWdw
	 */
	,onBtnSaveCloseResetRemarkWdw: function() {
		var dc = this._getDc_("contract");
					var record = dc.getRecord();
					var resaleContractCode = record.get("resaleContract");
					if(!Ext.isEmpty(resaleContractCode)){
						Ext.Msg.show({
				       title : "Note",
				       msg : Main.translate("applicationMsg", "updateLinkedSaleContract_lbl").concat(resaleContractCode),
				       icon : Ext.MessageBox.QUESTION,
				       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
				       fn : function(btnId) {
				              if( btnId === "yes" ){
				                     this.resetYes();
		              			}
				       },
				       scope : this
						});	
					} else {
						this.resetYes();
					}
	}
	
	/**
	 * On-Click handler for button btnCancelResetRemarkWdw
	 */
	,onBtnCancelResetRemarkWdw: function() {
		this._getWindow_("wdwRemarkReset").close();
		var dc = this._getDc_("contract"); 
							dc.setParamValue("remarks",""); 
	}
	
	/**
	 * On-Click handler for button btnCancelRemarkWdw
	 */
	,onBtnCancelRemarkWdw: function() {
		this._getWindow_("wdwRemarks").close();
	}
	
	/**
	 * On-Click handler for button btnAssignPriceComponents
	 */
	,onBtnAssignPriceComponents: function() {
		this._assignPriceComponents_();
	}
	
	/**
	 * On-Click handler for button btnCancelAssignPriceComponents
	 */
	,onBtnCancelAssignPriceComponents: function() {
		this._cancelAssignPriceComponents_();
	}
	
	/**
	 * On-Click handler for button btnGenerateExternalSaleContract
	 */
	,onBtnGenerateExternalSaleContract: function() {
		var successFn = function(dc,response) {
			this._showSaleContractDetails_(dc,response)
		};
		var o={
			name:"generateResellerContract",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("contract").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnGenerateInternalSaleContract
	 */
	,onBtnGenerateInternalSaleContract: function() {
		var successFn = function(dc,response) {
			this._showSaleContractDetails_(dc,response,true);
			this._getDc_("contract").doReloadPage();
		};
		var o={
			name:"generateInternalResellContract",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("contract").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnOpenCustomerWdw
	 */
	,onBtnOpenCustomerWdw: function() {
		this._getWindow_("wdwGenarateSaleContract").show();
	}
	
	/**
	 * On-Click handler for button btnNewRestriction
	 */
	,onBtnNewRestriction: function() {
		this._getDc_("contractPriceRestr").doNew();
	}
	
	/**
	 * On-Click handler for button btnSaveRestriction
	 */
	,onBtnSaveRestriction: function() {
		this._getDc_("contractPriceRestr").doSave();
	}
	
	/**
	 * On-Click handler for button btnCancelRestriction
	 */
	,onBtnCancelRestriction: function() {
		this._getDc_("contractPriceRestr").doCancel();
	}
	
	/**
	 * On-Click handler for button btnDeleteRestriction
	 */
	,onBtnDeleteRestriction: function() {
		this._getDc_("contractPriceRestr").doDelete();
	}
	
	/**
	 * On-Click handler for button btnApprove
	 */
	,onBtnApprove: function() {
		this.openApproveWindow();
	}
	
	/**
	 * On-Click handler for button btnApproveList
	 */
	,onBtnApproveList: function() {
		this.openApproveWindow();
	}
	
	/**
	 * On-Click handler for button btnReject
	 */
	,onBtnReject: function() {
		this.openRejectWindow();
	}
	
	/**
	 * On-Click handler for button btnRejectList
	 */
	,onBtnRejectList: function() {
		this.openRejectWindow();
	}
	
	/**
	 * On-Click handler for button btnSubmitForApproval
	 */
	,onBtnSubmitForApproval: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnSubmitForApprovalList
	 */
	,onBtnSubmitForApprovalList: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApprovalWdw
	 */
	,onBtnSaveCloseApprovalWdw: function() {
		this.submitForApprovalRpc()
	}
	
	/**
	 * On-Click handler for button btnCancelApprovalWdw
	 */
	,onBtnCancelApprovalWdw: function() {
		this._getWindow_("wdwApprovalNote").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApproveWdw
	 */
	,onBtnSaveCloseApproveWdw: function() {
		this.approveRpc()
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRejectWdw
	 */
	,onBtnSaveCloseRejectWdw: function() {
		this.rejectRpc()
	}
	
	/**
	 * On-Click handler for button btnCancelApproveWdw
	 */
	,onBtnCancelApproveWdw: function() {
		this._getWindow_("wdwApproveNote").close();
		this._getDc_("contract").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCancelRejectWdw
	 */
	,onBtnCancelRejectWdw: function() {
		this._getWindow_("wdwRejectNote").close();
		this._getDc_("contract").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnEditPriceComponent
	 */
	,onBtnEditPriceComponent: function() {
		this._getDc_("contractPriceCmpnClassic").doEditIn()
	}
	
	/**
	 * On-Click handler for button btnDeleteResale
	 */
	,onBtnDeleteResale: function() {
		
						Ext.Msg.show({
				       title : "Note",
				       msg : Main.translate("applicationMsg", "deleteInternalResaleContractConfirmation_lbl"),
				       icon : Ext.MessageBox.QUESTION,
				       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
				       fn : function(btnId) {
				              if( btnId === "yes" ){
				                     this.deleteResaleContract();
		              			}
				       },
				       scope : this
						});				
	}
	
	,assignPrice: function() {	
		this._showAsgnWindow_("atraxo.cmm.ui.extjs.asgn.PriceCtgryAssign_Asgn$Ui" ,{dc: "contractPriceCtgry", objectIdField: "id",leftAdvFilter: [{"fieldName":"idContract","value1":this._getDc_("contractPriceCtgry").getRecord().get("contract"),"operation":"="},{"fieldName":"id","value1":this._getDc_("contractPriceCtgry").getRecord().get("id"),"operation":"<>"}],rightAdvFilter: [{"fieldName":"idContract","value1":this._getDc_("contractPriceCtgry").getRecord().get("contract"),"operation":"="},{"fieldName":"id","value1":this._getDc_("contractPriceCtgry").getRecord().get("id"),"operation":"<>"}]
		,listeners: {close: {scope: this, fn: function() { this._getDc_("contractPriceCtgry").doReloadRecord();
		}} }});
	}
	
	,deleteResaleContract: function() {	
		var successFn = function() {
			this._getDc_("contract").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"deleteResaleContract",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("contract").doRpcData(o);
	}
	
	,onShowWdw: function() {	
		this._getWindow_("wdwTemp").show();;
		this._getDc_("template").doQuery();
	}
	
	,showWdwPricing: function() {	
		this._getWindow_("wdwPricing").show();
	}
	
	,onWdwTempClose: function() {	
		this._getDc_("contract").doCancel();
	}
	
	,setActiveYes: function() {	
		var successFn = function() {
			this._getDc_("contract").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"setActive",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("contract").doRpcData(o);
	}
	
	,updateStatus: function() {	
		var successFn = function() {
			this._getDc_("contract").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"updateStatus",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("contract").doRpcData(o);
	}
	
	,resetYes: function() {	
		var successFn = function() {
			this._getDc_("contract").doReloadPage();
			this._getWindow_("wdwRemarkReset").close();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"resetStatus",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("contract").doRpcData(o);
	}
	
	,priceCtgrySetDefault: function() {	
		var successFn = function(dc) {
			
									var dcc = this._getDc_("contractPriceCtgry");
									var isDefault = dcc.params.get("isDefault");
									if(isDefault){
										Ext.Msg.show({
									       title : "Warning",
									       msg: Main.translate("applicationMsg", "priceCatWarning__lbl"),
									       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
									       fn : function(btnId) {
									              if( btnId === "yes" ){
														dcc.params.set("isDefault",false);
									                    this.setAndSaveNewDefaultPriceCtgry();
							              		}
									       },
									       scope : this
										});
									}else{
										this.setAndSaveNewDefaultPriceCtgry();
									}
		};
		var o={
			name:"checkDefault",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("contractPriceCtgry").doRpcData(o);
	}
	
	,setAndSaveNewDefaultPriceCtgry: function() {	
		var successFn = function() {
			
								var cpc = this._getDc_("contractPriceCtgry");
								cpc.doSave();
		};
		var o={
			name:"setDefault",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("contractPriceCtgry").doRpcData(o);
	}
	
	,_whenBackFronPriceUpdate_: function() {
		
						var d = this._getDc_("contractPriceCtgry");
						d.canDoDelete = true;
						d.doReloadRecord();
	}
	
	,_when_called_from_dshboard_: function(id) {
		
						var dc = this._getDc_("contract");
						dc.doCancel();
						dc.doClearAllFilters();
						dc.setFilterValue("id", id);				
						dc.doQuery({showEdit: true}); 
	}
	
	,_checkPercent_: function() {
		
						var dc = this._getDc_("contractPriceCtgry");
						var r = dc.getRecord();
						var result = true;
						if (r) {
							var priceCtgryName = r.get("priceCtgryName");
							if (priceCtgryName === __CMM_TYPE__.PricingType._INDEX_) {
								result = false;
							}
						}
						return result;
	}
	
	,_markSelectedPriceCategories_: function() {
		
		                
		                var view = this._get_("priceCtgryAssignList");
		                var form = this._get_("priceCtgryEdit");
		                var percentageOf = form._get_("percentageOf");
		                var win = this._getWindow_("priceCategoryAsgnWdw");
		 
		                var f = function() {
		 
		                    if (win._isInEditForm_ === true) {   
		                        view.store.on("load", function(s) {   
		                            if (!Ext.isEmpty(percentageOf.getRawValue())) { 
		                                var percentageOfArray = percentageOf.getRawValue().split(",");
		                                s.each(function(r) {
		                                    if (percentageOfArray.indexOf(r.get("name")) !== -1 ) {
		                                        view.getSelectionModel().select(s.indexOf(r), true);
		                                    }
		 
		                                }, this);
		                            }
		                        }, this);
		                    }
							else {
								view.getSelectionModel().deselectAll();
							}
		 
		                }
		 
		                Ext.defer(f, 250, this);
	}
	
	,_assignPriceComponents_: function() {
		
		 
		                var grid = this._get_("priceCtgryAssignList");
		                var priceCatDc = this._getDc_("contractPriceCtgry");
		                var pricingBase = this._getDc_("pricingBase");
		                var s = grid.getSelectionModel().getSelection();
		                var win = this._getWindow_("priceCategoryAsgnWdw");
		                var r = pricingBase.getRecord();
		                var priceCatRecord = priceCatDc.getRecord();
		                var selected = [];
		                var selectedNames = [];
		 
		                Ext.each(s, function (item) {
		                  selectedNames.push(item.data.name);
		                  selected.push({id:item.data.id});
		                });
		 
		                if (r) {
		                    r.set("priceCategoryIdsJson",JSON.stringify(selected));
		                    r.set("priceComponents",selectedNames.join(","));
		                }
		                if (priceCatRecord && win._isInEditForm_ === true) {
		                    priceCatRecord.set("priceCategoryIdsJson",JSON.stringify(selected));
		                    priceCatRecord.set("percentageOf",selectedNames.join(","));
		                }
		 
		                win.close();
	}
	
	,_cancelAssignPriceComponents_: function() {
		
		 
		                var grid = this._get_("priceCtgryAssignList");
		                var dc = grid._controller_;
		                var win = this._getWindow_("priceCategoryAsgnWdw");
		                dc.doCancel();
		                win.close();
	}
	
	,_excludeComposite_: function(list) {
		
		                var s = list.store;
		                s.filterBy(function (record) {
		                    if (record.get("priceCtgryPricePer") !== __FMBAS_TYPE__.PriceInd._COMPOSITE_ && record.get("priceCtgryPricePer") !== __FMBAS_TYPE__.PriceInd._EVENT_) {
								return record;
							}
		                });
	}
	
	,_showSaleContractDetails_: function(dc,response,internalResale) {
		
		
					var bundle = "atraxo.mod.cmm";
					var frame = "atraxo.cmm.ui.extjs.frame.ContractCustomer_Ui";
					var generateWindow = this._getWindow_("wdwGenarateSaleContract");
					var mainPanel;
		
					if (getApplication().getFrameInstance(frame)) {
						mainPanel = getApplication().getFrameInstance(frame)._get_("main");
						mainPanel.hide();
					}
		
					getApplication().showFrame(frame,{
						url:Main.buildUiPath(bundle, frame, false),
						params: {
							saleContractId: this._getDc_("contract").getParamValue("generatedContractId"),
							saleContractCode: this._getDc_("contract").getParamValue("generatedContractCode"),
							internalResale: !Ext.isEmpty(internalResale) ? internalResale : null,
							// TO DO: check if user is allowed to view the contract ,userIsNotAllowedToView: 
						},
						callback: function (params) {
							this._after_contract_was_generated_(params)
						},
						frameOpenParams: {
							disableInitialQuery: true,
							showWorking: true
						}
					});
					generateWindow.close();
	}
	
	,_cancelContract_: function(win) {
		
					var dc = this._getDc_("contract");
					win.down("form")._isVisible_ = false;
					dc.doCancel();
	}
	
	,_onWdwwClose_: function(win) {
		
					var f = function(win) {
						var dc = this._getDc_("pricingBase");
						dc.doCancel(); 
						win.down("form")._isVisible_ = false; 
						dc._changeState_ = 0;
					};
					Ext.defer(f, 300, this, [win]);
	}
	
	,_continueCopyPurchaseContract_: function() {
		
						var dc = this._getDc_("contract");
						var successFn = function(dc, response) {
							var dcc = this._getDc_("contract");
							var params = JSON.parse(response.responseText).params; 
							var generatedContractId = params.generatedContractId;
							this._getWindow_("wdwCopyPurchaseContract").close();
							dcc.doCancel({reloadPage: true, generatedContractId: generatedContractId});
						}
						var o={
							name:"copyBuyContract",
							modal:true,
							callbacks:{
								successFn: successFn,
								successScope: this
							}
						};
						dc.doRpcData(o);
	}
	
	,_continueNewPurchaseContract_: function() {
		
					var doWork = function(){
		                  var btn = this._get_("btnContinueNewPurchaseContract");
						  if ( btn.isDisabled() ){
							// function is executed with a delay => check again the button status
							return;
						  }
		
						  var tempList = this._get_("TempList");
		       
		                  var selection = tempList.getSelectionModel().getSelection();
		                  var contractType = selection[0].data.contractType;
		                  var deliverySubType = selection[0].data.deliverySubtype;
		                  var scope = selection[0].data.scope;
		                               
		                  var dc = this._getDc_("contract");
		                  var record = dc.getRecord();
		
		                  var counterPartyId = record.get("resBuyerId");
		                  var counterPartyName = record.get("resBuyerName");
		
						  var supplierId = record.get("supplierId");
						  var supplierCode = record.get("supplierCode");
		
		                  var params = {
		                  		type: contractType,
		                        subType: deliverySubType,
		                        scope: scope,
		                        counterPartyId: counterPartyId,
		                        counterPartyName: counterPartyName,
								supplierId: supplierId,
								supplierCode: supplierCode
		                  }
		
		                  this._when_do_new_form_list(params);
		                  this._getWindow_("wdwNewPurchaseContract").close();
					};
					// execute function with a delay to be sure that the events on the customer LOV is executed
					Ext.defer(doWork, 100, this);
	}
	
	,_cancelNewPurchaseContract_: function() {
		
						this._getDc_("contract").doCancel();
					    this._getWindow_("wdwNewPurchaseContract").close();
	}
	
	,_cancelCopyPurchaseContract_: function() {
		
						this._getDc_("contract").doCancel();
						this._getWindow_("wdwCopyPurchaseContract").close();
	}
	
	,_cancelPriceFix_: function() {
		
						var wdw = this._getWindow_("priceFixWdw");
						wdw.close();
	}
	
	,_cancelCompositePrice_: function() {
		
		                var wdw = this._getWindow_("compositePriceWdw");
		                wdw.close();
	}
	
	,_cancelIndexPrice_: function() {
		
						var wdw = this._getWindow_("wdwPricing");
						wdw.close();
	}
	
	,_cancelEditIndexPrice_: function() {
		
						var wdw = this._getWindow_("wdwPricingEditFormula");
						wdw.close();				
	}
	
	,_editFormulaPrice_: function() {
		
						
						var pricingBase = this._getDc_("pricingBase");
						var pbStore = pricingBase.store;
		
						var contractPriceCtgry = this._getDc_("contractPriceCtgry");
						var pcRecord = contractPriceCtgry.getRecord();
						var pbRecord = pricingBase.getRecord();
						var pricingBaseId ="";
		
						if (pcRecord && pbRecord) {
							pricingBase._changeState_ = 2;
							pricingBaseId = pcRecord.get("pricingBaseId");
							pbStore.load({
								scope: this,
								callback: function() {
									var index = pbStore.find("id", pricingBaseId);
									
									pricingBase.setRecord(index);
									var window = this._getWindow_("wdwPricingEditFormula");
				                    window.show(undefined, function() {
										pricingBase.fireEvent("itsTimeToCalculateDensity");
									}, this);
								}
							});
							
							
						}		
		
						
	}
	
	,_disableDeleteIfIndex_: function() {
		
		
		
						var dc = this._getDc_("contractPriceCtgry");
						var rec = dc.getRecord();
		
						if (rec) {
							var priceCtgryName = rec.get("priceCtgryName");
							if (priceCtgryName === "Index") {
								dc.canDoDelete = false;
							} else {
								dc.canDoDelete = true;
							}
						}
						
						dc.doReloadPage();
		
	}
	
	,_checkPriceCtgryName_: function() {
		
		
						var dc = this._getDc_("contractPriceCtgry");
						var rec = dc.getRecord();
						var result = false;
		
						if (rec) {
							var priceCtgryName = rec.get("priceCtgryName");
							var priceCtgryPricePer = rec.get("priceCtgryPricePer");
							if (priceCtgryName !== "Index" && priceCtgryPricePer !== "Composite") {
								result = true;
							}
							
						}
						return result;
	}
	
	,_checkPriceCtgryPricePer_: function() {
		
		
						var dc = this._getDc_("contractPriceCtgry");
						var rec = dc.getRecord();
						if (rec) {
							var priceCtgryPricePer = rec.get("priceCtgryPricePer");
							if (priceCtgryPricePer !== "Composite") {
								return true;
							}					
						}
						return false;
	}
	
	,_checkIfContractAwaitingApproval_: function() {
		
		
						var dc = this._getDc_("contractPriceCtgry");
						var rec = dc.getRecord();
						var result = true;
						if (rec) {
							var bidApprovalStatus = rec.get("bidApprovalStatus");
							if (bidApprovalStatus === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_) {
								result = false;
							}					
						}
						return result;
	}
	
	,_checkIfFormulaPrice_: function() {
		
		
						var dc = this._getDc_("contractPriceCtgry");
						var rec = dc.getRecord();
						var result = true;
		
						if (rec) {
							var priceCtgryName = rec.get("priceCtgryName");
							if (priceCtgryName !== "Index") {
								result = false;
							}
						}
						return result;
	}
	
	,_setDefaultPb: function() {
		
		
						var dc = this._getDc_("pricingBase");
						dc.doCancel(); // SONE-2144
						dc.doNew({setIndexPrice: true});
						
	}
	
	,_setDefaultFix: function() {
		
		
						var dc = this._getDc_("pricingBase");
						dc._step_ = 1;
						dc.doCancel(); // SONE-2144
						dc.doNew({setFixPrice: true});
		
	}
	
	,_setDefaultDFT: function() {
		
		
						var dc = this._getDc_("pricingBase");
						dc._step_ = 2;
						dc.doCancel(); // SONE-2144
						dc.doNew({setDftPrice: true});
	}
	
	,_checkPricingBaseId_: function() {
		
		
						var dc = this._getDc_("contract");
						var rec = dc.getRecord();
						var result = false;
		
						if (rec) {
							var pricingBaseId = rec.get("pricingBaseId");
							if (Ext.isEmpty(pricingBaseId)) {
								result = true;
							}
						}
						return result;
	}
	
	,_setDefaultComposite: function() {
		
		 
		                var dc = this._getDc_("pricingBase");
		                dc._step_ = 3;
		                dc.doCancel(); // SONE-2144
		                dc.doNew({setCompositePrice: true});
		 
	}
	
	,_filterEditAfterQuery_: function(params,callBackFn,ctx) {
		
						if( Ext.Ajax.isLoading() ){
							Ext.defer(this._filterEditAfterQuery_,250,this,[params, callBackFn, ctx]);
						} else {
							callBackFn(ctx);
						}
	}
	
	,_when_called_from_sale_contracts_: function(params) {
		
						this._filterEditAfterQuery_(params,function(ctx) {
							var dc = ctx._getDc_("contract");
							dc.doClearAllFilters();
							dc.setFilterValue("code", params.purchaseContractCode);				
							dc.doQuery({showFilteredEdit: true});  
						}, this); 
	}
	
	,_when_called_for_details_: function(params) {
		
						this._filterEditAfterQuery_(params,function(ctx) {
							var dc = ctx._getDc_("contract");
							dc.doClearAllFilters();
							dc.setFilterValue("code", params.contractCode);				
							dc.doQuery({showFilteredEdit: true});
						}, this); 
						
	}
	
	,_when_called_from_appMenu_: function() {
		
						var dc = this._getDc_("contract");
						dc.doCancel();
						dc.doQuery({doNew: true});
	}
	
	,_when_do_new_: function(params) {
		
		
							var dc = this._getDc_("contract");
			
							// Dan: SONE-1632
							var detailsTab = this._get_("detailsTab");
							var paymentTerms = this._get_("paymentTerms");
							detailsTab.setActiveTab(paymentTerms);
			
							this._showStackedViewElement_("main", "canvas2", dc.fireEvent("adjustDatePicker"));
							this.setContractTemplate(params);
	}
	
	,_when_do_new_form_list: function(params) {
		
		
						var dc = this._getDc_("contract");
		                var detailsTab = this._get_("detailsTab");
		                var paymentTerms = this._get_("paymentTerms");
		                detailsTab.setActiveTab(paymentTerms);
		                this._showStackedViewElement_("main", "canvas2", dc.fireEvent("adjustDatePicker"));
						this.setSecondContractTemplate(params);
	}
	
	,setContractTemplate: function(params) {
		
		
						var dc = this._getDc_("contract");
						dc.doQuery();
						dc.on("afterDoQuerySuccess", function() {
		
							// Dan: SONE-2114:  waith for the doQuery() action to finish than call doNew()
		
							dc.doNew({newFromCustomers: true});
							var record = dc.getRecord();
		
							if (record) {
								record.beginEdit();
								record.set("type", params.type);
								record.set("subType", params.subType);
								record.set("scope", params.scope);
								record.set("supplierId", params.supplierId);
								record.set("supplierCode", params.supplierCode);
								record.set("resBuyerId", params.counterPartyId);
								record.set("resBuyerName", params.counterPartyName);
								record.set("holderCode",parent._ACTIVESUBSIDIARY_.code);
								record.endEdit();
							}
		
						}, this);
	}
	
	,setSecondContractTemplate: function(params) {
		
							var dc = this._getDc_("contract");                                       
		                    var record = dc.getRecord();
		                    if (record) {
									record.beginEdit();
		                            record.set("type", params.type);
		                            record.set("subType", params.subType);
		                            record.set("scope", params.scope);
		
		                            record.set("customerId", params.customerId);
		                            record.set("customerCode", params.customerCode);
		
									record.set("resBuyerId", params.counterPartyId);
		                            record.set("resBuyerName", params.counterPartyName);
									record.set("holderCode",parent._ACTIVESUBSIDIARY_.code);
		                            record.endEdit();
		                     }
		
	}
	
	,setContractValue: function() {
		
							var header = this._get_("Header");
							var PbEdit = this._get_("PbEdit");
		
							var headerRecord =  header._controller_.getRecord();
							var headerCode = headerRecord.get("code");
							
							var pbRecord = PbEdit._controller_.getRecord();
							
							pbRecord.set("contractCode",headerCode);
							var pricingBaseDc = this._getDc_("pricingBase");
							var recNumber = pricingBaseDc.store.totalCount;
							if(recNumber === 0){
								pricingBaseDc.getRecord().set("validFrom", this._getDc_("contract").getRecord().get("validFrom"));
							}
							pricingBaseDc.getRecord().set("validTo", this._getDc_("contract").getRecord().get("validTo"));
							if(pbRecord.get("pricingMethod") === "Index"){
								PbEdit.fireEvent("itsTimeToCalculateDensity", PbEdit);
							}
	}
	
	,setPriceFields: function() {
		
		
							// TODO set price fields
							var pricingBase = this._getDc_("pricingBase");
							var contractRecord = this._getDc_("contract").getRecord();
							var priceRecord = pricingBase.getRecord();
		
							if (contractRecord && priceRecord) {
								var financialSourceId = contractRecord.get("financialSourceId");
								var financialSourceCode = contractRecord.get("financialSourceCode");
								var avgMethodId = contractRecord.get("avgMethodId");
								var avgMethodName = contractRecord.get("avgMethodName");
								var period = contractRecord.get("exchangeRateOffset");
								var vat = contractRecord.get("vat");
								var pcCurr = contractRecord.get("settCurrId");
								var pcCurrCode = contractRecord.get("settCurrCode");
								var pcUnit = contractRecord.get("settUnitId");
								var pcUnitCode = contractRecord.get("settUnitCode");
								var validFrom = contractRecord.get("validFrom");
								var validTo = contractRecord.get("validTo");
		
								priceRecord.beginEdit();
								priceRecord.set("financialSourceId", financialSourceId);
								priceRecord.set("financialSourceCode", financialSourceCode);
								priceRecord.set("avgMethodIndicatorName", avgMethodName);
								priceRecord.set("avgMthdId", avgMethodId);
								priceRecord.set("exchangeRateOffset", period);
								priceRecord.set("vat", vat);
								priceRecord.set("pcCurr", pcCurr);
								priceRecord.set("pcCurrCode", pcCurrCode);
								priceRecord.set("pcUnit", pcUnit);
								priceRecord.set("pcUnitCode", pcUnitCode);
								priceRecord.set("validFrom", validFrom);
								priceRecord.set("validTo", validTo);
								priceRecord.endEdit();
							}
		
	}
	
	,_setSubsidiary_: function() {
		
						
						var ds = "fmbas_Subsidiary_Ds";
						var dc = this._getDc_("contract");
						var r = dc.getRecord();
		
						Ext.Ajax.request({
			            	url: Main.dsAPI(ds, "json").read,
			            	method: "POST",
			            	scope: this,
			            	success: function(response) {
		
								var data = JSON.parse(response.responseText).data;
								var len = data.length;
								if (len === 1 && r) {
									var id = data[0].id;
									var code = data[0].code;
									r.set("holderId", id);
									r.set("holderCode", code);
								}
		
							}
						});
		
	}
	
	,_afterCopyContract_: function(dc,generatedContractId) {
		
						var detailsTab = this._get_("detailsTab");
						var paymentTerms = this._get_("paymentTerms");
						detailsTab.setActiveTab(paymentTerms);
						dc.doClearAllFilters();
						dc.setFilterValue("id", generatedContractId);
						dc.doQuery();
	}
	
	,_applyRestrictionState_: function() {
		
		
						var contractPriceCtgry = this._getDc_("contractPriceCtgry");
						var r = contractPriceCtgry.getRecord();
						var result = false;
		
						if (r) {
							var restriction = r.get("restriction");
							if (restriction === true) {
								result	= true;
							}
						}
		
						return result;
	}
	
	,_afterLinkElementsPhase2_: function() {
		
						var contract = this._getDc_("contract");
						this._onContractRecordChange_(contract);
	}
	
	,_makeContractReadOnlyOnAwaitingApproval_: function(contract) {
		
						var r = contract.getRecord();
						var contractPriceCtgry = this._getDc_("contractPriceCtgry");
						var contractPriceCmpn = this._getDc_("contractPriceCmpn");
						if(r) {
							var approvalStatus = r.get("approvalStatus");
			
							var setChildrenState = function(state) {
								var childrenDcs = contract.children;
								for (var i = 0; i<childrenDcs.length; i++) {
									if (childrenDcs[i]._instanceKey_ !== "attachment") {
										childrenDcs[i].readOnly = state;
									}							
								}
							}
							
							if (approvalStatus === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_) {
									contract.readOnly = true;
									contractPriceCtgry.readOnly=true;
									contractPriceCmpn.readOnly=true;
									setChildrenState(true);
									contract.canDoNew = true;
							}
							else {
									contract.readOnly = false;
									contractPriceCtgry.readOnly=false;
									contractPriceCmpn.readOnly=false;
									setChildrenState(false);
							}
						}
	}
	
	,_onContractRecordChange_: function(contract) {
		
		                
						this._makeContractReadOnlyOnAwaitingApproval_ (contract);
						var r = contract.getRecord();
						var view = this._get_("Header");
						if( r && view ) {
							var customer = view._get_("supplier");
							if(customer && customer.inputEl){
								var status = r.get("status");
			
								// Start SONE-3918
								
								var customerName = r.get("supplierName");
								var toolTip = Ext.getCmp(this.customerCodeTooltipId);
			
								if (toolTip) {
									toolTip.destroy();
								}
			
								customer.inputEl.dom.removeAttribute("data-qtip");
			
								Ext.create("Ext.tip.ToolTip", {
									id: this.customerCodeTooltipId,
							        target: customer.getEl(),
							        html: customerName
							    });
			
								// End SONE-3918
			
								if (status === __CMM_TYPE__.ContractStatus._EFFECTIVE_ || status === __CMM_TYPE__.ContractStatus._EXPIRED_) {
									contract.canDoDelete = false;
									contract.updateDcState();	
								}
								else {
									contract.canDoDelete = true;
									contract.updateDcState();	
								}
							}
						}
	}
	
	,_setMainPriceCategory_: function() {
		
							var dc = this._getDc_("contractPriceCtgryAssign");
							var dc2 = this._getDc_("contractPriceCtgry");
							var r = dc2.getRecord();
							if (r) {			
								dc.setParamValue("mainPriceCategoryId", r.get("id"));
							}
	}
	
	,Cont: function() {
		
						var tempList = this._get_("TempList");
		
						var selection = tempList.getSelectionModel().getSelection();
						var contractType = selection[0].data.contractType;
						var deliverySubType = selection[0].data.deliverySubtype;
						var scope = selection[0].data.scope;
		
						var contractDc = this._getDc_("contract");
						var record = contractDc.getRecord();
		
						if (record) {
							record.beginEdit();
							record.set("type", contractType);
							record.set("subType", deliverySubType);
							record.set("scope", scope);
							record.set("holderCode",parent._ACTIVESUBSIDIARY_.code);
							record.endEdit();
						}				
		
						contractDc.doEditIn(); 
	}
	
	,canSubmitForApproval: function(dc) {
		
						var r = dc.getRecord();
						if (r) {
							return r.data.status === __CMM_TYPE__.BidStatus._DRAFT_ && 
								!(r.data.approvalStatus === __CMM_TYPE__.BidApprovalStatus._APPROVED_ || r.data.approvalStatus === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_);
						}
						return false;
	}
	
	,canApprove: function(dc) {
		
						var r = dc.getRecord();
						if (r) {
							return r.data.status === __CMM_TYPE__.BidStatus._DRAFT_ && r.data.approvalStatus === __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_ && r.data.canBeCompleted === true ;
						}
						return false;
	}
	
	,openApproveWindow: function() {
		
						var label = "Please provide an approval reason.";
						var title = "Approve";
						this.setupSubmitForApprovalWindow("wdwApproveNote","approveNote","btnSaveCloseApproveWdw","btnCancelApproveWdw" , label , title);				
	}
	
	,openRejectWindow: function() {
		
						var label = "Please provide a reason for rejection.";
						var title = "Reject";
						this.setupSubmitForApprovalWindow("wdwRejectNote","rejectNote","btnSaveCloseRejectWdw","btnCancelRejectWdw" , label , title);				
	}
	
	,openSubmitForApprovalWindow: function() {
		
						var label = "Add your note to be sent to your request to the approvers";
						var title = "Submit for approval";
						this.setupSubmitForApprovalWindow("wdwApprovalNote","approvalNote","btnSaveCloseApprovalWdw","btnCancelApprovalWdw" , label , title);				
	}
	
	,setupSubmitForApprovalWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var approvalNoteField = this._get_(theForm)._get_("approvalNote");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(true);
						
						approvalNoteField.fieldLabel = label;
						approvalNoteField.setValue("");
						window.title = title;
						
						approvalNoteField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
								cancelButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
								cancelButton.setDisabled(true);
						    }
						});
						
						if ("wdwApprovalNote" === theWindow) {
							var view = this._get_("documentsAssignList");
							view.store.load();
						}
						window.show();
	}
	
	,submitForApprovalRpc: function() {
		
						var callbackFn = function(dc,response) {
							var responseData = Ext.util.JSON.decode(response.responseText);
							if (responseData && responseData.params) {
								var responseDescription = responseData.params.submitForApprovalDescription;							
								var responseResult = responseData.params.submitForApprovalResult;
								
								//close window
								this._getWindow_("wdwApprovalNote").close();
								
								//display results (only if error)
								if (responseResult === false) {
									Main.error(responseDescription);
								}
								//reload page
								dc.doReloadPage();
							}
						};
		
						var callbackFailFn = function(dc,response) {
									Main.rpcFailure(response);
									//reload page
									dc.doReloadPage();
						};
		
						this._sendAttachments_();
		                var dc = this._getDc_("contract");
		                var t = {
					        name: "submitForApproval",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFailFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcData(t);
	}
	
	,_sendAttachments_: function() {
		
						var list = this._get_("documentsAssignList");
						var view = list.getView();
						var selectedAttachments = [];
						var dc = this._getDc_("contract");
						if (view.getSelectionModel().hasSelection()) {
						   var rows = view.getSelectionModel().getSelection();
						   Ext.each(rows, function(row) {
								selectedAttachments.push(row.data.id);
						   }, this);
						}
						dc.setParamValue("selectedAttachments",JSON.stringify(selectedAttachments));
	}
	
	,saveNewPriceCategory: function() {
						
						this.savePriceBases("New");
	}
	
	,saveClosePriceCategory: function() {
						
						this.savePriceBases("Close");
	}
	
	,saveEditPriceCategory: function() {
						
						this.savePriceBases("Edit");
	}
	
	,saveEditCompositePrice: function() {
		               
		                this.savePriceBases("EditComposite");
		 
	}
	
	,saveClosePriceBase: function() {
		
						var pb = this._getDc_("pricingBase");
						pb.doSave({doCloseNewIndexPriceWindowAfterSave: true });
	}
	
	,saveCloseCompositePrice: function() {
		
		                var pb = this._getDc_("pricingBase");
		                pb.doSave({doCloseCompositePriceAfterSave: true });
	}
	
	,saveClosePriceEditFormulaBase: function() {
		
						var pb = this._getDc_("pricingBase");
						pb.doSave({doCloseEditFormulaWindowAfterSave: true });
	}
	
	,saveClosePriceComp: function() {
		
						var pb = this._getDc_("contractPriceCmpnClassic");
						pb.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveClosePriceComp1: function() {
		
						var pb = this._getDc_("contractPriceCmpn");
						pb.doSave({doCloseNewWindowAfterSavee: true });
	}
	
	,showPriceIndexWdw: function(viewName,fieldToGet,valueToSet,fieldToReturn,fielToReturnValueIn) {
		
						var window = this._getWindow_("wdwPricing");
						window.show();
						this._setDefaultLovValue_(viewName, fieldToGet, valueToSet, fieldToReturn, fielToReturnValueIn);
	}
	
	,showPriceFixWdw: function(viewName,fieldToGet,valueToSet,fieldToReturn,fielToReturnValueIn,opt) {
		
						var window = this._getWindow_("priceFixWdw");
						window.show();
		
						if (!Ext.isEmpty(valueToSet)) {
							this._setDefaultLovValue_(viewName, fieldToGet, valueToSet, fieldToReturn, fielToReturnValueIn, opt);
						}			
	}
	
	,showCompositePriceWdw: function() {
		
		                var window = this._getWindow_("compositePriceWdw");
		                window.show();
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/SupplierContracts.html";
					Main.downloadFile(url, false);
	}
	
	,showWdwPriceComp: function() {
		
					var win = this._getWindow_("priceCompWdw");
					win.show(undefined, function() {
						var dc = this._getDc_("contractPriceCmpnClassic");
						var rec = dc.getRecord();
						if (rec) {
							var field = rec.get("contractPriceCatName");
							win.setTitle("Set a new price for "+field);
						}
						
					},this);
	}
	
	,showWdwPriceComp1: function() {
		
					var win = this._getWindow_("priceCompWdw");
					win.show(undefined, function() {
						var dc = this._getDc_("contractPriceCmpn");
						var rec = dc.getRecord();
						if (rec) {
							var field = rec.get("contractPriceCatName");
							win.setTitle("Change price for "+field);
						}
					},this);
	}
	
	,onWdwPricingShow: function() {
		
						this._get_("PbEdit")._enableDisableFields_(null);
	}
	
	,onPriceCompWdwClose: function() {
		
						var dc = this._getDc_("contractPriceCmpnClassic");
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,getDensity: function(dc,formName) {
		
		
						var successFn = function() {	
							if (Ext.isEmpty(formName)){
								return;
							}			
							var view = this._get_(formName);
							var field = view._get_("conversionFactor");
							var editorForm = field._getFormInstance_();
							if( editorForm ){
								editorForm._validateInput_();
							}
						};
						var o={
							name:"changeUnit",
							showWorking : false,
							callbacks:{
								successFn: successFn,
								successScope: this,
								failureScope: this
							},
							modal:false
						};
						dc.doRpcData(o);
	}
	
	,deleteContractPriceCtgry: function() {
		
						this._getDc_("contractPriceCtgry").doDelete();
	}
	
	,deleteContractPriceComponent: function() {
		
						this._getDc_("contractPriceCmpnClassic").doDelete();
	}
	
	,deleteContractPricingBases: function() {
		
						this._getDc_("pricingBase").doDelete();
	}
	
	,_setDefaultLovValue_: function(viewName,fieldToGet,valueToSet,fieldToReturn,fielToReturnValueIn,opt) {
		
		 
		                // -------------- Function config --------------
		 
		                var dcView = this._get_(viewName);
		 
		                // -------------- End function config --------------
		 
		                var field = dcView._get_(fieldToGet);
		                var valueField = field.valueField;
		                var record = dcView._controller_.getRecord();
		 
		                var store = field.store;			
		
		                store.load({
		                    callback: function(rec) {
								var valueToReturn = "";
								if (fieldToReturn instanceof Array) {
									valueToReturn = [];
								}
		 
		                        for (var i=0 ; i < rec.length; i++) {
		                            var data = rec[i].data;
		                            for (var key in data) {
		                                if (data[valueField] == valueToSet) {
		
											if (fieldToReturn instanceof Array) {
												for (var x = 0; x<fieldToReturn.length; x++) {
													valueToReturn.push(data[fieldToReturn[x]]);
												}
											}
											else {
												valueToReturn = data[fieldToReturn];
											}
		                                    break;									
		                                }
		                            }
		                        }
		
		                        if (record) {
		                            record.beginEdit();
		 
		                            record.set(field.dataIndex, valueToSet);
									if (fielToReturnValueIn instanceof Array) {
										for (var z = 0; z<fielToReturnValueIn.length; z++) {
											record.set(fielToReturnValueIn[z], valueToReturn[z]);
										}
									}
									else {
										record.set(fielToReturnValueIn, valueToReturn);
									}
		                            
		                            if (opt) {
		                                for (var key in opt) {
		                                    record.set(key, opt[key]); 
		                                }
		                            }
		                            record.endEdit();
		                        }
		                    }
		                },this);
	}
	
	,saveContractPriceCtgry: function(dc) {
						
						if(dc.getRecord().get("defaultPriceCtgy")){
							this.priceCtgrySetDefault();
						} else {
							dc.doSave();
						}
	}
	
	,savePriceBases: function(command) {
			
						var dc = this._getDc_("pricingBase");
						var r = dc.getRecord();
						if (r) {
							if(r.get("defaultPriceCtgy")){
								this.priceBasesCPCSetDefault(command);
							} else {
								this.savePbWithParameter(command);
							}
						}
						
	}
	
	,priceBasesCPCSetDefault: function(command) {
		
						var successFn = function() {			
							this.pricingBaseSaveDefaultQueston (command);	
						};
						var o={
							name:"checkDefault",
							callbacks:{
								successFn: successFn,
								successScope: this
							},
							modal:true
						};
						this._getDc_("pricingBase").doRpcData(o);
	}
	
	,pricingBaseSaveDefaultQueston: function(command) {
		
						var dc = this._getDc_("pricingBase");
								var isDefault = dc.params.get("isDefault");
								if(isDefault){
									Ext.Msg.show({
								       title : "Warning",
		                               msg : Main.translate("applicationMsg", "pricingBaseSaveDefaultQueston__lbl"),
								       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
								       fn : function(btnId) {
								            if( btnId === "yes" ){
													dc.params.set("isDefault",false);
								                    this.savePbWithParameter(command);
						              		}
								       },
								       scope : this
									});
								}else{
									this.savePbWithParameter(command);
								}
	}
	
	,savePbWithParameter: function(command) {
		
						var dc = this._getDc_("pricingBase");
						if(command === "New"){
							dc.doSave({doNewAfterSave: true });
						} else if(command === "Close"){
							dc.doSave({doCloseNewWindowAfterSave: true });
						} else if( command === "Edit"){
							dc.doSave({doCloseEditAfterSave: true });
						} else if( command === "EditComposite"){
		                    dc.doSave({doCloseEditCompositeAfterSave: true });
		                }
	}
	
	,fnCancelSelected: function() {
		
						var dc = this._getDc_("shipTo");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAll: function() {
		
						var dc = this._getDc_("shipTo");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isCancelEnabled(dc);
	}
	
	,approveRpc: function() {
		
						var callbackFn = function(dc,response) {
							var responseData = Ext.util.JSON.decode(response.responseText);
							if (responseData && responseData.params) {
								//close window
								this._getWindow_("wdwApproveNote").close();
								
								//reload page
								dc.doReloadPage();
							}
						};
		
		                var dc = this._getDc_("contract");
		                var t = {
					        name: "approve",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcData(t);
	}
	
	,rejectRpc: function() {
		
						var callbackFn = function(dc,response) {
							var responseData = Ext.util.JSON.decode(response.responseText);
							if (responseData && responseData.params) {
								//close window
								this._getWindow_("wdwRejectNote").close();
								
								//reload page
								dc.doReloadPage();
							}
						};
		
		                var dc = this._getDc_("contract");
		                var t = {
					        name: "reject",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcData(t);
	}
	
	,useWorkflowForApproval: function() {
		 
						return _SYSTEMPARAMETERS_.syspurchasecontractapproval === "true";
	}
	
	,canActivateContract: function() {
		
						var dc = this._getDc_("contract");
						var rec = dc.getRecord();
						if (rec) {
							if ( rec.data.status !== __CMM_TYPE__.ContractStatus._DRAFT_ ) {
								return false;	
							}
						}
						else {
							return false;
						}
						return true;
	}
});
