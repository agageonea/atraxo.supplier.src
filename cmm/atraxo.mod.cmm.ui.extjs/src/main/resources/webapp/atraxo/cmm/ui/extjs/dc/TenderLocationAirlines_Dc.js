/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocationAirlines_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.TenderLocationAirlines_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.TenderLocationAirlines_Ds
});

/* ================= EDIT FORM: View ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocationAirlines_Dc$View", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderLocationAirlines_Dc$View",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"note", bind:"{d.notes}", dataIndex:"notes", noEdit:true , width:440, noLabel: true, height:100})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"table1", layout: {type:"table", columns:1}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["table1"])
		.addChildrenTo("table1", ["note"]);
	}
});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocationAirlines_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.cmm_TenderLocationAirlines_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"airline", dataIndex:"airlineCode", width:100, allowBlank: false, _enableFn_: this._enableIfStatus_, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_AirlinesNameLov_Lov", allowBlank:false, maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "airlineId"} ]}})
		.addNumberColumn({name:"volume", dataIndex:"volume", width:150, allowBlank: false, sysDec:"dec_unit", _enableFn_: this._enableIfStatus_, maxLength:19, align:"right" })
		.addLov({name:"unit", dataIndex:"unitCode", width:50, allowBlank: false, _enableFn_: this._enableIfStatus_, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_MeasurmentUnitsLov_Lov", allowBlank:false, maxLength:2,
				retFieldMapping: [{lovField:"id", dsField: "unitId"} ]}})
		.addComboColumn({name:"period", dataIndex:"volumePeriod", hidden:true, width:120, allowBlank: false, _enableFn_: this._enableIfStatus_, 
			editor:{xtype:"combo", mode: 'local', allowBlank:false, triggerAction:'all', forceSelection:true, store:[ __CMM_TYPE__.Period._CONTRACT_PERIOD_, __CMM_TYPE__.Period._MONTHLY_, __CMM_TYPE__.Period._YEARLY_]}})
		.addTextColumn({name:"biddingStatus", dataIndex:"biddingStatus", hidden:true, width:100, noEdit: true, maxLength:32})
		.addTextColumn({name:"notes", dataIndex:"notes", width:250, noEdit: true, maxLength:4000,  renderer:function(value,meta,record) {return this._injectAssignButton_(value,meta,record);}})
		.addBooleanColumn({name:"isValid", dataIndex:"isValid", noEdit: true})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_injectAssignButton_: function(value,meta,record) {
		
						var id = Ext.id();
						meta.tdCls += "sone-cell-with-button";
						var ctx = this;
		
						Ext.defer(function () {	
							
							if( document.getElementById(id)){
						        Ext.widget("button", {
						            renderTo: id,
						            glyph: "xf1e5@FontAwesome",		
									cls: "sone-grid-assign-btn",									
						            handler: function (b) {	
										if (!Ext.isEmpty(record.get("notes"))) {
											b.focus(true,100); 
											ctx.__asgnBtnHandler__(record.get("notes"));		
										}					
									}
						        });
							} 
					    }, 50, this);	
		
						return value+"<div class='sone-assign-button' id="+id+"></div>";
					    
	},
	
	__asgnBtnHandler__: function(notes) {
		
						var ctrl = this._controller_;
						var frame = ctrl.getFrame();
						frame._openNotesWindow_(notes);
	},
	
	_enableIfStatus_: function() {
		
						var dc 		= this._controller_;
						var record 	= dc.getRecord();
		
						var id 	= record.get("id");
						var status 	= record.get("tenderStatus");
						
						//enable on new or when tenderStatus = Draft
						return id == null || status == "Draft";
	}
});

/* ================= EDIT-GRID: FourthStepList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocationAirlines_Dc$FourthStepList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.cmm_TenderLocationAirlines_Dc$FourthStepList",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"airlineCode", dataIndex:"airlineCode", width:50, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_AirlinesNameLov_Lov", maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "airlineId"} ]},  flex:1})
		.addNumberColumn({name:"volume", dataIndex:"volume", sysDec:"dec_unit", maxLength:19, align:"right",  flex:1 })
		.addLov({name:"unitCode", dataIndex:"unitCode", width:50, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
				retFieldMapping: [{lovField:"id", dsField: "unitId"} ]},  flex:1})
		.addComboColumn({name:"period", dataIndex:"volumePeriod", width:70, 
			editor:{xtype:"combo", mode: 'local', triggerAction:'all', forceSelection:true, store:[ __CMM_TYPE__.Period._CONTRACT_PERIOD_, __CMM_TYPE__.Period._MONTHLY_, __CMM_TYPE__.Period._YEARLY_]},  flex:1})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
