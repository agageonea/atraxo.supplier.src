/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.Contract_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.Contract_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.Contract_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.cmm.ui.extjs.dc.Contract_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.cmm_Contract_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"locCode", dataIndex:"locCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "locId"} ],
					filterFieldMapping: [{lovField:"isAirport", value: "true"} ]}})
			.addCombo({ xtype:"combo", name:"type", dataIndex:"type", store:[ __CMM_TYPE__.ContractType._PRODUCT_, __CMM_TYPE__.ContractType._FUELING_SERVICE_, __CMM_TYPE__.ContractType._GENERAL_SERVICE_, __CMM_TYPE__.ContractType._INSPECTION_, __CMM_TYPE__.ContractType._STORAGE_, __CMM_TYPE__.ContractType._CSO_]})
			.addCombo({ xtype:"combo", name:"subType", dataIndex:"subType", store:[ __CMM_TYPE__.ContractSubType._NA_, __CMM_TYPE__.ContractSubType._INTO_PLANE_, __CMM_TYPE__.ContractSubType._EX_HYDRANT_, __CMM_TYPE__.ContractSubType._AIRLINE_SUPPLY_, __CMM_TYPE__.ContractSubType._INTO_STORAGE_, __CMM_TYPE__.ContractSubType._SERVICE_]})
			.addCombo({ xtype:"combo", name:"scope", dataIndex:"scope", store:[ __CMM_TYPE__.ContractScope._INTO_PLANE_, __CMM_TYPE__.ContractScope._NON_INTO_PLANE_]})
			.addCombo({ xtype:"combo", name:"isSpot", dataIndex:"isSpot", store:[ __CMM_TYPE__.IsSpot._TERM_, __CMM_TYPE__.IsSpot._SPOT_]})
			.addDateField({name:"validFrom", dataIndex:"validFrom"})
			.addDateField({name:"validTo", dataIndex:"validTo"})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __CMM_TYPE__.ContractStatus._DRAFT_, __CMM_TYPE__.ContractStatus._ACTIVE_, __CMM_TYPE__.ContractStatus._EFFECTIVE_, __CMM_TYPE__.ContractStatus._EXPIRED_]})
			.addCombo({ xtype:"combo", name:"approvalStatus", dataIndex:"approvalStatus", store:[ __CMM_TYPE__.BidApprovalStatus._NEW_, __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_, __CMM_TYPE__.BidApprovalStatus._APPROVED_, __CMM_TYPE__.BidApprovalStatus._REJECTED_]})
			.addTextField({ name:"code", dataIndex:"code", maxLength:32})
			.addLov({name:"financialSourceCode", dataIndex:"financialSourceCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CodeLov_Lov", selectOnFocus:true, maxLength:25}})
			.addLov({name:"avgMethodName", dataIndex:"avgMethodName", maxLength:50,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.AvgMethLov_Lov", selectOnFocus:true, maxLength:50}})
			.addLov({name:"supplier", dataIndex:"supplierCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierCodeLov_Lov", selectOnFocus:true, maxLength:32}})
			.addLov({name:"contractHolder", dataIndex:"holderCode", width:240, maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.UserSubsidiaryLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"subsidiaryId", dsField: "holderId"} ]}})
		;
	}

});

/* ================= EDIT FORM: selectSupplier ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.Contract_Dc$selectSupplier", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_Contract_Dc$selectSupplier",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"SupplierName", bind:"{d.supplierName}", dataIndex:"supplierName", allowBlank:false, xtype:"fmbas_SupplierCodeLov_Lov", maxLength:100, labelAlign:"top",
			retFieldMapping: [{lovField:"id", dsField: "supplierId"} ,{lovField:"code", dsField: "supplierCode"} ,{lovField:"buyerId", dsField: "resBuyerId"} ,{lovField:"buyerName", dsField: "resBuyerName"} ,{lovField:"entityAlias", dsField: "cpEntityAlias"} ],listeners:{
			fpvalidchange:{scope:this, fn:this._enableContinue_}
		}})
		/* =========== containers =========== */
		.addPanel({ name:"col1", width:300, layout:"anchor"})
		.addPanel({ name:"main", autoScroll:true});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("col1", ["SupplierName"])
		.addChildrenTo("main", ["col1"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableContinue_: function(el,newValue) {
		
						var enableBtn = false;
						if( !Ext.isEmpty(el.getValue()) && newValue >= 0 ) {
							enableBtn = true;
						}
						this._controller_.fireEvent("enableContinueBtn", this, enableBtn);
	}
});

/* ================= GRID: TabList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.Contract_Dc$TabList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_Contract_Dc$TabList",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"status", dataIndex:"status", width:80,  draggable:false, hideable:false, renderer:function(value, metaData) {return this._badgifyColumn_(value); }})
		.addTextColumn({ name:"code", dataIndex:"code", width:100})
		.addTextColumn({ name:"location", dataIndex:"locCode", width:80})
		.addTextColumn({ name:"type", dataIndex:"type", width:70})
		.addTextColumn({ name:"subType", dataIndex:"subType", width:100})
		.addTextColumn({ name:"scope", dataIndex:"scope", width:80})
		.addTextColumn({ name:"isSpot", dataIndex:"isSpot", width:80})
		.addDateColumn({ name:"validFrom", dataIndex:"validFrom", _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"validTo", _mask_: Masks.DATE})
		.addTextColumn({ name:"approvalStatus", dataIndex:"approvalStatus", width:70})
		.addTextColumn({ name:"supplier", dataIndex:"supplierCode", width:100})
		.addTextColumn({ name:"shipToList", dataIndex:"shipToList", width:200})
		.addTextColumn({ name:"limitedTo", dataIndex:"limitedTo", hidden:true, width:80})
		.addTextColumn({ name:"tax", dataIndex:"tax", hidden:true, width:80})
		.addTextColumn({ name:"respBuyer", dataIndex:"resBuyerName", hidden:true, width:100})
		.addTextColumn({ name:"holderCode", dataIndex:"holderCode", hidden:true, width:100})
		.addTextColumn({ name:"contactName", dataIndex:"contactName", hidden:true, width:100})
		.addTextColumn({ name:"product", dataIndex:"product", hidden:true, width:80})
		.addTextColumn({ name:"cntpartyRef", dataIndex:"counterpartyReference", hidden:true, width:100})
		.addTextColumn({ name:"dealType", dataIndex:"dealType", hidden:true, width:100})
		.addTextColumn({ name:"ipcAgent", dataIndex:"iplCode", hidden:true, width:100})
		.addTextColumn({ name:"contactUnit", dataIndex:"contVolUnitCode", hidden:true, width:100})
		.addTextColumn({ name:"qtyType", dataIndex:"quantityType", hidden:true, width:100})
		.addTextColumn({ name:"ctrVolUnit", dataIndex:"contVolUnitCode", hidden:true, width:100})
		.addTextColumn({ name:"period", dataIndex:"period", hidden:true, width:100})
		.addNumberColumn({ name:"volToler", dataIndex:"volumeTolerance", hidden:true, width:100})
		.addNumberColumn({ name:"volShare", dataIndex:"volumeShare", hidden:true, width:100})
		.addTextColumn({ name:"resRefCode", dataIndex:"resaleRefCode", hidden:true, width:100})
		.addTextColumn({ name:"settUnit", dataIndex:"settUnitCode", hidden:true, width:100})
		.addNumberColumn({ name:"settDec", dataIndex:"settlementDecimals", hidden:true, width:100})
		.addTextColumn({ name:"settCurr", dataIndex:"settCurrCode", hidden:true, width:100})
		.addTextColumn({ name:"finSCode", dataIndex:"financialSourceCode", hidden:true, width:100})
		.addTextColumn({ name:"avgMthdName", dataIndex:"avgMethodName", hidden:true, width:100})
		.addTextColumn({ name:"exchRateOff", dataIndex:"exchangeRateOffset", hidden:true, width:100})
		.addNumberColumn({ name:"paymTerms", dataIndex:"paymentTerms", hidden:true, width:100})
		.addTextColumn({ name:"paymRefDay", dataIndex:"paymentRefDay", hidden:true, width:100})
		.addTextColumn({ name:"invFreq", dataIndex:"invoiceFreq", hidden:true, width:100})
		.addTextColumn({ name:"creditTerms", dataIndex:"creditTerms", hidden:true, width:100})
		.addTextColumn({ name:"invType", dataIndex:"invoiceType", hidden:true, width:100})
		.addTextColumn({ name:"invPayBy", dataIndex:"invoicePayableBy", hidden:true, width:100})
		.addTextColumn({ name:"vat", dataIndex:"vat", hidden:true, width:100})
		.addTextColumn({ name:"revPeriod", dataIndex:"reviewPeriod", hidden:true, width:100})
		.addTextColumn({ name:"revFirstPar", dataIndex:"reviewFirstParam", hidden:true, width:100})
		.addTextColumn({ name:"revSecParam", dataIndex:"reviewSecondParam", hidden:true, width:100})
		.addNumberColumn({ name:"revNoti", dataIndex:"reviewNotification", hidden:true, width:100})
		.addTextColumn({ name:"paymComment", dataIndex:"paymentComment", hidden:true, width:100})
		.addTextColumn({ name:"eventType", dataIndex:"eventType", hidden:true, width:100})
		.addNumberColumn({ name:"awardVol", dataIndex:"awardedVolume", hidden:true, width:100, sysDec:"dec_unit"})
		.addTextColumn({ name:"resaleContract", dataIndex:"resaleContract", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_badgifyColumn_: function(value) {
		
						var badgeCls = "sone-badge-default";
						if (value === __CMM_TYPE__.ContractStatus._DRAFT_) {
							badgeCls = "sone-badge-default";
						}
						else if (value === __CMM_TYPE__.ContractStatus._ACTIVE_) {
							badgeCls = "sone-badge-blue";
						}
						else if (value === __CMM_TYPE__.ContractStatus._EFFECTIVE_) {
							badgeCls = "sone-badge-green";
						}				
						else if (value === __CMM_TYPE__.ContractStatus._EXPIRED_) {
							badgeCls = "sone-badge-red";
						}
						var badge = "<div class='sone-badge "+badgeCls+"'>"+value+"</div>";
						return badge;
	}
});

/* ================= EDIT FORM: copySupplier ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.Contract_Dc$copySupplier", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_Contract_Dc$copySupplier",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"supplierField", bind:"{d.supplierField}", dataIndex:"supplierField", allowBlank:false, width:240, xtype:"fmbas_SupplierCodeLov_Lov", maxLength:32, labelAlign:"top",
			retFieldMapping: [{lovField:"id", dsParam: "companyId"} ],listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		/* =========== containers =========== */
		.addPanel({ name:"col1", width:240, layout:"anchor"})
		.addPanel({ name:"main", autoScroll:true, width:240});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("col1", ["supplierField"])
		.addChildrenTo("main", ["col1"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableContinue_: function(el,newValue,oldValue) {
		
						var ctrl = this._controller_;
						if (!Ext.isEmpty(el.getValue())) {
							ctrl.fireEvent("enableContinueCopyBtn");
						}
						else {
							ctrl.fireEvent("disableContinueCopyBtn");
						}
	},
	
	_shouldValidate_: function() {
		
						return this._isVisible_;
	}
});

/* ================= EDIT FORM: contractPeriodForm ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.Contract_Dc$contractPeriodForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_Contract_Dc$contractPeriodForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", allowBlank:false, labelAlign:"top"})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", allowBlank:false, labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:170, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["validFrom", "validTo"]);
	}
});

/* ================= EDIT FORM: Header ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.Contract_Dc$Header", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_Contract_Dc$Header",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btnToggleContractPeriod", scope: this, handler: this._toggleContractPeriod_, text: "...", style:"margin-left:3px"})
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", allowBlank:false, width:220})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", allowBlank:false, width:220})
		.addLov({name:"location", bind:"{d.locCode}", dataIndex:"locCode", _enableFn_: this._enableIfStatus_, allowBlank:false, width:200, xtype:"fmbas_LocationsLov_Lov", maxLength:25, labelWidth:85,
			retFieldMapping: [{lovField:"id", dsField: "locId"} ],
			filterFieldMapping: [{lovField:"isAirport", value: "true"} ]})
		.addLov({name:"contractHolder", bind:"{d.holderCode}", dataIndex:"holderCode", _enableFn_: this._hasResale_, allowBlank:false, width:240, xtype:"fmbas_UserSubsidiaryLov_Lov", maxLength:32, cls:"sone-flat-kpi sone-flat-combo", style:"width: 60px !important",
			retFieldMapping: [{lovField:"subsidiaryId", dsField: "holderId"} ]})
		.addCombo({ xtype:"combo", name:"termSpot", bind:"{d.isSpot}", dataIndex:"isSpot", _enableFn_: this._enableIfStatus_, allowBlank:false, width:285, store:[ __CMM_TYPE__.IsSpot._TERM_, __CMM_TYPE__.IsSpot._SPOT_], labelWidth:130})
		.addCombo({ xtype:"combo", name:"product", bind:"{d.product}", dataIndex:"product", _enableFn_: this._enableIfStatus_, allowBlank:false, width:200, store:[ __CMM_TYPE__.Product._JET_A_, __CMM_TYPE__.Product._JET_A1_, __CMM_TYPE__.Product._TS_1_, __CMM_TYPE__.Product._F_34_, __CMM_TYPE__.Product._JP_8_, __CMM_TYPE__.Product._RP_4_, __CMM_TYPE__.Product._T1_, __CMM_TYPE__.Product._A1_BIO_, __CMM_TYPE__.Product._CAT_2_, __CMM_TYPE__.Product._AVGAS_, __CMM_TYPE__.Product._AG_1_HL_, __CMM_TYPE__.Product._AG_80_, __CMM_TYPE__.Product._JA_1_A_, __CMM_TYPE__.Product._JAA_, __CMM_TYPE__.Product._JP_5_, __CMM_TYPE__.Product._NON_, __CMM_TYPE__.Product._OTH_], labelWidth:85})
		.addNumberField({name:"iataServiceLevel", bind:"{d.iataServiceLevel}", dataIndex:"iataServiceLevel", _enableFn_: this._enableIfStatus_, width:200, maxLength:1, labelWidth:150, cls:"number-with-spinner", maxValue:4, minValue:1})
		.addCombo({ xtype:"combo", name:"flightType", bind:"{d.limitedTo}", dataIndex:"limitedTo", _enableFn_: this._enableIfStatus_, width:220, store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_]})
		.addCombo({ xtype:"combo", name:"tax", bind:"{d.tax}", dataIndex:"tax", _enableFn_: this._enableIfStatus_, allowBlank:false, width:261, store:[ __CMM_TYPE__.TaxType._UNSPECIFIED_, __CMM_TYPE__.TaxType._BONDED_, __CMM_TYPE__.TaxType._DOMESTIC_, __CMM_TYPE__.TaxType._FOREIGN_TRADE_ZONE_, __CMM_TYPE__.TaxType._DUTY_FREE_, __CMM_TYPE__.TaxType._DUTY_PAID_, __CMM_TYPE__.TaxType._OTHER_], labelWidth:141})
		.addLov({name:"resBuyerName", bind:"{d.resBuyerName}", dataIndex:"resBuyerName", width:270, xtype:"fmbas_UserNameLov_Lov", maxLength:255, labelWidth:150,
			retFieldMapping: [{lovField:"id", dsField: "resBuyerId"} ]})
		.addLov({name:"contacts", bind:"{d.contactName}", dataIndex:"contactName", width:270, xtype:"fmbas_ContactsLov_Lov", maxLength:255, labelWidth:150,
			retFieldMapping: [{lovField:"id", dsField: "contactId"} ],
			filterFieldMapping: [{lovField:"objectId", dsField: "supplierId"}, {lovField:"objectType", dsField: "cpEntityAlias"} ]})
		.addToggleField({ name:"hardCopy", bind:"{d.hardCopy}", dataIndex:"hardCopy", labelWidth:150})
		.addPopoverTextField({name:"contractPeriod", width:150, hPos:"r", vPos:"b", dataType:"date", fieldsList:["validFrom","validTo"], formatList:[], editorForm:"cmm_Contract_Dc$contractPeriodForm", fieldsValueSeparator:" - "})
		.addDisplayFieldText({ name:"contractPeriodLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding-left:30px; float:right"})
		.addLov({name:"supplier", bind:"{d.supplierCode}", dataIndex:"supplierCode", _enableFn_: this._hasResale_, allowBlank:false, xtype:"fmbas_SupplierCodeLov_Lov", maxLength:32, cls:"sone-flat-kpi sone-flat-combo", style:"width: 120px !important",
			retFieldMapping: [{lovField:"id", dsField: "supplierId"} ]})
		.addDisplayFieldText({ name:"status", bind:"{d.status}", dataIndex:"status", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"type", bind:"{d.type}", dataIndex:"type", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"delivery", bind:"{d.subType}", dataIndex:"subType", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"scope", bind:"{d.scope}", dataIndex:"scope", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"formTitle", bind:"{d.formTitle}", dataIndex:"formTitle", noEdit:true , maxLength:16})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("location"),this._getConfig_("contractPeriodLabel"),this._getConfig_("contractPeriod"),this._getConfig_("btnToggleContractPeriod"),this._getConfig_("flightType"),this._getConfig_("contacts"),this._getConfig_("iataServiceLevel")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("product"),this._getConfig_("termSpot"),this._getConfig_("tax"),this._getConfig_("resBuyerName"),this._getConfig_("hardCopy")]})
		.add({name:"row", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("formTitle")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"p2",  cls:"sone-kpi-panel", layout: {type:"table"}})
		.addPanel({ name:"titleAndKpi",  style:"margin-bottom:10px", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"c0", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c1", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c2", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c3", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c4", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"title", width:300,  cls:"sone-title-panel", layout:"anchor"})
		.addPanel({ name:"col1", width:1500, layout:"anchor"})
		.addPanel({ name:"col2", width:1500, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["titleAndKpi", "col1", "col2"])
		.addChildrenTo("p2", ["c0", "c1", "c2", "c3", "c4"])
		.addChildrenTo("titleAndKpi", ["title", "p2"])
		.addChildrenTo("c0", ["contractHolder"])
		.addChildrenTo("c1", ["supplier"])
		.addChildrenTo("c2", ["status"])
		.addChildrenTo("c3", ["type"])
		.addChildrenTo("c4", ["delivery"])
		.addChildrenTo("title", ["row"])
		.addChildrenTo("col1", ["row1"])
		.addChildrenTo("col2", ["row2"]);
	},
	/* ==================== Business functions ==================== */
	
	_hasResale_: function() {
		
						var ctrl = this._controller_;
						var r = ctrl.getRecord();
						var result = true;
						if (r) {
							var resaleContract = r.get("resaleContract");
							if (!Ext.isEmpty(resaleContract)) {
								result = false;
							}
						}
						return result;
	},
	
	_toggleContractPeriod_: function() {
		
						var f = this._get_("contractPeriod");
						f._showEditor_();
	},
	
	_afterDefineElements_: function() {
		
		
						this._getBuilder_().change("titleAndKpi",{
							layout: {
						        type: "hbox",
						        align: "stretch"
						    },
						    defaults: {
						        flex: 1,
								height: 60
						    }
						});
						
	},
	
	_afterApplyStates_: function() {
		
						// Dan: SONE-1648 When new contract is captured, the focus is not placed in "Airport" field
						
						var field = this._controller_.getRecord();
						if(field){
							var ticketNoValue = field.get("code");
							var formTitle = this._get_("formTitle");	
							if (formTitle && formTitle.labelEl) {			
								var originalTitleLabel = formTitle.fieldLabel;
								formTitle.labelEl.update(originalTitleLabel+" #"+ticketNoValue);
							}
						}
	},
	
	_enableIfStatus_: function() {
		
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var status = record.get("status");
						var result = true;
		
						if (status=="Active" || status=="Effective" || status=="Expired") {
							result = false;
						}
		
						return result;
		
	}
});

/* ================= EDIT FORM: SelectCustomer ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.Contract_Dc$SelectCustomer", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_Contract_Dc$SelectCustomer",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"customerLov", bind:"{d.customerCode}", dataIndex:"customerCode", allowBlank:false, width:270, xtype:"fmbas_CustomerCodeExtendedLov_Lov", maxLength:100, labelAlign:"top",
			retFieldMapping: [{lovField:"id", dsField: "customerId"} ]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:270})
		.addPanel({ name:"c1", width:270, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["c1"])
		.addChildrenTo("c1", ["customerLov"]);
	},
	/* ==================== Business functions ==================== */
	
	_shouldValidate_: function() {
		
						return this._isVisible_;
	}
});

/* ================= EDIT FORM: Remark ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.Contract_Dc$Remark", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_Contract_Dc$Remark",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.remarks}", paramIndex:"remarks", labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  layout:"fit"})
		.addPanel({ name:"remarkCol1", layout:"fit"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["remarkCol1"])
		.addChildrenTo("remarkCol1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
						remarks.setReadOnly(false);
						return true;
	}
});

/* ================= GRID: Temp ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.Contract_Dc$Temp", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_Contract_Dc$Temp",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Volumes ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.Contract_Dc$Volumes", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_Contract_Dc$Volumes",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addNumberField({name:"awardedVolume", bind:"{d.awardedVolume}", dataIndex:"awardedVolume", _enableFn_: this._enableIfStatus_, width:110, noLabel: true, sysDec:"dec_unit", maxLength:19})
		.addTextField({ name:"settUnit", bind:"{d.settUnitCode}", dataIndex:"settUnitCode", noEdit:true , width:40, noLabel: true, maxLength:2, style:"padding-left:2px"})
		.addCombo({ xtype:"combo", name:"period", bind:"{d.period}", dataIndex:"period", _enableFn_: this._enableIfStatus_, width:120, noLabel: true, store:[ __CMM_TYPE__.Period._CONTRACT_PERIOD_, __CMM_TYPE__.Period._MONTHLY_, __CMM_TYPE__.Period._YEARLY_]})
		.addNumberField({name:"tolerance", bind:"{d.volumeTolerance}", dataIndex:"volumeTolerance", _enableFn_: this._enableIfStatus_, width:60, noLabel: true, maxLength:4,listeners:{
			fpchange:{scope:this, fn:function() {this._validatePercent_(this._get_('tolerance'))}}
		}})
		.addNumberField({name:"share", bind:"{d.volumeShare}", dataIndex:"volumeShare", _enableFn_: this._enableIfStatus_, width:60, noLabel: true, maxLength:4,listeners:{
			fpchange:{scope:this, fn:function() {this._validatePercent_(this._get_('share'))}}
		}})
		.addCombo({ xtype:"combo", name:"quantityType", bind:"{d.quantityType}", dataIndex:"quantityType", _enableFn_: this._enableIfStatus_, width:120, noLabel: true, store:[ __CMM_TYPE__.QuantityType._NET_VOLUME_, __CMM_TYPE__.QuantityType._GROSS_VOLUME_]})
		.addDisplayFieldText({ name:"percent", bind:"{d.percent}", dataIndex:"percent", maxLength:1, xtype:"percent", style:"padding-left:3px"})
		.addDisplayFieldText({ name:"percent1", bind:"{d.percent}", dataIndex:"percent", maxLength:1, xtype:"percent", style:"padding-left:3px"})
		.addDisplayFieldText({ name:"awardedVolumeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"periodLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"toleranceLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"shareLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		.addDisplayFieldText({ name:"quantityTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:30px"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", layout: {type:"table"}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["awardedVolumeLabel", "awardedVolume", "settUnit", "periodLabel", "period", "toleranceLabel", "tolerance", "percent", "shareLabel", "share", "percent1", "quantityTypeLabel", "quantityType"]);
	},
	/* ==================== Business functions ==================== */
	
	_validatePercent_: function(el) {
		
						var value = el.getValue();
						if (isNaN(value) || value < 0 || value > 100) {
							Main.warning("Value is out of range! The value must be within 0 and 100");
						    el.setValue("");
						}
	},
	
	_enableIfStatus_: function() {
		
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var status = record.get("status");
						var result = true;
						if (status=="Active" || status=="Effective" || status=="Expired") {
							result = false;
						}
						return result;
	}
});

/* ================= EDIT FORM: ApprovalNote ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.Contract_Dc$ApprovalNote", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_Contract_Dc$ApprovalNote",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"approvalNote", bind:"{p.approvalNote}", paramIndex:"approvalNote", width:580, labelAlign:"top", labelStyle:"font-weight:bold", fieldStyle:"margin-top: 5px;"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  layout:"fit"})
		.addPanel({ name:"col1", layout:"fit"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["approvalNote"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var approvalNote = this._get_("approvalNote");
						approvalNote.setRawValue("");
	}
});

/* ================= EDIT FORM: financialSourceAvgMethodPeriod ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.Contract_Dc$financialSourceAvgMethodPeriod", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_Contract_Dc$financialSourceAvgMethodPeriod",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", _enableFn_: this._enableIfCurrency_, allowBlank:false, xtype:"fmbas_ExchangeRateToFinancialSourceAndAvgFilteredLov_Lov", maxLength:25, labelAlign:"top",
			retFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"} ],
			filterFieldMapping: [{lovField:"currencyId", dsField: "settCurrId"} ],listeners:{
			fpchange:{scope:this, fn:this._enableAvgMethods_},
			expand:{scope:this, fn:this._filterDistinct_}
		}})
		.addLov({name:"avgMethodName", bind:"{d.avgMethodName}", dataIndex:"avgMethodName", _enableFn_: this._enableIfFinancialSource_, allowBlank:false, xtype:"fmbas_ExchangeRateToAvgMthdFilteredLov_Lov", maxLength:50, labelAlign:"top",
			retFieldMapping: [{lovField:"avgMthdId", dsField: "avgMethodId"} ],
			filterFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"}, {lovField:"currencyId", dsField: "settCurrId"} ],listeners:{
			expand:{scope:this, fn:this._filterDistinctAvg_}
		}})
		.addCombo({ xtype:"combo", name:"exchangeRateOffset", bind:"{d.exchangeRateOffset}", dataIndex:"exchangeRateOffset", _enableFn_: this._enableIfStatus_, allowBlank:false, store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_], labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:170, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["financialSourceCode", "avgMethodName", "exchangeRateOffset"]);
	},
	/* ==================== Business functions ==================== */
	
	_filterDistinctAvg_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("avgMthdName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	},
	
	_filterDistinct_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("financialSourceName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	},
	
	_enableAvgMethods_: function(el) {
		
		
						if (el._fpRawValue_ != el._onFocusVal_) {
							if( this._enableIfStatus_() ){
								var avgMethodName = this._get_("avgMethodName");
								var elVal = el.getValue(); 
								if (!Ext.isEmpty(elVal)) {
									avgMethodName.setReadOnly(false);
								}
								else {
									avgMethodName.setReadOnly(true);
								}
								avgMethodName.setValue("");
							}
						}
	},
	
	_enableIfStatus_: function() {
		
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = true;
		
						if (record) {
		
							var status = record.get("status");
		
							if (status == "Effective" || status == "Expired" || status == "Active") {
								result = false;
							}
						}
		
						return result;
	},
	
	_enableIfCurrency_: function() {
		
						
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = false;
		
						if (record) {
							var currencyCode = record.get("settCurrCode");
							var status = record.get("status");
							if (!Ext.isEmpty(currencyCode) && (status != "Effective" && status != "Expired" && status != "Active")) {
								result = true;
							}
						}
		
						return result;
		
	},
	
	_enableIfFinancialSource_: function() {
		
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = false;
						
		
						if (record) {
							var financialSourceCode = record.get("financialSourceCode");
							var status = record.get("status");
							if (!Ext.isEmpty(financialSourceCode) && (status != "Effective" && status != "Expired" && status != "Active")) {
								result = true;
							}
						}
		
						return result;
		
	}
});

/* ================= EDIT FORM: paymentTermsRefDay ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.Contract_Dc$paymentTermsRefDay", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_Contract_Dc$paymentTermsRefDay",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"paymentTermsLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, labelAlign:"left"})
		.addDisplayFieldText({ name:"daysLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, labelAlign:"left", style:"margin-left:5px"})
		.addNumberField({name:"paymentTerms", bind:"{d.paymentTerms}", dataIndex:"paymentTerms", _enableFn_: this._enableIfStatus_, allowBlank:false, width:120, noLabel: true, maxLength:4, cls:"number-with-spinner", minValue:0, maxValue:99999})
		.addCombo({ xtype:"combo", name:"paymentRefDay", bind:"{d.paymentRefDay}", dataIndex:"paymentRefDay", _enableFn_: this._enableIfStatus_, allowBlank:false, store:[ __FMBAS_TYPE__.PaymentDay._INVOICE_DATE_, __FMBAS_TYPE__.PaymentDay._INVOICE_RECEIVING_DATE_, __FMBAS_TYPE__.PaymentDay._LAST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._FIRST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._MIDPOINT_DELIVERY_DATE_], labelAlign:"top"})
		.add({name:"termsDays", xtype: "fieldcontainer", layout: {type:"hbox"}, noLabel: true, items: [this._getConfig_("paymentTerms"),this._getConfig_("daysLabel")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:170, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["paymentTermsLabel", "termsDays", "paymentRefDay"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableIfStatus_: function() {
		
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = true;
		
						if (record) {
		
							var status = record.get("status");
		
							if (status == "Effective" || status == "Expired" || status == "Active") {
								result = false;
							}
						}
		
						return result;
	}
});

/* ================= EDIT FORM: PaymentTerms ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.Contract_Dc$PaymentTerms", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_Contract_Dc$PaymentTerms",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btnToggle", scope: this, handler: this._toggle_, text: "...", style:"margin-left:3px"})
		.addButton({name:"btnTogglePaymentTerms", scope: this, handler: this._togglePaymentTerms_, text: "...", style:"margin-left:3px"})
		.addDisplayFieldText({ name:"settlement", bind:"{d.settlement}", dataIndex:"settlement", noEdit:true , maxLength:4000, labelWidth:300, labelAlign:"left"})
		.addDisplayFieldText({ name:"invoicing", bind:"{d.invoicing}", dataIndex:"invoicing", noEdit:true , maxLength:4000, labelWidth:300, labelAlign:"left"})
		.addDisplayFieldText({ name:"price", bind:"{d.price}", dataIndex:"price", noEdit:true , maxLength:4000, labelWidth:300, labelAlign:"left"})
		.addDisplayFieldText({ name:"days", bind:"{d.days}", dataIndex:"days", noEdit:true , maxLength:4000, labelWidth:120, labelAlign:"left"})
		.addLov({name:"settCurr", bind:"{d.settCurrCode}", dataIndex:"settCurrCode", _enableFn_: this._enableIfStatus_, allowBlank:false, width:200, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:120,
			retFieldMapping: [{lovField:"id", dsField: "settCurrId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ],listeners:{
			fpchange:{scope:this, fn:this._enableFs_}
		}})
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", allowBlank:false, xtype:"fmbas_ExchangeRateToFinancialSourceAndAvgFilteredLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"} ],
			filterFieldMapping: [{lovField:"currencyId", dsField: "settCurrId"} ]})
		.addLov({name:"avgMethodName", bind:"{d.avgMethodName}", dataIndex:"avgMethodName", allowBlank:false, xtype:"fmbas_ExchangeRateToAvgMthdFilteredLov_Lov", maxLength:50,
			retFieldMapping: [{lovField:"avgMthdId", dsField: "avgMethodId"} ],
			filterFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"}, {lovField:"currencyId", dsField: "settCurrId"} ]})
		.addCombo({ xtype:"combo", name:"exchangeRateOffset", bind:"{d.exchangeRateOffset}", dataIndex:"exchangeRateOffset", allowBlank:false, width:300, store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_]})
		.addLov({name:"settUnit", bind:"{d.settUnitCode}", dataIndex:"settUnitCode", _enableFn_: this._enableIfStatus_, allowBlank:false, width:220, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, labelWidth:140,
			retFieldMapping: [{lovField:"id", dsField: "settUnitId"} ]})
		.addNumberField({name:"paymentTerms", bind:"{d.paymentTerms}", dataIndex:"paymentTerms", allowBlank:false, maxLength:4})
		.addCombo({ xtype:"combo", name:"paymentRefDay", bind:"{d.paymentRefDay}", dataIndex:"paymentRefDay", allowBlank:false, width:280, store:[ __FMBAS_TYPE__.PaymentDay._INVOICE_DATE_, __FMBAS_TYPE__.PaymentDay._INVOICE_RECEIVING_DATE_, __FMBAS_TYPE__.PaymentDay._LAST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._FIRST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._MIDPOINT_DELIVERY_DATE_]})
		.addCombo({ xtype:"combo", name:"invoiceFreq", bind:"{d.invoiceFreq}", dataIndex:"invoiceFreq", _enableFn_: this._enableIfStatus_, allowBlank:false, width:329, store:[ __FMBAS_TYPE__.InvoiceFreq._WEEKLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIMONTHLY_, __FMBAS_TYPE__.InvoiceFreq._MONTHLY_, __FMBAS_TYPE__.InvoiceFreq._QUARTERLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._ANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._PER_DELIVERY_, __FMBAS_TYPE__.InvoiceFreq._DAILY_, __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_], labelWidth:155,listeners:{
			select:{scope:this, fn:function() {this._showFields_(this._get_('invoiceFreq'))}},
			expand:{scope:this, fn:this._filterInvoiceFreq_}
		}})
		.addNumberField({name:"invoiceFreqNumber", bind:"{d.invoiceFreqNumber}", dataIndex:"invoiceFreqNumber", _enableFn_: this._enableIfStatus_, _visibleFn_: function(dc, rec) { return dc.record.data.invoiceFreq == 
					__FMBAS_TYPE__.InvoiceFreq._EVERY_; } , width:60, noLabel: true, maxLength:2, cls:"number-with-spinner", minValue:2, maxValue:365})
		.addDisplayFieldText({ name:"daysFrequency", bind:"{d.daysFrequency}", dataIndex:"daysFrequency", noEdit:true , _enableFn_: function(dc, rec) { return dc.record.data.invoiceFreq =='Every'; } , _visibleFn_: function(dc, rec) { return dc.record.data.invoiceFreq == 
					__FMBAS_TYPE__.InvoiceFreq._EVERY_; } , maxLength:4000, labelWidth:120, labelAlign:"left", cls:"margin-left:5px"})
		.addCombo({ xtype:"combo", name:"invoicePayableBy", bind:"{d.invoicePayableBy}", dataIndex:"invoicePayableBy", _enableFn_: this._enableIfStatus_, store:[ __CMM_TYPE__.InvoicePayableBy._HEADQUARTER_, __CMM_TYPE__.InvoicePayableBy._LOCAL_OFFICE_], labelWidth:120})
		.addCombo({ xtype:"combo", name:"invoiceType", bind:"{d.invoiceType}", dataIndex:"invoiceType", allowBlank:false, width:370, store:[ __FMBAS_TYPE__.InvoiceType._XML_2_0_2_, __FMBAS_TYPE__.InvoiceType._XML_3_0_0_, __FMBAS_TYPE__.InvoiceType._XML_3_0_1_, __FMBAS_TYPE__.InvoiceType._XML_3_1_0_, __FMBAS_TYPE__.InvoiceType._EXCEL_, __FMBAS_TYPE__.InvoiceType._PAPER_, __FMBAS_TYPE__.InvoiceType._EDI_, __FMBAS_TYPE__.InvoiceType._PDF_FORMAT_, __FMBAS_TYPE__.InvoiceType._FISCAL_, __FMBAS_TYPE__.InvoiceType._EXPORT_, __FMBAS_TYPE__.InvoiceType._REGULAR_], labelWidth:195})
		.addCombo({ xtype:"combo", name:"vat", bind:"{d.vat}", dataIndex:"vat", _enableFn_: this._enableIfStatus_, allowBlank:false, width:370, store:[ __CMM_TYPE__.VatApplicability._ALL_EVENTS_, __CMM_TYPE__.VatApplicability._DOMESTIC_EVENTS_, __CMM_TYPE__.VatApplicability._INTERNATIONAL_EVENTS_, __CMM_TYPE__.VatApplicability._NOT_APPLICABLE_], labelWidth:195})
		.addCombo({ xtype:"combo", name:"creditTerms", bind:"{d.creditTerms}", dataIndex:"creditTerms", _enableFn_: this._enableIfStatus_, allowBlank:false, store:[ __FMBAS_TYPE__.CreditTerm._OPEN_CREDIT_, __FMBAS_TYPE__.CreditTerm._CREDIT_LINE_, __FMBAS_TYPE__.CreditTerm._BANK_GUARANTEE_, __FMBAS_TYPE__.CreditTerm._PREPAYMENT_], labelWidth:120})
		.addTextField({ name:"paymentComment", bind:"{d.paymentComment}", dataIndex:"paymentComment", _enableFn_: this._enableIfStatus_, width:665, maxLength:255, labelWidth:120})
		.addCombo({ xtype:"combo", name:"reviewPeriod", bind:"{d.reviewPeriod}", dataIndex:"reviewPeriod", width:300, store:[ __CMM_TYPE__.ReviewPeriod._NO_, __CMM_TYPE__.ReviewPeriod._WEEKLY_, __CMM_TYPE__.ReviewPeriod._SEMIMONTHLY_, __CMM_TYPE__.ReviewPeriod._MONTHLY_, __CMM_TYPE__.ReviewPeriod._QUARTERLY_, __CMM_TYPE__.ReviewPeriod._SEMIANNUALLY_, __CMM_TYPE__.ReviewPeriod._ANNUALLY_], labelWidth:150})
		.addTextField({ name:"reviewFirstParam", bind:"{d.reviewFirstParam}", dataIndex:"reviewFirstParam", width:150, maxLength:32})
		.addTextField({ name:"reviewSecondParam", bind:"{d.reviewSecondParam}", dataIndex:"reviewSecondParam", width:50, noLabel: true, maxLength:32})
		.addNumberField({name:"reviewNotification", bind:"{d.reviewNotification}", dataIndex:"reviewNotification", width:100, maxLength:4, labelWidth:50})
		.addPopoverTextField({name:"financialSourceAvgMethodPeriod", width:435, hPos:"r", vPos:"t", labelWidth:140, fieldsList:["financialSourceCode","avgMethodName","exchangeRateOffset"], formatList:[], editorForm:"cmm_Contract_Dc$financialSourceAvgMethodPeriod", fieldsValueSeparator:" / "})
		.addPopoverTextField({name:"paymentTermRefDay", width:295, hPos:"r", vPos:"b", labelWidth:120, fieldsList:["paymentTerms","paymentRefDay"], formatList:["",""], editorForm:"cmm_Contract_Dc$paymentTermsRefDay", fieldsValueSeparator:" days from ", allowBlank:false})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("settCurr"),this._getConfig_("settUnit"),this._getConfig_("financialSourceAvgMethodPeriod"),this._getConfig_("btnToggle")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("paymentTermRefDay"),this._getConfig_("btnTogglePaymentTerms"),this._getConfig_("invoiceFreq"),this._getConfig_("invoiceFreqNumber"),this._getConfig_("daysFrequency")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("invoicePayableBy"),this._getConfig_("invoiceType")]})
		.add({name:"row4", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("reviewPeriod"),this._getConfig_("reviewFirstParam"),this._getConfig_("reviewSecondParam"),this._getConfig_("reviewNotification"),this._getConfig_("days")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("creditTerms"),this._getConfig_("vat")]})
		.add({name:"sep1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("invoicing")]})
		.add({name:"sep2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("price")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:300, layout:"anchor"})
		.addPanel({ name:"col2", width:1200, layout:"anchor"})
		.addPanel({ name:"col4", width:1200, layout:"anchor"})
		.addPanel({ name:"col5", width:1200, layout:"anchor"})
		.addPanel({ name:"col6", width:1200, layout:"anchor"})
		.addPanel({ name:"col7", width:1200, layout:"anchor"})
		.addPanel({ name:"col8", width:1200, layout:"anchor"})
		.addPanel({ name:"col9", width:300, layout:"anchor"})
		.addPanel({ name:"col10", width:1200, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col4", "col5", "col6", "col7", "col8"])
		.addChildrenTo("col1", ["settlement"])
		.addChildrenTo("col2", ["row1"])
		.addChildrenTo("col4", ["sep1"])
		.addChildrenTo("col5", ["row2"])
		.addChildrenTo("col6", ["row3"])
		.addChildrenTo("col7", ["row5"])
		.addChildrenTo("col8", ["paymentComment"])
		.addChildrenTo("col9", ["sep2"])
		.addChildrenTo("col10", ["row4"]);
	},
	/* ==================== Business functions ==================== */
	
	_filterInvoiceFreq_: function(el) {
		
						var s = el.store;
						s.filter(function(r) {
							var value = r.get("field1");
							if ( value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_ && value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_ 
								&& value !== __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_) {
								return value;
							}
						});
	},
	
	_togglePaymentTerms_: function() {
		
						var f = this._get_("paymentTermRefDay");
						f._showEditor_();
	},
	
	_toggle_: function() {
		
						var f = this._get_("financialSourceAvgMethodPeriod");
						f._showEditor_();
	},
	
	_enableIfStatus_: function() {
		
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = true;
		
						if (record) {
		
							var status = record.get("status");
		
							if (status == "Effective" || status == "Expired" || status == "Active") {
								result = false;
							}
						}
		
						return result;
	},
	
	_showFields_: function(el) {
		
						var v  = el.getValue();
						var invoiceFreqNumber = this._get_("invoiceFreqNumber");
						var daysFrequency = this._get_("daysFrequency");
						if (v == __FMBAS_TYPE__.InvoiceFreq._EVERY_) {
							invoiceFreqNumber.setVisible(true);
							daysFrequency.setVisible(true);
						}
						else {
							invoiceFreqNumber.setVisible(false);
							daysFrequency.setVisible(false);
						}
	},
	
	_enableFs_: function(el) {
		
		
						if (el._fpRawValue_ != el._onFocusVal_) {
							if( this._enableIfStatus_() ){
								var ctrl = this._controller_;
								var r = ctrl.getRecord();
				                r.set("financialSourceCode",null);
								r.set("avgMethodName",null)
			                }
						}
	}
});
