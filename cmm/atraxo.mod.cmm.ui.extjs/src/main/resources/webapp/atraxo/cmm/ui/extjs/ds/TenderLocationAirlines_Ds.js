/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.TenderLocationAirlines_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_TenderLocationAirlines_Ds"
	},
	
	
	validators: {
		airlineCode: [{type: 'presence'}],
		volume: [{type: 'presence'}],
		unitCode: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("unitCode", !Ext.isEmpty(getApplication().session.user.volumeUnitCode) ? getApplication().session.user.volumeUnitCode : _ACTIVESUBSIDIARY_.volumeUnitCode);
		this.set("biddingStatus", "New");
		this.set("isValid", 1);
	},
	
	fields: [
		{name:"tenderStatus", type:"string"},
		{name:"tenderLocationId", type:"int", allowNull:true},
		{name:"airlineId", type:"int", allowNull:true},
		{name:"airlineCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"volume", type:"float", allowNull:true},
		{name:"volumePeriod", type:"string"},
		{name:"biddingStatus", type:"string"},
		{name:"notes", type:"string"},
		{name:"isValid", type:"boolean"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.TenderLocationAirlines_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"tenderStatus", type:"string"},
		{name:"tenderLocationId", type:"int", allowNull:true},
		{name:"airlineId", type:"int", allowNull:true},
		{name:"airlineCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"volume", type:"float", allowNull:true},
		{name:"volumePeriod", type:"string"},
		{name:"biddingStatus", type:"string"},
		{name:"notes", type:"string"},
		{name:"isValid", type:"boolean", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.cmm.ui.extjs.ds.TenderLocationAirlines_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"bidVersion", type:"string"},
		{name:"captureBidFromExistingBids", type:"boolean"},
		{name:"captureBidFromHolderMA", type:"boolean"},
		{name:"captureBidFromReceiverMA", type:"boolean"}
	]
});
