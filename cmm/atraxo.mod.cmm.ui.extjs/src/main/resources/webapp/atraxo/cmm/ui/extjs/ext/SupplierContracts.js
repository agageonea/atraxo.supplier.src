Ext.override(atraxo.fmbas.ui.extjs.frame.Suppliers_Ui, {

	_defineDcs_ : function() {
		this.callParent(arguments);

		// our stuff
		this._getBuilder_()
		.addDc("contract", Ext.create(atraxo.cmm.ui.extjs.dc.Contract_Dc, {}))
		.addDc("template", Ext.create(atraxo.cmm.ui.extjs.dc.Template_Dc,{ readOnly:true}))
		.linkDc("contract", "supplier", {fetchMode : "auto",fields : [{childField : "supplierId",parentField : "id"}]})
	},

	_defineElements_ : function() {
		this.callParent(arguments);

		this._getBuilder_()
//		.change("contractsList", {
//			xtype : "cmm_Contract_Dc$TabList",
//			_controller_ : this._dcs_.get("contract")
//		})
		.addButton({name:"buttonOpenHelp", glyph:"xf059@FontAwesome", iconCls:"glyph-yellow", disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"btnContinueSupplier", glyph:"xf064@FontAwesome", iconCls:"glyph-green", disabled:false, handler: this.onContinue,
				stateManager:{ name:"selected_one", dc:"template" }, scope:this})
				
		.addButton({name:"btnOpenCustomerWdw",glyph:"xf0c5@FontAwesome",iconCls:"glyph-yellow", disabled:true, handler: this.onBtnOpenCustomerWdw,
				stateManager:{ name:"selected_one", dc:"contract" , and: function(dc) {return (dc.record.data.status!='Expired');}}, scope:this})
		.addButton({name:"btnGenerateSaleContract", disabled:true, handler: this.onBtnGenerateSaleContract,
				stateManager:{ name:"selected_one", dc:"contract" , and: function(dc) {return (dc.record.data.status!='Expired');}}, scope:this})
		.addButton({name:"btnCancelSupplier", glyph:"xf05e@FontAwesome", iconCls:"glyph-red", disabled:false, handler: this.onCancel, scope:this})
		
		.addDcGridView("template", {name:"TempListSupplier", xtype:"cmm_Template_Dc$List"})
		.addDcGridView("contract", {name:"contractsList", _hasTitle_:true, xtype:"cmm_Contract_Dc$TabList"})		
		.addDcFilterFormView("contract", {name:"contractsFilter", xtype:"cmm_Contract_Dc$Filter"})
		.addWindow({name:"wdwTempSupplier", _hasTitle_:true, width:420, height:360, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("TempListSupplier")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnContinueSupplier"), this._elems_.get("btnCancelSupplier")]}]})
		.addDcFormView("contract", {name:"CustomerSelect", xtype:"cmm_Contract_Dc$SelectCustomer"})
		.addWindow({name:"wdwGenarateSaleContract", _hasTitle_:true, width:290, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("CustomerSelect")],  buttonAlign:"center", listeners:{close: {scope: this, fn: function() { this._cancelContract_()}}}, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnGenerateSaleContract")]}]});
		
		
		
	},
	
	_cancelContract_: function() {
		
		var dc = this._getDc_("contract");
		dc.doCancel();
	},
	
	_showSaleContractDetails_: function(dc,response) {
		
		
		var bundle = "atraxo.mod.cmm";
		var frame = "atraxo.cmm.ui.extjs.frame.ContractCustomer_Ui";
		var generateWindow = this._getWindow_("wdwGenarateSaleContract");

		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				saleContractId: this._getDc_("contract").getParamValue("generatedContractId")
			},
			callback: function (params) {
				this._after_contract_was_generated_(params)
			}
		});
		generateWindow.close();
	},
	
	_afterDefineElements_ : function() {
		this.callParent(arguments);

		this._getBuilder_()
		.change("contractsList", {
			title: "Purchase Contracts"
		})
		.change("buttonOpenHelp", {
			text: "Help",
			tooltip: "Open the contextual documentation"
		})
		.change("wdwTempSupplier", {
			title: "Select contract template",
			tooltip: "Reset"
		})
		.change("btnContinueSupplier", {
			text: "Continue",
			tooltip: "Continue with the definition of the contract header"
		})
		.change("btnCancelSupplier", {
			text: "Cancel",
			tooltip: "Cancel all changes"
		})
		.change("btnOpenCustomerWdw", {
			text: "Generate",
			tooltip: "Generate sale contract"
		})
		.change("btnGenerateSaleContract", {
			text: "Generate sale contract",
			tooltip: "Generate sale contract"
		})
		.change("wdwGenarateSaleContract", {
			title: "Generate sale contract"
		})		
	},
	
	_linkElements_: function() {
		this.callParent(arguments);
		this._getBuilder_()
		.addChildrenTo("detailsTab", ["contractsList"], null, 2)
		.addToolbarTo("contractsList", "tlbSupplierContract");
	},
	
	_defineToolbars_: function() {
		
		this.callParent(arguments);
		this._getBuilder_()
		.beginToolbar("tlbSupplierContract", {dc: "contract"})
			.addNew({glyph:"xf0fe@FontAwesome",iconCls:"glyph-blue"}).addEdit({glyph:"xf040@FontAwesome",iconCls:"glyph-green"}).addDelete({glyph:"xf00d@FontAwesome",iconCls:"glyph-red"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnOpenCustomerWdw"), this._elems_.get("buttonOpenHelp") ])
			.addReports()
		.end();
		
	},
	
	_afterOnReady_ :  function() {
		this._configureViewsAndFilters_();
		var dc = this._getDc_("contract");
		dc.on("onEditIn", function (dc) {
		    this.doEditContract();
			return false;
		}, this);
	},	
		
	doEditContract: function() {
		
		var bundle = "atraxo.mod.cmm";
		var frame = "atraxo.cmm.ui.extjs.frame.Contract_Ui";
		var dc = this._getDc_("contract");
		var record = dc.getRecord();
		var contractId = record.get("id");
		var contractCode = record.get("code");
		
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				contractCode: contractCode
			},
			callback: function (params) {
				this._when_called_for_details_(params);
			}
		});
		
	},
	
	/**
	 * On-Click handler for button btnGenerateSaleContract
	 */
	onBtnGenerateSaleContract: function() {
		var successFn = function(dc,response,serviceName,specs) {
			this._showSaleContractDetails_(dc,response)
		};
		var o={
			name:"generateResellerContract",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("contract").doRpcData(o);
	},
	
	/**
	 * On-Click handler for button btnOpenCustomerWdw
	 */
	onBtnOpenCustomerWdw: function() {
		this._getWindow_("wdwGenarateSaleContract").show();
	},
	
	onBtnSetActive: function() {
		Ext.Msg.show({
				       title : "Note",
				       msg : "Setting the contract as active you confirm that all contract details are captured and the system can set the contract as effective when the contract starting execution day is reached.",
				       icon : Ext.MessageBox.QUESTION,
				       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
				       fn : function(btnId) {
				              if( btnId == "yes" ){
				                     this.setActiveYes();
		              }
		       },
		       scope : this
				});
	},	
	
	onHelpWdw: function() {
		this.openHelp();
	},
	
	openHelp: function() {
		
		var url = Main.urlHelp+"/SupplierContracts.html";
		window.open( url, "SONE_Help");
	},
	
	_afterAfterDefineDcs_: function() {
		var contract = this._getDc_("contract");
		contract.on("afterDoNew", function (dc) {
			this.onShowContractWdw();
			contract.doCancel();
			return false;
				}, this);
		contract.on("recordChange", function (dc) {
            var r = contract.getRecord();
			if (r) {
				var status = r.get("status");
				if (status == __CMM_TYPE__.ContractStatus._EFFECTIVE_ || status == __CMM_TYPE__.ContractStatus._EXPIRED_) {
					contract.canDoDelete = false;
					contract.updateDcState();	
				}
				else {
					contract.canDoDelete = true;
					contract.updateDcState();	
				}
			}
        }, this);
	},
	
	_when_called_from_appMenu_ : function() {
		var detailsTab = this._get_("detailsTab");
		var contractsList = this._get_("contractsList");
		var dc = this._getDc_("contract");
		this._showStackedViewElement_("main", "canvas2");
		this.setActiveTabByTitle(detailsTab,contractsList);
		this.onShowContractWdw();
	},
	
	showNewWindow : function() {
		
		console.log("ok");
	},
	
	onShowContractWdw: function() {	
		this._getWindow_("wdwTempSupplier").show();
		this._getDc_("template").doQuery();
	},
	
	onCancel: function() {
		this._getDc_("contract").doCancel();
		this._getWindow_("wdwTempSupplier").close();
	},
	
	onContinue: function() {
		this._getWindow_("wdwTempSupplier").close();
		this.Cont();
	},
	
	Cont: function() {
		
		var tempList = this._get_("TempListSupplier");
	
		var selection = tempList.getSelectionModel().getSelection();
		var contractType = selection[0].data.contractType;
		var deliverySubType = selection[0].data.deliverySubtype;
		var scope = selection[0].data.scope;
		
		var bundle = "atraxo.mod.cmm";
		var frame = "atraxo.cmm.ui.extjs.frame.Contract_Ui";
		var dc = this._getDc_("contract");
		var record = dc.getRecord();
		
		var supplierRecord = this._getDc_("supplier").getRecord();

		var counterPartyId = supplierRecord.get("buyerId");
		var counterPartyName = supplierRecord.get("buyerName");
		
		var supplierId = supplierRecord.get("id");
		var supplierCode = supplierRecord.get("code");
		
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				type: contractType,
				subType: deliverySubType,
				scope: scope,
				supplierId: supplierId,
				supplierCode: supplierCode,
				counterPartyId: counterPartyId,
                counterPartyName: counterPartyName
			},
			callback: function (params) {
				this._when_do_new_(params);
			}
		});

	}

});
