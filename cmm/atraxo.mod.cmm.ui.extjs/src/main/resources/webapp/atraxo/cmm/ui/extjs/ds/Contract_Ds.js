/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.Contract_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_Contract_Ds"
	},
	
	
	validators: {
		locCode: [{type: 'presence'}],
		validFrom: [{type: 'presence'}],
		validTo: [{type: 'presence'}],
		type: [{type: 'presence'}],
		settCurrCode: [{type: 'presence'}],
		settUnitCode: [{type: 'presence'}],
		invoiceFreq: [{type: 'presence'}],
		avgMethodName: [{type: 'presence'}],
		paymentTerms: [{type: 'presence'}],
		financialSourceCode: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("status", "Draft");
		this.set("product", "Jet A1");
		this.set("limitedTo", "Unspecified");
		this.set("product", "Jet-A1");
		this.set("tax", "Unspecified");
		this.set("isSpot", "Term");
		this.set("paymentRefDay", "Invoice date");
		this.set("invoicePayableBy", "Headquarter");
		this.set("invoiceType", "Paper");
		this.set("vat", "Not applicable");
		this.set("creditTerms", "Open credit");
		this.set("reviewPeriod", "No");
		this.set("dealType", "Buy");
		this.set("period", "Contract period");
		this.set("volumeTolerance", 0);
		this.set("volumeShare", 100);
		this.set("quantityType", "Gross volume");
		this.set("invoiceFreqNumber", 3);
		this.set("isContract", true);
		this.set("bidStatus", "");
		this.set("bidTransmissionStatus", "");
		this.set("approvalStatus", "");
		this.set("bidPayementFreq", "");
		this.set("bidPaymentType", "");
		this.set("hardCopy", false);
		this.set("approvalStatus", "New");
		this.set("periodApprovalStatus", "New");
	},
	
	fields: [
		{name:"dest", type:"string", noFilter:true, noSort:true},
		{name:"settlement", type:"string", noFilter:true, noSort:true},
		{name:"invoicing", type:"string", noFilter:true, noSort:true},
		{name:"price", type:"string", noFilter:true, noSort:true},
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"events", type:"string", noFilter:true, noSort:true},
		{name:"days", type:"string", noFilter:true, noSort:true},
		{name:"daysFrequency", type:"string", noFilter:true, noSort:true},
		{name:"locId", type:"int", allowNull:true},
		{name:"locCode", type:"string"},
		{name:"holderId", type:"int", allowNull:true, noFilter:true},
		{name:"holderCode", type:"string"},
		{name:"supplierId", type:"int", allowNull:true, noFilter:true},
		{name:"cpRefId", type:"string", noFilter:true},
		{name:"cpEntityAlias", type:"string", noFilter:true},
		{name:"supplierCode", type:"string"},
		{name:"isSupplier", type:"boolean"},
		{name:"supplierName", type:"string"},
		{name:"isThirdParty", type:"boolean"},
		{name:"iplId", type:"int", allowNull:true},
		{name:"iplCode", type:"string"},
		{name:"resBuyerId", type:"string"},
		{name:"resBuyerName", type:"string"},
		{name:"contactId", type:"int", allowNull:true},
		{name:"contactName", type:"string"},
		{name:"contVolUnitId", type:"int", allowNull:true},
		{name:"contVolUnitCode", type:"string"},
		{name:"resaleRefId", type:"int", allowNull:true},
		{name:"resaleRefCode", type:"string"},
		{name:"settCurrId", type:"int", allowNull:true},
		{name:"settCurrCode", type:"string"},
		{name:"settUnitId", type:"int", allowNull:true},
		{name:"settUnitCode", type:"string"},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceCode", type:"string"},
		{name:"avgMethodId", type:"int", allowNull:true},
		{name:"avgMethodName", type:"string"},
		{name:"unit", type:"string", noFilter:true, noSort:true},
		{name:"code", type:"string"},
		{name:"dealType", type:"string"},
		{name:"type", type:"string"},
		{name:"subType", type:"string"},
		{name:"scope", type:"string"},
		{name:"isSpot", type:"string"},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"status", type:"string"},
		{name:"limitedTo", type:"string"},
		{name:"product", type:"string"},
		{name:"iataServiceLevel", type:"int", allowNull:true},
		{name:"tax", type:"string"},
		{name:"quantityType", type:"string"},
		{name:"period", type:"string"},
		{name:"volumeTolerance", type:"int", allowNull:true},
		{name:"volumeShare", type:"int", allowNull:true},
		{name:"counterpartyReference", type:"string"},
		{name:"settlementDecimals", type:"int", allowNull:true},
		{name:"exchangeRateOffset", type:"string"},
		{name:"paymentTerms", type:"int", allowNull:true},
		{name:"paymentRefDay", type:"string"},
		{name:"invoiceFreq", type:"string"},
		{name:"invoiceFreqNumber", type:"int", allowNull:true},
		{name:"creditTerms", type:"string"},
		{name:"invoiceType", type:"string"},
		{name:"invoicePayableBy", type:"string"},
		{name:"vat", type:"string"},
		{name:"reviewPeriod", type:"string"},
		{name:"reviewFirstParam", type:"string"},
		{name:"reviewSecondParam", type:"string"},
		{name:"reviewNotification", type:"int", allowNull:true},
		{name:"paymentComment", type:"string"},
		{name:"eventType", type:"string"},
		{name:"awardedVolume", type:"float", allowNull:true},
		{name:"offeredVolume", type:"float", allowNull:true},
		{name:"forex", type:"string"},
		{name:"hardCopy", type:"boolean"},
		{name:"bidStatus", type:"string"},
		{name:"bidTransmissionStatus", type:"string"},
		{name:"approvalStatus", type:"string"},
		{name:"bidPayementFreq", type:"string"},
		{name:"bidPaymentType", type:"string"},
		{name:"isContract", type:"boolean"},
		{name:"readOnly", type:"boolean"},
		{name:"periodApprovalStatus", type:"string"},
		{name:"formTitle", type:"string", noFilter:true, noSort:true},
		{name:"percent", type:"string", noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"customerId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"customerCode", type:"string", noFilter:true, noSort:true},
		{name:"supplierField", type:"string", noFilter:true, noSort:true},
		{name:"shipToList", type:"string", noFilter:true, noSort:true},
		{name:"resaleContract", type:"string", noFilter:true, noSort:true},
		{name:"canBeCompleted", type:"boolean", noFilter:true, noSort:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.Contract_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"dest", type:"string", noFilter:true, noSort:true},
		{name:"settlement", type:"string", noFilter:true, noSort:true},
		{name:"invoicing", type:"string", noFilter:true, noSort:true},
		{name:"price", type:"string", noFilter:true, noSort:true},
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"events", type:"string", noFilter:true, noSort:true},
		{name:"days", type:"string", noFilter:true, noSort:true},
		{name:"daysFrequency", type:"string", noFilter:true, noSort:true},
		{name:"locId", type:"int", allowNull:true},
		{name:"locCode", type:"string"},
		{name:"holderId", type:"int", allowNull:true, noFilter:true},
		{name:"holderCode", type:"string"},
		{name:"supplierId", type:"int", allowNull:true, noFilter:true},
		{name:"cpRefId", type:"string", noFilter:true},
		{name:"cpEntityAlias", type:"string", noFilter:true},
		{name:"supplierCode", type:"string"},
		{name:"isSupplier", type:"boolean", allowNull:true},
		{name:"supplierName", type:"string"},
		{name:"isThirdParty", type:"boolean", allowNull:true},
		{name:"iplId", type:"int", allowNull:true},
		{name:"iplCode", type:"string"},
		{name:"resBuyerId", type:"string"},
		{name:"resBuyerName", type:"string"},
		{name:"contactId", type:"int", allowNull:true},
		{name:"contactName", type:"string"},
		{name:"contVolUnitId", type:"int", allowNull:true},
		{name:"contVolUnitCode", type:"string"},
		{name:"resaleRefId", type:"int", allowNull:true},
		{name:"resaleRefCode", type:"string"},
		{name:"settCurrId", type:"int", allowNull:true},
		{name:"settCurrCode", type:"string"},
		{name:"settUnitId", type:"int", allowNull:true},
		{name:"settUnitCode", type:"string"},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceCode", type:"string"},
		{name:"avgMethodId", type:"int", allowNull:true},
		{name:"avgMethodName", type:"string"},
		{name:"unit", type:"string", noFilter:true, noSort:true},
		{name:"code", type:"string"},
		{name:"dealType", type:"string"},
		{name:"type", type:"string"},
		{name:"subType", type:"string"},
		{name:"scope", type:"string"},
		{name:"isSpot", type:"string"},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"status", type:"string"},
		{name:"limitedTo", type:"string"},
		{name:"product", type:"string"},
		{name:"iataServiceLevel", type:"int", allowNull:true},
		{name:"tax", type:"string"},
		{name:"quantityType", type:"string"},
		{name:"period", type:"string"},
		{name:"volumeTolerance", type:"int", allowNull:true},
		{name:"volumeShare", type:"int", allowNull:true},
		{name:"counterpartyReference", type:"string"},
		{name:"settlementDecimals", type:"int", allowNull:true},
		{name:"exchangeRateOffset", type:"string"},
		{name:"paymentTerms", type:"int", allowNull:true},
		{name:"paymentRefDay", type:"string"},
		{name:"invoiceFreq", type:"string"},
		{name:"invoiceFreqNumber", type:"int", allowNull:true},
		{name:"creditTerms", type:"string"},
		{name:"invoiceType", type:"string"},
		{name:"invoicePayableBy", type:"string"},
		{name:"vat", type:"string"},
		{name:"reviewPeriod", type:"string"},
		{name:"reviewFirstParam", type:"string"},
		{name:"reviewSecondParam", type:"string"},
		{name:"reviewNotification", type:"int", allowNull:true},
		{name:"paymentComment", type:"string"},
		{name:"eventType", type:"string"},
		{name:"awardedVolume", type:"float", allowNull:true},
		{name:"offeredVolume", type:"float", allowNull:true},
		{name:"forex", type:"string"},
		{name:"hardCopy", type:"boolean", allowNull:true},
		{name:"bidStatus", type:"string"},
		{name:"bidTransmissionStatus", type:"string"},
		{name:"approvalStatus", type:"string"},
		{name:"bidPayementFreq", type:"string"},
		{name:"bidPaymentType", type:"string"},
		{name:"isContract", type:"boolean", allowNull:true},
		{name:"readOnly", type:"boolean", allowNull:true},
		{name:"periodApprovalStatus", type:"string"},
		{name:"formTitle", type:"string", noFilter:true, noSort:true},
		{name:"percent", type:"string", noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"customerId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"customerCode", type:"string", noFilter:true, noSort:true},
		{name:"supplierField", type:"string", noFilter:true, noSort:true},
		{name:"shipToList", type:"string", noFilter:true, noSort:true},
		{name:"resaleContract", type:"string", noFilter:true, noSort:true},
		{name:"canBeCompleted", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.cmm.ui.extjs.ds.Contract_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"approvalNote", type:"string"},
		{name:"companyId", type:"int", allowNull:true},
		{name:"generatedContractCode", type:"string"},
		{name:"generatedContractId", type:"int", allowNull:true},
		{name:"remarks", type:"string"},
		{name:"selectedAttachments", type:"string"},
		{name:"submitForApprovalDescription", type:"string"},
		{name:"submitForApprovalResult", type:"boolean"},
		{name:"userIsNotAllowedToView", type:"boolean"}
	]
});
