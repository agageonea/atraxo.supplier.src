/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.OpenTender_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_OpenTender_Ds"
	},
	
	
	fields: [
		{name:"holderId", type:"int", allowNull:true},
		{name:"holderCode", type:"string"},
		{name:"holderName", type:"string"},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"tenderVersion", type:"int", allowNull:true},
		{name:"biddingPeriodFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"biddingPeriodTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"status", type:"string"},
		{name:"biddingStatus", type:"string"},
		{name:"publishedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"source", type:"string"},
		{name:"closed", type:"boolean"},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.OpenTender_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"holderId", type:"int", allowNull:true},
		{name:"holderCode", type:"string"},
		{name:"holderName", type:"string"},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"tenderVersion", type:"int", allowNull:true},
		{name:"biddingPeriodFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"biddingPeriodTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"status", type:"string"},
		{name:"biddingStatus", type:"string"},
		{name:"publishedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"source", type:"string"},
		{name:"closed", type:"boolean", allowNull:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.cmm.ui.extjs.ds.OpenTender_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"selectedLocation", type:"int", allowNull:true}
	]
});
