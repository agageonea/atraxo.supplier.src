/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdate_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.PriceUpdate_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.PriceUpdate_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdate_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.cmm_PriceUpdate_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"reason", dataIndex:"reason", maxLength:255})
			.addLov({name:"location", dataIndex:"locationCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsAreasLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"code", dsField: "locationCode"} ]}})
			.addLov({name:"priceCategory", dataIndex:"priceCategoryName", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.PriceCategoriesLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"name", dsField: "priceCategoryName"} ]}})
			.addNumberField({name:"newPrice", dataIndex:"newPrice", sysDec:"dec_crncy", maxLength:19})
			.addLov({name:"currency", dataIndex:"currencyCode", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"code", dsField: "currencyCode"} ]}})
			.addLov({name:"unit", dataIndex:"unitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MeasurmentUnitsLov_Lov", selectOnFocus:true, maxLength:2,
					retFieldMapping: [{lovField:"code", dsField: "unitCode"} ]}})
			.addDateField({name:"validFrom", dataIndex:"validFrom"})
			.addCombo({ xtype:"combo", name:"approvalStatus", dataIndex:"approvalStatus", store:[ __CMM_TYPE__.BidApprovalStatus._NEW_, __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_, __CMM_TYPE__.BidApprovalStatus._APPROVED_, __CMM_TYPE__.BidApprovalStatus._REJECTED_]})
			.addBooleanField({ name:"published", dataIndex:"published"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdate_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_PriceUpdate_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"reason", dataIndex:"reason", width:200})
		.addTextColumn({ name:"location", dataIndex:"locationCode", width:80})
		.addTextColumn({ name:"priceCategory", dataIndex:"priceCategoryName", width:180})
		.addNumberColumn({ name:"newPrice", dataIndex:"newPrice", width:80, sysDec:"dec_crncy"})
		.addTextColumn({ name:"currency", dataIndex:"currencyCode", width:80})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:60})
		.addDateColumn({ name:"validFrom", dataIndex:"validFrom", width:100, _mask_: Masks.DATE})
		.addTextColumn({ name:"approvalStatus", dataIndex:"approvalStatus", width:110})
		.addTextColumn({ name:"dealType", dataIndex:"dealType", hidden:true, width:120})
		.addTextColumn({ name:"contractStatus", dataIndex:"contractStatus", hidden:true, width:120})
		.addTextColumn({ name:"contractType", dataIndex:"contractType", hidden:true, width:120})
		.addTextColumn({ name:"deliveryPoint", dataIndex:"deliveryPoint", hidden:true, width:120})
		.addBooleanColumn({ name:"published", dataIndex:"published", width:80})
		.addTextColumn({ name:"publishedBy", dataIndex:"publishedByCode", hidden:true, width:120})
		.addDateColumn({ name:"publishedOn", dataIndex:"publishedOn", hidden:true, _mask_: Masks.DATE})
		.addTextColumn({ name:"submitterBy", dataIndex:"submittedByCode", hidden:true, width:120})
		.addDateColumn({ name:"submittedOn", dataIndex:"submittedOn", hidden:true, _mask_: Masks.DATE})
		.addTextColumn({ name:"approvedBy", dataIndex:"approvedByCode", hidden:true, width:120})
		.addDateColumn({ name:"approvedOn", dataIndex:"approvedOn", hidden:true, _mask_: Masks.DATE})
		.addTextColumn({ name:"contractHolder", dataIndex:"contractHolderCode", hidden:true, width:50})
		.addTextColumn({ name:"counterparty", dataIndex:"counterpartyCode", hidden:true, width:50})
		.addTextColumn({ name:"billTo", dataIndex:"billToCode", hidden:true, width:50})
		.addTextColumn({ name:"shipTo", dataIndex:"shipToCode", hidden:true, width:50})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: ApprovalNote ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdate_Dc$ApprovalNote", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PriceUpdate_Dc$ApprovalNote",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.approvalNote}", paramIndex:"approvalNote", width:580, labelAlign:"top", labelStyle:"font-weight:bold", fieldStyle:"margin-top: 5px;"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:580})
		.addPanel({ name:"col1", width:580, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
	}
});

/* ================= EDIT FORM: priceUpdateFourthStep ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdate_Dc$priceUpdateFourthStep", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PriceUpdate_Dc$priceUpdateFourthStep",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addNumberField({name:"newPrice", bind:"{d.newPrice}", dataIndex:"newPrice", width:120, noLabel: true, sysDec:"dec_crncy", maxLength:19,listeners:{
			change:{scope:this, fn:this._check4thStep_}
		}})
		.addTextField({ name:"currencyCode", bind:"{d.currencyCode}", dataIndex:"currencyCode", noEdit:true , width:50, noLabel: true, maxLength:3, style:"margin-left:5px !important"})
		.addLov({name:"unitCode", bind:"{d.unitCode}", dataIndex:"unitCode", width:60, noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, style:"margin-left:5px !important",
			retFieldMapping: [{lovField:"code", dsField: "unitCode"} ],listeners:{
			change:{scope:this, fn:this._check4thStep_}
		}})
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", width:120, noLabel: true,listeners:{
			change:{scope:this, fn:this._check4thStep_}
		}})
		.addTextArea({ name:"reason", bind:"{d.reason}", dataIndex:"reason", noLabel: true, style:"width:100% !important", maxLength:255,listeners:{
			change:{scope:this, fn:this._check4thStep_}
		}})
		.add({name:"priceCurrencyUnit", xtype: "fieldcontainer", layout: {type:"hbox"}, noLabel: true, items: [this._getConfig_("newPrice"),this._getConfig_("currencyCode"),this._getConfig_("unitCode")]})
		.addDisplayFieldText({ name:"enterPriceLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32})
		.addDisplayFieldText({ name:"enterStartingDateLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32})
		.addDisplayFieldText({ name:"enterReasonLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32})
		/* =========== containers =========== */
		.addPanel({ name:"fourthStepForm",  style:"padding:20px", layout: {type:"table", columns:1}})
		.addPanel({ name:"main", autoScroll:true});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("fourthStepForm", ["enterPriceLabel", "priceCurrencyUnit", "enterStartingDateLabel", "validFrom", "enterReasonLabel", "reason"])
		.addChildrenTo("main", ["fourthStepForm"]);
	},
	/* ==================== Business functions ==================== */
	
	_check4thStep_: function(el,newVal,oldVal) {
		
		
						var ctrl = this._controller_;
						var frame = ctrl.getFrame();
						var btnFinish = frame._get_("btnFinish");
						var price = this._get_("newPrice");
						var valPrice = price.getValue();
						var unit = this._get_("unitCode");
						var valUnit = unit.getValue();
						var date = this._get_("validFrom");
						var valDate = date.getValue();
						var reason = this._get_("reason");
						var valReason = reason.getValue();
							
						if (!Ext.isEmpty(valPrice) && !Ext.isEmpty(valUnit) && !Ext.isEmpty(valDate) && !Ext.isEmpty(valReason)) {
							btnFinish.enable();
						}
						else {
							btnFinish.disable();
						}
	}
});

/* ================= EDIT FORM: priceUpdateSecondStep ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdate_Dc$priceUpdateSecondStep", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PriceUpdate_Dc$priceUpdateSecondStep",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"contractHolderCode", bind:"{d.contractHolderCode}", dataIndex:"contractHolderCode", noLabel: true, xtype:"fmbas_UserSubsidiaryLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"subsidiaryId", dsField: "contractHolderId"} ]})
		.addLov({name:"counterpartyCode", bind:"{d.counterpartyCode}", dataIndex:"counterpartyCode", noLabel: true, xtype:"cmm_CounterPartyLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "counterpartyId"} ,{lovField:"code", dsField: "counterpartyCode"} ]})
		.addLov({name:"billToCode", bind:"{d.billToCode}", dataIndex:"billToCode", noLabel: true, xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "billToId"} ,{lovField:"code", dsField: "billToCode"} ]})
		.addLov({name:"shipToCode", bind:"{d.shipToCode}", dataIndex:"shipToCode", noLabel: true, xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "shipToId"} ,{lovField:"code", dsField: "shipToCode"} ]})
		.addToggleField({ name:"contractStatusEffective", bind:"{p.contractStatusEffective}", paramIndex:"contractStatusEffective", noLabel: true, listeners:{change: {fn:function(el,newVal,oldVal) {this._setValues_(el,newVal,oldVal,__CMM_TYPE__.ContractStatus._EFFECTIVE_,'contractStatusValues')}, scope: this}}})
		.addToggleField({ name:"contractStatusExpired", bind:"{p.contractStatusExpired}", paramIndex:"contractStatusExpired", noLabel: true, listeners:{change: {fn:function(el,newVal,oldVal) {this._setValues_(el,newVal,oldVal,__CMM_TYPE__.ContractStatus._EXPIRED_,'contractStatusValues')}, scope: this}}})
		.addToggleField({ name:"contractTypeSales", bind:"{p.contractTypeSales}", paramIndex:"contractTypeSales", noLabel: true, listeners:{change: {fn:function(el,newVal,oldVal) {this._setValues_(el,newVal,oldVal,__CMM_TYPE__.DealType._SELL_,'contractTypeValues')}, scope: this}}})
		.addToggleField({ name:"contractTypePurchase", bind:"{p.contractTypePurchase}", paramIndex:"contractTypePurchase", noLabel: true, listeners:{change: {fn:function(el,newVal,oldVal) {this._setValues_(el,newVal,oldVal,__CMM_TYPE__.DealType._BUY_,'contractTypeValues')}, scope: this}}})
		.addToggleField({ name:"contractForProduct", bind:"{p.contractForProduct}", paramIndex:"contractForProduct", noLabel: true, listeners:{change: {fn:function(el,newVal,oldVal) {this._setValues_(el,newVal,oldVal,__CMM_TYPE__.ContractType._PRODUCT_,'contractForValues')}, scope: this}}})
		.addToggleField({ name:"contractForService", bind:"{p.contractForService}", paramIndex:"contractForService", noLabel: true, listeners:{change: {fn:function(el,newVal,oldVal) {this._setValues_(el,newVal,oldVal,__CMM_TYPE__.ContractType._FUELING_SERVICE_,'contractForValues')}, scope: this}}})
		.addToggleField({ name:"deliveryPointIntoPlane", bind:"{p.deliveryPointIntoPlane}", paramIndex:"deliveryPointIntoPlane", noLabel: true, listeners:{change: {fn:function(el,newVal,oldVal) {this._setValues_(el,newVal,oldVal,__CMM_TYPE__.FuelDelivery._INTO_PLANE_,'deliveryPointValues')}, scope: this}}})
		.addToggleField({ name:"deliveryPointIntoStorage", bind:"{p.deliveryPointIntoStorage}", paramIndex:"deliveryPointIntoStorage", noLabel: true, listeners:{change: {fn:function(el,newVal,oldVal) {this._setValues_(el,newVal,oldVal,__CMM_TYPE__.FuelDelivery._INTO_STORAGE_,'deliveryPointValues')}, scope: this}}})
		.addDisplayFieldText({ name:"indicateLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, colspan:5})
		.addDisplayFieldText({ name:"contractHolderLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32})
		.addDisplayFieldText({ name:"contractStatusLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, colspan:4, style:"padding-left:80px"})
		.addDisplayFieldText({ name:"counterPartyLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32})
		.addDisplayFieldText({ name:"contractTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, colspan:4, style:"padding-left:80px"})
		.addDisplayFieldText({ name:"billToLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32})
		.addDisplayFieldText({ name:"shipToLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32})
		.addDisplayFieldText({ name:"contractForLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, colspan:4, style:"padding-left:80px"})
		.addDisplayFieldText({ name:"deliveryPointLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, colspan:4, style:"padding-left:80px"})
		.addDisplayFieldText({ name:"effectiveLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding-left:80px; float: right"})
		.addDisplayFieldText({ name:"expiredLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float: right; padding-left:20px"})
		.addDisplayFieldText({ name:"salesLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding-left:80px; float: right"})
		.addDisplayFieldText({ name:"purchaseLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float: right; padding-left:20px"})
		.addDisplayFieldText({ name:"productLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding-left:80px; float: right"})
		.addDisplayFieldText({ name:"serviceLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float: right; padding-left:20px"})
		.addDisplayFieldText({ name:"intoPlaneLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding-left:80px; float: right"})
		.addDisplayFieldText({ name:"intoStorageLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float: right; padding-left:20px"})
		.addTextField({ name:"contractStatusValues", bind:"{d.contractStatus}", dataIndex:"contractStatus", maxLength:255, style:"display:none"})
		.addTextField({ name:"contractTypeValues", bind:"{d.dealType}", dataIndex:"dealType", maxLength:255, style:"display:none"})
		.addTextField({ name:"contractForValues", bind:"{d.contractType}", dataIndex:"contractType", maxLength:255, style:"display:none"})
		.addTextField({ name:"deliveryPointValues", bind:"{d.deliveryPoint}", dataIndex:"deliveryPoint", maxLength:255, style:"display:none"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"secondStepForm",  style:"padding:20px", layout: {type:"table", columns:5}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["secondStepForm"])
		.addChildrenTo("secondStepForm", ["indicateLabel", "contractHolderLabel", "contractStatusLabel", "contractHolderCode", "effectiveLabel", "contractStatusEffective", "expiredLabel", "contractStatusExpired", "counterPartyLabel", "contractTypeLabel", "counterpartyCode", "salesLabel", "contractTypeSales", "purchaseLabel", "contractTypePurchase", "billToLabel", "contractForLabel", "billToCode", "productLabel", "contractForProduct", "serviceLabel", "contractForService", "shipToLabel", "deliveryPointLabel", "shipToCode", "intoPlaneLabel", "deliveryPointIntoPlane", "intoStorageLabel", "deliveryPointIntoStorage", "contractStatusValues", "contractTypeValues", "contractForValues", "deliveryPointValues"]);
	},
	/* ==================== Business functions ==================== */
	
	_setValues_: function(el,newVal,oldVal,status,field) {
		
		
						var ctx = this;
		
						var f = function() {
							var theField = ctx._get_(field);
							var fieldValue = theField.getValue(), newValue;
			
							if (el._fpRawValue_ != el._onFocusVal_) {
								newValue = fieldValue.replace(status,"").replace(/^,|,$/g,"");
								if (newVal === 0) {						
									theField.setValue(newValue);
								}
								else {
									if (!Ext.isEmpty(fieldValue)) {
										theField.setValue(newValue.concat(",").concat(status));
									}
									else {
										theField.setValue(newValue.concat(status));
									}
								}
							}
						}
		
						Ext.defer(f, 500, this);
						
	}
});

/* ================= EDIT FORM: priceUpdateFirstStep ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdate_Dc$priceUpdateFirstStep", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PriceUpdate_Dc$priceUpdateFirstStep",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"priceCategoryName", bind:"{d.priceCategoryName}", dataIndex:"priceCategoryName", allowBlank:false, noLabel: true, xtype:"cmm_PriceCategoriesLov_Lov", maxLength:32, labelAlign:"top",
			retFieldMapping: [{lovField:"id", dsField: "priceCategoryId"} ],listeners:{
			change:{scope:this, fn:this._checkCategoryAndLocation_}
		}})
		.addLov({name:"locationCode", bind:"{d.locationCode}", dataIndex:"locationCode", noLabel: true, xtype:"cmm_LocationsLov_Lov", maxLength:25, labelAlign:"top",
			retFieldMapping: [{lovField:"id", dsField: "locationId"} ],listeners:{
			change:{scope:this, fn:this._checkCategoryAndLocation_}
		}})
		.addDisplayFieldText({ name:"priceCategoryLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, colspan:5})
		.addDisplayFieldText({ name:"locationCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32})
		/* =========== containers =========== */
		.addPanel({ name:"firstStepForm",  style:"padding:20px", layout: {type:"table", columns:1}})
		.addPanel({ name:"main", autoScroll:true});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("firstStepForm", ["priceCategoryLabel", "priceCategoryName", "locationCodeLabel", "locationCode"])
		.addChildrenTo("main", ["firstStepForm"]);
	},
	/* ==================== Business functions ==================== */
	
	_checkCategoryAndLocation_: function() {
		
		
						var ctrl = this._controller_;
						var frame = ctrl.getFrame();
						var btnContinueStep1 = frame._get_("btnContinueStep1");
						var priceCategoryName = this._get_("priceCategoryName");
						var val1 = priceCategoryName.getValue();
		
						if (!Ext.isEmpty(val1)) {
							btnContinueStep1.enable();
						}
						else {
							btnContinueStep1.disable();
						}
						
	}
});

/* ================= EDIT FORM: priceUpdateSteps ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.PriceUpdate_Dc$priceUpdateSteps", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_PriceUpdate_Dc$priceUpdateSteps",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLabel({ name:"phase1"})
		.addLabel({ name:"phase2"})
		.addLabel({ name:"phase3"})
		.addLabel({ name:"phase4"})
		/* =========== containers =========== */
		.addPanel({name:"main"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addWizardLabels("main", ["phase1", "phase2", "phase3", "phase4"]);
	}
});
