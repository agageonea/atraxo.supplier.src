/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.ShipToCustomerLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_ShipToCustomerLov_Ds"
	},
	
	
	fields: [
		{name:"custId", type:"int", allowNull:true},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"type", type:"string"},
		{name:"refid", type:"string"},
		{name:"status", type:"string"},
		{name:"iataCode", type:"string"},
		{name:"isCustomer", type:"boolean"},
		{name:"isThirdParty", type:"boolean"},
		{name:"contractId", type:"int", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.ShipToCustomerLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"custId", type:"int", allowNull:true},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"type", type:"string"},
		{name:"refid", type:"string"},
		{name:"status", type:"string"},
		{name:"iataCode", type:"string"},
		{name:"isCustomer", type:"boolean", allowNull:true},
		{name:"isThirdParty", type:"boolean", allowNull:true},
		{name:"contractId", type:"int", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
