/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocation_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.TenderLocation_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.TenderLocation_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocation_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.cmm_TenderLocation_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"location", dataIndex:"locationCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "locationId"} ]}})
			.addLov({name:"country", dataIndex:"countryName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CountryNameLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "countryId"} ]}})
			.addCombo({ xtype:"combo", name:"product", dataIndex:"fuelProduct", store:[ __CMM_TYPE__.Product._JET_A_, __CMM_TYPE__.Product._JET_A1_, __CMM_TYPE__.Product._TS_1_, __CMM_TYPE__.Product._F_34_, __CMM_TYPE__.Product._JP_8_, __CMM_TYPE__.Product._RP_4_, __CMM_TYPE__.Product._T1_, __CMM_TYPE__.Product._A1_BIO_, __CMM_TYPE__.Product._CAT_2_, __CMM_TYPE__.Product._AVGAS_, __CMM_TYPE__.Product._AG_1_HL_, __CMM_TYPE__.Product._AG_80_, __CMM_TYPE__.Product._JA_1_A_, __CMM_TYPE__.Product._JAA_, __CMM_TYPE__.Product._JP_5_, __CMM_TYPE__.Product._NON_, __CMM_TYPE__.Product._OTH_]})
			.addNumberField({name:"volume", dataIndex:"volume", sysDec:"dec_unit", maxLength:19})
			.addLov({name:"volumeUnit", dataIndex:"volumeUnitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MeasurmentUnitsLov_Lov", selectOnFocus:true, maxLength:2,
					retFieldMapping: [{lovField:"id", dsField: "volumeUnitId"} ]}})
			.addCombo({ xtype:"combo", name:"period", dataIndex:"period", store:[ __CMM_TYPE__.Period._CONTRACT_PERIOD_, __CMM_TYPE__.Period._MONTHLY_, __CMM_TYPE__.Period._YEARLY_]})
			.addDateField({name:"agreementFrom", dataIndex:"agreementFrom"})
			.addDateField({name:"agreementTo", dataIndex:"agreementTo"})
			.addCombo({ xtype:"combo", name:"flightServiceType", dataIndex:"flightServiceType", store:[ __CMM_TYPE__.FlightServiceType._AD_HOC_, __CMM_TYPE__.FlightServiceType._SCHEDULED_, __CMM_TYPE__.FlightServiceType._GENERAL_AVIATION_]})
			.addCombo({ xtype:"combo", name:"biddingStatus", dataIndex:"locationBiddingStatus", store:[ __CMM_TYPE__.BiddingStatus._NEW_, __CMM_TYPE__.BiddingStatus._IN_NEGOTIATION_, __CMM_TYPE__.BiddingStatus._FINISHED_, __CMM_TYPE__.BiddingStatus._NO_BID_]})
			.addCombo({ xtype:"combo", name:"taxType", dataIndex:"taxType", store:[ __CMM_TYPE__.TaxType._UNSPECIFIED_, __CMM_TYPE__.TaxType._BONDED_, __CMM_TYPE__.TaxType._DOMESTIC_, __CMM_TYPE__.TaxType._FOREIGN_TRADE_ZONE_, __CMM_TYPE__.TaxType._DUTY_FREE_, __CMM_TYPE__.TaxType._DUTY_PAID_, __CMM_TYPE__.TaxType._OTHER_]})
			.addCombo({ xtype:"combo", name:"deliveryPoint", dataIndex:"deliveryPoint", store:[ __CMM_TYPE__.ContractSubType._NA_, __CMM_TYPE__.ContractSubType._INTO_PLANE_, __CMM_TYPE__.ContractSubType._EX_HYDRANT_, __CMM_TYPE__.ContractSubType._AIRLINE_SUPPLY_, __CMM_TYPE__.ContractSubType._INTO_STORAGE_, __CMM_TYPE__.ContractSubType._SERVICE_]})
			.addBooleanField({ name:"isValid", dataIndex:"isValid"})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocation_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.cmm_TenderLocation_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addComboColumn({name:"biddingStatus", dataIndex:"locationBiddingStatus", width:120, noUpdate: true, 
			editor:{xtype:"combo", mode: 'local', noUpdate:true, triggerAction:'all', forceSelection:true, store:[ __CMM_TYPE__.BiddingStatus._NEW_, __CMM_TYPE__.BiddingStatus._IN_NEGOTIATION_, __CMM_TYPE__.BiddingStatus._FINISHED_, __CMM_TYPE__.BiddingStatus._NO_BID_]},  renderer:function(value, metaData) {return this._badgifyColumn_(value); }})
		.addLov({name:"location", dataIndex:"locationCode", width:85, noUpdate: true, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_LocationsLov_Lov", allowBlank:false, noUpdate:true, maxLength:25,
				retFieldMapping: [{lovField:"id", dsField: "locationId"} ]}})
		.addTextColumn({name:"country", dataIndex:"countryName", width:200, noUpdate: true, maxLength:100, 
			editor: { xtype:"textfield", noUpdate:true, maxLength:100}})
		.addComboColumn({name:"product", dataIndex:"fuelProduct", width:80, noUpdate: true, 
			editor:{xtype:"combo", mode: 'local', noUpdate:true, triggerAction:'all', forceSelection:true, store:[ __CMM_TYPE__.Product._JET_A_, __CMM_TYPE__.Product._JET_A1_, __CMM_TYPE__.Product._TS_1_, __CMM_TYPE__.Product._F_34_, __CMM_TYPE__.Product._JP_8_, __CMM_TYPE__.Product._RP_4_, __CMM_TYPE__.Product._T1_, __CMM_TYPE__.Product._A1_BIO_, __CMM_TYPE__.Product._CAT_2_, __CMM_TYPE__.Product._AVGAS_, __CMM_TYPE__.Product._AG_1_HL_, __CMM_TYPE__.Product._AG_80_, __CMM_TYPE__.Product._JA_1_A_, __CMM_TYPE__.Product._JAA_, __CMM_TYPE__.Product._JP_5_, __CMM_TYPE__.Product._NON_, __CMM_TYPE__.Product._OTH_]}})
		.addNumberColumn({name:"volume", dataIndex:"volume", width:200, noUpdate: true, sysDec:"dec_unit", maxLength:19, align:"right" })
		.addLov({name:"volumeUnit", dataIndex:"volumeUnitCode", width:60, noUpdate: true, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_MeasurmentUnitsLov_Lov", allowBlank:false, noUpdate:true, maxLength:2,
				retFieldMapping: [{lovField:"id", dsField: "volumeUnitId"} ]}})
		.addComboColumn({name:"period", dataIndex:"period", hidden:true, width:110, noUpdate: true, 
			editor:{xtype:"combo", mode: 'local', noUpdate:true, triggerAction:'all', forceSelection:true, store:[ __CMM_TYPE__.Period._CONTRACT_PERIOD_, __CMM_TYPE__.Period._MONTHLY_, __CMM_TYPE__.Period._YEARLY_]}})
		.addDateColumn({name:"agreementFrom", dataIndex:"agreementFrom", width:110, noUpdate: true, allowBlank: false, _mask_: Masks.DATE })
		.addDateColumn({name:"agreementTo", dataIndex:"agreementTo", width:100, noUpdate: true, allowBlank: false, _mask_: Masks.DATE })
		.addComboColumn({name:"flightServiceType", dataIndex:"flightServiceType", width:130, noUpdate: true, allowBlank: false, 
			editor:{xtype:"combo", mode: 'local', allowBlank:false, noUpdate:true, triggerAction:'all', forceSelection:true, store:[ __CMM_TYPE__.FlightServiceType._AD_HOC_, __CMM_TYPE__.FlightServiceType._SCHEDULED_, __CMM_TYPE__.FlightServiceType._GENERAL_AVIATION_]}})
		.addBooleanColumn({name:"adHoc", dataIndex:"adHocLoc", hidden:true, width:75, noUpdate: true})
		.addDateColumn({name:"bidPeriodFrom", dataIndex:"bidPeriodFrom", hidden:true, width:115, noUpdate: true, allowBlank: false, _mask_: Masks.DATE })
		.addDateColumn({name:"bidPeriodTo", dataIndex:"bidPeriodTo", hidden:true, width:100, noUpdate: true, allowBlank: false, _mask_: Masks.DATE })
		.addDateColumn({name:"bidOpeningDate", dataIndex:"bidOpeningDate", hidden:true, width:120, noUpdate: true, allowBlank: false, _mask_: Masks.DATE })
		.addNumberColumn({name:"biddingRound", dataIndex:"round", hidden:true, width:110, noUpdate: true, maxLength:3, align:"right",  sortable:false })
		.addComboColumn({name:"taxType", dataIndex:"taxType", width:150, noUpdate: true, 
			editor:{xtype:"combo", mode: 'local', noUpdate:true, triggerAction:'all', forceSelection:true, store:[ __CMM_TYPE__.TaxType._UNSPECIFIED_, __CMM_TYPE__.TaxType._BONDED_, __CMM_TYPE__.TaxType._DOMESTIC_, __CMM_TYPE__.TaxType._FOREIGN_TRADE_ZONE_, __CMM_TYPE__.TaxType._DUTY_FREE_, __CMM_TYPE__.TaxType._DUTY_PAID_, __CMM_TYPE__.TaxType._OTHER_]}})
		.addComboColumn({name:"deliveryPoint", dataIndex:"deliveryPoint", width:140, noUpdate: true, 
			editor:{xtype:"combo", mode: 'local', noUpdate:true, triggerAction:'all', forceSelection:true, store:[ __CMM_TYPE__.ContractSubType._NA_, __CMM_TYPE__.ContractSubType._INTO_PLANE_, __CMM_TYPE__.ContractSubType._EX_HYDRANT_, __CMM_TYPE__.ContractSubType._AIRLINE_SUPPLY_, __CMM_TYPE__.ContractSubType._INTO_STORAGE_, __CMM_TYPE__.ContractSubType._SERVICE_]}})
		.addTextColumn({name:"packageIdentifier", dataIndex:"packageIdentifier", hidden:true, width:200, noUpdate: true, maxLength:100, 
			editor: { xtype:"textfield", noUpdate:true, maxLength:100}})
		.addTextColumn({name:"transmissionStatus", dataIndex:"transmissionStatus", hidden:true, width:135, noEdit: true, maxLength:32})
		.addComboColumn({name:"volumeOffer", dataIndex:"volumeOffer", hidden:true, width:100, noUpdate: true, 
			editor:{xtype:"combo", mode: 'local', noUpdate:true, triggerAction:'all', forceSelection:true, store:[ __FMBAS_TYPE__.TenderVolumeOffer._PER_AIRLINE_, __FMBAS_TYPE__.TenderVolumeOffer._PER_LOCATION_, __FMBAS_TYPE__.TenderVolumeOffer._UNDEFINED_]}})
		.addBooleanColumn({name:"isValid", dataIndex:"isValid", width:60, noEdit: true})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_badgifyColumn_: function(value) {
		
						var badgeCls = "sone-badge-cyan";
						if (value === __CMM_TYPE__.BiddingStatus._IN_NEGOTIATION_) {
							badgeCls = "sone-badge-yellow";
						}
						else if (value === __CMM_TYPE__.BiddingStatus._FINISHED_) {
							badgeCls = "sone-badge-blue";
						}
						else if (value === __CMM_TYPE__.BiddingStatus._NO_BID_) {
							badgeCls = "sone-badge-red";
						}
						else if (value === __CMM_TYPE__.TenderStatus._NEW_) {
							badgeCls = "sone-badge-green";
						}
						var badge = "<div class='sone-badge "+badgeCls+"'>"+value+"</div>";
						return badge;
	},
	
	_afterDefineElements_: function() {
		
						if(_SYSTEMPARAMETERS_.etenderclient === "true"){
							this._columns_.getByKey("agreementFrom").hidden = true ;
							this._columns_.getByKey("agreementTo").hidden = true ;
						}
	}
});

/* ================= EDIT-GRID: Edit ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocation_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.cmm_TenderLocation_Dc$Edit",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"location", dataIndex:"locationCode", width:50, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_LocationsLov_Lov", maxLength:25,
				retFieldMapping: [{lovField:"id", dsField: "locationId"} ]},  flex:1})
		.addComboColumn({name:"product", dataIndex:"fuelProduct", width:100, allowBlank: false, 
			editor:{xtype:"combo", mode: 'local', allowBlank:false, triggerAction:'all', forceSelection:true, store:[ __CMM_TYPE__.Product._JET_A_, __CMM_TYPE__.Product._JET_A1_, __CMM_TYPE__.Product._TS_1_, __CMM_TYPE__.Product._F_34_, __CMM_TYPE__.Product._JP_8_, __CMM_TYPE__.Product._RP_4_, __CMM_TYPE__.Product._T1_, __CMM_TYPE__.Product._A1_BIO_, __CMM_TYPE__.Product._CAT_2_, __CMM_TYPE__.Product._AVGAS_, __CMM_TYPE__.Product._AG_1_HL_, __CMM_TYPE__.Product._AG_80_, __CMM_TYPE__.Product._JA_1_A_, __CMM_TYPE__.Product._JAA_, __CMM_TYPE__.Product._JP_5_, __CMM_TYPE__.Product._NON_, __CMM_TYPE__.Product._OTH_]},  flex:1})
		.addNumberColumn({name:"volume", dataIndex:"volume", width:120, sysDec:"dec_unit", maxLength:19, align:"right",  flex:1 })
		.addLov({name:"volumeUnit", dataIndex:"volumeUnitCode", width:50, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
				retFieldMapping: [{lovField:"id", dsField: "volumeUnitId"} ]},  flex:1})
		.addComboColumn({name:"period", dataIndex:"period", width:100, 
			editor:{xtype:"combo", mode: 'local', triggerAction:'all', forceSelection:true, store:[ __CMM_TYPE__.Period._CONTRACT_PERIOD_, __CMM_TYPE__.Period._MONTHLY_, __CMM_TYPE__.Period._YEARLY_]},  flex:1})
		.addDateColumn({name:"agreementFrom", dataIndex:"agreementFrom", width:100, _mask_: Masks.DATE,  flex:1 })
		.addDateColumn({name:"agreementTo", dataIndex:"agreementTo", width:100, _mask_: Masks.DATE,  flex:1 })
		.addComboColumn({name:"deliveryPoint", dataIndex:"deliveryPoint", width:70, 
			editor:{xtype:"combo", mode: 'local', triggerAction:'all', forceSelection:true, store:[ __CMM_TYPE__.ContractSubType._NA_, __CMM_TYPE__.ContractSubType._INTO_PLANE_, __CMM_TYPE__.ContractSubType._EX_HYDRANT_, __CMM_TYPE__.ContractSubType._AIRLINE_SUPPLY_, __CMM_TYPE__.ContractSubType._INTO_STORAGE_, __CMM_TYPE__.ContractSubType._SERVICE_]},  flex:1})
		.addComboColumn({name:"taxType", dataIndex:"taxType", width:70, allowBlank: false, 
			editor:{xtype:"combo", mode: 'local', allowBlank:false, triggerAction:'all', forceSelection:true, store:[ __CMM_TYPE__.TaxType._UNSPECIFIED_, __CMM_TYPE__.TaxType._BONDED_, __CMM_TYPE__.TaxType._DOMESTIC_, __CMM_TYPE__.TaxType._FOREIGN_TRADE_ZONE_, __CMM_TYPE__.TaxType._DUTY_FREE_, __CMM_TYPE__.TaxType._DUTY_PAID_, __CMM_TYPE__.TaxType._OTHER_]},  flex:1, listeners:{change: {scope: this, fn:function(el) {this._controller_.getFrame()._applyStateAllButtons_()}}}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= GRID: AssignList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocation_Dc$AssignList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_TenderLocation_Dc$AssignList",
	_noPaginator_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"locationCode", dataIndex:"locationCode", width:50,  flex:1})
		.addTextColumn({ name:"locationName", dataIndex:"locationName", width:120,  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: contractPeriodForm ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocation_Dc$contractPeriodForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderLocation_Dc$contractPeriodForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDateField({name:"agreementFrom", bind:"{d.agreementFrom}", dataIndex:"agreementFrom", _enableFn_: this._enableIfStatus_, labelAlign:"top"})
		.addDateField({name:"agreementTo", bind:"{d.agreementTo}", dataIndex:"agreementTo", _enableFn_: this._enableIfStatus_, labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:170, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["agreementFrom", "agreementTo"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableIfStatus_: function() {
		
						var dc 		= this._controller_;
						var record 	= dc.getRecord();
						var status 	= record.get("tenderStatus");				
		
						return status === __CMM_TYPE__.TenderStatus._DRAFT_;
	}
});

/* ================= EDIT FORM: biddingPeriodForm ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocation_Dc$biddingPeriodForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderLocation_Dc$biddingPeriodForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDateField({name:"bidPeriodFrom", bind:"{d.bidPeriodFrom}", dataIndex:"bidPeriodFrom", noUpdate:true, labelAlign:"top"})
		.addDateField({name:"bidPeriodTo", bind:"{d.bidPeriodTo}", dataIndex:"bidPeriodTo", noUpdate:true, labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:170, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:170, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["bidPeriodFrom", "bidPeriodTo"]);
	}
});

/* ================= EDIT FORM: Header ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocation_Dc$Header", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderLocation_Dc$Header",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"formTitle", bind:"{d.formTitle}", dataIndex:"formTitle", maxLength:16})
		.addCombo({ xtype:"combo", name:"fuelProduct", bind:"{d.fuelProduct}", dataIndex:"fuelProduct", _enableFn_: this._enableIfStatus_, width:50, store:[ __CMM_TYPE__.Product._JET_A_, __CMM_TYPE__.Product._JET_A1_, __CMM_TYPE__.Product._TS_1_, __CMM_TYPE__.Product._F_34_, __CMM_TYPE__.Product._JP_8_, __CMM_TYPE__.Product._RP_4_, __CMM_TYPE__.Product._T1_, __CMM_TYPE__.Product._A1_BIO_, __CMM_TYPE__.Product._CAT_2_, __CMM_TYPE__.Product._AVGAS_, __CMM_TYPE__.Product._AG_1_HL_, __CMM_TYPE__.Product._AG_80_, __CMM_TYPE__.Product._JA_1_A_, __CMM_TYPE__.Product._JAA_, __CMM_TYPE__.Product._JP_5_, __CMM_TYPE__.Product._NON_, __CMM_TYPE__.Product._OTH_], cls:"sone-flat-kpi sone-flat-combo"})
		.addNumberField({name:"locationVolume", bind:"{d.volume}", dataIndex:"volume", _enableFn_: this._enableIfStatusAndHasTotalVolume_, sysDec:"dec_unit", maxLength:19, cls:"sone-flat-kpi sone-flat-field"})
		.addLov({name:"volumeUnit", bind:"{d.volumeUnitCode}", dataIndex:"volumeUnitCode", _enableFn_: this._enableIfStatusAndHasTotalVolume_, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, cls:"sone-flat-kpi sone-flat-combo", style:"width: 60px !important",
			retFieldMapping: [{lovField:"id", dsField: "volumeUnitId"} ]})
		.addCombo({ xtype:"combo", name:"period", bind:"{d.period}", dataIndex:"period", _enableFn_: this._enableIfStatusAndHasTotalVolume_, width:120, store:[ __CMM_TYPE__.Period._CONTRACT_PERIOD_, __CMM_TYPE__.Period._MONTHLY_, __CMM_TYPE__.Period._YEARLY_], cls:"sone-flat-kpi sone-flat-combo"})
		.addDisplayFieldText({ name:"locationBiddingStatus", bind:"{d.locationBiddingStatus}", dataIndex:"locationBiddingStatus", maxLength:32})
		.addCombo({ xtype:"combo", name:"flightServiceType", bind:"{d.flightServiceType}", dataIndex:"flightServiceType", _enableFn_: this._enableIfStatus_, allowBlank:false, store:[ __CMM_TYPE__.FlightServiceType._AD_HOC_, __CMM_TYPE__.FlightServiceType._SCHEDULED_, __CMM_TYPE__.FlightServiceType._GENERAL_AVIATION_], cls:"sone-flat-kpi sone-flat-combo-big"})
		.addToggleField({ name:"adHocLoc", bind:"{d.adHocLoc}", dataIndex:"adHocLoc", _enableFn_: this._enableIfStatus_})
		.addDateField({name:"agreementFrom", bind:"{d.agreementFrom}", dataIndex:"agreementFrom", _enableFn_: this._enableIfStatus_, noLabel: true})
		.addDateField({name:"agreementTo", bind:"{d.agreementTo}", dataIndex:"agreementTo", _enableFn_: this._enableIfStatus_, noLabel: true})
		.addPopoverTextField({name:"contractPeriod", hPos:"r", vPos:"b", fieldsList:["agreementFrom","agreementTo"], formatList:[], editorForm:"cmm_TenderLocation_Dc$contractPeriodForm", fieldsValueSeparator:" - "})
		.addLov({name:"refueler", bind:"{d.refuelerCode}", dataIndex:"refuelerCode", _enableFn_: this._enableIfStatus_, width:100, noLabel: true, xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"code", dsField: "refuelerCode"} ]})
		.addCombo({ xtype:"combo", name:"deliveryPoint", bind:"{d.deliveryPoint}", dataIndex:"deliveryPoint", _enableFn_: this._enableIfStatus_, width:120, noLabel: true, store:[ __CMM_TYPE__.ContractSubType._NA_, __CMM_TYPE__.ContractSubType._INTO_PLANE_, __CMM_TYPE__.ContractSubType._EX_HYDRANT_, __CMM_TYPE__.ContractSubType._AIRLINE_SUPPLY_, __CMM_TYPE__.ContractSubType._INTO_STORAGE_, __CMM_TYPE__.ContractSubType._SERVICE_]})
		.addNumberField({name:"iataServiceLevel", bind:"{d.iataServiceLevel}", dataIndex:"iataServiceLevel", _enableFn_: this._enableIfStatus_, width:45, noLabel: true, maxLength:1, cls:"number-with-spinner", maxValue:4, minValue:1})
		.addCombo({ xtype:"combo", name:"taxType", bind:"{d.taxType}", dataIndex:"taxType", _enableFn_: this._enableIfStatus_, width:150, noLabel: true, store:[ __CMM_TYPE__.TaxType._UNSPECIFIED_, __CMM_TYPE__.TaxType._BONDED_, __CMM_TYPE__.TaxType._DOMESTIC_, __CMM_TYPE__.TaxType._FOREIGN_TRADE_ZONE_, __CMM_TYPE__.TaxType._DUTY_FREE_, __CMM_TYPE__.TaxType._DUTY_PAID_, __CMM_TYPE__.TaxType._OTHER_]})
		.addTextField({ name:"packageIdentifier", bind:"{d.packageIdentifier}", dataIndex:"packageIdentifier", _enableFn_: this._enableIfStatus_, noLabel: true, maxLength:100})
		.addTextField({ name:"ASTMFuelSpec", bind:"{d.astmSpecification}", dataIndex:"astmSpecification", _enableFn_: this._enableIfStatus_, noLabel: true, maxLength:100})
		.addTextField({ name:"operatingHours", bind:"{d.operatingHours}", dataIndex:"operatingHours", _enableFn_: this._enableIfStatus_, noLabel: true, maxLength:100})
		.addTextField({ name:"titleTransfer", bind:"{d.titleTransfer}", dataIndex:"titleTransfer", _enableFn_: this._enableIfStatus_, width:120, noLabel: true, maxLength:100})
		.addNumberField({name:"biddingRound", bind:"{d.round}", dataIndex:"round", noUpdate:true, width:30, noLabel: true, maxLength:3})
		.addDateField({name:"bidPeriodFrom", bind:"{d.bidPeriodFrom}", dataIndex:"bidPeriodFrom", noUpdate:true, width:85, noLabel: true})
		.addDateField({name:"bidPeriodTo", bind:"{d.bidPeriodTo}", dataIndex:"bidPeriodTo", noUpdate:true, width:85, noLabel: true})
		.addPopoverTextField({name:"biddingPeriod", hPos:"r", vPos:"b", fieldsList:["bidPeriodFrom","bidPeriodTo"], formatList:[], editorForm:"cmm_TenderLocation_Dc$biddingPeriodForm", fieldsValueSeparator:" - "})
		.addDateField({name:"bidOpeningDate", bind:"{d.bidOpeningDate}", dataIndex:"bidOpeningDate", noUpdate:true, width:80, noLabel: true})
		.addDisplayFieldText({ name:"productLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"contractPeriodLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"contractToLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:20px"})
		.addDisplayFieldText({ name:"refuelerLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"deliveryPointLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"iataServiceLevelLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"taxTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"packageIdentifierLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"ASTMFuelSpecLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"operatingHoursLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"titleTransferLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"bidRoundLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"bidPeriodLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"bidOpeningDateLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.add({name:"row", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("formTitle")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"c1", width:300, defaults: { labelAlign:"top", style:"width:100px !important"}, layout:"anchor"})
		.addPanel({ name:"c2", width:300,  _parentCssCls_:"sone-combined-kpi-first", defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c3", width:300,  _parentCssCls_:"sone-combined-kpi-last", defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c4", width:300, defaults: { labelAlign:"top", style:"width:120px !important"}, layout:"anchor"})
		.addPanel({ name:"c5", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c6", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c7", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"titleAndKpi",  style:"margin-bottom:10px;"})
		.addPanel({ name:"title", width:300,  cls:"sone-title-panel", layout:"anchor"})
		.addPanel({ name:"p5",  cls:"sone-kpi-panel", layout: {type:"table"}})
		.addPanel({ name:"table", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left: 40px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3",  style:"margin-left: 40px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table4",  style:"margin-left: 40px", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["titleAndKpi", "table"])
		.addChildrenTo("c1", ["fuelProduct"])
		.addChildrenTo("c2", ["locationVolume"])
		.addChildrenTo("c3", ["volumeUnit"])
		.addChildrenTo("c4", ["period"])
		.addChildrenTo("c5", ["locationBiddingStatus"])
		.addChildrenTo("c6", ["flightServiceType"])
		.addChildrenTo("c7", ["adHocLoc"])
		.addChildrenTo("titleAndKpi", ["title", "p5"])
		.addChildrenTo("title", ["row"])
		.addChildrenTo("p5", ["c1", "c2", "c3", "c4", "c5", "c6", "c7"])
		.addChildrenTo("table", ["table1", "table2", "table3", "table4"])
		.addChildrenTo("table1", ["contractPeriodLabel", "contractPeriod", "deliveryPointLabel", "deliveryPoint", "refuelerLabel", "refueler"])
		.addChildrenTo("table2", ["iataServiceLevelLabel", "iataServiceLevel", "taxTypeLabel", "taxType", "titleTransferLabel", "titleTransfer"])
		.addChildrenTo("table3", ["bidRoundLabel", "biddingRound", "bidPeriodLabel", "biddingPeriod", "bidOpeningDateLabel", "bidOpeningDate"])
		.addChildrenTo("table4", ["packageIdentifierLabel", "packageIdentifier", "ASTMFuelSpecLabel", "ASTMFuelSpec", "operatingHoursLabel", "operatingHours"]);
	},
	/* ==================== Business functions ==================== */
	
	_afterDefineElements_: function() {
		
						this._getBuilder_().change("titleAndKpi",{
							layout: {
						        type: "hbox",
						        align: "stretch"
						    },
						    defaults: {
						        flex: 1,
								height: 60
						    }
						});
						
						this._controller_.on("afterDoServiceSuccess", function() {					
							this._applyStates_(this._controller_.getRecord());
						},this);
	},
	
	_afterApplyStates_: function() {
		
		                var field = this._controller_.getRecord();
		                if(field){
		                    var locCode = field.get("locationCode");
		                    var locationName = field.get("locationName");
		                    var formTitle = this._get_("formTitle");                
		                    var originalTitleLabel = formTitle.fieldLabel;
							if (!Ext.isEmpty(formTitle) && !Ext.isEmpty(formTitle.labelEl)) {
								formTitle.labelEl.update("<i class='fa fa-envelope'></i> " + originalTitleLabel + locCode + ", " + locationName);
							}
		                }
						
						this.resizeComboKPi("fuelProduct",120);
						this.resizeInputKPi("locationVolume",120);
						this.resizeComboKPi("volumeUnit",120);
						this.resizeComboKPi("period",200);
						this.resizeComboKPi("volumeUnit",60);
						this.resizeComboKPi("flightServiceType",150);
						
	},
	
	resizeInputKPi: function(fieldName,width) {
		
						var field = this._get_(fieldName);
						if (!Ext.isEmpty(field.inputEl)) {
							var inputElId = field.inputEl.dom.id;
							var inputWidth = width;
							document.getElementById(inputElId).style.width = inputWidth+"px";
						}
	},
	
	resizeComboKPi: function(fieldName,width) {
		
						var field = this._get_(fieldName);
						if (!Ext.isEmpty(field.inputEl) && !Ext.isEmpty(field.bodyEl)) {
							var inputElId = field.inputEl.dom.id;
							var bodyEl = field.bodyEl.dom.id;
							var inputWidth = width;
						
							document.getElementById(bodyEl).style.maxWidth = inputWidth+"px";
							document.getElementById(bodyEl).firstChild.style.width = inputWidth+"px";				
							document.getElementById(inputElId).style.width = inputWidth-50+"px";
						}
		
	},
	
	_enableIfStatus_: function() {
		
						var dc 		= this._controller_;
						var record 	= dc.getRecord();
						var status 	= record.get("tenderStatus");				
		
						return status==="Draft";
	},
	
	_enableIfStatusAndHasTotalVolume_: function() {
		
						var dc 		= this._controller_;
						var record 	= dc.getRecord();
						var status 	= record.get("tenderStatus");
						var hasTotalVolume 	= record.get("hasTotalVolume");
		
						return status=="Draft" && hasTotalVolume;
	}
});

/* ================= EDIT FORM: PaymentConditions ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocation_Dc$PaymentConditions", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderLocation_Dc$PaymentConditions",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"paymentCurr", bind:"{d.settlementCurrencyCode}", dataIndex:"settlementCurrencyCode", _enableFn_: this._enableIfStatus_, width:60, noLabel: true, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3,
			retFieldMapping: [{lovField:"id", dsField: "settlementCurrencyId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ],listeners:{
			fpchange:{scope:this, fn:this._enableFinancialSource_}
		}})
		.addLov({name:"pricingUnit", bind:"{d.settlementUnitCode}", dataIndex:"settlementUnitCode", _enableFn_: this._enableIfStatus_, width:60, noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "settlementUnitId"} ]})
		.addCombo({ xtype:"combo", name:"paymentMethod", bind:"{d.paymentMethod}", dataIndex:"paymentMethod", _enableFn_: this._enableIfStatus_, width:100, noLabel: true, store:[ __CMM_TYPE__.PaymentType._CONTRACT_, __CMM_TYPE__.PaymentType._FUEL_CARD_, __CMM_TYPE__.PaymentType._CREDIT_CARD_, __CMM_TYPE__.PaymentType._CASH_]})
		.addCombo({ xtype:"combo", name:"bankGuarantee", bind:"{d.depositRequired}", dataIndex:"depositRequired", _enableFn_: this._enableIfStatus_, width:80, noLabel: true, store:[ __CMM_TYPE__.YesNo._True_, __CMM_TYPE__.YesNo._False_]})
		.addLov({name:"finanacialSource", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", _enableFn_: this._enableIfPaymentCurrency_, width:100, noLabel: true, xtype:"fmbas_ExchangeRateToFinancialSourceAndAvgFilteredLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"} ],
			filterFieldMapping: [{lovField:"currencyId", dsField: "settlementCurrencyId"} ],listeners:{
			fpchange:{scope:this, fn:this._enableAvgMethod_},
			expand:{scope:this, fn:this._filterDistinctFinancialSource_}
		}})
		.addLov({name:"averagingMethod", bind:"{d.averagingMethodName}", dataIndex:"averagingMethodName", _enableFn_: this._enableIfFinancialSource_, width:250, noLabel: true, xtype:"fmbas_ExchangeRateToAvgMthdFilteredLov_Lov", maxLength:50,
			retFieldMapping: [{lovField:"avgMthdId", dsField: "averagingMethodId"} ],
			filterFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"}, {lovField:"currencyId", dsField: "settlementCurrencyId"} ],listeners:{
			expand:{scope:this, fn:this._filterDistinctAvgMthd_}
		}})
		.addCombo({ xtype:"combo", name:"offset", bind:"{d.exchangeRateOffset}", dataIndex:"exchangeRateOffset", _enableFn_: this._enableIfStatus_, width:100, noLabel: true, store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_], colspan:3})
		.addCombo({ xtype:"combo", name:"creditTerms", bind:"{d.creditTerms}", dataIndex:"creditTerms", _enableFn_: this._enableIfStatus_, width:110, noLabel: true, store:[ __CMM_TYPE__.PaymentChoise._OPEN_CREDIT_, __CMM_TYPE__.PaymentChoise._PREPAYMENT_], colspan:7})
		.addNumberField({name:"paymentTerms", bind:"{d.paymentTerms}", dataIndex:"paymentTerms", _enableFn_: this._enableIfStatus_, noLabel: true, maxLength:6})
		.addCombo({ xtype:"combo", name:"referenceDay", bind:"{d.paymentRefDate}", dataIndex:"paymentRefDate", _enableFn_: this._enableIfStatus_, noLabel: true, store:[ __FMBAS_TYPE__.PaymentDay._INVOICE_DATE_, __FMBAS_TYPE__.PaymentDay._INVOICE_RECEIVING_DATE_, __FMBAS_TYPE__.PaymentDay._LAST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._FIRST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._MIDPOINT_DELIVERY_DATE_]})
		.addCombo({ xtype:"combo", name:"invoiceFreq", bind:"{d.invoiceFrequency}", dataIndex:"invoiceFrequency", _enableFn_: this._enableIfStatus_, noLabel: true, store:[ __FMBAS_TYPE__.InvoiceFreq._WEEKLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIMONTHLY_, __FMBAS_TYPE__.InvoiceFreq._MONTHLY_, __FMBAS_TYPE__.InvoiceFreq._QUARTERLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._ANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._PER_DELIVERY_, __FMBAS_TYPE__.InvoiceFreq._DAILY_, __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_],listeners:{
			expand:{scope:this, fn:this._filterInvoiceFreq_}
		}})
		.addCombo({ xtype:"combo", name:"invoiceType", bind:"{d.invoiceType}", dataIndex:"invoiceType", _enableFn_: this._enableIfStatus_, noLabel: true, store:[ __FMBAS_TYPE__.InvoiceType._XML_2_0_2_, __FMBAS_TYPE__.InvoiceType._XML_3_0_0_, __FMBAS_TYPE__.InvoiceType._XML_3_0_1_, __FMBAS_TYPE__.InvoiceType._XML_3_1_0_, __FMBAS_TYPE__.InvoiceType._EXCEL_, __FMBAS_TYPE__.InvoiceType._PAPER_, __FMBAS_TYPE__.InvoiceType._EDI_, __FMBAS_TYPE__.InvoiceType._PDF_FORMAT_, __FMBAS_TYPE__.InvoiceType._FISCAL_, __FMBAS_TYPE__.InvoiceType._EXPORT_, __FMBAS_TYPE__.InvoiceType._REGULAR_]})
		.addNumberField({name:"preapaidDays", bind:"{d.numberOfDaysPrepaid}", dataIndex:"numberOfDaysPrepaid", _enableFn_: this._enableIfStatus_, width:60, noLabel: true, maxLength:6})
		.addCombo({ xtype:"combo", name:"paymentFreq", bind:"{d.prepaymentFrequency}", dataIndex:"prepaymentFrequency", _enableFn_: this._enableIfStatus_, width:120, noLabel: true, store:[ __FMBAS_TYPE__.InvoiceFreq._WEEKLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIMONTHLY_, __FMBAS_TYPE__.InvoiceFreq._MONTHLY_, __FMBAS_TYPE__.InvoiceFreq._QUARTERLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._ANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._PER_DELIVERY_, __FMBAS_TYPE__.InvoiceFreq._DAILY_, __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_],listeners:{
			expand:{scope:this, fn:this._filterInvoiceFreq_}
		}})
		.addNumberField({name:"paymentAmount", bind:"{d.prepaymentAmount}", dataIndex:"prepaymentAmount", _enableFn_: this._enableIfStatus_, width:150, noLabel: true, sysDec:"dec_amount", maxLength:19, colspan:3})
		.addNumberField({name:"paymentDays", bind:"{d.prepaymentDayOffset}", dataIndex:"prepaymentDayOffset", _enableFn_: this._enableIfStatus_, width:40, noLabel: true, maxLength:6})
		.addDisplayFieldText({ name:"paymentCurrLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"pricingUnitLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:40px"})
		.addDisplayFieldText({ name:"paymentMethodLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:40px"})
		.addDisplayFieldText({ name:"bankGuaranteeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:40px"})
		.addDisplayFieldText({ name:"finanacialSourceLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"averagingMethodLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:40px"})
		.addDisplayFieldText({ name:"offsetLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:40px"})
		.addDisplayFieldText({ name:"creditTermsLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"paymentTermsLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"referenceDayLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"invoiceFreqLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"invoiceTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"preapaidDaysLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"paymentFreqLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:40px"})
		.addDisplayFieldText({ name:"paymentAmountLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right; padding-left:40px"})
		.addDisplayFieldText({ name:"paymentDaysLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"formTitlePayment", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, colspan:8})
		.addDisplayFieldText({ name:"days", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding-left:5px"})
		.add({name:"paymentDatsAndDaysLbl", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("paymentDays"),this._getConfig_("days")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"table1", layout: {type:"table", columns:8}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["table1"])
		.addChildrenTo("table1", ["paymentCurrLabel", "paymentCurr", "pricingUnitLabel", "pricingUnit", "paymentMethodLabel", "paymentMethod", "bankGuaranteeLabel", "bankGuarantee", "formTitlePayment", "finanacialSourceLabel", "finanacialSource", "averagingMethodLabel", "averagingMethod", "offsetLabel", "offset", "creditTermsLabel", "creditTerms", "preapaidDaysLabel", "preapaidDays", "paymentFreqLabel", "paymentFreq", "paymentAmountLabel", "paymentAmount", "paymentDaysLabel", "paymentDatsAndDaysLbl"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableIfStatus_: function() {
		
						var dc 		= this._controller_;
						var record 	= dc.getRecord();
						var status 	= record.get("tenderStatus");				
		
						return status=="Draft";
	},
	
	_enableIfPaymentCurrency_: function() {
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = false;
		
						if (record) {
							var settlementCurrencyId = record.get("settlementCurrencyId");
							if (!Ext.isEmpty(settlementCurrencyId) && this._enableIfStatus_() ) {
								result = true;
							}
						}
		
						return result;
	},
	
	_enableIfFinancialSource_: function() {
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = false;
		
						if (record) {
							var financialSourceId = record.get("financialSourceId");
							if (!Ext.isEmpty(financialSourceId) && this._enableIfStatus_() ) {
								result = true;
							}
						}
		
						return result;
	},
	
	_enableFinancialSource_: function(el) {
		
						if (el._fpRawValue_ != el._onFocusVal_) {
							if( this._enableIfStatus_() ){
				                var finanacialSource = this._get_("finanacialSource");
								var averagingMethod = this._get_("averagingMethod");
				                var elVal = el.getValue();
				                if (!Ext.isEmpty(elVal)) {
				                    finanacialSource.setReadOnly(false);
				                }
				                else {
				                    finanacialSource.setReadOnly(true);
				                }
								
								//disable cascade elements						
								averagingMethod.setReadOnly(true);
		
								//set ui elements
				                finanacialSource._safeSetValue_("");
								averagingMethod._safeSetValue_("");
		
								//set record data
								var dc = this._controller_;
								var record = dc.getRecord();
								record.set("financialSourceId", null);
								record.set("averagingMethodId", null);
			                }
						}
	},
	
	_enableAvgMethod_: function(el) {
		
						if (el._fpRawValue_ != el._onFocusVal_) {
							if( this._enableIfStatus_() ){
		 						var averagingMethod = this._get_("averagingMethod");
				                var elVal = el.getValue();
				                if (!Ext.isEmpty(elVal)) {
									averagingMethod.setReadOnly(false);
				                }
				                else {
									averagingMethod.setReadOnly(true);
				                }
		
								//set ui elements
								averagingMethod._safeSetValue_("");
		
								//set record data
								var dc = this._controller_;
								var record = dc.getRecord();
								record.set("averagingMethodId", null);
			                }
						}
	},
	
	_filterDistinctFinancialSource_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("financialSourceName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	},
	
	_filterDistinctAvgMthd_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("avgMthdName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	},
	
	_filterInvoiceFreq_: function(el) {
		
						var s = el.store;
						s.filter(function(r) {
							var value = r.get("field1");
							if ( value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_ && value !== __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_) {
								return value;
							}
						});
	}
});

/* ================= EDIT FORM: ProductPricing ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocation_Dc$ProductPricing", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderLocation_Dc$ProductPricing",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addCombo({ xtype:"combo", name:"pricingBase", bind:"{d.pricingBase}", dataIndex:"pricingBase", _enableFn_: function(dc, rec) { return dc.record.data.tenderStatus == __CMM_TYPE__.TenderStatus._DRAFT_; } , noLabel: true, store:[ __CMM_TYPE__.PricingType._INDEX_, __CMM_TYPE__.PricingType._MARKET_], colspan:7,listeners:{
			fpchange:{scope:this, fn:this._setIndexOrMarketFields_}
		}})
		.addCombo({ xtype:"combo", name:"indexProvider", bind:"{d.dataProvider}", dataIndex:"dataProvider", _enableFn_: this._enableIfStatus_, _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , noLabel: true, store:[ __FMBAS_TYPE__.DataProvider._IMPORT_ARGUS_, __FMBAS_TYPE__.DataProvider._IMPORT_DDS_, __FMBAS_TYPE__.DataProvider._IMPORT_OPIS_, __FMBAS_TYPE__.DataProvider._IMPORT_SHARE_, __FMBAS_TYPE__.DataProvider._IMPORT_PLATTS_, __FMBAS_TYPE__.DataProvider._IMPORT_ECB_, __FMBAS_TYPE__.DataProvider._IMPORT_BLOOMBERG_, __FMBAS_TYPE__.DataProvider._USER_MAINTAINED_, __FMBAS_TYPE__.DataProvider._CALCULATED_, __FMBAS_TYPE__.DataProvider._OTHER_],listeners:{
			fpchange:{scope:this, fn:this._enableTimeSerie_}
		}})
		.addLov({name:"timeSerie", bind:"{d.indexProviderName}", dataIndex:"indexProviderName", _enableFn_: this._enableIfIndexProvider_, _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , noLabel: true, xtype:"fmbas_TimeSerieExtendedLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "indexProviderId"} ,{lovField:"description", dsField: "indexProviderDesc"} ,{lovField:"externalSerieName", dsField: "indexProviderCode"} ,{lovField:"unitCode", dsField: "densityConvFromUnitCode"} ,{lovField:"unitId", dsField: "densityConvFromUnitId"} ,{lovField:"unit2Code", dsField: "densityConvToUnitCode"} ,{lovField:"unit2Id", dsField: "densityConvToUnitId"} ,{lovField:"convFctr", dsField: "operator"} ,{lovField:"arithmOper", dsField: "conversionFactor"} ],
			filterFieldMapping: [{lovField:"serieType", value: "E"}, {lovField:"dataProvider", dsField: "dataProvider"}, {lovField:"status", value: "published"} ],listeners:{
			fpchange:{scope:this, fn:this._enableAvgMthd_}
		}})
		.addTextField({ name:"timeSerieDesc", bind:"{d.indexProviderDesc}", dataIndex:"indexProviderDesc", noEdit:true , _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , width:300, noLabel: true, maxLength:200})
		.addTextField({ name:"timeSerieCode", bind:"{d.indexProviderCode}", dataIndex:"indexProviderCode", noEdit:true , _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , noLabel: true, maxLength:15})
		.addLov({name:"indexAveragingMethod", bind:"{d.indexAveragingMethodName}", dataIndex:"indexAveragingMethodName", _enableFn_: this._enableIfTimeSerie_, _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , noLabel: true, xtype:"fmbas_TimeSerieAvgMthdLov_Lov", maxLength:50,
			retFieldMapping: [{lovField:"averagingMethodId", dsField: "indexAveragingMethodId"} ],
			filterFieldMapping: [{lovField:"tserieId", dsField: "indexProviderId"}, {lovField:"active", value: "true"} ],listeners:{
			fpchange:{scope:this, fn:this._enableOffset_}
		}})
		.addCombo({ xtype:"combo", name:"offset", bind:"{d.indexAveragingPeriod}", dataIndex:"indexAveragingPeriod", _enableFn_: this._enableIfAverageMethod_, _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , noLabel: true, store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_], colspan:5})
		.addDisplayFieldText({ name:"indexConv", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32})
		.addLov({name:"convertTo", bind:"{d.densityConvToUnitCode}", dataIndex:"densityConvToUnitCode", _enableFn_: this._enableIfStatus_, _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , width:80, noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "densityConvToUnitId"} ]})
		.addLov({name:"convertFrom", bind:"{d.densityConvFromUnitCode}", dataIndex:"densityConvFromUnitCode", _enableFn_: this._enableIfStatus_, _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , width:80, noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "densityConvFromUnitId"} ]})
		.addCombo({ xtype:"combo", name:"operator", bind:"{d.operator}", dataIndex:"operator", _enableFn_: this._enableIfStatus_, _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , width:80, noLabel: true, store:[ __FMBAS_TYPE__.Operator._MULTIPLY_, __FMBAS_TYPE__.Operator._DIVIDE_]})
		.addNumberField({name:"factor", bind:"{d.conversionFactor}", dataIndex:"conversionFactor", _enableFn_: this._enableIfStatus_, _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , width:80, noLabel: true, sysDec:"dec_unit", maxLength:19})
		.addNumberField({name:"density", bind:"{d.fuelDensity}", dataIndex:"fuelDensity", _enableFn_: this._enableIfStatus_, _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , width:80, noLabel: true, maxLength:19, decimals:6})
		.addLov({name:"densityWeight", bind:"{d.densityWeightUnitCode}", dataIndex:"densityWeightUnitCode", _enableFn_: this._enableIfStatus_, _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , width:80, noLabel: true, xtype:"fmbas_UnitsLov_Lov", maxLength:2, style:"padding-left:5px",
			retFieldMapping: [{lovField:"id", dsField: "densityWeightUnitId"} ],
			filterFieldMapping: [{lovField:"unitTypeInd", value: "Mass"} ]})
		.addLov({name:"densityVolume", bind:"{d.densityVolumeUnitCode}", dataIndex:"densityVolumeUnitCode", _enableFn_: this._enableIfStatus_, _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , width:80, noLabel: true, xtype:"fmbas_UnitsLov_Lov", maxLength:2, style:"padding-left:5px",
			retFieldMapping: [{lovField:"id", dsField: "densityVolumeUnitId"} ],
			filterFieldMapping: [{lovField:"unitTypeInd", value: "Volume"} ]})
		.addNumberField({name:"rate", bind:"{d.rate}", dataIndex:"rate", _enableFn_: this._enableIfStatus_, _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._MARKET_; } , width:80, noLabel: true, sysDec:"dec_prc", maxLength:19})
		.addLov({name:"marketCurrency", bind:"{d.marketCurrencyCode}", dataIndex:"marketCurrencyCode", _enableFn_: this._enableIfStatus_, _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._MARKET_; } , width:80, noLabel: true, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3,
			retFieldMapping: [{lovField:"id", dsField: "marketCurrencyId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addLov({name:"marketUnit", bind:"{d.marketPricingUnitCode}", dataIndex:"marketPricingUnitCode", _enableFn_: this._enableIfStatus_, _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._MARKET_; } , width:80, noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "marketPricingUnitId"} ]})
		.addCombo({ xtype:"combo", name:"sourceType", bind:"{d.sourceType}", dataIndex:"sourceType", _enableFn_: this._enableIfStatus_, _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._MARKET_; } , width:150, noLabel: true, store:[ __CMM_TYPE__.SourceType._EX_REFINERY_, __CMM_TYPE__.SourceType._GOVERNMENT_]})
		.addTextField({ name:"sourceName", bind:"{d.sourceName}", dataIndex:"sourceName", _enableFn_: this._enableIfStatus_, _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._MARKET_; } , width:250, noLabel: true, maxLength:100})
		.addDisplayFieldText({ name:"pricingBaseLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"indexProviderLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"timeSerieLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , maxLength:32, style:"float:right; padding-left:40px"})
		.addDisplayFieldText({ name:"timeSerieDescLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , maxLength:32, style:"float:right; padding-left:40px"})
		.addDisplayFieldText({ name:"timeSerieCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , maxLength:32, style:"float:right; padding-left:40px"})
		.addDisplayFieldText({ name:"averagingMethodLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"offsetLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , maxLength:32, style:"float:right; padding-left:40px"})
		.addDisplayFieldText({ name:"convertToLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"convertFromLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , maxLength:32, style:"float:right; padding-left:5px"})
		.addDisplayFieldText({ name:"operatorLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , maxLength:32, style:"float:right; padding-left:5px"})
		.addDisplayFieldText({ name:"factorLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , maxLength:32, style:"float:right; padding-left:5px"})
		.addDisplayFieldText({ name:"densityLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , maxLength:32, style:"float:right; padding-left:40px"})
		.addDisplayFieldText({ name:"rateFromLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._MARKET_; } , maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"marketCurrencyLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._MARKET_; } , maxLength:32, style:"float:right; padding-left:40px"})
		.addDisplayFieldText({ name:"marketUnitLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._MARKET_; } , maxLength:32, style:"float:right; padding-left:40px"})
		.addDisplayFieldText({ name:"sourceTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._MARKET_; } , maxLength:32, style:"float:right; padding-left:40px"})
		.addDisplayFieldText({ name:"sourceNameLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._MARKET_; } , maxLength:32, style:"float:right; padding-left:40px"})
		.addDisplayFieldText({ name:"indexConversion", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return dc.record && dc.record.data.pricingBase == __CMM_TYPE__.PricingType._INDEX_; } , maxLength:32, colspan:8})
		.add({name:"indexConversionComposite", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("convertTo"),this._getConfig_("convertFromLabel"),this._getConfig_("convertFrom"),this._getConfig_("operatorLabel"),this._getConfig_("operator"),this._getConfig_("factorLabel"),this._getConfig_("factor"),this._getConfig_("densityLabel"),this._getConfig_("density"),this._getConfig_("densityWeight"),this._getConfig_("densityVolume")]})
		.add({name:"marketComposite", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("rate"),this._getConfig_("marketCurrencyLabel"),this._getConfig_("marketCurrency"),this._getConfig_("marketUnitLabel"),this._getConfig_("marketUnit"),this._getConfig_("sourceTypeLabel"),this._getConfig_("sourceType"),this._getConfig_("sourceNameLabel"),this._getConfig_("sourceName")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"table1", layout: {type:"table", columns:8}})
		.addPanel({ name:"table2", layout: {type:"table", columns:8}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["table1", "table2"])
		.addChildrenTo("table1", ["pricingBaseLabel", "pricingBase", "indexProviderLabel", "indexProvider", "timeSerieLabel", "timeSerie", "timeSerieDescLabel", "timeSerieDesc", "timeSerieCodeLabel", "timeSerieCode", "averagingMethodLabel", "indexAveragingMethod", "offsetLabel", "offset", "indexConversion", "convertToLabel", "indexConversionComposite"])
		.addChildrenTo("table2", ["rateFromLabel", "marketComposite"]);
	},
	/* ==================== Business functions ==================== */
	
	_afterDefineElements_: function() {
		
						this._getBuilder_().change("indexConversionComposite",{
							colspan: 7
						});
						this._getBuilder_().change("marketComposite",{
							colspan: 7
						});
	},
	
	_setIndexOrMarketFields_: function(field,newValue,oldValue) {
		
						var indexVal = ["indexProviderLabel" , "indexProvider" , "timeSerieLabel" ,
								"timeSerie" , "timeSerieDescLabel" , "timeSerieDesc" , "timeSerieCodeLabel" , "timeSerieCode" , "averagingMethodLabel" ,
								"indexAveragingMethod" , "offsetLabel" , "offset" , "indexConversion" , "convertToLabel" , "convertTo" , "convertFromLabel" , 
								"convertFrom" , "operatorLabel" , "operator" , "factorLabel" , "factor" , "densityLabel" , "density" , "densityWeight" , "densityVolume"];
		
						var indexRecord = ["indexProviderId","indexAveragingMethodId","densityConvToUnitId","densityConvFromUnitId",
								"densityWeightUnitId","densityVolumeUnitId"];
		
						var marketVal = ["rateFromLabel" , "rate" , "marketCurrencyLabel" , "marketCurrency" , 
							"marketUnitLabel" , "marketUnit" , "sourceTypeLabel" , "sourceType" , "sourceNameLabel" , "sourceName"];
		
						var marketRecord = ["marketCurrencyId","marketPricingUnitId"];
		
						var dc = this._controller_;
						var record = dc.getRecord();
		
						if(field){
							if(newValue == "Index"){
								Ext.each(marketVal, function(e) {
									this._get_(e).hide();
									this._get_(e)._safeSetValue_("");
								}, this);
		
								Ext.each(marketRecord, function(e) {
									record.set(e, null);
								}, this);
		
								Ext.each(indexVal, function(e) {
									this._get_(e).show();
								}, this);
							} else if (newValue == "Market") {
								//disable cascade elements
								this._get_("timeSerie").setReadOnly(true);
								this._get_("indexAveragingMethod").setReadOnly(true);
								this._get_("offset").setReadOnly(true);
		
								Ext.each(indexVal, function(e) {
									this._get_(e).hide();
									this._get_(e)._safeSetValue_("");
								
								}, this);
		
								Ext.each(indexRecord, function(e) {
									record.set(e, null);
								}, this);
		
								Ext.each(marketVal, function(e) {
									this._get_(e).show();
								}, this);
							}
						}
	},
	
	_enableIfStatus_: function() {
		
						var dc 		= this._controller_;
						var record 	= dc.getRecord();
						var status 	= record.get("tenderStatus");				
		
						return status=="Draft";
	},
	
	_enableIfIndexProvider_: function() {
		
						var result = false;
						
						if( this._enableIfStatus_() ){
							var dc = this._controller_;
							var record = dc.getRecord();
			
							if (record) {
								var indexProvider = record.get("dataProvider");
								if (!Ext.isEmpty(indexProvider) && this._enableIfStatus_() ) {
									result = true;
								}
							}
						}
		
						return result;
	},
	
	_enableIfTimeSerie_: function() {
		
						var result = false;
		
						if( this._enableIfStatus_() ){
							var dc = this._controller_;
							var record = dc.getRecord();
			
							if (record) {
								var timeSerie = record.get("indexProviderId");
								if (!Ext.isEmpty(timeSerie) && this._enableIfStatus_() ) {
									result = true;
								}
							}
						}
		
						return result;
	},
	
	_enableIfAverageMethod_: function() {
		
						var result = false;
		
						if( this._enableIfStatus_() ){
							var dc = this._controller_;
							var record = dc.getRecord();
			
							if (record) {
								var timeSerie = record.get("indexAveragingMethodId");
								if (!Ext.isEmpty(timeSerie) && this._enableIfStatus_() ) {
									result = true;
								}
							}
						}
		
						return result;
	},
	
	_enableTimeSerie_: function(el) {
		
						if (el._fpRawValue_ != el._onFocusVal_) {
							if( this._enableIfStatus_() ){
				                var timeSerie = this._get_("timeSerie");
								var timeSerieDesc = this._get_("timeSerieDesc");
								var timeSerieCode = this._get_("timeSerieCode");
								var indexAveragingMethod = this._get_("indexAveragingMethod");
								var offset = this._get_("offset");
		
								var convertTo = this._get_("convertTo");
								var convertFrom = this._get_("convertFrom");
								var operator = this._get_("operator");
								var factor = this._get_("factor");
		
				                var elVal = el.getValue();
				                if (!Ext.isEmpty(elVal)) {
				                    timeSerie.setReadOnly(false);
				                }
				                else {
				                    timeSerie.setReadOnly(true);
				                }
		
								//disable childs
								indexAveragingMethod.setReadOnly(true);
								offset.setReadOnly(true);
		
								//set ui elements
				                timeSerie._safeSetValue_("");
								timeSerieDesc._safeSetValue_("");
								timeSerieCode._safeSetValue_("");
								
								indexAveragingMethod._safeSetValue_("");
								offset._safeSetValue_("");
		
								convertTo._safeSetValue_("");
								convertFrom._safeSetValue_("");
								operator._safeSetValue_("");
								factor._safeSetValue_("");
		
								//set record data
								var dc = this._controller_;
								var record = dc.getRecord();
		
								record.set("indexProviderId", null);
								record.set("indexAveragingMethodId", null);
		
								record.set("densityConvFromUnitId", null);
								record.set("densityConvToUnitId", null);
			                }
						}
	},
	
	_filterDistinctTimeSeries_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("indexProviderName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	},
	
	_enableAvgMthd_: function(el) {
		
						if (el._fpRawValue_ != el._onFocusVal_) {
							if( this._enableIfStatus_() ){
				                var elVal = el.getValue();
		
				                var indexAveragingMethod = this._get_("indexAveragingMethod");
								var offset = this._get_("offset");
		
				                if (!Ext.isEmpty(elVal)) {
				                    indexAveragingMethod.setReadOnly(false);
				                }
				                else {
				                    indexAveragingMethod.setReadOnly(true);
				                }
		
								//disable childs
								offset.setReadOnly(true);
		
								//set ui elements
				                indexAveragingMethod._safeSetValue_("");
								offset._safeSetValue_("");
								
		
								//set record data
								var dc = this._controller_;
								var record = dc.getRecord();
								record.set("indexAveragingMethodId", null);
			                }
						}
	},
	
	_enableOffset_: function(el) {
		
						if (el._fpRawValue_ != el._onFocusVal_) {
							if( this._enableIfStatus_() ){
				                var offset = this._get_("offset");
				                var elVal = el.getValue();
		
				                if (!Ext.isEmpty(elVal)) {
				                    offset.setReadOnly(false);
				                }
				                else {
				                    offset.setReadOnly(true);
				                }
		
								//set ui elements
				                offset.setValue("");
		
								//set record data
								var dc = this._controller_;
								var record = dc.getRecord();
								record.set("indexAveragingPeriod", null);
			                }
						}
	}
});

/* ================= EDIT FORM: New ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocation_Dc$New", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderLocation_Dc$New",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"location", bind:"{d.locationCode}", dataIndex:"locationCode", noLabel: true, xtype:"fmbas_LocationsLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"id", dsField: "locationId"} ,{lovField:"name", dsField: "locationName"} ]})
		.addTextField({ name:"product", bind:"{d.fuelProduct}", dataIndex:"fuelProduct", noLabel: true, maxLength:32})
		.addNumberField({name:"volume", bind:"{d.volume}", dataIndex:"volume", width:110, noLabel: true, sysDec:"dec_unit", maxLength:19})
		.addLov({name:"volumeUnit", bind:"{d.volumeUnitCode}", dataIndex:"volumeUnitCode", noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "volumeUnitId"} ]})
		.addCombo({ xtype:"combo", name:"period", bind:"{d.period}", dataIndex:"period", noLabel: true, store:[ __CMM_TYPE__.Period._CONTRACT_PERIOD_, __CMM_TYPE__.Period._MONTHLY_, __CMM_TYPE__.Period._YEARLY_]})
		.addDateField({name:"agreementFrom", bind:"{d.agreementFrom}", dataIndex:"agreementFrom", allowBlank:false, noLabel: true})
		.addDateField({name:"agreementTo", bind:"{d.agreementTo}", dataIndex:"agreementTo", allowBlank:false, noLabel: true})
		.addBooleanField({ name:"adHoc", bind:"{d.adHocLoc}", dataIndex:"adHocLoc", noLabel: true})
		.addCombo({ xtype:"combo", name:"locationBiddingStatus", bind:"{d.locationBiddingStatus}", dataIndex:"locationBiddingStatus", noLabel: true, store:[ __CMM_TYPE__.BiddingStatus._NEW_, __CMM_TYPE__.BiddingStatus._IN_NEGOTIATION_, __CMM_TYPE__.BiddingStatus._FINISHED_, __CMM_TYPE__.BiddingStatus._NO_BID_]})
		.addNumberField({name:"rate", bind:"{d.rate}", dataIndex:"rate", allowBlank:false, noLabel: true, sysDec:"dec_prc", maxLength:19})
		.addDisplayFieldText({ name:"locationLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"productLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"volumeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"periodLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"agreementFromLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"agreementToLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"adHocLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"locationBiddingStatusLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"rateLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.add({name:"row", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("volume"),this._getConfig_("volumeUnit")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["table1"])
		.addChildrenTo("table1", ["locationLabel", "location", "productLabel", "product", "volumeLabel", "row", "periodLabel", "period", "agreementFromLabel", "agreementFrom", "agreementToLabel", "agreementTo", "rateLabel", "rate", "locationBiddingStatusLabel", "locationBiddingStatus", "adHocLabel", "adHoc"]);
	}
});

/* ================= GRID: SelectList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocation_Dc$SelectList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_TenderLocation_Dc$SelectList",
	_noExport_: true,
	_noPaginator_: true,
	selType: null,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"location", dataIndex:"locationCode", width:100})
		.addTextColumn({ name:"biddingstatus", dataIndex:"locationBiddingStatus", width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: CaptureBid ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.TenderLocation_Dc$CaptureBid", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_TenderLocation_Dc$CaptureBid",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addToggleField({ name:"useTenderHolderMasterAgrement", bind:"{p.captureBidFromHolderMA}", paramIndex:"captureBidFromHolderMA", noLabel: true})
		.addToggleField({ name:"useFuelReceiverMasterAgrement", bind:"{p.captureBidFromReceiverMA}", paramIndex:"captureBidFromReceiverMA", noLabel: true})
		.addToggleField({ name:"useExistingBids", bind:"{p.captureBidFromExistingBids}", paramIndex:"captureBidFromExistingBids", noLabel: true})
		.addDisplayFieldText({ name:"useTenderHolderMasterAgrementLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding-right:70px"})
		.addDisplayFieldText({ name:"moreInfo1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding-right:70px; padding-top:0px; margin-top:-10px", colspan:2, cls:"sone-font-italic"})
		.addDisplayFieldText({ name:"useFuelReceiverMasterAgrementLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding-right:70px"})
		.addDisplayFieldText({ name:"moreInfo2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding-right:70px; padding-top:0px; margin-top:-10px", colspan:2, cls:"sone-font-italic"})
		.addDisplayFieldText({ name:"useExistingBidsLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding-right:70px"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"captureBidDetailes",  style:"padding:20px", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["captureBidDetailes"])
		.addChildrenTo("captureBidDetailes", ["useTenderHolderMasterAgrementLabel", "useTenderHolderMasterAgrement", "moreInfo1", "useFuelReceiverMasterAgrementLabel", "useFuelReceiverMasterAgrement", "moreInfo2", "useExistingBidsLabel", "useExistingBids"]);
	}
});
