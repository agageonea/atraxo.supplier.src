Ext.override(atraxo.cmm.ui.extjs.frame.Contract_Ui, {

	_reloadContractPriceCtgry_ : null,
	
	_afterShowStackedViewElement_ : function(svn, idx) {
		if( idx === "canvas2" && this._reloadContractPriceCtgry_ === true ){
			this._reloadContractPriceCtgry_ = false;
			this._getDc_("contractPriceCtgry").doReloadPage();
		}
	},
	
	_afterDefineElements_ : function() {
		var f = this._getElementConfig_("copySupplier");
		var f0 = this._getElementConfig_("CustomerSelect");
		var f1 = this._getElementConfig_("approveNote");
		var f2 = this._getElementConfig_("approvalNote");
		var f3 = this._getElementConfig_("rejectNote");
		if( f ){
			f._shouldDisableWhenDcIsReadOnly_ = false;
		}
		if( f0 ){
			f0._shouldDisableWhenDcIsReadOnly_ = false;
		}
		if( f1 ){
			f1._shouldDisableWhenDcIsReadOnly_ = false;
		}
		if( f2 ){
			f2._shouldDisableWhenDcIsReadOnly_ = false;
		}
		if( f3 ){
			f3._shouldDisableWhenDcIsReadOnly_ = false;
		}
	},
	
	_afterViewAndFilters_ : function(){
	
		// SONE-2766: Contracts(Purchases and Sales)/Prices: Total converted price
		// is not correct when a new price is added
		// A query is needed to trigger the recalculation of the total price because
		// there are situations when the currency defined in the view
		// is different than the one in the payment terms.
		this._getDc_("contractPriceCtgry").delayedQuery();
			
		// Dan: SONE-1554: Contract Management: the following error message appears when updating the contract\'s validity period 
		var priceList = this._get_("priceCtgryList");
		var contract = this._getDc_("contract");
		var viewFilterToolbar = Ext.getCmp(priceList._viewFilterToolbarId_);
	
		if (viewFilterToolbar) {
	
			var datePickerCmp = Ext.getCmp(viewFilterToolbar._datePickerId_);
	
			// Dan: SONE-2159: Purchase contracts: issues with price review date
	
			var adjustDate = function() {
				var r = contract.getRecord();
				if (r) {
					var validFrom = r.get("validFrom");
					var validTo = r.get("validTo");
					var datePickerValue = datePickerCmp.getValue();
	
					if (datePickerValue < validFrom) {
						datePickerCmp.setValue(validFrom);
						Main.warning("The selected date is out of the contract period!");
						
					}
					else if (datePickerValue > validTo) {
						datePickerCmp.setValue(validTo);
						Main.warning("The selected date is out of the contract period!");
					}
					
				}
			}
	
			var adjustDateIfExpired = function() {
	
				var r = contract.getRecord();
				if (r) {
					var status = r.get("status");
					var validTo = r.get("validTo");
					var currentDate = new Date();
	
					if (status  === "Expired") {
						datePickerCmp.setValue(validTo);
					}
					else {
						datePickerCmp.setValue(currentDate);
					}
				}
			}
	
			datePickerCmp.on("blur", function() {
				adjustDate();
			}, this);
	
			contract.on("onAfterEditIn", function() {
				adjustDateIfExpired();
			}, this);				
		}				
	},
	
	_enableDisableDeleteResale_ : function() {
		var ctrl = this._getDc_("contract");
		var r = ctrl.getRecord();
		var btn = this._get_("btnDeleteResale");
		if (r) {
			var resaleContract = r.get("resaleContract");
			if (!Ext.isEmpty(resaleContract)) {
				btn.setHidden(false);
			}
			else {
				btn.setHidden(true);
			}
		}
	},
	
	_afterDefineDcs_ : function() {
	
		var contract = this._getDc_("contract");
		var pricingBase = this._getDc_("pricingBase");
		var contractPriceCtgry =  this._getDc_("contractPriceCtgry");
		var contractPriceCmpn = this._getDc_("contractPriceCmpn");
		var contractPriceCmpnClassic = this._getDc_("contractPriceCmpnClassic");
		var contractPriceRestr = this._getDc_("contractPriceRestr");
		var contractPriceCtgryAssign = this._getDc_("contractPriceCtgryAssign");
		var shipTo = this._getDc_("shipTo");
		this.customerCodeTooltipId = Ext.id();
	
		contract.on("afterDoSaveFailure", function() {
			return false;
		}, this);
	
		contractPriceCtgryAssign.on("afterDoQuerySuccess", function(dc,ajax) {  
	
	        var markSelected = ajax.options.markSelected;
			var view = this._get_("priceCtgryAssignList");
			var form = this._get_("priceCtgryEdit");
			if (form) {
				var percentageOf = form._get_("percentageOf");
				var win = this._getWindow_("priceCategoryAsgnWdw");
				var s = dc.store;
	
				if (markSelected && markSelected === true) {	
					if (win._isInEditForm_ === true) {	
						if (!Ext.isEmpty(percentageOf.getRawValue())) {	
							var percentageOfArray = percentageOf.getRawValue().split(",");
							s.each(function(r) {
								if (percentageOfArray.indexOf(r.get("name")) !== -1 ) {
									view.getSelectionModel().select(s.indexOf(r), true);
								}
							}, this);
						}
					}
					else {
						view.getSelectionModel().deselectAll();
					}
				}
			}
			
	
	    } , this );
	
		contract.on("onEditOut", function(dc) { 
			dc.doReloadPage();
		}, this);
	
		contractPriceCtgry.on("onAssignPriceCategory", function() { 
			this.assignPrice();
		} , this );			
		
		contract.on("beforeDoDelete", function (dc) {
			var r = dc.getRecord();
			var message = Main.translate("msg", "dc_confirm_delete_selection");
			if (r) {
				var resaleContractCode = r.get("resaleContract");
				if (!Ext.isEmpty(resaleContractCode)) {
					message = message.concat(Main.translate("applicationMsg", "deleteLinkedSaleContract_lbl")).concat(resaleContractCode);
				}
				dc._controller_.commands.doDelete.confirmMessageBody = message;
			}
	        
	    }, this);
	
		contract.on("afterDoSaveSuccess", function (dc) {
	        this._applyStateAllButtons_();
			dc.doReloadRecord();
			this._getDc_("pricingBase").doReloadPage();
			dc._isCopyMode_ = null;
	    }, this);
	
		contractPriceRestr.on("afterDoSaveSuccess", function () {
	        this._applyStateAllButtons_();
	    }, this);
	
		contract.on("afterDoCancel", function (dc, ajaxResult) {
			if (ajaxResult.reloadPage === true) {
				dc.doReloadPage({filterGeneratedContract : true, generatedContractId : ajaxResult.generatedContractId});
			}
	        
	    }, this);
	
		contract.on("afterDoReloadPageSuccess", function (dc, ajaxResult) {
			if (ajaxResult.options.filterGeneratedContract === true) {
				this._showStackedViewElement_("main", "canvas2", this._afterCopyContract_(dc, ajaxResult.options.generatedContractId));
			}
	    }, this);
	
		contract.on("recordChange", function (obj) {
			this._onContractRecordChange_(obj.dc);
			this._enableDisableDeleteResale_();
	    }, this);
	
		// Dan: onEditIn, set the first tab as active to fix the focus problem
	
		contract.on("onEditIn", function () {
			var detailsTab = this._get_("detailsTab");
			var pricingBase = this._get_("prices");
			detailsTab.setActiveTab(pricingBase);
		}, this);
	
	    contract.on("enableContinueBtn", function(dc, val) {
	        var btn = this._get_("btnContinueNewPurchaseContract");
	        btn.setDisabled(!val);
	    }, this);
	
		contract.on("disableContinueCopyBtn", function() {
			var btn = this._get_("btnContinueCopyPurchaseContract");
			btn.setDisabled(true);
		}, this);
	
		contract.on("enableContinueCopyBtn", function() {
			var btn = this._get_("btnContinueCopyPurchaseContract");
			btn.setDisabled(false);
		}, this);
	
		contract.on("afterDoNew", function (dc, ajaxRequest) {
	        var newFromCustomers = ajaxRequest.newFromCustomers;
	        if (!newFromCustomers) {
					var window = this._getWindow_("wdwNewPurchaseContract");
	                window.show(undefined, function() {
	                var templateDc = this._getDc_("template");
					templateDc.doQuery();
	                }, this);       
	        }
	
			this._setSubsidiary_();
	                           
	     }, this);
	
		contract.beforeDoSave = function () {
	        var r = contract.getRecord();
			var holderCode = r.get("holderCode");
			var supplierCode = r.get("supplierCode");
			if(holderCode == supplierCode){
				Main.warning(Main.translate("applicationMsg","sameCompaniesWarning__lbl"));
				return false;
			}
	    };
	
		contract.on("afterDoQuerySuccess", function(dc, ajaxResult) {
			if (ajaxResult.options.doNew === true) {
				this._showStackedViewElement_("main", "canvas1", dc.doNew());
			}
			if (ajaxResult.options.showFilteredEdit === true) {
				this._showStackedViewElement_("main", "canvas2");
			}
			if (ajaxResult.options.showEdit === true) {
				this._showStackedViewElement_("main", "canvas2");
			}
		}, this);
	
		contractPriceCtgry.on("afterDoSaveSuccess", function(dc) { 
			var contractPriceRestrDC = this._getDc_("contractPriceRestr");
			var rec = dc.getRecord();
			if (rec) {
				var restriction = rec.get("restriction");
				contractPriceRestrDC.setParamValue("isEnabled", restriction);
				contractPriceRestrDC.doReloadPage();
			}
			pricingBase.doCancel();	
			contract.doReloadRecord();
		}, this);
	
		contractPriceCtgry.on("afterDoQuerySuccess", function(dc, ajaxResult) { 
			if (ajaxResult.options.showFilteredEdit === true) {
				this._showStackedViewElement_("main", "canvas3", dc.noReset = false); // SONE-2733
			}
		}, this);
	
		contractPriceCtgry.on("onEditOut", function(dc) { 
			dc.doReloadPage();
		}, this);	
	
		contractPriceCtgry.on("afterDoDeleteSuccess", function() {
			pricingBase.doReloadPage(); //important
			contract.doReloadRecord(); // SONE-2295
		} , this );
	
		pricingBase.on("itsTimeToCalculateDensity", function (dc, params) {
			var record = pricingBase.getRecord();
	        var formName;
            if (params && !Ext.isEmpty(params.formName)) {
            	formName = params.formName;
            }
			if (record) {
				var quotation = record.get("quotName");
				var quotUnitTypeInd = record.get("quotUnitTypeInd");
				var convUnitTypeInd = record.get("convUnitTypeInd");
				var convUnitCode = record.get("convUnitCode");
				if(!Ext.isEmpty(quotation) && quotUnitTypeInd !== convUnitTypeInd && !Ext.isEmpty(convUnitCode)){
					this.getDensity(pricingBase,formName);
				}
			}
		}, this, {buffer: 200});
	
		pricingBase.on("OnEditIn", function (dc) {
		   this.showWdwPricing();
								
			var pbRecord = dc.getRecord();
			if(pbRecord.get("pricingMethod") === "Index"){
				dc.fireEvent("itsTimeToCalculateDensity", dc);
			}
		}, this);
	
		contractPriceCmpnClassic.on("OnEditIn", function () {
	        this.showWdwPriceComp1();
	    }, this);
	
		contract.on("afterDoSaveFailure", function (dc) {
	        dc.doCancel();
	    }, this);
	
	    contractPriceCmpn.on("OnEditOut", function () {
	        contractPriceCtgry.doReloadPage();
	        contractPriceCtgry.canDoDelete = true;
	    }, this);
	
		pricingBase.on("afterDoNew", function (dc, ajaxResult) {
			var newRec, opt;
			if (ajaxResult.setIndexPrice === true) {
				dc._changeState_ = 1;
				this.showPriceIndexWdw("PbEdit","priceCatName","Index","id","priceCatId");
				newRec = dc.getRecord();                    	
				if (newRec) { 
	            	newRec.beginEdit();                    
					newRec.set("mainCategoryCode", __FMBAS_TYPE__.PriceType._PRODUCT_);                        
	            	newRec.endEdit();
	        	}
			} 
			if (ajaxResult.setFixPrice === true) {
				opt = {
					description: "Fix price"
				}
				dc._changeState_ = 1;
				newRec = dc.getRecord();                    	
				if (newRec) { 
	            	newRec.beginEdit();                    
					newRec.set("mainCategoryCode", __FMBAS_TYPE__.PriceType._PRODUCT_);                        
	            	newRec.endEdit();
	        	}
				this.showPriceFixWdw("priceFixNew","priceCatName","Fixed","id","priceCatId", opt);						
			} 
			if (ajaxResult.setDftPrice === true) {
				opt = {
					description: "Into Plane Service for low volume"
				}
				dc._changeState_ = 1;
				this.showPriceFixWdw("priceFixNew","priceCatName","","id","priceCatId", opt);
			}
			if (ajaxResult.setCompositePrice === true) {
	            dc._changeState_ = 1;
	            this.showCompositePriceWdw();
	        }
	
			this.setPriceFields();
			this._getDc_("pricingBase").getRecord().set("exchangeRateOffset", this._getDc_("contract").getRecord().get("exchangeRateOffset"));
		}, this);
	
		pricingBase.on("afterDoDeleteSuccess", function () {
			contractPriceCtgry.doReloadPage();
			contract.doReloadPage();
		}, this);
	
		pricingBase.on("afterDoSaveSuccess", function (dc, ajaxResult) {
	
			if (ajaxResult.options.options.doNewAfterSave === true){
				if(dc._step_ === 1) {
					this._setDefaultFix();
				} else if (dc._step_ === 2 ) {
					this._setDefaultDFT();
				} else if (dc._step_ === 3) {
	                this._setDefaultComposite();
				}
				contract.doReloadRecord();
				contractPriceCtgry.doReloadPage();
				
			} 
			if (ajaxResult.options.options.doCloseNewIndexPriceWindowAfterSave === true) {
				this._getWindow_("wdwPricing").close();
	
				contract.doReloadRecord();
				contractPriceCtgry.doReloadPage();
			}
			if (ajaxResult.options.options.doCloseCompositePriceAfterSave === true) {
	            this._getWindow_("compositePriceWdw").close();
	            contract.doReloadRecord();
	            contractPriceCtgry.doReloadPage();
	        }
			if (ajaxResult.options.options.doCloseNewWindowAfterSave === true){
				this._getWindow_("priceFixWdw").close();
	
				contract.doReloadRecord();
				contractPriceCtgry.doReloadPage();
			} 
			if (ajaxResult.options.options.doCloseEditFormulaWindowAfterSave === true) {
				this._getWindow_("wdwPricingEditFormula").close();
	
				contract.doReloadRecord();
				contractPriceCtgry.doReloadPage();
			}
	
			var jsonData, data, lastRecordId;
	
			// Dan: SONE-2108 bug fixes
	
			if(ajaxResult.options.options.doCloseEditAfterSave === true){
				this._getWindow_("priceFixWdw").close();
				jsonData = Ext.getResponseDataInJSON(ajaxResult.response);
				data = jsonData.data[0];
				lastRecordId = data.id;
				contractPriceCtgry.doClearAllFilters();
				contractPriceCtgry.setFilterValue("pricingBaseId", lastRecordId);
				contractPriceCtgry.doQuery({showFilteredEdit: true});
			} 
	
			// End Dan
	
			if(ajaxResult.options.options.doCloseEditCompositeAfterSave === true){
	
	            this._getWindow_("compositePriceWdw").close();
				jsonData = Ext.getResponseDataInJSON(ajaxResult.response);
	            data = jsonData.data[0];
	            lastRecordId = data.id;
	
	            contractPriceCtgry.doClearAllFilters();
	            contractPriceCtgry.setFilterValue("pricingBaseId", lastRecordId);
	            contractPriceCtgry.doQuery({showFilteredEdit: true});
	        }
	
		}, this);
	
		shipTo.on("afterDoNew", function (dc){
			var newRec = dc.getRecord();
			var contract = this._getDc_("contract");
	        var contractRecord = contract.getRecord();
			if (newRec && contractRecord) { 
	            newRec.beginEdit();    
				newRec.set("validFrom", contractRecord.get("validFrom"));
	            newRec.set("validTo", contractRecord.get("validTo"));
				dc.params.set("contractStatus",contractRecord.get("status"));
				dc.params.set("contractDealType",contractRecord.get("dealType"));
	            newRec.endEdit();
	        }
		},this);
	
		shipTo.on("afterDoSaveSuccess", function (dc){
			
			if (dc._volumeChanged_ === true) {
				var win = this._getWindow_("wdwRemarks");
				win.show();
			}
			
			contract.doReloadRecord();
		},this);
	
	
		contractPriceCmpn.on("afterDoNew", function (dc){
			this.showWdwPriceComp();
			var number = dc.store.totalCount;
			var rec = dc.getRecord();
			var contract = this._getDc_("contract");
			var contractRecord = contract.getRecord();
			
			if(number === 0 && rec && contractRecord) {
				rec.set("validFrom", contractRecord.get("validFrom"));
			}
			var dst = dc.getRecord();
			var src = this._getDc_("contract").getRecord();
	
			if (dst && src) {
	
				var priceCtgryPricePer = dst.get("priceCtgryPricePer");
	
				dst.beginEdit();
					dst.set("validTo", src.get("validTo"));
				dst.set("currency", src.get("settCurrId"));				
				dst.set("currencyCode", src.get("settCurrCode"));
				
				if (priceCtgryPricePer !== __FMBAS_TYPE__.PriceInd._EVENT_) {
					dst.set("unit", src.get("settUnitId"));
					dst.set("unitCode", src.get("settUnitCode"));
				}
				else {
					this._setDefaultLovValue_("priceCompEdit", "unitEvent", "EA", "id", "unit");
				}
				if (priceCtgryPricePer !== __FMBAS_TYPE__.PriceInd._PERCENT_) {
					dst.set("unit", src.get("settUnitId"));
					dst.set("unitCode", src.get("settUnitCode"));
				}
				else {
					this._setDefaultLovValue_("priceCompEdit", "unitPercent", "%", "id", "unit");
				}
				dst.endEdit();
			}
	
		}, this );
	
		contractPriceCmpnClassic.on("afterDoNew", function (dc){
			this.showWdwPriceComp();
			var number = dc.store.totalCount;
			var rec = dc.getRecord();
			var contract = this._getDc_("contract");
			var contractRecord = contract.getRecord();
			
			if(number === 0 && rec && contractRecord) {
				rec.set("validFrom", contractRecord.get("validFrom"));
			}
			var dst = dc.getRecord();
			var src = this._getDc_("contract").getRecord();
	
			if (dst && src) {
	
				var priceCtgryPricePer = dst.get("priceCtgryPricePer");
	
				dst.beginEdit();
					dst.set("validTo", src.get("validTo"));
				dst.set("currency", src.get("settCurrId"));				
				dst.set("currencyCode", src.get("settCurrCode"));
				
				if (priceCtgryPricePer !== __FMBAS_TYPE__.PriceInd._EVENT_) {
					dst.set("unit", src.get("settUnitId"));
					dst.set("unitCode", src.get("settUnitCode"));
				}
				else {
					this._setDefaultLovValue_("priceCompEdit", "unitEvent", "EA", "id", "unit");
				}
				if (priceCtgryPricePer !== __FMBAS_TYPE__.PriceInd._PERCENT_) {
					dst.set("unit", src.get("settUnitId"));
					dst.set("unitCode", src.get("settUnitCode"));
				}
				else {
					this._setDefaultLovValue_("priceCompEdit", "unitPercent", "%", "id", "unit");
				}
				dst.endEdit();
			}
	
		}, this );
	
		shipTo.on("afterDoDeleteFailure", function() {
			contract.doCancel();
			contract.doReloadRecord();
		}, this);
		
		shipTo.on("afterDoDeleteSuccess", function() {
			contract.doReloadRecord();
		}, this);
		
		
		contractPriceCmpn.on("afterDoSaveSuccess", function (dc, ajaxResult) {
			var rec = dc.getRecord();
			if (rec) {
				var message = rec.get("toleranceMessage");
				if(!Ext.isEmpty(message)){
					Main.warning(message);
				}
			}					
			
			if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
				this._getWindow_("priceCompWdw").close();
			}
			if (ajaxResult.options.options.doCloseNewWindowAfterSavee === true) {
				this._getWindow_("priceCompWdwEdit").close();
			}
			this._get_("priceCompList").store.load();
			this._get_("priceComponentConv").store.load();
		}, this);
	
		contractPriceCmpnClassic.on("afterDoSaveSuccess", function (dc, ajaxResult) {
	        if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
	            this._getWindow_("priceCompWdw").close();
	        }
			this._reloadContractPriceCtgry_ = true;
			this._get_("priceCompList").store.load();
			this._get_("priceComponentConv").store.load();
	    }, this);
	
		contractPriceCmpnClassic.on("afterDoDeleteSuccess", function () {
			this._get_("priceCompList").store.load();
			this._get_("priceComponentConv").store.load();
	    }, this);
	
		pricingBase.mon(pricingBase.store, "update", function() {
			var view = this._get_("priceFixNew");
			if(view){
				var record = view._controller_.getRecord();
				view._applyStates_(record);
			}
		}, this);
	},

	
	
	_setPriceRestrictionEditorType_ : function( editor , ctx , view ) {
	
		var fieldIndex = ctx.field;
		var col = ctx.column;
		var textEditor = {xtype:"textfield", maxLength:32};	
		var numberEditor = {xtype:"numberfield", maxLength:32};	
		var storeData = [];
	
		if (fieldIndex === "value") {
			var r = ctx.record;
			if (r) {
				var restrictionType = r.get("restrictionType");
				if (!Ext.isEmpty(restrictionType)) {
					var cfg;
	
					//////////// Aircraft type ////////////
	
					if (restrictionType === __CMM_TYPE__.RestrictionTypes._AIRCRAFT_TYPE_) {
						cfg = {
							_dcView_ : view, 
							xtype:"fmbas_AcTypesLov_Lov", 
							maxLength:32, 
							typeAhead: false,
							remoteFilter: true,
							remoteSort: true,
							forceSelection: true,
							retFieldMapping: [{
								lovField:"code", 
								dsField: "value"}
							]
						};
					}
	
					//////////// Fueling type ////////////
	
					else if (restrictionType === __CMM_TYPE__.RestrictionTypes._FUELING_TYPE_) {
						for (var key in __CMM_TYPE__.FuelTicketFuelingOperation) {
							storeData.push(__CMM_TYPE__.FuelTicketFuelingOperation[key]);
						}
						cfg = {
						    xtype: "combo",
							store: storeData,
							forceSelection: true
						};
					}
	
					//////////// Transport type ////////////
	
					else if (restrictionType === __CMM_TYPE__.RestrictionTypes._TRANSPORT_TYPE_ ) {
						for (var key in __CMM_TYPE__.FuelingType) {
							storeData.push(__CMM_TYPE__.FuelingType[key]);
						}
						cfg = {
						    xtype: "combo",
						    store: storeData,
							forceSelection: true
						};
					}
	
					//////////// Registration number ////////////
	
					else if (restrictionType === __CMM_TYPE__.RestrictionTypes._REGISTRATION_NUMBER_ ) {							
						cfg = {xtype:"textfield", maxLength:5, enforceMaxLength : true};
					}
	
					//////////// Operation type ////////////
	
					else if (restrictionType === __CMM_TYPE__.RestrictionTypes._OPERATION_TYPE_ ) {
						for (var key in __OPS_TYPE__.OperationType) {
							storeData.push(__OPS_TYPE__.OperationType[key]);
						}
						cfg = {
						    xtype: "combo",
						    store: storeData,
							forceSelection: true
						};
					}
	
					//////////// Ship to ////////////
	
					else if (restrictionType === __CMM_TYPE__.RestrictionTypes._SHIP_TO_) {
						var contractDc = this._getDc_("contract");
						var contractRecord = contractDc.getRecord();
						var contractId;
						if(contractRecord){
							contractId = contractRecord.get("id");
						}
						cfg = {
							_dcView_ : view, 
							xtype:"cmm_ShipToCustomerCodeLov_Lov", 
							maxLength:32,
							typeAhead: false,
							remoteFilter: true,
							remoteSort: true,
							forceSelection: true,
							filterFieldMapping: [
								{lovField:"isCustomer", value: true}, 
				                {lovField:"contractId", value: contractId}
							],
							retFieldMapping: [{
								lovField:"code", 
								dsField: "value"
							}],								
						};
					}
	
					//////////// Time of operation ////////////
	
					else if (restrictionType === __CMM_TYPE__.RestrictionTypes._TIME_OF_OPERATION_ ) {
						cfg = {
	                        xtype: "combo",
	                        selectOnFocus: true,
	                        queryMode: "local",
	                        store: ["00:00","00:15","00:30","00:45","01:00","01:15","01:30","01:45","02:00","02:15","02:30","02:45","03:00","03:15","03:30","03:45","04:00","04:15","04:30","04:45","05:00","05:15","05:30","05:45","06:00","06:15","06:30","06:45","07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45","10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45","13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"]
	                    };
					}
	
					//////////// Flight type ////////////
	
					else if (restrictionType === __CMM_TYPE__.RestrictionTypes._FLIGHT_TYPE_  ) {
						for (var key in __CMM_TYPE__.FlightTypeIndicator) {
							storeData.push(__CMM_TYPE__.FlightTypeIndicator[key]);
						}
						cfg = {
						    xtype: "combo",
						    store: storeData,
							forceSelection: true
						};
					}
	
					//////////// Volume ////////////
	
					else if (restrictionType === __CMM_TYPE__.RestrictionTypes._VOLUME_  ) {
						cfg = numberEditor;
					}
	
					//////////// END CASE ////////////
	
					else {
						cfg = textEditor;
					}
					col.setEditor(cfg);
	
				}
			}
		}
	
		// Clear the value on restriction type change
	
		if (fieldIndex === "restrictionType") {
			var ed = col.getEditor();
			ed.on("change", function() {
				var record = view._controller_.getRecord();
				record.set("value", "");
				record.set("unit", null);
				record.set("unitCode", "");
			}, this);
		}
	}
	
});
