/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceComponent_Dc", {
	extend: "e4e.dc.AbstractDc",
	_infiniteScroll_: true,
	recordModel: atraxo.cmm.ui.extjs.ds.ContractPriceComponent_Ds,
		
			/* ================ Business functions ================ */
	
	canDoNew: function() {
		var pDc = this.getParent(); return (pDc && pDc.record && (!pDc.record.phantom) && pDc.record.data.priceCtgryName !='Index'); 
	}

});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceComponent_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.cmm_ContractPriceComponent_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateField({name:"validFrom", dataIndex:"validFrom"})
			.addDateField({name:"validTo", dataIndex:"validTo"})
			.addLov({name:"currencyCode", dataIndex:"currencyCode", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3}})
			.addLov({name:"unitCode", dataIndex:"unitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.UnitsLov_Lov", selectOnFocus:true, maxLength:2}})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceComponent_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.cmm_ContractPriceComponent_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addDateColumn({name:"validFrom", dataIndex:"validFrom", width:200, allowBlank: false, _enableFn_: function(dc, rec, col, fld) { return dc.record.data.priceCtgryName !='Index'; } , _mask_: Masks.DATE })
		.addDateColumn({name:"validTo", dataIndex:"validTo", width:200, allowBlank: false, _enableFn_: function(dc, rec, col, fld) { return dc.record.data.priceCtgryName !='Index'; } , _mask_: Masks.DATE })
		.addNumberColumn({name:"value", dataIndex:"price", width:200, allowBlank: false, sysDec:"dec_crncy", _enableFn_: function(dc, rec, col, fld) { return dc.record.data.priceCtgryName !='Index'; } , maxLength:19, align:"right" })
		.addLov({name:"currency", dataIndex:"currencyCode", width:200, allowBlank: false, _enableFn_: function(dc, rec, col, fld) { return dc.record.data.priceCtgryName !='Index'; } , xtype:"gridcolumn", 
			editor:{xtype:"fmbas_CurrenciesLov_Lov", allowBlank:false, maxLength:3,
				retFieldMapping: [{lovField:"id", dsField: "currency"} ],
				filterFieldMapping: [{lovField:"active", value: "true"} ]}})
		.addLov({name:"unit", dataIndex:"unitCode", width:200, allowBlank: false, _enableFn_: function(dc, rec, col, fld) { return dc.record.data.priceCtgryName !='Index'; } , xtype:"gridcolumn", 
			editor:{xtype:"fmbas_MeasurmentUnitsLov_Lov", allowBlank:false, maxLength:2,
				retFieldMapping: [{lovField:"id", dsField: "unit"} ]}})
		.addBooleanColumn({name:"provisional", dataIndex:"provisional", width:200, _enableFn_: function(dc, rec, col, fld) { return dc.record.data.priceCtgryName !='Index'; } })
		.addBooleanColumn({name:"withHold", dataIndex:"withHold", width:200})
		.addTextColumn({name:"contractPriceCatName", dataIndex:"contractPriceCatName", hidden:true, width:100, noEdit: true, maxLength:100})
		.addTextColumn({name:"priceCtgryName", dataIndex:"priceCtgryName", hidden:true, width:100, noEdit: true, maxLength:32})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceComponent_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ContractPriceComponent_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addNumberField({name:"price", bind:"{d.price}", dataIndex:"price", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index' && dc.record.data.priceCtgryPricePer != 'Composite'; } , allowBlank:false, width:180, sysDec:"dec_crncy", maxLength:19, decimals:_SYSTEMPARAMETERS_.dec_crncy})
		.addLov({name:"currency", bind:"{d.currencyCode}", dataIndex:"currencyCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index' && dc.record.data.priceCtgryPricePer != 'Composite'; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !='Percent'; } , allowBlank:false, width:80, noLabel: true, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3,
			retFieldMapping: [{lovField:"id", dsField: "currency"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addLov({name:"unit", bind:"{d.unitCode}", dataIndex:"unitCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index' && dc.record.data.priceCtgryPricePer != 'Composite'; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer !='Event' && dc.record.data.priceCtgryPricePer !='Percent'; } , allowBlank:false, width:60, noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "unit"} ]})
		.addLov({name:"unitEvent", bind:"{d.unitCode}", dataIndex:"unitCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index' && dc.record.data.priceCtgryPricePer != 'Composite'; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer =='Event'; } , allowBlank:false, width:60, noLabel: true, xtype:"fmbas_MeasurmentUnitsEventLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "unit"} ]})
		.addLov({name:"unitPercent", bind:"{d.unitCode}", dataIndex:"unitCode", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer != 'Composite'; } , _visibleFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer =='Percent'; } , allowBlank:false, width:60, noLabel: true, xtype:"fmbas_MeasurmentUnitsPercentLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "unit"} ]})
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index'; } , allowBlank:false, width:220})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index'; } , allowBlank:false, width:220})
		.addBooleanField({ name:"provisional", bind:"{d.provisional}", dataIndex:"provisional", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryName !='Index' && dc.record.data.priceCtgryPricePer != 'Composite'; } })
		.addBooleanField({ name:"withHold", bind:"{d.withHold}", dataIndex:"withHold", _enableFn_: function(dc, rec) { return dc.record.data.priceCtgryPricePer != 'Composite'; } })
		.add({name:"row", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("price"),this._getConfig_("currency"),this._getConfig_("unit"),this._getConfig_("unitEvent"),this._getConfig_("unitPercent")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:380, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["row", "validFrom", "validTo", "provisional", "withHold"]);
	}
});

/* ================= GRID: SimpleList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceComponent_Dc$SimpleList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_ContractPriceComponent_Dc$SimpleList",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"validFrom", dataIndex:"validFrom", width:150, _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"validTo", width:150, _mask_: Masks.DATE})
		.addNumberColumn({ name:"value", dataIndex:"price", width:150, sysDec:"dec_crncy",  decimals:_SYSTEMPARAMETERS_.dec_crncy})
		.addTextColumn({ name:"currency", dataIndex:"currencyCode", width:150})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:150})
		.addTextColumn({ name:"contractPriceCatName", dataIndex:"contractPriceCatName", hidden:true, width:100})
		.addTextColumn({ name:"priceCtgryName", dataIndex:"priceCtgryName", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
