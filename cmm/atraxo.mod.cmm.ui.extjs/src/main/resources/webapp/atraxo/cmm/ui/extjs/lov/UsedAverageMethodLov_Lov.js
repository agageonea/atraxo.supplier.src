/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.lov.UsedAverageMethodLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.cmm_UsedAverageMethodLov_Lov",
	displayField: "name", 
	_columns_: ["code", "name"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{code}, {name}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.cmm.ui.extjs.ds.UsedAverageMethodsLov_Ds
});
