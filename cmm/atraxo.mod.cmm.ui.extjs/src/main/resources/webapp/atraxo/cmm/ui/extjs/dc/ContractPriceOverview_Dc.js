/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceOverview_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.cmm.ui.extjs.ds.ContractPriceOverview_Ds
});

/* ================= GRID: PriceList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceOverview_Dc$PriceList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.cmm_ContractPriceOverview_Dc$PriceList",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"priceCategory", dataIndex:"priceCtgryName", width:120})
		.addTextColumn({ name:"mainCategory", dataIndex:"mainCategoryCode", width:160})
		.addBooleanColumn({ name:"restrictionsParameter", dataIndex:"restriction", width:170})
		.addNumberColumn({ name:"price", dataIndex:"price", width:120, sysDec:"dec_crncy"})
		.addTextColumn({ name:"currency", dataIndex:"currencyCode", width:80})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:80})
		.addDateColumn({ name:"validFrom", dataIndex:"validFrom", width:200, _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"validTo", width:200, _mask_: Masks.DATE})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
