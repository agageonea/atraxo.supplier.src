/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.cmm.ui.extjs.ds.ContractOverview_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "cmm_ContractOverview_Ds"
	},
	
	
	fields: [
		{name:"locId", type:"int", allowNull:true},
		{name:"locCode", type:"string"},
		{name:"counterPartyId", type:"int", allowNull:true},
		{name:"counterPartyCode", type:"string"},
		{name:"settCurrId", type:"int", allowNull:true},
		{name:"settCurrCode", type:"string"},
		{name:"settUnitId", type:"int", allowNull:true},
		{name:"settUnitCode", type:"string"},
		{name:"price", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"iplId", type:"int", allowNull:true},
		{name:"iplCode", type:"string"},
		{name:"resBuyerId", type:"string"},
		{name:"resBuyerName", type:"string"},
		{name:"contactId", type:"int", allowNull:true},
		{name:"contactName", type:"string"},
		{name:"dealType", type:"string"},
		{name:"code", type:"string"},
		{name:"type", type:"string"},
		{name:"subType", type:"string"},
		{name:"scope", type:"string"},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"paymentTerms", type:"int", allowNull:true},
		{name:"creditTerms", type:"string"},
		{name:"status", type:"string"},
		{name:"limitedTo", type:"string"},
		{name:"product", type:"string"},
		{name:"tax", type:"string"},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.cmm.ui.extjs.ds.ContractOverview_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"locId", type:"int", allowNull:true},
		{name:"locCode", type:"string"},
		{name:"counterPartyId", type:"int", allowNull:true},
		{name:"counterPartyCode", type:"string"},
		{name:"settCurrId", type:"int", allowNull:true},
		{name:"settCurrCode", type:"string"},
		{name:"settUnitId", type:"int", allowNull:true},
		{name:"settUnitCode", type:"string"},
		{name:"price", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"iplId", type:"int", allowNull:true},
		{name:"iplCode", type:"string"},
		{name:"resBuyerId", type:"string"},
		{name:"resBuyerName", type:"string"},
		{name:"contactId", type:"int", allowNull:true},
		{name:"contactName", type:"string"},
		{name:"dealType", type:"string"},
		{name:"code", type:"string"},
		{name:"type", type:"string"},
		{name:"subType", type:"string"},
		{name:"scope", type:"string"},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"paymentTerms", type:"int", allowNull:true},
		{name:"creditTerms", type:"string"},
		{name:"status", type:"string"},
		{name:"limitedTo", type:"string"},
		{name:"product", type:"string"},
		{name:"tax", type:"string"},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.cmm.ui.extjs.ds.ContractOverview_DsParam", {
	extend: 'Ext.data.Model',


	validators: {
		p_currency: [{type: 'presence'}],
		p_unit: [{type: 'presence'}]
	},


	initParam: function() {
		this.set("p_currency", _SYSTEMPARAMETERS_.syscrncy);
		this.set("p_unit", _SYSTEMPARAMETERS_.sysvol);
	},

	fields: [
		{name:"p_currency", type:"string"},
		{name:"p_unit", type:"string"}
	]
});
