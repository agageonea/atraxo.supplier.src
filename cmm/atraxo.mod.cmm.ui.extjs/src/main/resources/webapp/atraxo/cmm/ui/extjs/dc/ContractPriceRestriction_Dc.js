/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceRestriction_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.ContractPriceRestriction_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.ContractPriceRestriction_Ds
});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ContractPriceRestriction_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.cmm_ContractPriceRestriction_Dc$List",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addComboColumn({name:"restrictionType", dataIndex:"restrictionType", width:200, 
			editor:{xtype:"combo", mode: 'local', triggerAction:'all', forceSelection:true, store:[ __CMM_TYPE__.RestrictionTypes._VOLUME_, __CMM_TYPE__.RestrictionTypes._AIRCRAFT_TYPE_, __CMM_TYPE__.RestrictionTypes._FUELING_TYPE_, __CMM_TYPE__.RestrictionTypes._TRANSPORT_TYPE_, __CMM_TYPE__.RestrictionTypes._FLIGHT_TYPE_, __CMM_TYPE__.RestrictionTypes._REGISTRATION_NUMBER_, __CMM_TYPE__.RestrictionTypes._TIME_OF_OPERATION_, __CMM_TYPE__.RestrictionTypes._SHIP_TO_]}})
		.addComboColumn({name:"operation", dataIndex:"operator", width:200, 
			editor:{xtype:"combo", mode: 'local', triggerAction:'all', forceSelection:true, store:[ __CMM_TYPE__.Operators._EQUAL_, __CMM_TYPE__.Operators._LOWER_, __CMM_TYPE__.Operators._HIGHER_, __CMM_TYPE__.Operators._LOWER_EQ_, __CMM_TYPE__.Operators._HIGHER_EQ_, __CMM_TYPE__.Operators._IN_, __CMM_TYPE__.Operators._NOT_EQUAL_]}})
		.addTextColumn({name:"value", dataIndex:"value", width:200, _enableFn_: function(dc, rec, col, fld) { return !Ext.isEmpty(dc.record.data.restrictionType); } , maxLength:100, 
			editor: { xtype:"textfield", maxLength:100}})
		.addLov({name:"unit", dataIndex:"unitCode", width:200, _enableFn_: function(dc, rec, col, fld) { return dc.record.data.restrictionType == __CMM_TYPE__.RestrictionTypes._VOLUME_; } , xtype:"gridcolumn", 
			editor:{xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
				retFieldMapping: [{lovField:"id", dsField: "unit"} ]}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
