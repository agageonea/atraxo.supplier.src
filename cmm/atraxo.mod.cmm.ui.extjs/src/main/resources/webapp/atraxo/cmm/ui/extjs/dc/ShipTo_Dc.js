/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.cmm.ui.extjs.dc.ShipTo_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.cmm.ui.extjs.ds.ShipTo_DsParam,
	recordModel: atraxo.cmm.ui.extjs.ds.ShipTo_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.cmm.ui.extjs.dc.ShipTo_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.cmm_ShipTo_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"customer", dataIndex:"customerCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "customerId"} ]}})
			.addLov({name:"customerName", dataIndex:"customerName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "customerId"} ]}})
			.addDateField({name:"validFrom", dataIndex:"validFrom"})
			.addDateField({name:"validTo", dataIndex:"validTo"})
			.addNumberField({name:"allocatedVolume", dataIndex:"allocatedVolume", sysDec:"dec_unit", maxLength:21})
			.addNumberField({name:"adjustedVolume", dataIndex:"adjustedVolume", sysDec:"dec_unit", maxLength:21})
			.addNumberField({name:"actualVolume", dataIndex:"actualVolume", sysDec:"dec_unit", maxLength:21})
			.addLov({name:"settlementUnit", dataIndex:"settlementUnitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.UnitsLov_Lov", selectOnFocus:true, maxLength:2,
					retFieldMapping: [{lovField:"id", dsField: "settlementUnitId"} ]}})
		;
	}

});

/* ================= EDIT FORM: EditRemark ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ShipTo_Dc$EditRemark", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.cmm_ShipTo_Dc$EditRemark",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.remarks}", paramIndex:"remarks", labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  layout:"fit"})
		.addPanel({ name:"remarkCol1", layout:"fit"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["remarkCol1"])
		.addChildrenTo("remarkCol1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
						remarks.setReadOnly(false);
						return true;
	}
});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ShipTo_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.cmm_ShipTo_Dc$EditList",
	_noPaginator_: true,

	/**
	 * 
	 */
	initComponent : function(config) {
		Ext.apply(this, {
			features: [{
				ftype: 'summary',
				remoteRoot: 'summaries',
				dock: 'bottom'
			}]
		});
		this.callParent(arguments);
	},
	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"shipTo", dataIndex:"customerCode", width:150, allowBlank: false, _enableFn_: function(dc, rec, col, fld) { return dc.record.data.id == null; } , xtype:"gridcolumn", 
			editor:{xtype:"fmbas_CustomerCodeLov_Lov", allowBlank:false, maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "customerId"} ,{lovField:"name", dsField: "customerName"} ]}})
		.addLov({name:"customerName", dataIndex:"customerName", width:150, noEdit: true, xtype:"gridcolumn"})
		.addDateColumn({name:"validFrom", dataIndex:"validFrom", width:150, allowBlank: false, _enableFn_: function(dc, rec, col, fld) { return dc.record.data.approvalStatus !== __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_; } , _mask_: Masks.DATE })
		.addDateColumn({name:"validTo", dataIndex:"validTo", width:150, allowBlank: false, _enableFn_: function(dc, rec, col, fld) { return dc.record.data.approvalStatus !== __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_; } , _mask_: Masks.DATE })
		.addNumberColumn({name:"allocatedVolume", dataIndex:"allocatedVolume", width:150, sysDec:"dec_unit",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_unit']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, _enableFn_: this._enableIfDraft_, maxLength:21, align:"right" })
		.addNumberColumn({name:"adjustedVolume", dataIndex:"adjustedVolume", width:150, sysDec:"dec_unit",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_unit']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, _enableFn_: function(dc, rec, col, fld) { return dc.record.data.approvalStatus !== __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_; } , maxLength:21, align:"right",listeners:{
			blur:{scope:this, fn:this._setVolumeChangedFlag_}, buffer: Main.viewConfig.GRID_EDITOR_CHANGE_EVT_BUFFER
		} })
		.addNumberColumn({name:"actualVolume", dataIndex:"actualVolume", width:150, noEdit: true, sysDec:"dec_unit",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_unit']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, maxLength:21, align:"right" })
		.addTextColumn({name:"settlementUnitCode", dataIndex:"settlementUnitCode", width:150, noEdit: true, maxLength:2})
		.addTextColumn({name:"contractPeriod", dataIndex:"contractPeriod", hidden:true, width:150, noEdit: true, maxLength:32})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._unitParamName_ = "unitParameter";
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_canChangeShipTo_: function() {
		
						if  ( _SYSTEMPARAMETERS_.syssalescontractapproval !== "true"){
							return true;
						}
						var dc = this._controller_;
						var rec = dc.getRecord();
						if (rec){
							return __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_ !== rec.get("approvalStatus") ;
						}
						return true;
	},
	
	_setVolumeChangedFlag_: function() {
		
						var dc = this._controller_;
						dc._volumeChanged_ = true;
	},
	
	_enableIfDraft_: function() {
						
						var dc = this._controller_;
						var contractStatus = dc.params.get("contractStatus");				
						return contractStatus === "Draft" && dc.record.data.approvalStatus !== __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_;					
	},
	
	_disableIfDraft_: function() {
		
						var result = true;
						var dc = this._controller_;
						var contractStatus = dc.params.get("contractStatus");
						
						if (contractStatus === "Draft") {
							result = false;
						}
						
						return result;
	}
});

/* ================= EDIT-GRID: EditListPurchase ================= */

Ext.define("atraxo.cmm.ui.extjs.dc.ShipTo_Dc$EditListPurchase", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.cmm_ShipTo_Dc$EditListPurchase",
	_noPaginator_: true,

	/**
	 * 
	 */
	initComponent : function(config) {
		Ext.apply(this, {
			features: [{
				ftype: 'summary',
				remoteRoot: 'summaries',
				dock: 'bottom'
			}]
		});
		this.callParent(arguments);
	},
	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"shipTo", dataIndex:"customerCode", width:150, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_CustomerCodeLov_Lov", allowBlank:false, maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "customerId"} ,{lovField:"name", dsField: "customerName"} ]}})
		.addLov({name:"customerName", dataIndex:"customerName", width:150, noEdit: true, xtype:"gridcolumn"})
		.addDateColumn({name:"validFrom", dataIndex:"validFrom", width:150, allowBlank: false, _mask_: Masks.DATE })
		.addDateColumn({name:"validTo", dataIndex:"validTo", width:150, allowBlank: false, _mask_: Masks.DATE })
		.addNumberColumn({name:"allocatedVolume", dataIndex:"allocatedVolume", width:150, sysDec:"dec_unit",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_unit']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, _enableFn_: this._enableIfDraft_, maxLength:21, align:"right" })
		.addNumberColumn({name:"adjustedVolume", dataIndex:"adjustedVolume", width:150, sysDec:"dec_unit",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_unit']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, maxLength:21, align:"right",listeners:{
			blur:{scope:this, fn:this._setVolumeChangedFlag_}, buffer: Main.viewConfig.GRID_EDITOR_CHANGE_EVT_BUFFER
		} })
		.addNumberColumn({name:"actualVolume", dataIndex:"actualVolume", width:150, noEdit: true, sysDec:"dec_unit",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_unit']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, maxLength:21, align:"right" })
		.addTextColumn({name:"settlementUnitCode", dataIndex:"settlementUnitCode", width:150, noEdit: true, maxLength:2})
		.addTextColumn({name:"contractPeriod", dataIndex:"contractPeriod", hidden:true, width:150, noEdit: true, maxLength:32})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._unitParamName_ = "unitParameter";
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_setVolumeChangedFlag_: function() {
		
						var dc = this._controller_;
						dc._volumeChanged_ = true;
	},
	
	_enableIfDraft_: function() {
		
						var result = false;
						var dc = this._controller_;
						var contractStatus = dc.params.get("contractStatus");
						
						if (contractStatus === "Draft") {
							result = true;
						}
						
						return result;
	},
	
	_disableIfDraft_: function() {
		
						var result = true;
						var dc = this._controller_;
						var contractStatus = dc.params.get("contractStatus");
						
						if (contractStatus === "Draft") {
							result = false;
						}
						
						return result;
	}
});
