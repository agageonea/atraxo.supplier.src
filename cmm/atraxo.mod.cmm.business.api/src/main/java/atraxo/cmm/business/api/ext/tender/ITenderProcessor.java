/**
 *
 */
package atraxo.cmm.business.api.ext.tender;

import javax.xml.ws.handler.MessageContext;

import atraxo.cmm.domain.impl.tender.Tender;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcknowledge;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderCancel;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderInvitation;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderNewRound;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderResponse;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderUpdate;

/**
 * @author zspeter
 */
public interface ITenderProcessor {

	FuelTenderResponse fuelTenderInvitation(FuelTenderInvitation tenderInvitation, MessageContext messageContext);

	FuelTenderResponse fuelTenderNewRound(FuelTenderNewRound tenderNewRound, MessageContext messageContext);

	FuelTenderResponse fuelTenderCancel(FuelTenderCancel tenderCancel, MessageContext messageContext);

	FuelTenderResponse fuelTenderUpdate(FuelTenderUpdate tenderUpdate, MessageContext messageContext);

	FuelTenderResponse fuelTenderAcknowledgement(FuelTenderAcknowledge tenderAcknowledgement, MessageContext messageContext);

	void sendReadNotification(Tender tender) throws BusinessException;
}
