/**
 *
 */
package atraxo.cmm.business.api.ext.bid;

import java.util.List;

import atraxo.cmm.domain.impl.contracts.Contract;

/**
 * @author zspeter
 */
public interface IBidVersionService {

	/**
	 * Calculate the next version for the input bids.
	 * 
	 * @param bids
	 * @return
	 */
	String calculateBidVersion(List<Contract> bids);
}
