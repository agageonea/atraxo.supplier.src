/**
 *
 */
package atraxo.cmm.business.api.ext.bid;

import javax.xml.ws.handler.MessageContext;

import seava.j4e.iata.fuelplus.iata.tender.FuelTenderAcceptAward;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBid;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBidAward;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBidDecline;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderCancelBid;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderDeclineAward;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderNoBid;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderResponse;

/**
 * @author zspeter
 */
public interface IBidProcessor {

	FuelTenderResponse fuelTenderBidDecline(FuelTenderBidDecline parameters, MessageContext messageContext);

	FuelTenderResponse fuelTenderBidAward(FuelTenderBidAward parameters, MessageContext messageContext);

	FuelTenderResponse fuelTenderNoBid(FuelTenderNoBid parameters);

	FuelTenderResponse fuelTenderBid(FuelTenderBid parameters);

	FuelTenderResponse fuelTenderCancelBid(FuelTenderCancelBid parameters);

	FuelTenderResponse fuelTenderAcceptAward(FuelTenderAcceptAward parameters);

	FuelTenderResponse fuelTenderDeclineAward(FuelTenderDeclineAward parameters);
}
