/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.contracts;

import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.cmm.domain.ext.contracts.CostVat;
import atraxo.cmm.domain.ext.contracts.SupplierContract;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractScope;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.ContractInvoice;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Contract} domain entity.
 */
public interface IContractService extends IEntityService<Contract> {

	/**
	 * Custom service getSupplierContract
	 *
	 * @return SupplierContract
	 */
	public SupplierContract getSupplierContract(Integer contractId,
			Date dateFrom, Date dateTo, Integer currencyId, Integer unitId,
			Integer avgMthdId, Integer finSrcId, MasterAgreementsPeriod offset)
			throws BusinessException;

	/**
	 * Custom service getSupplierContracts
	 *
	 * @return List
	 */
	public List getSupplierContracts(Locations location, Date dateFrom,
			Date dateTo, Currencies currency, Unit unit, AverageMethod avgMthd,
			FinancialSources finSrc, MasterAgreementsPeriod offset)
			throws BusinessException;

	/**
	 * Custom service getPrice
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getPrice(Integer id, String unitCode,
			String currencyCode, Date date) throws BusinessException;

	/**
	 * Custom service isUpdated
	 *
	 * @return Boolean
	 */
	public Boolean isUpdated(Integer id, String unitCode, String currencyCode)
			throws BusinessException;

	/**
	 * Custom service getPriceVat
	 *
	 * @return CostVat
	 */
	public CostVat getPriceVat(Integer id, String unitCode,
			String currencyCode, Date date, FlightTypeIndicator eventType)
			throws BusinessException;

	/**
	 * Custom service setActive
	 *
	 * @return void
	 */
	public void setActive(Contract contract) throws BusinessException;

	/**
	 * Custom service findBySupplierStatusLocationScopeType
	 *
	 * @return List<Contract>
	 */
	public List<Contract> findBySupplierStatusLocationScopeType(
			Suppliers supplier, ContractStatus status, Locations location,
			ContractScope scope, ContractType type) throws BusinessException;

	/**
	 * Custom service findByDateCustomerSupplierLocationFlightType
	 *
	 * @return List<Contract>
	 */
	public List<Contract> findByDateCustomerSupplierLocationFlightType(
			Date date, Suppliers supplier, Locations location, String type,
			String dealType) throws BusinessException;

	/**
	 * Custom service findByHolderLocation
	 *
	 * @return List<Contract>
	 */
	public List<Contract> findByHolderLocation(Customer holder,
			Locations location, Boolean isContract) throws BusinessException;

	/**
	 * Custom service findByLocation
	 *
	 * @return List<Contract>
	 */
	public List<Contract> findByLocation(Locations location, Boolean isContract)
			throws BusinessException;

	/**
	 * Custom service reset
	 *
	 * @return void
	 */
	public void reset(Contract contract, String remark)
			throws BusinessException;

	/**
	 * Custom service updateStatus
	 *
	 * @return void
	 */
	public void updateStatus(StringBuilder contractInfo)
			throws BusinessException;

	/**
	 * Custom service updateStatus
	 *
	 * @return void
	 */
	public void updateStatus(Contract contract) throws BusinessException;

	/**
	 * Custom service findContractByDeliveryDateCustomerLocation
	 *
	 * @return List<Contract>
	 */
	public List<Contract> findContractByDeliveryDateCustomerLocation(
			Integer customerId, Date deliveryDate, DealType dealType,
			Locations location) throws BusinessException;

	/**
	 * Custom service insertWithoutBusinessLogic
	 *
	 * @return void
	 */
	public void insertWithoutBusinessLogic(Contract contract)
			throws BusinessException;

	/**
	 * Custom service updateWithoutBusinessLogic
	 *
	 * @return void
	 */
	public void updateWithoutBusinessLogic(List<Contract> contracts)
			throws BusinessException;

	/**
	 * Custom service generateResaleContract
	 *
	 * @return Contract
	 */
	public Contract generateResaleContract(Contract contract, Customer customer)
			throws BusinessException;

	/**
	 * Custom service generateInternalResaleContract
	 *
	 * @return Contract
	 */
	public Contract generateInternalResaleContract(Contract contract)
			throws BusinessException;

	/**
	 * Custom service copyAndSaveSaleContract
	 *
	 * @return Integer
	 */
	public Integer copyAndSaveSaleContract(Contract contract, Customer customer)
			throws BusinessException;

	/**
	 * Custom service copyAndSaveBuyContract
	 *
	 * @return Integer
	 */
	public Integer copyAndSaveBuyContract(Contract contract, Suppliers supplier)
			throws BusinessException;

	/**
	 * Custom service getBlueprintSaleContract
	 *
	 * @return Contract
	 */
	public Contract getBlueprintSaleContract(Contract contract)
			throws BusinessException;

	/**
	 * Custom service findSaleContracts
	 *
	 * @return List<Contract>
	 */
	public List<Contract> findSaleContracts(Integer holderId,
			Customer customer, FlightTypeIndicator flightType,
			ContractType type, Locations location, Date date,
			Product productType) throws BusinessException;

	/**
	 * Custom service findPurchaseContracts
	 *
	 * @return List<Contract>
	 */
	public List<Contract> findPurchaseContracts(Suppliers supplier,
			FlightTypeIndicator flightType, Locations location, Date date,
			Product productType) throws BusinessException;

	/**
	 * Custom service findSaleContracts
	 *
	 * @return List<Contract>
	 */
	public List<Contract> findSaleContracts(FlightTypeIndicator flightType,
			ContractType type, Locations location, Date date,
			Product productType) throws BusinessException;

	/**
	 * Custom service findAllEffectiveSalesContracts
	 *
	 * @return List<Contract>
	 */
	public List<Contract> findAllEffectiveSalesContracts()
			throws BusinessException;

	/**
	 * Custom service checkIfCanActivateUpdate
	 *
	 * @return void
	 */
	public void checkIfCanActivateUpdate(Contract contract, Boolean update)
			throws BusinessException;

	/**
	 * Custom service checkIfCanSendForAppoval
	 *
	 * @return void
	 */
	public void checkIfCanSendForAppoval(Contract contract)
			throws BusinessException;

	/**
	 * Custom service findSiblingContract
	 *
	 * @return Contract
	 */
	public Contract findSiblingContract(Contract c) throws BusinessException;

	/**
	 * Custom service cancelTenderBid
	 *
	 * @return void
	 */
	public void cancelTenderBid(List<Contract> bids, String reason)
			throws BusinessException;

	/**
	 * Custom service acceptAwardTenderBid
	 *
	 * @return void
	 */
	public void acceptAwardTenderBid(Contract bid) throws BusinessException;

	/**
	 * Custom service declineAwardTenderBid
	 *
	 * @return void
	 */
	public void declineAwardTenderBid(Contract bid, String reason)
			throws BusinessException;

	/**
	 * Custom service generateSaleContractFromBid
	 *
	 * @return Integer
	 */
	public Integer generateSaleContractFromBid(Contract bid)
			throws BusinessException;

	/**
	 * Custom service addHistoryEntry
	 *
	 * @return void
	 */
	public void addHistoryEntry(Contract e, String action, String reason)
			throws BusinessException;

	/**
	 * Custom service submitForApproval
	 *
	 * @return void
	 */
	public void submitForApproval(Contract e, BidApprovalStatus status,
			String action, String reason) throws BusinessException;

	/**
	 * Custom service calculateRevisionVersion
	 *
	 * @return Integer
	 */
	public Integer calculateRevisionVersion(List<Contract> bids, String version)
			throws BusinessException;

	/**
	 * Custom service approve
	 *
	 * @return void
	 */
	public void approve(Contract e, String action, String reason)
			throws BusinessException;

	/**
	 * Custom service reject
	 *
	 * @return void
	 */
	public void reject(Contract e, String action, String reason)
			throws BusinessException;

	/**
	 * Custom service submitForApprovalPeriod
	 *
	 * @return void
	 */
	public void submitForApprovalPeriod(Contract e, BidApprovalStatus status,
			String action, String reason) throws BusinessException;

	/**
	 * Custom service submitForApprovalPrice
	 *
	 * @return void
	 */
	public void submitForApprovalPrice(Contract e, BidApprovalStatus status,
			String action, String reason) throws BusinessException;

	/**
	 * Custom service submitForApprovalShipTo
	 *
	 * @return void
	 */
	public void submitForApprovalShipTo(Contract e, BidApprovalStatus status,
			String action, String reason) throws BusinessException;

	/**
	 * Custom service approvePeriodUpdate
	 *
	 * @return void
	 */
	public void approvePeriodUpdate(Contract blueprint, String reason)
			throws BusinessException;

	/**
	 * Custom service rejectPeriodUpdate
	 *
	 * @return void
	 */
	public void rejectPeriodUpdate(Contract blueprint, String reason)
			throws BusinessException;

	/**
	 * Custom service approvePriceUpdate
	 *
	 * @return void
	 */
	public void approvePriceUpdate(Contract blueprint, String reason)
			throws BusinessException;

	/**
	 * Custom service rejectPriceUpdate
	 *
	 * @return void
	 */
	public void rejectPriceUpdate(Contract blueprint, String reason)
			throws BusinessException;

	/**
	 * Custom service approveShipToUpdate
	 *
	 * @return void
	 */
	public void approveShipToUpdate(Contract blueprint, String reason)
			throws BusinessException;

	/**
	 * Custom service rejectShipToUpdate
	 *
	 * @return void
	 */
	public void rejectShipToUpdate(Contract blueprint, String reason)
			throws BusinessException;

	/**
	 * Custom service persistChanges
	 *
	 * @return void
	 */
	public void persistChanges(Contract e) throws BusinessException;

	/**
	 * Custom service terminateAllWorkflows
	 *
	 * @return void
	 */
	public void terminateAllWorkflows(Contract contract)
			throws BusinessException;

	/**
	 * Custom service discardBlueprint
	 *
	 * @return void
	 */
	public void discardBlueprint(Contract blueprint, Contract original)
			throws BusinessException;

	/**
	 * Custom service publishBlueprint
	 *
	 * @return void
	 */
	public void publishBlueprint(Contract blueprint, Contract original)
			throws BusinessException;

	/**
	 * Custom service checkContractConditions
	 *
	 * @return void
	 */
	public void checkContractConditions(Contract contract)
			throws BusinessException;

	/**
	 * Custom service hasResaleReference
	 *
	 * @return Boolean
	 */
	public Boolean hasResaleReference(Contract contract)
			throws BusinessException;

	/**
	 * Custom service validateBid
	 *
	 * @return void
	 */
	public void validateBid(Contract bid) throws BusinessException;

	/**
	 * Custom service validateBid
	 *
	 * @return void
	 */
	public void validateBid(List<Contract> bid) throws BusinessException;

	/**
	 * Custom service deleteInternalResale
	 *
	 * @return void
	 */
	public void deleteInternalResale(Contract contract)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Contract
	 */
	public Contract findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return Contract
	 */
	public Contract findByBusiness(Integer id);

	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<Contract>
	 */
	public List<Contract> findByLocation(Locations location);

	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<Contract>
	 */
	public List<Contract> findByLocationId(Integer locationId);

	/**
	 * Find by reference: holder
	 *
	 * @param holder
	 * @return List<Contract>
	 */
	public List<Contract> findByHolder(Customer holder);

	/**
	 * Find by ID of reference: holder.id
	 *
	 * @param holderId
	 * @return List<Contract>
	 */
	public List<Contract> findByHolderId(Integer holderId);

	/**
	 * Find by reference: intoPlaneAgent
	 *
	 * @param intoPlaneAgent
	 * @return List<Contract>
	 */
	public List<Contract> findByIntoPlaneAgent(Suppliers intoPlaneAgent);

	/**
	 * Find by ID of reference: intoPlaneAgent.id
	 *
	 * @param intoPlaneAgentId
	 * @return List<Contract>
	 */
	public List<Contract> findByIntoPlaneAgentId(Integer intoPlaneAgentId);

	/**
	 * Find by reference: responsibleBuyer
	 *
	 * @param responsibleBuyer
	 * @return List<Contract>
	 */
	public List<Contract> findByResponsibleBuyer(UserSupp responsibleBuyer);

	/**
	 * Find by ID of reference: responsibleBuyer.id
	 *
	 * @param responsibleBuyerId
	 * @return List<Contract>
	 */
	public List<Contract> findByResponsibleBuyerId(String responsibleBuyerId);

	/**
	 * Find by reference: contact
	 *
	 * @param contact
	 * @return List<Contract>
	 */
	public List<Contract> findByContact(Contacts contact);

	/**
	 * Find by ID of reference: contact.id
	 *
	 * @param contactId
	 * @return List<Contract>
	 */
	public List<Contract> findByContactId(Integer contactId);

	/**
	 * Find by reference: contractVolumeUnit
	 *
	 * @param contractVolumeUnit
	 * @return List<Contract>
	 */
	public List<Contract> findByContractVolumeUnit(Unit contractVolumeUnit);

	/**
	 * Find by ID of reference: contractVolumeUnit.id
	 *
	 * @param contractVolumeUnitId
	 * @return List<Contract>
	 */
	public List<Contract> findByContractVolumeUnitId(
			Integer contractVolumeUnitId);

	/**
	 * Find by reference: resaleRef
	 *
	 * @param resaleRef
	 * @return List<Contract>
	 */
	public List<Contract> findByResaleRef(Contract resaleRef);

	/**
	 * Find by ID of reference: resaleRef.id
	 *
	 * @param resaleRefId
	 * @return List<Contract>
	 */
	public List<Contract> findByResaleRefId(Integer resaleRefId);

	/**
	 * Find by reference: settlementCurr
	 *
	 * @param settlementCurr
	 * @return List<Contract>
	 */
	public List<Contract> findBySettlementCurr(Currencies settlementCurr);

	/**
	 * Find by ID of reference: settlementCurr.id
	 *
	 * @param settlementCurrId
	 * @return List<Contract>
	 */
	public List<Contract> findBySettlementCurrId(Integer settlementCurrId);

	/**
	 * Find by reference: settlementUnit
	 *
	 * @param settlementUnit
	 * @return List<Contract>
	 */
	public List<Contract> findBySettlementUnit(Unit settlementUnit);

	/**
	 * Find by ID of reference: settlementUnit.id
	 *
	 * @param settlementUnitId
	 * @return List<Contract>
	 */
	public List<Contract> findBySettlementUnitId(Integer settlementUnitId);

	/**
	 * Find by reference: averageMethod
	 *
	 * @param averageMethod
	 * @return List<Contract>
	 */
	public List<Contract> findByAverageMethod(AverageMethod averageMethod);

	/**
	 * Find by ID of reference: averageMethod.id
	 *
	 * @param averageMethodId
	 * @return List<Contract>
	 */
	public List<Contract> findByAverageMethodId(Integer averageMethodId);

	/**
	 * Find by reference: financialSource
	 *
	 * @param financialSource
	 * @return List<Contract>
	 */
	public List<Contract> findByFinancialSource(FinancialSources financialSource);

	/**
	 * Find by ID of reference: financialSource.id
	 *
	 * @param financialSourceId
	 * @return List<Contract>
	 */
	public List<Contract> findByFinancialSourceId(Integer financialSourceId);

	/**
	 * Find by reference: riskHolder
	 *
	 * @param riskHolder
	 * @return List<Contract>
	 */
	public List<Contract> findByRiskHolder(Customer riskHolder);

	/**
	 * Find by ID of reference: riskHolder.id
	 *
	 * @param riskHolderId
	 * @return List<Contract>
	 */
	public List<Contract> findByRiskHolderId(Integer riskHolderId);

	/**
	 * Find by reference: supplier
	 *
	 * @param supplier
	 * @return List<Contract>
	 */
	public List<Contract> findBySupplier(Suppliers supplier);

	/**
	 * Find by ID of reference: supplier.id
	 *
	 * @param supplierId
	 * @return List<Contract>
	 */
	public List<Contract> findBySupplierId(Integer supplierId);

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<Contract>
	 */
	public List<Contract> findByCustomer(Customer customer);

	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<Contract>
	 */
	public List<Contract> findByCustomerId(Integer customerId);

	/**
	 * Find by reference: billTo
	 *
	 * @param billTo
	 * @return List<Contract>
	 */
	public List<Contract> findByBillTo(Customer billTo);

	/**
	 * Find by ID of reference: billTo.id
	 *
	 * @param billToId
	 * @return List<Contract>
	 */
	public List<Contract> findByBillToId(Integer billToId);

	/**
	 * Find by reference: bidTenderIdentification
	 *
	 * @param bidTenderIdentification
	 * @return List<Contract>
	 */
	public List<Contract> findByBidTenderIdentification(
			Tender bidTenderIdentification);

	/**
	 * Find by ID of reference: bidTenderIdentification.id
	 *
	 * @param bidTenderIdentificationId
	 * @return List<Contract>
	 */
	public List<Contract> findByBidTenderIdentificationId(
			Integer bidTenderIdentificationId);

	/**
	 * Find by reference: bidTenderLocation
	 *
	 * @param bidTenderLocation
	 * @return List<Contract>
	 */
	public List<Contract> findByBidTenderLocation(
			TenderLocation bidTenderLocation);

	/**
	 * Find by ID of reference: bidTenderLocation.id
	 *
	 * @param bidTenderLocationId
	 * @return List<Contract>
	 */
	public List<Contract> findByBidTenderLocationId(Integer bidTenderLocationId);

	/**
	 * Find by reference: bidReference
	 *
	 * @param bidReference
	 * @return List<Contract>
	 */
	public List<Contract> findByBidReference(Contract bidReference);

	/**
	 * Find by ID of reference: bidReference.id
	 *
	 * @param bidReferenceId
	 * @return List<Contract>
	 */
	public List<Contract> findByBidReferenceId(Integer bidReferenceId);

	/**
	 * Find by reference: invoiceTemplate
	 *
	 * @param invoiceTemplate
	 * @return List<Contract>
	 */
	public List<Contract> findByInvoiceTemplate(ExternalReport invoiceTemplate);

	/**
	 * Find by ID of reference: invoiceTemplate.id
	 *
	 * @param invoiceTemplateId
	 * @return List<Contract>
	 */
	public List<Contract> findByInvoiceTemplateId(String invoiceTemplateId);

	/**
	 * Find by reference: contractInvoice
	 *
	 * @param contractInvoice
	 * @return List<Contract>
	 */
	public List<Contract> findByContractInvoice(ContractInvoice contractInvoice);

	/**
	 * Find by ID of reference: contractInvoice.id
	 *
	 * @param contractInvoiceId
	 * @return List<Contract>
	 */
	public List<Contract> findByContractInvoiceId(Integer contractInvoiceId);

	/**
	 * Find by reference: priceCategories
	 *
	 * @param priceCategories
	 * @return List<Contract>
	 */
	public List<Contract> findByPriceCategories(
			ContractPriceCategory priceCategories);

	/**
	 * Find by ID of reference: priceCategories.id
	 *
	 * @param priceCategoriesId
	 * @return List<Contract>
	 */
	public List<Contract> findByPriceCategoriesId(Integer priceCategoriesId);

	/**
	 * Find by reference: pricingBases
	 *
	 * @param pricingBases
	 * @return List<Contract>
	 */
	public List<Contract> findByPricingBases(PricingBase pricingBases);

	/**
	 * Find by ID of reference: pricingBases.id
	 *
	 * @param pricingBasesId
	 * @return List<Contract>
	 */
	public List<Contract> findByPricingBasesId(Integer pricingBasesId);

	/**
	 * Find by reference: shipTo
	 *
	 * @param shipTo
	 * @return List<Contract>
	 */
	public List<Contract> findByShipTo(ShipTo shipTo);

	/**
	 * Find by ID of reference: shipTo.id
	 *
	 * @param shipToId
	 * @return List<Contract>
	 */
	public List<Contract> findByShipToId(Integer shipToId);
}
