package atraxo.cmm.business.api.ext.tender.updater;

import atraxo.cmm.domain.impl.tender.Tender;
import seava.j4e.api.exceptions.ActionNotSupportedException;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author abolindu
 */
public interface ITenderUpdaterService {

	/**
	 * @param receivedTender
	 * @return
	 * @throws BusinessException
	 * @throws ReflectiveOperationException
	 * @throws ActionNotSupportedException
	 */
	public Tender updateTender(Tender receivedTender) throws Exception;
}
