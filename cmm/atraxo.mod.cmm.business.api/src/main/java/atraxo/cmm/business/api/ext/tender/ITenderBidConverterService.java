package atraxo.cmm.business.api.ext.tender;

import seava.j4e.api.exceptions.BusinessException;

/**
 * @author abolindu
 */
public interface ITenderBidConverterService {

	/**
	 * Converts Excel file to xml
	 *
	 * @param resource
	 * @return
	 */
	public byte[] convert(byte[] resource, String fileName) throws BusinessException;

}
