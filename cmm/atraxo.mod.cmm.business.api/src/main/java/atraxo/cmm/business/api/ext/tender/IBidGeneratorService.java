package atraxo.cmm.business.api.ext.tender;

import java.util.Map;

import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public interface IBidGeneratorService {

	/**
	 * @param tenderLocationId
	 * @param bidVersion
	 * @param airlines
	 * @return
	 * @throws BusinessException
	 */
	int generateBidFromLocation(Integer tenderLocationId, Map<String, Object> params, TenderLocationAirlines... airlines) throws BusinessException;
}
