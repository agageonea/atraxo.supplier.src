/**
 *
 */
package atraxo.cmm.business.api.ext.bid;

import java.util.List;

import atraxo.cmm.domain.impl.contracts.Contract;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public interface IBidService {

	void awardTenderBid(List<Contract> bids) throws BusinessException;

	Integer copySupplierBid(Contract sourceBid, Integer tenderLocationId, String copyBidOptionName, String reason) throws BusinessException;

	void declineTenderBid(List<Contract> bids) throws BusinessException;

	Integer copyBid(Contract contract) throws BusinessException;

	void propagateBiddingStatus(Contract processedBid, String action, String reason) throws BusinessException;

}
