/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.priceUpdate;

import atraxo.cmm.domain.impl.priceUpdate.PriceUpdate;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdateCategories;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link PriceUpdateCategories} domain entity.
 */
public interface IPriceUpdateCategoriesService
		extends
			IEntityService<PriceUpdateCategories> {

	/**
	 * Find by unique key
	 *
	 * @return PriceUpdateCategories
	 */
	public PriceUpdateCategories findByBKey(PriceUpdate priceUpdate,
			String contract, Integer contractPriceCatId);

	/**
	 * Find by unique key
	 *
	 * @return PriceUpdateCategories
	 */
	public PriceUpdateCategories findByBKey(Long priceUpdateId,
			String contract, Integer contractPriceCatId);

	/**
	 * Find by unique key
	 *
	 * @return PriceUpdateCategories
	 */
	public PriceUpdateCategories findByBusiness(Integer id);

	/**
	 * Find by reference: priceUpdate
	 *
	 * @param priceUpdate
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByPriceUpdate(PriceUpdate priceUpdate);

	/**
	 * Find by ID of reference: priceUpdate.id
	 *
	 * @param priceUpdateId
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByPriceUpdateId(Integer priceUpdateId);

	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByLocation(Locations location);

	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByLocationId(Integer locationId);

	/**
	 * Find by reference: contractHolder
	 *
	 * @param contractHolder
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByContractHolder(
			Customer contractHolder);

	/**
	 * Find by ID of reference: contractHolder.id
	 *
	 * @param contractHolderId
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByContractHolderId(
			Integer contractHolderId);

	/**
	 * Find by reference: counterparty
	 *
	 * @param counterparty
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByCounterparty(Customer counterparty);

	/**
	 * Find by ID of reference: counterparty.id
	 *
	 * @param counterpartyId
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByCounterpartyId(
			Integer counterpartyId);

	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByCurrency(Currencies currency);

	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByCurrencyId(Integer currencyId);

	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByUnit(Unit unit);

	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<PriceUpdateCategories>
	 */
	public List<PriceUpdateCategories> findByUnitId(Integer unitId);
}
