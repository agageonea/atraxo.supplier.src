package atraxo.cmm.business.api.ext.bid;

import java.io.OutputStream;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderBid;

/**
 * @author abolindu
 */
public interface IIataBidService {

	/**
	 * @param fuelTenderBid
	 * @param out
	 */
	void generateIataXml(FuelTenderBid fuelTenderBid, OutputStream out) throws BusinessException;
}
