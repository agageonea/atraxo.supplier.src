package atraxo.cmm.business.api.ext.datahub;

import java.util.List;

import javax.xml.bind.JAXBException;

import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Transformer used to transform a model domain object in DTO used for sending data through an existing datahub ws
 *
 * @author mbotorogea
 * @author zspeter
 */
public interface IDataHubOutTransformer<E extends AbstractEntity, T> {

	/**
	 * Transforms one model entity of type <code>AbstractEntity</code> to a simple databub object in order to send it via a WS
	 *
	 * @param modelEntity <code>ModelEntity</code> of<code>AbstractEntity</code>
	 * @param paramMap a <code>Map<String, String></code> containg different params
	 * @returns the <code>DatahubObject</code>
	 * @throws BusinessException, JAXBException
	 */
	public T transformModelToDTO(E modelEntity, long msgId) throws BusinessException, JAXBException;

	/**
	 * Transforms one model entity of type <code>AbstractEntity</code> to a simple databub object with reason in order to send it via a WS
	 *
	 * @param modelEntity
	 * @param reason
	 * @return
	 * @throws BusinessException
	 * @throws JAXBException
	 */
	public T transformModelToDTO(E modelEntity, long msgId, String reason) throws BusinessException, JAXBException;

	/**
	 * Transforms one model entity of type <code>AbstractEntity</code> to a simple databub object in order to send it via a WS
	 *
	 * @param modelEntity
	 * @param refMsgId
	 * @param receiverInterface
	 * @return
	 * @throws BusinessException
	 * @throws JAXBException
	 */
	public T transformModelToDTO(E modelEntity, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException;

	/**
	 * Transforms more model entities of type <code>AbstractEntity</code> together with to simple databub objects in order to send it via a WS
	 *
	 * @param modelEntities <code>Collection</code> of<code>AbstractEntity</code>
	 * @param paramMap a <code>Map<String, String></code> containg different params
	 * @returns the <code>WSPayloadDataDTO</code>
	 * @throws BusinessException, JAXBException
	 */
	public T transformModelsToDTO(List<E> modelEntities, long msgId) throws BusinessException, JAXBException;

	/**
	 * Transforms more model entities of type <code>AbstractEntity</code> together with to simple databub objects in order to send it via a WS
	 *
	 * @param modelEntities
	 * @param msgId
	 * @param reason
	 * @return
	 * @throws BusinessException
	 * @throws JAXBException
	 */
	public T transformModelsToDTO(List<E> modelEntities, long msgId, String reason) throws BusinessException, JAXBException;

	/**
	 * Transforms more model entities of type <code>AbstractEntity</code> together with to simple databub objects in order to send it via a WS
	 *
	 * @param modelEntities
	 * @param refMsgId
	 * @param receiverInterface
	 * @return
	 * @throws BusinessException
	 * @throws JAXBException
	 */
	public T transformModelsToDTO(List<E> modelEntities, long msgId, long refMsgId, ExtInterfaceNames receiverInterface)
			throws BusinessException, JAXBException;

}
