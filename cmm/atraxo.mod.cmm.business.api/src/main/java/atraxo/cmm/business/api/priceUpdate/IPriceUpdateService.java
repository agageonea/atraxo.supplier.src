/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.priceUpdate;

import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdate;
import atraxo.cmm.domain.impl.priceUpdate.PriceUpdateCategories;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link PriceUpdate} domain entity.
 */
public interface IPriceUpdateService extends IEntityService<PriceUpdate> {

	/**
	 * Custom service findForPublishing
	 *
	 * @return PriceUpdate
	 */
	public PriceUpdate findForPublishing(boolean onlyApproved)
			throws BusinessException;

	/**
	 * Custom service publish
	 *
	 * @return void
	 */
	public void publish(List<PriceUpdate> list) throws BusinessException;

	/**
	 * Custom service approve
	 *
	 * @return void
	 */
	public void approve(List<PriceUpdate> list, String action, String reason)
			throws BusinessException;

	/**
	 * Custom service reject
	 *
	 * @return void
	 */
	public void reject(List<PriceUpdate> list, String action, String reason)
			throws BusinessException;

	/**
	 * Custom service refresh
	 *
	 * @return void
	 */
	public void refresh(String refId) throws BusinessException;

	/**
	 * Custom service submitForApproval
	 *
	 * @return void
	 */
	public void submitForApproval(PriceUpdate e, BidApprovalStatus status,
			String action, String reason) throws BusinessException;

	/**
	 * Custom service refresh
	 *
	 * @return void
	 */
	public void refresh(PriceUpdate e) throws BusinessException;

	/**
	 * Custom service reset
	 *
	 * @return void
	 */
	public void reset(Integer id) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return PriceUpdate
	 */
	public PriceUpdate findByBusiness(Integer id);

	/**
	 * Find by reference: priceCategory
	 *
	 * @param priceCategory
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByPriceCategory(PriceCategory priceCategory);

	/**
	 * Find by ID of reference: priceCategory.id
	 *
	 * @param priceCategoryId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByPriceCategoryId(Integer priceCategoryId);

	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByCurrency(Currencies currency);

	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByCurrencyId(Integer currencyId);

	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByUnit(Unit unit);

	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByUnitId(Integer unitId);

	/**
	 * Find by reference: publishedBy
	 *
	 * @param publishedBy
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByPublishedBy(UserSupp publishedBy);

	/**
	 * Find by ID of reference: publishedBy.id
	 *
	 * @param publishedById
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByPublishedById(String publishedById);

	/**
	 * Find by reference: submittedBy
	 *
	 * @param submittedBy
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findBySubmittedBy(UserSupp submittedBy);

	/**
	 * Find by ID of reference: submittedBy.id
	 *
	 * @param submittedById
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findBySubmittedById(String submittedById);

	/**
	 * Find by reference: approvedBy
	 *
	 * @param approvedBy
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByApprovedBy(UserSupp approvedBy);

	/**
	 * Find by ID of reference: approvedBy.id
	 *
	 * @param approvedById
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByApprovedById(String approvedById);

	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByLocation(Locations location);

	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByLocationId(Integer locationId);

	/**
	 * Find by reference: contractHolder
	 *
	 * @param contractHolder
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByContractHolder(Customer contractHolder);

	/**
	 * Find by ID of reference: contractHolder.id
	 *
	 * @param contractHolderId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByContractHolderId(Integer contractHolderId);

	/**
	 * Find by reference: counterparty
	 *
	 * @param counterparty
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByCounterparty(Customer counterparty);

	/**
	 * Find by ID of reference: counterparty.id
	 *
	 * @param counterpartyId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByCounterpartyId(Integer counterpartyId);

	/**
	 * Find by reference: billTo
	 *
	 * @param billTo
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByBillTo(Customer billTo);

	/**
	 * Find by ID of reference: billTo.id
	 *
	 * @param billToId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByBillToId(Integer billToId);

	/**
	 * Find by reference: shipTo
	 *
	 * @param shipTo
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByShipTo(Customer shipTo);

	/**
	 * Find by ID of reference: shipTo.id
	 *
	 * @param shipToId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByShipToId(Integer shipToId);

	/**
	 * Find by reference: priceUpdateCategories
	 *
	 * @param priceUpdateCategories
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByPriceUpdateCategories(
			PriceUpdateCategories priceUpdateCategories);

	/**
	 * Find by ID of reference: priceUpdateCategories.id
	 *
	 * @param priceUpdateCategoriesId
	 * @return List<PriceUpdate>
	 */
	public List<PriceUpdate> findByPriceUpdateCategoriesId(
			Integer priceUpdateCategoriesId);
}
