package atraxo.cmm.business.api.ext.datahub;

import java.util.Collection;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderResponse;
import seava.j4e.iata.fuelplus.iata.tender.FuelTenderServiceService;

public interface IDataHubClientRequestService<E extends AbstractEntity, T> {

	/**
	 * Submits the request to DataHub and receives a response
	 *
	 * @param client
	 * @param dto
	 * @return
	 * @throws BusinessException
	 */
	FuelTenderResponse submitRequest(FuelTenderServiceService client, T dto) throws BusinessException;

	/**
	 * Process and finalize the response from DataHub. Make the necessary adjustments to the entity.
	 *
	 * @param result
	 * @param list
	 * @param dtoClass
	 * @param reason
	 * @throws BusinessException
	 */
	void processDataHubBulkWsResult(WSExecutionResult result, Collection<E> list, Class<T> dtoClass, String reason) throws BusinessException;

	/**
	 * Process and finalize the response from DataHub. Make the necessary adjustments to the entity.
	 *
	 * @param result
	 * @param dtoClass
	 * @param reason
	 * @throws BusinessException
	 */
	void processDataHubSingleWsResult(WSExecutionResult result, E entity, Class<T> dtoClass, String reason) throws BusinessException;
}
