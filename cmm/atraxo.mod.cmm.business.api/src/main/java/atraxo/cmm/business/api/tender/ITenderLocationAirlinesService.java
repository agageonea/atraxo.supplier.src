/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.tender;

import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link TenderLocationAirlines} domain entity.
 */
public interface ITenderLocationAirlinesService
		extends
			IEntityService<TenderLocationAirlines> {

	/**
	 * Custom service updateBiddingStatus
	 *
	 * @return void
	 */
	public void updateBiddingStatus(TenderLocation location,
			List<TenderLocationAirlines> airlines, BiddingStatus biddingStatus,
			String action, String reason) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return TenderLocationAirlines
	 */
	public TenderLocationAirlines findByTender_location_airline(
			TenderLocation tenderLocation, Customer airline);

	/**
	 * Find by unique key
	 *
	 * @return TenderLocationAirlines
	 */
	public TenderLocationAirlines findByTender_location_airline(
			Long tenderLocationId, Long airlineId);

	/**
	 * Find by unique key
	 *
	 * @return TenderLocationAirlines
	 */
	public TenderLocationAirlines findByBusiness(Integer id);

	/**
	 * Find by reference: tenderLocation
	 *
	 * @param tenderLocation
	 * @return List<TenderLocationAirlines>
	 */
	public List<TenderLocationAirlines> findByTenderLocation(
			TenderLocation tenderLocation);

	/**
	 * Find by ID of reference: tenderLocation.id
	 *
	 * @param tenderLocationId
	 * @return List<TenderLocationAirlines>
	 */
	public List<TenderLocationAirlines> findByTenderLocationId(
			Integer tenderLocationId);

	/**
	 * Find by reference: airline
	 *
	 * @param airline
	 * @return List<TenderLocationAirlines>
	 */
	public List<TenderLocationAirlines> findByAirline(Customer airline);

	/**
	 * Find by ID of reference: airline.id
	 *
	 * @param airlineId
	 * @return List<TenderLocationAirlines>
	 */
	public List<TenderLocationAirlines> findByAirlineId(Integer airlineId);

	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<TenderLocationAirlines>
	 */
	public List<TenderLocationAirlines> findByUnit(Unit unit);

	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<TenderLocationAirlines>
	 */
	public List<TenderLocationAirlines> findByUnitId(Integer unitId);
}
