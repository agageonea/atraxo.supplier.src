/**
 *
 */
package atraxo.cmm.business.api.ext.datahub;

import java.util.Collection;

import javax.xml.bind.JAXBException;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Transform input objects to entity objects.
 *
 * @author zspeter
 */
public interface IDataHubInTransformer<E extends AbstractEntity, T> {

	/**
	 * Transforms one DTO to a model entity <code>AbstractEntity</code> in order to process the incoming request
	 *
	 * @param dto
	 * @param paramMap
	 * @return
	 * @throws BusinessException
	 * @throws JAXBException
	 */
	public E transformDTOToModel(T dto) throws BusinessException, JAXBException;

	/**
	 * Transforms one DTO to several model entities <code>AbstractEntity</code> in order to process the incoming request
	 *
	 * @param dto
	 * @return
	 * @throws BusinessException
	 * @throws JAXBException
	 */
	public Collection<E> transformDTOToModels(T dto) throws BusinessException, JAXBException;

}
