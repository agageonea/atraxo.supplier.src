/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.tender;

import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.customer.Customer;
import java.math.BigDecimal;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Tender} domain entity.
 */
public interface ITenderService extends IEntityService<Tender> {

	/**
	 * Custom service getTotalVolume
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getTotalVolume(Tender tenderHeader)
			throws BusinessException;

	/**
	 * Custom service publish
	 *
	 * @return void
	 */
	public void publish(Tender tender) throws BusinessException;

	/**
	 * Custom service unpublish
	 *
	 * @return void
	 */
	public void unpublish(Tender tender, String reason)
			throws BusinessException;

	/**
	 * Custom service calculateBiddingStatus
	 *
	 * @return BiddingStatus
	 */
	public BiddingStatus calculateBiddingStatus(Tender tender)
			throws BusinessException;

	/**
	 * Custom service updateBiddingStatus
	 *
	 * @return void
	 */
	public void updateBiddingStatus(Tender tender, BiddingStatus biddingStatus,
			String action) throws BusinessException;

	/**
	 * Custom service markRead
	 *
	 * @return void
	 */
	public void markRead(Tender tender) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Tender
	 */
	public Tender findByCode(String code, Long tenderVersion, Customer holder);

	/**
	 * Find by unique key
	 *
	 * @return Tender
	 */
	public Tender findByCode(String code, Long tenderVersion, Long holderId);

	/**
	 * Find by unique key
	 *
	 * @return Tender
	 */
	public Tender findByBusiness(Integer id);

	/**
	 * Find by reference: holder
	 *
	 * @param holder
	 * @return List<Tender>
	 */
	public List<Tender> findByHolder(Customer holder);

	/**
	 * Find by ID of reference: holder.id
	 *
	 * @param holderId
	 * @return List<Tender>
	 */
	public List<Tender> findByHolderId(Integer holderId);

	/**
	 * Find by reference: contact
	 *
	 * @param contact
	 * @return List<Tender>
	 */
	public List<Tender> findByContact(Contacts contact);

	/**
	 * Find by ID of reference: contact.id
	 *
	 * @param contactId
	 * @return List<Tender>
	 */
	public List<Tender> findByContactId(Integer contactId);

	/**
	 * Find by reference: tenderLocation
	 *
	 * @param tenderLocation
	 * @return List<Tender>
	 */
	public List<Tender> findByTenderLocation(TenderLocation tenderLocation);

	/**
	 * Find by ID of reference: tenderLocation.id
	 *
	 * @param tenderLocationId
	 * @return List<Tender>
	 */
	public List<Tender> findByTenderLocationId(Integer tenderLocationId);
}
