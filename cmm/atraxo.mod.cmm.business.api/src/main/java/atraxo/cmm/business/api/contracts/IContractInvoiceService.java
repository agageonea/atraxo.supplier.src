/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.contracts;

import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.ContractInvoice;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ContractInvoice} domain entity.
 */
public interface IContractInvoiceService
		extends
			IEntityService<ContractInvoice> {

	/**
	 * Custom service insertUpdateLink
	 *
	 * @return void
	 */
	public void insertUpdateLink(Contract contract, Integer invoiceId,
			String invoiceCode, String status, Date validFrom, Date validTo,
			DealType dealType) throws BusinessException;

	/**
	 * Custom service removeLink
	 *
	 * @return void
	 */
	public void removeLink(Contract contract) throws BusinessException;

	/**
	 * Custom service removeLink
	 *
	 * @return void
	 */
	public void removeLink(Integer invoiceId, DealType dealType)
			throws BusinessException;

	/**
	 * Custom service removeOutContract
	 *
	 * @return void
	 */
	public void removeOutContract(Contract contract) throws BusinessException;

	/**
	 * Custom service canModifyStatus
	 *
	 * @return boolean
	 */
	public boolean canModifyStatus(Contract contract) throws BusinessException;

	/**
	 * Custom service canModifyDates
	 *
	 * @return String
	 */
	public String canModifyDates(Contract contract) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return ContractInvoice
	 */
	public ContractInvoice findByInvoiceId(Integer invoiceId, DealType dealType);

	/**
	 * Find by unique key
	 *
	 * @return ContractInvoice
	 */
	public ContractInvoice findByBusiness(Integer id);

	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<ContractInvoice>
	 */
	public List<ContractInvoice> findByContract(Contract contract);

	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<ContractInvoice>
	 */
	public List<ContractInvoice> findByContractId(Integer contractId);
}
