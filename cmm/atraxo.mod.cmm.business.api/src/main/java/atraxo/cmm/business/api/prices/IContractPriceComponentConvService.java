/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.prices;

import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ContractPriceComponentConv} domain entity.
 */
public interface IContractPriceComponentConvService
		extends
			IEntityService<ContractPriceComponentConv> {

	/**
	 * Find by unique key
	 *
	 * @return ContractPriceComponentConv
	 */
	public ContractPriceComponentConv findByBusiness(Integer id);

	/**
	 * Find by reference: contractPriceComponent
	 *
	 * @param contractPriceComponent
	 * @return List<ContractPriceComponentConv>
	 */
	public List<ContractPriceComponentConv> findByContractPriceComponent(
			ContractPriceComponent contractPriceComponent);

	/**
	 * Find by ID of reference: contractPriceComponent.id
	 *
	 * @param contractPriceComponentId
	 * @return List<ContractPriceComponentConv>
	 */
	public List<ContractPriceComponentConv> findByContractPriceComponentId(
			Integer contractPriceComponentId);

	/**
	 * Find by reference: equivalentUnit
	 *
	 * @param equivalentUnit
	 * @return List<ContractPriceComponentConv>
	 */
	public List<ContractPriceComponentConv> findByEquivalentUnit(
			Unit equivalentUnit);

	/**
	 * Find by ID of reference: equivalentUnit.id
	 *
	 * @param equivalentUnitId
	 * @return List<ContractPriceComponentConv>
	 */
	public List<ContractPriceComponentConv> findByEquivalentUnitId(
			Integer equivalentUnitId);

	/**
	 * Find by reference: equivalentCurrency
	 *
	 * @param equivalentCurrency
	 * @return List<ContractPriceComponentConv>
	 */
	public List<ContractPriceComponentConv> findByEquivalentCurrency(
			Currencies equivalentCurrency);

	/**
	 * Find by ID of reference: equivalentCurrency.id
	 *
	 * @param equivalentCurrencyId
	 * @return List<ContractPriceComponentConv>
	 */
	public List<ContractPriceComponentConv> findByEquivalentCurrencyId(
			Integer equivalentCurrencyId);
}
