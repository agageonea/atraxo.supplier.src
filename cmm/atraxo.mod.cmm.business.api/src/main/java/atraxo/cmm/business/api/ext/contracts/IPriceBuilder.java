package atraxo.cmm.business.api.ext.contracts;

import java.util.Date;
import java.util.List;

import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public interface IPriceBuilder {

	/**
	 * @param e
	 * @throws BusinessException
	 */
	void buildPricingBases(PricingBase e) throws BusinessException;

	/**
	 * @param e
	 * @throws BusinessException
	 */
	void buildContractPriceCategory(ContractPriceCategory e) throws BusinessException;

	/**
	 * @param e
	 */
	void extendsNotContinous(PricingBase e);

	/**
	 * @param e
	 * @throws BusinessException
	 */
	void extendsToMargins(PricingBase e) throws BusinessException;

	/**
	 * @param e
	 */
	void updateChilds(ContractPriceCategory e) throws BusinessException;

	/**
	 * @param jsonStr
	 * @param cpc
	 * @throws BusinessException
	 */
	void assignParentPriceCategory(String jsonStr, ContractPriceCategory cpc) throws BusinessException;

	/**
	 * @param cpc
	 * @param indicator
	 * @throws BusinessException
	 */
	void updateParentPriceCategories(ContractPriceCategory cpc, CalculateIndicator indicator) throws BusinessException;

	/**
	 * @param list
	 * @param cotractValidFrom
	 * @param contractValidTo
	 * @return
	 * @throws BusinessException
	 */
	List<PricingBase> updateMargins(List<PricingBase> list, Date contractValidFrom, Date contractValidTo) throws BusinessException;
}
