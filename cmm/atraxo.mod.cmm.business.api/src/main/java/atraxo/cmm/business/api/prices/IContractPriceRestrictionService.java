/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.prices;

import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ContractPriceRestriction} domain entity.
 */
public interface IContractPriceRestrictionService
		extends
			IEntityService<ContractPriceRestriction> {

	/**
	 * Find by unique key
	 *
	 * @return ContractPriceRestriction
	 */
	public ContractPriceRestriction findByBusiness(Integer id);

	/**
	 * Find by reference: contractPriceCategory
	 *
	 * @param contractPriceCategory
	 * @return List<ContractPriceRestriction>
	 */
	public List<ContractPriceRestriction> findByContractPriceCategory(
			ContractPriceCategory contractPriceCategory);

	/**
	 * Find by ID of reference: contractPriceCategory.id
	 *
	 * @param contractPriceCategoryId
	 * @return List<ContractPriceRestriction>
	 */
	public List<ContractPriceRestriction> findByContractPriceCategoryId(
			Integer contractPriceCategoryId);

	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<ContractPriceRestriction>
	 */
	public List<ContractPriceRestriction> findByUnit(Unit unit);

	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<ContractPriceRestriction>
	 */
	public List<ContractPriceRestriction> findByUnitId(Integer unitId);
}
