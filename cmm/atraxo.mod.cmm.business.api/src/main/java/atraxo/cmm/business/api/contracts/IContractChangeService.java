/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.contracts;

import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.ContractChange;
import atraxo.fmbas.domain.impl.fmbas_type.ContractChangeType;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ContractChange} domain entity.
 */
public interface IContractChangeService extends IEntityService<ContractChange> {

	/**
	 * Custom service addNewChange
	 *
	 * @return void
	 */
	public void addNewChange(Contract contract, ContractChangeType type,
			String message) throws BusinessException;

	/**
	 * Custom service getContractChanges
	 *
	 * @return List<ContractChange>
	 */
	public List<ContractChange> getContractChanges(Contract contract)
			throws BusinessException;

	/**
	 * Custom service getWorkflowContractChanges
	 *
	 * @return List<ContractChange>
	 */
	public List<ContractChange> getWorkflowContractChanges(Contract contract)
			throws BusinessException;

	/**
	 * Custom service getContractChanges
	 *
	 * @return List<ContractChange>
	 */
	public List<ContractChange> getContractChanges(Contract contract,
			ContractChangeType type) throws BusinessException;

	/**
	 * Custom service removeContractChanges
	 *
	 * @return void
	 */
	public void removeContractChanges(Contract contract)
			throws BusinessException;

	/**
	 * Custom service removeContractChange
	 *
	 * @return void
	 */
	public void removeContractChange(Contract contract, ContractChangeType type)
			throws BusinessException;

	/**
	 * Custom service attachmentChange
	 *
	 * @return void
	 */
	public void attachmentChange(Attachment attachment)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return ContractChange
	 */
	public ContractChange findByBusiness(Integer id);
}
