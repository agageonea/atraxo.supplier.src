/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.prices;

import atraxo.cmm.domain.ext.ContractPriceComponentCheckResult;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ContractPriceComponent} domain entity.
 */
public interface IContractPriceComponentService
		extends
			IEntityService<ContractPriceComponent> {

	/**
	 * Custom service getPricingComponentsWithoutPricingBases
	 *
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> getPricingComponentsWithoutPricingBases(
			Contract contract) throws BusinessException;

	/**
	 * Custom service updateWithoutPersist
	 *
	 * @return void
	 */
	public void updateWithoutPersist(ContractPriceComponent e)
			throws BusinessException;

	/**
	 * Custom service insertWithoutPersist
	 *
	 * @return void
	 */
	public void insertWithoutPersist(ContractPriceComponent e)
			throws BusinessException;

	/**
	 * Custom service isIndex
	 *
	 * @return Boolean
	 */
	public Boolean isIndex(ContractPriceComponent e) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return ContractPriceComponent
	 */
	public ContractPriceComponent findByBusinessKey(
			ContractPriceCategory contrPriceCtgry, Date validFrom, Date validTo);

	/**
	 * Find by unique key
	 *
	 * @return ContractPriceComponent
	 */
	public ContractPriceComponent findByBusinessKey(Long contrPriceCtgryId,
			Date validFrom, Date validTo);

	/**
	 * Find by unique key
	 *
	 * @return ContractPriceComponent
	 */
	public ContractPriceComponent findByBusiness(Integer id);

	/**
	 * Find by reference: contrPriceCtgry
	 *
	 * @param contrPriceCtgry
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> findByContrPriceCtgry(
			ContractPriceCategory contrPriceCtgry);

	/**
	 * Find by ID of reference: contrPriceCtgry.id
	 *
	 * @param contrPriceCtgryId
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> findByContrPriceCtgryId(
			Integer contrPriceCtgryId);

	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> findByCurrency(Currencies currency);

	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> findByCurrencyId(Integer currencyId);

	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> findByUnit(Unit unit);

	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> findByUnitId(Integer unitId);

	/**
	 * Find by reference: convertedPrices
	 *
	 * @param convertedPrices
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> findByConvertedPrices(
			ContractPriceComponentConv convertedPrices);

	/**
	 * Find by ID of reference: convertedPrices.id
	 *
	 * @param convertedPricesId
	 * @return List<ContractPriceComponent>
	 */
	public List<ContractPriceComponent> findByConvertedPricesId(
			Integer convertedPricesId);
}
