/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.shipTo;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.fmbas.domain.impl.customer.Customer;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ShipTo} domain entity.
 */
public interface IShipToService extends IEntityService<ShipTo> {

	/**
	 * Find by unique key
	 *
	 * @return ShipTo
	 */
	public ShipTo findByContract_customer(Contract contract, Customer customer);

	/**
	 * Find by unique key
	 *
	 * @return ShipTo
	 */
	public ShipTo findByContract_customer(Long contractId, Long customerId);

	/**
	 * Find by unique key
	 *
	 * @return ShipTo
	 */
	public ShipTo findByBusiness(Integer id);

	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<ShipTo>
	 */
	public List<ShipTo> findByContract(Contract contract);

	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<ShipTo>
	 */
	public List<ShipTo> findByContractId(Integer contractId);

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<ShipTo>
	 */
	public List<ShipTo> findByCustomer(Customer customer);

	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<ShipTo>
	 */
	public List<ShipTo> findByCustomerId(Integer customerId);
}
