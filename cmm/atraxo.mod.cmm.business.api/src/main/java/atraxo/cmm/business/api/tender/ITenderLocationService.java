/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.tender;

import atraxo.cmm.domain.impl.cmm_type.BiddingStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.tender.Tender;
import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationAirlines;
import atraxo.cmm.domain.impl.tender.TenderLocationExpectedPrice;
import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.math.BigDecimal;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link TenderLocation} domain entity.
 */
public interface ITenderLocationService extends IEntityService<TenderLocation> {

	/**
	 * Custom service updateVolume
	 *
	 * @return void
	 */
	public void updateVolume(Integer locationId, Boolean resetVolume)
			throws BusinessException;

	/**
	 * Custom service updateBiddingStatus
	 *
	 * @return void
	 */
	public void updateBiddingStatus(TenderLocation location,
			BiddingStatus biddingStatus, String action, String reason)
			throws BusinessException;

	/**
	 * Custom service calculateBiddingStatus
	 *
	 * @return BiddingStatus
	 */
	public BiddingStatus calculateBiddingStatus(TenderLocation location)
			throws BusinessException;

	/**
	 * Custom service calculateVolumePerAgreementPeriod
	 *
	 * @return BigDecimal
	 */
	public BigDecimal calculateVolumePerAgreementPeriod(
			TenderLocation location, TenderLocationAirlines airline)
			throws BusinessException;

	/**
	 * Custom service generateBidForm
	 *
	 * @return byte[]
	 */
	public byte[] generateBidForm(Integer locationId) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return TenderLocation
	 */
	public TenderLocation findByCode(Locations location, Tender tender,
			TaxType taxType, Product fuelProduct,
			FlightServiceType flightServiceType, ContractSubType deliveryPoint);

	/**
	 * Find by unique key
	 *
	 * @return TenderLocation
	 */
	public TenderLocation findByCode(Long locationId, Long tenderId,
			TaxType taxType, Product fuelProduct,
			FlightServiceType flightServiceType, ContractSubType deliveryPoint);

	/**
	 * Find by unique key
	 *
	 * @return TenderLocation
	 */
	public TenderLocation findByBusiness(Integer id);

	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByLocation(Locations location);

	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByLocationId(Integer locationId);

	/**
	 * Find by reference: tender
	 *
	 * @param tender
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByTender(Tender tender);

	/**
	 * Find by ID of reference: tender.id
	 *
	 * @param tenderId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByTenderId(Integer tenderId);

	/**
	 * Find by reference: volumeUnit
	 *
	 * @param volumeUnit
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByVolumeUnit(Unit volumeUnit);

	/**
	 * Find by ID of reference: volumeUnit.id
	 *
	 * @param volumeUnitId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByVolumeUnitId(Integer volumeUnitId);

	/**
	 * Find by reference: densityWeightUnit
	 *
	 * @param densityWeightUnit
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByDensityWeightUnit(Unit densityWeightUnit);

	/**
	 * Find by ID of reference: densityWeightUnit.id
	 *
	 * @param densityWeightUnitId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByDensityWeightUnitId(
			Integer densityWeightUnitId);

	/**
	 * Find by reference: densityVolumeUnit
	 *
	 * @param densityVolumeUnit
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByDensityVolumeUnit(Unit densityVolumeUnit);

	/**
	 * Find by ID of reference: densityVolumeUnit.id
	 *
	 * @param densityVolumeUnitId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByDensityVolumeUnitId(
			Integer densityVolumeUnitId);

	/**
	 * Find by reference: densityConvFromUnit
	 *
	 * @param densityConvFromUnit
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByDensityConvFromUnit(
			Unit densityConvFromUnit);

	/**
	 * Find by ID of reference: densityConvFromUnit.id
	 *
	 * @param densityConvFromUnitId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByDensityConvFromUnitId(
			Integer densityConvFromUnitId);

	/**
	 * Find by reference: densityConvToUnit
	 *
	 * @param densityConvToUnit
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByDensityConvToUnit(Unit densityConvToUnit);

	/**
	 * Find by ID of reference: densityConvToUnit.id
	 *
	 * @param densityConvToUnitId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByDensityConvToUnitId(
			Integer densityConvToUnitId);

	/**
	 * Find by reference: marketCurrency
	 *
	 * @param marketCurrency
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByMarketCurrency(Currencies marketCurrency);

	/**
	 * Find by ID of reference: marketCurrency.id
	 *
	 * @param marketCurrencyId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByMarketCurrencyId(Integer marketCurrencyId);

	/**
	 * Find by reference: marketPricingUnit
	 *
	 * @param marketPricingUnit
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByMarketPricingUnit(Unit marketPricingUnit);

	/**
	 * Find by ID of reference: marketPricingUnit.id
	 *
	 * @param marketPricingUnitId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByMarketPricingUnitId(
			Integer marketPricingUnitId);

	/**
	 * Find by reference: financialSource
	 *
	 * @param financialSource
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByFinancialSource(
			FinancialSources financialSource);

	/**
	 * Find by ID of reference: financialSource.id
	 *
	 * @param financialSourceId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByFinancialSourceId(
			Integer financialSourceId);

	/**
	 * Find by reference: averagingMethod
	 *
	 * @param averagingMethod
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByAveragingMethod(
			AverageMethod averagingMethod);

	/**
	 * Find by ID of reference: averagingMethod.id
	 *
	 * @param averagingMethodId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByAveragingMethodId(
			Integer averagingMethodId);

	/**
	 * Find by reference: indexProvider
	 *
	 * @param indexProvider
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByIndexProvider(TimeSerie indexProvider);

	/**
	 * Find by ID of reference: indexProvider.id
	 *
	 * @param indexProviderId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByIndexProviderId(Integer indexProviderId);

	/**
	 * Find by reference: indexAveragingMethod
	 *
	 * @param indexAveragingMethod
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByIndexAveragingMethod(
			AverageMethod indexAveragingMethod);

	/**
	 * Find by ID of reference: indexAveragingMethod.id
	 *
	 * @param indexAveragingMethodId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByIndexAveragingMethodId(
			Integer indexAveragingMethodId);

	/**
	 * Find by reference: settlementCurrency
	 *
	 * @param settlementCurrency
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findBySettlementCurrency(
			Currencies settlementCurrency);

	/**
	 * Find by ID of reference: settlementCurrency.id
	 *
	 * @param settlementCurrencyId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findBySettlementCurrencyId(
			Integer settlementCurrencyId);

	/**
	 * Find by reference: settlementUnit
	 *
	 * @param settlementUnit
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findBySettlementUnit(Unit settlementUnit);

	/**
	 * Find by ID of reference: settlementUnit.id
	 *
	 * @param settlementUnitId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findBySettlementUnitId(Integer settlementUnitId);

	/**
	 * Find by reference: tenderLocationAirlines
	 *
	 * @param tenderLocationAirlines
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByTenderLocationAirlines(
			TenderLocationAirlines tenderLocationAirlines);

	/**
	 * Find by ID of reference: tenderLocationAirlines.id
	 *
	 * @param tenderLocationAirlinesId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByTenderLocationAirlinesId(
			Integer tenderLocationAirlinesId);

	/**
	 * Find by reference: bids
	 *
	 * @param bids
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByBids(Contract bids);

	/**
	 * Find by ID of reference: bids.id
	 *
	 * @param bidsId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByBidsId(Integer bidsId);

	/**
	 * Find by reference: tenderLocationRounds
	 *
	 * @param tenderLocationRounds
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByTenderLocationRounds(
			TenderLocationRound tenderLocationRounds);

	/**
	 * Find by ID of reference: tenderLocationRounds.id
	 *
	 * @param tenderLocationRoundsId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByTenderLocationRoundsId(
			Integer tenderLocationRoundsId);

	/**
	 * Find by reference: tenderLocationExpectedPrices
	 *
	 * @param tenderLocationExpectedPrices
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByTenderLocationExpectedPrices(
			TenderLocationExpectedPrice tenderLocationExpectedPrices);

	/**
	 * Find by ID of reference: tenderLocationExpectedPrices.id
	 *
	 * @param tenderLocationExpectedPricesId
	 * @return List<TenderLocation>
	 */
	public List<TenderLocation> findByTenderLocationExpectedPricesId(
			Integer tenderLocationExpectedPricesId);
}
