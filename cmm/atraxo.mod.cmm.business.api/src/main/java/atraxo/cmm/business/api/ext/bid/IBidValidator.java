/**
 *
 */
package atraxo.cmm.business.api.ext.bid;

import java.util.List;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.validationMessage.ValidationMessage;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public interface IBidValidator {

	/**
	 * Verifies bid for submission.
	 *
	 * @param bidId
	 * @return
	 * @throws BusinessException
	 */
	List<ValidationMessage> validateBid(int bidId) throws BusinessException;

	/**
	 * Verifies bid for submission.
	 *
	 * @param bid
	 * @return
	 * @throws BusinessException
	 */
	List<ValidationMessage> validateBid(Contract bid) throws BusinessException;

	/**
	 * Delete old validation messages.
	 *
	 * @param bid
	 * @throws BusinessException
	 */
	void clearExistingValidations(Contract bid) throws BusinessException;

	/**
	 * Check bid based on business key in persistence layer.
	 *
	 * @param bid
	 * @throws BusinessException
	 */
	void checkBidExistence(Contract bid) throws BusinessException;
}
