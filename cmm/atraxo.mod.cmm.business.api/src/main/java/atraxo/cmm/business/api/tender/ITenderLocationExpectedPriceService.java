/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.tender;

import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationExpectedPrice;
import atraxo.fmbas.domain.impl.categories.IataPC;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link TenderLocationExpectedPrice} domain entity.
 */
public interface ITenderLocationExpectedPriceService
		extends
			IEntityService<TenderLocationExpectedPrice> {

	/**
	 * Find by unique key
	 *
	 * @return TenderLocationExpectedPrice
	 */
	public TenderLocationExpectedPrice findByTender_location_expected_fees(
			TenderLocation tenderLocation, IataPC iataPriceCategory);

	/**
	 * Find by unique key
	 *
	 * @return TenderLocationExpectedPrice
	 */
	public TenderLocationExpectedPrice findByTender_location_expected_fees(
			Long tenderLocationId, Long iataPriceCategoryId);

	/**
	 * Find by unique key
	 *
	 * @return TenderLocationExpectedPrice
	 */
	public TenderLocationExpectedPrice findByBusiness(Integer id);

	/**
	 * Find by reference: tenderLocation
	 *
	 * @param tenderLocation
	 * @return List<TenderLocationExpectedPrice>
	 */
	public List<TenderLocationExpectedPrice> findByTenderLocation(
			TenderLocation tenderLocation);

	/**
	 * Find by ID of reference: tenderLocation.id
	 *
	 * @param tenderLocationId
	 * @return List<TenderLocationExpectedPrice>
	 */
	public List<TenderLocationExpectedPrice> findByTenderLocationId(
			Integer tenderLocationId);

	/**
	 * Find by reference: iataPriceCategory
	 *
	 * @param iataPriceCategory
	 * @return List<TenderLocationExpectedPrice>
	 */
	public List<TenderLocationExpectedPrice> findByIataPriceCategory(
			IataPC iataPriceCategory);

	/**
	 * Find by ID of reference: iataPriceCategory.id
	 *
	 * @param iataPriceCategoryId
	 * @return List<TenderLocationExpectedPrice>
	 */
	public List<TenderLocationExpectedPrice> findByIataPriceCategoryId(
			Integer iataPriceCategoryId);
}
