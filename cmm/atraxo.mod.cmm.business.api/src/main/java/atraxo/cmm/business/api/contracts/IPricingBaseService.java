/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.contracts;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.math.BigDecimal;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link PricingBase} domain entity.
 */
public interface IPricingBaseService extends IEntityService<PricingBase> {

	/**
	 * Custom service convertUnit
	 *
	 * @return BigDecimal
	 */
	public BigDecimal convertUnit(Integer fromUnitId, Integer toUnitId,
			BigDecimal value, BigDecimal density) throws BusinessException;

	/**
	 * Custom service updatePricingBase
	 *
	 * @return void
	 */
	public void updatePricingBase(PricingBase e) throws BusinessException;

	/**
	 * Custom service updateWithoutPersist
	 *
	 * @return void
	 */
	public void updateWithoutPersist(PricingBase e) throws BusinessException;

	/**
	 * Custom service insertAsIs
	 *
	 * @return void
	 */
	public void insertAsIs(PricingBase e) throws BusinessException;

	/**
	 * Custom service getNeighbourProductPbs
	 *
	 * @return List<PricingBase>
	 */
	public List<PricingBase> getNeighbourProductPbs(PricingBase e)
			throws BusinessException;

	/**
	 * Custom service buildProductPbList
	 *
	 * @return List<PricingBase>
	 */
	public List<PricingBase> buildProductPbList(Contract c)
			throws BusinessException;

	/**
	 * Custom service isProduct
	 *
	 * @return Boolean
	 */
	public Boolean isProduct(PricingBase pricingBase) throws BusinessException;

	/**
	 * Custom service isIndex
	 *
	 * @return Boolean
	 */
	public Boolean isIndex(PricingBase pricingBase) throws BusinessException;

	/**
	 * Custom service getQuotationValues
	 *
	 * @return List<QuotationValue>
	 */
	public List<QuotationValue> getQuotationValues(PricingBase pricingBase)
			throws BusinessException;

	/**
	 * Custom service isFeeOrTax
	 *
	 * @return Boolean
	 */
	public Boolean isFeeOrTax(PricingBase pricingBase) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return PricingBase
	 */
	public PricingBase findByBusiness(Integer id);

	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByContract(Contract contract);

	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByContractId(Integer contractId);

	/**
	 * Find by reference: quotation
	 *
	 * @param quotation
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByQuotation(Quotation quotation);

	/**
	 * Find by ID of reference: quotation.id
	 *
	 * @param quotationId
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByQuotationId(Integer quotationId);

	/**
	 * Find by reference: convUnit
	 *
	 * @param convUnit
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByConvUnit(Unit convUnit);

	/**
	 * Find by ID of reference: convUnit.id
	 *
	 * @param convUnitId
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByConvUnitId(Integer convUnitId);

	/**
	 * Find by reference: avgMethod
	 *
	 * @param avgMethod
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByAvgMethod(AverageMethod avgMethod);

	/**
	 * Find by ID of reference: avgMethod.id
	 *
	 * @param avgMethodId
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByAvgMethodId(Integer avgMethodId);

	/**
	 * Find by reference: priceCat
	 *
	 * @param priceCat
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByPriceCat(PriceCategory priceCat);

	/**
	 * Find by ID of reference: priceCat.id
	 *
	 * @param priceCatId
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByPriceCatId(Integer priceCatId);

	/**
	 * Find by reference: contractPriceCategories
	 *
	 * @param contractPriceCategories
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByContractPriceCategories(
			ContractPriceCategory contractPriceCategories);

	/**
	 * Find by ID of reference: contractPriceCategories.id
	 *
	 * @param contractPriceCategoriesId
	 * @return List<PricingBase>
	 */
	public List<PricingBase> findByContractPriceCategoriesId(
			Integer contractPriceCategoriesId);
}
