package atraxo.cmm.business.api.ext.prices.builder;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public interface IComponentPriceBuilder {

	/**
	 * @param e
	 */
	void extendsToMargins(ContractPriceComponent e);

	/**
	 * @param e
	 * @throws BusinessException
	 */
	void createConvertedPrices(ContractPriceComponent e) throws BusinessException;

	/**
	 * @param e
	 * @param insert
	 * @throws BusinessException
	 */
	void updateNeibours(ContractPriceComponent e, boolean insert) throws BusinessException;

	/**
	 * @param category
	 * @param price
	 * @param isProvisioned
	 * @param period
	 * @return
	 * @throws BusinessException
	 */
	ContractPriceComponent buildContractPriceComponent(ContractPriceCategory category, BigDecimal price, boolean isProvisioned, Date... period)
			throws BusinessException;

	/**
	 * @param list
	 * @param pbValidFrom
	 * @param pbValidTo
	 */
	void updateMargins(List<ContractPriceComponent> list, Date pbValidFrom, Date pbValidTo);
}
