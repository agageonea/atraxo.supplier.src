/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.tender;

import atraxo.cmm.domain.impl.tender.TenderLocation;
import atraxo.cmm.domain.impl.tender.TenderLocationRound;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link TenderLocationRound} domain entity.
 */
public interface ITenderLocationRoundService
		extends
			IEntityService<TenderLocationRound> {

	/**
	 * Find by unique key
	 *
	 * @return TenderLocationRound
	 */
	public TenderLocationRound findByTender_location_round(
			TenderLocation tenderLocation, Integer roundNo);

	/**
	 * Find by unique key
	 *
	 * @return TenderLocationRound
	 */
	public TenderLocationRound findByTender_location_round(
			Long tenderLocationId, Integer roundNo);

	/**
	 * Find by unique key
	 *
	 * @return TenderLocationRound
	 */
	public TenderLocationRound findByBusiness(Integer id);

	/**
	 * Find by reference: tenderLocation
	 *
	 * @param tenderLocation
	 * @return List<TenderLocationRound>
	 */
	public List<TenderLocationRound> findByTenderLocation(
			TenderLocation tenderLocation);

	/**
	 * Find by ID of reference: tenderLocation.id
	 *
	 * @param tenderLocationId
	 * @return List<TenderLocationRound>
	 */
	public List<TenderLocationRound> findByTenderLocationId(
			Integer tenderLocationId);
}
