/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.contracts;

import atraxo.cmm.domain.impl.contracts.MarkedContract;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link MarkedContract} domain entity.
 */
public interface IMarkedContractService extends IEntityService<MarkedContract> {

	/**
	 * Custom service findAnEntity
	 *
	 * @return MarkedContract
	 */
	public MarkedContract findAnEntity() throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return MarkedContract
	 */
	public MarkedContract findByContractId(Integer contractId);

	/**
	 * Find by unique key
	 *
	 * @return MarkedContract
	 */
	public MarkedContract findByBusiness(Integer id);
}
