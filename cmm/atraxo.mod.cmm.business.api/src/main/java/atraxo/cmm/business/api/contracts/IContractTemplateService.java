/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.contracts;

import atraxo.cmm.domain.impl.contracts.ContractTemplate;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ContractTemplate} domain entity.
 */
public interface IContractTemplateService
		extends
			IEntityService<ContractTemplate> {

	/**
	 * Find by unique key
	 *
	 * @return ContractTemplate
	 */
	public ContractTemplate findByTemplates(String contractType,
			String deliverySubtype);

	/**
	 * Find by unique key
	 *
	 * @return ContractTemplate
	 */
	public ContractTemplate findByBusiness(Integer id);
}
