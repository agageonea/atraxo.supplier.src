/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.cmm.business.api.prices;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.PricingBase;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ContractPriceCategory} domain entity.
 */
public interface IContractPriceCategoryService
		extends
			IEntityService<ContractPriceCategory> {

	/**
	 * Custom service getPriceComponent
	 *
	 * @return ContractPriceComponent
	 */
	public ContractPriceComponent getPriceComponent(
			ContractPriceCategory priceCategory, Date date)
			throws BusinessException;

	/**
	 * Custom service convertPrice
	 *
	 * @return BigDecimal
	 */
	public BigDecimal convertPrice(String fromUnitCode, String toUnitCode,
			String fromCurrencyCode, String toCurrencyCode, BigDecimal price,
			Integer finSrcId, Integer avgMthdId, MasterAgreementsPeriod offset,
			Date date) throws BusinessException;

	/**
	 * Custom service getPriceInCurrencyUnit
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getPriceInCurrencyUnit(
			ContractPriceCategory priceCategory, String unitCode,
			String currencyCode, Date date) throws BusinessException;

	/**
	 * Custom service getDefaultContractPriceCategories
	 *
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> getDefaultContractPriceCategories(
			Integer priceCategoryId, Integer contractId)
			throws BusinessException;

	/**
	 * Custom service updateWithoutPersist
	 *
	 * @return void
	 */
	public void updateWithoutPersist(ContractPriceCategory e)
			throws BusinessException;

	/**
	 * Custom service insertWithoutPersist
	 *
	 * @return void
	 */
	public void insertWithoutPersist(ContractPriceCategory e)
			throws BusinessException;

	/**
	 * Custom service updateAsIs
	 *
	 * @return void
	 */
	public void updateAsIs(ContractPriceCategory e) throws BusinessException;

	/**
	 * Custom service findByResaleRef
	 *
	 * @return ContractPriceCategory
	 */
	public ContractPriceCategory findByResaleRef(ContractPriceCategory e)
			throws BusinessException;

	/**
	 * Custom service findBluePrintByResaleRef
	 *
	 * @return ContractPriceCategory
	 */
	public ContractPriceCategory findBluePrintByResaleRef(
			ContractPriceCategory e) throws BusinessException;

	/**
	 * Custom service getCompositeChildsCategory
	 *
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> getCompositeChildsCategory(
			ContractPriceCategory temp) throws BusinessException;

	/**
	 * Custom service hasValidInterval
	 *
	 * @return Boolean
	 */
	public Boolean hasValidInterval(Date fuelingDate,
			ContractPriceCategory category) throws BusinessException;

	/**
	 * Custom service findByExchangeRate
	 *
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByExchangeRate(ExchangeRate e)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return ContractPriceCategory
	 */
	public ContractPriceCategory findByBusiness(Integer id);

	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByContract(Contract contract);

	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByContractId(Integer contractId);

	/**
	 * Find by reference: priceCategory
	 *
	 * @param priceCategory
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByPriceCategory(
			PriceCategory priceCategory);

	/**
	 * Find by ID of reference: priceCategory.id
	 *
	 * @param priceCategoryId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByPriceCategoryId(
			Integer priceCategoryId);

	/**
	 * Find by reference: pricingBases
	 *
	 * @param pricingBases
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByPricingBases(
			PricingBase pricingBases);

	/**
	 * Find by ID of reference: pricingBases.id
	 *
	 * @param pricingBasesId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByPricingBasesId(
			Integer pricingBasesId);

	/**
	 * Find by reference: financialSource
	 *
	 * @param financialSource
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByFinancialSource(
			FinancialSources financialSource);

	/**
	 * Find by ID of reference: financialSource.id
	 *
	 * @param financialSourceId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByFinancialSourceId(
			Integer financialSourceId);

	/**
	 * Find by reference: averageMethod
	 *
	 * @param averageMethod
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByAverageMethod(
			AverageMethod averageMethod);

	/**
	 * Find by ID of reference: averageMethod.id
	 *
	 * @param averageMethodId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByAverageMethodId(
			Integer averageMethodId);

	/**
	 * Find by reference: priceComponents
	 *
	 * @param priceComponents
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByPriceComponents(
			ContractPriceComponent priceComponents);

	/**
	 * Find by ID of reference: priceComponents.id
	 *
	 * @param priceComponentsId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByPriceComponentsId(
			Integer priceComponentsId);

	/**
	 * Find by reference: priceRestrictions
	 *
	 * @param priceRestrictions
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByPriceRestrictions(
			ContractPriceRestriction priceRestrictions);

	/**
	 * Find by ID of reference: priceRestrictions.id
	 *
	 * @param priceRestrictionsId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByPriceRestrictionsId(
			Integer priceRestrictionsId);

	/**
	 * Find by reference: childPriceCategory
	 *
	 * @param childPriceCategory
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByChildPriceCategory(
			ContractPriceCategory childPriceCategory);

	/**
	 * Find by ID of reference: childPriceCategory.id
	 *
	 * @param childPriceCategoryId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByChildPriceCategoryId(
			Integer childPriceCategoryId);

	/**
	 * Find by reference: parentPriceCategory
	 *
	 * @param parentPriceCategory
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByParentPriceCategory(
			ContractPriceCategory parentPriceCategory);

	/**
	 * Find by ID of reference: parentPriceCategory.id
	 *
	 * @param parentPriceCategoryId
	 * @return List<ContractPriceCategory>
	 */
	public List<ContractPriceCategory> findByParentPriceCategoryId(
			Integer parentPriceCategoryId);
}
