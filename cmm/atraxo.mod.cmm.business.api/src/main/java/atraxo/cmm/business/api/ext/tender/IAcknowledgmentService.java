package atraxo.cmm.business.api.ext.tender;

import atraxo.cmm.domain.ext.tender.MessageStatus;
import atraxo.cmm.domain.ext.tender.MessageType;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Service to handle IATA Tender acknowledge messages.
 *
 * @author zspeter
 */
public interface IAcknowledgmentService {

	/**
	 * Handle fuel tender acknolegment message
	 *
	 * @param refMsgId
	 * @param messageType
	 * @param messageStatus
	 * @throws BusinessException
	 */
	void fuelTenderAcknowledgement(Long refMsgId, MessageType messageType, MessageStatus messageStatus) throws BusinessException;
}
