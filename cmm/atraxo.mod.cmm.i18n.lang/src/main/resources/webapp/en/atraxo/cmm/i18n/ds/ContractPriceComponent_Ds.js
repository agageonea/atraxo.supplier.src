Ext.override(atraxo.cmm.i18n.ds.ContractPriceComponent_Ds, {
	contractPriceCatName__lbl: "Contract price category",
	contractPriceCatName__tlp: "Name",
	contractPriceCat__lbl: "Contr price ctgry (ID)",
	currencyCode__lbl: "Currency",
	currencyCode__tlp: "Currency code",
	currency__lbl: "Currency (ID)",
	priceCategoryId__lbl: "Price category (ID)",
	priceCtgryName__lbl: "Price category",
	priceCtgryName__tlp: "Price category",
	priceCtgryPricePer__lbl: "Price per",
	priceCtgryPricePer__tlp: "Price per",
	price__lbl: "Price",
	price__tlp: "Price",
	pricingBasesId__lbl: "Pricing bases (ID)",
	provisional__lbl: "Provisional",
	provisional__tlp: "Provisional",
	toleranceMessage__lbl: "Tolerance message",
	unitCode__lbl: "Unit",
	unitCode__tlp: "Code",
	unit__lbl: "Unit (ID)",
	validFrom__lbl: "Valid from",
	validFrom__tlp: "Valid from",
	validTo__lbl: "Valid to",
	validTo__tlp: "Valid to",
	withHold__lbl: "Withhold",
	withHold__tlp: "Withhold"
});
