
Ext.override(atraxo.cmm.i18n.dc.ContractCustomer_Dc$Filter, {
	avgMethodName__lbl: "Averaging method",
	customerName__lbl: "Customer name",
	customer__lbl: "Customer",
	financialSourceCode__lbl: "Financial source",
	periodApprovalStatus__lbl: "Period approval status",
	priceApprovalStatus__lbl: "Price approval status",
	shipToApprovalStatus__lbl: "Ship-to approval status"
});

Ext.override(atraxo.cmm.i18n.dc.ContractCustomer_Dc$Header, {
	btnToggleContractPeriod__lbl: "...",
	contactsLabel__lbl: "Contact person",
	contractHolderLabel__lbl: "Contract holder",
	contractPeriodLabel__lbl: "Contract period",
	customer__tlp: "Select customer",
	flightTypeLabel__lbl: "Event type",
	hardCopyLabel__lbl: "Hard copy received?",
	iataServiceLevelLabel__lbl: "IATA service level",
	locationLabel__lbl: "Location",
	productLabel__lbl: "Product",
	respBuyerLabel__lbl: "Account manager",
	taxLabel__lbl: "Tax type",
	termSpotLabel__lbl: "Term/Spot"
});

Ext.override(atraxo.cmm.i18n.dc.ContractCustomer_Dc$PaymentTerms, {
	avgMethodName__lbl: "Averaging method",
	btnTogglePaymentTerms__lbl: "...",
	btnToggle__lbl: "...",
	financialSourceAvgMethodPeriod__lbl: "Using ForEx of",
	financialSourceCode__lbl: "Financial source",
	paymentTermRefDay__lbl: "Payment terms"
});

Ext.override(atraxo.cmm.i18n.dc.ContractCustomer_Dc$Remark, {
	remarks__lbl: "Remark"
});

Ext.override(atraxo.cmm.i18n.dc.ContractCustomer_Dc$TabList, {
	avgMthdName__lbl: "Averaging method",
	bidRefereceCode__lbl: "Bid code",
	finSCode__lbl: "Financial source",
	invoiceTemplateName__lbl: "Name of invoice template",
	periodApprovalStatus__lbl: "Period approval status",
	priceApprovalStatus__lbl: "Price approval status",
	shipToApprovalStatus__lbl: "Ship-to approval status"
});

Ext.override(atraxo.cmm.i18n.dc.ContractCustomer_Dc$Volumes, {
	awardedVolumeLabel__lbl: "Awarded volume",
	periodLabel__lbl: "Period",
	quantityTypeLabel__lbl: "Quantity type",
	shareLabel__lbl: "Share percentage",
	toleranceLabel__lbl: "Tolerance"
});

Ext.override(atraxo.cmm.i18n.dc.ContractCustomer_Dc$copyCustomer, {
	customerField__lbl: "Select customer",
	customerField__tlp: "Select customer"
});

Ext.override(atraxo.cmm.i18n.dc.ContractCustomer_Dc$financialSourceAvgMethodPeriod, {
	avgMethodName__lbl: "Averaging method",
	financialSourceCode__lbl: "Financial source"
});

Ext.override(atraxo.cmm.i18n.dc.ContractCustomer_Dc$paymentTermsRefDay, {
	daysLabel__lbl: "days",
	paymentTermsLabel__lbl: "Payment terms"
});

Ext.override(atraxo.cmm.i18n.dc.ContractCustomer_Dc$selectCustomer, {
	customerName__lbl: "Select customer",
	customerName__tlp: "Select customer"
});
