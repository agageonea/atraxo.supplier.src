Ext.override(atraxo.cmm.i18n.ds.ValidationMessageBid_Ds, {
	bidRevision__lbl: "",
	bidVersion__lbl: "",
	concurencyCheckFailedBidsLabel__lbl: "",
	location__lbl: "",
	message__lbl: "Validation Message",
	message__tlp: "Validation Message",
	objectId__lbl: "Object Reference ID",
	objectType__lbl: "Object type",
	severity__lbl: "Severity",
	severity__tlp: "Severity",
	succesfullySubmitedBidsLabel__lbl: "",
	tenderName__lbl: "",
	validationErrorsBidsLabel__lbl: ""
});
