
Ext.override(atraxo.cmm.i18n.dc.BidValidations_Dc$BidValidations, {
	bidLocation__lbl: "Location",
	bidRevision__lbl: "Bid revision",
	bidVersion__lbl: "Bid version",
	message__lbl: "Failure Reason",
	objectId__lbl: "Object ID",
	tenderName__lbl: "Tender name"
});
