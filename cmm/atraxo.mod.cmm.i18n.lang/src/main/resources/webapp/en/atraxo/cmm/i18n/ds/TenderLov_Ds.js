Ext.override(atraxo.cmm.i18n.ds.TenderLov_Ds, {
	code__lbl: "Code",
	code__tlp: "Identifying code of tender",
	holderCode__lbl: "Holder",
	holderCode__tlp: "Account code",
	holderId__lbl: "Holder (ID)",
	holderId__tlp: "Identification of the tender holder",
	name__lbl: "Name",
	name__tlp: "Name of tender",
	version__lbl: "Version"
});
