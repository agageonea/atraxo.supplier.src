Ext.override(atraxo.cmm.i18n.ds.TenderLocationExpectedPrices_Ds, {
	iataPriceCategoryCode__lbl: "IATA Code",
	iataPriceCategoryCode__tlp: "IATA price category code",
	iataPriceCategoryId__lbl: "IATA Price Category (ID)",
	iataPriceCategoryName__lbl: "IATA Name",
	iataPriceCategoryName__tlp: "Name of IATA price category",
	iataPriceCategoryType__lbl: "Type",
	iataPriceCategoryType__tlp: "IATA price category type",
	tenderLocationId__lbl: "Tender location (ID)"
});
