Ext.override(atraxo.cmm.i18n.ds.ContractTemplate_Ds, {
	contractType__lbl: "Contract type",
	contractType__tlp: "Contract type",
	deliverySubtype__lbl: "Delivery point",
	deliverySubtype__tlp: "Fuel delivery point",
	dialog__lbl: "Dialog",
	dialog__tlp: "Dialog",
	scope__lbl: "Contract scope",
	scope__tlp: "The contract business scope: into plane or into storage"
});
