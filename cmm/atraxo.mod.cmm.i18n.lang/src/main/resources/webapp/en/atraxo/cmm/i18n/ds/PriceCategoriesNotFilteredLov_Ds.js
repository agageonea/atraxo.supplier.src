Ext.override(atraxo.cmm.i18n.ds.PriceCategoriesNotFilteredLov_Ds, {
	active__lbl: "Active?",
	active__tlp: "Active?",
	name__lbl: "Price category",
	name__tlp: "Price category",
	pricePer__lbl: "Price per",
	pricePer__tlp: "Price per",
	type__lbl: "Contract type",
	type__tlp: "Contract type"
});
