Ext.override(atraxo.cmm.i18n.ds.ContractChange_Ds, {
	contractId__lbl: "Contract ID",
	createdAt__lbl: "Created at",
	message__lbl: "Change(s)",
	type__lbl: "Contract type"
});
