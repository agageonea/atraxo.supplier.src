Ext.override(atraxo.cmm.i18n.ds.LocationsLov_Ds, {
	areaID__lbl: "Area ID",
	areaName__lbl: "Area name",
	areaName__tlp: "Name",
	code__lbl: "Code",
	code__tlp: "Code",
	name__lbl: "Location name",
	name__tlp: "Location name",
	type__lbl: "Type",
	type__tlp: "Type"
});
