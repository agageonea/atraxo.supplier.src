
Ext.override(atraxo.cmm.i18n.dc.PricingBase_Dc$EditFormulaPrice, {
	benchmarkLabel__lbl: "Benchmark",
	benchmarkPeriodLabel__lbl: " period",
	btnToggleBenchmark__lbl: "...",
	btnToggleConversionFactor__lbl: "...",
	btnTogglePeriod__lbl: "...",
	commaLabel__lbl: ", ",
	conversionFactorLabel__lbl: "Conversion factor",
	conversionFromLabel__lbl: "Convert from",
	conversionToLabel__lbl: "to",
	decimalPrecisionLabel__lbl: "decimals precision",
	description__lbl: "Price name",
	financialSourceCode__lbl: "Financial source",
	multiplyLabel__lbl: "by multiplying quotation with",
	periodLabel__lbl: "Period of validity",
	quotAvgMethodName__lbl: "Averaging method",
	secondCommaLabel__lbl: ", ",
	usingLabel__lbl: "using"
});

Ext.override(atraxo.cmm.i18n.dc.PricingBase_Dc$Filter, {
	avgMthdName__lbl: "Averaging method"
});

Ext.override(atraxo.cmm.i18n.dc.PricingBase_Dc$NewCompositePrice, {
	continousLabel__lbl: "Continuous?",
	defaultLabel__lbl: "Include in total price estimate?",
	nameLabel__lbl: "Name",
	newRestrictionLabel__lbl: "Apply restrictions",
	priceCatNameLabel__lbl: "Price category",
	priceComponentsLabel__lbl: "Price components",
	priceLabel__lbl: "Price value",
	provisionalLabel__lbl: "Provisional value",
	showAssignment__lbl: "...",
	validFromLabel__lbl: "Valid from",
	validToLabel__lbl: "to",
	vatLabel__lbl: "VAT applicability",
	vat__lbl: "VAT",
	withHoldLabel__lbl: "Withhold from payment"
});

Ext.override(atraxo.cmm.i18n.dc.PricingBase_Dc$NewFormulaPrice, {
	benchmarkLabel__lbl: "Benchmark",
	benchmarkPeriodLabel__lbl: " period",
	btnToggleBenchmark__lbl: "...",
	btnToggleConversionFactor__lbl: "...",
	btnTogglePeriod__lbl: "...",
	commaLabel__lbl: ", ",
	conversionFactorLabel__lbl: "Conversion factor",
	conversionFromLabel__lbl: "Convert from",
	conversionToLabel__lbl: "to",
	decimalPrecisionLabel__lbl: "decimals precision",
	description__lbl: "Price name",
	financialSourceCode__lbl: "Financial source",
	multiplyLabel__lbl: "by multiplying quotation with",
	periodLabel__lbl: "Period of validity",
	quotAvgMethodName__lbl: "Averaging method",
	secondCommaLabel__lbl: ", ",
	usingLabel__lbl: "using"
});

Ext.override(atraxo.cmm.i18n.dc.PricingBase_Dc$NewPCBuyContract, {
	avgMethodIndicatorName__lbl: "Averaging method",
	btnToggleForEx__lbl: "...",
	btnTogglePeriod__lbl: "...",
	comments__lbl: "Comments",
	continous__lbl: "Continuous?",
	default__lbl: "Include in total price?",
	exchangeRateOffset__lbl: "Period",
	financialSourceAvgMethodPeriod__lbl: "Using ForEx of",
	financialSourceCode__lbl: "Financial source",
	name__lbl: "Name",
	periodFromTo__lbl: "Period of validity",
	priceCatName__lbl: "Price category",
	price__lbl: "Price value",
	quantityType__lbl: "Quantity type",
	validTo__lbl: "to",
	vat__lbl: "VAT"
});

Ext.override(atraxo.cmm.i18n.dc.PricingBase_Dc$NewPCSellContract, {
	avgMethodIndicatorName__lbl: "Averaging method",
	btnToggleForEx__lbl: "...",
	btnTogglePeriod__lbl: "...",
	comments__lbl: "Comments",
	continous__lbl: "Continuous?",
	default__lbl: "Include in total price?",
	exchangeRateOffset__lbl: "Period",
	financialSourceAvgMethodPeriod__lbl: "Using ForEx of",
	financialSourceCode__lbl: "Financial source",
	name__lbl: "Name",
	periodFromTo__lbl: "Period of validity",
	priceCatName__lbl: "Price category",
	price__lbl: "Price value",
	quantityType__lbl: "Quantity type",
	validTo__lbl: "to",
	vat__lbl: "VAT"
});

Ext.override(atraxo.cmm.i18n.dc.PricingBase_Dc$benchmark, {
	averagingMethodLabel__lbl: "Averaging method",
	benchmarProviderkLabel__lbl: "Benchmark provider",
	benchmarkLabel__lbl: "Benchmark",
	exchangeRateOffsetLabel__lbl: "Pricing period",
	priceValueTypeLabel__lbl: "Price value type",
	quotAvgMethodName__lbl: "Averaging method",
	quotFinancialSourceCode__lbl: "Financial source",
	warning__lbl: "There is no benchmark in the system to meet the above condition."
});

Ext.override(atraxo.cmm.i18n.dc.PricingBase_Dc$conversionFactor, {
	benchmarkLabel__lbl: "From benchmark unit",
	conversionFactorLabel__lbl: "Conversion factor",
	conversionOperatorLabel__lbl: "Conversion operator",
	conversionPreciosionLabel__lbl: "Conversion precision",
	decimalsLabel__lbl: "decimals"
});

Ext.override(atraxo.cmm.i18n.dc.PricingBase_Dc$financialSourceAvgMethodPeriod, {
	avgMethodIndicatorName__lbl: "Averaging method",
	exchangeRateOffset__lbl: "Period",
	financialSourceCode__lbl: "Financial source"
});

Ext.override(atraxo.cmm.i18n.dc.PricingBase_Dc$financialSourceAvgMethodPeriodPurchase, {
	avgMethodIndicatorName__lbl: "Averaging method",
	exchangeRateOffset__lbl: "Period",
	financialSourceCode__lbl: "Financial source"
});

Ext.override(atraxo.cmm.i18n.dc.PricingBase_Dc$periodFromTo, {
	validTo__lbl: "to"
});

Ext.override(atraxo.cmm.i18n.dc.PricingBase_Dc$periodFromToPurchase, {
	validTo__lbl: "to"
});
