Ext.override(atraxo.cmm.i18n.ds.TenderLocationRound_Ds, {
	bidOpening__lbl: "Bid opening",
	bidRoundActive__lbl: "Is bid active",
	biddingFrom__lbl: "Bidding period from",
	biddingTo__lbl: "Bidding period to",
	roundNo__lbl: "Round no",
	tenderLocationId__lbl: "Tender location (ID)"
});
