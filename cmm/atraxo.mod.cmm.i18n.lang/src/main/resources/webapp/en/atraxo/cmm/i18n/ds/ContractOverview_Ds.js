Ext.override(atraxo.cmm.i18n.ds.ContractOverview_Ds, {
	code__lbl: "Contract",
	code__tlp: "Contract",
	contactId__lbl: "Contact (ID)",
	contactName__lbl: "Supplier contact",
	contactName__tlp: "Supplier contact",
	counterPartyCode__lbl: "Supplier",
	counterPartyCode__tlp: "Supplier",
	counterPartyId__lbl: "Supplier (ID)",
	creditTerms__lbl: "Credit terms",
	creditTerms__tlp: "Credit terms",
	dealType__lbl: "Deal type",
	dealType__tlp: "Deal type",
	iplCode__lbl: "IPL Agent",
	iplCode__tlp: "IPL agent",
	iplId__lbl: "Into plane agent (ID)",
	limitedTo__lbl: "Flight type",
	limitedTo__tlp: "Flight type",
	locCode__lbl: "Location",
	locCode__tlp: "Location",
	locId__lbl: "Location (ID)",
	p_currency__lbl: "Currency selected",
	p_unit__lbl: "Unit selected",
	paymentTerms__lbl: "Payment terms",
	paymentTerms__tlp: "Payment terms",
	price__lbl: "Price",
	price__tlp: "Price",
	product__lbl: "Product",
	product__tlp: "Product",
	resBuyerId__lbl: "Responsible buyer (ID)",
	resBuyerName__lbl: "Responsible buyer",
	resBuyerName__tlp: "Responsible buyer",
	scope__lbl: "Contract scope",
	scope__tlp: "The contract business scope: into plane or into storage",
	settCurrCode__lbl: "Currency",
	settCurrCode__tlp: "Currency",
	settCurrId__lbl: "Settlement currency (ID)",
	settUnitCode__lbl: "Unit",
	settUnitCode__tlp: "Unit",
	settUnitId__lbl: "Settlement unit (ID)",
	status__lbl: "Contract status",
	status__tlp: "Contract status",
	subType__lbl: "Delivery point",
	subType__tlp: "Delivery point of fuel",
	tax__lbl: "Tax type",
	tax__tlp: "Tax type",
	type__lbl: "Type",
	type__tlp: "Type",
	validFrom__lbl: "Valid from",
	validFrom__tlp: "Valid from",
	validTo__lbl: "Valid to",
	validTo__tlp: "Valid to"
});
