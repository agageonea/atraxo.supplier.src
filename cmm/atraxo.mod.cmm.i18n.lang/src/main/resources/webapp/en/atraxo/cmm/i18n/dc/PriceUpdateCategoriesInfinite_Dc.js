
Ext.override(atraxo.cmm.i18n.dc.PriceUpdateCategoriesInfinite_Dc$GridLabel, {
	priceCategoryLabel__lbl: "Select contracts where price has to be updated. On selected contracts [price_category] must have the same currency."
});

Ext.override(atraxo.cmm.i18n.dc.PriceUpdateCategoriesInfinite_Dc$NewList, {
	contract__lbl: "Contract",
	counterparty__lbl: "Counterparty",
	currency__lbl: "Currency",
	holder__lbl: "Holder",
	location__lbl: "Location",
	priceCategory__lbl: "Name",
	price__lbl: "Current price",
	unit__lbl: "Unit"
});
