Ext.override(atraxo.cmm.i18n.ds.ContractPriceRestriction_Ds, {
	contractPriceCtgry__lbl: "Contract price category (ID)",
	isEnabled__lbl: "Is enabled?",
	operator__lbl: "Operator",
	operator__tlp: "Operator",
	restrictionType__lbl: "Restriction type",
	restrictionType__tlp: "Restriction type",
	unitCode__lbl: "Unit",
	unitCode__tlp: "Code",
	unit__lbl: "Unit (ID)",
	value__lbl: "Value",
	value__tlp: "Value"
});
