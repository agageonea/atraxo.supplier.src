Ext.override(atraxo.cmm.i18n.ds.PriceUpdateCategoriesList_Ds, {
	contractHolderCode__lbl: "Contract holder",
	contractHolderCode__tlp: "Account code",
	contractHolderId__lbl: "Contract holder (ID)",
	contractPriceCatId__lbl: "Contract price category ID",
	contractPriceCatId__tlp: "Contract price category ID",
	contract__lbl: "Contract",
	contract__tlp: "The reference contract code",
	counterpartyCode__lbl: "Counterparty",
	counterpartyCode__tlp: "Account code",
	counterpartyId__lbl: "Counterparty (ID)",
	currencyCode__lbl: "Currency",
	currencyCode__tlp: "Currency code",
	currencyId__lbl: "Currency (ID)",
	dealType__lbl: "Contract type",
	dealType__tlp: "Indicates the contract types for the selected contracts",
	labelField__lbl: "",
	locationCode__lbl: "Location",
	locationCode__tlp: "Code",
	locationId__lbl: "Location (ID)",
	parentPriceUpdateId__lbl: "Parent price update ID",
	priceCatName__lbl: "Price category name",
	priceCatName__tlp: "Price category name",
	priceCategoryId__lbl: "Price category (ID)",
	priceCategoryName__lbl: "Price category",
	priceCategoryName__tlp: "Price category",
	priceUpdateId__lbl: "Price update (ID)",
	price__lbl: "Current price",
	price__tlp: "Current price category for selected contract",
	selected__lbl: "Selected for update",
	selected__tlp: "The current price category is selected for the price update job.",
	unitCode__lbl: "Unit",
	unitCode__tlp: "Code",
	unitId__lbl: "Unit (ID)"
});
