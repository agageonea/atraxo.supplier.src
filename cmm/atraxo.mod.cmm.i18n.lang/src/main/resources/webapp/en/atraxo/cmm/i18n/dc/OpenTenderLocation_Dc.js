
Ext.override(atraxo.cmm.i18n.dc.OpenTenderLocation_Dc$SelectList, {
	deliveryPoint__lbl: "Delivery point",
	flightServiceType__lbl: "Operational type",
	fuelProduct__lbl: "Product",
	name__lbl: "Location name",
	taxType__lbl: "Tax type"
});
