Ext.override(atraxo.cmm.i18n.ds.ContractPriceCategoryLov_Ds, {
	contCode__lbl: "Contract",
	contCode__tlp: "Contract #",
	contId__lbl: "Contract (ID)",
	name__lbl: "Price name",
	name__tlp: "Name of the price as it will be displayed on invoice"
});
