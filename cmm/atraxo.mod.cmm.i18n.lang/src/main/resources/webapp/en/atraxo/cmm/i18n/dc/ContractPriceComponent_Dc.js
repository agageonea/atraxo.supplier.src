
Ext.override(atraxo.cmm.i18n.dc.ContractPriceComponent_Dc$Edit, {
	price__lbl: "New price value"
});

Ext.override(atraxo.cmm.i18n.dc.ContractPriceComponent_Dc$List, {
	currency__lbl: "Currency",
	provisional__lbl: "Provisional",
	unit__lbl: "Unit",
	validFrom__lbl: "Valid from",
	validTo__lbl: "Valid to",
	value__lbl: "Value",
	withHold__lbl: "Withhold"
});

Ext.override(atraxo.cmm.i18n.dc.ContractPriceComponent_Dc$SimpleList, {
	currency__lbl: "Currency",
	unit__lbl: "Unit",
	validFrom__lbl: "Valid from",
	validTo__lbl: "Valid to",
	value__lbl: "Value"
});
