
Ext.override(atraxo.cmm.i18n.dc.Contract_Dc$Filter, {
	avgMethodName__lbl: "Averaging method",
	financialSourceCode__lbl: "Financial source",
	supplier__lbl: "Supplier"
});

Ext.override(atraxo.cmm.i18n.dc.Contract_Dc$Header, {
	btnToggleContractPeriod__lbl: "...",
	contractPeriodLabel__lbl: "Contract period",
	supplier__tlp: "Select supplier"
});

Ext.override(atraxo.cmm.i18n.dc.Contract_Dc$PaymentTerms, {
	btnTogglePaymentTerms__lbl: "...",
	btnToggle__lbl: "...",
	financialSourceAvgMethodPeriod__lbl: "Using ForEx of",
	paymentTermRefDay__lbl: "Payment terms"
});

Ext.override(atraxo.cmm.i18n.dc.Contract_Dc$SelectCustomer, {
	customerLov__lbl: "Select customer"
});

Ext.override(atraxo.cmm.i18n.dc.Contract_Dc$TabList, {
	avgMthdName__lbl: "Averaging method",
	finSCode__lbl: "Financial source"
});

Ext.override(atraxo.cmm.i18n.dc.Contract_Dc$Volumes, {
	awardedVolumeLabel__lbl: "Awarded volume",
	periodLabel__lbl: "Period",
	quantityTypeLabel__lbl: "Quantity type",
	shareLabel__lbl: "Share percentage",
	toleranceLabel__lbl: "Tolerance"
});

Ext.override(atraxo.cmm.i18n.dc.Contract_Dc$copySupplier, {
	supplierField__lbl: "Select supplier",
	supplierField__tlp: "Select supplier"
});

Ext.override(atraxo.cmm.i18n.dc.Contract_Dc$financialSourceAvgMethodPeriod, {
	avgMethodName__lbl: "Averaging method",
	financialSourceCode__lbl: "Financial source"
});

Ext.override(atraxo.cmm.i18n.dc.Contract_Dc$paymentTermsRefDay, {
	daysLabel__lbl: "days",
	paymentTermsLabel__lbl: "Payment terms"
});

Ext.override(atraxo.cmm.i18n.dc.Contract_Dc$selectSupplier, {
	SupplierName__lbl: "Select supplier",
	SupplierName__tlp: "Select supplier"
});
