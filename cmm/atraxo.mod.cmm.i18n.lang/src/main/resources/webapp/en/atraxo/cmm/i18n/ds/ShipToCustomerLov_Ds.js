Ext.override(atraxo.cmm.i18n.ds.ShipToCustomerLov_Ds, {
	code__lbl: "Customer",
	code__tlp: "Account code",
	contractId__lbl: "Contract (ID)",
	custId__lbl: "Customer (ID)",
	iataCode__lbl: "IATA code",
	iataCode__tlp: "IATA code",
	isCustomer__lbl: "Is customer?",
	isCustomer__tlp: "Is customer?",
	isThirdParty__lbl: "Is third party?",
	name__lbl: "Customer name",
	name__tlp: "Name",
	refid__lbl: "Customer (Ref-ID)",
	status__lbl: "Invoice status",
	status__tlp: "Contract status",
	type__lbl: "Contract type",
	type__tlp: "Contract type"
});
