Ext.override(atraxo.cmm.i18n.ds.BuyContractLov_Ds, {
	code__lbl: "Contract #",
	code__tlp: "Contract #",
	dealType__lbl: "Deal type",
	dealType__tlp: "Deal type",
	id__lbl: "ID",
	scope__lbl: "Contract scope",
	scope__tlp: "The contract business scope: into plane or into storage",
	status__lbl: "Contract status",
	status__tlp: "Contract status",
	subType__lbl: "Delivery point",
	subType__tlp: "Delivery point of fuel",
	type__lbl: "Type",
	type__tlp: "Type"
});
