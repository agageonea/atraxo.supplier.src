
Ext.override(atraxo.cmm.i18n.dc.TenderLocationAirlines_Dc$FourthStepList, {
	period__lbl: "Period",
	unitCode__lbl: "Unit",
	volume__lbl: "Volume/Period"
});

Ext.override(atraxo.cmm.i18n.dc.TenderLocationAirlines_Dc$List, {
	airline__lbl: "Airline",
	biddingStatus__lbl: "Bidding status",
	isValid__lbl: "Valid?",
	notes__lbl: "Notes",
	period__lbl: "Period",
	unit__lbl: "Unit",
	volume__lbl: "Volume/Period"
});
