Dear [[title]] [[fullName]],

Please note that a sales contract approval request was submitted by [[fullnameOfRequester]] for:
Contract: [[contractCode]]
Contract holder: [[contractHolder]]
Customer: [[customerName]]
Contract period: [[validFrom]] - [[validTo]]
Location: [[contractLocation]]

Note from requester: 
[[approvalRequestNote]]

Notes from prior level approvers:
[[approverNameFirstLevel]]
[[approvalNoteFirstLevel]]

[[approverNameSecondLevel]]
[[approvalNoteSecondLevel]]

[[approverNameThirdLevel]]
[[approvalNoteThirdLevel]]

Your supplier.ONE notification master