To Whom It May Concern:

Please find enclosed the sales invoice(s).

Should you have any question or concern, please do not hesitate to contact me.

Yours sincerely,

[[customer.accountManager]]
[[subsidiary.name]]