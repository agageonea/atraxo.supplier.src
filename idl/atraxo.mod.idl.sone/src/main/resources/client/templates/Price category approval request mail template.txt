Dear [[title]] [[fullName]],

Please note that a sales contract price category update approval request was submitted by [[fullnameOfRequester]] for:
Contract: [[contractCode]]
Contract holder: [[contractHolder]]
Location: [[contractLocation]]
Customer: [[customerName]]

The following price categories have been changed on the contract:

[[priceChange.changes]]

Note from requester: 
[[approvalRequestNote]]

Your supplier.ONE notification master