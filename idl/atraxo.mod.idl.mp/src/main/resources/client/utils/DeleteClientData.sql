DELIMITER $$

DROP PROCEDURE IF EXISTS `DeleteClientData`$$

CREATE DEFINER=`root`@`%` PROCEDURE `DeleteClientData`(IN p_client_code VARCHAR(64))
BEGIN
DECLARE p_clientid VARCHAR(64);
SET FOREIGN_KEY_CHECKS = 0;
SELECT  id  INTO p_clientid FROM sys_client WHERE CODE = p_client_code;
DELETE FROM  `bas_tserie` WHERE clientid  = p_clientid;
DELETE FROM  `bas_raw_tserie_item`WHERE clientid  = p_clientid;
DELETE FROM  `bas_tserie_average`WHERE clientid  = p_clientid;
DELETE FROM  `bas_tserie_item`WHERE clientid  = p_clientid;
DELETE FROM  `bas_quot`WHERE clientid  = p_clientid;
DELETE FROM  `bas_quotval`WHERE clientid  = p_clientid;
DELETE FROM  `bas_exch_rate` WHERE clientid  = p_clientid;
DELETE FROM  `bas_exch_rate_val` WHERE clientid  = p_clientid;
DELETE FROM  `bas_aircraft` WHERE clientid  = p_clientid;
DELETE FROM  `cmm_contr_price_cmpnt` WHERE clientid  = p_clientid;
DELETE FROM  `cmm_contr_price_cmpnt_conv` WHERE clientid  = p_clientid;
DELETE FROM  `cmm_contr_price_ctgry` WHERE clientid  = p_clientid;
DELETE FROM  `cmm_contr_price_restrict` WHERE clientid  = p_clientid;
DELETE FROM  `cmm_contr_prices`WHERE clientid  = p_clientid;
DELETE FROM  `cmm_contracts`WHERE clientid  = p_clientid;
DELETE FROM  `ops_fuel_price_location_quote` WHERE clientid  = p_clientid;
DELETE FROM  `ops_flight_events` WHERE clientid  = p_clientid;
DELETE FROM  `ops_fuel_price_location_quote`WHERE clientid  = p_clientid;
DELETE FROM  `ops_fuel_prices_quotes`WHERE clientid  = p_clientid;
DELETE FROM  `ops_fuel_requests`WHERE clientid  = p_clientid;
DELETE FROM  `ops_prices_policies`WHERE clientid  = p_clientid;
DELETE FROM  `ops_fuel_tickets`WHERE clientid  = p_clientid;
DELETE FROM  `ops_location_purchase_orders`WHERE clientid  = p_clientid;
DELETE FROM  `ops_purchase_orders`WHERE clientid  = p_clientid;
DELETE FROM  `ops_fuel_ticket_hard_copy`WHERE clientid  = p_clientid;
DELETE FROM  `bas_exposure`WHERE clientid  = p_clientid;
DELETE FROM  `bas_change_history`WHERE clientid  = p_clientid;
DELETE FROM  `bas_co_credit_lines`WHERE clientid  = p_clientid;
DELETE FROM  `bas_companies`WHERE clientid  = p_clientid;
DELETE FROM  `bas_contacts`WHERE clientid  = p_clientid;
DELETE FROM  `bas_master_agreements`WHERE clientid  = p_clientid;
DELETE FROM  `bas_notes`WHERE clientid  = p_clientid;
DELETE FROM  `bas_trade_references`WHERE clientid  = p_clientid;
DELETE FROM  `acc_delivery_notes`WHERE clientid  = p_clientid;
DELETE FROM  `acc_delivery_notes_contracts` WHERE clientid  = p_clientid;
DELETE FROM  `acc_fuel_events`WHERE clientid  = p_clientid;
DELETE FROM  `acc_invoice_lines`WHERE clientid  = p_clientid;
DELETE FROM  `acc_invoices`WHERE clientid  = p_clientid;
DELETE FROM  `acc_outgoing_invoice` WHERE clientid = p_clientid;
DELETE FROM  `acc_outgoing_invoice_line` WHERE clientid = p_clientid;
DELETE FROM  `acc_payable_accruals` WHERE clientid  = p_clientid;
DELETE FROM  `acc_receivable_accruals` WHERE clientid  = p_clientid;
SET FOREIGN_KEY_CHECKS = 1;
END$$

DELIMITER ;