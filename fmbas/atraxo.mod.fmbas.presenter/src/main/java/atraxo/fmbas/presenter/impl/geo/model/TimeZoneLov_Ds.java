/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.geo.model;

import atraxo.fmbas.domain.impl.geo.TimeZone;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = TimeZone.class)
public class TimeZoneLov_Ds extends AbstractLov_Ds<TimeZone> {

	public static final String ALIAS = "fmbas_TimeZoneLov_Ds";

	public static final String F_NAME = "name";
	public static final String F_ACTIVE = "active";

	@DsField
	private String name;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public TimeZoneLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public TimeZoneLov_Ds(TimeZone e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
