/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.asgn.qb;

import atraxo.fmbas.presenter.impl.customer.asgn.model.CustomerLocations_Asgn;
import atraxo.fmbas.presenter.impl.customer.asgn.model.CustomerLocations_AsgnParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

public class CustomerLocations_AsgnQbLeft
		extends
			QueryBuilderWithJpql<CustomerLocations_Asgn, CustomerLocations_Asgn, CustomerLocations_AsgnParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getLocationId() != null
				&& !"".equals(this.params.getLocationId())) {
			addFilterCondition("  e.customerLocation.id = :locationId ");
			addCustomFilterItem("locationId", this.params.getLocationId());
		}
	}
}
