/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.notification.service;

import java.util.List;

import atraxo.fmbas.business.api.notification.INotificationService;
import atraxo.fmbas.domain.impl.notification.Notification;
import atraxo.fmbas.presenter.impl.notification.model.Notification_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class Notification_DsService extends AbstractEntityDsService<Notification_Ds, Notification_Ds, Object, Notification>
		implements IDsService<Notification_Ds, Notification_Ds, Object> {

	@Override
	public List<Notification_Ds> find(IQueryBuilder<Notification_Ds, Notification_Ds, Object> builder) throws Exception {
		INotificationService srv = (INotificationService) this.getEntityService();
		List<Notification> entities = srv.findForUser(builder.getFilter().getUserCode());
		List<Notification_Ds> list = this.getConverter().entitiesToModels(entities, this.getEntityService().getEntityManager(), null);
		return list;

	}
}
