/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.aircraft.model;

import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Aircraft.class, sort = {@SortField(field = AircraftsLov_Ds.F_ACTYPECODE)})
public class AircraftsLov_Ds extends AbstractLov_Ds<Aircraft> {

	public static final String ALIAS = "fmbas_AircraftsLov_Ds";

	public static final String F_CUSTID = "custId";
	public static final String F_CUSTCODE = "custCode";
	public static final String F_ACTYPEID = "acTypeId";
	public static final String F_ACTYPECODE = "acTypeCode";
	public static final String F_REGISTRATION = "registration";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";

	@DsField(join = "left", path = "customer.id")
	private Integer custId;

	@DsField(join = "left", path = "customer.code")
	private String custCode;

	@DsField(join = "left", path = "acTypes.id")
	private Integer acTypeId;

	@DsField(join = "left", path = "acTypes.code")
	private String acTypeCode;

	@DsField
	private String registration;

	@DsField(jpqlFilter = " e.validTo >= :validFrom ")
	private Date validFrom;

	@DsField(jpqlFilter = " e.validFrom <= :validTo ")
	private Date validTo;

	/**
	 * Default constructor
	 */
	public AircraftsLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AircraftsLov_Ds(Aircraft e) {
		super(e);
	}

	public Integer getCustId() {
		return this.custId;
	}

	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	public String getCustCode() {
		return this.custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public Integer getAcTypeId() {
		return this.acTypeId;
	}

	public void setAcTypeId(Integer acTypeId) {
		this.acTypeId = acTypeId;
	}

	public String getAcTypeCode() {
		return this.acTypeCode;
	}

	public void setAcTypeCode(String acTypeCode) {
		this.acTypeCode = acTypeCode;
	}

	public String getRegistration() {
		return this.registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
}
