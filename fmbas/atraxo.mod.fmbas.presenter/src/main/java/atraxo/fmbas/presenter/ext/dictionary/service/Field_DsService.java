/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.dictionary.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.util.StringUtils;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.ext.dictionary.delegate.FieldListBuilder;
import atraxo.fmbas.domain.impl.dictionary.Field;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceParameter;
import atraxo.fmbas.presenter.impl.dictionary.model.Field_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service Field_DsService
 */
public class Field_DsService extends AbstractEntityDsService<Field_Ds, Field_Ds, Object, Field> implements IDsService<Field_Ds, Field_Ds, Object> {

	@Override
	public List<Field_Ds> find(IQueryBuilder<Field_Ds, Field_Ds, Object> builder, List<String> fieldNames) throws Exception {
		List<Field_Ds> retList = new ArrayList<>();
		IExternalInterfaceService externalInterfaceService = (IExternalInterfaceService) this.findEntityService(ExternalInterface.class);
		ExternalInterface ei = externalInterfaceService.findById(builder.getFilter().getExternalInterfaceId());
		Collection<ExternalInterfaceParameter> params = ei.getParams();
		String className = null;
		for (ExternalInterfaceParameter eip : params) {
			if ("Dictionary class name".equals(eip.getName())) {
				className = eip.getDefaultValue();
				break;
			}
		}
		if (!StringUtils.isEmpty(className)) {
			Class<?> clazz = Class.forName(className);
			List<String> listStr = FieldListBuilder.build(clazz, "");
			for (String str : listStr) {
				Field_Ds ds = new Field_Ds();
				ds.setExternalInterfaceId(ei.getId());
				ds.setExternalInterfaceName(ei.getName());
				ds.setFieldName(str);
				retList.add(ds);
			}
		}
		return retList;
	}

	@Override
	public Long count(IQueryBuilder<Field_Ds, Field_Ds, Object> builder) throws Exception {
		return Long.parseLong("1");
	}

}
