package atraxo.fmbas.presenter.ext.job.delegate;

import java.util.ArrayList;
import java.util.List;

import atraxo.ad.business.api.scheduler.IJobExecutionService;
import atraxo.ad.domain.impl.scheduler.JobExecution;
import atraxo.fmbas.presenter.impl.job.model.ActionResultHistory_Ds;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class ActionResultHistory_Pd extends AbstractPresenterDelegate {

	public void delete(List<ActionResultHistory_Ds> results) throws Exception {
		List<Long> ids = new ArrayList<>();
		IJobExecutionService service = (IJobExecutionService) this.findEntityService(JobExecution.class);
		for (ActionResultHistory_Ds result : results) {
			ids.add(result.getJobExId());
		}
		service.deleteJobExecution(ids);
	}
}
