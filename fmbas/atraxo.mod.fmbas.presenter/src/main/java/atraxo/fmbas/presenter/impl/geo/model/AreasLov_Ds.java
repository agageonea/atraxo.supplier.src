/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.geo.model;

import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Area.class)
public class AreasLov_Ds extends AbstractLov_Ds<Area> {

	public static final String ALIAS = "fmbas_AreasLov_Ds";

	public static final String F_CODE = "code";
	public static final String F_ACTIVE = "active";
	public static final String F_NAME = "name";

	@DsField
	private String code;

	@DsField
	private Boolean active;

	@DsField
	private String name;

	/**
	 * Default constructor
	 */
	public AreasLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AreasLov_Ds(Area e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
