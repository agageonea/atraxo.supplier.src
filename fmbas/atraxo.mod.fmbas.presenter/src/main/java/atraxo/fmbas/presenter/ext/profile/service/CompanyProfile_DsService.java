/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.profile.service;

import java.util.List;

import atraxo.fmbas.business.api.financialSources.IFinancialSourcesService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.profile.CompanyProfile;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.presenter.impl.profile.model.CompanyProfile_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class CompanyProfile_DsService extends AbstractEntityDsService<CompanyProfile_Ds, CompanyProfile_Ds, Object, CompanyProfile>
		implements IDsService<CompanyProfile_Ds, CompanyProfile_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<CompanyProfile_Ds, CompanyProfile_Ds, Object> builder, List<CompanyProfile_Ds> result) throws Exception {
		super.postFind(builder, result);
		ISystemParameterService sysParam = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		IFinancialSourcesService financialSource = (IFinancialSourcesService) this.findEntityService(FinancialSources.class);
		for (CompanyProfile_Ds profile : result) {
			profile.setSysCurrency(sysParam.getSysCurrency());
			profile.setSysUnits(sysParam.getSysUnit());
			profile.setSysFinancialSource(financialSource.getStandardFinancialSource().getCode());
		}
	}

	@Override
	protected void postUpdate(List<CompanyProfile_Ds> list, Object params) throws Exception {
		super.postUpdate(list, params);
		IFinancialSourcesService financialSource = (IFinancialSourcesService) this.findEntityService(FinancialSources.class);
		for (CompanyProfile_Ds ds : list) {
			financialSource.setStandardFinancialSource(ds.getSysFinancialSource());
		}
	}
}
