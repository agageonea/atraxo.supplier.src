/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.job.model;

import atraxo.ad.domain.impl.scheduler.JobExecutionParams;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;
import seava.j4e.presenter.model.AbstractDsModel;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = JobExecutionParams.class, sort = {
		@SortField(field = ActionResultHistory_Ds.F_STARTTIME, desc = true),
		@SortField(field = ActionResultHistory_Ds.F_JOBEXID)})
@RefLookups({@RefLookup(refId = ActionResultHistory_Ds.F_JOBEXID)})
public class ActionResultHistory_Ds extends AbstractDsModel<JobExecutionParams> {

	public static final String ALIAS = "fmbas_ActionResultHistory_Ds";

	public static final String F_VALUE = "value";
	public static final String F_NAME = "name";
	public static final String F_JOBEXID = "jobExId";
	public static final String F_STARTTIME = "startTime";
	public static final String F_ENDTIME = "endTime";
	public static final String F_STATUS = "status";
	public static final String F_EXITCODE = "exitCode";
	public static final String F_EXITMESSAGE = "exitMessage";
	public static final String F_ACTIONNAME = "actionName";
	public static final String F_DURATION = "duration";
	public static final String F_MSG = "msg";
	public static final String F_STACKTRACE = "stacktrace";

	@DsField(path = "longVal")
	private Long value;

	@DsField(path = "keyName")
	private String name;

	@DsField(join = "left", path = "jobExecution.id")
	private Long jobExId;

	@DsField(join = "left", path = "jobExecution.startTime")
	private Date startTime;

	@DsField(join = "left", path = "jobExecution.endTime")
	private Date endTime;

	@DsField(join = "left", path = "jobExecution.status")
	private String status;

	@DsField(join = "left", path = "jobExecution.exitCode")
	private String exitCode;

	@DsField(join = "left", path = "jobExecution.exitMessage")
	private String exitMessage;

	@DsField(fetch = false)
	private String actionName;

	@DsField(fetch = false)
	private String duration;

	@DsField(fetch = false)
	private String msg;

	@DsField(fetch = false)
	private String stacktrace;

	/**
	 * Default constructor
	 */
	public ActionResultHistory_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ActionResultHistory_Ds(JobExecutionParams e) {
		super(e);
	}

	public Long getValue() {
		return this.value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getJobExId() {
		return this.jobExId;
	}

	public void setJobExId(Long jobExId) {
		this.jobExId = jobExId;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getExitCode() {
		return this.exitCode;
	}

	public void setExitCode(String exitCode) {
		this.exitCode = exitCode;
	}

	public String getExitMessage() {
		return this.exitMessage;
	}

	public void setExitMessage(String exitMessage) {
		this.exitMessage = exitMessage;
	}

	public String getActionName() {
		return this.actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public String getDuration() {
		return this.duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getMsg() {
		return this.msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getStacktrace() {
		return this.stacktrace;
	}

	public void setStacktrace(String stacktrace) {
		this.stacktrace = stacktrace;
	}
}
