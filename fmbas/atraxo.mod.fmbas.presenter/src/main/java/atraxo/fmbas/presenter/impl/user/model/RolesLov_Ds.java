/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.user.model;

import atraxo.fmbas.domain.impl.user.RoleSupp;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;
import seava.j4e.api.model.IModelWithClientId;
import seava.j4e.api.model.IModelWithId;
import seava.j4e.presenter.model.AbstractDsModel;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = RoleSupp.class, sort = {@SortField(field = RolesLov_Ds.F_CODE)})
public class RolesLov_Ds extends AbstractDsModel<RoleSupp>
		implements
			IModelWithId<String>,
			IModelWithClientId {

	public static final String ALIAS = "fmbas_RolesLov_Ds";

	public static final String F_ID = "id";
	public static final String F_CLIENTID = "clientId";
	public static final String F_NAME = "name";
	public static final String F_CODE = "code";

	@DsField
	private String id;

	@DsField(noUpdate = true)
	private String clientId;

	@DsField
	private String name;

	@DsField
	private String code;

	/**
	 * Default constructor
	 */
	public RolesLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public RolesLov_Ds(RoleSupp e) {
		super(e);
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
