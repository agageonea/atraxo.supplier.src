/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.financialSources.model;

import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FinancialSources.class, jpqlWhere = "e.active = true", sort = {@SortField(field = FinancialSourceLov_Ds.F_CODE)})
public class FinancialSourceLov_Ds extends AbstractLov_Ds<FinancialSources> {

	public static final String ALIAS = "fmbas_FinancialSourceLov_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_ACTIVE = "active";

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public FinancialSourceLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FinancialSourceLov_Ds(FinancialSources e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
