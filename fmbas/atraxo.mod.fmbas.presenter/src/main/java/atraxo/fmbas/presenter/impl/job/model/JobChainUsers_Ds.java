/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.job.model;

import atraxo.fmbas.domain.impl.fmbas_type.JobChainNotificationCondition;
import atraxo.fmbas.domain.impl.fmbas_type.NotificationType;
import atraxo.fmbas.domain.impl.job.JobChainUser;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = JobChainUser.class)
@RefLookups({
		@RefLookup(refId = JobChainUsers_Ds.F_JOBCHAINID),
		@RefLookup(refId = JobChainUsers_Ds.F_USERID, namedQuery = UserSupp.NQ_FIND_BY_LOGIN, params = {@Param(name = "loginName", field = JobChainUsers_Ds.F_LOGINNAME)})})
public class JobChainUsers_Ds extends AbstractDs_Ds<JobChainUser> {

	public static final String ALIAS = "fmbas_JobChainUsers_Ds";

	public static final String F_JOBCHAINID = "jobChainId";
	public static final String F_JOBCHAINNAME = "jobChainName";
	public static final String F_USERID = "userId";
	public static final String F_USERNAME = "userName";
	public static final String F_USEREMAIL = "userEmail";
	public static final String F_LOGINNAME = "loginName";
	public static final String F_NOTIFICATIONCONDITION = "notificationCondition";
	public static final String F_ENABLED = "enabled";
	public static final String F_NOTIFICATIONTYPE = "notificationType";

	@DsField(join = "left", path = "jobChain.id")
	private Integer jobChainId;

	@DsField(join = "left", path = "jobChain.name")
	private String jobChainName;

	@DsField(join = "left", path = "user.id")
	private String userId;

	@DsField(join = "left", path = "user.name")
	private String userName;

	@DsField(join = "left", path = "user.email")
	private String userEmail;

	@DsField(join = "left", path = "user.loginName")
	private String loginName;

	@DsField
	private JobChainNotificationCondition notificationCondition;

	@DsField
	private Boolean enabled;

	@DsField
	private NotificationType notificationType;

	/**
	 * Default constructor
	 */
	public JobChainUsers_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public JobChainUsers_Ds(JobChainUser e) {
		super(e);
	}

	public Integer getJobChainId() {
		return this.jobChainId;
	}

	public void setJobChainId(Integer jobChainId) {
		this.jobChainId = jobChainId;
	}

	public String getJobChainName() {
		return this.jobChainName;
	}

	public void setJobChainName(String jobChainName) {
		this.jobChainName = jobChainName;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public JobChainNotificationCondition getNotificationCondition() {
		return this.notificationCondition;
	}

	public void setNotificationCondition(
			JobChainNotificationCondition notificationCondition) {
		this.notificationCondition = notificationCondition;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public NotificationType getNotificationType() {
		return this.notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}
}
