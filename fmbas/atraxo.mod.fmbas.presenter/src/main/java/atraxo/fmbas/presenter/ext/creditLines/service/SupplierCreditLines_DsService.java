/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.creditLines.service;

import java.util.List;

import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.presenter.impl.creditLines.model.SupplierCreditLines_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class SupplierCreditLines_DsService extends AbstractEntityDsService<SupplierCreditLines_Ds, SupplierCreditLines_Ds, Object, CreditLines>
		implements IDsService<SupplierCreditLines_Ds, SupplierCreditLines_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<SupplierCreditLines_Ds, SupplierCreditLines_Ds, Object> builder, List<SupplierCreditLines_Ds> result)
			throws Exception {
		super.postFind(builder, result);

		for (SupplierCreditLines_Ds ds : result) {

			ds.setExposure(0);
		}
	}
}
