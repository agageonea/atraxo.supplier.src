/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.dictionary.service;

import java.util.List;

import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.dictionary.IDictionaryService;
import atraxo.fmbas.domain.impl.dictionary.Condition;
import atraxo.fmbas.domain.impl.dictionary.Dictionary;
import atraxo.fmbas.presenter.impl.dictionary.model.Dictionary_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service Dictionary_DsService
 */
public class Dictionary_DsService extends AbstractEntityDsService<Dictionary_Ds, Dictionary_Ds, Object, Dictionary>
		implements IDsService<Dictionary_Ds, Dictionary_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<Dictionary_Ds, Dictionary_Ds, Object> builder, List<Dictionary_Ds> result) throws Exception {
		super.postFind(builder, result);
		IDictionaryService dictionaryService = (IDictionaryService) this.findEntityService(Dictionary.class);
		for (Dictionary_Ds ds : result) {
			Dictionary dictionary = dictionaryService.findById(ds.getId());
			if (!CollectionUtils.isEmpty(dictionary.getConditions())) {
				for (Condition c : dictionary.getConditions()) {
					ds.setCriteria((ds.getCriteria() != null ? ds.getCriteria() : "") + c.getFieldName() + " " + c.getOperator().getName() + " "
							+ c.getValue() + " ; ");
				}
			}
		}

	}

}
