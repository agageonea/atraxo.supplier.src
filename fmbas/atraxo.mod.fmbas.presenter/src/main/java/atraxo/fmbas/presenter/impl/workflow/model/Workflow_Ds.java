/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.workflow.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowDeployStatus;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowGroup;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Workflow.class)
@RefLookups({@RefLookup(refId = Workflow_Ds.F_CUSTOMERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Workflow_Ds.F_CUSTOMERCODE)})})
public class Workflow_Ds extends AbstractDs_Ds<Workflow> {

	public static final String ALIAS = "fmbas_Workflow_Ds";

	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_NAME = "name";
	public static final String F_CODE = "code";
	public static final String F_WORKFLOWFILE = "workflowFile";
	public static final String F_PROCESSKEY = "processKey";
	public static final String F_DESCRIPTION = "description";
	public static final String F_DEPLOYSTATUS = "deployStatus";
	public static final String F_DEPLOYVERSION = "deployVersion";
	public static final String F_WORKFLOWGROUP = "workflowGroup";
	public static final String F_LABELFIELD = "labelField";

	@DsField(join = "left", path = "subsidiary.code")
	private String customerCode;

	@DsField(join = "left", path = "subsidiary.id")
	private Integer customerId;

	@DsField
	private String name;

	@DsField
	private String code;

	@DsField
	private String workflowFile;

	@DsField
	private String processKey;

	@DsField
	private String description;

	@DsField
	private WorkflowDeployStatus deployStatus;

	@DsField
	private Integer deployVersion;

	@DsField
	private WorkflowGroup workflowGroup;

	@DsField(fetch = false)
	private String labelField;

	/**
	 * Default constructor
	 */
	public Workflow_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Workflow_Ds(Workflow e) {
		super(e);
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getWorkflowFile() {
		return this.workflowFile;
	}

	public void setWorkflowFile(String workflowFile) {
		this.workflowFile = workflowFile;
	}

	public String getProcessKey() {
		return this.processKey;
	}

	public void setProcessKey(String processKey) {
		this.processKey = processKey;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public WorkflowDeployStatus getDeployStatus() {
		return this.deployStatus;
	}

	public void setDeployStatus(WorkflowDeployStatus deployStatus) {
		this.deployStatus = deployStatus;
	}

	public Integer getDeployVersion() {
		return this.deployVersion;
	}

	public void setDeployVersion(Integer deployVersion) {
		this.deployVersion = deployVersion;
	}

	public WorkflowGroup getWorkflowGroup() {
		return this.workflowGroup;
	}

	public void setWorkflowGroup(WorkflowGroup workflowGroup) {
		this.workflowGroup = workflowGroup;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}
}
