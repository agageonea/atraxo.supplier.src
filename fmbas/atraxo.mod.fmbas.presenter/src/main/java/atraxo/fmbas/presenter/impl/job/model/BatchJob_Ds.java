/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.job.model;

import atraxo.fmbas.domain.impl.job.BatchJob;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = BatchJob.class)
public class BatchJob_Ds extends AbstractDs_Ds<BatchJob> {

	public static final String ALIAS = "fmbas_BatchJob_Ds";

	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_BEANNAME = "beanName";

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private String beanName;

	/**
	 * Default constructor
	 */
	public BatchJob_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public BatchJob_Ds(BatchJob e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBeanName() {
		return this.beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}
}
