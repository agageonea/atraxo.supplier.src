/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.asgn.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;
import seava.j4e.presenter.model.AbstractAsgnModel;

@Ds(entity = Customer.class, jpqlWhere = "(e.isThirdParty = true or e.isCustomer = true)", sort = {@SortField(field = FinalCustomerResseler_Asgn.f_code)})
public class FinalCustomerResseler_Asgn extends AbstractAsgnModel<Customer> {

	public static final String ALIAS = "fmbas_FinalCustomerResseler_Asgn";

	public static final String f_id = "id";
	public static final String f_name = "name";
	public static final String f_code = "code";
	public static final String f_iata = "iata";
	public static final String f_icao = "icao";

	@DsField(path = "id")
	private Integer id;

	@DsField(path = "name")
	private String name;

	@DsField(path = "code")
	private String code;

	@DsField(path = "iataCode")
	private String iata;

	@DsField(path = "icaoCode")
	private String icao;

	public FinalCustomerResseler_Asgn() {
	}

	public FinalCustomerResseler_Asgn(Customer e) {
		super();
		this.id = e.getId();
		this.name = e.getName();
		this.code = e.getCode();
		this.iata = e.getIataCode();
		this.icao = e.getIcaoCode();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getIata() {
		return this.iata;
	}

	public void setIata(String iata) {
		this.iata = iata;
	}

	public String getIcao() {
		return this.icao;
	}

	public void setIcao(String icao) {
		this.icao = icao;
	}
}
