/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.geo.service;

import java.util.List;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.fmbas_type.AreasType;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.presenter.impl.geo.model.Area_Ds;
import atraxo.fmbas.presenter.impl.geo.model.Area_DsParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class Area_DsService extends AbstractEntityDsService<Area_Ds, Area_Ds, Area_DsParam, Area> implements
		IDsService<Area_Ds, Area_Ds, Area_DsParam> {

	public static final String BASE_AREA = "WW";
	public static final String SYSTEM_AREA = "System";

	@Override
	protected void preUpdate(List<Area_Ds> list, Area_DsParam params) throws Exception {

		super.preUpdate(list, params);
		for (Area_Ds e : list) {
			if (this.isWWAreas(e)) {
				throw new BusinessException(BusinessErrorCode.AREA_WW, BusinessErrorCode.AREA_WW.getErrMsg());
			}
			if (AreasType._SYSTEM_.equals(e.getIndicator())) {
				throw new BusinessException(BusinessErrorCode.AREA_SYSTEM, BusinessErrorCode.AREA_SYSTEM.getErrMsg());
			}
		}
	}

	/**
	 * Verify if is the system's maintained area.
	 *
	 * @param e
	 * @return
	 */
	private boolean isWWAreas(Area_Ds e) {
		return e.getCode().matches(Area_DsService.BASE_AREA);
	}

}
