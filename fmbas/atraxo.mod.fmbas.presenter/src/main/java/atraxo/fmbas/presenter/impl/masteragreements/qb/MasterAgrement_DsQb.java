/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.masteragreements.qb;

import atraxo.fmbas.presenter.impl.masteragreements.model.MasterAgrement_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class MasterAgrement_DsQb
		extends
			QueryBuilderWithJpql<MasterAgrement_Ds, MasterAgrement_Ds, Object> {
}
