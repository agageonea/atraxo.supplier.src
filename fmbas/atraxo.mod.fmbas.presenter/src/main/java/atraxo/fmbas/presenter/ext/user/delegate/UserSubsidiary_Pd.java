package atraxo.fmbas.presenter.ext.user.delegate;

import java.util.ArrayList;
import java.util.List;

import atraxo.fmbas.business.api.user.IUserSubsidiaryService;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;
import atraxo.fmbas.presenter.impl.user.model.UserSubsidiary_Ds;
import seava.j4e.api.session.IOrganization;
import seava.j4e.api.session.Session;
import seava.j4e.commons.security.AppClient;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 *
 * @author zspeter
 *
 */
public class UserSubsidiary_Pd extends AbstractPresenterDelegate {

	/**
	 *
	 * @param ds
	 * @throws Exception
	 */
	public void setActive(UserSubsidiary_Ds ds) throws Exception {
		for (IOrganization org : Session.user.get().getProfile().getOrganizations()) {
			if (org.getCode().equals(ds.getCode())) {
				org.setUsed(true);
			} else {
				org.setUsed(false);
			}
		}
		AppClient client = (AppClient)Session.user.get().getClient();
		IUserSubsidiaryService service = (IUserSubsidiaryService) this.findEntityService(UserSubsidiary.class);
		UserSubsidiary subsidiary = service.findById(ds.getId());
		client.setActiveSubsidiaryId(subsidiary.getSubsidiary().getRefid());
	}

	/**
	 *
	 * @param ds
	 * @throws Exception
	 */
	public void setDefault(UserSubsidiary_Ds ds) throws Exception {
		IUserSubsidiaryService service = (IUserSubsidiaryService) this.findEntityService(UserSubsidiary.class);
		service.setDefaultSubsidiary(ds.getId());
	}

	/**
	 *
	 * @param dsList
	 * @throws Exception
	 */
	public void removeSubsidiary(List<UserSubsidiary_Ds> dsList) throws Exception {
		IUserSubsidiaryService service = (IUserSubsidiaryService) this.findEntityService(UserSubsidiary.class);
		List<UserSubsidiary> usList = new ArrayList<>();

		for (UserSubsidiary_Ds ds : dsList) {
			usList.add(service.findById(ds.getId()));
		}
		service.removeSubsidiary(usList);
	}

}
