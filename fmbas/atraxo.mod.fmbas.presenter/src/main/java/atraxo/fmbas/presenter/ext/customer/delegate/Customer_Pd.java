package atraxo.fmbas.presenter.ext.customer.delegate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.documentNumberSeries.IDocumentNumberSeriesService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceHistoryService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.ws.IRestClientCallbackService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.business.ws.ExternalInterfaceWSHelper;
import atraxo.fmbas.business.ws.client.PumaRestClient;
import atraxo.fmbas.business.ws.toNav.WSCustomerToDTOTransformer;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.documentNumberSeries.DocumentNumberSeries;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceHistory;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataProcessedStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataTransmissionStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.fmbas_type.DocumentNumberSeriesType;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import atraxo.fmbas.presenter.impl.customer.model.Customers_Ds;
import atraxo.fmbas.presenter.impl.customer.model.Customers_DsParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author vhojda
 */
public class Customer_Pd extends AbstractPresenterDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(Customer_Pd.class);

	private static final String EXPORT_OPTION_ALL = "all";
	private static final String EXPORT_OPTION_SELECTED = "selected";
	private static final String EXPORT_OPTION_FAILED = "failed";

	private static final String CUSTOMER_MODEL_PROPERY_TRANSMISSION_STATUS = "transmissionStatus";

	/**
	 * IF ticket status is equal with ORIGINAL or UPDATED and the approval status is FOR APPROVAL can we set status to OK.
	 *
	 * @param customerDs
	 * @throws Exception
	 */
	public void approve(Customers_Ds customerDs) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START approve()");
		}
		List<Customers_Ds> cust = new ArrayList<>();
		cust.add(customerDs);
		this.approveList(cust);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END approve()");
		}
	}

	/**
	 * @param custDsList
	 * @throws Exception
	 */
	public void approveList(List<Customers_Ds> custDsList) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START approveList()");
		}

		ICustomerService custService = (ICustomerService) this.findEntityService(Customer.class);
		List<Customer> customers = new ArrayList<>();

		StringBuilder sb = new StringBuilder("<ul>");
		for (Customers_Ds customerDs : custDsList) {
			Customer cust = custService.findById(customerDs.getId());
			if (CustomerStatus._INACTIVE_.equals(cust.getStatus()) || CustomerStatus._PROSPECT_.equals(cust.getStatus())) {
				customers.add(cust);
			} else {
				sb.append("<li>").append(cust.getName()).append("</li>");
			}
		}
		sb.append("</ul>");
		if (!customers.isEmpty()) {
			custService.approveCustomers(customers);
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END approveList()");
		}
		String msg = sb.toString();
		if (msg.contains("<li>")) {
			throw new BusinessException(BusinessErrorCode.CUSTOMER_RPC_ACTION_ERROR,
					String.format(BusinessErrorCode.CUSTOMER_RPC_ACTION_ERROR.getErrMsg(), msg));
		}

	}

	/**
	 * @param customerDs
	 * @param param
	 * @throws Exception
	 */
	public void deactivate(Customers_Ds customerDs, Customers_DsParam param) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START deactivate()");
		}
		ICustomerService custService = (ICustomerService) this.findEntityService(Customer.class);
		Customer cust = custService.findById(customerDs.getId());
		if (CustomerStatus._ACTIVE_.equals(cust.getStatus()) || CustomerStatus._PROSPECT_.equals(cust.getStatus())) {
			custService.deactivateCustomers(cust, param.getRemark());
		} else {
			StringBuilder sb = new StringBuilder();
			sb.append("<ul><li>").append(cust.getName()).append("</li></ul>");
			throw new BusinessException(BusinessErrorCode.CUSTOMER_RPC_ACTION_ERROR,
					String.format(BusinessErrorCode.CUSTOMER_RPC_ACTION_ERROR.getErrMsg(), sb.toString()));

		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END deactivate()");
		}
	}

	/**
	 * @param customerDs
	 * @throws Exception
	 */
	public void generateCode(Customers_Ds customerDs) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START generateCode()");
		}
		IDocumentNumberSeriesService srv = (IDocumentNumberSeriesService) this.findEntityService(DocumentNumberSeries.class);
		customerDs.setCode(srv.getNextDocumentNumber(DocumentNumberSeriesType._CUSTOMER_));

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END generateCode()");
		}
	}

	/**
	 * @param custDsList
	 * @param param
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void exportFuelPlusToAzureCustomer(List<Customers_Ds> custDsList, Customers_DsParam param) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START exportFuelPlusToAzureCustomer()");
		}
		ICustomerService custService = (ICustomerService) this.findEntityService(Customer.class);

		// get the export option
		String option = param.getExportCustomerOption();

		List<Customer> customers = null;

		if (option.equals(EXPORT_OPTION_ALL)) {
			// get all the customers
			customers = custService.findEntitiesByAttributes(Collections.<String, Object> emptyMap());
			// filter the results
			this.filterCustomers(customers);
		} else if (option.equals(EXPORT_OPTION_FAILED)) {
			// find only the failed ones
			Map<String, Object> params = new HashMap<>();
			params.put(CUSTOMER_MODEL_PROPERY_TRANSMISSION_STATUS, CustomerDataTransmissionStatus._FAILED_);
			customers = custService.findEntitiesByAttributes(params);
		} else if (option.equals(EXPORT_OPTION_SELECTED)) {
			// find the ones that were selected
			customers = this.getCustomersForSelectedOption(custService, custDsList);
		} else {
			LOGGER.error("ERROR:could not read export option for " + option + " !");
			param.setExportCustomerResult(false);
			param.setExportCustomerDescription(BusinessErrorCode.INTERFACE_EXPORT_CUSTOMERS_INCORRECT_OPTION.getErrMsg());
		}

		// check to see if the customers were brought OK from DB (even if none)
		if (customers != null) {
			// check to see if there actually are some customers to export
			if (!customers.isEmpty()) {

				WSCustomerToDTOTransformer transformer = (WSCustomerToDTOTransformer) this.getApplicationContext().getBean("transformerCustomerData");
				IExternalInterfaceHistoryService historySrv = (IExternalInterfaceHistoryService) this
						.findEntityService(ExternalInterfaceHistory.class);
				IExternalInterfaceService extInterfaceSrv = (IExternalInterfaceService) this.findEntityService(ExternalInterface.class);
				ExternalInterfaceMessageHistoryFacade messageFacade = (ExternalInterfaceMessageHistoryFacade) this.getApplicationContext()
						.getBean("externalInterfaceMessageHistoryFacade");
				ExternalInterfaceHistoryFacade historyFacade = (ExternalInterfaceHistoryFacade) this.getApplicationContext()
						.getBean("externalInterfaceHistoryFacade");
				ExternalInterface extInterface = extInterfaceSrv.findByName(ExtInterfaceNames.EXPORT_CUSTOMERS_TO_NAV.getValue());
				ExtInterfaceParameters params = new ExtInterfaceParameters(extInterface.getParams());
				boolean isBulkMode = Boolean.parseBoolean(params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_BULK_MODE));
				WSExecutionResult resultCustomer = new WSExecutionResult();
				resultCustomer.setSuccess(true);
				params.put(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_DESTINATION_ADDRESS, "");
				params.put(ExternalInterfaceWSHelper.CAN_MODIFY_DESTINATION_ADDRESS, "FALSE");
				ThreadPoolTaskExecutor taskExecutor = (ThreadPoolTaskExecutor) this.getApplicationContext().getBean("taskExecutor");
				if (isBulkMode) {
					PumaRestClient<Customer> client = new PumaRestClient<>(taskExecutor, extInterface, params, customers, historyFacade, transformer,
							messageFacade);
					client.setCallbackSrv((IRestClientCallbackService<Customer>) this.findEntityService(Customer.class));
					resultCustomer = client.start(1);
					this.setCustomerDataStatusAfterExport(customers, CustomerDataTransmissionStatus._IN_PROGRESS_,
							CustomerDataProcessedStatus._IN_PROGRESS_);
				} else {
					resultCustomer = this.exportCustomer(customers, transformer, taskExecutor, historySrv, extInterface, params);
				}

				if (resultCustomer == null) {
					resultCustomer = new WSExecutionResult();
					resultCustomer.setSuccess(true);
				}

				resultCustomer.setExecutionData(String.format(BusinessErrorCode.INTERFACE_EXPORT_CUSTOMERS_START.getErrMsg(), extInterface.getName(),
						extInterface.getName()));
				param.setExportCustomerResult(resultCustomer.isSuccess());
				param.setExportCustomerDescription(resultCustomer.getExecutionData());

			} else {
				param.setExportCustomerResult(true);
				param.setExportCustomerDescription(BusinessErrorCode.INTERFACE_EXPORT_CUSTOMERS_NO_CUSTOMERS.getErrMsg());
			}

		} else {
			LOGGER.error("ERROR:could not find Customers for " + customers + " customers and option " + option + "!");
			param.setExportCustomerResult(false);
			param.setExportCustomerDescription(BusinessErrorCode.INTERFACE_EXPORT_CUSTOMERS_INCORRECT_OPTION.getErrMsg());
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END exportFuelPlusToAzureCustomer()");
		}
	}

	@SuppressWarnings("unchecked")
	private WSExecutionResult exportCustomer(List<Customer> customers, WSCustomerToDTOTransformer transformer, ThreadPoolTaskExecutor taskExecutor,
			IExternalInterfaceHistoryService historySrv, ExternalInterface extInterface, ExtInterfaceParameters params) throws Exception {
		WSExecutionResult resultCustomer = new WSExecutionResult();
		ExternalInterfaceMessageHistoryFacade messageFacade = (ExternalInterfaceMessageHistoryFacade) this.getApplicationContext()
				.getBean("externalInterfaceMessageHistoryFacade");
		ExternalInterfaceHistoryFacade historyFacade = (ExternalInterfaceHistoryFacade) this.getApplicationContext()
				.getBean("externalInterfaceHistoryFacade");

		resultCustomer.setSuccess(true);
		params.put(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_DESTINATION_ADDRESS, "");
		params.put(ExternalInterfaceWSHelper.CAN_MODIFY_DESTINATION_ADDRESS, "FALSE");
		for (Customer customer : customers) {
			PumaRestClient<Customer> client = new PumaRestClient<>(taskExecutor, extInterface, params, Arrays.asList(customer), historyFacade,
					transformer, messageFacade);
			client.setCallbackSrv((IRestClientCallbackService<Customer>) this.findEntityService(Customer.class));
			resultCustomer = client.start(5);
			this.setCustomerDataStatusAfterExport(Arrays.asList(customer), CustomerDataTransmissionStatus._IN_PROGRESS_,
					CustomerDataProcessedStatus._IN_PROGRESS_);
		}
		return resultCustomer;
	}

	public void requestCreditInfo(Customers_Ds customerDs) throws Exception {

		ICustomerService custService = (ICustomerService) this.findEntityService(Customer.class);
		custService.requestCreditStatus(custService.findById(customerDs.getId()));

	}

	/**
	 * Sets the status of the customers after thei were exported
	 *
	 * @param customers
	 * @param statusTransmission
	 * @param statusProcessed
	 * @throws Exception
	 */
	private void setCustomerDataStatusAfterExport(List<Customer> customers, CustomerDataTransmissionStatus statusTransmission,
			CustomerDataProcessedStatus statusProcessed) throws Exception {
		ICustomerService custService = (ICustomerService) this.findEntityService(Customer.class);
		// set the new statuses
		for (Customer customer : customers) {
			customer.setTransmissionStatus(statusTransmission);
			customer.setProcessedStatus(statusProcessed);
		}
		custService.update(customers);
	}

	/**
	 * @param custService
	 * @param custDsList
	 * @return
	 */
	private List<Customer> getCustomersForSelectedOption(ICustomerService custService, List<Customers_Ds> custDsList) {
		List<Customer> customers = null;
		if (custDsList != null && !custDsList.isEmpty()) {
			// get the selected ones from the DB
			List<Object> custIds = new ArrayList<>();
			for (Customers_Ds custDs : custDsList) {
				custIds.add(custDs.getId());
			}
			customers = custService.findByIds(custIds);
			// filter the customers
			this.filterCustomers(customers);
		} else {
			customers = new ArrayList<>();
		}
		return customers;
	}

	/**
	 * @param customers
	 */
	private void filterCustomers(List<Customer> customers) {
		// make sure to let only the ones that have a certain transmission
		// status
		for (Iterator<Customer> it = customers.iterator(); it.hasNext();) {
			Customer cust = it.next();
			if (!(cust.getTransmissionStatus().equals(CustomerDataTransmissionStatus._NOT_AVAILABLE_)
					|| cust.getTransmissionStatus().equals(CustomerDataTransmissionStatus._PROCESSED_)
					|| cust.getTransmissionStatus().equals(CustomerDataTransmissionStatus._FAILED_))) {
				it.remove();
			}
		}
	}

}
