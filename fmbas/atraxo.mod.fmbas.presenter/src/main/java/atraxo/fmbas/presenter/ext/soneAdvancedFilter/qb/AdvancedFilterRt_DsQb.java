/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.soneAdvancedFilter.qb;

import atraxo.fmbas.presenter.impl.soneAdvancedFilter.model.AdvancedFilterRt_Ds;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class AdvancedFilterRt_DsQb extends QueryBuilderWithJpql<AdvancedFilterRt_Ds, AdvancedFilterRt_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		String userCode = Session.user.get().getCode();
		this.addFilterCondition(" (e.createdBy = :userCode or e.availableSystemwide = true ) ");
		this.addCustomFilterItem("userCode", userCode);
	}

}
