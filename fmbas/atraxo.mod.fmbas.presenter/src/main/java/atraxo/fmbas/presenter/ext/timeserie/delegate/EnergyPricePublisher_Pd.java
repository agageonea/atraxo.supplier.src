package atraxo.fmbas.presenter.ext.timeserie.delegate;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.domain.impl.timeserie.CompositeTimeSeriesSource;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.presenter.impl.timeserie.model.EnergyPriceTimeSerie_Ds;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class EnergyPricePublisher_Pd extends AbstractPresenterDelegate {

	private static final String PUBLISH_TIME_SERIES_CHANNEL = "publishTimeSeriesChannel";
	private static final Logger LOG = LoggerFactory.getLogger(EnergyPricePublisher_Pd.class);

	public void publish(EnergyPriceTimeSerie_Ds data) throws Exception {
		ITimeSerieService service = (ITimeSerieService) this.findEntityService(TimeSerie.class);

		Integer tsId = data.getId();
		TimeSerie ts = service.findById(tsId);

		List<TimeSerie> tsList = new ArrayList<>();
		tsList.add(ts);

		service.publish(tsList);
		this.publishCalculated(tsList, service);

		this.sendMessage(PUBLISH_TIME_SERIES_CHANNEL, tsId);
	}

	public void publish(List<EnergyPriceTimeSerie_Ds> data) throws Exception {

		List<Object> tsIds = new ArrayList<>();

		for (EnergyPriceTimeSerie_Ds tsData : data) {
			tsIds.add(tsData.getId());
		}
		ITimeSerieService service = (ITimeSerieService) this.findEntityService(TimeSerie.class);
		List<TimeSerie> tsList = service.findByIds(tsIds);
		service.publish(tsList);

		this.publishCalculated(tsList, service);
		for (TimeSerie ts : tsList) {
			this.sendMessage(PUBLISH_TIME_SERIES_CHANNEL, ts.getId());
		}
	}

	private void publishCalculated(List<TimeSerie> tsList, ITimeSerieService service) throws BusinessException {
		for (TimeSerie ts : tsList) {
			if (!CollectionUtils.isEmpty(ts.getSourceTimeSeries())) {
				List<TimeSerie> tsListTemp = new ArrayList<>();
				for (CompositeTimeSeriesSource ctss : ts.getSourceTimeSeries()) {
					tsListTemp.add(service.findById(ctss.getComposite().getId()));
				}
				try {
					service.publish(tsListTemp);
				} catch (BusinessException e) {
					LOG.info("Composite time serie publish.", e);
				}
				for (TimeSerie tempTs : tsListTemp) {
					this.sendMessage(PUBLISH_TIME_SERIES_CHANNEL, tempTs.getId());
				}

			}
		}
	}

}
