/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.suppliers.model;

import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Suppliers.class, jpqlWhere = "e.isSupplier = true", sort = {@SortField(field = SupplierLov_Ds.F_CODE)})
public class SupplierLov_Ds extends AbstractLov_Ds<Suppliers> {

	public static final String ALIAS = "fmbas_SupplierLov_Ds";

	public static final String F_BUYERID = "buyerId";
	public static final String F_BUYERNAME = "buyerName";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_IATACODE = "iataCode";
	public static final String F_IPLAGENT = "iplAgent";
	public static final String F_FUELSUPPLIER = "fuelSupplier";
	public static final String F_FIXEDBASEOPERATOR = "fixedBaseOperator";
	public static final String F_ISSUPPLIER = "isSupplier";

	@DsField(join = "left", path = "responsibleBuyer.id")
	private String buyerId;

	@DsField(join = "left", path = "responsibleBuyer.name")
	private String buyerName;

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private String iataCode;

	@DsField
	private Boolean iplAgent;

	@DsField
	private Boolean fuelSupplier;

	@DsField
	private Boolean fixedBaseOperator;

	@DsField
	private Boolean isSupplier;

	/**
	 * Default constructor
	 */
	public SupplierLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public SupplierLov_Ds(Suppliers e) {
		super(e);
	}

	public String getBuyerId() {
		return this.buyerId;
	}

	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}

	public String getBuyerName() {
		return this.buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public Boolean getIplAgent() {
		return this.iplAgent;
	}

	public void setIplAgent(Boolean iplAgent) {
		this.iplAgent = iplAgent;
	}

	public Boolean getFuelSupplier() {
		return this.fuelSupplier;
	}

	public void setFuelSupplier(Boolean fuelSupplier) {
		this.fuelSupplier = fuelSupplier;
	}

	public Boolean getFixedBaseOperator() {
		return this.fixedBaseOperator;
	}

	public void setFixedBaseOperator(Boolean fixedBaseOperator) {
		this.fixedBaseOperator = fixedBaseOperator;
	}

	public Boolean getIsSupplier() {
		return this.isSupplier;
	}

	public void setIsSupplier(Boolean isSupplier) {
		this.isSupplier = isSupplier;
	}
}
