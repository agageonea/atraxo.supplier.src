/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.soneDefaultViewState.model;

import atraxo.ad.domain.impl.security.User;
import atraxo.fmbas.domain.impl.soneDefaultViewState.SoneDefaultViewState;
import atraxo.fmbas.domain.impl.soneViewState.SoneViewState;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = SoneDefaultViewState.class)
@RefLookups({
		@RefLookup(refId = DefaultViewState_Ds.F_VIEWID, namedQuery = SoneViewState.NQ_FIND_BY_ID, params = {@Param(name = "id", field = DefaultViewState_Ds.F_VIEWID)}),
		@RefLookup(refId = DefaultViewState_Ds.F_USERID, namedQuery = User.NQ_FIND_BY_LOGIN, params = {@Param(name = "loginName", field = DefaultViewState_Ds.F_LOGINNAME)}),
		@RefLookup(refId = DefaultViewState_Ds.F_USERID, namedQuery = User.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = DefaultViewState_Ds.F_USERCODE)})})
public class DefaultViewState_Ds extends AbstractDs_Ds<SoneDefaultViewState> {

	public static final String ALIAS = "fmbas_DefaultViewState_Ds";

	public static final String F_ADVANCEDFILTERID = "advancedFilterId";
	public static final String F_ADVANCEDFILTERNAME = "advancedFilterName";
	public static final String F_STANDARDFILTERVALUE = "standardFilterValue";
	public static final String F_ADVANCEDFILTERVALUE = "advancedFilterValue";
	public static final String F_DEFAULTFILTER = "defaultFilter";
	public static final String F_USERID = "userId";
	public static final String F_USERNAME = "userName";
	public static final String F_LOGINNAME = "loginName";
	public static final String F_USERCODE = "userCode";
	public static final String F_VIEWID = "viewId";
	public static final String F_NAME = "name";
	public static final String F_CMP = "cmp";
	public static final String F_CMPTYPE = "cmpType";
	public static final String F_VALUE = "value";
	public static final String F_PAGESIZE = "pageSize";
	public static final String F_TOTALS = "totals";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_CURRENCYCODE = "currencyCode";
	public static final String F_ISSTANDARD = "isStandard";
	public static final String F_AVAILABLESYSTEMWIDE = "availableSystemwide";
	public static final String F_ACTIVE = "active";

	@DsField(join = "left", path = "view.advancedFilter.id")
	private Integer advancedFilterId;

	@DsField(join = "left", path = "view.advancedFilter.name")
	private String advancedFilterName;

	@DsField(join = "left", path = "view.advancedFilter.standardFilterValue")
	private String standardFilterValue;

	@DsField(join = "left", path = "view.advancedFilter.advancedFilterValue")
	private String advancedFilterValue;

	@DsField(join = "left", path = "view.advancedFilter.defaultFilter")
	private Boolean defaultFilter;

	@DsField(join = "left", path = "user.id")
	private String userId;

	@DsField(join = "left", path = "user.name")
	private String userName;

	@DsField(join = "left", path = "user.loginName")
	private String loginName;

	@DsField(join = "left", path = "user.code")
	private String userCode;

	@DsField(join = "left", path = "view.id")
	private Integer viewId;

	@DsField(join = "left", path = "view.name")
	private String name;

	@DsField(join = "left", path = "view.cmp")
	private String cmp;

	@DsField(join = "left", path = "view.cmpType")
	private String cmpType;

	@DsField(join = "left", path = "view.value")
	private String value;

	@DsField(join = "left", path = "view.pageSize")
	private Integer pageSize;

	@DsField(join = "left", path = "view.totals")
	private Boolean totals;

	@DsField(join = "left", path = "view.unitCode")
	private String unitCode;

	@DsField(join = "left", path = "view.currencyCode")
	private String currencyCode;

	@DsField(join = "left", path = "view.isStandard")
	private Boolean isStandard;

	@DsField(join = "left", path = "view.availableSystemwide")
	private Boolean availableSystemwide;

	@DsField(join = "left", path = "view.active")
	private Boolean active;

	/**
	 * Default constructor
	 */
	public DefaultViewState_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DefaultViewState_Ds(SoneDefaultViewState e) {
		super(e);
	}

	public Integer getAdvancedFilterId() {
		return this.advancedFilterId;
	}

	public void setAdvancedFilterId(Integer advancedFilterId) {
		this.advancedFilterId = advancedFilterId;
	}

	public String getAdvancedFilterName() {
		return this.advancedFilterName;
	}

	public void setAdvancedFilterName(String advancedFilterName) {
		this.advancedFilterName = advancedFilterName;
	}

	public String getStandardFilterValue() {
		return this.standardFilterValue;
	}

	public void setStandardFilterValue(String standardFilterValue) {
		this.standardFilterValue = standardFilterValue;
	}

	public String getAdvancedFilterValue() {
		return this.advancedFilterValue;
	}

	public void setAdvancedFilterValue(String advancedFilterValue) {
		this.advancedFilterValue = advancedFilterValue;
	}

	public Boolean getDefaultFilter() {
		return this.defaultFilter;
	}

	public void setDefaultFilter(Boolean defaultFilter) {
		this.defaultFilter = defaultFilter;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getUserCode() {
		return this.userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Integer getViewId() {
		return this.viewId;
	}

	public void setViewId(Integer viewId) {
		this.viewId = viewId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCmp() {
		return this.cmp;
	}

	public void setCmp(String cmp) {
		this.cmp = cmp;
	}

	public String getCmpType() {
		return this.cmpType;
	}

	public void setCmpType(String cmpType) {
		this.cmpType = cmpType;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getPageSize() {
		return this.pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Boolean getTotals() {
		return this.totals;
	}

	public void setTotals(Boolean totals) {
		this.totals = totals;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Boolean getIsStandard() {
		return this.isStandard;
	}

	public void setIsStandard(Boolean isStandard) {
		this.isStandard = isStandard;
	}

	public Boolean getAvailableSystemwide() {
		return this.availableSystemwide;
	}

	public void setAvailableSystemwide(Boolean availableSystemwide) {
		this.availableSystemwide = availableSystemwide;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
