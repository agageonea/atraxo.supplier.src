/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.fmbas.presenter.ext.customer.qb;

import java.util.List;

import atraxo.fmbas.presenter.impl.customer.model.CustomerGroupLov_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

/**
 * Custom Query Builder CustomerGroupLov_DsQb
 */
public class CustomerGroupLov_DsQb extends QueryBuilderWithJpql<CustomerGroupLov_Ds, CustomerGroupLov_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();

		Integer customerId = this.filter.getId() == null ? null : Integer.valueOf(this.filter.getId().intValue());
		Integer parentId = this.filter.getParentGroupId() == null ? null : Integer.valueOf(this.filter.getParentGroupId().intValue());
		Integer finalId = parentId == null ? customerId : parentId;
		List list = this.getEntityManager()
				.createNativeQuery("select id from BAS_COMPANIES e where e.id =" + finalId + " or e.parent_group_id=" + finalId + ";")
				.getResultList();
		StringBuilder sb = new StringBuilder();
		for (Object obj : list) {
			sb.append((sb.toString().isEmpty() ? " " : ",") + obj + " ");
		}
		this.addFilterCondition(sb.toString().isEmpty() ? "" : "e.id in (" + sb.toString() + ")");
		this.filter.setId(null);
		this.filter.setParentGroupId(null);
	}
}
