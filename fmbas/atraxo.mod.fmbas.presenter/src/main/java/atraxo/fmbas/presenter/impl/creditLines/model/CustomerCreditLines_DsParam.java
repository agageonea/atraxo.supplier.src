/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.creditLines.model;

/**
 * Generated code. Do not modify in this file.
 */
public class CustomerCreditLines_DsParam {

	public static final String f_updateReason = "updateReason";

	private String updateReason;

	public String getUpdateReason() {
		return this.updateReason;
	}

	public void setUpdateReason(String updateReason) {
		this.updateReason = updateReason;
	}
}
