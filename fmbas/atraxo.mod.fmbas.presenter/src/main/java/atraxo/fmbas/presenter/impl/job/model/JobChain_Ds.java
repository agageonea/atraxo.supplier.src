/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.job.model;

import atraxo.fmbas.domain.impl.fmbas_type.JobRunResult;
import atraxo.fmbas.domain.impl.fmbas_type.JobStatus;
import atraxo.fmbas.domain.impl.job.JobChain;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = JobChain.class)
public class JobChain_Ds extends AbstractDs_Ds<JobChain> {

	public static final String ALIAS = "fmbas_JobChain_Ds";

	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_STATUS = "status";
	public static final String F_NEXTRUNTIME = "nextRunTime";
	public static final String F_LASTRUNTIME = "lastRunTime";
	public static final String F_LASTRUNRESULT = "lastRunResult";
	public static final String F_ENABLED = "enabled";
	public static final String F_PARAMIDENTIFIER = "paramIdentifier";
	public static final String F_TRIGERS = "trigers";

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private JobStatus status;

	@DsField
	private Date nextRunTime;

	@DsField
	private Date lastRunTime;

	@DsField
	private JobRunResult lastRunResult;

	@DsField
	private Boolean enabled;

	@DsField(fetch = false)
	private String paramIdentifier;

	@DsField(fetch = false)
	private String trigers;

	/**
	 * Default constructor
	 */
	public JobChain_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public JobChain_Ds(JobChain e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public JobStatus getStatus() {
		return this.status;
	}

	public void setStatus(JobStatus status) {
		this.status = status;
	}

	public Date getNextRunTime() {
		return this.nextRunTime;
	}

	public void setNextRunTime(Date nextRunTime) {
		this.nextRunTime = nextRunTime;
	}

	public Date getLastRunTime() {
		return this.lastRunTime;
	}

	public void setLastRunTime(Date lastRunTime) {
		this.lastRunTime = lastRunTime;
	}

	public JobRunResult getLastRunResult() {
		return this.lastRunResult;
	}

	public void setLastRunResult(JobRunResult lastRunResult) {
		this.lastRunResult = lastRunResult;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getParamIdentifier() {
		return this.paramIdentifier;
	}

	public void setParamIdentifier(String paramIdentifier) {
		this.paramIdentifier = paramIdentifier;
	}

	public String getTrigers() {
		return this.trigers;
	}

	public void setTrigers(String trigers) {
		this.trigers = trigers;
	}
}
