/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.externalInterfaces.model;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceHistory;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfacesHistoryStatus;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ExternalInterfaceHistory.class, sort = {@SortField(field = ExternalInterfacesHistory_Ds.F_RESPONSESENT, desc = true)})
@RefLookups({@RefLookup(refId = ExternalInterfacesHistory_Ds.F_EXTERNALINTERFACEID)})
public class ExternalInterfacesHistory_Ds
		extends
			AbstractDs_Ds<ExternalInterfaceHistory> {

	public static final String ALIAS = "fmbas_ExternalInterfacesHistory_Ds";

	public static final String F_EXTERNALINTERFACEID = "externalInterfaceId";
	public static final String F_EXTERNALINTERFACENAME = "externalInterfaceName";
	public static final String F_REQUESTRECEIVED = "requestReceived";
	public static final String F_RESPONSESENT = "responseSent";
	public static final String F_STATUS = "status";
	public static final String F_REQUESTER = "requester";
	public static final String F_RESULT = "result";

	@DsField(join = "left", path = "externalInterface.id")
	private Integer externalInterfaceId;

	@DsField(join = "left", path = "externalInterface.name")
	private String externalInterfaceName;

	@DsField
	private Date requestReceived;

	@DsField
	private Date responseSent;

	@DsField
	private ExternalInterfacesHistoryStatus status;

	@DsField
	private String requester;

	@DsField
	private String result;

	/**
	 * Default constructor
	 */
	public ExternalInterfacesHistory_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ExternalInterfacesHistory_Ds(ExternalInterfaceHistory e) {
		super(e);
	}

	public Integer getExternalInterfaceId() {
		return this.externalInterfaceId;
	}

	public void setExternalInterfaceId(Integer externalInterfaceId) {
		this.externalInterfaceId = externalInterfaceId;
	}

	public String getExternalInterfaceName() {
		return this.externalInterfaceName;
	}

	public void setExternalInterfaceName(String externalInterfaceName) {
		this.externalInterfaceName = externalInterfaceName;
	}

	public Date getRequestReceived() {
		return this.requestReceived;
	}

	public void setRequestReceived(Date requestReceived) {
		this.requestReceived = requestReceived;
	}

	public Date getResponseSent() {
		return this.responseSent;
	}

	public void setResponseSent(Date responseSent) {
		this.responseSent = responseSent;
	}

	public ExternalInterfacesHistoryStatus getStatus() {
		return this.status;
	}

	public void setStatus(ExternalInterfacesHistoryStatus status) {
		this.status = status;
	}

	public String getRequester() {
		return this.requester;
	}

	public void setRequester(String requester) {
		this.requester = requester;
	}

	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
