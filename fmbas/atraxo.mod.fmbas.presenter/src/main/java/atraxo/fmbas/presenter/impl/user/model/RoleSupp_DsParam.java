/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.user.model;

/**
 * Generated code. Do not modify in this file.
 */
public class RoleSupp_DsParam {

	public static final String f_withUser = "withUser";
	public static final String f_withUserId = "withUserId";
	public static final String f_withPrivilege = "withPrivilege";
	public static final String f_withPrivilegeId = "withPrivilegeId";

	private String withUser;

	private String withUserId;

	private String withPrivilege;

	private String withPrivilegeId;

	public String getWithUser() {
		return this.withUser;
	}

	public void setWithUser(String withUser) {
		this.withUser = withUser;
	}

	public String getWithUserId() {
		return this.withUserId;
	}

	public void setWithUserId(String withUserId) {
		this.withUserId = withUserId;
	}

	public String getWithPrivilege() {
		return this.withPrivilege;
	}

	public void setWithPrivilege(String withPrivilege) {
		this.withPrivilege = withPrivilege;
	}

	public String getWithPrivilegeId() {
		return this.withPrivilegeId;
	}

	public void setWithPrivilegeId(String withPrivilegeId) {
		this.withPrivilegeId = withPrivilegeId;
	}
}
