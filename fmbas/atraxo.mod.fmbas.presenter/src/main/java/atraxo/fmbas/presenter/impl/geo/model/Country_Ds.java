/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.geo.model;

import atraxo.fmbas.domain.impl.fmbas_type.Continents;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Country.class, sort = {@SortField(field = Country_Ds.F_NAME)})
@RefLookups({@RefLookup(refId = Country_Ds.F_COUNTRYID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Country_Ds.F_COUNTRYCODE)})})
public class Country_Ds extends AbstractDs_Ds<Country> {

	public static final String ALIAS = "fmbas_Country_Ds";

	public static final String F_COUNTRYID = "countryId";
	public static final String F_COUNTRYCODE = "countryCode";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_CONTINENT = "continent";
	public static final String F_ACTIVE = "active";
	public static final String F_TEXT = "text";

	@DsField(join = "left", path = "country.id")
	private Integer countryId;

	@DsField(join = "left", path = "country.code")
	private String countryCode;

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private Continents continent;

	@DsField
	private Boolean active;

	@DsField(fetch = false)
	private String text;

	/**
	 * Default constructor
	 */
	public Country_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Country_Ds(Country e) {
		super(e);
	}

	public Integer getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Continents getContinent() {
		return this.continent;
	}

	public void setContinent(Continents continent) {
		this.continent = continent;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
