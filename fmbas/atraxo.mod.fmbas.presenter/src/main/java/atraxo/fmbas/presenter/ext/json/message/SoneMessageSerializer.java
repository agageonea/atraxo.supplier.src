package atraxo.fmbas.presenter.ext.json.message;

import java.io.IOException;
import java.util.Locale;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;

import atraxo.fmbas.domain.ext.json.message.SoneMessage;
import seava.j4e.api.session.Session;

public class SoneMessageSerializer extends JsonSerializer<SoneMessage> implements MessageSourceAware {

	private MessageSource messageSource;

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;

	}

	@Override
	public void serialize(SoneMessage value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
		String msg = this.messageSource.getMessage(Integer.toString(value.getKey()), null, value.getMessage(), new Locale(Session.user.get()
				.getSettings().getLanguage()));
		jgen.writeString(value.getKey() + "||" + msg);
	}

}
