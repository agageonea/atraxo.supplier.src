package atraxo.fmbas.presenter.ext.externalInterfaces.delegate;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.presenter.impl.externalInterfaces.model.ExternalInterfaces_Ds;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class ExternalInterface_Pd extends AbstractPresenterDelegate {

	public void setEnable(ExternalInterfaces_Ds ds) throws Exception {
		IExternalInterfaceService service = (IExternalInterfaceService) this.findEntityService(ExternalInterface.class);
		service.setEnableDissableStatus(ds.getId(), true);
	}

	public void setDissable(ExternalInterfaces_Ds ds) throws Exception {
		IExternalInterfaceService service = (IExternalInterfaceService) this.findEntityService(ExternalInterface.class);
		service.setEnableDissableStatus(ds.getId(), false);
	}
}
