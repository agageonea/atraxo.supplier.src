/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.workflowTask.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.activiti.engine.TaskService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.user.IRoleSuppService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceService;
import atraxo.fmbas.business.ext.bpm.WorkflowEntities;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.domain.impl.abstracts.AbstractSubsidiary;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowPeriodGroup;
import atraxo.fmbas.domain.impl.user.RoleSupp;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import atraxo.fmbas.domain.impl.workflowTask.WorkflowTask;
import atraxo.fmbas.presenter.impl.workflowTask.model.WorkflowTask_Ds;
import atraxo.fmbas.presenter.impl.workflowTask.model.WorkflowTask_DsParam;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.service.business.IEntityService;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.api.session.Session;
import seava.j4e.business.bpm.ActivitiBpmService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service WorkflowTask_DsService
 */
public class WorkflowTask_DsService extends AbstractEntityDsService<WorkflowTask_Ds, WorkflowTask_Ds, WorkflowTask_DsParam, WorkflowTask>
		implements IDsService<WorkflowTask_Ds, WorkflowTask_Ds, WorkflowTask_DsParam> {

	private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowTask_DsService.class);

	@Autowired
	private ActivitiBpmService activitiBpmService;
	@Autowired
	private IRoleSuppService roleService;
	@Autowired
	private IUserSuppService userService;
	@Autowired
	private IWorkflowInstanceService wkfInstanceService;

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.presenter.service.ds.AbstractEntityDsReadService#postFind(seava.j4e.api.action.query.IQueryBuilder, java.util.List)
	 */
	@Override
	protected void postFind(IQueryBuilder<WorkflowTask_Ds, WorkflowTask_Ds, WorkflowTask_DsParam> builder, List<WorkflowTask_Ds> result)
			throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START postFind()");
		}

		String username = Session.user.get().getLoginName();
		if (username != null) {

			// add user candidates
			TaskService taskService = this.activitiBpmService.getTaskService();
			List<Task> candidateTasksUser = taskService.createTaskQuery().taskTenantId(Session.user.get().getClientId()).taskCandidateUser(username)
					.list();

			// add role candidates
			UserSupp user = this.userService.findByLogin(username);
			List<String> rolesCodes = new ArrayList<>();
			List<RoleSupp> roles = this.roleService.findByUsers(user);
			for (RoleSupp role : roles) {
				rolesCodes.add(role.getCode());
			}
			List<Task> candidateTasksGroup = taskService.createTaskQuery().taskTenantId(Session.user.get().getClientId())
					.taskCandidateGroupIn(rolesCodes).list();

			// set the candidates, so that the task can be seen by candidates
			List<Task> allTasks = new ArrayList<>();
			allTasks.addAll(candidateTasksUser);
			allTasks.addAll(candidateTasksGroup);

			Set<String> subsidiariesSet = this.getSubsidiariesForUser(user);

			for (Task task : allTasks) {
				WorkflowInstance instance = this.getWorkflowInstanceForTask(task);

				if (instance != null) {
					boolean taskSubsidiaryOk = this.isTaskOkForSubsidiary(task, subsidiariesSet, instance);
					if (taskSubsidiaryOk) {
						WorkflowTask_Ds ds = this.toWkfTaskDs(task, instance);
						ds.setCandidate(username);
						result.add(ds);
					}
				}
			}

			// add the assigness as well
			List<Task> assigneeTasks = taskService.createTaskQuery().taskAssignee(username).list();
			for (Task task : assigneeTasks) {
				WorkflowInstance instance = this.getWorkflowInstanceForTask(task);
				if (instance != null) {
					boolean taskSubsidiaryOk = this.isTaskOkForSubsidiary(task, subsidiariesSet, instance);
					if (taskSubsidiaryOk) {
						WorkflowTask_Ds ds = this.toWkfTaskDs(task, instance);
						result.add(ds);
					}
				}
			}

			// order result
			result.sort((o1, o2) -> o1.getOwner().compareTo(o2.getOwner()));

		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END postFind()");
		}
	}

	/**
	 * Checks if a specific <code>Task</code> should be seen by the user with a <code>Set</code> of subsidiariy ids; this algorithm gets the
	 * <code>ProcessInstance</code>, then the <code>WorkflowInstance</code>, then the <code>Workflow</code> and checks the subsidiaries; if this
	 * doesn't work then, basen on the <code>WorkflowInstance</code>, all the <code>WorkflowInstanceEntity</code>es are brought and are all checked
	 * for subsidiaries
	 *
	 * @param task
	 * @param subsidiariesSet
	 * @param wkfInstance
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private boolean isTaskOkForSubsidiary(Task task, Set<String> subsidiariesSet, WorkflowInstance wkfInstance) {
		boolean taskSubsidiaryOk = false;

		// first, attempt to compare the subsidiary of the workflow with the subsidiaries of the user
		if ((wkfInstance.getWorkflow() != null) && (wkfInstance.getWorkflow().getSubsidiary() != null)) {
			String refIDFromWorkflowSubsidiary = wkfInstance.getWorkflow().getSubsidiary().getRefid();
			if (subsidiariesSet.contains(refIDFromWorkflowSubsidiary)) {
				taskSubsidiaryOk = true;
			}
		}

		// if subsidiary is not found it means the workflow changed its subsidiary, or was deleted, or something, so we go to find the entities
		// used by the workflow and get their subsidiaries
		if (!taskSubsidiaryOk) {
			Collection<WorkflowInstanceEntity> entities = wkfInstance.getEntitites();
			if (!entities.isEmpty()) {
				int subsOk = 0;
				for (WorkflowInstanceEntity ent : entities) {
					IEntityService service = (IEntityService) this.getApplicationContext().getBean(ent.getObjectType());
					Object object = service.findById(ent.getObjectId());

					if (object != null) {
						if (object instanceof AbstractSubsidiary) {
							AbstractSubsidiary abstractObjectSub = (AbstractSubsidiary) object;
							String subsidiaryId = abstractObjectSub.getSubsidiaryId();
							if (subsidiariesSet.contains(subsidiaryId)) {
								subsOk++;
							}
						} else {
							// assume that it's ok, because if no subsidiary is provided, then it means it's available for everybody
							subsOk++;
						}
					}

				}
				if (subsOk == entities.size()) {
					taskSubsidiaryOk = true;
				}
			}
		}

		return taskSubsidiaryOk;
	}

	/**
	 * @param user
	 * @return
	 */
	private Set<String> getSubsidiariesForUser(UserSupp user) {
		Set<String> userSubIdSet = new HashSet<>();
		for (UserSubsidiary userSub : user.getSubsidiaries()) {
			userSubIdSet.add(userSub.getSubsidiary().getRefid());
		}
		return userSubIdSet;
	}

	/**
	 * @param task
	 * @param activitiBpmService
	 * @param serv
	 * @return
	 */
	private WorkflowInstance getWorkflowInstanceForTask(Task task) {
		WorkflowInstance instance;
		ProcessInstance processInstance = this.activitiBpmService.getRuntimeService().createProcessInstanceQuery()
				.processInstanceId(task.getProcessInstanceId()).singleResult();
		if (processInstance != null) {
			// attempt to set the owner and description based on the workflow instance
			String bussinessKey = processInstance.getBusinessKey();
			if (!StringUtils.isEmpty(bussinessKey)) {
				Integer id = null;
				try {
					id = Integer.parseInt(bussinessKey);
				} catch (NumberFormatException e1) {
					LOGGER.warn("WARNING:the workflow instance id is " + bussinessKey, e1);
					return null;
				}
				try {
					instance = this.wkfInstanceService.findByBusiness(id);
				} catch (ApplicationException e) {
					LOGGER.warn("WARNING:could not find workflow instance for id " + bussinessKey + " and the current client id !", e);
					return null;
				}
			} else {
				LOGGER.warn("WARNING:the workflow instance id is %s.", bussinessKey);
				return null;
			}
		} else {
			LOGGER.warn("WARNING:could not find process instance for id " + task.getProcessInstanceId());
			return null;
		}
		return instance;
	}

	/**
	 * @param task
	 * @param activitiBpmService
	 * @param instance
	 * @return
	 */
	private WorkflowTask_Ds toWkfTaskDs(Task task, WorkflowInstance wkfInstance) {
		WorkflowTask_Ds ds = new WorkflowTask_Ds();
		ds.setOwner(wkfInstance.getWorkflow().getName() + "::" + wkfInstance.getName());
		ds.setWorkflowInstanceId(wkfInstance.getId());
		ds.setDescription(wkfInstance.getDescription());

		// set the process based on the process definition
		String processDefId = task.getProcessDefinitionId();
		List<ProcessDefinition> lstProcessDefinitions = this.activitiBpmService.getRepositoryService().createProcessDefinitionQuery()
				.processDefinitionId(processDefId).list();
		if (!lstProcessDefinitions.isEmpty()) {
			ProcessDefinition processDef = lstProcessDefinitions.get(0);
			ds.setProcess(processDef.getName());
		}

		// set the rest of the properties
		ds.setTaskId(task.getId());
		ds.setName(task.getName());
		ds.setAssignee(task.getAssignee());
		ds.setCreatedAt(task.getCreateTime());
		UserSupp user = this.userService.findByCode(wkfInstance.getCreatedBy());
		TaskService taskService = this.activitiBpmService.getTaskService();
		String taskInfo = this.getTaskInformation(task, user, taskService).replaceAll("\r\n", "</br>").replace("\n", "</br>").replaceAll("'",
				"&quot;");
		ds.setDetail(taskInfo);
		Map<String, Object> variables = this.getTaskVariables(task, taskService);
		if (variables.containsKey(WorkflowVariablesConstants.TASK_EMAIL_SUBJECT)) {
			ds.setSubject(variables.get(WorkflowVariablesConstants.TASK_EMAIL_SUBJECT).toString());
		} else {
			ds.setSubject(task.getDescription() != null ? task.getDescription() : task.getName());
		}
		ds.setCreatedBy(this.getFullUserName(user));
		ds.setGroup(wkfInstance.getWorkflow().getWorkflowGroup().getName());
		this.setEntityInformation(taskService, task.getId(), ds, wkfInstance.getWorkflow().getName());

		Date today = GregorianCalendar.getInstance().getTime();
		if (DateUtils.isSameDay(today, task.getCreateTime())) {
			ds.setPeriodGroup(WorkflowPeriodGroup._TODAY_.getName());
		} else {
			Date yesterday = DateUtils.addDays(today, -1);
			if (DateUtils.isSameDay(yesterday, task.getCreateTime())) {
				ds.setPeriodGroup(WorkflowPeriodGroup._YESTERDAY_.getName());
			} else {
				ds.setPeriodGroup(WorkflowPeriodGroup._OLDER_.getName());
			}
		}

		return ds;
	}

	/**
	 * @param taskId
	 * @param taskService
	 * @return
	 */
	private Map<String, Object> getTaskVariables(String taskId, TaskService taskService) {
		Map<String, Object> variables = new HashMap<>();
		try {
			variables = taskService.getVariables(taskId);
		} catch (Exception e) {
			LOGGER.warn(
					"WARNING:Activiti could not extract variables from Task, most likely because the EmailDto changed and can not be deserialized! Will use EMPTY variables MAP!",
					e);
		}
		return variables;
	}

	private Map<String, Object> getTaskVariables(Task task, TaskService taskService) {
		return this.getTaskVariables(task.getId(), taskService);
	}

	private void setEntityInformation(TaskService taskService, String taskId, WorkflowTask_Ds ds, String wkfName) {
		String id = "-1";
		Map<String, Object> variables = this.getTaskVariables(taskId, taskService);
		if (variables.containsKey(WorkflowVariablesConstants.BID_ID_VAR)) {
			id = variables.get(WorkflowVariablesConstants.BID_ID_VAR).toString();
			ds.setEntityFrame(WorkflowEntities.BID.getFrame());
			ds.setEntityModule(WorkflowEntities.BID.getModule());
		}

		if (variables.containsKey(WorkflowVariablesConstants.CONTRACT_ID_VAR)) {
			id = variables.get(WorkflowVariablesConstants.CONTRACT_ID_VAR).toString();
			if (wkfName.startsWith(WorkflowNames.PURCHASE_CONTRACT_APPROVAL.getWorkflowName())) {
				ds.setEntityFrame(WorkflowEntities.PURCHASE_CONTRACT.getFrame());
				ds.setEntityModule(WorkflowEntities.PURCHASE_CONTRACT.getModule());
			} else {
				ds.setEntityFrame(WorkflowEntities.SELL_CONTRACT.getFrame());
				ds.setEntityModule(WorkflowEntities.SELL_CONTRACT.getModule());
			}
		}

		if (variables.containsKey(WorkflowVariablesConstants.INVOICE_ID_VAR)) {
			id = variables.get(WorkflowVariablesConstants.INVOICE_ID_VAR).toString();
			ds.setEntityFrame(WorkflowEntities.INVOICE.getFrame());
			ds.setEntityModule(WorkflowEntities.INVOICE.getModule());
		}
		if (variables.containsKey(WorkflowVariablesConstants.PRICE_UPDATE_ID_VAR)) {
			id = variables.get(WorkflowVariablesConstants.PRICE_UPDATE_ID_VAR).toString();
			ds.setEntityFrame(WorkflowEntities.PRICE_UPDATE.getFrame());
			ds.setEntityModule(WorkflowEntities.PRICE_UPDATE.getModule());
		}
		if (variables.containsKey(WorkflowVariablesConstants.FUEL_TICKET_ID_VAR)) {
			id = variables.get(WorkflowVariablesConstants.FUEL_TICKET_ID_VAR).toString();
			ds.setEntityFrame(WorkflowEntities.FUEL_TICKET.getFrame());
			ds.setEntityModule(WorkflowEntities.FUEL_TICKET.getModule());
		}
		if (variables.containsKey(WorkflowVariablesConstants.TIME_SERIE_TYPE)) {
			id = variables.get(WorkflowVariablesConstants.TIME_SERIE_ID).toString();
			if ("X".equalsIgnoreCase(variables.get(WorkflowVariablesConstants.TIME_SERIE_TYPE).toString())) {
				ds.setEntityFrame(WorkflowEntities.EXCHANGE_RATE.getFrame());
				ds.setEntityModule(WorkflowEntities.EXCHANGE_RATE.getModule());
			} else {
				ds.setEntityFrame(WorkflowEntities.ENERGY_PRICE.getFrame());
				ds.setEntityModule(WorkflowEntities.ENERGY_PRICE.getModule());
			}
		}
		try {
			ds.setEntityId(Integer.parseInt(id));
		} catch (NumberFormatException nfe) {
			if (LOGGER.isTraceEnabled()) {
				LOGGER.trace(nfe.getMessage(), nfe);
			}
			ds.setEntityId(-1);
		}
	}

	private String getTaskInformation(Task task, UserSupp user, TaskService taskService) {
		Map<String, Object> variables = this.getTaskVariables(task, taskService);
		if (variables.containsKey(WorkflowVariablesConstants.TASK_EMAILS)) {
			@SuppressWarnings("unchecked")
			Map<String, String> emailMap = (Map<String, String>) variables.get(WorkflowVariablesConstants.TASK_EMAILS);
			if (emailMap.containsKey(user.getEmail())) {
				return emailMap.get(user.getEmail());
			}
		}
		return StringUtils.EMPTY;
	}

	private String getFullUserName(UserSupp user) {
		return user.getFirstName() + " " + user.getLastName();
	}

}
