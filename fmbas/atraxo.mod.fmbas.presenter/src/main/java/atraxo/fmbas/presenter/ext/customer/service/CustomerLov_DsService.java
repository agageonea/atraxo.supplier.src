/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.customer.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.presenter.impl.customer.model.CustomerLov_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class CustomerLov_DsService extends AbstractEntityDsService<CustomerLov_Ds, CustomerLov_Ds, Object, Customer>
		implements IDsService<CustomerLov_Ds, CustomerLov_Ds, Object> {

	private static final Logger LOG = LoggerFactory.getLogger(CustomerLov_DsService.class);

	@Override
	protected void postFind(IQueryBuilder<CustomerLov_Ds, CustomerLov_Ds, Object> builder, List<CustomerLov_Ds> result) throws Exception {
		super.postFind(builder, result);
		this.setPrimaryContact(result);
	}

	private void setPrimaryContact(List<CustomerLov_Ds> result) throws Exception {
		ICustomerService service = (ICustomerService) this.findEntityService(Customer.class);
		for (CustomerLov_Ds ds : result) {
			Customer customer = ds._getEntity_();
			try {
				Contacts primaryContact = service.getPrimaryContact(customer);
				ds.setPrimaryContactId(primaryContact.getId());
				ds.setPrimaryContactName(primaryContact.getFirstName() + " " + primaryContact.getLastName());
			} catch (BusinessException e) {
				if (LOG.isDebugEnabled()) {
					LOG.debug(e.getMessage(), e);
				}
				LOG.info("Primary contact not found!");
			}
		}
	}
}