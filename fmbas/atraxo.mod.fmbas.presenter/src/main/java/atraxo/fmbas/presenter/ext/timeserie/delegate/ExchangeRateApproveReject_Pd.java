package atraxo.fmbas.presenter.ext.timeserie.delegate;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.presenter.ext.timeserie.service.ExchangeRateApproveRejectService;
import atraxo.fmbas.presenter.impl.timeserie.model.ExchangeRateTimeSerie_Ds;
import atraxo.fmbas.presenter.impl.timeserie.model.ExchangeRateTimeSerie_DsParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class ExchangeRateApproveReject_Pd extends AbstractPresenterDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExchangeRateApproveReject_Pd.class);

	/**
	 * Submits for approval an Exchange rate time series
	 *
	 * @param ds
	 * @param params
	 * @throws Exception
	 */
	public void submitForApproval(ExchangeRateTimeSerie_Ds ds, ExchangeRateTimeSerie_DsParam params) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START submitForApproval()");
		}
		ExchangeRateApproveRejectService service = this.getApplicationContext().getBean(ExchangeRateApproveRejectService.class);
		service.submitForApproval(ds, params);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END submitForApproval()");
		}
	}

	/**
	 * Approves current Exchange Rate Serie workflow task
	 *
	 * @param data
	 * @param param
	 * @throws Exception
	 */
	public void approve(ExchangeRateTimeSerie_Ds data, ExchangeRateTimeSerie_DsParam params) throws BusinessException {
		ExchangeRateApproveRejectService service = this.getApplicationContext().getBean(ExchangeRateApproveRejectService.class);
		service.approve(data, params);
	}

	/**
	 * Rejects current Exchange Rate Serie workflow task
	 *
	 * @param data
	 * @param param
	 * @throws Exception
	 */
	public void reject(ExchangeRateTimeSerie_Ds data, ExchangeRateTimeSerie_DsParam params) throws BusinessException {
		ExchangeRateApproveRejectService service = this.getApplicationContext().getBean(ExchangeRateApproveRejectService.class);
		service.reject(data, params);
	}

	/**
	 * Approves current Energy Time Serie workflow task
	 *
	 * @param data
	 * @param param
	 * @throws Exception
	 */
	public void approveList(List<ExchangeRateTimeSerie_Ds> data, ExchangeRateTimeSerie_DsParam params) {
		ExchangeRateApproveRejectService service = this.getApplicationContext().getBean(ExchangeRateApproveRejectService.class);
		service.approveList(data, params);
	}

	/**
	 * Rejects current Energy Time Serie workflow task
	 *
	 * @param data
	 * @param param
	 * @throws Exception
	 */
	public void rejectList(List<ExchangeRateTimeSerie_Ds> data, ExchangeRateTimeSerie_DsParam params) {
		ExchangeRateApproveRejectService service = this.getApplicationContext().getBean(ExchangeRateApproveRejectService.class);
		service.rejectList(data, params);
	}
}
