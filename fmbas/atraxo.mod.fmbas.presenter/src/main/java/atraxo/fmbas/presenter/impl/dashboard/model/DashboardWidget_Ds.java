/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.dashboard.model;

import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.model.IModelWithId;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DashboardWidgets.class)
@RefLookups({@RefLookup(refId = DashboardWidget_Ds.F_LAYOUTID),
		@RefLookup(refId = DashboardWidget_Ds.F_DASHBOARDID),
		@RefLookup(refId = DashboardWidget_Ds.F_WIDGETID)})
public class DashboardWidget_Ds extends AbstractDs_Ds<DashboardWidgets>
		implements
			IModelWithId<Integer> {

	public static final String ALIAS = "fmbas_DashboardWidget_Ds";

	public static final String F_DASHBOARDID = "dashboardId";
	public static final String F_DASHBOARDNAME = "dashboardName";
	public static final String F_LAYOUTID = "layoutId";
	public static final String F_LAYOUTNAME = "layoutName";
	public static final String F_WIDGETID = "widgetId";
	public static final String F_WIDGETNAME = "widgetName";
	public static final String F_WIDGETDS = "widgetDs";
	public static final String F_WIDGETMETHOD = "widgetMethod";
	public static final String F_ID = "id";
	public static final String F_LAYOUTREGIONID = "layoutRegionId";
	public static final String F_WIDGETINDEX = "widgetIndex";

	@DsField(join = "left", path = "dashboard.id")
	private Integer dashboardId;

	@DsField(join = "left", path = "dashboard.name")
	private String dashboardName;

	@DsField(join = "left", path = "layout.id")
	private Integer layoutId;

	@DsField(join = "left", path = "layout.name")
	private String layoutName;

	@DsField(join = "left", path = "widget.id")
	private Integer widgetId;

	@DsField(join = "left", path = "widget.name")
	private String widgetName;

	@DsField(join = "left", path = "widget.dsName")
	private String widgetDs;

	@DsField(join = "left", path = "widget.methodName")
	private String widgetMethod;

	@DsField
	private Integer id;

	@DsField
	private String layoutRegionId;

	@DsField
	private String widgetIndex;

	/**
	 * Default constructor
	 */
	public DashboardWidget_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DashboardWidget_Ds(DashboardWidgets e) {
		super(e);
	}

	public Integer getDashboardId() {
		return this.dashboardId;
	}

	public void setDashboardId(Integer dashboardId) {
		this.dashboardId = dashboardId;
	}

	public String getDashboardName() {
		return this.dashboardName;
	}

	public void setDashboardName(String dashboardName) {
		this.dashboardName = dashboardName;
	}

	public Integer getLayoutId() {
		return this.layoutId;
	}

	public void setLayoutId(Integer layoutId) {
		this.layoutId = layoutId;
	}

	public String getLayoutName() {
		return this.layoutName;
	}

	public void setLayoutName(String layoutName) {
		this.layoutName = layoutName;
	}

	public Integer getWidgetId() {
		return this.widgetId;
	}

	public void setWidgetId(Integer widgetId) {
		this.widgetId = widgetId;
	}

	public String getWidgetName() {
		return this.widgetName;
	}

	public void setWidgetName(String widgetName) {
		this.widgetName = widgetName;
	}

	public String getWidgetDs() {
		return this.widgetDs;
	}

	public void setWidgetDs(String widgetDs) {
		this.widgetDs = widgetDs;
	}

	public String getWidgetMethod() {
		return this.widgetMethod;
	}

	public void setWidgetMethod(String widgetMethod) {
		this.widgetMethod = widgetMethod;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLayoutRegionId() {
		return this.layoutRegionId;
	}

	public void setLayoutRegionId(String layoutRegionId) {
		this.layoutRegionId = layoutRegionId;
	}

	public String getWidgetIndex() {
		return this.widgetIndex;
	}

	public void setWidgetIndex(String widgetIndex) {
		this.widgetIndex = widgetIndex;
	}
}
