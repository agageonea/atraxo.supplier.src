/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.uom.model;

import atraxo.fmbas.domain.impl.uom.FisLov;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FisLov.class)
public class FisLov_Ds extends AbstractDs_Ds<FisLov> {

	public static final String ALIAS = "fmbas_FisLov_Ds";

	public static final String F_SYSTEM = "system";
	public static final String F_TABLE = "table";
	public static final String F_CODE1 = "code1";
	public static final String F_CODE2 = "code2";
	public static final String F_ATTRIBUT1 = "attribut1";
	public static final String F_ATTRIBUT2 = "attribut2";
	public static final String F_ACTIVE = "active";
	public static final String F_POS = "pos";

	@DsField(path = "lovSystem")
	private String system;

	@DsField(path = "lovTable")
	private String table;

	@DsField
	private String code1;

	@DsField
	private String code2;

	@DsField
	private String attribut1;

	@DsField
	private String attribut2;

	@DsField
	private Boolean active;

	@DsField
	private Integer pos;

	/**
	 * Default constructor
	 */
	public FisLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FisLov_Ds(FisLov e) {
		super(e);
	}

	public String getSystem() {
		return this.system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getTable() {
		return this.table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getCode1() {
		return this.code1;
	}

	public void setCode1(String code1) {
		this.code1 = code1;
	}

	public String getCode2() {
		return this.code2;
	}

	public void setCode2(String code2) {
		this.code2 = code2;
	}

	public String getAttribut1() {
		return this.attribut1;
	}

	public void setAttribut1(String attribut1) {
		this.attribut1 = attribut1;
	}

	public String getAttribut2() {
		return this.attribut2;
	}

	public void setAttribut2(String attribut2) {
		this.attribut2 = attribut2;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getPos() {
		return this.pos;
	}

	public void setPos(Integer pos) {
		this.pos = pos;
	}
}
