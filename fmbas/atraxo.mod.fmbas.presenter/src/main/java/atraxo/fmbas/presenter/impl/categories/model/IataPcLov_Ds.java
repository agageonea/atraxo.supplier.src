/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.categories.model;

import atraxo.fmbas.domain.impl.categories.IataPC;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = IataPC.class, sort = {@SortField(field = IataPcLov_Ds.F_CODE)})
public class IataPcLov_Ds extends AbstractLov_Ds<IataPC> {

	public static final String ALIAS = "fmbas_IataPcLov_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";

	@DsField
	private String code;

	@DsField
	private String name;

	/**
	 * Default constructor
	 */
	public IataPcLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public IataPcLov_Ds(IataPC e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
