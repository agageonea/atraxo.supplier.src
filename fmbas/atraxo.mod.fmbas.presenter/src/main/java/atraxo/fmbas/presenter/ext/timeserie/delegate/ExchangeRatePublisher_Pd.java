package atraxo.fmbas.presenter.ext.timeserie.delegate;

import java.util.ArrayList;
import java.util.List;

import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.presenter.impl.timeserie.model.ExchangeRateTimeSerie_Ds;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author zspeter
 */
public class ExchangeRatePublisher_Pd extends AbstractPresenterDelegate {

	private static final String PUBLISH_TIME_SERIES_CHANNEL = "publishTimeSeriesChannel";

	public void publish(ExchangeRateTimeSerie_Ds data) throws Exception {
		ITimeSerieService service = (ITimeSerieService) this.findEntityService(TimeSerie.class);

		Integer tsId = data.getId();
		TimeSerie ts = service.findById(tsId);

		List<TimeSerie> tsList = new ArrayList<>();
		tsList.add(ts);

		service.publish(tsList);
		this.sendMessage(PUBLISH_TIME_SERIES_CHANNEL, ts.getId());
	}

	public void publish(List<ExchangeRateTimeSerie_Ds> data) throws Exception {
		List<Object> tsIds = new ArrayList<>();
		for (ExchangeRateTimeSerie_Ds tsData : data) {
			tsIds.add(tsData.getId());
		}
		ITimeSerieService service = (ITimeSerieService) this.findEntityService(TimeSerie.class);
		List<TimeSerie> tsList = service.findByIds(tsIds);
		service.publish(tsList);
		for (TimeSerie ts : tsList) {
			this.sendMessage(PUBLISH_TIME_SERIES_CHANNEL, ts.getId());
		}

	}

}
