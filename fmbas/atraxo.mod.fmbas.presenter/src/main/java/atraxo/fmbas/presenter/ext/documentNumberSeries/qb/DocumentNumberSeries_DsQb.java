/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.fmbas.presenter.ext.documentNumberSeries.qb;

import atraxo.fmbas.domain.impl.fmbas_type.DocumentNumberSeriesType;
import atraxo.fmbas.presenter.impl.documentNumberSeries.model.DocumentNumberSeries_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class DocumentNumberSeries_DsQb extends QueryBuilderWithJpql<DocumentNumberSeries_Ds, DocumentNumberSeries_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();
		this.addFilterCondition("e.type = :type");
		this.addCustomFilterItem("type", DocumentNumberSeriesType._CUSTOMER_);
	}

}
