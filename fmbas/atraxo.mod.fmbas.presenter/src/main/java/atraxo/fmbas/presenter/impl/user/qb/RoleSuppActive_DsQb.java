/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.user.qb;

import atraxo.fmbas.presenter.impl.user.model.RoleSuppActive_Ds;
import atraxo.fmbas.presenter.impl.user.model.RoleSuppActive_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class RoleSuppActive_DsQb
		extends
			QueryBuilderWithJpql<RoleSuppActive_Ds, RoleSuppActive_Ds, RoleSuppActive_DsParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getWithUserId() != null
				&& !"".equals(this.params.getWithUserId())) {
			addFilterCondition("  e.id in ( select p.id from  Role p, IN (p.users) c where c.id = :withUserId )  ");
			addCustomFilterItem("withUserId", this.params.getWithUserId());
		}
		if (this.params != null && this.params.getWithPrivilegeId() != null
				&& !"".equals(this.params.getWithPrivilegeId())) {
			addFilterCondition("  e.id in ( select p.id from  Role p, IN (p.accessControls) c where c.id = :withPrivilegeId )  ");
			addCustomFilterItem("withPrivilegeId",
					this.params.getWithPrivilegeId());
		}
	}
}
