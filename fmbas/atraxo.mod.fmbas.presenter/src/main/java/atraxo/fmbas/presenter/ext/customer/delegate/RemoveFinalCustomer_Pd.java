package atraxo.fmbas.presenter.ext.customer.delegate;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.presenter.impl.customer.model.FinalCustomer_Ds;
import atraxo.fmbas.presenter.impl.customer.model.FinalCustomer_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class RemoveFinalCustomer_Pd extends AbstractPresenterDelegate {

	public void remove(List<FinalCustomer_Ds> list, FinalCustomer_DsParam params) throws Exception {
		ICustomerService customerService = (ICustomerService) this.findEntityService(Customer.class);
		for (FinalCustomer_Ds ds : list) {
			Customer finalCustomer = customerService.findById(ds.getId());
			Customer reseller = customerService.findById(params.getCustomerId());
			Collection<Customer> resellers = finalCustomer.getReseller();
			Collection<Customer> finalCustomers = reseller.getFinalCustomer();
			this.removeFromCollection(reseller.getId(), resellers);
			this.removeFromCollection(ds.getId(), finalCustomers);
			customerService.update(reseller);
			customerService.update(finalCustomer);
		}
	}

	private void removeFromCollection(Integer id, Collection<Customer> resellers) {
		Iterator<Customer> iter = resellers.iterator();
		while (iter.hasNext()) {
			Customer cust = iter.next();
			if (cust.getId().equals(id)) {
				iter.remove();
			}
		}
	}

}
