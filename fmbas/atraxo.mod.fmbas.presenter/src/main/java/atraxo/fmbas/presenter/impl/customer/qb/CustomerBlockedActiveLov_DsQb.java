/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.qb;

import atraxo.fmbas.presenter.impl.customer.model.CustomerBlockedActiveLov_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class CustomerBlockedActiveLov_DsQb
		extends
			QueryBuilderWithJpql<CustomerBlockedActiveLov_Ds, CustomerBlockedActiveLov_Ds, Object> {

	@Override
	public void setFilter(CustomerBlockedActiveLov_Ds filter) {
		filter.setIsCustomer(true);
		super.setFilter(filter);
	}
}
