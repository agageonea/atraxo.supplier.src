/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.geo.model;

import atraxo.fmbas.domain.impl.fmbas_type.GeoType;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Locations.class)
public class LocationsAreasLov_Ds extends AbstractLov_Ds<Locations> {

	public static final String ALIAS = "fmbas_LocationsAreasLov_Ds";

	public static final String F_NAME = "name";
	public static final String F_CODE = "code";
	public static final String F_TYPE = "type";

	@DsField
	private String name;

	@DsField
	private String code;

	@DsField(fetch = false)
	private GeoType type;

	/**
	 * Default constructor
	 */
	public LocationsAreasLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public LocationsAreasLov_Ds(Locations e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public GeoType getType() {
		return this.type;
	}

	public void setType(GeoType type) {
		this.type = type;
	}
}
