/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.soneViewState.model;

import atraxo.fmbas.domain.impl.soneViewState.SoneViewState;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = SoneViewState.class)
public class ViewCmpLov_Ds extends AbstractDs_Ds<SoneViewState> {

	public static final String ALIAS = "fmbas_ViewCmpLov_Ds";

	public static final String F_CMP = "cmp";
	public static final String F_CMPTYPE = "cmpType";

	@DsField
	private String cmp;

	@DsField
	private String cmpType;

	/**
	 * Default constructor
	 */
	public ViewCmpLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ViewCmpLov_Ds(SoneViewState e) {
		super(e);
	}

	public String getCmp() {
		return this.cmp;
	}

	public void setCmp(String cmp) {
		this.cmp = cmp;
	}

	public String getCmpType() {
		return this.cmpType;
	}

	public void setCmpType(String cmpType) {
		this.cmpType = cmpType;
	}
}
