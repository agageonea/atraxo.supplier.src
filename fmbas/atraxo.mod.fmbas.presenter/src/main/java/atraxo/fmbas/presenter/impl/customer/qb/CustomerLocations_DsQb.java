/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.qb;

import atraxo.fmbas.presenter.impl.customer.model.CustomerLocations_Ds;
import atraxo.fmbas.presenter.impl.customer.model.CustomerLocations_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class CustomerLocations_DsQb
		extends
			QueryBuilderWithJpql<CustomerLocations_Ds, CustomerLocations_Ds, CustomerLocations_DsParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getCustomerId() != null
				&& !"".equals(this.params.getCustomerId())) {
			addFilterCondition("  e.id in ( select cl.id from CustomerLocations cl, IN (cl.customers) c where c.id = :customerId ) ");
			addCustomFilterItem("customerId", this.params.getCustomerId());
		}
	}
}
