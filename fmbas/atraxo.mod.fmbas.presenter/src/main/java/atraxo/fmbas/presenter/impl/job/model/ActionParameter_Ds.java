/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.job.model;

import atraxo.fmbas.domain.impl.fmbas_type.JobParamAssignType;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamType;
import atraxo.fmbas.domain.impl.job.ActionParameter;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ActionParameter.class, sort = {@SortField(field = ActionParameter_Ds.F_NAME)})
@RefLookups({@RefLookup(refId = ActionParameter_Ds.F_ACTIONID)})
public class ActionParameter_Ds extends AbstractDs_Ds<ActionParameter> {

	public static final String ALIAS = "fmbas_ActionParameter_Ds";

	public static final String F_ACTIONID = "actionId";
	public static final String F_JOBUID = "jobUID";
	public static final String F_ACTIONNAME = "actionName";
	public static final String F_ACTIONORDER = "actionOrder";
	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_VALUE = "value";
	public static final String F_BUSINESSVALUE = "businessValue";
	public static final String F_DEFAULTVALUE = "defaultValue";
	public static final String F_TYPE = "type";
	public static final String F_ASSIGNTYPE = "assignType";
	public static final String F_REFVALUES = "refValues";
	public static final String F_REFBUSINESSVALUES = "refBusinessValues";
	public static final String F_READONLY = "readOnly";
	public static final String F_SUBSIDIARYID = "subsidiaryId";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_FILEPATH = "filePath";

	@DsField(join = "left", path = "jobAction.id")
	private Integer actionId;

	@DsField(join = "left", path = "jobAction.jobUID")
	private String jobUID;

	@DsField(join = "left", path = "jobAction.name")
	private String actionName;

	@DsField(join = "left", path = "jobAction.order")
	private Integer actionOrder;

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private String value;

	@DsField
	private String businessValue;

	@DsField
	private String defaultValue;

	@DsField
	private JobParamType type;

	@DsField
	private JobParamAssignType assignType;

	@DsField
	private String refValues;

	@DsField
	private String refBusinessValues;

	@DsField
	private Boolean readOnly;

	@DsField(fetch = false)
	private Integer subsidiaryId;

	@DsField(fetch = false)
	private Integer customerId;

	@DsField(fetch = false)
	private String filePath;

	/**
	 * Default constructor
	 */
	public ActionParameter_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ActionParameter_Ds(ActionParameter e) {
		super(e);
	}

	public Integer getActionId() {
		return this.actionId;
	}

	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}

	public String getJobUID() {
		return this.jobUID;
	}

	public void setJobUID(String jobUID) {
		this.jobUID = jobUID;
	}

	public String getActionName() {
		return this.actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public Integer getActionOrder() {
		return this.actionOrder;
	}

	public void setActionOrder(Integer actionOrder) {
		this.actionOrder = actionOrder;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getBusinessValue() {
		return this.businessValue;
	}

	public void setBusinessValue(String businessValue) {
		this.businessValue = businessValue;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public JobParamType getType() {
		return this.type;
	}

	public void setType(JobParamType type) {
		this.type = type;
	}

	public JobParamAssignType getAssignType() {
		return this.assignType;
	}

	public void setAssignType(JobParamAssignType assignType) {
		this.assignType = assignType;
	}

	public String getRefValues() {
		return this.refValues;
	}

	public void setRefValues(String refValues) {
		this.refValues = refValues;
	}

	public String getRefBusinessValues() {
		return this.refBusinessValues;
	}

	public void setRefBusinessValues(String refBusinessValues) {
		this.refBusinessValues = refBusinessValues;
	}

	public Boolean getReadOnly() {
		return this.readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	public Integer getSubsidiaryId() {
		return this.subsidiaryId;
	}

	public void setSubsidiaryId(Integer subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getFilePath() {
		return this.filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
}
