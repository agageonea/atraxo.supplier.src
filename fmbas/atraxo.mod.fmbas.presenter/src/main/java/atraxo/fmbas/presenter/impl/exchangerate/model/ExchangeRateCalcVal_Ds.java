/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.exchangerate.model;

import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ExchangeRate.class, sort = {
		@SortField(field = ExchangeRateCalcVal_Ds.F_FINANCIALSOURCECODE),
		@SortField(field = ExchangeRateCalcVal_Ds.F_AVGMETHODINDICATORNAME)})
@RefLookups({@RefLookup(refId = ExchangeRateCalcVal_Ds.F_FINANCIALSOURCEID),
		@RefLookup(refId = ExchangeRateCalcVal_Ds.F_AVGMTHDID)})
public class ExchangeRateCalcVal_Ds extends AbstractDs_Ds<ExchangeRate> {

	public static final String ALIAS = "fmbas_ExchangeRateCalcVal_Ds";

	public static final String F_CALCVAL = "calcVal";
	public static final String F_AVGMTHDID = "avgMthdId";
	public static final String F_AVGMETHODINDICATORNAME = "avgMethodIndicatorName";
	public static final String F_AVGMETHODINDICATORCODE = "avgMethodIndicatorCode";
	public static final String F_FINANCIALSOURCEID = "financialSourceId";
	public static final String F_FINANCIALSOURCECODE = "financialSourceCode";

	@DsField(fetch = false)
	private String calcVal;

	@DsField(join = "left", path = "avgMethodIndicator.id")
	private Integer avgMthdId;

	@DsField(join = "left", path = "avgMethodIndicator.name")
	private String avgMethodIndicatorName;

	@DsField(join = "left", path = "avgMethodIndicator.code")
	private String avgMethodIndicatorCode;

	@DsField(join = "left", path = "finsource.id")
	private Integer financialSourceId;

	@DsField(join = "left", path = "finsource.code")
	private String financialSourceCode;

	/**
	 * Default constructor
	 */
	public ExchangeRateCalcVal_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ExchangeRateCalcVal_Ds(ExchangeRate e) {
		super(e);
	}

	public String getCalcVal() {
		return this.calcVal;
	}

	public void setCalcVal(String calcVal) {
		this.calcVal = calcVal;
	}

	public Integer getAvgMthdId() {
		return this.avgMthdId;
	}

	public void setAvgMthdId(Integer avgMthdId) {
		this.avgMthdId = avgMthdId;
	}

	public String getAvgMethodIndicatorName() {
		return this.avgMethodIndicatorName;
	}

	public void setAvgMethodIndicatorName(String avgMethodIndicatorName) {
		this.avgMethodIndicatorName = avgMethodIndicatorName;
	}

	public String getAvgMethodIndicatorCode() {
		return this.avgMethodIndicatorCode;
	}

	public void setAvgMethodIndicatorCode(String avgMethodIndicatorCode) {
		this.avgMethodIndicatorCode = avgMethodIndicatorCode;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}
}
