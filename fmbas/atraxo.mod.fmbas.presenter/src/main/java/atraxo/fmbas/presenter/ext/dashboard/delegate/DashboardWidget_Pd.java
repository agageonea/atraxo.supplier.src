package atraxo.fmbas.presenter.ext.dashboard.delegate;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.dashboard.IAssignedWidgetParametersService;
import atraxo.fmbas.business.api.dashboard.IDashboardWidgetsService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;
import atraxo.fmbas.domain.impl.fmbas_type.UpliftedVolumePeriod;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.dashboard.model.DashboardWidget_Ds;
import atraxo.fmbas.presenter.impl.dashboard.model.DashboardWidget_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class DashboardWidget_Pd extends AbstractPresenterDelegate {

	private static final String FUEL_PLUS_LOGO_IMAGE = "https://www.fuelplus.com/wp-content/themes/fuelplus_flat/images/logo.png";
	private static final Logger LOG = LoggerFactory.getLogger(DashboardWidget_Pd.class);

	public void getUpliftedVolume(DashboardWidget_Ds ds, DashboardWidget_DsParam params) {
		try {
			if (ds.getId() == null) {
				return;
			}
			IUnitService unitService = (IUnitService) this.findEntityService(Unit.class);
			IDashboardWidgetsService dashboardWidgetsService = (IDashboardWidgetsService) this.findEntityService(DashboardWidgets.class);
			IAssignedWidgetParametersService assignedWidgetParametersService = (IAssignedWidgetParametersService) this
					.findEntityService(AssignedWidgetParameters.class);
			Object bean = this.getApplicationContext().getBean("FuelEvent");
			DashboardWidgets dashboardWidgets = dashboardWidgetsService.findByBusiness(ds.getId());
			String unitStr = assignedWidgetParametersService.getUnit(new ArrayList<>(dashboardWidgets.getAssignedWidgetParameters()));
			Unit unit = unitService.findByCode(unitStr);
			String periodString = assignedWidgetParametersService.getPeriod(new ArrayList<>(dashboardWidgets.getAssignedWidgetParameters()));
			UpliftedVolumePeriod volumePeriod = UpliftedVolumePeriod.getByName(periodString);
			for (Method method : bean.getClass().getDeclaredMethods()) {
				if (method.getName().equals("calculateVolume")) {
					Object invokeParams[] = new Object[3];
					Date[] period = this.getPeriod(volumePeriod);
					invokeParams[0] = period[0];
					invokeParams[1] = period[1];
					invokeParams[2] = unit;
					Object val = method.invoke(bean, invokeParams);
					String retVal = this.buildJson(unit, val, volumePeriod);
					params.setRpcResponse(retVal);
				}
			}
		} catch (Exception e) {
			LOG.warn("Uplifted volume calculation problem!", e);
		}
	}

	public void getCustomImage(DashboardWidget_Ds ds, DashboardWidget_DsParam params) {

		try {
			if (ds.getId() == null) {
				return;
			}
			IDashboardWidgetsService dashboardWidgetsService = (IDashboardWidgetsService) this.findEntityService(DashboardWidgets.class);
			DashboardWidgets dashboardWidgets = dashboardWidgetsService.findByBusiness(ds.getId());
			String imgSrc = this.getImageSource(new ArrayList<>(dashboardWidgets.getAssignedWidgetParameters()));
			String imgSrcJson = this.buildCustomImageJson(imgSrc);
			params.setRpcResponse(imgSrcJson);
		} catch (Exception e) {
			LOG.warn("Could not upload dashboard image!", e);
		}

	}

	public String getImageSource(List<AssignedWidgetParameters> assignedWidgetParameter) {
		if (CollectionUtils.isEmpty(assignedWidgetParameter)) {
			return FUEL_PLUS_LOGO_IMAGE;
		}
		for (AssignedWidgetParameters wp : assignedWidgetParameter) {
			if ("IMAGE SOURCE".equalsIgnoreCase(wp.getWidgetParameters().getName())) {
				return wp.getBlobValue() == null ? wp.getDefaultValue()
						: "data:image/png;base64," + Base64.getEncoder().encodeToString(wp.getBlobValue());
			}
		}
		return FUEL_PLUS_LOGO_IMAGE;
	}

	public void countBufferedTickets(DashboardWidget_Ds ds, DashboardWidget_DsParam params) {
		try {
			if (ds.getId() == null) {
				return;
			}

			Object bean = this.getApplicationContext().getBean("FuelTransaction");
			for (Method method : bean.getClass().getDeclaredMethods()) {
				if (method.getName().equals("count")) {
					Object value = method.invoke(bean);
					String retVal = this.buildBufferedJson(value);
					params.setRpcResponse(retVal);
				}
			}
		} catch (Exception e) {
			LOG.warn("Uplifted volume calculation problem!", e);
		}
	}

	private String buildJson(Unit unit, Object value, UpliftedVolumePeriod period) throws JSONException {
		JSONObject json = new JSONObject();
		json.put("value", value);
		json.put("PERIOD", period.getName());
		json.put("UNIT OF MEASURE", unit.getCode());
		return json.toString();
	}

	public void proxyConnect(DashboardWidget_Ds ds, DashboardWidget_DsParam params) throws JSONException {
		try {
			if (ds.getId() == null) {
				return;
			}
			String value = "{\"success\":true,\"message\":null,\"executionTime\":21,\"totalCount\":5,\"data\":[{\"__clientRecordId__\":null,\"__filterAttrString__\":null,\"id\":1,\"refid\":\"7BBF362F-F665-4117-9815-6CC15D1F347B\",\"clientId\":\"06A246A436EE449296623587746AB650\",\"createdAt\":\"2018-04-17 12:15:41\",\"modifiedAt\":\"2018-04-17 12:15:46\",\"createdBy\":\"admin_pav\",\"modifiedBy\":\"admin_pav\",\"version\":2,\"entityAlias\":\"AssignedWidgetParameters\",\"entityFqn\":\"atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters\",\"dashboardWidgetId\":1,\"layoutRegionId\":\"layout-2-zone-2\",\"widgetIndex\":\"0\",\"widgetId\":45,\"widgetName\":\"External link content\",\"widgetCode\":\"ELC\",\"parameterId\":82,\"parameterValue\":\"\",\"parameterDefaultValue\":\"JSON\",\"parameterRefValues\":\"JSON,HTML\",\"parameterRefBusinessValues\":\"\",\"type\":\"enumeration\",\"name\":\"RESPONSE FORMAT\",\"value\":\"JSON\",\"defaultValue\":\"JSON\",\"readOnly\":false,\"refValues\":\"JSON,HTML\",\"refBusinessValues\":\"\",\"layoutId\":2},{\"__clientRecordId__\":null,\"__filterAttrString__\":null,\"id\":2,\"refid\":\"9885D5D1-8D05-42F4-BA64-724C0579EB9B\",\"clientId\":\"06A246A436EE449296623587746AB650\",\"createdAt\":\"2018-04-17 12:15:41\",\"modifiedAt\":\"2018-04-17 12:15:46\",\"createdBy\":\"admin_pav\",\"modifiedBy\":\"admin_pav\",\"version\":2,\"entityAlias\":\"AssignedWidgetParameters\",\"entityFqn\":\"atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters\",\"dashboardWidgetId\":1,\"layoutRegionId\":\"layout-2-zone-2\",\"widgetIndex\":\"0\",\"widgetId\":45,\"widgetName\":\"External link content\",\"widgetCode\":\"ELC\",\"parameterId\":79,\"parameterValue\":\"\",\"parameterDefaultValue\":\"\",\"parameterRefValues\":\"\",\"parameterRefBusinessValues\":\"\",\"type\":\"string\",\"name\":\"URL\",\"value\":\"aaa\",\"defaultValue\":\"\",\"readOnly\":false,\"refValues\":\"\",\"refBusinessValues\":\"\",\"layoutId\":2},{\"__clientRecordId__\":null,\"__filterAttrString__\":null,\"id\":3,\"refid\":\"1CBD6886-E15D-4041-9CB4-4B3B4D34414B\",\"clientId\":\"06A246A436EE449296623587746AB650\",\"createdAt\":\"2018-04-17 12:15:41\",\"modifiedAt\":\"2018-04-17 12:15:46\",\"createdBy\":\"admin_pav\",\"modifiedBy\":\"admin_pav\",\"version\":2,\"entityAlias\":\"AssignedWidgetParameters\",\"entityFqn\":\"atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters\",\"dashboardWidgetId\":1,\"layoutRegionId\":\"layout-2-zone-2\",\"widgetIndex\":\"0\",\"widgetId\":45,\"widgetName\":\"External link content\",\"widgetCode\":\"ELC\",\"parameterId\":81,\"parameterValue\":\"\",\"parameterDefaultValue\":\"\",\"parameterRefValues\":\"\",\"parameterRefBusinessValues\":\"\",\"type\":\"textarea\",\"name\":\"BODY\",\"value\":\"aaaa\",\"defaultValue\":\"\",\"readOnly\":false,\"refValues\":\"\",\"refBusinessValues\":\"\",\"layoutId\":2},{\"__clientRecordId__\":null,\"__filterAttrString__\":null,\"id\":4,\"refid\":\"29B17967-9FF2-40C4-9E61-C5585E1621FF\",\"clientId\":\"06A246A436EE449296623587746AB650\",\"createdAt\":\"2018-04-17 12:15:41\",\"modifiedAt\":\"2018-04-17 12:15:46\",\"createdBy\":\"admin_pav\",\"modifiedBy\":\"admin_pav\",\"version\":2,\"entityAlias\":\"AssignedWidgetParameters\",\"entityFqn\":\"atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters\",\"dashboardWidgetId\":1,\"layoutRegionId\":\"layout-2-zone-2\",\"widgetIndex\":\"0\",\"widgetId\":45,\"widgetName\":\"External link content\",\"widgetCode\":\"ELC\",\"parameterId\":80,\"parameterValue\":\"\",\"parameterDefaultValue\":\"\",\"parameterRefValues\":\"\",\"parameterRefBusinessValues\":\"\",\"type\":\"textarea\",\"name\":\"HEADER\",\"value\":\"aaa\",\"defaultValue\":\"\",\"readOnly\":false,\"refValues\":\"\",\"refBusinessValues\":\"\",\"layoutId\":2},{\"__clientRecordId__\":null,\"__filterAttrString__\":null,\"id\":5,\"refid\":\"46B4177B-3ABE-4E75-AEA7-2027E4C1D6C2\",\"clientId\":\"06A246A436EE449296623587746AB650\",\"createdAt\":\"2018-04-17 12:15:41\",\"modifiedAt\":\"2018-04-17 12:15:46\",\"createdBy\":\"admin_pav\",\"modifiedBy\":\"admin_pav\",\"version\":2,\"entityAlias\":\"AssignedWidgetParameters\",\"entityFqn\":\"atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters\",\"dashboardWidgetId\":1,\"layoutRegionId\":\"layout-2-zone-2\",\"widgetIndex\":\"0\",\"widgetId\":45,\"widgetName\":\"External link content\",\"widgetCode\":\"ELC\",\"parameterId\":78,\"parameterValue\":\"\",\"parameterDefaultValue\":\"GET\",\"parameterRefValues\":\"GET,POST,HEAD,PUT,DELETE,OPTIONS,CONNECT\",\"parameterRefBusinessValues\":\"\",\"type\":\"enumeration\",\"name\":\"METHOD\",\"value\":\"GET\",\"defaultValue\":\"GET\",\"readOnly\":false,\"refValues\":\"GET,POST,HEAD,PUT,DELETE,OPTIONS,CONNECT\",\"refBusinessValues\":\"\",\"layoutId\":2}],\"summaries\":{},\"params\":null}"; // JSON
			// String value = "This is a test"; // HTML value
			String retVal = this.buildProxyResponse(value);
			params.setRpcResponse(retVal);

		} catch (Exception e) {
			LOG.warn("Problem while sending response!", e);
		}
	}

	private String buildProxyResponse(Object value) throws JSONException {

		// Value can be a JSON or HTML

		return value.toString();
	}

	private String buildBufferedJson(Object value) throws JSONException {
		JSONObject json = new JSONObject();
		json.put("value", value);
		return json.toString();
	}

	private String buildCustomImageJson(String imgSrc) throws JSONException {
		JSONObject json = new JSONObject();
		json.put("src", imgSrc);
		return json.toString();
	}

	private Date[] getPeriod(UpliftedVolumePeriod volumePeriod) {
		Calendar cal = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		Date[] period = new Date[2];
		switch (volumePeriod) {
		case _EMPTY_:
		default:
		case _THIS_YEAR_:
			cal.set(Calendar.DAY_OF_MONTH, 1);
			cal.set(Calendar.MONTH, 1);
			this.set0Hour(cal);
			period[0] = cal.getTime();
			this.set24Hour(cal2);
			period[1] = cal2.getTime();
			return period;
		case _LAST_MONTH_:
			cal.add(Calendar.MONTH, -1);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			this.set0Hour(cal);
			period[0] = cal.getTime();
			cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
			this.set24Hour(cal);
			period[1] = cal.getTime();
			return period;
		case _LAST_WEEK_:
			cal.set(Calendar.WEEK_OF_YEAR, -1);
			cal.set(Calendar.DAY_OF_WEEK, cal.getActualMinimum(Calendar.DAY_OF_WEEK));
			this.set0Hour(cal);
			period[0] = cal.getTime();
			cal.set(Calendar.DAY_OF_WEEK, cal.getActualMaximum(Calendar.DAY_OF_WEEK));
			this.set24Hour(cal);
			period[1] = cal.getTime();
			return period;
		case _LAST_YEAR_:
			cal.add(Calendar.YEAR, -1);
			cal.set(Calendar.MONTH, 1);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			this.set0Hour(cal);
			period[0] = cal.getTime();
			cal.set(Calendar.MONTH, Calendar.DECEMBER);
			cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
			this.set24Hour(cal);
			period[1] = cal.getTime();
			return period;
		case _THIS_MONTH_:
			cal.set(Calendar.DAY_OF_MONTH, 1);
			this.set0Hour(cal);
			period[0] = cal.getTime();
			this.set24Hour(cal2);
			period[1] = cal2.getTime();
			return period;
		case _THIS_WEEK_:
			cal.set(Calendar.DAY_OF_WEEK, 1);
			this.set0Hour(cal);
			period[0] = cal.getTime();
			this.set24Hour(cal2);
			period[1] = cal2.getTime();
			return period;
		case _TODAY_:
			this.set0Hour(cal);
			period[0] = cal.getTime();
			this.set24Hour(cal);
			period[1] = cal.getTime();
			return period;
		case _YESTERDAY_:
			cal.set(Calendar.DAY_OF_YEAR, -1);
			this.set0Hour(cal);
			period[0] = cal.getTime();
			this.set24Hour(cal);
			period[1] = cal.getTime();
			return period;
		}
	}

	private void set0Hour(Calendar cal) {
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
	}

	private void set24Hour(Calendar cal) {
		cal.set(Calendar.HOUR, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
	}
}
