/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.dashboard.model;

import atraxo.fmbas.domain.impl.dashboard.WidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.Widgets;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamType;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = WidgetParameters.class)
@RefLookups({@RefLookup(refId = WidgetParameter_Ds.F_WIDGETID, namedQuery = Widgets.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = WidgetParameter_Ds.F_WIDGETCODE)})})
public class WidgetParameter_Ds extends AbstractDs_Ds<WidgetParameters> {

	public static final String ALIAS = "fmbas_WidgetParameter_Ds";

	public static final String F_WIDGETID = "widgetId";
	public static final String F_WIDGETCODE = "widgetCode";
	public static final String F_WIDGETNAME = "widgetName";
	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_TYPE = "type";
	public static final String F_VALUE = "value";
	public static final String F_DEFAULTVALUE = "defaultValue";
	public static final String F_READONLY = "readOnly";
	public static final String F_REFVALUES = "refValues";
	public static final String F_REFBUSINESSVALUES = "refBusinessValues";

	@DsField(join = "left", path = "widget.id")
	private Integer widgetId;

	@DsField(join = "left", path = "widget.code")
	private String widgetCode;

	@DsField(join = "left", path = "widget.name")
	private String widgetName;

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private JobParamType type;

	@DsField
	private String value;

	@DsField
	private String defaultValue;

	@DsField
	private Boolean readOnly;

	@DsField
	private String refValues;

	@DsField
	private String refBusinessValues;

	/**
	 * Default constructor
	 */
	public WidgetParameter_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public WidgetParameter_Ds(WidgetParameters e) {
		super(e);
	}

	public Integer getWidgetId() {
		return this.widgetId;
	}

	public void setWidgetId(Integer widgetId) {
		this.widgetId = widgetId;
	}

	public String getWidgetCode() {
		return this.widgetCode;
	}

	public void setWidgetCode(String widgetCode) {
		this.widgetCode = widgetCode;
	}

	public String getWidgetName() {
		return this.widgetName;
	}

	public void setWidgetName(String widgetName) {
		this.widgetName = widgetName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public JobParamType getType() {
		return this.type;
	}

	public void setType(JobParamType type) {
		this.type = type;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Boolean getReadOnly() {
		return this.readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	public String getRefValues() {
		return this.refValues;
	}

	public void setRefValues(String refValues) {
		this.refValues = refValues;
	}

	public String getRefBusinessValues() {
		return this.refBusinessValues;
	}

	public void setRefBusinessValues(String refBusinessValues) {
		this.refBusinessValues = refBusinessValues;
	}
}
