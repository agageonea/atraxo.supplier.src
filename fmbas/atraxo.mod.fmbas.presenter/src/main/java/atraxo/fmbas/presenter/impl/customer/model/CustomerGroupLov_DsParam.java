/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

/**
 * Generated code. Do not modify in this file.
 */
public class CustomerGroupLov_DsParam {

	public static final String f_customerId = "customerId";
	public static final String f_customerParentId = "customerParentId";

	private Integer customerId;

	private Integer customerParentId;

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getCustomerParentId() {
		return this.customerParentId;
	}

	public void setCustomerParentId(Integer customerParentId) {
		this.customerParentId = customerParentId;
	}
}
