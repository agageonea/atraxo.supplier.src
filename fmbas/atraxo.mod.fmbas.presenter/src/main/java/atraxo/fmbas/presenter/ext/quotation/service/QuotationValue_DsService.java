/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.quotation.service;

import java.math.RoundingMode;
import java.util.List;

import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import atraxo.fmbas.presenter.impl.quotation.model.QuotationValue_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class QuotationValue_DsService extends AbstractEntityDsService<QuotationValue_Ds, QuotationValue_Ds, Object, QuotationValue>
		implements IDsService<QuotationValue_Ds, QuotationValue_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<QuotationValue_Ds, QuotationValue_Ds, Object> builder, List<QuotationValue_Ds> result) throws Exception {

		super.postFind(builder, result);

		for (QuotationValue_Ds qv : result) {

			qv.setRate(qv.getRate().setScale(qv.getDecimals(), RoundingMode.HALF_UP));

		}
	}
}
