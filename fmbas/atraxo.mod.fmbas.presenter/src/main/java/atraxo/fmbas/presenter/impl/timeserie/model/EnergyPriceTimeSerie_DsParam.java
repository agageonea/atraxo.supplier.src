/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.timeserie.model;

/**
 * Generated code. Do not modify in this file.
 */
public class EnergyPriceTimeSerie_DsParam {

	public static final String f_approvalNote = "approvalNote";
	public static final String f_submitForApprovalDescription = "submitForApprovalDescription";
	public static final String f_submitForApprovalResult = "submitForApprovalResult";
	public static final String f_approveRejectResult = "approveRejectResult";

	private String approvalNote;

	private String submitForApprovalDescription;

	private Boolean submitForApprovalResult;

	private String approveRejectResult;

	public String getApprovalNote() {
		return this.approvalNote;
	}

	public void setApprovalNote(String approvalNote) {
		this.approvalNote = approvalNote;
	}

	public String getSubmitForApprovalDescription() {
		return this.submitForApprovalDescription;
	}

	public void setSubmitForApprovalDescription(
			String submitForApprovalDescription) {
		this.submitForApprovalDescription = submitForApprovalDescription;
	}

	public Boolean getSubmitForApprovalResult() {
		return this.submitForApprovalResult;
	}

	public void setSubmitForApprovalResult(Boolean submitForApprovalResult) {
		this.submitForApprovalResult = submitForApprovalResult;
	}

	public String getApproveRejectResult() {
		return this.approveRejectResult;
	}

	public void setApproveRejectResult(String approveRejectResult) {
		this.approveRejectResult = approveRejectResult;
	}
}
