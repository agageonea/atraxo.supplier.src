/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.geo.model;

import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Locations.class, sort = {
		@SortField(field = LocationsLov_Ds.F_CODE),
		@SortField(field = LocationsLov_Ds.F_IATACODE)})
public class LocationsLov_Ds extends AbstractLov_Ds<Locations> {

	public static final String ALIAS = "fmbas_LocationsLov_Ds";

	public static final String F_NAME = "name";
	public static final String F_CODE = "code";
	public static final String F_IATACODE = "iataCode";
	public static final String F_ICAOCODE = "icaoCode";
	public static final String F_ISAIRPORT = "isAirport";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";

	@DsField
	private String name;

	@DsField
	private String code;

	@DsField
	private String iataCode;

	@DsField
	private String icaoCode;

	@DsField
	private Boolean isAirport;

	@DsField(jpqlFilter = " e.validTo >= :validFrom ")
	private Date validFrom;

	@DsField(jpqlFilter = " e.validFrom <= :validTo ")
	private Date validTo;

	/**
	 * Default constructor
	 */
	public LocationsLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public LocationsLov_Ds(Locations e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public String getIcaoCode() {
		return this.icaoCode;
	}

	public void setIcaoCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}

	public Boolean getIsAirport() {
		return this.isAirport;
	}

	public void setIsAirport(Boolean isAirport) {
		this.isAirport = isAirport;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
}
