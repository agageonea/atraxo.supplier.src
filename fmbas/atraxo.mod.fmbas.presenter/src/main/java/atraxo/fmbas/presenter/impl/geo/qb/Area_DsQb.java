/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.geo.qb;

import atraxo.fmbas.presenter.impl.geo.model.Area_Ds;
import atraxo.fmbas.presenter.impl.geo.model.Area_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class Area_DsQb
		extends
			QueryBuilderWithJpql<Area_Ds, Area_Ds, Area_DsParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getLocationId() != null
				&& !"".equals(this.params.getLocationId())) {
			addFilterCondition("   e.id in ( select ar.id from  Area ar, IN (ar.locations) c where c.id = :locationId ) ");
			addCustomFilterItem("locationId", this.params.getLocationId());
		}
	}
}
