/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.vat.model;

import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.vat.Vat;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Vat.class, sort = {@SortField(field = Vat_Ds.F_COUNTRYNAME)})
@RefLookups({@RefLookup(refId = Vat_Ds.F_COUNTRYID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Vat_Ds.F_COUNTRYCODE)})})
public class Vat_Ds extends AbstractDs_Ds<Vat> {

	public static final String ALIAS = "fmbas_Vat_Ds";

	public static final String F_COUNTRYID = "countryId";
	public static final String F_COUNTRYCODE = "countryCode";
	public static final String F_COUNTRYNAME = "countryName";
	public static final String F_CODE = "code";

	@DsField(join = "left", path = "country.id")
	private Integer countryId;

	@DsField(join = "left", path = "country.code")
	private String countryCode;

	@DsField(join = "left", path = "country.name")
	private String countryName;

	@DsField
	private String code;

	/**
	 * Default constructor
	 */
	public Vat_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Vat_Ds(Vat e) {
		super(e);
	}

	public Integer getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
