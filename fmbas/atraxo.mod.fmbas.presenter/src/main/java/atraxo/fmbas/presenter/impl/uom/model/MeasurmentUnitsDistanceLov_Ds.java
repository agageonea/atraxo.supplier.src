/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.uom.model;

import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.model.IModelWithId;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Unit.class)
public class MeasurmentUnitsDistanceLov_Ds extends AbstractLov_Ds<Unit>
		implements
			IModelWithId<Integer> {

	public static final String ALIAS = "fmbas_MeasurmentUnitsDistanceLov_Ds";

	public static final String F_ID = "id";
	public static final String F_CODE = "code";
	public static final String F_IATACODE = "iataCode";
	public static final String F_NAME = "name";
	public static final String F_UNITTYPEIND = "unitTypeInd";

	@DsField
	private Integer id;

	@DsField
	private String code;

	@DsField
	private String iataCode;

	@DsField
	private String name;

	@DsField(path = "unittypeInd")
	private UnitType unitTypeInd;

	/**
	 * Default constructor
	 */
	public MeasurmentUnitsDistanceLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public MeasurmentUnitsDistanceLov_Ds(Unit e) {
		super(e);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UnitType getUnitTypeInd() {
		return this.unitTypeInd;
	}

	public void setUnitTypeInd(UnitType unitTypeInd) {
		this.unitTypeInd = unitTypeInd;
	}
}
