/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.profile.model;

import atraxo.fmbas.domain.impl.profile.CompanyProfile;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = CompanyProfile.class)
public class CompanyProfileLov_Ds extends AbstractLov_Ds<CompanyProfile> {

	public static final String ALIAS = "fmbas_CompanyProfileLov_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";

	@DsField
	private String code;

	@DsField
	private String name;

	/**
	 * Default constructor
	 */
	public CompanyProfileLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public CompanyProfileLov_Ds(CompanyProfile e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
