/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.profile.model;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.profile.BankAccount;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = BankAccount.class, sort = {@SortField(field = BankAccount_Ds.F_ACCOUNTHOLDER)})
@RefLookups({
		@RefLookup(refId = BankAccount_Ds.F_CURRENCYID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = BankAccount_Ds.F_CURRENCYCODE)}),
		@RefLookup(refId = BankAccount_Ds.F_SUBSIDIARYID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = BankAccount_Ds.F_SUBSIDIARYCODE)})})
public class BankAccount_Ds extends AbstractDs_Ds<BankAccount> {

	public static final String ALIAS = "fmbas_BankAccount_Ds";

	public static final String F_ACCOUNTNUMBER = "accountNumber";
	public static final String F_ACCOUNTHOLDER = "accountHolder";
	public static final String F_BANKNAME = "bankName";
	public static final String F_BANKADRESS = "bankAdress";
	public static final String F_BRANCHCODE = "branchCode";
	public static final String F_BRANCHNAME = "branchName";
	public static final String F_IBANCODE = "ibanCode";
	public static final String F_SWIFTCODE = "swiftCode";
	public static final String F_CUSTOMERCODES = "customerCodes";
	public static final String F_CURRENCYCODE = "currencyCode";
	public static final String F_SUBSIDIARYCODE = "subsidiaryCode";
	public static final String F_CURRENCYID = "currencyId";
	public static final String F_SUBSIDIARYID = "subsidiaryId";

	@DsField
	private String accountNumber;

	@DsField
	private String accountHolder;

	@DsField
	private String bankName;

	@DsField
	private String bankAdress;

	@DsField
	private String branchCode;

	@DsField
	private String branchName;

	@DsField
	private String ibanCode;

	@DsField
	private String swiftCode;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private String customerCodes;

	@DsField(join = "left", path = "currency.code")
	private String currencyCode;

	@DsField(join = "left", path = "subsidiary.code")
	private String subsidiaryCode;

	@DsField(join = "left", path = "currency.id")
	private Integer currencyId;

	@DsField(join = "left", path = "subsidiary.id")
	private Integer subsidiaryId;

	/**
	 * Default constructor
	 */
	public BankAccount_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public BankAccount_Ds(BankAccount e) {
		super(e);
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountHolder() {
		return this.accountHolder;
	}

	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAdress() {
		return this.bankAdress;
	}

	public void setBankAdress(String bankAdress) {
		this.bankAdress = bankAdress;
	}

	public String getBranchCode() {
		return this.branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getIbanCode() {
		return this.ibanCode;
	}

	public void setIbanCode(String ibanCode) {
		this.ibanCode = ibanCode;
	}

	public String getSwiftCode() {
		return this.swiftCode;
	}

	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	public String getCustomerCodes() {
		return this.customerCodes;
	}

	public void setCustomerCodes(String customerCodes) {
		this.customerCodes = customerCodes;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getSubsidiaryCode() {
		return this.subsidiaryCode;
	}

	public void setSubsidiaryCode(String subsidiaryCode) {
		this.subsidiaryCode = subsidiaryCode;
	}

	public Integer getCurrencyId() {
		return this.currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public Integer getSubsidiaryId() {
		return this.subsidiaryId;
	}

	public void setSubsidiaryId(Integer subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}
}
