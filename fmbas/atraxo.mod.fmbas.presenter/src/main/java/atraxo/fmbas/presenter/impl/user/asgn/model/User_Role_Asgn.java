/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.user.asgn.model;

import atraxo.fmbas.domain.impl.user.RoleSupp;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;
import seava.j4e.presenter.model.AbstractAsgnModel;

@Ds(entity = RoleSupp.class, jpqlWhere = "e.active = true", sort = {@SortField(field = User_Role_Asgn.f_code)})
public class User_Role_Asgn extends AbstractAsgnModel<RoleSupp> {

	public static final String ALIAS = "fmbas_User_Role_Asgn";

	public static final String f_id = "id";
	public static final String f_code = "code";
	public static final String f_name = "name";
	public static final String f_description = "description";

	@DsField(path = "id")
	private String id;

	@DsField(path = "code")
	private String code;

	@DsField(path = "name")
	private String name;

	@DsField(path = "description")
	private String description;

	public User_Role_Asgn() {
	}

	public User_Role_Asgn(RoleSupp e) {
		super();
		this.id = e.getId();
		this.code = e.getCode();
		this.name = e.getName();
		this.description = e.getDescription();
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
