package atraxo.fmbas.presenter.ext;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.ad.business.api.security.IMenuItemService;
import atraxo.ad.domain.impl.security.MenuItem;
import atraxo.fmbas.business.api.kpi.IKpiService;
import atraxo.fmbas.domain.impl.kpi.Kpi;
import seava.j4e.api.Constants;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.extensions.IExtensionContentProvider;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterBaseService;

public class ExtensionProviderKpis<M, F, P> extends AbstractPresenterBaseService implements IExtensionContentProvider {

	private final static Logger LOG = LoggerFactory.getLogger(ExtensionProviderKpis.class);

	@Override
	public String getContent(String targetFrame) throws Exception {
		return this.listKpis(targetFrame);
	}

	protected String listKpis(String targetFrame) throws Exception {

		IKpiService kpiService = (IKpiService) this.findEntityService(Kpi.class);
		IMenuItemService menuItemService = (IMenuItemService) this.findEntityService(MenuItem.class);
		JSONArray jsonArray = new JSONArray();

		try {
			MenuItem menuItem = menuItemService.findByFrame(targetFrame);
			List<Kpi> listKpi = kpiService.findByMenuItem(menuItem);
			if (listKpi.isEmpty()) {
				throw new ApplicationException(J4eErrorCode.DB_NO_RESULT, "No KPIs found.");
			}

			for (Kpi kpi : listKpi) {
				if (!kpi.getActive()) {
					continue;
				}
				IDsService<M, F, P> dsService = this.findDsService(kpi.getDsName());
				Method[] methods = dsService.getClass().getMethods();
				BigDecimal result = BigDecimal.ZERO;
				for (Method m : methods) {
					if (m.getName().equals(kpi.getMethodName())) {
						result = (BigDecimal) m.invoke(dsService);
						break;

					}
				}
				JSONObject json = new JSONObject();

				json.put("glyphCode", kpi.getGlyphCode());
				json.put("glyphFont", kpi.getGlyphFont());
				json.put("order", kpi.getKpiOrder());

				// TODO: set the title value based on the language stored in the cookie
				// if the language is "sys" the title should inherit the value from "getTitle()"
				// if the language is "en" the title should inherit the value from getTitleEn(), etc...

				json.put("title", kpi.getTitle());
				if (kpi.getDownLimit() == null) {
					json.put("color", Constants.COLOR_GRASS_GREEN);
				} else {
					if (result.compareTo(BigDecimal.valueOf(kpi.getDownLimit())) <= 0) {
						json.put("color", kpi.getReverse() ? Constants.COLOR_GRASS_GREEN : Constants.COLOR_RED);
					} else if (result.compareTo(BigDecimal.valueOf(kpi.getUpLimit())) >= 0) {
						json.put("color", kpi.getReverse() ? Constants.COLOR_RED : Constants.COLOR_GRASS_GREEN);
					} else {
						json.put("color", Constants.COLOR_YELLOW);
					}
				}

				json.put("active", kpi.getActive());

				DecimalFormat df = new DecimalFormat();
				DecimalFormatSymbols dfSymbols = new DecimalFormatSymbols();
				dfSymbols.setDecimalSeparator(Session.user.get().getSettings().getDecimalSeparator().charAt(0));
				dfSymbols.setGroupingSeparator(Session.user.get().getSettings().getThousandSeparator().charAt(0));
				df.setDecimalFormatSymbols(dfSymbols);

				json.put("value", df.format(result) + " " + kpi.getUnit());

				jsonArray.put(json);
			}
		} catch (ApplicationException apEx) {
			if (J4eErrorCode.DB_NO_RESULT.equals(apEx.getErrorCode())) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("No menu item found:  " + apEx.getMessage(), apEx);
				}
				LOG.info("No menu item found!");
			} else if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(apEx.getErrorCode())) {
				LOG.warn("Too many menu items found:  " + apEx.getMessage(), apEx);
			} else {
				throw apEx;
			}
		}
		return "_KPIS_ = " + jsonArray.toString();
	}

}
