/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.workflowTask.model;

import atraxo.fmbas.domain.impl.workflowTask.WorkflowTask;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.model.IModelWithId;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = WorkflowTask.class)
public class WorkflowTask_Ds extends AbstractDs_Ds<WorkflowTask>
		implements
			IModelWithId<Integer> {

	public static final String ALIAS = "fmbas_WorkflowTask_Ds";

	public static final String F_ID = "id";
	public static final String F_TASKID = "taskId";
	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_CANDIDATE = "candidate";
	public static final String F_ASSIGNEE = "assignee";
	public static final String F_OWNER = "owner";
	public static final String F_PROCESS = "process";
	public static final String F_WORKFLOWINSTANCEID = "workflowInstanceId";
	public static final String F_WORKFLOWTASKNOTE = "workflowTaskNote";
	public static final String F_WORKFLOWTASKACCEPTANCE = "workflowTaskAcceptance";
	public static final String F_GROUP = "group";
	public static final String F_DETAIL = "detail";
	public static final String F_SUBJECT = "subject";
	public static final String F_PERIODGROUP = "periodGroup";
	public static final String F_ENTITYID = "entityId";
	public static final String F_ENTITYFRAME = "entityFrame";
	public static final String F_ENTITYMODULE = "entityModule";

	@DsField
	private Integer id;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private String taskId;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private String name;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private String description;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private String candidate;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private String assignee;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private String owner;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private String process;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private Integer workflowInstanceId;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private String workflowTaskNote;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private Boolean workflowTaskAcceptance;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private String group;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private String detail;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private String subject;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private String periodGroup;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private Integer entityId;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private String entityFrame;

	@DsField(noInsert = true, noUpdate = true, fetch = false)
	private String entityModule;

	/**
	 * Default constructor
	 */
	public WorkflowTask_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public WorkflowTask_Ds(WorkflowTask e) {
		super(e);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTaskId() {
		return this.taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCandidate() {
		return this.candidate;
	}

	public void setCandidate(String candidate) {
		this.candidate = candidate;
	}

	public String getAssignee() {
		return this.assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getProcess() {
		return this.process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public Integer getWorkflowInstanceId() {
		return this.workflowInstanceId;
	}

	public void setWorkflowInstanceId(Integer workflowInstanceId) {
		this.workflowInstanceId = workflowInstanceId;
	}

	public String getWorkflowTaskNote() {
		return this.workflowTaskNote;
	}

	public void setWorkflowTaskNote(String workflowTaskNote) {
		this.workflowTaskNote = workflowTaskNote;
	}

	public Boolean getWorkflowTaskAcceptance() {
		return this.workflowTaskAcceptance;
	}

	public void setWorkflowTaskAcceptance(Boolean workflowTaskAcceptance) {
		this.workflowTaskAcceptance = workflowTaskAcceptance;
	}

	public String getGroup() {
		return this.group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getDetail() {
		return this.detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getPeriodGroup() {
		return this.periodGroup;
	}

	public void setPeriodGroup(String periodGroup) {
		this.periodGroup = periodGroup;
	}

	public Integer getEntityId() {
		return this.entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	public String getEntityFrame() {
		return this.entityFrame;
	}

	public void setEntityFrame(String entityFrame) {
		this.entityFrame = entityFrame;
	}

	public String getEntityModule() {
		return this.entityModule;
	}

	public void setEntityModule(String entityModule) {
		this.entityModule = entityModule;
	}
}
