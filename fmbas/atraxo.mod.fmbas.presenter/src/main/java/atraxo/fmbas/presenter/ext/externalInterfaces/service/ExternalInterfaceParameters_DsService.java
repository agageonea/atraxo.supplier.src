/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.externalInterfaces.service;

import java.util.List;
import java.util.Random;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceParameter;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamType;
import atraxo.fmbas.presenter.impl.externalInterfaces.model.ExternalInterfaceParameters_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service ExternalInterfaceParameters_DsService
 */
public class ExternalInterfaceParameters_DsService
		extends AbstractEntityDsService<ExternalInterfaceParameters_Ds, ExternalInterfaceParameters_Ds, Object, ExternalInterfaceParameter>
		implements IDsService<ExternalInterfaceParameters_Ds, ExternalInterfaceParameters_Ds, Object> {

	private static final String CHARS = "1234567890qwertyuiopasdfghjklzxcvbnm";

	@Override
	protected void postFind(IQueryBuilder<ExternalInterfaceParameters_Ds, ExternalInterfaceParameters_Ds, Object> builder,
			List<ExternalInterfaceParameters_Ds> result) throws Exception {
		for (ExternalInterfaceParameters_Ds ds : result) {
			if (ds.getReadOnly() != null && ds.getReadOnly() && JobParamType._MASKED_.equals(ds.getType())) {
				ds.setCurrentValue(this.randomString(ds.getCurrentValue().length()));
				ds.setDefaultValue(this.randomString(ds.getDefaultValue().length()));
			}
		}
	}

	private String randomString(int length) {
		Random rand = new Random();
		StringBuilder buf = new StringBuilder();
		for (int i = 0; i < length; i++) {
			buf.append(CHARS.charAt(rand.nextInt(CHARS.length())));
		}
		return buf.toString();
	}

}
