package atraxo.fmbas.presenter.ext.user.delegate;

import java.util.List;

import org.springframework.security.crypto.codec.Base64;

import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.action.result.IDsMarshaller;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterDelegate;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.presenter.impl.user.model.UserSupp_Ds;
import atraxo.fmbas.presenter.impl.user.model.UserSupp_DsParam;
import atraxo.fmbas.presenter.impl.workflowTask.model.WorkflowTask_Ds;
import atraxo.fmbas.presenter.impl.workflowTask.model.WorkflowTask_DsParam;

public class User_Pd extends AbstractPresenterDelegate {

	public void changePassword(UserSupp_Ds ds, UserSupp_DsParam params) throws Exception {

		String newPassword = params.getNewPassword();
		String confPassword = params.getConfirmPassword();

		if (newPassword == null || newPassword.equals("")) {
			throw new Exception("Password must not be empty!");
		}

		if (!newPassword.equals(confPassword)) {
			throw new Exception("New password is not confirmed correctly !");
		}

		((IUserSuppService) this.findEntityService(UserSupp.class)).doChangePassword(ds.getId(), newPassword);
	}

	public void saveAvatar(UserSupp_Ds ds, UserSupp_DsParam params) throws Exception {

		String avatarStr = params.getAvatar();

		IUserSuppService service = (IUserSuppService) this.findEntityService(UserSupp.class);
		UserSupp userSupp = service.findByCode(Session.user.get().getCode());
		byte[] picture = this.string2byteArray(avatarStr);
		userSupp.setPicture(picture);
		service.update(userSupp);
	}

	public byte[] string2byteArray(String imageStr) {
		if (imageStr == null || imageStr.length() < 1) {
			return null;
		}
		return Base64.decode(imageStr.replace(' ', '+').getBytes());
	}

	public void getUserActivities(UserSupp_Ds ds, UserSupp_DsParam params) throws Exception {
		IDsService<WorkflowTask_Ds, WorkflowTask_Ds, WorkflowTask_DsParam> dsSrv = this.findDsService(WorkflowTask_Ds.class);
		WorkflowTask_Ds wsDs = new WorkflowTask_Ds();
		WorkflowTask_DsParam wsDsParam = new WorkflowTask_DsParam();
		IQueryBuilder<WorkflowTask_Ds, WorkflowTask_Ds, WorkflowTask_DsParam> builder = dsSrv.createQueryBuilder().addFilter(wsDs)
				.addParams(wsDsParam);
		List<WorkflowTask_Ds> list = dsSrv.find(builder);
		IDsMarshaller<WorkflowTask_Ds, WorkflowTask_Ds, WorkflowTask_DsParam> marshaller = dsSrv.createMarshaller("json");
		String result = marshaller.writeListToString(list);
		params.setRpcResponse(result);
		params.setActivitiesSize(list.size());

	}
}
