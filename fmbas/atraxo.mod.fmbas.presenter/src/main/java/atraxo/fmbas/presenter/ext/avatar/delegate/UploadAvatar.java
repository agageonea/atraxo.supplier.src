package atraxo.fmbas.presenter.ext.avatar.delegate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.ad.domain.impl.security.User;
import atraxo.fmbas.business.api.avatars.IAvatarService;
import atraxo.fmbas.domain.impl.avatars.Avatar;
import seava.j4e.api.descriptor.IUploadedFileDescriptor;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.service.IFileUploadService;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class UploadAvatar extends AbstractPresenterDelegate implements IFileUploadService {

	public static final String KEY_ID = "id";
	public static final String KEY_NAME = "name";
	public static final String KEY_TYPEID = "typeId";
	public static final String KEY_USER_ID = "userId";

	private List<String> paramNames;

	public UploadAvatar() {
		super();
		paramNames = new ArrayList<String>();
		paramNames.add(KEY_ID);
		paramNames.add(KEY_NAME);
		paramNames.add(KEY_TYPEID);
		paramNames.add(KEY_USER_ID);
	}

	@Override
	public List<String> getParamNames() {
		return paramNames;
	}

	@Override
	public Map<String, Object> execute(IUploadedFileDescriptor fileDescriptor, InputStream inputStream, Map<String, String> params) throws Exception {

		String id = params.get(KEY_ID);
		String name = params.get(KEY_NAME);
		String typeId = params.get(KEY_TYPEID);
		String userId = params.get(KEY_USER_ID);

		Map<String, Object> result = new HashMap<String, Object>();

		IAvatarService srv = (IAvatarService) this.findEntityService(Avatar.class);

		AttachmentType attchType = srv.findById(typeId, AttachmentType.class);

		String path = attchType.getUploadPath();

		if (path == null || "".equals(path)) {
			throw new BusinessException(ErrorCode.G_FILE_INVALID_LOCATION, "No upload path specified for attachment type `" + attchType.getName()
					+ "`");
		}
		String extension = fileDescriptor.getOriginalName().substring(fileDescriptor.getOriginalName().lastIndexOf('.') + 1);
		
//		List<String> allowedExtensions = Arrays.asList("jpg", "jpeg", "gif", "png");
//		boolean contains = allowedExtensions.contains(extension);
//		
//		if (contains == true) {
			// create attachment record

			if (id == null || "".equals(id)) {

				Avatar a = new Avatar();
				a.setType(attchType);
				a.setUser(srv.findById(userId, User.class));			
				a.setFileName(fileDescriptor.getOriginalName());
				a.setName(name);
				a.setActive(true);
				a.setContentType(extension);

				srv.insert(a);

				id = a.getId();
			}

			// save file to disk

			String newFileName = id + "." + extension;
			File dir = new File(path);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			OutputStream outputStream = new FileOutputStream(path + "/" + newFileName);
			IOUtils.copy(inputStream, outputStream);
			outputStream.close();
			
			
//		}
//		else {
//			
//			throw new BusinessException(ErrorCode.G_FILE_EXTENSION_NOT_ALLOWED, "The file you're trying to upload is not an image!\r\nPlease upload a valid image file!");
//		}

		
		return result;
		

	}

}
