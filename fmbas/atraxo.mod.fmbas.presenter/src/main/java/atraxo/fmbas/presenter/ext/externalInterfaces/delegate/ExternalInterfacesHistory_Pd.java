package atraxo.fmbas.presenter.ext.externalInterfaces.delegate;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceHistoryService;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceHistory;
import atraxo.fmbas.presenter.impl.externalInterfaces.model.ExternalInterfaces_Ds;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class ExternalInterfacesHistory_Pd extends AbstractPresenterDelegate {

	public void empty(ExternalInterfaces_Ds ds) throws Exception {
		IExternalInterfaceHistoryService service = (IExternalInterfaceHistoryService) this.findEntityService(ExternalInterfaceHistory.class);
		service.empty(ds.getId());
	}

}
