/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.timeserie.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesApprovalStatus;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import atraxo.fmbas.presenter.impl.timeserie.model.EnergyPriceTimeSerie_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class EnergyPriceTimeSerie_DsService extends AbstractEntityDsService<EnergyPriceTimeSerie_Ds, EnergyPriceTimeSerie_Ds, Object, TimeSerie>
		implements IDsService<EnergyPriceTimeSerie_Ds, EnergyPriceTimeSerie_Ds, Object> {

	private static final Logger LOG = LoggerFactory.getLogger(EnergyPriceTimeSerie_DsService.class);

	@Autowired
	private ISystemParameterService sysParamService;

	@Autowired
	private IWorkflowInstanceEntityService workflowInstanceEntityService;

	@Autowired
	private IWorkflowBpmManager workflowBpmManager;

	@Autowired
	private ITimeSerieService timeSerieService;

	@Override
	protected void postFind(IQueryBuilder<EnergyPriceTimeSerie_Ds, EnergyPriceTimeSerie_Ds, Object> builder, List<EnergyPriceTimeSerie_Ds> result)
			throws Exception {
		super.postFind(builder, result);

		for (EnergyPriceTimeSerie_Ds ds : result) {
			// calculezi conform regulii
			String measure = ds.getCurrency1Code() + "/" + ds.getUnitCode();

			// Set the enable rules for approve and reject workflow buttons
			Boolean canBeCompleted = Boolean.FALSE;
			if (this.sysParamService.getEnergyPriceApprovalWorkflow()
					&& TimeSeriesApprovalStatus._AWAITING_APPROVAL_.equals(ds.getApprovalStatus())) {

				try {
					WorkflowInstanceEntity wie = this.workflowInstanceEntityService.findByBusinessKey(
							WorkflowNames.ENERGY_PRICE_TIME_SERIES_APPROVAL.getWorkflowName(), ds.getId(),
							ds._getEntity_().getClass().getSimpleName());
					canBeCompleted = this.workflowBpmManager.canComplete(wie.getWorkflowInstance().getId());
				} catch (Exception e) {
					LOG.warn("Error trying to find workflow instance!", e);
				}
			}

			TimeSerie ts = ds._getEntity_();

			ds.setMeasure(measure);
			ds.setCanBeCompleted(canBeCompleted);
			ds.setHasComponents(!CollectionUtils.isEmpty(ts.getCompositeTimeSeries()));
			Long rawTimeSerieCount = this.timeSerieService.countRawTimeSerieItems(ts);
			ds.setHasTimeSerieItems(rawTimeSerieCount > 0);
		}

	}

}
