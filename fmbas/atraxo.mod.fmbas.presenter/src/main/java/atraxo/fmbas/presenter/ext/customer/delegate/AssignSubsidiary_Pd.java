package atraxo.fmbas.presenter.ext.customer.delegate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.user.IUserSubsidiaryService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.presenter.impl.customer.model.SubsidiaryAssignList_Ds;
import atraxo.fmbas.presenter.impl.customer.model.SubsidiaryAssignList_DsParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class AssignSubsidiary_Pd extends AbstractPresenterDelegate {

	public void assign(List<SubsidiaryAssignList_Ds> dsList, SubsidiaryAssignList_DsParam param) throws Exception {
		IUserSubsidiaryService service = (IUserSubsidiaryService) this.findEntityService(UserSubsidiary.class);
		String userId = param.getUserId();
		List<UserSubsidiary> uSList = service.findByUserId(userId);
		for (SubsidiaryAssignList_Ds ds : dsList) {
			if (!this.isAssigned(ds, uSList)) {
				this.createUserSubsidiary(ds, userId, service);
			}
		}
		this.deleteDeassignedConections(dsList, uSList, service);
		this.manageDefaultSubsidiary(service, userId);
	}

	private void deleteDeassignedConections(List<SubsidiaryAssignList_Ds> dsList, List<UserSubsidiary> uSList, IUserSubsidiaryService service)
			throws BusinessException {
		List<Object> deleteList = new ArrayList<Object>();
		for (UserSubsidiary us : uSList) {
			if (this.canDelete(us, dsList)) {
				deleteList.add(us.getId());
			}
		}
		service.deleteByIds(deleteList);
	}

	private boolean canDelete(UserSubsidiary us, List<SubsidiaryAssignList_Ds> dsList) {
		for (SubsidiaryAssignList_Ds ds : dsList) {
			if (ds.getId().equals(us.getSubsidiary().getId())) {
				return false;
			}
		}
		return true;
	}

	private boolean isAssigned(SubsidiaryAssignList_Ds ds, List<UserSubsidiary> uSList) {
		for (UserSubsidiary us : uSList) {
			if (us.getSubsidiary().getId().equals(ds.getId())) {
				return true;
			}
		}
		return false;
	}

	private void createUserSubsidiary(SubsidiaryAssignList_Ds ds, String userId, IUserSubsidiaryService service) throws Exception {
		IUserSuppService userService = (IUserSuppService) this.findEntityService(UserSupp.class);
		ICustomerService subsidiaryService = (ICustomerService) this.findEntityService(Customer.class);
		Customer subsidiary = subsidiaryService.findById(ds.getId());
		UserSupp user = userService.findById(userId);
		UserSubsidiary us = new UserSubsidiary();
		us.setMainSubsidiary(false);
		us.setSubsidiary(subsidiary);
		us.setUser(user);
		service.insert(us);
	}

	private void manageDefaultSubsidiary(IUserSubsidiaryService service, String userId) throws Exception {
		List<UserSubsidiary> list = service.findByUserId(userId);
		if (CollectionUtils.isEmpty(list)) {
			return;
		}
		boolean isDefaultSubsidiary = false;
		UserSubsidiary loginedSubsidiary = null;
		for (UserSubsidiary us : list) {
			if (us.getMainSubsidiary()) {
				isDefaultSubsidiary = true;
			}
			if (us.getSubsidiary().getCode().equals(Session.user.get().getClientCode())) {
				loginedSubsidiary = us;
			}
		}
		if (!isDefaultSubsidiary) {
			UserSubsidiary defaultSub = null;
			if (loginedSubsidiary != null) {
				defaultSub = service.findById(loginedSubsidiary.getId());
			} else {
				Random rand = new Random();
				int randSub = rand.nextInt(list.size());
				defaultSub = service.findById(list.get(randSub).getId());
			}
			defaultSub.setMainSubsidiary(true);
			service.update(defaultSub);
		}
	}

}
