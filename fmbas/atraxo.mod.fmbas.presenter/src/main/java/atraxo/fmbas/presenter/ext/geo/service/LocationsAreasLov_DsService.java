/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package. This file is subject to the
 * terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.geo.service;

import java.util.List;

import atraxo.fmbas.business.api.geo.IAreaService;
import atraxo.fmbas.domain.impl.fmbas_type.GeoType;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.presenter.impl.geo.model.LocationsAreasLov_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class LocationsAreasLov_DsService extends AbstractEntityDsService<LocationsAreasLov_Ds, LocationsAreasLov_Ds, Object, Locations> implements
		IDsService<LocationsAreasLov_Ds, LocationsAreasLov_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<LocationsAreasLov_Ds, LocationsAreasLov_Ds, Object> builder, List<LocationsAreasLov_Ds> result)
			throws Exception {
		super.postFind(builder, result);

		IAreaService srv = (IAreaService) this.findEntityService(Area.class);

		List<Area> list = srv.findActive();
		for (LocationsAreasLov_Ds ds : result) {
			ds.setType(GeoType._LOCATION_);
		}
		for (Area a : list) {
			LocationsAreasLov_Ds areaDs = new LocationsAreasLov_Ds();
			areaDs.setId(a.getId());
			areaDs.setCode(a.getCode());
			areaDs.setName(a.getName());
			areaDs.setType(GeoType._AREA_);
			result.add(areaDs);
		}
	}

	@Override
	public Long count(IQueryBuilder<LocationsAreasLov_Ds, LocationsAreasLov_Ds, Object> builder) throws Exception {
		Long count = super.count(builder);
		IAreaService srv = (IAreaService) this.findEntityService(Area.class);
		List<Area> list = srv.findActive();
		count += list.size();
		return count;
	}

}
