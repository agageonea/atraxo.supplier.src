/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.suppliers.service;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.suppliers.ISuppliersService;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.presenter.ext.dashboard.service.Widget_DsService;
import atraxo.fmbas.presenter.impl.suppliers.model.Suppliers_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class Suppliers_DsService extends AbstractEntityDsService<Suppliers_Ds, Suppliers_Ds, Object, Suppliers>
		implements IDsService<Suppliers_Ds, Suppliers_Ds, Object> {

	private final static Logger LOG = LoggerFactory.getLogger(Widget_DsService.class);

	@Override
	protected void postFind(IQueryBuilder<Suppliers_Ds, Suppliers_Ds, Object> builder, List<Suppliers_Ds> result) throws Exception {

		super.postFind(builder, result);

		ISuppliersService service = (ISuppliersService) this.findEntityService(Suppliers.class);
		Contacts ct = null;

		for (Suppliers_Ds ds : result) {
			Suppliers supplier = ds._getEntity_();
			if (supplier != null) {
				try {
					ct = service.getPrimaryContact(supplier);
					ds.setContactName(ct.getFirstName() + " " + ct.getLastName());
					ds.setContactEmail(ct.getEmail());
					ds.setContactPhone(ct.getBusinessPhone());
				} catch (BusinessException e) {
					// do nothing
					LOG.info("Could not get the primary contact, will do nothing !", e);
				}
			}
			ds.setRoles(this.getRoles(ds));
			ds.setExposure(new BigDecimal(0));
		}
	}

	@Override
	protected void postInsert(List<Suppliers_Ds> list, Object params) throws Exception {
		super.postInsert(list, params);

		for (Suppliers_Ds ds : list) {
			ds.setRoles(this.getRoles(ds));
		}
	}

	/**
	 * Build the roles string.
	 *
	 * @param s
	 * @return
	 */
	private String getRoles(Suppliers_Ds s) {
		String roles = "";
		if (s.getFuelSupplier() != null && s.getFuelSupplier()) {
			roles += "Fuel provider";
		}
		if (s.getIplAgent() != null && s.getIplAgent()) {
			roles += roles.matches("") ? "IPL Agent" : "/" + "IPL Agent";
		}
		if (s.getFixedBaseOperator() != null && s.getFixedBaseOperator()) {
			roles += roles.matches("") ? "Fixed base operator" : "/" + "Fixed base operator";
		}
		return roles;
	}
}
