/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.uom.model;

import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;
import seava.j4e.api.model.IModelWithId;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Unit.class, sort = {@SortField(field = Units_Ds.F_CODE)})
public class Units_Ds extends AbstractDs_Ds<Unit>
		implements
			IModelWithId<Integer> {

	public static final String ALIAS = "fmbas_Units_Ds";

	public static final String F_ID = "id";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_UNITTYPEIND = "unittypeInd";
	public static final String F_FACTOR = "factor";
	public static final String F_IATACODE = "iataCode";
	public static final String F_ACTIVE = "active";

	@DsField
	private Integer id;

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private UnitType unittypeInd;

	@DsField
	private BigDecimal factor;

	@DsField
	private String iataCode;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public Units_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Units_Ds(Unit e) {
		super(e);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UnitType getUnittypeInd() {
		return this.unittypeInd;
	}

	public void setUnittypeInd(UnitType unittypeInd) {
		this.unittypeInd = unittypeInd;
	}

	public BigDecimal getFactor() {
		return this.factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
