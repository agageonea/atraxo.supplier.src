/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.timeserie.model;

import atraxo.fmbas.domain.impl.timeserie.TimeSerieAverage;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = TimeSerieAverage.class)
public class TimeSerieAvgMthdLov_Ds extends AbstractLov_Ds<TimeSerieAverage> {

	public static final String ALIAS = "fmbas_TimeSerieAvgMthdLov_Ds";

	public static final String F_ACTIVE = "active";
	public static final String F_TSERIEID = "tserieId";
	public static final String F_TSERIENAME = "tserieName";
	public static final String F_AVERAGINGMETHODID = "averagingMethodId";
	public static final String F_AVERAGINGMETHODCODE = "averagingMethodCode";
	public static final String F_AVERAGINGMETHODNAME = "averagingMethodName";

	@DsField
	private Boolean active;

	@DsField(join = "left", path = "tserie.id")
	private Integer tserieId;

	@DsField(join = "left", path = "tserie.serieName")
	private String tserieName;

	@DsField(join = "left", path = "averagingMethod.id")
	private Integer averagingMethodId;

	@DsField(join = "left", path = "averagingMethod.code")
	private String averagingMethodCode;

	@DsField(join = "left", path = "averagingMethod.name")
	private String averagingMethodName;

	/**
	 * Default constructor
	 */
	public TimeSerieAvgMthdLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public TimeSerieAvgMthdLov_Ds(TimeSerieAverage e) {
		super(e);
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getTserieId() {
		return this.tserieId;
	}

	public void setTserieId(Integer tserieId) {
		this.tserieId = tserieId;
	}

	public String getTserieName() {
		return this.tserieName;
	}

	public void setTserieName(String tserieName) {
		this.tserieName = tserieName;
	}

	public Integer getAveragingMethodId() {
		return this.averagingMethodId;
	}

	public void setAveragingMethodId(Integer averagingMethodId) {
		this.averagingMethodId = averagingMethodId;
	}

	public String getAveragingMethodCode() {
		return this.averagingMethodCode;
	}

	public void setAveragingMethodCode(String averagingMethodCode) {
		this.averagingMethodCode = averagingMethodCode;
	}

	public String getAveragingMethodName() {
		return this.averagingMethodName;
	}

	public void setAveragingMethodName(String averagingMethodName) {
		this.averagingMethodName = averagingMethodName;
	}
}
