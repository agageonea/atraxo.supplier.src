/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.exchangerate.service;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import atraxo.fmbas.business.api.exchangerate.IExchangeRateValueService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;
import atraxo.fmbas.presenter.impl.exchangerate.model.ExchangeRate_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ExchangeRate_DsService extends AbstractEntityDsService<ExchangeRate_Ds, ExchangeRate_Ds, Object, ExchangeRate>
		implements IDsService<ExchangeRate_Ds, ExchangeRate_Ds, Object> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExchangeRate_DsService.class);

	@Autowired
	private IExchangeRateValueService srv;

	@Override
	protected void postFind(IQueryBuilder<ExchangeRate_Ds, ExchangeRate_Ds, Object> builder, List<ExchangeRate_Ds> result) throws Exception {

		super.postFind(builder, result);

		for (ExchangeRate_Ds qv : result) {

			List<ExchangeRateValue> list = this.srv.findByExchRateId(qv.getId());
			if (!CollectionUtils.isEmpty(list)) {
				list.sort((o1, o2) -> o1.getVldFrDtRef().compareTo(o2.getVldFrDtRef()));
				qv.setFirstUsage(list.get(0).getVldFrDtRef());
				qv.setLastUsage(list.get(list.size() - 1).getVldToDtRef());
			}

		}
	}

	/**
	 * @param firstDate
	 * @return
	 */

	// Sets first day of the month for Monthly Calendar and Monthly trade

	private Date getFirstUsageMonth(Date firstDate) {

		Calendar calFirst = Calendar.getInstance();
		calFirst.setTime(firstDate);
		calFirst.set(Calendar.DAY_OF_MONTH, 1);
		return calFirst.getTime();
	}

	/**
	 * @param lastDate
	 * @return
	 */

	// Sets last day of the month for Monthly Calendar and Monthly trade

	private Date getLastUsageMonth(Date lastDate) {

		Calendar calLast = Calendar.getInstance();
		calLast.setTime(lastDate);
		calLast.set(Calendar.DAY_OF_MONTH, calLast.getActualMaximum(Calendar.DAY_OF_MONTH));
		return calLast.getTime();
	}

	/**
	 * @param firstDate
	 * @return
	 */

	// Sets first day of the week for Weekly Calendar and Weekly trade

	private Date getFirstUsageWeek(Date firstDate) {
		Calendar calFirst = Calendar.getInstance();
		calFirst.setTime(firstDate);
		calFirst.setFirstDayOfWeek(Calendar.MONDAY);
		if (calFirst.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
			Integer addDay = calFirst.getActualMinimum(Calendar.DAY_OF_WEEK);
			calFirst.add(Calendar.DAY_OF_YEAR, -(calFirst.get(Calendar.DAY_OF_WEEK) - addDay) + 2);

		} else {
			calFirst.add(Calendar.DAY_OF_YEAR, 1);
		}
		return calFirst.getTime();
	}

	/**
	 * @param lastDate
	 * @return
	 */

	private Date getLastUsageWeek(Date lastDate) {
		Calendar calLast = Calendar.getInstance();
		calLast.setTime(lastDate);
		if (calLast.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
			Integer addDay = calLast.getActualMaximum(Calendar.DAY_OF_WEEK);
			calLast.add(Calendar.DAY_OF_YEAR, (addDay - calLast.get(Calendar.DAY_OF_WEEK)) + 2);

		} else {
			calLast.add(Calendar.DAY_OF_YEAR, 1);
		}
		return calLast.getTime();
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void deleteByIds(List<Object> ids) throws Exception {
		try {
			List<ExchangeRate> list = this.getEntityService().findByIds(ids);
			this.getEntityService().delete(list);
		} catch (Exception e) {
			LOGGER.error("Could not delete the objects by ids because they are used by another entities !", e);
			throw new BusinessException(BusinessErrorCode.USED_BY_ANOTHER_ENTITY_EXCHANGE_RATE,
					BusinessErrorCode.USED_BY_ANOTHER_ENTITY_EXCHANGE_RATE.getErrMsg());
		}
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void deleteById(Object id) throws Exception {
		this.deleteByIds(Arrays.asList(id));
	}

}
