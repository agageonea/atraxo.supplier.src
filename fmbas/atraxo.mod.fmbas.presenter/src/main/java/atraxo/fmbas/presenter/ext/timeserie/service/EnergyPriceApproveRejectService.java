/**
 *
 */
package atraxo.fmbas.presenter.ext.timeserie.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.presenter.impl.timeserie.model.EnergyPriceTimeSerie_Ds;
import atraxo.fmbas.presenter.impl.timeserie.model.EnergyPriceTimeSerie_DsParam;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public class EnergyPriceApproveRejectService extends AbstractTimeSeriesApproveRejectService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnergyPriceApproveRejectService.class);
	@Autowired
	private ISystemParameterService paramSrv;

	/**
	 * Submits for approval an Energy Price time series.
	 *
	 * @param ds
	 * @param params
	 * @throws Exception
	 */
	public void submitForApproval(EnergyPriceTimeSerie_Ds ds, EnergyPriceTimeSerie_DsParam params) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START submitForApproval.");
		}

		this.submitForApprovalTimeSerie(ds.getId(), params.getApprovalNote());

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END submitForApproval.");
		}
	}

	/**
	 * Approves current Energy Time Series workflow task
	 *
	 * @param data
	 * @param param
	 * @throws Exception
	 */
	public void approve(EnergyPriceTimeSerie_Ds data, EnergyPriceTimeSerie_DsParam params) throws BusinessException {
		this.completeCurrentTask(data.getId(), true, params.getApprovalNote());
	}

	/**
	 * Rejects current Energy Time Serie workflow task
	 *
	 * @param data
	 * @param param
	 * @throws Exception
	 */
	public void reject(EnergyPriceTimeSerie_Ds data, EnergyPriceTimeSerie_DsParam params) throws BusinessException {
		this.completeCurrentTask(data.getId(), false, params.getApprovalNote());
	}

	/**
	 * Approves current Energy Time Serie workflow task
	 *
	 * @param data
	 * @param param
	 * @throws Exception
	 */
	public void approveList(List<EnergyPriceTimeSerie_Ds> data, EnergyPriceTimeSerie_DsParam params) {
		List<String> failedToApproveTimeSeries = new ArrayList<>();
		for (EnergyPriceTimeSerie_Ds energyPriceTimeSerie : data) {
			try {
				this.approve(energyPriceTimeSerie, params);
			} catch (Exception e) {
				LOGGER.error("Error", e);
				failedToApproveTimeSeries.add(energyPriceTimeSerie.getSerieName() + " : " + e.getMessage());
			}
		}
		params.setApproveRejectResult(this.buildApproveFeedbackMessage(failedToApproveTimeSeries, data.size()));
	}

	/**
	 * Rejects current Energy Time Serie workflow task
	 *
	 * @param data
	 * @param param
	 * @throws Exception
	 */
	public void rejectList(List<EnergyPriceTimeSerie_Ds> data, EnergyPriceTimeSerie_DsParam params) {
		List<String> failedToApproveTimeSeries = new ArrayList<>();
		for (EnergyPriceTimeSerie_Ds energyPriceTimeSerie : data) {
			try {
				this.reject(energyPriceTimeSerie, params);
			} catch (Exception e) {
				LOGGER.error("Error", e);
				failedToApproveTimeSeries.add(energyPriceTimeSerie.getSerieName() + " : " + e.getMessage());
			}
		}
		params.setApproveRejectResult(this.buildRejectFeedbackMessage(failedToApproveTimeSeries, data.size()));
	}

	@Override
	protected WorkflowNames getWorkflowNameFromTimeSerie() {
		return WorkflowNames.ENERGY_PRICE_TIME_SERIES_APPROVAL;
	}

	@Override
	protected String getTimeSerieType() {
		return "Energy price";
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.presenter.ext.timeserie.service.AbstractTimeSeriesApproveRejectService#checkWorkflowIsEnabled()
	 */
	@Override
	protected void checkWorkflowIsEnabled() throws BusinessException {
		if (!this.paramSrv.getEnergyPriceApprovalWorkflow()) { // the system param for the workflow is false
			throw new BusinessException(BusinessErrorCode.ENERGY_RATE_TIME_SERIE_WORKFLOW_NO_SYS_PARAM,
					BusinessErrorCode.ENERGY_RATE_TIME_SERIE_WORKFLOW_NO_SYS_PARAM.getErrMsg());
		}

	}

}
