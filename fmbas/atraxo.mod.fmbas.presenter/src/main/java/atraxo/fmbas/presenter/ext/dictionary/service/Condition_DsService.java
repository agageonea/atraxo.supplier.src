/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.dictionary.service;

import atraxo.fmbas.domain.impl.dictionary.Condition;
import atraxo.fmbas.presenter.impl.dictionary.model.Condition_Ds;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service Condition_DsService
 */
public class Condition_DsService
		extends
			AbstractEntityDsService<Condition_Ds, Condition_Ds, Object, Condition>
		implements
			IDsService<Condition_Ds, Condition_Ds, Object> {

	// Implement me ...

}
