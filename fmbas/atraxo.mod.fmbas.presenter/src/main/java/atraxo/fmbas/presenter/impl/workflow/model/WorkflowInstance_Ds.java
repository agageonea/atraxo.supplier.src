/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.workflow.model;

import atraxo.fmbas.domain.impl.fmbas_type.WorkflowInstanceStatus;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = WorkflowInstance.class, sort = {@SortField(field = WorkflowInstance_Ds.F_STARTEXECUTIONDATE, desc = true)})
@RefLookups({@RefLookup(refId = WorkflowInstance_Ds.F_WORKFLOWID, namedQuery = Workflow.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = WorkflowInstance_Ds.F_WORKFLOWCODE)})})
public class WorkflowInstance_Ds extends AbstractDs_Ds<WorkflowInstance> {

	public static final String ALIAS = "fmbas_WorkflowInstance_Ds";

	public static final String F_WORKFLOWID = "workflowId";
	public static final String F_WORKFLOWCODE = "workflowCode";
	public static final String F_STARTEXECUTIONUSER = "startExecutionUser";
	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_STATUS = "status";
	public static final String F_STEP = "step";
	public static final String F_RESULT = "result";
	public static final String F_STARTEXECUTIONDATE = "startExecutionDate";
	public static final String F_WORKFLOWVERSION = "workflowVersion";

	@DsField(join = "left", path = "workflow.id")
	private Integer workflowId;

	@DsField(join = "left", path = "workflow.code")
	private String workflowCode;

	@DsField(join = "left", path = "startExecutionUser.loginName")
	private String startExecutionUser;

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private WorkflowInstanceStatus status;

	@DsField
	private String step;

	@DsField
	private String result;

	@DsField
	private Date startExecutionDate;

	@DsField
	private Integer workflowVersion;

	/**
	 * Default constructor
	 */
	public WorkflowInstance_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public WorkflowInstance_Ds(WorkflowInstance e) {
		super(e);
	}

	public Integer getWorkflowId() {
		return this.workflowId;
	}

	public void setWorkflowId(Integer workflowId) {
		this.workflowId = workflowId;
	}

	public String getWorkflowCode() {
		return this.workflowCode;
	}

	public void setWorkflowCode(String workflowCode) {
		this.workflowCode = workflowCode;
	}

	public String getStartExecutionUser() {
		return this.startExecutionUser;
	}

	public void setStartExecutionUser(String startExecutionUser) {
		this.startExecutionUser = startExecutionUser;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public WorkflowInstanceStatus getStatus() {
		return this.status;
	}

	public void setStatus(WorkflowInstanceStatus status) {
		this.status = status;
	}

	public String getStep() {
		return this.step;
	}

	public void setStep(String step) {
		this.step = step;
	}

	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Date getStartExecutionDate() {
		return this.startExecutionDate;
	}

	public void setStartExecutionDate(Date startExecutionDate) {
		this.startExecutionDate = startExecutionDate;
	}

	public Integer getWorkflowVersion() {
		return this.workflowVersion;
	}

	public void setWorkflowVersion(Integer workflowVersion) {
		this.workflowVersion = workflowVersion;
	}
}
