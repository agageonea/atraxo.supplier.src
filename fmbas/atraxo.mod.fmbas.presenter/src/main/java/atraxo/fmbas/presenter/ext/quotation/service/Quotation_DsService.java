/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.quotation.service;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import atraxo.fmbas.business.api.quotation.IQuotationValueService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import atraxo.fmbas.presenter.impl.quotation.model.Quotation_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class Quotation_DsService extends AbstractEntityDsService<Quotation_Ds, Quotation_Ds, Object, Quotation>
		implements IDsService<Quotation_Ds, Quotation_Ds, Object> {

	private static final Logger LOGGER = LoggerFactory.getLogger(Quotation_DsService.class);

	@Autowired
	private IQuotationValueService srv;

	@Override
	protected void postFind(IQueryBuilder<Quotation_Ds, Quotation_Ds, Object> builder, List<Quotation_Ds> result) throws Exception {

		super.postFind(builder, result);

		for (Quotation_Ds qv : result) {
			List<QuotationValue> list = this.srv.findByQuotationId(qv.getId());
			if (!CollectionUtils.isEmpty(list)) {
				list.sort((o1, o2) -> o1.getValidFromDate().compareTo(o2.getValidFromDate()));
				qv.setFirstUsage(list.get(0).getValidFromDate());
				qv.setLastUsage(list.get(list.size() - 1).getValidToDate());
			}
		}
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void deleteByIds(List<Object> ids) throws Exception {
		List<Quotation> list = this.getEntityService().findByIds(ids);
		this.getEntityService().delete(list);
		try {
			this.getEntityService().getEntityManager().flush();
		} catch (Exception e) {
			LOGGER.error("Could not deleted entities by ids because they are used by another entities !", e);
			throw new BusinessException(BusinessErrorCode.USED_BY_ANOTHER_ENTITY_QUOTATION,
					BusinessErrorCode.USED_BY_ANOTHER_ENTITY_QUOTATION.getErrMsg());
		}
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void deleteById(Object id) throws Exception {
		this.deleteByIds(Arrays.asList(id));
	}
}
