/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.customer.service;

import java.util.List;

import atraxo.fmbas.business.api.user.IUserSubsidiaryService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;
import atraxo.fmbas.presenter.impl.customer.model.SubsidiaryAssignList_Ds;
import atraxo.fmbas.presenter.impl.customer.model.SubsidiaryAssignList_DsParam;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class SubsidiaryAssignList_DsService extends
		AbstractEntityDsService<SubsidiaryAssignList_Ds, SubsidiaryAssignList_Ds, SubsidiaryAssignList_DsParam, Customer> implements
		IDsService<SubsidiaryAssignList_Ds, SubsidiaryAssignList_Ds, SubsidiaryAssignList_DsParam> {

	@Override
	protected void postFind(IQueryBuilder<SubsidiaryAssignList_Ds, SubsidiaryAssignList_Ds, SubsidiaryAssignList_DsParam> builder,
			List<SubsidiaryAssignList_Ds> result) throws Exception {
		super.postFind(builder, result);
		IUserSubsidiaryService service = (IUserSubsidiaryService) this.findEntityService(UserSubsidiary.class);
		String userId = builder.getParams().getUserId();
		List<UserSubsidiary> usList = service.findByUserId(userId);
		for (UserSubsidiary us : usList) {
			this.setAssigned(us, result);
		}
	}

	private void setAssigned(UserSubsidiary us, List<SubsidiaryAssignList_Ds> result) {
		for (SubsidiaryAssignList_Ds ds : result) {
			if (ds.getId().equals(us.getSubsidiary().getId())) {
				ds.setIsSelected(true);
				return;
			}
		}
	}

}
