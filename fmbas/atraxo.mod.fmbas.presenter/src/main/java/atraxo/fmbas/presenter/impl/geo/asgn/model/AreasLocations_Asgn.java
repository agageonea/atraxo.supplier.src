/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.geo.asgn.model;

import atraxo.fmbas.domain.impl.geo.Locations;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;
import seava.j4e.presenter.model.AbstractAsgnModel;

@Ds(entity = Locations.class, sort = {@SortField(field = AreasLocations_Asgn.f_name)})
public class AreasLocations_Asgn extends AbstractAsgnModel<Locations> {

	public static final String ALIAS = "fmbas_AreasLocations_Asgn";

	public static final String f_id = "id";
	public static final String f_name = "name";
	public static final String f_code = "code";
	public static final String f_validFrom = "validFrom";
	public static final String f_validTo = "validTo";

	@DsField(path = "id")
	private Integer id;

	@DsField(path = "name")
	private String name;

	@DsField(path = "code")
	private String code;

	@DsField(path = "validFrom")
	private Date validFrom;

	@DsField(path = "validTo")
	private Date validTo;

	public AreasLocations_Asgn() {
	}

	public AreasLocations_Asgn(Locations e) {
		super();
		this.id = e.getId();
		this.name = e.getName();
		this.code = e.getCode();
		this.validFrom = e.getValidFrom();
		this.validTo = e.getValidTo();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
}
