/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.timeserie.model;

import atraxo.fmbas.domain.impl.fmbas_type.DataProvider;
import atraxo.fmbas.domain.impl.fmbas_type.SerieFreqInd;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesStatus;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = TimeSerie.class)
public class TimeSerieLov_Ds extends AbstractLov_Ds<TimeSerie> {

	public static final String ALIAS = "fmbas_TimeSerieLov_Ds";

	public static final String F_MEASURE = "measure";
	public static final String F_SERIENAME = "serieName";
	public static final String F_SERIETYPE = "serieType";
	public static final String F_SERIEFREQIND = "serieFreqInd";
	public static final String F_DESCRIPTION = "description";
	public static final String F_STATUS = "status";
	public static final String F_DATAPROVIDER = "dataProvider";
	public static final String F_EXTERNALSERIENAME = "externalSerieName";
	public static final String F_FINANCIALSOURCEID = "financialSourceId";
	public static final String F_FINANCIALSOURCECODE = "financialSourceCode";
	public static final String F_CURRENCYID = "currencyId";
	public static final String F_CURRCODE = "currCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";

	@DsField(fetch = false)
	private String measure;

	@DsField
	private String serieName;

	@DsField
	private String serieType;

	@DsField
	private SerieFreqInd serieFreqInd;

	@DsField
	private String description;

	@DsField
	private TimeSeriesStatus status;

	@DsField
	private DataProvider dataProvider;

	@DsField
	private String externalSerieName;

	@DsField(join = "left", path = "financialSource.id")
	private Integer financialSourceId;

	@DsField(join = "left", path = "financialSource.code")
	private String financialSourceCode;

	@DsField(join = "left", path = "currency1Id.id")
	private Integer currencyId;

	@DsField(join = "left", path = "currency1Id.code")
	private String currCode;

	@DsField(join = "left", path = "unitId.id")
	private Integer unitId;

	@DsField(join = "left", path = "unitId.code")
	private String unitCode;

	/**
	 * Default constructor
	 */
	public TimeSerieLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public TimeSerieLov_Ds(TimeSerie e) {
		super(e);
	}

	public String getMeasure() {
		return this.measure;
	}

	public void setMeasure(String measure) {
		this.measure = measure;
	}

	public String getSerieName() {
		return this.serieName;
	}

	public void setSerieName(String serieName) {
		this.serieName = serieName;
	}

	public String getSerieType() {
		return this.serieType;
	}

	public void setSerieType(String serieType) {
		this.serieType = serieType;
	}

	public SerieFreqInd getSerieFreqInd() {
		return this.serieFreqInd;
	}

	public void setSerieFreqInd(SerieFreqInd serieFreqInd) {
		this.serieFreqInd = serieFreqInd;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public TimeSeriesStatus getStatus() {
		return this.status;
	}

	public void setStatus(TimeSeriesStatus status) {
		this.status = status;
	}

	public DataProvider getDataProvider() {
		return this.dataProvider;
	}

	public void setDataProvider(DataProvider dataProvider) {
		this.dataProvider = dataProvider;
	}

	public String getExternalSerieName() {
		return this.externalSerieName;
	}

	public void setExternalSerieName(String externalSerieName) {
		this.externalSerieName = externalSerieName;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}

	public Integer getCurrencyId() {
		return this.currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public String getCurrCode() {
		return this.currCode;
	}

	public void setCurrCode(String currCode) {
		this.currCode = currCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}
}
