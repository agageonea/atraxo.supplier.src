/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.avatars.model;

import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.ad.domain.impl.security.User;
import atraxo.fmbas.domain.impl.avatars.Avatar;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDsCharId_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Avatar.class)
@RefLookups({
		@RefLookup(refId = Avatar_Ds.F_USERID, namedQuery = User.NQ_FIND_BY_LOGIN, params = {@Param(name = "loginName", field = Avatar_Ds.F_LOGINNAME)}),
		@RefLookup(refId = Avatar_Ds.F_TYPEID, namedQuery = AttachmentType.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = Avatar_Ds.F_TYPE)})})
public class Avatar_Ds extends AbstractDsCharId_Ds<Avatar> {

	public static final String ALIAS = "fmbas_Avatar_Ds";

	public static final String F_USERNAME = "userName";
	public static final String F_USERID = "userId";
	public static final String F_LOGINNAME = "loginName";
	public static final String F_NAME = "name";
	public static final String F_FILENAME = "fileName";
	public static final String F_LOCATION = "location";
	public static final String F_CONTENTTYPE = "contentType";
	public static final String F_ACTIVE = "active";
	public static final String F_TYPEID = "typeId";
	public static final String F_TYPE = "type";
	public static final String F_BASEURL = "baseUrl";
	public static final String F_FILEPATH = "filePath";

	@DsField(join = "left", path = "user.name")
	private String userName;

	@DsField(join = "left", path = "user.id")
	private String userId;

	@DsField(join = "left", path = "user.loginName")
	private String loginName;

	@DsField
	private String name;

	@DsField
	private String fileName;

	@DsField
	private String location;

	@DsField
	private String contentType;

	@DsField
	private Boolean active;

	@DsField(join = "left", path = "type.id")
	private String typeId;

	@DsField(join = "left", path = "type.name")
	private String type;

	@DsField(join = "left", path = "type.baseUrl")
	private String baseUrl;

	@DsField(fetch = false)
	private String filePath;

	/**
	 * Default constructor
	 */
	public Avatar_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Avatar_Ds(Avatar e) {
		super(e);
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getTypeId() {
		return this.typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBaseUrl() {
		return this.baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getFilePath() {
		return this.filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
}
