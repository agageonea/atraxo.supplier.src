package atraxo.fmbas.presenter.ext.user.widget;

import java.util.List;

import atraxo.fmbas.presenter.impl.workflowTask.model.WorkflowTask_Ds;

public class WidgetModel {

	private List<WorkflowTask_Ds> list;
	private int size;

	public List<WorkflowTask_Ds> getList() {
		return this.list;
	}

	public void setList(List<WorkflowTask_Ds> list) {
		this.list = list;
	}

	public int getSize() {
		return this.size;
	}

	public void setSize(int size) {
		this.size = size;
	}

}
