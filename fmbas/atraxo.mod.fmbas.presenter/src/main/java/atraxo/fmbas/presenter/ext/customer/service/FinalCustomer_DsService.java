/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.customer.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.presenter.impl.customer.model.FinalCustomer_Ds;
import atraxo.fmbas.presenter.impl.customer.model.FinalCustomer_DsParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class FinalCustomer_DsService extends AbstractEntityDsService<FinalCustomer_Ds, FinalCustomer_Ds, FinalCustomer_DsParam, Customer> implements
		IDsService<FinalCustomer_Ds, FinalCustomer_Ds, FinalCustomer_DsParam> {

	@Override
	protected void preInsert(FinalCustomer_Ds ds, Customer e, FinalCustomer_DsParam params) throws Exception {
		// TODO Auto-generated method stub
		super.preInsert(ds, e, params);
		ICustomerService customerService = (ICustomerService) this.findEntityService(Customer.class);
		Customer reseller = customerService.findById(params.getCustomerId());
		if (e.getReseller() == null) {
			e.setReseller(new ArrayList<Customer>());
		}
		e.getReseller().add(reseller);
		reseller.getFinalCustomer().add(e);
	}

	@Override
	protected void preInsert(FinalCustomer_Ds ds, FinalCustomer_DsParam params) throws Exception {
		super.preInsert(ds, params);
		ICustomerService customerService = (ICustomerService) this.findEntityService(Customer.class);
		Map<String, Object> param = new HashMap<String, Object>();
		Map<String, Object> param2 = new HashMap<String, Object>();
		param.put("isThirdParty", true);
		param2.put("isCustomer", true);
		List<Customer> finalCustomers = customerService.findEntitiesByAttributes(Customer.class, param);
		finalCustomers.addAll(customerService.findEntitiesByAttributes(Customer.class, param2));
		for (Customer finalCustomer : finalCustomers) {
			// if (finalCustomer.getIataCode() == null || ds.getIataCode() == null
			// || finalCustomer.getIcaoCode() == null || ds.getIcaoCode() == null || finalCustomer.getIataCode().equals("") ||
			// finalCustomer.getIcaoCode().equals("") || ds.getIataCode().equals("") || ds.getIcaoCode().equals("")) {
			// continue;
			// }
			// if (finalCustomer.getIataCode().equalsIgnoreCase(ds.getIataCode())
			// || finalCustomer.getIcaoCode().equalsIgnoreCase(ds.getIcaoCode())) {
			// throw new BusinessException(BusinessErrorCode.IATA_ICAO_UNIQUE, BusinessErrorCode.IATA_ICAO_UNIQUE.getErrMsg());
			// }
			if(finalCustomer.getIataCode() != null){
				if ((ds.getIataCode() != "" && (finalCustomer.getIataCode() != null || finalCustomer.getIataCode() != ""))
						&& finalCustomer.getIataCode().equalsIgnoreCase(ds.getIataCode())) {
					throw new BusinessException(BusinessErrorCode.IATA_NOT_EXISTS, BusinessErrorCode.IATA_NOT_EXISTS.getErrMsg());
				}
			}
			if(finalCustomer.getIcaoCode() != null){
				if ((ds.getIcaoCode() != "" && (finalCustomer.getIcaoCode() != null || finalCustomer.getIcaoCode() != ""))
						&& finalCustomer.getIcaoCode().equalsIgnoreCase(ds.getIcaoCode())) {
					throw new BusinessException(BusinessErrorCode.ICAO_NOT_EXISTS, BusinessErrorCode.ICAO_NOT_EXISTS.getErrMsg());
				}
			}
		}
	}

	@Override
	protected void postInsertAfterModel(FinalCustomer_Ds ds, Customer e, FinalCustomer_DsParam params) throws Exception {
		super.postInsertAfterModel(ds, e, params);

	}

	@Override
	protected void preDelete(List<Object> ids) throws Exception {
		super.preDelete(ids);
		ICustomerService customerService = (ICustomerService) this.findEntityService(Customer.class);
		List<Customer> customers = customerService.findByIds(ids);
		for (Customer customer : customers) {
			if (customer.getIsCustomer() != null && customer.getIsCustomer()) {
				throw new BusinessException(BusinessErrorCode.CUSTOMER_TYPE_COMPANY, BusinessErrorCode.CUSTOMER_TYPE_COMPANY.getErrMsg());
			}
		}
	}

}
