/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.history.cnv;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import atraxo.fmbas.business.api.ext.history.IMessageFormatterService;
import atraxo.fmbas.domain.ext.history.HistoryValue;
import atraxo.fmbas.domain.impl.history.ChangeHistory;
import atraxo.fmbas.presenter.impl.history.model.ChangeHistory_Ds;
import seava.j4e.api.action.result.IDsConverter;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.converter.AbstractDsConverter;

/**
 * Custom converter ChangeHistory_DsCnv.
 */
public class ChangeHistory_DsCnv extends AbstractDsConverter<ChangeHistory_Ds, ChangeHistory>
		implements IDsConverter<ChangeHistory_Ds, ChangeHistory> {

	private static final String BUNDLE_NAME = "locale.sone.history.messages";
	private static final Logger LOG = LoggerFactory.getLogger(HistoryValue.class);

	@Override
	protected void entityToModel_(ChangeHistory e, ChangeHistory_Ds m, EntityManager em, Map<String, Method> setters, Map<String, String> refpaths)
			throws Exception {
		super.entityToModel_(e, m, em, setters, refpaths);
		if (e.getObjectValue() != null) {
			m.setObjectValue(this.internationalize(e.getObjectValue()));
		}
		if (e.getRemarks() != null) {
			m.setRemarks(this.internationalize(e.getRemarks()));
		}
	}

	private String internationalize(String message) throws IOException, BusinessException {
		String i18nMessage;
		final ObjectMapper mapper = new ObjectMapper();
		if (isJSONValid(mapper, message)) {
			if (this.canReadValue(mapper, message, mapper.getTypeFactory().constructType(HistoryValue.class))) {
				HistoryValue value = mapper.readValue(message, HistoryValue.class);
				i18nMessage = this.getMessage(value.getCode().trim().replaceAll(" ", "_"), value.getCode(), value.getArguments().toArray());
			} else if (this.canReadValue(mapper, message, mapper.getTypeFactory().constructCollectionType(List.class, HistoryValue.class))) {
				List<HistoryValue> values = mapper.readValue(message,
						mapper.getTypeFactory().constructCollectionType(List.class, HistoryValue.class));
				StringBuilder sb = new StringBuilder();
				for (HistoryValue value : values) {
					sb.append(this.getMessage(value.getCode().trim().replaceAll(" ", "_"), value.getCode(), value.getArguments().toArray()));
					sb.append("</br>");
				}
				i18nMessage = sb.toString();
			} else {
				i18nMessage = this.getMessage(message.trim().replaceAll(" ", "_"), message);
			}
		} else {
			i18nMessage = message != null ? this.getMessage(message.trim().replaceAll(" ", "_"), message) : StringUtils.EMPTY;
		}
		return i18nMessage;
	}

	private <T extends JavaType> boolean canReadValue(final ObjectMapper mapper, String jsonString, T t) throws IOException {
		boolean canRead = false;
		try {
			if (jsonString != null) {
				mapper.readValue(jsonString, t);
				canRead = true;
			}
		} catch (JsonMappingException e) {
			// do nothing
		}
		return canRead;
	}

	public static boolean isJSONValid(final ObjectMapper mapper, String jsonInString) {
		boolean isValid = false;
		try {
			if (jsonInString != null) {
				mapper.readTree(jsonInString);
				isValid = true;
			}
		} catch (IOException e) {
			// do nothing
		}
		return isValid;
	}

	public String getMessage(String code, String defaultMessage, Object... args) throws BusinessException {
		ResourceBundle boundle;
		try {
			String language = Session.user.get().getSettings().getLanguage();
			if (language.equals("sys")) {
				boundle = ResourceBundle.getBundle(BUNDLE_NAME);
			} else {
				boundle = ResourceBundle.getBundle(BUNDLE_NAME, new Locale(language));
			}
		} catch (NullPointerException e) {
			LOG.trace("No session while testing", e);
			boundle = ResourceBundle.getBundle(BUNDLE_NAME);
		}

		Object[] formattedArguments = this.formatArguments(args);

		try {
			String message = boundle.containsKey(code) ? boundle.getString(code) : defaultMessage;
			return String.format(message.trim(), formattedArguments);
		} catch (MissingResourceException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
			return defaultMessage;
		}
	}

	private Object[] formatArguments(Object... args) throws BusinessException {
		List<Object> formattedArgumentList = new LinkedList<>();
		IMessageFormatterService formatter = (IMessageFormatterService) this.getApplicationContext().getBean("messageFormatterService");
		for (Object argument : Arrays.asList(args)) {
			Object formattedArgument = formatter.formatArguments(argument);
			formattedArgumentList.add(formattedArgument);
		}
		return formattedArgumentList.toArray();
	}

}
