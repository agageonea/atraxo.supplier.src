package atraxo.fmbas.presenter.ext.geo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.presenter.impl.geo.model.LocationsLov_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class LocationsLov_DsService extends AbstractEntityDsService<LocationsLov_Ds, LocationsLov_Ds, Object, Locations>
		implements IDsService<LocationsLov_Ds, LocationsLov_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<LocationsLov_Ds, LocationsLov_Ds, Object> builder, List<LocationsLov_Ds> result) throws Exception {
		super.postFind(builder, result);
		Date date = GregorianCalendar.getInstance().getTime();
		List<LocationsLov_Ds> removeList = new ArrayList<>();
		for (LocationsLov_Ds loc : result) {
			if (loc.getValidFrom().compareTo(date) > 0 || loc.getValidTo().compareTo(date) < 0) {
				removeList.add(loc);
			}
		}
		result.removeAll(removeList);
	}

}
