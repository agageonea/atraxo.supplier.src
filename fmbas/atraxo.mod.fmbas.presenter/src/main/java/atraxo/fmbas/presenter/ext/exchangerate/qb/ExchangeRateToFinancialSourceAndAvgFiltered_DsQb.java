/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

package atraxo.fmbas.presenter.ext.exchangerate.qb;

import atraxo.fmbas.presenter.impl.exchangerate.model.ExchangeRateToFinancialSourceAndAvgFiltered_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

/**
 * Custom Query Builder ExchangeRateToFinancialSourceAndAvgFiltered_DsQb
 */
public class ExchangeRateToFinancialSourceAndAvgFiltered_DsQb
		extends
			QueryBuilderWithJpql<ExchangeRateToFinancialSourceAndAvgFiltered_Ds, ExchangeRateToFinancialSourceAndAvgFiltered_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		//reset where clause
		this.where = new StringBuffer();
		super.beforeBuildWhere();

	}

}
