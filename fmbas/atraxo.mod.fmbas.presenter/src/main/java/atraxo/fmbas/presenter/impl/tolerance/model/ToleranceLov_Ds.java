/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.tolerance.model;

import atraxo.fmbas.domain.impl.fmbas_type.ToleranceType;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Tolerance.class, sort = {@SortField(field = ToleranceLov_Ds.F_NAME)})
public class ToleranceLov_Ds extends AbstractLov_Ds<Tolerance> {

	public static final String ALIAS = "fmbas_ToleranceLov_Ds";

	public static final String F_NAME = "name";
	public static final String F_TOLERANCETYPE = "toleranceType";
	public static final String F_ACTIVE = "active";

	@DsField
	private String name;

	@DsField
	private ToleranceType toleranceType;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public ToleranceLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ToleranceLov_Ds(Tolerance e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ToleranceType getToleranceType() {
		return this.toleranceType;
	}

	public void setToleranceType(ToleranceType toleranceType) {
		this.toleranceType = toleranceType;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
