/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Customer.class, sort = {@SortField(field = CustomerSimpleLov_Ds.F_CODE)})
public class CustomerSimpleLov_Ds extends AbstractLov_Ds<Customer> {

	public static final String ALIAS = "fmbas_CustomerSimpleLov_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_ISCUSTOMER = "isCustomer";

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private Boolean isCustomer;

	/**
	 * Default constructor
	 */
	public CustomerSimpleLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public CustomerSimpleLov_Ds(Customer e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsCustomer() {
		return this.isCustomer;
	}

	public void setIsCustomer(Boolean isCustomer) {
		this.isCustomer = isCustomer;
	}
}
