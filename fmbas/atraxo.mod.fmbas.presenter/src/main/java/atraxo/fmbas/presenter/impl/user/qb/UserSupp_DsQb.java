/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.user.qb;

import atraxo.fmbas.presenter.impl.user.model.UserSupp_Ds;
import atraxo.fmbas.presenter.impl.user.model.UserSupp_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class UserSupp_DsQb
		extends
			QueryBuilderWithJpql<UserSupp_Ds, UserSupp_Ds, UserSupp_DsParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getWithRoleId() != null
				&& !"".equals(this.params.getWithRoleId())) {
			addFilterCondition("  e.id in ( select p.id from  User p, IN (p.roles) c where c.id = :withRoleId )  ");
			addCustomFilterItem("withRoleId", this.params.getWithRoleId());
		}
		if (this.params != null && this.params.getInGroupId() != null
				&& !"".equals(this.params.getInGroupId())) {
			addFilterCondition("  e.id in ( select p.id from  User p, IN (p.groups) c where c.id = :inGroupId )  ");
			addCustomFilterItem("inGroupId", this.params.getInGroupId());
		}
	}
}
