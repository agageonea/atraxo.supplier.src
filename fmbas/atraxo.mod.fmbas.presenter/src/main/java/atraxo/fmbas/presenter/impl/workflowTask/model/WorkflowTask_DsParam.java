/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.workflowTask.model;

/**
 * Generated code. Do not modify in this file.
 */
public class WorkflowTask_DsParam {

	public static final String f_workflowTaskName = "workflowTaskName";

	private String workflowTaskName;

	public String getWorkflowTaskName() {
		return this.workflowTaskName;
	}

	public void setWorkflowTaskName(String workflowTaskName) {
		this.workflowTaskName = workflowTaskName;
	}
}
