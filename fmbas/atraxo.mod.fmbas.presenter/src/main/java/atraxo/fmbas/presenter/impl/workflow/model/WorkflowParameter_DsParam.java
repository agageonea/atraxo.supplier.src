/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.workflow.model;

/**
 * Generated code. Do not modify in this file.
 */
public class WorkflowParameter_DsParam {

	public static final String f_xxx = "xxx";

	private String xxx;

	public String getXxx() {
		return this.xxx;
	}

	public void setXxx(String xxx) {
		this.xxx = xxx;
	}
}
