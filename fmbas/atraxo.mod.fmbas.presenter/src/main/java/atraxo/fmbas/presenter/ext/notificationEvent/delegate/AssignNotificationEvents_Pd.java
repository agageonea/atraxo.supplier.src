package atraxo.fmbas.presenter.ext.notificationEvent.delegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import atraxo.fmbas.business.api.contacts.IContactsService;
import atraxo.fmbas.business.api.notificationEvent.INotificationEventService;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import atraxo.fmbas.presenter.impl.notificationEvent.model.NotificationEventList_Ds;
import atraxo.fmbas.presenter.impl.notificationEvent.model.NotificationEventList_DsParam;
import atraxo.fmbas.presenter.impl.notificationEvent.model.NotificationEvent_Ds;
import atraxo.fmbas.presenter.impl.notificationEvent.model.NotificationEvent_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class AssignNotificationEvents_Pd extends AbstractPresenterDelegate {

	public void assign(List<NotificationEvent_Ds> dsList, NotificationEvent_DsParam params) throws Exception {
		IContactsService service = (IContactsService) this.findEntityService(Contacts.class);
		Contacts contact = service.findById(params.getContactId());
		INotificationEventService neServ = (INotificationEventService) this.findEntityService(NotificationEvent.class);
		Collection<NotificationEvent> notEventCol = new ArrayList<>();
		for (NotificationEvent_Ds ds : dsList) {
			notEventCol.add(neServ.findById(ds.getId()));
		}
		contact.setNotification(notEventCol);
		service.update(contact);
	}

	public void remove(List<NotificationEventList_Ds> dsList, NotificationEventList_DsParam params) throws Exception {
		IContactsService service = (IContactsService) this.findEntityService(Contacts.class);
		Contacts contact = service.findById(params.getContactId());
		INotificationEventService neServ = (INotificationEventService) this.findEntityService(NotificationEvent.class);
		Collection<NotificationEvent> notEventCol = new ArrayList<>();
		for (NotificationEventList_Ds ds : dsList) {
			notEventCol.add(neServ.findById(ds.getId()));
		}
		contact.getNotification().removeAll(notEventCol);
		service.update(contact);
	}

}