/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.geo.model;

import atraxo.fmbas.domain.impl.fmbas_type.AreasType;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Area.class, sort = {@SortField(field = Area_Ds.F_NAME)})
public class Area_Ds extends AbstractDs_Ds<Area> {

	public static final String ALIAS = "fmbas_Area_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_INDICATOR = "indicator";
	public static final String F_ACTIVE = "active";

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private AreasType indicator;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public Area_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Area_Ds(Area e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AreasType getIndicator() {
		return this.indicator;
	}

	public void setIndicator(AreasType indicator) {
		this.indicator = indicator;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
