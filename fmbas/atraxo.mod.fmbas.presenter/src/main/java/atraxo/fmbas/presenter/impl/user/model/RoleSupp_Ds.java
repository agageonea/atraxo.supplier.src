/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.user.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeWithCode_Ds;
import atraxo.fmbas.domain.impl.user.RoleSupp;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = RoleSupp.class, sort = {@SortField(field = RoleSupp_Ds.F_CODE)})
public class RoleSupp_Ds extends AbstractTypeWithCode_Ds<RoleSupp> {

	public static final String ALIAS = "fmbas_RoleSupp_Ds";

	/**
	 * Default constructor
	 */
	public RoleSupp_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public RoleSupp_Ds(RoleSupp e) {
		super(e);
	}
}
