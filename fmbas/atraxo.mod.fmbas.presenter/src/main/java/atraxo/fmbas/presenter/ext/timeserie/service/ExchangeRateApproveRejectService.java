/**
 *
 */
package atraxo.fmbas.presenter.ext.timeserie.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.presenter.impl.timeserie.model.ExchangeRateTimeSerie_Ds;
import atraxo.fmbas.presenter.impl.timeserie.model.ExchangeRateTimeSerie_DsParam;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public class ExchangeRateApproveRejectService extends AbstractTimeSeriesApproveRejectService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExchangeRateApproveRejectService.class);

	@Autowired
	private ISystemParameterService paramSrv;

	/**
	 * Submits for approval an Exchange rate time series
	 *
	 * @param ds
	 * @param params
	 * @throws Exception
	 */
	public void submitForApproval(ExchangeRateTimeSerie_Ds ds, ExchangeRateTimeSerie_DsParam params) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START submitForApproval()");
		}

		this.submitForApprovalTimeSerie(ds.getId(), params.getApprovalNote());

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END submitForApproval()");
		}
	}

	/**
	 * Approves current Exchange Rate Serie workflow task
	 *
	 * @param data
	 * @param param
	 * @throws Exception
	 */
	public void approve(ExchangeRateTimeSerie_Ds data, ExchangeRateTimeSerie_DsParam params) throws BusinessException {
		this.completeCurrentTask(data.getId(), true, params.getApprovalNote());
	}

	/**
	 * Rejects current Exchange Rate Serie workflow task
	 *
	 * @param data
	 * @param param
	 * @throws Exception
	 */
	public void reject(ExchangeRateTimeSerie_Ds data, ExchangeRateTimeSerie_DsParam params) throws BusinessException {
		this.completeCurrentTask(data.getId(), false, params.getApprovalNote());
	}

	/**
	 * Approves current Energy Time Serie workflow task
	 *
	 * @param data
	 * @param param
	 * @throws Exception
	 */
	public void approveList(List<ExchangeRateTimeSerie_Ds> data, ExchangeRateTimeSerie_DsParam params) {
		List<String> failedToApproveTimeSeries = new ArrayList<>();
		for (ExchangeRateTimeSerie_Ds exchangeRateTimeSerie : data) {
			try {
				this.approve(exchangeRateTimeSerie, params);
			} catch (Exception e) {
				LOGGER.error("Error", e);
				failedToApproveTimeSeries.add(exchangeRateTimeSerie.getSerieName() + " : " + e.getMessage());
			}
		}
		params.setApproveRejectResult(this.buildApproveFeedbackMessage(failedToApproveTimeSeries, data.size()));
	}

	/**
	 * Rejects current Energy Time Serie workflow task
	 *
	 * @param data
	 * @param param
	 * @throws Exception
	 */
	public void rejectList(List<ExchangeRateTimeSerie_Ds> data, ExchangeRateTimeSerie_DsParam params) {
		List<String> failedToApproveTimeSeries = new ArrayList<>();
		for (ExchangeRateTimeSerie_Ds energyPriceTimeSerie : data) {
			try {
				this.reject(energyPriceTimeSerie, params);
			} catch (BusinessException e) {
				LOGGER.error("Error", e);
				failedToApproveTimeSeries.add(energyPriceTimeSerie.getSerieName() + " : " + e.getMessage());
			}
		}
		params.setApproveRejectResult(this.buildRejectFeedbackMessage(failedToApproveTimeSeries, data.size()));
	}

	@Override
	protected WorkflowNames getWorkflowNameFromTimeSerie() {
		return WorkflowNames.EXCHANGE_RATE_TIME_SERIES_APPROVAL;
	}

	@Override
	protected String getTimeSerieType() {
		return "Exchange Rate";
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.presenter.ext.timeserie.service.AbstractTimeSeriesApproveRejectService#checkWorkflowIsEnabled()
	 */
	@Override
	protected void checkWorkflowIsEnabled() throws BusinessException {
		if (!this.paramSrv.getExchangeRateApprovalWorkflow()) { // the system param for the workflow is false
			throw new BusinessException(BusinessErrorCode.EXCHANGE_RATE_TIME_SERIE_WORKFLOW_NO_SYS_PARAM,
					BusinessErrorCode.EXCHANGE_RATE_TIME_SERIE_WORKFLOW_NO_SYS_PARAM.getErrMsg());
		}
	}

}
