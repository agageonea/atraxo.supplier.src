/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.qb;

import atraxo.fmbas.presenter.impl.customer.model.CustomerGroupLov_Ds;
import atraxo.fmbas.presenter.impl.customer.model.CustomerGroupLov_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class CustomerGroupLov_DsQb
		extends
			QueryBuilderWithJpql<CustomerGroupLov_Ds, CustomerGroupLov_Ds, CustomerGroupLov_DsParam> {

	@Override
	public void setFilter(CustomerGroupLov_Ds filter) {
		filter.setIsCustomer(true);
		super.setFilter(filter);
	}
}
