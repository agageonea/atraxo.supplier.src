/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.workflow.service;

import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.presenter.impl.workflow.model.Workflow_Ds;
import atraxo.fmbas.presenter.impl.workflow.model.Workflow_DsParam;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service Workflow_DsService
 */
public class Workflow_DsService extends AbstractEntityDsService<Workflow_Ds, Workflow_Ds, Workflow_DsParam, Workflow>
		implements IDsService<Workflow_Ds, Workflow_Ds, Workflow_DsParam> {

}
