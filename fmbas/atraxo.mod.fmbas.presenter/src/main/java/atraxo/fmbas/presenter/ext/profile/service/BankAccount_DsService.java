/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.profile.service;

import java.util.Collection;
import java.util.List;

import org.springframework.util.CollectionUtils;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.profile.BankAccount;
import atraxo.fmbas.presenter.impl.profile.model.BankAccount_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service BankAccount_DsService
 */
public class BankAccount_DsService extends AbstractEntityDsService<BankAccount_Ds, BankAccount_Ds, Object, BankAccount>
		implements IDsService<BankAccount_Ds, BankAccount_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<BankAccount_Ds, BankAccount_Ds, Object> builder, List<BankAccount_Ds> result) throws Exception {
		for (BankAccount_Ds ds : result) {
			StringBuilder customersStr = new StringBuilder();
			Collection<Customer> customers = ds._getEntity_().getCustomers();
			if (!CollectionUtils.isEmpty(customers)) {
				for (Customer customer : customers) {
					customersStr.append(customersStr.length() == 0 ? customer.getCode() : "," + customer.getCode());
				}
			}
			ds.setCustomerCodes(customersStr.toString());
		}
	}

}
