/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.templates.model;

/**
 * Generated code. Do not modify in this file.
 */
public class Templates_DsParam {

	public static final String f_previewFile = "previewFile";
	public static final String f_isUpdate = "isUpdate";
	public static final String f_templateId = "templateId";

	private String previewFile;

	private Boolean isUpdate;

	private Integer templateId;

	public String getPreviewFile() {
		return this.previewFile;
	}

	public void setPreviewFile(String previewFile) {
		this.previewFile = previewFile;
	}

	public Boolean getIsUpdate() {
		return this.isUpdate;
	}

	public void setIsUpdate(Boolean isUpdate) {
		this.isUpdate = isUpdate;
	}

	public Integer getTemplateId() {
		return this.templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}
}
