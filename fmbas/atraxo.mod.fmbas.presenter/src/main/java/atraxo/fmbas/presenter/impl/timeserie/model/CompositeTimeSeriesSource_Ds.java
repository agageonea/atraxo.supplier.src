/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.timeserie.model;

import atraxo.fmbas.domain.impl.timeserie.CompositeTimeSeriesSource;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = CompositeTimeSeriesSource.class)
@RefLookups({
		@RefLookup(refId = CompositeTimeSeriesSource_Ds.F_TIMESERIECOMPID),
		@RefLookup(refId = CompositeTimeSeriesSource_Ds.F_TIMESERIEID)})
public class CompositeTimeSeriesSource_Ds
		extends
			AbstractDs_Ds<CompositeTimeSeriesSource> {

	public static final String ALIAS = "fmbas_CompositeTimeSeriesSource_Ds";

	public static final String F_TIMESERIECOMPID = "timeSerieCompId";
	public static final String F_TIMESERIEID = "timeSerieId";
	public static final String F_TIMESERIENAME = "timeSerieName";
	public static final String F_TIMESERIEDESCRIPTION = "timeSerieDescription";
	public static final String F_FINANCIALSOURCEID = "financialSourceId";
	public static final String F_FINANCIALSOURCECODE = "financialSourceCode";
	public static final String F_CURRENCYID = "currencyId";
	public static final String F_CURRCODE = "currCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_WEIGHTING = "weighting";
	public static final String F_FACTOR = "factor";

	@DsField(join = "left", path = "composite.id")
	private Integer timeSerieCompId;

	@DsField(join = "left", path = "source.id")
	private Integer timeSerieId;

	@DsField(join = "left", path = "source.serieName")
	private String timeSerieName;

	@DsField(join = "left", path = "source.description")
	private String timeSerieDescription;

	@DsField(join = "left", path = "source.financialSource.id")
	private Integer financialSourceId;

	@DsField(join = "left", path = "source.financialSource.code")
	private String financialSourceCode;

	@DsField(join = "left", path = "source.currency1Id.id")
	private Integer currencyId;

	@DsField(join = "left", path = "source.currency1Id.code")
	private String currCode;

	@DsField(join = "left", path = "source.unitId.id")
	private Integer unitId;

	@DsField(join = "left", path = "source.unitId.code")
	private String unitCode;

	@DsField
	private Integer weighting;

	@DsField
	private BigDecimal factor;

	/**
	 * Default constructor
	 */
	public CompositeTimeSeriesSource_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public CompositeTimeSeriesSource_Ds(CompositeTimeSeriesSource e) {
		super(e);
	}

	public Integer getTimeSerieCompId() {
		return this.timeSerieCompId;
	}

	public void setTimeSerieCompId(Integer timeSerieCompId) {
		this.timeSerieCompId = timeSerieCompId;
	}

	public Integer getTimeSerieId() {
		return this.timeSerieId;
	}

	public void setTimeSerieId(Integer timeSerieId) {
		this.timeSerieId = timeSerieId;
	}

	public String getTimeSerieName() {
		return this.timeSerieName;
	}

	public void setTimeSerieName(String timeSerieName) {
		this.timeSerieName = timeSerieName;
	}

	public String getTimeSerieDescription() {
		return this.timeSerieDescription;
	}

	public void setTimeSerieDescription(String timeSerieDescription) {
		this.timeSerieDescription = timeSerieDescription;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}

	public Integer getCurrencyId() {
		return this.currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public String getCurrCode() {
		return this.currCode;
	}

	public void setCurrCode(String currCode) {
		this.currCode = currCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public Integer getWeighting() {
		return this.weighting;
	}

	public void setWeighting(Integer weighting) {
		this.weighting = weighting;
	}

	public BigDecimal getFactor() {
		return this.factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}
}
