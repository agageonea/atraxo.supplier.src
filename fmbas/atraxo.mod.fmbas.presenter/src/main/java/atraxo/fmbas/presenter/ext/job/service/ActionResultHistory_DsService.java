/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.job.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DurationFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.ad.business.api.scheduler.IJobExecutionParamsService;
import atraxo.ad.domain.impl.scheduler.JobExecutionParams;
import atraxo.fmbas.presenter.impl.job.model.ActionResultHistory_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.domain.batchjob.JobParameter;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ActionResultHistory_DsService extends AbstractEntityDsService<ActionResultHistory_Ds, ActionResultHistory_Ds, Object, JobExecutionParams>
		implements IDsService<ActionResultHistory_Ds, ActionResultHistory_Ds, Object> {

	private static final String PIPE = "|";

	@Autowired
	private IJobExecutionParamsService jobExecutionParamsService;

	@Override
	protected void postFind(IQueryBuilder<ActionResultHistory_Ds, ActionResultHistory_Ds, Object> builder, List<ActionResultHistory_Ds> result)
			throws Exception {
		super.postFind(builder, result);
		long diff = 0;
		Map<Long, String> names = new HashMap<>();
		for (ActionResultHistory_Ds ds : result) {
			if (!names.containsKey(ds.getJobExId())) {
				names.put(ds.getJobExId(), this.getActionName(ds));
			}
			ds.setActionName(names.get(ds.getJobExId()));
			if (ds.getEndTime() != null && ds.getStartTime() != null) {
				diff = ds.getEndTime().getTime() - ds.getStartTime().getTime();
			}
			ds.setDuration(DurationFormatUtils.formatDuration(diff, "H:mm:ss", true));
			int idx = ds.getExitMessage().indexOf(PIPE);
			if (idx > 0) {
				ds.setMsg(ds.getExitMessage().substring(0, ds.getExitMessage().indexOf(PIPE)));
				ds.setStacktrace(ds.getExitMessage().substring(ds.getExitMessage().indexOf(PIPE) + 1));
			} else {
				ds.setStacktrace(ds.getExitMessage());
			}
		}
	}

	private String getActionName(ActionResultHistory_Ds ds) {
		Map<String, Object> params = new HashMap<>();
		params.put("jobExecution", ds._getEntity_().getJobExecution());
		List<JobExecutionParams> list = this.jobExecutionParamsService.findEntitiesByAttributes(params);
		for (JobExecutionParams p : list) {
			if (p.getKeyName().equalsIgnoreCase(JobParameter.ACTIONNAME.name())) {
				return p.getStringVal();
			}
		}
		return "";
	}
}
