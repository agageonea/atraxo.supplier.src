/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.exchangerate.model;

import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ExchangeRate.class)
public class ExchangeRateToFinancialSourceAndAvgFiltered_Ds
		extends
			AbstractLov_Ds<ExchangeRate> {

	public static final String ALIAS = "fmbas_ExchangeRateToFinancialSourceAndAvgFiltered_Ds";

	public static final String F_NAME = "name";
	public static final String F_FROMCURRENCYCODE = "fromCurrencyCode";
	public static final String F_FROMCURRENCYID = "fromCurrencyId";
	public static final String F_TOCURRENCYCODE = "toCurrencyCode";
	public static final String F_TOCURRENCYID = "toCurrencyId";
	public static final String F_AVGMTHDID = "avgMthdId";
	public static final String F_AVGMTHDNAME = "avgMthdName";
	public static final String F_AVGMTHDCODE = "avgMthdCode";
	public static final String F_FINANCIALSOURCEID = "financialSourceId";
	public static final String F_FINANCIALSOURCECODE = "financialSourceCode";
	public static final String F_FINANCIALSOURCENAME = "financialSourceName";
	public static final String F_CURRENCYID = "currencyId";

	@DsField
	private String name;

	@DsField(join = "left", path = "currency1.code")
	private String fromCurrencyCode;

	@DsField(join = "left", path = "currency1.id")
	private Integer fromCurrencyId;

	@DsField(join = "left", path = "currency2.code")
	private String toCurrencyCode;

	@DsField(join = "left", path = "currency2.id")
	private Integer toCurrencyId;

	@DsField(join = "left", path = "avgMethodIndicator.id")
	private Integer avgMthdId;

	@DsField(join = "left", path = "avgMethodIndicator.name")
	private String avgMthdName;

	@DsField(join = "left", path = "avgMethodIndicator.code")
	private String avgMthdCode;

	@DsField(join = "left", path = "finsource.id")
	private Integer financialSourceId;

	@DsField(join = "left", path = "finsource.code")
	private String financialSourceCode;

	@DsField(join = "left", path = "finsource.name")
	private String financialSourceName;

	@DsField(fetch = false)
	private Integer currencyId;

	/**
	 * Default constructor
	 */
	public ExchangeRateToFinancialSourceAndAvgFiltered_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ExchangeRateToFinancialSourceAndAvgFiltered_Ds(ExchangeRate e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFromCurrencyCode() {
		return this.fromCurrencyCode;
	}

	public void setFromCurrencyCode(String fromCurrencyCode) {
		this.fromCurrencyCode = fromCurrencyCode;
	}

	public Integer getFromCurrencyId() {
		return this.fromCurrencyId;
	}

	public void setFromCurrencyId(Integer fromCurrencyId) {
		this.fromCurrencyId = fromCurrencyId;
	}

	public String getToCurrencyCode() {
		return this.toCurrencyCode;
	}

	public void setToCurrencyCode(String toCurrencyCode) {
		this.toCurrencyCode = toCurrencyCode;
	}

	public Integer getToCurrencyId() {
		return this.toCurrencyId;
	}

	public void setToCurrencyId(Integer toCurrencyId) {
		this.toCurrencyId = toCurrencyId;
	}

	public Integer getAvgMthdId() {
		return this.avgMthdId;
	}

	public void setAvgMthdId(Integer avgMthdId) {
		this.avgMthdId = avgMthdId;
	}

	public String getAvgMthdName() {
		return this.avgMthdName;
	}

	public void setAvgMthdName(String avgMthdName) {
		this.avgMthdName = avgMthdName;
	}

	public String getAvgMthdCode() {
		return this.avgMthdCode;
	}

	public void setAvgMthdCode(String avgMthdCode) {
		this.avgMthdCode = avgMthdCode;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}

	public String getFinancialSourceName() {
		return this.financialSourceName;
	}

	public void setFinancialSourceName(String financialSourceName) {
		this.financialSourceName = financialSourceName;
	}

	public Integer getCurrencyId() {
		return this.currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}
}
