/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.exchangerate.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;
import atraxo.fmbas.presenter.impl.exchangerate.model.ExchangeRateValue_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ExchangeRateValue_DsService extends AbstractEntityDsService<ExchangeRateValue_Ds, ExchangeRateValue_Ds, Object, ExchangeRateValue>
		implements IDsService<ExchangeRateValue_Ds, ExchangeRateValue_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<ExchangeRateValue_Ds, ExchangeRateValue_Ds, Object> builder, List<ExchangeRateValue_Ds> result)
			throws Exception {
		super.postFind(builder, result);

		BigDecimal one = BigDecimal.ONE;
		for (ExchangeRateValue_Ds erv : result) {
			int decimals = erv.getDecimals();
			erv.setPerRate(one.divide(erv.getRate(), MathContext.DECIMAL64).setScale(decimals, RoundingMode.HALF_UP));
			erv.setRate(erv.getRate().setScale(decimals, RoundingMode.HALF_UP));

		}

	}
}
