/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.workflow.model;

/**
 * Generated code. Do not modify in this file.
 */
public class Workflow_DsParam {

	public static final String f_subsidiary = "subsidiary";
	public static final String f_copyResultDescription = "copyResultDescription";
	public static final String f_copyResultValue = "copyResultValue";

	private String subsidiary;

	private String copyResultDescription;

	private Boolean copyResultValue;

	public String getSubsidiary() {
		return this.subsidiary;
	}

	public void setSubsidiary(String subsidiary) {
		this.subsidiary = subsidiary;
	}

	public String getCopyResultDescription() {
		return this.copyResultDescription;
	}

	public void setCopyResultDescription(String copyResultDescription) {
		this.copyResultDescription = copyResultDescription;
	}

	public Boolean getCopyResultValue() {
		return this.copyResultValue;
	}

	public void setCopyResultValue(Boolean copyResultValue) {
		this.copyResultValue = copyResultValue;
	}
}
