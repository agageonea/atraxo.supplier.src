/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.fmbas.presenter.ext.customer.qb;

import atraxo.fmbas.presenter.impl.customer.model.SubsidiaryAssignList_Ds;
import atraxo.fmbas.presenter.impl.customer.model.SubsidiaryAssignList_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class SubsidiaryAssignList_DsQb extends QueryBuilderWithJpql<SubsidiaryAssignList_Ds, SubsidiaryAssignList_Ds, SubsidiaryAssignList_DsParam> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();
		this.addFilterCondition("e.isSubsidiary = :isSubsidiary");
		this.addCustomFilterItem("isSubsidiary", true);
	}

}
