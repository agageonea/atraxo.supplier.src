/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.quotation.model;

import atraxo.fmbas.domain.impl.fmbas_type.DataProvider;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.fmbas_type.ValueType;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Quotation.class, sort = {@SortField(field = Quotation_Ds.F_NAME),
		@SortField(field = Quotation_Ds.F_AVGMETHODINDICATORCODE)})
@RefLookups({@RefLookup(refId = Quotation_Ds.F_CURRENCY1ID),
		@RefLookup(refId = Quotation_Ds.F_CURRENCY2ID),
		@RefLookup(refId = Quotation_Ds.F_UNITID),
		@RefLookup(refId = Quotation_Ds.F_TSID),
		@RefLookup(refId = Quotation_Ds.F_AVGID)})
public class Quotation_Ds extends AbstractDs_Ds<Quotation> {

	public static final String ALIAS = "fmbas_Quotation_Ds";

	public static final String F_CONVFCTR = "convFctr";
	public static final String F_SOURCE = "source";
	public static final String F_TSID = "tsId";
	public static final String F_TSNAME = "tsName";
	public static final String F_ARITHMOPER = "arithmOper";
	public static final String F_CURRENCY1ID = "currency1iD";
	public static final String F_CURRENCY1CODE = "currency1Code";
	public static final String F_CURRENCY2ID = "currency2Id";
	public static final String F_CURRENCY2CODE = "currency2Code";
	public static final String F_AVGID = "avgId";
	public static final String F_AVGMETHODINDICATORNAME = "avgMethodIndicatorName";
	public static final String F_AVGMETHODINDICATORCODE = "avgMethodIndicatorCode";
	public static final String F_NAME = "name";
	public static final String F_ACTIVE = "active";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_UNITTYPEIND = "unitTypeInd";
	public static final String F_UNIT2ID = "unit2Id";
	public static final String F_UNIT2CODE = "unit2Code";
	public static final String F_UNIT2TYPEIND = "unit2TypeInd";
	public static final String F_CALCVAL = "calcVal";
	public static final String F_VALUETYPE = "valueType";
	public static final String F_FIRSTUSAGE = "firstUsage";
	public static final String F_LASTUSAGE = "lastUsage";

	@DsField(join = "left", path = "timeseries.convFctr")
	private Operator convFctr;

	@DsField(join = "left", path = "timeseries.dataProvider")
	private DataProvider source;

	@DsField(join = "left", path = "timeseries.id")
	private Integer tsId;

	@DsField(join = "left", path = "timeseries.serieName")
	private String tsName;

	@DsField(join = "left", path = "timeseries.arithmOper")
	private BigDecimal arithmOper;

	@DsField(join = "left", path = "timeseries.currency1Id.id")
	private Integer currency1iD;

	@DsField(join = "left", path = "timeseries.currency1Id.code")
	private String currency1Code;

	@DsField(join = "left", path = "timeseries.currency2Id.id")
	private Integer currency2Id;

	@DsField(join = "left", path = "timeseries.currency2Id.code")
	private String currency2Code;

	@DsField(join = "left", path = "avgMethodIndicator.id")
	private Integer avgId;

	@DsField(join = "left", path = "avgMethodIndicator.name")
	private String avgMethodIndicatorName;

	@DsField(join = "left", path = "avgMethodIndicator.code")
	private String avgMethodIndicatorCode;

	@DsField
	private String name;

	@DsField
	private Boolean active;

	@DsField(join = "left", path = "unit.id")
	private Integer unitId;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	@DsField(join = "left", path = "unit.unittypeInd")
	private UnitType unitTypeInd;

	@DsField(join = "left", path = "timeseries.unit2Id.id")
	private Integer unit2Id;

	@DsField(join = "left", path = "timeseries.unit2Id.code")
	private String unit2Code;

	@DsField(join = "left", path = "timeseries.unit2Id.unittypeInd")
	private UnitType unit2TypeInd;

	@DsField(fetch = false)
	private String calcVal;

	@DsField
	private ValueType valueType;

	@DsField(fetch = false)
	private Date firstUsage;

	@DsField(fetch = false)
	private Date lastUsage;

	/**
	 * Default constructor
	 */
	public Quotation_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Quotation_Ds(Quotation e) {
		super(e);
	}

	public Operator getConvFctr() {
		return this.convFctr;
	}

	public void setConvFctr(Operator convFctr) {
		this.convFctr = convFctr;
	}

	public DataProvider getSource() {
		return this.source;
	}

	public void setSource(DataProvider source) {
		this.source = source;
	}

	public Integer getTsId() {
		return this.tsId;
	}

	public void setTsId(Integer tsId) {
		this.tsId = tsId;
	}

	public String getTsName() {
		return this.tsName;
	}

	public void setTsName(String tsName) {
		this.tsName = tsName;
	}

	public BigDecimal getArithmOper() {
		return this.arithmOper;
	}

	public void setArithmOper(BigDecimal arithmOper) {
		this.arithmOper = arithmOper;
	}

	public Integer getCurrency1iD() {
		return this.currency1iD;
	}

	public void setCurrency1iD(Integer currency1iD) {
		this.currency1iD = currency1iD;
	}

	public String getCurrency1Code() {
		return this.currency1Code;
	}

	public void setCurrency1Code(String currency1Code) {
		this.currency1Code = currency1Code;
	}

	public Integer getCurrency2Id() {
		return this.currency2Id;
	}

	public void setCurrency2Id(Integer currency2Id) {
		this.currency2Id = currency2Id;
	}

	public String getCurrency2Code() {
		return this.currency2Code;
	}

	public void setCurrency2Code(String currency2Code) {
		this.currency2Code = currency2Code;
	}

	public Integer getAvgId() {
		return this.avgId;
	}

	public void setAvgId(Integer avgId) {
		this.avgId = avgId;
	}

	public String getAvgMethodIndicatorName() {
		return this.avgMethodIndicatorName;
	}

	public void setAvgMethodIndicatorName(String avgMethodIndicatorName) {
		this.avgMethodIndicatorName = avgMethodIndicatorName;
	}

	public String getAvgMethodIndicatorCode() {
		return this.avgMethodIndicatorCode;
	}

	public void setAvgMethodIndicatorCode(String avgMethodIndicatorCode) {
		this.avgMethodIndicatorCode = avgMethodIndicatorCode;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public UnitType getUnitTypeInd() {
		return this.unitTypeInd;
	}

	public void setUnitTypeInd(UnitType unitTypeInd) {
		this.unitTypeInd = unitTypeInd;
	}

	public Integer getUnit2Id() {
		return this.unit2Id;
	}

	public void setUnit2Id(Integer unit2Id) {
		this.unit2Id = unit2Id;
	}

	public String getUnit2Code() {
		return this.unit2Code;
	}

	public void setUnit2Code(String unit2Code) {
		this.unit2Code = unit2Code;
	}

	public UnitType getUnit2TypeInd() {
		return this.unit2TypeInd;
	}

	public void setUnit2TypeInd(UnitType unit2TypeInd) {
		this.unit2TypeInd = unit2TypeInd;
	}

	public String getCalcVal() {
		return this.calcVal;
	}

	public void setCalcVal(String calcVal) {
		this.calcVal = calcVal;
	}

	public ValueType getValueType() {
		return this.valueType;
	}

	public void setValueType(ValueType valueType) {
		this.valueType = valueType;
	}

	public Date getFirstUsage() {
		return this.firstUsage;
	}

	public void setFirstUsage(Date firstUsage) {
		this.firstUsage = firstUsage;
	}

	public Date getLastUsage() {
		return this.lastUsage;
	}

	public void setLastUsage(Date lastUsage) {
		this.lastUsage = lastUsage;
	}
}
