/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

/**
 * Generated code. Do not modify in this file.
 */
public class CustomerLocations_DsParam {

	public static final String f_customerId = "customerId";

	private Integer customerId;

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
}
