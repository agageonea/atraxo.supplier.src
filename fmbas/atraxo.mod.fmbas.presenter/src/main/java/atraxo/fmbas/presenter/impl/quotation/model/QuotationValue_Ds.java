/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.quotation.model;

import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = QuotationValue.class, sort = {@SortField(field = QuotationValue_Ds.F_VALIDFROMDATE, desc = true)})
@RefLookups({@RefLookup(refId = QuotationValue_Ds.F_QUOTID)})
public class QuotationValue_Ds extends AbstractDs_Ds<QuotationValue> {

	public static final String ALIAS = "fmbas_QuotationValue_Ds";

	public static final String F_QUOTID = "quotId";
	public static final String F_QUOTNAME = "quotName";
	public static final String F_METHODNAME = "methodName";
	public static final String F_VALIDFROMDATE = "validFromDate";
	public static final String F_VALIDTODATE = "validToDate";
	public static final String F_ISPROVISIONED = "isProvisioned";
	public static final String F_RATE = "rate";
	public static final String F_ACTIVE = "active";
	public static final String F_DECIMALS = "decimals";

	@DsField(join = "left", path = "quotation.id")
	private Integer quotId;

	@DsField(join = "left", path = "quotation.name")
	private String quotName;

	@DsField(join = "left", path = "quotation.avgMethodIndicator.code")
	private String methodName;

	@DsField(jpqlFilter = " e.validToDate >= :validFromDate ")
	private Date validFromDate;

	@DsField(jpqlFilter = " e.validFromDate <= :validToDate ")
	private Date validToDate;

	@DsField
	private Boolean isProvisioned;

	@DsField
	private BigDecimal rate;

	@DsField
	private Boolean active;

	@DsField(join = "left", path = "quotation.timeseries.decimals")
	private Integer decimals;

	/**
	 * Default constructor
	 */
	public QuotationValue_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public QuotationValue_Ds(QuotationValue e) {
		super(e);
	}

	public Integer getQuotId() {
		return this.quotId;
	}

	public void setQuotId(Integer quotId) {
		this.quotId = quotId;
	}

	public String getQuotName() {
		return this.quotName;
	}

	public void setQuotName(String quotName) {
		this.quotName = quotName;
	}

	public String getMethodName() {
		return this.methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Date getValidFromDate() {
		return this.validFromDate;
	}

	public void setValidFromDate(Date validFromDate) {
		this.validFromDate = validFromDate;
	}

	public Date getValidToDate() {
		return this.validToDate;
	}

	public void setValidToDate(Date validToDate) {
		this.validToDate = validToDate;
	}

	public Boolean getIsProvisioned() {
		return this.isProvisioned;
	}

	public void setIsProvisioned(Boolean isProvisioned) {
		this.isProvisioned = isProvisioned;
	}

	public BigDecimal getRate() {
		return this.rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getDecimals() {
		return this.decimals;
	}

	public void setDecimals(Integer decimals) {
		this.decimals = decimals;
	}
}
