/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.exchangerate.model;

import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ExchangeRate.class)
public class CalcValLov_Ds extends AbstractLov_Ds<ExchangeRate> {

	public static final String ALIAS = "fmbas_CalcValLov_Ds";

	public static final String F_NAME = "name";
	public static final String F_CALCVAL = "calcVal";
	public static final String F_AVGMTHDID = "avgMthdId";
	public static final String F_FINANCIALSOURCEID = "financialSourceId";

	@DsField
	private String name;

	@DsField(fetch = false)
	private String calcVal;

	@DsField(join = "left", path = "avgMethodIndicator.id")
	private Integer avgMthdId;

	@DsField(join = "left", path = "finsource.id")
	private Integer financialSourceId;

	/**
	 * Default constructor
	 */
	public CalcValLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public CalcValLov_Ds(ExchangeRate e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCalcVal() {
		return this.calcVal;
	}

	public void setCalcVal(String calcVal) {
		this.calcVal = calcVal;
	}

	public Integer getAvgMthdId() {
		return this.avgMthdId;
	}

	public void setAvgMthdId(Integer avgMthdId) {
		this.avgMthdId = avgMthdId;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}
}
