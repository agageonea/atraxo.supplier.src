package atraxo.fmbas.presenter.ext.templates.delegate;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.codehaus.jettison.json.JSONObject;

import atraxo.fmbas.business.api.templates.ITemplateService;
import atraxo.fmbas.business.ext.mailMerge.service.MailMerge_Service;
import atraxo.fmbas.domain.impl.templates.Template;
import atraxo.fmbas.presenter.impl.templates.model.Templates_Ds;
import atraxo.fmbas.presenter.impl.templates.model.Templates_DsParam;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class Template_Pd extends AbstractPresenterDelegate {

	private static final String POINT = ".";

	public void getPreview(Templates_Ds ds, Templates_DsParam params) throws Exception {
		ITemplateService templateService = (ITemplateService) this.findEntityService(Template.class);
		Template template = templateService.findById(ds.getId());
		String fileExtension = this.getExtension(template.getFileName());
		JSONObject json = new JSONObject();
		JSONObject format = new JSONObject();
		format.put("format", fileExtension);
		json.put("header", format);
		json.put("data", new JSONObject("{}"));
		byte[] fileBytes = templateService.getPreview(template, json.toString());
		String fileNameGen = generateFile(fileExtension, fileBytes);
		params.setPreviewFile(fileNameGen + POINT + fileExtension);
	}

	public void downloadTemplate(Templates_Ds ds, Templates_DsParam params) throws Exception {
		ITemplateService templateService = (ITemplateService) this.findEntityService(Template.class);
		Template template = templateService.findById(ds.getId());
		MailMerge_Service mailMergeService = this.getApplicationContext().getBean(MailMerge_Service.class);
		byte[] fileBytes = mailMergeService.downloadTemplate(template);
		String path = Session.user.get().getWorkspace().getTempPath();
		File file = new File(path + File.separator + template.getFileName());
		FileUtils.writeByteArrayToFile(file.getAbsoluteFile(), fileBytes);
		params.setPreviewFile(template.getFileName());
	}

	private String generateFile(String fileExtension, byte[] fileBytes) throws IOException {
		String path = Session.user.get().getWorkspace().getTempPath();
		String fileNameGen = UUID.randomUUID().toString();
		File file = new File(path + File.separator + fileNameGen + POINT + fileExtension);
		FileUtils.writeByteArrayToFile(file.getAbsoluteFile(), fileBytes);
		return fileNameGen;
	}

	private String getExtension(String fileName) {
		return FilenameUtils.getExtension(fileName);
	}

}
