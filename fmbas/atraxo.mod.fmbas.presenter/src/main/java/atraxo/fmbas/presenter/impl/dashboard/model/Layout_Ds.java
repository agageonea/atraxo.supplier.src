/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.dashboard.model;

import atraxo.fmbas.domain.impl.dashboard.Layouts;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Layouts.class)
public class Layout_Ds extends AbstractDs_Ds<Layouts> {

	public static final String ALIAS = "fmbas_Layout_Ds";

	public static final String F_THUMBPATH = "thumbPath";
	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_CFGPATH = "cfgPath";

	@DsField
	private String thumbPath;

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private String cfgPath;

	/**
	 * Default constructor
	 */
	public Layout_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Layout_Ds(Layouts e) {
		super(e);
	}

	public String getThumbPath() {
		return this.thumbPath;
	}

	public void setThumbPath(String thumbPath) {
		this.thumbPath = thumbPath;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCfgPath() {
		return this.cfgPath;
	}

	public void setCfgPath(String cfgPath) {
		this.cfgPath = cfgPath;
	}
}
