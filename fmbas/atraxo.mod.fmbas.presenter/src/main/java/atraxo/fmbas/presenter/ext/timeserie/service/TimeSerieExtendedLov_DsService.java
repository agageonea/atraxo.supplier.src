/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.timeserie.service;

import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.presenter.impl.timeserie.model.TimeSerieExtendedLov_Ds;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service TimeSerieExtendedLov_DsService
 */
public class TimeSerieExtendedLov_DsService
		extends
			AbstractEntityDsService<TimeSerieExtendedLov_Ds, TimeSerieExtendedLov_Ds, Object, TimeSerie>
		implements
			IDsService<TimeSerieExtendedLov_Ds, TimeSerieExtendedLov_Ds, Object> {

	// Implement me ...

}
