/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CreditUpdateRequestStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataProcessedStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataTransmissionStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerType;
import atraxo.fmbas.domain.impl.fmbas_type.NatureOfBusiness;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.geo.TimeZone;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Customer.class, sort = {@SortField(field = Subsidiary_Ds.F_CODE)})
@RefLookups({
		@RefLookup(refId = Subsidiary_Ds.F_OFFICECOUNTRYID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Subsidiary_Ds.F_OFFICECOUNTRYCODE)}),
		@RefLookup(refId = Subsidiary_Ds.F_OFFICESTATEID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Subsidiary_Ds.F_OFFICESTATECODE)}),
		@RefLookup(refId = Subsidiary_Ds.F_TIMEZONEID, namedQuery = TimeZone.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = Subsidiary_Ds.F_TIMEZONENAME)}),
		@RefLookup(refId = Subsidiary_Ds.F_SUBSIDIARYCURRENCYID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Subsidiary_Ds.F_SUBSIDIARYCURRENCYCODE)}),
		@RefLookup(refId = Subsidiary_Ds.F_DEFAULTVOLUMEUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Subsidiary_Ds.F_DEFAULTVOLUMEUNITCODE)}),
		@RefLookup(refId = Subsidiary_Ds.F_DEFAULTWEIGHTUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Subsidiary_Ds.F_DEFAULTWEIGHTUNITCODE)}),
		@RefLookup(refId = Subsidiary_Ds.F_ASSIGNEDAREAID, namedQuery = Area.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Subsidiary_Ds.F_ASSIGNEDAREACODE)})})
public class Subsidiary_Ds extends AbstractDs_Ds<Customer> {

	public static final String ALIAS = "fmbas_Subsidiary_Ds";

	public static final String F_SEPARATOR = "separator";
	public static final String F_OFFICECOUNTRYID = "officeCountryId";
	public static final String F_OFFICECOUNTRYCODE = "officeCountryCode";
	public static final String F_OFFICECOUNTRYNAME = "officeCountryName";
	public static final String F_OFFICESTATEID = "officeStateId";
	public static final String F_OFFICESTATECODE = "officeStateCode";
	public static final String F_OFFICESTATENAME = "officeStateName";
	public static final String F_ASSIGNEDAREAID = "assignedAreaId";
	public static final String F_ASSIGNEDAREACODE = "assignedAreaCode";
	public static final String F_ASSIGNEDAREANAME = "assignedAreaName";
	public static final String F_DEFAULTVOLUMEUNITID = "defaultVolumeUnitId";
	public static final String F_DEFAULTVOLUMEUNITCODE = "defaultVolumeUnitCode";
	public static final String F_DEFAULTWEIGHTUNITID = "defaultWeightUnitId";
	public static final String F_DEFAULTWEIGHTUNITCODE = "defaultWeightUnitCode";
	public static final String F_OFFICESTREET = "officeStreet";
	public static final String F_OFFICECITY = "officeCity";
	public static final String F_OFFICEPOSTALCODE = "officePostalCode";
	public static final String F_ISSUBSIDIARY = "isSubsidiary";
	public static final String F_ISCUSTOMER = "isCustomer";
	public static final String F_ISSUPPLIER = "isSupplier";
	public static final String F_NAME = "name";
	public static final String F_CODE = "code";
	public static final String F_NATUREOFBUSINESS = "natureOfBusiness";
	public static final String F_STATUS = "status";
	public static final String F_TYPE = "type";
	public static final String F_IPLAGENT = "iplAgent";
	public static final String F_FUELSUPPLIER = "fuelSupplier";
	public static final String F_FIXEDBASEOPERATOR = "fixedBaseOperator";
	public static final String F_ACCOUNTNUMBER = "accountNumber";
	public static final String F_PHONENO = "phoneNo";
	public static final String F_FAXNO = "faxNo";
	public static final String F_WEBSITE = "website";
	public static final String F_EMAIL = "email";
	public static final String F_BANKNAME = "bankName";
	public static final String F_BANKADDRESS = "bankAddress";
	public static final String F_BANKACCOUNTNO = "bankAccountNo";
	public static final String F_BANKIBANNO = "bankIbanNo";
	public static final String F_BANKSWIFTNO = "bankSwiftNo";
	public static final String F_TIMEZONEID = "timeZoneId";
	public static final String F_TIMEZONENAME = "timeZoneName";
	public static final String F_SUBSIDIARYCURRENCYID = "subsidiaryCurrencyId";
	public static final String F_SUBSIDIARYCURRENCYCODE = "subsidiaryCurrencyCode";
	public static final String F_SUBSIDIARYCURRENCYACTIVE = "subsidiaryCurrencyActive";
	public static final String F_TRANSMISSIONSTATUS = "transmissionStatus";
	public static final String F_PROCESSEDSTATUS = "processedStatus";
	public static final String F_CREDITUPDATEREQUESTSTATUS = "creditUpdateRequestStatus";
	public static final String F_LABELFIELD = "labelField";
	public static final String F_DUMMYFIELD = "dummyField";

	@DsField(fetch = false)
	private String separator;

	@DsField(join = "left", path = "officeCountry.id")
	private Integer officeCountryId;

	@DsField(join = "left", path = "officeCountry.code")
	private String officeCountryCode;

	@DsField(join = "left", path = "officeCountry.name")
	private String officeCountryName;

	@DsField(join = "left", path = "officeState.id")
	private Integer officeStateId;

	@DsField(join = "left", path = "officeState.code")
	private String officeStateCode;

	@DsField(join = "left", path = "officeState.name")
	private String officeStateName;

	@DsField(join = "left", path = "assignedArea.id")
	private Integer assignedAreaId;

	@DsField(join = "left", path = "assignedArea.code")
	private String assignedAreaCode;

	@DsField(join = "left", path = "assignedArea.name")
	private String assignedAreaName;

	@DsField(join = "left", path = "defaultVolumeUnit.id")
	private Integer defaultVolumeUnitId;

	@DsField(join = "left", path = "defaultVolumeUnit.code")
	private String defaultVolumeUnitCode;

	@DsField(join = "left", path = "defaultWeightUnit.id")
	private Integer defaultWeightUnitId;

	@DsField(join = "left", path = "defaultWeightUnit.code")
	private String defaultWeightUnitCode;

	@DsField
	private String officeStreet;

	@DsField
	private String officeCity;

	@DsField
	private String officePostalCode;

	@DsField
	private Boolean isSubsidiary;

	@DsField
	private Boolean isCustomer;

	@DsField
	private Boolean isSupplier;

	@DsField
	private String name;

	@DsField
	private String code;

	@DsField
	private NatureOfBusiness natureOfBusiness;

	@DsField
	private CustomerStatus status;

	@DsField
	private CustomerType type;

	@DsField
	private Boolean iplAgent;

	@DsField
	private Boolean fuelSupplier;

	@DsField
	private Boolean fixedBaseOperator;

	@DsField
	private String accountNumber;

	@DsField
	private String phoneNo;

	@DsField
	private String faxNo;

	@DsField
	private String website;

	@DsField
	private String email;

	@DsField
	private String bankName;

	@DsField
	private String bankAddress;

	@DsField
	private String bankAccountNo;

	@DsField
	private String bankIbanNo;

	@DsField
	private String bankSwiftNo;

	@DsField(join = "left", path = "timeZone.id")
	private Integer timeZoneId;

	@DsField(join = "left", path = "timeZone.name")
	private String timeZoneName;

	@DsField(join = "left", path = "subsidiaryCurrency.id")
	private Integer subsidiaryCurrencyId;

	@DsField(join = "left", path = "subsidiaryCurrency.code")
	private String subsidiaryCurrencyCode;

	@DsField(join = "left", path = "subsidiaryCurrency.active")
	private Boolean subsidiaryCurrencyActive;

	@DsField
	private CustomerDataTransmissionStatus transmissionStatus;

	@DsField
	private CustomerDataProcessedStatus processedStatus;

	@DsField
	private CreditUpdateRequestStatus creditUpdateRequestStatus;

	@DsField(fetch = false)
	private String labelField;

	@DsField(fetch = false)
	private String dummyField;

	/**
	 * Default constructor
	 */
	public Subsidiary_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Subsidiary_Ds(Customer e) {
		super(e);
	}

	public String getSeparator() {
		return this.separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public Integer getOfficeCountryId() {
		return this.officeCountryId;
	}

	public void setOfficeCountryId(Integer officeCountryId) {
		this.officeCountryId = officeCountryId;
	}

	public String getOfficeCountryCode() {
		return this.officeCountryCode;
	}

	public void setOfficeCountryCode(String officeCountryCode) {
		this.officeCountryCode = officeCountryCode;
	}

	public String getOfficeCountryName() {
		return this.officeCountryName;
	}

	public void setOfficeCountryName(String officeCountryName) {
		this.officeCountryName = officeCountryName;
	}

	public Integer getOfficeStateId() {
		return this.officeStateId;
	}

	public void setOfficeStateId(Integer officeStateId) {
		this.officeStateId = officeStateId;
	}

	public String getOfficeStateCode() {
		return this.officeStateCode;
	}

	public void setOfficeStateCode(String officeStateCode) {
		this.officeStateCode = officeStateCode;
	}

	public String getOfficeStateName() {
		return this.officeStateName;
	}

	public void setOfficeStateName(String officeStateName) {
		this.officeStateName = officeStateName;
	}

	public Integer getAssignedAreaId() {
		return this.assignedAreaId;
	}

	public void setAssignedAreaId(Integer assignedAreaId) {
		this.assignedAreaId = assignedAreaId;
	}

	public String getAssignedAreaCode() {
		return this.assignedAreaCode;
	}

	public void setAssignedAreaCode(String assignedAreaCode) {
		this.assignedAreaCode = assignedAreaCode;
	}

	public String getAssignedAreaName() {
		return this.assignedAreaName;
	}

	public void setAssignedAreaName(String assignedAreaName) {
		this.assignedAreaName = assignedAreaName;
	}

	public Integer getDefaultVolumeUnitId() {
		return this.defaultVolumeUnitId;
	}

	public void setDefaultVolumeUnitId(Integer defaultVolumeUnitId) {
		this.defaultVolumeUnitId = defaultVolumeUnitId;
	}

	public String getDefaultVolumeUnitCode() {
		return this.defaultVolumeUnitCode;
	}

	public void setDefaultVolumeUnitCode(String defaultVolumeUnitCode) {
		this.defaultVolumeUnitCode = defaultVolumeUnitCode;
	}

	public Integer getDefaultWeightUnitId() {
		return this.defaultWeightUnitId;
	}

	public void setDefaultWeightUnitId(Integer defaultWeightUnitId) {
		this.defaultWeightUnitId = defaultWeightUnitId;
	}

	public String getDefaultWeightUnitCode() {
		return this.defaultWeightUnitCode;
	}

	public void setDefaultWeightUnitCode(String defaultWeightUnitCode) {
		this.defaultWeightUnitCode = defaultWeightUnitCode;
	}

	public String getOfficeStreet() {
		return this.officeStreet;
	}

	public void setOfficeStreet(String officeStreet) {
		this.officeStreet = officeStreet;
	}

	public String getOfficeCity() {
		return this.officeCity;
	}

	public void setOfficeCity(String officeCity) {
		this.officeCity = officeCity;
	}

	public String getOfficePostalCode() {
		return this.officePostalCode;
	}

	public void setOfficePostalCode(String officePostalCode) {
		this.officePostalCode = officePostalCode;
	}

	public Boolean getIsSubsidiary() {
		return this.isSubsidiary;
	}

	public void setIsSubsidiary(Boolean isSubsidiary) {
		this.isSubsidiary = isSubsidiary;
	}

	public Boolean getIsCustomer() {
		return this.isCustomer;
	}

	public void setIsCustomer(Boolean isCustomer) {
		this.isCustomer = isCustomer;
	}

	public Boolean getIsSupplier() {
		return this.isSupplier;
	}

	public void setIsSupplier(Boolean isSupplier) {
		this.isSupplier = isSupplier;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public NatureOfBusiness getNatureOfBusiness() {
		return this.natureOfBusiness;
	}

	public void setNatureOfBusiness(NatureOfBusiness natureOfBusiness) {
		this.natureOfBusiness = natureOfBusiness;
	}

	public CustomerStatus getStatus() {
		return this.status;
	}

	public void setStatus(CustomerStatus status) {
		this.status = status;
	}

	public CustomerType getType() {
		return this.type;
	}

	public void setType(CustomerType type) {
		this.type = type;
	}

	public Boolean getIplAgent() {
		return this.iplAgent;
	}

	public void setIplAgent(Boolean iplAgent) {
		this.iplAgent = iplAgent;
	}

	public Boolean getFuelSupplier() {
		return this.fuelSupplier;
	}

	public void setFuelSupplier(Boolean fuelSupplier) {
		this.fuelSupplier = fuelSupplier;
	}

	public Boolean getFixedBaseOperator() {
		return this.fixedBaseOperator;
	}

	public void setFixedBaseOperator(Boolean fixedBaseOperator) {
		this.fixedBaseOperator = fixedBaseOperator;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getPhoneNo() {
		return this.phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getFaxNo() {
		return this.faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAddress() {
		return this.bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public String getBankAccountNo() {
		return this.bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getBankIbanNo() {
		return this.bankIbanNo;
	}

	public void setBankIbanNo(String bankIbanNo) {
		this.bankIbanNo = bankIbanNo;
	}

	public String getBankSwiftNo() {
		return this.bankSwiftNo;
	}

	public void setBankSwiftNo(String bankSwiftNo) {
		this.bankSwiftNo = bankSwiftNo;
	}

	public Integer getTimeZoneId() {
		return this.timeZoneId;
	}

	public void setTimeZoneId(Integer timeZoneId) {
		this.timeZoneId = timeZoneId;
	}

	public String getTimeZoneName() {
		return this.timeZoneName;
	}

	public void setTimeZoneName(String timeZoneName) {
		this.timeZoneName = timeZoneName;
	}

	public Integer getSubsidiaryCurrencyId() {
		return this.subsidiaryCurrencyId;
	}

	public void setSubsidiaryCurrencyId(Integer subsidiaryCurrencyId) {
		this.subsidiaryCurrencyId = subsidiaryCurrencyId;
	}

	public String getSubsidiaryCurrencyCode() {
		return this.subsidiaryCurrencyCode;
	}

	public void setSubsidiaryCurrencyCode(String subsidiaryCurrencyCode) {
		this.subsidiaryCurrencyCode = subsidiaryCurrencyCode;
	}

	public Boolean getSubsidiaryCurrencyActive() {
		return this.subsidiaryCurrencyActive;
	}

	public void setSubsidiaryCurrencyActive(Boolean subsidiaryCurrencyActive) {
		this.subsidiaryCurrencyActive = subsidiaryCurrencyActive;
	}

	public CustomerDataTransmissionStatus getTransmissionStatus() {
		return this.transmissionStatus;
	}

	public void setTransmissionStatus(
			CustomerDataTransmissionStatus transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public CustomerDataProcessedStatus getProcessedStatus() {
		return this.processedStatus;
	}

	public void setProcessedStatus(CustomerDataProcessedStatus processedStatus) {
		this.processedStatus = processedStatus;
	}

	public CreditUpdateRequestStatus getCreditUpdateRequestStatus() {
		return this.creditUpdateRequestStatus;
	}

	public void setCreditUpdateRequestStatus(
			CreditUpdateRequestStatus creditUpdateRequestStatus) {
		this.creditUpdateRequestStatus = creditUpdateRequestStatus;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}

	public String getDummyField() {
		return this.dummyField;
	}

	public void setDummyField(String dummyField) {
		this.dummyField = dummyField;
	}
}
