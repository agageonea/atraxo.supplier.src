/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.exchangerate.model;

import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ExchangeRateValue.class, sort = {@SortField(field = ExchangeRateValue_Ds.F_VLDFRDTREF, desc = true)})
@RefLookups({@RefLookup(refId = ExchangeRateValue_Ds.F_CURRENCY1ID),
		@RefLookup(refId = ExchangeRateValue_Ds.F_CURRENCY2ID),
		@RefLookup(refId = ExchangeRateValue_Ds.F_EXCHANGERATEID)})
public class ExchangeRateValue_Ds extends AbstractDs_Ds<ExchangeRateValue> {

	public static final String ALIAS = "fmbas_ExchangeRateValue_Ds";

	public static final String F_EXCHANGERATEID = "exchangeRateId";
	public static final String F_CURRENCY1ID = "currency1Id";
	public static final String F_CURRENCY1CODE = "currency1Code";
	public static final String F_CURRENCY2ID = "currency2Id";
	public static final String F_CURRENCY2CODE = "currency2Code";
	public static final String F_VLDFRDTREF = "vldFrDtRef";
	public static final String F_VLDTODTREF = "vldToDtRef";
	public static final String F_ISPRVSN = "isPrvsn";
	public static final String F_RATE = "rate";
	public static final String F_POSTTYPEIND = "postTypeInd";
	public static final String F_ACTIVE = "active";
	public static final String F_PERRATE = "perRate";
	public static final String F_METHODNAME = "methodName";
	public static final String F_LONGMETHODNAME = "longMethodName";
	public static final String F_DECIMALS = "decimals";

	@DsField(join = "left", path = "exchRate.id")
	private Integer exchangeRateId;

	@DsField(join = "left", path = "exchRate.currency1.id")
	private Integer currency1Id;

	@DsField(join = "left", path = "exchRate.currency1.code")
	private String currency1Code;

	@DsField(join = "left", path = "exchRate.currency2.id")
	private Integer currency2Id;

	@DsField(join = "left", path = "exchRate.currency2.code")
	private String currency2Code;

	@DsField(jpqlFilter = " e.vldToDtRef >= :vldFrDtRef ")
	private Date vldFrDtRef;

	@DsField(jpqlFilter = " e.vldFrDtRef >= :vldToDtRef ")
	private Date vldToDtRef;

	@DsField
	private Boolean isPrvsn;

	@DsField
	private BigDecimal rate;

	@DsField
	private Integer postTypeInd;

	@DsField
	private Boolean active;

	@DsField(fetch = false)
	private BigDecimal perRate;

	@DsField(join = "left", path = "exchRate.avgMethodIndicator.code")
	private String methodName;

	@DsField(join = "left", path = "exchRate.avgMethodIndicator.name")
	private String longMethodName;

	@DsField(join = "left", path = "exchRate.tserie.decimals")
	private Integer decimals;

	/**
	 * Default constructor
	 */
	public ExchangeRateValue_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ExchangeRateValue_Ds(ExchangeRateValue e) {
		super(e);
	}

	public Integer getExchangeRateId() {
		return this.exchangeRateId;
	}

	public void setExchangeRateId(Integer exchangeRateId) {
		this.exchangeRateId = exchangeRateId;
	}

	public Integer getCurrency1Id() {
		return this.currency1Id;
	}

	public void setCurrency1Id(Integer currency1Id) {
		this.currency1Id = currency1Id;
	}

	public String getCurrency1Code() {
		return this.currency1Code;
	}

	public void setCurrency1Code(String currency1Code) {
		this.currency1Code = currency1Code;
	}

	public Integer getCurrency2Id() {
		return this.currency2Id;
	}

	public void setCurrency2Id(Integer currency2Id) {
		this.currency2Id = currency2Id;
	}

	public String getCurrency2Code() {
		return this.currency2Code;
	}

	public void setCurrency2Code(String currency2Code) {
		this.currency2Code = currency2Code;
	}

	public Date getVldFrDtRef() {
		return this.vldFrDtRef;
	}

	public void setVldFrDtRef(Date vldFrDtRef) {
		this.vldFrDtRef = vldFrDtRef;
	}

	public Date getVldToDtRef() {
		return this.vldToDtRef;
	}

	public void setVldToDtRef(Date vldToDtRef) {
		this.vldToDtRef = vldToDtRef;
	}

	public Boolean getIsPrvsn() {
		return this.isPrvsn;
	}

	public void setIsPrvsn(Boolean isPrvsn) {
		this.isPrvsn = isPrvsn;
	}

	public BigDecimal getRate() {
		return this.rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Integer getPostTypeInd() {
		return this.postTypeInd;
	}

	public void setPostTypeInd(Integer postTypeInd) {
		this.postTypeInd = postTypeInd;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public BigDecimal getPerRate() {
		return this.perRate;
	}

	public void setPerRate(BigDecimal perRate) {
		this.perRate = perRate;
	}

	public String getMethodName() {
		return this.methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getLongMethodName() {
		return this.longMethodName;
	}

	public void setLongMethodName(String longMethodName) {
		this.longMethodName = longMethodName;
	}

	public Integer getDecimals() {
		return this.decimals;
	}

	public void setDecimals(Integer decimals) {
		this.decimals = decimals;
	}
}
