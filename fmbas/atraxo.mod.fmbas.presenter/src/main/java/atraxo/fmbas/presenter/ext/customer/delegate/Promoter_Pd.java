package atraxo.fmbas.presenter.ext.customer.delegate;

import java.util.ArrayList;
import java.util.List;

import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.presenter.impl.customer.model.FinalCustomer_Ds;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class Promoter_Pd extends AbstractPresenterDelegate {

	public void promote(List<FinalCustomer_Ds> list) throws Exception {
		ICustomerService srv = (ICustomerService) this.findEntityService(Customer.class);
		List<Customer> customers = new ArrayList<>();
		for (FinalCustomer_Ds ds : list) {
			customers.add(srv.findById(ds.getId()));
		}
		srv.promoteFinalCustomers(customers);
	}

}
