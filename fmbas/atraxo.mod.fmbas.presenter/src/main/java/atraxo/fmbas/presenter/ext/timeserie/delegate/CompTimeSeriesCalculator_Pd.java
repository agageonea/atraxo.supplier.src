package atraxo.fmbas.presenter.ext.timeserie.delegate;

import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.presenter.impl.timeserie.model.EnergyPriceTimeSerie_Ds;
import atraxo.fmbas.presenter.impl.timeserie.model.ExchangeRateTimeSerie_Ds;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class CompTimeSeriesCalculator_Pd extends AbstractPresenterDelegate {

	public void calculate(EnergyPriceTimeSerie_Ds ds) throws Exception {
		this.calculate(ds.getId());
	}

	public void calculate(ExchangeRateTimeSerie_Ds ds) throws Exception {
		this.calculate(ds.getId());
	}

	private void calculate(Integer id) throws Exception {
		ITimeSerieService srv = (ITimeSerieService) this.findEntityService(TimeSerie.class);
		srv.calculate(id);
	}

}
