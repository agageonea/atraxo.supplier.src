/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.dictionary.model;

import atraxo.fmbas.domain.impl.dictionary.Field;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.presenter.model.AbstractDsModel;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Field.class)
public class Field_Ds extends AbstractDsModel<Field> {

	public static final String ALIAS = "fmbas_Field_Ds";

	public static final String F_EXTERNALINTERFACEID = "externalInterfaceId";
	public static final String F_EXTERNALINTERFACENAME = "externalInterfaceName";
	public static final String F_FIELDNAME = "fieldName";

	@DsField(fetch = false)
	private Integer externalInterfaceId;

	@DsField(fetch = false)
	private String externalInterfaceName;

	@DsField(fetch = false)
	private String fieldName;

	/**
	 * Default constructor
	 */
	public Field_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Field_Ds(Field e) {
		super(e);
	}

	public Integer getExternalInterfaceId() {
		return this.externalInterfaceId;
	}

	public void setExternalInterfaceId(Integer externalInterfaceId) {
		this.externalInterfaceId = externalInterfaceId;
	}

	public String getExternalInterfaceName() {
		return this.externalInterfaceName;
	}

	public void setExternalInterfaceName(String externalInterfaceName) {
		this.externalInterfaceName = externalInterfaceName;
	}

	public String getFieldName() {
		return this.fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
}
