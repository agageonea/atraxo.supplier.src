/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.creditLines.service;

import java.util.List;

import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.presenter.impl.creditLines.model.CustomerCreditLines_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class CustomerCreditLines_DsService extends AbstractEntityDsService<CustomerCreditLines_Ds, CustomerCreditLines_Ds, Object, CreditLines>
		implements IDsService<CustomerCreditLines_Ds, CustomerCreditLines_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<CustomerCreditLines_Ds, CustomerCreditLines_Ds, Object> builder, List<CustomerCreditLines_Ds> result)
			throws Exception {
		super.postFind(builder, result);
		for (CustomerCreditLines_Ds res : result) {
			res.setCurrencyDisplay(res.getCurrencyCode());
		}
	}

}
