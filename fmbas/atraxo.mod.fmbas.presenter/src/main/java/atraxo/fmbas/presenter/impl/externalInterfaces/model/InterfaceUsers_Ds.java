/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.externalInterfaces.model;

import atraxo.fmbas.domain.impl.externalInterfaces.InterfaceUser;
import atraxo.fmbas.domain.impl.fmbas_type.InterfaceNotificationCondition;
import atraxo.fmbas.domain.impl.fmbas_type.NotificationType;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = InterfaceUser.class)
@RefLookups({
		@RefLookup(refId = InterfaceUsers_Ds.F_INTERFACEID),
		@RefLookup(refId = InterfaceUsers_Ds.F_USERID, namedQuery = UserSupp.NQ_FIND_BY_LOGIN, params = {@Param(name = "loginName", field = InterfaceUsers_Ds.F_LOGINNAME)}),
		@RefLookup(refId = InterfaceUsers_Ds.F_EMAILTEMPLATEID)})
public class InterfaceUsers_Ds extends AbstractDs_Ds<InterfaceUser> {

	public static final String ALIAS = "fmbas_InterfaceUsers_Ds";

	public static final String F_INTERFACEID = "interfaceId";
	public static final String F_INTERFACENAME = "interfaceName";
	public static final String F_USERID = "userId";
	public static final String F_USERNAME = "userName";
	public static final String F_USEREMAIL = "userEmail";
	public static final String F_LOGINNAME = "loginName";
	public static final String F_EMAILTEMPLATEID = "emailTemplateId";
	public static final String F_EMAILTEMPLATENAME = "emailTemplateName";
	public static final String F_NOTIFICATIONCONDITION = "notificationCondition";
	public static final String F_ENABLED = "enabled";
	public static final String F_NOTIFICATIONTYPE = "notificationType";
	public static final String F_EMAILSUBJECT = "emailSubject";

	@DsField(join = "left", path = "externalInterface.id")
	private Integer interfaceId;

	@DsField(join = "left", path = "externalInterface.name")
	private String interfaceName;

	@DsField(join = "left", path = "user.id")
	private String userId;

	@DsField(join = "left", path = "user.name")
	private String userName;

	@DsField(join = "left", path = "user.email")
	private String userEmail;

	@DsField(join = "left", path = "user.loginName")
	private String loginName;

	@DsField(join = "left", path = "emailTemplate.id")
	private Integer emailTemplateId;

	@DsField(join = "left", path = "emailTemplate.name")
	private String emailTemplateName;

	@DsField
	private InterfaceNotificationCondition notificationCondition;

	@DsField
	private Boolean enabled;

	@DsField
	private NotificationType notificationType;

	@DsField
	private String emailSubject;

	/**
	 * Default constructor
	 */
	public InterfaceUsers_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public InterfaceUsers_Ds(InterfaceUser e) {
		super(e);
	}

	public Integer getInterfaceId() {
		return this.interfaceId;
	}

	public void setInterfaceId(Integer interfaceId) {
		this.interfaceId = interfaceId;
	}

	public String getInterfaceName() {
		return this.interfaceName;
	}

	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Integer getEmailTemplateId() {
		return this.emailTemplateId;
	}

	public void setEmailTemplateId(Integer emailTemplateId) {
		this.emailTemplateId = emailTemplateId;
	}

	public String getEmailTemplateName() {
		return this.emailTemplateName;
	}

	public void setEmailTemplateName(String emailTemplateName) {
		this.emailTemplateName = emailTemplateName;
	}

	public InterfaceNotificationCondition getNotificationCondition() {
		return this.notificationCondition;
	}

	public void setNotificationCondition(
			InterfaceNotificationCondition notificationCondition) {
		this.notificationCondition = notificationCondition;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public NotificationType getNotificationType() {
		return this.notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public String getEmailSubject() {
		return this.emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
}
