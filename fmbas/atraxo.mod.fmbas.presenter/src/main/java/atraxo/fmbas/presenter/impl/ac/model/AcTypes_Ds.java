/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.ac.model;

import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = AcTypes.class, sort = {@SortField(field = AcTypes_Ds.F_CODE)})
@RefLookups({@RefLookup(refId = AcTypes_Ds.F_UNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = AcTypes_Ds.F_UNITCODE)})})
public class AcTypes_Ds extends AbstractDs_Ds<AcTypes> {

	public static final String ALIAS = "fmbas_AcTypes_Ds";

	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_CODE = "code";
	public static final String F_ACTIVE = "active";
	public static final String F_NAME = "name";
	public static final String F_TANKCAPACITY = "tankCapacity";
	public static final String F_BURNOFFPROH = "burnoffProH";

	@DsField(join = "left", path = "unit.id")
	private Integer unitId;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	@DsField
	private String code;

	@DsField
	private Boolean active;

	@DsField
	private String name;

	@DsField
	private Integer tankCapacity;

	@DsField
	private Integer burnoffProH;

	/**
	 * Default constructor
	 */
	public AcTypes_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AcTypes_Ds(AcTypes e) {
		super(e);
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTankCapacity() {
		return this.tankCapacity;
	}

	public void setTankCapacity(Integer tankCapacity) {
		this.tankCapacity = tankCapacity;
	}

	public Integer getBurnoffProH() {
		return this.burnoffProH;
	}

	public void setBurnoffProH(Integer burnoffProH) {
		this.burnoffProH = burnoffProH;
	}
}
