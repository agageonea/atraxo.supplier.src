/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.externalInterfaces.service;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceHistoryService;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceHistory;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfacesHistoryStatus;
import atraxo.fmbas.domain.impl.fmbas_type.LastRequestStatus;
import atraxo.fmbas.presenter.impl.externalInterfaces.model.ExternalInterfaces_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service ExternalInterfaces_DsService
 */
public class ExternalInterfaces_DsService extends AbstractEntityDsService<ExternalInterfaces_Ds, ExternalInterfaces_Ds, Object, ExternalInterface>
		implements IDsService<ExternalInterfaces_Ds, ExternalInterfaces_Ds, Object> {

	@Autowired
	private IExternalInterfaceHistoryService srv;

	@Override
	protected void postFind(IQueryBuilder<ExternalInterfaces_Ds, ExternalInterfaces_Ds, Object> builder, List<ExternalInterfaces_Ds> result)
			throws Exception {
		for (Iterator<ExternalInterfaces_Ds> iter = result.iterator(); iter.hasNext();) {
			ExternalInterfaces_Ds ds = iter.next();
			ExternalInterfaceHistory e = this.srv.findLastEntry(ds.getId());
			if (e != null) {
				ds.setLastRequest(e.getRequestReceived());
				if (ExternalInterfacesHistoryStatus._SUCCESS_.equals(e.getStatus())) {
					ds.setLastRequestStatus(LastRequestStatus._SUCCESS_.getName());
				} else {
					ds.setLastRequestStatus(LastRequestStatus._FAILURE_.getName());
				}
			}
		}
	}
}
