package atraxo.fmbas.presenter.ext.customer.exception;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import seava.j4e.api.exceptions.BusinessException;

public class FinalCustomerDeleteException extends BusinessException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param names
	 *            - a comma separated list with customer names.
	 */
	public FinalCustomerDeleteException(String names) {
		super(BusinessErrorCode.FINAL_CUSTOMER_DELETE_ERROR, String.format(BusinessErrorCode.FINAL_CUSTOMER_DELETE_ERROR.getErrMsg(), names));
	}
}
