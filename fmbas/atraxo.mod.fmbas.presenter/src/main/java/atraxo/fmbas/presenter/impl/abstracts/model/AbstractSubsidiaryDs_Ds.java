/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.abstracts.model;

import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.model.IModelWithSubsidiaryId;

/**
 * Generated code. Do not modify in this file.
 */
public class AbstractSubsidiaryDs_Ds<E> extends AbstractDs_Ds<E>
		implements
			IModelWithSubsidiaryId {

	public static final String ALIAS = "fmbas_AbstractSubsidiaryDs_Ds";

	public static final String F_SUBSIDIARYID = "subsidiaryId";

	@DsField(noUpdate = true)
	private String subsidiaryId;

	/**
	 * Default constructor
	 */
	public AbstractSubsidiaryDs_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AbstractSubsidiaryDs_Ds(E e) {
		super(e);
	}

	public String getSubsidiaryId() {
		return this.subsidiaryId;
	}

	public void setSubsidiaryId(String subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}
}
