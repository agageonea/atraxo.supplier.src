/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.geo.model;

import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.geo.TimeZone;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;
import seava.j4e.api.model.IModelWithId;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Locations.class, sort = {@SortField(field = Locations_Ds.F_NAME)})
@RefLookups({
		@RefLookup(refId = Locations_Ds.F_COUNTRYID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Locations_Ds.F_COUNTRYCODE)}),
		@RefLookup(refId = Locations_Ds.F_SUBCOUNTRYID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Locations_Ds.F_SUBCOUNTRYCODE)}),
		@RefLookup(refId = Locations_Ds.F_TIMEID, namedQuery = TimeZone.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = Locations_Ds.F_TIMENAME)})})
public class Locations_Ds extends AbstractDs_Ds<Locations>
		implements
			IModelWithId<Integer> {

	public static final String ALIAS = "fmbas_Locations_Ds";

	public static final String F_COUNTRYID = "countryId";
	public static final String F_COUNTRYCODE = "countryCode";
	public static final String F_COUNTRYNAME = "countryName";
	public static final String F_SUBCOUNTRYID = "subCountryId";
	public static final String F_SUBCOUNTRYCODE = "subCountryCode";
	public static final String F_TIMEID = "timeId";
	public static final String F_TIMENAME = "timeName";
	public static final String F_ID = "id";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_IATACODE = "iataCode";
	public static final String F_ICAOCODE = "icaoCode";
	public static final String F_ISAIRPORT = "isAirport";
	public static final String F_LATITUDE = "latitude";
	public static final String F_LONGITUDE = "longitude";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";

	@DsField(join = "left", path = "country.id")
	private Integer countryId;

	@DsField(join = "left", path = "country.code")
	private String countryCode;

	@DsField(join = "left", path = "country.name")
	private String countryName;

	@DsField(join = "left", path = "subCountry.id")
	private Integer subCountryId;

	@DsField(join = "left", path = "subCountry.code")
	private String subCountryCode;

	@DsField(join = "left", path = "timezone.id")
	private Integer timeId;

	@DsField(join = "left", path = "timezone.name")
	private String timeName;

	@DsField
	private Integer id;

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private String iataCode;

	@DsField
	private String icaoCode;

	@DsField
	private Boolean isAirport;

	@DsField
	private BigDecimal latitude;

	@DsField
	private BigDecimal longitude;

	@DsField(jpqlFilter = " e.validTo >= :validFrom ")
	private Date validFrom;

	@DsField(jpqlFilter = " e.validFrom <= :validTo ")
	private Date validTo;

	/**
	 * Default constructor
	 */
	public Locations_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Locations_Ds(Locations e) {
		super(e);
	}

	public Integer getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Integer getSubCountryId() {
		return this.subCountryId;
	}

	public void setSubCountryId(Integer subCountryId) {
		this.subCountryId = subCountryId;
	}

	public String getSubCountryCode() {
		return this.subCountryCode;
	}

	public void setSubCountryCode(String subCountryCode) {
		this.subCountryCode = subCountryCode;
	}

	public Integer getTimeId() {
		return this.timeId;
	}

	public void setTimeId(Integer timeId) {
		this.timeId = timeId;
	}

	public String getTimeName() {
		return this.timeName;
	}

	public void setTimeName(String timeName) {
		this.timeName = timeName;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public String getIcaoCode() {
		return this.icaoCode;
	}

	public void setIcaoCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}

	public Boolean getIsAirport() {
		return this.isAirport;
	}

	public void setIsAirport(Boolean isAirport) {
		this.isAirport = isAirport;
	}

	public BigDecimal getLatitude() {
		return this.latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return this.longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
}
