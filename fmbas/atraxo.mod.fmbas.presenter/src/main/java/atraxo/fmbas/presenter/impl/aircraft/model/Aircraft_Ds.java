/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.aircraft.model;

import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Aircraft.class, sort = {@SortField(field = Aircraft_Ds.F_ACTYPECODE)})
@RefLookups({
		@RefLookup(refId = Aircraft_Ds.F_ACTYPEID, namedQuery = AcTypes.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Aircraft_Ds.F_ACTYPECODE)}),
		@RefLookup(refId = Aircraft_Ds.F_CUSTID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Aircraft_Ds.F_CUSTCODE)}),
		@RefLookup(refId = Aircraft_Ds.F_OWNERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Aircraft_Ds.F_OWNERCODE)}),
		@RefLookup(refId = Aircraft_Ds.F_OPERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Aircraft_Ds.F_OPERCODE)})})
public class Aircraft_Ds extends AbstractDs_Ds<Aircraft> {

	public static final String ALIAS = "fmbas_Aircraft_Ds";

	public static final String F_ACTYPEID = "acTypeId";
	public static final String F_ACTYPECODE = "acTypeCode";
	public static final String F_CUSTID = "custId";
	public static final String F_CUSTCODE = "custCode";
	public static final String F_OWNERID = "ownerId";
	public static final String F_OWNERCODE = "ownerCode";
	public static final String F_OPERID = "operId";
	public static final String F_OPERCODE = "operCode";
	public static final String F_REGISTRATION = "registration";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_LABELFIELD = "labelField";

	@DsField(join = "left", path = "acTypes.id")
	private Integer acTypeId;

	@DsField(join = "left", path = "acTypes.code")
	private String acTypeCode;

	@DsField(join = "left", path = "customer.id")
	private Integer custId;

	@DsField(join = "left", path = "customer.code")
	private String custCode;

	@DsField(join = "left", path = "owner.id")
	private Integer ownerId;

	@DsField(join = "left", path = "owner.code")
	private String ownerCode;

	@DsField(join = "left", path = "operator.id")
	private Integer operId;

	@DsField(join = "left", path = "operator.code")
	private String operCode;

	@DsField
	private String registration;

	@DsField(jpqlFilter = " e.validTo >= :validFrom ")
	private Date validFrom;

	@DsField(jpqlFilter = " e.validFrom <= :validTo ")
	private Date validTo;

	@DsField(fetch = false)
	private String labelField;

	/**
	 * Default constructor
	 */
	public Aircraft_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Aircraft_Ds(Aircraft e) {
		super(e);
	}

	public Integer getAcTypeId() {
		return this.acTypeId;
	}

	public void setAcTypeId(Integer acTypeId) {
		this.acTypeId = acTypeId;
	}

	public String getAcTypeCode() {
		return this.acTypeCode;
	}

	public void setAcTypeCode(String acTypeCode) {
		this.acTypeCode = acTypeCode;
	}

	public Integer getCustId() {
		return this.custId;
	}

	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	public String getCustCode() {
		return this.custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public Integer getOwnerId() {
		return this.ownerId;
	}

	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}

	public String getOwnerCode() {
		return this.ownerCode;
	}

	public void setOwnerCode(String ownerCode) {
		this.ownerCode = ownerCode;
	}

	public Integer getOperId() {
		return this.operId;
	}

	public void setOperId(Integer operId) {
		this.operId = operId;
	}

	public String getOperCode() {
		return this.operCode;
	}

	public void setOperCode(String operCode) {
		this.operCode = operCode;
	}

	public String getRegistration() {
		return this.registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}
}
