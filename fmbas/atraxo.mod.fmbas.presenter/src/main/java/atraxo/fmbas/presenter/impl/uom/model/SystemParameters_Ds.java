/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.uom.model;

import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = SystemParameter.class)
public class SystemParameters_Ds extends AbstractDs_Ds<SystemParameter> {

	public static final String ALIAS = "fmbas_SystemParameters_Ds";

	public static final String F_CODE = "code";
	public static final String F_VALUE = "value";
	public static final String F_SYSUNITS = "sysUnits";
	public static final String F_SYSLENGHT = "sysLenght";
	public static final String F_SYSVOL = "sysVol";
	public static final String F_SYSWEIGHT = "sysWeight";
	public static final String F_SYSDENSITY = "sysDensity";
	public static final String F_SYSCURRENCY = "sysCurrency";
	public static final String F_SYSAVGMTHD = "sysAvgMthd";

	@DsField
	private String code;

	@DsField
	private String value;

	@DsField(fetch = false)
	private String sysUnits;

	@DsField(fetch = false)
	private String sysLenght;

	@DsField(fetch = false)
	private String sysVol;

	@DsField(fetch = false)
	private String sysWeight;

	@DsField(fetch = false)
	private BigDecimal sysDensity;

	@DsField(fetch = false)
	private String sysCurrency;

	@DsField(fetch = false)
	private String sysAvgMthd;

	/**
	 * Default constructor
	 */
	public SystemParameters_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public SystemParameters_Ds(SystemParameter e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getSysUnits() {
		return this.sysUnits;
	}

	public void setSysUnits(String sysUnits) {
		this.sysUnits = sysUnits;
	}

	public String getSysLenght() {
		return this.sysLenght;
	}

	public void setSysLenght(String sysLenght) {
		this.sysLenght = sysLenght;
	}

	public String getSysVol() {
		return this.sysVol;
	}

	public void setSysVol(String sysVol) {
		this.sysVol = sysVol;
	}

	public String getSysWeight() {
		return this.sysWeight;
	}

	public void setSysWeight(String sysWeight) {
		this.sysWeight = sysWeight;
	}

	public BigDecimal getSysDensity() {
		return this.sysDensity;
	}

	public void setSysDensity(BigDecimal sysDensity) {
		this.sysDensity = sysDensity;
	}

	public String getSysCurrency() {
		return this.sysCurrency;
	}

	public void setSysCurrency(String sysCurrency) {
		this.sysCurrency = sysCurrency;
	}

	public String getSysAvgMthd() {
		return this.sysAvgMthd;
	}

	public void setSysAvgMthd(String sysAvgMthd) {
		this.sysAvgMthd = sysAvgMthd;
	}
}
