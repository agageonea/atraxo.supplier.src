/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.fmbas.presenter.ext.uom.qb;

import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.presenter.impl.uom.model.MeasurmentUnitsEventLov_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class MeasurmentUnitsEventLov_DsQb extends QueryBuilderWithJpql<MeasurmentUnitsEventLov_Ds, MeasurmentUnitsEventLov_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();

		this.addFilterCondition("(e.unittypeInd = :eventUnit)");
		
		this.addCustomFilterItem("eventUnit", UnitType._EVENT_);
	}
}
