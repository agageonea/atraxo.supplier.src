/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.job.model;

import atraxo.ad.domain.impl.scheduler.StepExecution;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.presenter.model.AbstractDsModel;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = StepExecution.class)
@RefLookups({@RefLookup(refId = StepExecution_Ds.F_JOBEXECSTEPID)})
public class StepExecution_Ds extends AbstractDsModel<StepExecution> {

	public static final String ALIAS = "fmbas_StepExecution_Ds";

	public static final String F_JOBEXECSTEPEXITCODE = "jobExecStepExitCode";
	public static final String F_JOBEXECSTEPID = "jobExecStepId";
	public static final String F_JOBEXECSTEPEXITMESSAGE = "jobExecStepExitMessage";
	public static final String F_JOBEXECSTEPSTATUS = "jobExecStepStatus";
	public static final String F_JOBNAME = "jobName";
	public static final String F_JOBINSTANCEID = "jobInstanceId";
	public static final String F_JOBINSTANCEKEY = "jobInstanceKey";
	public static final String F_COMMITCOUNT = "commitCount";
	public static final String F_STARTTIME = "startTime";
	public static final String F_ENDTIME = "endTime";
	public static final String F_EXITCODE = "exitCode";
	public static final String F_EXITMESSAGE = "exitMessage";
	public static final String F_READCOUNT = "readCount";
	public static final String F_ROLLBACKCOUNT = "rollbackCount";
	public static final String F_STATUS = "status";
	public static final String F_WRITECOUNT = "writeCount";

	@DsField(join = "left", path = "jobExecStep.exitCode")
	private String jobExecStepExitCode;

	@DsField(join = "left", path = "jobExecStep.id")
	private Long jobExecStepId;

	@DsField(join = "left", path = "jobExecStep.exitMessage")
	private String jobExecStepExitMessage;

	@DsField(join = "left", path = "jobExecStep.status")
	private String jobExecStepStatus;

	@DsField(join = "left", path = "jobExecStep.jobInstance.jobName")
	private String jobName;

	@DsField(join = "left", path = "jobExecStep.jobInstance.id")
	private Long jobInstanceId;

	@DsField(join = "left", path = "jobExecStep.jobInstance.jobKey")
	private String jobInstanceKey;

	@DsField
	private Long commitCount;

	@DsField
	private Date startTime;

	@DsField
	private Date endTime;

	@DsField
	private String exitCode;

	@DsField
	private String exitMessage;

	@DsField
	private Long readCount;

	@DsField
	private Long rollbackCount;

	@DsField
	private String status;

	@DsField
	private Long writeCount;

	/**
	 * Default constructor
	 */
	public StepExecution_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public StepExecution_Ds(StepExecution e) {
		super(e);
	}

	public String getJobExecStepExitCode() {
		return this.jobExecStepExitCode;
	}

	public void setJobExecStepExitCode(String jobExecStepExitCode) {
		this.jobExecStepExitCode = jobExecStepExitCode;
	}

	public Long getJobExecStepId() {
		return this.jobExecStepId;
	}

	public void setJobExecStepId(Long jobExecStepId) {
		this.jobExecStepId = jobExecStepId;
	}

	public String getJobExecStepExitMessage() {
		return this.jobExecStepExitMessage;
	}

	public void setJobExecStepExitMessage(String jobExecStepExitMessage) {
		this.jobExecStepExitMessage = jobExecStepExitMessage;
	}

	public String getJobExecStepStatus() {
		return this.jobExecStepStatus;
	}

	public void setJobExecStepStatus(String jobExecStepStatus) {
		this.jobExecStepStatus = jobExecStepStatus;
	}

	public String getJobName() {
		return this.jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public Long getJobInstanceId() {
		return this.jobInstanceId;
	}

	public void setJobInstanceId(Long jobInstanceId) {
		this.jobInstanceId = jobInstanceId;
	}

	public String getJobInstanceKey() {
		return this.jobInstanceKey;
	}

	public void setJobInstanceKey(String jobInstanceKey) {
		this.jobInstanceKey = jobInstanceKey;
	}

	public Long getCommitCount() {
		return this.commitCount;
	}

	public void setCommitCount(Long commitCount) {
		this.commitCount = commitCount;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getExitCode() {
		return this.exitCode;
	}

	public void setExitCode(String exitCode) {
		this.exitCode = exitCode;
	}

	public String getExitMessage() {
		return this.exitMessage;
	}

	public void setExitMessage(String exitMessage) {
		this.exitMessage = exitMessage;
	}

	public Long getReadCount() {
		return this.readCount;
	}

	public void setReadCount(Long readCount) {
		this.readCount = readCount;
	}

	public Long getRollbackCount() {
		return this.rollbackCount;
	}

	public void setRollbackCount(Long rollbackCount) {
		this.rollbackCount = rollbackCount;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getWriteCount() {
		return this.writeCount;
	}

	public void setWriteCount(Long writeCount) {
		this.writeCount = writeCount;
	}
}
