/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.user.service;

import java.util.List;

import javax.persistence.PersistenceException;

import atraxo.ad.domain.impl.security.User;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.fmbas_type.Department;
import atraxo.fmbas.domain.impl.fmbas_type.Title;
import atraxo.fmbas.domain.impl.fmbas_type.WorkOffice;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.presenter.impl.user.model.UserSupp_Ds;
import atraxo.fmbas.presenter.impl.user.model.UserSupp_DsParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class UserSupp_DsService extends AbstractEntityDsService<UserSupp_Ds, UserSupp_Ds, UserSupp_DsParam, UserSupp>
		implements IDsService<UserSupp_Ds, UserSupp_Ds, UserSupp_DsParam> {

	@Override
	protected void preInsert(UserSupp_Ds ds, UserSupp_DsParam params) throws Exception {
		super.preInsert(ds, params);
		this.setCombos(ds);
	}

	@Override
	protected void preUpdate(UserSupp_Ds ds, UserSupp_DsParam params) throws Exception {
		super.preUpdate(ds, params);
		this.setCombos(ds);
	}

	@Override
	public void deleteByIds(List<Object> ids) throws Exception {
		try {
			super.deleteByIds(ids);
		} catch (PersistenceException e) {
			throw new BusinessException(BusinessErrorCode.ENTITY_CAN_NOT_DELETED,
					String.format(BusinessErrorCode.ENTITY_CAN_NOT_DELETED.getErrMsg(), User.class.getSimpleName()), e);
		}
	}

	private void setCombos(UserSupp_Ds ds) {
		if (ds.getWorkOffice() == null) {
			ds.setWorkOffice(WorkOffice._EMPTY_);
		}
		if (ds.getTitle() == null) {
			ds.setTitle(Title._EMPTY_);
		}
		if (ds.getDepartment() == null) {
			ds.setDepartment(Department._EMPTY_);
		}
	}

}
