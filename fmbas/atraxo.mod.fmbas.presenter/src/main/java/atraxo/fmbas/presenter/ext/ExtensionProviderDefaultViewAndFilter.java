package atraxo.fmbas.presenter.ext;

import java.util.List;

import atraxo.fmbas.business.api.soneAdvancedFilter.ISoneAdvancedFilterService;
import atraxo.fmbas.business.api.soneDefaultViewState.ISoneDefaultViewStateService;
import atraxo.fmbas.domain.impl.soneAdvancedFilter.SoneAdvancedFilter;
import atraxo.fmbas.domain.impl.soneDefaultViewState.SoneDefaultViewState;
import seava.j4e.api.extensions.IExtensionContentProvider;
import seava.j4e.presenter.service.AbstractPresenterBaseService;

public class ExtensionProviderDefaultViewAndFilter extends AbstractPresenterBaseService implements IExtensionContentProvider {

	@Override
	public String getContent(String targetFrame) throws Exception {
		StringBuffer sb = new StringBuffer();

		String cmpName = targetFrame;
		List<SoneDefaultViewState> soneDefaultViewState = this.listDefaultViewStates(cmpName);
		List<SoneAdvancedFilter> advancedFilters = this.listAdvancedFilters(cmpName);
		return this.addDefaultElements(targetFrame, sb, soneDefaultViewState, advancedFilters, cmpName);
	}

	protected String addDefaultElements(String targetFrame, StringBuffer sb, List<SoneDefaultViewState> viewList, List<SoneAdvancedFilter> filterList,
			String cmpName) throws Exception {

		Integer cnt = 0;
		String viewValue = "";
		Integer viewId = null;
		
		
		Integer pageSize = null;
		Boolean totals = false;
		String viewName = "";
		
		String cmp1 = "";
		String unit = "";
		String currency = "";
		String createdBy = "";
		Boolean availableSystemWide = false;
		

		sb.append("\n\n\t __DEFAULTELEMENTS__ = [ \n");

		for (SoneDefaultViewState soneDefaultViewState : viewList) { 
			viewName = soneDefaultViewState.getView().getName();
			viewValue = soneDefaultViewState.getView().getValue();
			pageSize = soneDefaultViewState.getView().getPageSize();
			totals = soneDefaultViewState.getView().getTotals();
			viewId = soneDefaultViewState.getView().getId();
			cmp1 = soneDefaultViewState.getView().getCmp();
			unit = soneDefaultViewState.getView().getUnitCode();
			currency = soneDefaultViewState.getView().getCurrencyCode();
			createdBy = soneDefaultViewState.getView().getCreatedBy();
			availableSystemWide = soneDefaultViewState.getView().getAvailableSystemwide();
			
			Integer filterId = null;
			String filterName = "";
			String standardFilterValue = "";
			String advancedFilterValue = "";
			String filterCreatedBy = "";
			Boolean filterAvailableSystemWide = false;

			// Dan - if the filter is not created by the current user or it is not available system wide than do not create the default elements

			// Dan - default view should have a default filter

			if (soneDefaultViewState.getView().getAdvancedFilter() != null) {
				filterName = soneDefaultViewState.getView().getAdvancedFilter().getName();
				standardFilterValue = soneDefaultViewState.getView().getAdvancedFilter().getStandardFilterValue();
				advancedFilterValue = soneDefaultViewState.getView().getAdvancedFilter().getAdvancedFilterValue();
				filterId = soneDefaultViewState.getView().getAdvancedFilter().getId();
				filterCreatedBy = soneDefaultViewState.getView().getAdvancedFilter().getCreatedBy();
				filterAvailableSystemWide = soneDefaultViewState.getView().getAdvancedFilter().getAvailableSystemwide();
			}

			if (unit == null) {
				unit = "";
			}
			if (currency == null) {
				currency = "";
			}

			if (cnt > 0) {
				sb.append(", ");
			}
			
			cnt++;
			sb.append("\t { \n");
			// keep only the component name - cut the dialog/frame extension from the name
			sb.append("\t\t cmp : '" + cmp1.substring(cmpName.length() + 1) + "', \n");
			sb.append("\t\t validFor : '" + createdBy + "', \n");
			sb.append("\t\t pageSize : '" + pageSize + "', \n");
			sb.append("\t\t totals : " + totals + ", \n");
			sb.append("\t\t unit : '" + unit + "', \n");
			sb.append("\t\t currency : '" + currency + "', \n");
			sb.append("\t\t view : { name: '" + viewName + "', value: '" + viewValue + "', id: " + viewId + ", createdBy: '" + createdBy
					+ "', availableSystemWide: " + availableSystemWide + "}, \n");
			sb.append("\t\t filter : { name: '" + filterName + "', standardFilterValue: '" + standardFilterValue + "',advancedFilterValue: '"
					+ advancedFilterValue + "', id: " + filterId + ", createdBy: '" + filterCreatedBy + "', availableSystemWide: "
					+ filterAvailableSystemWide + "} \n");
			sb.append("\t } \n");
		}

		sb.append("\t ]; \n");
		return sb.toString();
	}

	private List<SoneDefaultViewState> listDefaultViewStates(String cmpName) throws Exception {

		ISoneDefaultViewStateService srv = (ISoneDefaultViewStateService) this.findEntityService(SoneDefaultViewState.class);
		return srv.getDefaultViewStates(cmpName);
	}

	private List<SoneAdvancedFilter> listAdvancedFilters(String cmpName) throws Exception {

		ISoneAdvancedFilterService srv = (ISoneAdvancedFilterService) this.findEntityService(SoneAdvancedFilter.class);
		return srv.getDefaultFilters(cmpName);

	}
}
