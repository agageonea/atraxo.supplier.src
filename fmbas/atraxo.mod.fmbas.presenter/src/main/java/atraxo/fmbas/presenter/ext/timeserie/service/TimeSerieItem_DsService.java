/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.timeserie.service;

import java.text.DateFormat;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.timeserie.RawTimeSerieItem;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.presenter.impl.timeserie.model.TimeSerieItem_Ds;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

// Implemented in framework Yes/No, no need for this at the moment

public class TimeSerieItem_DsService extends AbstractEntityDsService<TimeSerieItem_Ds, TimeSerieItem_Ds, Object, RawTimeSerieItem>
		implements IDsService<TimeSerieItem_Ds, TimeSerieItem_Ds, Object> {

	@Override
	protected void preUpdate(List<TimeSerieItem_Ds> list, Object params) throws Exception {
		super.preUpdate(list, params);
		for (TimeSerieItem_Ds tsi : list) {
			tsi.setCalculated(false);
		}
	}

	@Override
	protected void preInsert(List<TimeSerieItem_Ds> list, Object params) throws Exception {
		super.preInsert(list, params);

		TimeSerie ts = null;
		if (!CollectionUtils.isEmpty(list)) {
			Integer timeSerieId = list.get(0).getTserie();
			ts = this.findEntityService(TimeSerie.class).findById(timeSerieId);
		}

		for (TimeSerieItem_Ds tsi : list) {
			if (ts != null && ts.getFirstUsage() != null && ts.getLastUsage() != null && this.checkItemInterval(ts, tsi)) {
				throw new BusinessException(BusinessErrorCode.TSI_NOT_VALID,
						String.format(BusinessErrorCode.TSI_NOT_VALID.getErrMsg(), DateFormat.getDateInstance().format(tsi.getItemdate())));

			}
		}
	}

	/**
	 * Check if the new item is between the from and last usage. If the element is between must be updated not inserted
	 * 
	 * @param ts
	 * @param tsi
	 * @return
	 */
	private boolean checkItemInterval(TimeSerie ts, TimeSerieItem_Ds tsi) {
		if (tsi.getItemdate().after(DateUtils.addDays(ts.getFirstUsage(), -1)) && DateUtils.addDays(ts.getLastUsage(), +1).after(tsi.getItemdate())) {
			return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}
}
