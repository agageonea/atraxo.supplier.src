/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.workflow.model;

/**
 * Generated code. Do not modify in this file.
 */
public class WorkflowInstance_DsParam {

	public static final String f_wfkTerminateOption = "wfkTerminateOption";
	public static final String f_terminateWfkInstancesResult = "terminateWfkInstancesResult";
	public static final String f_terminateWkfInstancesDescription = "terminateWkfInstancesDescription";

	private String wfkTerminateOption;

	private Boolean terminateWfkInstancesResult;

	private String terminateWkfInstancesDescription;

	public String getWfkTerminateOption() {
		return this.wfkTerminateOption;
	}

	public void setWfkTerminateOption(String wfkTerminateOption) {
		this.wfkTerminateOption = wfkTerminateOption;
	}

	public Boolean getTerminateWfkInstancesResult() {
		return this.terminateWfkInstancesResult;
	}

	public void setTerminateWfkInstancesResult(
			Boolean terminateWfkInstancesResult) {
		this.terminateWfkInstancesResult = terminateWfkInstancesResult;
	}

	public String getTerminateWkfInstancesDescription() {
		return this.terminateWkfInstancesDescription;
	}

	public void setTerminateWkfInstancesDescription(
			String terminateWkfInstancesDescription) {
		this.terminateWkfInstancesDescription = terminateWkfInstancesDescription;
	}
}
