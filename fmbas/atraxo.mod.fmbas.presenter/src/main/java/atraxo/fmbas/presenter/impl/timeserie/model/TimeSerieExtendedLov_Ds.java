/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.timeserie.model;

import atraxo.fmbas.domain.impl.fmbas_type.DataProvider;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesStatus;
import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import java.math.BigDecimal;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = TimeSerie.class, sort = {@SortField(field = TimeSerieExtendedLov_Ds.F_SERIENAME)})
public class TimeSerieExtendedLov_Ds extends AbstractLov_Ds<TimeSerie> {

	public static final String ALIAS = "fmbas_TimeSerieExtendedLov_Ds";

	public static final String F_TSID = "tsId";
	public static final String F_SERIENAME = "serieName";
	public static final String F_EXTERNALSERIENAME = "externalSerieName";
	public static final String F_DESCRIPTION = "description";
	public static final String F_ARITHMOPER = "arithmOper";
	public static final String F_CONVFCTR = "convFctr";
	public static final String F_DECIMALS = "decimals";
	public static final String F_DATAPROVIDER = "dataProvider";
	public static final String F_SERIETYPE = "serieType";
	public static final String F_STATUS = "status";
	public static final String F_ACTIVE = "active";
	public static final String F_CURRENCY1ID = "currency1iD";
	public static final String F_CURRENCY1CODE = "currency1Code";
	public static final String F_CURRENCY2ID = "currency2Id";
	public static final String F_CURRENCY2CODE = "currency2Code";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_UNITTYPEIND = "unitTypeInd";
	public static final String F_UNIT2ID = "unit2Id";
	public static final String F_UNIT2CODE = "unit2Code";
	public static final String F_UNIT2TYPEIND = "unit2TypeInd";

	@DsField(path = "id")
	private Integer tsId;

	@DsField
	private String serieName;

	@DsField
	private String externalSerieName;

	@DsField
	private String description;

	@DsField
	private BigDecimal arithmOper;

	@DsField
	private Operator convFctr;

	@DsField
	private Integer decimals;

	@DsField
	private DataProvider dataProvider;

	@DsField
	private String serieType;

	@DsField
	private TimeSeriesStatus status;

	@DsField
	private Boolean active;

	@DsField(join = "left", path = "currency1Id.id")
	private Integer currency1iD;

	@DsField(join = "left", path = "currency1Id.code")
	private String currency1Code;

	@DsField(join = "left", path = "currency2Id.id")
	private Integer currency2Id;

	@DsField(join = "left", path = "currency2Id.code")
	private String currency2Code;

	@DsField(join = "left", path = "unitId.id")
	private Integer unitId;

	@DsField(join = "left", path = "unitId.code")
	private String unitCode;

	@DsField(join = "left", path = "unitId.unittypeInd")
	private UnitType unitTypeInd;

	@DsField(join = "left", path = "unit2Id.id")
	private Integer unit2Id;

	@DsField(join = "left", path = "unit2Id.code")
	private String unit2Code;

	@DsField(join = "left", path = "unit2Id.unittypeInd")
	private UnitType unit2TypeInd;

	/**
	 * Default constructor
	 */
	public TimeSerieExtendedLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public TimeSerieExtendedLov_Ds(TimeSerie e) {
		super(e);
	}

	public Integer getTsId() {
		return this.tsId;
	}

	public void setTsId(Integer tsId) {
		this.tsId = tsId;
	}

	public String getSerieName() {
		return this.serieName;
	}

	public void setSerieName(String serieName) {
		this.serieName = serieName;
	}

	public String getExternalSerieName() {
		return this.externalSerieName;
	}

	public void setExternalSerieName(String externalSerieName) {
		this.externalSerieName = externalSerieName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getArithmOper() {
		return this.arithmOper;
	}

	public void setArithmOper(BigDecimal arithmOper) {
		this.arithmOper = arithmOper;
	}

	public Operator getConvFctr() {
		return this.convFctr;
	}

	public void setConvFctr(Operator convFctr) {
		this.convFctr = convFctr;
	}

	public Integer getDecimals() {
		return this.decimals;
	}

	public void setDecimals(Integer decimals) {
		this.decimals = decimals;
	}

	public DataProvider getDataProvider() {
		return this.dataProvider;
	}

	public void setDataProvider(DataProvider dataProvider) {
		this.dataProvider = dataProvider;
	}

	public String getSerieType() {
		return this.serieType;
	}

	public void setSerieType(String serieType) {
		this.serieType = serieType;
	}

	public TimeSeriesStatus getStatus() {
		return this.status;
	}

	public void setStatus(TimeSeriesStatus status) {
		this.status = status;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getCurrency1iD() {
		return this.currency1iD;
	}

	public void setCurrency1iD(Integer currency1iD) {
		this.currency1iD = currency1iD;
	}

	public String getCurrency1Code() {
		return this.currency1Code;
	}

	public void setCurrency1Code(String currency1Code) {
		this.currency1Code = currency1Code;
	}

	public Integer getCurrency2Id() {
		return this.currency2Id;
	}

	public void setCurrency2Id(Integer currency2Id) {
		this.currency2Id = currency2Id;
	}

	public String getCurrency2Code() {
		return this.currency2Code;
	}

	public void setCurrency2Code(String currency2Code) {
		this.currency2Code = currency2Code;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public UnitType getUnitTypeInd() {
		return this.unitTypeInd;
	}

	public void setUnitTypeInd(UnitType unitTypeInd) {
		this.unitTypeInd = unitTypeInd;
	}

	public Integer getUnit2Id() {
		return this.unit2Id;
	}

	public void setUnit2Id(Integer unit2Id) {
		this.unit2Id = unit2Id;
	}

	public String getUnit2Code() {
		return this.unit2Code;
	}

	public void setUnit2Code(String unit2Code) {
		this.unit2Code = unit2Code;
	}

	public UnitType getUnit2TypeInd() {
		return this.unit2TypeInd;
	}

	public void setUnit2TypeInd(UnitType unit2TypeInd) {
		this.unit2TypeInd = unit2TypeInd;
	}
}
