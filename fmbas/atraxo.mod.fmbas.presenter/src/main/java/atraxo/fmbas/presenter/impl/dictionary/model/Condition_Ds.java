/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.dictionary.model;

import atraxo.fmbas.domain.impl.dictionary.Condition;
import atraxo.fmbas.domain.impl.fmbas_type.DictionaryOperator;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Condition.class, sort = {@SortField(field = Condition_Ds.F_FIELDNAME, desc = true)})
@RefLookups({@RefLookup(refId = Condition_Ds.F_DICTIONARYID)})
public class Condition_Ds extends AbstractDs_Ds<Condition> {

	public static final String ALIAS = "fmbas_Condition_Ds";

	public static final String F_EXTERNALINTERFACEID = "externalInterfaceId";
	public static final String F_DICTIONARYID = "dictionaryId";
	public static final String F_DICTIONARYFIELDNAME = "dictionaryFieldName";
	public static final String F_FIELDNAME = "fieldName";
	public static final String F_OPERATOR = "operator";
	public static final String F_VALUE = "value";

	@DsField(join = "left", path = "dictionary.externalInterface.id")
	private Integer externalInterfaceId;

	@DsField(join = "left", path = "dictionary.id")
	private Integer dictionaryId;

	@DsField(join = "left", path = "dictionary.fieldName")
	private String dictionaryFieldName;

	@DsField
	private String fieldName;

	@DsField
	private DictionaryOperator operator;

	@DsField
	private String value;

	/**
	 * Default constructor
	 */
	public Condition_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Condition_Ds(Condition e) {
		super(e);
	}

	public Integer getExternalInterfaceId() {
		return this.externalInterfaceId;
	}

	public void setExternalInterfaceId(Integer externalInterfaceId) {
		this.externalInterfaceId = externalInterfaceId;
	}

	public Integer getDictionaryId() {
		return this.dictionaryId;
	}

	public void setDictionaryId(Integer dictionaryId) {
		this.dictionaryId = dictionaryId;
	}

	public String getDictionaryFieldName() {
		return this.dictionaryFieldName;
	}

	public void setDictionaryFieldName(String dictionaryFieldName) {
		this.dictionaryFieldName = dictionaryFieldName;
	}

	public String getFieldName() {
		return this.fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public DictionaryOperator getOperator() {
		return this.operator;
	}

	public void setOperator(DictionaryOperator operator) {
		this.operator = operator;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
