/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.sys.model;

import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = SystemParameter.class, sort = {@SortField(field = SystemParameter_Ds.F_CODE)})
public class SystemParameter_Ds extends AbstractDs_Ds<SystemParameter> {

	public static final String ALIAS = "fmbas_SystemParameter_Ds";

	public static final String F_CODE = "code";
	public static final String F_VALUE = "value";
	public static final String F_TYPE = "type";
	public static final String F_DESCRIPTION = "description";
	public static final String F_EDITABLE = "editable";
	public static final String F_REFVALUES = "refValues";

	@DsField
	private String code;

	@DsField
	private String value;

	@DsField
	private String type;

	@DsField
	private String description;

	@DsField
	private Boolean editable;

	@DsField
	private String refValues;

	/**
	 * Default constructor
	 */
	public SystemParameter_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public SystemParameter_Ds(SystemParameter e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getEditable() {
		return this.editable;
	}

	public void setEditable(Boolean editable) {
		this.editable = editable;
	}

	public String getRefValues() {
		return this.refValues;
	}

	public void setRefValues(String refValues) {
		this.refValues = refValues;
	}
}
