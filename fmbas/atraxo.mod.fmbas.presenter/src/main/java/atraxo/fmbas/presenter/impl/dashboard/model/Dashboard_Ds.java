/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.dashboard.model;

import atraxo.fmbas.domain.impl.dashboard.Dashboards;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Dashboards.class, sort = {@SortField(field = Dashboard_Ds.F_NAME)})
@RefLookups({@RefLookup(refId = Dashboard_Ds.F_LAYOUTID)})
public class Dashboard_Ds extends AbstractDs_Ds<Dashboards> {

	public static final String ALIAS = "fmbas_Dashboard_Ds";

	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_ISDEFAULT = "isDefault";
	public static final String F_LAYOUTID = "layoutId";
	public static final String F_LAYOUTNAME = "layoutName";

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private Boolean isDefault;

	@DsField(join = "left", path = "layout.id")
	private Integer layoutId;

	@DsField(join = "left", path = "layout.name")
	private String layoutName;

	/**
	 * Default constructor
	 */
	public Dashboard_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Dashboard_Ds(Dashboards e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsDefault() {
		return this.isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Integer getLayoutId() {
		return this.layoutId;
	}

	public void setLayoutId(Integer layoutId) {
		this.layoutId = layoutId;
	}

	public String getLayoutName() {
		return this.layoutName;
	}

	public void setLayoutName(String layoutName) {
		this.layoutName = layoutName;
	}
}
