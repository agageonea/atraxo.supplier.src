package atraxo.fmbas.presenter.ext.notification.delegate;

import atraxo.fmbas.business.api.notification.INotificationService;
import atraxo.fmbas.domain.impl.notification.Notification;
import atraxo.fmbas.presenter.impl.notification.model.Notification_Ds;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class Notification_Pd extends AbstractPresenterDelegate {

	public void clean(Notification_Ds ds) throws Exception {
		INotificationService srv = (INotificationService) this.findEntityService(Notification.class);
		srv.deleteForUser(ds.getUserCode());
	}

}
