/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.dashboard.model;

import atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.WidgetParameters;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamType;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = AssignedWidgetParameters.class)
@RefLookups({
		@RefLookup(refId = AssignedWidgetParameter_Ds.F_DASHBOARDWIDGETID),
		@RefLookup(refId = AssignedWidgetParameter_Ds.F_PARAMETERID, namedQuery = WidgetParameters.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = AssignedWidgetParameter_Ds.F_NAME)})})
public class AssignedWidgetParameter_Ds
		extends
			AbstractDs_Ds<AssignedWidgetParameters> {

	public static final String ALIAS = "fmbas_AssignedWidgetParameter_Ds";

	public static final String F_DASHBOARDWIDGETID = "dashboardWidgetId";
	public static final String F_LAYOUTREGIONID = "layoutRegionId";
	public static final String F_WIDGETINDEX = "widgetIndex";
	public static final String F_WIDGETID = "widgetId";
	public static final String F_WIDGETNAME = "widgetName";
	public static final String F_WIDGETCODE = "widgetCode";
	public static final String F_PARAMETERID = "parameterId";
	public static final String F_PARAMETERVALUE = "parameterValue";
	public static final String F_PARAMETERDEFAULTVALUE = "parameterDefaultValue";
	public static final String F_PARAMETERREFVALUES = "parameterRefValues";
	public static final String F_PARAMETERREFBUSINESSVALUES = "parameterRefBusinessValues";
	public static final String F_TYPE = "type";
	public static final String F_NAME = "name";
	public static final String F_VALUE = "value";
	public static final String F_DEFAULTVALUE = "defaultValue";
	public static final String F_READONLY = "readOnly";
	public static final String F_REFVALUES = "refValues";
	public static final String F_REFBUSINESSVALUES = "refBusinessValues";
	public static final String F_LAYOUTID = "layoutId";
	public static final String F_BLOBVALUE = "blobValue";

	@DsField(join = "left", path = "dashboardWidgets.id")
	private Integer dashboardWidgetId;

	@DsField(join = "left", path = "dashboardWidgets.layoutRegionId")
	private String layoutRegionId;

	@DsField(join = "left", path = "dashboardWidgets.widgetIndex")
	private String widgetIndex;

	@DsField(join = "left", path = "dashboardWidgets.widget.id")
	private Integer widgetId;

	@DsField(join = "left", path = "dashboardWidgets.widget.name")
	private String widgetName;

	@DsField(join = "left", path = "dashboardWidgets.widget.code")
	private String widgetCode;

	@DsField(join = "left", path = "widgetParameters.id")
	private Integer parameterId;

	@DsField(join = "left", path = "widgetParameters.value")
	private String parameterValue;

	@DsField(join = "left", path = "widgetParameters.defaultValue")
	private String parameterDefaultValue;

	@DsField(join = "left", path = "widgetParameters.refValues")
	private String parameterRefValues;

	@DsField(join = "left", path = "widgetParameters.refBusinessValues")
	private String parameterRefBusinessValues;

	@DsField(join = "left", path = "widgetParameters.type")
	private JobParamType type;

	@DsField(join = "left", path = "widgetParameters.name")
	private String name;

	@DsField
	private String value;

	@DsField
	private String defaultValue;

	@DsField
	private Boolean readOnly;

	@DsField
	private String refValues;

	@DsField
	private String refBusinessValues;

	@DsField(join = "left", path = "dashboardWidgets.layout.id")
	private Integer layoutId;

	@DsField
	private byte[] blobValue;

	/**
	 * Default constructor
	 */
	public AssignedWidgetParameter_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AssignedWidgetParameter_Ds(AssignedWidgetParameters e) {
		super(e);
	}

	public Integer getDashboardWidgetId() {
		return this.dashboardWidgetId;
	}

	public void setDashboardWidgetId(Integer dashboardWidgetId) {
		this.dashboardWidgetId = dashboardWidgetId;
	}

	public String getLayoutRegionId() {
		return this.layoutRegionId;
	}

	public void setLayoutRegionId(String layoutRegionId) {
		this.layoutRegionId = layoutRegionId;
	}

	public String getWidgetIndex() {
		return this.widgetIndex;
	}

	public void setWidgetIndex(String widgetIndex) {
		this.widgetIndex = widgetIndex;
	}

	public Integer getWidgetId() {
		return this.widgetId;
	}

	public void setWidgetId(Integer widgetId) {
		this.widgetId = widgetId;
	}

	public String getWidgetName() {
		return this.widgetName;
	}

	public void setWidgetName(String widgetName) {
		this.widgetName = widgetName;
	}

	public String getWidgetCode() {
		return this.widgetCode;
	}

	public void setWidgetCode(String widgetCode) {
		this.widgetCode = widgetCode;
	}

	public Integer getParameterId() {
		return this.parameterId;
	}

	public void setParameterId(Integer parameterId) {
		this.parameterId = parameterId;
	}

	public String getParameterValue() {
		return this.parameterValue;
	}

	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}

	public String getParameterDefaultValue() {
		return this.parameterDefaultValue;
	}

	public void setParameterDefaultValue(String parameterDefaultValue) {
		this.parameterDefaultValue = parameterDefaultValue;
	}

	public String getParameterRefValues() {
		return this.parameterRefValues;
	}

	public void setParameterRefValues(String parameterRefValues) {
		this.parameterRefValues = parameterRefValues;
	}

	public String getParameterRefBusinessValues() {
		return this.parameterRefBusinessValues;
	}

	public void setParameterRefBusinessValues(String parameterRefBusinessValues) {
		this.parameterRefBusinessValues = parameterRefBusinessValues;
	}

	public JobParamType getType() {
		return this.type;
	}

	public void setType(JobParamType type) {
		this.type = type;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Boolean getReadOnly() {
		return this.readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	public String getRefValues() {
		return this.refValues;
	}

	public void setRefValues(String refValues) {
		this.refValues = refValues;
	}

	public String getRefBusinessValues() {
		return this.refBusinessValues;
	}

	public void setRefBusinessValues(String refBusinessValues) {
		this.refBusinessValues = refBusinessValues;
	}

	public Integer getLayoutId() {
		return this.layoutId;
	}

	public void setLayoutId(Integer layoutId) {
		this.layoutId = layoutId;
	}

	public byte[] getBlobValue() {
		return this.blobValue;
	}

	public void setBlobValue(byte[] blobValue) {
		this.blobValue = blobValue;
	}
}
