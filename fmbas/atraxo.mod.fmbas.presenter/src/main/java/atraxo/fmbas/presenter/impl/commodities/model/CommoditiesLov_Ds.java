/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.commodities.model;

import atraxo.fmbas.domain.impl.commodities.Commoditie;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;
import seava.j4e.api.model.IModelWithId;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Commoditie.class, sort = {@SortField(field = CommoditiesLov_Ds.F_CODE)})
public class CommoditiesLov_Ds extends AbstractLov_Ds<Commoditie>
		implements
			IModelWithId<Integer> {

	public static final String ALIAS = "fmbas_CommoditiesLov_Ds";

	public static final String F_ID = "id";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";

	@DsField
	private Integer id;

	@DsField
	private String code;

	@DsField
	private String name;

	/**
	 * Default constructor
	 */
	public CommoditiesLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public CommoditiesLov_Ds(Commoditie e) {
		super(e);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
