/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.job.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import atraxo.fmbas.business.api.job.IJobChainService;
import atraxo.fmbas.business.api.job.ITriggerService;
import atraxo.fmbas.domain.impl.job.JobChain;
import atraxo.fmbas.domain.impl.job.Trigger;
import atraxo.fmbas.presenter.impl.job.model.JobChain_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class JobChain_DsService extends AbstractEntityDsService<JobChain_Ds, JobChain_Ds, Object, JobChain>
		implements IDsService<JobChain_Ds, JobChain_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<JobChain_Ds, JobChain_Ds, Object> builder, List<JobChain_Ds> result) throws Exception {
		super.postFind(builder, result);
		ITriggerService triggerService = (ITriggerService) this.findEntityService(Trigger.class);
		for (JobChain_Ds ds : result) {
			ds.setParamIdentifier("JOBCHAIN");
			List<Trigger> triggers = triggerService.findByJobChainId(ds.getId());
			if (triggers != null) {
				StringBuilder sb = new StringBuilder();
				for (Trigger t : triggers) {
					sb.append(sb.toString().isEmpty() ? t.getType().getName()
							: new StringBuilder().append(" , ").append(t.getType().getName()).toString());
				}
				ds.setTrigers(sb.toString());
			}
		}
	}

	@Override
	@Transactional
	public void deleteByIds(List<Object> ids) throws Exception {
		IJobChainService srv = (IJobChainService) this.getEntityService();
		List<JobChain> list = srv.findByIds(ids);
		srv.delete(list);
	}
}
