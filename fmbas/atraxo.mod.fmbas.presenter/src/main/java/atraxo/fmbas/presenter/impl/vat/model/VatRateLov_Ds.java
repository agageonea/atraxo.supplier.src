/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.vat.model;

import atraxo.fmbas.domain.impl.vat.VatRate;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = VatRate.class, sort = {@SortField(field = VatRateLov_Ds.F_ID)})
public class VatRateLov_Ds extends AbstractLov_Ds<VatRate> {

	public static final String ALIAS = "fmbas_VatRateLov_Ds";

	public static final String F_VATID = "vatId";
	public static final String F_VATCODE = "vatCode";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_NOTE = "note";

	@DsField(join = "left", path = "vat.id")
	private Integer vatId;

	@DsField(join = "left", path = "vat.code")
	private String vatCode;

	@DsField
	private Date validFrom;

	@DsField
	private Date validTo;

	@DsField
	private String note;

	/**
	 * Default constructor
	 */
	public VatRateLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public VatRateLov_Ds(VatRate e) {
		super(e);
	}

	public Integer getVatId() {
		return this.vatId;
	}

	public void setVatId(Integer vatId) {
		this.vatId = vatId;
	}

	public String getVatCode() {
		return this.vatCode;
	}

	public void setVatCode(String vatCode) {
		this.vatCode = vatCode;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}
