/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.BusinessType;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerType;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Customer.class, sort = {@SortField(field = CustomerMpLov_Ds.F_CODE)})
public class CustomerMpLov_Ds extends AbstractLov_Ds<Customer> {

	public static final String ALIAS = "fmbas_CustomerMpLov_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_TYPE = "type";
	public static final String F_REFID = "refid";
	public static final String F_STATUS = "status";
	public static final String F_ISCUSTOMER = "isCustomer";
	public static final String F_BUSINESS = "business";
	public static final String F_BUYERID = "buyerId";
	public static final String F_BUYERNAME = "buyerName";
	public static final String F_BUYERCODE = "buyerCode";
	public static final String F_PRIMARYCONTACTID = "primaryContactId";
	public static final String F_PRIMARYCONTACTNAME = "primaryContactName";

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private CustomerType type;

	@DsField
	private String refid;

	@DsField
	private CustomerStatus status;

	@DsField
	private Boolean isCustomer;

	@DsField(fetch = false)
	private BusinessType business;

	@DsField(join = "left", path = "responsibleBuyer.id")
	private String buyerId;

	@DsField(join = "left", path = "responsibleBuyer.name")
	private String buyerName;

	@DsField(join = "left", path = "responsibleBuyer.code")
	private String buyerCode;

	@DsField(fetch = false)
	private Integer primaryContactId;

	@DsField(fetch = false)
	private String primaryContactName;

	/**
	 * Default constructor
	 */
	public CustomerMpLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public CustomerMpLov_Ds(Customer e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CustomerType getType() {
		return this.type;
	}

	public void setType(CustomerType type) {
		this.type = type;
	}

	public String getRefid() {
		return this.refid;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}

	public CustomerStatus getStatus() {
		return this.status;
	}

	public void setStatus(CustomerStatus status) {
		this.status = status;
	}

	public Boolean getIsCustomer() {
		return this.isCustomer;
	}

	public void setIsCustomer(Boolean isCustomer) {
		this.isCustomer = isCustomer;
	}

	public BusinessType getBusiness() {
		return this.business;
	}

	public void setBusiness(BusinessType business) {
		this.business = business;
	}

	public String getBuyerId() {
		return this.buyerId;
	}

	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}

	public String getBuyerName() {
		return this.buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getBuyerCode() {
		return this.buyerCode;
	}

	public void setBuyerCode(String buyerCode) {
		this.buyerCode = buyerCode;
	}

	public Integer getPrimaryContactId() {
		return this.primaryContactId;
	}

	public void setPrimaryContactId(Integer primaryContactId) {
		this.primaryContactId = primaryContactId;
	}

	public String getPrimaryContactName() {
		return this.primaryContactName;
	}

	public void setPrimaryContactName(String primaryContactName) {
		this.primaryContactName = primaryContactName;
	}
}
