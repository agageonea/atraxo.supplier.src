/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.NatureOfBusiness;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;
import seava.j4e.api.model.IModelWithId;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Customer.class, sort = {@SortField(field = AirlinesLov_Ds.F_CODE)})
public class AirlinesLov_Ds extends AbstractLov_Ds<Customer>
		implements
			IModelWithId<Integer> {

	public static final String ALIAS = "fmbas_AirlinesLov_Ds";

	public static final String F_ID = "id";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_NATUREOFBUSINESS = "natureOfBusiness";

	@DsField
	private Integer id;

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private NatureOfBusiness natureOfBusiness;

	/**
	 * Default constructor
	 */
	public AirlinesLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AirlinesLov_Ds(Customer e) {
		super(e);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public NatureOfBusiness getNatureOfBusiness() {
		return this.natureOfBusiness;
	}

	public void setNatureOfBusiness(NatureOfBusiness natureOfBusiness) {
		this.natureOfBusiness = natureOfBusiness;
	}
}
