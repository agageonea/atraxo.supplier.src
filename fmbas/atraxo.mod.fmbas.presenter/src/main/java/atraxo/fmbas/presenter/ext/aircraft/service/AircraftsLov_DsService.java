package atraxo.fmbas.presenter.ext.aircraft.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.presenter.impl.aircraft.model.AircraftsLov_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class AircraftsLov_DsService extends AbstractEntityDsService<AircraftsLov_Ds, AircraftsLov_Ds, Object, Aircraft> implements
		IDsService<AircraftsLov_Ds, AircraftsLov_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<AircraftsLov_Ds, AircraftsLov_Ds, Object> builder, List<AircraftsLov_Ds> result) throws Exception {
		super.postFind(builder, result);
		List<AircraftsLov_Ds> removeList = new ArrayList<AircraftsLov_Ds>();
		for (AircraftsLov_Ds aircraft : result) {
			if (aircraft.getValidFrom().after(new Date()) || aircraft.getValidTo().before(new Date())) {
				removeList.add(aircraft);
			}
		}
		result.removeAll(removeList);
	}
}
