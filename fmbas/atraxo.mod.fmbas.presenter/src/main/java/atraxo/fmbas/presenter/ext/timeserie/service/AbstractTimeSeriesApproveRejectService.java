/**
 *
 */
package atraxo.fmbas.presenter.ext.timeserie.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.ext.bpm.WorkflowStartResult;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.business.ext.bpm.WorkflowMsgConstants;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.workflow.ClaimedWorkflowException;
import atraxo.fmbas.business.ext.util.StringUtil;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesApprovalStatus;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesStatus;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieAverage;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterBaseService;

/**
 * @author zspeter
 */
public abstract class AbstractTimeSeriesApproveRejectService extends AbstractPresenterBaseService {

	private static final String UL_START = "<ul>";
	private static final String UL_END = "</ul>";
	private static final String LI_START = "<li>";
	private static final String LI_END = "</li>";
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractTimeSeriesApproveRejectService.class);

	@Autowired
	private ITimeSerieService timeSerieService;
	@Autowired
	private IWorkflowBpmManager workflowBpmManager;
	@Autowired
	private IWorkflowInstanceEntityService workflowInstanceEntityService;
	@Autowired
	private IUserSuppService userService;

	protected abstract WorkflowNames getWorkflowNameFromTimeSerie();

	protected abstract String getTimeSerieType();

	protected abstract void checkWorkflowIsEnabled() throws BusinessException;

	protected void submitForApprovalTimeSerie(int timeSerieId, String approvalNote) throws BusinessException {

		TimeSerie timeSerie = this.getTimeSerie(timeSerieId);

		this.checkWorkflowIsEnabled();

		this.checkTimeSerieIsProperlySetUp(timeSerie);

		if (this.canSubmitForApproval(timeSerie)) {

			WorkflowStartResult startResult = this.startWorkflow(approvalNote, timeSerie);
			if (!startResult.isStarted()) {
				this.updateTimeSeriesOnFailToSubmit(timeSerie, startResult);
			}

		} else {
			throw new BusinessException(BusinessErrorCode.TIME_SERIE_RPC_ACTION_ERROR,
					String.format(BusinessErrorCode.TIME_SERIE_RPC_ACTION_ERROR.getErrMsg(), StringUtil.unorderedList(timeSerie.getSerieName())));
		}
	}

	/**
	 * Completes the current task associated with the time serie
	 *
	 * @param timeSerieId
	 * @param approve
	 * @param note
	 * @throws Exception
	 */
	protected synchronized void completeCurrentTask(Integer timeSerieId, boolean approve, String note) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START completeCurrentTask()");
		}

		TimeSerie timeSerie = this.retrieveTimeSerieFromId(timeSerieId);

		WorkflowInstanceEntity workflowInstanceEntity = this.getWorkflowInstanceEntityService(timeSerie);

		try {
			this.workflowBpmManager.claimCompleteTask(workflowInstanceEntity.getWorkflowInstance().getId(), approve, note);
		} catch (ClaimedWorkflowException e1) {
			throw e1;
		} catch (Exception e) {
			LOGGER.error("Could not complete worflow task for timeserie " + timeSerie.getSerieName(), e);

			this.workflowBpmManager.terminateWorkflow(workflowInstanceEntity.getWorkflowInstance().getId(),
					BusinessErrorCode.TIME_SERIE_WORKFLOW_IDENTIFICATION_ERROR.getErrMsg() + ":" + e.getMessage());

			throw new BusinessException(BusinessErrorCode.TIME_SERIE_WORKFLOW_IDENTIFICATION_ERROR,
					BusinessErrorCode.TIME_SERIE_WORKFLOW_IDENTIFICATION_ERROR.getErrMsg() + ":" + e.getMessage());
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END completeCurrentTask()");
		}
	}

	protected String buildApproveFeedbackMessage(List<String> failedToApproveTimeSeries, int totalNumberOfTimeSeries) {

		String summaryFeedbackMessage = String.format(BusinessErrorCode.SUCCESFULLY_APPROVED_X_FROM_Y_TIMESERIES.getErrMsg(),
				totalNumberOfTimeSeries - failedToApproveTimeSeries.size(), totalNumberOfTimeSeries);

		return this.buildFeedbackMessage(failedToApproveTimeSeries, summaryFeedbackMessage);
	}

	/**
	 * @param failedToRejectTimeSeries
	 * @param totalNumberOfTimeSeries
	 * @return Method that returns a html formatted message regarding the bulk approval/rejection action
	 */
	protected String buildRejectFeedbackMessage(List<String> failedToRejectTimeSeries, int totalNumberOfTimeSeries) {
		String summaryFeedbackMessage = String.format(BusinessErrorCode.SUCCESFULLY_REJECTED_X_FROM_Y_TIMESERIES.getErrMsg(),
				totalNumberOfTimeSeries - failedToRejectTimeSeries.size(), totalNumberOfTimeSeries);

		return this.buildFeedbackMessage(failedToRejectTimeSeries, summaryFeedbackMessage);
	}

	private WorkflowInstanceEntity getWorkflowInstanceEntityService(TimeSerie timeSerie) throws BusinessException {

		WorkflowNames workflowName = this.getWorkflowNameFromTimeSerie();

		return this.workflowInstanceEntityService.findByBusinessKey(workflowName.getWorkflowName(), timeSerie.getId(),
				timeSerie.getClass().getSimpleName());
	}

	private TimeSerie retrieveTimeSerieFromId(Integer timeSerieId) throws BusinessException {
		TimeSerie timeSerie = this.timeSerieService.findById(timeSerieId);
		this.checkConcurency(timeSerie);
		return timeSerie;
	}

	private String buildFeedbackMessage(List<String> failedToProcessTimeSeries, String messageToShow) {
		StringBuilder feedbackMessage = new StringBuilder(messageToShow);
		if (!failedToProcessTimeSeries.isEmpty()) {
			feedbackMessage.append(UL_START);
			failedToProcessTimeSeries.forEach(failedMessage -> feedbackMessage.append(LI_START + failedMessage + LI_END));
			feedbackMessage.append(UL_END);
		}
		return feedbackMessage.toString();
	}

	/**
	 * @param params
	 * @param timeSerie
	 * @return
	 */
	private Map<String, Object> getWorkflowVariablesMap(String approvalNote, TimeSerie timeSerie) {
		Map<String, Object> vars = new HashMap<>();
		vars.put(WorkflowVariablesConstants.VAR_WKF_ENTITY_DESCRIPTION,
				String.format(BusinessErrorCode.TIME_SERIE_X_APPROVAL_REQUEST_FOR_Y.getErrMsg(), this.getTimeSerieType(), timeSerie.getSerieName()));
		vars.put(WorkflowVariablesConstants.TIME_SERIE_ID, Integer.toString(timeSerie.getId()));
		vars.put(WorkflowVariablesConstants.TIME_SERIE_TYPE, timeSerie.getSerieType());
		vars.put(WorkflowVariablesConstants.APPROVE_NOTE, approvalNote);
		vars.put(WorkflowVariablesConstants.FULLNAME_REQUESTER_VAR, this.getFullNameOfRequester());
		return vars;
	}

	/**
	 * Re-check the conditions that allows a user to submit for approval a time serie
	 *
	 * @param outInvoice
	 * @return
	 */
	private boolean canSubmitForApproval(TimeSerie timeSerie) {
		boolean canSubmit = false;
		if (timeSerie.getStatus() == TimeSeriesStatus._WORK_ && timeSerie.getApprovalStatus() == TimeSeriesApprovalStatus._NEW_
				|| timeSerie.getApprovalStatus() == TimeSeriesApprovalStatus._REJECTED_) {
			canSubmit = true;
		}
		return canSubmit;
	}

	private WorkflowStartResult startWorkflow(String approvalNote, TimeSerie timeSerie) throws BusinessException {

		// submit for approval for the entity(change status, fill in History)
		this.timeSerieService.submitForApproval(timeSerie, TimeSeriesApprovalStatus._AWAITING_APPROVAL_,
				WorkflowMsgConstants.MSG_STARTED_FOR_APPROVAL_OK, approvalNote);

		// start workflow after adding workflow parameters
		Map<String, Object> vars = this.getWorkflowVariablesMap(approvalNote, timeSerie);
		WorkflowNames workflowName = this.getWorkflowNameFromTimeSerie();
		return this.workflowBpmManager.startWorkflow(workflowName.getWorkflowName(), vars, "", timeSerie);

	}

	/**
	 * @param timeSerieId
	 * @return
	 * @throws BusinessException
	 */
	private TimeSerie getTimeSerie(int timeSerieId) throws BusinessException {
		TimeSerie timeSerie = null;
		try {
			timeSerie = this.timeSerieService.findByBusiness(timeSerieId);
		} catch (Exception e) {
			LOGGER.error("Error", e);
			throw new BusinessException(BusinessErrorCode.TIME_SERIE_WORKFLOW_ENTITY_NOT_EXISTING,
					String.format(BusinessErrorCode.TIME_SERIE_WORKFLOW_ENTITY_NOT_EXISTING.getErrMsg(), timeSerieId, e));
		}
		return timeSerie;
	}

	private void checkTimeSerieIsProperlySetUp(TimeSerie timeSerie) throws BusinessException {
		// check if there are raw items in the time series
		if (timeSerie.getRawTimeserieItems().isEmpty()) {
			throw new BusinessException(BusinessErrorCode.TIME_SERIE_NO_RAW_ITEMS_EXIST, BusinessErrorCode.TIME_SERIE_NO_RAW_ITEMS_EXIST.getErrMsg());
		}
		// check is
		if (!this.hasAverageMethodDefined(timeSerie)) {
			throw new BusinessException(BusinessErrorCode.TIME_SERIE_NO_AVERAGE_METHOD_ACTIVE,
					BusinessErrorCode.TIME_SERIE_NO_AVERAGE_METHOD_ACTIVE.getErrMsg());
		}
	}

	/**
	 * @param timeSerie
	 * @return
	 */
	private Boolean hasAverageMethodDefined(TimeSerie timeSerie) {
		Boolean hasActiveAverage = false;
		for (TimeSerieAverage tsAverage : timeSerie.getTimeserieAvg()) {
			if (tsAverage.getActive()) {
				hasActiveAverage = true;
				break;
			}
		}
		return hasActiveAverage;
	}

	private String getFullNameOfRequester() {
		UserSupp user = this.userService.findByLogin(Session.user.get().getLoginName());
		return user.getFirstName() + " " + user.getLastName();
	}

	/**
	 * Check for concurency (same business logic as the UI)
	 *
	 * @param timeSerie
	 * @throws Exception
	 */
	private void checkConcurency(TimeSerie timeSerie) throws BusinessException {
		this.checkWorkflowIsEnabled();
		this.checkTimeSerieIsInAwaitingApproval(timeSerie);
	}

	private void updateTimeSeriesOnFailToSubmit(TimeSerie timeSerie, WorkflowStartResult startResult) throws BusinessException {
		// submit for approval for the entity(change status, fill in History)
		this.timeSerieService.submitForApproval(this.timeSerieService.findById(timeSerie.getId()), TimeSeriesApprovalStatus._REJECTED_,
				WorkflowMsgConstants.MSG_STARTED_FOR_APPROVAL_NOT_OK, startResult.getReason());
		throw new BusinessException(BusinessErrorCode.TIME_SERIE_WORKFLOW_NOT_START_REASON,
				BusinessErrorCode.TIME_SERIE_WORKFLOW_NOT_START_REASON.getErrMsg() + startResult.getReason());
	}

	private void checkTimeSerieIsInAwaitingApproval(TimeSerie timeSerie) throws BusinessException {
		if (TimeSeriesApprovalStatus._AWAITING_APPROVAL_ != timeSerie.getApprovalStatus()) {
			throw new BusinessException(BusinessErrorCode.TIME_SERIE_RPC_ACTION_ERROR_SHORT,
					BusinessErrorCode.TIME_SERIE_RPC_ACTION_ERROR_SHORT.getErrMsg());
		}
	}
}
