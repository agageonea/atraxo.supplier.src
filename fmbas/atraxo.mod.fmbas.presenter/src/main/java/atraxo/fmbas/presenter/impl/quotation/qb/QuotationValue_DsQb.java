/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.quotation.qb;

import atraxo.fmbas.presenter.impl.quotation.model.QuotationValue_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class QuotationValue_DsQb
		extends
			QueryBuilderWithJpql<QuotationValue_Ds, QuotationValue_Ds, Object> {
}
