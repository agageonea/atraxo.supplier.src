/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.externalInterfaces.model;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceParameter;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamType;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ExternalInterfaceParameter.class)
@RefLookups({@RefLookup(refId = ExternalInterfaceParameters_Ds.F_EXTERNALINTERFACEID, namedQuery = ExternalInterface.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = ExternalInterfaceParameters_Ds.F_EXTERNALINTERFACENAME)})})
public class ExternalInterfaceParameters_Ds
		extends
			AbstractDs_Ds<ExternalInterfaceParameter> {

	public static final String ALIAS = "fmbas_ExternalInterfaceParameters_Ds";

	public static final String F_EXTERNALINTERFACEID = "externalInterfaceId";
	public static final String F_EXTERNALINTERFACENAME = "externalInterfaceName";
	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_CURRENTVALUE = "currentValue";
	public static final String F_DEFAULTVALUE = "defaultValue";
	public static final String F_TYPE = "type";
	public static final String F_REFVALUES = "refValues";
	public static final String F_REFBUSINESSVALUES = "refBusinessValues";
	public static final String F_READONLY = "readOnly";

	@DsField(join = "left", path = "externalInterface.id")
	private Integer externalInterfaceId;

	@DsField(join = "left", path = "externalInterface.name")
	private String externalInterfaceName;

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private String currentValue;

	@DsField
	private String defaultValue;

	@DsField
	private JobParamType type;

	@DsField
	private String refValues;

	@DsField
	private String refBusinessValues;

	@DsField
	private Boolean readOnly;

	/**
	 * Default constructor
	 */
	public ExternalInterfaceParameters_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ExternalInterfaceParameters_Ds(ExternalInterfaceParameter e) {
		super(e);
	}

	public Integer getExternalInterfaceId() {
		return this.externalInterfaceId;
	}

	public void setExternalInterfaceId(Integer externalInterfaceId) {
		this.externalInterfaceId = externalInterfaceId;
	}

	public String getExternalInterfaceName() {
		return this.externalInterfaceName;
	}

	public void setExternalInterfaceName(String externalInterfaceName) {
		this.externalInterfaceName = externalInterfaceName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCurrentValue() {
		return this.currentValue;
	}

	public void setCurrentValue(String currentValue) {
		this.currentValue = currentValue;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public JobParamType getType() {
		return this.type;
	}

	public void setType(JobParamType type) {
		this.type = type;
	}

	public String getRefValues() {
		return this.refValues;
	}

	public void setRefValues(String refValues) {
		this.refValues = refValues;
	}

	public String getRefBusinessValues() {
		return this.refBusinessValues;
	}

	public void setRefBusinessValues(String refBusinessValues) {
		this.refBusinessValues = refBusinessValues;
	}

	public Boolean getReadOnly() {
		return this.readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}
}
