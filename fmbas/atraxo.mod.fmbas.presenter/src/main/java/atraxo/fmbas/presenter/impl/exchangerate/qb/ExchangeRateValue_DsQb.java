/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.exchangerate.qb;

import atraxo.fmbas.presenter.impl.exchangerate.model.ExchangeRateValue_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class ExchangeRateValue_DsQb
		extends
			QueryBuilderWithJpql<ExchangeRateValue_Ds, ExchangeRateValue_Ds, Object> {
}
