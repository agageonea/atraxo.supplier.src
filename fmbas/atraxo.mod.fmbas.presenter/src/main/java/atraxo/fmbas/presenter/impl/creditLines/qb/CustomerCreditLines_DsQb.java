/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.creditLines.qb;

import atraxo.fmbas.presenter.impl.creditLines.model.CustomerCreditLines_Ds;
import atraxo.fmbas.presenter.impl.creditLines.model.CustomerCreditLines_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class CustomerCreditLines_DsQb
		extends
			QueryBuilderWithJpql<CustomerCreditLines_Ds, CustomerCreditLines_Ds, CustomerCreditLines_DsParam> {
}
