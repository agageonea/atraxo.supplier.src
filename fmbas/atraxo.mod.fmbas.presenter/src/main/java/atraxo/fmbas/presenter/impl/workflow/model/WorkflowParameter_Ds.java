/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.workflow.model;

import atraxo.fmbas.domain.impl.fmbas_type.WorkflowParamType;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = WorkflowParameter.class, sort = {@SortField(field = WorkflowParameter_Ds.F_NAME)})
@RefLookups({@RefLookup(refId = WorkflowParameter_Ds.F_WORKFLOWID, namedQuery = Workflow.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = WorkflowParameter_Ds.F_WORKFLOWCODE)})})
public class WorkflowParameter_Ds extends AbstractDs_Ds<WorkflowParameter> {

	public static final String ALIAS = "fmbas_WorkflowParameter_Ds";

	public static final String F_WORKFLOWID = "workflowId";
	public static final String F_WORKFLOWCODE = "workflowCode";
	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_PARAMVALUE = "paramValue";
	public static final String F_DEFAULTVALUE = "defaultValue";
	public static final String F_TYPE = "type";
	public static final String F_MANDATORY = "mandatory";
	public static final String F_REFVALUES = "refValues";

	@DsField(join = "left", path = "workflow.id")
	private Integer workflowId;

	@DsField(join = "left", path = "workflow.code")
	private String workflowCode;

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private String paramValue;

	@DsField
	private String defaultValue;

	@DsField
	private WorkflowParamType type;

	@DsField
	private Boolean mandatory;

	@DsField
	private String refValues;

	/**
	 * Default constructor
	 */
	public WorkflowParameter_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public WorkflowParameter_Ds(WorkflowParameter e) {
		super(e);
	}

	public Integer getWorkflowId() {
		return this.workflowId;
	}

	public void setWorkflowId(Integer workflowId) {
		this.workflowId = workflowId;
	}

	public String getWorkflowCode() {
		return this.workflowCode;
	}

	public void setWorkflowCode(String workflowCode) {
		this.workflowCode = workflowCode;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getParamValue() {
		return this.paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public WorkflowParamType getType() {
		return this.type;
	}

	public void setType(WorkflowParamType type) {
		this.type = type;
	}

	public Boolean getMandatory() {
		return this.mandatory;
	}

	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}

	public String getRefValues() {
		return this.refValues;
	}

	public void setRefValues(String refValues) {
		this.refValues = refValues;
	}
}
