/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.quotation.service;

import java.util.ArrayList;
import java.util.List;

import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.presenter.impl.quotation.model.QuotationNameLov_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class QuotationNameLov_DsService extends AbstractEntityDsService<QuotationNameLov_Ds, QuotationNameLov_Ds, Object, Quotation>
		implements IDsService<QuotationNameLov_Ds, QuotationNameLov_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<QuotationNameLov_Ds, QuotationNameLov_Ds, Object> builder, List<QuotationNameLov_Ds> result)
			throws Exception {

		super.postFind(builder, result);

		List<QuotationNameLov_Ds> list = new ArrayList<>();

		for (QuotationNameLov_Ds ds : result) {
			this.addToReturnList(list, ds);
		}
		result.retainAll(list);
	}

	private void addToReturnList(List<QuotationNameLov_Ds> list, QuotationNameLov_Ds ds) {
		for (QuotationNameLov_Ds e : list) {
			if (e.getName().equalsIgnoreCase(ds.getName())) {
				return;
			}
		}
		list.add(ds);
	}

}
