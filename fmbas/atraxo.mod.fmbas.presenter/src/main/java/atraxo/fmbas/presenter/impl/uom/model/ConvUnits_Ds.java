/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.uom.model;

import atraxo.fmbas.domain.impl.uom.ConvUnit;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ConvUnit.class)
@RefLookups({
		@RefLookup(refId = ConvUnits_Ds.F_UNITFROMID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = ConvUnits_Ds.F_UNITFROMCODE)}),
		@RefLookup(refId = ConvUnits_Ds.F_UNITTOID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = ConvUnits_Ds.F_UNITTOCODE)})})
public class ConvUnits_Ds extends AbstractDs_Ds<ConvUnit> {

	public static final String ALIAS = "fmbas_ConvUnits_Ds";

	public static final String F_CONVTYPE = "convType";
	public static final String F_UNITFROMID = "unitFromId";
	public static final String F_UNITFROMCODE = "unitFromCode";
	public static final String F_UNITTOID = "unitToId";
	public static final String F_UNITTOCODE = "unitToCode";
	public static final String F_FACTOR = "factor";
	public static final String F_ACTIVE = "active";

	@DsField
	private Integer convType;

	@DsField(join = "left", path = "unitFromId.id")
	private Integer unitFromId;

	@DsField(join = "left", path = "unitFromId.code")
	private String unitFromCode;

	@DsField(join = "left", path = "unitToId.id")
	private Integer unitToId;

	@DsField(join = "left", path = "unitToId.code")
	private String unitToCode;

	@DsField
	private BigDecimal factor;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public ConvUnits_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ConvUnits_Ds(ConvUnit e) {
		super(e);
	}

	public Integer getConvType() {
		return this.convType;
	}

	public void setConvType(Integer convType) {
		this.convType = convType;
	}

	public Integer getUnitFromId() {
		return this.unitFromId;
	}

	public void setUnitFromId(Integer unitFromId) {
		this.unitFromId = unitFromId;
	}

	public String getUnitFromCode() {
		return this.unitFromCode;
	}

	public void setUnitFromCode(String unitFromCode) {
		this.unitFromCode = unitFromCode;
	}

	public Integer getUnitToId() {
		return this.unitToId;
	}

	public void setUnitToId(Integer unitToId) {
		this.unitToId = unitToId;
	}

	public String getUnitToCode() {
		return this.unitToCode;
	}

	public void setUnitToCode(String unitToCode) {
		this.unitToCode = unitToCode;
	}

	public BigDecimal getFactor() {
		return this.factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
