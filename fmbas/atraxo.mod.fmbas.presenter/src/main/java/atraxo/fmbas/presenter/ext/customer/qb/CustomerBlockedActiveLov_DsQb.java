/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.fmbas.presenter.ext.customer.qb;

import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.presenter.impl.customer.model.CustomerBlockedActiveLov_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

/**
 * Custom Query Builder CustomerBlockedActiveLov_DsQb
 */
public class CustomerBlockedActiveLov_DsQb extends QueryBuilderWithJpql<CustomerBlockedActiveLov_Ds, CustomerBlockedActiveLov_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();
		this.addFilterCondition("e.status in (:status1, :status2)");
		this.addCustomFilterItem("status1", CustomerStatus._ACTIVE_);
		this.addCustomFilterItem("status2", CustomerStatus._BLOCKED_);
	}

}
