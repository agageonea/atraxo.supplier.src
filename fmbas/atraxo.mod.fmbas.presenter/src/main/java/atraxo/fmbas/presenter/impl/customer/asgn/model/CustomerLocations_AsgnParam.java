/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.asgn.model;

public class CustomerLocations_AsgnParam {

	public static final String f_locationId = "locationId";

	private Integer locationId;

	public Integer getLocationId() {
		return this.locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}
}
