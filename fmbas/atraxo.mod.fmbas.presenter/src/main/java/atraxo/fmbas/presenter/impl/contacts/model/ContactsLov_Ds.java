/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.contacts.model;

import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Contacts.class, sort = {@SortField(field = ContactsLov_Ds.F_FIRSTNAME)})
public class ContactsLov_Ds extends AbstractLov_Ds<Contacts> {

	public static final String ALIAS = "fmbas_ContactsLov_Ds";

	public static final String F_FIRSTNAME = "firstName";
	public static final String F_LASTNAME = "lastName";
	public static final String F_FULLNAMEFIELD = "fullNameField";
	public static final String F_EMAIL = "email";
	public static final String F_BUSINESSPHONE = "businessPhone";
	public static final String F_OBJECTID = "objectId";
	public static final String F_OBJECTTYPE = "objectType";

	@DsField
	private String firstName;

	@DsField
	private String lastName;

	@DsField(fetch = false)
	private String fullNameField;

	@DsField
	private String email;

	@DsField
	private String businessPhone;

	@DsField
	private Integer objectId;

	@DsField
	private String objectType;

	/**
	 * Default constructor
	 */
	public ContactsLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ContactsLov_Ds(Contacts e) {
		super(e);
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullNameField() {
		return this.fullNameField;
	}

	public void setFullNameField(String fullNameField) {
		this.fullNameField = fullNameField;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBusinessPhone() {
		return this.businessPhone;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	public Integer getObjectId() {
		return this.objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
}
