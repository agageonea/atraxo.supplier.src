/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.job.model;

import atraxo.fmbas.domain.impl.fmbas_type.JobParamAssignType;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamType;
import atraxo.fmbas.domain.impl.job.BatchJob;
import atraxo.fmbas.domain.impl.job.BatchJobParameter;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = BatchJobParameter.class)
@RefLookups({@RefLookup(refId = BatchJobParameter_Ds.F_BATCHJOBID, namedQuery = BatchJob.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = BatchJobParameter_Ds.F_BATCHJOBNAME)})})
public class BatchJobParameter_Ds extends AbstractDs_Ds<BatchJobParameter> {

	public static final String ALIAS = "fmbas_BatchJobParameter_Ds";

	public static final String F_BATCHJOBID = "batchJobId";
	public static final String F_BATCHJOBNAME = "batchJobName";
	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_VALUE = "value";
	public static final String F_TYPE = "type";
	public static final String F_ASSIGNTYPE = "assignType";
	public static final String F_REFVALUES = "refValues";
	public static final String F_REFBUSINESSVALUES = "refBusinessValues";
	public static final String F_READONLY = "readOnly";

	@DsField(join = "left", path = "batchJob.id")
	private Integer batchJobId;

	@DsField(join = "left", path = "batchJob.name")
	private String batchJobName;

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private String value;

	@DsField
	private JobParamType type;

	@DsField
	private JobParamAssignType assignType;

	@DsField
	private String refValues;

	@DsField
	private String refBusinessValues;

	@DsField
	private Boolean readOnly;

	/**
	 * Default constructor
	 */
	public BatchJobParameter_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public BatchJobParameter_Ds(BatchJobParameter e) {
		super(e);
	}

	public Integer getBatchJobId() {
		return this.batchJobId;
	}

	public void setBatchJobId(Integer batchJobId) {
		this.batchJobId = batchJobId;
	}

	public String getBatchJobName() {
		return this.batchJobName;
	}

	public void setBatchJobName(String batchJobName) {
		this.batchJobName = batchJobName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public JobParamType getType() {
		return this.type;
	}

	public void setType(JobParamType type) {
		this.type = type;
	}

	public JobParamAssignType getAssignType() {
		return this.assignType;
	}

	public void setAssignType(JobParamAssignType assignType) {
		this.assignType = assignType;
	}

	public String getRefValues() {
		return this.refValues;
	}

	public void setRefValues(String refValues) {
		this.refValues = refValues;
	}

	public String getRefBusinessValues() {
		return this.refBusinessValues;
	}

	public void setRefBusinessValues(String refBusinessValues) {
		this.refBusinessValues = refBusinessValues;
	}

	public Boolean getReadOnly() {
		return this.readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}
}
