/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.profile.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.profile.AccountingRules;
import atraxo.fmbas.presenter.impl.profile.model.AccountingRules_Ds;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service AccountingRules_DsService
 */
public class AccountingRules_DsService extends AbstractEntityDsService<AccountingRules_Ds, AccountingRules_Ds, Object, AccountingRules>
		implements IDsService<AccountingRules_Ds, AccountingRules_Ds, Object> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AccountingRules_DsService.class);

	@Override
	protected void onInsert(List<AccountingRules_Ds> list, List<AccountingRules> entities, Object params) throws Exception {
		try {
			super.onInsert(list, entities, params);
		} catch (BusinessException be) {
			LOGGER.warn("Duplicate found on Accounting rules.", be);
			throw new BusinessException(BusinessErrorCode.ACCCOUNTING_RULE_ONCE_SUBSIDIARY,
					BusinessErrorCode.ACCCOUNTING_RULE_ONCE_SUBSIDIARY.getErrMsg());
		}
	}

	@Override
	protected void onUpdate(List<AccountingRules_Ds> list, List<AccountingRules> entities, Object params) throws BusinessException, Exception {
		try {
			super.onUpdate(list, entities, params);
		} catch (BusinessException be) {
			LOGGER.warn("Duplicate found on Accounting rules.", be);
			throw new BusinessException(BusinessErrorCode.ACCCOUNTING_RULE_ONCE_SUBSIDIARY,
					BusinessErrorCode.ACCCOUNTING_RULE_ONCE_SUBSIDIARY.getErrMsg());
		}
	}

}
