/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.timeserie.model;

import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = AverageMethod.class, jpqlWhere = "e.active = true")
public class AvgMthdLov_Ds extends AbstractLov_Ds<AverageMethod> {

	public static final String ALIAS = "fmbas_AvgMthdLov_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_ACTIVE = "active";

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public AvgMthdLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AvgMthdLov_Ds(AverageMethod e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
