/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.fmbas.presenter.ext.uom.qb;

import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.presenter.impl.uom.model.MeasurmentUnitsDistanceLov_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class MeasurmentUnitsDistanceLov_DsQb extends QueryBuilderWithJpql<MeasurmentUnitsDistanceLov_Ds, MeasurmentUnitsDistanceLov_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();

		this.addFilterCondition("(e.unittypeInd = :distance)");

		this.addCustomFilterItem("distance", UnitType._LENGTH_);
	}

}
