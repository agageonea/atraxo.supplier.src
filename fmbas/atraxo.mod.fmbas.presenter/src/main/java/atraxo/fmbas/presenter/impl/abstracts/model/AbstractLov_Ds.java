/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.abstracts.model;

import seava.j4e.api.annotation.DsField;
import seava.j4e.api.model.IModelWithClientId;
import seava.j4e.api.model.IModelWithId;
import seava.j4e.presenter.model.AbstractDsModel;

/**
 * Generated code. Do not modify in this file.
 */
public class AbstractLov_Ds<E> extends AbstractDsModel<E>
		implements
			IModelWithId<Integer>,
			IModelWithClientId {

	public static final String ALIAS = "fmbas_AbstractLov_Ds";

	public static final String F_ID = "id";
	public static final String F_CLIENTID = "clientId";
	public static final String F_ENTITYALIAS = "entityAlias";
	public static final String F_ENTITYFQN = "entityFqn";

	@DsField
	private Integer id;

	@DsField
	private String clientId;

	@DsField(fetch = false)
	private String entityAlias;

	@DsField(fetch = false)
	private String entityFqn;

	/**
	 * Default constructor
	 */
	public AbstractLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AbstractLov_Ds(E e) {
		super(e);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getEntityAlias() {
		return this.entityAlias;
	}

	public void setEntityAlias(String entityAlias) {
		this.entityAlias = entityAlias;
	}

	public String getEntityFqn() {
		return this.entityFqn;
	}

	public void setEntityFqn(String entityFqn) {
		this.entityFqn = entityFqn;
	}
}
