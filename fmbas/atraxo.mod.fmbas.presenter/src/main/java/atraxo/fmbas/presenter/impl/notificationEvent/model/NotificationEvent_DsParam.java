/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.notificationEvent.model;

/**
 * Generated code. Do not modify in this file.
 */
public class NotificationEvent_DsParam {

	public static final String f_contactId = "contactId";

	private Integer contactId;

	public Integer getContactId() {
		return this.contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}
}
