package atraxo.fmbas.presenter.ext;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.security.crypto.codec.Base64;

import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.extensions.IExtensionContentProvider;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterBaseService;

public class ExtensionProviderUserAvatar extends AbstractPresenterBaseService implements IExtensionContentProvider {

	@Override
	public String getContent(String targetFrame) throws Exception {

		List<UserSupp> userSupp = this.listAvatar();
		return this.addDefaultElements(targetFrame, userSupp);
	}

	protected String addDefaultElements(String targetFrame, List<UserSupp> userSupp) throws Exception {

		byte[] avatar = null;
		StringBuffer sb = new StringBuffer();

		sb.append("; _USERAVATAR_ = '");

		for (UserSupp us : userSupp) {
			avatar = us.getPicture();
			if (avatar != null) {
				String b64Avatar = new String(Base64.encode(avatar));
				sb.append(b64Avatar);
			}

		}

		sb.append("';");

		return sb.toString();
	}

	private List<UserSupp> listAvatar() throws Exception {

		IUserSuppService service = (IUserSuppService) this.findEntityService(UserSupp.class);
		EntityManager em = service.getEntityManager();

		String getUserSupp = "SELECT e FROM " + UserSupp.class.getSimpleName() + " e WHERE e.clientId=:clientId and e.code=:userCode";
		TypedQuery<UserSupp> userSuppQuery = em.createQuery(getUserSupp, UserSupp.class);
		userSuppQuery.setParameter("clientId", Session.user.get().getClientId());
		userSuppQuery.setParameter("userCode", Session.user.get().getCode());
		List<UserSupp> userSupp = userSuppQuery.getResultList();
		return userSupp;
	}

}
