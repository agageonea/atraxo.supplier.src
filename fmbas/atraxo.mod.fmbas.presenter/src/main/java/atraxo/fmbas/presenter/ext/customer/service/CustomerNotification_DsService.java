/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.customer.service;

import java.util.Collection;

import atraxo.ad.domain.impl.security.User;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.customer.CustomerNotification;
import atraxo.fmbas.presenter.impl.customer.model.CustomerNotification_Ds;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class CustomerNotification_DsService
		extends AbstractEntityDsService<CustomerNotification_Ds, CustomerNotification_Ds, Object, CustomerNotification>
		implements IDsService<CustomerNotification_Ds, CustomerNotification_Ds, Object> {

	@Override
	protected void preInsert(CustomerNotification_Ds ds, CustomerNotification e, Object params) throws Exception {
		ICustomerService srv = (ICustomerService) this.findEntityService(Customer.class);
		Customer customer = srv.findById(e.getCustomer().getId());
		for (CustomerNotification notify : customer.getCustomerNotification()) {
			if (notify.getTemplate().getType().equals(e.getTemplate().getType()) && notify.getAssignedArea().equals(e.getAssignedArea())) {
				throw new BusinessException(BusinessErrorCode.SAME_NOTIFICATION_TYPE,
						String.format(BusinessErrorCode.SAME_NOTIFICATION_TYPE.getErrMsg(), User.class.getSimpleName()));
			}
		}
	}

	@Override
	protected void preUpdateAfterEntity(CustomerNotification_Ds ds, CustomerNotification e, Object params) throws Exception {
		ICustomerService srv = (ICustomerService) this.findEntityService(Customer.class);
		Customer customer = srv.findById(e.getCustomer().getId());
		Collection<CustomerNotification> customerNotifications = customer.getCustomerNotification();
		for (CustomerNotification notify : customerNotifications) {
			if (!notify.getId().equals(e.getId()) && notify.getNotificationEvent().equals(e.getNotificationEvent())
					&& notify.getAssignedArea().equals(e.getAssignedArea())) {
				throw new BusinessException(BusinessErrorCode.SAME_NOTIFICATION_TYPE,
						String.format(BusinessErrorCode.SAME_NOTIFICATION_TYPE.getErrMsg(), User.class.getSimpleName()));
			}
		}
	}

}
