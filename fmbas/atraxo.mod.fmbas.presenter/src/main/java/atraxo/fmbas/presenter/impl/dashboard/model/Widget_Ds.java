/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.dashboard.model;

import atraxo.fmbas.domain.impl.dashboard.Widgets;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Widgets.class)
public class Widget_Ds extends AbstractDs_Ds<Widgets> {

	public static final String ALIAS = "fmbas_Widget_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_THUMBPATH = "thumbPath";
	public static final String F_CFGPATH = "cfgPath";
	public static final String F_DSNAME = "dsName";
	public static final String F_METHODNAME = "methodName";

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private String thumbPath;

	@DsField
	private String cfgPath;

	@DsField
	private String dsName;

	@DsField
	private String methodName;

	/**
	 * Default constructor
	 */
	public Widget_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Widget_Ds(Widgets e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getThumbPath() {
		return this.thumbPath;
	}

	public void setThumbPath(String thumbPath) {
		this.thumbPath = thumbPath;
	}

	public String getCfgPath() {
		return this.cfgPath;
	}

	public void setCfgPath(String cfgPath) {
		this.cfgPath = cfgPath;
	}

	public String getDsName() {
		return this.dsName;
	}

	public void setDsName(String dsName) {
		this.dsName = dsName;
	}

	public String getMethodName() {
		return this.methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
}
