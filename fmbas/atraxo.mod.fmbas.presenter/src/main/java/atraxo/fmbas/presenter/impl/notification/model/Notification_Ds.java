/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.notification.model;

import atraxo.ad.domain.impl.ad.Category;
import atraxo.ad.domain.impl.ad.NotificationAction;
import atraxo.ad.domain.impl.ad.Priority;
import atraxo.fmbas.domain.impl.notification.Notification;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Notification.class)
public class Notification_Ds extends AbstractDs_Ds<Notification> {

	public static final String ALIAS = "fmbas_Notification_Ds";

	public static final String F_TITLE = "title";
	public static final String F_DESCRIPION = "descripion";
	public static final String F_PRIORITY = "priority";
	public static final String F_CATEGORY = "category";
	public static final String F_EVENTDATE = "eventDate";
	public static final String F_LIFETIME = "lifeTime";
	public static final String F_ACTION = "action";
	public static final String F_LINK = "link";
	public static final String F_METHODNAME = "methodName";
	public static final String F_METHODPARAM = "methodParam";
	public static final String F_SEPARATOR = "separator";
	public static final String F_USERCODE = "userCode";

	@DsField
	private String title;

	@DsField
	private String descripion;

	@DsField
	private Priority priority;

	@DsField
	private Category category;

	@DsField
	private Date eventDate;

	@DsField
	private Date lifeTime;

	@DsField
	private NotificationAction action;

	@DsField
	private String link;

	@DsField
	private String methodName;

	@DsField
	private String methodParam;

	@DsField(fetch = false)
	private String separator;

	@DsField(fetch = false)
	private String userCode;

	/**
	 * Default constructor
	 */
	public Notification_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Notification_Ds(Notification e) {
		super(e);
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescripion() {
		return this.descripion;
	}

	public void setDescripion(String descripion) {
		this.descripion = descripion;
	}

	public Priority getPriority() {
		return this.priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Date getEventDate() {
		return this.eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Date getLifeTime() {
		return this.lifeTime;
	}

	public void setLifeTime(Date lifeTime) {
		this.lifeTime = lifeTime;
	}

	public NotificationAction getAction() {
		return this.action;
	}

	public void setAction(NotificationAction action) {
		this.action = action;
	}

	public String getLink() {
		return this.link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getMethodName() {
		return this.methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getMethodParam() {
		return this.methodParam;
	}

	public void setMethodParam(String methodParam) {
		this.methodParam = methodParam;
	}

	public String getSeparator() {
		return this.separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public String getUserCode() {
		return this.userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
}
