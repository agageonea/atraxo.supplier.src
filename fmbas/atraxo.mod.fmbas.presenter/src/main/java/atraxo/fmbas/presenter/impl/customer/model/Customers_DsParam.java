/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

/**
 * Generated code. Do not modify in this file.
 */
public class Customers_DsParam {

	public static final String f_remark = "remark";
	public static final String f_finalCustId = "finalCustId";
	public static final String f_generatedCustomerCode = "generatedCustomerCode";
	public static final String f_exportCustomerDescription = "exportCustomerDescription";
	public static final String f_exportCustomerResult = "exportCustomerResult";
	public static final String f_exportCustomerOption = "exportCustomerOption";

	private String remark;

	private Integer finalCustId;

	private String generatedCustomerCode;

	private String exportCustomerDescription;

	private Boolean exportCustomerResult;

	private String exportCustomerOption;

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getFinalCustId() {
		return this.finalCustId;
	}

	public void setFinalCustId(Integer finalCustId) {
		this.finalCustId = finalCustId;
	}

	public String getGeneratedCustomerCode() {
		return this.generatedCustomerCode;
	}

	public void setGeneratedCustomerCode(String generatedCustomerCode) {
		this.generatedCustomerCode = generatedCustomerCode;
	}

	public String getExportCustomerDescription() {
		return this.exportCustomerDescription;
	}

	public void setExportCustomerDescription(String exportCustomerDescription) {
		this.exportCustomerDescription = exportCustomerDescription;
	}

	public Boolean getExportCustomerResult() {
		return this.exportCustomerResult;
	}

	public void setExportCustomerResult(Boolean exportCustomerResult) {
		this.exportCustomerResult = exportCustomerResult;
	}

	public String getExportCustomerOption() {
		return this.exportCustomerOption;
	}

	public void setExportCustomerOption(String exportCustomerOption) {
		this.exportCustomerOption = exportCustomerOption;
	}
}
