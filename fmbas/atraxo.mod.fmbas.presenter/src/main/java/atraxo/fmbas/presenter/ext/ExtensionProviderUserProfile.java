package atraxo.fmbas.presenter.ext;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.domain.impl.fmbas_type.Title;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.extensions.IExtensionContentProvider;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterBaseService;

public class ExtensionProviderUserProfile extends AbstractPresenterBaseService implements IExtensionContentProvider {

	@Override
	public String getContent(String targetFrame) throws Exception {

		List<UserSupp> userSupp = this.listUserProfile();
		return this.addUserProfile(targetFrame, userSupp);
	}

	protected String addUserProfile(String targetFrame, List<UserSupp> userSupp) throws Exception {

		
		StringBuffer sb = new StringBuffer();
		String firstName = null;
		String lastName = null;
		Title title = null;
		
		sb.append("; _USERPROFILE_ = { ");

		for (UserSupp us : userSupp) {
			firstName = us.getFirstName();
			lastName = us.getLastName();
			title = us.getTitle();
			sb.append("title : '" + title + "', ");
			sb.append("firstName : '" + firstName + "', ");
			sb.append("lastName : '" + lastName + "', ");

		}

		sb.append("};");
		int ind = sb.lastIndexOf(",");
		if (ind > 0) {
			sb.replace(ind, ind + 1, "");
		}

		return sb.toString();
	}

	private List<UserSupp> listUserProfile() throws Exception {

		IUserSuppService service = (IUserSuppService) this.findEntityService(UserSupp.class);
		EntityManager em = service.getEntityManager();

		String getUserSupp = "SELECT e FROM " + UserSupp.class.getSimpleName() + " e WHERE e.clientId=:clientId and e.code=:userCode";
		TypedQuery<UserSupp> userSuppQuery = em.createQuery(getUserSupp, UserSupp.class);
		userSuppQuery.setParameter("clientId", Session.user.get().getClientId());
		userSuppQuery.setParameter("userCode", Session.user.get().getCode());
		List<UserSupp> userSupp = userSuppQuery.getResultList();
		return userSupp;
	}

}