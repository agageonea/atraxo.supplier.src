/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.dashboard.model;

import atraxo.fmbas.domain.impl.dashboard.Layouts;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Layouts.class)
public class LayoutLov_Ds extends AbstractDs_Ds<Layouts> {

	public static final String ALIAS = "fmbas_LayoutLov_Ds";

	public static final String F_NAME = "name";

	@DsField
	private String name;

	/**
	 * Default constructor
	 */
	public LayoutLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public LayoutLov_Ds(Layouts e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
