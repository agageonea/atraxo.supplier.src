/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.user.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = UserSubsidiary.class, sort = {@SortField(field = UserSubsidiaryLov_Ds.F_CODE)})
@RefLookups({
		@RefLookup(refId = UserSubsidiaryLov_Ds.F_SUBSIDIARYID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = UserSubsidiaryLov_Ds.F_CODE)}),
		@RefLookup(refId = UserSubsidiaryLov_Ds.F_USERID, namedQuery = UserSupp.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = UserSubsidiaryLov_Ds.F_USERCODE)})})
public class UserSubsidiaryLov_Ds extends AbstractLov_Ds<UserSubsidiary> {

	public static final String ALIAS = "fmbas_UserSubsidiaryLov_Ds";

	public static final String F_MAINSUBSIDIARY = "mainSubsidiary";
	public static final String F_USERCODE = "userCode";
	public static final String F_USERID = "userId";
	public static final String F_SUBSIDIARYID = "subsidiaryId";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";

	@DsField
	private Boolean mainSubsidiary;

	@DsField(join = "left", path = "user.code")
	private String userCode;

	@DsField(join = "left", path = "user.id")
	private String userId;

	@DsField(join = "left", path = "subsidiary.id")
	private Integer subsidiaryId;

	@DsField(join = "left", path = "subsidiary.code")
	private String code;

	@DsField(join = "left", path = "subsidiary.name")
	private String name;

	/**
	 * Default constructor
	 */
	public UserSubsidiaryLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public UserSubsidiaryLov_Ds(UserSubsidiary e) {
		super(e);
	}

	public Boolean getMainSubsidiary() {
		return this.mainSubsidiary;
	}

	public void setMainSubsidiary(Boolean mainSubsidiary) {
		this.mainSubsidiary = mainSubsidiary;
	}

	public String getUserCode() {
		return this.userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getSubsidiaryId() {
		return this.subsidiaryId;
	}

	public void setSubsidiaryId(Integer subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
