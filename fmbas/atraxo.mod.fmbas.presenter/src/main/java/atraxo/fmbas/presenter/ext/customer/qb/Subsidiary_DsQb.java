/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

package atraxo.fmbas.presenter.ext.customer.qb;

import atraxo.fmbas.presenter.impl.customer.model.Subsidiary_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class Subsidiary_DsQb
		extends
			QueryBuilderWithJpql<Subsidiary_Ds, Subsidiary_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();
		this.addFilterCondition("e.isSubsidiary = :isSubsidiary");
		this.addCustomFilterItem("isSubsidiary", true);
	}

}
