package atraxo.fmbas.presenter.ext.workflow.delegate;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.workflow.IWorkflowParameterService;
import atraxo.fmbas.business.api.workflow.IWorkflowService;
import atraxo.fmbas.business.ext.bpm.WorkflowBpmManager;
import atraxo.fmbas.business.ext.util.EntityCloner;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;
import atraxo.fmbas.presenter.impl.workflow.model.Workflow_Ds;
import atraxo.fmbas.presenter.impl.workflow.model.Workflow_DsParam;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author vhojda
 */
public class Workflow_Pd extends AbstractPresenterDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(Workflow_Pd.class);

	/**
	 * @param wkf
	 * @throws BusinessException
	 */
	public void deployWorkflow(Workflow_Ds wkf) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START deployWorkflow()");
		}
		IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean("workflowBpmManager");
		workflowBpmManager.deployWorkflow(wkf.getId(), wkf.getWorkflowFile());

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END deployWorkflow()");
		}
	}

	/**
	 * @param wkf
	 * @throws BusinessException
	 */
	public void copyWorkflow(Workflow_Ds wkf, Workflow_DsParam param) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START copyWorkflow()");
		}

		// assume the response is OK
		param.setCopyResultValue(Boolean.TRUE);

		// check Workflow existence
		IWorkflowService workflowService = this.getApplicationContext().getBean(IWorkflowService.class);
		IWorkflowParameterService workflowParameterService = this.getApplicationContext().getBean(IWorkflowParameterService.class);
		ICustomerService customerService = this.getApplicationContext().getBean(ICustomerService.class);

		Workflow dbWorkflow = null;
		try {
			dbWorkflow = workflowService.findByNameAndSubsidiary(wkf.getName(), param.getSubsidiary());
		} catch (ApplicationException e) {
			// do nothing, since it is possible to not have workflows with this properties
			LOGGER.info(
					"Could not find a workflow by name and subsidiary, will  do nothing, since it is possible to not have workflows with this properties.",
					e);
		}

		if (dbWorkflow != null) {
			param.setCopyResultValue(Boolean.FALSE);
			param.setCopyResultDescription(
					"There is already a workflow for subsidiary '" + param.getSubsidiary() + "'. Please repeat the operation.");
		} else {

			try {
				dbWorkflow = workflowService.findById(wkf.getId());
				Customer subCustomer = customerService.findByCode(param.getSubsidiary());

				// first copy the workflow
				Workflow anotherWorkflow = EntityCloner.cloneEntity(dbWorkflow);
				anotherWorkflow.setSubsidiary(subCustomer);
				workflowService.insert(anotherWorkflow);

				// then, copy the params
				List<WorkflowParameter> anotherParamsList = new ArrayList<>();
				for (WorkflowParameter dbParam : dbWorkflow.getParams()) {
					WorkflowParameter anotherParam = EntityCloner.cloneEntity(dbParam);
					anotherParam.setWorkflow(anotherWorkflow);
					anotherParamsList.add(anotherParam);
				}
				workflowParameterService.insert(anotherParamsList);

			} catch (Exception e) {
				LOGGER.error("ERROR:could not copy entity " + dbWorkflow, e);
				param.setCopyResultValue(Boolean.FALSE);
				param.setCopyResultDescription(
						"There was an internal error when copying the workflow.Please try again. Error:" + e.getLocalizedMessage());
			}

		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END copyWorkflow()");
		}
	}

}
