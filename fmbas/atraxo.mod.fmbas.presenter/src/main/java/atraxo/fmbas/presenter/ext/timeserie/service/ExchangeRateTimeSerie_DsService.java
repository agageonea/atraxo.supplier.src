/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.timeserie.service;

import java.util.List;

import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.domain.impl.fmbas_type.PostTypeInd;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesApprovalStatus;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import atraxo.fmbas.presenter.impl.timeserie.model.ExchangeRateTimeSerie_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ExchangeRateTimeSerie_DsService extends AbstractEntityDsService<ExchangeRateTimeSerie_Ds, ExchangeRateTimeSerie_Ds, Object, TimeSerie>
		implements IDsService<ExchangeRateTimeSerie_Ds, ExchangeRateTimeSerie_Ds, Object> {

	private static final Logger LOG = LoggerFactory.getLogger(ExchangeRateTimeSerie_DsService.class);

	@Autowired
	private ISystemParameterService sysParamService;

	@Autowired
	private IWorkflowInstanceEntityService workflowInstanceEntityService;

	@Autowired
	private IWorkflowBpmManager workflowBpmManager;
	@Autowired
	private ITimeSerieService timeSerieService;

	@Override
	protected void postFind(IQueryBuilder<ExchangeRateTimeSerie_Ds, ExchangeRateTimeSerie_Ds, Object> builder, List<ExchangeRateTimeSerie_Ds> result)
			throws Exception {
		super.postFind(builder, result);

		for (ExchangeRateTimeSerie_Ds ds : result) {
			// calculezi conform regulii
			String measure;

			if (ds.getPostTypeInd() == null || ds.getPostTypeInd().equals(PostTypeInd._AMOUNT_)) {
				measure = ds.getCurrency1Code() + "/" + ds.getCurrency2Code();
			} else {
				measure = ds.getCurrency2Code() + "/" + ds.getCurrency1Code();
			}

			// Set the enable rules for approve and reject workflow buttons
			Boolean canBeCompleted = Boolean.FALSE;
			if (this.sysParamService.getExchangeRateApprovalWorkflow()
					&& TimeSeriesApprovalStatus._AWAITING_APPROVAL_.equals(ds.getApprovalStatus())) {

				try {
					WorkflowInstanceEntity wie = this.workflowInstanceEntityService.findByBusinessKey(
							WorkflowNames.EXCHANGE_RATE_TIME_SERIES_APPROVAL.getWorkflowName(), ds.getId(),
							ds._getEntity_().getClass().getSimpleName());
					canBeCompleted = this.workflowBpmManager.canComplete(wie.getWorkflowInstance().getId());
				} catch (Exception e) {
					LOG.warn("Error trying to find workflow instance!", e);
				}
			}

			TimeSerie rawTSItem = ds._getEntity_();

			ds.setMeasure(measure);
			ds.setCanBeCompleted(canBeCompleted);
			StopWatch stopWatch = new StopWatch();
			stopWatch.start();

			Long rawTimeSerieCount = this.timeSerieService.countRawTimeSerieItems(rawTSItem);

			stopWatch.stop();

			long millis = stopWatch.getTime();
			System.out.println("=====================>Ellapsed time :" + millis);
			ds.setHasTimeSerieItems(rawTimeSerieCount > 0);
		}
	}

}
