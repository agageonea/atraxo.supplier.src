/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.financialSources.model;

import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FinancialSources.class, sort = {@SortField(field = FinancialSources_Ds.F_NAME)})
public class FinancialSources_Ds extends AbstractDs_Ds<FinancialSources> {

	public static final String ALIAS = "fmbas_FinancialSources_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_ISSTDFLG = "isStdFlg";
	public static final String F_DESCRIPTION = "description";
	public static final String F_ACTIVE = "active";

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private Boolean isStdFlg;

	@DsField
	private String description;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public FinancialSources_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FinancialSources_Ds(FinancialSources e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsStdFlg() {
		return this.isStdFlg;
	}

	public void setIsStdFlg(Boolean isStdFlg) {
		this.isStdFlg = isStdFlg;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
