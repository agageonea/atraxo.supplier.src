/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.externalInterfaces.model;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceType;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ExternalInterface.class)
public class ExternalInterfaces_Ds extends AbstractDs_Ds<ExternalInterface> {

	public static final String ALIAS = "fmbas_ExternalInterfaces_Ds";

	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_ENABLED = "enabled";
	public static final String F_EXTERNALINTERFACETYPE = "externalInterfaceType";
	public static final String F_LASTREQUEST = "lastRequest";
	public static final String F_LASTREQUESTSTATUS = "lastRequestStatus";

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private Boolean enabled;

	@DsField
	private ExternalInterfaceType externalInterfaceType;

	@DsField(fetch = false)
	private Date lastRequest;

	@DsField(fetch = false)
	private String lastRequestStatus;

	/**
	 * Default constructor
	 */
	public ExternalInterfaces_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ExternalInterfaces_Ds(ExternalInterface e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public ExternalInterfaceType getExternalInterfaceType() {
		return this.externalInterfaceType;
	}

	public void setExternalInterfaceType(
			ExternalInterfaceType externalInterfaceType) {
		this.externalInterfaceType = externalInterfaceType;
	}

	public Date getLastRequest() {
		return this.lastRequest;
	}

	public void setLastRequest(Date lastRequest) {
		this.lastRequest = lastRequest;
	}

	public String getLastRequestStatus() {
		return this.lastRequestStatus;
	}

	public void setLastRequestStatus(String lastRequestStatus) {
		this.lastRequestStatus = lastRequestStatus;
	}
}
