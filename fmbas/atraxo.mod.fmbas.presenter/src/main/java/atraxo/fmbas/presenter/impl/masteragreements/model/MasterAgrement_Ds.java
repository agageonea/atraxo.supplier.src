/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.masteragreements.model;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceType;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsStatus;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.masteragreements.MasterAgreement;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = MasterAgreement.class, sort = {@SortField(field = MasterAgrement_Ds.F_VALIDFROM, desc = true)})
@RefLookups({
		@RefLookup(refId = MasterAgrement_Ds.F_COMPANYID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = MasterAgrement_Ds.F_COMPANY)}),
		@RefLookup(refId = MasterAgrement_Ds.F_CURENCYID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = MasterAgrement_Ds.F_CURRENCYCODE)}),
		@RefLookup(refId = MasterAgrement_Ds.F_FINANCIALSOURCEID),
		@RefLookup(refId = MasterAgrement_Ds.F_AVGMTHDID)})
public class MasterAgrement_Ds extends AbstractDs_Ds<MasterAgreement> {

	public static final String ALIAS = "fmbas_MasterAgrement_Ds";

	public static final String F_PERCENT = "percent";
	public static final String F_DAYS = "days";
	public static final String F_GENERALTERMSTITLE = "generalTermsTitle";
	public static final String F_PAYMENTTERMSTITLE = "paymentTermsTitle";
	public static final String F_CREDITTERMSTITLE = "creditTermsTitle";
	public static final String F_SEPARATOR = "separator";
	public static final String F_COMPANYID = "companyId";
	public static final String F_COMPANY = "company";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_EVERGREEN = "evergreen";
	public static final String F_RENEWALREMINDER = "renewalReminder";
	public static final String F_STATUS = "status";
	public static final String F_CURENCYID = "curencyId";
	public static final String F_CURRENCYCODE = "currencyCode";
	public static final String F_PERIOD = "period";
	public static final String F_PAYMENTTERMS = "paymentTerms";
	public static final String F_REFERENCETO = "referenceTo";
	public static final String F_INVOICEFREQUENCY = "invoiceFrequency";
	public static final String F_INVOICETYPE = "invoiceType";
	public static final String F_FINANCIALSOURCEID = "financialSourceId";
	public static final String F_FINANCIALSOURCECODE = "financialSourceCode";
	public static final String F_AVGMTHDID = "avgMthdId";
	public static final String F_AVGMTHD = "avgMthd";
	public static final String F_CREDITCARD = "creditCard";
	public static final String F_EXPOSURE = "exposure";
	public static final String F_FOREX = "forex";

	@DsField(fetch = false)
	private String percent;

	@DsField(fetch = false)
	private String days;

	@DsField(fetch = false)
	private String generalTermsTitle;

	@DsField(fetch = false)
	private String paymentTermsTitle;

	@DsField(fetch = false)
	private String creditTermsTitle;

	@DsField(fetch = false)
	private String separator;

	@DsField(join = "left", path = "company.id")
	private Integer companyId;

	@DsField(join = "left", path = "company.code")
	private String company;

	@DsField(jpqlFilter = " e.validTo >= :validFrom ")
	private Date validFrom;

	@DsField(jpqlFilter = " e.validFrom <= :validTo ")
	private Date validTo;

	@DsField
	private Boolean evergreen;

	@DsField
	private Date renewalReminder;

	@DsField
	private MasterAgreementsStatus status;

	@DsField(join = "left", path = "currency.id")
	private Integer curencyId;

	@DsField(join = "left", path = "currency.code")
	private String currencyCode;

	@DsField
	private MasterAgreementsPeriod period;

	@DsField
	private Integer paymentTerms;

	@DsField
	private PaymentDay referenceTo;

	@DsField
	private InvoiceFreq invoiceFrequency;

	@DsField
	private InvoiceType invoiceType;

	@DsField(join = "left", path = "financialsource.id")
	private Integer financialSourceId;

	@DsField(join = "left", path = "financialsource.code")
	private String financialSourceCode;

	@DsField(join = "left", path = "averageMethod.id")
	private Integer avgMthdId;

	@DsField(join = "left", path = "averageMethod.name")
	private String avgMthd;

	@DsField
	private Boolean creditCard;

	@DsField(fetch = false)
	private Integer exposure;

	@DsField(fetch = false, path = "calcVal")
	private String forex;

	/**
	 * Default constructor
	 */
	public MasterAgrement_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public MasterAgrement_Ds(MasterAgreement e) {
		super(e);
	}

	public String getPercent() {
		return this.percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	public String getDays() {
		return this.days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public String getGeneralTermsTitle() {
		return this.generalTermsTitle;
	}

	public void setGeneralTermsTitle(String generalTermsTitle) {
		this.generalTermsTitle = generalTermsTitle;
	}

	public String getPaymentTermsTitle() {
		return this.paymentTermsTitle;
	}

	public void setPaymentTermsTitle(String paymentTermsTitle) {
		this.paymentTermsTitle = paymentTermsTitle;
	}

	public String getCreditTermsTitle() {
		return this.creditTermsTitle;
	}

	public void setCreditTermsTitle(String creditTermsTitle) {
		this.creditTermsTitle = creditTermsTitle;
	}

	public String getSeparator() {
		return this.separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public Integer getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Boolean getEvergreen() {
		return this.evergreen;
	}

	public void setEvergreen(Boolean evergreen) {
		this.evergreen = evergreen;
	}

	public Date getRenewalReminder() {
		return this.renewalReminder;
	}

	public void setRenewalReminder(Date renewalReminder) {
		this.renewalReminder = renewalReminder;
	}

	public MasterAgreementsStatus getStatus() {
		return this.status;
	}

	public void setStatus(MasterAgreementsStatus status) {
		this.status = status;
	}

	public Integer getCurencyId() {
		return this.curencyId;
	}

	public void setCurencyId(Integer curencyId) {
		this.curencyId = curencyId;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public MasterAgreementsPeriod getPeriod() {
		return this.period;
	}

	public void setPeriod(MasterAgreementsPeriod period) {
		this.period = period;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public PaymentDay getReferenceTo() {
		return this.referenceTo;
	}

	public void setReferenceTo(PaymentDay referenceTo) {
		this.referenceTo = referenceTo;
	}

	public InvoiceFreq getInvoiceFrequency() {
		return this.invoiceFrequency;
	}

	public void setInvoiceFrequency(InvoiceFreq invoiceFrequency) {
		this.invoiceFrequency = invoiceFrequency;
	}

	public InvoiceType getInvoiceType() {
		return this.invoiceType;
	}

	public void setInvoiceType(InvoiceType invoiceType) {
		this.invoiceType = invoiceType;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}

	public Integer getAvgMthdId() {
		return this.avgMthdId;
	}

	public void setAvgMthdId(Integer avgMthdId) {
		this.avgMthdId = avgMthdId;
	}

	public String getAvgMthd() {
		return this.avgMthd;
	}

	public void setAvgMthd(String avgMthd) {
		this.avgMthd = avgMthd;
	}

	public Boolean getCreditCard() {
		return this.creditCard;
	}

	public void setCreditCard(Boolean creditCard) {
		this.creditCard = creditCard;
	}

	public Integer getExposure() {
		return this.exposure;
	}

	public void setExposure(Integer exposure) {
		this.exposure = exposure;
	}

	public String getForex() {
		return this.forex;
	}

	public void setForex(String forex) {
		this.forex = forex;
	}
}
