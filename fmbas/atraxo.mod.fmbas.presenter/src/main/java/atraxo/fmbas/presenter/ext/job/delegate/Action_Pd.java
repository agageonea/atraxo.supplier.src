package atraxo.fmbas.presenter.ext.job.delegate;

import atraxo.fmbas.business.api.job.IActionService;
import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.presenter.impl.job.model.Action_Ds;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class Action_Pd extends AbstractPresenterDelegate {

	public void moveUp(Action_Ds ds) throws Exception {
		IActionService service = (IActionService) this.findEntityService(Action.class);
		Action e = service.findById(ds.getId());
		service.move(e, -1);
	}

	public void moveDown(Action_Ds ds) throws Exception {
		IActionService service = (IActionService) this.findEntityService(Action.class);
		Action e = service.findById(ds.getId());
		service.move(e, 1);
	}

}
