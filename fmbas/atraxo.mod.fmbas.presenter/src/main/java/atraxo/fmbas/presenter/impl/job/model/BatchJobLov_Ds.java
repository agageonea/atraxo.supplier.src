/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.job.model;

import atraxo.fmbas.domain.impl.job.BatchJob;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = BatchJob.class)
public class BatchJobLov_Ds extends AbstractLov_Ds<BatchJob> {

	public static final String ALIAS = "fmbas_BatchJobLov_Ds";

	public static final String F_NAME = "name";

	@DsField
	private String name;

	/**
	 * Default constructor
	 */
	public BatchJobLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public BatchJobLov_Ds(BatchJob e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
