/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerType;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Customer.class, sort = {@SortField(field = CustomerBlockedActiveLov_Ds.F_CODE)})
public class CustomerBlockedActiveLov_Ds extends AbstractLov_Ds<Customer> {

	public static final String ALIAS = "fmbas_CustomerBlockedActiveLov_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_TYPE = "type";
	public static final String F_REFID = "refid";
	public static final String F_STATUS = "status";
	public static final String F_IATACODE = "iataCode";
	public static final String F_ISCUSTOMER = "isCustomer";
	public static final String F_BUYERID = "buyerId";
	public static final String F_BUYERNAME = "buyerName";
	public static final String F_BUYERCODE = "buyerCode";
	public static final String F_PARENTGROUPID = "parentGroupId";
	public static final String F_PARENTGROUPCODE = "parentGroupCode";
	public static final String F_PRIMARYCONTACTID = "primaryContactId";
	public static final String F_PRIMARYCONTACTNAME = "primaryContactName";

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private CustomerType type;

	@DsField
	private String refid;

	@DsField
	private CustomerStatus status;

	@DsField
	private String iataCode;

	@DsField
	private Boolean isCustomer;

	@DsField(join = "left", path = "responsibleBuyer.id")
	private String buyerId;

	@DsField(join = "left", path = "responsibleBuyer.name")
	private String buyerName;

	@DsField(join = "left", path = "responsibleBuyer.code")
	private String buyerCode;

	@DsField(join = "left", path = "parent.id")
	private Integer parentGroupId;

	@DsField(join = "left", path = "parent.code")
	private String parentGroupCode;

	@DsField(fetch = false)
	private Integer primaryContactId;

	@DsField(fetch = false)
	private String primaryContactName;

	/**
	 * Default constructor
	 */
	public CustomerBlockedActiveLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public CustomerBlockedActiveLov_Ds(Customer e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CustomerType getType() {
		return this.type;
	}

	public void setType(CustomerType type) {
		this.type = type;
	}

	public String getRefid() {
		return this.refid;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}

	public CustomerStatus getStatus() {
		return this.status;
	}

	public void setStatus(CustomerStatus status) {
		this.status = status;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public Boolean getIsCustomer() {
		return this.isCustomer;
	}

	public void setIsCustomer(Boolean isCustomer) {
		this.isCustomer = isCustomer;
	}

	public String getBuyerId() {
		return this.buyerId;
	}

	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}

	public String getBuyerName() {
		return this.buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getBuyerCode() {
		return this.buyerCode;
	}

	public void setBuyerCode(String buyerCode) {
		this.buyerCode = buyerCode;
	}

	public Integer getParentGroupId() {
		return this.parentGroupId;
	}

	public void setParentGroupId(Integer parentGroupId) {
		this.parentGroupId = parentGroupId;
	}

	public String getParentGroupCode() {
		return this.parentGroupCode;
	}

	public void setParentGroupCode(String parentGroupCode) {
		this.parentGroupCode = parentGroupCode;
	}

	public Integer getPrimaryContactId() {
		return this.primaryContactId;
	}

	public void setPrimaryContactId(Integer primaryContactId) {
		this.primaryContactId = primaryContactId;
	}

	public String getPrimaryContactName() {
		return this.primaryContactName;
	}

	public void setPrimaryContactName(String primaryContactName) {
		this.primaryContactName = primaryContactName;
	}
}
