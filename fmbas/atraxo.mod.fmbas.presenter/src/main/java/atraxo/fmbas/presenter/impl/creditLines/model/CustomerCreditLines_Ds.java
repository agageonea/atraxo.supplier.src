/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.creditLines.model;

import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CreditType;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = CreditLines.class, sort = {@SortField(field = CustomerCreditLines_Ds.F_VALIDFROM, desc = true)})
@RefLookups({
		@RefLookup(refId = CustomerCreditLines_Ds.F_CURRENCYID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = CustomerCreditLines_Ds.F_CURRENCYCODE)}),
		@RefLookup(refId = CustomerCreditLines_Ds.F_COMPANYID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = CustomerCreditLines_Ds.F_COMPANYCODE)})})
public class CustomerCreditLines_Ds extends AbstractDs_Ds<CreditLines> {

	public static final String ALIAS = "fmbas_CustomerCreditLines_Ds";

	public static final String F_EXPOSURE = "exposure";
	public static final String F_PERCENT = "percent";
	public static final String F_LABELFIELD = "labelField";
	public static final String F_CURRENCYID = "currencyId";
	public static final String F_CURRENCYCODE = "currencyCode";
	public static final String F_CURRENCYCODEGRID = "currencyCodeGrid";
	public static final String F_CURRENCYNAME = "currencyName";
	public static final String F_COMPANYID = "companyId";
	public static final String F_COMPANYCODE = "companyCode";
	public static final String F_COMPANYNAME = "companyName";
	public static final String F_AMOUNT = "amount";
	public static final String F_AVAILABILITY = "availability";
	public static final String F_ALERTPERCENTAGE = "alertPercentage";
	public static final String F_CREDITTERM = "creditTerm";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_ACTIVE = "active";
	public static final String F_CHANGEREASON = "changeReason";
	public static final String F_CURRENCYDISPLAY = "currencyDisplay";

	@DsField(fetch = false)
	private Integer exposure;

	@DsField(fetch = false)
	private String percent;

	@DsField(fetch = false)
	private String labelField;

	@DsField(join = "left", path = "currency.id")
	private Integer currencyId;

	@DsField(join = "left", path = "currency.code")
	private String currencyCode;

	@DsField(join = "left", path = "currency.code")
	private String currencyCodeGrid;

	@DsField(join = "left", path = "currency.name")
	private String currencyName;

	@DsField(join = "left", path = "customer.id")
	private Integer companyId;

	@DsField(join = "left", path = "customer.code")
	private String companyCode;

	@DsField(join = "left", path = "customer.name")
	private String companyName;

	@DsField
	private Integer amount;

	@DsField
	private Integer availability;

	@DsField
	private Integer alertPercentage;

	@DsField
	private CreditType creditTerm;

	@DsField(jpqlFilter = " e.validTo >= :validFrom ")
	private Date validFrom;

	@DsField(jpqlFilter = " e.validFrom <= :validTo ")
	private Date validTo;

	@DsField
	private Boolean active;

	@DsField(fetch = false)
	private String changeReason;

	@DsField(fetch = false)
	private String currencyDisplay;

	/**
	 * Default constructor
	 */
	public CustomerCreditLines_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public CustomerCreditLines_Ds(CreditLines e) {
		super(e);
	}

	public Integer getExposure() {
		return this.exposure;
	}

	public void setExposure(Integer exposure) {
		this.exposure = exposure;
	}

	public String getPercent() {
		return this.percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}

	public Integer getCurrencyId() {
		return this.currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getCurrencyCodeGrid() {
		return this.currencyCodeGrid;
	}

	public void setCurrencyCodeGrid(String currencyCodeGrid) {
		this.currencyCodeGrid = currencyCodeGrid;
	}

	public String getCurrencyName() {
		return this.currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public Integer getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getCompanyCode() {
		return this.companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Integer getAmount() {
		return this.amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getAvailability() {
		return this.availability;
	}

	public void setAvailability(Integer availability) {
		this.availability = availability;
	}

	public Integer getAlertPercentage() {
		return this.alertPercentage;
	}

	public void setAlertPercentage(Integer alertPercentage) {
		this.alertPercentage = alertPercentage;
	}

	public CreditType getCreditTerm() {
		return this.creditTerm;
	}

	public void setCreditTerm(CreditType creditTerm) {
		this.creditTerm = creditTerm;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getChangeReason() {
		return this.changeReason;
	}

	public void setChangeReason(String changeReason) {
		this.changeReason = changeReason;
	}

	public String getCurrencyDisplay() {
		return this.currencyDisplay;
	}

	public void setCurrencyDisplay(String currencyDisplay) {
		this.currencyDisplay = currencyDisplay;
	}
}
