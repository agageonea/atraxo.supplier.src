/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.abstracts.model;

import java.util.Date;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.model.IModelWithClientId;
import seava.j4e.api.model.IModelWithId;
import seava.j4e.presenter.model.AbstractDsModel;

/**
 * Generated code. Do not modify in this file.
 */
public class AbstractDs_Ds<E> extends AbstractDsModel<E>
		implements
			IModelWithId<Integer>,
			IModelWithClientId {

	public static final String ALIAS = "fmbas_AbstractDs_Ds";

	public static final String F_ID = "id";
	public static final String F_REFID = "refid";
	public static final String F_CLIENTID = "clientId";
	public static final String F_CREATEDAT = "createdAt";
	public static final String F_MODIFIEDAT = "modifiedAt";
	public static final String F_CREATEDBY = "createdBy";
	public static final String F_MODIFIEDBY = "modifiedBy";
	public static final String F_VERSION = "version";
	public static final String F_ENTITYALIAS = "entityAlias";
	public static final String F_ENTITYFQN = "entityFqn";

	@DsField(noUpdate = true)
	private Integer id;

	@DsField(noUpdate = true)
	private String refid;

	@DsField(noUpdate = true)
	private String clientId;

	@DsField(noUpdate = true)
	private Date createdAt;

	@DsField(noUpdate = true)
	private Date modifiedAt;

	@DsField(noUpdate = true)
	private String createdBy;

	@DsField(noUpdate = true)
	private String modifiedBy;

	@DsField
	private Long version;

	@DsField(fetch = false)
	private String entityAlias;

	@DsField(fetch = false)
	private String entityFqn;

	/**
	 * Default constructor
	 */
	public AbstractDs_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AbstractDs_Ds(E e) {
		super(e);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRefid() {
		return this.refid;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return this.modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getEntityAlias() {
		return this.entityAlias;
	}

	public void setEntityAlias(String entityAlias) {
		this.entityAlias = entityAlias;
	}

	public String getEntityFqn() {
		return this.entityFqn;
	}

	public void setEntityFqn(String entityFqn) {
		this.entityFqn = entityFqn;
	}
}
