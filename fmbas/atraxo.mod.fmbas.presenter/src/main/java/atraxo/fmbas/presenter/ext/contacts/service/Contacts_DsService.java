/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.contacts.service;

import java.util.List;

import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.presenter.impl.contacts.model.Contacts_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class Contacts_DsService extends AbstractEntityDsService<Contacts_Ds, Contacts_Ds, Object, Contacts> implements
		IDsService<Contacts_Ds, Contacts_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<Contacts_Ds, Contacts_Ds, Object> builder, List<Contacts_Ds> result) throws Exception {
		super.postFind(builder, result);

		this.fullName(result);
	}

	@Override
	protected void preInsert(List<Contacts_Ds> list, Object params) throws Exception {
		super.preInsert(list, params);
		this.fullName(list);
	}

	@Override
	protected void preUpdate(List<Contacts_Ds> list, Object params) throws Exception {
		super.preUpdate(list, params);
		this.fullName(list);
	}

	private void fullName(List<Contacts_Ds> result) {
		for (Contacts_Ds ds : result) {
			String fullName = "";

			if (ds.getFirstName() != null || ds.getLastName() != null) {
				fullName = ds.getFirstName() + " " + ds.getLastName();
			}

			ds.setFullName(fullName);
		}
	}

}
