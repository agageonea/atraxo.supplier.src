/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.exchangerate.service;

import java.util.List;

import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.presenter.impl.exchangerate.model.ExchangeRateToFinancialSourceAndAvgFiltered_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ExchangeRateToFinancialSourceAndAvgFiltered_DsService extends
		AbstractEntityDsService<ExchangeRateToFinancialSourceAndAvgFiltered_Ds, ExchangeRateToFinancialSourceAndAvgFiltered_Ds, Object, ExchangeRate>
		implements IDsService<ExchangeRateToFinancialSourceAndAvgFiltered_Ds, ExchangeRateToFinancialSourceAndAvgFiltered_Ds, Object> {

	@Override
	public List<ExchangeRateToFinancialSourceAndAvgFiltered_Ds> find(
			IQueryBuilder<ExchangeRateToFinancialSourceAndAvgFiltered_Ds, ExchangeRateToFinancialSourceAndAvgFiltered_Ds, Object> builder,
			List<String> fieldNames) throws Exception {

		int currencyId = builder.getFilter().getCurrencyId();
		builder.getFilter().setFromCurrencyId(currencyId);
		builder.getFilter().setToCurrencyId(null);
		builder.getFilter().setCurrencyId(null);
		List<ExchangeRateToFinancialSourceAndAvgFiltered_Ds> result = super.find(builder, fieldNames);

		builder.getFilter().setToCurrencyId(currencyId);
		builder.getFilter().setFromCurrencyId(null);
		result.addAll(super.find(builder, fieldNames));

		return result;
	}
}
