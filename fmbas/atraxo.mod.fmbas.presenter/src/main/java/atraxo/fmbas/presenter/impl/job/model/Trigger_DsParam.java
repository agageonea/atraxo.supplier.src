/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.job.model;

import atraxo.fmbas.domain.impl.fmbas_type.DaysOfMonth;
import atraxo.fmbas.domain.impl.fmbas_type.DaysOfWeek;
import atraxo.fmbas.domain.impl.fmbas_type.Months;
import atraxo.fmbas.domain.impl.fmbas_type.WeekOfMonth;
/**
 * Generated code. Do not modify in this file.
 */
public class Trigger_DsParam {

	public static final String f_daysOfMonthCombo = "daysOfMonthCombo";
	public static final String f_daysOfWeekCombo = "daysOfWeekCombo";
	public static final String f_monthsCombo = "monthsCombo";
	public static final String f_weekOfMonthCombo = "weekOfMonthCombo";

	private DaysOfMonth daysOfMonthCombo;

	private DaysOfWeek daysOfWeekCombo;

	private Months monthsCombo;

	private WeekOfMonth weekOfMonthCombo;

	public DaysOfMonth getDaysOfMonthCombo() {
		return this.daysOfMonthCombo;
	}

	public void setDaysOfMonthCombo(DaysOfMonth daysOfMonthCombo) {
		this.daysOfMonthCombo = daysOfMonthCombo;
	}

	public DaysOfWeek getDaysOfWeekCombo() {
		return this.daysOfWeekCombo;
	}

	public void setDaysOfWeekCombo(DaysOfWeek daysOfWeekCombo) {
		this.daysOfWeekCombo = daysOfWeekCombo;
	}

	public Months getMonthsCombo() {
		return this.monthsCombo;
	}

	public void setMonthsCombo(Months monthsCombo) {
		this.monthsCombo = monthsCombo;
	}

	public WeekOfMonth getWeekOfMonthCombo() {
		return this.weekOfMonthCombo;
	}

	public void setWeekOfMonthCombo(WeekOfMonth weekOfMonthCombo) {
		this.weekOfMonthCombo = weekOfMonthCombo;
	}
}
