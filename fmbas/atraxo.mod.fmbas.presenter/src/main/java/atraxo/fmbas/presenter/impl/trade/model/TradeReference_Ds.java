/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.trade.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.trade.TradeReference;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;
import seava.j4e.api.model.IModelWithClientId;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = TradeReference.class, sort = {@SortField(field = TradeReference_Ds.F_COMPANYNAME)})
@RefLookups({@RefLookup(refId = TradeReference_Ds.F_COMPANYID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = TradeReference_Ds.F_COMPANYCODE)})})
public class TradeReference_Ds extends AbstractDs_Ds<TradeReference>
		implements
			IModelWithClientId {

	public static final String ALIAS = "fmbas_TradeReference_Ds";

	public static final String F_COMPANYID = "companyId";
	public static final String F_COMPANYCODE = "companyCode";
	public static final String F_COMPANYADDRESS = "companyAddress";
	public static final String F_ACTIVE = "active";
	public static final String F_CLIENTID = "clientId";
	public static final String F_COMPANYNAME = "companyName";
	public static final String F_CONTACTPERSON = "contactPerson";
	public static final String F_CONTACTNUMBER = "contactNumber";
	public static final String F_FAX = "fax";
	public static final String F_EMAIL = "email";
	public static final String F_ISVERIFIED = "isVerified";
	public static final String F_VERIFICATIONNOTES = "verificationNotes";
	public static final String F_VERIFIEDBY = "verifiedBy";

	@DsField(join = "left", path = "company.id")
	private Integer companyId;

	@DsField(join = "left", path = "company.code")
	private String companyCode;

	@DsField
	private String companyAddress;

	@DsField
	private Boolean active;

	@DsField
	private String clientId;

	@DsField
	private String companyName;

	@DsField
	private String contactPerson;

	@DsField
	private String contactNumber;

	@DsField
	private String fax;

	@DsField
	private String email;

	@DsField
	private Boolean isVerified;

	@DsField
	private String verificationNotes;

	@DsField
	private String verifiedBy;

	/**
	 * Default constructor
	 */
	public TradeReference_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public TradeReference_Ds(TradeReference e) {
		super(e);
	}

	public Integer getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getCompanyCode() {
		return this.companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyAddress() {
		return this.companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getContactPerson() {
		return this.contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactNumber() {
		return this.contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getIsVerified() {
		return this.isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public String getVerificationNotes() {
		return this.verificationNotes;
	}

	public void setVerificationNotes(String verificationNotes) {
		this.verificationNotes = verificationNotes;
	}

	public String getVerifiedBy() {
		return this.verifiedBy;
	}

	public void setVerifiedBy(String verifiedBy) {
		this.verifiedBy = verifiedBy;
	}
}
