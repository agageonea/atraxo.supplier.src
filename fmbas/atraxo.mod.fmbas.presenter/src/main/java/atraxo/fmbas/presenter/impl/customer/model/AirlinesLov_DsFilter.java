/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;
import atraxo.fmbas.presenter.impl.customer.model.AirlinesLov_Ds;

/**
 * Helper filter object to run query by example with range values.
 *
 * Generated code. Do not modify in this file.
 */
public class AirlinesLov_DsFilter extends AirlinesLov_Ds {
}
