/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.profile.asgn.qb;

import atraxo.fmbas.presenter.impl.profile.asgn.model.Bankaccount_Customer_Asgn;
import atraxo.fmbas.presenter.impl.profile.asgn.model.Bankaccount_Customer_AsgnParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

public class Bankaccount_Customer_AsgnQbLeft
		extends
			QueryBuilderWithJpql<Bankaccount_Customer_Asgn, Bankaccount_Customer_Asgn, Bankaccount_Customer_AsgnParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getCustomerId() != null
				&& !"".equals(this.params.getCustomerId())) {
			addFilterCondition("  e.customers.id = :customerId ");
			addCustomFilterItem("customerId", this.params.getCustomerId());
		}
	}
}
