/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.profile.model;

import atraxo.fmbas.domain.impl.profile.CompanyProfile;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = CompanyProfile.class)
public class CompanyProfile_Ds extends AbstractDs_Ds<CompanyProfile> {

	public static final String ALIAS = "fmbas_CompanyProfile_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_LABELFIELD = "labelField";
	public static final String F_SYSCURRENCY = "sysCurrency";
	public static final String F_SYSUNITS = "sysUnits";
	public static final String F_SYSFINANCIALSOURCE = "sysFinancialSource";

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField(fetch = false)
	private String labelField;

	@DsField(fetch = false)
	private String sysCurrency;

	@DsField(fetch = false)
	private String sysUnits;

	@DsField(fetch = false)
	private String sysFinancialSource;

	/**
	 * Default constructor
	 */
	public CompanyProfile_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public CompanyProfile_Ds(CompanyProfile e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}

	public String getSysCurrency() {
		return this.sysCurrency;
	}

	public void setSysCurrency(String sysCurrency) {
		this.sysCurrency = sysCurrency;
	}

	public String getSysUnits() {
		return this.sysUnits;
	}

	public void setSysUnits(String sysUnits) {
		this.sysUnits = sysUnits;
	}

	public String getSysFinancialSource() {
		return this.sysFinancialSource;
	}

	public void setSysFinancialSource(String sysFinancialSource) {
		this.sysFinancialSource = sysFinancialSource;
	}
}
