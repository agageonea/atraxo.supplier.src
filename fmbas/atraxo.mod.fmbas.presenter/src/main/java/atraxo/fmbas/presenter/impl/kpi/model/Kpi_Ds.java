/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.kpi.model;

import atraxo.ad.domain.impl.security.MenuItem;
import atraxo.fmbas.domain.impl.fmbas_type.glyphFont;
import atraxo.fmbas.domain.impl.kpi.Kpi;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Kpi.class)
@RefLookups({@RefLookup(refId = Kpi_Ds.F_FRAMEID, namedQuery = MenuItem.NQ_FIND_BY_TITLE, params = {@Param(name = "title", field = Kpi_Ds.F_FRAMETITLE)})})
public class Kpi_Ds extends AbstractDs_Ds<Kpi> {

	public static final String ALIAS = "fmbas_Kpi_Ds";

	public static final String F_GLYPHCODE = "glyphCode";
	public static final String F_GLYPHFONT = "glyphFont";
	public static final String F_TITLE = "title";
	public static final String F_TITLEEN = "titleEn";
	public static final String F_TITLEDE = "titleDe";
	public static final String F_TITLEJP = "titleJp";
	public static final String F_TITLEES = "titleEs";
	public static final String F_TITLEFR = "titleFr";
	public static final String F_TITLEAR = "titleAr";
	public static final String F_ACTIVE = "active";
	public static final String F_DOWNLIMIT = "downLimit";
	public static final String F_UPLIMIT = "upLimit";
	public static final String F_DESCRIPTION = "description";
	public static final String F_CREATEDBY = "createdBy";
	public static final String F_DSNAME = "dsName";
	public static final String F_METHODNAME = "methodName";
	public static final String F_REVERSE = "reverse";
	public static final String F_UNIT = "unit";
	public static final String F_KPIORDER = "kpiOrder";
	public static final String F_FRAMEID = "frameId";
	public static final String F_FRAMETITLE = "frameTitle";
	public static final String F_FRAMEBUNDLE = "frameBundle";
	public static final String F_FRAMENAME = "frameName";

	@DsField
	private String glyphCode;

	@DsField
	private glyphFont glyphFont;

	@DsField
	private String title;

	@DsField
	private String titleEn;

	@DsField
	private String titleDe;

	@DsField
	private String titleJp;

	@DsField
	private String titleEs;

	@DsField
	private String titleFr;

	@DsField
	private String titleAr;

	@DsField
	private Boolean active;

	@DsField
	private Integer downLimit;

	@DsField
	private Integer upLimit;

	@DsField
	private String description;

	@DsField
	private String createdBy;

	@DsField
	private String dsName;

	@DsField
	private String methodName;

	@DsField
	private Boolean reverse;

	@DsField
	private String unit;

	@DsField
	private Integer kpiOrder;

	@DsField(join = "left", path = "menuItem.id")
	private String frameId;

	@DsField(join = "left", path = "menuItem.title")
	private String frameTitle;

	@DsField(join = "left", path = "menuItem.bundle")
	private String frameBundle;

	@DsField(join = "left", path = "menuItem.frame")
	private String frameName;

	/**
	 * Default constructor
	 */
	public Kpi_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Kpi_Ds(Kpi e) {
		super(e);
	}

	public String getGlyphCode() {
		return this.glyphCode;
	}

	public void setGlyphCode(String glyphCode) {
		this.glyphCode = glyphCode;
	}

	public glyphFont getGlyphFont() {
		return this.glyphFont;
	}

	public void setGlyphFont(glyphFont glyphFont) {
		this.glyphFont = glyphFont;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleEn() {
		return this.titleEn;
	}

	public void setTitleEn(String titleEn) {
		this.titleEn = titleEn;
	}

	public String getTitleDe() {
		return this.titleDe;
	}

	public void setTitleDe(String titleDe) {
		this.titleDe = titleDe;
	}

	public String getTitleJp() {
		return this.titleJp;
	}

	public void setTitleJp(String titleJp) {
		this.titleJp = titleJp;
	}

	public String getTitleEs() {
		return this.titleEs;
	}

	public void setTitleEs(String titleEs) {
		this.titleEs = titleEs;
	}

	public String getTitleFr() {
		return this.titleFr;
	}

	public void setTitleFr(String titleFr) {
		this.titleFr = titleFr;
	}

	public String getTitleAr() {
		return this.titleAr;
	}

	public void setTitleAr(String titleAr) {
		this.titleAr = titleAr;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getDownLimit() {
		return this.downLimit;
	}

	public void setDownLimit(Integer downLimit) {
		this.downLimit = downLimit;
	}

	public Integer getUpLimit() {
		return this.upLimit;
	}

	public void setUpLimit(Integer upLimit) {
		this.upLimit = upLimit;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getDsName() {
		return this.dsName;
	}

	public void setDsName(String dsName) {
		this.dsName = dsName;
	}

	public String getMethodName() {
		return this.methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Boolean getReverse() {
		return this.reverse;
	}

	public void setReverse(Boolean reverse) {
		this.reverse = reverse;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getKpiOrder() {
		return this.kpiOrder;
	}

	public void setKpiOrder(Integer kpiOrder) {
		this.kpiOrder = kpiOrder;
	}

	public String getFrameId() {
		return this.frameId;
	}

	public void setFrameId(String frameId) {
		this.frameId = frameId;
	}

	public String getFrameTitle() {
		return this.frameTitle;
	}

	public void setFrameTitle(String frameTitle) {
		this.frameTitle = frameTitle;
	}

	public String getFrameBundle() {
		return this.frameBundle;
	}

	public void setFrameBundle(String frameBundle) {
		this.frameBundle = frameBundle;
	}

	public String getFrameName() {
		return this.frameName;
	}

	public void setFrameName(String frameName) {
		this.frameName = frameName;
	}
}
