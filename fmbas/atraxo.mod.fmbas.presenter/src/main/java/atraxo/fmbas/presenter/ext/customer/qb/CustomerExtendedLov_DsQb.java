/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.fmbas.presenter.ext.customer.qb;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.presenter.impl.customer.model.CustomerExtendedLov_Ds;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

/**
 * Custom Query Builder CustomerExtendedLov_DsQb
 */
public class CustomerExtendedLov_DsQb extends QueryBuilderWithJpql<CustomerExtendedLov_Ds, CustomerExtendedLov_Ds, Object> {

	private final static Logger LOG = LoggerFactory.getLogger(CustomerExtendedLov_DsQb.class);

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();

		String clientId = Session.user.get().getClientId();
		Object object = true;
		try {
			object = this.getEntityManager()
					.createNativeQuery(
							"select e.value from BAS_SYSTEM_PARAMETERS e where e.clientid ='" + clientId + "' and e.code='RESTRICTBLOCKED'")
					.getSingleResult();
		} catch (NoResultException | NonUniqueResultException e) {
			LOG.warn("The RESTRICTBLACKLISTED system parameter is missing or duplicate: " + e.getMessage(), e);
		}
		boolean restricted = Boolean.parseBoolean(object.toString());
		String query = ":status1" + (restricted ? "" : ", :status2");
		this.addFilterCondition("e.status in (" + query + ")");
		this.addCustomFilterItem("status1", CustomerStatus._ACTIVE_);
		if (!restricted) {
			this.addCustomFilterItem("status2", CustomerStatus._BLOCKED_);
		}
	}
}
