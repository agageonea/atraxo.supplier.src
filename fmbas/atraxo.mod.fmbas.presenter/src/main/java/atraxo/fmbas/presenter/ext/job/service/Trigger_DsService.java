/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.job.service;

import java.util.Arrays;
import java.util.List;

import atraxo.fmbas.domain.impl.fmbas_type.DaysOfWeek;
import atraxo.fmbas.domain.impl.fmbas_type.TimeDuration;
import atraxo.fmbas.domain.impl.fmbas_type.TimeInterval;
import atraxo.fmbas.domain.impl.job.Trigger;
import atraxo.fmbas.presenter.impl.job.model.Trigger_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class Trigger_DsService extends AbstractEntityDsService<Trigger_Ds, Trigger_Ds, Object, Trigger>
		implements IDsService<Trigger_Ds, Trigger_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<Trigger_Ds, Trigger_Ds, Object> builder, List<Trigger_Ds> result) throws Exception {
		super.postFind(builder, result);

		for (Trigger_Ds trigger_Ds : result) {
			if (!TimeDuration._EMPTY_.equals(trigger_Ds.getTimeout())) {
				trigger_Ds.setTimeoutChk(true);
			}
			if (!TimeInterval._EMPTY_.equals(trigger_Ds.getRepeatInterval())) {
				trigger_Ds.setRepChk(true);
			}
			List<String> selectedWeekDays = Arrays.asList(trigger_Ds.getDaysOfWeek().split(","));
			for (String weekDay : selectedWeekDays) {
				if (weekDay.length() == 0) {
					continue;
				}
				DaysOfWeek day = DaysOfWeek.getByName(weekDay.toUpperCase());
				switch (day) {
				case _MONDAY_:
					trigger_Ds.setMonday(true);
					break;
				case _TUESDAY_:
					trigger_Ds.setTuesday(true);
					break;
				case _WEDNESDAY_:
					trigger_Ds.setWednesday(true);
					break;
				case _THURSDAY_:
					trigger_Ds.setThursday(true);
					break;
				case _FRIDAY_:
					trigger_Ds.setFriday(true);
					break;
				case _SATURDAY_:
					trigger_Ds.setSaturday(true);
					break;
				case _SUNDAY_:
					trigger_Ds.setSunday(true);
					break;
				}
			}
		}
	}

}
