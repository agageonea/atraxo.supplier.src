/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

/**
 * Generated code. Do not modify in this file.
 */
public class CustomersMp_DsParam {

	public static final String f_remark = "remark";
	public static final String f_contactFirstName = "contactFirstName";
	public static final String f_contactLastName = "contactLastName";
	public static final String f_contactMobilePhone = "contactMobilePhone";
	public static final String f_contactEmail = "contactEmail";
	public static final String f_customerLocationId = "customerLocationId";

	private String remark;

	private String contactFirstName;

	private String contactLastName;

	private String contactMobilePhone;

	private String contactEmail;

	private Integer customerLocationId;

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getContactFirstName() {
		return this.contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return this.contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getContactMobilePhone() {
		return this.contactMobilePhone;
	}

	public void setContactMobilePhone(String contactMobilePhone) {
		this.contactMobilePhone = contactMobilePhone;
	}

	public String getContactEmail() {
		return this.contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public Integer getCustomerLocationId() {
		return this.customerLocationId;
	}

	public void setCustomerLocationId(Integer customerLocationId) {
		this.customerLocationId = customerLocationId;
	}
}
