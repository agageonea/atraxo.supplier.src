/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerType;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;
import seava.j4e.api.model.IModelWithId;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Customer.class, sort = {@SortField(field = CustomerGroupLov_Ds.F_CODE)})
public class CustomerGroupLov_Ds extends AbstractLov_Ds<Customer>
		implements
			IModelWithId<Integer> {

	public static final String ALIAS = "fmbas_CustomerGroupLov_Ds";

	public static final String F_ID = "id";
	public static final String F_CODE = "code";
	public static final String F_TYPE = "type";
	public static final String F_ISCUSTOMER = "isCustomer";
	public static final String F_PARENTGROUPID = "parentGroupId";
	public static final String F_PARENTGROUPCODE = "parentGroupCode";

	@DsField
	private Integer id;

	@DsField
	private String code;

	@DsField
	private CustomerType type;

	@DsField
	private Boolean isCustomer;

	@DsField(join = "left", path = "parent.id")
	private Integer parentGroupId;

	@DsField(join = "left", path = "parent.code")
	private String parentGroupCode;

	/**
	 * Default constructor
	 */
	public CustomerGroupLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public CustomerGroupLov_Ds(Customer e) {
		super(e);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public CustomerType getType() {
		return this.type;
	}

	public void setType(CustomerType type) {
		this.type = type;
	}

	public Boolean getIsCustomer() {
		return this.isCustomer;
	}

	public void setIsCustomer(Boolean isCustomer) {
		this.isCustomer = isCustomer;
	}

	public Integer getParentGroupId() {
		return this.parentGroupId;
	}

	public void setParentGroupId(Integer parentGroupId) {
		this.parentGroupId = parentGroupId;
	}

	public String getParentGroupCode() {
		return this.parentGroupCode;
	}

	public void setParentGroupCode(String parentGroupCode) {
		this.parentGroupCode = parentGroupCode;
	}
}
