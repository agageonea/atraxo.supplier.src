/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.categories.model;

import atraxo.fmbas.domain.impl.categories.MainCategory;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = MainCategory.class, sort = {@SortField(field = MainCategories_Ds.F_CODE)})
public class MainCategories_Ds extends AbstractDs_Ds<MainCategory> {

	public static final String ALIAS = "fmbas_MainCategories_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_ISSYS = "isSys";
	public static final String F_ISPROD = "isProd";
	public static final String F_ACTIVE = "active";

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private Boolean isSys;

	@DsField
	private Boolean isProd;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public MainCategories_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public MainCategories_Ds(MainCategory e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsSys() {
		return this.isSys;
	}

	public void setIsSys(Boolean isSys) {
		this.isSys = isSys;
	}

	public Boolean getIsProd() {
		return this.isProd;
	}

	public void setIsProd(Boolean isProd) {
		this.isProd = isProd;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
