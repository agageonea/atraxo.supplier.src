/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.categories.model;

import atraxo.fmbas.domain.impl.categories.MainCategory;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = MainCategory.class, sort = {@SortField(field = MainCategoriesLov_Ds.F_CODE)})
public class MainCategoriesLov_Ds extends AbstractLov_Ds<MainCategory> {

	public static final String ALIAS = "fmbas_MainCategoriesLov_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";

	@DsField
	private String code;

	@DsField
	private String name;

	/**
	 * Default constructor
	 */
	public MainCategoriesLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public MainCategoriesLov_Ds(MainCategory e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
