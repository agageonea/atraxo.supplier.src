/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.qb;

import atraxo.fmbas.presenter.impl.customer.model.Customers_Ds;
import atraxo.fmbas.presenter.impl.customer.model.Customers_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class Customers_DsQb
		extends
			QueryBuilderWithJpql<Customers_Ds, Customers_Ds, Customers_DsParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getFinalCustId() != null
				&& !"".equals(this.params.getFinalCustId())) {
			addFilterCondition("   e.id in ( select cu.id from Customer cu, IN (cu.finalCustomer) c where c.id = :finalCustId ) ");
			addCustomFilterItem("finalCustId", this.params.getFinalCustId());
		}
	}
}
