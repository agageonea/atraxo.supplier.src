package atraxo.fmbas.presenter.ext.customer.delegate;

import java.util.ArrayList;
import java.util.List;

import atraxo.fmbas.business.api.customer.ICustomerLocationsService;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.customer.CustomerLocations;
import atraxo.fmbas.presenter.impl.customer.model.CustomerLocations_Ds;
import atraxo.fmbas.presenter.impl.customer.model.CustomerLocations_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class CustomerLocations_Pd extends AbstractPresenterDelegate {

	public void removeList(List<CustomerLocations_Ds> locDsList, CustomerLocations_DsParam params) throws Exception {
		ICustomerLocationsService locService = (ICustomerLocationsService) this.findEntityService(CustomerLocations.class);
		ICustomerService custService = (ICustomerService) this.findEntityService(Customer.class);

		// find customer and load locations collection
		Customer customer = custService.findByIdFetchLocations(params.getCustomerId());

		// create list of selected locations from ds
		List<CustomerLocations> locations = new ArrayList<>();
		for (CustomerLocations_Ds locationDs : locDsList) {
			locations.add(locService.findById(locationDs.getId()));
		}

		// remove selected locations and update the customer and his locations collection
		customer.getLocations().removeAll(locations);
		custService.update(customer);
	}

}
