package atraxo.fmbas.presenter.ext.templates.delegate;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import atraxo.fmbas.business.api.templates.ITemplateService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.templates.Template;
import seava.j4e.api.descriptor.IUploadedFileDescriptor;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.IFileUploadService;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class UploadTemplate extends AbstractPresenterDelegate implements IFileUploadService {

	private static final String DESCRIPTION = "description";
	private static final String TYPE_NAME = "typeName";
	private static final String NAME = "name";
	private static final String IS_UPDATE = "isUpdate";
	private static final String TEMPLATE_ID = "templateId";
	private static final String SYSTEM_USE = "systemUse";
	private List<String> paramNames;

	@Override
	public List<String> getParamNames() {
		this.paramNames = new ArrayList<>();
		this.paramNames.add(NAME);
		this.paramNames.add(TYPE_NAME);
		this.paramNames.add(DESCRIPTION);
		this.paramNames.add(IS_UPDATE);
		this.paramNames.add(TEMPLATE_ID);
		this.paramNames.add(SYSTEM_USE);
		return this.paramNames;
	}

	@Override
	public Map<String, Object> execute(IUploadedFileDescriptor fileDescriptor, InputStream inputStream, Map<String, String> params) throws Exception {
		ITemplateService templateService = (ITemplateService) this.findEntityService(Template.class);
		Map<String, Object> result = new HashMap<String, Object>();
		byte[] file = IOUtils.toByteArray(inputStream);
		String description = params.get(UploadTemplate.DESCRIPTION);
		String name = params.get(UploadTemplate.NAME);
		String typeName = params.get(UploadTemplate.TYPE_NAME);
		String isUpdateStr = params.get(IS_UPDATE);
		Boolean isUpdate = new Boolean(isUpdateStr);
		Boolean systemUse = params.get(SYSTEM_USE).equals("1") ? true : false;
		String filename = fileDescriptor.getOriginalName();
		String fileExtension = this.getExtension(filename);
		if ("XLSX".equalsIgnoreCase(fileExtension) || "DOCX".equalsIgnoreCase(fileExtension) || "TXT".equalsIgnoreCase(fileExtension)
				|| "HTML".equalsIgnoreCase(fileExtension)) {
			if (isUpdate) {
				String templateIdStr = params.get(TEMPLATE_ID);
				Integer templateId = Integer.parseInt(templateIdStr);
				Template template = templateService.findById(templateId);
				templateService.updateTemplate(file, filename, template, name, description, systemUse);
			} else {
				templateService.insertTemplate(file, filename, description, name, typeName, systemUse);
			}
		} else {
			throw new BusinessException(BusinessErrorCode.WRONG_FILE_FORMAT, BusinessErrorCode.WRONG_FILE_FORMAT.getErrMsg());
		}
		return result;
	}

	private String getExtension(String fileName) {
		return FilenameUtils.getExtension(fileName);
	}
}
