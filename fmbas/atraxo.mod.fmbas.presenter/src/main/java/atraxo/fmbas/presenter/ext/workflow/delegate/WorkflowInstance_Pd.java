package atraxo.fmbas.presenter.ext.workflow.delegate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceService;
import atraxo.fmbas.business.api.workflow.IWorkflowService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.util.LambdaUtil;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowInstanceStatus;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.presenter.impl.workflow.model.WorkflowInstance_Ds;
import atraxo.fmbas.presenter.impl.workflow.model.WorkflowInstance_DsParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author vhojda
 */
public class WorkflowInstance_Pd extends AbstractPresenterDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowInstance_Pd.class);

	private static final String WORKFLOW_TERMINATE_OPTION_SELECTED = "selected";
	private static final String WORKFLOW_TERMINATE_OPTION_ALL = "all";

	/**
	 * @param wkf
	 * @throws Exception
	 */
	public void terminateInstances(List<WorkflowInstance_Ds> instanceDsList, WorkflowInstance_DsParam param) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START terminateInstances()");
		}

		IWorkflowService workflowSrv = this.getApplicationContext().getBean(IWorkflowService.class);
		Workflow wkf = workflowSrv.findByBusiness(instanceDsList.get(0).getWorkflowId());

		String terminationReason = String.format(BusinessErrorCode.INSTANCE_TERMINATION_RESON.getErrMsg(), Session.user.get().getLoginName());
		List<Integer> wkfInstanceIdList = new ArrayList<>();
		String option = param.getWfkTerminateOption();
		if (option.equals(WORKFLOW_TERMINATE_OPTION_SELECTED)) {
			for (WorkflowInstance_Ds instanceDs : instanceDsList) {
				wkfInstanceIdList.add(instanceDs.getId());
			}
		} else if (option.equals(WORKFLOW_TERMINATE_OPTION_ALL)) {
			wkfInstanceIdList = this.getWorkflowInstancesIdsForTerminateOptionAll(wkf);
		} else {
			LOGGER.error("ERROR:could not read export option for " + option + " !");
			param.setTerminateWfkInstancesResult(false);
			param.setTerminateWkfInstancesDescription(BusinessErrorCode.INTERFACE_EXPORT_CUSTOMERS_INCORRECT_OPTION.getErrMsg());
		}
		if (!wkfInstanceIdList.isEmpty()) {
			IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean("workflowBpmManager");
			Integer terminatedCount = workflowBpmManager.terminateWorkflow(wkfInstanceIdList, terminationReason);
			if (terminatedCount != wkfInstanceIdList.size()) {
				String description = String.format(BusinessErrorCode.INSTANCE_TERMINATION_FAILURE_RESULT.getErrMsg(),
						wkfInstanceIdList.size() - terminatedCount);
				param.setTerminateWfkInstancesResult(false);
				param.setTerminateWkfInstancesDescription(description);
			} else {
				param.setTerminateWfkInstancesResult(true);
			}
		} else {
			param.setTerminateWfkInstancesResult(true);
		}
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END terminateInstances()");
		}
	}

	private List<Integer> getWorkflowInstancesIdsForTerminateOptionAll(Workflow wkf) throws BusinessException {
		IWorkflowInstanceService workflowInstanceSrv = this.getApplicationContext().getBean(IWorkflowInstanceService.class);
		List<Integer> wkfInstanceIdList = new ArrayList<>();

		Map<String, Object> params = new HashMap<>();
		params.put("workflow", wkf);
		params.put("status", LambdaUtil.filterEnum(WorkflowInstanceStatus.class,
				c -> c.equals(WorkflowInstanceStatus._IN_PROGRESS_) || c.equals(WorkflowInstanceStatus._FAILURE_)));
		List<WorkflowInstance> wkfInstanceList = workflowInstanceSrv.findEntitiesByAttributes(params);
		for (WorkflowInstance instance : wkfInstanceList) {
			wkfInstanceIdList.add(instance.getId());
		}
		return wkfInstanceIdList;
	}

	/**
	 * @param wkf
	 * @throws BusinessException
	 */
	public void resumeInstance(WorkflowInstance_Ds instanceDs) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START resumeInstance()");
		}

		IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean("workflowBpmManager");
		workflowBpmManager.resumeWorkflow(instanceDs.getId());

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END resumeInstance()");
		}
	}

}
