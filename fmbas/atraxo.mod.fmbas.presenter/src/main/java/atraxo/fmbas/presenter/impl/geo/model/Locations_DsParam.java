/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.geo.model;

/**
 * Generated code. Do not modify in this file.
 */
public class Locations_DsParam {

	public static final String f_areaId = "areaId";

	private Integer areaId;

	public Integer getAreaId() {
		return this.areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}
}
