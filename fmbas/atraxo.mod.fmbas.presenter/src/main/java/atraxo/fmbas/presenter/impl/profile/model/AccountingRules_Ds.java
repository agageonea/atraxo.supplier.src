/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.profile.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.AccountingRulesType;
import atraxo.fmbas.domain.impl.profile.AccountingRules;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = AccountingRules.class)
@RefLookups({@RefLookup(refId = AccountingRules_Ds.F_SUBSIDIARYID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = AccountingRules_Ds.F_SUBSIDIARYCODE)})})
public class AccountingRules_Ds extends AbstractDs_Ds<AccountingRules> {

	public static final String ALIAS = "fmbas_AccountingRules_Ds";

	public static final String F_RULETYPE = "ruleType";
	public static final String F_SUBSIDIARYID = "subsidiaryId";
	public static final String F_SUBSIDIARYCODE = "subsidiaryCode";

	@DsField
	private AccountingRulesType ruleType;

	@DsField(join = "left", path = "subsidiary.id")
	private Integer subsidiaryId;

	@DsField(join = "left", path = "subsidiary.code")
	private String subsidiaryCode;

	/**
	 * Default constructor
	 */
	public AccountingRules_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AccountingRules_Ds(AccountingRules e) {
		super(e);
	}

	public AccountingRulesType getRuleType() {
		return this.ruleType;
	}

	public void setRuleType(AccountingRulesType ruleType) {
		this.ruleType = ruleType;
	}

	public Integer getSubsidiaryId() {
		return this.subsidiaryId;
	}

	public void setSubsidiaryId(Integer subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}

	public String getSubsidiaryCode() {
		return this.subsidiaryCode;
	}

	public void setSubsidiaryCode(String subsidiaryCode) {
		this.subsidiaryCode = subsidiaryCode;
	}
}
