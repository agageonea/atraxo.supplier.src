/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.dashboard.model;

/**
 * Generated code. Do not modify in this file.
 */
public class DashboardWidget_DsParam {

	public static final String f_rpcResponse = "rpcResponse";

	private String rpcResponse;

	public String getRpcResponse() {
		return this.rpcResponse;
	}

	public void setRpcResponse(String rpcResponse) {
		this.rpcResponse = rpcResponse;
	}
}
