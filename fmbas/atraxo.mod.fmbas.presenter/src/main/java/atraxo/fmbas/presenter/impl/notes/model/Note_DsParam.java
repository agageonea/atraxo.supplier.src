/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.notes.model;

import java.util.Date;
/**
 * Generated code. Do not modify in this file.
 */
public class Note_DsParam {

	public static final String f_createdAfter = "createdAfter";
	public static final String f_createdBefore = "createdBefore";

	private Date createdAfter;

	private Date createdBefore;

	public Date getCreatedAfter() {
		return this.createdAfter;
	}

	public void setCreatedAfter(Date createdAfter) {
		this.createdAfter = createdAfter;
	}

	public Date getCreatedBefore() {
		return this.createdBefore;
	}

	public void setCreatedBefore(Date createdBefore) {
		this.createdBefore = createdBefore;
	}
}
