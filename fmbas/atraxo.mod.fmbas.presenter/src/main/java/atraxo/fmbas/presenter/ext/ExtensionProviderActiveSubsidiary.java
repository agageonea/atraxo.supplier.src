package atraxo.fmbas.presenter.ext;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.extensions.IExtensionContentProvider;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterBaseService;
import atraxo.ad.domain.impl.security.User;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.user.IUserSubsidiaryService;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;

/**
 * @author zspeter
 */
public class ExtensionProviderActiveSubsidiary extends AbstractPresenterBaseService implements IExtensionContentProvider {

	private static final String SYS_VOL = "sysvol";
	private static final String SYS_WEIGHT = "sysweight";
	private static final String SYS_CURRENCY = "syscrncy";

	@Autowired
	private ISystemParameterService paramService;

	@Autowired
	private IUserSubsidiaryService userSubsidiaryService;

	@Override
	public String getContent(String targetFrame) throws Exception {

		List<UserSubsidiary> userSubsidiaries = this.getUserSubsidiaries();
		return this.addDefaultElements(userSubsidiaries);
	}

	protected String addDefaultElements(List<UserSubsidiary> userSubsidiaries) throws BusinessException {

		StringBuilder sb = new StringBuilder();

		sb.append("_ACTIVESUBSIDIARY_ = { ");

		if (!userSubsidiaries.isEmpty()) {
			for (UserSubsidiary us : userSubsidiaries) {
				sb.append("code:'").append(us.getSubsidiary().getCode()).append("',");
				sb.append("name:'").append(us.getSubsidiary().getName()).append("',");
				sb.append("id:'").append(us.getSubsidiary().getId()).append("',");
				if (us.getSubsidiary().getDefaultVolumeUnit() != null) {
					sb.append("volumeUnitCode:'").append(us.getSubsidiary().getDefaultVolumeUnit().getCode()).append("',");
				} else {
					this.setVolumeUnit(sb, this.getSystemParameter(SYS_VOL));
				}

				if (us.getSubsidiary().getDefaultWeightUnit() != null) {
					sb.append("weightUnitCode:'").append(us.getSubsidiary().getDefaultWeightUnit().getCode()).append("',");
				} else {
					this.setWeightUnit(sb, this.getSystemParameter(SYS_WEIGHT));
				}

				if (us.getSubsidiary().getSubsidiaryCurrency() != null) {
					sb.append("currencyUnitCode:'").append(us.getSubsidiary().getSubsidiaryCurrency().getCode()).append("',");
				} else {
					this.setCurrencyUnit(sb, this.getSystemParameter(SYS_CURRENCY));
				}
			}
		} else {
			this.setUnits(sb);
		}

		sb.append("};");
		int ind = sb.lastIndexOf(",");
		if (ind > 0) {
			sb.replace(ind, ind + 1, "");
		}

		return sb.toString();
	}

	private void setUnits(StringBuilder sb) throws BusinessException {
		List<SystemParameter> params = this.getSystemParameters();
		for (SystemParameter param : params) {
			switch (param.getCode().toLowerCase()) {
			case SYS_VOL:
				this.setVolumeUnit(sb, param);
				break;
			case SYS_WEIGHT:
				this.setWeightUnit(sb, param);
				break;
			case SYS_CURRENCY:
				this.setCurrencyUnit(sb, param);
				break;
			default:
				break;
			}
		}
	}

	private void setVolumeUnit(StringBuilder sb, SystemParameter param) throws BusinessException {
		sb.append("volumeUnitCode:'").append(param.getValue()).append("',");
	}

	private void setWeightUnit(StringBuilder sb, SystemParameter param) throws BusinessException {
		sb.append("weightUnitCode:'").append(param.getValue()).append("',");
	}

	private void setCurrencyUnit(StringBuilder sb, SystemParameter param) throws BusinessException {
		sb.append("currencyUnitCode:'").append(param.getValue()).append("',");
	}

	private List<UserSubsidiary> getUserSubsidiaries() throws BusinessException {

		EntityManager em = this.userSubsidiaryService.getEntityManager();

		String getUserSubsidiaries = "SELECT e FROM " + UserSubsidiary.class.getSimpleName()
				+ " e WHERE e.clientId=:clientId AND e.mainSubsidiary = true AND e.user.id IN (SELECT u.id FROM " + User.class.getSimpleName()
				+ " u WHERE u.clientId=:clientId AND u.code=:userCode)";
		TypedQuery<UserSubsidiary> userSubsidiaryQuery = em.createQuery(getUserSubsidiaries, UserSubsidiary.class);
		userSubsidiaryQuery.setParameter("clientId", Session.user.get().getClientId());
		userSubsidiaryQuery.setParameter("userCode", Session.user.get().getCode());
		return userSubsidiaryQuery.getResultList();
	}

	private List<SystemParameter> getSystemParameters() throws BusinessException {

		EntityManager em = this.paramService.getEntityManager();

		String getSystemParameters = "SELECT e FROM " + SystemParameter.class.getSimpleName() + " e WHERE e.clientId=:clientId";
		TypedQuery<SystemParameter> systemParameterQuery = em.createQuery(getSystemParameters, SystemParameter.class);
		systemParameterQuery.setParameter("clientId", Session.user.get().getClientId());

		return systemParameterQuery.getResultList();
	}

	private SystemParameter getSystemParameter(String sysParam) throws BusinessException {
		SystemParameter systemParam = null;

		List<SystemParameter> params = this.getSystemParameters();
		for (SystemParameter param : params) {
			if (sysParam.equalsIgnoreCase(param.getCode())) {
				systemParam = param;
			}
		}

		return systemParam;
	}
}
