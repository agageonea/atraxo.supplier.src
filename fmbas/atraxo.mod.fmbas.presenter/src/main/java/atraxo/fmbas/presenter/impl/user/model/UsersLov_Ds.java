/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.user.model;

import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;
import seava.j4e.api.model.IModelWithClientId;
import seava.j4e.api.model.IModelWithId;
import seava.j4e.presenter.model.AbstractDsModel;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = UserSupp.class, sort = {@SortField(field = UsersLov_Ds.F_USERCODE)})
public class UsersLov_Ds extends AbstractDsModel<UserSupp>
		implements
			IModelWithId<String>,
			IModelWithClientId {

	public static final String ALIAS = "fmbas_UsersLov_Ds";

	public static final String F_ID = "id";
	public static final String F_CLIENTID = "clientId";
	public static final String F_USERNAME = "userName";
	public static final String F_LOGINNAME = "loginName";
	public static final String F_EMAIL = "email";
	public static final String F_USERCODE = "userCode";
	public static final String F_ACTIVE = "active";

	@DsField
	private String id;

	@DsField(noUpdate = true)
	private String clientId;

	@DsField(path = "name")
	private String userName;

	@DsField
	private String loginName;

	@DsField
	private String email;

	@DsField(path = "code")
	private String userCode;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public UsersLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public UsersLov_Ds(UserSupp e) {
		super(e);
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserCode() {
		return this.userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
