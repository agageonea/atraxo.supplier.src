/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.notificationEvent.model;

import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = NotificationEvent.class)
public class NotificationEventList_Ds extends AbstractDs_Ds<NotificationEvent> {

	public static final String ALIAS = "fmbas_NotificationEventList_Ds";

	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";

	@DsField
	private String name;

	@DsField
	private String description;

	/**
	 * Default constructor
	 */
	public NotificationEventList_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public NotificationEventList_Ds(NotificationEvent e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
