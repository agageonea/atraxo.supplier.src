/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.exchangerate.service;

import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.presenter.impl.exchangerate.model.ExchangeRateCalcVal_Ds;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ExchangeRateCalcVal_DsService extends AbstractEntityDsService<ExchangeRateCalcVal_Ds, ExchangeRateCalcVal_Ds, Object, ExchangeRate>
		implements IDsService<ExchangeRateCalcVal_Ds, ExchangeRateCalcVal_Ds, Object> {

	// Implement me ...

}
