/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.timeserie.cnv;

import javax.persistence.EntityManager;

import atraxo.fmbas.domain.impl.fmbas_type.ValueType;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.presenter.impl.timeserie.model.ExchangeRateTimeSerie_Ds;
import seava.j4e.api.action.result.IDsConverter;
import seava.j4e.presenter.converter.AbstractDsConverter;

/**
 * Custom converter ExchangeRateTimeSerie_DsCnv
 */
public class ExchangeRateTimeSerie_DsCnv extends AbstractDsConverter<ExchangeRateTimeSerie_Ds, TimeSerie>
		implements IDsConverter<ExchangeRateTimeSerie_Ds, TimeSerie> {

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.presenter.converter.AbstractDsConverter#modelToEntity(java.lang.Object, java.lang.Object, boolean,
	 * javax.persistence.EntityManager)
	 */
	@Override
	public void modelToEntity(ExchangeRateTimeSerie_Ds m, TimeSerie e, boolean isInsert, EntityManager em) throws Exception {
		super.modelToEntity(m, e, isInsert, em);
		if (e.getValueType() == null) {
			e.setValueType(ValueType._USER_CALCULATED_PRICE_);
		}
	}
}
