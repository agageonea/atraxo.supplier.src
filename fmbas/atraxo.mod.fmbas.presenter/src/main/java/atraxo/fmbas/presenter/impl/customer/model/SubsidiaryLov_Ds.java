/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerType;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Customer.class, sort = {@SortField(field = SubsidiaryLov_Ds.F_CODE)})
public class SubsidiaryLov_Ds extends AbstractLov_Ds<Customer> {

	public static final String ALIAS = "fmbas_SubsidiaryLov_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_TYPE = "type";
	public static final String F_REFID = "refid";
	public static final String F_STATUS = "status";
	public static final String F_ISCUSTOMER = "isCustomer";
	public static final String F_ISSUBSIDIARY = "isSubsidiary";

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private CustomerType type;

	@DsField
	private String refid;

	@DsField
	private CustomerStatus status;

	@DsField
	private Boolean isCustomer;

	@DsField
	private Boolean isSubsidiary;

	/**
	 * Default constructor
	 */
	public SubsidiaryLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public SubsidiaryLov_Ds(Customer e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CustomerType getType() {
		return this.type;
	}

	public void setType(CustomerType type) {
		this.type = type;
	}

	public String getRefid() {
		return this.refid;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}

	public CustomerStatus getStatus() {
		return this.status;
	}

	public void setStatus(CustomerStatus status) {
		this.status = status;
	}

	public Boolean getIsCustomer() {
		return this.isCustomer;
	}

	public void setIsCustomer(Boolean isCustomer) {
		this.isCustomer = isCustomer;
	}

	public Boolean getIsSubsidiary() {
		return this.isSubsidiary;
	}

	public void setIsSubsidiary(Boolean isSubsidiary) {
		this.isSubsidiary = isSubsidiary;
	}
}
