/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.geo.asgn.qb;

import atraxo.fmbas.presenter.impl.geo.asgn.model.AreasLocations_Asgn;
import atraxo.fmbas.presenter.impl.geo.asgn.model.AreasLocations_AsgnParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

public class AreasLocations_AsgnQbRight
		extends
			QueryBuilderWithJpql<AreasLocations_Asgn, AreasLocations_Asgn, AreasLocations_AsgnParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getLocationId() != null
				&& !"".equals(this.params.getLocationId())) {
			addFilterCondition(" ");
			addCustomFilterItem("locationId", this.params.getLocationId());
		}
		if (this.params != null && this.params.getActDate() != null
				&& !"".equals(this.params.getActDate())) {
			addFilterCondition("  e.validFrom = :actDate");
			addCustomFilterItem("actDate", this.params.getActDate());
		}
	}
}
