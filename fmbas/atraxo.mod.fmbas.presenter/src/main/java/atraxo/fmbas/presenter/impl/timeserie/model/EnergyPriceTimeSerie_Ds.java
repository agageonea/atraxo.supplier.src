/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.timeserie.model;

import atraxo.fmbas.domain.impl.commodities.Commoditie;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.DataProvider;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.fmbas_type.PostTypeInd;
import atraxo.fmbas.domain.impl.fmbas_type.SerieFreqInd;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesApprovalStatus;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ValueType;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = TimeSerie.class, jpqlWhere = "e.serieType = 'E'", sort = {@SortField(field = EnergyPriceTimeSerie_Ds.F_SERIENAME)})
@RefLookups({
		@RefLookup(refId = EnergyPriceTimeSerie_Ds.F_CURRENCY1ID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = EnergyPriceTimeSerie_Ds.F_CURRENCY1CODE)}),
		@RefLookup(refId = EnergyPriceTimeSerie_Ds.F_CURRENCY2ID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = EnergyPriceTimeSerie_Ds.F_CURRENCY2CODE)}),
		@RefLookup(refId = EnergyPriceTimeSerie_Ds.F_FINANCIALSOURCE, namedQuery = FinancialSources.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = EnergyPriceTimeSerie_Ds.F_FINANCIALSOURCECODE)}),
		@RefLookup(refId = EnergyPriceTimeSerie_Ds.F_COMMODITYID, namedQuery = Commoditie.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = EnergyPriceTimeSerie_Ds.F_COMMODITYCODE)}),
		@RefLookup(refId = EnergyPriceTimeSerie_Ds.F_UNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = EnergyPriceTimeSerie_Ds.F_UNITCODE)}),
		@RefLookup(refId = EnergyPriceTimeSerie_Ds.F_UNIT2ID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = EnergyPriceTimeSerie_Ds.F_UNIT2CODE)})})
public class EnergyPriceTimeSerie_Ds extends AbstractDs_Ds<TimeSerie> {

	public static final String ALIAS = "fmbas_EnergyPriceTimeSerie_Ds";

	public static final String F_MEASURE = "measure";
	public static final String F_SERIENAME = "serieName";
	public static final String F_DESCRIPTION = "description";
	public static final String F_SERIETYPE = "serieType";
	public static final String F_SERIEFREQIND = "serieFreqInd";
	public static final String F_DECIMALS = "decimals";
	public static final String F_DATAPROVIDER = "dataProvider";
	public static final String F_EXTERNALSERIENAME = "externalSerieName";
	public static final String F_ACTIVATION = "activation";
	public static final String F_FIRSTUSAGE = "firstUsage";
	public static final String F_LASTUSAGE = "lastUsage";
	public static final String F_FACTOR = "factor";
	public static final String F_SUPPLYITEMUPDATE = "supplyItemUpdate";
	public static final String F_STATUS = "status";
	public static final String F_APPROVALSTATUS = "approvalStatus";
	public static final String F_VALUETYPE = "valueType";
	public static final String F_CURRENCY1ID = "currency1Id";
	public static final String F_CURRENCY1CODE = "currency1Code";
	public static final String F_CURRENCY2ID = "currency2Id";
	public static final String F_CURRENCY2CODE = "currency2Code";
	public static final String F_COMMODITYID = "commodityId";
	public static final String F_COMMODITYNAME = "commodityName";
	public static final String F_COMMODITYCODE = "commodityCode";
	public static final String F_POSTTYPEIND = "postTypeInd";
	public static final String F_FINANCIALSOURCE = "financialSource";
	public static final String F_FINANCIALSOURCECODE = "financialSourceCode";
	public static final String F_FINANCIALSOURCENAME = "financialSourceName";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_CONVFCTR = "convFctr";
	public static final String F_ARITHMOPER = "arithmOper";
	public static final String F_UNIT2ID = "unit2Id";
	public static final String F_UNIT2CODE = "unit2Code";
	public static final String F_ACTIVE = "active";
	public static final String F_HASTIMESERIEITEMS = "hasTimeSerieItems";
	public static final String F_HASCOMPONENTS = "hasComponents";
	public static final String F_COMPONENTSCOUNT = "componentsCount";
	public static final String F_CANBECOMPLETED = "canBeCompleted";

	@DsField(fetch = false)
	private String measure;

	@DsField
	private String serieName;

	@DsField
	private String description;

	@DsField
	private String serieType;

	@DsField
	private SerieFreqInd serieFreqInd;

	@DsField
	private Integer decimals;

	@DsField
	private DataProvider dataProvider;

	@DsField
	private String externalSerieName;

	@DsField
	private String activation;

	@DsField
	private Date firstUsage;

	@DsField
	private Date lastUsage;

	@DsField
	private BigDecimal factor;

	@DsField
	private String supplyItemUpdate;

	@DsField
	private TimeSeriesStatus status;

	@DsField
	private TimeSeriesApprovalStatus approvalStatus;

	@DsField
	private ValueType valueType;

	@DsField(join = "left", path = "currency1Id.id")
	private Integer currency1Id;

	@DsField(join = "left", path = "currency1Id.code")
	private String currency1Code;

	@DsField(join = "left", path = "currency2Id.id")
	private Integer currency2Id;

	@DsField(join = "left", path = "currency2Id.code")
	private String currency2Code;

	@DsField(join = "left", path = "commoditieId.id")
	private Integer commodityId;

	@DsField(join = "left", path = "commoditieId.name")
	private String commodityName;

	@DsField(join = "left", path = "commoditieId.code")
	private String commodityCode;

	@DsField
	private PostTypeInd postTypeInd;

	@DsField(join = "left", path = "financialSource.id")
	private Integer financialSource;

	@DsField(join = "left", path = "financialSource.code")
	private String financialSourceCode;

	@DsField(join = "left", path = "financialSource.name")
	private String financialSourceName;

	@DsField(join = "left", path = "unitId.id")
	private Integer unitId;

	@DsField(join = "left", path = "unitId.code")
	private String unitCode;

	@DsField
	private Operator convFctr;

	@DsField
	private BigDecimal arithmOper;

	@DsField(join = "left", path = "unit2Id.id")
	private Integer unit2Id;

	@DsField(join = "left", path = "unit2Id.code")
	private String unit2Code;

	@DsField
	private Boolean active;

	@DsField(fetch = false)
	private Boolean hasTimeSerieItems;

	@DsField(fetch = false)
	private Boolean hasComponents;

	@DsField(fetch = false)
	private Integer componentsCount;

	@DsField(fetch = false)
	private Boolean canBeCompleted;

	/**
	 * Default constructor
	 */
	public EnergyPriceTimeSerie_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public EnergyPriceTimeSerie_Ds(TimeSerie e) {
		super(e);
	}

	public String getMeasure() {
		return this.measure;
	}

	public void setMeasure(String measure) {
		this.measure = measure;
	}

	public String getSerieName() {
		return this.serieName;
	}

	public void setSerieName(String serieName) {
		this.serieName = serieName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSerieType() {
		return this.serieType;
	}

	public void setSerieType(String serieType) {
		this.serieType = serieType;
	}

	public SerieFreqInd getSerieFreqInd() {
		return this.serieFreqInd;
	}

	public void setSerieFreqInd(SerieFreqInd serieFreqInd) {
		this.serieFreqInd = serieFreqInd;
	}

	public Integer getDecimals() {
		return this.decimals;
	}

	public void setDecimals(Integer decimals) {
		this.decimals = decimals;
	}

	public DataProvider getDataProvider() {
		return this.dataProvider;
	}

	public void setDataProvider(DataProvider dataProvider) {
		this.dataProvider = dataProvider;
	}

	public String getExternalSerieName() {
		return this.externalSerieName;
	}

	public void setExternalSerieName(String externalSerieName) {
		this.externalSerieName = externalSerieName;
	}

	public String getActivation() {
		return this.activation;
	}

	public void setActivation(String activation) {
		this.activation = activation;
	}

	public Date getFirstUsage() {
		return this.firstUsage;
	}

	public void setFirstUsage(Date firstUsage) {
		this.firstUsage = firstUsage;
	}

	public Date getLastUsage() {
		return this.lastUsage;
	}

	public void setLastUsage(Date lastUsage) {
		this.lastUsage = lastUsage;
	}

	public BigDecimal getFactor() {
		return this.factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

	public String getSupplyItemUpdate() {
		return this.supplyItemUpdate;
	}

	public void setSupplyItemUpdate(String supplyItemUpdate) {
		this.supplyItemUpdate = supplyItemUpdate;
	}

	public TimeSeriesStatus getStatus() {
		return this.status;
	}

	public void setStatus(TimeSeriesStatus status) {
		this.status = status;
	}

	public TimeSeriesApprovalStatus getApprovalStatus() {
		return this.approvalStatus;
	}

	public void setApprovalStatus(TimeSeriesApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public ValueType getValueType() {
		return this.valueType;
	}

	public void setValueType(ValueType valueType) {
		this.valueType = valueType;
	}

	public Integer getCurrency1Id() {
		return this.currency1Id;
	}

	public void setCurrency1Id(Integer currency1Id) {
		this.currency1Id = currency1Id;
	}

	public String getCurrency1Code() {
		return this.currency1Code;
	}

	public void setCurrency1Code(String currency1Code) {
		this.currency1Code = currency1Code;
	}

	public Integer getCurrency2Id() {
		return this.currency2Id;
	}

	public void setCurrency2Id(Integer currency2Id) {
		this.currency2Id = currency2Id;
	}

	public String getCurrency2Code() {
		return this.currency2Code;
	}

	public void setCurrency2Code(String currency2Code) {
		this.currency2Code = currency2Code;
	}

	public Integer getCommodityId() {
		return this.commodityId;
	}

	public void setCommodityId(Integer commodityId) {
		this.commodityId = commodityId;
	}

	public String getCommodityName() {
		return this.commodityName;
	}

	public void setCommodityName(String commodityName) {
		this.commodityName = commodityName;
	}

	public String getCommodityCode() {
		return this.commodityCode;
	}

	public void setCommodityCode(String commodityCode) {
		this.commodityCode = commodityCode;
	}

	public PostTypeInd getPostTypeInd() {
		return this.postTypeInd;
	}

	public void setPostTypeInd(PostTypeInd postTypeInd) {
		this.postTypeInd = postTypeInd;
	}

	public Integer getFinancialSource() {
		return this.financialSource;
	}

	public void setFinancialSource(Integer financialSource) {
		this.financialSource = financialSource;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}

	public String getFinancialSourceName() {
		return this.financialSourceName;
	}

	public void setFinancialSourceName(String financialSourceName) {
		this.financialSourceName = financialSourceName;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public Operator getConvFctr() {
		return this.convFctr;
	}

	public void setConvFctr(Operator convFctr) {
		this.convFctr = convFctr;
	}

	public BigDecimal getArithmOper() {
		return this.arithmOper;
	}

	public void setArithmOper(BigDecimal arithmOper) {
		this.arithmOper = arithmOper;
	}

	public Integer getUnit2Id() {
		return this.unit2Id;
	}

	public void setUnit2Id(Integer unit2Id) {
		this.unit2Id = unit2Id;
	}

	public String getUnit2Code() {
		return this.unit2Code;
	}

	public void setUnit2Code(String unit2Code) {
		this.unit2Code = unit2Code;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getHasTimeSerieItems() {
		return this.hasTimeSerieItems;
	}

	public void setHasTimeSerieItems(Boolean hasTimeSerieItems) {
		this.hasTimeSerieItems = hasTimeSerieItems;
	}

	public Boolean getHasComponents() {
		return this.hasComponents;
	}

	public void setHasComponents(Boolean hasComponents) {
		this.hasComponents = hasComponents;
	}

	public Integer getComponentsCount() {
		return this.componentsCount;
	}

	public void setComponentsCount(Integer componentsCount) {
		this.componentsCount = componentsCount;
	}

	public Boolean getCanBeCompleted() {
		return this.canBeCompleted;
	}

	public void setCanBeCompleted(Boolean canBeCompleted) {
		this.canBeCompleted = canBeCompleted;
	}
}
