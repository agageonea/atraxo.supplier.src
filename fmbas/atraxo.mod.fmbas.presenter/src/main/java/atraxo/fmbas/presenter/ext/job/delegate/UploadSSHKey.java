package atraxo.fmbas.presenter.ext.job.delegate;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atraxo.fmbas.business.api.job.IActionParameterService;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamType;
import atraxo.fmbas.domain.impl.job.ActionParameter;
import seava.j4e.api.descriptor.IUploadedFileDescriptor;
import seava.j4e.api.service.IFileUploadService;
import seava.j4e.business.service.storage.StorageService;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * Presenter delegate to upload SSH keys used by job actions.
 *
 * @author zspeter
 */
public class UploadSSHKey extends AbstractPresenterDelegate implements IFileUploadService {

	private static final String SSH_STORE = "SSHStore";
	private static final String KEYNAME = "KEYNAME";
	private static final String KEY_ID = "id";
	private List<String> paramNames;

	/**
	 * Default constructor
	 */
	public UploadSSHKey() {
		this.paramNames = new ArrayList<>();
		this.paramNames.add(KEY_ID);
	}

	@Override
	public List<String> getParamNames() {
		return this.paramNames;
	}

	@Override
	public Map<String, Object> execute(IUploadedFileDescriptor fileDescriptor, InputStream inputStream, Map<String, String> params) throws Exception {
		IActionParameterService srv = (IActionParameterService) this.findEntityService(ActionParameter.class);
		StorageService storageService = (StorageService) this.getApplicationContext().getBean("storageService");
		Map<String, Object> result = new HashMap<>();
		String id = params.get(KEY_ID);
		ActionParameter actionParameter = srv.findById(Integer.valueOf(id));
		String refId = actionParameter.getRefid();
		if (JobParamType._KEY_.equals(actionParameter.getType()) && KEYNAME.equals(actionParameter.getName()) && refId != null) {
			storageService.uploadFile(inputStream, actionParameter.getRefid(), fileDescriptor.getOriginalName(), SSH_STORE);
		}
		return result;
	}

}
