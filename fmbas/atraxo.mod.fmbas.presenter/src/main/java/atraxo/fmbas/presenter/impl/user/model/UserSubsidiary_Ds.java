/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.user.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = UserSubsidiary.class, sort = {@SortField(field = UserSubsidiary_Ds.F_CODE)})
@RefLookups({
		@RefLookup(refId = UserSubsidiary_Ds.F_SUBSIDIARYID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = UserSubsidiary_Ds.F_CODE)}),
		@RefLookup(refId = UserSubsidiary_Ds.F_USERID, namedQuery = UserSupp.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = UserSubsidiary_Ds.F_USERCODE)}),
		@RefLookup(refId = UserSubsidiary_Ds.F_DEFAULTVOLUMEUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = UserSubsidiary_Ds.F_DEFAULTVOLUMEUNITCODE)}),
		@RefLookup(refId = UserSubsidiary_Ds.F_DEFAULTWEIGHTUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = UserSubsidiary_Ds.F_DEFAULTWEIGHTUNITCODE)})})
public class UserSubsidiary_Ds extends AbstractDs_Ds<UserSubsidiary> {

	public static final String ALIAS = "fmbas_UserSubsidiary_Ds";

	public static final String F_MAINSUBSIDIARY = "mainSubsidiary";
	public static final String F_USERCODE = "userCode";
	public static final String F_USERNAME = "userName";
	public static final String F_USERID = "userId";
	public static final String F_SUBSIDIARYID = "subsidiaryId";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_OFFICECOUNTRYNAME = "officeCountryName";
	public static final String F_OFFICESTATENAME = "officeStateName";
	public static final String F_DEFAULTVOLUMEUNITID = "defaultVolumeUnitId";
	public static final String F_DEFAULTVOLUMEUNITCODE = "defaultVolumeUnitCode";
	public static final String F_DEFAULTWEIGHTUNITID = "defaultWeightUnitId";
	public static final String F_DEFAULTWEIGHTUNITCODE = "defaultWeightUnitCode";
	public static final String F_DEFAULTCURRENCYID = "defaultCurrencyId";
	public static final String F_DEFAULTCURRENCYCODE = "defaultCurrencyCode";

	@DsField
	private Boolean mainSubsidiary;

	@DsField(join = "left", path = "user.code")
	private String userCode;

	@DsField(join = "left", path = "user.name")
	private String userName;

	@DsField(join = "left", path = "user.id")
	private String userId;

	@DsField(join = "left", path = "subsidiary.id")
	private Integer subsidiaryId;

	@DsField(join = "left", path = "subsidiary.code")
	private String code;

	@DsField(join = "left", path = "subsidiary.name")
	private String name;

	@DsField(join = "left", path = "subsidiary.officeCountry.name")
	private String officeCountryName;

	@DsField(join = "left", path = "subsidiary.officeState.name")
	private String officeStateName;

	@DsField(join = "left", path = "subsidiary.defaultVolumeUnit.id")
	private Integer defaultVolumeUnitId;

	@DsField(join = "left", path = "subsidiary.defaultVolumeUnit.code")
	private String defaultVolumeUnitCode;

	@DsField(join = "left", path = "subsidiary.defaultWeightUnit.id")
	private Integer defaultWeightUnitId;

	@DsField(join = "left", path = "subsidiary.defaultWeightUnit.code")
	private String defaultWeightUnitCode;

	@DsField(join = "left", path = "subsidiary.subsidiaryCurrency.id")
	private Integer defaultCurrencyId;

	@DsField(join = "left", path = "subsidiary.subsidiaryCurrency.code")
	private String defaultCurrencyCode;

	/**
	 * Default constructor
	 */
	public UserSubsidiary_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public UserSubsidiary_Ds(UserSubsidiary e) {
		super(e);
	}

	public Boolean getMainSubsidiary() {
		return this.mainSubsidiary;
	}

	public void setMainSubsidiary(Boolean mainSubsidiary) {
		this.mainSubsidiary = mainSubsidiary;
	}

	public String getUserCode() {
		return this.userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getSubsidiaryId() {
		return this.subsidiaryId;
	}

	public void setSubsidiaryId(Integer subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOfficeCountryName() {
		return this.officeCountryName;
	}

	public void setOfficeCountryName(String officeCountryName) {
		this.officeCountryName = officeCountryName;
	}

	public String getOfficeStateName() {
		return this.officeStateName;
	}

	public void setOfficeStateName(String officeStateName) {
		this.officeStateName = officeStateName;
	}

	public Integer getDefaultVolumeUnitId() {
		return this.defaultVolumeUnitId;
	}

	public void setDefaultVolumeUnitId(Integer defaultVolumeUnitId) {
		this.defaultVolumeUnitId = defaultVolumeUnitId;
	}

	public String getDefaultVolumeUnitCode() {
		return this.defaultVolumeUnitCode;
	}

	public void setDefaultVolumeUnitCode(String defaultVolumeUnitCode) {
		this.defaultVolumeUnitCode = defaultVolumeUnitCode;
	}

	public Integer getDefaultWeightUnitId() {
		return this.defaultWeightUnitId;
	}

	public void setDefaultWeightUnitId(Integer defaultWeightUnitId) {
		this.defaultWeightUnitId = defaultWeightUnitId;
	}

	public String getDefaultWeightUnitCode() {
		return this.defaultWeightUnitCode;
	}

	public void setDefaultWeightUnitCode(String defaultWeightUnitCode) {
		this.defaultWeightUnitCode = defaultWeightUnitCode;
	}

	public Integer getDefaultCurrencyId() {
		return this.defaultCurrencyId;
	}

	public void setDefaultCurrencyId(Integer defaultCurrencyId) {
		this.defaultCurrencyId = defaultCurrencyId;
	}

	public String getDefaultCurrencyCode() {
		return this.defaultCurrencyCode;
	}

	public void setDefaultCurrencyCode(String defaultCurrencyCode) {
		this.defaultCurrencyCode = defaultCurrencyCode;
	}
}
