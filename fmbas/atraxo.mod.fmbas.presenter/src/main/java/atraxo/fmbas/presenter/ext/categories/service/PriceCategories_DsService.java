/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.categories.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionSystemException;

import atraxo.fmbas.business.ext.exceptions.DuplicatePriceCategoryException;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.presenter.impl.categories.model.PriceCategories_Ds;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class PriceCategories_DsService
		extends AbstractEntityDsService<PriceCategories_Ds, PriceCategories_Ds, Object, PriceCategory>
		implements IDsService<PriceCategories_Ds, PriceCategories_Ds, Object> {

	private static final Logger LOG = LoggerFactory.getLogger(PriceCategories_DsService.class);

	@Override
	protected void onInsert(List<PriceCategories_Ds> list, List<PriceCategory> entities, Object params)
			throws Exception {
		try {
			super.onInsert(list, entities, params);
		} catch (TransactionSystemException e) {
			LOG.error(e.getMessage(), e);
			throw new DuplicatePriceCategoryException(PriceCategory.class.getSimpleName(), "\"name\"");
		}
	}

}
