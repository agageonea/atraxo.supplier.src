/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.ac.model;

import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = AcTypes.class, sort = {@SortField(field = AcTypesLov_Ds.F_CODE)})
public class AcTypesLov_Ds extends AbstractLov_Ds<AcTypes> {

	public static final String ALIAS = "fmbas_AcTypesLov_Ds";

	public static final String F_CODE = "code";
	public static final String F_ACTIVE = "active";
	public static final String F_NAME = "name";
	public static final String F_TANKCAPACITY = "tankCapacity";

	@DsField
	private String code;

	@DsField
	private Boolean active;

	@DsField
	private String name;

	@DsField
	private Integer tankCapacity;

	/**
	 * Default constructor
	 */
	public AcTypesLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AcTypesLov_Ds(AcTypes e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTankCapacity() {
		return this.tankCapacity;
	}

	public void setTankCapacity(Integer tankCapacity) {
		this.tankCapacity = tankCapacity;
	}
}
