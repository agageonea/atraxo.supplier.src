/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.vat.model;

import atraxo.fmbas.domain.impl.vat.Vat;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Vat.class, sort = {@SortField(field = VatLov_Ds.F_CODE)})
public class VatLov_Ds extends AbstractLov_Ds<Vat> {

	public static final String ALIAS = "fmbas_VatLov_Ds";

	public static final String F_CODE = "code";
	public static final String F_COUNTRYNAME = "countryName";
	public static final String F_COUNTRYCODE = "countryCode";

	@DsField
	private String code;

	@DsField(join = "left", path = "country.name")
	private String countryName;

	@DsField(join = "left", path = "country.code")
	private String countryCode;

	/**
	 * Default constructor
	 */
	public VatLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public VatLov_Ds(Vat e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}
