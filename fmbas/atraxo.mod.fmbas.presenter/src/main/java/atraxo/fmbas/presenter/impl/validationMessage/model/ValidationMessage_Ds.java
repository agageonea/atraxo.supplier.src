/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.validationMessage.model;

import atraxo.fmbas.domain.impl.fmbas_type.ValidationMessageSeverity;
import atraxo.fmbas.domain.impl.validationMessage.ValidationMessage;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ValidationMessage.class, sort = {@SortField(field = ValidationMessage_Ds.F_CREATEDAT)})
public class ValidationMessage_Ds extends AbstractDs_Ds<ValidationMessage> {

	public static final String ALIAS = "fmbas_ValidationMessage_Ds";

	public static final String F_OBJECTID = "objectId";
	public static final String F_OBJECTTYPE = "objectType";
	public static final String F_MESSAGE = "message";
	public static final String F_SEVERITY = "severity";
	public static final String F_LABELFIELD = "labelField";

	@DsField
	private String objectId;

	@DsField
	private String objectType;

	@DsField
	private String message;

	@DsField
	private ValidationMessageSeverity severity;

	@DsField(fetch = false)
	private String labelField;

	/**
	 * Default constructor
	 */
	public ValidationMessage_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ValidationMessage_Ds(ValidationMessage e) {
		super(e);
	}

	public String getObjectId() {
		return this.objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ValidationMessageSeverity getSeverity() {
		return this.severity;
	}

	public void setSeverity(ValidationMessageSeverity severity) {
		this.severity = severity;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}
}
