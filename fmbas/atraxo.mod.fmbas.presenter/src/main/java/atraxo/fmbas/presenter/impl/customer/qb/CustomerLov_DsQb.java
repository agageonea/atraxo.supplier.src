/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.qb;

import atraxo.fmbas.presenter.impl.customer.model.CustomerLov_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class CustomerLov_DsQb
		extends
			QueryBuilderWithJpql<CustomerLov_Ds, CustomerLov_Ds, Object> {

	@Override
	public void setFilter(CustomerLov_Ds filter) {
		filter.setIsCustomer(true);
		super.setFilter(filter);
	}
}
