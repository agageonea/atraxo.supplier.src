/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

package atraxo.fmbas.presenter.ext.uom.qb;

import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.presenter.impl.uom.model.MeasurmentUnitsTimeLov_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class MeasurmentUnitsTimeLov_DsQb extends QueryBuilderWithJpql<MeasurmentUnitsTimeLov_Ds, MeasurmentUnitsTimeLov_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();

		this.addFilterCondition("(e.unittypeInd = :durationUnit)");

		this.addCustomFilterItem("durationUnit", UnitType._DURATION_);
	}
}
