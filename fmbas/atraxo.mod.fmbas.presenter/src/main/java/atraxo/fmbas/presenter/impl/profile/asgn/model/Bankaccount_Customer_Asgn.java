/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.profile.asgn.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;
import seava.j4e.presenter.model.AbstractAsgnModel;

@Ds(entity = Customer.class, sort = {@SortField(field = Bankaccount_Customer_Asgn.f_name)})
public class Bankaccount_Customer_Asgn extends AbstractAsgnModel<Customer> {

	public static final String ALIAS = "fmbas_Bankaccount_Customer_Asgn";

	public static final String f_id = "id";
	public static final String f_name = "name";
	public static final String f_code = "code";

	@DsField(path = "id")
	private Integer id;

	@DsField(path = "name")
	private String name;

	@DsField(path = "code")
	private String code;

	public Bankaccount_Customer_Asgn() {
	}

	public Bankaccount_Customer_Asgn(Customer e) {
		super();
		this.id = e.getId();
		this.name = e.getName();
		this.code = e.getCode();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
