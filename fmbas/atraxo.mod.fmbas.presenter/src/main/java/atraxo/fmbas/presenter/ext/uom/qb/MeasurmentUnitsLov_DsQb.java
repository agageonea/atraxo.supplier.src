/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.fmbas.presenter.ext.uom.qb;

import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.presenter.impl.uom.model.MeasurmentUnitsLov_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class MeasurmentUnitsLov_DsQb extends QueryBuilderWithJpql<MeasurmentUnitsLov_Ds, MeasurmentUnitsLov_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();

		this.addFilterCondition("(e.unittypeInd = :massUnit or e.unittypeInd = :volumeUnit)");

		this.addCustomFilterItem("massUnit", UnitType._MASS_);
		this.addCustomFilterItem("volumeUnit", UnitType._VOLUME_);
	}
}
