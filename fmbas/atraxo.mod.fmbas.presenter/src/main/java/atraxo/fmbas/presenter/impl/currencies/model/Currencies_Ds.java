/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.currencies.model;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Currencies.class, sort = {@SortField(field = Currencies_Ds.F_NAME)})
public class Currencies_Ds extends AbstractDs_Ds<Currencies> {

	public static final String ALIAS = "fmbas_Currencies_Ds";

	public static final String F_ISONO = "isoNo";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_DECCNT = "decCnt";
	public static final String F_ACTIVE = "active";

	@DsField
	private String isoNo;

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private Integer decCnt;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public Currencies_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Currencies_Ds(Currencies e) {
		super(e);
	}

	public String getIsoNo() {
		return this.isoNo;
	}

	public void setIsoNo(String isoNo) {
		this.isoNo = isoNo;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDecCnt() {
		return this.decCnt;
	}

	public void setDecCnt(Integer decCnt) {
		this.decCnt = decCnt;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
