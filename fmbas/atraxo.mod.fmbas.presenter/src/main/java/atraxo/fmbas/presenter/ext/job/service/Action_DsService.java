/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.job.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.presenter.impl.job.model.Action_Ds;
import seava.j4e.api.service.business.IEntityService;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class Action_DsService extends AbstractEntityDsService<Action_Ds, Action_Ds, Object, Action>
		implements IDsService<Action_Ds, Action_Ds, Object> {

	@Override
	@Transactional
	public void deleteByIds(List<Object> ids) throws Exception {
		IEntityService<Action> srv = this.getEntityService();
		List<Action> list = srv.findByIds(ids);
		srv.delete(list);
	}
}
