/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.dashboard.service;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.domain.impl.dashboard.Widgets;
import atraxo.fmbas.presenter.impl.dashboard.model.Widget_Ds;
import seava.j4e.api.Constants;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.security.IAuthorizationFactory;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service Widget_DsService
 */
public class Widget_DsService extends AbstractEntityDsService<Widget_Ds, Widget_Ds, Object, Widgets>
		implements IDsService<Widget_Ds, Widget_Ds, Object> {

	private final static Logger LOG = LoggerFactory.getLogger(Widget_DsService.class);

	@Override
	protected void postFind(IQueryBuilder<Widget_Ds, Widget_Ds, Object> builder, List<Widget_Ds> result) throws Exception {
		super.postFind(builder, result);
		IAuthorizationFactory authorizationFactory = this.getApplicationContext().getBean(IAuthorizationFactory.class);
		Iterator<Widget_Ds> iter = result.iterator();
		while (iter.hasNext()) {
			Widget_Ds widgetDs = iter.next();
			String dsName = widgetDs.getDsName();
			String rpcName = widgetDs.getMethodName();
			try {
				authorizationFactory.getDsAuthorizationProvider().authorize(dsName, Constants.DS_ACTION_RPC, rpcName);
			} catch (Exception e) {
				LOG.warn("Could not authorize DS for rpc " + rpcName + ", will remove the Widget_Ds!", e);
				iter.remove();
			}
		}
		builder.putExtraAttribute("rec-count", new Long(result.size()));
	}

	@Override
	public Long count(IQueryBuilder<Widget_Ds, Widget_Ds, Object> builder) throws Exception {
		return (Long) builder.getExtraAttribute("rec-count");
	}
}
