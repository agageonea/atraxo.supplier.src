/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.abstracts.model;

import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
public class AbstractSubsidiaryLov_Ds<E> extends AbstractLov_Ds<E> {

	public static final String ALIAS = "fmbas_AbstractSubsidiaryLov_Ds";

	public static final String F_SUBSIDIARYID = "subsidiaryId";

	@DsField(noUpdate = true)
	private String subsidiaryId;

	/**
	 * Default constructor
	 */
	public AbstractSubsidiaryLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AbstractSubsidiaryLov_Ds(E e) {
		super(e);
	}

	public String getSubsidiaryId() {
		return this.subsidiaryId;
	}

	public void setSubsidiaryId(String subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}
}
