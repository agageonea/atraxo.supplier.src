/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.notes.model;

import atraxo.fmbas.domain.impl.notes.Notes;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Notes.class, sort = {@SortField(field = Note_Ds.F_CREATEDAT)})
public class Note_Ds extends AbstractDs_Ds<Notes> {

	public static final String ALIAS = "fmbas_Note_Ds";

	public static final String F_OBJECTID = "objectId";
	public static final String F_OBJECTTYPE = "objectType";
	public static final String F_NOTES = "notes";

	@DsField
	private Integer objectId;

	@DsField
	private String objectType;

	@DsField
	private String notes;

	/**
	 * Default constructor
	 */
	public Note_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Note_Ds(Notes e) {
		super(e);
	}

	public Integer getObjectId() {
		return this.objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
