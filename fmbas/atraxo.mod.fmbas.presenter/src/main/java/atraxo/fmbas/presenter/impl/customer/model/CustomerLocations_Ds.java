/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

import atraxo.fmbas.domain.impl.customer.CustomerLocations;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = CustomerLocations.class, sort = {@SortField(field = CustomerLocations_Ds.F_NAME)})
@RefLookups({@RefLookup(refId = CustomerLocations_Ds.F_COUNTRYID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = CustomerLocations_Ds.F_COUNTRYCODE)})})
public class CustomerLocations_Ds extends AbstractDs_Ds<CustomerLocations> {

	public static final String ALIAS = "fmbas_CustomerLocations_Ds";

	public static final String F_COUNTRYID = "countryId";
	public static final String F_COUNTRYCODE = "countryCode";
	public static final String F_COUNTRYNAME = "countryName";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_IATACODE = "iataCode";
	public static final String F_ICAOCODE = "icaoCode";
	public static final String F_ISAIRPORT = "isAirport";

	@DsField(join = "left", path = "country.id")
	private Integer countryId;

	@DsField(join = "left", path = "country.code")
	private String countryCode;

	@DsField(join = "left", path = "country.name")
	private String countryName;

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private String iataCode;

	@DsField
	private String icaoCode;

	@DsField
	private Boolean isAirport;

	/**
	 * Default constructor
	 */
	public CustomerLocations_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public CustomerLocations_Ds(CustomerLocations e) {
		super(e);
	}

	public Integer getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public String getIcaoCode() {
		return this.icaoCode;
	}

	public void setIcaoCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}

	public Boolean getIsAirport() {
		return this.isAirport;
	}

	public void setIsAirport(Boolean isAirport) {
		this.isAirport = isAirport;
	}
}
