/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.timeserie.model;

import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesStatus;
import atraxo.fmbas.domain.impl.timeserie.RawTimeSerieItem;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = RawTimeSerieItem.class, sort = {@SortField(field = TimeSerieItem_Ds.F_ITEMDATE, desc = true)})
@RefLookups({@RefLookup(refId = TimeSerieItem_Ds.F_TSERIE, namedQuery = TimeSerie.NQ_FIND_BY_NAME, params = {@Param(name = "serieName", field = TimeSerieItem_Ds.F_TSERIE)})})
public class TimeSerieItem_Ds extends AbstractDs_Ds<RawTimeSerieItem> {

	public static final String ALIAS = "fmbas_TimeSerieItem_Ds";

	public static final String F_TSERIE = "tserie";
	public static final String F_SERIENAME = "serieName";
	public static final String F_SERIESTATUS = "serieStatus";
	public static final String F_ITEMDATE = "itemdate";
	public static final String F_ITEMVALUE = "itemValue";
	public static final String F_CALCULATED = "calculated";
	public static final String F_TRANSFRED = "transfred";
	public static final String F_TRANSFEREDITEMUPDATE = "transferedItemUpdate";
	public static final String F_UPDATE = "update";
	public static final String F_ACTIVE = "active";

	@DsField(join = "left", path = "tserie.id")
	private Integer tserie;

	@DsField(join = "left", path = "tserie.serieName")
	private String serieName;

	@DsField(join = "left", path = "tserie.status")
	private TimeSeriesStatus serieStatus;

	@DsField(path = "itemDate")
	private Date itemdate;

	@DsField
	private BigDecimal itemValue;

	@DsField
	private Boolean calculated;

	@DsField(path = "transfered")
	private Boolean transfred;

	@DsField
	private Boolean transferedItemUpdate;

	@DsField(path = "updated")
	private Boolean update;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public TimeSerieItem_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public TimeSerieItem_Ds(RawTimeSerieItem e) {
		super(e);
	}

	public Integer getTserie() {
		return this.tserie;
	}

	public void setTserie(Integer tserie) {
		this.tserie = tserie;
	}

	public String getSerieName() {
		return this.serieName;
	}

	public void setSerieName(String serieName) {
		this.serieName = serieName;
	}

	public TimeSeriesStatus getSerieStatus() {
		return this.serieStatus;
	}

	public void setSerieStatus(TimeSeriesStatus serieStatus) {
		this.serieStatus = serieStatus;
	}

	public Date getItemdate() {
		return this.itemdate;
	}

	public void setItemdate(Date itemdate) {
		this.itemdate = itemdate;
	}

	public BigDecimal getItemValue() {
		return this.itemValue;
	}

	public void setItemValue(BigDecimal itemValue) {
		this.itemValue = itemValue;
	}

	public Boolean getCalculated() {
		return this.calculated;
	}

	public void setCalculated(Boolean calculated) {
		this.calculated = calculated;
	}

	public Boolean getTransfred() {
		return this.transfred;
	}

	public void setTransfred(Boolean transfred) {
		this.transfred = transfred;
	}

	public Boolean getTransferedItemUpdate() {
		return this.transferedItemUpdate;
	}

	public void setTransferedItemUpdate(Boolean transferedItemUpdate) {
		this.transferedItemUpdate = transferedItemUpdate;
	}

	public Boolean getUpdate() {
		return this.update;
	}

	public void setUpdate(Boolean update) {
		this.update = update;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
