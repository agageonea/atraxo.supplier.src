/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.workflow.model;

import atraxo.fmbas.domain.impl.fmbas_type.JobChainNotificationCondition;
import atraxo.fmbas.domain.impl.fmbas_type.NotificationType;
import atraxo.fmbas.domain.impl.workflow.WorkflowNotification;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = WorkflowNotification.class)
@RefLookups({@RefLookup(refId = WorkflowNotifications_Ds.F_WORKFLOWID),
		@RefLookup(refId = WorkflowNotifications_Ds.F_USERID)})
public class WorkflowNotifications_Ds
		extends
			AbstractDs_Ds<WorkflowNotification> {

	public static final String ALIAS = "fmbas_WorkflowNotifications_Ds";

	public static final String F_WORKFLOWID = "workflowId";
	public static final String F_WORKFLOWNAME = "workflowName";
	public static final String F_USERID = "userId";
	public static final String F_USERNAME = "userName";
	public static final String F_USEREMAIL = "userEmail";
	public static final String F_NOTIFICATIONCONDITION = "notificationCondition";
	public static final String F_ENABLED = "enabled";
	public static final String F_NOTIFICATIONTYPE = "notificationType";

	@DsField(join = "left", path = "workflow.id")
	private Integer workflowId;

	@DsField(join = "left", path = "workflow.name")
	private String workflowName;

	@DsField(join = "left", path = "user.id")
	private String userId;

	@DsField(join = "left", path = "user.name")
	private String userName;

	@DsField(join = "left", path = "user.email")
	private String userEmail;

	@DsField
	private JobChainNotificationCondition notificationCondition;

	@DsField
	private Boolean enabled;

	@DsField
	private NotificationType notificationType;

	/**
	 * Default constructor
	 */
	public WorkflowNotifications_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public WorkflowNotifications_Ds(WorkflowNotification e) {
		super(e);
	}

	public Integer getWorkflowId() {
		return this.workflowId;
	}

	public void setWorkflowId(Integer workflowId) {
		this.workflowId = workflowId;
	}

	public String getWorkflowName() {
		return this.workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public JobChainNotificationCondition getNotificationCondition() {
		return this.notificationCondition;
	}

	public void setNotificationCondition(
			JobChainNotificationCondition notificationCondition) {
		this.notificationCondition = notificationCondition;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public NotificationType getNotificationType() {
		return this.notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}
}
