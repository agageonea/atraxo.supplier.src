/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.customer.CustomerNotification;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import atraxo.fmbas.domain.impl.templates.Template;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = CustomerNotification.class)
@RefLookups({
		@RefLookup(refId = CustomerNotification_Ds.F_CUSTOMERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = CustomerNotification_Ds.F_CUSTOMERCODE)}),
		@RefLookup(refId = CustomerNotification_Ds.F_TEMPLATEID, namedQuery = Template.NQ_FIND_BY_KEY_PRIMITIVE, params = {
				@Param(name = "userId", field = CustomerNotification_Ds.F_SUBSIDIARYCODE),
				@Param(name = "subsidiaryId", field = CustomerNotification_Ds.F_USERCODE),
				@Param(name = "name", field = CustomerNotification_Ds.F_TEMPLATENAME)}),
		@RefLookup(refId = CustomerNotification_Ds.F_EMAILBODYID, namedQuery = Template.NQ_FIND_BY_KEY_PRIMITIVE, params = {
				@Param(name = "userId", field = CustomerNotification_Ds.F_SUBSIDIARYCODE),
				@Param(name = "subsidiaryId", field = CustomerNotification_Ds.F_USERCODE),
				@Param(name = "name", field = CustomerNotification_Ds.F_TEMPLATENAME)}),
		@RefLookup(refId = CustomerNotification_Ds.F_NOTIFICATIONEVENTID, namedQuery = NotificationEvent.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = CustomerNotification_Ds.F_NOTIFICATIONEVENTNAME)}),
		@RefLookup(refId = CustomerNotification_Ds.F_ASSIGNEDAREAID, namedQuery = Area.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = CustomerNotification_Ds.F_ASSIGNEDAREACODE)})})
public class CustomerNotification_Ds
		extends
			AbstractDs_Ds<CustomerNotification> {

	public static final String ALIAS = "fmbas_CustomerNotification_Ds";

	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERNAME = "customerName";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_TEMPLATEID = "templateId";
	public static final String F_TEMPLATENAME = "templateName";
	public static final String F_EMAILBODY = "emailBody";
	public static final String F_EMAILBODYID = "emailBodyId";
	public static final String F_SUBSIDIARYCODE = "subsidiaryCode";
	public static final String F_USERCODE = "userCode";
	public static final String F_NOTIFICATIONEVENTID = "notificationEventId";
	public static final String F_NOTIFICATIONEVENTNAME = "notificationEventName";
	public static final String F_NOTIFICATIONEVENTDESCRIPTION = "notificationEventDescription";
	public static final String F_ASSIGNEDAREAID = "assignedAreaId";
	public static final String F_ASSIGNEDAREACODE = "assignedAreaCode";
	public static final String F_ASSIGNEDAREANAME = "assignedAreaName";

	@DsField(join = "left", path = "customer.id")
	private Integer customerId;

	@DsField(join = "left", path = "customer.name")
	private String customerName;

	@DsField(join = "left", path = "customer.code")
	private String customerCode;

	@DsField(join = "left", path = "template.id")
	private Integer templateId;

	@DsField(join = "left", path = "template.name")
	private String templateName;

	@DsField(join = "left", path = "emailBody.name")
	private String emailBody;

	@DsField(join = "left", path = "emailBody.id")
	private Integer emailBodyId;

	@DsField(join = "left", path = "template.subsidiary.code")
	private String subsidiaryCode;

	@DsField(join = "left", path = "template.user.code")
	private String userCode;

	@DsField(join = "left", path = "notificationEvent.id")
	private Integer notificationEventId;

	@DsField(join = "left", path = "notificationEvent.name")
	private String notificationEventName;

	@DsField(join = "left", path = "notificationEvent.description")
	private String notificationEventDescription;

	@DsField(join = "left", path = "assignedArea.id")
	private Integer assignedAreaId;

	@DsField(join = "left", path = "assignedArea.code")
	private String assignedAreaCode;

	@DsField(join = "left", path = "assignedArea.name")
	private String assignedAreaName;

	/**
	 * Default constructor
	 */
	public CustomerNotification_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public CustomerNotification_Ds(CustomerNotification e) {
		super(e);
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public Integer getTemplateId() {
		return this.templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public String getTemplateName() {
		return this.templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getEmailBody() {
		return this.emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	public Integer getEmailBodyId() {
		return this.emailBodyId;
	}

	public void setEmailBodyId(Integer emailBodyId) {
		this.emailBodyId = emailBodyId;
	}

	public String getSubsidiaryCode() {
		return this.subsidiaryCode;
	}

	public void setSubsidiaryCode(String subsidiaryCode) {
		this.subsidiaryCode = subsidiaryCode;
	}

	public String getUserCode() {
		return this.userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Integer getNotificationEventId() {
		return this.notificationEventId;
	}

	public void setNotificationEventId(Integer notificationEventId) {
		this.notificationEventId = notificationEventId;
	}

	public String getNotificationEventName() {
		return this.notificationEventName;
	}

	public void setNotificationEventName(String notificationEventName) {
		this.notificationEventName = notificationEventName;
	}

	public String getNotificationEventDescription() {
		return this.notificationEventDescription;
	}

	public void setNotificationEventDescription(
			String notificationEventDescription) {
		this.notificationEventDescription = notificationEventDescription;
	}

	public Integer getAssignedAreaId() {
		return this.assignedAreaId;
	}

	public void setAssignedAreaId(Integer assignedAreaId) {
		this.assignedAreaId = assignedAreaId;
	}

	public String getAssignedAreaCode() {
		return this.assignedAreaCode;
	}

	public void setAssignedAreaCode(String assignedAreaCode) {
		this.assignedAreaCode = assignedAreaCode;
	}

	public String getAssignedAreaName() {
		return this.assignedAreaName;
	}

	public void setAssignedAreaName(String assignedAreaName) {
		this.assignedAreaName = assignedAreaName;
	}
}
