package atraxo.fmbas.presenter.ext.json.message;

import java.io.IOException;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

import atraxo.fmbas.domain.ext.json.message.SoneMessage;

public class SoneMessageDeserializer extends JsonDeserializer<SoneMessage> {

	@Override
	public SoneMessage deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		String jsonValue = jp.getText();
		String[] jsonValues = jsonValue.split("||");
		if (jsonValues.length == 2) {
			for (SoneMessage msg : SoneMessage.values()) {
				if (msg.getKey() == Integer.parseInt(jsonValues[0])) {
					return msg;
				}
			}
		}
		return SoneMessage.EMPTY;
	}

}
