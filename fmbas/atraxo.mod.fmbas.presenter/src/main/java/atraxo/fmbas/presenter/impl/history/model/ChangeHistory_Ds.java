/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.history.model;

import atraxo.fmbas.domain.impl.history.ChangeHistory;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ChangeHistory.class, sort = {
		@SortField(field = ChangeHistory_Ds.F_CREATEDAT, desc = true),
		@SortField(field = ChangeHistory_Ds.F_ID, desc = true)})
public class ChangeHistory_Ds extends AbstractDs_Ds<ChangeHistory> {

	public static final String ALIAS = "fmbas_ChangeHistory_Ds";

	public static final String F_OBJECTID = "objectId";
	public static final String F_OBJECTVALUE = "objectValue";
	public static final String F_OBJECTTYPE = "objectType";
	public static final String F_REMARKS = "remarks";
	public static final String F_REASONFIELD = "reasonField";

	@DsField
	private Integer objectId;

	@DsField
	private String objectValue;

	@DsField
	private String objectType;

	@DsField
	private String remarks;

	@DsField(fetch = false)
	private String reasonField;

	/**
	 * Default constructor
	 */
	public ChangeHistory_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ChangeHistory_Ds(ChangeHistory e) {
		super(e);
	}

	public Integer getObjectId() {
		return this.objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public String getObjectValue() {
		return this.objectValue;
	}

	public void setObjectValue(String objectValue) {
		this.objectValue = objectValue;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getReasonField() {
		return this.reasonField;
	}

	public void setReasonField(String reasonField) {
		this.reasonField = reasonField;
	}
}
