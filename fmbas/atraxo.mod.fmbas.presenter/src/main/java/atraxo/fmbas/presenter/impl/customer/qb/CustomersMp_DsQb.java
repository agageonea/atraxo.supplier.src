/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.qb;

import atraxo.fmbas.presenter.impl.customer.model.CustomersMp_Ds;
import atraxo.fmbas.presenter.impl.customer.model.CustomersMp_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class CustomersMp_DsQb
		extends
			QueryBuilderWithJpql<CustomersMp_Ds, CustomersMp_Ds, CustomersMp_DsParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getCustomerLocationId() != null
				&& !"".equals(this.params.getCustomerLocationId())) {
			addFilterCondition("  e.id in ( select mc.id from Customer mc, IN (mc.locations) cl where cl.id = :customerLocationId ) ");
			addCustomFilterItem("customerLocationId",
					this.params.getCustomerLocationId());
		}
	}
}
