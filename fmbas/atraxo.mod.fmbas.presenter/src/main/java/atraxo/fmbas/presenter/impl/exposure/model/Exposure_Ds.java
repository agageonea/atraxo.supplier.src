/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.exposure.model;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.exposure.Exposure;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.fmbas_type.ExposureLastAction;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Exposure.class, sort = {@SortField(field = Exposure_Ds.F_ID)})
@RefLookups({
		@RefLookup(refId = Exposure_Ds.F_CUSTOMERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Exposure_Ds.F_CUSTOMERCODE)}),
		@RefLookup(refId = Exposure_Ds.F_CURRENCYID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Exposure_Ds.F_CURRENCYCODE)})})
public class Exposure_Ds extends AbstractDs_Ds<Exposure> {

	public static final String ALIAS = "fmbas_Exposure_Ds";

	public static final String F_TYPE = "type";
	public static final String F_ACTUALUTILIZATION = "actualUtilization";
	public static final String F_LASTACTION = "lastAction";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_CURRENCYID = "currencyId";
	public static final String F_CURRENCYCODE = "currencyCode";
	public static final String F_CURRENCYNAME = "currencyName";

	@DsField
	private CreditTerm type;

	@DsField
	private BigDecimal actualUtilization;

	@DsField
	private ExposureLastAction lastAction;

	@DsField(join = "left", path = "customer.id")
	private Integer customerId;

	@DsField(join = "left", path = "customer.code")
	private String customerCode;

	@DsField(join = "left", path = "currency.id")
	private Integer currencyId;

	@DsField(join = "left", path = "currency.code")
	private String currencyCode;

	@DsField(join = "left", path = "currency.name")
	private String currencyName;

	/**
	 * Default constructor
	 */
	public Exposure_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Exposure_Ds(Exposure e) {
		super(e);
	}

	public CreditTerm getType() {
		return this.type;
	}

	public void setType(CreditTerm type) {
		this.type = type;
	}

	public BigDecimal getActualUtilization() {
		return this.actualUtilization;
	}

	public void setActualUtilization(BigDecimal actualUtilization) {
		this.actualUtilization = actualUtilization;
	}

	public ExposureLastAction getLastAction() {
		return this.lastAction;
	}

	public void setLastAction(ExposureLastAction lastAction) {
		this.lastAction = lastAction;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public Integer getCurrencyId() {
		return this.currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getCurrencyName() {
		return this.currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}
}
