/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.geo.qb;

import atraxo.fmbas.presenter.impl.geo.model.Locations_Ds;
import atraxo.fmbas.presenter.impl.geo.model.Locations_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class Locations_DsQb
		extends
			QueryBuilderWithJpql<Locations_Ds, Locations_Ds, Locations_DsParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getAreaId() != null
				&& !"".equals(this.params.getAreaId())) {
			addFilterCondition("   e.id in ( select p.id from  Locations p, IN (p.areas) c where c.id = :areaId ) ");
			addCustomFilterItem("areaId", this.params.getAreaId());
		}
	}
}
