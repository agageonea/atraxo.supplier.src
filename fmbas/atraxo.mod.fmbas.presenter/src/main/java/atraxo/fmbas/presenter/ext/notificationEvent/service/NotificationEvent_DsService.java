/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.notificationEvent.service;

import java.util.List;

import atraxo.fmbas.business.api.notificationEvent.INotificationEventService;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import atraxo.fmbas.presenter.impl.notificationEvent.model.NotificationEvent_Ds;
import atraxo.fmbas.presenter.impl.notificationEvent.model.NotificationEvent_DsParam;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class NotificationEvent_DsService extends
		AbstractEntityDsService<NotificationEvent_Ds, NotificationEvent_Ds, NotificationEvent_DsParam, NotificationEvent> implements
		IDsService<NotificationEvent_Ds, NotificationEvent_Ds, NotificationEvent_DsParam> {

	@Override
	protected void postFind(IQueryBuilder<NotificationEvent_Ds, NotificationEvent_Ds, NotificationEvent_DsParam> builder,
			List<NotificationEvent_Ds> result) throws Exception {
		super.postFind(builder, result);
		INotificationEventService service = (INotificationEventService) this.findEntityService(NotificationEvent.class);
		Integer contact = builder.getParams().getContactId();
		List<NotificationEvent> neList = service.findByContactId(contact);
		for (NotificationEvent ds : neList) {
			this.setAssigned(ds, result);
		}
	}

	private void setAssigned(NotificationEvent ne, List<NotificationEvent_Ds> result) {
		for (NotificationEvent_Ds ds : result) {
			if (ne.getId().equals(ds.getId())) {
				ds.setIsSelected(true);
				return;
			}
		}
	}
}
