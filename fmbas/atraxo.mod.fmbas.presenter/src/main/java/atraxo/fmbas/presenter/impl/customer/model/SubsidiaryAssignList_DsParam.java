/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

/**
 * Generated code. Do not modify in this file.
 */
public class SubsidiaryAssignList_DsParam {

	public static final String f_userCode = "userCode";
	public static final String f_userId = "userId";

	private String userCode;

	private String userId;

	public String getUserCode() {
		return this.userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
