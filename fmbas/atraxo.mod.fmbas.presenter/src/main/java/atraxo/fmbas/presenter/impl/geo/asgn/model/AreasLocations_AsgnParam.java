/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.geo.asgn.model;

import java.util.Date;
public class AreasLocations_AsgnParam {

	public static final String f_locationId = "locationId";
	public static final String f_actDate = "actDate";

	private Integer locationId;

	private Date actDate;

	public Integer getLocationId() {
		return this.locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public Date getActDate() {
		return this.actDate;
	}

	public void setActDate(Date actDate) {
		this.actDate = actDate;
	}
}
