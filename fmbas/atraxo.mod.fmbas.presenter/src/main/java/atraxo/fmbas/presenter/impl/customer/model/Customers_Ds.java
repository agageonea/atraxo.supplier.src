/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CreditUpdateRequestStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataProcessedStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataTransmissionStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerType;
import atraxo.fmbas.domain.impl.fmbas_type.NatureOfBusiness;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.geo.TimeZone;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Customer.class, jpqlWhere = "e.isCustomer = true", sort = {@SortField(field = Customers_Ds.F_CODE)})
@RefLookups({
		@RefLookup(refId = Customers_Ds.F_PARENTID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Customers_Ds.F_PARENTCODE)}),
		@RefLookup(refId = Customers_Ds.F_BUYERID, namedQuery = UserSupp.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Customers_Ds.F_BUYERCODE)}),
		@RefLookup(refId = Customers_Ds.F_BILLINGCOUNTRYID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Customers_Ds.F_BILLINGCOUNTRYCODE)}),
		@RefLookup(refId = Customers_Ds.F_OFFICECOUNTRYID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Customers_Ds.F_OFFICECOUNTRYCODE)}),
		@RefLookup(refId = Customers_Ds.F_BILLINGSTATEID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Customers_Ds.F_BILLINGSTATECODE)}),
		@RefLookup(refId = Customers_Ds.F_OFFICESTATEID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Customers_Ds.F_OFFICESTATECODE)}),
		@RefLookup(refId = Customers_Ds.F_TIMEZONEID, namedQuery = TimeZone.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = Customers_Ds.F_TIMEZONENAME)})})
public class Customers_Ds extends AbstractDs_Ds<Customer> {

	public static final String ALIAS = "fmbas_Customers_Ds";

	public static final String F_SEPARATOR = "separator";
	public static final String F_CURRENTSTATUS = "currentStatus";
	public static final String F_CITY = "city";
	public static final String F_COUNTRY = "country";
	public static final String F_EXPOSURE = "exposure";
	public static final String F_ALERTLIMIT = "alertLimit";
	public static final String F_BUYERID = "buyerId";
	public static final String F_BUYERNAME = "buyerName";
	public static final String F_BUYERCODE = "buyerCode";
	public static final String F_PARENTNAME = "parentName";
	public static final String F_PARENTID = "parentId";
	public static final String F_PARENTCODE = "parentCode";
	public static final String F_OFFICECOUNTRYID = "officeCountryId";
	public static final String F_OFFICECOUNTRYCODE = "officeCountryCode";
	public static final String F_OFFICECOUNTRYNAME = "officeCountryName";
	public static final String F_OFFICESTATEID = "officeStateId";
	public static final String F_OFFICESTATECODE = "officeStateCode";
	public static final String F_OFFICESTATENAME = "officeStateName";
	public static final String F_OFFICESTREET = "officeStreet";
	public static final String F_OFFICECITY = "officeCity";
	public static final String F_OFFICEPOSTALCODE = "officePostalCode";
	public static final String F_BILLINGCOUNTRYID = "billingCountryId";
	public static final String F_BILLINGCOUNTRYCODE = "billingCountryCode";
	public static final String F_BILLINGCOUNTRYNAME = "billingCountryName";
	public static final String F_BILLINGSTATEID = "billingStateId";
	public static final String F_BILLINGSTATECODE = "billingStateCode";
	public static final String F_BILLINGSTATENAME = "billingStateName";
	public static final String F_BILLINGSTREET = "billingStreet";
	public static final String F_BILLINGCITY = "billingCity";
	public static final String F_BILLINGPOSTALCODE = "billingPostalCode";
	public static final String F_VATREGNUMBER = "vatRegNumber";
	public static final String F_TAXIDNUMBER = "taxIdNumber";
	public static final String F_INCOMETAXNUMBER = "incomeTaxNumber";
	public static final String F_ACCOUNTNUMBER = "accountNumber";
	public static final String F_SYSTEM = "system";
	public static final String F_IATACODE = "iataCode";
	public static final String F_ICAOCODE = "icaoCode";
	public static final String F_TIMEZONEID = "timeZoneId";
	public static final String F_TIMEZONENAME = "timeZoneName";
	public static final String F_ISTHIRDPARTY = "isThirdParty";
	public static final String F_ISSUBSIDIARY = "isSubsidiary";
	public static final String F_BANKNAME = "bankName";
	public static final String F_BANKADDRESS = "bankAddress";
	public static final String F_BANKACCOUNTNO = "bankAccountNo";
	public static final String F_BANKIBANNO = "bankIbanNo";
	public static final String F_BANKSWIFTNO = "bankSwiftNo";
	public static final String F_CREDITTERM = "creditTerm";
	public static final String F_CREDITLIMIT = "creditLimit";
	public static final String F_CREDITLIMITANDCURRENCY = "creditLimitAndCurrency";
	public static final String F_CREDITCURR = "creditCurr";
	public static final String F_CREDITUTIL = "creditUtil";
	public static final String F_CREDITUTILANDPERCENT = "creditUtilAndPercent";
	public static final String F_NAME = "name";
	public static final String F_CODE = "code";
	public static final String F_STATUS = "status";
	public static final String F_TYPE = "type";
	public static final String F_PHONENO = "phoneNo";
	public static final String F_FAXNO = "faxNo";
	public static final String F_WEBSITE = "website";
	public static final String F_EMAIL = "email";
	public static final String F_DATEOFINC = "dateOfInc";
	public static final String F_NATUREOFBUSINESS = "natureOfBusiness";
	public static final String F_TRADELICENSE = "tradeLicense";
	public static final String F_TLVALIDFROM = "tlValidFrom";
	public static final String F_TLVALIDTO = "tlValidTo";
	public static final String F_AOC = "aoc";
	public static final String F_AOCVALIDFROM = "aocValidFrom";
	public static final String F_AOCVALIDTO = "aocValidTo";
	public static final String F_ISCUSTOMER = "isCustomer";
	public static final String F_USEFORBILLING = "useForBilling";
	public static final String F_TRANSMISSIONSTATUS = "transmissionStatus";
	public static final String F_PROCESSEDSTATUS = "processedStatus";
	public static final String F_CREDITUPDATEREQUESTSTATUS = "creditUpdateRequestStatus";
	public static final String F_LABELFIELD = "labelField";
	public static final String F_DUMMYFIELD = "dummyField";

	@DsField(fetch = false)
	private String separator;

	@DsField(fetch = false)
	private String currentStatus;

	@DsField(fetch = false)
	private String city;

	@DsField(fetch = false)
	private String country;

	@DsField(fetch = false)
	private String exposure;

	@DsField(fetch = false)
	private Integer alertLimit;

	@DsField(join = "left", path = "responsibleBuyer.id")
	private String buyerId;

	@DsField(join = "left", path = "responsibleBuyer.name")
	private String buyerName;

	@DsField(join = "left", path = "responsibleBuyer.code")
	private String buyerCode;

	@DsField(join = "left", path = "parent.name")
	private String parentName;

	@DsField(join = "left", path = "parent.id")
	private Integer parentId;

	@DsField(join = "left", path = "parent.code")
	private String parentCode;

	@DsField(join = "left", path = "officeCountry.id")
	private Integer officeCountryId;

	@DsField(join = "left", path = "officeCountry.code")
	private String officeCountryCode;

	@DsField(join = "left", path = "officeCountry.name")
	private String officeCountryName;

	@DsField(join = "left", path = "officeState.id")
	private Integer officeStateId;

	@DsField(join = "left", path = "officeState.code")
	private String officeStateCode;

	@DsField(join = "left", path = "officeState.name")
	private String officeStateName;

	@DsField
	private String officeStreet;

	@DsField
	private String officeCity;

	@DsField
	private String officePostalCode;

	@DsField(join = "left", path = "billingCountry.id")
	private Integer billingCountryId;

	@DsField(join = "left", path = "billingCountry.code")
	private String billingCountryCode;

	@DsField(join = "left", path = "billingCountry.name")
	private String billingCountryName;

	@DsField(join = "left", path = "billingState.id")
	private Integer billingStateId;

	@DsField(join = "left", path = "billingState.code")
	private String billingStateCode;

	@DsField(join = "left", path = "billingState.name")
	private String billingStateName;

	@DsField
	private String billingStreet;

	@DsField
	private String billingCity;

	@DsField
	private String billingPostalCode;

	@DsField
	private String vatRegNumber;

	@DsField
	private String taxIdNumber;

	@DsField
	private String incomeTaxNumber;

	@DsField
	private String accountNumber;

	@DsField
	private Boolean system;

	@DsField
	private String iataCode;

	@DsField
	private String icaoCode;

	@DsField(join = "left", path = "timeZone.id")
	private Integer timeZoneId;

	@DsField(join = "left", path = "timeZone.name")
	private String timeZoneName;

	@DsField
	private Boolean isThirdParty;

	@DsField
	private Boolean isSubsidiary;

	@DsField
	private String bankName;

	@DsField
	private String bankAddress;

	@DsField
	private String bankAccountNo;

	@DsField
	private String bankIbanNo;

	@DsField
	private String bankSwiftNo;

	@DsField(fetch = false)
	private String creditTerm;

	@DsField(fetch = false)
	private BigDecimal creditLimit;

	@DsField(fetch = false)
	private String creditLimitAndCurrency;

	@DsField(fetch = false)
	private String creditCurr;

	@DsField(fetch = false)
	private BigDecimal creditUtil;

	@DsField(fetch = false)
	private String creditUtilAndPercent;

	@DsField
	private String name;

	@DsField
	private String code;

	@DsField
	private CustomerStatus status;

	@DsField
	private CustomerType type;

	@DsField
	private String phoneNo;

	@DsField
	private String faxNo;

	@DsField
	private String website;

	@DsField
	private String email;

	@DsField
	private Date dateOfInc;

	@DsField
	private NatureOfBusiness natureOfBusiness;

	@DsField
	private String tradeLicense;

	@DsField
	private Date tlValidFrom;

	@DsField
	private Date tlValidTo;

	@DsField
	private String aoc;

	@DsField
	private Date aocValidFrom;

	@DsField
	private Date aocValidTo;

	@DsField
	private Boolean isCustomer;

	@DsField
	private Boolean useForBilling;

	@DsField
	private CustomerDataTransmissionStatus transmissionStatus;

	@DsField
	private CustomerDataProcessedStatus processedStatus;

	@DsField
	private CreditUpdateRequestStatus creditUpdateRequestStatus;

	@DsField(fetch = false)
	private String labelField;

	@DsField(fetch = false)
	private String dummyField;

	/**
	 * Default constructor
	 */
	public Customers_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Customers_Ds(Customer e) {
		super(e);
	}

	public String getSeparator() {
		return this.separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public String getCurrentStatus() {
		return this.currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getExposure() {
		return this.exposure;
	}

	public void setExposure(String exposure) {
		this.exposure = exposure;
	}

	public Integer getAlertLimit() {
		return this.alertLimit;
	}

	public void setAlertLimit(Integer alertLimit) {
		this.alertLimit = alertLimit;
	}

	public String getBuyerId() {
		return this.buyerId;
	}

	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}

	public String getBuyerName() {
		return this.buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getBuyerCode() {
		return this.buyerCode;
	}

	public void setBuyerCode(String buyerCode) {
		this.buyerCode = buyerCode;
	}

	public String getParentName() {
		return this.parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public Integer getParentId() {
		return this.parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getParentCode() {
		return this.parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public Integer getOfficeCountryId() {
		return this.officeCountryId;
	}

	public void setOfficeCountryId(Integer officeCountryId) {
		this.officeCountryId = officeCountryId;
	}

	public String getOfficeCountryCode() {
		return this.officeCountryCode;
	}

	public void setOfficeCountryCode(String officeCountryCode) {
		this.officeCountryCode = officeCountryCode;
	}

	public String getOfficeCountryName() {
		return this.officeCountryName;
	}

	public void setOfficeCountryName(String officeCountryName) {
		this.officeCountryName = officeCountryName;
	}

	public Integer getOfficeStateId() {
		return this.officeStateId;
	}

	public void setOfficeStateId(Integer officeStateId) {
		this.officeStateId = officeStateId;
	}

	public String getOfficeStateCode() {
		return this.officeStateCode;
	}

	public void setOfficeStateCode(String officeStateCode) {
		this.officeStateCode = officeStateCode;
	}

	public String getOfficeStateName() {
		return this.officeStateName;
	}

	public void setOfficeStateName(String officeStateName) {
		this.officeStateName = officeStateName;
	}

	public String getOfficeStreet() {
		return this.officeStreet;
	}

	public void setOfficeStreet(String officeStreet) {
		this.officeStreet = officeStreet;
	}

	public String getOfficeCity() {
		return this.officeCity;
	}

	public void setOfficeCity(String officeCity) {
		this.officeCity = officeCity;
	}

	public String getOfficePostalCode() {
		return this.officePostalCode;
	}

	public void setOfficePostalCode(String officePostalCode) {
		this.officePostalCode = officePostalCode;
	}

	public Integer getBillingCountryId() {
		return this.billingCountryId;
	}

	public void setBillingCountryId(Integer billingCountryId) {
		this.billingCountryId = billingCountryId;
	}

	public String getBillingCountryCode() {
		return this.billingCountryCode;
	}

	public void setBillingCountryCode(String billingCountryCode) {
		this.billingCountryCode = billingCountryCode;
	}

	public String getBillingCountryName() {
		return this.billingCountryName;
	}

	public void setBillingCountryName(String billingCountryName) {
		this.billingCountryName = billingCountryName;
	}

	public Integer getBillingStateId() {
		return this.billingStateId;
	}

	public void setBillingStateId(Integer billingStateId) {
		this.billingStateId = billingStateId;
	}

	public String getBillingStateCode() {
		return this.billingStateCode;
	}

	public void setBillingStateCode(String billingStateCode) {
		this.billingStateCode = billingStateCode;
	}

	public String getBillingStateName() {
		return this.billingStateName;
	}

	public void setBillingStateName(String billingStateName) {
		this.billingStateName = billingStateName;
	}

	public String getBillingStreet() {
		return this.billingStreet;
	}

	public void setBillingStreet(String billingStreet) {
		this.billingStreet = billingStreet;
	}

	public String getBillingCity() {
		return this.billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public String getBillingPostalCode() {
		return this.billingPostalCode;
	}

	public void setBillingPostalCode(String billingPostalCode) {
		this.billingPostalCode = billingPostalCode;
	}

	public String getVatRegNumber() {
		return this.vatRegNumber;
	}

	public void setVatRegNumber(String vatRegNumber) {
		this.vatRegNumber = vatRegNumber;
	}

	public String getTaxIdNumber() {
		return this.taxIdNumber;
	}

	public void setTaxIdNumber(String taxIdNumber) {
		this.taxIdNumber = taxIdNumber;
	}

	public String getIncomeTaxNumber() {
		return this.incomeTaxNumber;
	}

	public void setIncomeTaxNumber(String incomeTaxNumber) {
		this.incomeTaxNumber = incomeTaxNumber;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Boolean getSystem() {
		return this.system;
	}

	public void setSystem(Boolean system) {
		this.system = system;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public String getIcaoCode() {
		return this.icaoCode;
	}

	public void setIcaoCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}

	public Integer getTimeZoneId() {
		return this.timeZoneId;
	}

	public void setTimeZoneId(Integer timeZoneId) {
		this.timeZoneId = timeZoneId;
	}

	public String getTimeZoneName() {
		return this.timeZoneName;
	}

	public void setTimeZoneName(String timeZoneName) {
		this.timeZoneName = timeZoneName;
	}

	public Boolean getIsThirdParty() {
		return this.isThirdParty;
	}

	public void setIsThirdParty(Boolean isThirdParty) {
		this.isThirdParty = isThirdParty;
	}

	public Boolean getIsSubsidiary() {
		return this.isSubsidiary;
	}

	public void setIsSubsidiary(Boolean isSubsidiary) {
		this.isSubsidiary = isSubsidiary;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAddress() {
		return this.bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public String getBankAccountNo() {
		return this.bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getBankIbanNo() {
		return this.bankIbanNo;
	}

	public void setBankIbanNo(String bankIbanNo) {
		this.bankIbanNo = bankIbanNo;
	}

	public String getBankSwiftNo() {
		return this.bankSwiftNo;
	}

	public void setBankSwiftNo(String bankSwiftNo) {
		this.bankSwiftNo = bankSwiftNo;
	}

	public String getCreditTerm() {
		return this.creditTerm;
	}

	public void setCreditTerm(String creditTerm) {
		this.creditTerm = creditTerm;
	}

	public BigDecimal getCreditLimit() {
		return this.creditLimit;
	}

	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getCreditLimitAndCurrency() {
		return this.creditLimitAndCurrency;
	}

	public void setCreditLimitAndCurrency(String creditLimitAndCurrency) {
		this.creditLimitAndCurrency = creditLimitAndCurrency;
	}

	public String getCreditCurr() {
		return this.creditCurr;
	}

	public void setCreditCurr(String creditCurr) {
		this.creditCurr = creditCurr;
	}

	public BigDecimal getCreditUtil() {
		return this.creditUtil;
	}

	public void setCreditUtil(BigDecimal creditUtil) {
		this.creditUtil = creditUtil;
	}

	public String getCreditUtilAndPercent() {
		return this.creditUtilAndPercent;
	}

	public void setCreditUtilAndPercent(String creditUtilAndPercent) {
		this.creditUtilAndPercent = creditUtilAndPercent;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public CustomerStatus getStatus() {
		return this.status;
	}

	public void setStatus(CustomerStatus status) {
		this.status = status;
	}

	public CustomerType getType() {
		return this.type;
	}

	public void setType(CustomerType type) {
		this.type = type;
	}

	public String getPhoneNo() {
		return this.phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getFaxNo() {
		return this.faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDateOfInc() {
		return this.dateOfInc;
	}

	public void setDateOfInc(Date dateOfInc) {
		this.dateOfInc = dateOfInc;
	}

	public NatureOfBusiness getNatureOfBusiness() {
		return this.natureOfBusiness;
	}

	public void setNatureOfBusiness(NatureOfBusiness natureOfBusiness) {
		this.natureOfBusiness = natureOfBusiness;
	}

	public String getTradeLicense() {
		return this.tradeLicense;
	}

	public void setTradeLicense(String tradeLicense) {
		this.tradeLicense = tradeLicense;
	}

	public Date getTlValidFrom() {
		return this.tlValidFrom;
	}

	public void setTlValidFrom(Date tlValidFrom) {
		this.tlValidFrom = tlValidFrom;
	}

	public Date getTlValidTo() {
		return this.tlValidTo;
	}

	public void setTlValidTo(Date tlValidTo) {
		this.tlValidTo = tlValidTo;
	}

	public String getAoc() {
		return this.aoc;
	}

	public void setAoc(String aoc) {
		this.aoc = aoc;
	}

	public Date getAocValidFrom() {
		return this.aocValidFrom;
	}

	public void setAocValidFrom(Date aocValidFrom) {
		this.aocValidFrom = aocValidFrom;
	}

	public Date getAocValidTo() {
		return this.aocValidTo;
	}

	public void setAocValidTo(Date aocValidTo) {
		this.aocValidTo = aocValidTo;
	}

	public Boolean getIsCustomer() {
		return this.isCustomer;
	}

	public void setIsCustomer(Boolean isCustomer) {
		this.isCustomer = isCustomer;
	}

	public Boolean getUseForBilling() {
		return this.useForBilling;
	}

	public void setUseForBilling(Boolean useForBilling) {
		this.useForBilling = useForBilling;
	}

	public CustomerDataTransmissionStatus getTransmissionStatus() {
		return this.transmissionStatus;
	}

	public void setTransmissionStatus(
			CustomerDataTransmissionStatus transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public CustomerDataProcessedStatus getProcessedStatus() {
		return this.processedStatus;
	}

	public void setProcessedStatus(CustomerDataProcessedStatus processedStatus) {
		this.processedStatus = processedStatus;
	}

	public CreditUpdateRequestStatus getCreditUpdateRequestStatus() {
		return this.creditUpdateRequestStatus;
	}

	public void setCreditUpdateRequestStatus(
			CreditUpdateRequestStatus creditUpdateRequestStatus) {
		this.creditUpdateRequestStatus = creditUpdateRequestStatus;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}

	public String getDummyField() {
		return this.dummyField;
	}

	public void setDummyField(String dummyField) {
		this.dummyField = dummyField;
	}
}
