/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.user.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeWithCode_Ds;
import atraxo.fmbas.domain.impl.user.RoleSupp;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = RoleSupp.class, jpqlWhere = "e.active = true", sort = {@SortField(field = RoleSuppActive_Ds.F_CODE)})
public class RoleSuppActive_Ds extends AbstractTypeWithCode_Ds<RoleSupp> {

	public static final String ALIAS = "fmbas_RoleSuppActive_Ds";

	/**
	 * Default constructor
	 */
	public RoleSuppActive_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public RoleSuppActive_Ds(RoleSupp e) {
		super(e);
	}
}
