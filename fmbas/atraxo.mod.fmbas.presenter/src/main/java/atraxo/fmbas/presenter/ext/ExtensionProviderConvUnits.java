package atraxo.fmbas.presenter.ext;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.extensions.IExtensionContentProvider;
import seava.j4e.api.extensions.IExtensions;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterBaseService;

/**
 * @author tlukacs
 */
public class ExtensionProviderConvUnits extends AbstractPresenterBaseService implements IExtensionContentProvider {

	@Override
	public String getContent(String targetFrame) throws Exception {

		if (IExtensions.UI_EXTJS_MAIN.equals(targetFrame)) {
			List<Unit> units = this.listUnit();
			return this.addElements(units);
		}
		return null;
	}

	protected String addElements(List<Unit> units) throws Exception {

		UnitType unittype;
		String utc;
		String code;
		BigDecimal factor;
		StringBuilder sb = new StringBuilder();

		sb.append("_CONV_UNITS_ = [ ");

		for (Unit u : units) {
			code = u.getCode();
			factor = u.getFactor();
			unittype = u.getUnittypeInd();
			switch (unittype) {
			case _MASS_:
				utc = "M";
				break;
			case _VOLUME_:
				utc = "V";
				break;
			case _PERCENT_:
				utc = "P";
				break;
			case _EVENT_:
				utc = "E";
				break;
			case _TEMPERATURE_:
				utc = "T";
				break;
			case _DURATION_:
				utc = "D";
				break;
			case _LENGTH_:
				utc = "L";
				break;
			default:
				utc = "";
			}
			sb.append("{code:'");
			sb.append(code);
			sb.append("',utc:'");
			sb.append(utc);
			sb.append("',factor:");
			sb.append(factor.toString());
			sb.append("},");
		}

		int ind = sb.lastIndexOf(",");
		if (ind > 0) {
			sb.replace(ind, ind + 1, "");
		}

		sb.append("];");

		return sb.toString();
	}

	private List<Unit> listUnit() throws Exception {

		IUnitService srv = (IUnitService) this.findEntityService(Unit.class);
		EntityManager em = srv.getEntityManager();

		String getUnit = "SELECT e FROM " + Unit.class.getSimpleName() + " e WHERE e.clientId=:clientId";
		TypedQuery<Unit> unitQuery = em.createQuery(getUnit, Unit.class);
		unitQuery.setParameter("clientId", Session.user.get().getClientId());

		return unitQuery.getResultList();
	}

}
