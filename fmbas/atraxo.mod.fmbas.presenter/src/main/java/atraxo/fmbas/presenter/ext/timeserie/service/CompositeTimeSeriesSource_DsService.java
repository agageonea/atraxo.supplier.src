/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.timeserie.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.InvalidCompositeTimeSeries;
import atraxo.fmbas.domain.impl.timeserie.CompositeTimeSeriesSource;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.presenter.impl.timeserie.model.CompositeTimeSeriesSource_Ds;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class CompositeTimeSeriesSource_DsService
		extends AbstractEntityDsService<CompositeTimeSeriesSource_Ds, CompositeTimeSeriesSource_Ds, Object, CompositeTimeSeriesSource>
		implements IDsService<CompositeTimeSeriesSource_Ds, CompositeTimeSeriesSource_Ds, Object> {

	@Autowired
	private ITimeSerieService timeSerieService;

	@Override
	protected void preInsert(CompositeTimeSeriesSource_Ds ds, Object params) throws Exception {
		super.preInsert(ds, params);
		this.validateInputData(ds);
	}

	@Override
	protected void preUpdate(CompositeTimeSeriesSource_Ds ds, Object params) throws Exception {
		super.preUpdate(ds, params);
		this.validateInputData(ds);
	}

	private void validateInputData(CompositeTimeSeriesSource_Ds ds) throws InvalidCompositeTimeSeries {
		if (ds.getWeighting() == null || ds.getFactor() == null) {
			throw new InvalidCompositeTimeSeries(BusinessErrorCode.COMP_TIME_SERIES_NULL_FIELDS);
		}
		if (ds.getWeighting() <= 0 || ds.getWeighting() >= 100) {
			throw new InvalidCompositeTimeSeries(BusinessErrorCode.COMP_TIME_SERIES_INV_WEIGHTING);

		}

	}

	@Override
	protected void preUpdate(List<CompositeTimeSeriesSource_Ds> list, Object params) throws Exception {
		super.preUpdate(list, params);
		this.validateDuplicateCompositeTSSource(list);
	}

	@Override
	protected void preInsert(List<CompositeTimeSeriesSource_Ds> inputList, Object params) throws Exception {
		super.preInsert(inputList, params);
		List<CompositeTimeSeriesSource_Ds> dbList = this.getSourcesFromDb(inputList.get(0));

		List<CompositeTimeSeriesSource_Ds> mergedList = new ArrayList<>(inputList);
		mergedList.addAll(dbList);
		mergedList
				.sort((CompositeTimeSeriesSource_Ds o1, CompositeTimeSeriesSource_Ds o2) -> o1.getTimeSerieName().compareTo(o2.getTimeSerieName()));

		this.validateListForDuplicates(mergedList);
	}

	private List<CompositeTimeSeriesSource_Ds> getSourcesFromDb(CompositeTimeSeriesSource_Ds ds) throws Exception {
		TimeSerie ts = this.timeSerieService.findById(ds.getTimeSerieCompId());
		List<CompositeTimeSeriesSource> entitiesList = new ArrayList<>(ts.getCompositeTimeSeries());
		return this.getConverter().entitiesToModels(entitiesList, this.getEntityService().getEntityManager(), null);

	}

	private void validateDuplicateCompositeTSSource(List<CompositeTimeSeriesSource_Ds> inputList) throws Exception {
		List<CompositeTimeSeriesSource_Ds> dbList = this.getSourcesFromDb(inputList.get(0));

		Map<Integer, CompositeTimeSeriesSource_Ds> helperMap = dbList.stream()
				.collect(Collectors.toMap(CompositeTimeSeriesSource_Ds::getId, o -> o));
		helperMap.putAll(inputList.stream().collect(Collectors.toMap(CompositeTimeSeriesSource_Ds::getId, o -> o)));

		List<CompositeTimeSeriesSource_Ds> mergedList = new ArrayList<>(helperMap.values());
		mergedList
				.sort((CompositeTimeSeriesSource_Ds o1, CompositeTimeSeriesSource_Ds o2) -> o1.getTimeSerieName().compareTo(o2.getTimeSerieName()));

		this.validateListForDuplicates(mergedList);
	}

	private void validateListForDuplicates(List<CompositeTimeSeriesSource_Ds> suspectedSources) throws BusinessException {
		Set<String> detectedDuplicates = new HashSet<>();
		for (int i = 0; i < suspectedSources.size() - 1; i++) {
			if (suspectedSources.get(i).getTimeSerieName().equals(suspectedSources.get(i + 1).getTimeSerieName())) {
				detectedDuplicates.add(suspectedSources.get(i).getTimeSerieName());
			}
		}
		if (!detectedDuplicates.isEmpty()) {
			String duplicateDistinctString = StringUtils.join(detectedDuplicates, ",");
			String errMsg = String.format(BusinessErrorCode.COMP_TIME_SERIES_DUPLICATE_COMPONENT.getErrMsg(), duplicateDistinctString);
			throw new BusinessException(BusinessErrorCode.COMP_TIME_SERIES_DUPLICATE_COMPONENT, errMsg);
		}
	}

}
