/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.geo.model;

import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;
import seava.j4e.api.model.IModelWithId;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Country.class, sort = {@SortField(field = CountryLov_Ds.F_CODE)})
public class CountryLov_Ds extends AbstractLov_Ds<Country>
		implements
			IModelWithId<Integer> {

	public static final String ALIAS = "fmbas_CountryLov_Ds";

	public static final String F_ID = "id";
	public static final String F_COUNTRYID = "countryId";
	public static final String F_COUNTRYCODE = "countryCode";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_ACTIVE = "active";

	@DsField
	private Integer id;

	@DsField(join = "left", path = "country.id")
	private Integer countryId;

	@DsField(join = "left", path = "country.code")
	private String countryCode;

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public CountryLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public CountryLov_Ds(Country e) {
		super(e);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
