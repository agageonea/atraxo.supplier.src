/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.externalInterfaces.model;

/**
 * Generated code. Do not modify in this file.
 */
public class ExternalInterfaceMessageHistory_DsParam {

	public static final String f_messageContent = "messageContent";

	private String messageContent;

	public String getMessageContent() {
		return this.messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}
}
