/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.workflow.service;

import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;
import atraxo.fmbas.presenter.impl.workflow.model.WorkflowParameter_Ds;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service WorkflowParameter_DsService
 */
public class WorkflowParameter_DsService
		extends
			AbstractEntityDsService<WorkflowParameter_Ds, WorkflowParameter_Ds, Object, WorkflowParameter>
		implements
			IDsService<WorkflowParameter_Ds, WorkflowParameter_Ds, Object> {

	// Implement me ...

}
