/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.categories.model;

import atraxo.fmbas.domain.impl.categories.IataPC;
import atraxo.fmbas.domain.impl.fmbas_type.ProductSource;
import atraxo.fmbas.domain.impl.fmbas_type.Use;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = IataPC.class, sort = {@SortField(field = IataPC_Ds.F_CODE)})
public class IataPC_Ds extends AbstractDs_Ds<IataPC> {

	public static final String ALIAS = "fmbas_IataPC_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_USE = "use";
	public static final String F_ACTIVE = "active";
	public static final String F_SOURCE = "source";
	public static final String F_SOURCENOTES = "sourceNotes";

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private Use use;

	@DsField
	private Boolean active;

	@DsField
	private ProductSource source;

	@DsField
	private String sourceNotes;

	/**
	 * Default constructor
	 */
	public IataPC_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public IataPC_Ds(IataPC e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Use getUse() {
		return this.use;
	}

	public void setUse(Use use) {
		this.use = use;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public ProductSource getSource() {
		return this.source;
	}

	public void setSource(ProductSource source) {
		this.source = source;
	}

	public String getSourceNotes() {
		return this.sourceNotes;
	}

	public void setSourceNotes(String sourceNotes) {
		this.sourceNotes = sourceNotes;
	}
}
