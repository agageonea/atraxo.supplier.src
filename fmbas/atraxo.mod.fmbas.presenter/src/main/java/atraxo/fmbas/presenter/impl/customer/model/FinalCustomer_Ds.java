/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CreditUpdateRequestStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataProcessedStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataTransmissionStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerType;
import atraxo.fmbas.domain.impl.fmbas_type.NatureOfBusiness;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Customer.class, sort = {@SortField(field = FinalCustomer_Ds.F_CODE)})
public class FinalCustomer_Ds extends AbstractDs_Ds<Customer> {

	public static final String ALIAS = "fmbas_FinalCustomer_Ds";

	public static final String F_ISTHIRDPARTY = "isThirdParty";
	public static final String F_NAME = "name";
	public static final String F_CODE = "code";
	public static final String F_IATACODE = "iataCode";
	public static final String F_ICAOCODE = "icaoCode";
	public static final String F_NATUREOFBUSINESS = "natureOfBusiness";
	public static final String F_STATUS = "status";
	public static final String F_TYPE = "type";
	public static final String F_TRANSMISSIONSTATUS = "transmissionStatus";
	public static final String F_PROCESSEDSTATUS = "processedStatus";
	public static final String F_CREDITUPDATEREQUESTSTATUS = "creditUpdateRequestStatus";
	public static final String F_LABELFIELD = "labelField";

	@DsField
	private Boolean isThirdParty;

	@DsField
	private String name;

	@DsField
	private String code;

	@DsField
	private String iataCode;

	@DsField
	private String icaoCode;

	@DsField
	private NatureOfBusiness natureOfBusiness;

	@DsField
	private CustomerStatus status;

	@DsField
	private CustomerType type;

	@DsField
	private CustomerDataTransmissionStatus transmissionStatus;

	@DsField
	private CustomerDataProcessedStatus processedStatus;

	@DsField
	private CreditUpdateRequestStatus creditUpdateRequestStatus;

	@DsField(fetch = false)
	private String labelField;

	/**
	 * Default constructor
	 */
	public FinalCustomer_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FinalCustomer_Ds(Customer e) {
		super(e);
	}

	public Boolean getIsThirdParty() {
		return this.isThirdParty;
	}

	public void setIsThirdParty(Boolean isThirdParty) {
		this.isThirdParty = isThirdParty;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public String getIcaoCode() {
		return this.icaoCode;
	}

	public void setIcaoCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}

	public NatureOfBusiness getNatureOfBusiness() {
		return this.natureOfBusiness;
	}

	public void setNatureOfBusiness(NatureOfBusiness natureOfBusiness) {
		this.natureOfBusiness = natureOfBusiness;
	}

	public CustomerStatus getStatus() {
		return this.status;
	}

	public void setStatus(CustomerStatus status) {
		this.status = status;
	}

	public CustomerType getType() {
		return this.type;
	}

	public void setType(CustomerType type) {
		this.type = type;
	}

	public CustomerDataTransmissionStatus getTransmissionStatus() {
		return this.transmissionStatus;
	}

	public void setTransmissionStatus(
			CustomerDataTransmissionStatus transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public CustomerDataProcessedStatus getProcessedStatus() {
		return this.processedStatus;
	}

	public void setProcessedStatus(CustomerDataProcessedStatus processedStatus) {
		this.processedStatus = processedStatus;
	}

	public CreditUpdateRequestStatus getCreditUpdateRequestStatus() {
		return this.creditUpdateRequestStatus;
	}

	public void setCreditUpdateRequestStatus(
			CreditUpdateRequestStatus creditUpdateRequestStatus) {
		this.creditUpdateRequestStatus = creditUpdateRequestStatus;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}
}
