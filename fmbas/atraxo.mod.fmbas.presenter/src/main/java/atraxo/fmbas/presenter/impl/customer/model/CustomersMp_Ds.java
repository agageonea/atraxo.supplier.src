/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.BusinessType;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerType;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Customer.class, jpqlWhere = "e.isSubsidiary = true", sort = {@SortField(field = CustomersMp_Ds.F_CODE)})
@RefLookups({
		@RefLookup(refId = CustomersMp_Ds.F_BUYERID, namedQuery = UserSupp.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = CustomersMp_Ds.F_BUYERCODE)}),
		@RefLookup(refId = CustomersMp_Ds.F_BILLINGCOUNTRYID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = CustomersMp_Ds.F_BILLINGCOUNTRYCODE)}),
		@RefLookup(refId = CustomersMp_Ds.F_OFFICECOUNTRYID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = CustomersMp_Ds.F_OFFICECOUNTRYCODE)}),
		@RefLookup(refId = CustomersMp_Ds.F_BILLINGSTATEID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = CustomersMp_Ds.F_BILLINGSTATECODE)}),
		@RefLookup(refId = CustomersMp_Ds.F_OFFICESTATEID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = CustomersMp_Ds.F_OFFICESTATECODE)})})
public class CustomersMp_Ds extends AbstractDs_Ds<Customer> {

	public static final String ALIAS = "fmbas_CustomersMp_Ds";

	public static final String F_IATACODE = "iataCode";
	public static final String F_ICAOCODE = "icaoCode";
	public static final String F_NAME = "name";
	public static final String F_CODE = "code";
	public static final String F_STATUS = "status";
	public static final String F_TYPE = "type";
	public static final String F_PHONENO = "phoneNo";
	public static final String F_DATEOFINC = "dateOfInc";
	public static final String F_ISCUSTOMER = "isCustomer";
	public static final String F_ISSUPPLIER = "isSupplier";
	public static final String F_ISSUBSIDIARY = "isSubsidiary";
	public static final String F_FUELSUPPLIER = "fuelSupplier";
	public static final String F_ACCOUNTNUMBER = "accountNumber";
	public static final String F_FAXNO = "faxNo";
	public static final String F_WEBSITE = "website";
	public static final String F_OFFICESTREET = "officeStreet";
	public static final String F_OFFICECITY = "officeCity";
	public static final String F_OFFICEPOSTALCODE = "officePostalCode";
	public static final String F_BILLINGSTREET = "billingStreet";
	public static final String F_BILLINGCITY = "billingCity";
	public static final String F_BILLINGPOSTALCODE = "billingPostalCode";
	public static final String F_USEFORBILLING = "useForBilling";
	public static final String F_BUSINESS = "business";
	public static final String F_BUYERID = "buyerId";
	public static final String F_BUYERNAME = "buyerName";
	public static final String F_BUYERCODE = "buyerCode";
	public static final String F_OFFICECOUNTRYID = "officeCountryId";
	public static final String F_OFFICECOUNTRYCODE = "officeCountryCode";
	public static final String F_OFFICECOUNTRYNAME = "officeCountryName";
	public static final String F_OFFICESTATEID = "officeStateId";
	public static final String F_OFFICESTATECODE = "officeStateCode";
	public static final String F_OFFICESTATENAME = "officeStateName";
	public static final String F_BILLINGCOUNTRYID = "billingCountryId";
	public static final String F_BILLINGCOUNTRYCODE = "billingCountryCode";
	public static final String F_BILLINGCOUNTRYNAME = "billingCountryName";
	public static final String F_BILLINGSTATEID = "billingStateId";
	public static final String F_BILLINGSTATECODE = "billingStateCode";
	public static final String F_BILLINGSTATENAME = "billingStateName";
	public static final String F_CONTACTNAME = "contactName";
	public static final String F_LABELFIELD = "labelField";
	public static final String F_DUMMYFIELD = "dummyField";
	public static final String F_SEPARATOR = "separator";

	@DsField
	private String iataCode;

	@DsField
	private String icaoCode;

	@DsField
	private String name;

	@DsField
	private String code;

	@DsField
	private CustomerStatus status;

	@DsField
	private CustomerType type;

	@DsField
	private String phoneNo;

	@DsField
	private Date dateOfInc;

	@DsField
	private Boolean isCustomer;

	@DsField
	private Boolean isSupplier;

	@DsField
	private Boolean isSubsidiary;

	@DsField
	private Boolean fuelSupplier;

	@DsField
	private String accountNumber;

	@DsField
	private String faxNo;

	@DsField
	private String website;

	@DsField
	private String officeStreet;

	@DsField
	private String officeCity;

	@DsField
	private String officePostalCode;

	@DsField
	private String billingStreet;

	@DsField
	private String billingCity;

	@DsField
	private String billingPostalCode;

	@DsField
	private Boolean useForBilling;

	@DsField(fetch = false)
	private BusinessType business;

	@DsField(join = "left", path = "responsibleBuyer.id")
	private String buyerId;

	@DsField(join = "left", path = "responsibleBuyer.name")
	private String buyerName;

	@DsField(join = "left", path = "responsibleBuyer.code")
	private String buyerCode;

	@DsField(join = "left", path = "officeCountry.id")
	private Integer officeCountryId;

	@DsField(join = "left", path = "officeCountry.code")
	private String officeCountryCode;

	@DsField(join = "left", path = "officeCountry.name")
	private String officeCountryName;

	@DsField(join = "left", path = "officeState.id")
	private Integer officeStateId;

	@DsField(join = "left", path = "officeState.code")
	private String officeStateCode;

	@DsField(join = "left", path = "officeState.name")
	private String officeStateName;

	@DsField(join = "left", path = "billingCountry.id")
	private Integer billingCountryId;

	@DsField(join = "left", path = "billingCountry.code")
	private String billingCountryCode;

	@DsField(join = "left", path = "billingCountry.name")
	private String billingCountryName;

	@DsField(join = "left", path = "billingState.id")
	private Integer billingStateId;

	@DsField(join = "left", path = "billingState.code")
	private String billingStateCode;

	@DsField(join = "left", path = "billingState.name")
	private String billingStateName;

	@DsField(fetch = false)
	private String contactName;

	@DsField(fetch = false)
	private String labelField;

	@DsField(fetch = false)
	private String dummyField;

	@DsField(fetch = false)
	private String separator;

	/**
	 * Default constructor
	 */
	public CustomersMp_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public CustomersMp_Ds(Customer e) {
		super(e);
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public String getIcaoCode() {
		return this.icaoCode;
	}

	public void setIcaoCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public CustomerStatus getStatus() {
		return this.status;
	}

	public void setStatus(CustomerStatus status) {
		this.status = status;
	}

	public CustomerType getType() {
		return this.type;
	}

	public void setType(CustomerType type) {
		this.type = type;
	}

	public String getPhoneNo() {
		return this.phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public Date getDateOfInc() {
		return this.dateOfInc;
	}

	public void setDateOfInc(Date dateOfInc) {
		this.dateOfInc = dateOfInc;
	}

	public Boolean getIsCustomer() {
		return this.isCustomer;
	}

	public void setIsCustomer(Boolean isCustomer) {
		this.isCustomer = isCustomer;
	}

	public Boolean getIsSupplier() {
		return this.isSupplier;
	}

	public void setIsSupplier(Boolean isSupplier) {
		this.isSupplier = isSupplier;
	}

	public Boolean getIsSubsidiary() {
		return this.isSubsidiary;
	}

	public void setIsSubsidiary(Boolean isSubsidiary) {
		this.isSubsidiary = isSubsidiary;
	}

	public Boolean getFuelSupplier() {
		return this.fuelSupplier;
	}

	public void setFuelSupplier(Boolean fuelSupplier) {
		this.fuelSupplier = fuelSupplier;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getFaxNo() {
		return this.faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getOfficeStreet() {
		return this.officeStreet;
	}

	public void setOfficeStreet(String officeStreet) {
		this.officeStreet = officeStreet;
	}

	public String getOfficeCity() {
		return this.officeCity;
	}

	public void setOfficeCity(String officeCity) {
		this.officeCity = officeCity;
	}

	public String getOfficePostalCode() {
		return this.officePostalCode;
	}

	public void setOfficePostalCode(String officePostalCode) {
		this.officePostalCode = officePostalCode;
	}

	public String getBillingStreet() {
		return this.billingStreet;
	}

	public void setBillingStreet(String billingStreet) {
		this.billingStreet = billingStreet;
	}

	public String getBillingCity() {
		return this.billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public String getBillingPostalCode() {
		return this.billingPostalCode;
	}

	public void setBillingPostalCode(String billingPostalCode) {
		this.billingPostalCode = billingPostalCode;
	}

	public Boolean getUseForBilling() {
		return this.useForBilling;
	}

	public void setUseForBilling(Boolean useForBilling) {
		this.useForBilling = useForBilling;
	}

	public BusinessType getBusiness() {
		return this.business;
	}

	public void setBusiness(BusinessType business) {
		this.business = business;
	}

	public String getBuyerId() {
		return this.buyerId;
	}

	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}

	public String getBuyerName() {
		return this.buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getBuyerCode() {
		return this.buyerCode;
	}

	public void setBuyerCode(String buyerCode) {
		this.buyerCode = buyerCode;
	}

	public Integer getOfficeCountryId() {
		return this.officeCountryId;
	}

	public void setOfficeCountryId(Integer officeCountryId) {
		this.officeCountryId = officeCountryId;
	}

	public String getOfficeCountryCode() {
		return this.officeCountryCode;
	}

	public void setOfficeCountryCode(String officeCountryCode) {
		this.officeCountryCode = officeCountryCode;
	}

	public String getOfficeCountryName() {
		return this.officeCountryName;
	}

	public void setOfficeCountryName(String officeCountryName) {
		this.officeCountryName = officeCountryName;
	}

	public Integer getOfficeStateId() {
		return this.officeStateId;
	}

	public void setOfficeStateId(Integer officeStateId) {
		this.officeStateId = officeStateId;
	}

	public String getOfficeStateCode() {
		return this.officeStateCode;
	}

	public void setOfficeStateCode(String officeStateCode) {
		this.officeStateCode = officeStateCode;
	}

	public String getOfficeStateName() {
		return this.officeStateName;
	}

	public void setOfficeStateName(String officeStateName) {
		this.officeStateName = officeStateName;
	}

	public Integer getBillingCountryId() {
		return this.billingCountryId;
	}

	public void setBillingCountryId(Integer billingCountryId) {
		this.billingCountryId = billingCountryId;
	}

	public String getBillingCountryCode() {
		return this.billingCountryCode;
	}

	public void setBillingCountryCode(String billingCountryCode) {
		this.billingCountryCode = billingCountryCode;
	}

	public String getBillingCountryName() {
		return this.billingCountryName;
	}

	public void setBillingCountryName(String billingCountryName) {
		this.billingCountryName = billingCountryName;
	}

	public Integer getBillingStateId() {
		return this.billingStateId;
	}

	public void setBillingStateId(Integer billingStateId) {
		this.billingStateId = billingStateId;
	}

	public String getBillingStateCode() {
		return this.billingStateCode;
	}

	public void setBillingStateCode(String billingStateCode) {
		this.billingStateCode = billingStateCode;
	}

	public String getBillingStateName() {
		return this.billingStateName;
	}

	public void setBillingStateName(String billingStateName) {
		this.billingStateName = billingStateName;
	}

	public String getContactName() {
		return this.contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}

	public String getDummyField() {
		return this.dummyField;
	}

	public void setDummyField(String dummyField) {
		this.dummyField = dummyField;
	}

	public String getSeparator() {
		return this.separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}
}
