/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.templates.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import atraxo.fmbas.domain.impl.templates.Template;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Template.class)
@RefLookups({
		@RefLookup(refId = Templates_Ds.F_USERID, namedQuery = UserSupp.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Templates_Ds.F_USERCODE)}),
		@RefLookup(refId = Templates_Ds.F_SUBSIDIARYID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Templates_Ds.F_SUBSIDIARYCODE)}),
		@RefLookup(refId = Templates_Ds.F_TYPEID, namedQuery = NotificationEvent.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = Templates_Ds.F_TYPENAME)})})
public class Templates_Ds extends AbstractDs_Ds<Template> {

	public static final String ALIAS = "fmbas_Templates_Ds";

	public static final String F_USERID = "userId";
	public static final String F_USERNAME = "userName";
	public static final String F_USERCODE = "userCode";
	public static final String F_TYPEID = "typeId";
	public static final String F_TYPENAME = "typeName";
	public static final String F_SUBSIDIARYID = "subsidiaryId";
	public static final String F_SUBSIDIARYCODE = "subsidiaryCode";
	public static final String F_SUBSIDIARYNAME = "subsidiaryName";
	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_FILENAME = "fileName";
	public static final String F_UPLOADDATE = "uploadDate";
	public static final String F_FILEREFERENCE = "fileReference";
	public static final String F_SYSTEMUSE = "systemUse";
	public static final String F_FILE = "file";

	@DsField(join = "left", path = "user.id")
	private String userId;

	@DsField(join = "left", path = "user.name")
	private String userName;

	@DsField(join = "left", path = "user.code")
	private String userCode;

	@DsField(join = "left", path = "type.id")
	private Integer typeId;

	@DsField(join = "left", path = "type.name")
	private String typeName;

	@DsField(join = "left", path = "subsidiary.id")
	private Integer subsidiaryId;

	@DsField(join = "left", path = "subsidiary.code")
	private String subsidiaryCode;

	@DsField(join = "left", path = "subsidiary.name")
	private String subsidiaryName;

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private String fileName;

	@DsField
	private Date uploadDate;

	@DsField
	private String fileReference;

	@DsField
	private Boolean systemUse;

	@DsField(fetch = false)
	private String file;

	/**
	 * Default constructor
	 */
	public Templates_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Templates_Ds(Template e) {
		super(e);
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserCode() {
		return this.userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public Integer getTypeId() {
		return this.typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return this.typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getSubsidiaryId() {
		return this.subsidiaryId;
	}

	public void setSubsidiaryId(Integer subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}

	public String getSubsidiaryCode() {
		return this.subsidiaryCode;
	}

	public void setSubsidiaryCode(String subsidiaryCode) {
		this.subsidiaryCode = subsidiaryCode;
	}

	public String getSubsidiaryName() {
		return this.subsidiaryName;
	}

	public void setSubsidiaryName(String subsidiaryName) {
		this.subsidiaryName = subsidiaryName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getUploadDate() {
		return this.uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getFileReference() {
		return this.fileReference;
	}

	public void setFileReference(String fileReference) {
		this.fileReference = fileReference;
	}

	public Boolean getSystemUse() {
		return this.systemUse;
	}

	public void setSystemUse(Boolean systemUse) {
		this.systemUse = systemUse;
	}

	public String getFile() {
		return this.file;
	}

	public void setFile(String file) {
		this.file = file;
	}
}
