/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.documentNumberSeries.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atraxo.fmbas.business.api.documentNumberSeries.IDocumentNumberSeriesService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.documentNumberSeries.DocumentNumberSeries;
import atraxo.fmbas.presenter.impl.documentNumberSeries.model.DocumentNumberSeries_Ds;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class DocumentNumberSeries_DsService
		extends AbstractEntityDsService<DocumentNumberSeries_Ds, DocumentNumberSeries_Ds, Object, DocumentNumberSeries>
		implements IDsService<DocumentNumberSeries_Ds, DocumentNumberSeries_Ds, Object> {

	@Override
	protected void preUpdate(DocumentNumberSeries_Ds ds, Object params) throws Exception {
		super.preUpdate(ds, params);
		this.validateEndIndex(ds);
		this.enable(ds);
	}

	@Override
	protected void preInsert(DocumentNumberSeries_Ds ds, Object params) throws Exception {
		super.preInsert(ds, params);
		if (ds.getCurrentIndex() == null) {
			ds.setCurrentIndex(ds.getStartIndex());
		}
		this.validateStartIndex(ds);
		this.validateEndIndex(ds);
		this.enable(ds);
	}

	private void validateEndIndex(DocumentNumberSeries_Ds ds) throws BusinessException {
		if (ds.getStartIndex().compareTo(ds.getEndIndex()) >= 0) {
			throw new BusinessException(BusinessErrorCode.INVALID_INDEX, BusinessErrorCode.INVALID_INDEX.getErrMsg());
		}
	}

	private void validateStartIndex(DocumentNumberSeries_Ds ds) throws BusinessException {
		if (ds.getCurrentIndex().compareTo(ds.getStartIndex()) > 0) {
			throw new BusinessException(BusinessErrorCode.INVALID_START_INDEX, BusinessErrorCode.INVALID_START_INDEX.getErrMsg());
		}
	}

	private void enable(DocumentNumberSeries_Ds ds) throws Exception, BusinessException {
		if (ds.getEnabled()) {
			IDocumentNumberSeriesService srv = (IDocumentNumberSeriesService) this.findEntityService(DocumentNumberSeries.class);
			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put("enabled", Boolean.TRUE);
			List<DocumentNumberSeries> list = srv.findEntitiesByAttributes(paramMap);
			if (!list.isEmpty()) {
				Iterator<DocumentNumberSeries> iter = list.iterator();
				while (iter.hasNext()) {
					DocumentNumberSeries serie = iter.next();
					if (!serie.getId().equals(ds.getId()) && serie.getType().equals(ds.getType())) {
						serie.setEnabled(false);
					} else {
						iter.remove();
					}
				}
				srv.update(list);
			}
		}
	}
}
