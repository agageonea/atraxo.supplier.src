/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.timeserie.cnv;

import javax.persistence.EntityManager;

import atraxo.fmbas.domain.impl.fmbas_type.ValueType;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.presenter.impl.timeserie.model.EnergyPriceTimeSerie_Ds;
import seava.j4e.api.action.result.IDsConverter;
import seava.j4e.presenter.converter.AbstractDsConverter;

/**
 * Custom converter EnergyPriceTimeSerie_DsCnv
 */
public class EnergyPriceTimeSerie_DsCnv extends AbstractDsConverter<EnergyPriceTimeSerie_Ds, TimeSerie>
		implements IDsConverter<EnergyPriceTimeSerie_Ds, TimeSerie> {

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.presenter.converter.AbstractDsConverter#modelToEntity(java.lang.Object, java.lang.Object, boolean,
	 * javax.persistence.EntityManager)
	 */
	@Override
	public void modelToEntity(EnergyPriceTimeSerie_Ds m, TimeSerie e, boolean isInsert, EntityManager em) throws Exception {
		super.modelToEntity(m, e, isInsert, em);
		if (e.getValueType() == null) {
			e.setValueType(ValueType._USER_CALCULATED_PRICE_);
		}
	}
}
