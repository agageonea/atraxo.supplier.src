/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.tolerance.model;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.fmbas_type.ParamType;
import atraxo.fmbas.domain.impl.fmbas_type.TolLimits;
import atraxo.fmbas.domain.impl.fmbas_type.ToleranceType;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Tolerance.class, sort = {@SortField(field = Tolerance_Ds.F_NAME)})
@RefLookups({
		@RefLookup(refId = Tolerance_Ds.F_UNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Tolerance_Ds.F_UNITCODE)}),
		@RefLookup(refId = Tolerance_Ds.F_CURRID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Tolerance_Ds.F_CURRCODE)})})
public class Tolerance_Ds extends AbstractDs_Ds<Tolerance> {

	public static final String ALIAS = "fmbas_Tolerance_Ds";

	public static final String F_PERCENT = "percent";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_CURRID = "currId";
	public static final String F_CURRCODE = "currCode";
	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_PARAMETERTYPE = "parameterType";
	public static final String F_TOLERANCEFOR = "toleranceFor";
	public static final String F_ABSDEVDOWN = "absDevDown";
	public static final String F_ABSDEVUP = "absDevUp";
	public static final String F_RELATIVEDEVDOWN = "relativeDevDown";
	public static final String F_RELATIVEDEVUP = "relativeDevUp";
	public static final String F_TOLERANCETYPE = "toleranceType";
	public static final String F_SYSTEM = "system";
	public static final String F_TOLLMTIND = "tolLmtInd";
	public static final String F_ACTIVE = "active";

	@DsField(fetch = false)
	private String percent;

	@DsField(join = "left", path = "refUnit.id")
	private Integer unitId;

	@DsField(join = "left", path = "refUnit.code")
	private String unitCode;

	@DsField(join = "left", path = "currency.id")
	private Integer currId;

	@DsField(join = "left", path = "currency.code")
	private String currCode;

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private ParamType parameterType;

	@DsField
	private String toleranceFor;

	@DsField
	private BigDecimal absDevDown;

	@DsField
	private BigDecimal absDevUp;

	@DsField
	private BigDecimal relativeDevDown;

	@DsField
	private BigDecimal relativeDevUp;

	@DsField
	private ToleranceType toleranceType;

	@DsField
	private Boolean system;

	@DsField
	private TolLimits tolLmtInd;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public Tolerance_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Tolerance_Ds(Tolerance e) {
		super(e);
	}

	public String getPercent() {
		return this.percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public Integer getCurrId() {
		return this.currId;
	}

	public void setCurrId(Integer currId) {
		this.currId = currId;
	}

	public String getCurrCode() {
		return this.currCode;
	}

	public void setCurrCode(String currCode) {
		this.currCode = currCode;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ParamType getParameterType() {
		return this.parameterType;
	}

	public void setParameterType(ParamType parameterType) {
		this.parameterType = parameterType;
	}

	public String getToleranceFor() {
		return this.toleranceFor;
	}

	public void setToleranceFor(String toleranceFor) {
		this.toleranceFor = toleranceFor;
	}

	public BigDecimal getAbsDevDown() {
		return this.absDevDown;
	}

	public void setAbsDevDown(BigDecimal absDevDown) {
		this.absDevDown = absDevDown;
	}

	public BigDecimal getAbsDevUp() {
		return this.absDevUp;
	}

	public void setAbsDevUp(BigDecimal absDevUp) {
		this.absDevUp = absDevUp;
	}

	public BigDecimal getRelativeDevDown() {
		return this.relativeDevDown;
	}

	public void setRelativeDevDown(BigDecimal relativeDevDown) {
		this.relativeDevDown = relativeDevDown;
	}

	public BigDecimal getRelativeDevUp() {
		return this.relativeDevUp;
	}

	public void setRelativeDevUp(BigDecimal relativeDevUp) {
		this.relativeDevUp = relativeDevUp;
	}

	public ToleranceType getToleranceType() {
		return this.toleranceType;
	}

	public void setToleranceType(ToleranceType toleranceType) {
		this.toleranceType = toleranceType;
	}

	public Boolean getSystem() {
		return this.system;
	}

	public void setSystem(Boolean system) {
		this.system = system;
	}

	public TolLimits getTolLmtInd() {
		return this.tolLmtInd;
	}

	public void setTolLmtInd(TolLimits tolLmtInd) {
		this.tolLmtInd = tolLmtInd;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
