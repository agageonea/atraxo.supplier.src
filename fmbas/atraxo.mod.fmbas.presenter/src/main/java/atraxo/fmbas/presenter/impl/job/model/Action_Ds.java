/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.job.model;

import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.BatchJob;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Action.class, sort = {@SortField(field = Action_Ds.F_ORDER)})
@RefLookups({
		@RefLookup(refId = Action_Ds.F_JOBCHAINID),
		@RefLookup(refId = Action_Ds.F_BATCHJOBID, namedQuery = BatchJob.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = Action_Ds.F_BATCHJOBNAME)})})
public class Action_Ds extends AbstractDs_Ds<Action> {

	public static final String ALIAS = "fmbas_Action_Ds";

	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_ORDER = "order";
	public static final String F_BATCHJOBNAME = "batchJobName";
	public static final String F_BATCHJOBID = "batchJobId";
	public static final String F_JOBCHAINID = "jobChainId";

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private Integer order;

	@DsField(join = "left", path = "batchJob.name")
	private String batchJobName;

	@DsField(join = "left", path = "batchJob.id")
	private Integer batchJobId;

	@DsField(join = "left", path = "jobChain.id")
	private Integer jobChainId;

	/**
	 * Default constructor
	 */
	public Action_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Action_Ds(Action e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getOrder() {
		return this.order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public String getBatchJobName() {
		return this.batchJobName;
	}

	public void setBatchJobName(String batchJobName) {
		this.batchJobName = batchJobName;
	}

	public Integer getBatchJobId() {
		return this.batchJobId;
	}

	public void setBatchJobId(Integer batchJobId) {
		this.batchJobId = batchJobId;
	}

	public Integer getJobChainId() {
		return this.jobChainId;
	}

	public void setJobChainId(Integer jobChainId) {
		this.jobChainId = jobChainId;
	}
}
