/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.vat.model;

import atraxo.fmbas.domain.impl.vat.Vat;
import atraxo.fmbas.domain.impl.vat.VatRate;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = VatRate.class, sort = {@SortField(field = VatRate_Ds.F_ID)})
@RefLookups({@RefLookup(refId = VatRate_Ds.F_VATID, namedQuery = Vat.NQ_FIND_BY_CODE_PRIMITIVE, params = {@Param(name = "code", field = VatRate_Ds.F_VATCODE)})})
public class VatRate_Ds extends AbstractDs_Ds<VatRate> {

	public static final String ALIAS = "fmbas_VatRate_Ds";

	public static final String F_VATID = "vatId";
	public static final String F_VATCODE = "vatCode";
	public static final String F_RATE = "rate";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_NOTE = "note";

	@DsField(join = "left", path = "vat.id")
	private Integer vatId;

	@DsField(join = "left", path = "vat.code")
	private String vatCode;

	@DsField
	private BigDecimal rate;

	@DsField
	private Date validFrom;

	@DsField
	private Date validTo;

	@DsField
	private String note;

	/**
	 * Default constructor
	 */
	public VatRate_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public VatRate_Ds(VatRate e) {
		super(e);
	}

	public Integer getVatId() {
		return this.vatId;
	}

	public void setVatId(Integer vatId) {
		this.vatId = vatId;
	}

	public String getVatCode() {
		return this.vatCode;
	}

	public void setVatCode(String vatCode) {
		this.vatCode = vatCode;
	}

	public BigDecimal getRate() {
		return this.rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}
