package atraxo.fmbas.presenter.ext.documentNumberSeries.delegate;

import atraxo.fmbas.presenter.impl.documentNumberSeries.model.DocumentNumberSeries_Ds;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class UpdateSeriesStatus_Pd extends AbstractPresenterDelegate {

	public void enable(DocumentNumberSeries_Ds ds) throws Exception {
		ds.setEnabled(true);
		this.findDsService(DocumentNumberSeries_Ds.class).update(ds, null);
	}

	public void disable(DocumentNumberSeries_Ds ds) throws Exception {
		ds.setEnabled(false);
		this.findDsService(DocumentNumberSeries_Ds.class).update(ds, null);
	}

}
