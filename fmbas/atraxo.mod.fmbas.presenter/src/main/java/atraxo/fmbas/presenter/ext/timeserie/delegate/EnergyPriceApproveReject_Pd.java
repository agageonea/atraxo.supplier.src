package atraxo.fmbas.presenter.ext.timeserie.delegate;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.presenter.ext.timeserie.service.EnergyPriceApproveRejectService;
import atraxo.fmbas.presenter.impl.timeserie.model.EnergyPriceTimeSerie_Ds;
import atraxo.fmbas.presenter.impl.timeserie.model.EnergyPriceTimeSerie_DsParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class EnergyPriceApproveReject_Pd extends AbstractPresenterDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnergyPriceApproveReject_Pd.class);

	/**
	 * Submits for approval an Energy Price time serie
	 *
	 * @param ds
	 * @param params
	 * @throws Exception
	 */
	public void submitForApproval(EnergyPriceTimeSerie_Ds ds, EnergyPriceTimeSerie_DsParam params) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START submitForApproval()");
		}

		EnergyPriceApproveRejectService service = this.getApplicationContext().getBean(EnergyPriceApproveRejectService.class);
		service.submitForApproval(ds, params);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END submitForApproval()");
		}
	}

	/**
	 * Approves current Energy Time Series workflow task
	 *
	 * @param data
	 * @param param
	 * @throws Exception
	 */
	public void approve(EnergyPriceTimeSerie_Ds data, EnergyPriceTimeSerie_DsParam params) throws BusinessException {
		EnergyPriceApproveRejectService service = this.getApplicationContext().getBean(EnergyPriceApproveRejectService.class);
		service.approve(data, params);
	}

	/**
	 * Rejects current Energy Time Serie workflow task
	 *
	 * @param data
	 * @param param
	 * @throws Exception
	 */
	public void reject(EnergyPriceTimeSerie_Ds data, EnergyPriceTimeSerie_DsParam params) throws BusinessException {
		EnergyPriceApproveRejectService service = this.getApplicationContext().getBean(EnergyPriceApproveRejectService.class);
		service.reject(data, params);
	}

	/**
	 * Approves current Energy time series workflow task.
	 *
	 * @param data
	 * @param param
	 * @throws Exception
	 */
	public void approveList(List<EnergyPriceTimeSerie_Ds> data, EnergyPriceTimeSerie_DsParam params) {
		EnergyPriceApproveRejectService service = this.getApplicationContext().getBean(EnergyPriceApproveRejectService.class);
		service.approveList(data, params);
	}

	/**
	 * Rejects current Energy time series workflow task.
	 *
	 * @param data
	 * @param param
	 * @throws Exception
	 */
	public void rejectList(List<EnergyPriceTimeSerie_Ds> data, EnergyPriceTimeSerie_DsParam params) {
		EnergyPriceApproveRejectService service = this.getApplicationContext().getBean(EnergyPriceApproveRejectService.class);
		service.rejectList(data, params);
	}

}
