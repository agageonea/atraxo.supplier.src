/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.user.model;

import atraxo.abstracts.presenter.impl.tenant.model.AbstractTypeWithCode_Ds;
import atraxo.ad.domain.impl.ad.LanguageCodes;
import atraxo.ad.domain.impl.ad.TNumberSeparator;
import atraxo.ad.domain.impl.system.DateFormat;
import atraxo.fmbas.domain.impl.fmbas_type.Department;
import atraxo.fmbas.domain.impl.fmbas_type.Title;
import atraxo.fmbas.domain.impl.fmbas_type.WorkOffice;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = UserSupp.class, sort = {@SortField(field = UserSupp_Ds.F_NAME)})
@RefLookups({
		@RefLookup(refId = UserSupp_Ds.F_DATEFORMATID, namedQuery = DateFormat.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = UserSupp_Ds.F_DATEFORMAT)}),
		@RefLookup(refId = UserSupp_Ds.F_LOCID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = UserSupp_Ds.F_LOCCODE)})})
public class UserSupp_Ds extends AbstractTypeWithCode_Ds<UserSupp> {

	public static final String ALIAS = "fmbas_UserSupp_Ds";

	public static final String F_LOGINNAME = "loginName";
	public static final String F_CODE = "code";
	public static final String F_EMAIL = "email";
	public static final String F_DECIMALSEPARATOR = "decimalSeparator";
	public static final String F_THOUSANDSEPARATOR = "thousandSeparator";
	public static final String F_DATEFORMATID = "dateFormatId";
	public static final String F_DATEFORMAT = "dateFormat";
	public static final String F_LOCID = "locId";
	public static final String F_LOCCODE = "locCode";
	public static final String F_TITLE = "title";
	public static final String F_FIRSTNAME = "firstName";
	public static final String F_LASTNAME = "lastName";
	public static final String F_JOBTITLE = "jobTitle";
	public static final String F_DEPARTMENT = "department";
	public static final String F_WORKOFFICE = "workOffice";
	public static final String F_BUSINESSPHONE = "businessPhone";
	public static final String F_MOBILEPHONE = "mobilePhone";
	public static final String F_PICTURE = "picture";
	public static final String F_USERLANGUAGE = "userLanguage";
	public static final String F_LOGINCODE = "loginCode";
	public static final String F_LABELFIELD = "labelField";
	public static final String F_PICTUREFIELD = "pictureField";

	@DsField
	private String loginName;

	@DsField(noUpdate = true)
	private String code;

	@DsField
	private String email;

	@DsField
	private TNumberSeparator decimalSeparator;

	@DsField
	private TNumberSeparator thousandSeparator;

	@DsField(join = "left", path = "dateFormat.id")
	private String dateFormatId;

	@DsField(join = "left", path = "dateFormat.name")
	private String dateFormat;

	@DsField(join = "left", path = "location.id")
	private Integer locId;

	@DsField(join = "left", path = "location.code")
	private String locCode;

	@DsField
	private Title title;

	@DsField
	private String firstName;

	@DsField
	private String lastName;

	@DsField
	private String jobTitle;

	@DsField
	private Department department;

	@DsField
	private WorkOffice workOffice;

	@DsField
	private String businessPhone;

	@DsField
	private String mobilePhone;

	@DsField
	private byte[] picture;

	@DsField
	private LanguageCodes userLanguage;

	@DsField(fetch = false)
	private String loginCode;

	@DsField(fetch = false)
	private String labelField;

	@DsField(fetch = false)
	private String pictureField;

	/**
	 * Default constructor
	 */
	public UserSupp_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public UserSupp_Ds(UserSupp e) {
		super(e);
	}

	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public TNumberSeparator getDecimalSeparator() {
		return this.decimalSeparator;
	}

	public void setDecimalSeparator(TNumberSeparator decimalSeparator) {
		this.decimalSeparator = decimalSeparator;
	}

	public TNumberSeparator getThousandSeparator() {
		return this.thousandSeparator;
	}

	public void setThousandSeparator(TNumberSeparator thousandSeparator) {
		this.thousandSeparator = thousandSeparator;
	}

	public String getDateFormatId() {
		return this.dateFormatId;
	}

	public void setDateFormatId(String dateFormatId) {
		this.dateFormatId = dateFormatId;
	}

	public String getDateFormat() {
		return this.dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public Integer getLocId() {
		return this.locId;
	}

	public void setLocId(Integer locId) {
		this.locId = locId;
	}

	public String getLocCode() {
		return this.locCode;
	}

	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}

	public Title getTitle() {
		return this.title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getJobTitle() {
		return this.jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public Department getDepartment() {
		return this.department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public WorkOffice getWorkOffice() {
		return this.workOffice;
	}

	public void setWorkOffice(WorkOffice workOffice) {
		this.workOffice = workOffice;
	}

	public String getBusinessPhone() {
		return this.businessPhone;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	public String getMobilePhone() {
		return this.mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public byte[] getPicture() {
		return this.picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

	public LanguageCodes getUserLanguage() {
		return this.userLanguage;
	}

	public void setUserLanguage(LanguageCodes userLanguage) {
		this.userLanguage = userLanguage;
	}

	public String getLoginCode() {
		return this.loginCode;
	}

	public void setLoginCode(String loginCode) {
		this.loginCode = loginCode;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}

	public String getPictureField() {
		return this.pictureField;
	}

	public void setPictureField(String pictureField) {
		this.pictureField = pictureField;
	}
}
