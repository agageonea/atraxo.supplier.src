package atraxo.fmbas.presenter.ext.creditLines.delegate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.creditLines.ICreditLinesService;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.fmbas_type.ExposureLastAction;
import atraxo.fmbas.presenter.impl.creditLines.model.CustomerCreditLines_Ds;
import atraxo.fmbas.presenter.impl.creditLines.model.CustomerCreditLines_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author vhojda
 */
public class CustomerCreditLines_Pd extends AbstractPresenterDelegate {

	final static Logger LOGGER = LoggerFactory.getLogger(CustomerCreditLines_Pd.class);

	/**
	 * @param creditLinesDs
	 * @throws Exception
	 */
	public void customSave(CustomerCreditLines_Ds creditLinesDs, CustomerCreditLines_DsParam param) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START customSave()");
		}

		ICreditLinesService service = (ICreditLinesService) this.findEntityService(CreditLines.class);

		CreditLines credit = service.findById(creditLinesDs.getId());

		Integer initialAvailability = credit.getAvailability();

		credit.setActive(creditLinesDs.getActive());
		credit.setAmount(creditLinesDs.getAmount());
		credit.setAvailability(creditLinesDs.getAvailability());
		credit.setAlertPercentage(creditLinesDs.getAlertPercentage());
		credit.setValidFrom(creditLinesDs.getValidFrom());
		credit.setValidTo(creditLinesDs.getValidTo());
		if (!creditLinesDs.getCurrencyCode().equals(credit.getCurrency().getCode())) {
			ICurrenciesService currenciesService = (ICurrenciesService) this.findEntityService(Currencies.class);
			credit.setCurrency(currenciesService.findByCode(creditLinesDs.getCurrencyCode()));
		}

		// make sure to put some availability (default)
		if (credit.getAvailability() == null) {
			credit.setAvailability(credit.getCreditTerm().equals(CreditTerm._PREPAYMENT_) ? 0 : credit.getAmount());
		}

		// update and trigger the exposure calculation (if the case)
		if (!credit.getAvailability().equals(initialAvailability) && credit.getActive()) {
			service.updateCreditTriggerExposure(credit, ExposureLastAction._RECONCILIATION_, param.getUpdateReason());
		} else {
			service.update(credit, param.getUpdateReason());
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END customSave()");
		}
	}

}
