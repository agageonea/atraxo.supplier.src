/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.exchangerate.model;

import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ExchangeRate.class, sort = {
		@SortField(field = ExchangeRate_Ds.F_CURRENCY1CODE),
		@SortField(field = ExchangeRate_Ds.F_CURRENCY2CODE)})
@RefLookups({@RefLookup(refId = ExchangeRate_Ds.F_CURRENCY1ID),
		@RefLookup(refId = ExchangeRate_Ds.F_CURRENCY2ID),
		@RefLookup(refId = ExchangeRate_Ds.F_FINANCIALSOURCEID),
		@RefLookup(refId = ExchangeRate_Ds.F_AVGMTHDID)})
public class ExchangeRate_Ds extends AbstractDs_Ds<ExchangeRate> {

	public static final String ALIAS = "fmbas_ExchangeRate_Ds";

	public static final String F_CONVFCTR = "convFctr";
	public static final String F_DECIMALS = "decimals";
	public static final String F_CURRENCY1ID = "currency1iD";
	public static final String F_CURRENCY1CODE = "currency1Code";
	public static final String F_CURRENCY2ID = "currency2Id";
	public static final String F_CURRENCY2CODE = "currency2Code";
	public static final String F_AVGMTHDID = "avgMthdId";
	public static final String F_AVGMETHODINDICATORNAME = "avgMethodIndicatorName";
	public static final String F_AVGMETHODINDICATORCODE = "avgMethodIndicatorCode";
	public static final String F_ACTIVE = "active";
	public static final String F_DESCRIPTION = "description";
	public static final String F_NAME = "name";
	public static final String F_CALCVAL = "calcVal";
	public static final String F_FINANCIALSOURCEID = "financialSourceId";
	public static final String F_FINANCIALSOURCECODE = "financialSourceCode";
	public static final String F_FIRSTUSAGE = "firstUsage";
	public static final String F_LASTUSAGE = "lastUsage";

	@DsField(join = "left", path = "tserie.convFctr")
	private Operator convFctr;

	@DsField(join = "left", path = "tserie.decimals")
	private Integer decimals;

	@DsField(join = "left", path = "tserie.currency1Id.id")
	private Integer currency1iD;

	@DsField(join = "left", path = "tserie.currency1Id.code")
	private String currency1Code;

	@DsField(join = "left", path = "tserie.currency2Id.id")
	private Integer currency2Id;

	@DsField(join = "left", path = "tserie.currency2Id.code")
	private String currency2Code;

	@DsField(join = "left", path = "avgMethodIndicator.id")
	private Integer avgMthdId;

	@DsField(join = "left", path = "avgMethodIndicator.name")
	private String avgMethodIndicatorName;

	@DsField(join = "left", path = "avgMethodIndicator.code")
	private String avgMethodIndicatorCode;

	@DsField
	private Boolean active;

	@DsField
	private String description;

	@DsField
	private String name;

	@DsField(fetch = false)
	private String calcVal;

	@DsField(join = "left", path = "finsource.id")
	private Integer financialSourceId;

	@DsField(join = "left", path = "finsource.code")
	private String financialSourceCode;

	@DsField(fetch = false)
	private Date firstUsage;

	@DsField(fetch = false)
	private Date lastUsage;

	/**
	 * Default constructor
	 */
	public ExchangeRate_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ExchangeRate_Ds(ExchangeRate e) {
		super(e);
	}

	public Operator getConvFctr() {
		return this.convFctr;
	}

	public void setConvFctr(Operator convFctr) {
		this.convFctr = convFctr;
	}

	public Integer getDecimals() {
		return this.decimals;
	}

	public void setDecimals(Integer decimals) {
		this.decimals = decimals;
	}

	public Integer getCurrency1iD() {
		return this.currency1iD;
	}

	public void setCurrency1iD(Integer currency1iD) {
		this.currency1iD = currency1iD;
	}

	public String getCurrency1Code() {
		return this.currency1Code;
	}

	public void setCurrency1Code(String currency1Code) {
		this.currency1Code = currency1Code;
	}

	public Integer getCurrency2Id() {
		return this.currency2Id;
	}

	public void setCurrency2Id(Integer currency2Id) {
		this.currency2Id = currency2Id;
	}

	public String getCurrency2Code() {
		return this.currency2Code;
	}

	public void setCurrency2Code(String currency2Code) {
		this.currency2Code = currency2Code;
	}

	public Integer getAvgMthdId() {
		return this.avgMthdId;
	}

	public void setAvgMthdId(Integer avgMthdId) {
		this.avgMthdId = avgMthdId;
	}

	public String getAvgMethodIndicatorName() {
		return this.avgMethodIndicatorName;
	}

	public void setAvgMethodIndicatorName(String avgMethodIndicatorName) {
		this.avgMethodIndicatorName = avgMethodIndicatorName;
	}

	public String getAvgMethodIndicatorCode() {
		return this.avgMethodIndicatorCode;
	}

	public void setAvgMethodIndicatorCode(String avgMethodIndicatorCode) {
		this.avgMethodIndicatorCode = avgMethodIndicatorCode;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCalcVal() {
		return this.calcVal;
	}

	public void setCalcVal(String calcVal) {
		this.calcVal = calcVal;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}

	public Date getFirstUsage() {
		return this.firstUsage;
	}

	public void setFirstUsage(Date firstUsage) {
		this.firstUsage = firstUsage;
	}

	public Date getLastUsage() {
		return this.lastUsage;
	}

	public void setLastUsage(Date lastUsage) {
		this.lastUsage = lastUsage;
	}
}
