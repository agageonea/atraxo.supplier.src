package atraxo.fmbas.presenter.ext.externalInterfaces.delegate;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceMessageHistoryService;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import atraxo.fmbas.presenter.impl.externalInterfaces.model.ExternalInterfaceMessageHistory_Ds;
import atraxo.fmbas.presenter.impl.externalInterfaces.model.ExternalInterfaceMessageHistory_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class ExternalInterfaceMessageHistory_Pd extends AbstractPresenterDelegate {

	public void readSentMessage(ExternalInterfaceMessageHistory_Ds ds, ExternalInterfaceMessageHistory_DsParam params) throws Exception {

		IExternalInterfaceMessageHistoryService extIntMsgHistServ = (IExternalInterfaceMessageHistoryService) this
				.findEntityService(ExternalInterfaceMessageHistory.class);
		params.setMessageContent(extIntMsgHistServ.readFromS3(ds.getMessageId(), true));

	}

	public void readReceivedMessage(ExternalInterfaceMessageHistory_Ds ds, ExternalInterfaceMessageHistory_DsParam params) throws Exception {
		IExternalInterfaceMessageHistoryService extIntMsgHistServ = (IExternalInterfaceMessageHistoryService) this
				.findEntityService(ExternalInterfaceMessageHistory.class);
		params.setMessageContent(extIntMsgHistServ.readFromS3(ds.getResponseMessageId(), false));
	}
}
