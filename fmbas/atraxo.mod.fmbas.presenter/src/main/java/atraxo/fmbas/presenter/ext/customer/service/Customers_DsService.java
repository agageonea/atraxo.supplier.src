/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.customer.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.exposure.IExposureService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.ws.IRestClientCallbackService;
import atraxo.fmbas.business.api.ws.WSModelToDTOTransformer;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.business.ws.ExternalInterfaceWSHelper;
import atraxo.fmbas.business.ws.client.PumaRestClient;
import atraxo.fmbas.business.ws.toNav.WSCustomerToDTOTransformer;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.exposure.Exposure;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataProcessedStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataTransmissionStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import atraxo.fmbas.presenter.ext.customer.exception.FinalCustomerDeleteException;
import atraxo.fmbas.presenter.impl.customer.model.Customers_Ds;
import atraxo.fmbas.presenter.impl.customer.model.Customers_DsParam;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * @author zspeter
 */
public class Customers_DsService extends AbstractEntityDsService<Customers_Ds, Customers_Ds, Customers_DsParam, Customer>
		implements IDsService<Customers_Ds, Customers_Ds, Customers_DsParam> {

	private static final Logger LOG = LoggerFactory.getLogger(Customers_DsService.class);
	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;

	@Override
	protected void preUpdateAfterEntity(Customers_Ds ds, Customer customer, Customers_DsParam params) throws Exception {
		// check if status is PROSPECT and transmission status is NOT AVAILABLE or PROCESSED or FAILED
		if (!customer.getStatus().equals(CustomerStatus._PROSPECT_)
				&& (customer.getTransmissionStatus().equals(CustomerDataTransmissionStatus._NOT_AVAILABLE_)
						|| customer.getTransmissionStatus().equals(CustomerDataTransmissionStatus._PROCESSED_)
						|| customer.getTransmissionStatus().equals(CustomerDataTransmissionStatus._FAILED_))) {

			// export the customers
			IExternalInterfaceService extInterfaceSrv = (IExternalInterfaceService) this.findEntityService(ExternalInterface.class);
			ExternalInterface extInterface = extInterfaceSrv.findByName(ExtInterfaceNames.EXPORT_CUSTOMERS_TO_NAV.getValue());
			if (extInterface.getEnabled()) {
				WSModelToDTOTransformer<Customer> transformer = this.getApplicationContext().getBean(WSCustomerToDTOTransformer.class);
				customer.setTransmissionStatus(CustomerDataTransmissionStatus._IN_PROGRESS_);
				customer.setProcessedStatus(CustomerDataProcessedStatus._IN_PROGRESS_);
				ExtInterfaceParameters extIntParams = new ExtInterfaceParameters(extInterface.getParams());
				this.exportCustomer(Arrays.asList(customer), transformer, extInterface, extIntParams);
			}

		}

	}

	@SuppressWarnings("unchecked")
	private WSExecutionResult exportCustomer(List<Customer> customers, WSModelToDTOTransformer<Customer> transformer, ExternalInterface extInterface,
			ExtInterfaceParameters params) throws Exception {
		WSExecutionResult resultCustomer = new WSExecutionResult();
		ExternalInterfaceMessageHistoryFacade messageFacade = (ExternalInterfaceMessageHistoryFacade) this.getApplicationContext()
				.getBean("externalInterfaceMessageHistoryFacade");
		ExternalInterfaceHistoryFacade historyFacade = (ExternalInterfaceHistoryFacade) this.getApplicationContext()
				.getBean("externalInterfaceHistoryFacade");

		resultCustomer.setSuccess(true);
		resultCustomer.setExecutionData(
				String.format(BusinessErrorCode.INTERFACE_EXPORT_CUSTOMERS_START.getErrMsg(), extInterface.getName(), extInterface.getName()));
		params.put(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_DESTINATION_ADDRESS, "");
		params.put(ExternalInterfaceWSHelper.CAN_MODIFY_DESTINATION_ADDRESS, "FALSE");
		for (Customer customer : customers) {
			PumaRestClient<Customer> client = new PumaRestClient<>(this.taskExecutor, extInterface, params, Arrays.asList(customer), historyFacade,
					transformer, messageFacade);

			client.setCallbackSrv((IRestClientCallbackService<Customer>) this.findEntityService(Customer.class));
			resultCustomer = client.start(1);
		}
		return resultCustomer;
	}

	@Override
	protected void postFind(IQueryBuilder<Customers_Ds, Customers_Ds, Customers_DsParam> builder, List<Customers_Ds> result) throws Exception {
		ICustomerService custServ = (ICustomerService) this.getEntityService();
		Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH);
		for (Customers_Ds custDs : result) {
			Customer cust = custDs._getEntity_();
			CreditLines customerCreditLines = custServ.getValidCreditLineWithoutException(cust, date);
			if (customerCreditLines != null) {
				custDs.setAlertLimit(customerCreditLines.getAlertPercentage());
				custDs.setCreditUtil(this.exposureCalc(customerCreditLines));
				custDs.setCreditUtilAndPercent(custDs.getCreditUtil() + "%");
			}
		}
	}

	@Override
	protected void preDelete(List<Object> ids) throws Exception {
		super.preDelete(ids);
		ICustomerService srv = (ICustomerService) this.findEntityService(Customer.class);
		List<Customer> customerList = srv.findByIds(ids);
		StringBuilder sb = new StringBuilder();
		for (Customer customer : customerList) {
			if (customer.getIsThirdParty() != null && customer.getIsThirdParty() && (customer.getIsCustomer() || customer.getReseller().isEmpty())) {
				sb.append(", ");
				sb.append(customer.getName());
			}
		}
		if (sb.length() != 0) {
			throw new FinalCustomerDeleteException(sb.substring(1).trim());
		}

	}

	private BigDecimal exposureCalc(CreditLines cl) throws Exception {
		BigDecimal result = BigDecimal.ZERO;

		IExposureService service = (IExposureService) this.findEntityService(Exposure.class);
		List<Exposure> exposures = service.findByCustomer(cl.getCustomer());
		if (!exposures.isEmpty()) {
			if (exposures.size() > 1) {
				LOG.warn("WARNING:Multiple exposures found for Customer " + cl.getCustomer().getId() + "! Will return the first one ! ");
			}
			result = exposures.get(0).getActualUtilization().setScale(0, BigDecimal.ROUND_HALF_UP);
		} else {
			LOG.warn("WARNING:could not find exposure for Customer " + cl.getCustomer().getId() + "! Will return ZERO exposure value !");
		}
		return result;
	}

	/**
	 * Used for KPIs.
	 *
	 * @return
	 * @throws Exception
	 */
	public BigDecimal getAllocatedCredit() throws Exception {
		ICustomerService srv = (ICustomerService) this.findEntityService(Customer.class);
		return srv.getAllocatedCredit().setScale(2, RoundingMode.HALF_UP);
	}

	/**
	 * Used for KPIs.
	 *
	 * @return
	 * @throws Exception
	 */
	public BigDecimal getCreditUtilizationRation() throws Exception {
		ICustomerService srv = (ICustomerService) this.findEntityService(Customer.class);
		return srv.getCreditUtilizationRation().setScale(2, RoundingMode.HALF_UP);
	}
}
