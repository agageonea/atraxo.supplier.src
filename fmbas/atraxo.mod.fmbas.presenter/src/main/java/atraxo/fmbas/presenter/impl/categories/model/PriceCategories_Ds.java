/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.categories.model;

import atraxo.fmbas.domain.impl.categories.IataPC;
import atraxo.fmbas.domain.impl.categories.MainCategory;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = PriceCategory.class, sort = {@SortField(field = PriceCategories_Ds.F_NAME)})
@RefLookups({
		@RefLookup(refId = PriceCategories_Ds.F_MAINID, namedQuery = MainCategory.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceCategories_Ds.F_MAINCODE)}),
		@RefLookup(refId = PriceCategories_Ds.F_IATAID, namedQuery = IataPC.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PriceCategories_Ds.F_IATACODE)})})
public class PriceCategories_Ds extends AbstractDs_Ds<PriceCategory> {

	public static final String ALIAS = "fmbas_PriceCategories_Ds";

	public static final String F_IATAID = "iataId";
	public static final String F_IATACODE = "iataCode";
	public static final String F_MAINID = "mainId";
	public static final String F_MAINCODE = "mainCode";
	public static final String F_NAME = "name";
	public static final String F_DESCRIPTION = "description";
	public static final String F_TYPE = "type";
	public static final String F_PRICEPER = "pricePer";
	public static final String F_ISINDEXBASED = "isIndexBased";
	public static final String F_ISSYS = "isSys";
	public static final String F_ISMOVEMENT = "isMovement";
	public static final String F_ISTRANSPORT = "isTransport";
	public static final String F_ISPERIODICAL = "isPeriodical";
	public static final String F_ISCSO = "isCso";
	public static final String F_ACTIVE = "active";
	public static final String F_LABELFIELD = "labelField";

	@DsField(join = "left", path = "iata.id")
	private Integer iataId;

	@DsField(join = "left", path = "iata.code")
	private String iataCode;

	@DsField(join = "left", path = "mainCategory.id")
	private Integer mainId;

	@DsField(join = "left", path = "mainCategory.code")
	private String mainCode;

	@DsField
	private String name;

	@DsField
	private String description;

	@DsField
	private PriceType type;

	@DsField
	private PriceInd pricePer;

	@DsField
	private Boolean isIndexBased;

	@DsField
	private Boolean isSys;

	@DsField
	private Boolean isMovement;

	@DsField
	private Boolean isTransport;

	@DsField
	private Boolean isPeriodical;

	@DsField
	private Boolean isCso;

	@DsField
	private Boolean active;

	@DsField(fetch = false)
	private String labelField;

	/**
	 * Default constructor
	 */
	public PriceCategories_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public PriceCategories_Ds(PriceCategory e) {
		super(e);
	}

	public Integer getIataId() {
		return this.iataId;
	}

	public void setIataId(Integer iataId) {
		this.iataId = iataId;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public Integer getMainId() {
		return this.mainId;
	}

	public void setMainId(Integer mainId) {
		this.mainId = mainId;
	}

	public String getMainCode() {
		return this.mainCode;
	}

	public void setMainCode(String mainCode) {
		this.mainCode = mainCode;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public PriceType getType() {
		return this.type;
	}

	public void setType(PriceType type) {
		this.type = type;
	}

	public PriceInd getPricePer() {
		return this.pricePer;
	}

	public void setPricePer(PriceInd pricePer) {
		this.pricePer = pricePer;
	}

	public Boolean getIsIndexBased() {
		return this.isIndexBased;
	}

	public void setIsIndexBased(Boolean isIndexBased) {
		this.isIndexBased = isIndexBased;
	}

	public Boolean getIsSys() {
		return this.isSys;
	}

	public void setIsSys(Boolean isSys) {
		this.isSys = isSys;
	}

	public Boolean getIsMovement() {
		return this.isMovement;
	}

	public void setIsMovement(Boolean isMovement) {
		this.isMovement = isMovement;
	}

	public Boolean getIsTransport() {
		return this.isTransport;
	}

	public void setIsTransport(Boolean isTransport) {
		this.isTransport = isTransport;
	}

	public Boolean getIsPeriodical() {
		return this.isPeriodical;
	}

	public void setIsPeriodical(Boolean isPeriodical) {
		this.isPeriodical = isPeriodical;
	}

	public Boolean getIsCso() {
		return this.isCso;
	}

	public void setIsCso(Boolean isCso) {
		this.isCso = isCso;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}
}
