/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.job.model;

import atraxo.fmbas.domain.impl.fmbas_type.RepeatDuration;
import atraxo.fmbas.domain.impl.fmbas_type.TimeDuration;
import atraxo.fmbas.domain.impl.fmbas_type.TimeInterval;
import atraxo.fmbas.domain.impl.fmbas_type.TriggerType;
import atraxo.fmbas.domain.impl.job.Trigger;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Trigger.class)
@RefLookups({@RefLookup(refId = Trigger_Ds.F_JOBCHAINID)})
public class Trigger_Ds extends AbstractDs_Ds<Trigger> {

	public static final String ALIAS = "fmbas_Trigger_Ds";

	public static final String F_TYPE = "type";
	public static final String F_STARTDATE = "startDate";
	public static final String F_INTINDAYS = "intInDays";
	public static final String F_INTINWEEKS = "intInWeeks";
	public static final String F_DAYSOFMONTH = "daysOfMonth";
	public static final String F_DAYSOFWEEK = "daysOfWeek";
	public static final String F_MONTHS = "months";
	public static final String F_WEEKOFMONTH = "weekOfMonth";
	public static final String F_ONMONTHDAYS = "onMonthDays";
	public static final String F_ONWEEKDAYS = "onWeekDays";
	public static final String F_REPEATINTERVAL = "repeatInterval";
	public static final String F_REPEATDURATION = "repeatDuration";
	public static final String F_TIMEOUT = "timeout";
	public static final String F_ACTIVE = "active";
	public static final String F_TIMEREFERENCE = "timeReference";
	public static final String F_DETAILS = "details";
	public static final String F_JOBCHAINID = "jobChainId";
	public static final String F_TEMP = "temp";
	public static final String F_DAYS = "days";
	public static final String F_WEEKS = "weeks";
	public static final String F_MONDAY = "monday";
	public static final String F_TUESDAY = "tuesday";
	public static final String F_WEDNESDAY = "wednesday";
	public static final String F_THURSDAY = "thursday";
	public static final String F_FRIDAY = "friday";
	public static final String F_SATURDAY = "saturday";
	public static final String F_SUNDAY = "sunday";
	public static final String F_REPCHK = "repChk";
	public static final String F_TIMEOUTCHK = "timeoutChk";
	public static final String F_ENABLE = "enable";
	public static final String F_TIME = "time";
	public static final String F_SEPARATOR = "separator";

	@DsField
	private TriggerType type;

	@DsField
	private Date startDate;

	@DsField
	private Integer intInDays;

	@DsField
	private Integer intInWeeks;

	@DsField
	private String daysOfMonth;

	@DsField
	private String daysOfWeek;

	@DsField
	private String months;

	@DsField
	private String weekOfMonth;

	@DsField
	private Boolean onMonthDays;

	@DsField
	private Boolean onWeekDays;

	@DsField
	private TimeInterval repeatInterval;

	@DsField
	private RepeatDuration repeatDuration;

	@DsField
	private TimeDuration timeout;

	@DsField
	private Boolean active;

	@DsField
	private Boolean timeReference;

	@DsField
	private String details;

	@DsField(join = "left", path = "jobChain.id")
	private Integer jobChainId;

	@DsField(fetch = false)
	private String temp;

	@DsField(fetch = false)
	private String days;

	@DsField(fetch = false)
	private String weeks;

	@DsField(fetch = false)
	private Boolean monday;

	@DsField(fetch = false)
	private Boolean tuesday;

	@DsField(fetch = false)
	private Boolean wednesday;

	@DsField(fetch = false)
	private Boolean thursday;

	@DsField(fetch = false)
	private Boolean friday;

	@DsField(fetch = false)
	private Boolean saturday;

	@DsField(fetch = false)
	private Boolean sunday;

	@DsField(fetch = false)
	private Boolean repChk;

	@DsField(fetch = false)
	private Boolean timeoutChk;

	@DsField(fetch = false)
	private String enable;

	@DsField(fetch = false)
	private String time;

	@DsField(fetch = false)
	private String separator;

	/**
	 * Default constructor
	 */
	public Trigger_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Trigger_Ds(Trigger e) {
		super(e);
	}

	public TriggerType getType() {
		return this.type;
	}

	public void setType(TriggerType type) {
		this.type = type;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Integer getIntInDays() {
		return this.intInDays;
	}

	public void setIntInDays(Integer intInDays) {
		this.intInDays = intInDays;
	}

	public Integer getIntInWeeks() {
		return this.intInWeeks;
	}

	public void setIntInWeeks(Integer intInWeeks) {
		this.intInWeeks = intInWeeks;
	}

	public String getDaysOfMonth() {
		return this.daysOfMonth;
	}

	public void setDaysOfMonth(String daysOfMonth) {
		this.daysOfMonth = daysOfMonth;
	}

	public String getDaysOfWeek() {
		return this.daysOfWeek;
	}

	public void setDaysOfWeek(String daysOfWeek) {
		this.daysOfWeek = daysOfWeek;
	}

	public String getMonths() {
		return this.months;
	}

	public void setMonths(String months) {
		this.months = months;
	}

	public String getWeekOfMonth() {
		return this.weekOfMonth;
	}

	public void setWeekOfMonth(String weekOfMonth) {
		this.weekOfMonth = weekOfMonth;
	}

	public Boolean getOnMonthDays() {
		return this.onMonthDays;
	}

	public void setOnMonthDays(Boolean onMonthDays) {
		this.onMonthDays = onMonthDays;
	}

	public Boolean getOnWeekDays() {
		return this.onWeekDays;
	}

	public void setOnWeekDays(Boolean onWeekDays) {
		this.onWeekDays = onWeekDays;
	}

	public TimeInterval getRepeatInterval() {
		return this.repeatInterval;
	}

	public void setRepeatInterval(TimeInterval repeatInterval) {
		this.repeatInterval = repeatInterval;
	}

	public RepeatDuration getRepeatDuration() {
		return this.repeatDuration;
	}

	public void setRepeatDuration(RepeatDuration repeatDuration) {
		this.repeatDuration = repeatDuration;
	}

	public TimeDuration getTimeout() {
		return this.timeout;
	}

	public void setTimeout(TimeDuration timeout) {
		this.timeout = timeout;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getTimeReference() {
		return this.timeReference;
	}

	public void setTimeReference(Boolean timeReference) {
		this.timeReference = timeReference;
	}

	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Integer getJobChainId() {
		return this.jobChainId;
	}

	public void setJobChainId(Integer jobChainId) {
		this.jobChainId = jobChainId;
	}

	public String getTemp() {
		return this.temp;
	}

	public void setTemp(String temp) {
		this.temp = temp;
	}

	public String getDays() {
		return this.days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public String getWeeks() {
		return this.weeks;
	}

	public void setWeeks(String weeks) {
		this.weeks = weeks;
	}

	public Boolean getMonday() {
		return this.monday;
	}

	public void setMonday(Boolean monday) {
		this.monday = monday;
	}

	public Boolean getTuesday() {
		return this.tuesday;
	}

	public void setTuesday(Boolean tuesday) {
		this.tuesday = tuesday;
	}

	public Boolean getWednesday() {
		return this.wednesday;
	}

	public void setWednesday(Boolean wednesday) {
		this.wednesday = wednesday;
	}

	public Boolean getThursday() {
		return this.thursday;
	}

	public void setThursday(Boolean thursday) {
		this.thursday = thursday;
	}

	public Boolean getFriday() {
		return this.friday;
	}

	public void setFriday(Boolean friday) {
		this.friday = friday;
	}

	public Boolean getSaturday() {
		return this.saturday;
	}

	public void setSaturday(Boolean saturday) {
		this.saturday = saturday;
	}

	public Boolean getSunday() {
		return this.sunday;
	}

	public void setSunday(Boolean sunday) {
		this.sunday = sunday;
	}

	public Boolean getRepChk() {
		return this.repChk;
	}

	public void setRepChk(Boolean repChk) {
		this.repChk = repChk;
	}

	public Boolean getTimeoutChk() {
		return this.timeoutChk;
	}

	public void setTimeoutChk(Boolean timeoutChk) {
		this.timeoutChk = timeoutChk;
	}

	public String getEnable() {
		return this.enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

	public String getTime() {
		return this.time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getSeparator() {
		return this.separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}
}
