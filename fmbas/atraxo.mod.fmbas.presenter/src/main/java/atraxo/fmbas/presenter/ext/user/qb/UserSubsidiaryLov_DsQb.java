/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

package atraxo.fmbas.presenter.ext.user.qb;

import atraxo.fmbas.presenter.impl.user.model.UserSubsidiaryLov_Ds;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class UserSubsidiaryLov_DsQb
		extends
			QueryBuilderWithJpql<UserSubsidiaryLov_Ds, UserSubsidiaryLov_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();

		this.addFilterCondition("(e.user.code = :loggedInUserCode)");
		
		this.addCustomFilterItem("loggedInUserCode", Session.user.get().getCode());
	}

}
