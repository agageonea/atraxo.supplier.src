/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.suppliers.model;

import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Suppliers.class, jpqlWhere = "e.isSupplier = true", sort = {@SortField(field = Suppliers_Ds.F_CODE)})
@RefLookups({@RefLookup(refId = Suppliers_Ds.F_BUYERID, namedQuery = UserSupp.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Suppliers_Ds.F_BUYERNAME)})})
public class Suppliers_Ds extends AbstractDs_Ds<Suppliers> {

	public static final String ALIAS = "fmbas_Suppliers_Ds";

	public static final String F_CONTACTNAME = "contactName";
	public static final String F_CONTACTPHONE = "contactPhone";
	public static final String F_CONTACTEMAIL = "contactEmail";
	public static final String F_SECTIONTITLE = "sectionTitle";
	public static final String F_BUYERID = "buyerId";
	public static final String F_BUYERNAME = "buyerName";
	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_IATACODE = "iataCode";
	public static final String F_IPLAGENT = "iplAgent";
	public static final String F_FUELSUPPLIER = "fuelSupplier";
	public static final String F_FIXEDBASEOPERATOR = "fixedBaseOperator";
	public static final String F_NOTES = "notes";
	public static final String F_ISSUPPLIER = "isSupplier";
	public static final String F_ROLES = "roles";
	public static final String F_EXPOSURE = "exposure";

	@DsField(fetch = false)
	private String contactName;

	@DsField(fetch = false)
	private String contactPhone;

	@DsField(fetch = false)
	private String contactEmail;

	@DsField(fetch = false)
	private String sectionTitle;

	@DsField(join = "left", path = "responsibleBuyer.id")
	private String buyerId;

	@DsField(join = "left", path = "responsibleBuyer.name")
	private String buyerName;

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField
	private String iataCode;

	@DsField
	private Boolean iplAgent;

	@DsField
	private Boolean fuelSupplier;

	@DsField
	private Boolean fixedBaseOperator;

	@DsField
	private String notes;

	@DsField
	private Boolean isSupplier;

	@DsField(fetch = false)
	private String roles;

	@DsField(fetch = false)
	private BigDecimal exposure;

	/**
	 * Default constructor
	 */
	public Suppliers_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Suppliers_Ds(Suppliers e) {
		super(e);
	}

	public String getContactName() {
		return this.contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactPhone() {
		return this.contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactEmail() {
		return this.contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getSectionTitle() {
		return this.sectionTitle;
	}

	public void setSectionTitle(String sectionTitle) {
		this.sectionTitle = sectionTitle;
	}

	public String getBuyerId() {
		return this.buyerId;
	}

	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}

	public String getBuyerName() {
		return this.buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public Boolean getIplAgent() {
		return this.iplAgent;
	}

	public void setIplAgent(Boolean iplAgent) {
		this.iplAgent = iplAgent;
	}

	public Boolean getFuelSupplier() {
		return this.fuelSupplier;
	}

	public void setFuelSupplier(Boolean fuelSupplier) {
		this.fuelSupplier = fuelSupplier;
	}

	public Boolean getFixedBaseOperator() {
		return this.fixedBaseOperator;
	}

	public void setFixedBaseOperator(Boolean fixedBaseOperator) {
		this.fixedBaseOperator = fixedBaseOperator;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getIsSupplier() {
		return this.isSupplier;
	}

	public void setIsSupplier(Boolean isSupplier) {
		this.isSupplier = isSupplier;
	}

	public String getRoles() {
		return this.roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public BigDecimal getExposure() {
		return this.exposure;
	}

	public void setExposure(BigDecimal exposure) {
		this.exposure = exposure;
	}
}
