package atraxo.fmbas.presenter.ext;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import seava.j4e.api.extensions.IExtensionContentProvider;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterBaseService;

/**
 *
 * @author zspeter
 *
 */
public class ExtensionProviderSystemParameters extends AbstractPresenterBaseService implements IExtensionContentProvider {

	@Override
	public String getContent(String targetFrame) throws Exception {

		List<SystemParameter> systemParameters = this.listSystemParameters();
		return this.addDefaultElements(systemParameters);
	}

	protected String addDefaultElements(List<SystemParameter> systemParameters) throws Exception {

		String value ;
		String code ;
		StringBuilder sb = new StringBuilder();

		sb.append("_SYSTEMPARAMETERS_ = { ");

		for (SystemParameter sp : systemParameters) {
			code = sp.getCode().toLowerCase();
			value = sp.getValue();
			sb.append(code + " : '" + value + "', ");

		}

		sb.append("};");
		int ind = sb.lastIndexOf(",");
		if (ind > 0) {
			sb.replace(ind, ind + 1, "");
		}

		return sb.toString();
	}

	private List<SystemParameter> listSystemParameters() throws Exception {

		ISystemParameterService srv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		EntityManager em = srv.getEntityManager();

		String getSystemParameters = "SELECT e FROM " + SystemParameter.class.getSimpleName() + " e WHERE e.clientId=:clientId";
		TypedQuery<SystemParameter> systemParameterQuery = em.createQuery(getSystemParameters, SystemParameter.class);
		systemParameterQuery.setParameter("clientId", Session.user.get().getClientId());

		return systemParameterQuery.getResultList();
	}

}
