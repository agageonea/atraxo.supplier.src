/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.job.service;

import java.util.List;

import atraxo.ad.domain.impl.scheduler.StepExecution;
import atraxo.fmbas.presenter.impl.job.model.StepExecution_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service StepExecution_DsService
 */
public class StepExecution_DsService extends AbstractEntityDsService<StepExecution_Ds, StepExecution_Ds, Object, StepExecution>
		implements IDsService<StepExecution_Ds, StepExecution_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<StepExecution_Ds, StepExecution_Ds, Object> builder, List<StepExecution_Ds> result) throws Exception {
		// TODO Auto-generated method stub
		super.postFind(builder, result);
	}

}
