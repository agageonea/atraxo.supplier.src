/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.soneViewState.model;

import atraxo.fmbas.domain.impl.soneAdvancedFilter.SoneAdvancedFilter;
import atraxo.fmbas.domain.impl.soneViewState.SoneViewState;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = SoneViewState.class)
@RefLookups({@RefLookup(refId = ViewStateRt_Ds.F_ADVANCEDFILTERID, namedQuery = SoneAdvancedFilter.NQ_FIND_BY_NAME_CMP, params = {
		@Param(name = "name", field = ViewStateRt_Ds.F_ADVANCEDFILTERNAME),
		@Param(name = "cmp", field = ViewStateRt_Ds.F_CMP)})})
public class ViewStateRt_Ds extends AbstractDs_Ds<SoneViewState> {

	public static final String ALIAS = "fmbas_ViewStateRt_Ds";

	public static final String F_ADVANCEDFILTERID = "advancedFilterId";
	public static final String F_ADVANCEDFILTERNAME = "advancedFilterName";
	public static final String F_STANDARDFILTERVALUE = "standardFilterValue";
	public static final String F_ADVANCEDFILTERVALUE = "advancedFilterValue";
	public static final String F_DEFAULTFILTER = "defaultFilter";
	public static final String F_ADVANCEDFILTERCMP = "advancedFilterCmp";
	public static final String F_NAME = "name";
	public static final String F_CMP = "cmp";
	public static final String F_CMPTYPE = "cmpType";
	public static final String F_DESCRIPTION = "description";
	public static final String F_VALUE = "value";
	public static final String F_NOTES = "notes";
	public static final String F_PAGESIZE = "pageSize";
	public static final String F_TOTALS = "totals";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_CURRENCYCODE = "currencyCode";
	public static final String F_ISSTANDARD = "isStandard";
	public static final String F_DEFAULTVIEW = "defaultView";
	public static final String F_AVAILABLESYSTEMWIDE = "availableSystemwide";
	public static final String F_ACTIVE = "active";

	@DsField(join = "left", path = "advancedFilter.id")
	private Integer advancedFilterId;

	@DsField(join = "left", path = "advancedFilter.name")
	private String advancedFilterName;

	@DsField(join = "left", path = "advancedFilter.standardFilterValue")
	private String standardFilterValue;

	@DsField(join = "left", path = "advancedFilter.advancedFilterValue")
	private String advancedFilterValue;

	@DsField(join = "left", path = "advancedFilter.defaultFilter")
	private Boolean defaultFilter;

	@DsField(join = "left", path = "advancedFilter.cmp")
	private String advancedFilterCmp;

	@DsField
	private String name;

	@DsField
	private String cmp;

	@DsField
	private String cmpType;

	@DsField
	private String description;

	@DsField
	private String value;

	@DsField
	private String notes;

	@DsField
	private Integer pageSize;

	@DsField
	private Boolean totals;

	@DsField
	private String unitCode;

	@DsField
	private String currencyCode;

	@DsField
	private Boolean isStandard;

	@DsField
	private Boolean defaultView;

	@DsField
	private Boolean availableSystemwide;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public ViewStateRt_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ViewStateRt_Ds(SoneViewState e) {
		super(e);
	}

	public Integer getAdvancedFilterId() {
		return this.advancedFilterId;
	}

	public void setAdvancedFilterId(Integer advancedFilterId) {
		this.advancedFilterId = advancedFilterId;
	}

	public String getAdvancedFilterName() {
		return this.advancedFilterName;
	}

	public void setAdvancedFilterName(String advancedFilterName) {
		this.advancedFilterName = advancedFilterName;
	}

	public String getStandardFilterValue() {
		return this.standardFilterValue;
	}

	public void setStandardFilterValue(String standardFilterValue) {
		this.standardFilterValue = standardFilterValue;
	}

	public String getAdvancedFilterValue() {
		return this.advancedFilterValue;
	}

	public void setAdvancedFilterValue(String advancedFilterValue) {
		this.advancedFilterValue = advancedFilterValue;
	}

	public Boolean getDefaultFilter() {
		return this.defaultFilter;
	}

	public void setDefaultFilter(Boolean defaultFilter) {
		this.defaultFilter = defaultFilter;
	}

	public String getAdvancedFilterCmp() {
		return this.advancedFilterCmp;
	}

	public void setAdvancedFilterCmp(String advancedFilterCmp) {
		this.advancedFilterCmp = advancedFilterCmp;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCmp() {
		return this.cmp;
	}

	public void setCmp(String cmp) {
		this.cmp = cmp;
	}

	public String getCmpType() {
		return this.cmpType;
	}

	public void setCmpType(String cmpType) {
		this.cmpType = cmpType;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getPageSize() {
		return this.pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Boolean getTotals() {
		return this.totals;
	}

	public void setTotals(Boolean totals) {
		this.totals = totals;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Boolean getIsStandard() {
		return this.isStandard;
	}

	public void setIsStandard(Boolean isStandard) {
		this.isStandard = isStandard;
	}

	public Boolean getDefaultView() {
		return this.defaultView;
	}

	public void setDefaultView(Boolean defaultView) {
		this.defaultView = defaultView;
	}

	public Boolean getAvailableSystemwide() {
		return this.availableSystemwide;
	}

	public void setAvailableSystemwide(Boolean availableSystemwide) {
		this.availableSystemwide = availableSystemwide;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
