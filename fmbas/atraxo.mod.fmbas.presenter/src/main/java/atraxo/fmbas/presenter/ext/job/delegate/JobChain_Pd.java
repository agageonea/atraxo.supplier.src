package atraxo.fmbas.presenter.ext.job.delegate;

import atraxo.ad.business.api.scheduler.IJobExecutionService;
import atraxo.ad.domain.impl.scheduler.JobExecution;
import atraxo.fmbas.business.api.job.IJobChainService;
import atraxo.fmbas.domain.impl.job.JobChain;
import atraxo.fmbas.presenter.impl.job.model.JobChain_Ds;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class JobChain_Pd extends AbstractPresenterDelegate {

	public void activate(JobChain_Ds ds) throws Exception {
		IJobChainService service = (IJobChainService) this.findEntityService(JobChain.class);
		service.activate(ds.getId());
	}

	public void runNow(JobChain_Ds ds) throws Exception {
		IJobChainService service = (IJobChainService) this.findEntityService(JobChain.class);
		service.runNow(ds.getId());
	}

	public void deactivate(JobChain_Ds ds) throws Exception {
		IJobChainService service = (IJobChainService) this.findEntityService(JobChain.class);
		service.deactivate(ds.getId());
	}

	public void empty(JobChain_Ds ds) throws Exception {
		IJobExecutionService service = (IJobExecutionService) this.findEntityService(JobExecution.class);
		service.emptyHistory(ds.getId());
	}
}
