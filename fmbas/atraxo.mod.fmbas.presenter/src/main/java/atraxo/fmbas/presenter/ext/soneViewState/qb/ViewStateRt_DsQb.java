/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.soneViewState.qb;

import atraxo.fmbas.presenter.impl.soneViewState.model.ViewStateRt_Ds;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class ViewStateRt_DsQb extends QueryBuilderWithJpql<ViewStateRt_Ds, ViewStateRt_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		String userCode = Session.user.get().getCode();
		this.addFilterCondition(" (e.createdBy = :userCode or e.availableSystemwide = true ) ");
		this.addCustomFilterItem("userCode", userCode);
	}

}
