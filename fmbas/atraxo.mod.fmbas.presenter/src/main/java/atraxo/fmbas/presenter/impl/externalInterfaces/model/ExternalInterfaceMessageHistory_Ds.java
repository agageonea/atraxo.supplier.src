/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.externalInterfaces.model;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ExternalInterfaceMessageHistory.class, sort = {@SortField(field = ExternalInterfaceMessageHistory_Ds.F_EXECUTIONTIME, desc = true)})
@RefLookups({@RefLookup(refId = ExternalInterfaceMessageHistory_Ds.F_EXTERNALINTERFACEID)})
public class ExternalInterfaceMessageHistory_Ds
		extends
			AbstractDs_Ds<ExternalInterfaceMessageHistory> {

	public static final String ALIAS = "fmbas_ExternalInterfaceMessageHistory_Ds";

	public static final String F_EXTERNALINTERFACEID = "externalInterfaceId";
	public static final String F_EXTERNALINTERFACENAME = "externalInterfaceName";
	public static final String F_OBJECTID = "objectId";
	public static final String F_OBJECTTYPE = "objectType";
	public static final String F_OBJECTREFID = "objectRefId";
	public static final String F_MESSAGEID = "messageId";
	public static final String F_RESPONSEMESSAGEID = "responseMessageId";
	public static final String F_EXECUTIONTIME = "executionTime";
	public static final String F_RESPONSEMESSAGETIME = "responseMessageTime";
	public static final String F_STATUS = "status";

	@DsField(join = "left", path = "externalInterface.id")
	private Integer externalInterfaceId;

	@DsField(join = "left", path = "externalInterface.name")
	private String externalInterfaceName;

	@DsField
	private Integer objectId;

	@DsField
	private String objectType;

	@DsField
	private String objectRefId;

	@DsField
	private String messageId;

	@DsField
	private String responseMessageId;

	@DsField
	private Date executionTime;

	@DsField(path = "responseTime")
	private Date responseMessageTime;

	@DsField
	private ExternalInterfaceMessageHistoryStatus status;

	/**
	 * Default constructor
	 */
	public ExternalInterfaceMessageHistory_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ExternalInterfaceMessageHistory_Ds(ExternalInterfaceMessageHistory e) {
		super(e);
	}

	public Integer getExternalInterfaceId() {
		return this.externalInterfaceId;
	}

	public void setExternalInterfaceId(Integer externalInterfaceId) {
		this.externalInterfaceId = externalInterfaceId;
	}

	public String getExternalInterfaceName() {
		return this.externalInterfaceName;
	}

	public void setExternalInterfaceName(String externalInterfaceName) {
		this.externalInterfaceName = externalInterfaceName;
	}

	public Integer getObjectId() {
		return this.objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getObjectRefId() {
		return this.objectRefId;
	}

	public void setObjectRefId(String objectRefId) {
		this.objectRefId = objectRefId;
	}

	public String getMessageId() {
		return this.messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getResponseMessageId() {
		return this.responseMessageId;
	}

	public void setResponseMessageId(String responseMessageId) {
		this.responseMessageId = responseMessageId;
	}

	public Date getExecutionTime() {
		return this.executionTime;
	}

	public void setExecutionTime(Date executionTime) {
		this.executionTime = executionTime;
	}

	public Date getResponseMessageTime() {
		return this.responseMessageTime;
	}

	public void setResponseMessageTime(Date responseMessageTime) {
		this.responseMessageTime = responseMessageTime;
	}

	public ExternalInterfaceMessageHistoryStatus getStatus() {
		return this.status;
	}

	public void setStatus(ExternalInterfaceMessageHistoryStatus status) {
		this.status = status;
	}
}
