/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.notificationEvent.qb;

import atraxo.fmbas.presenter.impl.notificationEvent.model.NotificationEventList_Ds;
import atraxo.fmbas.presenter.impl.notificationEvent.model.NotificationEventList_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class NotificationEventList_DsQb
		extends
			QueryBuilderWithJpql<NotificationEventList_Ds, NotificationEventList_Ds, NotificationEventList_DsParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getContactId() != null
				&& !"".equals(this.params.getContactId())) {
			addFilterCondition("   e.id in ( select p.id from NotificationEvent p, IN (p.contact) c where c.id = :contactId ) ");
			addCustomFilterItem("contactId", this.params.getContactId());
		}
	}
}
