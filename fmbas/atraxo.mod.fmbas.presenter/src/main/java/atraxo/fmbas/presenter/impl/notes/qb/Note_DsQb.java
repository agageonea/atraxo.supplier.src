/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.notes.qb;

import atraxo.fmbas.presenter.impl.notes.model.Note_Ds;
import atraxo.fmbas.presenter.impl.notes.model.Note_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class Note_DsQb
		extends
			QueryBuilderWithJpql<Note_Ds, Note_Ds, Note_DsParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getCreatedAfter() != null
				&& !"".equals(this.params.getCreatedAfter())) {
			addFilterCondition("  e.createdAt >= :createdAfter ");
			addCustomFilterItem("createdAfter", this.params.getCreatedAfter());
		}
		if (this.params != null && this.params.getCreatedBefore() != null
				&& !"".equals(this.params.getCreatedBefore())) {
			addFilterCondition("  e.createdAt <= :createdBefore ");
			addCustomFilterItem("createdBefore", this.params.getCreatedBefore());
		}
	}
}
