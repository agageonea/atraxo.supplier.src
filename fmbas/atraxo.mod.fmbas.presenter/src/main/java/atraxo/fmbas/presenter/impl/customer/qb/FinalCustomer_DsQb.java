/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.qb;

import atraxo.fmbas.presenter.impl.customer.model.FinalCustomer_Ds;
import atraxo.fmbas.presenter.impl.customer.model.FinalCustomer_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class FinalCustomer_DsQb
		extends
			QueryBuilderWithJpql<FinalCustomer_Ds, FinalCustomer_Ds, FinalCustomer_DsParam> {

	@Override
	public void beforeBuildWhere() {
		if (this.params != null && this.params.getCustomerId() != null
				&& !"".equals(this.params.getCustomerId())) {
			addFilterCondition("   e.id in ( select fc.id from Customer fc, IN (fc.reseller) c where c.id = :customerId ) ");
			addCustomFilterItem("customerId", this.params.getCustomerId());
		}
	}
}
