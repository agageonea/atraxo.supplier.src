/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.commodities.qb;

import atraxo.fmbas.presenter.impl.commodities.model.Commoditie_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class Commoditie_DsQb
		extends
			QueryBuilderWithJpql<Commoditie_Ds, Commoditie_Ds, Object> {
}
