/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.dictionary.model;

import atraxo.fmbas.domain.impl.dictionary.Dictionary;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Dictionary.class, sort = {@SortField(field = Dictionary_Ds.F_FIELDNAME, desc = true)})
@RefLookups({@RefLookup(refId = Dictionary_Ds.F_EXTERNALINTERFACEID, namedQuery = ExternalInterface.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = Dictionary_Ds.F_EXTERNALINTERFACENAME)})})
public class Dictionary_Ds extends AbstractDs_Ds<Dictionary> {

	public static final String ALIAS = "fmbas_Dictionary_Ds";

	public static final String F_EXTERNALINTERFACEID = "externalInterfaceId";
	public static final String F_EXTERNALINTERFACENAME = "externalInterfaceName";
	public static final String F_FIELDNAME = "fieldName";
	public static final String F_ORIGINALVALUE = "originalValue";
	public static final String F_NEWVALUE = "newValue";
	public static final String F_CRITERIA = "criteria";

	@DsField(join = "left", path = "externalInterface.id")
	private Integer externalInterfaceId;

	@DsField(join = "left", path = "externalInterface.name")
	private String externalInterfaceName;

	@DsField
	private String fieldName;

	@DsField
	private String originalValue;

	@DsField
	private String newValue;

	@DsField(fetch = false)
	private String criteria;

	/**
	 * Default constructor
	 */
	public Dictionary_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Dictionary_Ds(Dictionary e) {
		super(e);
	}

	public Integer getExternalInterfaceId() {
		return this.externalInterfaceId;
	}

	public void setExternalInterfaceId(Integer externalInterfaceId) {
		this.externalInterfaceId = externalInterfaceId;
	}

	public String getExternalInterfaceName() {
		return this.externalInterfaceName;
	}

	public void setExternalInterfaceName(String externalInterfaceName) {
		this.externalInterfaceName = externalInterfaceName;
	}

	public String getFieldName() {
		return this.fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getOriginalValue() {
		return this.originalValue;
	}

	public void setOriginalValue(String originalValue) {
		this.originalValue = originalValue;
	}

	public String getNewValue() {
		return this.newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	public String getCriteria() {
		return this.criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}
}
