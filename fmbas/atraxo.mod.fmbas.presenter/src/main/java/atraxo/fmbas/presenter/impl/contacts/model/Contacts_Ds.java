/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.contacts.model;

import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.fmbas_type.Department;
import atraxo.fmbas.domain.impl.fmbas_type.Title;
import atraxo.fmbas.domain.impl.fmbas_type.WorkOffice;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Contacts.class, sort = {@SortField(field = Contacts_Ds.F_FIRSTNAME)})
@RefLookups({@RefLookup(refId = Contacts_Ds.F_COUNTRYID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Contacts_Ds.F_COUNTRYCODE)})})
public class Contacts_Ds extends AbstractDs_Ds<Contacts> {

	public static final String ALIAS = "fmbas_Contacts_Ds";

	public static final String F_FULLNAME = "fullName";
	public static final String F_ADDRESSTITLE = "addressTitle";
	public static final String F_PHONEEMAILS = "phoneEmails";
	public static final String F_COUNTRYID = "countryId";
	public static final String F_COUNTRYNAME = "countryName";
	public static final String F_COUNTRYCODE = "countryCode";
	public static final String F_OBJECTID = "objectId";
	public static final String F_OBJECTTYPE = "objectType";
	public static final String F_JOBTITLE = "jobTitle";
	public static final String F_TITLE = "title";
	public static final String F_FIRSTNAME = "firstName";
	public static final String F_LASTNAME = "lastName";
	public static final String F_BUSINESSUNIT = "businessUnit";
	public static final String F_BUSINESSPHONE = "businessPhone";
	public static final String F_MOBILEPHONE = "mobilePhone";
	public static final String F_FAXNUMBER = "faxNumber";
	public static final String F_EMAIL = "email";
	public static final String F_WORKOFFICE = "workOffice";
	public static final String F_CITY = "city";
	public static final String F_STREET = "street";
	public static final String F_STATE = "state";
	public static final String F_ZIP = "zip";
	public static final String F_ISPRIMARY = "isPrimary";
	public static final String F_ACTIVE = "active";
	public static final String F_NOTES = "notes";
	public static final String F_FULLNAMEFIELD = "fullNameField";

	@DsField(fetch = false)
	private String fullName;

	@DsField(fetch = false)
	private String addressTitle;

	@DsField(fetch = false)
	private String phoneEmails;

	@DsField(join = "left", path = "country.id")
	private Integer countryId;

	@DsField(join = "left", path = "country.name")
	private String countryName;

	@DsField(join = "left", path = "country.code")
	private String countryCode;

	@DsField
	private Integer objectId;

	@DsField
	private String objectType;

	@DsField
	private String jobTitle;

	@DsField
	private Title title;

	@DsField
	private String firstName;

	@DsField
	private String lastName;

	@DsField
	private Department businessUnit;

	@DsField
	private String businessPhone;

	@DsField
	private String mobilePhone;

	@DsField
	private String faxNumber;

	@DsField
	private String email;

	@DsField
	private WorkOffice workOffice;

	@DsField
	private String city;

	@DsField
	private String street;

	@DsField
	private String state;

	@DsField
	private String zip;

	@DsField
	private Boolean isPrimary;

	@DsField
	private Boolean active;

	@DsField
	private String notes;

	@DsField(fetch = false)
	private String fullNameField;

	/**
	 * Default constructor
	 */
	public Contacts_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Contacts_Ds(Contacts e) {
		super(e);
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getAddressTitle() {
		return this.addressTitle;
	}

	public void setAddressTitle(String addressTitle) {
		this.addressTitle = addressTitle;
	}

	public String getPhoneEmails() {
		return this.phoneEmails;
	}

	public void setPhoneEmails(String phoneEmails) {
		this.phoneEmails = phoneEmails;
	}

	public Integer getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Integer getObjectId() {
		return this.objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getJobTitle() {
		return this.jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public Title getTitle() {
		return this.title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Department getBusinessUnit() {
		return this.businessUnit;
	}

	public void setBusinessUnit(Department businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getBusinessPhone() {
		return this.businessPhone;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	public String getMobilePhone() {
		return this.mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getFaxNumber() {
		return this.faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public WorkOffice getWorkOffice() {
		return this.workOffice;
	}

	public void setWorkOffice(WorkOffice workOffice) {
		this.workOffice = workOffice;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Boolean getIsPrimary() {
		return this.isPrimary;
	}

	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getFullNameField() {
		return this.fullNameField;
	}

	public void setFullNameField(String fullNameField) {
		this.fullNameField = fullNameField;
	}
}
