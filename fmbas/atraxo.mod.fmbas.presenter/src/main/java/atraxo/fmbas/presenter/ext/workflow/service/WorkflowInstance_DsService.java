/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.workflow.service;

import java.util.List;

import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;

import atraxo.fmbas.business.api.workflow.IWorkflowInstanceService;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.presenter.impl.workflow.model.WorkflowInstance_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.business.bpm.ActivitiBpmService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service WorkflowInstance_DsService
 */
public class WorkflowInstance_DsService extends AbstractEntityDsService<WorkflowInstance_Ds, WorkflowInstance_Ds, Object, WorkflowInstance>
		implements IDsService<WorkflowInstance_Ds, WorkflowInstance_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<WorkflowInstance_Ds, WorkflowInstance_Ds, Object> builder, List<WorkflowInstance_Ds> result)
			throws Exception {
		super.postFind(builder, result);

		IWorkflowInstanceService serv = this.getApplicationContext().getBean(IWorkflowInstanceService.class);

		ActivitiBpmService activitiBpmService = this.getApplicationContext().getBean(ActivitiBpmService.class);

		for (WorkflowInstance_Ds ds : result) {
			WorkflowInstance wkfInstance = serv.findByBusiness(ds.getId());

			// get the process instance
			List<ProcessInstance> instances = activitiBpmService.getRuntimeService().createProcessInstanceQuery()
					.processInstanceBusinessKey(wkfInstance.getId() + "").list();
			for (ProcessInstance inst : instances) {
				// check for any active task so to fill in the name of the task + possible asignee
				Task task = activitiBpmService.getTaskService().createTaskQuery().processInstanceBusinessKey(inst.getBusinessKey()).singleResult();
				if (task != null) {
					ds.setStep(task.getName());
					if (task.getAssignee() != null) {
						ds.setStep(ds.getStep() + " : " + task.getAssignee());
					}
				}
			}
		}
	}

}
