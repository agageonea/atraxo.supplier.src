/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.customer.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Customer.class, sort = {@SortField(field = SubsidiaryAssignList_Ds.F_CODE)})
@RefLookups({
		@RefLookup(refId = SubsidiaryAssignList_Ds.F_OFFICECOUNTRYID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = SubsidiaryAssignList_Ds.F_OFFICECOUNTRYCODE)}),
		@RefLookup(refId = SubsidiaryAssignList_Ds.F_OFFICESTATEID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = SubsidiaryAssignList_Ds.F_OFFICESTATECODE)})})
public class SubsidiaryAssignList_Ds extends AbstractDs_Ds<Customer> {

	public static final String ALIAS = "fmbas_SubsidiaryAssignList_Ds";

	public static final String F_OFFICECOUNTRYID = "officeCountryId";
	public static final String F_OFFICECOUNTRYCODE = "officeCountryCode";
	public static final String F_OFFICECOUNTRYNAME = "officeCountryName";
	public static final String F_OFFICESTATEID = "officeStateId";
	public static final String F_OFFICESTATECODE = "officeStateCode";
	public static final String F_OFFICESTATENAME = "officeStateName";
	public static final String F_OFFICECITY = "officeCity";
	public static final String F_NAME = "name";
	public static final String F_CODE = "code";
	public static final String F_ISSELECTED = "isSelected";

	@DsField(join = "left", path = "officeCountry.id")
	private Integer officeCountryId;

	@DsField(join = "left", path = "officeCountry.code")
	private String officeCountryCode;

	@DsField(join = "left", path = "officeCountry.name")
	private String officeCountryName;

	@DsField(join = "left", path = "officeState.id")
	private Integer officeStateId;

	@DsField(join = "left", path = "officeState.code")
	private String officeStateCode;

	@DsField(join = "left", path = "officeState.name")
	private String officeStateName;

	@DsField
	private String officeCity;

	@DsField
	private String name;

	@DsField
	private String code;

	@DsField(fetch = false)
	private Boolean isSelected;

	/**
	 * Default constructor
	 */
	public SubsidiaryAssignList_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public SubsidiaryAssignList_Ds(Customer e) {
		super(e);
	}

	public Integer getOfficeCountryId() {
		return this.officeCountryId;
	}

	public void setOfficeCountryId(Integer officeCountryId) {
		this.officeCountryId = officeCountryId;
	}

	public String getOfficeCountryCode() {
		return this.officeCountryCode;
	}

	public void setOfficeCountryCode(String officeCountryCode) {
		this.officeCountryCode = officeCountryCode;
	}

	public String getOfficeCountryName() {
		return this.officeCountryName;
	}

	public void setOfficeCountryName(String officeCountryName) {
		this.officeCountryName = officeCountryName;
	}

	public Integer getOfficeStateId() {
		return this.officeStateId;
	}

	public void setOfficeStateId(Integer officeStateId) {
		this.officeStateId = officeStateId;
	}

	public String getOfficeStateCode() {
		return this.officeStateCode;
	}

	public void setOfficeStateCode(String officeStateCode) {
		this.officeStateCode = officeStateCode;
	}

	public String getOfficeStateName() {
		return this.officeStateName;
	}

	public void setOfficeStateName(String officeStateName) {
		this.officeStateName = officeStateName;
	}

	public String getOfficeCity() {
		return this.officeCity;
	}

	public void setOfficeCity(String officeCity) {
		this.officeCity = officeCity;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getIsSelected() {
		return this.isSelected;
	}

	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}
}
