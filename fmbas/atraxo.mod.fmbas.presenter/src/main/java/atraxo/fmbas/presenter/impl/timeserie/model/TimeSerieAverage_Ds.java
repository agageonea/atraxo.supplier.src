/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.timeserie.model;

import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieAverage;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = TimeSerieAverage.class, sort = {@SortField(field = TimeSerieAverage_Ds.F_AVGCODE)})
@RefLookups({@RefLookup(refId = TimeSerieAverage_Ds.F_TSERIEID, namedQuery = TimeSerie.NQ_FIND_BY_NAME, params = {@Param(name = "serieName", field = TimeSerieAverage_Ds.F_TSERIEID)})})
public class TimeSerieAverage_Ds extends AbstractDs_Ds<TimeSerieAverage> {

	public static final String ALIAS = "fmbas_TimeSerieAverage_Ds";

	public static final String F_AVGCODE = "avgCode";
	public static final String F_AVGNAME = "avgName";
	public static final String F_AVGACTIVE = "avgActive";
	public static final String F_EFFECTIVEOFFSETDAY = "effectiveOffsetDay";
	public static final String F_EFFECTIVEOFFSETINTERVAL = "effectiveOffsetInterval";
	public static final String F_TSERIEID = "tserieId";
	public static final String F_ACTIVE = "active";

	@DsField(join = "left", path = "averagingMethod.code")
	private String avgCode;

	@DsField(join = "left", path = "averagingMethod.name")
	private String avgName;

	@DsField(join = "left", path = "averagingMethod.active")
	private Boolean avgActive;

	@DsField
	private Integer effectiveOffsetDay;

	@DsField
	private Integer effectiveOffsetInterval;

	@DsField(join = "left", path = "tserie.id")
	private Integer tserieId;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public TimeSerieAverage_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public TimeSerieAverage_Ds(TimeSerieAverage e) {
		super(e);
	}

	public String getAvgCode() {
		return this.avgCode;
	}

	public void setAvgCode(String avgCode) {
		this.avgCode = avgCode;
	}

	public String getAvgName() {
		return this.avgName;
	}

	public void setAvgName(String avgName) {
		this.avgName = avgName;
	}

	public Boolean getAvgActive() {
		return this.avgActive;
	}

	public void setAvgActive(Boolean avgActive) {
		this.avgActive = avgActive;
	}

	public Integer getEffectiveOffsetDay() {
		return this.effectiveOffsetDay;
	}

	public void setEffectiveOffsetDay(Integer effectiveOffsetDay) {
		this.effectiveOffsetDay = effectiveOffsetDay;
	}

	public Integer getEffectiveOffsetInterval() {
		return this.effectiveOffsetInterval;
	}

	public void setEffectiveOffsetInterval(Integer effectiveOffsetInterval) {
		this.effectiveOffsetInterval = effectiveOffsetInterval;
	}

	public Integer getTserieId() {
		return this.tserieId;
	}

	public void setTserieId(Integer tserieId) {
		this.tserieId = tserieId;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
