/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.aircraft.qb;

import atraxo.fmbas.presenter.impl.aircraft.model.AircraftsLov_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class AircraftsLov_DsQb
		extends
			QueryBuilderWithJpql<AircraftsLov_Ds, AircraftsLov_Ds, Object> {
}
