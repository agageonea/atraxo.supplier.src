/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.job.service;

import java.util.List;
import java.util.Random;

import atraxo.fmbas.domain.impl.fmbas_type.JobParamType;
import atraxo.fmbas.domain.impl.job.ActionParameter;
import atraxo.fmbas.presenter.impl.job.model.ActionParameter_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service ActionParameter_DsService
 */
public class ActionParameter_DsService extends AbstractEntityDsService<ActionParameter_Ds, ActionParameter_Ds, Object, ActionParameter>
		implements IDsService<ActionParameter_Ds, ActionParameter_Ds, Object> {

	private static final String CHARS = "1234567890qwertyuiopasdfghjklzxcvbnm";

	@Override
	protected void postFind(IQueryBuilder<ActionParameter_Ds, ActionParameter_Ds, Object> builder, List<ActionParameter_Ds> result) throws Exception {
		for (ActionParameter_Ds ds : result) {
			if (ds.getReadOnly() != null && ds.getReadOnly() && JobParamType._MASKED_.equals(ds.getType())) {
				ds.setValue(this.randomString(ds.getValue().length()));
				ds.setDefaultValue(this.randomString(ds.getDefaultValue().length()));
			}
		}
	}

	private String randomString(int length) {
		Random rand = new Random();
		StringBuilder buf = new StringBuilder();
		for (int i = 0; i < length; i++) {
			buf.append(CHARS.charAt(rand.nextInt(CHARS.length())));
		}
		return buf.toString();
	}

}
