/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.uom.service;

import java.math.BigDecimal;
import java.util.List;

import atraxo.fmbas.domain.ext.types.FmbasSysParam;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.presenter.impl.uom.model.SystemParameters_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class SystemParameters_DsService extends AbstractEntityDsService<SystemParameters_Ds, SystemParameters_Ds, Object, SystemParameter> implements
		IDsService<SystemParameters_Ds, SystemParameters_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<SystemParameters_Ds, SystemParameters_Ds, Object> builder, List<SystemParameters_Ds> result)
			throws Exception {
		super.postFind(builder, result);

		SystemParameters_Ds displayedDs = new SystemParameters_Ds();
		for (SystemParameters_Ds ds : result) {
			FmbasSysParam sysParam = FmbasSysParam.getByName(ds._getEntity_().getCode());
			switch (sysParam) {
			case SYSUNITS:
				displayedDs.setSysUnits(ds._getEntity_().getValue());
				break;
			case SYSCRNCY:
				displayedDs.setSysCurrency(ds._getEntity_().getValue());
				break;
			case SYSDENS:
				displayedDs.setSysDensity(new BigDecimal(ds._getEntity_().getValue()));
				break;
			case SYSLENGTH:
				displayedDs.setSysLenght(ds._getEntity_().getValue());
				break;
			case SYSVOL:
				displayedDs.setSysVol(ds._getEntity_().getValue());
				break;
			case SYSAVGMTHD:
				displayedDs.setSysAvgMthd(ds._getEntity_().getValue());
				break;
			case SYSWEIGHT:
				displayedDs.setSysWeight(ds._getEntity_().getValue());
				break;
			default:
				break;
			}
		}
		result.clear();
		result.add(displayedDs);
	}
}
