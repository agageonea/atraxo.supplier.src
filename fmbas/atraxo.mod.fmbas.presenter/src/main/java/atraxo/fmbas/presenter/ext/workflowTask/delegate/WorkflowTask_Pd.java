package atraxo.fmbas.presenter.ext.workflowTask.delegate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.presenter.impl.workflowTask.model.WorkflowTask_Ds;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author vhojda
 */
public class WorkflowTask_Pd extends AbstractPresenterDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowTask_Pd.class);

	/**
	 * @param wkfTask
	 * @throws BusinessException
	 */
	public void claimWorkflowTask(WorkflowTask_Ds wkfTask) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START claimWorkflowTask()");
		}

		IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean("workflowBpmManager");
		workflowBpmManager.claimTask(wkfTask.getWorkflowInstanceId(), wkfTask.getTaskId());

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END claimWorkflowTask()");
		}
	}

	/**
	 * @param wkfTask
	 * @throws BusinessException
	 */
	public void completeWorkflowTask(WorkflowTask_Ds wkfTask) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START completeWorkflowTask()");
		}
		boolean accepted = wkfTask.getWorkflowTaskAcceptance() != null ? wkfTask.getWorkflowTaskAcceptance().booleanValue() : false;

		IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean("workflowBpmManager");
		workflowBpmManager.completeTask(wkfTask.getWorkflowInstanceId(), wkfTask.getTaskId(), accepted, wkfTask.getWorkflowTaskNote());

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END completeWorkflowTask()");
		}
	}

}
