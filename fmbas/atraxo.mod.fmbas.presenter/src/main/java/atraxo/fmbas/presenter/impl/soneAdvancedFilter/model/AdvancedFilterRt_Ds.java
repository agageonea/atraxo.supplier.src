/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.soneAdvancedFilter.model;

import atraxo.fmbas.domain.impl.soneAdvancedFilter.SoneAdvancedFilter;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = SoneAdvancedFilter.class)
public class AdvancedFilterRt_Ds extends AbstractDs_Ds<SoneAdvancedFilter> {

	public static final String ALIAS = "fmbas_AdvancedFilterRt_Ds";

	public static final String F_NAME = "name";
	public static final String F_CMP = "cmp";
	public static final String F_CMPTYPE = "cmpType";
	public static final String F_STANDARDFILTERVALUE = "standardFilterValue";
	public static final String F_ADVANCEDFILTERVALUE = "advancedFilterValue";
	public static final String F_DEFAULTFILTER = "defaultFilter";
	public static final String F_ISSTANDARD = "isStandard";
	public static final String F_ACTIVE = "active";
	public static final String F_AVAILABLESYSTEMWIDE = "availableSystemwide";
	public static final String F_CREATEDBY = "createdBy";

	@DsField
	private String name;

	@DsField
	private String cmp;

	@DsField
	private String cmpType;

	@DsField
	private String standardFilterValue;

	@DsField
	private String advancedFilterValue;

	@DsField
	private Boolean defaultFilter;

	@DsField
	private Boolean isStandard;

	@DsField
	private Boolean active;

	@DsField
	private Boolean availableSystemwide;

	@DsField
	private String createdBy;

	/**
	 * Default constructor
	 */
	public AdvancedFilterRt_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public AdvancedFilterRt_Ds(SoneAdvancedFilter e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCmp() {
		return this.cmp;
	}

	public void setCmp(String cmp) {
		this.cmp = cmp;
	}

	public String getCmpType() {
		return this.cmpType;
	}

	public void setCmpType(String cmpType) {
		this.cmpType = cmpType;
	}

	public String getStandardFilterValue() {
		return this.standardFilterValue;
	}

	public void setStandardFilterValue(String standardFilterValue) {
		this.standardFilterValue = standardFilterValue;
	}

	public String getAdvancedFilterValue() {
		return this.advancedFilterValue;
	}

	public void setAdvancedFilterValue(String advancedFilterValue) {
		this.advancedFilterValue = advancedFilterValue;
	}

	public Boolean getDefaultFilter() {
		return this.defaultFilter;
	}

	public void setDefaultFilter(Boolean defaultFilter) {
		this.defaultFilter = defaultFilter;
	}

	public Boolean getIsStandard() {
		return this.isStandard;
	}

	public void setIsStandard(Boolean isStandard) {
		this.isStandard = isStandard;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getAvailableSystemwide() {
		return this.availableSystemwide;
	}

	public void setAvailableSystemwide(Boolean availableSystemwide) {
		this.availableSystemwide = availableSystemwide;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
}
