/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.ext.customer.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.contacts.IContactsService;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.BusinessType;
import atraxo.fmbas.domain.impl.fmbas_type.Department;
import atraxo.fmbas.domain.impl.fmbas_type.Title;
import atraxo.fmbas.domain.impl.fmbas_type.WorkOffice;
import atraxo.fmbas.presenter.impl.customer.model.CustomersMp_Ds;
import atraxo.fmbas.presenter.impl.customer.model.CustomersMp_DsParam;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service CustomersMp_DsService
 */
public class CustomersMp_DsService extends AbstractEntityDsService<CustomersMp_Ds, CustomersMp_Ds, CustomersMp_DsParam, Customer>
		implements IDsService<CustomersMp_Ds, CustomersMp_Ds, CustomersMp_DsParam> {

	private static final Logger LOG = LoggerFactory.getLogger(CustomersMp_DsService.class);

	@Override
	protected void postInsertAfterModel(CustomersMp_Ds ds, Customer e, CustomersMp_DsParam params) throws Exception {
		super.postInsertAfterModel(ds, e, params);

		if (params != null) {
			Contacts contact = new Contacts();
			contact.setIsPrimary(true);
			contact.setBusinessUnit(Department._EMPTY_);
			contact.setTitle(Title._EMPTY_);
			contact.setWorkOffice(WorkOffice._EMPTY_);
			contact.setFirstName(params.getContactFirstName());
			contact.setLastName(params.getContactLastName());
			contact.setMobilePhone(params.getContactMobilePhone());
			contact.setEmail(params.getContactEmail());
			contact.setObjectId(e.getId());
			contact.setObjectType(e.getEntityAlias());

			IContactsService contactsService = (IContactsService) this.findEntityService(Contacts.class);
			contactsService.insert(contact);
		}
	}

	@Override
	protected void postFind(IQueryBuilder<CustomersMp_Ds, CustomersMp_Ds, CustomersMp_DsParam> builder, List<CustomersMp_Ds> result)
			throws Exception {
		super.postFind(builder, result);
		this.getPrimaryContact(result);
	}

	@Override
	protected void postUpdateAfterModel(CustomersMp_Ds ds, Customer e, CustomersMp_DsParam params) throws Exception {
		super.postUpdateAfterModel(ds, e, params);
		this.getBusinessType(ds, e);
	}

	protected void getPrimaryContact(List<CustomersMp_Ds> result) throws Exception {
		ICustomerService service = (ICustomerService) this.findEntityService(Customer.class);
		Contacts ct;

		for (CustomersMp_Ds ds : result) {
			Customer customer = ds._getEntity_();
			try {
				ct = service.getPrimaryContact(customer);
				ds.setContactName(ct.getFirstName() + " " + ct.getLastName());

				this.getBusinessType(ds, customer);
			} catch (BusinessException e) {
				if (LOG.isDebugEnabled()) {
					LOG.debug(e.getMessage(), e);
				}
				LOG.info("Primary contact not found!");
			}
		}
	}

	protected void getBusinessType(CustomersMp_Ds ds, Customer customer) throws BusinessException {
		if (customer.getIsSupplier()) {
			ds.setBusiness(BusinessType._SUPPLIER_);
		} else if (customer.getIsCustomer()) {
			ds.setBusiness(BusinessType._AIRLINE_);
		}
	}
}
