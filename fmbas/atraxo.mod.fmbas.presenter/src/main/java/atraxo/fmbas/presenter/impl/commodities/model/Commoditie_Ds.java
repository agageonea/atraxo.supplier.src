/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.presenter.impl.commodities.model;

import atraxo.fmbas.domain.impl.commodities.Commoditie;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Commoditie.class, sort = {@SortField(field = Commoditie_Ds.F_NAME)})
public class Commoditie_Ds extends AbstractDs_Ds<Commoditie> {

	public static final String ALIAS = "fmbas_Commoditie_Ds";

	public static final String F_CODE = "code";
	public static final String F_NAME = "name";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";

	@DsField
	private String code;

	@DsField
	private String name;

	@DsField(jpqlFilter = " e.validTo >= :validFrom ")
	private Date validFrom;

	@DsField(jpqlFilter = " e.validFrom <= :validTo ")
	private Date validTo;

	/**
	 * Default constructor
	 */
	public Commoditie_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Commoditie_Ds(Commoditie e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
}
