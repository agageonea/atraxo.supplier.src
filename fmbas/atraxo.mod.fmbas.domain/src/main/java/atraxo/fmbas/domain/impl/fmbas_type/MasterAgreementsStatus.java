/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum MasterAgreementsStatus {

	_EMPTY_(""), _NEGOTIATION_("Negotiation"), _EFFECTIVE_("Effective"), _ON_HOLD_(
			"On hold"), _TERMINATED_("Terminated");

	private String name;

	private MasterAgreementsStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static MasterAgreementsStatus getByName(String name) {
		for (MasterAgreementsStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent MasterAgreementsStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
