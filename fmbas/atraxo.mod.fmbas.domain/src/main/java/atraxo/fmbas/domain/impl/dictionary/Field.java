/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.dictionary;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.annotations.ReadOnly;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link Field} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Field.NQ_FIND_BY_CODE, query = "SELECT e FROM Field e WHERE e.clientId = :clientId and e.externalInterfaceId = :externalInterfaceId and e.fieldName = :fieldName", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Field.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Field e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Field.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Field.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "externalinterfaceid", "fieldname"})})
@ReadOnly
public class Field extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "FIELD";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "Field.findByCode";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Field.findByBusiness";

	@Transient
	private Integer externalInterfaceId;

	@Transient
	private String externalInterfaceName;

	@Transient
	private String fieldName;

	public Integer getExternalInterfaceId() {
		return this.externalInterfaceId;
	}

	public void setExternalInterfaceId(Integer externalInterfaceId) {
		this.externalInterfaceId = externalInterfaceId;
	}

	public String getExternalInterfaceName() {
		return this.externalInterfaceName;
	}

	public void setExternalInterfaceName(String externalInterfaceName) {
		this.externalInterfaceName = externalInterfaceName;
	}

	public String getFieldName() {
		return this.fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
