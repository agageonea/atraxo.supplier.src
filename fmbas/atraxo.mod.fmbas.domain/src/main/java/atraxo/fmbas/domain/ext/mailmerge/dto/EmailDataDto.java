package atraxo.fmbas.domain.ext.mailmerge.dto;

public class EmailDataDto extends AbstractDto {

	/**
	 *
	 */
	private static final long serialVersionUID = -513562208053981661L;
	private String firstName;
	private String lastName;
	private String fullName;
	private String title;
	private String email;
	private SubsidiaryDto subsidiary;
	private CustomerDto customer;
	private WebServiceDto webService;

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public SubsidiaryDto getSubsidiary() {
		return this.subsidiary;
	}

	public void setSubsidiary(SubsidiaryDto subsidiary) {
		this.subsidiary = subsidiary;
	}

	public CustomerDto getCustomer() {
		return this.customer;
	}

	public void setCustomer(CustomerDto customer) {
		this.customer = customer;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public WebServiceDto getWebService() {
		return this.webService;
	}

	public void setWebService(WebServiceDto webService) {
		this.webService = webService;
	}

	@Override
	public String toString() {
		return "EmailDataDto [firstName=" + this.firstName + ", lastName=" + this.lastName + ", fullName=" + this.fullName + ", title=" + this.title
				+ ", email=" + this.email + ", subsidiary=" + this.subsidiary + ", customer=" + this.customer + ", webService=" + this.webService
				+ "]";
	}

}
