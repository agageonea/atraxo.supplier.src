/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum UpliftedVolumePeriod {

	_EMPTY_(""), _TODAY_("Today"), _YESTERDAY_("Yesterday"), _THIS_WEEK_(
			"This week"), _LAST_WEEK_("Last week"), _THIS_MONTH_("This Month"), _LAST_MONTH_(
			"Last Month"), _THIS_YEAR_("This Year"), _LAST_YEAR_("Last Year");

	private String name;

	private UpliftedVolumePeriod(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static UpliftedVolumePeriod getByName(String name) {
		for (UpliftedVolumePeriod status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent UpliftedVolumePeriod with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
