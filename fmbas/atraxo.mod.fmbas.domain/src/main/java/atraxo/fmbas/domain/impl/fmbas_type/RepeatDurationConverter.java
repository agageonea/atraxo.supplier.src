/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link RepeatDuration} enum.
 * Generated code. Do not modify in this file.
 */
public class RepeatDurationConverter
		implements
			AttributeConverter<RepeatDuration, String> {

	@Override
	public String convertToDatabaseColumn(RepeatDuration value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null RepeatDuration.");
		}
		return value.getName();
	}

	@Override
	public RepeatDuration convertToEntityAttribute(String value) {
		return RepeatDuration.getByName(value);
	}

}
