/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.suppliers;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Suppliers} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Suppliers.NQ_FIND_BY_CODE, query = "SELECT e FROM Suppliers e WHERE e.clientId = :clientId and e.code = :code", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Suppliers.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Suppliers e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Suppliers.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Suppliers.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "code"})})
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class Suppliers extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_COMPANIES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "Suppliers.findByCode";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Suppliers.findByBusiness";

	@NotBlank
	@Column(name = "code", nullable = false, length = 32)
	private String code;

	@NotBlank
	@Column(name = "name", nullable = false, length = 100)
	private String name;

	@Column(name = "iata_code", length = 2)
	private String iataCode;

	@Column(name = "ipl_agent")
	private Boolean iplAgent;

	@Column(name = "fuel_supplier")
	private Boolean fuelSupplier;

	@Column(name = "fixed_base_operator")
	private Boolean fixedBaseOperator;

	@Column(name = "notes", length = 255)
	private String notes;

	@Column(name = "is_supplier")
	private Boolean isSupplier;

	@Column(name = "is_customer")
	private Boolean isCustomer;

	@Column(name = "is_third_party")
	private Boolean isThirdParty;

	@Column(name = "is_subsidiary")
	private Boolean isSubsidiary;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = UserSupp.class)
	@JoinColumn(name = "responsible_buyer", referencedColumnName = "id")
	private UserSupp responsibleBuyer;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public Boolean getIplAgent() {
		return this.iplAgent;
	}

	public void setIplAgent(Boolean iplAgent) {
		this.iplAgent = iplAgent;
	}

	public Boolean getFuelSupplier() {
		return this.fuelSupplier;
	}

	public void setFuelSupplier(Boolean fuelSupplier) {
		this.fuelSupplier = fuelSupplier;
	}

	public Boolean getFixedBaseOperator() {
		return this.fixedBaseOperator;
	}

	public void setFixedBaseOperator(Boolean fixedBaseOperator) {
		this.fixedBaseOperator = fixedBaseOperator;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getIsSupplier() {
		return this.isSupplier;
	}

	public void setIsSupplier(Boolean isSupplier) {
		this.isSupplier = isSupplier;
	}

	public Boolean getIsCustomer() {
		return this.isCustomer;
	}

	public void setIsCustomer(Boolean isCustomer) {
		this.isCustomer = isCustomer;
	}

	public Boolean getIsThirdParty() {
		return this.isThirdParty;
	}

	public void setIsThirdParty(Boolean isThirdParty) {
		this.isThirdParty = isThirdParty;
	}

	public Boolean getIsSubsidiary() {
		return this.isSubsidiary;
	}

	public void setIsSubsidiary(Boolean isSubsidiary) {
		this.isSubsidiary = isSubsidiary;
	}

	public UserSupp getResponsibleBuyer() {
		return this.responsibleBuyer;
	}

	public void setResponsibleBuyer(UserSupp responsibleBuyer) {
		if (responsibleBuyer != null) {
			this.__validate_client_context__(responsibleBuyer.getClientId());
		}
		this.responsibleBuyer = responsibleBuyer;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
