/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.workflow;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowParamType;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowParamTypeConverter;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link WorkflowParameter} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = WorkflowParameter.NQ_FIND_BY_KEY, query = "SELECT e FROM WorkflowParameter e WHERE e.clientId = :clientId and e.workflow = :workflow and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = WorkflowParameter.NQ_FIND_BY_KEY_PRIMITIVE, query = "SELECT e FROM WorkflowParameter e WHERE e.clientId = :clientId and e.workflow.id = :workflowId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = WorkflowParameter.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM WorkflowParameter e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = WorkflowParameter.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = WorkflowParameter.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "workflow_id", "name"})})
public class WorkflowParameter extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_WORKFLOW_PARAMS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Key.
	 */
	public static final String NQ_FIND_BY_KEY = "WorkflowParameter.findByKey";
	/**
	 * Named query find by unique key: Key using the ID field for references.
	 */
	public static final String NQ_FIND_BY_KEY_PRIMITIVE = "WorkflowParameter.findByKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "WorkflowParameter.findByBusiness";

	@NotBlank
	@Column(name = "name", nullable = false, length = 255)
	private String name;

	@Column(name = "description", length = 1000)
	private String description;

	@Column(name = "parameter_value", length = 1000)
	private String paramValue;

	@Column(name = "default_value", length = 1000)
	private String defaultValue;

	@NotBlank
	@Column(name = "type", nullable = false, length = 32)
	@Convert(converter = WorkflowParamTypeConverter.class)
	private WorkflowParamType type;

	@Column(name = "mandatory")
	private Boolean mandatory;

	@Column(name = "ref_values", length = 1000)
	private String refValues;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Workflow.class)
	@JoinColumn(name = "workflow_id", referencedColumnName = "id")
	private Workflow workflow;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getParamValue() {
		return this.paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public WorkflowParamType getType() {
		return this.type;
	}

	public void setType(WorkflowParamType type) {
		this.type = type;
	}

	public Boolean getMandatory() {
		return this.mandatory;
	}

	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}

	public String getRefValues() {
		return this.refValues;
	}

	public void setRefValues(String refValues) {
		this.refValues = refValues;
	}

	public Workflow getWorkflow() {
		return this.workflow;
	}

	public void setWorkflow(Workflow workflow) {
		if (workflow != null) {
			this.__validate_client_context__(workflow.getClientId());
		}
		this.workflow = workflow;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.workflow == null) ? 0 : this.workflow.hashCode());
		result = prime * result
				+ ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		WorkflowParameter other = (WorkflowParameter) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.workflow == null) {
			if (other.workflow != null) {
				return false;
			}
		} else if (!this.workflow.equals(other.workflow)) {
			return false;
		}
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		return true;
	}
}
