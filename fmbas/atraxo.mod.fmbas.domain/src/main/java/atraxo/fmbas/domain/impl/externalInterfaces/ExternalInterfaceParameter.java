/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.externalInterfaces;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamType;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamTypeConverter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link ExternalInterfaceParameter} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = ExternalInterfaceParameter.NQ_FIND_BY_KEY, query = "SELECT e FROM ExternalInterfaceParameter e WHERE e.clientId = :clientId and e.externalInterface = :externalInterface and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ExternalInterfaceParameter.NQ_FIND_BY_KEY_PRIMITIVE, query = "SELECT e FROM ExternalInterfaceParameter e WHERE e.clientId = :clientId and e.externalInterface.id = :externalInterfaceId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ExternalInterfaceParameter.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ExternalInterfaceParameter e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ExternalInterfaceParameter.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = ExternalInterfaceParameter.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "interface_id", "name"})})
public class ExternalInterfaceParameter extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "BAS_EXTERNAL_INTERFACES_PARAMS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Key.
	 */
	public static final String NQ_FIND_BY_KEY = "ExternalInterfaceParameter.findByKey";
	/**
	 * Named query find by unique key: Key using the ID field for references.
	 */
	public static final String NQ_FIND_BY_KEY_PRIMITIVE = "ExternalInterfaceParameter.findByKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ExternalInterfaceParameter.findByBusiness";

	@NotBlank
	@Column(name = "name", nullable = false, length = 100)
	private String name;

	@Column(name = "description", length = 1000)
	private String description;

	@Column(name = "current_value", length = 1000)
	private String currentValue;

	@Column(name = "default_value", length = 1000)
	private String defaultValue;

	@Column(name = "business_value", length = 1000)
	private String businessValue;

	@NotBlank
	@Column(name = "type", nullable = false, length = 32)
	@Convert(converter = JobParamTypeConverter.class)
	private JobParamType type;

	@Column(name = "ref_values", length = 255)
	private String refValues;

	@Column(name = "ref_business_values", length = 1000)
	private String refBusinessValues;

	@Column(name = "read_only")
	private Boolean readOnly;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ExternalInterface.class)
	@JoinColumn(name = "interface_id", referencedColumnName = "id")
	private ExternalInterface externalInterface;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCurrentValue() {
		return this.currentValue;
	}

	public void setCurrentValue(String currentValue) {
		this.currentValue = currentValue;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getBusinessValue() {
		return this.businessValue;
	}

	public void setBusinessValue(String businessValue) {
		this.businessValue = businessValue;
	}

	public JobParamType getType() {
		return this.type;
	}

	public void setType(JobParamType type) {
		this.type = type;
	}

	public String getRefValues() {
		return this.refValues;
	}

	public void setRefValues(String refValues) {
		this.refValues = refValues;
	}

	public String getRefBusinessValues() {
		return this.refBusinessValues;
	}

	public void setRefBusinessValues(String refBusinessValues) {
		this.refBusinessValues = refBusinessValues;
	}

	public Boolean getReadOnly() {
		return this.readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	public ExternalInterface getExternalInterface() {
		return this.externalInterface;
	}

	public void setExternalInterface(ExternalInterface externalInterface) {
		if (externalInterface != null) {
			this.__validate_client_context__(externalInterface.getClientId());
		}
		this.externalInterface = externalInterface;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime
				* result
				+ ((this.externalInterface == null)
						? 0
						: this.externalInterface.hashCode());
		result = prime * result
				+ ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		ExternalInterfaceParameter other = (ExternalInterfaceParameter) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.externalInterface == null) {
			if (other.externalInterface != null) {
				return false;
			}
		} else if (!this.externalInterface.equals(other.externalInterface)) {
			return false;
		}
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		return true;
	}
}
