package atraxo.fmbas.domain.ext.mailmerge.dto;

/**
 * DTO Class for Exchange Rate Time Series workflow mail sending process
 *
 * @author aradu
 */
public class ExchangeRateTimeSerieApprovalDto extends AbstractDto {

	private static final long serialVersionUID = 7786121774555897780L;

	private String title;
	private String fullName;

	private String timeSeries;
	private String fullNameOfRequester;
	private String approvalRequestNote;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getTimeSeries() {
		return this.timeSeries;
	}

	public void setTimeSeries(String timeSeries) {
		this.timeSeries = timeSeries;
	}

	public String getFullNameOfRequester() {
		return this.fullNameOfRequester;
	}

	public void setFullNameOfRequester(String fullNameOfRequester) {
		this.fullNameOfRequester = fullNameOfRequester;
	}

	public String getApprovalRequestNote() {
		return this.approvalRequestNote;
	}

	public void setApprovalRequestNote(String approvalRequestNote) {
		this.approvalRequestNote = approvalRequestNote;
	}

	@Override
	public String toString() {
		return "ExchangeRateTimeSerieApprovalDto [title=" + this.title + ", fullName=" + this.fullName + ", timeSeries=" + this.timeSeries
				+ ", fullNameOfRequester=" + this.fullNameOfRequester + ", approvalRequestNote=" + this.approvalRequestNote + "]";
	}

}
