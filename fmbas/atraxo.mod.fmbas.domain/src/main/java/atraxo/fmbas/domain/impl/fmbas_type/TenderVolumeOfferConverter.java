/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TenderVolumeOffer} enum.
 * Generated code. Do not modify in this file.
 */
public class TenderVolumeOfferConverter
		implements
			AttributeConverter<TenderVolumeOffer, String> {

	@Override
	public String convertToDatabaseColumn(TenderVolumeOffer value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TenderVolumeOffer.");
		}
		return value.getName();
	}

	@Override
	public TenderVolumeOffer convertToEntityAttribute(String value) {
		return TenderVolumeOffer.getByName(value);
	}

}
