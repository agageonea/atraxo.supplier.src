/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum DictionaryOperator {

	_EMPTY_(""), _Equal_("="), _NotEqual_("!=");

	private String name;

	private DictionaryOperator(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static DictionaryOperator getByName(String name) {
		for (DictionaryOperator status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent DictionaryOperator with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
