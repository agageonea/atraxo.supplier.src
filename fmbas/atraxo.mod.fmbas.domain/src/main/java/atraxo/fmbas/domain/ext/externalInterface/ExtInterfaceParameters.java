package atraxo.fmbas.domain.ext.externalInterface;

/**
 * Enumeration to store external interface parameter names.
 *
 * @author zspeter
 */
public enum ExtInterfaceParameters {
	TIMEOUT_INTTERVAL("Timeout interval");

	private String displayName;

	private ExtInterfaceParameters(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return this.displayName;
	}

}
