/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum InterfaceNotificationCondition {

	_EMPTY_(""), _ON_FAILURE_("On failure"), _ON_SUCCESS_("On success"), _ALL_EVENTS_(
			"All events");

	private String name;

	private InterfaceNotificationCondition(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static InterfaceNotificationCondition getByName(String name) {
		for (InterfaceNotificationCondition status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent InterfaceNotificationCondition with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
