/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum NotificationType {

	_EMPTY_(""), _EMAIL_("E-mail message"), _EVENT_("Notification event"), _BOTH_(
			"Both");

	private String name;

	private NotificationType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static NotificationType getByName(String name) {
		for (NotificationType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent NotificationType with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
