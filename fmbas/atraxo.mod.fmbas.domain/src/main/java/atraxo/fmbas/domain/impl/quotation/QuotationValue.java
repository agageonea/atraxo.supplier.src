/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.quotation;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link QuotationValue} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = QuotationValue.NQ_FIND_BY_BUSINESSKEY, query = "SELECT e FROM QuotationValue e WHERE e.clientId = :clientId and e.quotation = :quotation and e.validFromDate = :validFromDate and e.validToDate = :validToDate", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = QuotationValue.NQ_FIND_BY_BUSINESSKEY_PRIMITIVE, query = "SELECT e FROM QuotationValue e WHERE e.clientId = :clientId and e.quotation.id = :quotationId and e.validFromDate = :validFromDate and e.validToDate = :validToDate", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = QuotationValue.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM QuotationValue e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = QuotationValue.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = QuotationValue.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "quot_id", "vld_fr_dt_ref",
		"vld_to_dt_ref"})})
public class QuotationValue extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_QUOTVAL";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: BusinessKey.
	 */
	public static final String NQ_FIND_BY_BUSINESSKEY = "QuotationValue.findByBusinessKey";
	/**
	 * Named query find by unique key: BusinessKey using the ID field for references.
	 */
	public static final String NQ_FIND_BY_BUSINESSKEY_PRIMITIVE = "QuotationValue.findByBusinessKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "QuotationValue.findByBusiness";

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "vld_fr_dt_ref", nullable = false)
	private Date validFromDate;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "vld_to_dt_ref", nullable = false)
	private Date validToDate;

	@NotNull
	@Column(name = "is_prvsn", nullable = false)
	private Boolean isProvisioned;

	@NotNull
	@Column(name = "rate", nullable = false, precision = 19, scale = 6)
	private BigDecimal rate;

	@Column(name = "active")
	private Boolean active;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Quotation.class)
	@JoinColumn(name = "quot_id", referencedColumnName = "id")
	private Quotation quotation;

	public Date getValidFromDate() {
		return this.validFromDate;
	}

	public void setValidFromDate(Date validFromDate) {
		this.validFromDate = validFromDate;
	}

	public Date getValidToDate() {
		return this.validToDate;
	}

	public void setValidToDate(Date validToDate) {
		this.validToDate = validToDate;
	}

	public Boolean getIsProvisioned() {
		return this.isProvisioned;
	}

	public void setIsProvisioned(Boolean isProvisioned) {
		this.isProvisioned = isProvisioned;
	}

	public BigDecimal getRate() {
		return this.rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Quotation getQuotation() {
		return this.quotation;
	}

	public void setQuotation(Quotation quotation) {
		if (quotation != null) {
			this.__validate_client_context__(quotation.getClientId());
		}
		this.quotation = quotation;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.quotation == null) ? 0 : this.quotation.hashCode());
		result = prime
				* result
				+ ((this.validFromDate == null) ? 0 : this.validFromDate
						.hashCode());
		result = prime
				* result
				+ ((this.validToDate == null) ? 0 : this.validToDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		QuotationValue other = (QuotationValue) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.quotation == null) {
			if (other.quotation != null) {
				return false;
			}
		} else if (!this.quotation.equals(other.quotation)) {
			return false;
		}
		if (this.validFromDate == null) {
			if (other.validFromDate != null) {
				return false;
			}
		} else if (!this.validFromDate.equals(other.validFromDate)) {
			return false;
		}
		if (this.validToDate == null) {
			if (other.validToDate != null) {
				return false;
			}
		} else if (!this.validToDate.equals(other.validToDate)) {
			return false;
		}
		return true;
	}
}
