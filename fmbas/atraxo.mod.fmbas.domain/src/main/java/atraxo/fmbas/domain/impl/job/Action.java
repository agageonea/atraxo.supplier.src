/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.job;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.ActionParameter;
import atraxo.fmbas.domain.impl.job.BatchJob;
import atraxo.fmbas.domain.impl.job.JobChain;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link Action} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = Action.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Action e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Action.TABLE_NAME)
@Cache(coordinationType = CacheCoordinationType.SEND_NEW_OBJECTS_WITH_CHANGES, isolation = CacheIsolationType.ISOLATED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class Action extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_JOB_ACTIONS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Action.findByBusiness";

	@Column(name = "name", length = 32)
	private String name;

	@Column(name = "description", length = 255)
	private String description;

	@NotNull
	@Column(name = "seq", nullable = false, precision = 2)
	private Integer order;

	@Column(name = "job_uid", length = 50)
	private String jobUID;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = JobChain.class)
	@JoinColumn(name = "job_chain_id", referencedColumnName = "id")
	private JobChain jobChain;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Action.class)
	@JoinColumn(name = "next_action_id", referencedColumnName = "id")
	private Action nextAction;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = BatchJob.class)
	@JoinColumn(name = "batch_job_id", referencedColumnName = "id")
	private BatchJob batchJob;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = ActionParameter.class, mappedBy = "jobAction", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<ActionParameter> actionParameters;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getOrder() {
		return this.order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public String getJobUID() {
		return this.jobUID;
	}

	public void setJobUID(String jobUID) {
		this.jobUID = jobUID;
	}

	public JobChain getJobChain() {
		return this.jobChain;
	}

	public void setJobChain(JobChain jobChain) {
		if (jobChain != null) {
			this.__validate_client_context__(jobChain.getClientId());
		}
		this.jobChain = jobChain;
	}
	public Action getNextAction() {
		return this.nextAction;
	}

	public void setNextAction(Action nextAction) {
		if (nextAction != null) {
			this.__validate_client_context__(nextAction.getClientId());
		}
		this.nextAction = nextAction;
	}
	public BatchJob getBatchJob() {
		return this.batchJob;
	}

	public void setBatchJob(BatchJob batchJob) {
		if (batchJob != null) {
			this.__validate_client_context__(batchJob.getClientId());
		}
		this.batchJob = batchJob;
	}

	public Collection<ActionParameter> getActionParameters() {
		return this.actionParameters;
	}

	public void setActionParameters(Collection<ActionParameter> actionParameters) {
		this.actionParameters = actionParameters;
	}

	/**
	 * @param e
	 */
	public void addToActionParameters(ActionParameter e) {
		if (this.actionParameters == null) {
			this.actionParameters = new ArrayList<>();
		}
		e.setJobAction(this);
		this.actionParameters.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
