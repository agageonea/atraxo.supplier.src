/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.timeserie;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link CompositeTimeSeriesSource} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = CompositeTimeSeriesSource.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM CompositeTimeSeriesSource e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = CompositeTimeSeriesSource.TABLE_NAME)
public class CompositeTimeSeriesSource extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "BAS_COMP_TSERIE_SOURCE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "CompositeTimeSeriesSource.findByBusiness";

	@NotNull
	@Column(name = "weighting", nullable = false, precision = 2)
	private Integer weighting;

	@NotNull
	@Column(name = "factor", nullable = false, precision = 19, scale = 6)
	private BigDecimal factor;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = TimeSerie.class)
	@JoinColumn(name = "composite_id", referencedColumnName = "id")
	private TimeSerie composite;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = TimeSerie.class)
	@JoinColumn(name = "source_id", referencedColumnName = "id")
	private TimeSerie source;

	public Integer getWeighting() {
		return this.weighting;
	}

	public void setWeighting(Integer weighting) {
		this.weighting = weighting;
	}

	public BigDecimal getFactor() {
		return this.factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

	public TimeSerie getComposite() {
		return this.composite;
	}

	public void setComposite(TimeSerie composite) {
		if (composite != null) {
			this.__validate_client_context__(composite.getClientId());
		}
		this.composite = composite;
	}
	public TimeSerie getSource() {
		return this.source;
	}

	public void setSource(TimeSerie source) {
		if (source != null) {
			this.__validate_client_context__(source.getClientId());
		}
		this.source = source;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
