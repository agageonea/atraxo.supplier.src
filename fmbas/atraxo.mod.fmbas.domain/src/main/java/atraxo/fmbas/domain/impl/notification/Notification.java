/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.notification;

import atraxo.ad.domain.impl.ad.Category;
import atraxo.ad.domain.impl.ad.CategoryConverter;
import atraxo.ad.domain.impl.ad.NotificationAction;
import atraxo.ad.domain.impl.ad.NotificationActionConverter;
import atraxo.ad.domain.impl.ad.Priority;
import atraxo.ad.domain.impl.ad.PriorityConverter;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Notification} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = Notification.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Notification e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Notification.TABLE_NAME)
public class Notification extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_NOTIFICATIONS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Notification.findByBusiness";

	@NotBlank
	@Column(name = "title", nullable = false, length = 64)
	private String title;

	@Column(name = "descripion", length = 255)
	private String descripion;

	@NotBlank
	@Column(name = "priority", nullable = false, length = 50)
	@Convert(converter = PriorityConverter.class)
	private Priority priority;

	@NotBlank
	@Column(name = "category", nullable = false, length = 50)
	@Convert(converter = CategoryConverter.class)
	private Category category;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "eventdate")
	private Date eventDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "lifetime")
	private Date lifeTime;

	@NotBlank
	@Column(name = "action", nullable = false, length = 50)
	@Convert(converter = NotificationActionConverter.class)
	private NotificationAction action;

	@Column(name = "link", length = 255)
	private String link;

	@Transient
	private String status;

	@Transient
	private String result;

	@Column(name = "method_name", length = 64)
	private String methodName;

	@Column(name = "method_param", length = 255)
	private String methodParam;

	@ManyToMany
	@JoinTable(name = "BAS_USERS_NOTIFICATIONS", joinColumns = {@JoinColumn(name = "notifications_id")}, inverseJoinColumns = {@JoinColumn(name = "users_id")})
	private Collection<UserSupp> users;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescripion() {
		return this.descripion;
	}

	public void setDescripion(String descripion) {
		this.descripion = descripion;
	}

	public Priority getPriority() {
		return this.priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Date getEventDate() {
		return this.eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Date getLifeTime() {
		return this.lifeTime;
	}

	public void setLifeTime(Date lifeTime) {
		this.lifeTime = lifeTime;
	}

	public NotificationAction getAction() {
		return this.action;
	}

	public void setAction(NotificationAction action) {
		this.action = action;
	}

	public String getLink() {
		return this.link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMethodName() {
		return this.methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getMethodParam() {
		return this.methodParam;
	}

	public void setMethodParam(String methodParam) {
		this.methodParam = methodParam;
	}

	public Collection<UserSupp> getUsers() {
		return this.users;
	}

	public void setUsers(Collection<UserSupp> users) {
		this.users = users;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
