/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ValueType} enum.
 * Generated code. Do not modify in this file.
 */
public class ValueTypeConverter
		implements
			AttributeConverter<ValueType, String> {

	@Override
	public String convertToDatabaseColumn(ValueType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ValueType.");
		}
		return value.getName();
	}

	@Override
	public ValueType convertToEntityAttribute(String value) {
		return ValueType.getByName(value);
	}

}
