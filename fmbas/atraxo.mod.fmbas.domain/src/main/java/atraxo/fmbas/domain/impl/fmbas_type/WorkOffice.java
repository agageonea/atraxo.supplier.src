/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum WorkOffice {

	_EMPTY_(""), _HEAD_OFFICE_("Head office"), _BRANCH_("Branch"), _AIRPORT_OFFICE_(
			"Airport office");

	private String name;

	private WorkOffice(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static WorkOffice getByName(String name) {
		for (WorkOffice status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent WorkOffice with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
