/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.timeserie;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link TimeSerieAverage} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = TimeSerieAverage.NQ_FIND_BY_TSERIEAVG, query = "SELECT e FROM TimeSerieAverage e WHERE e.clientId = :clientId and e.tserie = :tserie and e.averagingMethod = :averagingMethod", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = TimeSerieAverage.NQ_FIND_BY_TSERIEAVG_PRIMITIVE, query = "SELECT e FROM TimeSerieAverage e WHERE e.clientId = :clientId and e.tserie.id = :tserieId and e.averagingMethod.id = :averagingMethodId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = TimeSerieAverage.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM TimeSerieAverage e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = TimeSerieAverage.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = TimeSerieAverage.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "tserie_id", "avg_mthd_id"})})
public class TimeSerieAverage extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_TSERIE_AVERAGE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: TserieAvg.
	 */
	public static final String NQ_FIND_BY_TSERIEAVG = "TimeSerieAverage.findByTserieAvg";
	/**
	 * Named query find by unique key: TserieAvg using the ID field for references.
	 */
	public static final String NQ_FIND_BY_TSERIEAVG_PRIMITIVE = "TimeSerieAverage.findByTserieAvg_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "TimeSerieAverage.findByBusiness";

	@Column(name = "effective_offset_day", precision = 11)
	private Integer effectiveOffsetDay;

	@Column(name = "effective_offset_interval", precision = 11)
	private Integer effectiveOffsetInterval;

	@Column(name = "active")
	private Boolean active;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = TimeSerie.class)
	@JoinColumn(name = "tserie_id", referencedColumnName = "id")
	private TimeSerie tserie;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AverageMethod.class)
	@JoinColumn(name = "avg_mthd_id", referencedColumnName = "id")
	private AverageMethod averagingMethod;

	public Integer getEffectiveOffsetDay() {
		return this.effectiveOffsetDay;
	}

	public void setEffectiveOffsetDay(Integer effectiveOffsetDay) {
		this.effectiveOffsetDay = effectiveOffsetDay;
	}

	public Integer getEffectiveOffsetInterval() {
		return this.effectiveOffsetInterval;
	}

	public void setEffectiveOffsetInterval(Integer effectiveOffsetInterval) {
		this.effectiveOffsetInterval = effectiveOffsetInterval;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public TimeSerie getTserie() {
		return this.tserie;
	}

	public void setTserie(TimeSerie tserie) {
		if (tserie != null) {
			this.__validate_client_context__(tserie.getClientId());
		}
		this.tserie = tserie;
	}
	public AverageMethod getAveragingMethod() {
		return this.averagingMethod;
	}

	public void setAveragingMethod(AverageMethod averagingMethod) {
		if (averagingMethod != null) {
			this.__validate_client_context__(averagingMethod.getClientId());
		}
		this.averagingMethod = averagingMethod;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
