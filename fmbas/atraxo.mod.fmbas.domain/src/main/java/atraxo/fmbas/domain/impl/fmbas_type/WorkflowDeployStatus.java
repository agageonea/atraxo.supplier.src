/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum WorkflowDeployStatus {

	_EMPTY_(""), _DEPLOYED_("Deployed"), _NEW_("New");

	private String name;

	private WorkflowDeployStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static WorkflowDeployStatus getByName(String name) {
		for (WorkflowDeployStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent WorkflowDeployStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
