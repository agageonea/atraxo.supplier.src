/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link Department} enum.
 * Generated code. Do not modify in this file.
 */
public class DepartmentConverter
		implements
			AttributeConverter<Department, String> {

	@Override
	public String convertToDatabaseColumn(Department value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null Department.");
		}
		return value.getName();
	}

	@Override
	public Department convertToEntityAttribute(String value) {
		return Department.getByName(value);
	}

}
