/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum JobRunResult {

	_EMPTY_(""), _SUCCESS_("Success"), _FAILURE_("Failure"), _COMPLETED___FAILED_(
			"Completed - Failed"), _COMPLETED___SUCCESSFUL_(
			"Completed - Successful");

	private String name;

	private JobRunResult(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static JobRunResult getByName(String name) {
		for (JobRunResult status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent JobRunResult with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
