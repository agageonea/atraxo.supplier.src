/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link NatureOfBusiness} enum.
 * Generated code. Do not modify in this file.
 */
public class NatureOfBusinessConverter
		implements
			AttributeConverter<NatureOfBusiness, String> {

	@Override
	public String convertToDatabaseColumn(NatureOfBusiness value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null NatureOfBusiness.");
		}
		return value.getName();
	}

	@Override
	public NatureOfBusiness convertToEntityAttribute(String value) {
		return NatureOfBusiness.getByName(value);
	}

}
