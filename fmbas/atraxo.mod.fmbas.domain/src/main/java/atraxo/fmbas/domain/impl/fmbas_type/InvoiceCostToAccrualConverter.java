/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link InvoiceCostToAccrual} enum.
 * Generated code. Do not modify in this file.
 */
public class InvoiceCostToAccrualConverter
		implements
			AttributeConverter<InvoiceCostToAccrual, String> {

	@Override
	public String convertToDatabaseColumn(InvoiceCostToAccrual value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null InvoiceCostToAccrual.");
		}
		return value.getName();
	}

	@Override
	public InvoiceCostToAccrual convertToEntityAttribute(String value) {
		return InvoiceCostToAccrual.getByName(value);
	}

}
