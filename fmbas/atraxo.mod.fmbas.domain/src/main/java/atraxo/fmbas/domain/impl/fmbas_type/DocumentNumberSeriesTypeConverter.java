/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link DocumentNumberSeriesType} enum.
 * Generated code. Do not modify in this file.
 */
public class DocumentNumberSeriesTypeConverter
		implements
			AttributeConverter<DocumentNumberSeriesType, String> {

	@Override
	public String convertToDatabaseColumn(DocumentNumberSeriesType value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null DocumentNumberSeriesType.");
		}
		return value.getName();
	}

	@Override
	public DocumentNumberSeriesType convertToEntityAttribute(String value) {
		return DocumentNumberSeriesType.getByName(value);
	}

}
