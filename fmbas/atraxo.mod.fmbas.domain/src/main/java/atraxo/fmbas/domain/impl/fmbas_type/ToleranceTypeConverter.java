/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ToleranceType} enum.
 * Generated code. Do not modify in this file.
 */
public class ToleranceTypeConverter
		implements
			AttributeConverter<ToleranceType, String> {

	@Override
	public String convertToDatabaseColumn(ToleranceType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ToleranceType.");
		}
		return value.getName();
	}

	@Override
	public ToleranceType convertToEntityAttribute(String value) {
		return ToleranceType.getByName(value);
	}

}
