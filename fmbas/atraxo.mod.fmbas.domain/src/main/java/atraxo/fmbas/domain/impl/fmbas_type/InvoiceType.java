/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum InvoiceType {

	_EMPTY_("", ""), _XML_2_0_2_("XML_2_0_2", "XML_2_0_2"), _XML_3_0_0_(
			"XML_3_0_0", "XML_3_0_0"), _XML_3_0_1_("XML_3_0_1", "XML_3_0_1"), _XML_3_1_0_(
			"XML_3_1_0", "XML_3_1_0"), _EXCEL_("Excel", "EXC"), _PAPER_(
			"Paper", "PPR"), _EDI_("EDI", "EDI"), _PDF_FORMAT_("PDF Format",
			"PDF"), _FISCAL_("Fiscal", "CCF"), _EXPORT_("Export", "FEX"), _REGULAR_(
			"Regular", "FCF");

	private String name;
	private String code;

	private InvoiceType(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static InvoiceType getByName(String name) {
		for (InvoiceType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent InvoiceType with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static InvoiceType getByCode(String code) {
		for (InvoiceType status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent InvoiceType with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
