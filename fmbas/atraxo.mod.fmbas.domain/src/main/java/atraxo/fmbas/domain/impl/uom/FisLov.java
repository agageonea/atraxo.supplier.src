/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.uom;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.ReadOnly;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link FisLov} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = FisLov.NQ_FIND_BY_ID, query = "SELECT e FROM FisLov e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = FisLov.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM FisLov e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = FisLov.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = FisLov.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "id"})})
@ReadOnly
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class FisLov extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_FIS_LOV";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Id.
	 */
	public static final String NQ_FIND_BY_ID = "FisLov.findById";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "FisLov.findByBusiness";

	@Column(name = "lov_system", length = 32)
	private String lovSystem;

	@Column(name = "lov_table", length = 35)
	private String lovTable;

	@Column(name = "code_1", length = 20)
	private String code1;

	@Column(name = "code_2", length = 20)
	private String code2;

	@Column(name = "attribut_1", length = 50)
	private String attribut1;

	@Column(name = "attribut_2", length = 50)
	private String attribut2;

	@Column(name = "pos", precision = 4)
	private Integer pos;

	@Column(name = "active")
	private Boolean active;

	public String getLovSystem() {
		return this.lovSystem;
	}

	public void setLovSystem(String lovSystem) {
		this.lovSystem = lovSystem;
	}

	public String getLovTable() {
		return this.lovTable;
	}

	public void setLovTable(String lovTable) {
		this.lovTable = lovTable;
	}

	public String getCode1() {
		return this.code1;
	}

	public void setCode1(String code1) {
		this.code1 = code1;
	}

	public String getCode2() {
		return this.code2;
	}

	public void setCode2(String code2) {
		this.code2 = code2;
	}

	public String getAttribut1() {
		return this.attribut1;
	}

	public void setAttribut1(String attribut1) {
		this.attribut1 = attribut1;
	}

	public String getAttribut2() {
		return this.attribut2;
	}

	public void setAttribut2(String attribut2) {
		this.attribut2 = attribut2;
	}

	public Integer getPos() {
		return this.pos;
	}

	public void setPos(Integer pos) {
		this.pos = pos;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
