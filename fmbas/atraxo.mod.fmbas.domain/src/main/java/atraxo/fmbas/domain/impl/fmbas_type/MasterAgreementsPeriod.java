/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum MasterAgreementsPeriod {

	_EMPTY_("", 0), _CURRENT_("Current", 0), _PREVIOUS_("Previous", -1), _CURRENT__2_(
			"Current -2", -2), _CURRENT__1_("Current +1", 1);

	private String name;
	private int delta;

	private MasterAgreementsPeriod(String name, int delta) {
		this.name = name;
		this.delta = delta;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static MasterAgreementsPeriod getByName(String name) {
		for (MasterAgreementsPeriod status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent MasterAgreementsPeriod with name: " + name);
	}

	public int getDelta() {
		return this.delta;
	}

	public static MasterAgreementsPeriod getByDelta(int code) {
		for (MasterAgreementsPeriod status : values()) {
			if (status.getDelta() == code) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent MasterAgreementsPeriod with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
