/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ProductSource {

	_EMPTY_(""), _STANDARD_("Standard"), _CUSTOM_("Custom");

	private String name;

	private ProductSource(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ProductSource getByName(String name) {
		for (ProductSource status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent ProductSource with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
