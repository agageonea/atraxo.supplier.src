/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.vat;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.vat.Vat;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link VatRate} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = VatRate.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM VatRate e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = VatRate.TABLE_NAME)
public class VatRate extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_VAT_RATES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "VatRate.findByBusiness";

	@Column(name = "rate", precision = 19, scale = 6)
	private BigDecimal rate;

	@Temporal(TemporalType.DATE)
	@Column(name = "valid_from")
	private Date validFrom;

	@Temporal(TemporalType.DATE)
	@Column(name = "valid_to")
	private Date validTo;

	@Column(name = "note", length = 255)
	private String note;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Vat.class)
	@JoinColumn(name = "vat_id", referencedColumnName = "id")
	private Vat vat;

	public BigDecimal getRate() {
		return this.rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Vat getVat() {
		return this.vat;
	}

	public void setVat(Vat vat) {
		if (vat != null) {
			this.__validate_client_context__(vat.getClientId());
		}
		this.vat = vat;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
