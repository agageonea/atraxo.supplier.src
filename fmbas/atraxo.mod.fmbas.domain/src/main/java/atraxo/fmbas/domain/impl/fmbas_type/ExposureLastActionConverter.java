/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ExposureLastAction} enum.
 * Generated code. Do not modify in this file.
 */
public class ExposureLastActionConverter
		implements
			AttributeConverter<ExposureLastAction, String> {

	@Override
	public String convertToDatabaseColumn(ExposureLastAction value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null ExposureLastAction.");
		}
		return value.getName();
	}

	@Override
	public ExposureLastAction convertToEntityAttribute(String value) {
		return ExposureLastAction.getByName(value);
	}

}
