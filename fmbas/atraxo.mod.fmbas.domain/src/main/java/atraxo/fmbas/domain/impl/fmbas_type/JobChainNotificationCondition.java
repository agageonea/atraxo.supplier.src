/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum JobChainNotificationCondition {

	_EMPTY_(""), _ON_FAILURE_("On failure"), _ON_SUCCESS_("On success"), _ALL_EVENTS_(
			"All events");

	private String name;

	private JobChainNotificationCondition(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static JobChainNotificationCondition getByName(String name) {
		for (JobChainNotificationCondition status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent JobChainNotificationCondition with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
