/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum CustomerType {

	_EMPTY_(""), _INDEPENDENT_("Independent"), _GROUP_("Group"), _GROUP_MEMBER_(
			"Group Member");

	private String name;

	private CustomerType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static CustomerType getByName(String name) {
		for (CustomerType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent CustomerType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
