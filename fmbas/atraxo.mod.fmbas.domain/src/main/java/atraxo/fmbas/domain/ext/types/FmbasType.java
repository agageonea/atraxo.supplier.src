package atraxo.fmbas.domain.ext.types;

public class FmbasType {

	// ParamType domain type
	public static final String ParamType__NOT_AVAILABLE = "Not available";
	public static final String ParamType__ACTYPE = "A/C type";
	public static final String ParamType__CURRENCY = "Currency";
	public static final String ParamType__SUPPLIER = "Supplier";
	public static final String ParamType__UPLIFT_STATUS = "Uplift status";
	public static final String ParamType__STORAGE_TYPE = "Storage type";

	// ToleranceType domain type
	public static final String ToleranceType__QUANTITY = "Quantity";
	public static final String ToleranceType__DIMENSIONLESS_NUMBER = "Dimensionless number";
	public static final String ToleranceType__PRICE = "Price";
	public static final String ToleranceType__COST = "Cost";

	// PostTypeInd domain type
	public static final String PostTypeInd__AMOUNT = "1 [Currency] = value [Ref.Currency]";
	public static final String PostTypeInd__PRICE = "1 [Ref.Currency] = value [Currency]";

	// DataProvider domain type
	public static final String DataProvider__CALCULATED = "Calculated";
	public static final String DataProvider__IMPORT_ARGUS = "IMPORT/ARGUS";
	public static final String DataProvider__IMPORT_DDS = "IMPORT/DDS";
	public static final String DataProvider__IMPORT_OPIS = "IMPORT/OPIS";
	public static final String DataProvider__IMPORT_SHARE = "IMPORT/SHARE";
	public static final String DataProvider__IMPORT_PLATTS = "IMPORT/PLATTS";
	public static final String DataProvider__USER_MAINTAINED = "User maintained";

	// SerieFreqInd domain type
	public static final String SerieFreqInd__DAILY = "Daily";
	public static final String SerieFreqInd__WEEKDAY = "Weekday";
	public static final String SerieFreqInd__WEEKLY = "Weekly";
	public static final String SerieFreqInd__SEMIMONTHLY = "Semimonthly";
	public static final String SerieFreqInd__MONTHLY = "Monthly";
	public static final String SerieFreqInd__QUARTERLY = "Quarterly";

	// PriceInd domain type
	public static final String PriceInd_VOLUME = "Volume";
	public static final String PriceInd_EVENT = "Event";
	public static final String PriceInd_PERIOD = "Period";
	public static final String PriceInd_PERCENT = "Percent";

	// UnitType domain type
	public static final String UnitType_MASS = "Mass";
	public static final String UnitType_VOLUME = "Volume";
	public static final String UnitType_PERCENT = "Percent";
	public static final String UnitType_EACH = "Each";
	public static final String UnitType_TEMPERATURE = "Temperature";
	public static final String UnitType_DURATION = "Duration";
	public static final String UnitType_LENGHT = "Lenght";
}
