/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum DocumentNumberSeriesType {

	_EMPTY_(""), _INVOICE_("Invoice"), _CUSTOMER_("Customer"), _CREDIT_MEMO_(
			"Credit Memo"), _CONSOLIDATED_STATEMENT_("Consolidated statement");

	private String name;

	private DocumentNumberSeriesType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static DocumentNumberSeriesType getByName(String name) {
		for (DocumentNumberSeriesType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent DocumentNumberSeriesType with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
