package atraxo.fmbas.domain.ext.masteragreement;

public enum ReferenceTo {
	INVOICE_DATE("Invoice date"), RECEIVING_DATE("Receiving date of the invoice"), LAST_DELIVERY("Last delivery date");

	private String displayName;

	private ReferenceTo(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public ReferenceTo getByDisplayName(String displayName) {
		for (ReferenceTo referenceTo : values()) {
			if (referenceTo.getDisplayName().equalsIgnoreCase(displayName)) {
				return referenceTo;
			}
		}
		throw new RuntimeException("Inexistent reference to with name: " + displayName);
	}

}
