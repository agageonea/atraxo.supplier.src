/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.workflow;

import atraxo.ad.domain.impl.security.User;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowInstanceStatus;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowInstanceStatusConverter;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link WorkflowInstance} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = WorkflowInstance.NQ_FIND_BY_KEY, query = "SELECT e FROM WorkflowInstance e WHERE e.clientId = :clientId and e.workflow = :workflow and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = WorkflowInstance.NQ_FIND_BY_KEY_PRIMITIVE, query = "SELECT e FROM WorkflowInstance e WHERE e.clientId = :clientId and e.workflow.id = :workflowId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = WorkflowInstance.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM WorkflowInstance e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = WorkflowInstance.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = WorkflowInstance.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "workflow_id", "name"})})
public class WorkflowInstance extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_WORKFLOW_INSTANCE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Key.
	 */
	public static final String NQ_FIND_BY_KEY = "WorkflowInstance.findByKey";
	/**
	 * Named query find by unique key: Key using the ID field for references.
	 */
	public static final String NQ_FIND_BY_KEY_PRIMITIVE = "WorkflowInstance.findByKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "WorkflowInstance.findByBusiness";

	@NotBlank
	@Column(name = "name", nullable = false, length = 255)
	private String name;

	@Column(name = "workflow_instance_status", length = 32)
	@Convert(converter = WorkflowInstanceStatusConverter.class)
	private WorkflowInstanceStatus status;

	@Column(name = "step", length = 100)
	private String step;

	@Column(name = "description", length = 1000)
	private String description;

	@Column(name = "workflowversion", precision = 10)
	private Integer workflowVersion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "startexecutiondate")
	private Date startExecutionDate;

	@Column(name = "result", length = 1000)
	private String result;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Workflow.class)
	@JoinColumn(name = "workflow_id", referencedColumnName = "id")
	private Workflow workflow;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User startExecutionUser;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = WorkflowInstanceEntity.class, mappedBy = "workflowInstance", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<WorkflowInstanceEntity> entitites;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public WorkflowInstanceStatus getStatus() {
		return this.status;
	}

	public void setStatus(WorkflowInstanceStatus status) {
		this.status = status;
	}

	public String getStep() {
		return this.step;
	}

	public void setStep(String step) {
		this.step = step;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getWorkflowVersion() {
		return this.workflowVersion;
	}

	public void setWorkflowVersion(Integer workflowVersion) {
		this.workflowVersion = workflowVersion;
	}

	public Date getStartExecutionDate() {
		return this.startExecutionDate;
	}

	public void setStartExecutionDate(Date startExecutionDate) {
		this.startExecutionDate = startExecutionDate;
	}

	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Workflow getWorkflow() {
		return this.workflow;
	}

	public void setWorkflow(Workflow workflow) {
		if (workflow != null) {
			this.__validate_client_context__(workflow.getClientId());
		}
		this.workflow = workflow;
	}
	public User getStartExecutionUser() {
		return this.startExecutionUser;
	}

	public void setStartExecutionUser(User startExecutionUser) {
		if (startExecutionUser != null) {
			this.__validate_client_context__(startExecutionUser.getClientId());
		}
		this.startExecutionUser = startExecutionUser;
	}

	public Collection<WorkflowInstanceEntity> getEntitites() {
		return this.entitites;
	}

	public void setEntitites(Collection<WorkflowInstanceEntity> entitites) {
		this.entitites = entitites;
	}

	/**
	 * @param e
	 */
	public void addToEntitites(WorkflowInstanceEntity e) {
		if (this.entitites == null) {
			this.entitites = new ArrayList<>();
		}
		e.setWorkflowInstance(this);
		this.entitites.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.workflow == null) ? 0 : this.workflow.hashCode());
		result = prime * result
				+ ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		WorkflowInstance other = (WorkflowInstance) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.workflow == null) {
			if (other.workflow != null) {
				return false;
			}
		} else if (!this.workflow.equals(other.workflow)) {
			return false;
		}
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		return true;
	}
}
