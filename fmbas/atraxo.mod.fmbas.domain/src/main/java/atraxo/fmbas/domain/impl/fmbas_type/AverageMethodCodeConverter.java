/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link AverageMethodCode} enum.
 * Generated code. Do not modify in this file.
 */
public class AverageMethodCodeConverter
		implements
			AttributeConverter<AverageMethodCode, String> {

	@Override
	public String convertToDatabaseColumn(AverageMethodCode value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null AverageMethodCode.");
		}
		return value.getName();
	}

	@Override
	public AverageMethodCode convertToEntityAttribute(String value) {
		return AverageMethodCode.getByName(value);
	}

}
