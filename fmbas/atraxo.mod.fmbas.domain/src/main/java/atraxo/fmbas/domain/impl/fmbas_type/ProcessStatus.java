/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ProcessStatus {

	_EMPTY_(""), _SUCCESS_("Success"), _FAILURE_("Failure");

	private String name;

	private ProcessStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ProcessStatus getByName(String name) {
		for (ProcessStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent ProcessStatus with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
