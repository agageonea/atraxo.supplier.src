/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.user;

import atraxo.abstracts.domain.impl.tenant.AbstractTypeWithCode;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link RoleSupp} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = RoleSupp.NQ_FIND_BY_CODE, query = "SELECT e FROM RoleSupp e WHERE e.clientId = :clientId and e.code = :code", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = RoleSupp.NQ_FIND_BY_NAME, query = "SELECT e FROM RoleSupp e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = RoleSupp.TABLE_NAME, uniqueConstraints = {
		@UniqueConstraint(name = RoleSupp.TABLE_NAME + "_UK1", columnNames = {
				"clientid", "code"}),
		@UniqueConstraint(name = RoleSupp.TABLE_NAME + "_UK2", columnNames = {
				"clientid", "name"})})
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class RoleSupp extends AbstractTypeWithCode implements Serializable {

	public static final String TABLE_NAME = "AD_ROLE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "RoleSupp.findByCode";
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "RoleSupp.findByName";

	@ManyToMany(mappedBy = "roles")
	private Collection<UserSupp> users;

	public Collection<UserSupp> getUsers() {
		return this.users;
	}

	public void setUsers(Collection<UserSupp> users) {
		this.users = users;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
