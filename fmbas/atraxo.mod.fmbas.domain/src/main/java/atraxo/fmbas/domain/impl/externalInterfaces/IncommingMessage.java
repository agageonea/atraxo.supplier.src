/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.externalInterfaces;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.fmbas_type.IncomingMessageStatus;
import atraxo.fmbas.domain.impl.fmbas_type.IncomingMessageStatusConverter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link IncommingMessage} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = IncommingMessage.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM IncommingMessage e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = IncommingMessage.NQ_IDX_FIND_BY_STATUS_INTERACE_ID_CLIENTID_CREATED_AT, query = "SELECT e FROM IncommingMessage e WHERE e.clientId = :clientId and e.status = :status and e.externalInterface = :externalInterface and e.createdAt = :createdAt", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = IncommingMessage.NQ_IDX_FIND_BY_STATUS_INTERACE_ID_CLIENTID_CREATED_AT_PRIMITIVE, query = "SELECT e FROM IncommingMessage e WHERE e.clientId = :clientId and e.status = :status and e.externalInterface.id = :externalInterfaceId and e.createdAt = :createdAt", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = IncommingMessage.TABLE_NAME)
public class IncommingMessage extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_INCOMING_MESSAGES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "IncommingMessage.findByBusiness";
	/**
	 * Named query find by index key: Status_interace_id_clientid_created_at.
	 */
	public static final String NQ_IDX_FIND_BY_STATUS_INTERACE_ID_CLIENTID_CREATED_AT = "IncommingMessage.findByIdxStatus_interace_id_clientid_created_at";
	/**
	 * Named query find by index key: Status_interace_id_clientid_created_at using the index field for references.
	 */
	public static final String NQ_IDX_FIND_BY_STATUS_INTERACE_ID_CLIENTID_CREATED_AT_PRIMITIVE = "IncommingMessage.findByIdxStatus_interace_id_clientid_created_at_PRIMITIVE";

	@NotBlank
	@Column(name = "message_id", nullable = false, length = 100)
	private String messageId;

	@Column(name = "message", length = 4000)
	private String message;

	@Column(name = "status", length = 50)
	@Convert(converter = IncomingMessageStatusConverter.class)
	private IncomingMessageStatus status;

	@NotBlank
	@Column(name = "bean_name", nullable = false, length = 25)
	private String beanName;

	@NotBlank
	@Column(name = "method_name", nullable = false, length = 50)
	private String methodName;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ExternalInterface.class)
	@JoinColumn(name = "interface_id", referencedColumnName = "id")
	private ExternalInterface externalInterface;

	public String getMessageId() {
		return this.messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public IncomingMessageStatus getStatus() {
		return this.status;
	}

	public void setStatus(IncomingMessageStatus status) {
		this.status = status;
	}

	public String getBeanName() {
		return this.beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	public String getMethodName() {
		return this.methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public ExternalInterface getExternalInterface() {
		return this.externalInterface;
	}

	public void setExternalInterface(ExternalInterface externalInterface) {
		if (externalInterface != null) {
			this.__validate_client_context__(externalInterface.getClientId());
		}
		this.externalInterface = externalInterface;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
