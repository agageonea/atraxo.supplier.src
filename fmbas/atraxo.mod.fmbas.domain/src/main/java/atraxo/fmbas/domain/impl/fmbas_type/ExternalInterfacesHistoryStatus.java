/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ExternalInterfacesHistoryStatus {

	_EMPTY_(""), _SUCCESS_("Success"), _FAILURE_("Failure"), _FAILED_TIMEOUT_(
			"Failed-Timeout"), _FAILED_COMMUNICATION_("Failed-Communication"), _FAILED_NEGATIVE_ACKNOWLEDGEMENT_(
			"Failed-Negative Acknowledgement");

	private String name;

	private ExternalInterfacesHistoryStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ExternalInterfacesHistoryStatus getByName(String name) {
		for (ExternalInterfacesHistoryStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent ExternalInterfacesHistoryStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
