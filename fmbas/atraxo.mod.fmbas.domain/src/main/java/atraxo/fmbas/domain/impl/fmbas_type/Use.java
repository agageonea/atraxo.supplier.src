/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum Use {

	_EMPTY_("", 0), _TAX_("Tax", 3), _PRODUCT_("Product", 1), _FEE_("Fee", 2);

	private String name;
	private int order;

	private Use(String name, int order) {
		this.name = name;
		this.order = order;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static Use getByName(String name) {
		for (Use status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Use with name: " + name);
	}

	public int getOrder() {
		return this.order;
	}

	public static Use getByOrder(int code) {
		for (Use status : values()) {
			if (status.getOrder() == code) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Use with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
