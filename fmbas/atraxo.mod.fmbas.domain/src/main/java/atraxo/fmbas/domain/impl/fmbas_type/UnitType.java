/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum UnitType {

	_EMPTY_(""), _MASS_("Mass"), _VOLUME_("Volume"), _PERCENT_("Percent"), _EVENT_(
			"Event"), _TEMPERATURE_("Temperature"), _DURATION_("Duration"), _LENGTH_(
			"Length");

	private String name;

	private UnitType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static UnitType getByName(String name) {
		for (UnitType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent UnitType with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
