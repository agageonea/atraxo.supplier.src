/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TolLimits {

	_EMPTY_(""), _NARROWER_("Narrower"), _WIDER_("Wider");

	private String name;

	private TolLimits(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TolLimits getByName(String name) {
		for (TolLimits status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TolLimits with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
