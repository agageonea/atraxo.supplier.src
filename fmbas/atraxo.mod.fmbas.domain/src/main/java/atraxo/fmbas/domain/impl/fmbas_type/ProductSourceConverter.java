/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ProductSource} enum.
 * Generated code. Do not modify in this file.
 */
public class ProductSourceConverter
		implements
			AttributeConverter<ProductSource, String> {

	@Override
	public String convertToDatabaseColumn(ProductSource value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ProductSource.");
		}
		return value.getName();
	}

	@Override
	public ProductSource convertToEntityAttribute(String value) {
		return ProductSource.getByName(value);
	}

}
