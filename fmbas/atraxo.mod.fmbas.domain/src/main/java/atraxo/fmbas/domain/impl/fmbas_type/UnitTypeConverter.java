/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link UnitType} enum.
 * Generated code. Do not modify in this file.
 */
public class UnitTypeConverter implements AttributeConverter<UnitType, String> {

	@Override
	public String convertToDatabaseColumn(UnitType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null UnitType.");
		}
		return value.getName();
	}

	@Override
	public UnitType convertToEntityAttribute(String value) {
		return UnitType.getByName(value);
	}

}
