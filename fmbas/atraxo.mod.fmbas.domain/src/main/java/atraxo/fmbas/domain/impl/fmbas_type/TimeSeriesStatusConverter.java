/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TimeSeriesStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class TimeSeriesStatusConverter
		implements
			AttributeConverter<TimeSeriesStatus, String> {

	@Override
	public String convertToDatabaseColumn(TimeSeriesStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TimeSeriesStatus.");
		}
		return value.getName();
	}

	@Override
	public TimeSeriesStatus convertToEntityAttribute(String value) {
		return TimeSeriesStatus.getByName(value);
	}

}
