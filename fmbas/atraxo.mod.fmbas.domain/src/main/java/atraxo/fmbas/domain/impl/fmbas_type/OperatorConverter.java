/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link Operator} enum.
 * Generated code. Do not modify in this file.
 */
public class OperatorConverter implements AttributeConverter<Operator, String> {

	@Override
	public String convertToDatabaseColumn(Operator value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null Operator.");
		}
		return value.getName();
	}

	@Override
	public Operator convertToEntityAttribute(String value) {
		return Operator.getByName(value);
	}

}
