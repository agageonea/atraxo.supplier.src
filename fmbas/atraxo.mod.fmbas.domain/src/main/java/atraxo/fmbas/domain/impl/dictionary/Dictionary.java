/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.dictionary;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.dictionary.Condition;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Dictionary} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = Dictionary.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Dictionary e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Dictionary.TABLE_NAME)
public class Dictionary extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_DICTIONARY";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Dictionary.findByBusiness";

	@NotBlank
	@Column(name = "field_name", nullable = false, length = 250)
	private String fieldName;

	@NotBlank
	@Column(name = "original_value", nullable = false, length = 100)
	private String originalValue;

	@NotBlank
	@Column(name = "new_value", nullable = false, length = 100)
	private String newValue;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ExternalInterface.class)
	@JoinColumn(name = "external_interface_id", referencedColumnName = "id")
	private ExternalInterface externalInterface;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = Condition.class, mappedBy = "dictionary", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<Condition> conditions;

	public String getFieldName() {
		return this.fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getOriginalValue() {
		return this.originalValue;
	}

	public void setOriginalValue(String originalValue) {
		this.originalValue = originalValue;
	}

	public String getNewValue() {
		return this.newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	public ExternalInterface getExternalInterface() {
		return this.externalInterface;
	}

	public void setExternalInterface(ExternalInterface externalInterface) {
		if (externalInterface != null) {
			this.__validate_client_context__(externalInterface.getClientId());
		}
		this.externalInterface = externalInterface;
	}

	public Collection<Condition> getConditions() {
		return this.conditions;
	}

	public void setConditions(Collection<Condition> conditions) {
		this.conditions = conditions;
	}

	/**
	 * @param e
	 */
	public void addToConditions(Condition e) {
		if (this.conditions == null) {
			this.conditions = new ArrayList<>();
		}
		e.setDictionary(this);
		this.conditions.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
