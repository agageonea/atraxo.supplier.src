/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link WorkflowParamType} enum.
 * Generated code. Do not modify in this file.
 */
public class WorkflowParamTypeConverter
		implements
			AttributeConverter<WorkflowParamType, String> {

	@Override
	public String convertToDatabaseColumn(WorkflowParamType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null WorkflowParamType.");
		}
		return value.getName();
	}

	@Override
	public WorkflowParamType convertToEntityAttribute(String value) {
		return WorkflowParamType.getByName(value);
	}

}
