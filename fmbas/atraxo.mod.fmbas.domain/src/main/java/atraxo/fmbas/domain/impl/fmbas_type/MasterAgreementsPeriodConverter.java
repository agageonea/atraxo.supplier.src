/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link MasterAgreementsPeriod} enum.
 * Generated code. Do not modify in this file.
 */
public class MasterAgreementsPeriodConverter
		implements
			AttributeConverter<MasterAgreementsPeriod, String> {

	@Override
	public String convertToDatabaseColumn(MasterAgreementsPeriod value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null MasterAgreementsPeriod.");
		}
		return value.getName();
	}

	@Override
	public MasterAgreementsPeriod convertToEntityAttribute(String value) {
		return MasterAgreementsPeriod.getByName(value);
	}

}
