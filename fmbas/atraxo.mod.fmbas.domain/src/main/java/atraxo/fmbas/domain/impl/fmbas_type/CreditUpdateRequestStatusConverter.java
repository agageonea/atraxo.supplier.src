/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link CreditUpdateRequestStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class CreditUpdateRequestStatusConverter
		implements
			AttributeConverter<CreditUpdateRequestStatus, String> {

	@Override
	public String convertToDatabaseColumn(CreditUpdateRequestStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null CreditUpdateRequestStatus.");
		}
		return value.getName();
	}

	@Override
	public CreditUpdateRequestStatus convertToEntityAttribute(String value) {
		return CreditUpdateRequestStatus.getByName(value);
	}

}
