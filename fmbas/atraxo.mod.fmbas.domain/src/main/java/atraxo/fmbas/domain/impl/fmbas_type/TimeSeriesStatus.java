/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TimeSeriesStatus {

	_EMPTY_(""), _WORK_("work"), _PUBLISHED_("published");

	private String name;

	private TimeSeriesStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TimeSeriesStatus getByName(String name) {
		for (TimeSeriesStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent TimeSeriesStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
