/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.workflow;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link WorkflowInstanceEntity} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = WorkflowInstanceEntity.NQ_FIND_BY_BUSINESSKEY, query = "SELECT e FROM WorkflowInstanceEntity e WHERE e.clientId = :clientId and e.name = :name and e.objectId = :objectId and e.objectType = :objectType", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = WorkflowInstanceEntity.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM WorkflowInstanceEntity e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = WorkflowInstanceEntity.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = WorkflowInstanceEntity.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "name", "object_id", "object_type"})})
public class WorkflowInstanceEntity extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "BAS_WORKFLOW_INSTANCE_ENTITIES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: BusinessKey.
	 */
	public static final String NQ_FIND_BY_BUSINESSKEY = "WorkflowInstanceEntity.findByBusinessKey";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "WorkflowInstanceEntity.findByBusiness";

	@NotBlank
	@Column(name = "name", nullable = false, length = 255)
	private String name;

	@NotNull
	@Column(name = "object_id", nullable = false, precision = 11)
	private Integer objectId;

	@NotBlank
	@Column(name = "object_type", nullable = false, length = 32)
	private String objectType;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = WorkflowInstance.class)
	@JoinColumn(name = "workflow_instance_id", referencedColumnName = "id")
	private WorkflowInstance workflowInstance;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getObjectId() {
		return this.objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public WorkflowInstance getWorkflowInstance() {
		return this.workflowInstance;
	}

	public void setWorkflowInstance(WorkflowInstance workflowInstance) {
		if (workflowInstance != null) {
			this.__validate_client_context__(workflowInstance.getClientId());
		}
		this.workflowInstance = workflowInstance;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.name == null) ? 0 : this.name.hashCode());
		result = prime * result
				+ ((this.objectId == null) ? 0 : this.objectId.hashCode());
		result = prime * result
				+ ((this.objectType == null) ? 0 : this.objectType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		WorkflowInstanceEntity other = (WorkflowInstanceEntity) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		if (this.objectId == null) {
			if (other.objectId != null) {
				return false;
			}
		} else if (!this.objectId.equals(other.objectId)) {
			return false;
		}
		if (this.objectType == null) {
			if (other.objectType != null) {
				return false;
			}
		} else if (!this.objectType.equals(other.objectType)) {
			return false;
		}
		return true;
	}
}
