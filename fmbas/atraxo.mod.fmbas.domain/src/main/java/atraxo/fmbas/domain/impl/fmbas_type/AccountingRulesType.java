/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum AccountingRulesType {

	_EMPTY_(""), _EXPORT_DETAILED_COMPOSITE_PRICE_(
			"Export detailed composite price"), _REPLACE_ON_INVOICE_EXPORT_THE_INVOICE_RECEIVER_WITH_SHIP_TO_(
			"Replace on invoice export the invoice receiver with ship-to");

	private String name;

	private AccountingRulesType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static AccountingRulesType getByName(String name) {
		for (AccountingRulesType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent AccountingRulesType with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
