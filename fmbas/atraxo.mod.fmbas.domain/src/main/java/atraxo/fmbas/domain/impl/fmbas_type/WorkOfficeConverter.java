/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link WorkOffice} enum.
 * Generated code. Do not modify in this file.
 */
public class WorkOfficeConverter
		implements
			AttributeConverter<WorkOffice, String> {

	@Override
	public String convertToDatabaseColumn(WorkOffice value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null WorkOffice.");
		}
		return value.getName();
	}

	@Override
	public WorkOffice convertToEntityAttribute(String value) {
		return WorkOffice.getByName(value);
	}

}
