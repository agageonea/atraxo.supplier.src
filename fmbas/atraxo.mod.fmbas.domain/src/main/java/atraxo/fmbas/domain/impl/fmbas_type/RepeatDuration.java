/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum RepeatDuration {

	_EMPTY_("", 0), _INDEFINITELY_("Indefinitely", 0), _15_MINUTES_(
			"15 minutes", 15), _30_MINUTES_("30 minutes", 30), _1_HOUR_(
			"1 hour", 60), _12_HOURS_("12 hours", 12 * 60), _1_DAY_("1 day",
			24 * 60);

	private String name;
	private int duration;

	private RepeatDuration(String name, int duration) {
		this.name = name;
		this.duration = duration;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static RepeatDuration getByName(String name) {
		for (RepeatDuration status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent RepeatDuration with name: "
				+ name);
	}

	public int getDuration() {
		return this.duration;
	}

	public static RepeatDuration getByDuration(int code) {
		for (RepeatDuration status : values()) {
			if (status.getDuration() == code) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent RepeatDuration with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
