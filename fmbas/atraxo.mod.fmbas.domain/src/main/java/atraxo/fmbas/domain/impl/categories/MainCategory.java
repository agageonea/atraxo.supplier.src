/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.categories;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link MainCategory} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = MainCategory.NQ_FIND_BY_CODE, query = "SELECT e FROM MainCategory e WHERE e.clientId = :clientId and e.code = :code", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = MainCategory.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM MainCategory e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = MainCategory.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = MainCategory.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "code"})})
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class MainCategory extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_MAIN_CATEGORIES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "MainCategory.findByCode";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "MainCategory.findByBusiness";

	@NotBlank
	@Column(name = "code", nullable = false, length = 25)
	private String code;

	@Column(name = "name", length = 100)
	private String name;

	@NotNull
	@Column(name = "is_prod", nullable = false)
	private Boolean isProd;

	@NotNull
	@Column(name = "is_sys", nullable = false)
	private Boolean isSys;

	@Column(name = "active")
	private Boolean active;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsProd() {
		return this.isProd;
	}

	public void setIsProd(Boolean isProd) {
		this.isProd = isProd;
	}

	public Boolean getIsSys() {
		return this.isSys;
	}

	public void setIsSys(Boolean isSys) {
		this.isSys = isSys;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.isProd == null) {
			this.isProd = new Boolean(true);
		}
		if (this.isSys == null) {
			this.isSys = new Boolean(true);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
