package atraxo.fmbas.domain.ext.categories;

public enum PriceCategoryPricePer {
	VOLUME("Volume"), EVENT("Event"), PERCENTAGE("Percentage"), PERIOD("Period");

	private String displayName;

	private PriceCategoryPricePer(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public PriceCategoryPricePer getByDisplayName(String displayName) {
		for (PriceCategoryPricePer pricePer : values()) {
			if (pricePer.getDisplayName().equalsIgnoreCase(displayName)) {
				return pricePer;
			}
		}
		throw new RuntimeException("Inexistent price per with name: " + displayName);
	}

}
