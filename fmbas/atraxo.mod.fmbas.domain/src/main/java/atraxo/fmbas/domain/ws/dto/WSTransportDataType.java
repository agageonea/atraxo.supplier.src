package atraxo.fmbas.domain.ws.dto;

/**
 * @author vhojda
 */
public enum WSTransportDataType {

	CONTRACTS, CUSTOMERS, FUEL_TRANSACTIONS, FUEL_EVENT, SALES_INVOICES, UNKNOWN;
}
