/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.profile;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.AccountingRulesType;
import atraxo.fmbas.domain.impl.fmbas_type.AccountingRulesTypeConverter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link AccountingRules} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = AccountingRules.NQ_FIND_BY_ACCOUNTINGRULES, query = "SELECT e FROM AccountingRules e WHERE e.clientId = :clientId and e.ruleType = :ruleType and e.subsidiary = :subsidiary", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = AccountingRules.NQ_FIND_BY_ACCOUNTINGRULES_PRIMITIVE, query = "SELECT e FROM AccountingRules e WHERE e.clientId = :clientId and e.ruleType = :ruleType and e.subsidiary.id = :subsidiaryId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = AccountingRules.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM AccountingRules e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = AccountingRules.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = AccountingRules.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "rule_type", "subsidiary_id"})})
public class AccountingRules extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_ACCOUNTING_RULES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: AccountingRules.
	 */
	public static final String NQ_FIND_BY_ACCOUNTINGRULES = "AccountingRules.findByAccountingRules";
	/**
	 * Named query find by unique key: AccountingRules using the ID field for references.
	 */
	public static final String NQ_FIND_BY_ACCOUNTINGRULES_PRIMITIVE = "AccountingRules.findByAccountingRules_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "AccountingRules.findByBusiness";

	@Column(name = "rule_type", length = 100)
	@Convert(converter = AccountingRulesTypeConverter.class)
	private AccountingRulesType ruleType;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "subsidiary_id", referencedColumnName = "id")
	private Customer subsidiary;

	public AccountingRulesType getRuleType() {
		return this.ruleType;
	}

	public void setRuleType(AccountingRulesType ruleType) {
		this.ruleType = ruleType;
	}

	public Customer getSubsidiary() {
		return this.subsidiary;
	}

	public void setSubsidiary(Customer subsidiary) {
		if (subsidiary != null) {
			this.__validate_client_context__(subsidiary.getClientId());
		}
		this.subsidiary = subsidiary;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.ruleType == null) ? 0 : this.ruleType.hashCode());
		result = prime * result
				+ ((this.subsidiary == null) ? 0 : this.subsidiary.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		AccountingRules other = (AccountingRules) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.ruleType == null) {
			if (other.ruleType != null) {
				return false;
			}
		} else if (!this.ruleType.equals(other.ruleType)) {
			return false;
		}
		if (this.subsidiary == null) {
			if (other.subsidiary != null) {
				return false;
			}
		} else if (!this.subsidiary.equals(other.subsidiary)) {
			return false;
		}
		return true;
	}
}
