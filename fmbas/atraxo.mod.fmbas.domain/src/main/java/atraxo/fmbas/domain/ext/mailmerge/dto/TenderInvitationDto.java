package atraxo.fmbas.domain.ext.mailmerge.dto;

/**
 * DTO Class for new tender invitation mail sending process
 *
 * @author aradu
 */
public class TenderInvitationDto extends AbstractDto {

	private static final long serialVersionUID = -4292219455641509058L;

	private String tenderIssuer;
	private String tenderCode;
	private String tenderName;
	private String tenderVersion;
	private String contactPerson;
	private String contactEmail;
	private String biddingFrom;
	private String biddingTo;

	private String title;
	private String fullName;
	private String firstName;
	private String lastName;
	private String email;
	private SubsidiaryDto subsidiary;
	private CustomerDto customer;
	private WebServiceDto webService;

	public String getTenderIssuer() {
		return this.tenderIssuer;
	}

	public void setTenderIssuer(String tenderIssuer) {
		this.tenderIssuer = tenderIssuer;
	}

	public String getTenderCode() {
		return this.tenderCode;
	}

	public void setTenderCode(String tenderCode) {
		this.tenderCode = tenderCode;
	}

	public String getTenderName() {
		return this.tenderName;
	}

	public void setTenderName(String tenderName) {
		this.tenderName = tenderName;
	}

	public String getTenderVersion() {
		return this.tenderVersion;
	}

	public void setTenderVersion(String tenderVersion) {
		this.tenderVersion = tenderVersion;
	}

	public String getContactPerson() {
		return this.contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactEmail() {
		return this.contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getBiddingFrom() {
		return this.biddingFrom;
	}

	public void setBiddingFrom(String biddingFrom) {
		this.biddingFrom = biddingFrom;
	}

	public String getBiddingTo() {
		return this.biddingTo;
	}

	public void setBiddingTo(String biddingTo) {
		this.biddingTo = biddingTo;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public SubsidiaryDto getSubsidiary() {
		return this.subsidiary;
	}

	public void setSubsidiary(SubsidiaryDto subsidiary) {
		this.subsidiary = subsidiary;
	}

	public CustomerDto getCustomer() {
		return this.customer;
	}

	public void setCustomer(CustomerDto customer) {
		this.customer = customer;
	}

	public WebServiceDto getWebService() {
		return this.webService;
	}

	public void setWebService(WebServiceDto webService) {
		this.webService = webService;
	}

	@Override
	public String toString() {
		return "TenderInvitationDto [tenderIssuer=" + this.tenderIssuer + ", tenderCode=" + this.tenderCode + ", tenderName=" + this.tenderName
				+ ", tenderVersion=" + this.tenderVersion + ", contactPerson=" + this.contactPerson + ", contactEmail=" + this.contactEmail
				+ ", biddingFrom=" + this.biddingFrom + ", biddingTo=" + this.biddingTo + ", title=" + this.title + ", fullName=" + this.fullName
				+ ", firstName=" + this.firstName + ", lastName=" + this.lastName + ", email=" + this.email + ", subsidiary=" + this.subsidiary
				+ ", customer=" + this.customer + ", webService=" + this.webService + "]";
	}

}
