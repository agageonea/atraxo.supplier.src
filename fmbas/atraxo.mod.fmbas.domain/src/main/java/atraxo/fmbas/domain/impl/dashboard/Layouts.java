/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.dashboard;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Layouts} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Layouts.NQ_FIND_BY_NAME, query = "SELECT e FROM Layouts e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Layouts.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Layouts e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Layouts.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Layouts.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "name"})})
public class Layouts extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_LAYOUTS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "Layouts.findByName";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Layouts.findByBusiness";

	@NotBlank
	@Column(name = "name", nullable = false, length = 100)
	private String name;

	@NotBlank
	@Column(name = "description", nullable = false, length = 1000)
	private String description;

	@NotBlank
	@Column(name = "thumb_path", nullable = false, length = 400)
	private String thumbPath;

	@NotBlank
	@Column(name = "cfg_path", nullable = false, length = 400)
	private String cfgPath;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = DashboardWidgets.class, mappedBy = "layout")
	private Collection<DashboardWidgets> dashboardWidgets;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getThumbPath() {
		return this.thumbPath;
	}

	public void setThumbPath(String thumbPath) {
		this.thumbPath = thumbPath;
	}

	public String getCfgPath() {
		return this.cfgPath;
	}

	public void setCfgPath(String cfgPath) {
		this.cfgPath = cfgPath;
	}

	public Collection<DashboardWidgets> getDashboardWidgets() {
		return this.dashboardWidgets;
	}

	public void setDashboardWidgets(
			Collection<DashboardWidgets> dashboardWidgets) {
		this.dashboardWidgets = dashboardWidgets;
	}

	/**
	 * @param e
	 */
	public void addToDashboardWidgets(DashboardWidgets e) {
		if (this.dashboardWidgets == null) {
			this.dashboardWidgets = new ArrayList<>();
		}
		e.setLayout(this);
		this.dashboardWidgets.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
