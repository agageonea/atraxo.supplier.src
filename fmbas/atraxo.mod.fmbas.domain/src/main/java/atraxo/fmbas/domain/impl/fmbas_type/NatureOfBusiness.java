/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum NatureOfBusiness {

	_EMPTY_(""), _AIRLINE_("Airline"), _RESELLER_("Reseller"), _GENERAL_AVIATION_(
			"General aviation"), _GOVERNMENT_("Government"), _MILITARY_(
			"Military"), _OTHERS_("Others");

	private String name;

	private NatureOfBusiness(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static NatureOfBusiness getByName(String name) {
		for (NatureOfBusiness status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent NatureOfBusiness with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
