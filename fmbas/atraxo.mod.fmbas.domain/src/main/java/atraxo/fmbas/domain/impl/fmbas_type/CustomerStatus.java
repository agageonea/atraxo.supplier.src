/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum CustomerStatus {

	_EMPTY_(""), _ACTIVE_("Active"), _INACTIVE_("Inactive"), _PROSPECT_(
			"Prospect"), _BLOCKED_("Blocked");

	private String name;

	private CustomerStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static CustomerStatus getByName(String name) {
		for (CustomerStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent CustomerStatus with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
