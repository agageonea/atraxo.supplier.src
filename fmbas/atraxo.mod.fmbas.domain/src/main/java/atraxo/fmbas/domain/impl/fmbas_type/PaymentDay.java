/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum PaymentDay {

	_EMPTY_("", ""), _INVOICE_DATE_("Invoice date", "ID"), _INVOICE_RECEIVING_DATE_(
			"Invoice receiving date", "IR"), _LAST_DELIVERY_DATE_(
			"Last delivery date", "LD"), _FIRST_DELIVERY_DATE_(
			"First Delivery Date", "FD"), _MIDPOINT_DELIVERY_DATE_(
			"Midpoint Delivery Date", "MD");

	private String name;
	private String code;

	private PaymentDay(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static PaymentDay getByName(String name) {
		for (PaymentDay status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent PaymentDay with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static PaymentDay getByCode(String code) {
		for (PaymentDay status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent PaymentDay with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
