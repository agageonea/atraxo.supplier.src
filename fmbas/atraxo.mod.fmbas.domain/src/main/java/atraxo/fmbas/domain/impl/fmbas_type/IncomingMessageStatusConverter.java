/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link IncomingMessageStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class IncomingMessageStatusConverter
		implements
			AttributeConverter<IncomingMessageStatus, String> {

	@Override
	public String convertToDatabaseColumn(IncomingMessageStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null IncomingMessageStatus.");
		}
		return value.getName();
	}

	@Override
	public IncomingMessageStatus convertToEntityAttribute(String value) {
		return IncomingMessageStatus.getByName(value);
	}

}
