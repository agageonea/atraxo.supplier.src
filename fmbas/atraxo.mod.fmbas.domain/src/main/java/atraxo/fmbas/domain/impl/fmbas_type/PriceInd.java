/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum PriceInd {

	_EMPTY_("", ""), _VOLUME_("Volume", "CHECK_CONTRACT_PRICE_VOLUME"), _EVENT_(
			"Event", "CHECK_CONTRACT_PRICE_EVENT"), _COMPOSITE_("Composite", ""), _PERCENT_(
			"Percent", "CHECK_CONTRACT_PRICE_PERCENTAGE");

	private String name;
	private String toleranceName;

	private PriceInd(String name, String toleranceName) {
		this.name = name;
		this.toleranceName = toleranceName;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static PriceInd getByName(String name) {
		for (PriceInd status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent PriceInd with name: " + name);
	}

	public String getToleranceName() {
		return this.toleranceName;
	}

	public static PriceInd getByToleranceName(String code) {
		for (PriceInd status : values()) {
			if (status.getToleranceName().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent PriceInd with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
