/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.tolerance;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.fmbas_type.ParamType;
import atraxo.fmbas.domain.impl.fmbas_type.ParamTypeConverter;
import atraxo.fmbas.domain.impl.fmbas_type.TolLimits;
import atraxo.fmbas.domain.impl.fmbas_type.TolLimitsConverter;
import atraxo.fmbas.domain.impl.fmbas_type.ToleranceType;
import atraxo.fmbas.domain.impl.fmbas_type.ToleranceTypeConverter;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Tolerance} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Tolerance.NQ_FIND_BY_NAME, query = "SELECT e FROM Tolerance e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Tolerance.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Tolerance e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Tolerance.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Tolerance.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "name"})})
public class Tolerance extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_TOLERANCES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "Tolerance.findByName";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Tolerance.findByBusiness";

	@NotBlank
	@Column(name = "name", nullable = false, length = 64)
	private String name;

	@Column(name = "description", length = 255)
	private String description;

	@NotBlank
	@Column(name = "parameter_type", nullable = false, length = 32)
	@Convert(converter = ParamTypeConverter.class)
	private ParamType parameterType;

	@NotBlank
	@Column(name = "tolerance_for", nullable = false, length = 32)
	private String toleranceFor;

	@Column(name = "abs_dev_down", precision = 19, scale = 6)
	private BigDecimal absDevDown;

	@Column(name = "abs_dev_up", precision = 19, scale = 6)
	private BigDecimal absDevUp;

	@Column(name = "relative_dev_down", precision = 19, scale = 6)
	private BigDecimal relativeDevDown;

	@Column(name = "relative_dev_up", precision = 19, scale = 6)
	private BigDecimal relativeDevUp;

	@NotBlank
	@Column(name = "tolerance_type", nullable = false, length = 32)
	@Convert(converter = ToleranceTypeConverter.class)
	private ToleranceType toleranceType;

	@Column(name = "system")
	private Boolean system;

	@Column(name = "tol_lmt_ind", length = 32)
	@Convert(converter = TolLimitsConverter.class)
	private TolLimits tolLmtInd;

	@Column(name = "active")
	private Boolean active;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_id", referencedColumnName = "id")
	private Unit refUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "currency_id", referencedColumnName = "id")
	private Currencies currency;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ParamType getParameterType() {
		return this.parameterType;
	}

	public void setParameterType(ParamType parameterType) {
		this.parameterType = parameterType;
	}

	public String getToleranceFor() {
		return this.toleranceFor;
	}

	public void setToleranceFor(String toleranceFor) {
		this.toleranceFor = toleranceFor;
	}

	public BigDecimal getAbsDevDown() {
		return this.absDevDown;
	}

	public void setAbsDevDown(BigDecimal absDevDown) {
		this.absDevDown = absDevDown;
	}

	public BigDecimal getAbsDevUp() {
		return this.absDevUp;
	}

	public void setAbsDevUp(BigDecimal absDevUp) {
		this.absDevUp = absDevUp;
	}

	public BigDecimal getRelativeDevDown() {
		return this.relativeDevDown;
	}

	public void setRelativeDevDown(BigDecimal relativeDevDown) {
		this.relativeDevDown = relativeDevDown;
	}

	public BigDecimal getRelativeDevUp() {
		return this.relativeDevUp;
	}

	public void setRelativeDevUp(BigDecimal relativeDevUp) {
		this.relativeDevUp = relativeDevUp;
	}

	public ToleranceType getToleranceType() {
		return this.toleranceType;
	}

	public void setToleranceType(ToleranceType toleranceType) {
		this.toleranceType = toleranceType;
	}

	public Boolean getSystem() {
		return this.system;
	}

	public void setSystem(Boolean system) {
		this.system = system;
	}

	public TolLimits getTolLmtInd() {
		return this.tolLmtInd;
	}

	public void setTolLmtInd(TolLimits tolLmtInd) {
		this.tolLmtInd = tolLmtInd;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Unit getRefUnit() {
		return this.refUnit;
	}

	public void setRefUnit(Unit refUnit) {
		if (refUnit != null) {
			this.__validate_client_context__(refUnit.getClientId());
		}
		this.refUnit = refUnit;
	}
	public Currencies getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currencies currency) {
		if (currency != null) {
			this.__validate_client_context__(currency.getClientId());
		}
		this.currency = currency;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
