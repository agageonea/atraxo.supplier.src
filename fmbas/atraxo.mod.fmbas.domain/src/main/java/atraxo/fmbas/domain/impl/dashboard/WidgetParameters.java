/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.dashboard;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.Widgets;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamType;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamTypeConverter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link WidgetParameters} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = WidgetParameters.NQ_FIND_BY_NAME, query = "SELECT e FROM WidgetParameters e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = WidgetParameters.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM WidgetParameters e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = WidgetParameters.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = WidgetParameters.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "name"})})
public class WidgetParameters extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_WIDGET_PARAMETERS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "WidgetParameters.findByName";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "WidgetParameters.findByBusiness";

	@NotBlank
	@Column(name = "name", nullable = false, length = 32)
	private String name;

	@NotBlank
	@Column(name = "type", nullable = false, length = 32)
	@Convert(converter = JobParamTypeConverter.class)
	private JobParamType type;

	@NotBlank
	@Column(name = "description", nullable = false, length = 1000)
	private String description;

	@Column(name = "value", length = 100)
	private String value;

	@Column(name = "default_value", length = 50)
	private String defaultValue;

	@Column(name = "read_only")
	private Boolean readOnly;

	@Column(name = "ref_values", length = 255)
	private String refValues;

	@Column(name = "ref_business_values", length = 1000)
	private String refBusinessValues;

	@Lob
	@Column(name = "blob_value")
	private byte[] blobValue;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Widgets.class)
	@JoinColumn(name = "widget_id", referencedColumnName = "id")
	private Widgets widget;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = AssignedWidgetParameters.class, mappedBy = "widgetParameters")
	private Collection<AssignedWidgetParameters> assignedWidgetParameters;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public JobParamType getType() {
		return this.type;
	}

	public void setType(JobParamType type) {
		this.type = type;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Boolean getReadOnly() {
		return this.readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	public String getRefValues() {
		return this.refValues;
	}

	public void setRefValues(String refValues) {
		this.refValues = refValues;
	}

	public String getRefBusinessValues() {
		return this.refBusinessValues;
	}

	public void setRefBusinessValues(String refBusinessValues) {
		this.refBusinessValues = refBusinessValues;
	}

	public byte[] getBlobValue() {
		return this.blobValue;
	}

	public void setBlobValue(byte[] blobValue) {
		this.blobValue = blobValue;
	}

	public Widgets getWidget() {
		return this.widget;
	}

	public void setWidget(Widgets widget) {
		if (widget != null) {
			this.__validate_client_context__(widget.getClientId());
		}
		this.widget = widget;
	}

	public Collection<AssignedWidgetParameters> getAssignedWidgetParameters() {
		return this.assignedWidgetParameters;
	}

	public void setAssignedWidgetParameters(
			Collection<AssignedWidgetParameters> assignedWidgetParameters) {
		this.assignedWidgetParameters = assignedWidgetParameters;
	}

	/**
	 * @param e
	 */
	public void addToAssignedWidgetParameters(AssignedWidgetParameters e) {
		if (this.assignedWidgetParameters == null) {
			this.assignedWidgetParameters = new ArrayList<>();
		}
		e.setWidgetParameters(this);
		this.assignedWidgetParameters.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
