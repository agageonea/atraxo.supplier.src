/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link CustomerDataProcessedStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class CustomerDataProcessedStatusConverter
		implements
			AttributeConverter<CustomerDataProcessedStatus, String> {

	@Override
	public String convertToDatabaseColumn(CustomerDataProcessedStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null CustomerDataProcessedStatus.");
		}
		return value.getName();
	}

	@Override
	public CustomerDataProcessedStatus convertToEntityAttribute(String value) {
		return CustomerDataProcessedStatus.getByName(value);
	}

}
