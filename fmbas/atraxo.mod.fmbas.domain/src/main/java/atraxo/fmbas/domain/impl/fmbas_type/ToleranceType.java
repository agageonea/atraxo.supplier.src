/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ToleranceType {

	_EMPTY_(""), _QUANTITY_("Quantity"), _DIMENSIONLESS_NUMBER_(
			"Dimensionless number"), _PRICE_("Price"), _COST_("Cost"), _AMOUNT_(
			"Amount");

	private String name;

	private ToleranceType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ToleranceType getByName(String name) {
		for (ToleranceType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent ToleranceType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
