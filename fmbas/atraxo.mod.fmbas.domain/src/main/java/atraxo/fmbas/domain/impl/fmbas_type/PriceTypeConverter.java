/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link PriceType} enum.
 * Generated code. Do not modify in this file.
 */
public class PriceTypeConverter
		implements
			AttributeConverter<PriceType, String> {

	@Override
	public String convertToDatabaseColumn(PriceType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null PriceType.");
		}
		return value.getName();
	}

	@Override
	public PriceType convertToEntityAttribute(String value) {
		return PriceType.getByName(value);
	}

}
