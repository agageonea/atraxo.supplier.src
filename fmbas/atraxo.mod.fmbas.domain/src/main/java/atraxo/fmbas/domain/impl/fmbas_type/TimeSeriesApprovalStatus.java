/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TimeSeriesApprovalStatus {

	_EMPTY_(""), _NEW_("New"), _APPROVED_("Approved"), _REJECTED_("Rejected"), _AWAITING_APPROVAL_(
			"Awaiting approval");

	private String name;

	private TimeSeriesApprovalStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TimeSeriesApprovalStatus getByName(String name) {
		for (TimeSeriesApprovalStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent TimeSeriesApprovalStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
