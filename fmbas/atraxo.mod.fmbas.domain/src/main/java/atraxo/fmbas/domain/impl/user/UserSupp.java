/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.user;

import atraxo.abstracts.domain.impl.tenant.AbstractTypeWithCode;
import atraxo.ad.domain.impl.ad.LanguageCodes;
import atraxo.ad.domain.impl.ad.LanguageCodesConverter;
import atraxo.ad.domain.impl.ad.TNumberSeparator;
import atraxo.ad.domain.impl.ad.TNumberSeparatorConverter;
import atraxo.ad.domain.impl.system.DateFormat;
import atraxo.fmbas.domain.impl.fmbas_type.Department;
import atraxo.fmbas.domain.impl.fmbas_type.DepartmentConverter;
import atraxo.fmbas.domain.impl.fmbas_type.Title;
import atraxo.fmbas.domain.impl.fmbas_type.TitleConverter;
import atraxo.fmbas.domain.impl.fmbas_type.WorkOffice;
import atraxo.fmbas.domain.impl.fmbas_type.WorkOfficeConverter;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.notification.Notification;
import atraxo.fmbas.domain.impl.user.RoleSupp;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link UserSupp} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = UserSupp.NQ_FIND_BY_CODE, query = "SELECT e FROM UserSupp e WHERE e.clientId = :clientId and e.code = :code", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = UserSupp.NQ_FIND_BY_LOGIN, query = "SELECT e FROM UserSupp e WHERE e.clientId = :clientId and e.loginName = :loginName", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = UserSupp.TABLE_NAME, uniqueConstraints = {
		@UniqueConstraint(name = UserSupp.TABLE_NAME + "_UK1", columnNames = {
				"clientid", "code"}),
		@UniqueConstraint(name = UserSupp.TABLE_NAME + "_UK2", columnNames = {
				"clientid", "loginname"})})
public class UserSupp extends AbstractTypeWithCode implements Serializable {

	public static final String TABLE_NAME = "AD_USR";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "UserSupp.findByCode";
	/**
	 * Named query find by unique key: Login.
	 */
	public static final String NQ_FIND_BY_LOGIN = "UserSupp.findByLogin";

	@NotBlank
	@Column(name = "loginname", nullable = false, length = 255)
	private String loginName;

	@Column(name = "password", length = 255)
	private String password;

	@Column(name = "email", length = 128)
	private String email;

	@NotNull
	@Column(name = "locked", nullable = false)
	private Boolean locked;

	@Column(name = "decimalseparator", length = 1)
	@Convert(converter = TNumberSeparatorConverter.class)
	private TNumberSeparator decimalSeparator;

	@Column(name = "thousandseparator", length = 1)
	@Convert(converter = TNumberSeparatorConverter.class)
	private TNumberSeparator thousandSeparator;

	@Column(name = "title", length = 32)
	@Convert(converter = TitleConverter.class)
	private Title title;

	@Column(name = "first_name", length = 64)
	private String firstName;

	@Column(name = "last_name", length = 64)
	private String lastName;

	@Column(name = "job_title", length = 100)
	private String jobTitle;

	@Column(name = "department", length = 32)
	@Convert(converter = DepartmentConverter.class)
	private Department department;

	@Column(name = "work_office", length = 32)
	@Convert(converter = WorkOfficeConverter.class)
	private WorkOffice workOffice;

	@Column(name = "business_phone", length = 100)
	private String businessPhone;

	@Column(name = "mobile_phone", length = 100)
	private String mobilePhone;

	@Lob
	@Column(name = "picture")
	private byte[] picture;

	@NotBlank
	@Column(name = "language_code", nullable = false, length = 50)
	@Convert(converter = LanguageCodesConverter.class)
	private LanguageCodes userLanguage;

	@Transient
	private String emailSubject;

	@Transient
	private String emailTemplateRefId;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = DateFormat.class)
	@JoinColumn(name = "dateformat_id", referencedColumnName = "id")
	private DateFormat dateFormat;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "location_id", referencedColumnName = "id")
	private Locations location;

	@ManyToMany
	@JoinTable(name = "AD_USR_ROLE", joinColumns = {@JoinColumn(name = "users_id")}, inverseJoinColumns = {@JoinColumn(name = "roles_id")})
	private Collection<RoleSupp> roles;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = UserSubsidiary.class, mappedBy = "user")
	private Collection<UserSubsidiary> subsidiaries;

	@ManyToMany(mappedBy = "users")
	private Collection<Notification> notifications;

	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getLocked() {
		return this.locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public TNumberSeparator getDecimalSeparator() {
		return this.decimalSeparator;
	}

	public void setDecimalSeparator(TNumberSeparator decimalSeparator) {
		this.decimalSeparator = decimalSeparator;
	}

	public TNumberSeparator getThousandSeparator() {
		return this.thousandSeparator;
	}

	public void setThousandSeparator(TNumberSeparator thousandSeparator) {
		this.thousandSeparator = thousandSeparator;
	}

	public Title getTitle() {
		return this.title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getJobTitle() {
		return this.jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public Department getDepartment() {
		return this.department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public WorkOffice getWorkOffice() {
		return this.workOffice;
	}

	public void setWorkOffice(WorkOffice workOffice) {
		this.workOffice = workOffice;
	}

	public String getBusinessPhone() {
		return this.businessPhone;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	public String getMobilePhone() {
		return this.mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public byte[] getPicture() {
		return this.picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

	public LanguageCodes getUserLanguage() {
		return this.userLanguage;
	}

	public void setUserLanguage(LanguageCodes userLanguage) {
		this.userLanguage = userLanguage;
	}

	public String getEmailSubject() {
		return this.emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailTemplateRefId() {
		return this.emailTemplateRefId;
	}

	public void setEmailTemplateRefId(String emailTemplateRefId) {
		this.emailTemplateRefId = emailTemplateRefId;
	}

	public DateFormat getDateFormat() {
		return this.dateFormat;
	}

	public void setDateFormat(DateFormat dateFormat) {
		this.dateFormat = dateFormat;
	}
	public Locations getLocation() {
		return this.location;
	}

	public void setLocation(Locations location) {
		if (location != null) {
			this.__validate_client_context__(location.getClientId());
		}
		this.location = location;
	}

	public Collection<RoleSupp> getRoles() {
		return this.roles;
	}

	public void setRoles(Collection<RoleSupp> roles) {
		this.roles = roles;
	}

	public Collection<UserSubsidiary> getSubsidiaries() {
		return this.subsidiaries;
	}

	public void setSubsidiaries(Collection<UserSubsidiary> subsidiaries) {
		this.subsidiaries = subsidiaries;
	}

	/**
	 * @param e
	 */
	public void addToSubsidiaries(UserSubsidiary e) {
		if (this.subsidiaries == null) {
			this.subsidiaries = new ArrayList<>();
		}
		e.setUser(this);
		this.subsidiaries.add(e);
	}
	public Collection<Notification> getNotifications() {
		return this.notifications;
	}

	public void setNotifications(Collection<Notification> notifications) {
		this.notifications = notifications;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.locked == null) {
			this.locked = new Boolean(false);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
