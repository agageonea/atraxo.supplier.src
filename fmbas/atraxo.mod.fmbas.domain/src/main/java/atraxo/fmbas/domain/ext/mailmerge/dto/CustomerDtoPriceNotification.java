package atraxo.fmbas.domain.ext.mailmerge.dto;

import java.util.Date;

public class CustomerDtoPriceNotification extends CustomerDto {

	/**
	 *
	 */
	private static final long serialVersionUID = -2932897220575298584L;

	private Date customStartDate;
	private Date customEndDate;

	public Date getCustomStartDate() {
		return this.customStartDate;
	}

	public void setCustomStartDate(Date customStartDate) {
		this.customStartDate = customStartDate;
	}

	public Date getCustomEndDate() {
		return this.customEndDate;
	}

	public void setCustomEndDate(Date customEndDate) {
		this.customEndDate = customEndDate;
	}

}
