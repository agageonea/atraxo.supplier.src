/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum JobParamType {

	_EMPTY_(""), _STRING_("string"), _INTEGER_("integer"), _BOOLEAN_("boolean"), _DATE_(
			"date"), _ENUMERATION_("enumeration"), _LOV_("lov"), _MASKED_(
			"masked"), _ASSIGN_("assign"), _KEY_("key"), _TEXTAREA_("textarea");

	private String name;

	private JobParamType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static JobParamType getByName(String name) {
		for (JobParamType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent JobParamType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
