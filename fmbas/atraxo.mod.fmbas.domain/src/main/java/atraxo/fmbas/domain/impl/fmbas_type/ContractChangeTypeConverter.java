/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ContractChangeType} enum.
 * Generated code. Do not modify in this file.
 */
public class ContractChangeTypeConverter
		implements
			AttributeConverter<ContractChangeType, String> {

	@Override
	public String convertToDatabaseColumn(ContractChangeType value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null ContractChangeType.");
		}
		return value.getName();
	}

	@Override
	public ContractChangeType convertToEntityAttribute(String value) {
		return ContractChangeType.getByName(value);
	}

}
