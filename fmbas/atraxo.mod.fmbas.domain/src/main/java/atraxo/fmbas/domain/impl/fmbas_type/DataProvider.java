/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum DataProvider {

	_EMPTY_("", ""), _IMPORT_ARGUS_("IMPORT/ARGUS", "ARGUS"), _IMPORT_DDS_(
			"IMPORT/DDS", "DDS"), _IMPORT_OPIS_("IMPORT/OPIS", "OPIS"), _IMPORT_SHARE_(
			"IMPORT/SHARE", "SHARE"), _IMPORT_PLATTS_("IMPORT/PLATTS", "PLATTS"), _IMPORT_ECB_(
			"IMPORT/ECB", "ECB"), _IMPORT_BLOOMBERG_("IMPORT/BLOOMBERG",
			"BLOOMBERG"), _USER_MAINTAINED_("User maintained", ""), _CALCULATED_(
			"Calculated", ""), _OTHER_("Other", "OTHER");

	private String name;
	private String code;

	private DataProvider(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static DataProvider getByName(String name) {
		for (DataProvider status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent DataProvider with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static DataProvider getByCode(String code) {
		for (DataProvider status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent DataProvider with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
