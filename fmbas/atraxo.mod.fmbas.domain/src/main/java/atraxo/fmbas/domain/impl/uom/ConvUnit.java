/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.uom;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link ConvUnit} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = ConvUnit.NQ_FIND_BY_UNITSCONVFACT, query = "SELECT e FROM ConvUnit e WHERE e.clientId = :clientId and e.unitFromId = :unitFromId and e.unitToId = :unitToId and e.convType = :convType", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ConvUnit.NQ_FIND_BY_UNITSCONVFACT_PRIMITIVE, query = "SELECT e FROM ConvUnit e WHERE e.clientId = :clientId and e.unitFromId.id = :unitFromIdId and e.unitToId.id = :unitToIdId and e.convType = :convType", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ConvUnit.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ConvUnit e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ConvUnit.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = ConvUnit.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "unit_from_id", "unit_to_id",
		"conv_type"})})
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class ConvUnit extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_UNITS_CONV_FACTORS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: UnitsConvFact.
	 */
	public static final String NQ_FIND_BY_UNITSCONVFACT = "ConvUnit.findByUnitsConvFact";
	/**
	 * Named query find by unique key: UnitsConvFact using the ID field for references.
	 */
	public static final String NQ_FIND_BY_UNITSCONVFACT_PRIMITIVE = "ConvUnit.findByUnitsConvFact_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ConvUnit.findByBusiness";

	@Column(name = "conv_type", precision = 11)
	private Integer convType;

	@Column(name = "factor", precision = 19, scale = 6)
	private BigDecimal factor;

	@Column(name = "active")
	private Boolean active;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_from_id", referencedColumnName = "id")
	private Unit unitFromId;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_to_id", referencedColumnName = "id")
	private Unit unitToId;

	public Integer getConvType() {
		return this.convType;
	}

	public void setConvType(Integer convType) {
		this.convType = convType;
	}

	public BigDecimal getFactor() {
		return this.factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Unit getUnitFromId() {
		return this.unitFromId;
	}

	public void setUnitFromId(Unit unitFromId) {
		if (unitFromId != null) {
			this.__validate_client_context__(unitFromId.getClientId());
		}
		this.unitFromId = unitFromId;
	}
	public Unit getUnitToId() {
		return this.unitToId;
	}

	public void setUnitToId(Unit unitToId) {
		if (unitToId != null) {
			this.__validate_client_context__(unitToId.getClientId());
		}
		this.unitToId = unitToId;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
