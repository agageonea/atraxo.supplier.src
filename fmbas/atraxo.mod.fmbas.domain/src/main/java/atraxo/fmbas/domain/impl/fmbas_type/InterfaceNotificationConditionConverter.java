/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link InterfaceNotificationCondition} enum.
 * Generated code. Do not modify in this file.
 */
public class InterfaceNotificationConditionConverter
		implements
			AttributeConverter<InterfaceNotificationCondition, String> {

	@Override
	public String convertToDatabaseColumn(InterfaceNotificationCondition value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null InterfaceNotificationCondition.");
		}
		return value.getName();
	}

	@Override
	public InterfaceNotificationCondition convertToEntityAttribute(String value) {
		return InterfaceNotificationCondition.getByName(value);
	}

}
