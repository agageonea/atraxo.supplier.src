/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.exchangerate;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link ExchangeRateValue} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = ExchangeRateValue.NQ_FIND_BY_BUSINESSKEY, query = "SELECT e FROM ExchangeRateValue e WHERE e.clientId = :clientId and e.exchRate = :exchRate and e.vldFrDtRef = :vldFrDtRef and e.vldToDtRef = :vldToDtRef", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ExchangeRateValue.NQ_FIND_BY_BUSINESSKEY_PRIMITIVE, query = "SELECT e FROM ExchangeRateValue e WHERE e.clientId = :clientId and e.exchRate.id = :exchRateId and e.vldFrDtRef = :vldFrDtRef and e.vldToDtRef = :vldToDtRef", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ExchangeRateValue.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ExchangeRateValue e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ExchangeRateValue.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = ExchangeRateValue.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "exch_rate_id", "vld_fr_dt_ref",
		"vld_to_dt_ref"})})
public class ExchangeRateValue extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_EXCH_RATE_VAL";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: BusinessKey.
	 */
	public static final String NQ_FIND_BY_BUSINESSKEY = "ExchangeRateValue.findByBusinessKey";
	/**
	 * Named query find by unique key: BusinessKey using the ID field for references.
	 */
	public static final String NQ_FIND_BY_BUSINESSKEY_PRIMITIVE = "ExchangeRateValue.findByBusinessKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ExchangeRateValue.findByBusiness";

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "vld_fr_dt_ref", nullable = false)
	private Date vldFrDtRef;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "vld_to_dt_ref", nullable = false)
	private Date vldToDtRef;

	@NotNull
	@Column(name = "is_prvsn", nullable = false)
	private Boolean isPrvsn;

	@NotNull
	@Column(name = "rate", nullable = false, precision = 19, scale = 6)
	private BigDecimal rate;

	@NotNull
	@Column(name = "post_type_ind", nullable = false, precision = 4)
	private Integer postTypeInd;

	@Column(name = "active")
	private Boolean active;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ExchangeRate.class)
	@JoinColumn(name = "exch_rate_id", referencedColumnName = "id")
	private ExchangeRate exchRate;

	public Date getVldFrDtRef() {
		return this.vldFrDtRef;
	}

	public void setVldFrDtRef(Date vldFrDtRef) {
		this.vldFrDtRef = vldFrDtRef;
	}

	public Date getVldToDtRef() {
		return this.vldToDtRef;
	}

	public void setVldToDtRef(Date vldToDtRef) {
		this.vldToDtRef = vldToDtRef;
	}

	public Boolean getIsPrvsn() {
		return this.isPrvsn;
	}

	public void setIsPrvsn(Boolean isPrvsn) {
		this.isPrvsn = isPrvsn;
	}

	public BigDecimal getRate() {
		return this.rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Integer getPostTypeInd() {
		return this.postTypeInd;
	}

	public void setPostTypeInd(Integer postTypeInd) {
		this.postTypeInd = postTypeInd;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public ExchangeRate getExchRate() {
		return this.exchRate;
	}

	public void setExchRate(ExchangeRate exchRate) {
		if (exchRate != null) {
			this.__validate_client_context__(exchRate.getClientId());
		}
		this.exchRate = exchRate;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.exchRate == null) ? 0 : this.exchRate.hashCode());
		result = prime * result
				+ ((this.vldFrDtRef == null) ? 0 : this.vldFrDtRef.hashCode());
		result = prime * result
				+ ((this.vldToDtRef == null) ? 0 : this.vldToDtRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		ExchangeRateValue other = (ExchangeRateValue) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.exchRate == null) {
			if (other.exchRate != null) {
				return false;
			}
		} else if (!this.exchRate.equals(other.exchRate)) {
			return false;
		}
		if (this.vldFrDtRef == null) {
			if (other.vldFrDtRef != null) {
				return false;
			}
		} else if (!this.vldFrDtRef.equals(other.vldFrDtRef)) {
			return false;
		}
		if (this.vldToDtRef == null) {
			if (other.vldToDtRef != null) {
				return false;
			}
		} else if (!this.vldToDtRef.equals(other.vldToDtRef)) {
			return false;
		}
		return true;
	}
}
