/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.aircraft;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.customer.Customer;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Aircraft} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Aircraft.NQ_FIND_BY_REG, query = "SELECT e FROM Aircraft e WHERE e.clientId = :clientId and e.customer = :customer and e.registration = :registration", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Aircraft.NQ_FIND_BY_REG_PRIMITIVE, query = "SELECT e FROM Aircraft e WHERE e.clientId = :clientId and e.customer.id = :customerId and e.registration = :registration", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Aircraft.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Aircraft e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Aircraft.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Aircraft.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "customer_id", "registration"})})
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class Aircraft extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_AIRCRAFT";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Reg.
	 */
	public static final String NQ_FIND_BY_REG = "Aircraft.findByReg";
	/**
	 * Named query find by unique key: Reg using the ID field for references.
	 */
	public static final String NQ_FIND_BY_REG_PRIMITIVE = "Aircraft.findByReg_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Aircraft.findByBusiness";

	@NotBlank
	@Column(name = "registration", nullable = false, length = 10)
	private String registration;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "valid_from", nullable = false)
	private Date validFrom;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "valid_to", nullable = false)
	private Date validTo;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AcTypes.class, cascade = {CascadeType.MERGE})
	@JoinColumn(name = "aircraft_type_id", referencedColumnName = "id")
	private AcTypes acTypes;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "owner_id", referencedColumnName = "id")
	private Customer owner;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "operator_id", referencedColumnName = "id")
	private Customer operator;

	public String getRegistration() {
		return this.registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public AcTypes getAcTypes() {
		return this.acTypes;
	}

	public void setAcTypes(AcTypes acTypes) {
		if (acTypes != null) {
			this.__validate_client_context__(acTypes.getClientId());
		}
		this.acTypes = acTypes;
	}
	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		if (customer != null) {
			this.__validate_client_context__(customer.getClientId());
		}
		this.customer = customer;
	}
	public Customer getOwner() {
		return this.owner;
	}

	public void setOwner(Customer owner) {
		if (owner != null) {
			this.__validate_client_context__(owner.getClientId());
		}
		this.owner = owner;
	}
	public Customer getOperator() {
		return this.operator;
	}

	public void setOperator(Customer operator) {
		if (operator != null) {
			this.__validate_client_context__(operator.getClientId());
		}
		this.operator = operator;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
