package atraxo.fmbas.domain.ext.uom;

import atraxo.fmbas.domain.impl.uom.Unit;

public class Density {

	private Unit fromUnit;
	private Unit toUnit;
	private double density;

	public Density(Unit fromUnit, Unit toUnit, double density) {
		super();
		this.fromUnit = fromUnit;
		this.toUnit = toUnit;
		this.density = density;
	}

	public Unit getFromUnit() {
		return this.fromUnit;
	}

	public Unit getToUnit() {
		return this.toUnit;
	}

	public double getDensity() {
		return this.density;
	}

}
