/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum InvoiceFreq {

	_EMPTY_("", ""), _WEEKLY_("Weekly", "W/7"), _SEMIMONTHLY_("Semimonthly",
			"S/15"), _MONTHLY_("Monthly", "M30"), _QUARTERLY_("Quarterly",
			"Q/4"), _SEMIANNUALLY_("Semiannually", "Q/2"), _ANNUALLY_(
			"Annually", "A/1"), _PER_DELIVERY_("Per Delivery", "B/0"), _DAILY_(
			"Daily", "D/1"), _EVERY_3_DAYS_("Every 3 days", "D/3"), _EVERY_10_DAYS_(
			"Every 10 days", "T/10"), _EVERY_("Every", "E/N");

	private String name;
	private String code;

	private InvoiceFreq(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static InvoiceFreq getByName(String name) {
		for (InvoiceFreq status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent InvoiceFreq with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static InvoiceFreq getByCode(String code) {
		for (InvoiceFreq status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent InvoiceFreq with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
