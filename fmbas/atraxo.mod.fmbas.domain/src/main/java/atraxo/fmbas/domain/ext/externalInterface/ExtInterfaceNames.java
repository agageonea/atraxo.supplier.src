package atraxo.fmbas.domain.ext.externalInterface;

public enum ExtInterfaceNames {
	EXPORT_CUSTOMERS_TO_NAV("Export Customers to NAV"),
	FUEL_TICKET_PROCESSING_RESULT("Fuel ticket processing result"),
	IMPORT_IATA_FUEL_TRANSACTIONS("Import IATA Fuel Transactions"),
	CUSTOMER_CREDIT_LINE_UPDATE("Customer credit line update"),
	EXPORT_CUSTOMERS_TO_EBITS("Export Customers to eBits"),
	EXPORT_FUEL_EVENTS_TO_NAV("Send fuel events"),
	EXPORT_CONTRACTS_TO_EBITS("Export Contracts to eBits"),
	CLIENT_UPDATE_REQUEST("Client credit update request"),
	EXPORT_INVOICES_TO_NAV("Export Invoices to NAV"),
	EXPORT_CREDIT_MEMO_TO_NAV("Export Credit Memo to NAV"),
	ACK_EXPORT_CUSTOMER_DATA_NAV("Acknowledgement for submitted customer data"),
	ACK_EXPORT_CUSTOMER_DATA_EBITS("Acknowledgment on delivery of customer data"),
	ACK_EXPORT_CONTRACT_DATA_EBITS("Acknowledgment on delivery of contracts data"),
	ACK_INVOICE_INTERFACE("Sales invoice acknowledgment"),
	ACK_CREDIT_MEMO_INTERFACE("Credit memo acknowledgment"),
	ACK_FUEL_EVENT_INTERFACE("Fuel event acknowledgment"),
	ACK_FUEL_TICKET_INTERFACE("Fuel Ticket acknowledgement"),
	FUEL_TENDER_INVITATION_INTERFACE("Fuel Tender Invitation"),
	FUEL_TENDER_NEW_ROUND_INTERFACE("Fuel Tender New Round"),
	FUEL_TENDER_NO_BID_INTERFACE("Fuel Tender No Bid"),
	FUEL_TENDER_UPDATE_INTERFACE("Fuel Tender Update"),
	FUEL_TENDER_ACEEPT_AWARD("Fuel Tender Accept Award"),
	FUEL_TENDER_DECLINE_AWARD("Fuel Tender Decline Award"),
	FUEL_TENDER_CANCEL_INTERFACE("Fuel Tender Cancel"),
	FUEL_TENDER_BID_INTERFACE("Fuel Tender Bid"),
	FUEL_TENDER_BID_AWARD_INTERFACE("Fuel Tender Bid Award"),
	FUEL_TENDER_BID_DECLINE_INTERFACE("Fuel Tender Bid Decline"),
	FUEL_TENDER_BID_CANCEL_INTERFACE("Fuel Tender Bid Cancel"),
	FUEL_TENDER_PROCESSING_RESULT("Fuel Tender Processing Result"),
	FUEL_TENDER_ACKNOWLEDGMENT_INTERFACE("Fuel Tender Acknowledgement"),
	ACK_FROM_RICOH_PRINTER("Acknowledgement from Ricoh printer");

	private String value;

	private ExtInterfaceNames(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
};
