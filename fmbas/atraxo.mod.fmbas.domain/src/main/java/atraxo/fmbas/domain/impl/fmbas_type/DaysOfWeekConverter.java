/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link DaysOfWeek} enum.
 * Generated code. Do not modify in this file.
 */
public class DaysOfWeekConverter
		implements
			AttributeConverter<DaysOfWeek, String> {

	@Override
	public String convertToDatabaseColumn(DaysOfWeek value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null DaysOfWeek.");
		}
		return value.getName();
	}

	@Override
	public DaysOfWeek convertToEntityAttribute(String value) {
		return DaysOfWeek.getByName(value);
	}

}
