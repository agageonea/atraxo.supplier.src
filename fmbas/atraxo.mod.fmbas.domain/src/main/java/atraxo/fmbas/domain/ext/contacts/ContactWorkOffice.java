package atraxo.fmbas.domain.ext.contacts;

public enum ContactWorkOffice {
	HEADOFFICE("Head Office"), BRANCH("Branch"), AIRPORTOFFICE("Airport Office");

	private String displayName;

	private ContactWorkOffice(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public ContactWorkOffice getByDisplayName(String displayName) {
		for (ContactWorkOffice contactWorkOffice : values()) {
			if (contactWorkOffice.getDisplayName().equalsIgnoreCase(displayName)) {
				return contactWorkOffice;
			}
		}
		throw new RuntimeException("Inexistent work office with name: " + displayName);
	}

}
