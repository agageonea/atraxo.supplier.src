/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.job;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.JobRunResult;
import atraxo.fmbas.domain.impl.fmbas_type.JobRunResultConverter;
import atraxo.fmbas.domain.impl.fmbas_type.JobStatus;
import atraxo.fmbas.domain.impl.fmbas_type.JobStatusConverter;
import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.JobChainUser;
import atraxo.fmbas.domain.impl.job.Trigger;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link JobChain} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = JobChain.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM JobChain e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = JobChain.TABLE_NAME)
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class JobChain extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_JOB_CHAINS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "JobChain.findByBusiness";

	@NotBlank
	@Column(name = "name", nullable = false, length = 32)
	private String name;

	@Column(name = "description", length = 255)
	private String description;

	@NotBlank
	@Column(name = "status", nullable = false, length = 50)
	@Convert(converter = JobStatusConverter.class)
	private JobStatus status;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "next_run_time")
	private Date nextRunTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_run_time")
	private Date lastRunTime;

	@NotBlank
	@Column(name = "last_run_result", nullable = false, length = 32)
	@Convert(converter = JobRunResultConverter.class)
	private JobRunResult lastRunResult;

	@NotNull
	@Column(name = "enabled", nullable = false)
	private Boolean enabled;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = Trigger.class, mappedBy = "jobChain", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	private Collection<Trigger> triggers;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = Action.class, mappedBy = "jobChain", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	private Collection<Action> actions;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = JobChainUser.class, mappedBy = "jobChain", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	private Collection<JobChainUser> users;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public JobStatus getStatus() {
		return this.status;
	}

	public void setStatus(JobStatus status) {
		this.status = status;
	}

	public Date getNextRunTime() {
		return this.nextRunTime;
	}

	public void setNextRunTime(Date nextRunTime) {
		this.nextRunTime = nextRunTime;
	}

	public Date getLastRunTime() {
		return this.lastRunTime;
	}

	public void setLastRunTime(Date lastRunTime) {
		this.lastRunTime = lastRunTime;
	}

	public JobRunResult getLastRunResult() {
		return this.lastRunResult;
	}

	public void setLastRunResult(JobRunResult lastRunResult) {
		this.lastRunResult = lastRunResult;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Collection<Trigger> getTriggers() {
		return this.triggers;
	}

	public void setTriggers(Collection<Trigger> triggers) {
		this.triggers = triggers;
	}

	/**
	 * @param e
	 */
	public void addToTriggers(Trigger e) {
		if (this.triggers == null) {
			this.triggers = new ArrayList<>();
		}
		e.setJobChain(this);
		this.triggers.add(e);
	}
	public Collection<Action> getActions() {
		return this.actions;
	}

	public void setActions(Collection<Action> actions) {
		this.actions = actions;
	}

	/**
	 * @param e
	 */
	public void addToActions(Action e) {
		if (this.actions == null) {
			this.actions = new ArrayList<>();
		}
		e.setJobChain(this);
		this.actions.add(e);
	}
	public Collection<JobChainUser> getUsers() {
		return this.users;
	}

	public void setUsers(Collection<JobChainUser> users) {
		this.users = users;
	}

	/**
	 * @param e
	 */
	public void addToUsers(JobChainUser e) {
		if (this.users == null) {
			this.users = new ArrayList<>();
		}
		e.setJobChain(this);
		this.users.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.enabled == null) {
			this.enabled = new Boolean(false);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
