package atraxo.fmbas.domain.ext.mailmerge.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

public class EmailDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -8820668416642988114L;

	private String subject;
	private String from;
	private List<EmailAddressesDto> to;

	@JsonSerialize(include = Inclusion.NON_DEFAULT)
	private Set<String> cc = new HashSet<>();

	@JsonSerialize(include = Inclusion.NON_DEFAULT)
	private Set<String> bcc = new HashSet<>();

	private List<String> attachments;

	public List<EmailAddressesDto> getTo() {
		return this.to;
	}

	public void setTo(List<EmailAddressesDto> data) {
		this.to = data;
	}

	public Set<String> getCc() {
		return this.cc;
	}

	public void setCc(Set<String> cc) {
		this.cc = cc;
	}

	public Set<String> getBcc() {
		return this.bcc;
	}

	public void setBcc(Set<String> bcc) {
		this.bcc = bcc;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getFrom() {
		return this.from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public List<String> getAttachments() {
		return this.attachments;
	}

	public void setAttachments(List<String> attachments) {
		this.attachments = attachments;
	}

	@Override
	public String toString() {
		return "EmailDto [subject=" + this.subject + ", from=" + this.from + ", to=" + this.to + ", attachments=" + this.attachments + "]";
	}

}
