/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link CustomerStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class CustomerStatusConverter
		implements
			AttributeConverter<CustomerStatus, String> {

	@Override
	public String convertToDatabaseColumn(CustomerStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null CustomerStatus.");
		}
		return value.getName();
	}

	@Override
	public CustomerStatus convertToEntityAttribute(String value) {
		return CustomerStatus.getByName(value);
	}

}
