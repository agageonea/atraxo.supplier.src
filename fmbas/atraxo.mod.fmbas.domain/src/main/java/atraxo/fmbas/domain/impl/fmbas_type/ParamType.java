/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ParamType {

	_EMPTY_(""), _NOT_AVAILABLE_("Not available"), _A_C_TYPE_("A/C type"), _CURRENCY_(
			"Currency"), _SUPPLIER_("Supplier"), _UPLIFT_STATUS_(
			"Uplift status"), _STORAGE_TYPE_("Storage type");

	private String name;

	private ParamType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ParamType getByName(String name) {
		for (ParamType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent ParamType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
