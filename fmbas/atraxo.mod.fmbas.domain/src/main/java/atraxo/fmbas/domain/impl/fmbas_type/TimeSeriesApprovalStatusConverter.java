/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TimeSeriesApprovalStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class TimeSeriesApprovalStatusConverter
		implements
			AttributeConverter<TimeSeriesApprovalStatus, String> {

	@Override
	public String convertToDatabaseColumn(TimeSeriesApprovalStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null TimeSeriesApprovalStatus.");
		}
		return value.getName();
	}

	@Override
	public TimeSeriesApprovalStatus convertToEntityAttribute(String value) {
		return TimeSeriesApprovalStatus.getByName(value);
	}

}
