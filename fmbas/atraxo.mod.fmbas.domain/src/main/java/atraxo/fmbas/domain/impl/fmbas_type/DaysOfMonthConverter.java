/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link DaysOfMonth} enum.
 * Generated code. Do not modify in this file.
 */
public class DaysOfMonthConverter
		implements
			AttributeConverter<DaysOfMonth, String> {

	@Override
	public String convertToDatabaseColumn(DaysOfMonth value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null DaysOfMonth.");
		}
		return value.getName();
	}

	@Override
	public DaysOfMonth convertToEntityAttribute(String value) {
		return DaysOfMonth.getByName(value);
	}

}
