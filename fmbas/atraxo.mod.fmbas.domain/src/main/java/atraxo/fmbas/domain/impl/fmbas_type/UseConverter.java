/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link Use} enum.
 * Generated code. Do not modify in this file.
 */
public class UseConverter implements AttributeConverter<Use, String> {

	@Override
	public String convertToDatabaseColumn(Use value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null Use.");
		}
		return value.getName();
	}

	@Override
	public Use convertToEntityAttribute(String value) {
		return Use.getByName(value);
	}

}
