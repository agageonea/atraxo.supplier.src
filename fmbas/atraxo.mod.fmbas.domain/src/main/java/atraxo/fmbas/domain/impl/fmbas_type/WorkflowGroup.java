/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum WorkflowGroup {

	_EMPTY_(""), _CONTRACT_MANAGEMENT_("Contract Management"), _FUEL_TICKETS_(
			"Fuel Tickets"), _ACCOUNTING_("Accounting"), _PRICING_("Pricing"), _TENDER___BID_MANAGEMENT_(
			"Tender & Bid Management");

	private String name;

	private WorkflowGroup(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static WorkflowGroup getByName(String name) {
		for (WorkflowGroup status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent WorkflowGroup with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
