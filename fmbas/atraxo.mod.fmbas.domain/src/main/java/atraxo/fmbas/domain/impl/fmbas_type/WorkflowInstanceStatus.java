/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum WorkflowInstanceStatus {

	_EMPTY_(""), _IN_PROGRESS_("In progress"), _SUCCESS_("Success"), _FAILURE_(
			"Failure"), _TERMINATED_("Terminated");

	private String name;

	private WorkflowInstanceStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static WorkflowInstanceStatus getByName(String name) {
		for (WorkflowInstanceStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent WorkflowInstanceStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
