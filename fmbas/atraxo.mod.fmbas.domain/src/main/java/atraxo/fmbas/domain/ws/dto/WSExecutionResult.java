package atraxo.fmbas.domain.ws.dto;

/**
 * Object used for wrapping the result from executing a WS client;
 *
 * @author vhojda
 */
public class WSExecutionResult {

	private static final int MAX_EXECUTION_DATA_LENGTH = 4000;

	private boolean success;

	private String executionData = "";

	private WSExecutionType executionType;

	/**
	 *
	 */
	public WSExecutionResult() {
		this.success = false;
	}

	/**
	 * @param success
	 * @param executionData
	 * @param executionType
	 */
	public WSExecutionResult(boolean success, String executionData, WSExecutionType executionType) {
		super();
		this.success = success;
		this.executionData = executionData;
		this.executionType = executionType;
	}

	public boolean isSuccess() {
		return this.success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getExecutionData() {
		return this.executionData;
	}

	public void setExecutionData(String executionData) {
		this.executionData = executionData;
	}

	public void appendExecutionData(String data) {
		this.executionData = data + ";" + this.executionData;
	}

	public WSExecutionType getExecutionType() {
		return this.executionType;
	}

	public void setExecutionType(WSExecutionType executionType) {
		this.executionType = executionType;
	}

	/**
	 * This method returns a "trimmed" version of the execution data value, because of different DB or other length constraints
	 *
	 * @return
	 */
	public String getTrimmedExecutionData() {
		String data = this.executionData;
		if ((this.executionData != null) && (this.executionData.length() > MAX_EXECUTION_DATA_LENGTH)) {
			data = this.executionData.substring(0, MAX_EXECUTION_DATA_LENGTH - 1);
		}
		return data;
	}

	@Override
	public String toString() {
		return "WSExecutionResult [success=" + this.success + ", executionData=" + this.executionData + ", executionType=" + this.executionType + "]";
	}

}
