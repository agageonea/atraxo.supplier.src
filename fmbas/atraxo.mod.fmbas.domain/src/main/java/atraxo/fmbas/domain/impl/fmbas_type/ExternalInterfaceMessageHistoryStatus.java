/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ExternalInterfaceMessageHistoryStatus {

	_EMPTY_(""), _EXPORTED_("Exported"), _FAILED_("Failed"), _ACKNOWLEDGED_(
			"Acknowledged"), _FAILED_TIMEOUT_("Failed-Timeout"), _FAILED_COMMUNICATION_(
			"Failed-Communication"), _FAILED_NEGATIVE_ACKNOWLEDGEMENT_(
			"Failed-Negative Acknowledgement"), _SENT_("Sent");

	private String name;

	private ExternalInterfaceMessageHistoryStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ExternalInterfaceMessageHistoryStatus getByName(String name) {
		for (ExternalInterfaceMessageHistoryStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent ExternalInterfaceMessageHistoryStatus with name: "
						+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
