/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.dictionary;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.dictionary.Dictionary;
import atraxo.fmbas.domain.impl.fmbas_type.DictionaryOperator;
import atraxo.fmbas.domain.impl.fmbas_type.DictionaryOperatorConverter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Condition} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = Condition.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Condition e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Condition.TABLE_NAME)
public class Condition extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_CONDITION";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Condition.findByBusiness";

	@NotBlank
	@Column(name = "field_name", nullable = false, length = 250)
	private String fieldName;

	@NotBlank
	@Column(name = "operator", nullable = false, length = 32)
	@Convert(converter = DictionaryOperatorConverter.class)
	private DictionaryOperator operator;

	@NotBlank
	@Column(name = "condition_value", nullable = false, length = 250)
	private String value;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Dictionary.class)
	@JoinColumn(name = "dictionary_id", referencedColumnName = "id")
	private Dictionary dictionary;

	public String getFieldName() {
		return this.fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public DictionaryOperator getOperator() {
		return this.operator;
	}

	public void setOperator(DictionaryOperator operator) {
		this.operator = operator;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Dictionary getDictionary() {
		return this.dictionary;
	}

	public void setDictionary(Dictionary dictionary) {
		if (dictionary != null) {
			this.__validate_client_context__(dictionary.getClientId());
		}
		this.dictionary = dictionary;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
