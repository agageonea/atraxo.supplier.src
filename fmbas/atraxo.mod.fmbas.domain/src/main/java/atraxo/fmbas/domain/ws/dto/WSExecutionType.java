package atraxo.fmbas.domain.ws.dto;

/**
 * @author vhojda
 *
 */
public enum WSExecutionType {
	EXPORT_DATA, IMPORT_DATA;
}
