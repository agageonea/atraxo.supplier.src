/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum CreditTerm {

	_EMPTY_(""), _OPEN_CREDIT_("Open credit"), _CREDIT_LINE_("Credit line"), _BANK_GUARANTEE_(
			"Bank guarantee"), _PREPAYMENT_("Prepayment");

	private String name;

	private CreditTerm(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static CreditTerm getByName(String name) {
		for (CreditTerm status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent CreditTerm with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
