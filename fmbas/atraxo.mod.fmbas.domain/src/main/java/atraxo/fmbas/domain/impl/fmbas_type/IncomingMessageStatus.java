/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum IncomingMessageStatus {

	_EMPTY_(""), _NEW_("New"), _PROCESSING_("Processing");

	private String name;

	private IncomingMessageStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static IncomingMessageStatus getByName(String name) {
		for (IncomingMessageStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent IncomingMessageStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
