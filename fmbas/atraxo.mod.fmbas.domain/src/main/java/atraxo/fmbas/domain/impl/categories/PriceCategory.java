/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.categories;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.categories.IataPC;
import atraxo.fmbas.domain.impl.categories.MainCategory;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.fmbas_type.PriceIndConverter;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import atraxo.fmbas.domain.impl.fmbas_type.PriceTypeConverter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link PriceCategory} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = PriceCategory.NQ_FIND_BY_NAME, query = "SELECT e FROM PriceCategory e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = PriceCategory.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM PriceCategory e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = PriceCategory.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = PriceCategory.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "name"})})
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class PriceCategory extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_PRICE_CATEGORIES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "PriceCategory.findByName";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "PriceCategory.findByBusiness";

	@Column(name = "name", length = 32)
	private String name;

	@Column(name = "description", length = 255)
	private String description;

	@Column(name = "type", length = 32)
	@Convert(converter = PriceTypeConverter.class)
	private PriceType type;

	@NotBlank
	@Column(name = "price_per", nullable = false, length = 32)
	@Convert(converter = PriceIndConverter.class)
	private PriceInd pricePer;

	@NotNull
	@Column(name = "is_index_based", nullable = false)
	private Boolean isIndexBased;

	@NotNull
	@Column(name = "is_sys", nullable = false)
	private Boolean isSys;

	@NotNull
	@Column(name = "is_movement", nullable = false)
	private Boolean isMovement;

	@NotNull
	@Column(name = "is_transport", nullable = false)
	private Boolean isTransport;

	@NotNull
	@Column(name = "is_periodical", nullable = false)
	private Boolean isPeriodical;

	@NotNull
	@Column(name = "is_cso", nullable = false)
	private Boolean isCso;

	@Column(name = "active")
	private Boolean active;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = MainCategory.class)
	@JoinColumn(name = "main_category_id", referencedColumnName = "id")
	private MainCategory mainCategory;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = IataPC.class)
	@JoinColumn(name = "std_price_ctgry_id", referencedColumnName = "id")
	private IataPC iata;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public PriceType getType() {
		return this.type;
	}

	public void setType(PriceType type) {
		this.type = type;
	}

	public PriceInd getPricePer() {
		return this.pricePer;
	}

	public void setPricePer(PriceInd pricePer) {
		this.pricePer = pricePer;
	}

	public Boolean getIsIndexBased() {
		return this.isIndexBased;
	}

	public void setIsIndexBased(Boolean isIndexBased) {
		this.isIndexBased = isIndexBased;
	}

	public Boolean getIsSys() {
		return this.isSys;
	}

	public void setIsSys(Boolean isSys) {
		this.isSys = isSys;
	}

	public Boolean getIsMovement() {
		return this.isMovement;
	}

	public void setIsMovement(Boolean isMovement) {
		this.isMovement = isMovement;
	}

	public Boolean getIsTransport() {
		return this.isTransport;
	}

	public void setIsTransport(Boolean isTransport) {
		this.isTransport = isTransport;
	}

	public Boolean getIsPeriodical() {
		return this.isPeriodical;
	}

	public void setIsPeriodical(Boolean isPeriodical) {
		this.isPeriodical = isPeriodical;
	}

	public Boolean getIsCso() {
		return this.isCso;
	}

	public void setIsCso(Boolean isCso) {
		this.isCso = isCso;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public MainCategory getMainCategory() {
		return this.mainCategory;
	}

	public void setMainCategory(MainCategory mainCategory) {
		if (mainCategory != null) {
			this.__validate_client_context__(mainCategory.getClientId());
		}
		this.mainCategory = mainCategory;
	}
	public IataPC getIata() {
		return this.iata;
	}

	public void setIata(IataPC iata) {
		if (iata != null) {
			this.__validate_client_context__(iata.getClientId());
		}
		this.iata = iata;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.isIndexBased == null) {
			this.isIndexBased = new Boolean(true);
		}
		if (this.isSys == null) {
			this.isSys = new Boolean(true);
		}
		if (this.isMovement == null) {
			this.isMovement = new Boolean(true);
		}
		if (this.isTransport == null) {
			this.isTransport = new Boolean(true);
		}
		if (this.isPeriodical == null) {
			this.isPeriodical = new Boolean(true);
		}
		if (this.isCso == null) {
			this.isCso = new Boolean(true);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		PriceCategory other = (PriceCategory) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		return true;
	}
}
