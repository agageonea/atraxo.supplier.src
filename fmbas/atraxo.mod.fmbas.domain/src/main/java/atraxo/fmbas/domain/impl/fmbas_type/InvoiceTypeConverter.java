/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link InvoiceType} enum.
 * Generated code. Do not modify in this file.
 */
public class InvoiceTypeConverter
		implements
			AttributeConverter<InvoiceType, String> {

	@Override
	public String convertToDatabaseColumn(InvoiceType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null InvoiceType.");
		}
		return value.getName();
	}

	@Override
	public InvoiceType convertToEntityAttribute(String value) {
		return InvoiceType.getByName(value);
	}

}
