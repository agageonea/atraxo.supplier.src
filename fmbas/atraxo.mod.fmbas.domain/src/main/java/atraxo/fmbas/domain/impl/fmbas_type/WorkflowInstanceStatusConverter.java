/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link WorkflowInstanceStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class WorkflowInstanceStatusConverter
		implements
			AttributeConverter<WorkflowInstanceStatus, String> {

	@Override
	public String convertToDatabaseColumn(WorkflowInstanceStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null WorkflowInstanceStatus.");
		}
		return value.getName();
	}

	@Override
	public WorkflowInstanceStatus convertToEntityAttribute(String value) {
		return WorkflowInstanceStatus.getByName(value);
	}

}
