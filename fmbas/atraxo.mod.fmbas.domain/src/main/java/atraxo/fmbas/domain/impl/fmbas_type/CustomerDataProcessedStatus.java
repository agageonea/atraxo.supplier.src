/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum CustomerDataProcessedStatus {

	_EMPTY_(""), _NOT_AVAILABLE_("Not available"), _IN_PROGRESS_("In progress"), _SUCCESS_(
			"Success"), _FAILED_("Failed");

	private String name;

	private CustomerDataProcessedStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static CustomerDataProcessedStatus getByName(String name) {
		for (CustomerDataProcessedStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent CustomerDataProcessedStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
