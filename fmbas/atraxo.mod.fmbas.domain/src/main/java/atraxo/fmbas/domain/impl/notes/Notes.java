/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.notes;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Notes} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Notes.NQ_FIND_BY_ID, query = "SELECT e FROM Notes e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Notes.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Notes e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Notes.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Notes.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "id"})})
public class Notes extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_NOTES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Id.
	 */
	public static final String NQ_FIND_BY_ID = "Notes.findById";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Notes.findByBusiness";

	@NotNull
	@Column(name = "object_id", nullable = false, precision = 11)
	private Integer objectId;

	@NotBlank
	@Column(name = "object_type", nullable = false, length = 32)
	private String objectType;

	@NotBlank
	@Column(name = "notes", nullable = false, length = 2000)
	private String notes;

	public Integer getObjectId() {
		return this.objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
