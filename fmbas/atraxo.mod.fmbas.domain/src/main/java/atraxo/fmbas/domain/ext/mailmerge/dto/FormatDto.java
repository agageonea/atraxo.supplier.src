package atraxo.fmbas.domain.ext.mailmerge.dto;

/**
 * @author apetho
 */
public class FormatDto {

	private String format;

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

}
