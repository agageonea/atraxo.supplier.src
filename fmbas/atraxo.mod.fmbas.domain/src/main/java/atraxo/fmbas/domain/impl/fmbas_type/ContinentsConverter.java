/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link Continents} enum.
 * Generated code. Do not modify in this file.
 */
public class ContinentsConverter
		implements
			AttributeConverter<Continents, String> {

	@Override
	public String convertToDatabaseColumn(Continents value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null Continents.");
		}
		return value.getName();
	}

	@Override
	public Continents convertToEntityAttribute(String value) {
		return Continents.getByName(value);
	}

}
