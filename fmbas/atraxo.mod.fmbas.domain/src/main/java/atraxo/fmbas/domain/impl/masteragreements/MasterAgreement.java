/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.masteragreements;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreqConverter;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceType;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceTypeConverter;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriodConverter;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsStatus;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsStatusConverter;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDayConverter;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link MasterAgreement} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = MasterAgreement.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM MasterAgreement e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = MasterAgreement.TABLE_NAME)
public class MasterAgreement extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_MASTER_AGREEMENTS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "MasterAgreement.findByBusiness";

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "valid_from", nullable = false)
	private Date validFrom;

	@Temporal(TemporalType.DATE)
	@Column(name = "valid_to")
	private Date validTo;

	@Column(name = "evergreen")
	private Boolean evergreen;

	@Temporal(TemporalType.DATE)
	@Column(name = "renewal_reminder")
	private Date renewalReminder;

	@Column(name = "status", length = 32)
	@Convert(converter = MasterAgreementsStatusConverter.class)
	private MasterAgreementsStatus status;

	@NotBlank
	@Column(name = "period", nullable = false, length = 32)
	@Convert(converter = MasterAgreementsPeriodConverter.class)
	private MasterAgreementsPeriod period;

	@Column(name = "payment_terms", precision = 11)
	private Integer paymentTerms;

	@Column(name = "reference_to", length = 32)
	@Convert(converter = PaymentDayConverter.class)
	private PaymentDay referenceTo;

	@Column(name = "invoice_frequency", length = 32)
	@Convert(converter = InvoiceFreqConverter.class)
	private InvoiceFreq invoiceFrequency;

	@Column(name = "invoice_type", length = 32)
	@Convert(converter = InvoiceTypeConverter.class)
	private InvoiceType invoiceType;

	@Column(name = "credit_card")
	private Boolean creditCard;

	@Transient
	private String calcVal;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "company_id", referencedColumnName = "id")
	private Customer company;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "currency_id", referencedColumnName = "id")
	private Currencies currency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FinancialSources.class)
	@JoinColumn(name = "financial_source_id", referencedColumnName = "id")
	private FinancialSources financialsource;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AverageMethod.class)
	@JoinColumn(name = "average_method_id", referencedColumnName = "id")
	private AverageMethod averageMethod;

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Boolean getEvergreen() {
		return this.evergreen;
	}

	public void setEvergreen(Boolean evergreen) {
		this.evergreen = evergreen;
	}

	public Date getRenewalReminder() {
		return this.renewalReminder;
	}

	public void setRenewalReminder(Date renewalReminder) {
		this.renewalReminder = renewalReminder;
	}

	public MasterAgreementsStatus getStatus() {
		return this.status;
	}

	public void setStatus(MasterAgreementsStatus status) {
		this.status = status;
	}

	public MasterAgreementsPeriod getPeriod() {
		return this.period;
	}

	public void setPeriod(MasterAgreementsPeriod period) {
		this.period = period;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public PaymentDay getReferenceTo() {
		return this.referenceTo;
	}

	public void setReferenceTo(PaymentDay referenceTo) {
		this.referenceTo = referenceTo;
	}

	public InvoiceFreq getInvoiceFrequency() {
		return this.invoiceFrequency;
	}

	public void setInvoiceFrequency(InvoiceFreq invoiceFrequency) {
		this.invoiceFrequency = invoiceFrequency;
	}

	public InvoiceType getInvoiceType() {
		return this.invoiceType;
	}

	public void setInvoiceType(InvoiceType invoiceType) {
		this.invoiceType = invoiceType;
	}

	public Boolean getCreditCard() {
		return this.creditCard;
	}

	public void setCreditCard(Boolean creditCard) {
		this.creditCard = creditCard;
	}

	public String getCalcVal() {
		return this.getFinancialsource().getCode() + "/"
				+ this.getAverageMethod().getName();
	}

	public void setCalcVal(String calcVal) {
		this.calcVal = calcVal;
	}

	public Customer getCompany() {
		return this.company;
	}

	public void setCompany(Customer company) {
		if (company != null) {
			this.__validate_client_context__(company.getClientId());
		}
		this.company = company;
	}
	public Currencies getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currencies currency) {
		if (currency != null) {
			this.__validate_client_context__(currency.getClientId());
		}
		this.currency = currency;
	}
	public FinancialSources getFinancialsource() {
		return this.financialsource;
	}

	public void setFinancialsource(FinancialSources financialsource) {
		if (financialsource != null) {
			this.__validate_client_context__(financialsource.getClientId());
		}
		this.financialsource = financialsource;
	}
	public AverageMethod getAverageMethod() {
		return this.averageMethod;
	}

	public void setAverageMethod(AverageMethod averageMethod) {
		if (averageMethod != null) {
			this.__validate_client_context__(averageMethod.getClientId());
		}
		this.averageMethod = averageMethod;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
