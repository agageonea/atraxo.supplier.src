/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.job;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamAssignType;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamAssignTypeConverter;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamType;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamTypeConverter;
import atraxo.fmbas.domain.impl.job.BatchJob;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link BatchJobParameter} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = BatchJobParameter.NQ_FIND_BY_NAME, query = "SELECT e FROM BatchJobParameter e WHERE e.clientId = :clientId and e.batchJob = :batchJob and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = BatchJobParameter.NQ_FIND_BY_NAME_PRIMITIVE, query = "SELECT e FROM BatchJobParameter e WHERE e.clientId = :clientId and e.batchJob.id = :batchJobId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = BatchJobParameter.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM BatchJobParameter e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = BatchJobParameter.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = BatchJobParameter.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "batch_job_id", "name"})})
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class BatchJobParameter extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_BATCH_JOB_PARAMETERS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "BatchJobParameter.findByName";
	/**
	 * Named query find by unique key: Name using the ID field for references.
	 */
	public static final String NQ_FIND_BY_NAME_PRIMITIVE = "BatchJobParameter.findByName_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "BatchJobParameter.findByBusiness";

	@NotBlank
	@Column(name = "name", nullable = false, length = 32)
	private String name;

	@NotBlank
	@Column(name = "description", nullable = false, length = 255)
	private String description;

	@NotBlank
	@Column(name = "value", nullable = false, length = 50)
	private String value;

	@NotBlank
	@Column(name = "type", nullable = false, length = 32)
	@Convert(converter = JobParamTypeConverter.class)
	private JobParamType type;

	@NotBlank
	@Column(name = "assigntype", nullable = false, length = 32)
	@Convert(converter = JobParamAssignTypeConverter.class)
	private JobParamAssignType assignType;

	@Column(name = "ref_values", length = 255)
	private String refValues;

	@Column(name = "ref_business_values", length = 1000)
	private String refBusinessValues;

	@Column(name = "read_only")
	private Boolean readOnly;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = BatchJob.class)
	@JoinColumn(name = "batch_job_id", referencedColumnName = "id")
	private BatchJob batchJob;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public JobParamType getType() {
		return this.type;
	}

	public void setType(JobParamType type) {
		this.type = type;
	}

	public JobParamAssignType getAssignType() {
		return this.assignType;
	}

	public void setAssignType(JobParamAssignType assignType) {
		this.assignType = assignType;
	}

	public String getRefValues() {
		return this.refValues;
	}

	public void setRefValues(String refValues) {
		this.refValues = refValues;
	}

	public String getRefBusinessValues() {
		return this.refBusinessValues;
	}

	public void setRefBusinessValues(String refBusinessValues) {
		this.refBusinessValues = refBusinessValues;
	}

	public Boolean getReadOnly() {
		return this.readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	public BatchJob getBatchJob() {
		return this.batchJob;
	}

	public void setBatchJob(BatchJob batchJob) {
		if (batchJob != null) {
			this.__validate_client_context__(batchJob.getClientId());
		}
		this.batchJob = batchJob;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
