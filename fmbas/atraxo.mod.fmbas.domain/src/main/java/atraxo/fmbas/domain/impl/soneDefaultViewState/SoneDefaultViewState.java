/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.soneDefaultViewState;

import atraxo.ad.domain.impl.security.User;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.soneViewState.SoneViewState;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link SoneDefaultViewState} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = SoneDefaultViewState.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM SoneDefaultViewState e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = SoneDefaultViewState.TABLE_NAME)
public class SoneDefaultViewState extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "BAS_DEFAULT_VIEW_STATES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "SoneDefaultViewState.findByBusiness";

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = SoneViewState.class)
	@JoinColumn(name = "view_id", referencedColumnName = "id")
	private SoneViewState view;
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		if (user != null) {
			this.__validate_client_context__(user.getClientId());
		}
		this.user = user;
	}
	public SoneViewState getView() {
		return this.view;
	}

	public void setView(SoneViewState view) {
		if (view != null) {
			this.__validate_client_context__(view.getClientId());
		}
		this.view = view;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
