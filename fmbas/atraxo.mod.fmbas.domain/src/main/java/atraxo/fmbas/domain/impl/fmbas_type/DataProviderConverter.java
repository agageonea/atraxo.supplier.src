/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link DataProvider} enum.
 * Generated code. Do not modify in this file.
 */
public class DataProviderConverter
		implements
			AttributeConverter<DataProvider, String> {

	@Override
	public String convertToDatabaseColumn(DataProvider value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null DataProvider.");
		}
		return value.getName();
	}

	@Override
	public DataProvider convertToEntityAttribute(String value) {
		return DataProvider.getByName(value);
	}

}
