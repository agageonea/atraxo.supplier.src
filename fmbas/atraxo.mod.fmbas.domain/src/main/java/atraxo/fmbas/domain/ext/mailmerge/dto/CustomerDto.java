package atraxo.fmbas.domain.ext.mailmerge.dto;

import java.util.ArrayList;
import java.util.List;

public class CustomerDto extends AbstractDto {

	/**
	 *
	 */
	private static final long serialVersionUID = -2932897220575298584L;

	private List<AbstractDto> contracts;
	private String name;
	private String code;
	private String accountManager;
	private String officeStreet;
	private String officePostalCode;
	private String officeCountry;
	private String officeCity;

	public String getOfficeCity() {
		return this.officeCity;
	}

	public void setOfficeCity(String officeCity) {
		this.officeCity = officeCity;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getOfficeStreet() {
		return this.officeStreet;
	}

	public void setOfficeStreet(String officeStreet) {
		this.officeStreet = officeStreet;
	}

	public String getOfficePostalCode() {
		return this.officePostalCode;
	}

	public void setOfficePostalCode(String officePostalCode) {
		this.officePostalCode = officePostalCode;
	}

	public String getOfficeCountry() {
		return this.officeCountry;
	}

	public void setOfficeCountry(String officeCountry) {
		this.officeCountry = officeCountry;
	}

	public List<AbstractDto> getContracts() {
		return this.contracts;
	}

	public void setContracts(List<AbstractDto> contracts) {
		this.contracts = contracts;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccountManager() {
		return this.accountManager;
	}

	public void setAccountManager(String accountManager) {
		this.accountManager = accountManager;
	}

	public void addContract(AbstractDto contract) {
		if (this.contracts == null) {
			this.contracts = new ArrayList<>();
		}
		this.contracts.add(contract);
	}

	@Override
	public String toString() {
		return "CustomerDto [contracts=" + this.contracts + ", name=" + this.name + ", code=" + this.code + ", accountManager=" + this.accountManager
				+ ", officeStreet=" + this.officeStreet + ", officePostalCode=" + this.officePostalCode + ", officeCountry=" + this.officeCountry
				+ ", officeCity=" + this.officeCity + "]";
	}

}
