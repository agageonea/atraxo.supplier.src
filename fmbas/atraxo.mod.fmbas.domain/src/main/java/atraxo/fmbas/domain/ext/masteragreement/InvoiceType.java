package atraxo.fmbas.domain.ext.masteragreement;

public enum InvoiceType {
	ELECTRONIC("Electronic"), PAPER_TO_HQ("Paper to headquarters"), PAPER_TO_LOCAL("Paper to local");

	private String displayName;

	private InvoiceType(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public InvoiceType getByDisplayName(String displayName) {
		for (InvoiceType invoiceType : values()) {
			if (invoiceType.getDisplayName().equalsIgnoreCase(displayName)) {
				return invoiceType;
			}
		}
		throw new RuntimeException("Inexistent invoice type with name: " + displayName);
	}

}
