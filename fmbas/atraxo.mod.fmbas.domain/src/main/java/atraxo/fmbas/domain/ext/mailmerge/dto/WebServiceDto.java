package atraxo.fmbas.domain.ext.mailmerge.dto;

import java.util.Date;

/**
 * @author zspeter
 */
public class WebServiceDto extends AbstractDto {

	/**
	 *
	 */
	private static final long serialVersionUID = -8558946032325534542L;

	private String name;
	private String description;
	private Date lastRequestTime;
	private String status;
	private String result;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getLastRequestTime() {
		return this.lastRequestTime;
	}

	public void setLastRequestTime(Date lastRequestTime) {
		this.lastRequestTime = lastRequestTime;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "WebServiceDto [name=" + this.name + ", description=" + this.description + ", lastRequestTime=" + this.lastRequestTime + ", status="
				+ this.status + ", result=" + this.result + "]";
	}

}
