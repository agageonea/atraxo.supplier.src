/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ValidationMessageSeverity {

	_EMPTY_(""), _INFO_("Info"), _WARNING_("Warning"), _ERROR_("Error");

	private String name;

	private ValidationMessageSeverity(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ValidationMessageSeverity getByName(String name) {
		for (ValidationMessageSeverity status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent ValidationMessageSeverity with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
