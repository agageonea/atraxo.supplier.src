/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum PostTypeInd {

	_EMPTY_(""), _AMOUNT_("1 [Ref.Currency] = value [Currency]"), _PRICE_(
			"1 [Currency] = value [Ref.Currency]");

	private String name;

	private PostTypeInd(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static PostTypeInd getByName(String name) {
		for (PostTypeInd status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent PostTypeInd with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
