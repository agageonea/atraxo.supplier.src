/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link PostTypeInd} enum.
 * Generated code. Do not modify in this file.
 */
public class PostTypeIndConverter
		implements
			AttributeConverter<PostTypeInd, String> {

	@Override
	public String convertToDatabaseColumn(PostTypeInd value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null PostTypeInd.");
		}
		return value.getName();
	}

	@Override
	public PostTypeInd convertToEntityAttribute(String value) {
		return PostTypeInd.getByName(value);
	}

}
