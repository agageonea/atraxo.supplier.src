/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link Title} enum.
 * Generated code. Do not modify in this file.
 */
public class TitleConverter implements AttributeConverter<Title, String> {

	@Override
	public String convertToDatabaseColumn(Title value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null Title.");
		}
		return value.getName();
	}

	@Override
	public Title convertToEntityAttribute(String value) {
		return Title.getByName(value);
	}

}
