package atraxo.fmbas.domain.ext.masteragreement;

public enum Status {
	NEGOTIATION("Negotiation"), EFFECTIVE("Effective"), ON_HOLD("On Hold"), TERMINATED("Terminated");

	private String displayName;

	private Status(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public Status getByDisplayName(String displayName) {
		for (Status status : values()) {
			if (status.getDisplayName().equalsIgnoreCase(displayName)) {
				return status;
			}
		}
		throw new RuntimeException("Inexistent status with name: " + displayName);
	}

}
