/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum DaysOfWeek {

	_EMPTY_(""), _MONDAY_("Monday"), _TUESDAY_("Tuesday"), _WEDNESDAY_(
			"Wednesday"), _THURSDAY_("Thursday"), _FRIDAY_("Friday"), _SATURDAY_(
			"Saturday"), _SUNDAY_("Sunday");

	private String name;

	private DaysOfWeek(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static DaysOfWeek getByName(String name) {
		for (DaysOfWeek status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent DaysOfWeek with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
