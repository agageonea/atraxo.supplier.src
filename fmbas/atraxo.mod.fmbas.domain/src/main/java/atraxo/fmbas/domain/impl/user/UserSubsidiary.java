/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.user;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link UserSubsidiary} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = UserSubsidiary.NQ_FIND_BY_KEY, query = "SELECT e FROM UserSubsidiary e WHERE e.clientId = :clientId and e.user = :user and e.subsidiary = :subsidiary", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = UserSubsidiary.NQ_FIND_BY_KEY_PRIMITIVE, query = "SELECT e FROM UserSubsidiary e WHERE e.clientId = :clientId and e.user.id = :userId and e.subsidiary.id = :subsidiaryId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = UserSubsidiary.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM UserSubsidiary e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = UserSubsidiary.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = UserSubsidiary.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "user_id", "subsidiary_id"})})
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class UserSubsidiary extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_USER_SUBSIDIARIES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Key.
	 */
	public static final String NQ_FIND_BY_KEY = "UserSubsidiary.findByKey";
	/**
	 * Named query find by unique key: Key using the ID field for references.
	 */
	public static final String NQ_FIND_BY_KEY_PRIMITIVE = "UserSubsidiary.findByKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "UserSubsidiary.findByBusiness";

	@Column(name = "mainsubsidiary")
	private Boolean mainSubsidiary;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = UserSupp.class)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private UserSupp user;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "subsidiary_id", referencedColumnName = "id")
	private Customer subsidiary;

	public Boolean getMainSubsidiary() {
		return this.mainSubsidiary;
	}

	public void setMainSubsidiary(Boolean mainSubsidiary) {
		this.mainSubsidiary = mainSubsidiary;
	}

	public UserSupp getUser() {
		return this.user;
	}

	public void setUser(UserSupp user) {
		if (user != null) {
			this.__validate_client_context__(user.getClientId());
		}
		this.user = user;
	}
	public Customer getSubsidiary() {
		return this.subsidiary;
	}

	public void setSubsidiary(Customer subsidiary) {
		if (subsidiary != null) {
			this.__validate_client_context__(subsidiary.getClientId());
		}
		this.subsidiary = subsidiary;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
