/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum WorkflowPeriodGroup {

	_EMPTY_(""), _TODAY_("Today"), _YESTERDAY_("Yesterday"), _OLDER_("Older");

	private String name;

	private WorkflowPeriodGroup(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static WorkflowPeriodGroup getByName(String name) {
		for (WorkflowPeriodGroup status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent WorkflowPeriodGroup with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
