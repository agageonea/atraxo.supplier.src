/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.abstracts;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import seava.j4e.api.model.IModelWithClientId;
import seava.j4e.api.model.IModelWithId;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.Session;

/**
 * Entity class for {@link AbstractEntity} domain entity.
 * Generated code. Do not modify in this file.
 */
@MappedSuperclass
public abstract class AbstractEntity
		implements
			Serializable,
			IModelWithId<Integer>,
			IModelWithClientId {

	private static final long serialVersionUID = -8865917134914502125L;

	private static final Logger LOG = LoggerFactory
			.getLogger(AbstractEntity.class);

	/**
	 * Default constructor: used to set clientId
	 */
	public AbstractEntity() {
		try {
			IUser u = Session.user.get();
			if (u != null) {
				this.clientId = u.getClientId();
			}
		} catch (Exception e) {
			LOG.debug("Cannot set client id on entity.", e);
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NotNull
	@Column(name = "id", nullable = false, precision = 11)
	private Integer id;

	@NotBlank
	@Column(name = "clientid", nullable = false, length = 64)
	private String clientId;

	@NotBlank
	@Column(name = "refid", nullable = false, length = 64)
	private String refid;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", nullable = false)
	private Date createdAt;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_at", nullable = false)
	private Date modifiedAt;

	@NotBlank
	@Column(name = "created_by", nullable = false, length = 32)
	private String createdBy;

	@NotBlank
	@Column(name = "modified_by", nullable = false, length = 32)
	private String modifiedBy;

	@Version
	@Column(name = "version", precision = 10)
	private Long version;

	@Transient
	private String entityAlias;

	@Transient
	private String entityFqn;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getRefid() {
		return this.refid;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return this.modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Long getVersion() {
		return this.version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getEntityAlias() {
		return (this.entityAlias == null || this.entityAlias.isEmpty()) ? this
				.getClass().getSimpleName() : this.entityAlias;
	}

	public void setEntityAlias(String entityAlias) {
		this.entityAlias = entityAlias;
	}

	public String getEntityFqn() {
		return this.getClass().getCanonicalName();
	}

	public void setEntityFqn(String entityFqn) {
		this.entityFqn = entityFqn;
	}

	/**
	 * called before persist
	 */
	@PrePersist
	public void prePersist() {
		if (this.createdAt == null) {
			this.createdAt = new Date();
		}
		this.modifiedAt = new Date();
		if (this.createdBy == null || "".equals(this.createdBy)) {
			this.createdBy = Session.user.get().getCode();
		}
		this.modifiedBy = Session.user.get().getCode();
		this.clientId = Session.user.get().getClient().getId();
		if (this.refid == null || "".equals(this.refid)) {
			this.refid = UUID.randomUUID().toString().toUpperCase();
		}
	}

	/**
	 * called before update
	 */
	@PreUpdate
	public void preUpdate() {
		this.modifiedAt = new Date();
		this.modifiedBy = Session.user.get().getCode();
		this.__validate_client_context__(this.clientId);
	}

	protected void __validate_client_context__(String clientId) {
		if (clientId != null && "SYS".equals(Session.user.get().getCode())) {
			return;
		}
		if (clientId != null
				&& !Session.user.get().getClient().getId().equals(clientId)) {
			throw new RuntimeException(
					"Client conflict detected. You are trying to work with an entity which belongs to client with id=`"
							+ clientId
							+ "` but the current session is connected to client with id=`"
							+ Session.user.get().getClient().getId() + "` ");
		}
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		AbstractEntity other = (AbstractEntity) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!this.id.equals(other.id)) {
			return false;
		}
		return true;
	}
}
