/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link WidgetType} enum.
 * Generated code. Do not modify in this file.
 */
public class WidgetTypeConverter
		implements
			AttributeConverter<WidgetType, String> {

	@Override
	public String convertToDatabaseColumn(WidgetType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null WidgetType.");
		}
		return value.getName();
	}

	@Override
	public WidgetType convertToEntityAttribute(String value) {
		return WidgetType.getByName(value);
	}

}
