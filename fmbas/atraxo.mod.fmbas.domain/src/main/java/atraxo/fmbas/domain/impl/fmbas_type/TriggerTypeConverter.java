/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TriggerType} enum.
 * Generated code. Do not modify in this file.
 */
public class TriggerTypeConverter
		implements
			AttributeConverter<TriggerType, String> {

	@Override
	public String convertToDatabaseColumn(TriggerType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TriggerType.");
		}
		return value.getName();
	}

	@Override
	public TriggerType convertToEntityAttribute(String value) {
		return TriggerType.getByName(value);
	}

}
