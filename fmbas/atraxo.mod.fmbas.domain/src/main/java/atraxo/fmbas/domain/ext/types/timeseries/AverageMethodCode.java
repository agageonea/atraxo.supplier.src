package atraxo.fmbas.domain.ext.types.timeseries;

import java.util.Calendar;

public enum AverageMethodCode {
	WC("Weekly calendar", Calendar.WEEK_OF_YEAR),
	WT("Weekly Traded", Calendar.WEEK_OF_YEAR),
	MC("Monthly calendar", Calendar.MONTH),
	MT("Monthly Traded", Calendar.MONTH),
	DC("Daily calendar", Calendar.DAY_OF_YEAR),
	SC("Semi-monthly calendar", Calendar.WEEK_OF_MONTH),
	ST("Semi-monthly traded", Calendar.WEEK_OF_MONTH),
	FT("Fortnight (11th-25th)/(26th-10th) traded days", Calendar.WEEK_OF_MONTH),
	FC("Fortnight (11th-25th)/(26th-10th) calendar days", Calendar.WEEK_OF_MONTH);

	private String desc;
	private int calField;

	private AverageMethodCode(String description, int calField) {
		this.desc = description;
		this.calField = calField;
	}

	public static AverageMethodCode getByName(String name) {
		for (AverageMethodCode avgMthdCode : values()) {
			if (avgMthdCode.name().equalsIgnoreCase(name)) {
				return avgMthdCode;
			}
		}
		throw new IllegalArgumentException("Invalid average method name: " + name);
	}

	public String getDescription() {
		return this.desc;
	}

	public int getCalField() {
		return this.calField;
	}

}
