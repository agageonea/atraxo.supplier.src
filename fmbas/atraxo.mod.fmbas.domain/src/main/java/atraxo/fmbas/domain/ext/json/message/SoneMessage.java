package atraxo.fmbas.domain.ext.json.message;

public enum SoneMessage {

	EMPTY(
			0,
			""),
	SUPPLIER_CHANGE_FUEL_PRICE_IS_HIGHER(
			1000,
			"Supplier fuel price is higher that the old supplier fuel price"),
	SUPPLIER_CHANGE_SELLING_PRICE_IS_LOWER(
			1001,
			"Supplier fuel price is higher than current selling fuel price."),
	FUEL_QUPTE_LOCATION_MARKUP_TOLERANCE(
			2001,
			"The new markup is lower than the minimal defined in price policy.");

	private int key;
	private String message;

	private SoneMessage(int key, String message) {
		this.key = key;
		this.message = message;
	}

	public int getKey() {
		return this.key;
	}

	public String getMessage() {
		return this.message;
	}

	public static SoneMessage create() {
		return null;
	}
}
