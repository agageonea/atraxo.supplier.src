/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.soneAdvancedFilter;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link SoneAdvancedFilter} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = SoneAdvancedFilter.NQ_FIND_BY_ID, query = "SELECT e FROM SoneAdvancedFilter e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = SoneAdvancedFilter.NQ_FIND_BY_NAME_CMP, query = "SELECT e FROM SoneAdvancedFilter e WHERE e.clientId = :clientId and e.name = :name and e.cmp = :cmp", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = SoneAdvancedFilter.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM SoneAdvancedFilter e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = SoneAdvancedFilter.TABLE_NAME, uniqueConstraints = {
		@UniqueConstraint(name = SoneAdvancedFilter.TABLE_NAME + "_UK1", columnNames = {
				"clientid", "id"}),
		@UniqueConstraint(name = SoneAdvancedFilter.TABLE_NAME + "_UK2", columnNames = {
				"clientid", "name", "cmp"})})
public class SoneAdvancedFilter extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_ADVANCED_FILTER";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Id.
	 */
	public static final String NQ_FIND_BY_ID = "SoneAdvancedFilter.findById";
	/**
	 * Named query find by unique key: Name_cmp.
	 */
	public static final String NQ_FIND_BY_NAME_CMP = "SoneAdvancedFilter.findByName_cmp";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "SoneAdvancedFilter.findByBusiness";

	@Column(name = "active")
	private Boolean active;

	@Column(name = "cmp", length = 255)
	private String cmp;

	@Column(name = "cmptype", length = 16)
	private String cmpType;

	@Column(name = "name", length = 255)
	private String name;

	@Column(name = "standard_filter_value", length = 4000)
	private String standardFilterValue;

	@Column(name = "advanced_filter_value", length = 4000)
	private String advancedFilterValue;

	@Column(name = "is_standard")
	private Boolean isStandard;

	@Column(name = "default_filter")
	private Boolean defaultFilter;

	@Column(name = "available_systemwide")
	private Boolean availableSystemwide;

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCmp() {
		return this.cmp;
	}

	public void setCmp(String cmp) {
		this.cmp = cmp;
	}

	public String getCmpType() {
		return this.cmpType;
	}

	public void setCmpType(String cmpType) {
		this.cmpType = cmpType;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStandardFilterValue() {
		return this.standardFilterValue;
	}

	public void setStandardFilterValue(String standardFilterValue) {
		this.standardFilterValue = standardFilterValue;
	}

	public String getAdvancedFilterValue() {
		return this.advancedFilterValue;
	}

	public void setAdvancedFilterValue(String advancedFilterValue) {
		this.advancedFilterValue = advancedFilterValue;
	}

	public Boolean getIsStandard() {
		return this.isStandard;
	}

	public void setIsStandard(Boolean isStandard) {
		this.isStandard = isStandard;
	}

	public Boolean getDefaultFilter() {
		return this.defaultFilter;
	}

	public void setDefaultFilter(Boolean defaultFilter) {
		this.defaultFilter = defaultFilter;
	}

	public Boolean getAvailableSystemwide() {
		return this.availableSystemwide;
	}

	public void setAvailableSystemwide(Boolean availableSystemwide) {
		this.availableSystemwide = availableSystemwide;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.name == null) ? 0 : this.name.hashCode());
		result = prime * result
				+ ((this.cmp == null) ? 0 : this.cmp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		SoneAdvancedFilter other = (SoneAdvancedFilter) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		if (this.cmp == null) {
			if (other.cmp != null) {
				return false;
			}
		} else if (!this.cmp.equals(other.cmp)) {
			return false;
		}
		return true;
	}
}
