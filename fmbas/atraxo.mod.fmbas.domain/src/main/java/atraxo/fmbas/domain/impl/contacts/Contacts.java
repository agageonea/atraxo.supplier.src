/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.contacts;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.Department;
import atraxo.fmbas.domain.impl.fmbas_type.DepartmentConverter;
import atraxo.fmbas.domain.impl.fmbas_type.Title;
import atraxo.fmbas.domain.impl.fmbas_type.TitleConverter;
import atraxo.fmbas.domain.impl.fmbas_type.WorkOffice;
import atraxo.fmbas.domain.impl.fmbas_type.WorkOfficeConverter;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Contacts} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Contacts.NQ_FIND_BY_ID, query = "SELECT e FROM Contacts e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Contacts.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Contacts e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Contacts.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Contacts.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "id"})})
public class Contacts extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_CONTACTS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Id.
	 */
	public static final String NQ_FIND_BY_ID = "Contacts.findById";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Contacts.findByBusiness";

	@NotNull
	@Column(name = "object_id", nullable = false, precision = 11)
	private Integer objectId;

	@NotBlank
	@Column(name = "object_type", nullable = false, length = 32)
	private String objectType;

	@Column(name = "job_title", length = 32)
	private String jobTitle;

	@Column(name = "title", length = 32)
	@Convert(converter = TitleConverter.class)
	private Title title;

	@Column(name = "first_name", length = 64)
	private String firstName;

	@Column(name = "last_name", length = 64)
	private String lastName;

	@Column(name = "business_unit", length = 32)
	@Convert(converter = DepartmentConverter.class)
	private Department businessUnit;

	@Column(name = "business_phone", length = 100)
	private String businessPhone;

	@Column(name = "mobile_phone", length = 100)
	private String mobilePhone;

	@Column(name = "fax_number", length = 100)
	private String faxNumber;

	@Column(name = "email", length = 100)
	private String email;

	@Column(name = "work_office", length = 32)
	@Convert(converter = WorkOfficeConverter.class)
	private WorkOffice workOffice;

	@Column(name = "city", length = 100)
	private String city;

	@Column(name = "street", length = 1000)
	private String street;

	@Column(name = "state", length = 100)
	private String state;

	@Column(name = "zip", length = 64)
	private String zip;

	@Column(name = "is_primary")
	private Boolean isPrimary;

	@Column(name = "notes", length = 1000)
	private String notes;

	@Column(name = "active")
	private Boolean active;

	@Transient
	private String fullNameField;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Country.class)
	@JoinColumn(name = "country", referencedColumnName = "id")
	private Country country;

	@ManyToMany
	@JoinTable(name = "BAS_CONTACT_NOTIFICATION", joinColumns = {@JoinColumn(name = "contact_id")}, inverseJoinColumns = {@JoinColumn(name = "notification_id")})
	private Collection<NotificationEvent> notification;

	public Integer getObjectId() {
		return this.objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getJobTitle() {
		return this.jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public Title getTitle() {
		return this.title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Department getBusinessUnit() {
		return this.businessUnit;
	}

	public void setBusinessUnit(Department businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getBusinessPhone() {
		return this.businessPhone;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	public String getMobilePhone() {
		return this.mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getFaxNumber() {
		return this.faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public WorkOffice getWorkOffice() {
		return this.workOffice;
	}

	public void setWorkOffice(WorkOffice workOffice) {
		this.workOffice = workOffice;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Boolean getIsPrimary() {
		return this.isPrimary;
	}

	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getFullNameField() {
		return this.getFirstName() + " " + this.getLastName();
	}

	public void setFullNameField(String fullNameField) {
		this.fullNameField = fullNameField;
	}

	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		if (country != null) {
			this.__validate_client_context__(country.getClientId());
		}
		this.country = country;
	}

	public Collection<NotificationEvent> getNotification() {
		return this.notification;
	}

	public void setNotification(Collection<NotificationEvent> notification) {
		this.notification = notification;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
