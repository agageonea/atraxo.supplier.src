/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.job;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamAssignType;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamAssignTypeConverter;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamType;
import atraxo.fmbas.domain.impl.fmbas_type.JobParamTypeConverter;
import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.BatchJobParameter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link ActionParameter} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = ActionParameter.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ActionParameter e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ActionParameter.TABLE_NAME)
public class ActionParameter extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_JOB_ACTION_PARAMETERS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ActionParameter.findByBusiness";

	@NotBlank
	@Column(name = "name", nullable = false, length = 32)
	private String name;

	@NotBlank
	@Column(name = "description", nullable = false, length = 255)
	private String description;

	@Column(name = "value", length = 100)
	private String value;

	@Column(name = "business_value", length = 1000)
	private String businessValue;

	@NotBlank
	@Column(name = "default_value", nullable = false, length = 50)
	private String defaultValue;

	@NotBlank
	@Column(name = "type", nullable = false, length = 32)
	@Convert(converter = JobParamTypeConverter.class)
	private JobParamType type;

	@NotBlank
	@Column(name = "assigntype", nullable = false, length = 32)
	@Convert(converter = JobParamAssignTypeConverter.class)
	private JobParamAssignType assignType;

	@Column(name = "ref_values", length = 255)
	private String refValues;

	@Column(name = "ref_business_values", length = 1000)
	private String refBusinessValues;

	@Column(name = "read_only")
	private Boolean readOnly;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Action.class)
	@JoinColumn(name = "action_id", referencedColumnName = "id")
	private Action jobAction;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = BatchJobParameter.class)
	@JoinColumn(name = "batch_job_parameter_id", referencedColumnName = "id")
	private BatchJobParameter batchJobParameter;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getBusinessValue() {
		return this.businessValue;
	}

	public void setBusinessValue(String businessValue) {
		this.businessValue = businessValue;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public JobParamType getType() {
		return this.type;
	}

	public void setType(JobParamType type) {
		this.type = type;
	}

	public JobParamAssignType getAssignType() {
		return this.assignType;
	}

	public void setAssignType(JobParamAssignType assignType) {
		this.assignType = assignType;
	}

	public String getRefValues() {
		return this.refValues;
	}

	public void setRefValues(String refValues) {
		this.refValues = refValues;
	}

	public String getRefBusinessValues() {
		return this.refBusinessValues;
	}

	public void setRefBusinessValues(String refBusinessValues) {
		this.refBusinessValues = refBusinessValues;
	}

	public Boolean getReadOnly() {
		return this.readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	public Action getJobAction() {
		return this.jobAction;
	}

	public void setJobAction(Action jobAction) {
		if (jobAction != null) {
			this.__validate_client_context__(jobAction.getClientId());
		}
		this.jobAction = jobAction;
	}
	public BatchJobParameter getBatchJobParameter() {
		return this.batchJobParameter;
	}

	public void setBatchJobParameter(BatchJobParameter batchJobParameter) {
		if (batchJobParameter != null) {
			this.__validate_client_context__(batchJobParameter.getClientId());
		}
		this.batchJobParameter = batchJobParameter;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
