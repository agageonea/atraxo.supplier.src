/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.soneViewState;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.soneAdvancedFilter.SoneAdvancedFilter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link SoneViewState} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = SoneViewState.NQ_FIND_BY_ID, query = "SELECT e FROM SoneViewState e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = SoneViewState.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM SoneViewState e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = SoneViewState.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = SoneViewState.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "id"})})
public class SoneViewState extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_VIEW_STATE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Id.
	 */
	public static final String NQ_FIND_BY_ID = "SoneViewState.findById";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "SoneViewState.findByBusiness";

	@Column(name = "active")
	private Boolean active;

	@Column(name = "cmp", length = 255)
	private String cmp;

	@Column(name = "cmptype", length = 16)
	private String cmpType;

	@Column(name = "description", length = 400)
	private String description;

	@Column(name = "name", length = 255)
	private String name;

	@Column(name = "notes", length = 4000)
	private String notes;

	@Column(name = "value", length = 4000)
	private String value;

	@Column(name = "page_size", precision = 11)
	private Integer pageSize;

	@Column(name = "totals")
	private Boolean totals;

	@Column(name = "unit_code", length = 2)
	private String unitCode;

	@Column(name = "currency_code", length = 3)
	private String currencyCode;

	@Column(name = "is_standard")
	private Boolean isStandard;

	@Column(name = "default_view")
	private Boolean defaultView;

	@Column(name = "available_systemwide")
	private Boolean availableSystemwide;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = SoneAdvancedFilter.class)
	@JoinColumn(name = "filter_id", referencedColumnName = "id")
	private SoneAdvancedFilter advancedFilter;

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCmp() {
		return this.cmp;
	}

	public void setCmp(String cmp) {
		this.cmp = cmp;
	}

	public String getCmpType() {
		return this.cmpType;
	}

	public void setCmpType(String cmpType) {
		this.cmpType = cmpType;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getPageSize() {
		return this.pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Boolean getTotals() {
		return this.totals;
	}

	public void setTotals(Boolean totals) {
		this.totals = totals;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Boolean getIsStandard() {
		return this.isStandard;
	}

	public void setIsStandard(Boolean isStandard) {
		this.isStandard = isStandard;
	}

	public Boolean getDefaultView() {
		return this.defaultView;
	}

	public void setDefaultView(Boolean defaultView) {
		this.defaultView = defaultView;
	}

	public Boolean getAvailableSystemwide() {
		return this.availableSystemwide;
	}

	public void setAvailableSystemwide(Boolean availableSystemwide) {
		this.availableSystemwide = availableSystemwide;
	}

	public SoneAdvancedFilter getAdvancedFilter() {
		return this.advancedFilter;
	}

	public void setAdvancedFilter(SoneAdvancedFilter advancedFilter) {
		if (advancedFilter != null) {
			this.__validate_client_context__(advancedFilter.getClientId());
		}
		this.advancedFilter = advancedFilter;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
