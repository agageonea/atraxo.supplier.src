/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link MasterAgreementsStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class MasterAgreementsStatusConverter
		implements
			AttributeConverter<MasterAgreementsStatus, String> {

	@Override
	public String convertToDatabaseColumn(MasterAgreementsStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null MasterAgreementsStatus.");
		}
		return value.getName();
	}

	@Override
	public MasterAgreementsStatus convertToEntityAttribute(String value) {
		return MasterAgreementsStatus.getByName(value);
	}

}
