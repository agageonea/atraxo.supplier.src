/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FieldChangeName {

	_EMPTY_(""), _VALIDFROM_("ValidFrom"), _VALIDTO_("ValidTo"), _VOLUME_(
			"Volume");

	private String name;

	private FieldChangeName(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FieldChangeName getByName(String name) {
		for (FieldChangeName status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent FieldChangeName with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
