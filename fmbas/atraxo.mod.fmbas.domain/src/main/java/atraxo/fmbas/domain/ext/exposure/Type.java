package atraxo.fmbas.domain.ext.exposure;

public enum Type {
	
	CREDIT_LINE("Credit line"), 
	PREPAYMENT("Prepayment"), 
	BANK_GUARANTEE("Bank guarantee");		
	
	private String name;
	
	private Type(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public static Type getByName(String name) {
		for(Type status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new RuntimeException("Inexistent exposure type status with name: " + name);
	}
}
