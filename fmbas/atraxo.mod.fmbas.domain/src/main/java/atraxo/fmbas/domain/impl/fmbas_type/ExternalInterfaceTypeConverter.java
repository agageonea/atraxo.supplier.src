/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ExternalInterfaceType} enum.
 * Generated code. Do not modify in this file.
 */
public class ExternalInterfaceTypeConverter
		implements
			AttributeConverter<ExternalInterfaceType, String> {

	@Override
	public String convertToDatabaseColumn(ExternalInterfaceType value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null ExternalInterfaceType.");
		}
		return value.getName();
	}

	@Override
	public ExternalInterfaceType convertToEntityAttribute(String value) {
		return ExternalInterfaceType.getByName(value);
	}

}
