/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link JobStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class JobStatusConverter
		implements
			AttributeConverter<JobStatus, String> {

	@Override
	public String convertToDatabaseColumn(JobStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null JobStatus.");
		}
		return value.getName();
	}

	@Override
	public JobStatus convertToEntityAttribute(String value) {
		return JobStatus.getByName(value);
	}

}
