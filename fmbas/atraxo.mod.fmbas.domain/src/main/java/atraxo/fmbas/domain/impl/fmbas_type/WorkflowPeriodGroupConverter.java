/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link WorkflowPeriodGroup} enum.
 * Generated code. Do not modify in this file.
 */
public class WorkflowPeriodGroupConverter
		implements
			AttributeConverter<WorkflowPeriodGroup, String> {

	@Override
	public String convertToDatabaseColumn(WorkflowPeriodGroup value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null WorkflowPeriodGroup.");
		}
		return value.getName();
	}

	@Override
	public WorkflowPeriodGroup convertToEntityAttribute(String value) {
		return WorkflowPeriodGroup.getByName(value);
	}

}
