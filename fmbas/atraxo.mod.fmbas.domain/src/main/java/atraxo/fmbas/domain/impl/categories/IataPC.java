/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.categories;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.ProductSource;
import atraxo.fmbas.domain.impl.fmbas_type.ProductSourceConverter;
import atraxo.fmbas.domain.impl.fmbas_type.Use;
import atraxo.fmbas.domain.impl.fmbas_type.UseConverter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link IataPC} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = IataPC.NQ_FIND_BY_CODE, query = "SELECT e FROM IataPC e WHERE e.clientId = :clientId and e.code = :code", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = IataPC.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM IataPC e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = IataPC.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = IataPC.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "code"})})
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class IataPC extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_STD_PRICE_CTGRY";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "IataPC.findByCode";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "IataPC.findByBusiness";

	@NotBlank
	@Column(name = "code", nullable = false, length = 25)
	private String code;

	@Column(name = "name", length = 100)
	private String name;

	@Column(name = "use_cat", length = 32)
	@Convert(converter = UseConverter.class)
	private Use use;

	@Column(name = "active")
	private Boolean active;

	@NotBlank
	@Column(name = "source", nullable = false, length = 32)
	@Convert(converter = ProductSourceConverter.class)
	private ProductSource source;

	@Column(name = "source_notes", length = 255)
	private String sourceNotes;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Use getUse() {
		return this.use;
	}

	public void setUse(Use use) {
		this.use = use;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public ProductSource getSource() {
		return this.source;
	}

	public void setSource(ProductSource source) {
		this.source = source;
	}

	public String getSourceNotes() {
		return this.sourceNotes;
	}

	public void setSourceNotes(String sourceNotes) {
		this.sourceNotes = sourceNotes;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
