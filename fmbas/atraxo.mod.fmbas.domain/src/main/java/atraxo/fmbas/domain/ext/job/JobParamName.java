package atraxo.fmbas.domain.ext.job;

/**
 * Enum used for wrapping a parameter job from DB (Table bas_batch_job_parameters)
 *
 * @author vhojda
 */
public enum JobParamName {

	TARGETSYSTEM(
			"TARGETSYSTEM"),
	ENVIRONMENT(
			"ENVIRONMENT"),
	WEBSERVICEURL(
			"WEBSERVICEURL"),
	AUTHENTICATION(
			"AUTHENTICATION"),
	USERNAME(
			"USERNAME"),
	PASSWORD(
			"PASSWORD"),

	// import ECB ExchangeRate
	CURRENCY(
			"EUR"),
	OVERWRITE(
			"OVERWRITE"),
	UPDATE(
			"update"),
	NO_TIME_SERIES(
			"no_time_series"),
	SKIPPED(
			"sipped"),
	UPDATED(
			"updated"),
	IMPORTED(
			"imported"),
	TOTAL_PROCESSED(
			"total_processed"),
	DAYS_PROCESSED(
			"days_processed"),
	TIME_SERIES(
			"time_series");

	private String dbName;

	/**
	 * @param dbName
	 */
	JobParamName(String dbName) {
		this.dbName = dbName;
	}

	public String getDbName() {
		return this.dbName;
	}

}
