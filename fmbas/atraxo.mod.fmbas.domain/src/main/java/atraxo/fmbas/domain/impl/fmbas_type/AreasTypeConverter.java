/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link AreasType} enum.
 * Generated code. Do not modify in this file.
 */
public class AreasTypeConverter
		implements
			AttributeConverter<AreasType, String> {

	@Override
	public String convertToDatabaseColumn(AreasType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null AreasType.");
		}
		return value.getName();
	}

	@Override
	public AreasType convertToEntityAttribute(String value) {
		return AreasType.getByName(value);
	}

}
