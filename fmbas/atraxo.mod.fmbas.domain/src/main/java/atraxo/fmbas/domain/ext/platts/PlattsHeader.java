package atraxo.fmbas.domain.ext.platts;

public class PlattsHeader {
	private String provider;
	private String generationDate;
	private String size;
	private String type;
	private String category;
	private String date;

	public String getProvider() {
		return this.provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getGenerationDate() {
		return this.generationDate;
	}

	public void setGenerationDate(String generationDate) {
		this.generationDate = generationDate;
	}

	public String getSize() {
		return this.size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "PlattsHeader [Provider=" + this.provider + ", GenerationDate=" + this.generationDate + ", Size=" + this.size + ", Type=" + this.type
				+ ", Category=" + this.category + ", Date=" + this.date + "]";
	}

}
