package atraxo.fmbas.domain.ext.mailmerge.dto;

import java.io.Serializable;

public class SubsidiaryDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -4639868997890421268L;
	private String name;
	private String web;
	private String phone;
	private String fax;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWeb() {
		return this.web;
	}

	public void setWeb(String web) {
		this.web = web;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Override
	public String toString() {
		return "SubsidiaryDto [name=" + this.name + ", web=" + this.web + ", phone=" + this.phone + ", fax=" + this.fax + "]";
	}

}
