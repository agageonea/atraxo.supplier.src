/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.profile;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link BankAccount} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = BankAccount.NQ_FIND_BY_BANKACCOUNT, query = "SELECT e FROM BankAccount e WHERE e.clientId = :clientId and e.accountNumber = :accountNumber and e.currency = :currency and e.subsidiary = :subsidiary", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = BankAccount.NQ_FIND_BY_BANKACCOUNT_PRIMITIVE, query = "SELECT e FROM BankAccount e WHERE e.clientId = :clientId and e.accountNumber = :accountNumber and e.currency.id = :currencyId and e.subsidiary.id = :subsidiaryId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = BankAccount.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM BankAccount e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = BankAccount.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = BankAccount.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "accountnumber", "currency_id",
		"subsidiary_id"})})
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class BankAccount extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_BANK_ACCOUNTS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: BankAccount.
	 */
	public static final String NQ_FIND_BY_BANKACCOUNT = "BankAccount.findByBankAccount";
	/**
	 * Named query find by unique key: BankAccount using the ID field for references.
	 */
	public static final String NQ_FIND_BY_BANKACCOUNT_PRIMITIVE = "BankAccount.findByBankAccount_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "BankAccount.findByBusiness";

	@NotBlank
	@Column(name = "accountnumber", nullable = false, length = 64)
	private String accountNumber;

	@Column(name = "accountholder", length = 64)
	private String accountHolder;

	@Column(name = "bankname", length = 64)
	private String bankName;

	@Column(name = "bankadress", length = 200)
	private String bankAdress;

	@Column(name = "branchcode", length = 15)
	private String branchCode;

	@Column(name = "branchname", length = 50)
	private String branchName;

	@Column(name = "ibancode", length = 64)
	private String ibanCode;

	@Column(name = "swiftcode", length = 20)
	private String swiftCode;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "currency_id", referencedColumnName = "id")
	private Currencies currency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "subsidiary_id", referencedColumnName = "id")
	private Customer subsidiary;

	@ManyToMany
	@JoinTable(name = "BAS_BANKACCOUNT_CUSTOMER", joinColumns = {@JoinColumn(name = "BANK_ACCOUNT_ID")}, inverseJoinColumns = {@JoinColumn(name = "CUSTOMER_ID")})
	private Collection<Customer> customers;

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountHolder() {
		return this.accountHolder;
	}

	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAdress() {
		return this.bankAdress;
	}

	public void setBankAdress(String bankAdress) {
		this.bankAdress = bankAdress;
	}

	public String getBranchCode() {
		return this.branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getIbanCode() {
		return this.ibanCode;
	}

	public void setIbanCode(String ibanCode) {
		this.ibanCode = ibanCode;
	}

	public String getSwiftCode() {
		return this.swiftCode;
	}

	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	public Currencies getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currencies currency) {
		if (currency != null) {
			this.__validate_client_context__(currency.getClientId());
		}
		this.currency = currency;
	}
	public Customer getSubsidiary() {
		return this.subsidiary;
	}

	public void setSubsidiary(Customer subsidiary) {
		if (subsidiary != null) {
			this.__validate_client_context__(subsidiary.getClientId());
		}
		this.subsidiary = subsidiary;
	}

	public Collection<Customer> getCustomers() {
		return this.customers;
	}

	public void setCustomers(Collection<Customer> customers) {
		this.customers = customers;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime
				* result
				+ ((this.accountNumber == null) ? 0 : this.accountNumber
						.hashCode());
		result = prime * result
				+ ((this.currency == null) ? 0 : this.currency.hashCode());
		result = prime * result
				+ ((this.subsidiary == null) ? 0 : this.subsidiary.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		BankAccount other = (BankAccount) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.accountNumber == null) {
			if (other.accountNumber != null) {
				return false;
			}
		} else if (!this.accountNumber.equals(other.accountNumber)) {
			return false;
		}
		if (this.currency == null) {
			if (other.currency != null) {
				return false;
			}
		} else if (!this.currency.equals(other.currency)) {
			return false;
		}
		if (this.subsidiary == null) {
			if (other.subsidiary != null) {
				return false;
			}
		} else if (!this.subsidiary.equals(other.subsidiary)) {
			return false;
		}
		return true;
	}
}
