/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum Operator {

	_EMPTY_("", ""), _MULTIPLY_("Multiply", "M"), _DIVIDE_("Divide", "D");

	private String name;
	private String code;

	private Operator(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static Operator getByName(String name) {
		for (Operator status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Operator with name: " + name);
	}

	public String getCode() {
		return this.code;
	}

	public static Operator getByCode(String code) {
		for (Operator status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Operator with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
