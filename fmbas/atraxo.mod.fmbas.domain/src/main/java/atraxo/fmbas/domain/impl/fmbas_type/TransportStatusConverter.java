/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TransportStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class TransportStatusConverter
		implements
			AttributeConverter<TransportStatus, String> {

	@Override
	public String convertToDatabaseColumn(TransportStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TransportStatus.");
		}
		return value.getName();
	}

	@Override
	public TransportStatus convertToEntityAttribute(String value) {
		return TransportStatus.getByName(value);
	}

}
