/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.workflow;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowDeployStatus;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowDeployStatusConverter;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowGroup;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowGroupConverter;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowNotification;
import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Workflow} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Workflow.NQ_FIND_BY_CODE, query = "SELECT e FROM Workflow e WHERE e.clientId = :clientId and e.code = :code", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Workflow.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Workflow e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Workflow.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Workflow.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "code"})})
public class Workflow extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_WORKFLOW";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "Workflow.findByCode";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Workflow.findByBusiness";

	@NotBlank
	@Column(name = "name", nullable = false, length = 64)
	private String name;

	@NotBlank
	@Column(name = "code", nullable = false, length = 64)
	private String code;

	@NotBlank
	@Column(name = "workflowfile", nullable = false, length = 64)
	private String workflowFile;

	@NotBlank
	@Column(name = "processkey", nullable = false, length = 64)
	private String processKey;

	@Column(name = "description", length = 64)
	private String description;

	@Column(name = "deploystatus", length = 32)
	@Convert(converter = WorkflowDeployStatusConverter.class)
	private WorkflowDeployStatus deployStatus;

	@Column(name = "deployversion", precision = 10)
	private Integer deployVersion;

	@NotBlank
	@Column(name = "workflowgroup", nullable = false, length = 32)
	@Convert(converter = WorkflowGroupConverter.class)
	private WorkflowGroup workflowGroup;

	@Column(name = "redeploy")
	private Boolean redeploy;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "subsidiary_id", referencedColumnName = "id")
	private Customer subsidiary;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = WorkflowNotification.class, mappedBy = "workflow", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	private Collection<WorkflowNotification> notifications;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = WorkflowInstance.class, mappedBy = "workflow", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	private Collection<WorkflowInstance> instances;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = WorkflowParameter.class, mappedBy = "workflow", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	private Collection<WorkflowParameter> params;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getWorkflowFile() {
		return this.workflowFile;
	}

	public void setWorkflowFile(String workflowFile) {
		this.workflowFile = workflowFile;
	}

	public String getProcessKey() {
		return this.processKey;
	}

	public void setProcessKey(String processKey) {
		this.processKey = processKey;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public WorkflowDeployStatus getDeployStatus() {
		return this.deployStatus;
	}

	public void setDeployStatus(WorkflowDeployStatus deployStatus) {
		this.deployStatus = deployStatus;
	}

	public Integer getDeployVersion() {
		return this.deployVersion;
	}

	public void setDeployVersion(Integer deployVersion) {
		this.deployVersion = deployVersion;
	}

	public WorkflowGroup getWorkflowGroup() {
		return this.workflowGroup;
	}

	public void setWorkflowGroup(WorkflowGroup workflowGroup) {
		this.workflowGroup = workflowGroup;
	}

	public Boolean getRedeploy() {
		return this.redeploy;
	}

	public void setRedeploy(Boolean redeploy) {
		this.redeploy = redeploy;
	}

	public Customer getSubsidiary() {
		return this.subsidiary;
	}

	public void setSubsidiary(Customer subsidiary) {
		if (subsidiary != null) {
			this.__validate_client_context__(subsidiary.getClientId());
		}
		this.subsidiary = subsidiary;
	}

	public Collection<WorkflowNotification> getNotifications() {
		return this.notifications;
	}

	public void setNotifications(Collection<WorkflowNotification> notifications) {
		this.notifications = notifications;
	}

	/**
	 * @param e
	 */
	public void addToNotifications(WorkflowNotification e) {
		if (this.notifications == null) {
			this.notifications = new ArrayList<>();
		}
		e.setWorkflow(this);
		this.notifications.add(e);
	}
	public Collection<WorkflowInstance> getInstances() {
		return this.instances;
	}

	public void setInstances(Collection<WorkflowInstance> instances) {
		this.instances = instances;
	}

	/**
	 * @param e
	 */
	public void addToInstances(WorkflowInstance e) {
		if (this.instances == null) {
			this.instances = new ArrayList<>();
		}
		e.setWorkflow(this);
		this.instances.add(e);
	}
	public Collection<WorkflowParameter> getParams() {
		return this.params;
	}

	public void setParams(Collection<WorkflowParameter> params) {
		this.params = params;
	}

	/**
	 * @param e
	 */
	public void addToParams(WorkflowParameter e) {
		if (this.params == null) {
			this.params = new ArrayList<>();
		}
		e.setWorkflow(this);
		this.params.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.code == null) ? 0 : this.code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Workflow other = (Workflow) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.code == null) {
			if (other.code != null) {
				return false;
			}
		} else if (!this.code.equals(other.code)) {
			return false;
		}
		return true;
	}
}
