/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link JobRunResult} enum.
 * Generated code. Do not modify in this file.
 */
public class JobRunResultConverter
		implements
			AttributeConverter<JobRunResult, String> {

	@Override
	public String convertToDatabaseColumn(JobRunResult value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null JobRunResult.");
		}
		return value.getName();
	}

	@Override
	public JobRunResult convertToEntityAttribute(String value) {
		return JobRunResult.getByName(value);
	}

}
