/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.exposure;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTermConverter;
import atraxo.fmbas.domain.impl.fmbas_type.ExposureLastAction;
import atraxo.fmbas.domain.impl.fmbas_type.ExposureLastActionConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Exposure} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Exposure.NQ_FIND_BY_KEY, query = "SELECT e FROM Exposure e WHERE e.clientId = :clientId and e.customer = :customer and e.type = :type", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Exposure.NQ_FIND_BY_KEY_PRIMITIVE, query = "SELECT e FROM Exposure e WHERE e.clientId = :clientId and e.customer.id = :customerId and e.type = :type", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Exposure.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Exposure e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Exposure.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Exposure.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "company_id", "type"})})
public class Exposure extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_EXPOSURE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Key.
	 */
	public static final String NQ_FIND_BY_KEY = "Exposure.findByKey";
	/**
	 * Named query find by unique key: Key using the ID field for references.
	 */
	public static final String NQ_FIND_BY_KEY_PRIMITIVE = "Exposure.findByKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Exposure.findByBusiness";

	@NotBlank
	@Column(name = "type", nullable = false, length = 32)
	@Convert(converter = CreditTermConverter.class)
	private CreditTerm type;

	@NotNull
	@Column(name = "actual_utilization", nullable = false, precision = 19, scale = 6)
	private BigDecimal actualUtilization;

	@NotBlank
	@Column(name = "last_action", nullable = false, length = 32)
	@Convert(converter = ExposureLastActionConverter.class)
	private ExposureLastAction lastAction;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "company_id", referencedColumnName = "id")
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "currency_id", referencedColumnName = "id")
	private Currencies currency;

	public CreditTerm getType() {
		return this.type;
	}

	public void setType(CreditTerm type) {
		this.type = type;
	}

	public BigDecimal getActualUtilization() {
		return this.actualUtilization;
	}

	public void setActualUtilization(BigDecimal actualUtilization) {
		this.actualUtilization = actualUtilization;
	}

	public ExposureLastAction getLastAction() {
		return this.lastAction;
	}

	public void setLastAction(ExposureLastAction lastAction) {
		this.lastAction = lastAction;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		if (customer != null) {
			this.__validate_client_context__(customer.getClientId());
		}
		this.customer = customer;
	}
	public Currencies getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currencies currency) {
		if (currency != null) {
			this.__validate_client_context__(currency.getClientId());
		}
		this.currency = currency;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
