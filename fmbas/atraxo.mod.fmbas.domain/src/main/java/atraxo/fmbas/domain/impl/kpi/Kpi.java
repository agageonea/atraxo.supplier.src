/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.kpi;

import atraxo.ad.domain.impl.security.MenuItem;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.glyphFont;
import atraxo.fmbas.domain.impl.fmbas_type.glyphFontConverter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Kpi} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Kpi.NQ_FIND_BY_KPI, query = "SELECT e FROM Kpi e WHERE e.clientId = :clientId and e.dsName = :dsName and e.methodName = :methodName", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Kpi.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Kpi e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Kpi.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Kpi.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "dsname", "methodname"})})
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class Kpi extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_KPI";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Kpi.
	 */
	public static final String NQ_FIND_BY_KPI = "Kpi.findByKpi";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Kpi.findByBusiness";

	@NotBlank
	@Column(name = "dsname", nullable = false, length = 64)
	private String dsName;

	@NotBlank
	@Column(name = "methodname", nullable = false, length = 64)
	private String methodName;

	@NotBlank
	@Column(name = "glyph_code", nullable = false, length = 32)
	private String glyphCode;

	@NotBlank
	@Column(name = "glyph_font", nullable = false, length = 50)
	@Convert(converter = glyphFontConverter.class)
	private glyphFont glyphFont;

	@NotBlank
	@Column(name = "title", nullable = false, length = 255)
	private String title;

	@Column(name = "title_en", length = 255)
	private String titleEn;

	@Column(name = "title_de", length = 255)
	private String titleDe;

	@Column(name = "title_jp", length = 255)
	private String titleJp;

	@Column(name = "title_es", length = 255)
	private String titleEs;

	@Column(name = "title_fr", length = 255)
	private String titleFr;

	@Column(name = "title_ar", length = 255)
	private String titleAr;

	@Column(name = "down_limit", precision = 10)
	private Integer downLimit;

	@Column(name = "up_limit", precision = 10)
	private Integer upLimit;

	@Column(name = "description", length = 1000)
	private String description;

	@NotNull
	@Column(name = "active", nullable = false)
	private Boolean active;

	@NotNull
	@Column(name = "reverse", nullable = false)
	private Boolean reverse;

	@NotNull
	@Column(name = "kpi_order", nullable = false, precision = 2)
	private Integer kpiOrder;

	@Column(name = "unit", length = 10)
	private String unit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = MenuItem.class)
	@JoinColumn(name = "frame_id", referencedColumnName = "id")
	private MenuItem menuItem;

	public String getDsName() {
		return this.dsName;
	}

	public void setDsName(String dsName) {
		this.dsName = dsName;
	}

	public String getMethodName() {
		return this.methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getGlyphCode() {
		return this.glyphCode;
	}

	public void setGlyphCode(String glyphCode) {
		this.glyphCode = glyphCode;
	}

	public glyphFont getGlyphFont() {
		return this.glyphFont;
	}

	public void setGlyphFont(glyphFont glyphFont) {
		this.glyphFont = glyphFont;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleEn() {
		return this.titleEn;
	}

	public void setTitleEn(String titleEn) {
		this.titleEn = titleEn;
	}

	public String getTitleDe() {
		return this.titleDe;
	}

	public void setTitleDe(String titleDe) {
		this.titleDe = titleDe;
	}

	public String getTitleJp() {
		return this.titleJp;
	}

	public void setTitleJp(String titleJp) {
		this.titleJp = titleJp;
	}

	public String getTitleEs() {
		return this.titleEs;
	}

	public void setTitleEs(String titleEs) {
		this.titleEs = titleEs;
	}

	public String getTitleFr() {
		return this.titleFr;
	}

	public void setTitleFr(String titleFr) {
		this.titleFr = titleFr;
	}

	public String getTitleAr() {
		return this.titleAr;
	}

	public void setTitleAr(String titleAr) {
		this.titleAr = titleAr;
	}

	public Integer getDownLimit() {
		return this.downLimit;
	}

	public void setDownLimit(Integer downLimit) {
		this.downLimit = downLimit;
	}

	public Integer getUpLimit() {
		return this.upLimit;
	}

	public void setUpLimit(Integer upLimit) {
		this.upLimit = upLimit;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getReverse() {
		return this.reverse;
	}

	public void setReverse(Boolean reverse) {
		this.reverse = reverse;
	}

	public Integer getKpiOrder() {
		return this.kpiOrder;
	}

	public void setKpiOrder(Integer kpiOrder) {
		this.kpiOrder = kpiOrder;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public MenuItem getMenuItem() {
		return this.menuItem;
	}

	public void setMenuItem(MenuItem menuItem) {
		if (menuItem != null) {
			this.__validate_client_context__(menuItem.getClientId());
		}
		this.menuItem = menuItem;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.active == null) {
			this.active = new Boolean(true);
		}
		if (this.reverse == null) {
			this.reverse = new Boolean(false);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
