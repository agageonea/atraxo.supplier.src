package atraxo.fmbas.domain.ext.geo;

public enum AreaIndicator {
	SYSTEM("System"), USER("User");

	private String displayName;

	private AreaIndicator(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public static AreaIndicator getByDisplayName(String displayName) {
		for (AreaIndicator areaIndicator : values()) {
			if (areaIndicator.getDisplayName().equalsIgnoreCase(displayName)) {
				return areaIndicator;
			}
		}
		throw new RuntimeException("Inexistent area indicator with name: " + displayName);
	}

}
