/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link SerieFreqInd} enum.
 * Generated code. Do not modify in this file.
 */
public class SerieFreqIndConverter
		implements
			AttributeConverter<SerieFreqInd, String> {

	@Override
	public String convertToDatabaseColumn(SerieFreqInd value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null SerieFreqInd.");
		}
		return value.getName();
	}

	@Override
	public SerieFreqInd convertToEntityAttribute(String value) {
		return SerieFreqInd.getByName(value);
	}

}
