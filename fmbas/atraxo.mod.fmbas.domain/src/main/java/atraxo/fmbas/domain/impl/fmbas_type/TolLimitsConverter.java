/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TolLimits} enum.
 * Generated code. Do not modify in this file.
 */
public class TolLimitsConverter
		implements
			AttributeConverter<TolLimits, String> {

	@Override
	public String convertToDatabaseColumn(TolLimits value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TolLimits.");
		}
		return value.getName();
	}

	@Override
	public TolLimits convertToEntityAttribute(String value) {
		return TolLimits.getByName(value);
	}

}
