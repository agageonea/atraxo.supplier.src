/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.exchangerate;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link ExchangeRate} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = ExchangeRate.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ExchangeRate e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ExchangeRate.TABLE_NAME)
public class ExchangeRate extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_EXCH_RATE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ExchangeRate.findByBusiness";

	@Column(name = "description", length = 200)
	private String description;

	@Column(name = "active")
	private Boolean active;

	@Column(name = "name", length = 100)
	private String name;

	@Transient
	private String calcVal;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = TimeSerie.class)
	@JoinColumn(name = "tserie_id", referencedColumnName = "id")
	private TimeSerie tserie;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "curency_1_id", referencedColumnName = "id")
	private Currencies currency1;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "curency_2_id", referencedColumnName = "id")
	private Currencies currency2;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FinancialSources.class)
	@JoinColumn(name = "finsource_id", referencedColumnName = "id")
	private FinancialSources finsource;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AverageMethod.class)
	@JoinColumn(name = "avg_mthd_id", referencedColumnName = "id")
	private AverageMethod avgMethodIndicator;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = ExchangeRateValue.class, mappedBy = "exchRate", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<ExchangeRateValue> exchangeRateValue;

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCalcVal() {
		return this.getFinsource().getCode() + "/"
				+ this.getAvgMethodIndicator().getName();
	}

	public void setCalcVal(String calcVal) {
		this.calcVal = calcVal;
	}

	public TimeSerie getTserie() {
		return this.tserie;
	}

	public void setTserie(TimeSerie tserie) {
		if (tserie != null) {
			this.__validate_client_context__(tserie.getClientId());
		}
		this.tserie = tserie;
	}
	public Currencies getCurrency1() {
		return this.currency1;
	}

	public void setCurrency1(Currencies currency1) {
		if (currency1 != null) {
			this.__validate_client_context__(currency1.getClientId());
		}
		this.currency1 = currency1;
	}
	public Currencies getCurrency2() {
		return this.currency2;
	}

	public void setCurrency2(Currencies currency2) {
		if (currency2 != null) {
			this.__validate_client_context__(currency2.getClientId());
		}
		this.currency2 = currency2;
	}
	public FinancialSources getFinsource() {
		return this.finsource;
	}

	public void setFinsource(FinancialSources finsource) {
		if (finsource != null) {
			this.__validate_client_context__(finsource.getClientId());
		}
		this.finsource = finsource;
	}
	public AverageMethod getAvgMethodIndicator() {
		return this.avgMethodIndicator;
	}

	public void setAvgMethodIndicator(AverageMethod avgMethodIndicator) {
		if (avgMethodIndicator != null) {
			this.__validate_client_context__(avgMethodIndicator.getClientId());
		}
		this.avgMethodIndicator = avgMethodIndicator;
	}

	public Collection<ExchangeRateValue> getExchangeRateValue() {
		return this.exchangeRateValue;
	}

	public void setExchangeRateValue(
			Collection<ExchangeRateValue> exchangeRateValue) {
		this.exchangeRateValue = exchangeRateValue;
	}

	/**
	 * @param e
	 */
	public void addToExchangeRateValue(ExchangeRateValue e) {
		if (this.exchangeRateValue == null) {
			this.exchangeRateValue = new ArrayList<>();
		}
		e.setExchRate(this);
		this.exchangeRateValue.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
