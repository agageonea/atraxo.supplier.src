/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TimeDuration {

	_EMPTY_(""), _30_MINUTES_("30 minutes"), _1_HOUR_("1 hour"), _2_HOURS_(
			"2 hours"), _4_HOURS_("4 hours"), _12_HOURS_("12 hours"), _1_DAY_(
			"1 day"), _3_DAYS_("3 days");

	private String name;

	private TimeDuration(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TimeDuration getByName(String name) {
		for (TimeDuration status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TimeDuration with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
