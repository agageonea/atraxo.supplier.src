/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum Months {

	_EMPTY_(""), _ALL_("All"), _JANUARY_("January"), _FEBRUARY_("February"), _MARCH_(
			"March"), _APRIL_("April"), _MAY_("May"), _JUNE_("June"), _JULY_(
			"July"), _AUGUST_("August"), _SEPTEMBER_("September"), _OCTOBER_(
			"October"), _NOVEMBER_("November"), _DECEMBER_("December");

	private String name;

	private Months(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static Months getByName(String name) {
		for (Months status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Months with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
