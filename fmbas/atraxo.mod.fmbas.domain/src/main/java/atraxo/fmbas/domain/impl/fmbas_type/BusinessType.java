/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum BusinessType {

	_EMPTY_(""), _SUPPLIER_("Supplier"), _AIRLINE_("Airline");

	private String name;

	private BusinessType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static BusinessType getByName(String name) {
		for (BusinessType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent BusinessType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
