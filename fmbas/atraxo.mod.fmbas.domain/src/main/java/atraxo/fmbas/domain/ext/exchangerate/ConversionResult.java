package atraxo.fmbas.domain.ext.exchangerate;

import java.math.BigDecimal;

/**
 * Class used to hold data about an exchange rate conversion.
 *
 * @author zspeter
 *
 */
public class ConversionResult {

	/**
	 * Converted value.
	 */
	private BigDecimal value;

	/**
	 * Factor used for conversion.
	 */
	private BigDecimal factor;

	public ConversionResult() {
	}

	public ConversionResult(BigDecimal value, BigDecimal factor) {
		this.value = value;
		this.factor = factor;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public BigDecimal getFactor() {
		return this.factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

}
