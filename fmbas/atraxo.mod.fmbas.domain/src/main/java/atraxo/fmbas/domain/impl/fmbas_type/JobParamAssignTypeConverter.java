/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link JobParamAssignType} enum.
 * Generated code. Do not modify in this file.
 */
public class JobParamAssignTypeConverter
		implements
			AttributeConverter<JobParamAssignType, String> {

	@Override
	public String convertToDatabaseColumn(JobParamAssignType value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null JobParamAssignType.");
		}
		return value.getName();
	}

	@Override
	public JobParamAssignType convertToEntityAttribute(String value) {
		return JobParamAssignType.getByName(value);
	}

}
