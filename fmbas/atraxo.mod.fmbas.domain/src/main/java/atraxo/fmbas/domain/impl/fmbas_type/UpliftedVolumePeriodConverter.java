/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link UpliftedVolumePeriod} enum.
 * Generated code. Do not modify in this file.
 */
public class UpliftedVolumePeriodConverter
		implements
			AttributeConverter<UpliftedVolumePeriod, String> {

	@Override
	public String convertToDatabaseColumn(UpliftedVolumePeriod value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null UpliftedVolumePeriod.");
		}
		return value.getName();
	}

	@Override
	public UpliftedVolumePeriod convertToEntityAttribute(String value) {
		return UpliftedVolumePeriod.getByName(value);
	}

}
