/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum PriceType {

	_EMPTY_(""), _PRODUCT_("Product"), _DIFFERENTIAL_("Differential"), _INTO_PLANE_FEE_(
			"Into Plane Fee"), _OTHER_FEE_("Other fee"), _TAX_("Tax");

	private String name;

	private PriceType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static PriceType getByName(String name) {
		for (PriceType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent PriceType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
