/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.job;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.RepeatDuration;
import atraxo.fmbas.domain.impl.fmbas_type.RepeatDurationConverter;
import atraxo.fmbas.domain.impl.fmbas_type.TimeDuration;
import atraxo.fmbas.domain.impl.fmbas_type.TimeDurationConverter;
import atraxo.fmbas.domain.impl.fmbas_type.TimeInterval;
import atraxo.fmbas.domain.impl.fmbas_type.TimeIntervalConverter;
import atraxo.fmbas.domain.impl.fmbas_type.TriggerType;
import atraxo.fmbas.domain.impl.fmbas_type.TriggerTypeConverter;
import atraxo.fmbas.domain.impl.job.JobChain;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Trigger} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = Trigger.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Trigger e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Trigger.TABLE_NAME)
public class Trigger extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_JOB_TRIGGERS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Trigger.findByBusiness";

	@NotBlank
	@Column(name = "type", nullable = false, length = 32)
	@Convert(converter = TriggerTypeConverter.class)
	private TriggerType type;

	@NotBlank
	@Column(name = "details", nullable = false, length = 255)
	private String details;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_date", nullable = false)
	private Date startDate;

	@Column(name = "int_in_days", precision = 3)
	private Integer intInDays;

	@Column(name = "int_in_weeks", precision = 3)
	private Integer intInWeeks;

	@Column(name = "months", length = 255)
	private String months;

	@Column(name = "days_of_week", length = 255)
	private String daysOfWeek;

	@Column(name = "days_of_month", length = 255)
	private String daysOfMonth;

	@Column(name = "week_of_month", length = 255)
	private String weekOfMonth;

	@Column(name = "on_month_days")
	private Boolean onMonthDays;

	@Column(name = "on_week_days")
	private Boolean onWeekDays;

	@Column(name = "repeat_int", length = 32)
	@Convert(converter = TimeIntervalConverter.class)
	private TimeInterval repeatInterval;

	@Column(name = "repeat_dur", length = 32)
	@Convert(converter = RepeatDurationConverter.class)
	private RepeatDuration repeatDuration;

	@Column(name = "repeat_count", length = 32)
	@Convert(converter = TimeDurationConverter.class)
	private TimeDuration timeout;

	@Column(name = "status")
	private Boolean active;

	@Column(name = "use_utc")
	private Boolean timeReference;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = JobChain.class)
	@JoinColumn(name = "job_id", referencedColumnName = "id")
	private JobChain jobChain;

	public TriggerType getType() {
		return this.type;
	}

	public void setType(TriggerType type) {
		this.type = type;
	}

	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Integer getIntInDays() {
		return this.intInDays;
	}

	public void setIntInDays(Integer intInDays) {
		this.intInDays = intInDays;
	}

	public Integer getIntInWeeks() {
		return this.intInWeeks;
	}

	public void setIntInWeeks(Integer intInWeeks) {
		this.intInWeeks = intInWeeks;
	}

	public String getMonths() {
		return this.months;
	}

	public void setMonths(String months) {
		this.months = months;
	}

	public String getDaysOfWeek() {
		return this.daysOfWeek;
	}

	public void setDaysOfWeek(String daysOfWeek) {
		this.daysOfWeek = daysOfWeek;
	}

	public String getDaysOfMonth() {
		return this.daysOfMonth;
	}

	public void setDaysOfMonth(String daysOfMonth) {
		this.daysOfMonth = daysOfMonth;
	}

	public String getWeekOfMonth() {
		return this.weekOfMonth;
	}

	public void setWeekOfMonth(String weekOfMonth) {
		this.weekOfMonth = weekOfMonth;
	}

	public Boolean getOnMonthDays() {
		return this.onMonthDays;
	}

	public void setOnMonthDays(Boolean onMonthDays) {
		this.onMonthDays = onMonthDays;
	}

	public Boolean getOnWeekDays() {
		return this.onWeekDays;
	}

	public void setOnWeekDays(Boolean onWeekDays) {
		this.onWeekDays = onWeekDays;
	}

	public TimeInterval getRepeatInterval() {
		return this.repeatInterval;
	}

	public void setRepeatInterval(TimeInterval repeatInterval) {
		this.repeatInterval = repeatInterval;
	}

	public RepeatDuration getRepeatDuration() {
		return this.repeatDuration;
	}

	public void setRepeatDuration(RepeatDuration repeatDuration) {
		this.repeatDuration = repeatDuration;
	}

	public TimeDuration getTimeout() {
		return this.timeout;
	}

	public void setTimeout(TimeDuration timeout) {
		this.timeout = timeout;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getTimeReference() {
		return this.timeReference;
	}

	public void setTimeReference(Boolean timeReference) {
		this.timeReference = timeReference;
	}

	public JobChain getJobChain() {
		return this.jobChain;
	}

	public void setJobChain(JobChain jobChain) {
		if (jobChain != null) {
			this.__validate_client_context__(jobChain.getClientId());
		}
		this.jobChain = jobChain;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
