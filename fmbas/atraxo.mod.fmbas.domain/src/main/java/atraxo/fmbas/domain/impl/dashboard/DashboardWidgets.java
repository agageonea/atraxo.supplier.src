/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.dashboard;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.Dashboards;
import atraxo.fmbas.domain.impl.dashboard.Layouts;
import atraxo.fmbas.domain.impl.dashboard.Widgets;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link DashboardWidgets} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = DashboardWidgets.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM DashboardWidgets e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = DashboardWidgets.TABLE_NAME)
public class DashboardWidgets extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_DASHBOARD_WIDGETS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "DashboardWidgets.findByBusiness";

	@NotBlank
	@Column(name = "layout_region_id", nullable = false, length = 100)
	private String layoutRegionId;

	@NotBlank
	@Column(name = "widget_index", nullable = false, length = 2)
	private String widgetIndex;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Dashboards.class)
	@JoinColumn(name = "dashboard_id", referencedColumnName = "id")
	private Dashboards dashboard;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Layouts.class)
	@JoinColumn(name = "layout_id", referencedColumnName = "id")
	private Layouts layout;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Widgets.class)
	@JoinColumn(name = "widget_id", referencedColumnName = "id")
	private Widgets widget;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = AssignedWidgetParameters.class, mappedBy = "dashboardWidgets")
	private Collection<AssignedWidgetParameters> assignedWidgetParameters;

	public String getLayoutRegionId() {
		return this.layoutRegionId;
	}

	public void setLayoutRegionId(String layoutRegionId) {
		this.layoutRegionId = layoutRegionId;
	}

	public String getWidgetIndex() {
		return this.widgetIndex;
	}

	public void setWidgetIndex(String widgetIndex) {
		this.widgetIndex = widgetIndex;
	}

	public Dashboards getDashboard() {
		return this.dashboard;
	}

	public void setDashboard(Dashboards dashboard) {
		if (dashboard != null) {
			this.__validate_client_context__(dashboard.getClientId());
		}
		this.dashboard = dashboard;
	}
	public Layouts getLayout() {
		return this.layout;
	}

	public void setLayout(Layouts layout) {
		if (layout != null) {
			this.__validate_client_context__(layout.getClientId());
		}
		this.layout = layout;
	}
	public Widgets getWidget() {
		return this.widget;
	}

	public void setWidget(Widgets widget) {
		if (widget != null) {
			this.__validate_client_context__(widget.getClientId());
		}
		this.widget = widget;
	}

	public Collection<AssignedWidgetParameters> getAssignedWidgetParameters() {
		return this.assignedWidgetParameters;
	}

	public void setAssignedWidgetParameters(
			Collection<AssignedWidgetParameters> assignedWidgetParameters) {
		this.assignedWidgetParameters = assignedWidgetParameters;
	}

	/**
	 * @param e
	 */
	public void addToAssignedWidgetParameters(AssignedWidgetParameters e) {
		if (this.assignedWidgetParameters == null) {
			this.assignedWidgetParameters = new ArrayList<>();
		}
		e.setDashboardWidgets(this);
		this.assignedWidgetParameters.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
