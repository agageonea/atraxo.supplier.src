/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.templates;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.customer.CustomerNotification;
import atraxo.fmbas.domain.impl.externalInterfaces.InterfaceUser;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Template} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Template.NQ_FIND_BY_KEY, query = "SELECT e FROM Template e WHERE e.clientId = :clientId and e.user = :user and e.subsidiary = :subsidiary and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Template.NQ_FIND_BY_KEY_PRIMITIVE, query = "SELECT e FROM Template e WHERE e.clientId = :clientId and e.user.id = :userId and e.subsidiary.id = :subsidiaryId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Template.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Template e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Template.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Template.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "user_id", "subsidiary_id", "name"})})
public class Template extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_TEMPLATES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Key.
	 */
	public static final String NQ_FIND_BY_KEY = "Template.findByKey";
	/**
	 * Named query find by unique key: Key using the ID field for references.
	 */
	public static final String NQ_FIND_BY_KEY_PRIMITIVE = "Template.findByKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Template.findByBusiness";

	@NotBlank
	@Column(name = "name", nullable = false, length = 100)
	private String name;

	@Column(name = "description", length = 1000)
	private String description;

	@Column(name = "filename", length = 100)
	private String fileName;

	@Temporal(TemporalType.DATE)
	@Column(name = "uploaddate")
	private Date uploadDate;

	@Column(name = "filereference", length = 100)
	private String fileReference;

	@Column(name = "system_use")
	private Boolean systemUse;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = NotificationEvent.class)
	@JoinColumn(name = "type_id", referencedColumnName = "id")
	private NotificationEvent type;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = UserSupp.class)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private UserSupp user;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "subsidiary_id", referencedColumnName = "id")
	private Customer subsidiary;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = InterfaceUser.class, mappedBy = "emailTemplate")
	private Collection<InterfaceUser> interfaceUser;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = CustomerNotification.class, mappedBy = "template")
	private Collection<CustomerNotification> customerNotification;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getUploadDate() {
		return this.uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getFileReference() {
		return this.fileReference;
	}

	public void setFileReference(String fileReference) {
		this.fileReference = fileReference;
	}

	public Boolean getSystemUse() {
		return this.systemUse;
	}

	public void setSystemUse(Boolean systemUse) {
		this.systemUse = systemUse;
	}

	public NotificationEvent getType() {
		return this.type;
	}

	public void setType(NotificationEvent type) {
		if (type != null) {
			this.__validate_client_context__(type.getClientId());
		}
		this.type = type;
	}
	public UserSupp getUser() {
		return this.user;
	}

	public void setUser(UserSupp user) {
		if (user != null) {
			this.__validate_client_context__(user.getClientId());
		}
		this.user = user;
	}
	public Customer getSubsidiary() {
		return this.subsidiary;
	}

	public void setSubsidiary(Customer subsidiary) {
		if (subsidiary != null) {
			this.__validate_client_context__(subsidiary.getClientId());
		}
		this.subsidiary = subsidiary;
	}

	public Collection<InterfaceUser> getInterfaceUser() {
		return this.interfaceUser;
	}

	public void setInterfaceUser(Collection<InterfaceUser> interfaceUser) {
		this.interfaceUser = interfaceUser;
	}

	/**
	 * @param e
	 */
	public void addToInterfaceUser(InterfaceUser e) {
		if (this.interfaceUser == null) {
			this.interfaceUser = new ArrayList<>();
		}
		e.setEmailTemplate(this);
		this.interfaceUser.add(e);
	}
	public Collection<CustomerNotification> getCustomerNotification() {
		return this.customerNotification;
	}

	public void setCustomerNotification(
			Collection<CustomerNotification> customerNotification) {
		this.customerNotification = customerNotification;
	}

	/**
	 * @param e
	 */
	public void addToCustomerNotification(CustomerNotification e) {
		if (this.customerNotification == null) {
			this.customerNotification = new ArrayList<>();
		}
		e.setTemplate(this);
		this.customerNotification.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.user == null) ? 0 : this.user.hashCode());
		result = prime * result
				+ ((this.subsidiary == null) ? 0 : this.subsidiary.hashCode());
		result = prime * result
				+ ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Template other = (Template) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.user == null) {
			if (other.user != null) {
				return false;
			}
		} else if (!this.user.equals(other.user)) {
			return false;
		}
		if (this.subsidiary == null) {
			if (other.subsidiary != null) {
				return false;
			}
		} else if (!this.subsidiary.equals(other.subsidiary)) {
			return false;
		}
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		return true;
	}
}
