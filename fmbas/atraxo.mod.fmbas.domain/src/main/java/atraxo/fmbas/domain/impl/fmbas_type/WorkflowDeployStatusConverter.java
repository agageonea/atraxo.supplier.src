/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link WorkflowDeployStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class WorkflowDeployStatusConverter
		implements
			AttributeConverter<WorkflowDeployStatus, String> {

	@Override
	public String convertToDatabaseColumn(WorkflowDeployStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null WorkflowDeployStatus.");
		}
		return value.getName();
	}

	@Override
	public WorkflowDeployStatus convertToEntityAttribute(String value) {
		return WorkflowDeployStatus.getByName(value);
	}

}
