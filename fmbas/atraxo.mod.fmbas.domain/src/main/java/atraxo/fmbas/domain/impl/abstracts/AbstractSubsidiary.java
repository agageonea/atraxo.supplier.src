/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.abstracts;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import seava.j4e.api.model.IModelWithSubsidiaryId;
import seava.j4e.api.session.Session;

/**
 * Entity class for {@link AbstractSubsidiary} domain entity.
 * Generated code. Do not modify in this file.
 */
@MappedSuperclass
public abstract class AbstractSubsidiary extends AbstractEntity
		implements
			Serializable,
			IModelWithSubsidiaryId {

	private static final long serialVersionUID = -8865917134914502125L;

	@Column(name = "subsidiary_id", nullable = true, length = 64)
	private String subsidiaryId;

	public String getSubsidiaryId() {
		return this.subsidiaryId;
	}

	public void setSubsidiaryId(String subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.subsidiaryId == null) {
			this.subsidiaryId = Session.user.get().getClient()
					.getActiveSubsidiaryId();
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
