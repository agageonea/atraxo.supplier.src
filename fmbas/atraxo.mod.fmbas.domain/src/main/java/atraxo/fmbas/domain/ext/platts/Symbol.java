package atraxo.fmbas.domain.ext.platts;

public enum Symbol {
	N, C, F, X, D;
}
