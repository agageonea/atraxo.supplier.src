package atraxo.fmbas.domain.ext.history;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HistoryMessage extends HashMap<Object, List<HistoryValue>> {

	private static final long serialVersionUID = -1377977732092780366L;

	public void merge(Map<Object, List<HistoryValue>> historyMessage) {
		historyMessage.forEach((key, value) -> this.merge(key, value, HistoryMessage::concatValues));
	}

	private static List<HistoryValue> concatValues(List<HistoryValue> l1, List<HistoryValue> l2) {
		return Stream.concat(l1.stream(), l2.stream()).collect(Collectors.toList());
	}
}
