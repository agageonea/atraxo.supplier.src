/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link JobParamType} enum.
 * Generated code. Do not modify in this file.
 */
public class JobParamTypeConverter
		implements
			AttributeConverter<JobParamType, String> {

	@Override
	public String convertToDatabaseColumn(JobParamType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null JobParamType.");
		}
		return value.getName();
	}

	@Override
	public JobParamType convertToEntityAttribute(String value) {
		return JobParamType.getByName(value);
	}

}
