package atraxo.fmbas.domain.ext.masteragreement;

public enum Period {
	PREVIOUS("Previous period"), CURRENT("Current preiod"), NEXT("Next period"), BEFORE_PREVIOUS("Period before previous period");

	private String displayName;

	private Period(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public Period getByDisplayName(String displayName) {
		for (Period period : values()) {
			if (period.getDisplayName().equalsIgnoreCase(displayName)) {
				return period;
			}
		}
		throw new RuntimeException("Inexistent period with name: " + displayName);
	}

}
