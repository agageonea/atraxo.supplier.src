/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TransportStatus {

	_EMPTY_(""), _NEW_("New"), _SENT_("Sent"), _DELIVERED_("Delivered"), _PROCESSED_(
			"Processed"), _FAILURE_("Failure");

	private String name;

	private TransportStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TransportStatus getByName(String name) {
		for (TransportStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TransportStatus with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
