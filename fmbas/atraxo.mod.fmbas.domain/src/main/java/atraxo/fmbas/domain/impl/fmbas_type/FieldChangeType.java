/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FieldChangeType {

	_EMPTY_(""), _INTEGER_("Integer"), _BIGDECIMAL_("BigDecimal"), _STRING_(
			"String"), _DATE_("Date"), _LONG_("Long"), _BOOLEAN_("Boolean"), _BLOB_(
			"Blob");

	private String name;

	private FieldChangeType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FieldChangeType getByName(String name) {
		for (FieldChangeType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent FieldChangeType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
