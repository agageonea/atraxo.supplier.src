/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ProcessStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class ProcessStatusConverter
		implements
			AttributeConverter<ProcessStatus, String> {

	@Override
	public String convertToDatabaseColumn(ProcessStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ProcessStatus.");
		}
		return value.getName();
	}

	@Override
	public ProcessStatus convertToEntityAttribute(String value) {
		return ProcessStatus.getByName(value);
	}

}
