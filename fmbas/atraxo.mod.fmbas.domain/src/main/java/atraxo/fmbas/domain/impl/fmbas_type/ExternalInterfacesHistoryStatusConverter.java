/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ExternalInterfacesHistoryStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class ExternalInterfacesHistoryStatusConverter
		implements
			AttributeConverter<ExternalInterfacesHistoryStatus, String> {

	@Override
	public String convertToDatabaseColumn(ExternalInterfacesHistoryStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null ExternalInterfacesHistoryStatus.");
		}
		return value.getName();
	}

	@Override
	public ExternalInterfacesHistoryStatus convertToEntityAttribute(String value) {
		return ExternalInterfacesHistoryStatus.getByName(value);
	}

}
