/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link PaymentDay} enum.
 * Generated code. Do not modify in this file.
 */
public class PaymentDayConverter
		implements
			AttributeConverter<PaymentDay, String> {

	@Override
	public String convertToDatabaseColumn(PaymentDay value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null PaymentDay.");
		}
		return value.getName();
	}

	@Override
	public PaymentDay convertToEntityAttribute(String value) {
		return PaymentDay.getByName(value);
	}

}
