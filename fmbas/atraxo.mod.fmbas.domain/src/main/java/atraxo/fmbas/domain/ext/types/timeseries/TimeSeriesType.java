package atraxo.fmbas.domain.ext.types.timeseries;

public enum TimeSeriesType {
	ENERGYQUOTATION("E"), EXCHANGERATE("X");

	private String code;

	private TimeSeriesType(String code) {
		this.code = code;
	}

	public String getCode() {
		return this.code;
	}

	public static TimeSeriesType getByCode(String code) {
		for (TimeSeriesType type : values()) {
			if (code.equalsIgnoreCase(type.getCode())) {
				return type;
			}
		}
		throw new RuntimeException("Invalid time series type: " + code);
	}
}
