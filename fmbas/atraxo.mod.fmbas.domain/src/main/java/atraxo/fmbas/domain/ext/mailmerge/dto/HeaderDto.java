package atraxo.fmbas.domain.ext.mailmerge.dto;

public class HeaderDto {

	private FormatDto header;
	private AbstractDto data;
	private EmailDto email;

	public FormatDto getHeader() {
		return header;
	}

	public void setHeader(FormatDto header) {
		this.header = header;
	}

	public AbstractDto getData() {
		return data;
	}

	public void setData(AbstractDto data) {
		this.data = data;
	}

	public EmailDto getEmail() {
		return email;
	}

	public void setEmail(EmailDto email) {
		this.email = email;
	}

}
