/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum CreditType {

	_EMPTY_(""), _CREDIT_LINE_("Credit line"), _BANK_GUARANTEE_(
			"Bank guarantee"), _PREPAYMENT_("Prepayment");

	private String name;

	private CreditType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static CreditType getByName(String name) {
		for (CreditType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent CreditType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
