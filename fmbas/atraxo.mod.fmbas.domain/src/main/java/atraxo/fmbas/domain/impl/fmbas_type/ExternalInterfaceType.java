/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ExternalInterfaceType {

	_EMPTY_(""), _INBOUND_("Inbound"), _OUTBOUND_("Outbound");

	private String name;

	private ExternalInterfaceType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ExternalInterfaceType getByName(String name) {
		for (ExternalInterfaceType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent ExternalInterfaceType with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
