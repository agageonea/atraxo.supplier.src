package atraxo.fmbas.domain.ext.platts;

public class PlattsData {
	private String classification;
	private String symbol;
	private String date;
	private String value;
	private String changedDate;

	public String getClassification() {
		return this.classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getSymbol() {
		return this.symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getChangedDate() {
		return this.changedDate;
	}

	public void setChangedDate(String changedDate) {
		this.changedDate = changedDate;
	}

	@Override
	public String toString() {
		return "PlattsData [classification=" + this.classification + ", symbol=" + this.symbol + ", date=" + this.date + ", value=" + this.value
				+ "]";
	}
}