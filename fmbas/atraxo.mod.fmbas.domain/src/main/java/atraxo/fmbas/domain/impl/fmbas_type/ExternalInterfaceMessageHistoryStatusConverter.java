/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ExternalInterfaceMessageHistoryStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class ExternalInterfaceMessageHistoryStatusConverter
		implements
			AttributeConverter<ExternalInterfaceMessageHistoryStatus, String> {

	@Override
	public String convertToDatabaseColumn(
			ExternalInterfaceMessageHistoryStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null ExternalInterfaceMessageHistoryStatus.");
		}
		return value.getName();
	}

	@Override
	public ExternalInterfaceMessageHistoryStatus convertToEntityAttribute(
			String value) {
		return ExternalInterfaceMessageHistoryStatus.getByName(value);
	}

}
