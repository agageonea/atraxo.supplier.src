/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.customer;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.customer.CustomerLocations;
import atraxo.fmbas.domain.impl.customer.CustomerNotification;
import atraxo.fmbas.domain.impl.exposure.Exposure;
import atraxo.fmbas.domain.impl.fmbas_type.BusinessType;
import atraxo.fmbas.domain.impl.fmbas_type.CreditUpdateRequestStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CreditUpdateRequestStatusConverter;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataProcessedStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataProcessedStatusConverter;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataTransmissionStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataTransmissionStatusConverter;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatusConverter;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerType;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerTypeConverter;
import atraxo.fmbas.domain.impl.fmbas_type.NatureOfBusiness;
import atraxo.fmbas.domain.impl.fmbas_type.NatureOfBusinessConverter;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.geo.TimeZone;
import atraxo.fmbas.domain.impl.masteragreements.MasterAgreement;
import atraxo.fmbas.domain.impl.profile.AccountingRules;
import atraxo.fmbas.domain.impl.profile.BankAccount;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Customer} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Customer.NQ_FIND_BY_CODE, query = "SELECT e FROM Customer e WHERE e.clientId = :clientId and e.code = :code", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Customer.NQ_FIND_BY_AREAS, query = "SELECT e FROM Customer e WHERE e.clientId = :clientId and e.assignedArea = :assignedArea and e.isSubsidiary = :isSubsidiary", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Customer.NQ_FIND_BY_AREAS_PRIMITIVE, query = "SELECT e FROM Customer e WHERE e.clientId = :clientId and e.assignedArea.id = :assignedAreaId and e.isSubsidiary = :isSubsidiary", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Customer.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Customer e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Customer.TABLE_NAME, uniqueConstraints = {
		@UniqueConstraint(name = Customer.TABLE_NAME + "_UK1", columnNames = {
				"clientid", "code"}),
		@UniqueConstraint(name = Customer.TABLE_NAME + "_UK2", columnNames = {
				"clientid", "assigned_area", "is_subsidiary"})})
public class Customer extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_COMPANIES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "Customer.findByCode";
	/**
	 * Named query find by unique key: Areas.
	 */
	public static final String NQ_FIND_BY_AREAS = "Customer.findByAreas";
	/**
	 * Named query find by unique key: Areas using the ID field for references.
	 */
	public static final String NQ_FIND_BY_AREAS_PRIMITIVE = "Customer.findByAreas_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Customer.findByBusiness";

	@NotBlank
	@Column(name = "code", nullable = false, length = 32)
	private String code;

	@NotBlank
	@Column(name = "name", nullable = false, length = 100)
	private String name;

	@Column(name = "status", length = 32)
	@Convert(converter = CustomerStatusConverter.class)
	private CustomerStatus status;

	@Column(name = "type", length = 32)
	@Convert(converter = CustomerTypeConverter.class)
	private CustomerType type;

	@Column(name = "phone_number", length = 100)
	private String phoneNo;

	@Column(name = "fax_number", length = 100)
	private String faxNo;

	@Column(name = "website", length = 100)
	private String website;

	@Column(name = "email", length = 250)
	private String email;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_inc")
	private Date dateOfInc;

	@Column(name = "nature_of_business", length = 32)
	@Convert(converter = NatureOfBusinessConverter.class)
	private NatureOfBusiness natureOfBusiness;

	@Column(name = "tl", length = 100)
	private String tradeLicense;

	@Temporal(TemporalType.DATE)
	@Column(name = "tl_valid_from")
	private Date tlValidFrom;

	@Temporal(TemporalType.DATE)
	@Column(name = "tl_valid_to")
	private Date tlValidTo;

	@Column(name = "aoc_number", length = 100)
	private String aoc;

	@Temporal(TemporalType.DATE)
	@Column(name = "aoc_valid_from")
	private Date aocValidFrom;

	@Temporal(TemporalType.DATE)
	@Column(name = "aoc_valid_to")
	private Date aocValidTo;

	@Column(name = "vat_reg_number", length = 32)
	private String vatRegNumber;

	@Column(name = "tax_id_number", length = 32)
	private String taxIdNumber;

	@Column(name = "income_tax_number", length = 32)
	private String incomeTaxNumber;

	@Column(name = "account_number", length = 32)
	private String accountNumber;

	@Column(name = "system")
	private Boolean system;

	@Column(name = "office_street", length = 50)
	private String officeStreet;

	@Column(name = "office_city", length = 100)
	private String officeCity;

	@Column(name = "office_postal_code", length = 32)
	private String officePostalCode;

	@Column(name = "billing_street", length = 50)
	private String billingStreet;

	@Column(name = "billing_city", length = 100)
	private String billingCity;

	@Column(name = "billing_postal_code", length = 32)
	private String billingPostalCode;

	@Column(name = "iata_code", length = 4)
	private String iataCode;

	@Column(name = "icao_code", length = 3)
	private String icaoCode;

	@Column(name = "use_for_billing")
	private Boolean useForBilling;

	@Column(name = "is_customer")
	private Boolean isCustomer;

	@Column(name = "is_supplier")
	private Boolean isSupplier;

	@Column(name = "is_third_party")
	private Boolean isThirdParty;

	@Column(name = "is_subsidiary")
	private Boolean isSubsidiary;

	@Column(name = "bank_name", length = 100)
	private String bankName;

	@Column(name = "bank_address", length = 255)
	private String bankAddress;

	@Column(name = "bank_account_no", length = 100)
	private String bankAccountNo;

	@Column(name = "bank_iban_no", length = 35)
	private String bankIbanNo;

	@Column(name = "bank_swift_no", length = 15)
	private String bankSwiftNo;

	@Column(name = "ipl_agent")
	private Boolean iplAgent;

	@Column(name = "fuel_supplier")
	private Boolean fuelSupplier;

	@Column(name = "fixed_base_operator")
	private Boolean fixedBaseOperator;

	@Column(name = "notes", length = 255)
	private String notes;

	@NotBlank
	@Column(name = "transmission_status", nullable = false, length = 32)
	@Convert(converter = CustomerDataTransmissionStatusConverter.class)
	private CustomerDataTransmissionStatus transmissionStatus;

	@NotBlank
	@Column(name = "processed_status", nullable = false, length = 32)
	@Convert(converter = CustomerDataProcessedStatusConverter.class)
	private CustomerDataProcessedStatus processedStatus;

	@NotBlank
	@Column(name = "credit_update_status", nullable = false, length = 32)
	@Convert(converter = CreditUpdateRequestStatusConverter.class)
	private CreditUpdateRequestStatus creditUpdateRequestStatus;

	@Transient
	private BusinessType business;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "parent_group_id", referencedColumnName = "id")
	private Customer parent;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = UserSupp.class)
	@JoinColumn(name = "responsible_buyer", referencedColumnName = "id")
	private UserSupp responsibleBuyer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Country.class)
	@JoinColumn(name = "office_country", referencedColumnName = "id")
	private Country officeCountry;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Country.class)
	@JoinColumn(name = "office_state", referencedColumnName = "id")
	private Country officeState;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Country.class)
	@JoinColumn(name = "billing_country", referencedColumnName = "id")
	private Country billingCountry;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Country.class)
	@JoinColumn(name = "billing_state", referencedColumnName = "id")
	private Country billingState;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = TimeZone.class)
	@JoinColumn(name = "time_zone", referencedColumnName = "id")
	private TimeZone timeZone;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "subsidiary_currency_id", referencedColumnName = "id")
	private Currencies subsidiaryCurrency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "default_volume_unit_id", referencedColumnName = "id")
	private Unit defaultVolumeUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "default_weight_unit_id", referencedColumnName = "id")
	private Unit defaultWeightUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Area.class)
	@JoinColumn(name = "assigned_area", referencedColumnName = "id")
	private Area assignedArea;

	@ManyToMany(mappedBy = "customers")
	private Collection<BankAccount> bankAccounts;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = AccountingRules.class, mappedBy = "subsidiary", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	private Collection<AccountingRules> accountingRules;

	@ManyToMany
	@JoinTable(name = "BAS_CUSTOMER_LOCATIONS", joinColumns = {@JoinColumn(name = "CUSTOMER_ID")}, inverseJoinColumns = {@JoinColumn(name = "LOCATION_ID")})
	private Collection<CustomerLocations> locations;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = CreditLines.class, mappedBy = "customer", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	private Collection<CreditLines> creditLines;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = CustomerNotification.class, mappedBy = "customer")
	private Collection<CustomerNotification> customerNotification;

	@ManyToMany(cascade = {CascadeType.REMOVE})
	@JoinTable(name = "bas_final_customers", joinColumns = {@JoinColumn(name = "FINAL_CUSTOMER_ID")}, inverseJoinColumns = {@JoinColumn(name = "RESSELER_ID")})
	private Collection<Customer> reseller;

	@ManyToMany(mappedBy = "reseller")
	private Collection<Customer> finalCustomer;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = Exposure.class, mappedBy = "customer", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	private Collection<Exposure> exposure;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = MasterAgreement.class, mappedBy = "company", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	private Collection<MasterAgreement> masterAgreement;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = Aircraft.class, mappedBy = "customer", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<Aircraft> aircraft;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CustomerStatus getStatus() {
		return this.status;
	}

	public void setStatus(CustomerStatus status) {
		this.status = status;
	}

	public CustomerType getType() {
		return this.type;
	}

	public void setType(CustomerType type) {
		this.type = type;
	}

	public String getPhoneNo() {
		return this.phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getFaxNo() {
		return this.faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDateOfInc() {
		return this.dateOfInc;
	}

	public void setDateOfInc(Date dateOfInc) {
		this.dateOfInc = dateOfInc;
	}

	public NatureOfBusiness getNatureOfBusiness() {
		return this.natureOfBusiness;
	}

	public void setNatureOfBusiness(NatureOfBusiness natureOfBusiness) {
		this.natureOfBusiness = natureOfBusiness;
	}

	public String getTradeLicense() {
		return this.tradeLicense;
	}

	public void setTradeLicense(String tradeLicense) {
		this.tradeLicense = tradeLicense;
	}

	public Date getTlValidFrom() {
		return this.tlValidFrom;
	}

	public void setTlValidFrom(Date tlValidFrom) {
		this.tlValidFrom = tlValidFrom;
	}

	public Date getTlValidTo() {
		return this.tlValidTo;
	}

	public void setTlValidTo(Date tlValidTo) {
		this.tlValidTo = tlValidTo;
	}

	public String getAoc() {
		return this.aoc;
	}

	public void setAoc(String aoc) {
		this.aoc = aoc;
	}

	public Date getAocValidFrom() {
		return this.aocValidFrom;
	}

	public void setAocValidFrom(Date aocValidFrom) {
		this.aocValidFrom = aocValidFrom;
	}

	public Date getAocValidTo() {
		return this.aocValidTo;
	}

	public void setAocValidTo(Date aocValidTo) {
		this.aocValidTo = aocValidTo;
	}

	public String getVatRegNumber() {
		return this.vatRegNumber;
	}

	public void setVatRegNumber(String vatRegNumber) {
		this.vatRegNumber = vatRegNumber;
	}

	public String getTaxIdNumber() {
		return this.taxIdNumber;
	}

	public void setTaxIdNumber(String taxIdNumber) {
		this.taxIdNumber = taxIdNumber;
	}

	public String getIncomeTaxNumber() {
		return this.incomeTaxNumber;
	}

	public void setIncomeTaxNumber(String incomeTaxNumber) {
		this.incomeTaxNumber = incomeTaxNumber;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Boolean getSystem() {
		return this.system;
	}

	public void setSystem(Boolean system) {
		this.system = system;
	}

	public String getOfficeStreet() {
		return this.officeStreet;
	}

	public void setOfficeStreet(String officeStreet) {
		this.officeStreet = officeStreet;
	}

	public String getOfficeCity() {
		return this.officeCity;
	}

	public void setOfficeCity(String officeCity) {
		this.officeCity = officeCity;
	}

	public String getOfficePostalCode() {
		return this.officePostalCode;
	}

	public void setOfficePostalCode(String officePostalCode) {
		this.officePostalCode = officePostalCode;
	}

	public String getBillingStreet() {
		return this.billingStreet;
	}

	public void setBillingStreet(String billingStreet) {
		this.billingStreet = billingStreet;
	}

	public String getBillingCity() {
		return this.billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public String getBillingPostalCode() {
		return this.billingPostalCode;
	}

	public void setBillingPostalCode(String billingPostalCode) {
		this.billingPostalCode = billingPostalCode;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public String getIcaoCode() {
		return this.icaoCode;
	}

	public void setIcaoCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}

	public Boolean getUseForBilling() {
		return this.useForBilling;
	}

	public void setUseForBilling(Boolean useForBilling) {
		this.useForBilling = useForBilling;
	}

	public Boolean getIsCustomer() {
		return this.isCustomer;
	}

	public void setIsCustomer(Boolean isCustomer) {
		this.isCustomer = isCustomer;
	}

	public Boolean getIsSupplier() {
		return this.isSupplier;
	}

	public void setIsSupplier(Boolean isSupplier) {
		this.isSupplier = isSupplier;
	}

	public Boolean getIsThirdParty() {
		return this.isThirdParty;
	}

	public void setIsThirdParty(Boolean isThirdParty) {
		this.isThirdParty = isThirdParty;
	}

	public Boolean getIsSubsidiary() {
		return this.isSubsidiary;
	}

	public void setIsSubsidiary(Boolean isSubsidiary) {
		this.isSubsidiary = isSubsidiary;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAddress() {
		return this.bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public String getBankAccountNo() {
		return this.bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getBankIbanNo() {
		return this.bankIbanNo;
	}

	public void setBankIbanNo(String bankIbanNo) {
		this.bankIbanNo = bankIbanNo;
	}

	public String getBankSwiftNo() {
		return this.bankSwiftNo;
	}

	public void setBankSwiftNo(String bankSwiftNo) {
		this.bankSwiftNo = bankSwiftNo;
	}

	public Boolean getIplAgent() {
		return this.iplAgent;
	}

	public void setIplAgent(Boolean iplAgent) {
		this.iplAgent = iplAgent;
	}

	public Boolean getFuelSupplier() {
		return this.fuelSupplier;
	}

	public void setFuelSupplier(Boolean fuelSupplier) {
		this.fuelSupplier = fuelSupplier;
	}

	public Boolean getFixedBaseOperator() {
		return this.fixedBaseOperator;
	}

	public void setFixedBaseOperator(Boolean fixedBaseOperator) {
		this.fixedBaseOperator = fixedBaseOperator;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public CustomerDataTransmissionStatus getTransmissionStatus() {
		return this.transmissionStatus;
	}

	public void setTransmissionStatus(
			CustomerDataTransmissionStatus transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public CustomerDataProcessedStatus getProcessedStatus() {
		return this.processedStatus;
	}

	public void setProcessedStatus(CustomerDataProcessedStatus processedStatus) {
		this.processedStatus = processedStatus;
	}

	public CreditUpdateRequestStatus getCreditUpdateRequestStatus() {
		return this.creditUpdateRequestStatus;
	}

	public void setCreditUpdateRequestStatus(
			CreditUpdateRequestStatus creditUpdateRequestStatus) {
		this.creditUpdateRequestStatus = creditUpdateRequestStatus;
	}

	public BusinessType getBusiness() {
		return this.business;
	}

	public void setBusiness(BusinessType business) {
		this.business = business;
	}

	public Customer getParent() {
		return this.parent;
	}

	public void setParent(Customer parent) {
		if (parent != null) {
			this.__validate_client_context__(parent.getClientId());
		}
		this.parent = parent;
	}
	public UserSupp getResponsibleBuyer() {
		return this.responsibleBuyer;
	}

	public void setResponsibleBuyer(UserSupp responsibleBuyer) {
		if (responsibleBuyer != null) {
			this.__validate_client_context__(responsibleBuyer.getClientId());
		}
		this.responsibleBuyer = responsibleBuyer;
	}
	public Country getOfficeCountry() {
		return this.officeCountry;
	}

	public void setOfficeCountry(Country officeCountry) {
		if (officeCountry != null) {
			this.__validate_client_context__(officeCountry.getClientId());
		}
		this.officeCountry = officeCountry;
	}
	public Country getOfficeState() {
		return this.officeState;
	}

	public void setOfficeState(Country officeState) {
		if (officeState != null) {
			this.__validate_client_context__(officeState.getClientId());
		}
		this.officeState = officeState;
	}
	public Country getBillingCountry() {
		return this.billingCountry;
	}

	public void setBillingCountry(Country billingCountry) {
		if (billingCountry != null) {
			this.__validate_client_context__(billingCountry.getClientId());
		}
		this.billingCountry = billingCountry;
	}
	public Country getBillingState() {
		return this.billingState;
	}

	public void setBillingState(Country billingState) {
		if (billingState != null) {
			this.__validate_client_context__(billingState.getClientId());
		}
		this.billingState = billingState;
	}
	public TimeZone getTimeZone() {
		return this.timeZone;
	}

	public void setTimeZone(TimeZone timeZone) {
		if (timeZone != null) {
			this.__validate_client_context__(timeZone.getClientId());
		}
		this.timeZone = timeZone;
	}
	public Currencies getSubsidiaryCurrency() {
		return this.subsidiaryCurrency;
	}

	public void setSubsidiaryCurrency(Currencies subsidiaryCurrency) {
		if (subsidiaryCurrency != null) {
			this.__validate_client_context__(subsidiaryCurrency.getClientId());
		}
		this.subsidiaryCurrency = subsidiaryCurrency;
	}
	public Unit getDefaultVolumeUnit() {
		return this.defaultVolumeUnit;
	}

	public void setDefaultVolumeUnit(Unit defaultVolumeUnit) {
		if (defaultVolumeUnit != null) {
			this.__validate_client_context__(defaultVolumeUnit.getClientId());
		}
		this.defaultVolumeUnit = defaultVolumeUnit;
	}
	public Unit getDefaultWeightUnit() {
		return this.defaultWeightUnit;
	}

	public void setDefaultWeightUnit(Unit defaultWeightUnit) {
		if (defaultWeightUnit != null) {
			this.__validate_client_context__(defaultWeightUnit.getClientId());
		}
		this.defaultWeightUnit = defaultWeightUnit;
	}
	public Area getAssignedArea() {
		return this.assignedArea;
	}

	public void setAssignedArea(Area assignedArea) {
		if (assignedArea != null) {
			this.__validate_client_context__(assignedArea.getClientId());
		}
		this.assignedArea = assignedArea;
	}

	public Collection<BankAccount> getBankAccounts() {
		return this.bankAccounts;
	}

	public void setBankAccounts(Collection<BankAccount> bankAccounts) {
		this.bankAccounts = bankAccounts;
	}

	public Collection<AccountingRules> getAccountingRules() {
		return this.accountingRules;
	}

	public void setAccountingRules(Collection<AccountingRules> accountingRules) {
		this.accountingRules = accountingRules;
	}

	/**
	 * @param e
	 */
	public void addToAccountingRules(AccountingRules e) {
		if (this.accountingRules == null) {
			this.accountingRules = new ArrayList<>();
		}
		e.setSubsidiary(this);
		this.accountingRules.add(e);
	}
	public Collection<CustomerLocations> getLocations() {
		return this.locations;
	}

	public void setLocations(Collection<CustomerLocations> locations) {
		this.locations = locations;
	}

	public Collection<CreditLines> getCreditLines() {
		return this.creditLines;
	}

	public void setCreditLines(Collection<CreditLines> creditLines) {
		this.creditLines = creditLines;
	}

	/**
	 * @param e
	 */
	public void addToCreditLines(CreditLines e) {
		if (this.creditLines == null) {
			this.creditLines = new ArrayList<>();
		}
		e.setCustomer(this);
		this.creditLines.add(e);
	}
	public Collection<CustomerNotification> getCustomerNotification() {
		return this.customerNotification;
	}

	public void setCustomerNotification(
			Collection<CustomerNotification> customerNotification) {
		this.customerNotification = customerNotification;
	}

	/**
	 * @param e
	 */
	public void addToCustomerNotification(CustomerNotification e) {
		if (this.customerNotification == null) {
			this.customerNotification = new ArrayList<>();
		}
		e.setCustomer(this);
		this.customerNotification.add(e);
	}
	public Collection<Customer> getReseller() {
		return this.reseller;
	}

	public void setReseller(Collection<Customer> reseller) {
		this.reseller = reseller;
	}

	public Collection<Customer> getFinalCustomer() {
		return this.finalCustomer;
	}

	public void setFinalCustomer(Collection<Customer> finalCustomer) {
		this.finalCustomer = finalCustomer;
	}

	public Collection<Exposure> getExposure() {
		return this.exposure;
	}

	public void setExposure(Collection<Exposure> exposure) {
		this.exposure = exposure;
	}

	/**
	 * @param e
	 */
	public void addToExposure(Exposure e) {
		if (this.exposure == null) {
			this.exposure = new ArrayList<>();
		}
		e.setCustomer(this);
		this.exposure.add(e);
	}
	public Collection<MasterAgreement> getMasterAgreement() {
		return this.masterAgreement;
	}

	public void setMasterAgreement(Collection<MasterAgreement> masterAgreement) {
		this.masterAgreement = masterAgreement;
	}

	/**
	 * @param e
	 */
	public void addToMasterAgreement(MasterAgreement e) {
		if (this.masterAgreement == null) {
			this.masterAgreement = new ArrayList<>();
		}
		e.setCompany(this);
		this.masterAgreement.add(e);
	}
	public Collection<Aircraft> getAircraft() {
		return this.aircraft;
	}

	public void setAircraft(Collection<Aircraft> aircraft) {
		this.aircraft = aircraft;
	}

	/**
	 * @param e
	 */
	public void addToAircraft(Aircraft e) {
		if (this.aircraft == null) {
			this.aircraft = new ArrayList<>();
		}
		e.setCustomer(this);
		this.aircraft.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.code == null) ? 0 : this.code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Customer other = (Customer) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.code == null) {
			if (other.code != null) {
				return false;
			}
		} else if (!this.code.equals(other.code)) {
			return false;
		}
		return true;
	}
}
