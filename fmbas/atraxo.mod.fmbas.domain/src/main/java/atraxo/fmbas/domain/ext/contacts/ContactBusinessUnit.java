package atraxo.fmbas.domain.ext.contacts;

public enum ContactBusinessUnit {
	OPERATION("Operation"), COMERCIAL("Comercial");

	private String displayName;

	private ContactBusinessUnit(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public ContactBusinessUnit getByDisplayName(String displayName) {
		for (ContactBusinessUnit contactBusinessUnit : values()) {
			if (contactBusinessUnit.getDisplayName().equalsIgnoreCase(displayName)) {
				return contactBusinessUnit;
			}
		}
		throw new RuntimeException("Inexistent business unit with name: " + displayName);
	}

}
