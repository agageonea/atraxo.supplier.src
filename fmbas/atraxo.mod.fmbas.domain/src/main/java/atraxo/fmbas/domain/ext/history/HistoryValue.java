/**
 *
 */
package atraxo.fmbas.domain.ext.history;

import java.util.List;

/**
 * @author zspeter
 */
public class HistoryValue {

	private String code;
	private List<Object> arguments;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<Object> getArguments() {
		return this.arguments;
	}

	public void setArguments(List<Object> arguments) {
		this.arguments = arguments;
	}
}
