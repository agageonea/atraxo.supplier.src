package atraxo.fmbas.domain.ext.mailmerge.dto;

import java.io.Serializable;

public class EmailAddressesDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4661873067208525173L;

	private String title;
	private String name;
	private String address;
	private AbstractDto data;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AbstractDto getData() {
		return this.data;
	}

	public void setData(AbstractDto data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "EmailAddressesDto [title=" + this.title + ", name=" + this.name + ", address=" + this.address + ", data=" + this.data + "]";
	}

}
