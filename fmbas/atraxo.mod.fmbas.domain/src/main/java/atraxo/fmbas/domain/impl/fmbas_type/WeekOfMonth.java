/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum WeekOfMonth {

	_EMPTY_(""), _FIRST_("First"), _SECOND_("Second"), _THIRD_("Third"), _FOURTH_(
			"Fourth"), _LAST_("Last");

	private String name;

	private WeekOfMonth(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static WeekOfMonth getByName(String name) {
		for (WeekOfMonth status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent WeekOfMonth with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
