/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.dashboard;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;
import atraxo.fmbas.domain.impl.dashboard.Layouts;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Dashboards} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = Dashboards.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Dashboards e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Dashboards.TABLE_NAME)
public class Dashboards extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_DASHBOARDS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Dashboards.findByBusiness";

	@NotBlank
	@Column(name = "name", nullable = false, length = 100)
	private String name;

	@NotBlank
	@Column(name = "description", nullable = false, length = 1000)
	private String description;

	@NotNull
	@Column(name = "is_default", nullable = false)
	private Boolean isDefault;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Layouts.class)
	@JoinColumn(name = "layout_id", referencedColumnName = "id")
	private Layouts layout;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = DashboardWidgets.class, mappedBy = "dashboard")
	private Collection<DashboardWidgets> dashboardWidgets;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsDefault() {
		return this.isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Layouts getLayout() {
		return this.layout;
	}

	public void setLayout(Layouts layout) {
		if (layout != null) {
			this.__validate_client_context__(layout.getClientId());
		}
		this.layout = layout;
	}

	public Collection<DashboardWidgets> getDashboardWidgets() {
		return this.dashboardWidgets;
	}

	public void setDashboardWidgets(
			Collection<DashboardWidgets> dashboardWidgets) {
		this.dashboardWidgets = dashboardWidgets;
	}

	/**
	 * @param e
	 */
	public void addToDashboardWidgets(DashboardWidgets e) {
		if (this.dashboardWidgets == null) {
			this.dashboardWidgets = new ArrayList<>();
		}
		e.setDashboard(this);
		this.dashboardWidgets.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.isDefault == null) {
			this.isDefault = new Boolean(false);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
