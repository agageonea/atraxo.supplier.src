/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link DictionaryOperator} enum.
 * Generated code. Do not modify in this file.
 */
public class DictionaryOperatorConverter
		implements
			AttributeConverter<DictionaryOperator, String> {

	@Override
	public String convertToDatabaseColumn(DictionaryOperator value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null DictionaryOperator.");
		}
		return value.getName();
	}

	@Override
	public DictionaryOperator convertToEntityAttribute(String value) {
		return DictionaryOperator.getByName(value);
	}

}
