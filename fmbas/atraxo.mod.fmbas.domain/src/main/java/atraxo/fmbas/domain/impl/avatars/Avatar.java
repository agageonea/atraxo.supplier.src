/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.avatars;

import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.ad.domain.impl.security.User;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntityCharId;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Avatar} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = Avatar.NQ_FIND_BY_ID, query = "SELECT e FROM Avatar e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Avatar.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Avatar.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "id"})})
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class Avatar extends AbstractEntityCharId implements Serializable {

	public static final String TABLE_NAME = "BAS_AVATARS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Id.
	 */
	public static final String NQ_FIND_BY_ID = "Avatar.findById";

	@NotBlank
	@Column(name = "name", nullable = false, length = 255)
	private String name;

	@NotBlank
	@Column(name = "filename", nullable = false, length = 255)
	private String fileName;

	@Column(name = "location", length = 400)
	private String location;

	@NotBlank
	@Column(name = "contenttype", nullable = false, length = 32)
	private String contentType;

	@NotNull
	@Column(name = "active", nullable = false)
	private Boolean active;

	@Transient
	private String url;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AttachmentType.class)
	@JoinColumn(name = "type_id", referencedColumnName = "id")
	private AttachmentType type;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getUrl() {
		return this.type.getBaseUrl() + "/" + this.getId() + "."
				+ this.contentType;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		if (user != null) {
			this.__validate_client_context__(user.getClientId());
		}
		this.user = user;
	}
	public AttachmentType getType() {
		return this.type;
	}

	public void setType(AttachmentType type) {
		if (type != null) {
			this.__validate_client_context__(type.getClientId());
		}
		this.type = type;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.active == null) {
			this.active = new Boolean(true);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
