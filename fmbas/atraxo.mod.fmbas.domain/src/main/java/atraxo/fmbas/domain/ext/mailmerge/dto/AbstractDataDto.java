package atraxo.fmbas.domain.ext.mailmerge.dto;

/**
 * @author mbotorogea
 */
public abstract class AbstractDataDto extends AbstractDto {

	private static final long serialVersionUID = 552652249433273852L;

	/**
	 * Sets default data values for null or empty properties
	 */
	public abstract void setDefaultValues();
}
