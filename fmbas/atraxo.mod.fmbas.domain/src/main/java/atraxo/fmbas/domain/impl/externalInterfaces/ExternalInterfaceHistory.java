/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.externalInterfaces;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfacesHistoryStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfacesHistoryStatusConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link ExternalInterfaceHistory} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = ExternalInterfaceHistory.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ExternalInterfaceHistory e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ExternalInterfaceHistory.NQ_IDX_FIND_BY_MESSAGE_ID, query = "SELECT e FROM ExternalInterfaceHistory e WHERE e.clientId = :clientId and e.msgId = :msgId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ExternalInterfaceHistory.TABLE_NAME)
public class ExternalInterfaceHistory extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "BAS_EXTERNAL_INTERFACES_HISTORY";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ExternalInterfaceHistory.findByBusiness";
	/**
	 * Named query find by index key: Message_id.
	 */
	public static final String NQ_IDX_FIND_BY_MESSAGE_ID = "ExternalInterfaceHistory.findByIdxMessage_id";

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "request_received", nullable = false)
	private Date requestReceived;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "response_sent", nullable = false)
	private Date responseSent;

	@NotBlank
	@Column(name = "status", nullable = false, length = 32)
	@Convert(converter = ExternalInterfacesHistoryStatusConverter.class)
	private ExternalInterfacesHistoryStatus status;

	@Column(name = "requester", length = 1000)
	private String requester;

	@Column(name = "result", length = 4000)
	private String result;

	@Column(name = "message_id", length = 100)
	private String msgId;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ExternalInterface.class)
	@JoinColumn(name = "interface_id", referencedColumnName = "id")
	private ExternalInterface externalInterface;

	public Date getRequestReceived() {
		return this.requestReceived;
	}

	public void setRequestReceived(Date requestReceived) {
		this.requestReceived = requestReceived;
	}

	public Date getResponseSent() {
		return this.responseSent;
	}

	public void setResponseSent(Date responseSent) {
		this.responseSent = responseSent;
	}

	public ExternalInterfacesHistoryStatus getStatus() {
		return this.status;
	}

	public void setStatus(ExternalInterfacesHistoryStatus status) {
		this.status = status;
	}

	public String getRequester() {
		return this.requester;
	}

	public void setRequester(String requester) {
		this.requester = requester;
	}

	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMsgId() {
		return this.msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public ExternalInterface getExternalInterface() {
		return this.externalInterface;
	}

	public void setExternalInterface(ExternalInterface externalInterface) {
		if (externalInterface != null) {
			this.__validate_client_context__(externalInterface.getClientId());
		}
		this.externalInterface = externalInterface;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
