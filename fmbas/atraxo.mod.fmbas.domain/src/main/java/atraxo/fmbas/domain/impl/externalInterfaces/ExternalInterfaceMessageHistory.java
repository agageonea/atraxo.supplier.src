/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.externalInterfaces;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatusConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link ExternalInterfaceMessageHistory} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = ExternalInterfaceMessageHistory.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ExternalInterfaceMessageHistory e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ExternalInterfaceMessageHistory.NQ_IDX_FIND_BY_MESSAGE_ID, query = "SELECT e FROM ExternalInterfaceMessageHistory e WHERE e.clientId = :clientId and e.messageId = :messageId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ExternalInterfaceMessageHistory.TABLE_NAME)
public class ExternalInterfaceMessageHistory extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "BAS_EXTERNAL_INTERF_MESSAGE_HIST";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ExternalInterfaceMessageHistory.findByBusiness";
	/**
	 * Named query find by index key: Message_id.
	 */
	public static final String NQ_IDX_FIND_BY_MESSAGE_ID = "ExternalInterfaceMessageHistory.findByIdxMessage_id";

	@NotNull
	@Column(name = "object_id", nullable = false, precision = 11)
	private Integer objectId;

	@NotBlank
	@Column(name = "object_type", nullable = false, length = 100)
	private String objectType;

	@NotBlank
	@Column(name = "object_ref_id", nullable = false, length = 100)
	private String objectRefId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "response_time")
	private Date responseTime;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "execution_time", nullable = false)
	private Date executionTime;

	@NotBlank
	@Column(name = "message_id", nullable = false, length = 100)
	private String messageId;

	@Column(name = "response_message_id", length = 100)
	private String responseMessageId;

	@NotBlank
	@Column(name = "status", nullable = false, length = 32)
	@Convert(converter = ExternalInterfaceMessageHistoryStatusConverter.class)
	private ExternalInterfaceMessageHistoryStatus status;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ExternalInterface.class)
	@JoinColumn(name = "interface_id", referencedColumnName = "id")
	private ExternalInterface externalInterface;

	public Integer getObjectId() {
		return this.objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getObjectRefId() {
		return this.objectRefId;
	}

	public void setObjectRefId(String objectRefId) {
		this.objectRefId = objectRefId;
	}

	public Date getResponseTime() {
		return this.responseTime;
	}

	public void setResponseTime(Date responseTime) {
		this.responseTime = responseTime;
	}

	public Date getExecutionTime() {
		return this.executionTime;
	}

	public void setExecutionTime(Date executionTime) {
		this.executionTime = executionTime;
	}

	public String getMessageId() {
		return this.messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getResponseMessageId() {
		return this.responseMessageId;
	}

	public void setResponseMessageId(String responseMessageId) {
		this.responseMessageId = responseMessageId;
	}

	public ExternalInterfaceMessageHistoryStatus getStatus() {
		return this.status;
	}

	public void setStatus(ExternalInterfaceMessageHistoryStatus status) {
		this.status = status;
	}

	public ExternalInterface getExternalInterface() {
		return this.externalInterface;
	}

	public void setExternalInterface(ExternalInterface externalInterface) {
		if (externalInterface != null) {
			this.__validate_client_context__(externalInterface.getClientId());
		}
		this.externalInterface = externalInterface;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
