/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum WidgetType {

	_EMPTY_(""), _KPI_("KPI"), _CHART_("Chart"), _LIST_("List");

	private String name;

	private WidgetType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static WidgetType getByName(String name) {
		for (WidgetType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent WidgetType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
