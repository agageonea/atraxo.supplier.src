/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.externalInterfaces;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.dictionary.Dictionary;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceParameter;
import atraxo.fmbas.domain.impl.externalInterfaces.InterfaceUser;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceType;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceTypeConverter;
import atraxo.fmbas.domain.impl.fmbas_type.LastRequestStatus;
import atraxo.fmbas.domain.impl.fmbas_type.LastRequestStatusConverter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link ExternalInterface} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = ExternalInterface.NQ_FIND_BY_NAME, query = "SELECT e FROM ExternalInterface e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = ExternalInterface.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ExternalInterface e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ExternalInterface.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = ExternalInterface.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "name"})})
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class ExternalInterface extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_EXTERNAL_INTERFACES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "ExternalInterface.findByName";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ExternalInterface.findByBusiness";

	@NotBlank
	@Column(name = "name", nullable = false, length = 100)
	private String name;

	@Column(name = "description", length = 1000)
	private String description;

	@NotNull
	@Column(name = "enabled", nullable = false)
	private Boolean enabled;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_request")
	private Date lastRequest;

	@Column(name = "last_request_status", length = 32)
	@Convert(converter = LastRequestStatusConverter.class)
	private LastRequestStatus lastRequestStatus;

	@Column(name = "type", length = 64)
	@Convert(converter = ExternalInterfaceTypeConverter.class)
	private ExternalInterfaceType externalInterfaceType;

	@Transient
	private Boolean async;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = InterfaceUser.class, mappedBy = "externalInterface", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<InterfaceUser> users;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = ExternalInterfaceParameter.class, mappedBy = "externalInterface", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	private Collection<ExternalInterfaceParameter> params;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = Dictionary.class, mappedBy = "externalInterface", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<Dictionary> dictionaries;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Date getLastRequest() {
		return this.lastRequest;
	}

	public void setLastRequest(Date lastRequest) {
		this.lastRequest = lastRequest;
	}

	public LastRequestStatus getLastRequestStatus() {
		return this.lastRequestStatus;
	}

	public void setLastRequestStatus(LastRequestStatus lastRequestStatus) {
		this.lastRequestStatus = lastRequestStatus;
	}

	public ExternalInterfaceType getExternalInterfaceType() {
		return this.externalInterfaceType;
	}

	public void setExternalInterfaceType(
			ExternalInterfaceType externalInterfaceType) {
		this.externalInterfaceType = externalInterfaceType;
	}

	public Boolean getAsync() {
		return this.name.toLowerCase().contains("ack") ? true : false;
	}

	public void setAsync(Boolean async) {
		this.async = async;
	}

	public Collection<InterfaceUser> getUsers() {
		return this.users;
	}

	public void setUsers(Collection<InterfaceUser> users) {
		this.users = users;
	}

	/**
	 * @param e
	 */
	public void addToUsers(InterfaceUser e) {
		if (this.users == null) {
			this.users = new ArrayList<>();
		}
		e.setExternalInterface(this);
		this.users.add(e);
	}
	public Collection<ExternalInterfaceParameter> getParams() {
		return this.params;
	}

	public void setParams(Collection<ExternalInterfaceParameter> params) {
		this.params = params;
	}

	/**
	 * @param e
	 */
	public void addToParams(ExternalInterfaceParameter e) {
		if (this.params == null) {
			this.params = new ArrayList<>();
		}
		e.setExternalInterface(this);
		this.params.add(e);
	}
	public Collection<Dictionary> getDictionaries() {
		return this.dictionaries;
	}

	public void setDictionaries(Collection<Dictionary> dictionaries) {
		this.dictionaries = dictionaries;
	}

	/**
	 * @param e
	 */
	public void addToDictionaries(Dictionary e) {
		if (this.dictionaries == null) {
			this.dictionaries = new ArrayList<>();
		}
		e.setExternalInterface(this);
		this.dictionaries.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.enabled == null) {
			this.enabled = new Boolean(true);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
