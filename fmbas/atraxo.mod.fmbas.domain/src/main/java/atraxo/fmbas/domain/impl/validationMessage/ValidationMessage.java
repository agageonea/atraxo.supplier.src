/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.validationMessage;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.ValidationMessageSeverity;
import atraxo.fmbas.domain.impl.fmbas_type.ValidationMessageSeverityConverter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link ValidationMessage} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = ValidationMessage.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ValidationMessage e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ValidationMessage.TABLE_NAME)
public class ValidationMessage extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_VALIDATION_MESSAGES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ValidationMessage.findByBusiness";

	@NotBlank
	@Column(name = "object_id", nullable = false, length = 64)
	private String objectId;

	@NotBlank
	@Column(name = "object_type", nullable = false, length = 32)
	private String objectType;

	@NotBlank
	@Column(name = "message", nullable = false, length = 2000)
	private String message;

	@Column(name = "severity", length = 32)
	@Convert(converter = ValidationMessageSeverityConverter.class)
	private ValidationMessageSeverity severity;

	public String getObjectId() {
		return this.objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ValidationMessageSeverity getSeverity() {
		return this.severity;
	}

	public void setSeverity(ValidationMessageSeverity severity) {
		this.severity = severity;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
