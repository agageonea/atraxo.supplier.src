/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.vat;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.vat.VatRate;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Vat} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Vat.NQ_FIND_BY_CODE, query = "SELECT e FROM Vat e WHERE e.clientId = :clientId and e.code = :code and e.country = :country", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Vat.NQ_FIND_BY_CODE_PRIMITIVE, query = "SELECT e FROM Vat e WHERE e.clientId = :clientId and e.code = :code and e.country.id = :countryId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Vat.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Vat e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Vat.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Vat.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "code", "country_id"})})
public class Vat extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_VAT";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "Vat.findByCode";
	/**
	 * Named query find by unique key: Code using the ID field for references.
	 */
	public static final String NQ_FIND_BY_CODE_PRIMITIVE = "Vat.findByCode_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Vat.findByBusiness";

	@NotBlank
	@Column(name = "code", nullable = false, length = 32)
	private String code;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Country.class)
	@JoinColumn(name = "country_id", referencedColumnName = "id")
	private Country country;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = VatRate.class, mappedBy = "vat", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	private Collection<VatRate> vatRate;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		if (country != null) {
			this.__validate_client_context__(country.getClientId());
		}
		this.country = country;
	}

	public Collection<VatRate> getVatRate() {
		return this.vatRate;
	}

	public void setVatRate(Collection<VatRate> vatRate) {
		this.vatRate = vatRate;
	}

	/**
	 * @param e
	 */
	public void addToVatRate(VatRate e) {
		if (this.vatRate == null) {
			this.vatRate = new ArrayList<>();
		}
		e.setVat(this);
		this.vatRate.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
