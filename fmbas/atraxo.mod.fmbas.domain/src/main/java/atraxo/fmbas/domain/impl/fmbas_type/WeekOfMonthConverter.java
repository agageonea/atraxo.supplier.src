/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link WeekOfMonth} enum.
 * Generated code. Do not modify in this file.
 */
public class WeekOfMonthConverter
		implements
			AttributeConverter<WeekOfMonth, String> {

	@Override
	public String convertToDatabaseColumn(WeekOfMonth value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null WeekOfMonth.");
		}
		return value.getName();
	}

	@Override
	public WeekOfMonth convertToEntityAttribute(String value) {
		return WeekOfMonth.getByName(value);
	}

}
