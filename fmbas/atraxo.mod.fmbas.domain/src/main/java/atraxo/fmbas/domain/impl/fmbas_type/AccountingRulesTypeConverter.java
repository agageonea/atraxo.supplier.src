/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link AccountingRulesType} enum.
 * Generated code. Do not modify in this file.
 */
public class AccountingRulesTypeConverter
		implements
			AttributeConverter<AccountingRulesType, String> {

	@Override
	public String convertToDatabaseColumn(AccountingRulesType value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null AccountingRulesType.");
		}
		return value.getName();
	}

	@Override
	public AccountingRulesType convertToEntityAttribute(String value) {
		return AccountingRulesType.getByName(value);
	}

}
