/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum Continents {

	_EMPTY_(""), _AFRICA_("Africa"), _ANTARCTICA_("Antarctica"), _ASIA_("Asia"), _OCEANIA_(
			"Oceania"), _EUROPE_("Europe"), _NORTH_AMERICA_("North America"), _SOUTH_AMERICA_(
			"South America");

	private String name;

	private Continents(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static Continents getByName(String name) {
		for (Continents status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Continents with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
