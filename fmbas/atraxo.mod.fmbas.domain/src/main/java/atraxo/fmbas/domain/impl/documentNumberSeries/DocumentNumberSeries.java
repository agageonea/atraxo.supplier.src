/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.documentNumberSeries;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.DocumentNumberSeriesType;
import atraxo.fmbas.domain.impl.fmbas_type.DocumentNumberSeriesTypeConverter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link DocumentNumberSeries} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = DocumentNumberSeries.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM DocumentNumberSeries e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = DocumentNumberSeries.TABLE_NAME)
public class DocumentNumberSeries extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "BAS_DOCUMENT_NUMBER_SERIES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "DocumentNumberSeries.findByBusiness";

	@Column(name = "prefix", length = 64)
	private String prefix;

	@Column(name = "start_index", precision = 11)
	private Integer startIndex;

	@Column(name = "end_index", precision = 11)
	private Integer endIndex;

	@Column(name = "current_index", precision = 11)
	private Integer currentIndex;

	@Column(name = "enabled")
	private Boolean enabled;

	@Column(name = "restart")
	private Boolean restart;

	@Column(name = "type", length = 32)
	@Convert(converter = DocumentNumberSeriesTypeConverter.class)
	private DocumentNumberSeriesType type;

	public String getPrefix() {
		return this.prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public Integer getStartIndex() {
		return this.startIndex;
	}

	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
	}

	public Integer getEndIndex() {
		return this.endIndex;
	}

	public void setEndIndex(Integer endIndex) {
		this.endIndex = endIndex;
	}

	public Integer getCurrentIndex() {
		return this.currentIndex;
	}

	public void setCurrentIndex(Integer currentIndex) {
		this.currentIndex = currentIndex;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getRestart() {
		return this.restart;
	}

	public void setRestart(Boolean restart) {
		this.restart = restart;
	}

	public DocumentNumberSeriesType getType() {
		return this.type;
	}

	public void setType(DocumentNumberSeriesType type) {
		this.type = type;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
