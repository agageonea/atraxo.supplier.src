/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.job;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.job.BatchJobParameter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.config.CacheIsolationType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link BatchJob} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = BatchJob.NQ_FIND_BY_NAME, query = "SELECT e FROM BatchJob e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = BatchJob.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM BatchJob e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = BatchJob.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = BatchJob.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "name"})})
@Cache(coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS, isolation = CacheIsolationType.SHARED, type = CacheType.SOFT, size = 1000, expiry = 36000000)
public class BatchJob extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_BATCH_JOBS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "BatchJob.findByName";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "BatchJob.findByBusiness";

	@NotBlank
	@Column(name = "name", nullable = false, length = 32)
	private String name;

	@Column(name = "description", length = 255)
	private String description;

	@NotBlank
	@Column(name = "bean_name", nullable = false, length = 32)
	private String beanName;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = BatchJobParameter.class, mappedBy = "batchJob", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<BatchJobParameter> jobParameters;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBeanName() {
		return this.beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	public Collection<BatchJobParameter> getJobParameters() {
		return this.jobParameters;
	}

	public void setJobParameters(Collection<BatchJobParameter> jobParameters) {
		this.jobParameters = jobParameters;
	}

	/**
	 * @param e
	 */
	public void addToJobParameters(BatchJobParameter e) {
		if (this.jobParameters == null) {
			this.jobParameters = new ArrayList<>();
		}
		e.setBatchJob(this);
		this.jobParameters.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
