/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.externalInterfaces;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.fmbas_type.InterfaceNotificationCondition;
import atraxo.fmbas.domain.impl.fmbas_type.InterfaceNotificationConditionConverter;
import atraxo.fmbas.domain.impl.fmbas_type.NotificationType;
import atraxo.fmbas.domain.impl.fmbas_type.NotificationTypeConverter;
import atraxo.fmbas.domain.impl.templates.Template;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link InterfaceUser} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = InterfaceUser.NQ_FIND_BY_KEY, query = "SELECT e FROM InterfaceUser e WHERE e.clientId = :clientId and e.externalInterface = :externalInterface and e.user = :user", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = InterfaceUser.NQ_FIND_BY_KEY_PRIMITIVE, query = "SELECT e FROM InterfaceUser e WHERE e.clientId = :clientId and e.externalInterface.id = :externalInterfaceId and e.user.id = :userId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = InterfaceUser.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM InterfaceUser e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = InterfaceUser.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = InterfaceUser.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "interface_id", "user_id"})})
public class InterfaceUser extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_INTERFACE_USER";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Key.
	 */
	public static final String NQ_FIND_BY_KEY = "InterfaceUser.findByKey";
	/**
	 * Named query find by unique key: Key using the ID field for references.
	 */
	public static final String NQ_FIND_BY_KEY_PRIMITIVE = "InterfaceUser.findByKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "InterfaceUser.findByBusiness";

	@NotBlank
	@Column(name = "notification_condition", nullable = false, length = 32)
	@Convert(converter = InterfaceNotificationConditionConverter.class)
	private InterfaceNotificationCondition notificationCondition;

	@NotNull
	@Column(name = "enabled", nullable = false)
	private Boolean enabled;

	@NotBlank
	@Column(name = "notification_type", nullable = false, length = 32)
	@Convert(converter = NotificationTypeConverter.class)
	private NotificationType notificationType;

	@Column(name = "emailsubject", length = 100)
	private String emailSubject;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ExternalInterface.class)
	@JoinColumn(name = "interface_id", referencedColumnName = "id")
	private ExternalInterface externalInterface;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = UserSupp.class)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private UserSupp user;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Template.class)
	@JoinColumn(name = "email_template_id", referencedColumnName = "id")
	private Template emailTemplate;

	public InterfaceNotificationCondition getNotificationCondition() {
		return this.notificationCondition;
	}

	public void setNotificationCondition(
			InterfaceNotificationCondition notificationCondition) {
		this.notificationCondition = notificationCondition;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public NotificationType getNotificationType() {
		return this.notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public String getEmailSubject() {
		return this.emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public ExternalInterface getExternalInterface() {
		return this.externalInterface;
	}

	public void setExternalInterface(ExternalInterface externalInterface) {
		if (externalInterface != null) {
			this.__validate_client_context__(externalInterface.getClientId());
		}
		this.externalInterface = externalInterface;
	}
	public UserSupp getUser() {
		return this.user;
	}

	public void setUser(UserSupp user) {
		if (user != null) {
			this.__validate_client_context__(user.getClientId());
		}
		this.user = user;
	}
	public Template getEmailTemplate() {
		return this.emailTemplate;
	}

	public void setEmailTemplate(Template emailTemplate) {
		if (emailTemplate != null) {
			this.__validate_client_context__(emailTemplate.getClientId());
		}
		this.emailTemplate = emailTemplate;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.enabled == null) {
			this.enabled = new Boolean(true);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime
				* result
				+ ((this.externalInterface == null)
						? 0
						: this.externalInterface.hashCode());
		result = prime * result
				+ ((this.user == null) ? 0 : this.user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		InterfaceUser other = (InterfaceUser) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.externalInterface == null) {
			if (other.externalInterface != null) {
				return false;
			}
		} else if (!this.externalInterface.equals(other.externalInterface)) {
			return false;
		}
		if (this.user == null) {
			if (other.user != null) {
				return false;
			}
		} else if (!this.user.equals(other.user)) {
			return false;
		}
		return true;
	}
}
