/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum DaysOfMonth {

	_EMPTY_(""), _ALL_("All"), _1_("1"), _2_("2"), _3_("3"), _4_("4"), _5_("5"), _6_(
			"6"), _7_("7"), _8_("8"), _9_("9"), _10_("10"), _11_("11"), _12_(
			"12"), _13_("13"), _14_("14"), _15_("15"), _16_("16"), _17_("17"), _18_(
			"18"), _19_("19"), _20_("20"), _21_("21"), _22_("22"), _23_("23"), _24_(
			"24"), _25_("25"), _26_("26"), _27_("27"), _28_("28"), _29_("29"), _30_(
			"30"), _31_("31"), _LAST_("Last");

	private String name;

	private DaysOfMonth(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static DaysOfMonth getByName(String name) {
		for (DaysOfMonth status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent DaysOfMonth with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
