/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ParamType} enum.
 * Generated code. Do not modify in this file.
 */
public class ParamTypeConverter
		implements
			AttributeConverter<ParamType, String> {

	@Override
	public String convertToDatabaseColumn(ParamType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ParamType.");
		}
		return value.getName();
	}

	@Override
	public ParamType convertToEntityAttribute(String value) {
		return ParamType.getByName(value);
	}

}
