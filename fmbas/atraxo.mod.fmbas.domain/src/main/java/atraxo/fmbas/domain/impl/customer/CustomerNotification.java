/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.customer;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import atraxo.fmbas.domain.impl.templates.Template;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link CustomerNotification} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = CustomerNotification.NQ_FIND_BY_KEY, query = "SELECT e FROM CustomerNotification e WHERE e.clientId = :clientId and e.customer = :customer and e.notificationEvent = :notificationEvent and e.assignedArea = :assignedArea", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = CustomerNotification.NQ_FIND_BY_KEY_PRIMITIVE, query = "SELECT e FROM CustomerNotification e WHERE e.clientId = :clientId and e.customer.id = :customerId and e.notificationEvent.id = :notificationEventId and e.assignedArea.id = :assignedAreaId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = CustomerNotification.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM CustomerNotification e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = CustomerNotification.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = CustomerNotification.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "customer_id",
		"notificationevent_id", "assigned_area"})})
public class CustomerNotification extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "BAS_CUSTOMER_NOTIFICATIONS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Key.
	 */
	public static final String NQ_FIND_BY_KEY = "CustomerNotification.findByKey";
	/**
	 * Named query find by unique key: Key using the ID field for references.
	 */
	public static final String NQ_FIND_BY_KEY_PRIMITIVE = "CustomerNotification.findByKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "CustomerNotification.findByBusiness";

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Template.class)
	@JoinColumn(name = "template_id", referencedColumnName = "id")
	private Template template;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Template.class)
	@JoinColumn(name = "emailbody_id", referencedColumnName = "id")
	private Template emailBody;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = NotificationEvent.class)
	@JoinColumn(name = "notificationevent_id", referencedColumnName = "id")
	private NotificationEvent notificationEvent;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Area.class)
	@JoinColumn(name = "assigned_area", referencedColumnName = "id")
	private Area assignedArea;
	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		if (customer != null) {
			this.__validate_client_context__(customer.getClientId());
		}
		this.customer = customer;
	}
	public Template getTemplate() {
		return this.template;
	}

	public void setTemplate(Template template) {
		if (template != null) {
			this.__validate_client_context__(template.getClientId());
		}
		this.template = template;
	}
	public Template getEmailBody() {
		return this.emailBody;
	}

	public void setEmailBody(Template emailBody) {
		if (emailBody != null) {
			this.__validate_client_context__(emailBody.getClientId());
		}
		this.emailBody = emailBody;
	}
	public NotificationEvent getNotificationEvent() {
		return this.notificationEvent;
	}

	public void setNotificationEvent(NotificationEvent notificationEvent) {
		if (notificationEvent != null) {
			this.__validate_client_context__(notificationEvent.getClientId());
		}
		this.notificationEvent = notificationEvent;
	}
	public Area getAssignedArea() {
		return this.assignedArea;
	}

	public void setAssignedArea(Area assignedArea) {
		if (assignedArea != null) {
			this.__validate_client_context__(assignedArea.getClientId());
		}
		this.assignedArea = assignedArea;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
