/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.timeserie;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.commodities.Commoditie;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.DataProvider;
import atraxo.fmbas.domain.impl.fmbas_type.DataProviderConverter;
import atraxo.fmbas.domain.impl.fmbas_type.Operator;
import atraxo.fmbas.domain.impl.fmbas_type.OperatorConverter;
import atraxo.fmbas.domain.impl.fmbas_type.PostTypeInd;
import atraxo.fmbas.domain.impl.fmbas_type.PostTypeIndConverter;
import atraxo.fmbas.domain.impl.fmbas_type.SerieFreqInd;
import atraxo.fmbas.domain.impl.fmbas_type.SerieFreqIndConverter;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesApprovalStatus;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesApprovalStatusConverter;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesStatus;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesStatusConverter;
import atraxo.fmbas.domain.impl.fmbas_type.ValueType;
import atraxo.fmbas.domain.impl.fmbas_type.ValueTypeConverter;
import atraxo.fmbas.domain.impl.timeserie.CompositeTimeSeriesSource;
import atraxo.fmbas.domain.impl.timeserie.RawTimeSerieItem;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieAverage;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link TimeSerie} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = TimeSerie.NQ_FIND_BY_NAME, query = "SELECT e FROM TimeSerie e WHERE e.clientId = :clientId and e.serieName = :serieName", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = TimeSerie.NQ_FIND_BY_CODE, query = "SELECT e FROM TimeSerie e WHERE e.clientId = :clientId and e.externalSerieName = :externalSerieName", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = TimeSerie.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM TimeSerie e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = TimeSerie.TABLE_NAME, uniqueConstraints = {
		@UniqueConstraint(name = TimeSerie.TABLE_NAME + "_UK1", columnNames = {
				"clientid", "serie_name"}),
		@UniqueConstraint(name = TimeSerie.TABLE_NAME + "_UK2", columnNames = {
				"clientid", "external_serie_name"})})
public class TimeSerie extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_TSERIE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "TimeSerie.findByName";
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "TimeSerie.findByCode";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "TimeSerie.findByBusiness";

	@NotBlank
	@Column(name = "serie_name", nullable = false, length = 32)
	private String serieName;

	@NotBlank
	@Column(name = "description", nullable = false, length = 200)
	private String description;

	@NotBlank
	@Column(name = "serie_type", nullable = false, length = 1)
	private String serieType;

	@NotBlank
	@Column(name = "serie_freq_ind", nullable = false, length = 32)
	@Convert(converter = SerieFreqIndConverter.class)
	private SerieFreqInd serieFreqInd;

	@NotNull
	@Column(name = "decimals", nullable = false, precision = 3)
	private Integer decimals;

	@NotBlank
	@Column(name = "data_provider", nullable = false, length = 32)
	@Convert(converter = DataProviderConverter.class)
	private DataProvider dataProvider;

	@NotBlank
	@Column(name = "external_serie_name", nullable = false, length = 15)
	private String externalSerieName;

	@NotBlank
	@Column(name = "activation", nullable = false, length = 1)
	private String activation;

	@Temporal(TemporalType.DATE)
	@Column(name = "first_usage")
	private Date firstUsage;

	@Temporal(TemporalType.DATE)
	@Column(name = "last_usage")
	private Date lastUsage;

	@NotNull
	@Column(name = "factor", nullable = false, precision = 19, scale = 6)
	private BigDecimal factor;

	@NotBlank
	@Column(name = "supply_item_update", nullable = false, length = 1)
	private String supplyItemUpdate;

	@Column(name = "post_type_ind", length = 40)
	@Convert(converter = PostTypeIndConverter.class)
	private PostTypeInd postTypeInd;

	@Column(name = "conv_fctr", length = 32)
	@Convert(converter = OperatorConverter.class)
	private Operator convFctr;

	@Column(name = "arithm_oper", precision = 19, scale = 6)
	private BigDecimal arithmOper;

	@Column(name = "active")
	private Boolean active;

	@Column(name = "status", length = 40)
	@Convert(converter = TimeSeriesStatusConverter.class)
	private TimeSeriesStatus status;

	@Column(name = "approval_status", length = 32)
	@Convert(converter = TimeSeriesApprovalStatusConverter.class)
	private TimeSeriesApprovalStatus approvalStatus;

	@NotBlank
	@Column(name = "value_type", nullable = false, length = 32)
	@Convert(converter = ValueTypeConverter.class)
	private ValueType valueType;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "currency_1_id", referencedColumnName = "id")
	private Currencies currency1Id;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "currency_2_id", referencedColumnName = "id")
	private Currencies currency2Id;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Commoditie.class)
	@JoinColumn(name = "commoditie_id", referencedColumnName = "id")
	private Commoditie commoditieId;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_id", referencedColumnName = "id")
	private Unit unitId;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit2_id", referencedColumnName = "id")
	private Unit unit2Id;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FinancialSources.class)
	@JoinColumn(name = "financial_source_id", referencedColumnName = "id")
	private FinancialSources financialSource;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = RawTimeSerieItem.class, mappedBy = "tserie", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<RawTimeSerieItem> rawTimeserieItems;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = TimeSerieItem.class, mappedBy = "tserie", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	private Collection<TimeSerieItem> timeserieItems;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = TimeSerieAverage.class, mappedBy = "tserie")
	private Collection<TimeSerieAverage> timeserieAvg;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = CompositeTimeSeriesSource.class, mappedBy = "composite", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	private Collection<CompositeTimeSeriesSource> compositeTimeSeries;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = CompositeTimeSeriesSource.class, mappedBy = "source")
	private Collection<CompositeTimeSeriesSource> sourceTimeSeries;

	public String getSerieName() {
		return this.serieName;
	}

	public void setSerieName(String serieName) {
		this.serieName = serieName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSerieType() {
		return this.serieType;
	}

	public void setSerieType(String serieType) {
		this.serieType = serieType;
	}

	public SerieFreqInd getSerieFreqInd() {
		return this.serieFreqInd;
	}

	public void setSerieFreqInd(SerieFreqInd serieFreqInd) {
		this.serieFreqInd = serieFreqInd;
	}

	public Integer getDecimals() {
		return this.decimals;
	}

	public void setDecimals(Integer decimals) {
		this.decimals = decimals;
	}

	public DataProvider getDataProvider() {
		return this.dataProvider;
	}

	public void setDataProvider(DataProvider dataProvider) {
		this.dataProvider = dataProvider;
	}

	public String getExternalSerieName() {
		return this.externalSerieName;
	}

	public void setExternalSerieName(String externalSerieName) {
		this.externalSerieName = externalSerieName;
	}

	public String getActivation() {
		return this.activation;
	}

	public void setActivation(String activation) {
		this.activation = activation;
	}

	public Date getFirstUsage() {
		return this.firstUsage;
	}

	public void setFirstUsage(Date firstUsage) {
		this.firstUsage = firstUsage;
	}

	public Date getLastUsage() {
		return this.lastUsage;
	}

	public void setLastUsage(Date lastUsage) {
		this.lastUsage = lastUsage;
	}

	public BigDecimal getFactor() {
		return this.factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

	public String getSupplyItemUpdate() {
		return this.supplyItemUpdate;
	}

	public void setSupplyItemUpdate(String supplyItemUpdate) {
		this.supplyItemUpdate = supplyItemUpdate;
	}

	public PostTypeInd getPostTypeInd() {
		return this.postTypeInd;
	}

	public void setPostTypeInd(PostTypeInd postTypeInd) {
		this.postTypeInd = postTypeInd;
	}

	public Operator getConvFctr() {
		return this.convFctr;
	}

	public void setConvFctr(Operator convFctr) {
		this.convFctr = convFctr;
	}

	public BigDecimal getArithmOper() {
		return this.arithmOper;
	}

	public void setArithmOper(BigDecimal arithmOper) {
		this.arithmOper = arithmOper;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public TimeSeriesStatus getStatus() {
		return this.status;
	}

	public void setStatus(TimeSeriesStatus status) {
		this.status = status;
	}

	public TimeSeriesApprovalStatus getApprovalStatus() {
		return this.approvalStatus;
	}

	public void setApprovalStatus(TimeSeriesApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public ValueType getValueType() {
		return this.valueType;
	}

	public void setValueType(ValueType valueType) {
		this.valueType = valueType;
	}

	public Currencies getCurrency1Id() {
		return this.currency1Id;
	}

	public void setCurrency1Id(Currencies currency1Id) {
		if (currency1Id != null) {
			this.__validate_client_context__(currency1Id.getClientId());
		}
		this.currency1Id = currency1Id;
	}
	public Currencies getCurrency2Id() {
		return this.currency2Id;
	}

	public void setCurrency2Id(Currencies currency2Id) {
		if (currency2Id != null) {
			this.__validate_client_context__(currency2Id.getClientId());
		}
		this.currency2Id = currency2Id;
	}
	public Commoditie getCommoditieId() {
		return this.commoditieId;
	}

	public void setCommoditieId(Commoditie commoditieId) {
		if (commoditieId != null) {
			this.__validate_client_context__(commoditieId.getClientId());
		}
		this.commoditieId = commoditieId;
	}
	public Unit getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Unit unitId) {
		if (unitId != null) {
			this.__validate_client_context__(unitId.getClientId());
		}
		this.unitId = unitId;
	}
	public Unit getUnit2Id() {
		return this.unit2Id;
	}

	public void setUnit2Id(Unit unit2Id) {
		if (unit2Id != null) {
			this.__validate_client_context__(unit2Id.getClientId());
		}
		this.unit2Id = unit2Id;
	}
	public FinancialSources getFinancialSource() {
		return this.financialSource;
	}

	public void setFinancialSource(FinancialSources financialSource) {
		if (financialSource != null) {
			this.__validate_client_context__(financialSource.getClientId());
		}
		this.financialSource = financialSource;
	}

	public Collection<RawTimeSerieItem> getRawTimeserieItems() {
		return this.rawTimeserieItems;
	}

	public void setRawTimeserieItems(
			Collection<RawTimeSerieItem> rawTimeserieItems) {
		this.rawTimeserieItems = rawTimeserieItems;
	}

	/**
	 * @param e
	 */
	public void addToRawTimeserieItems(RawTimeSerieItem e) {
		if (this.rawTimeserieItems == null) {
			this.rawTimeserieItems = new ArrayList<>();
		}
		e.setTserie(this);
		this.rawTimeserieItems.add(e);
	}
	public Collection<TimeSerieItem> getTimeserieItems() {
		return this.timeserieItems;
	}

	public void setTimeserieItems(Collection<TimeSerieItem> timeserieItems) {
		this.timeserieItems = timeserieItems;
	}

	/**
	 * @param e
	 */
	public void addToTimeserieItems(TimeSerieItem e) {
		if (this.timeserieItems == null) {
			this.timeserieItems = new ArrayList<>();
		}
		e.setTserie(this);
		this.timeserieItems.add(e);
	}
	public Collection<TimeSerieAverage> getTimeserieAvg() {
		return this.timeserieAvg;
	}

	public void setTimeserieAvg(Collection<TimeSerieAverage> timeserieAvg) {
		this.timeserieAvg = timeserieAvg;
	}

	/**
	 * @param e
	 */
	public void addToTimeserieAvg(TimeSerieAverage e) {
		if (this.timeserieAvg == null) {
			this.timeserieAvg = new ArrayList<>();
		}
		e.setTserie(this);
		this.timeserieAvg.add(e);
	}
	public Collection<CompositeTimeSeriesSource> getCompositeTimeSeries() {
		return this.compositeTimeSeries;
	}

	public void setCompositeTimeSeries(
			Collection<CompositeTimeSeriesSource> compositeTimeSeries) {
		this.compositeTimeSeries = compositeTimeSeries;
	}

	/**
	 * @param e
	 */
	public void addToCompositeTimeSeries(CompositeTimeSeriesSource e) {
		if (this.compositeTimeSeries == null) {
			this.compositeTimeSeries = new ArrayList<>();
		}
		e.setComposite(this);
		this.compositeTimeSeries.add(e);
	}
	public Collection<CompositeTimeSeriesSource> getSourceTimeSeries() {
		return this.sourceTimeSeries;
	}

	public void setSourceTimeSeries(
			Collection<CompositeTimeSeriesSource> sourceTimeSeries) {
		this.sourceTimeSeries = sourceTimeSeries;
	}

	/**
	 * @param e
	 */
	public void addToSourceTimeSeries(CompositeTimeSeriesSource e) {
		if (this.sourceTimeSeries == null) {
			this.sourceTimeSeries = new ArrayList<>();
		}
		e.setSource(this);
		this.sourceTimeSeries.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
