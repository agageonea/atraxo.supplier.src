/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.workflow;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.JobChainNotificationCondition;
import atraxo.fmbas.domain.impl.fmbas_type.JobChainNotificationConditionConverter;
import atraxo.fmbas.domain.impl.fmbas_type.NotificationType;
import atraxo.fmbas.domain.impl.fmbas_type.NotificationTypeConverter;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link WorkflowNotification} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = WorkflowNotification.NQ_FIND_BY_KEY, query = "SELECT e FROM WorkflowNotification e WHERE e.clientId = :clientId and e.workflow = :workflow and e.user = :user", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = WorkflowNotification.NQ_FIND_BY_KEY_PRIMITIVE, query = "SELECT e FROM WorkflowNotification e WHERE e.clientId = :clientId and e.workflow.id = :workflowId and e.user.id = :userId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = WorkflowNotification.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM WorkflowNotification e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = WorkflowNotification.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = WorkflowNotification.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "workflow_id", "user_id"})})
public class WorkflowNotification extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "BAS_WORKFLOW_NOTIFICATION";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Key.
	 */
	public static final String NQ_FIND_BY_KEY = "WorkflowNotification.findByKey";
	/**
	 * Named query find by unique key: Key using the ID field for references.
	 */
	public static final String NQ_FIND_BY_KEY_PRIMITIVE = "WorkflowNotification.findByKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "WorkflowNotification.findByBusiness";

	@NotBlank
	@Column(name = "notification_condition", nullable = false, length = 32)
	@Convert(converter = JobChainNotificationConditionConverter.class)
	private JobChainNotificationCondition notificationCondition;

	@NotNull
	@Column(name = "enabled", nullable = false)
	private Boolean enabled;

	@NotBlank
	@Column(name = "notification_type", nullable = false, length = 32)
	@Convert(converter = NotificationTypeConverter.class)
	private NotificationType notificationType;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Workflow.class)
	@JoinColumn(name = "workflow_id", referencedColumnName = "id")
	private Workflow workflow;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = UserSupp.class)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private UserSupp user;

	public JobChainNotificationCondition getNotificationCondition() {
		return this.notificationCondition;
	}

	public void setNotificationCondition(
			JobChainNotificationCondition notificationCondition) {
		this.notificationCondition = notificationCondition;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public NotificationType getNotificationType() {
		return this.notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public Workflow getWorkflow() {
		return this.workflow;
	}

	public void setWorkflow(Workflow workflow) {
		if (workflow != null) {
			this.__validate_client_context__(workflow.getClientId());
		}
		this.workflow = workflow;
	}
	public UserSupp getUser() {
		return this.user;
	}

	public void setUser(UserSupp user) {
		if (user != null) {
			this.__validate_client_context__(user.getClientId());
		}
		this.user = user;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.enabled == null) {
			this.enabled = new Boolean(true);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.workflow == null) ? 0 : this.workflow.hashCode());
		result = prime * result
				+ ((this.user == null) ? 0 : this.user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		WorkflowNotification other = (WorkflowNotification) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.workflow == null) {
			if (other.workflow != null) {
				return false;
			}
		} else if (!this.workflow.equals(other.workflow)) {
			return false;
		}
		if (this.user == null) {
			if (other.user != null) {
				return false;
			}
		} else if (!this.user.equals(other.user)) {
			return false;
		}
		return true;
	}
}
