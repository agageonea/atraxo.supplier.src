/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.quotation;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.fmbas_type.ValueType;
import atraxo.fmbas.domain.impl.fmbas_type.ValueTypeConverter;
import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Quotation} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Quotation.NQ_FIND_BY_NAME, query = "SELECT e FROM Quotation e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Quotation.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Quotation e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Quotation.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Quotation.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "name"})})
public class Quotation extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_QUOT";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "Quotation.findByName";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Quotation.findByBusiness";

	@NotBlank
	@Column(name = "name", nullable = false, length = 100)
	private String name;

	@Column(name = "active")
	private Boolean active;

	@NotBlank
	@Column(name = "value_type", nullable = false, length = 32)
	@Convert(converter = ValueTypeConverter.class)
	private ValueType valueType;

	@Transient
	private String calcVal;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = TimeSerie.class)
	@JoinColumn(name = "tserie_id", referencedColumnName = "id")
	private TimeSerie timeseries;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "crncy_id", referencedColumnName = "id")
	private Currencies currency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_id", referencedColumnName = "id")
	private Unit unit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AverageMethod.class)
	@JoinColumn(name = "avg_mthd_id", referencedColumnName = "id")
	private AverageMethod avgMethodIndicator;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = QuotationValue.class, mappedBy = "quotation", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<QuotationValue> quotationValues;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public ValueType getValueType() {
		return this.valueType;
	}

	public void setValueType(ValueType valueType) {
		this.valueType = valueType;
	}

	public String getCalcVal() {
		return this.getName() + "/" + this.getAvgMethodIndicator().getName();
	}

	public void setCalcVal(String calcVal) {
		this.calcVal = calcVal;
	}

	public TimeSerie getTimeseries() {
		return this.timeseries;
	}

	public void setTimeseries(TimeSerie timeseries) {
		if (timeseries != null) {
			this.__validate_client_context__(timeseries.getClientId());
		}
		this.timeseries = timeseries;
	}
	public Currencies getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currencies currency) {
		if (currency != null) {
			this.__validate_client_context__(currency.getClientId());
		}
		this.currency = currency;
	}
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		if (unit != null) {
			this.__validate_client_context__(unit.getClientId());
		}
		this.unit = unit;
	}
	public AverageMethod getAvgMethodIndicator() {
		return this.avgMethodIndicator;
	}

	public void setAvgMethodIndicator(AverageMethod avgMethodIndicator) {
		if (avgMethodIndicator != null) {
			this.__validate_client_context__(avgMethodIndicator.getClientId());
		}
		this.avgMethodIndicator = avgMethodIndicator;
	}

	public Collection<QuotationValue> getQuotationValues() {
		return this.quotationValues;
	}

	public void setQuotationValues(Collection<QuotationValue> quotationValues) {
		this.quotationValues = quotationValues;
	}

	/**
	 * @param e
	 */
	public void addToQuotationValues(QuotationValue e) {
		if (this.quotationValues == null) {
			this.quotationValues = new ArrayList<>();
		}
		e.setQuotation(this);
		this.quotationValues.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
