/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.timeserie;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link TimeSerieItem} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = TimeSerieItem.NQ_FIND_BY_TSI_BUSINESS, query = "SELECT e FROM TimeSerieItem e WHERE e.clientId = :clientId and e.tserie = :tserie and e.itemDate = :itemDate", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = TimeSerieItem.NQ_FIND_BY_TSI_BUSINESS_PRIMITIVE, query = "SELECT e FROM TimeSerieItem e WHERE e.clientId = :clientId and e.tserie.id = :tserieId and e.itemDate = :itemDate", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = TimeSerieItem.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM TimeSerieItem e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = TimeSerieItem.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = TimeSerieItem.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "tserie_id", "item_date"})})
public class TimeSerieItem extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_TSERIE_ITEM";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Tsi_business.
	 */
	public static final String NQ_FIND_BY_TSI_BUSINESS = "TimeSerieItem.findByTsi_business";
	/**
	 * Named query find by unique key: Tsi_business using the ID field for references.
	 */
	public static final String NQ_FIND_BY_TSI_BUSINESS_PRIMITIVE = "TimeSerieItem.findByTsi_business_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "TimeSerieItem.findByBusiness";

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "item_date", nullable = false)
	private Date itemDate;

	@NotNull
	@Column(name = "item_value", nullable = false, precision = 19, scale = 6)
	private BigDecimal itemValue;

	@NotNull
	@Column(name = "calculated", nullable = false)
	private Boolean calculated;

	@NotNull
	@Column(name = "transfered", nullable = false)
	private Boolean transfered;

	@NotNull
	@Column(name = "transfered_item_update", nullable = false)
	private Boolean transferedItemUpdate;

	@NotNull
	@Column(name = "updated", nullable = false)
	private Boolean updated;

	@Column(name = "active")
	private Boolean active;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = TimeSerie.class)
	@JoinColumn(name = "tserie_id", referencedColumnName = "id")
	private TimeSerie tserie;

	public Date getItemDate() {
		return this.itemDate;
	}

	public void setItemDate(Date itemDate) {
		this.itemDate = itemDate;
	}

	public BigDecimal getItemValue() {
		return this.itemValue;
	}

	public void setItemValue(BigDecimal itemValue) {
		this.itemValue = itemValue;
	}

	public Boolean getCalculated() {
		return this.calculated;
	}

	public void setCalculated(Boolean calculated) {
		this.calculated = calculated;
	}

	public Boolean getTransfered() {
		return this.transfered;
	}

	public void setTransfered(Boolean transfered) {
		this.transfered = transfered;
	}

	public Boolean getTransferedItemUpdate() {
		return this.transferedItemUpdate;
	}

	public void setTransferedItemUpdate(Boolean transferedItemUpdate) {
		this.transferedItemUpdate = transferedItemUpdate;
	}

	public Boolean getUpdated() {
		return this.updated;
	}

	public void setUpdated(Boolean updated) {
		this.updated = updated;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public TimeSerie getTserie() {
		return this.tserie;
	}

	public void setTserie(TimeSerie tserie) {
		if (tserie != null) {
			this.__validate_client_context__(tserie.getClientId());
		}
		this.tserie = tserie;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.tserie == null) ? 0 : this.tserie.hashCode());
		result = prime * result
				+ ((this.itemDate == null) ? 0 : this.itemDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		TimeSerieItem other = (TimeSerieItem) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.tserie == null) {
			if (other.tserie != null) {
				return false;
			}
		} else if (!this.tserie.equals(other.tserie)) {
			return false;
		}
		if (this.itemDate == null) {
			if (other.itemDate != null) {
				return false;
			}
		} else if (!this.itemDate.equals(other.itemDate)) {
			return false;
		}
		return true;
	}
}
