/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.dashboard;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;
import atraxo.fmbas.domain.impl.dashboard.WidgetParameters;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link AssignedWidgetParameters} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = AssignedWidgetParameters.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM AssignedWidgetParameters e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = AssignedWidgetParameters.TABLE_NAME)
public class AssignedWidgetParameters extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "BAS_ASSIGNED_WIDGET_PARAMETERS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "AssignedWidgetParameters.findByBusiness";

	@Column(name = "value", length = 100)
	private String value;

	@Lob
	@Column(name = "blob_value")
	private byte[] blobValue;

	@Column(name = "default_value", length = 50)
	private String defaultValue;

	@Column(name = "read_only")
	private Boolean readOnly;

	@Column(name = "ref_values", length = 255)
	private String refValues;

	@Column(name = "ref_business_values", length = 1000)
	private String refBusinessValues;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = DashboardWidgets.class)
	@JoinColumn(name = "dashboard_widget_id", referencedColumnName = "id")
	private DashboardWidgets dashboardWidgets;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = WidgetParameters.class)
	@JoinColumn(name = "widget_parameter_id", referencedColumnName = "id")
	private WidgetParameters widgetParameters;

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public byte[] getBlobValue() {
		return this.blobValue;
	}

	public void setBlobValue(byte[] blobValue) {
		this.blobValue = blobValue;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Boolean getReadOnly() {
		return this.readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	public String getRefValues() {
		return this.refValues;
	}

	public void setRefValues(String refValues) {
		this.refValues = refValues;
	}

	public String getRefBusinessValues() {
		return this.refBusinessValues;
	}

	public void setRefBusinessValues(String refBusinessValues) {
		this.refBusinessValues = refBusinessValues;
	}

	public DashboardWidgets getDashboardWidgets() {
		return this.dashboardWidgets;
	}

	public void setDashboardWidgets(DashboardWidgets dashboardWidgets) {
		if (dashboardWidgets != null) {
			this.__validate_client_context__(dashboardWidgets.getClientId());
		}
		this.dashboardWidgets = dashboardWidgets;
	}
	public WidgetParameters getWidgetParameters() {
		return this.widgetParameters;
	}

	public void setWidgetParameters(WidgetParameters widgetParameters) {
		if (widgetParameters != null) {
			this.__validate_client_context__(widgetParameters.getClientId());
		}
		this.widgetParameters = widgetParameters;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
