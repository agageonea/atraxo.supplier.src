/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum InvoiceCostToAccrual {

	_EMPTY_(""), _ON_RELEASE_("On release"), _ON_PAYMENT_("On payment");

	private String name;

	private InvoiceCostToAccrual(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static InvoiceCostToAccrual getByName(String name) {
		for (InvoiceCostToAccrual status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent InvoiceCostToAccrual with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
