/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ValueType {

	_EMPTY_(""), _HIGH_PRICE_("High price"), _LOW_PRICE_("Low price"), _MEAN_PRICE_(
			"Mean price"), _USER_CALCULATED_PRICE_("User calculated price");

	private String name;

	private ValueType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ValueType getByName(String name) {
		for (ValueType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent ValueType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
