/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum CustomerDataTransmissionStatus {

	_EMPTY_("", ""), _NOT_AVAILABLE_("Not available", "Not available"), _IN_PROGRESS_(
			"In progress", "In progress"), _SENT_("Sent", "Sent"), _DELIVERED_(
			"Delivered", "Delivered"), _PROCESSED_("Processed", "Processed"), _FAILED_(
			"Failed", "Failure");

	private String name;
	private String navValue;

	private CustomerDataTransmissionStatus(String name, String navValue) {
		this.name = name;
		this.navValue = navValue;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static CustomerDataTransmissionStatus getByName(String name) {
		for (CustomerDataTransmissionStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent CustomerDataTransmissionStatus with name: " + name);
	}

	public String getNavValue() {
		return this.navValue;
	}

	public static CustomerDataTransmissionStatus getByNavValue(String code) {
		for (CustomerDataTransmissionStatus status : values()) {
			if (status.getNavValue().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent CustomerDataTransmissionStatus with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
