/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link JobChainNotificationCondition} enum.
 * Generated code. Do not modify in this file.
 */
public class JobChainNotificationConditionConverter
		implements
			AttributeConverter<JobChainNotificationCondition, String> {

	@Override
	public String convertToDatabaseColumn(JobChainNotificationCondition value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null JobChainNotificationCondition.");
		}
		return value.getName();
	}

	@Override
	public JobChainNotificationCondition convertToEntityAttribute(String value) {
		return JobChainNotificationCondition.getByName(value);
	}

}
