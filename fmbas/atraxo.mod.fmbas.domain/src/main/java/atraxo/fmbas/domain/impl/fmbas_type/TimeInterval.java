/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TimeInterval {

	_EMPTY_("", 0), _5_MINUTES_("5 minutes", 5), _10_MINUTES_("10 minutes", 10), _15_MINUTES_(
			"15 minutes", 15), _30_MINUTES_("30 minutes", 30), _1_HOUR_(
			"1 hour", 60);

	private String name;
	private int duration;

	private TimeInterval(String name, int duration) {
		this.name = name;
		this.duration = duration;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TimeInterval getByName(String name) {
		for (TimeInterval status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TimeInterval with name: "
				+ name);
	}

	public int getDuration() {
		return this.duration;
	}

	public static TimeInterval getByDuration(int code) {
		for (TimeInterval status : values()) {
			if (status.getDuration() == code) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TimeInterval with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
