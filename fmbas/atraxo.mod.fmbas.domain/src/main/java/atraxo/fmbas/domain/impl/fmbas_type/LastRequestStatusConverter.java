/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link LastRequestStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class LastRequestStatusConverter
		implements
			AttributeConverter<LastRequestStatus, String> {

	@Override
	public String convertToDatabaseColumn(LastRequestStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null LastRequestStatus.");
		}
		return value.getName();
	}

	@Override
	public LastRequestStatus convertToEntityAttribute(String value) {
		return LastRequestStatus.getByName(value);
	}

}
