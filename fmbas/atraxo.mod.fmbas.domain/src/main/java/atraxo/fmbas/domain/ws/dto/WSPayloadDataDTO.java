package atraxo.fmbas.domain.ws.dto;

import org.w3c.dom.Element;

/**
 * @author vhojda
 *
 */
public class WSPayloadDataDTO {

	private Element payload;
	
	/**
	 * Default constructor
	 */
	public WSPayloadDataDTO(){
		
	}

	public Element getPayload() {
		return payload;
	}

	public void setPayload(Element payload) {
		this.payload = payload;
	}


	@Override
	public String toString() {
		return "WSPayloadDataDTO [payload=" + payload + "]";
	}
	
	
}
