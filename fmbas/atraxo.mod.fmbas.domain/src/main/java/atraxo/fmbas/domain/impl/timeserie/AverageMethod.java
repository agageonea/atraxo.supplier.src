/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.timeserie;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link AverageMethod} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = AverageMethod.NQ_FIND_BY_NAME, query = "SELECT e FROM AverageMethod e WHERE e.clientId = :clientId and e.code = :code", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = AverageMethod.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM AverageMethod e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = AverageMethod.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = AverageMethod.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "code"})})
public class AverageMethod extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "BAS_AVG_MTHDS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "AverageMethod.findByName";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "AverageMethod.findByBusiness";

	@NotBlank
	@Column(name = "code", nullable = false, length = 32)
	private String code;

	@NotBlank
	@Column(name = "name", nullable = false, length = 50)
	private String name;

	@Column(name = "active")
	private Boolean active;

	@Column(name = "default_method")
	private Boolean defaultMethod;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getDefaultMethod() {
		return this.defaultMethod;
	}

	public void setDefaultMethod(Boolean defaultMethod) {
		this.defaultMethod = defaultMethod;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
