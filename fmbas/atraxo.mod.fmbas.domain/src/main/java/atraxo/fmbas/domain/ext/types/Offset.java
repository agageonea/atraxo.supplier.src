package atraxo.fmbas.domain.ext.types;

public enum Offset {
	CURRENT("current", 0) {
	},
	PREVIOUS("previous", -1) {
	},
	CURRENT_2("current -2", -2) {
	},
	NEXT("current +1", 1) {
	};

	private String displayName;
	private int delta;

	private Offset(String name, int delta) {
		this.displayName = name;
		this.delta = delta;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public int getDelta() {
		return this.delta;
	}

	public static Offset getByName(String name) {
		for (Offset offset : values()) {
			if (offset.getDisplayName().equalsIgnoreCase(name)) {
				return offset;
			}
		}
		throw new RuntimeException("Invalid quotation offset name: " + name);
	}

	/**
	 * @param name
	 * @return
	 */
	public static boolean contains(String name) {
		if (name == null) {
			return false;
		}
		for (Offset offset : values()) {
			if (offset.getDisplayName().equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}
}
