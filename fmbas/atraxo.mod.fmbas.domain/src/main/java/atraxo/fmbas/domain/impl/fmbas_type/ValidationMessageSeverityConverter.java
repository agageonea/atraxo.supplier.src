/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.domain.impl.fmbas_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ValidationMessageSeverity} enum.
 * Generated code. Do not modify in this file.
 */
public class ValidationMessageSeverityConverter
		implements
			AttributeConverter<ValidationMessageSeverity, String> {

	@Override
	public String convertToDatabaseColumn(ValidationMessageSeverity value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null ValidationMessageSeverity.");
		}
		return value.getName();
	}

	@Override
	public ValidationMessageSeverity convertToEntityAttribute(String value) {
		return ValidationMessageSeverity.getByName(value);
	}

}
