/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.ExchangeRateVal_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.ExchangeRateValue_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.ExchangeRateVal_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_ExchangeRateVal_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateField({name:"validFrom", dataIndex:"vldFrDtRef"})
			.addDateField({name:"validTo", dataIndex:"vldToDtRef"})
			.addNumberField({name:"rate", dataIndex:"rate", sysDec:"dec_xr", maxLength:19})
			.addBooleanField({ name:"prov", dataIndex:"isPrvsn"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ExchangeRateVal_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_ExchangeRateVal_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"validFrom", dataIndex:"vldFrDtRef", _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"vldToDtRef", _mask_: Masks.DATE})
		.addNumberColumn({ name:"rate", dataIndex:"perRate", sysDec:"dec_xr"})
		.addNumberColumn({ name:"perRate", dataIndex:"rate", sysDec:"dec_xr"})
		.addBooleanColumn({ name:"provisioning", dataIndex:"isPrvsn", width:120})
		.addBooleanColumn({ name:"active", dataIndex:"active", hidden:true})
		.addNumberColumn({ name:"postTypeIndicator", dataIndex:"postTypeInd", hidden:true})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
