/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.WidgetParameter_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.WidgetParameter_Ds
});

/* ================= FILTER: WidgetParameterFilter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.WidgetParameter_Dc$WidgetParameterFilter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_WidgetParameter_Dc$WidgetParameterFilter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", width:100, maxLength:32})
			.addTextField({ name:"description", dataIndex:"description", width:100, maxLength:1000})
			.addCombo({ xtype:"combo", name:"type", dataIndex:"type", width:100, store:[ __FMBAS_TYPE__.JobParamType._STRING_, __FMBAS_TYPE__.JobParamType._INTEGER_, __FMBAS_TYPE__.JobParamType._BOOLEAN_, __FMBAS_TYPE__.JobParamType._DATE_, __FMBAS_TYPE__.JobParamType._ENUMERATION_, __FMBAS_TYPE__.JobParamType._LOV_, __FMBAS_TYPE__.JobParamType._MASKED_, __FMBAS_TYPE__.JobParamType._ASSIGN_, __FMBAS_TYPE__.JobParamType._KEY_, __FMBAS_TYPE__.JobParamType._TEXTAREA_]})
			.addTextField({ name:"value", dataIndex:"value", width:100, maxLength:100})
			.addTextField({ name:"defaultValue", dataIndex:"defaultValue", width:100, maxLength:50})
			.addBooleanField({ name:"readOnly", dataIndex:"readOnly", width:100})
		;
	}

});

/* ================= EDIT-GRID: WidgetParameterList ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.WidgetParameter_Dc$WidgetParameterList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_WidgetParameter_Dc$WidgetParameterList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"name", dataIndex:"name", width:50, noEdit: true, allowBlank: false, maxLength:32,  flex:1})
		.addTextColumn({name:"description", dataIndex:"description", width:200, noEdit: true, maxLength:1000,  flex:1})
		.addComboColumn({name:"type", dataIndex:"type", width:70, noEdit: true, allowBlank: false,  flex:1})
		.addTextColumn({name:"value", dataIndex:"value", width:50, maxLength:100,  flex:1, 
			editor: { xtype:"textfield", maxLength:100}})
		.addTextColumn({name:"defaultValue", dataIndex:"defaultValue", width:50, noEdit: true, allowBlank: false, maxLength:50,  flex:1})
		.addBooleanColumn({name:"readOnly", dataIndex:"readOnly", noEdit: true, allowBlank: false,  flex:1})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
