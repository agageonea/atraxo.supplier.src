/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.CompositeTimeSerie_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.CompositeTimeSeriesSource_Ds
});

/* ================= EDIT-GRID: GridEnergyPrice ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.CompositeTimeSerie_Dc$GridEnergyPrice", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_CompositeTimeSerie_Dc$GridEnergyPrice",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"timeSerie", dataIndex:"timeSerieName", width:150, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_TimeSerieLov_Lov", maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "timeSerieId"} ,{lovField:"description", dsField: "timeSerieDescription"} ,{lovField:"financialSourceCode", dsField: "financialSourceCode"} ,{lovField:"currCode", dsField: "currCode"} ,{lovField:"unitCode", dsField: "unitCode"} ]}})
		.addTextColumn({name:"description", dataIndex:"timeSerieDescription", width:200, noEdit: true, maxLength:200})
		.addTextColumn({name:"source", dataIndex:"financialSourceCode", width:100, noEdit: true, maxLength:25})
		.addTextColumn({name:"currency", dataIndex:"currCode", width:100, noEdit: true, maxLength:3})
		.addTextColumn({name:"unit", dataIndex:"unitCode", width:100, noEdit: true, maxLength:2})
		.addNumberColumn({name:"weighting", dataIndex:"weighting", width:100, maxLength:2, align:"right" })
		.addNumberColumn({name:"factor", dataIndex:"factor", width:80, sysDec:"dec_prc", maxLength:19, align:"right" })
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
