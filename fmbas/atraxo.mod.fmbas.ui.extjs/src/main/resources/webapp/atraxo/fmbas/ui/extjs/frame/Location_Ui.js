/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.Location_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Location_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("location", Ext.create(atraxo.fmbas.ui.extjs.dc.Locations_Dc,{trackEditMode: true}))
		.addDc("contacts", Ext.create(atraxo.fmbas.ui.extjs.dc.Contacts_Dc,{}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.linkDc("contacts", "location",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("note", "location",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnOnEditWdwClose",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnOnEditWdwClose, scope:this})
		.addButton({name:"btnSaveCloseWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseWdw, scope:this})
		.addButton({name:"btnSaveNewWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewWdw, scope:this})
		.addButton({name:"btnSaveEditWdw",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnSaveEditWdw, scope:this})
		.addButton({name:"btnSaveNewContactWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewContactWdw, scope:this})
		.addButton({name:"btnSaveCloseContactWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseContactWdw, scope:this})
		.addButton({name:"btnSaveCloseNoteWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseNoteWdw, scope:this})
		.addButton({name:"btnCloseContactWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseContactWdw, scope:this})
		.addButton({name:"btnCloseNoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseNoteWdw, scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdw1",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw1, scope:this})
		.addDcFilterFormView("location", {name:"locationFilter", xtype:"fmbas_Locations_Dc$Filter"})
		.addDcGridView("location", {name:"locationList", xtype:"fmbas_Locations_Dc$ListPopUp"})
		.addDcFormView("location", {name:"locationEdit", xtype:"fmbas_Locations_Dc$Edit"})
		.addDcFormView("location", {name:"locationEditDetails", xtype:"fmbas_Locations_Dc$Edit"})
		.addDcGridView("location", {name:"airportInfList", _hasTitle_:true, xtype:"fmbas_Locations_Dc$Temp",  disabled:true})
		.addDcGridView("location", {name:"notamList", _hasTitle_:true, xtype:"fmbas_Locations_Dc$Temp",  disabled:true})
		.addDcGridView("location", {name:"taxesFeesList", _hasTitle_:true, xtype:"fmbas_Locations_Dc$Temp",  disabled:true})
		.addDcFilterFormView("contacts", {name:"contactFilter", xtype:"fmbas_Contacts_Dc$Filter"})
		.addDcGridView("contacts", {name:"contactsList", _hasTitle_:true, xtype:"fmbas_Contacts_Dc$List"})
		.addDcFormView("contacts", {name:"contactEdit", xtype:"fmbas_Contacts_Dc$Edit"})
		.addDcFormView("contacts", {name:"contactEditContext", xtype:"fmbas_Contacts_Dc$Edit"})
		.addDcGridView("note", {name:"notesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcFormView("note", {name:"notesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addWindow({name:"wdwLocation", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("locationEdit")],  listeners:{ close:{fn:this.onLocationWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewWdw"), this._elems_.get("btnSaveCloseWdw"), this._elems_.get("btnSaveEditWdw"), this._elems_.get("btnOnEditWdwClose")]}]})
		.addWindow({name:"noteWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("notesEdit")],  listeners:{ close:{fn:this.onNoteWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseNoteWdw"), this._elems_.get("btnCloseNoteWdw")]}]})
		.addWindow({name:"contactWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("contactEdit")],  listeners:{ close:{fn:this.onContactWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewContactWdw"), this._elems_.get("btnSaveCloseContactWdw"), this._elems_.get("btnCloseContactWdw")]}]})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas2", dcName:"location"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["locationList"], ["center"])
		.addChildrenTo("canvas2", ["locationEditDetails", "detailsTab"], ["north", "center"])
		.addChildrenTo("canvas3", ["contactEditContext"], ["center"])
		.addChildrenTo("detailsTab", ["contactsList"])
		.addToolbarTo("locationList", "tlbLocList")
		.addToolbarTo("locationEditDetails", "tlbLocEditList")
		.addToolbarTo("notesList", "tlbNoteList")
		.addToolbarTo("contactsList", "tlbContacts")
		.addToolbarTo("contactEditContext", "tlbContactContext");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2", "canvas3"])
		.addChildrenTo2("detailsTab", ["notesList", "airportInfList", "notamList", "taxesFeesList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbLocList", {dc: "location"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbLocEditList", {dc: "location"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw1")])
			.addReports()
		.end()
		.beginToolbar("tlbNoteList", {dc: "note"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbContacts", {dc: "contacts"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas3"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbContactContext", {dc: "contacts"})
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas2"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnOnEditWdwClose
	 */
	,onBtnOnEditWdwClose: function() {
		this.onLocationWdwClose();
		this._getWindow_("wdwLocation").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseWdw
	 */
	,onBtnSaveCloseWdw: function() {
		this.saveClose();
	}
	
	/**
	 * On-Click handler for button btnSaveNewWdw
	 */
	,onBtnSaveNewWdw: function() {
		this.saveNew();
	}
	
	/**
	 * On-Click handler for button btnSaveEditWdw
	 */
	,onBtnSaveEditWdw: function() {
		this.saveEdit();
	}
	
	/**
	 * On-Click handler for button btnSaveNewContactWdw
	 */
	,onBtnSaveNewContactWdw: function() {
		this.saveNewContact();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseContactWdw
	 */
	,onBtnSaveCloseContactWdw: function() {
		this.saveCloseContact();
		this._getDc_("contacts").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseNoteWdw
	 */
	,onBtnSaveCloseNoteWdw: function() {
		this.saveCloseNote();
		this._getDc_("note").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCloseContactWdw
	 */
	,onBtnCloseContactWdw: function() {
		this.onContactWdwClose();
		this._getWindow_("contactWdw").close();
	}
	
	/**
	 * On-Click handler for button btnCloseNoteWdw
	 */
	,onBtnCloseNoteWdw: function() {
		this.onNoteWdwClose();
		this._getWindow_("noteWdw").close();
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdw1
	 */
	,onHelpWdw1: function() {
		this.openHelp();
	}
	
	,onShowWdw: function() {	
		this._getWindow_("wdwLocation").show();
	}
	
	,showContactWdw: function() {	
		this._getWindow_("contactWdw").show();
	}
	
	,showNoteWdw: function() {	
		this._getWindow_("noteWdw").show();
	}
	
	,_afterDefineDcs_: function() {
		
		
					this._getDc_("location").on("afterDoNew", function (dc) {
						dc.setEditMode();
						this.onShowWdw();
					}, this);
		 	
					var contact = this._getDc_("contacts");
					var note = this._getDc_("note");
		
					note.on("afterDoQuerySuccess", function (dc) {
						var rec = dc.getRecord();
					    if (rec) {
							var date = rec.get("createdAt");
							var newDate = ("0" + date.getDate()).slice(-2)+"/"+("0" + (date.getMonth() + 1)).slice(-2)+"/"+date.getFullYear();
					    }
					}, this);
		
					note.on("afterDoNew", function (dc) {
					   this.showNoteWdw();
					}, this);
		
					contact.on("afterDoNew", function (dc) {
						dc.setEditMode();
						this.showContactWdw();
					}, this);
		
					note.on("afterDoEditIn", function (dc) {
						this._getWindow_("noteWdw").show();
					}, this);
		
					this._getDc_("contacts").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("contactWdw").close();
						}
					}, this);
		
					this._getDc_("note").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("noteWdw").close();
						}
					}, this);
					
					this._getDc_("location").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwLocation").close();
						} else if (ajaxResult.options.options.doCloseEditAfterSave === true) {
							this._getWindow_("wdwLocation").close();
							dc.doEditIn();
						}
					}, this);
	}
	
	,saveNew: function() {
		
					var loc = this._getDc_("location");
					loc.doSave({doNewAfterSave:true});
	}
	
	,saveClose: function() {
		
					var loc = this._getDc_("location");
					loc.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveEdit: function() {
		
					var loc = this._getDc_("location");
					loc.doSave({doCloseEditAfterSave: true });
	}
	
	,saveCloseContact: function() {
		
					var contact = this._getDc_("contacts");
					contact.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveCloseNote: function() {
		
					var note = this._getDc_("note");
					note.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveNewContact: function() {
		
					var cont = this._getDc_("contacts");
					cont.doSave({doNewAfterSave:true});
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/Locations.html";
					window.open( url, "SONE_Help");
	}
	
	,onContactWdwClose: function() {
		
						var dc = this._getDc_("contacts");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,onNoteWdwClose: function() {
		
						var dc = this._getDc_("note");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,onLocationWdwClose: function() {
		
						var dc = this._getDc_("location");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
});
