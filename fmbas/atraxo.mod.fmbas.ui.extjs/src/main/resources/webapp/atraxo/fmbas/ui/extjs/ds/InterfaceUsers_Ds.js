/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.InterfaceUsers_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_InterfaceUsers_Ds"
	},
	
	
	validators: {
		userName: [{type: 'presence'}],
		userEmail: [{type: 'presence'}],
		notificationCondition: [{type: 'presence'}],
		notificationType: [{type: 'presence'}]
	},
	
	fields: [
		{name:"interfaceId", type:"int", allowNull:true},
		{name:"interfaceName", type:"string"},
		{name:"userId", type:"string"},
		{name:"userName", type:"string"},
		{name:"userEmail", type:"string"},
		{name:"loginName", type:"string"},
		{name:"emailTemplateId", type:"int", allowNull:true},
		{name:"emailTemplateName", type:"string"},
		{name:"notificationCondition", type:"string"},
		{name:"enabled", type:"boolean"},
		{name:"notificationType", type:"string"},
		{name:"emailSubject", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.InterfaceUsers_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"interfaceId", type:"int", allowNull:true},
		{name:"interfaceName", type:"string"},
		{name:"userId", type:"string"},
		{name:"userName", type:"string"},
		{name:"userEmail", type:"string"},
		{name:"loginName", type:"string"},
		{name:"emailTemplateId", type:"int", allowNull:true},
		{name:"emailTemplateName", type:"string"},
		{name:"notificationCondition", type:"string"},
		{name:"enabled", type:"boolean", allowNull:true},
		{name:"notificationType", type:"string"},
		{name:"emailSubject", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
