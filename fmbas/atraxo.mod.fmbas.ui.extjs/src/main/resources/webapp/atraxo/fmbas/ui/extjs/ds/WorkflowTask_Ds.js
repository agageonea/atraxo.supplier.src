/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.WorkflowTask_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_WorkflowTask_Ds"
	},
	
	
	fields: [
		{name:"id", type:"int", allowNull:true},
		{name:"taskId", type:"string", noFilter:true, noSort:true},
		{name:"name", type:"string", noFilter:true, noSort:true},
		{name:"description", type:"string", noFilter:true, noSort:true},
		{name:"candidate", type:"string", noFilter:true, noSort:true},
		{name:"assignee", type:"string", noFilter:true, noSort:true},
		{name:"owner", type:"string", noFilter:true, noSort:true},
		{name:"process", type:"string", noFilter:true, noSort:true},
		{name:"workflowInstanceId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"workflowTaskNote", type:"string", noFilter:true, noSort:true},
		{name:"workflowTaskAcceptance", type:"boolean", noFilter:true, noSort:true},
		{name:"group", type:"string", noFilter:true, noSort:true},
		{name:"detail", type:"string", noFilter:true, noSort:true},
		{name:"subject", type:"string", noFilter:true, noSort:true},
		{name:"periodGroup", type:"string", noFilter:true, noSort:true},
		{name:"entityId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"entityFrame", type:"string", noFilter:true, noSort:true},
		{name:"entityModule", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.WorkflowTask_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"id", type:"int", allowNull:true},
		{name:"taskId", type:"string", noFilter:true, noSort:true},
		{name:"name", type:"string", noFilter:true, noSort:true},
		{name:"description", type:"string", noFilter:true, noSort:true},
		{name:"candidate", type:"string", noFilter:true, noSort:true},
		{name:"assignee", type:"string", noFilter:true, noSort:true},
		{name:"owner", type:"string", noFilter:true, noSort:true},
		{name:"process", type:"string", noFilter:true, noSort:true},
		{name:"workflowInstanceId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"workflowTaskNote", type:"string", noFilter:true, noSort:true},
		{name:"workflowTaskAcceptance", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"group", type:"string", noFilter:true, noSort:true},
		{name:"detail", type:"string", noFilter:true, noSort:true},
		{name:"subject", type:"string", noFilter:true, noSort:true},
		{name:"periodGroup", type:"string", noFilter:true, noSort:true},
		{name:"entityId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"entityFrame", type:"string", noFilter:true, noSort:true},
		{name:"entityModule", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.fmbas.ui.extjs.ds.WorkflowTask_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"workflowTaskName", type:"string"}
	]
});
