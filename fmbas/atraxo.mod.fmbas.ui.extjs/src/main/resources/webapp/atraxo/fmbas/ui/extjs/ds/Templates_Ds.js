/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Templates_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_Templates_Ds"
	},
	
	
	fields: [
		{name:"userId", type:"string"},
		{name:"userName", type:"string"},
		{name:"userCode", type:"string"},
		{name:"typeId", type:"int", allowNull:true},
		{name:"typeName", type:"string"},
		{name:"subsidiaryId", type:"int", allowNull:true},
		{name:"subsidiaryCode", type:"string"},
		{name:"subsidiaryName", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"fileName", type:"string"},
		{name:"uploadDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"fileReference", type:"string"},
		{name:"systemUse", type:"boolean"},
		{name:"file", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Templates_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"userId", type:"string"},
		{name:"userName", type:"string"},
		{name:"userCode", type:"string"},
		{name:"typeId", type:"int", allowNull:true},
		{name:"typeName", type:"string"},
		{name:"subsidiaryId", type:"int", allowNull:true},
		{name:"subsidiaryCode", type:"string"},
		{name:"subsidiaryName", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"fileName", type:"string"},
		{name:"uploadDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"fileReference", type:"string"},
		{name:"systemUse", type:"boolean", allowNull:true},
		{name:"file", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.fmbas.ui.extjs.ds.Templates_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"isUpdate", type:"boolean"},
		{name:"previewFile", type:"string"},
		{name:"templateId", type:"int", allowNull:true}
	]
});
