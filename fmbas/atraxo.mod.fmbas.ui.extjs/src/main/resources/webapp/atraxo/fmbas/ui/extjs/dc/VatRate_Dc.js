/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.VatRate_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.VatRate_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.VatRate_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_VatRate_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addNumberField({name:"rate", dataIndex:"rate", sysDec:"dec_crncy", maxLength:19})
			.addDateField({name:"validFrom", dataIndex:"validFrom"})
			.addDateField({name:"validTo", dataIndex:"validTo"})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.VatRate_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_VatRate_Dc$List",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addNumberColumn({name:"rate", dataIndex:"rate", width:200, allowBlank: false, sysDec:"dec_crncy", maxLength:19, align:"right",  renderer:function(val, meta, record, rowIndex) { return Ext.isEmpty(val)?"":val+"%"  ;} })
		.addDateColumn({name:"validFrom", dataIndex:"validFrom", width:200, allowBlank: false, _mask_: Masks.DATE })
		.addDateColumn({name:"validTo", dataIndex:"validTo", width:200, allowBlank: false, _mask_: Masks.DATE })
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
