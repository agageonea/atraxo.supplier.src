/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.NotificationEvent_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.NotificationEvent_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.NotificationEvent_Ds
});

/* ================= EDIT-GRID: ListEvents ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.NotificationEvent_Dc$ListEvents", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_NotificationEvent_Dc$ListEvents",
	_noPaginator_: true,
	selType: null,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addBooleanColumn({name:"isSelected", dataIndex:"isSelected", width:40,  hideLabel:"true", listeners:{checkchange: {scope: this, fn: function(el, rowIndex, checked, eOpts) {this._selectDeselectRow_( el, rowIndex, checked, eOpts);}}}})
		.addTextColumn({name:"name", dataIndex:"name", width:200, noEdit: true, maxLength:255})
		.addTextColumn({name:"description", dataIndex:"description", width:250, noEdit: true, maxLength:255})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_selectDeselectRow_: function(el,rowIndex,checked,eOpts) {
		
						var view = this.getView();
						if (checked == true) {
							this.getSelectionModel().select(rowIndex, true);
						}
						else {
							this.getSelectionModel().deselect(rowIndex, false);
						}
	}
});
