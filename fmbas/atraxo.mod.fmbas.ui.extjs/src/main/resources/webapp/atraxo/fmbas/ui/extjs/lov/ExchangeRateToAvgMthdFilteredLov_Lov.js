/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.ExchangeRateToAvgMthdFilteredLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_ExchangeRateToAvgMthdFilteredLov_Lov",
	displayField: "avgMthdName", 
	_columns_: ["avgMthdCode", "avgMthdName"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{avgMthdName}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.fmbas.ui.extjs.ds.ExchangeRateToFinancialSourceAndAvgFiltered_Ds
});
