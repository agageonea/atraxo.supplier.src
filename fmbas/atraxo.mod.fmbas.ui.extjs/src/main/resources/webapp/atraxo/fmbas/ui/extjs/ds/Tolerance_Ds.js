/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Tolerance_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_Tolerance_Ds"
	},
	
	
	validators: {
		name: [{type: 'presence'}],
		parameterType: [{type: 'presence'}],
		description: [{type: 'presence'}],
		toleranceType: [{type: 'presence'}],
		tolLmtInd: [{type: 'presence'}]
	},
	
	fields: [
		{name:"percent", type:"string", noFilter:true, noSort:true},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"currId", type:"int", allowNull:true},
		{name:"currCode", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"parameterType", type:"string"},
		{name:"toleranceFor", type:"string"},
		{name:"absDevDown", type:"float", allowNull:true},
		{name:"absDevUp", type:"float", allowNull:true},
		{name:"relativeDevDown", type:"float", allowNull:true},
		{name:"relativeDevUp", type:"float", allowNull:true},
		{name:"toleranceType", type:"string"},
		{name:"system", type:"boolean"},
		{name:"tolLmtInd", type:"string"},
		{name:"active", type:"boolean"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Tolerance_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"percent", type:"string", noFilter:true, noSort:true},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"currId", type:"int", allowNull:true},
		{name:"currCode", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"parameterType", type:"string"},
		{name:"toleranceFor", type:"string"},
		{name:"absDevDown", type:"float", allowNull:true},
		{name:"absDevUp", type:"float", allowNull:true},
		{name:"relativeDevDown", type:"float", allowNull:true},
		{name:"relativeDevUp", type:"float", allowNull:true},
		{name:"toleranceType", type:"string"},
		{name:"system", type:"boolean", allowNull:true},
		{name:"tolLmtInd", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
