/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.Customers_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Customers_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("customer", Ext.create(atraxo.fmbas.ui.extjs.dc.Customers_Dc,{trackEditMode: true}))
		.addDc("finalCust", Ext.create(atraxo.fmbas.ui.extjs.dc.FinalCustomer_Dc,{}))
		.addDc("attachment", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("contact", Ext.create(atraxo.fmbas.ui.extjs.dc.Contacts_Dc,{}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("tradeReference", Ext.create(atraxo.fmbas.ui.extjs.dc.TradeReference_Dc,{}))
		.addDc("masterAgreement", Ext.create(atraxo.fmbas.ui.extjs.dc.MasterAgrement_Dc,{}))
		.addDc("masterHistory", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("aircraft", Ext.create(atraxo.fmbas.ui.extjs.dc.Aircraft_Dc,{multiEdit: true}))
		.addDc("notifications", Ext.create(atraxo.fmbas.ui.extjs.dc.CustomerNotification_Dc,{multiEdit: true}))
		.addDc("notificationEvent", Ext.create(atraxo.fmbas.ui.extjs.dc.NotificationEvent_Dc,{multiEdit: true}))
		.addDc("notifEvent", Ext.create(atraxo.fmbas.ui.extjs.dc.NotificationEventList_Dc,{}))
		.addDc("creditLine", Ext.create(atraxo.fmbas.ui.extjs.dc.CustomerCreditLine_Dc,{}))
		.addDc("creditLineHistory", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.linkDc("finalCust", "customer",{fetchMode:"auto",fields:[
					{childParam:"customerId", parentField:"id"}]})
				.linkDc("attachment", "customer",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}]})
				.linkDc("contact", "customer",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("note", "customer",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("history", "customer",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("tradeReference", "customer",{fetchMode:"auto",fields:[
					{childField:"companyId", parentField:"id"}]})
				.linkDc("masterAgreement", "customer",{fetchMode:"auto",fields:[
					{childField:"companyId", parentField:"id"}]})
				.linkDc("masterHistory", "masterAgreement",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("aircraft", "customer",{fetchMode:"auto",fields:[
					{childField:"custId", parentField:"id"}, {childField:"ownerCode", parentField:"code"}, {childField:"operCode", parentField:"code"}]})
				.linkDc("notifications", "customer",{fetchMode:"auto",fields:[
					{childField:"customerId", parentField:"id"}]})
				.linkDc("notificationEvent", "contact",{fetchMode:"auto",fields:[
					{childField:"clientId", parentField:"clientId"}, {childParam:"contactId", parentField:"id"}]})
				.linkDc("notifEvent", "contact",{fetchMode:"auto",fields:[
					{childParam:"contactId", parentField:"id"}]})
				.linkDc("creditLine", "customer",{fetchMode:"auto",fields:[
					{childField:"companyId", parentField:"id"}]})
				.linkDc("creditLineHistory", "creditLine",{fetchMode:"auto",reloadChildrenOnParentUpdate:true,fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnOnEditWdwClose",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnOnEditWdwClose, scope:this})
		.addButton({name:"btnStatusCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnStatusCancel,stateManager:[{ name:"record_is_dirty", dc:"history"}], scope:this})
		.addButton({name:"btnSaveCloseWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseWdw, scope:this})
		.addButton({name:"btnSaveNewWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewWdw, scope:this})
		.addButton({name:"btnSaveEditWdw",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnSaveEditWdw, scope:this})
		.addButton({name:"btnSaveNewContactWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewContactWdw, scope:this})
		.addButton({name:"btnSaveNewTradeRefWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewTradeRefWdw, scope:this})
		.addButton({name:"btnSaveNewMasterAgreementWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewMasterAgreementWdw, scope:this})
		.addButton({name:"btnSaveCloseContactWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseContactWdw, scope:this})
		.addButton({name:"btnSaveCloseMasterAgreementWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseMasterAgreementWdw, scope:this})
		.addButton({name:"btnCloseContactWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseContactWdw, scope:this})
		.addButton({name:"btnCloseMasterAgreementWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseMasterAgreementWdw, scope:this})
		.addButton({name:"btnCloseTradeRefWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseTradeRefWdw, scope:this})
		.addButton({name:"btnCloseNoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseNoteWdw, scope:this})
		.addButton({name:"btnSaveCloseNoteWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseNoteWdw, scope:this})
		.addButton({name:"btnSaveCloseTradeRefWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseTradeRefWdw, scope:this})
		.addButton({name:"btnSaveCloseHistoryWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseHistoryWdw, scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdwEditor",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdwEditor, scope:this})
		.addButton({name:"btnApprove",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApprove,stateManager:[{ name:"selected_not_zero", dc:"customer", and: function(dc) {return (this.canApprove(dc) && dc.isReadOnly() == false);} }], scope:this})
		.addButton({name:"btnApproveEdit",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApproveEdit,stateManager:[{ name:"selected_one", dc:"customer", and: function(dc) {return (dc.record && (dc.record.data.status==__FMBAS_TYPE__.CustomerStatus._PROSPECT_ || dc.record.data.status==__FMBAS_TYPE__.CustomerStatus._INACTIVE_) && dc.isReadOnly() == false && dc.isDirty() == false);} }], scope:this})
		.addButton({name:"btnBlock",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnBlock,stateManager:[{ name:"selected_one", dc:"customer", and: function(dc) {return (dc.record && (dc.record.data.status==__FMBAS_TYPE__.CustomerStatus._ACTIVE_ || dc.record.data.status==__FMBAS_TYPE__.CustomerStatus._PROSPECT_) && dc.isReadOnly() == false);} }], scope:this})
		.addButton({name:"btnBlockEdit",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnBlockEdit,stateManager:[{ name:"selected_one", dc:"customer", and: function(dc) {return (dc.record && (dc.record.data.status==__FMBAS_TYPE__.CustomerStatus._ACTIVE_ || dc.record.data.status==__FMBAS_TYPE__.CustomerStatus._PROSPECT_) && dc.isReadOnly() == false && dc.isDirty() == false);} }], scope:this})
		.addButton({name:"btnSaveCloseBlockHistoryWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseBlockHistoryWdw, scope:this})
		.addButton({name:"btnBlockCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnBlockCancel, scope:this})
		.addButton({name:"btnRequestCreditInfo",glyph:fp_asc.level_up.glyph, disabled:true, handler: this.onBtnRequestCreditInfo,stateManager:[{ name:"selected_one", dc:"customer", and: function(dc) {return (dc.record && dc.record.data.creditUpdateRequestStatus!=__FMBAS_TYPE__.CreditUpdateRequestStatus._IN_PROGRESS_ && dc.isDirty() == false);} }], scope:this})
		.addButton({name:"btnRemoveAssign",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onBtnRemoveAssign, scope:this})
		.addButton({name:"btnPromote",glyph:fp_asc.commit_glyph.glyph,iconCls: fp_asc.commit_glyph.css, disabled:false, handler: this.onBtnPromote, scope:this})
		.addButton({name:"assignCustomers",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onAssignCustomers, scope:this})
		.addButton({name:"btnFinalCustWdwClose",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnFinalCustWdwClose, scope:this})
		.addButton({name:"btnSaveCloseFinalCustWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseFinalCustWdw, scope:this})
		.addButton({name:"btnSaveNewFinalCustWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewFinalCustWdw, scope:this})
		.addButton({name:"btnFinalCustWdwEditClose",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnFinalCustWdwEditClose, scope:this})
		.addButton({name:"btnSaveCloseFinalCustEditWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseFinalCustEditWdw, scope:this})
		.addButton({name:"btnAssignContactNotif",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnAssignContactNotif, scope:this})
		.addButton({name:"btnCancelAssign",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelAssign, scope:this})
		.addButton({name:"btnAssignNotifEvents",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnAssignNotifEvents, scope:this})
		.addButton({name:"btnRemoveNotifEvents",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onBtnRemoveNotifEvents, scope:this})
		.addButton({name:"btnExportWithMenu",glyph:fp_asc.send_glyph.glyph,iconCls: fp_asc.send_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"customer"}], scope:this})
		.addButton({name:"btnExportSelected",glyph:fp_asc.send_glyph.glyph,iconCls: fp_asc.send_glyph.css, disabled:true,  _btnContainerName_:"btnExportWithMenu", _isDefaultHandler_:true, handler: this.onBtnExportSelected,stateManager:[{ name:"selected_not_zero", dc:"customer", and: function(dc) {return (this.canExport(dc));} }], scope:this})
		.addButton({name:"btnExportFailed",glyph:fp_asc.send_glyph.glyph,iconCls: fp_asc.send_glyph.css, disabled:false,  _btnContainerName_:"btnExportWithMenu", handler: this.onBtnExportFailed, scope:this})
		.addButton({name:"btnCloseCreditWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseCreditWdw, scope:this})
		.addButton({name:"btnSaveNewCreditWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewCreditWdw, scope:this})
		.addButton({name:"btnSaveCloseCreditWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseCreditWdw, scope:this})
		.addButton({name:"btnSaveCreditLine",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSaveCreditLine,stateManager:[{ name:"selected_one_dirty", dc:"creditLine", and: function(dc) {return (this.fnSaveEnabled(dc));} }], scope:this})
		.addButton({name:"btnSaveCloseCLHistoryWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseCLHistoryWdw, scope:this})
		.addDcFilterFormView("customer", {name:"customerFilter", xtype:"fmbas_Customers_Dc$Filter"})
		.addDcGridView("customer", {name:"customerList", xtype:"fmbas_Customers_Dc$List"})
		.addDcFormView("customer", {name:"customerEdit", xtype:"fmbas_Customers_Dc$Edit", _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcFormView("customer", {name:"customerAddresses", _hasTitle_:true, xtype:"fmbas_Customers_Dc$Addresses"})
		.addDcFormView("customer", {name:"customerNewPopup", xtype:"fmbas_Customers_Dc$NewCustomerPopup"})
		.addDcFormView("customer", {name:"customerEditContext", xtype:"fmbas_Customers_Dc$Edit"})
		.addDcGridView("finalCust", {name:"finalCustList", _hasTitle_:true, xtype:"fmbas_FinalCustomer_Dc$List"})
		.addDcFormView("finalCust", {name:"finalCustNew", xtype:"fmbas_FinalCustomer_Dc$Edit"})
		.addDcFormView("finalCust", {name:"finalCustEdit", xtype:"fmbas_FinalCustomer_Dc$Edit"})
		.addDcFilterFormView("aircraft", {name:"aircraft", xtype:"fmbas_Aircraft_Dc$Filter"})
		.addDcEditGridView("aircraft", {name:"aircraftsList", _hasTitle_:true, xtype:"fmbas_Aircraft_Dc$List", frame:true})
		.addDcFormView("history", {name:"HistoryEdit", xtype:"fmbas_ChangeHistory_Dc$Edit"})
		.addDcGridView("history", {name:"HistoryList", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFilterFormView("contact", {name:"contactFilter", xtype:"fmbas_Contacts_Dc$Filter"})
		.addDcGridView("contact", {name:"contactsList", _hasTitle_:true, xtype:"fmbas_Contacts_Dc$List"})
		.addDcFormView("contact", {name:"contactEdit", xtype:"fmbas_Contacts_Dc$Edit"})
		.addDcFormView("contact", {name:"contactTab", _hasTitle_:true, xtype:"fmbas_Contacts_Dc$Tab"})
		.addDcFormView("contact", {name:"contactEditContext", xtype:"fmbas_Contacts_Dc$EditDetails"})
		.addDcGridView("note", {name:"notesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcFormView("note", {name:"notesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addDcFilterFormView("tradeReference", {name:"tradeFilter", xtype:"fmbas_TradeReference_Dc$Filter"})
		.addDcGridView("tradeReference", {name:"tradeRefList", _hasTitle_:true, xtype:"fmbas_TradeReference_Dc$List"})
		.addDcFormView("tradeReference", {name:"tradeRefEdit", xtype:"fmbas_TradeReference_Dc$Edit"})
		.addDcFormView("tradeReference", {name:"tradeRefEditContext", xtype:"fmbas_TradeReference_Dc$Edit"})
		.addDcFilterFormView("masterAgreement", {name:"masterAgrementsFilter", xtype:"fmbas_MasterAgrement_Dc$Filter"})
		.addDcGridView("masterAgreement", {name:"masterAgrementsList", _hasTitle_:true, xtype:"fmbas_MasterAgrement_Dc$List"})
		.addDcFormView("masterAgreement", {name:"masterAgrementsEdit", xtype:"fmbas_MasterAgrement_Dc$Edit"})
		.addDcFormView("masterAgreement", {name:"masterAgrementsEditContext", xtype:"fmbas_MasterAgrement_Dc$Edit", _acquireFocusUpdate_: false})
		.addDcGridView("masterHistory", {name:"masterHistoryList", xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("masterHistory", {name:"masterHistoryEdit", xtype:"fmbas_ChangeHistory_Dc$EditRemark"})
		.addDcFormView("customer", {name:"BlockRemarkEdit", xtype:"fmbas_Customers_Dc$EditRemark"})
		.addDcGridView("attachment", {name:"attachmentList", _hasTitle_:true, xtype:"ad_Attachment_Dc$List"})
		.addDcFormView("attachment", {name:"attachmentEdit", xtype:"ad_Attachment_Dc$New"})
		.addDcEditGridView("notifications", {name:"notificationList", _hasTitle_:true, xtype:"fmbas_CustomerNotification_Dc$List", frame:true})
		.addDcFilterFormView("notifications", {name:"notificationFiler", xtype:"fmbas_CustomerNotification_Dc$Filter"})
		.addDcEditGridView("notificationEvent", {name:"notifEventList", xtype:"fmbas_NotificationEvent_Dc$ListEvents", frame:true})
		.addDcGridView("notifEvent", {name:"neList", _hasTitle_:true, xtype:"fmbas_NotificationEventList_Dc$List"})
		.addDcFilterFormView("creditLine", {name:"creditLineFilter", xtype:"fmbas_CustomerCreditLine_Dc$Filter"})
		.addDcGridView("creditLine", {name:"creditLineList", _hasTitle_:true, xtype:"fmbas_CustomerCreditLine_Dc$List"})
		.addDcFormView("creditLine", {name:"creditLineEdit", xtype:"fmbas_CustomerCreditLine_Dc$Edit", _acquireFocusUpdate_: false})
		.addDcFormView("creditLine", {name:"creditLineEditContext", xtype:"fmbas_CustomerCreditLine_Dc$EditContext"})
		.addDcGridView("creditLineHistory", {name:"creditLineHistoryList", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("creditLine", {name:"creditLineHistoryEdit", xtype:"fmbas_CustomerCreditLine_Dc$CreditHistory"})
		.addWindow({name:"wdwCustomer", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("customerNewPopup")],  listeners:{ close:{fn:this.onWdwCustomerClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewWdw"), this._elems_.get("btnSaveCloseWdw"), this._elems_.get("btnSaveEditWdw"), this._elems_.get("btnOnEditWdwClose")]}]})
		.addWindow({name:"contactWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("contactEdit")],  listeners:{ close:{fn:this.onContactWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewContactWdw"), this._elems_.get("btnSaveCloseContactWdw"), this._elems_.get("btnCloseContactWdw")]}]})
		.addWindow({name:"masterAgreementWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("masterAgrementsEdit")],  listeners:{ close:{fn:this.onMasterAgreementWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseMasterAgreementWdw"), this._elems_.get("btnCloseMasterAgreementWdw")]}]})
		.addWindow({name:"noteWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("notesEdit")],  listeners:{ close:{fn:this.onNoteWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseNoteWdw"), this._elems_.get("btnCloseNoteWdw")]}]})
		.addWindow({name:"tradeRefWdw", _hasTitle_:true, width:360, height:440, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("tradeRefEdit")],  listeners:{ close:{fn:this.onTradeRefWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewTradeRefWdw"), this._elems_.get("btnSaveCloseTradeRefWdw"), this._elems_.get("btnCloseTradeRefWdw")]}]})
		.addWindow({name:"masterAgreementHistoryWdw", _hasTitle_:true, width:440, height:220, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("masterHistoryEdit")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseHistoryWdw")]}]})
		.addWindow({name:"finalCustomerWdw", _hasTitle_:true, width:360, height:240, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("finalCustNew")],  listeners:{ close:{fn:this.onWdwFinalCustomerClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseFinalCustWdw"), this._elems_.get("btnSaveNewFinalCustWdw"), this._elems_.get("btnFinalCustWdwClose")]}]})
		.addWindow({name:"finalCustomerEditWdw", _hasTitle_:true, width:360, height:240, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("finalCustEdit")],  listeners:{ close:{fn:this.onWdwFinalCustomerEditClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseFinalCustEditWdw"), this._elems_.get("btnFinalCustWdwEditClose")]}]})
		.addWindow({name:"wdwBlockReason", _hasTitle_:true, width:440, height:220, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("BlockRemarkEdit")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseBlockHistoryWdw"), this._elems_.get("btnBlockCancel")]}]})
		.addWindow({name:"wdwNotifAssign", _hasTitle_:true, width:520, height:380, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("notifEventList")],  listeners:{ close:{fn:this.onwdwAssignClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnAssignNotifEvents"), this._elems_.get("btnCancelAssign")]}]})
		.addWindow({name:"creditLineWdw", _hasTitle_:true, width:350, height:325, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("creditLineEdit")],  listeners:{ close:{fn:this.onCreditLineWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewCreditWdw"), this._elems_.get("btnSaveCloseCreditWdw"), this._elems_.get("btnCloseCreditWdw")]}]})
		.addWindow({name:"creditLineHistoryWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("creditLineHistoryEdit")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseCLHistoryWdw")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas4", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas5", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas6", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTabContact", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"detailsTabContact", containerPanelName:"canvas3", dcName:"contact"})
		
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas2", dcName:"customer"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["customerList"], ["center"])
		.addChildrenTo("canvas2", ["customerEdit", "detailsTab"], ["north", "center"])
		.addChildrenTo("canvas3", ["contactEditContext", "detailsTabContact"], ["north", "center"])
		.addChildrenTo("canvas4", ["tradeRefEditContext"], ["center"])
		.addChildrenTo("canvas5", ["masterAgrementsEditContext", "masterHistoryList"], ["north", "center"])
		.addChildrenTo("canvas6", ["creditLineEditContext", "creditLineHistoryList"], ["north", "center"])
		.addChildrenTo("detailsTabContact", ["contactTab"])
		.addChildrenTo("detailsTab", ["customerAddresses"])
		.addToolbarTo("customerList", "tlbCustomerList")
		.addToolbarTo("customerEdit", "tlbCustomerContext")
		.addToolbarTo("contactsList", "tlbContacts")
		.addToolbarTo("contactEditContext", "tlbContactContext")
		.addToolbarTo("notesList", "tlbNoteList")
		.addToolbarTo("tradeRefEditContext", "tlbTradeRefContext")
		.addToolbarTo("tradeRefList", "tlbTradeRefEdit")
		.addToolbarTo("masterAgrementsList", "tlbMasterAgreementEdit")
		.addToolbarTo("masterAgrementsEditContext", "tlbMasterAgreementContext")
		.addToolbarTo("aircraftsList", "tlbAircraft")
		.addToolbarTo("attachmentList", "tlbAttachments")
		.addToolbarTo("finalCustList", "tlbFinalCustomer")
		.addToolbarTo("notificationList", "tlbNotifications")
		.addToolbarTo("neList", "tlbContactNotif")
		.addToolbarTo("creditLineList", "tlbCreditLineList")
		.addToolbarTo("creditLineEditContext", "tlbCreditLineEditContext");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2", "canvas3", "canvas4", "canvas5", "canvas6"])
		.addChildrenTo2("detailsTabContact", ["neList"])
		.addChildrenTo2("detailsTab", ["contactsList", "finalCustList", "creditLineList", "masterAgrementsList", "aircraftsList", "tradeRefList", "notificationList", "notesList", "attachmentList", "HistoryList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbCustomerList", {dc: "customer"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnApprove"),this._elems_.get("btnBlock"),this._elems_.get("btnExportWithMenu"),this._elems_.get("btnExportSelected"),this._elems_.get("btnExportFailed"),this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbCustomerContext", {dc: "customer"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnApproveEdit"),this._elems_.get("btnBlockEdit"),this._elems_.get("btnRequestCreditInfo"),this._elems_.get("helpWdwEditor")])
			.addReports()
		.end()
		.beginToolbar("tlbContacts", {dc: "contact"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas3"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbContactContext", {dc: "contact"})
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas2"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbNoteList", {dc: "note"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbTradeRefContext", {dc: "tradeReference"})
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas2"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbTradeRefEdit", {dc: "tradeReference"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas4"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbMasterAgreementEdit", {dc: "masterAgreement"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas5"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbMasterAgreementContext", {dc: "masterAgreement"})
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas2"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbAircraft", {dc: "aircraft"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbAttachments", {dc: "attachment"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbFinalCustomer", {dc: "finalCust"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("assignCustomers"),this._elems_.get("btnRemoveAssign"),this._elems_.get("btnPromote")])
			.addReports()
		.end()
		.beginToolbar("tlbNotifications", {dc: "notifications"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbContactNotif", {dc: "notifEvent"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnAssignContactNotif"),this._elems_.get("btnRemoveNotifEvents")])
			.addReports()
		.end()
		.beginToolbar("tlbCreditLineList", {dc: "creditLine"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas6"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbCreditLineEditContext", {dc: "creditLine"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas2"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnSaveCreditLine")])
			.addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addPrevRec({glyph:fp_asc.prev_glyph.glyph})
			.addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end();
	},
			
			/**
			 * Frame monitoring list definition
			 */
			_defineFrameMonList_: function() {
				this._getBuilder_()
				.addFrameMon("FuelTicket_Ui",{_dcName_:"aircraft", _eventName_:"afterDoSaveSuccess", _fn_:this._reloadAircraftDc_})
				;
			}
	
	,_applyTabLoadRestriction_: function(){
		this._getBuilder_()
		.addTabLoadRestriction("contact","detailsTab",["contactsList"])
		.addTabLoadRestriction("finalCust","detailsTab",["finalCustList"])
		.addTabLoadRestriction("note","detailsTab",["notesList"])
		.addTabLoadRestriction("tradeReference","detailsTab",["tradeRefList"])
		.addTabLoadRestriction("creditLine","detailsTab",["creditLineList"])
		.addTabLoadRestriction("masterAgreement","detailsTab",["masterAgrementsList"])
		.addTabLoadRestriction("aircraft","detailsTab",["aircraftsList"])
		.addTabLoadRestriction("history","detailsTab",["HistoryList"])
		.addTabLoadRestriction("attachment","detailsTab",["attachmentList"])
		.addTabLoadRestriction("notifications","detailsTab",["notificationList"])
		;
	}

	
	/**
	 * On-Click handler for button btnOnEditWdwClose
	 */
	,onBtnOnEditWdwClose: function() {
		this.onWdwCustomerClose();
		this._getWindow_("wdwCustomer").close();
	}
	
	/**
	 * On-Click handler for button btnStatusCancel
	 */
	,onBtnStatusCancel: function() {
		this.cancelStatusHistory();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseWdw
	 */
	,onBtnSaveCloseWdw: function() {
		this.saveClose();
	}
	
	/**
	 * On-Click handler for button btnSaveNewWdw
	 */
	,onBtnSaveNewWdw: function() {
		this.saveNew();
	}
	
	/**
	 * On-Click handler for button btnSaveEditWdw
	 */
	,onBtnSaveEditWdw: function() {
		this.saveEdit();
	}
	
	/**
	 * On-Click handler for button btnSaveNewContactWdw
	 */
	,onBtnSaveNewContactWdw: function() {
		this.saveNewContact();
	}
	
	/**
	 * On-Click handler for button btnSaveNewTradeRefWdw
	 */
	,onBtnSaveNewTradeRefWdw: function() {
		this.saveNewTradeRef();
	}
	
	/**
	 * On-Click handler for button btnSaveNewMasterAgreementWdw
	 */
	,onBtnSaveNewMasterAgreementWdw: function() {
		this.saveNewMasterAgreement();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseContactWdw
	 */
	,onBtnSaveCloseContactWdw: function() {
		this.saveCloseContact();
		this._getDc_("contact").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseMasterAgreementWdw
	 */
	,onBtnSaveCloseMasterAgreementWdw: function() {
		this.saveCloseMasterAgreement();
		this._getDc_("masterAgreement").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCloseContactWdw
	 */
	,onBtnCloseContactWdw: function() {
		this.onContactWdwClose();
		this._getWindow_("contactWdw").close();
	}
	
	/**
	 * On-Click handler for button btnCloseMasterAgreementWdw
	 */
	,onBtnCloseMasterAgreementWdw: function() {
		this.onMasterAgreementWdwClose();
		this._getWindow_("masterAgreementWdw").close();
	}
	
	/**
	 * On-Click handler for button btnCloseTradeRefWdw
	 */
	,onBtnCloseTradeRefWdw: function() {
		this.onTradeRefWdwClose();
		this._getWindow_("tradeRefWdw").close();
	}
	
	/**
	 * On-Click handler for button btnCloseNoteWdw
	 */
	,onBtnCloseNoteWdw: function() {
		this.onNoteWdwClose();
		this._getWindow_("noteWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseNoteWdw
	 */
	,onBtnSaveCloseNoteWdw: function() {
		this.saveCloseNote();
		this._getDc_("note").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseTradeRefWdw
	 */
	,onBtnSaveCloseTradeRefWdw: function() {
		this.saveCloseTradeRef();
		this._getDc_("note").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseHistoryWdw
	 */
	,onBtnSaveCloseHistoryWdw: function() {
		this.saveCloseHistory();
		this._getDc_("masterHistory").doReloadPage();
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openCustomerHelp();
	}
	
	/**
	 * On-Click handler for button helpWdwEditor
	 */
	,onHelpWdwEditor: function() {
		this.openCustomerHelp();
	}
	
	/**
	 * On-Click handler for button btnApprove
	 */
	,onBtnApprove: function() {
		var successFn = function() {
			this._getDc_("history").doReloadPage();
			this._applyStateAllButtons_()
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"approveList",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("customer").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnApproveEdit
	 */
	,onBtnApproveEdit: function() {
		var successFn = function() {
			this._getDc_("history").doReloadPage();
			this._applyStateAllButtons_()
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"approve",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("customer").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnBlock
	 */
	,onBtnBlock: function() {
		this.block();
	}
	
	/**
	 * On-Click handler for button btnBlockEdit
	 */
	,onBtnBlockEdit: function() {
		this.block();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseBlockHistoryWdw
	 */
	,onBtnSaveCloseBlockHistoryWdw: function() {
		var successFn = function(dc) {
			this._getWindow_("wdwBlockReason").close();
			dc.setParamValue("remark","")
			this._getDc_("history").doReloadPage();
			this._applyStateAllButtons_()
		};
		var failureFn = function(dc,response) {
			this._getWindow_("wdwBlockReason").close();
			Main.rpcFailure(response);
		};
		var o={
			name:"deactivate",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("customer").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnBlockCancel
	 */
	,onBtnBlockCancel: function() {
		this._getWindow_("wdwBlockReason").close();
	}
	
	/**
	 * On-Click handler for button btnRequestCreditInfo
	 */
	,onBtnRequestCreditInfo: function() {
		var o={
			name:"requestCreditInfo",
			modal:true
		};
		this._getDc_("customer").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnRemoveAssign
	 */
	,onBtnRemoveAssign: function() {
		var successFn = function() {
			this._getDc_("finalCust").doReloadPage();
			this._getDc_("customer").doReloadRecord();
		};
		var o={
			name:"remove",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("finalCust").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnPromote
	 */
	,onBtnPromote: function() {
		this.warningPromote();
	}
	
	/**
	 * On-Click handler for button assignCustomers
	 */
	,onAssignCustomers: function() {
		this._showAsgnWindow_("atraxo.fmbas.ui.extjs.asgn.FinalCustomerResseler_Asgn$Ui" ,{dc: "customer", objectIdField: "id"
		,listeners: {close: {scope: this, fn: function() { this._getDc_("finalCust").doReloadPage();
		}} }});
	}
	
	/**
	 * On-Click handler for button btnFinalCustWdwClose
	 */
	,onBtnFinalCustWdwClose: function() {
		this.onWdwFinalCustomerClose();
		this._getWindow_("finalCustomerWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseFinalCustWdw
	 */
	,onBtnSaveCloseFinalCustWdw: function() {
		this.saveCloseFinalCust();
	}
	
	/**
	 * On-Click handler for button btnSaveNewFinalCustWdw
	 */
	,onBtnSaveNewFinalCustWdw: function() {
		this.saveNewFinalCust();
	}
	
	/**
	 * On-Click handler for button btnFinalCustWdwEditClose
	 */
	,onBtnFinalCustWdwEditClose: function() {
		this.onWdwFinalCustomerEditClose();
		this._getWindow_("finalCustomerEditWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseFinalCustEditWdw
	 */
	,onBtnSaveCloseFinalCustEditWdw: function() {
		this.saveCloseFinalCustEdit();
	}
	
	/**
	 * On-Click handler for button btnAssignContactNotif
	 */
	,onBtnAssignContactNotif: function() {
		this._showAssignWdw_();
	}
	
	/**
	 * On-Click handler for button btnCancelAssign
	 */
	,onBtnCancelAssign: function() {
		this._getDc_("notificationEvent").doCancel();
		this._getWindow_("wdwNotifAssign").close();
	}
	
	/**
	 * On-Click handler for button btnAssignNotifEvents
	 */
	,onBtnAssignNotifEvents: function() {
		var successFn = function() {
			this._getWindow_("wdwNotifAssign").close();
			this._getDc_("contact").doReloadPage();
		};
		var o={
			name:"assign",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("notificationEvent").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnRemoveNotifEvents
	 */
	,onBtnRemoveNotifEvents: function() {
		var successFn = function() {
			this._getDc_("contact").doReloadPage();
		};
		var o={
			name:"remove",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("notifEvent").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnExportSelected
	 */
	,onBtnExportSelected: function() {
		this._exportCustomersToNAV('selected')
	}
	
	/**
	 * On-Click handler for button btnExportFailed
	 */
	,onBtnExportFailed: function() {
		this._exportCustomersToNAV('failed')
	}
	
	/**
	 * On-Click handler for button btnCloseCreditWdw
	 */
	,onBtnCloseCreditWdw: function() {
		this.onCreditLineWdwClose();
		this._getWindow_("creditLineWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveNewCreditWdw
	 */
	,onBtnSaveNewCreditWdw: function() {
		this.saveNewCredit();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseCreditWdw
	 */
	,onBtnSaveCloseCreditWdw: function() {
		this.saveCloseCredit();
	}
	
	/**
	 * On-Click handler for button btnSaveCreditLine
	 */
	,onBtnSaveCreditLine: function() {
		this._getDc_("creditLine").doSave();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseCLHistoryWdw
	 */
	,onBtnSaveCloseCLHistoryWdw: function() {
		this._getDc_("creditLine").setParamValue("updateReason",this._get_("creditLineHistoryEdit")._get_("changeReason").getValue())
		var successFn = function() {
			this._getDc_("creditLine").doCancel();
			this._getWindow_("creditLineHistoryWdw").close();
			this._getDc_("customer").doReloadRecord();
		};
		var o={
			name:"customSave",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("creditLine").doRpcData(o);
	}
	
	,onWdwStatusClose: function() {	
		this._getDc_("history").doCancel();
	}
	
	,showNoteWdw: function() {	
		this._getWindow_("noteWdw").show();
	}
	
	,showContactWdw: function() {	
		this._getWindow_("contactWdw").show();
	}
	
	,showMasterAgreementWdw: function() {	
		this._getWindow_("masterAgreementWdw").show();
	}
	
	,showTradeRefWdw: function() {	
		this._getWindow_("tradeRefWdw").show();
	}
	
	,_onShow_from_externalInterfaceMessageHistory_: function(params) {
			
						var dc = this._getDc_("customer");
						dc.setFilterValue("id", params.objectId);
						dc.doQuery({queryFromExternalInterface: true});
	}
	
	,_exportCustomer_: function() {
		
						var customer = this._getDc_("customer");
						var callbackFn = function(dc,response,serviceName,specs) {
		
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
		
								var responseDescription = responseData.params.exportCustomerDescription;							
								var responseResult = responseData.params.exportCustomerResult;
		
								if (responseResult == true) {
									Main.info(Main.encodeHTML(responseDescription)); 
								}
								else {
									Main.error(Main.encodeHTML(responseDescription));
								}
							}
						};
						var o={
							name:"exportFuelPlusToAzureCustomer",
							callbacks:{
								successFn: callbackFn,
								successScope: this,
								failureFn : callbackFn,
								failureScope : this
							},
							modal:true
						};
						customer.doRpcDataList(o);
	}
	
	,_showAssignWdw_: function() {
		
						var win = this._getWindow_("wdwNotifAssign");
						var grid = this._get_("notifEventList");
						win.show(undefined, function() {
							var dc = this._getDc_("notificationEvent");
							var s = dc.store;
							s.load({
							    callback: function (records, operation, success) { 
							        for (var i =0; i < records.length; i++) {
										var isSelected = records[i].get("isSelected");
										var rowIndex = grid.store.indexOf(records[i]);
										if (isSelected == true) {
											grid.getSelectionModel().select(rowIndex, true);
										}
										else {
											grid.getSelectionModel().deselect(rowIndex);
										}
									}
							    }
							})
							
						}, this);
	}
	
	,onwdwAssignClose: function() {
		
						this._get_("notifEventList").getSelectionModel().deselectAll();
						var dc = this._getDc_("notificationEvent");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						} 
	}
	
	,_reloadAircraftDc_: function() {
		
						var dc = this._getDc_("aircraft");
						dc.doReloadPage();
	}
	
	,warningPromote: function() {
		
		
					var finalCust = this._getDc_("finalCust");
		
					Ext.Msg.show({
							    title: "Warning",
							    msg: "The selected final customer(s) are going to be converted to full customer accounts.",
							    buttons: Ext.MessageBox.YES + Ext.MessageBox.NO,
							    fn: function(btnId) {
							        if (btnId == "yes") {
							            var successFn = function(dc,response,serviceName,specs) {
											finalCust.doReloadRecord();
										};
										var o={
											name:"promote",
											callbacks:{
												successFn: successFn,
												successScope: this
											},
											modal:true
										};
										finalCust.doRpcDataList(o);
							        }
							    },
								buttonText: {
					                yes: "Ok", no: "Cancel"
					            },
							    scope: this
							});
	}
	
	,_onTabChanged_: function() {
		
						this._getDc_("contract").doReloadPage();
	}
	
	,setActiveTabByTitle: function(tabPanel,tab) {
		
						tabPanel.setActiveTab(tab);
	}
	
	,_afterDefineDcs_: function() {
		
		
					var customer = this._getDc_("customer");
					var contact = this._getDc_("contact");
					var note = this._getDc_("note");
					var tradeReference = this._getDc_("tradeReference");
					var masterAgreement = this._getDc_("masterAgreement");
					var masterHistory = this._getDc_("masterHistory");
					var aircraft = this._getDc_("aircraft");
					var finalCust = this._getDc_("finalCust");
					var creditLine = this._getDc_("creditLine");
					var creditLineHistory = this._getDc_("creditLineHistory");
		
					customer.on("showWindow", function(dc) {
						this._getWindow_("wdwStatus").show();
						this._get_("statusEdit")._get_("remarks").allowBlank = true;
						this._get_("statusEdit")._get_("remarks").validate();				
					} , this );
						
					customer.on("editStatusHistory", function(dc) {
						this._getDc_("status").doNew();
						this._get_("statusEdit")._get_("remarks").allowBlank=false;
		
					}, this);
		
					customer.on("onEditOut", function (dc) {
						customer.updateDcState();
					}, this);
		
					customer.on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						dc.doReloadPage();
					},this);
		
					customer.on("recordChange", function (dc) {
						var rec = dc.newRecord;
					    if (rec) {
							var system = rec.get("system");
							customer.canDoNew = true;
		
							var isTransmissionInProgress = function(record){
								var inProgress = false; 
								var transmissionStatus = record.get("transmissionStatus");
								if ( transmissionStatus && transmissionStatus == __FMBAS_TYPE__.CustomerDataTransmissionStatus._IN_PROGRESS_ ){
									inProgress = true;
								}
								return inProgress;
							}
		
							var setChildrenState = function(state) {
								var childrenDcs = customer.children;
		
								for (var i = 0; i<childrenDcs.length; i++) {
									if (childrenDcs[i]._instanceKey_ != "contract") {
										childrenDcs[i].readOnly = state;
									}							
								}
							}
						
							if (system == true || isTransmissionInProgress(rec)) {
								customer.readOnly = true;
								setChildrenState(true);
							}
							else {
								customer.readOnly = false;
								setChildrenState(false);
							}
					    }
						var buttons = ["btnApprove","btnBlock","btnApproveEdit","btnBlockEdit"];
						this._applyStateButtons_(buttons);
						customer.updateDcState();
					}, this);
		
					note.on("afterDoQuerySuccess", function (dc) {
						var rec = dc.getRecord();
					    if (rec) {
							var date = rec.get("createdAt");
							var newDate = ("0" + date.getDate()).slice(-2)+"/"+("0" + (date.getMonth() + 1)).slice(-2)+"/"+date.getFullYear();
					    }
					}, this);
					
					customer.on("afterDoQuerySuccess", function(dc, ajaxResult) {
							if(ajaxResult.options.queryFromExternalInterface === true) {
								if (ajaxResult.records.length != 0) {
									this._showStackedViewElement_("main", "canvas1");
								} else {
									Main.info(Main.translate("applicationMsg", "externalInterfaceViewNoObj__lbl"));
									return;
								}
							}
						}, this);
		
					note.on("afterDoNew", function (dc) { 
						dc.setEditMode();
						this.showNoteWdw();
					}, this);
		
					finalCust.on("afterDoNew", function (dc) {
						var rec = dc.getRecord();
					    if (rec) {
							rec.beginEdit();
					        rec.set("isThirdParty", true);
							rec.set("natureOfBusiness","");
							rec.set("status","Active");
							rec.set("type","");					
							rec.endEdit();
					    }
					    this.showFinalCustWdw();
					}, this);
		
					finalCust.on("onEditIn", function (dc) {
						dc.setEditMode();
					    this.showFinalCustEditWdw();
					}, this);
		
					customer.on("afterDoNew", function (dc) {
						var rec = dc.getRecord();
					    if (rec) {
					        rec.set("isCustomer", "1");
							rec.set("status","Prospect");
					    }
						dc.setEditMode();
					    this.onShowWdw();
					}, this);
		
					contact.on("afterDoNew", function (dc) {
						dc.setEditMode();
						this.showContactWdw();
					}, this);
		
					masterAgreement.on("afterDoNew", function (dc) {
						dc.setEditMode();
						this.showMasterAgreementWdw();
					}, this);
		
					tradeReference.on("afterDoNew", function (dc) {
						dc.setEditMode();
						this.showTradeRefWdw();
					}, this);
		
					masterAgreement.on("afterDoSaveSuccess", function (dc, ajaxResult) {
						var action = ajaxResult.batch.operations[0].action;
						if(action=="update"){
							masterHistory.doNew();
						}
						customer.doReloadRecord();
					}, this);
		
					masterHistory.on("afterDoNew", function (dc) {
						this._getWindow_("masterAgreementHistoryWdw").show();
					}, this);
		
					note.on("afterDoEditIn", function (dc) {
						this._getWindow_("noteWdw").show();
					}, this);
		
					this._getDc_("customer").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwCustomer").close();
						} else if (ajaxResult.options.options.doCloseEditAfterSave === true) {
							this._getWindow_("wdwCustomer").close();
							var r = Ext.getResponseDataInJSON(ajaxResult.response);
							if (r) {
								var data = r.data[0];
								var code = data.code;
								dc.setFilterValue("code", code);
								dc.doQuery();
								this._showStackedViewElement_("main", "canvas2");
							}
							
						} 
					}, this);
		
					this._getDc_("finalCust").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("finalCustomerWdw").close();
						}
					}, this);
		
					this._getDc_("contact").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("contactWdw").close();
						}
						dc.doReloadPage();
					}, this);
		
					this._getDc_("masterAgreement").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("masterAgreementWdw").close();
						}
					}, this);
		
					this._getDc_("tradeReference").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("tradeRefWdw").close();
						}
					}, this);
		
					this._getDc_("note").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("noteWdw").close();
						}
					}, this);
		
					this._getDc_("masterHistory").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("masterAgreementHistoryWdw").close();
						}
					}, this);
		
					creditLine.on("afterDoNew", function (dc, ajaxResult) {
						dc.setEditMode();
						var window = this._getWindow_("creditLineWdw");
						window.show();
						Ext.defer(function(verifyActiveCreditLine){
							if (verifyActiveCreditLine === true) {
								this.checkActiveCreditLine();
							}
							this._setCreditLimit_();
						},300,this,[ajaxResult.verifyActiveCreditLine]);
						
					}, this);
		
					creditLine.on("afterDoSaveSuccess", function (dc, ajaxResult) {
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew({verifyActiveCreditLine:true});
							customer.doReloadRecord();
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("creditLineWdw").close();
							this.checkActiveCreditLine();
							customer.doReloadRecord();
							dc.doReloadRecord();
						}
						var action = ajaxResult.batch.operations[0].action;
						if(action=="update"){
							this.checkActiveCreditLine();
						}
		
					}, this);
					
					creditLine.on("afterDoCancel", function (dc) {
						dc.doReloadPage();
					}, this);
		
	}
	
	,_setCreditLimit_: function() {
		
						var dc = this._getDc_("creditLine");
						var r = dc.getRecord();
						if (r) {
							var creditTerm = r.get("creditTerm");
							if (creditTerm == __FMBAS_TYPE__.CreditType._PREPAYMENT_) {
								r.set("amount",0);
							}
							
						}
	}
	
	,checkActiveCreditLine: function() {
		
					var rec = this._getDc_("creditLine").store;
					var count = 0;
					if(rec){
						if(rec.data.length > 1) {
							for (var i =0; i < rec.data.length; i++) {
								if(rec.data.items[i].data.active == true)
									count++;
							}
						}
						if(count>1) {
							Main.warning("The system will deactivate the active credit line!");
						}
					}
	}
	
	,canApprove: function(dc) {
		 
						var selectedRecords = dc.selectedRecords;				
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(!(record.data.status == __FMBAS_TYPE__.CustomerStatus._PROSPECT_ || record.data.status == __FMBAS_TYPE__.CustomerStatus._INACTIVE_)){
								return false;			
							}
						}
						return true;
	}
	
	,setupHistoryWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var remarksField = this._get_(theForm)._get_("remarks");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(false);
						remarksField.setReadOnly(false);
						
						remarksField.fieldLabel = label;
						window.title = title;
						
						window.show();
		
						remarksField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
						    }
						});
	}
	
	,canExport: function(dc) {
		 
						var selectedRecords = dc.selectedRecords;				
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var receivedStatus =  selectedRecords[i].get("transmissionStatus");
							if( !( Ext.isEmpty(receivedStatus) || (receivedStatus==__FMBAS_TYPE__.CustomerDataTransmissionStatus._NOT_AVAILABLE_ 
								|| receivedStatus==__FMBAS_TYPE__.CustomerDataTransmissionStatus._PROCESSED_ 
								|| receivedStatus==__FMBAS_TYPE__.CustomerDataTransmissionStatus._FAILED_) ) ) {
								return false;			
							}
						}
						return true;
	}
	
	,block: function() {
		
						// Setup the reset window
						var label = "You are about to make this customer inactive.<br/> Please provide the reason for this.";
						var title = "Deactivate Customer";
						this.setupHistoryWindow("wdwBlockReason","BlockRemarkEdit","btnSaveCloseBlockHistoryWdw","btnBlockCancel" , label , title);				
	}
	
	,saveNew: function() {
		
					var loc = this._getDc_("customer");
					loc.doSave({doNewAfterSave:true});
	}
	
	,saveClose: function() {
		
					var loc = this._getDc_("customer");
					loc.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveStatusHistory: function() {
		
					var status = this._getDc_("status");
					status.doSave({doCloseAfterUpdate: true });
					this._getDc_("customer").doReloadPage();
	}
	
	,cancelStatusHistory: function() {
		
					this._getDc_("customer").doCancel();
					this._getDc_("status").doCancel();
					this._getWindow_("wdwStatus").close();
							
	}
	
	,saveEdit: function() {
		
					var loc = this._getDc_("customer");
					loc.doSave({doCloseEditAfterSave: true });
	}
	
	,saveNewContact: function() {
		
					var cont = this._getDc_("contact");
					cont.doSave({doNewAfterSave:true});
	}
	
	,saveNewMasterAgreement: function() {
		
					var ma = this._getDc_("masterAgreement");
					ma.doSave({doNewAfterSave:true});
	}
	
	,saveNewTradeRef: function() {
		
					var trd = this._getDc_("tradeReference");
					trd.doSave({doNewAfterSave:true});
	}
	
	,saveCloseContact: function() {
		
					var contact = this._getDc_("contact");
					contact.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveCloseMasterAgreement: function() {
		
					var masterAgreement = this._getDc_("masterAgreement");
					masterAgreement.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveCloseNote: function() {
		
					var note = this._getDc_("note");
					note.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveCloseTradeRef: function() {
		
					var tradeRef = this._getDc_("tradeReference");
					tradeRef.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveCloseHistory: function() {
		
					var masterHistory = this._getDc_("masterHistory");
					masterHistory.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,onWdwCustomerClose: function() {
		
						var dc = this._getDc_("customer");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,saveCloseFinalCust: function() {
		
					var finalCust = this._getDc_("finalCust");
					finalCust.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveNewFinalCust: function() {
		
					var finalCust = this._getDc_("finalCust");
					finalCust.doSave({doNewAfterSave:true});
	}
	
	,onWdwFinalCustomerClose: function() {
		
						var dc = this._getDc_("finalCust");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,saveCloseFinalCustEdit: function() {
		
					var cust = this._getDc_("finalCust");
					cust.doSave();
					this._getWindow_("finalCustomerEditWdw").close();
	}
	
	,onWdwFinalCustomerEditClose: function() {
		
						var dc = this._getDc_("finalCust");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,showFinalCustWdw: function() {
		
						var win = this._getWindow_("finalCustomerWdw");
						win.show();
	}
	
	,showFinalCustEditWdw: function() {
		
						var win = this._getWindow_("finalCustomerEditWdw");
						win.show();
	}
	
	,onShowWdw: function() {
		
						var win = this._getWindow_("wdwCustomer");
						var dc = this._getDc_("customer");
					    win.show();
		var o={
								name:"generateCustomerCode",
								modal:true
							};
						dc.doRpcData(o);
	}
	
	,onContactWdwClose: function() {
		
						var dc = this._getDc_("contact");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,onMasterAgreementWdwClose: function() {
		
						var dc = this._getDc_("masterAgreement");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,onNoteWdwClose: function() {
		
						var dc = this._getDc_("note");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,onTradeRefWdwClose: function() {
		
						var dc = this._getDc_("tradeReference");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,_exportCustomersToNAV: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								var responseDescription = responseData.params.exportCustomerDescription;							
								var responseResult = responseData.params.exportCustomerResult;
		
								if (responseResult == true) {
									Main.info(Main.encodeHTML(responseDescription)); 
								}
								else {
									Main.error(Main.encodeHTML(responseDescription));
								}
								dc.doReloadPage();
							}
						};
		
		                var dc = this._getDc_("customer");
						dc.setParamValue("exportCustomerOption", param);
		                var t = {
					        name: "exportFuelPlusToAzureCustomer",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcDataList(t);
		                
	}
	
	,saveNewCredit: function() {
		
					var dc = this._getDc_("creditLine");
					dc.doSave({doNewAfterSave:true});
	}
	
	,saveCloseCredit: function() {
		
					var creditLine = this._getDc_("creditLine");
					creditLine.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,onCreditLineWdwClose: function() {
		
						var dc = this._getDc_("creditLine");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,openCustomerHelp: function() {
		
					var url = Main.urlHelp+"/CustomerAccounts.html";
					window.open( url, "SONE_Help");
	}
	
	,fnSaveEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isSaveEnabled(dc);
	}
});
