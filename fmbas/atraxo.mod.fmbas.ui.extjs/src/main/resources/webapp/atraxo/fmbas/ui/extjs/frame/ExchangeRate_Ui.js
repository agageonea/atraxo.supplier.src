/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.ExchangeRate_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.ExchangeRate_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("exchangeRate", Ext.create(atraxo.fmbas.ui.extjs.dc.ExchangeRate_Dc,{}))
		.addDc("timeSerieAverage", Ext.create(atraxo.fmbas.ui.extjs.dc.TimeSerieAverage_Dc,{multiEdit: true}))
		.addDc("timeSerieItem", Ext.create(atraxo.fmbas.ui.extjs.dc.TimeSerieItem_Dc,{multiEdit: true}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.linkDc("timeSerieAverage", "exchangeRate",{fetchMode:"auto",reloadChildrenOnParentUpdate:true,fields:[
					{childField:"tserieId", parentField:"id"}]})
				.linkDc("timeSerieItem", "exchangeRate",{fetchMode:"auto",reloadChildrenOnParentUpdate:true,fields:[
					{childField:"tserie", parentField:"id"}]})
				.linkDc("note", "exchangeRate",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("history", "exchangeRate",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnOnEditWdwClose",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnOnEditWdwClose, scope:this})
		.addButton({name:"btnSaveCloseWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseWdw, scope:this})
		.addButton({name:"btnSaveNewWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewWdw, scope:this})
		.addButton({name:"showExchangeRate",glyph:fp_asc.chart_glyph.glyph,iconCls: fp_asc.chart_glyph.css, disabled:true, handler: this.onShowExchangeRate,stateManager:[{ name:"selected_one", dc:"exchangeRate"}], scope:this})
		.addButton({name:"btnDeleteSerieItem",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteSerieItem,stateManager:[{ name:"selected_one", dc:"timeSerieItem", and: function(dc) {return (this.canDeleteTimeSerieItem(dc));} }], scope:this})
		.addButton({name:"btnPublishSerie",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnPublishSerie,stateManager:[{ name:"selected_one_clean", dc:"timeSerieItem", and: function() {return (this._enablePublishBtn_());} }], scope:this})
		.addButton({name:"btnPublishSeries",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnPublishSeries,stateManager:[{ name:"selected_one_clean", dc:"exchangeRate", and: function() {return (this._enablePublishBtn_());} }], scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdw1",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw1, scope:this})
		.addButton({name:"helpWdw2",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw2, scope:this})
		.addButton({name:"btnCancelWithMenu",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"timeSerieItem", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", handler: this.onBtnCancelSelected, scope:this})
		.addButton({name:"btnCancelAll",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", _isDefaultHandler_:true, handler: this.onBtnCancelAll, scope:this})
		.addButton({name:"btnSubmitForApprovalList",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitForApprovalList,stateManager:[{ name:"selected_one_clean", dc:"exchangeRate", and: function(dc) {return (this.canSubmitForApproval(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnSubmitForApprovalEdit",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitForApprovalEdit,stateManager:[{ name:"selected_one_clean", dc:"exchangeRate", and: function(dc) {return (this.canSubmitForApproval(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnSubmitForApprovalExchangeRates",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitForApprovalExchangeRates,stateManager:[{ name:"selected_one_clean", dc:"exchangeRate", and: function(dc) {return (this.canSubmitForApproval(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnSaveCloseApprovalWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApprovalWdw, scope:this})
		.addButton({name:"btnCancelApprovalWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApprovalWdw, scope:this})
		.addButton({name:"btnApproveList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApproveList,stateManager:[{ name:"selected_not_zero", dc:"exchangeRate", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysexchangerateapproval === 'true' })
		.addButton({name:"btnApproveEdit",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApproveEdit,stateManager:[{ name:"selected_one_clean", dc:"exchangeRate", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysexchangerateapproval === 'true' })
		.addButton({name:"btnRejectList",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnRejectList,stateManager:[{ name:"selected_not_zero", dc:"exchangeRate", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysexchangerateapproval === 'true' })
		.addButton({name:"btnRejectEdit",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnRejectEdit,stateManager:[{ name:"selected_one_clean", dc:"exchangeRate", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysexchangerateapproval === 'true' })
		.addButton({name:"btnSaveCloseApproveWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApproveWdw, scope:this})
		.addButton({name:"btnSaveCloseRejectWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRejectWdw, scope:this})
		.addButton({name:"btnCancelApproveWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApproveWdw, scope:this})
		.addButton({name:"btnCancelRejectWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRejectWdw, scope:this})
		.addDcFilterFormView("exchangeRate", {name:"exchangeRateFilter", xtype:"fmbas_ExchangeRate_Dc$Filter"})
		.addDcGridView("exchangeRate", {name:"exchangeRateList", xtype:"fmbas_ExchangeRate_Dc$List"})
		.addDcEditGridView("timeSerieAverage", {name:"timeSerieAverageList", _hasTitle_:true, xtype:"fmbas_TimeSerieAverage_Dc$List", frame:true})
		.addDcEditGridView("timeSerieItem", {name:"timeSerieItemList", xtype:"fmbas_TimeSerieItem_Dc$ListContext", frame:true})
		.addDcFormView("exchangeRate", {name:"exchangeRateEdit", xtype:"fmbas_ExchangeRate_Dc$Edit", _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcFormView("exchangeRate", {name:"exchangeRateDetails", xtype:"fmbas_ExchangeRate_Dc$EditContext"})
		.addDcFormView("exchangeRate", {name:"exchangeRateQuotationsDetails", xtype:"fmbas_ExchangeRate_Dc$EditQuotationContext"})
		.addDcGridView("note", {name:"NotesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcGridView("history", {name:"History", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("exchangeRate", {name:"approvalNote", xtype:"fmbas_ExchangeRate_Dc$ApprovalNote"})
		.addDcFormView("exchangeRate", {name:"approveNote", xtype:"fmbas_ExchangeRate_Dc$ApprovalNote"})
		.addDcFormView("exchangeRate", {name:"rejectNote", xtype:"fmbas_ExchangeRate_Dc$ApprovalNote"})
		.addWindow({name:"wdwExchangeRate", _hasTitle_:true, width:700, height:310, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("exchangeRateEdit")],  listeners:{ close:{fn:this.onWdwExchangeRateClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewWdw"), this._elems_.get("btnSaveCloseWdw"), this._elems_.get("btnOnEditWdwClose")]}]})
		.addWindow({name:"wdwApprovalNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("approvalNote")],  closable:true, listeners:{ show:{fn:this.onApprovalWdwShow, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseApprovalWdw"), this._elems_.get("btnCancelApprovalWdw")]}]})
		.addWindow({name:"wdwApproveNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("approveNote")],  closable:true, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseApproveWdw"), this._elems_.get("btnCancelApproveWdw")]}]})
		.addWindow({name:"wdwRejectNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("rejectNote")],  closable:true, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRejectWdw"), this._elems_.get("btnCancelRejectWdw")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas2", dcName:"exchangeRate"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["exchangeRateList"], ["center"])
		.addChildrenTo("canvas2", ["exchangeRateDetails", "detailsTab"], ["north", "center"])
		.addChildrenTo("canvas3", ["exchangeRateQuotationsDetails", "timeSerieItemList"], ["north", "center"])
		.addChildrenTo("detailsTab", ["timeSerieAverageList"])
		.addToolbarTo("exchangeRateList", "tlbExchangeRateList")
		.addToolbarTo("exchangeRateDetails", "tlbExchangeRateEdit")
		.addToolbarTo("timeSerieAverageList", "tlbtimeSerieAverage")
		.addToolbarTo("timeSerieItemList", "tlbtimeSerieItemList")
		.addToolbarTo("exchangeRateQuotationsDetails", "tlbExchangeRateQuotations");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2", "canvas3"])
		.addChildrenTo2("detailsTab", ["History"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbExchangeRateList", {dc: "exchangeRate"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("showExchangeRate"),this._elems_.get("btnSubmitForApprovalList"),this._elems_.get("btnApproveList"),this._elems_.get("btnRejectList"),this._elems_.get("btnPublishSeries"),this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbExchangeRateEdit", {dc: "exchangeRate"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,inContainer:"main",showView:"canvas1"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnSubmitForApprovalEdit"),this._elems_.get("helpWdw1")])
			.addReports()
		.end()
		.beginToolbar("tlbtimeSerieAverage", {dc: "timeSerieAverage"})
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbtimeSerieItemList", {dc: "timeSerieItem"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelWithMenu"),this._elems_.get("btnCancelSelected"),this._elems_.get("btnCancelAll"),this._elems_.get("btnDeleteSerieItem"),this._elems_.get("btnSubmitForApprovalExchangeRates"),this._elems_.get("btnApproveEdit"),this._elems_.get("btnRejectEdit"),this._elems_.get("btnPublishSerie")])
			.addReports()
		.end()
		.beginToolbar("tlbExchangeRateQuotations", {dc: "exchangeRate"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw2")])
			.addReports()
		.end();
	}
	
	,_applyCustomStateManagers_: function(){
		this._getBuilder_().beginStateFnDef('tlbExchangeRateList', {dc: 'exchangeRate'})
		.setStateFn_Delete(this.canDeleteExchangeRate)
		.endStateFnSet();
	}
	

	
	/**
	 * On-Click handler for button btnOnEditWdwClose
	 */
	,onBtnOnEditWdwClose: function() {
		this.onWdwExchangeRateClose();
		this._getWindow_("wdwExchangeRate").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseWdw
	 */
	,onBtnSaveCloseWdw: function() {
		this.saveClose();
	}
	
	/**
	 * On-Click handler for button btnSaveNewWdw
	 */
	,onBtnSaveNewWdw: function() {
		this.saveNew();
	}
	
	/**
	 * On-Click handler for button showExchangeRate
	 */
	,onShowExchangeRate: function() {
		this._showStackedViewElement_("main", "canvas3"); 
	}
	
	/**
	 * On-Click handler for button btnDeleteSerieItem
	 */
	,onBtnDeleteSerieItem: function() {
		this.deleteSerieItem();
	}
	
	/**
	 * On-Click handler for button btnPublishSerie
	 */
	,onBtnPublishSerie: function() {
		var successFn = function() {
			this._getDc_("exchangeRate").doReloadPage();
		};
		var o={
			name:"publish",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("exchangeRate").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnPublishSeries
	 */
	,onBtnPublishSeries: function() {
		var successFn = function() {
			this._getDc_("exchangeRate").doReloadPage();
		};
		var o={
			name:"publishElements",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("exchangeRate").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdw1
	 */
	,onHelpWdw1: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdw2
	 */
	,onHelpWdw2: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnCancelSelected
	 */
	,onBtnCancelSelected: function() {
		this.fnCancelSelected();
	}
	
	/**
	 * On-Click handler for button btnCancelAll
	 */
	,onBtnCancelAll: function() {
		this.fnCancelAll();
	}
	
	/**
	 * On-Click handler for button btnSubmitForApprovalList
	 */
	,onBtnSubmitForApprovalList: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnSubmitForApprovalEdit
	 */
	,onBtnSubmitForApprovalEdit: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnSubmitForApprovalExchangeRates
	 */
	,onBtnSubmitForApprovalExchangeRates: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApprovalWdw
	 */
	,onBtnSaveCloseApprovalWdw: function() {
		var successFn = function() {
			this._getWindow_("wdwApprovalNote").close();
			this._getDc_("exchangeRate").doEditOut();
			this._getDc_("exchangeRate").doReloadPage();
		};
		var o={
			name:"submitForApproval",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("exchangeRate").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCancelApprovalWdw
	 */
	,onBtnCancelApprovalWdw: function() {
		this._getWindow_("wdwApprovalNote").close();
		this._getDc_("exchangeRate").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnApproveList
	 */
	,onBtnApproveList: function() {
		this.openApproveWindow();
	}
	
	/**
	 * On-Click handler for button btnApproveEdit
	 */
	,onBtnApproveEdit: function() {
		this.openApproveWindow();
	}
	
	/**
	 * On-Click handler for button btnRejectList
	 */
	,onBtnRejectList: function() {
		this.openRejectWindow();
	}
	
	/**
	 * On-Click handler for button btnRejectEdit
	 */
	,onBtnRejectEdit: function() {
		this.openRejectWindow();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApproveWdw
	 */
	,onBtnSaveCloseApproveWdw: function() {
		var successFn = function(dc,response) {
			this._getWindow_("wdwApproveNote").close();
			this.initFeedback(response)
			this._getDc_("exchangeRate").doReloadPage();
		};
		var o={
			name:"approveList",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("exchangeRate").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRejectWdw
	 */
	,onBtnSaveCloseRejectWdw: function() {
		var successFn = function(dc,response) {
			this._getWindow_("wdwRejectNote").close();
			this.initFeedback(response)
			this._getDc_("exchangeRate").doReloadPage();
		};
		var o={
			name:"rejectList",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("exchangeRate").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnCancelApproveWdw
	 */
	,onBtnCancelApproveWdw: function() {
		this._getWindow_("wdwApproveNote").close();
		this._getDc_("exchangeRate").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCancelRejectWdw
	 */
	,onBtnCancelRejectWdw: function() {
		this._getWindow_("wdwRejectNote").close();
		this._getDc_("exchangeRate").doReloadPage();
	}
	
	,showWindow: function() {	
		this._getWindow_("wdwExchangeRate").show();
	}
	
	,_when_called_from_dshboard_: function(id) {
		
						var dc = this._getDc_("exchangeRate");
						dc.doCancel();
						dc.doClearAllFilters();
						dc.setFilterValue("id", id);				
						dc.doQuery({showEdit: true}); 
	}
	
	,canApproveReject: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var r = selectedRecords[i];
							var canBeApprovedRejected = r.data.approvalStatus == __FMBAS_TYPE__.TimeSeriesApprovalStatus._AWAITING_APPROVAL_ && r.data.canBeCompleted == true;
							if(canBeApprovedRejected === false) {
								return false;
							}
						}
						return true;
	}
	
	,openApproveWindow: function() {
		
						var label = "Please provide an approval reason.";
						var title = "Approve";
						this.setupPopUpWindow("wdwApproveNote","approveNote","btnSaveCloseApproveWdw","btnCancelApproveWdw" , label , title);				
	}
	
	,openRejectWindow: function() {
		
						var label = "Please provide a reason for rejection.";
						var title = "Reject";
						this.setupPopUpWindow("wdwRejectNote","rejectNote","btnSaveCloseRejectWdw","btnCancelRejectWdw" , label , title);				
	}
	
	,setupPopUpWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var remarksField = this._get_(theForm)._get_("remarks");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						var ctx = this;
						
						saveButton.setDisabled(true);
						
						remarksField.fieldLabel = label;
		
						remarksField.setValue("");
						window.title = title;
		
						remarksField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
								cancelButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
						    }
						});
						window.show();
	}
	
	,canSubmitForApproval: function(dc) {
		
						var record = dc.getRecord();
						if(record) {
							var approvalStatus = record.data.approvalStatus;
							var status = record.data.status;
							if ( status === __FMBAS_TYPE__.TimeSeriesStatus._WORK_ && (approvalStatus === __FMBAS_TYPE__.TimeSeriesApprovalStatus._NEW_ 
									|| approvalStatus === __FMBAS_TYPE__.TimeSeriesApprovalStatus._REJECTED_)) {
									return true;
								}
						}
						return false;
	}
	
	,canDeleteExchangeRate: function(dc) {
		
						if ( _SYSTEMPARAMETERS_.sysexchangerateapproval === "true" ) {
							var selectedRecords = dc.selectedRecords;
							for(var i=0 ; i<selectedRecords.length ; ++i){
								var data = selectedRecords[i].data;
								if( data.approvalStatus === __FMBAS_TYPE__.TimeSeriesApprovalStatus._AWAITING_APPROVAL_ ){
									return false;
								}
							}
						}
						return true;
	}
	
	,canDeleteTimeSerieItem: function(dc) {
		
						var rec = dc.getRecord();
						if (rec) {
							var calc = ( rec.data.calculated=="0" ); 
							if ( _SYSTEMPARAMETERS_.sysexchangerateapproval === "true" ) {
								var dcExchangeRate = this._getDc_("exchangeRate");
								var recExchangeRate = dcExchangeRate.record;
								if (recExchangeRate) {
									var awaitingApproval = (recExchangeRate.data.approvalStatus === __FMBAS_TYPE__.TimeSeriesApprovalStatus._AWAITING_APPROVAL_);
									if (awaitingApproval) {
										return false;
									}
								}
							}
							return calc;
						}
						return false;
	}
	
	,openSubmitForApprovalWindow: function() {
		
						var label = "Add your note to be sent to your request to the approvers";
						var title = "Submit for approval";
						this.setupPopUpWindow("wdwApprovalNote","approvalNote","btnSaveCloseApprovalWdw","btnCancelApprovalWdw" , label , title);				
	}
	
	,useWorkflowForApproval: function() {
		
						return _SYSTEMPARAMETERS_.sysexchangerateapproval === "true" ;
	}
	
	,_onExchangeRateRecordChange_: function(exchangeRate) {
		
						if ( this.useWorkflowForApproval() ) {
							this._makeExchangeRateReadOnlyOnAwaitingApproval_ (exchangeRate);
						}
	}
	
	,_makeExchangeRateReadOnlyOnAwaitingApproval_: function(exchangeRate) {
		
						var record = exchangeRate.getRecord();
						if(record) {
							var setChildrenState = function(state) {
								var childrenDcs = exchangeRate.children;
								for (var i = 0; i<childrenDcs.length; i++) {
									childrenDcs[i].readOnly = state;
								}
							}
							var approvalStatus = record.get("approvalStatus");
							if (approvalStatus == __FMBAS_TYPE__.TimeSeriesApprovalStatus._AWAITING_APPROVAL_) {
								exchangeRate._maskEditable_ = true;
								setChildrenState(true);
							}
							else { 	
								exchangeRate._maskEditable_ = false;					
								setChildrenState(false);
							}
						}
	}
	
	,_enablePublishBtn_: function() {
		
						var dc = this._getDc_("exchangeRate");
						var r = dc.getRecord();
						var result = false;
						if (r) {
							var status = r.get("status");
							var hasTs = r.get("hasTimeSerieItems");
							var approvalStatus = r.get("approvalStatus");
							var approvalResult = (_SYSTEMPARAMETERS_.sysexchangerateapproval === "false" || (_SYSTEMPARAMETERS_.sysexchangerateapproval === "true"  && approvalStatus === __FMBAS_TYPE__.TimeSeriesApprovalStatus._APPROVED_))
							if (status === "work" && hasTs == true && approvalResult) {
								result = true;
							}
						}
						return result;
	}
	
	,fnCancelSelected: function() {
		
						var dc = this._getDc_("timeSerieItem");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAll: function() {
		
						var dc = this._getDc_("timeSerieItem");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isCancelEnabled(dc);
	}
	
	,_afterDefineDcs_: function() {
		
					var cmp = this._getDc_("exchangeRate");
					var serieItem = this._getDc_("timeSerieItem");
					var avg = this._getDc_("timeSerieAverage");
		
					cmp.on("recordReload", function(dc, ajaxResult) { 
						this._applyStateAllButtons_();
					}, this);
		
					cmp.on("afterDoQuerySuccess", function(dc, ajaxResult) { 
						if (ajaxResult.options.showEdit == true) {
							this._showStackedViewElement_("main", "canvas3");
						}
					}, this);
		
					serieItem.on("afterDoSaveSuccess", function(dc, ajaxResult) {
						cmp.doReloadRecord();
					}, this);
		
					avg.on("afterDoSaveSuccess", function(dc, ajaxResult) {
						cmp.doReloadRecord();
					}, this);
		
					serieItem.on("afterDoDeleteFailure", function() {
						serieItem.doCancel();
					}, this);
		
					cmp.on("afterDoNew", function (dc) {
					    var rec = dc.getRecord();
					    if (rec) {
					        rec.set("serieType", "X");
					        rec.set("factor", "1");
					        rec.set("activation", "y");
					        rec.set("supplyItemUpdate", "n");
					    }
						dc.setEditMode();
					    this.showWindow();
					}, this);
		
					cmp.on("onEditOut", function (dc) {
					    dc.doReloadRecord();
					}, this);
		
					cmp.on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwExchangeRate").close();
						}
					}, this);
		
					serieItem.on("afterDoSaveSuccess", function (dc) {
							dc.doReloadPage(); }, this)
		
					cmp.on("recordChange", function (obj) {
							this._onExchangeRateRecordChange_(obj.dc);
			            }, this);
	}
	
	,saveNew: function() {
		
					var exRate = this._getDc_("exchangeRate");
					exRate.doSave({doNewAfterSave:true});
	}
	
	,saveClose: function() {
		
					var exRate = this._getDc_("exchangeRate");
					exRate.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/ExchangeRatesTimeSeries.html";
					window.open( url, "SONE_Help");
	}
	
	,onWdwExchangeRateClose: function() {
		
						var dc = this._getDc_("exchangeRate");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,deleteSerieItem: function() {
		
						this._getDc_("timeSerieItem").doDelete();
						this._getDc_("timeSerieItem").on("afterDoDeleteSuccess", function() {
							this._getDc_("exchangeRate").doReloadPage();
						} , this );
	}
	
	,_afterDefineElements_: function() {
		
						var approveNote = this._getElementConfig_("approveNote"); 
						var rejectNote = this._getElementConfig_("rejectNote");
		
						if( approveNote ){
							approveNote._shouldDisableIfDcIsMasked_ = false;
						}
		
						if( rejectNote ){
							rejectNote._shouldDisableIfDcIsMasked_ = false;
						}
	}
	
	,initFeedback: function(response) {
		
						var responseData = Ext.getResponseDataInJSON(response);
						Main.info(responseData.params.approveRejectResult); 
	}
});
