/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.Vat_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Vat_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("vat", Ext.create(atraxo.fmbas.ui.extjs.dc.Vat_Dc,{multiEdit: true}))
		.addDc("vatRate", Ext.create(atraxo.fmbas.ui.extjs.dc.VatRate_Dc,{multiEdit: true}))
		.linkDc("vatRate", "vat",{fetchMode:"auto",fields:[
					{childField:"vatId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addDcEditGridView("vat", {name:"VatList", height:300, xtype:"fmbas_Vat_Dc$List", frame:true})
		.addDcFilterFormView("vat", {name:"VatFilter", xtype:"fmbas_Vat_Dc$Filter"})
		.addDcEditGridView("vatRate", {name:"VatRateList", xtype:"fmbas_VatRate_Dc$List", frame:true})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["VatList", "VatRateList"], ["north", "center"])
		.addToolbarTo("VatList", "tlbVatList")
		.addToolbarTo("VatRateList", "tlbVatRateList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbVatList", {dc: "vat"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbVatRateList", {dc: "vatRate"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/Vat.html";
					window.open( url, "SONE_Help");
	}
});
