/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.WorkflowParameter_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_WorkflowParameter_Ds"
	},
	
	
	fields: [
		{name:"workflowId", type:"int", allowNull:true},
		{name:"workflowCode", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"paramValue", type:"string"},
		{name:"defaultValue", type:"string"},
		{name:"type", type:"string"},
		{name:"mandatory", type:"boolean"},
		{name:"refValues", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.WorkflowParameter_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"workflowId", type:"int", allowNull:true},
		{name:"workflowCode", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"paramValue", type:"string"},
		{name:"defaultValue", type:"string"},
		{name:"type", type:"string"},
		{name:"mandatory", type:"boolean", allowNull:true},
		{name:"refValues", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.fmbas.ui.extjs.ds.WorkflowParameter_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"xxx", type:"string"}
	]
});
