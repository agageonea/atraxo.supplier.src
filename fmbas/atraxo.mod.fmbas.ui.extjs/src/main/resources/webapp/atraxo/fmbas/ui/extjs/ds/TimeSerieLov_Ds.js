/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.TimeSerieLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_TimeSerieLov_Ds"
	},
	
	
	fields: [
		{name:"measure", type:"string", noFilter:true, noSort:true},
		{name:"serieName", type:"string"},
		{name:"serieType", type:"string"},
		{name:"serieFreqInd", type:"string"},
		{name:"description", type:"string"},
		{name:"status", type:"string"},
		{name:"dataProvider", type:"string"},
		{name:"externalSerieName", type:"string"},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceCode", type:"string"},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.TimeSerieLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"measure", type:"string", noFilter:true, noSort:true},
		{name:"serieName", type:"string"},
		{name:"serieType", type:"string"},
		{name:"serieFreqInd", type:"string"},
		{name:"description", type:"string"},
		{name:"status", type:"string"},
		{name:"dataProvider", type:"string"},
		{name:"externalSerieName", type:"string"},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceCode", type:"string"},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
