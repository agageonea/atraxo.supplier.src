/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.Workflow_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Workflow_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("workflow", Ext.create(atraxo.fmbas.ui.extjs.dc.Workflow_Dc,{multiEdit: true}))
		.addDc("workflowParameters", Ext.create(atraxo.fmbas.ui.extjs.dc.WorkflowParameters_Dc,{multiEdit: true}))
		.addDc("workflowInstances", Ext.create(atraxo.fmbas.ui.extjs.dc.WorkflowInstance_Dc,{multiEdit: true}))
		.addDc("workflowNotification", Ext.create(atraxo.fmbas.ui.extjs.dc.WorkflowNotifications_Dc,{multiEdit: true}))
		.addDc("roleActive", Ext.create(atraxo.fmbas.ui.extjs.dc.RoleActive_Dc,{}))
		.linkDc("workflowParameters", "workflow",{fetchMode:"auto",fields:[
					{childField:"workflowId", parentField:"id"}]})
				.linkDc("workflowInstances", "workflow",{fetchMode:"auto",fields:[
					{childField:"workflowId", parentField:"id"}]})
				.linkDc("workflowNotification", "workflow",{fetchMode:"auto",fields:[
					{childField:"workflowId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnDeployWkfRpc",glyph:fp_asc.deploy_glyph.glyph,iconCls: fp_asc.deploy_glyph.css, disabled:true, handler: this.onBtnDeployWkfRpc,stateManager:[{ name:"selected_one", dc:"workflow"}], scope:this})
		.addButton({name:"btnTerminateWkfInstanceMenu",glyph:fp_asc.stop_glyph.glyph,iconCls: fp_asc.stop_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"workflowInstances"}], scope:this})
		.addButton({name:"btnTerminateWkfSelectedInstance",glyph:fp_asc.stop_glyph.glyph,iconCls: fp_asc.stop_glyph.css, disabled:true,  _btnContainerName_:"btnTerminateWkfInstanceMenu", _isDefaultHandler_:true, handler: this.onBtnTerminateWkfSelectedInstance,stateManager:[{ name:"selected_not_zero", dc:"workflowInstances", and: function(dc) {return (this.canTerminate(dc));} }], scope:this})
		.addButton({name:"btnTerminateWkfAllInstance",glyph:fp_asc.stop_glyph.glyph,iconCls: fp_asc.stop_glyph.css, disabled:true,  _btnContainerName_:"btnTerminateWkfInstanceMenu", handler: this.onBtnTerminateWkfAllInstance,stateManager:[{ name:"selected_not_zero", dc:"workflowInstances"}], scope:this})
		.addButton({name:"btnResumeWkfInstanceRpc",glyph:fp_asc.play_glyph.glyph,iconCls: fp_asc.play_glyph.css, disabled:true, handler: this.onBtnResumeWkfInstanceRpc,stateManager:[{ name:"selected_one", dc:"workflowInstances", and: function(dc) {return (dc.record.data.status==__FMBAS_TYPE__.WorkflowInstanceStatus._FAILURE_);} }], scope:this})
		.addButton({name:"btnCopyWorkflow",glyph:fp_asc.copy_glyph.glyph,iconCls: fp_asc.copy_glyph.css, disabled:false, handler: this.onBtnCopyWorkflow, scope:this})
		.addButton({name:"btnContinueCopy",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnContinueCopy, scope:this})
		.addButton({name:"btnCancelCopy",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelCopy, scope:this})
		.addButton({name:"btnAddNotification",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnAddNotification, scope:this})
		.addButton({name:"btnSaveNotification",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSaveNotification,stateManager:[{ name:"record_is_dirty", dc:"workflowNotification"}], scope:this})
		.addButton({name:"btnCancelNotification",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnCancelNotification,stateManager:[{ name:"record_is_dirty", dc:"workflowNotification"}], scope:this})
		.addButton({name:"btnRemoveNotification",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnRemoveNotification,stateManager:[{ name:"selected_not_zero", dc:"workflowNotification", and: function(dc) {return (dc.isDirty() == false);} }], scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addDcFilterFormView("workflow", {name:"filterWorkflows", xtype:"fmbas_Workflow_Dc$Filter"})
		.addDcEditGridView("workflow", {name:"workflowEdit", height:300, xtype:"fmbas_Workflow_Dc$WorkFlowForm", frame:true})
		.addDcFormView("workflow", {name:"workflowSubsidiary", xtype:"fmbas_Workflow_Dc$SelectSubsidiary"})
		.addDcEditGridView("workflowParameters", {name:"workflowParameterList", _hasTitle_:true, height:350, xtype:"fmbas_WorkflowParameters_Dc$List", frame:true,  listeners:{ beforeedit: { scope: this, fn: function(editor, ctx) { this._setEditor_(editor, ctx, this._get_('workflowParameterList'))}}}})
		.addDcFilterFormView("workflowInstances", {name:"filterInstances", xtype:"fmbas_WorkflowInstance_Dc$Filter"})
		.addDcEditGridView("workflowInstances", {name:"workflowInstancesList", _hasTitle_:true, height:350, xtype:"fmbas_WorkflowInstance_Dc$List", frame:true})
		.addDcEditGridView("workflowNotification", {name:"workflowNotification", _hasTitle_:true, height:350, xtype:"fmbas_WorkflowNotifications_Dc$List", frame:true})
		.addDcGridView("roleActive", {name:"rolesAssignList", xtype:"fmbas_RoleActive_Dc$ListActive",  padding:"0px 10px 5px 10px"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", height:300, xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addWindow({name:"SelectSubsidiaryWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("workflowSubsidiary")],  closable:true, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnContinueCopy"), this._elems_.get("btnCancelCopy")]}]})
		.addAsgnWindow({name:"rolesAsgnWdw", _hasTitle_:true, width:800, height:400,_gridName_: "rolesAssignList",
			_srcDcName_: "workflowParameters", _srcField_: "paramValue", _srcIdField_: "paramValue",
			_gridField_: "code",  listeners:{show: {scope: this,fn: function(w) {this._resetWindow_(w);}}}, cls:"sone-assign-window"})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas1", dcName:"workflowParameters"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["workflowEdit", "detailsTab"], ["center", "south"])
		.addChildrenTo("detailsTab", ["workflowParameterList"])
		.addToolbarTo("workflowEdit", "tlbWorkflowForm")
		.addToolbarTo("workflowParameterList", "tlbWorkflowParameters")
		.addToolbarTo("workflowInstancesList", "tlbWorkflowWkfInstances")
		.addToolbarTo("workflowNotification", "tlbNotification");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("detailsTab", ["workflowInstancesList", "workflowNotification"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbWorkflowForm", {dc: "workflow"})
			.addButtons([])
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnDeployWkfRpc"),this._elems_.get("btnCopyWorkflow"),this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbWorkflowParameters", {dc: "workflowParameters"})
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbWorkflowWkfInstances", {dc: "workflowInstances"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnTerminateWkfInstanceMenu"),this._elems_.get("btnTerminateWkfSelectedInstance"),this._elems_.get("btnTerminateWkfAllInstance"),this._elems_.get("btnResumeWkfInstanceRpc")])
			.addReports()
		.end()
		.beginToolbar("tlbNotification", {dc: "workflowNotification"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnAddNotification"),this._elems_.get("btnSaveNotification"),this._elems_.get("btnCancelNotification"),this._elems_.get("btnRemoveNotification")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnDeployWkfRpc
	 */
	,onBtnDeployWkfRpc: function() {
		var o={
			name:"deployWorkflow",
			modal:true
		};
		this._getDc_("workflow").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnTerminateWkfSelectedInstance
	 */
	,onBtnTerminateWkfSelectedInstance: function() {
		this._terminateWfkInstancesConfirmation('selected')
	}
	
	/**
	 * On-Click handler for button btnTerminateWkfAllInstance
	 */
	,onBtnTerminateWkfAllInstance: function() {
		this._terminateWfkInstancesConfirmation('all')
	}
	
	/**
	 * On-Click handler for button btnResumeWkfInstanceRpc
	 */
	,onBtnResumeWkfInstanceRpc: function() {
		var successFn = function() {
			this._reloadWorkflowInstancePage_();
		};
		var o={
			name:"resumeInstance",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("workflowInstances").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCopyWorkflow
	 */
	,onBtnCopyWorkflow: function() {
		this._getWindow_("SelectSubsidiaryWdw").show();
	}
	
	/**
	 * On-Click handler for button btnContinueCopy
	 */
	,onBtnContinueCopy: function() {
		this.copyWorkflowRpc()
	}
	
	/**
	 * On-Click handler for button btnCancelCopy
	 */
	,onBtnCancelCopy: function() {
		this._getDc_("workflow").doCancel();
		this._getWindow_("SelectSubsidiaryWdw").close();
		this._getDc_("workflow").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnAddNotification
	 */
	,onBtnAddNotification: function() {
		this._getDc_("workflowNotification").doNew();
	}
	
	/**
	 * On-Click handler for button btnSaveNotification
	 */
	,onBtnSaveNotification: function() {
		this._getDc_("workflowNotification").doSave();
	}
	
	/**
	 * On-Click handler for button btnCancelNotification
	 */
	,onBtnCancelNotification: function() {
		this._getDc_("workflowNotification").doCancel();
	}
	
	/**
	 * On-Click handler for button btnRemoveNotification
	 */
	,onBtnRemoveNotification: function() {
		this._getDc_("workflowNotification").doDelete();
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	,onSelectWdwClose: function() {	
		this._getDc_("workflow").doCancel();;
		this._getWindow_("SelectSubsidiaryWdw").close();;
		this._getDc_("workflow").doReloadPage();
	}
	
	,_setInstanceActive_: function(params) {
		
						var mainDc = this._getDc_("workflow");
						mainDc.setFilterValue("id", params.wkf_id);
		                mainDc.doQuery();
		
						var detailsTab = this._get_("detailsTab");
		                var workflowInstancesList = this._get_("workflowInstancesList");
		                detailsTab.setActiveTab(workflowInstancesList);
		
						var dc = this._getDc_("workflowInstances");
						dc.setFilterValue("id", params.wkf_instance_id);
		                dc.doQuery();
	}
	
	,_resetWindow_: function(w) {
		
						var dc = this._getDc_("roleActive");
						var f = Ext.getCmp(this._roleSearchFieldId_);
						dc.advancedFilter = null;
						f.setValue("");
						dc.doQuery();
	}
	
	,_afterDefineDcs_: function() {
		
						var workflow = this._getDc_("workflow");
						workflow.on("afterDoDeleteFailure", function() {
							workflow.doCancel();
						}, this);
		
						var notif = this._getDc_("workflowNotification");
						notif.on("afterDoSaveSuccess", function(dc) {	
							dc.doReloadPage();
						} , this );
		
	}
	
	,_reloadWorkflowInstancePage_: function() {
		
						var workflowInstanceDc = this._getDc_("workflowInstances");
						workflowInstanceDc.doReloadPage();
	}
	
	,copyWorkflowRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								var responseDescription = responseData.params.copyResultDescription;							
								var responseResult = responseData.params.copyResultValue;
								
								//close window
								this._getWindow_("SelectSubsidiaryWdw").close();
		
								//display results
								if (responseResult == false) {
									Main.error(responseDescription);
								}
								else{
									//reload page
									dc.doReloadPage();
								}
							}
						};
		
		                var dc = this._getDc_("workflow");
		                var t = {
					        name: "copyWorkflow",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcData(t);
	}
	
	,_setEditor_: function(editor,ctx,view) {
		
					var fieldIndex = ctx.field;
					var col = ctx.column;
					var cfg = {};
					var textEditor = {xtype:"textfield", maxLength:255};	
					var numberEditor = {xtype:"numberfield", minValue:0, maxLength:32};	
					var field = col.field;
					var storeData = [];
					var dateFormat = "mm/dd/yyy";
					var passwordEditor = {
						xtype:"textfield", 
						cls: "sone-masked-field",
						inputType: Ext.isChrome ? "text" : "password",
					};	
					
					var frame = this;
					if (fieldIndex == "paramValue") {
						var r = ctx.record;
						if (r) {
							
							var type = r.get("type");
							var refValues = r.get("refValues");
							if (!Ext.isEmpty(type)) {
		
								//////////// CASE 1 - Type is _ENUMERATION_ ////////////
		
								if (type == "enumeration") {
									cfg = {
										xtype: "combo",
							            selectOnFocus: true,
							            queryMode: "local",
							            store: refValues.split(",")
									};
		
								}
		
								//////////// CASE 2 - Type is _LOV_ ////////////
		
								else if (type == "lov") {
		
									cfg = {
										_dcView_ : view, 
										xtype: refValues, 
										maxLength:100, 
										typeAhead: false,
										remoteFilter: true,
										remoteSort: true,
										forceSelection: true,
										retFieldMapping: [{
											lovField:"code", 
											dsField: "value"}
										]
									};
								}
		
								//////////// CASE 3 - Type is boolean ////////////
		
								else if (type == "boolean") {
									cfg = {
										xtype: "combo",
							            selectOnFocus: true,
							            queryMode: "local",
							            store: ["true","false"]
									};
		
								}
		
								//////////// CASE 4 - Type is integer ////////////
		
								else if (type == "integer") {
									cfg = numberEditor;
		
								}
		
								//////////// CASE 3 - Type is _MASKED_ ////////////
		
								else if (type == __FMBAS_TYPE__.JobParamType._MASKED_) {
									cfg = passwordEditor;
								}
		
								else {
									cfg = textEditor;
								}
		
								col.setEditor(cfg);
							}
						}
					}
	}
	
	,_afterDefineElements_: function() {
		
					var builder = this._getBuilder_();
					this._roleSearchFieldId_ = Ext.id();
					builder.change("rolesAssignList",this._injectSearchField_(this._roleSearchFieldId_,"rolesAssignList","Search for role code/role", 250, true, true));
	}
	
	,canTerminate: function(dc) {
		
					var selectedRecords = dc.selectedRecords;
					for(var i=0 ; i<selectedRecords.length ; ++i){
						var status = selectedRecords[i].get("status");
						if(status!==__FMBAS_TYPE__.WorkflowInstanceStatus._IN_PROGRESS_ 
							&& status!==__FMBAS_TYPE__.WorkflowInstanceStatus._FAILURE_){
							return false;
						}
					}
					return true;
	}
	
	,_terminateWfkInstancesConfirmation: function(param) {
		
				var workflowDc = this._getDc_("workflow");
				var workflowInstanceDc = this._getDc_("workflowInstances");
				
				var selectedWorkflow = workflowDc.getRecord();
				var selectedWkfName;
				if(selectedWorkflow){
					selectedWkfName = selectedWorkflow.get("name");
				}
				if(this.canTerminate(workflowInstanceDc) || param === "all"){
					if(workflowInstanceDc.selectedRecords.length > 1 || param === "all"){
						Ext.Msg.show({
							       title : "Terminate Workflow Instances",
							       msg : "You are about to terminate multiple instances of the \""+selectedWkfName+"\" workflow. Are you sure?",
							       icon : Ext.MessageBox.WARNING,
							       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
							       fn : function(btnId) {
							              if( btnId == "yes" ){
							                     this._terminateWkfInstances(param);
					              }
					       },
					       scope : this
							});
					} else {
						this._terminateWkfInstances(param);
					}
				}
	}
	
	,_terminateWkfInstances: function(param) {
		
					var callbackFn = function(dc,response,serviceName,specs) { 
						var responseData = Ext.getResponseDataInJSON(response);
						if (responseData && responseData.params) {
							var responseDescription = responseData.params.terminateWkfInstancesDescription;							
							var responseResult = responseData.params.terminateWfkInstancesResult;
							if (responseResult == false) {
								Main.error(responseDescription);
							}
							dc.doReloadPage();
						}
					};
		
					var wkfInstanceDc = this._getDc_("workflowInstances");
					wkfInstanceDc.setParamValue("wfkTerminateOption", param);
					var t = {
					       name: "terminateInstances",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					wkfInstanceDc.doRpcDataList(t);
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/Workflows.html";
					window.open( url, "SONE_Help");
	}
	
	,_injectSearchField_: function(id,gridName,emptyLabelText,fieldWidth,showResetBtn,buttonInsideField,extraElems) {
		
		
						var defaultMargin = "0px 8px 0px 0px";
						var marginInside = "0px 8px 0px -42px";
						var buttonInsideCls = "sone-search-btn-inside";
						var defaultCls = "sone-search-btn-outside";
						var btnId = id+"-btn";				
		
						var toolbarItems = [{ 
							xtype: "textfield",
							id: id,
						    width: fieldWidth ? fieldWidth : 200,
						    emptyText: emptyLabelText,
						    listeners: {
						        change: {
						            scope: this,
									buffer: 1000,
						            fn: function(field, newVal) {
		
						                var grid = this._get_(gridName);
										var resetBtn = Ext.getCmp(btnId);							
						                
						                if (newVal && newVal != "*") {
		
											var af = [];
											var dateFilter = [];
											var dc = grid._controller_;
											var s = dc.store;
		
											// Check if the records have been changed and if yes discard the chages
		
											if (s.getNewRecords().length > 0 || s.getUpdatedRecords().length > 0 || s.getRemovedRecords().length > 0) {
												s.rejectChanges();
											}
		
											for(i=0;i<grid.columns.length;i++){
												var operation ="like";
												var value1 = "%"+newVal+"%";
		
												var o = {
													"id":null,
													"fieldName":grid.columns[i].dataIndex,
													"operation":operation,
													"value1": value1,
													"groupOp":"OR1"
												};
												if (grid.columns[i].hidden == false && grid.columns[i].dataIndex !== "price" && grid.columns[i].dataIndex !== "unitCode" && grid.columns[i].dataIndex !== "selected") {
													af.push(o);
												}
												
											}
		
											resetBtn.show();									
											dc.advancedFilter = af;
											dc.doQuery();
						                }
										else {
											resetBtn.hide();
										}
						            }
						        }
						    }
						},{
							glyph: "xf00d@FontAwesome",
		                    xtype: "button",
							id : btnId,
							hidden: true,
							margin: buttonInsideField === true ? marginInside : defaultMargin,
							cls: buttonInsideField === true ? buttonInsideCls : defaultCls,
							listeners: {
								click: {
									scope: this,
									fn: function(b) {
										var searchFieldId = Ext.getCmp(id);
										var grid = this._get_(gridName);
										var dc = grid._controller_;
										searchFieldId.setValue("");
										b.hide();
										dc.advancedFilter = null;
										dc.doQuery();
									}
								}
							}
						}];
		
						if (extraElems) {
							var c = toolbarItems.concat(extraElems);
							toolbarItems = c;
						}
		
		
		
						return { tbar: toolbarItems}
	}
});
