/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.ExchangeRate_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.ExchangeRateTimeSerie_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.ExchangeRateTimeSerie_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.ExchangeRate_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_ExchangeRate_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"serieName", dataIndex:"serieName", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.TimeSerieLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"serieName", dsField: "serieName"} ],
					filterFieldMapping: [{lovField:"serieType", value: "X"} ]}})
			.addTextField({ name:"description", dataIndex:"description", maxLength:200})
			.addLov({name:"financialSourceCode", dataIndex:"financialSourceCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CodeLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "financialSource"} ],
					filterFieldMapping: [{lovField:"active", value: "true"} ]}})
			.addCombo({ xtype:"combo", name:"serieFreqInd", dataIndex:"serieFreqInd", store:[ __FMBAS_TYPE__.SerieFreqInd._DAILY_, __FMBAS_TYPE__.SerieFreqInd._WEEKDAY_, __FMBAS_TYPE__.SerieFreqInd._WEEKLY_, __FMBAS_TYPE__.SerieFreqInd._SEMIMONTHLY_, __FMBAS_TYPE__.SerieFreqInd._MONTHLY_, __FMBAS_TYPE__.SerieFreqInd._QUARTERLY_]})
			.addLov({name:"currency", dataIndex:"currency1Code", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3}})
			.addLov({name:"reference", dataIndex:"currency2Code", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3}})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __FMBAS_TYPE__.TimeSeriesStatus._WORK_, __FMBAS_TYPE__.TimeSeriesStatus._PUBLISHED_]})
			.addCombo({ xtype:"combo", name:"approvalStatus", dataIndex:"approvalStatus", store:[ __FMBAS_TYPE__.TimeSeriesApprovalStatus._NEW_, __FMBAS_TYPE__.TimeSeriesApprovalStatus._APPROVED_, __FMBAS_TYPE__.TimeSeriesApprovalStatus._REJECTED_, __FMBAS_TYPE__.TimeSeriesApprovalStatus._AWAITING_APPROVAL_]})
			.addDateField({name:"firstUsage", dataIndex:"firstUsage"})
			.addDateField({name:"lastUsage", dataIndex:"lastUsage"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ExchangeRate_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_ExchangeRate_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"serieName", dataIndex:"serieName", width:120})
		.addTextColumn({ name:"description", dataIndex:"description", width:120})
		.addTextColumn({ name:"financialSourceCode", dataIndex:"financialSourceCode", width:120})
		.addTextColumn({ name:"currency", dataIndex:"currency1Code", width:80})
		.addTextColumn({ name:"reference", dataIndex:"currency2Code", width:120})
		.addDateColumn({ name:"firstUsage", dataIndex:"firstUsage", _mask_: Masks.DATE})
		.addDateColumn({ name:"lastUsage", dataIndex:"lastUsage", _mask_: Masks.DATE})
		.addTextColumn({ name:"status", dataIndex:"status", width:100})
		.addTextColumn({ name:"approvalStatus", dataIndex:"approvalStatus", width:130})
		.addTextColumn({ name:"serieType", dataIndex:"serieType", hidden:true, width:60})
		.addTextColumn({ name:"dataProvider", dataIndex:"dataProvider", hidden:true, width:100})
		.addTextColumn({ name:"indicator", dataIndex:"postTypeInd", hidden:true, width:120})
		.addTextColumn({ name:"serieFreqInd", dataIndex:"serieFreqInd", hidden:true, width:100})
		.addNumberColumn({ name:"decimals", dataIndex:"decimals", hidden:true, width:100})
		.addTextColumn({ name:"externalSerieName", dataIndex:"externalSerieName", hidden:true, width:100})
		.addTextColumn({ name:"activation", dataIndex:"activation", hidden:true, width:100})
		.addNumberColumn({ name:"factor", dataIndex:"factor", hidden:true, width:100, sysDec:"dec_prc"})
		.addTextColumn({ name:"supplyItemUpdate", dataIndex:"supplyItemUpdate", hidden:true, width:150})
		.addNumberColumn({ name:"convFactor", dataIndex:"factor", hidden:true, width:100, sysDec:"dec_prc"})
		.addNumberColumn({ name:"arithmOper", dataIndex:"arithmOper", hidden:true, width:100, decimals:6})
		.addBooleanColumn({ name:"active", dataIndex:"active", hidden:true, width:100})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", hidden:true, width:100})
		.addTextColumn({ name:"unit2", dataIndex:"unit2Code", hidden:true, width:100})
		.addTextColumn({ name:"financialSourceName", dataIndex:"financialSourceName", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_afterDefineElements_: function() {
		
						if(_SYSTEMPARAMETERS_.sysexchangerateapproval === "false"){
							this._columns_.getByKey("approvalStatus").hidden = true ;
						}
	}
});

/* ================= EDIT FORM: ApprovalNote ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ExchangeRate_Dc$ApprovalNote", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_ExchangeRate_Dc$ApprovalNote",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.approvalNote}", paramIndex:"approvalNote", width:580, labelAlign:"top", labelStyle:"font-weight:bold", fieldStyle:"margin-top: 5px;"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:580})
		.addPanel({ name:"col1", width:580, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
		                       var remarks = this._get_("remarks");
		                       remarks.setRawValue("");
	}
});

/* ================= GRID: ListV ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ExchangeRate_Dc$ListV", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_ExchangeRate_Dc$ListV",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"fromCurrency", dataIndex:"currency1Code", width:50})
		.addTextColumn({ name:"intoCurrency", dataIndex:"currency2Code", width:50})
		.addTextColumn({ name:"source", dataIndex:"financialSourceName", width:120})
		.addTextColumn({ name:"averagingMethod", dataIndex:"serieFreqInd", width:70})
		.addDateColumn({ name:"validFrom", dataIndex:"firstUsage", _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"lastUsage", _mask_: Masks.DATE})
		.addTextColumn({ name:"serieName", dataIndex:"serieName", hidden:true, width:120})
		.addTextColumn({ name:"description", dataIndex:"description", hidden:true, width:120})
		.addTextColumn({ name:"financialSourceCode", dataIndex:"financialSourceCode", hidden:true, width:120})
		.addDateColumn({ name:"firstUsage", dataIndex:"firstUsage", hidden:true, _mask_: Masks.DATE})
		.addDateColumn({ name:"lastUsage", dataIndex:"lastUsage", hidden:true, _mask_: Masks.DATE})
		.addTextColumn({ name:"status", dataIndex:"status", hidden:true, width:100})
		.addTextColumn({ name:"dataProvider", dataIndex:"dataProvider", hidden:true, width:100})
		.addTextColumn({ name:"indicator", dataIndex:"postTypeInd", hidden:true, width:120})
		.addTextColumn({ name:"serieFreqInd", dataIndex:"serieFreqInd", hidden:true, width:100})
		.addNumberColumn({ name:"decimals", dataIndex:"decimals", hidden:true, width:100})
		.addTextColumn({ name:"externalSerieName", dataIndex:"externalSerieName", hidden:true, width:100})
		.addTextColumn({ name:"activation", dataIndex:"activation", hidden:true, width:100})
		.addNumberColumn({ name:"factor", dataIndex:"factor", hidden:true, width:100, sysDec:"dec_prc"})
		.addTextColumn({ name:"supplyItemUpdate", dataIndex:"supplyItemUpdate", hidden:true, width:150})
		.addNumberColumn({ name:"convFactor", dataIndex:"factor", hidden:true, width:100, sysDec:"dec_prc"})
		.addNumberColumn({ name:"arithmOper", dataIndex:"arithmOper", hidden:true, width:100, decimals:6})
		.addBooleanColumn({ name:"active", dataIndex:"active", hidden:true, width:100})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", hidden:true, width:100})
		.addTextColumn({ name:"unit2", dataIndex:"unit2Code", hidden:true, width:100})
		.addTextColumn({ name:"financialSourceName", dataIndex:"financialSourceName", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: GridV ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ExchangeRate_Dc$GridV", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_ExchangeRate_Dc$GridV",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"fromCurrency", bind:"{d.currency1Code}", dataIndex:"currency1Code", maxLength:3})
		.addTextField({ name:"intoCurrency", bind:"{d.currency2Code}", dataIndex:"currency2Code", maxLength:3})
		.addTextField({ name:"source", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", maxLength:25})
		.addTextField({ name:"averagingMethod", bind:"{d.serieFreqInd}", dataIndex:"serieFreqInd", maxLength:32})
		.addDateField({name:"validFrom", bind:"{d.firstUsage}", dataIndex:"firstUsage"})
		.addDateField({name:"validTo", bind:"{d.lastUsage}", dataIndex:"lastUsage"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:510, layout:"anchor"})
		.addPanel({ name:"col2", width:510, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["fromCurrency", "intoCurrency", "source", "averagingMethod"])
		.addChildrenTo("col2", ["validFrom", "validTo"]);
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ExchangeRate_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_ExchangeRate_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"serieName", bind:"{d.serieName}", dataIndex:"serieName", allowBlank:false, maxLength:32, labelWidth:140})
		.addCombo({ xtype:"combo", name:"dataProvider", bind:"{d.dataProvider}", dataIndex:"dataProvider", allowBlank:false, store:[ __FMBAS_TYPE__.DataProvider._IMPORT_ARGUS_, __FMBAS_TYPE__.DataProvider._IMPORT_DDS_, __FMBAS_TYPE__.DataProvider._IMPORT_OPIS_, __FMBAS_TYPE__.DataProvider._IMPORT_SHARE_, __FMBAS_TYPE__.DataProvider._IMPORT_PLATTS_, __FMBAS_TYPE__.DataProvider._IMPORT_ECB_, __FMBAS_TYPE__.DataProvider._IMPORT_BLOOMBERG_, __FMBAS_TYPE__.DataProvider._USER_MAINTAINED_, __FMBAS_TYPE__.DataProvider._CALCULATED_, __FMBAS_TYPE__.DataProvider._OTHER_], labelWidth:120,listeners:{
			expand:{scope:this, fn:this._filterNotCalculated_}
		}})
		.addTextField({ name:"description", bind:"{d.description}", dataIndex:"description", allowBlank:false, maxLength:200, labelWidth:140})
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", allowBlank:false, xtype:"fmbas_CodeLov_Lov", maxLength:25, labelWidth:120,
			retFieldMapping: [{lovField:"id", dsField: "financialSource"} ,{lovField:"name", dsField: "financialSourceName"} ]})
		.addTextField({ name:"externalSerieName", bind:"{d.externalSerieName}", dataIndex:"externalSerieName", allowBlank:false, maxLength:15, labelWidth:140})
		.addCombo({ xtype:"combo", name:"serieFreqInd", bind:"{d.serieFreqInd}", dataIndex:"serieFreqInd", allowBlank:false, store:[ __FMBAS_TYPE__.SerieFreqInd._DAILY_, __FMBAS_TYPE__.SerieFreqInd._WEEKDAY_, __FMBAS_TYPE__.SerieFreqInd._WEEKLY_, __FMBAS_TYPE__.SerieFreqInd._SEMIMONTHLY_, __FMBAS_TYPE__.SerieFreqInd._MONTHLY_, __FMBAS_TYPE__.SerieFreqInd._QUARTERLY_], labelWidth:120})
		.addLov({name:"currency1Code", bind:"{d.currency1Code}", dataIndex:"currency1Code", allowBlank:false, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:140,
			retFieldMapping: [{lovField:"id", dsField: "currency1Id"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addNumberField({name:"decimals", bind:"{d.decimals}", dataIndex:"decimals", allowBlank:false, width:150, maxLength:3, labelWidth:120})
		.addLov({name:"currency2Code", bind:"{d.currency2Code}", dataIndex:"currency2Code", allowBlank:false, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:140,
			retFieldMapping: [{lovField:"id", dsField: "currency2Id"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addCombo({ xtype:"combo", name:"postTypeInd", bind:"{d.postTypeInd}", dataIndex:"postTypeInd", allowBlank:false, store:[ __FMBAS_TYPE__.PostTypeInd._AMOUNT_, __FMBAS_TYPE__.PostTypeInd._PRICE_], labelWidth:140})
		.add({name:"nameProvider", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("serieName"),this._getConfig_("dataProvider")]})
		.add({name:"descriptionSource", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("description"),this._getConfig_("financialSourceCode")]})
		.add({name:"codeFrequency", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("externalSerieName"),this._getConfig_("serieFreqInd")]})
		.add({name:"currencyDecimals", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("currency1Code"),this._getConfig_("decimals")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:620, layout:"anchor"})
		.addPanel({ name:"col2", width:620, layout:"anchor"})
		.addPanel({ name:"col3", width:620, layout:"anchor"})
		.addPanel({ name:"col4", width:620, layout:"anchor"})
		.addPanel({ name:"col5", width:300, layout:"anchor"})
		.addPanel({ name:"col6", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5", "col6"])
		.addChildrenTo("col1", ["nameProvider"])
		.addChildrenTo("col2", ["descriptionSource"])
		.addChildrenTo("col3", ["codeFrequency"])
		.addChildrenTo("col4", ["currencyDecimals"])
		.addChildrenTo("col5", ["currency2Code"])
		.addChildrenTo("col6", ["postTypeInd"]);
	},
	/* ==================== Business functions ==================== */
	
	_filterNotCalculated_: function(el) {
		
						var store = el.store;
		
						store.filterBy(function (record) {
		                	if (record.get("field1") !== __FMBAS_TYPE__.DataProvider._CALCULATED_) return record;
		                });
		
	}
});

/* ================= EDIT FORM: EditContext ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ExchangeRate_Dc$EditContext", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_ExchangeRate_Dc$EditContext",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"serieName", bind:"{d.serieName}", dataIndex:"serieName", allowBlank:false, maxLength:32, labelWidth:140})
		.addCombo({ xtype:"combo", name:"dataProvider", bind:"{d.dataProvider}", dataIndex:"dataProvider", allowBlank:false, store:[ __FMBAS_TYPE__.DataProvider._IMPORT_ARGUS_, __FMBAS_TYPE__.DataProvider._IMPORT_DDS_, __FMBAS_TYPE__.DataProvider._IMPORT_OPIS_, __FMBAS_TYPE__.DataProvider._IMPORT_SHARE_, __FMBAS_TYPE__.DataProvider._IMPORT_PLATTS_, __FMBAS_TYPE__.DataProvider._IMPORT_ECB_, __FMBAS_TYPE__.DataProvider._IMPORT_BLOOMBERG_, __FMBAS_TYPE__.DataProvider._USER_MAINTAINED_, __FMBAS_TYPE__.DataProvider._CALCULATED_, __FMBAS_TYPE__.DataProvider._OTHER_], labelWidth:100,listeners:{
			expand:{scope:this, fn:this._filterNotCalculated_}
		}})
		.addTextField({ name:"description", bind:"{d.description}", dataIndex:"description", allowBlank:false, maxLength:200, labelWidth:140})
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", allowBlank:false, xtype:"fmbas_CodeLov_Lov", maxLength:25, labelWidth:100,
			retFieldMapping: [{lovField:"id", dsField: "financialSource"} ,{lovField:"name", dsField: "financialSourceName"} ]})
		.addTextField({ name:"externalSerieName", bind:"{d.externalSerieName}", dataIndex:"externalSerieName", allowBlank:false, maxLength:15, labelWidth:140})
		.addCombo({ xtype:"combo", name:"serieFreqInd", bind:"{d.serieFreqInd}", dataIndex:"serieFreqInd", noEdit:true , allowBlank:false, store:[ __FMBAS_TYPE__.SerieFreqInd._DAILY_, __FMBAS_TYPE__.SerieFreqInd._WEEKDAY_, __FMBAS_TYPE__.SerieFreqInd._WEEKLY_, __FMBAS_TYPE__.SerieFreqInd._SEMIMONTHLY_, __FMBAS_TYPE__.SerieFreqInd._MONTHLY_, __FMBAS_TYPE__.SerieFreqInd._QUARTERLY_]})
		.addLov({name:"currency1Code", bind:"{d.currency1Code}", dataIndex:"currency1Code", allowBlank:false, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:140,
			retFieldMapping: [{lovField:"id", dsField: "currency1Id"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addNumberField({name:"decimals", bind:"{d.decimals}", dataIndex:"decimals", allowBlank:false, width:150, maxLength:3, labelWidth:100})
		.addLov({name:"currency2Code", bind:"{d.currency2Code}", dataIndex:"currency2Code", allowBlank:false, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:140,
			retFieldMapping: [{lovField:"id", dsField: "currency2Id"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addCombo({ xtype:"combo", name:"postTypeInd", bind:"{d.postTypeInd}", dataIndex:"postTypeInd", allowBlank:false, store:[ __FMBAS_TYPE__.PostTypeInd._AMOUNT_, __FMBAS_TYPE__.PostTypeInd._PRICE_], labelWidth:140})
		.addDateField({name:"firstUsage", bind:"{d.firstUsage}", dataIndex:"firstUsage", noEdit:true , labelWidth:100})
		.addDateField({name:"lastUsage", bind:"{d.lastUsage}", dataIndex:"lastUsage", noEdit:true , labelWidth:100})
		.addHiddenField({ name:"hasTimeSerieItems", bind:"{d.hasTimeSerieItems}", dataIndex:"hasTimeSerieItems"})
		.add({name:"nameProviderQuotedFrom", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("serieName"),this._getConfig_("dataProvider"),this._getConfig_("firstUsage")]})
		.add({name:"descriptionSourceQuotedTo", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("description"),this._getConfig_("financialSourceCode"),this._getConfig_("lastUsage")]})
		.add({name:"codeFrequency", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("externalSerieName"),this._getConfig_("serieFreqInd")]})
		.add({name:"currencyDecimals", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("currency1Code"),this._getConfig_("decimals")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:900, layout:"anchor"})
		.addPanel({ name:"col2", width:900, layout:"anchor"})
		.addPanel({ name:"col3", width:660, layout:"anchor"})
		.addPanel({ name:"col4", width:660, layout:"anchor"})
		.addPanel({ name:"col5", width:300, layout:"anchor"})
		.addPanel({ name:"col6", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5", "col6"])
		.addChildrenTo("col1", ["nameProviderQuotedFrom"])
		.addChildrenTo("col2", ["descriptionSourceQuotedTo"])
		.addChildrenTo("col3", ["codeFrequency"])
		.addChildrenTo("col4", ["currencyDecimals"])
		.addChildrenTo("col5", ["currency2Code"])
		.addChildrenTo("col6", ["postTypeInd", "hasTimeSerieItems"]);
	},
	/* ==================== Business functions ==================== */
	
	_filterNotCalculated_: function(el) {
		
						var store = el.store;
		
						store.filterBy(function (record) {
		                	if (record.get("field1") !== __FMBAS_TYPE__.DataProvider._CALCULATED_) return record;
		                });
		
	}
});

/* ================= EDIT FORM: EditQuotationContext ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ExchangeRate_Dc$EditQuotationContext", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_ExchangeRate_Dc$EditQuotationContext",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"serieName", bind:"{d.serieName}", dataIndex:"serieName", noEdit:true , maxLength:32})
		.addCombo({ xtype:"combo", name:"dataProvider", bind:"{d.dataProvider}", dataIndex:"dataProvider", noEdit:true , store:[ __FMBAS_TYPE__.DataProvider._IMPORT_ARGUS_, __FMBAS_TYPE__.DataProvider._IMPORT_DDS_, __FMBAS_TYPE__.DataProvider._IMPORT_OPIS_, __FMBAS_TYPE__.DataProvider._IMPORT_SHARE_, __FMBAS_TYPE__.DataProvider._IMPORT_PLATTS_, __FMBAS_TYPE__.DataProvider._IMPORT_ECB_, __FMBAS_TYPE__.DataProvider._IMPORT_BLOOMBERG_, __FMBAS_TYPE__.DataProvider._USER_MAINTAINED_, __FMBAS_TYPE__.DataProvider._CALCULATED_, __FMBAS_TYPE__.DataProvider._OTHER_], labelWidth:120})
		.addTextField({ name:"description", bind:"{d.description}", dataIndex:"description", noEdit:true , maxLength:200})
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", noEdit:true , xtype:"fmbas_CodeLov_Lov", maxLength:25, labelWidth:120,
			retFieldMapping: [{lovField:"id", dsField: "financialSource"} ]})
		.addCombo({ xtype:"combo", name:"serieFreqInd", bind:"{d.serieFreqInd}", dataIndex:"serieFreqInd", noEdit:true , store:[ __FMBAS_TYPE__.SerieFreqInd._DAILY_, __FMBAS_TYPE__.SerieFreqInd._WEEKDAY_, __FMBAS_TYPE__.SerieFreqInd._WEEKLY_, __FMBAS_TYPE__.SerieFreqInd._SEMIMONTHLY_, __FMBAS_TYPE__.SerieFreqInd._MONTHLY_, __FMBAS_TYPE__.SerieFreqInd._QUARTERLY_], labelWidth:120})
		.addLov({name:"currency1Code", bind:"{d.currency1Code}", dataIndex:"currency1Code", noEdit:true , xtype:"fmbas_CurrenciesLov_Lov", maxLength:3,
			retFieldMapping: [{lovField:"id", dsField: "currency1Id"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addLov({name:"currency2Code", bind:"{d.currency2Code}", dataIndex:"currency2Code", noEdit:true , xtype:"fmbas_CurrenciesLov_Lov", maxLength:3,
			retFieldMapping: [{lovField:"id", dsField: "currency2Id"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addDateField({name:"firstUsage", bind:"{d.firstUsage}", dataIndex:"firstUsage", noEdit:true , labelWidth:100})
		.addDateField({name:"lastUsage", bind:"{d.lastUsage}", dataIndex:"lastUsage", noEdit:true , labelWidth:100})
		.add({name:"nameProviderQuotedFrom", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("serieName"),this._getConfig_("dataProvider"),this._getConfig_("firstUsage")]})
		.add({name:"descriptionSourceQuotedTo", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("description"),this._getConfig_("financialSourceCode"),this._getConfig_("lastUsage")]})
		.add({name:"codeFrequency", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("currency1Code"),this._getConfig_("serieFreqInd")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:900, layout:"anchor"})
		.addPanel({ name:"col2", width:900, layout:"anchor"})
		.addPanel({ name:"col3", width:660, layout:"anchor"})
		.addPanel({ name:"col5", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col5"])
		.addChildrenTo("col1", ["nameProviderQuotedFrom"])
		.addChildrenTo("col2", ["descriptionSourceQuotedTo"])
		.addChildrenTo("col3", ["codeFrequency"])
		.addChildrenTo("col5", ["currency2Code"]);
	}
});
