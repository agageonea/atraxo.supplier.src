/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.CustomersMp_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.CustomersMp_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("customer", Ext.create(atraxo.fmbas.ui.extjs.dc.CustomersMp_Dc,{trackEditMode: true}))
		.addDc("contact", Ext.create(atraxo.fmbas.ui.extjs.dc.Contacts_Dc,{}))
		.addDc("locations", Ext.create(atraxo.fmbas.ui.extjs.dc.CustomerLocations_Dc,{}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.addDc("attachment", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.linkDc("contact", "customer",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("locations", "customer",{fetchMode:"auto",fields:[
					{childParam:"customerId", parentField:"id"}]})
				.linkDc("note", "customer",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("attachment", "customer",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}]})
				.linkDc("history", "customer",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnCustomerActivate",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnCustomerActivate,stateManager:[{ name:"selected_not_zero", dc:"customer", and: function(dc) {return (this.canApprove(dc));} }], scope:this})
		.addButton({name:"btnCustomerActivateEdit",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnCustomerActivateEdit,stateManager:[{ name:"selected_one", dc:"customer", and: function(dc) {return ((dc.record.data.status==__FMBAS_TYPE__.CustomerStatus._PROSPECT_ || dc.record.data.status==__FMBAS_TYPE__.CustomerStatus._INACTIVE_) && this._getDc_("customer").isReadOnly() == false);} }], scope:this})
		.addButton({name:"btnCustomerDeactivate",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnCustomerDeactivate,stateManager:[{ name:"selected_one", dc:"customer", and: function(dc) {return ((dc.record.data.status==__FMBAS_TYPE__.CustomerStatus._ACTIVE_ || dc.record.data.status==__FMBAS_TYPE__.CustomerStatus._PROSPECT_) && this._getDc_("customer").isReadOnly() == false);} }], scope:this})
		.addButton({name:"btnCustomerDeactivateEdit",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnCustomerDeactivateEdit,stateManager:[{ name:"selected_one", dc:"customer", and: function(dc) {return ((dc.record.data.status==__FMBAS_TYPE__.CustomerStatus._ACTIVE_ || dc.record.data.status==__FMBAS_TYPE__.CustomerStatus._PROSPECT_) && this._getDc_("customer").isReadOnly() == false);} }], scope:this})
		.addButton({name:"btnCustomerSaveCloseWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnCustomerSaveCloseWdw, scope:this})
		.addButton({name:"btnCustomerSaveEditWdw",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnCustomerSaveEditWdw, scope:this})
		.addButton({name:"btnCustomerSaveNewWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnCustomerSaveNewWdw, scope:this})
		.addButton({name:"btnCustomerCacelCloseWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCustomerCacelCloseWdw, scope:this})
		.addButton({name:"btnContactSaveNewWdw", disabled:false, handler: this.onBtnContactSaveNewWdw, scope:this})
		.addButton({name:"btnContactSaveCloseWdw", disabled:false, handler: this.onBtnContactSaveCloseWdw, scope:this})
		.addButton({name:"btnContactCancelCloseWdw", disabled:false, handler: this.onBtnContactCancelCloseWdw, scope:this})
		.addButton({name:"assignCustomerLocations",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onAssignCustomerLocations, scope:this})
		.addButton({name:"deleteCustomerLocations",glyph:fp_asc.minus_glyph.glyph,iconCls: fp_asc.minus_glyph.css, disabled:true, handler: this.onDeleteCustomerLocations,stateManager:[{ name:"selected_not_zero", dc:"locations"}], scope:this})
		.addButton({name:"btnNoteSaveCloseWdw", disabled:false, handler: this.onBtnNoteSaveCloseWdw, scope:this})
		.addButton({name:"btnNoteCloseWdw", disabled:false, handler: this.onBtnNoteCloseWdw, scope:this})
		.addButton({name:"btnHistorySaveCloseDeactivateWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnHistorySaveCloseDeactivateWdw, scope:this})
		.addButton({name:"btnCancelDeactivateWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelDeactivateWdw, scope:this})
		.addDcFilterFormView("customer", {name:"customerFilter", xtype:"fmbas_CustomersMp_Dc$Filter"})
		.addDcGridView("customer", {name:"customerList", xtype:"fmbas_CustomersMp_Dc$List"})
		.addDcFormView("customer", {name:"customerNew", xtype:"fmbas_CustomersMp_Dc$New"})
		.addDcFormView("customer", {name:"customerEdit", xtype:"fmbas_CustomersMp_Dc$Edit", _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcFormView("customer", {name:"deactivateRemarkEdit", xtype:"fmbas_CustomersMp_Dc$EditRemark"})
		.addDcFormView("customer", {name:"customerAddresses", xtype:"fmbas_CustomersMp_Dc$Addresses"})
		.addDcFilterFormView("contact", {name:"contactFilter", xtype:"fmbas_Contacts_Dc$Filter"})
		.addDcGridView("contact", {name:"contactsList", xtype:"fmbas_Contacts_Dc$List"})
		.addDcFormView("contact", {name:"contactEdit", xtype:"fmbas_Contacts_Dc$Edit"})
		.addDcFormView("contact", {name:"contactEditContext", xtype:"fmbas_Contacts_Dc$EditDetails"})
		.addDcFormView("contact", {name:"contactTab", _hasTitle_:true, xtype:"fmbas_Contacts_Dc$Tab"})
		.addDcFilterFormView("locations", {name:"locationsFilter", xtype:"fmbas_CustomerLocations_Dc$Filter"})
		.addDcGridView("locations", {name:"locationsList", xtype:"fmbas_CustomerLocations_Dc$CustomerLocation"})
		.addDcGridView("note", {name:"notesList", xtype:"fmbas_Note_Dc$List"})
		.addDcFormView("note", {name:"notesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addDcGridView("attachment", {name:"attachmentList", xtype:"ad_Attachment_Dc$List"})
		.addDcFormView("attachment", {name:"attachmentEdit", xtype:"ad_Attachment_Dc$New"})
		.addDcGridView("history", {name:"historyList", xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addWindow({name:"customerWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("customerNew")],  listeners:{ close:{fn:this.onCustomerCloseWdw, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCustomerSaveCloseWdw"), this._elems_.get("btnCustomerSaveEditWdw"), this._elems_.get("btnCustomerSaveNewWdw"), this._elems_.get("btnCustomerCacelCloseWdw")]}]})
		.addWindow({name:"customerDeactivateWdw", _hasTitle_:true, width:440, height:220, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("deactivateRemarkEdit")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnHistorySaveCloseDeactivateWdw"), this._elems_.get("btnCancelDeactivateWdw")]}]})
		.addWindow({name:"contactWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("contactEdit")],  listeners:{ close:{fn:this.onContactCloseWdw, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnContactSaveNewWdw"), this._elems_.get("btnContactSaveCloseWdw"), this._elems_.get("btnContactCancelCloseWdw")]}]})
		.addWindow({name:"noteWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("notesEdit")],  listeners:{ close:{fn:this.onNoteCloseWdw, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnNoteSaveCloseWdw"), this._elems_.get("btnNoteCloseWdw")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, xtype:"panel", scrollable:"y"})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTabContact", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"dp1", _hasTitle_:true, layout:"fit", collapsible:true, titleCollapse:true, xtype:"panel", header:{titlePosition: 2}})
		.addPanel({name:"dp2", _hasTitle_:true, layout:"fit", collapsible:true, titleCollapse:true, xtype:"panel", header:{titlePosition: 2}})
		.addPanel({name:"dp3", _hasTitle_:true, layout:"fit", collapsible:true, titleCollapse:true, xtype:"panel", header:{titlePosition: 2}})
		.addPanel({name:"dp4", _hasTitle_:true, layout:"fit", collapsible:true, titleCollapse:true, collapsed:true,
		 xtype:"panel", header:{titlePosition: 2}})
		.addPanel({name:"dp5", _hasTitle_:true, layout:"fit", collapsible:true, titleCollapse:true, collapsed:true,
		 xtype:"panel", header:{titlePosition: 2}})
		.addPanel({name:"dp6", _hasTitle_:true, layout:"fit", collapsible:true, titleCollapse:true, collapsed:true,
		 xtype:"panel", header:{titlePosition: 2}})
		.addTabPanelForUserFields({tabPanelName:"detailsTabContact", containerPanelName:"canvas3", dcName:"contact"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["customerList"], ["center"])
		.addChildrenTo("canvas2", ["customerEdit", "dp1", "dp2", "dp3", "dp4", "dp5", "dp6"])
		.addChildrenTo("canvas3", ["contactEditContext", "detailsTabContact"], ["north", "center"])
		.addChildrenTo("detailsTabContact", ["contactTab"])
		.addChildrenTo("dp1", ["customerAddresses"])
		.addChildrenTo("dp2", ["contactsList"])
		.addChildrenTo("dp3", ["locationsList"])
		.addToolbarTo("customerList", "tlbCustomerList")
		.addToolbarTo("customerEdit", "tlbCustomerEdit")
		.addToolbarTo("contactsList", "tlbContacts")
		.addToolbarTo("locationsList", "tlbCustomerLocations")
		.addToolbarTo("contactEditContext", "tlbContactContext")
		.addToolbarTo("notesList", "tlbNoteList")
		.addToolbarTo("attachmentList", "tlbAttachments");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2", "canvas3"])
		.addChildrenTo2("dp4", ["notesList"])
		.addChildrenTo2("dp5", ["historyList"])
		.addChildrenTo2("dp6", ["attachmentList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbCustomerList", {dc: "customer"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCustomerActivate"),this._elems_.get("btnCustomerDeactivate")])
			.addReports()
		.end()
		.beginToolbar("tlbCustomerEdit", {dc: "customer"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCustomerActivateEdit"),this._elems_.get("btnCustomerDeactivateEdit")])
			.addReports()
		.end()
		.beginToolbar("tlbContacts", {dc: "contact"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas3"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbCustomerLocations", {dc: "locations"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("assignCustomerLocations"),this._elems_.get("deleteCustomerLocations")])
			.addReports()
		.end()
		.beginToolbar("tlbContactContext", {dc: "contact"})
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas2"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbNoteList", {dc: "note"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbAttachments", {dc: "attachment"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end();
	}
	
	,_applyContentLoadRestriction_: function(){
		this._getBuilder_()
		.addContentLoadRestriction("contact","dp2")
		.addContentLoadRestriction("locations","dp3")
		.addContentLoadRestriction("note","dp4")
		.addContentLoadRestriction("attachment","dp5")
		.addContentLoadRestriction("history","dp5")
		;
	}

	
	/**
	 * On-Click handler for button btnCustomerActivate
	 */
	,onBtnCustomerActivate: function() {
		var successFn = function() {
			this._getDc_("customer").doReloadPage();
			this._getDc_("history").doReloadPage();
			this._applyStateAllButtons_()
		};
		var o={
			name:"approveList",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("customer").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnCustomerActivateEdit
	 */
	,onBtnCustomerActivateEdit: function() {
		var successFn = function() {
			this._getDc_("customer").doReloadPage();
			this._getDc_("history").doReloadPage();
			this._applyStateAllButtons_()
		};
		var o={
			name:"approve",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("customer").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCustomerDeactivate
	 */
	,onBtnCustomerDeactivate: function() {
		this.deactivateCustomer();
	}
	
	/**
	 * On-Click handler for button btnCustomerDeactivateEdit
	 */
	,onBtnCustomerDeactivateEdit: function() {
		this.deactivateCustomer();
	}
	
	/**
	 * On-Click handler for button btnCustomerSaveCloseWdw
	 */
	,onBtnCustomerSaveCloseWdw: function() {
		this.saveCloseCustomer();
		this._getDc_("customer").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCustomerSaveEditWdw
	 */
	,onBtnCustomerSaveEditWdw: function() {
		this.saveEditCustomer();
	}
	
	/**
	 * On-Click handler for button btnCustomerSaveNewWdw
	 */
	,onBtnCustomerSaveNewWdw: function() {
		this.saveNewCustomer();
		this._getDc_("customer").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCustomerCacelCloseWdw
	 */
	,onBtnCustomerCacelCloseWdw: function() {
		this.onCustomerCloseWdw();
		this._getWindow_("customerWdw").close();
	}
	
	/**
	 * On-Click handler for button btnContactSaveNewWdw
	 */
	,onBtnContactSaveNewWdw: function() {
		this.saveNewContact();
	}
	
	/**
	 * On-Click handler for button btnContactSaveCloseWdw
	 */
	,onBtnContactSaveCloseWdw: function() {
		this.saveCloseContact();
		this._getDc_("contact").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnContactCancelCloseWdw
	 */
	,onBtnContactCancelCloseWdw: function() {
		this.onContactCloseWdw();
		this._getWindow_("contactWdw").close();
	}
	
	/**
	 * On-Click handler for button assignCustomerLocations
	 */
	,onAssignCustomerLocations: function() {
		this._showAsgnWindow_("atraxo.fmbas.ui.extjs.asgn.CustomerLocations_Asgn$Ui" ,{dc: "customer", objectIdField: "id"
		,listeners: {close: {scope: this, fn: function() { this._getDc_("locations").doReloadPage();
		}} }});
	}
	
	/**
	 * On-Click handler for button deleteCustomerLocations
	 */
	,onDeleteCustomerLocations: function() {
		var successFn = function() {
			this._getDc_("locations").doReloadPage();
		};
		var o={
			name:"removeList",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("locations").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnNoteSaveCloseWdw
	 */
	,onBtnNoteSaveCloseWdw: function() {
		this.saveCloseNote();
		this._getDc_("note").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnNoteCloseWdw
	 */
	,onBtnNoteCloseWdw: function() {
		this.onNoteCloseWdw();
		this._getWindow_("noteWdw").close();
	}
	
	/**
	 * On-Click handler for button btnHistorySaveCloseDeactivateWdw
	 */
	,onBtnHistorySaveCloseDeactivateWdw: function() {
		var successFn = function(dc) {
			this._getDc_("customer").doReloadPage();
			this._getWindow_("customerDeactivateWdw").close();
			dc.setParamValue("remark","")
			this._getDc_("history").doReloadPage();
			this._applyStateAllButtons_()
		};
		var o={
			name:"deactivate",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("customer").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCancelDeactivateWdw
	 */
	,onBtnCancelDeactivateWdw: function() {
		this._getWindow_("customerDeactivateWdw").close();
	}
	
	,customerShowWdw: function() {	
		this._getWindow_("customerWdw").show();
	}
	
	,contactShowWdw: function() {	
		this._getWindow_("contactWdw").show();
	}
	
	,noteShowWdw: function() {	
		this._getWindow_("noteWdw").show();
	}
	
	,_afterDefineDcs_: function() {
		
				var customer = this._getDc_("customer");			
				var contact = this._getDc_("contact");
				var note = this._getDc_("note");
		
				customer.on("afterDoNew", function (dc) {
					var rec = dc.getRecord();
					dc.setEditMode();
				    this.customerShowWdw();
				}, this);
		
				customer.on("afterDoSaveSuccess", function (dc, ajaxResult) { 
					if (ajaxResult.options.options.doNewAfterSave === true) {
						dc.doNew(); 
					} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
						this._getWindow_("customerWdw").close();
					} else if (ajaxResult.options.options.doCloseEditAfterSave === true) {
						this._getWindow_("customerWdw").close();
						dc.doEditIn();
					}
				}, this);
		
				contact.on("afterDoNew", function (dc) {
					dc.setEditMode();
					this.contactShowWdw();
				}, this);
		
				note.on("afterDoNew", function (dc) {
					dc.setEditMode();
					this.noteShowWdw();
				}, this);
		
				note.on("afterDoEditIn", function (dc) {
					this._getWindow_("noteWdw").show();
				}, this);
		
				note.on("afterDoSaveSuccess", function (dc, ajaxResult) { 
					if (ajaxResult.options.options.doNewAfterSave === true) {
						dc.doNew(); 
					} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
						this._getWindow_("noteWdw").close();
					}
				}, this);
	}
	
	,saveNewCustomer: function() {
		
				var customer = this._getDc_("customer");
				customer.doSave({doNewAfterSave:true});
	}
	
	,saveCloseCustomer: function() {
		
				var customer = this._getDc_("customer");
				customer.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveEditCustomer: function() {
		
				var customer = this._getDc_("customer");
				customer.doSave({doCloseEditAfterSave: true });
	}
	
	,onCustomerCloseWdw: function() {
		
				var dc = this._getDc_("customer");
				dc.clearEditMode();
				if(dc.isDirty()){
					dc.doCancel();
				}
	}
	
	,canApprove: function(dc) {
		 
				var selectedRecords = dc.selectedRecords;				
				for(var i=0 ; i<selectedRecords.length ; ++i){
					var record = selectedRecords[i];
					if( !(record.data.status == __FMBAS_TYPE__.CustomerStatus._PROSPECT_ || record.data.status == __FMBAS_TYPE__.CustomerStatus._INACTIVE_) ){
						return false;
					}
				}
				return true;
	}
	
	,deactivateCustomer: function() {
		
				var label = "You are about to make this customer inactive.<br/> Please provide the reason for this.";
				var title = "Deactivate Customer";
				this.setupHistoryWindow("customerDeactivateWdw","deactivateRemarkEdit","btnHistorySaveCloseDeactivateWdw","btnCancelDeactivateWdw" , label , title);				
	}
	
	,saveNewContact: function() {
		
				var contact = this._getDc_("contact");
				contact.doSave({doNewAfterSave:true});
	}
	
	,saveCloseContact: function() {
		
				var contact = this._getDc_("contact");
				contact.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,onContactCloseWdw: function() {
		
				var dc = this._getDc_("contact");
				dc.clearEditMode();
				if(dc.isDirty()){
					dc.doCancel();
				}
	}
	
	,saveCloseNote: function() {
		
				var note = this._getDc_("note");
				note.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,onNoteCloseWdw: function() {
		
				var dc = this._getDc_("note");
				dc.clearEditMode();
				if(dc.isDirty()){
					dc.doCancel();
				}
	}
	
	,setupHistoryWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
				var window = this._getWindow_(theWindow);
				var remarksField = this._get_(theForm)._get_("remarks");
				var saveButton = this._get_(saveBtn);
				var cancelButton = this._get_(cancelBtn);
				
				saveButton.setDisabled(true);
				cancelButton.setDisabled(false);
				remarksField.setReadOnly(false);
				
				remarksField.fieldLabel = label;
				window.title = title;
				
				window.show();
		
				remarksField.on("change", function(field) {
				    if (field.value.length > 0) {
				        saveButton.setDisabled(false);
				    } else {
				        saveButton.setDisabled(true);
				    }
				});
	}
});
