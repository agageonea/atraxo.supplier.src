/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.WorkflowInstance_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_WorkflowInstance_Ds"
	},
	
	
	fields: [
		{name:"workflowId", type:"int", allowNull:true},
		{name:"workflowCode", type:"string"},
		{name:"startExecutionUser", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"status", type:"string"},
		{name:"step", type:"string"},
		{name:"result", type:"string"},
		{name:"startExecutionDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"workflowVersion", type:"int", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.WorkflowInstance_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"workflowId", type:"int", allowNull:true},
		{name:"workflowCode", type:"string"},
		{name:"startExecutionUser", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"status", type:"string"},
		{name:"step", type:"string"},
		{name:"result", type:"string"},
		{name:"startExecutionDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"workflowVersion", type:"int", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.fmbas.ui.extjs.ds.WorkflowInstance_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"terminateWfkInstancesResult", type:"boolean"},
		{name:"terminateWkfInstancesDescription", type:"string"},
		{name:"wfkTerminateOption", type:"string"}
	]
});
