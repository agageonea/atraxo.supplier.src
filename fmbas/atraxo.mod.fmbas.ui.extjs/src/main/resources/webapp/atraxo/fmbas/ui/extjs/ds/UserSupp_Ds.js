/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.UserSupp_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_UserSupp_Ds"
	},
	
	
	validators: {
		name: [{type: 'presence'}],
		loginName: [{type: 'presence'}],
		firstName: [{type: 'presence'}],
		lastName: [{type: 'presence'}],
		email: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("active", true);
		this.set("department", "");
		this.set("workOffice", "");
		this.set("title", "");
	},
	
	fields: [
		{name:"loginName", type:"string"},
		{name:"code", type:"string"},
		{name:"email", type:"string", noFilter:true},
		{name:"decimalSeparator", type:"string"},
		{name:"thousandSeparator", type:"string"},
		{name:"dateFormatId", type:"string", noFilter:true},
		{name:"dateFormat", type:"string", noFilter:true},
		{name:"locId", type:"int", allowNull:true, noFilter:true},
		{name:"locCode", type:"string"},
		{name:"title", type:"string"},
		{name:"firstName", type:"string"},
		{name:"lastName", type:"string"},
		{name:"jobTitle", type:"string"},
		{name:"department", type:"string"},
		{name:"workOffice", type:"string"},
		{name:"businessPhone", type:"string"},
		{name:"mobilePhone", type:"string"},
		{name:"picture", type:"string"},
		{name:"userLanguage", type:"string"},
		{name:"loginCode", type:"string", noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"pictureField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"string", noFilter:true},
		{name:"clientId", type:"string", noFilter:true},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string", noFilter:true},
		{name:"notes", type:"string", noFilter:true},
		{name:"active", type:"boolean"},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true},
		{name:"createdBy", type:"string", noFilter:true},
		{name:"modifiedBy", type:"string", noFilter:true},
		{name:"version", type:"int", allowNull:true, noFilter:true},
		{name:"refid", type:"string", noFilter:true},
		{name:"entityAlias", type:"string", noFilter:true},
		{name:"entityFqn", type:"string", noFilter:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.UserSupp_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"loginName", type:"string"},
		{name:"code", type:"string"},
		{name:"email", type:"string", noFilter:true},
		{name:"decimalSeparator", type:"string"},
		{name:"thousandSeparator", type:"string"},
		{name:"dateFormatId", type:"string", noFilter:true},
		{name:"dateFormat", type:"string", noFilter:true},
		{name:"locId", type:"int", allowNull:true, noFilter:true},
		{name:"locCode", type:"string"},
		{name:"title", type:"string"},
		{name:"firstName", type:"string"},
		{name:"lastName", type:"string"},
		{name:"jobTitle", type:"string"},
		{name:"department", type:"string"},
		{name:"workOffice", type:"string"},
		{name:"businessPhone", type:"string"},
		{name:"mobilePhone", type:"string"},
		{name:"picture", type:"string"},
		{name:"userLanguage", type:"string"},
		{name:"loginCode", type:"string", noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"pictureField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"string", noFilter:true},
		{name:"clientId", type:"string", noFilter:true},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string", noFilter:true},
		{name:"notes", type:"string", noFilter:true},
		{name:"active", type:"boolean", allowNull:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true},
		{name:"createdBy", type:"string", noFilter:true},
		{name:"modifiedBy", type:"string", noFilter:true},
		{name:"version", type:"int", allowNull:true, noFilter:true},
		{name:"refid", type:"string", noFilter:true},
		{name:"entityAlias", type:"string", noFilter:true},
		{name:"entityFqn", type:"string", noFilter:true}
	]
});

Ext.define("atraxo.fmbas.ui.extjs.ds.UserSupp_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"activitiesSize", type:"int", allowNull:true},
		{name:"avatar", type:"string"},
		{name:"confirmPassword", type:"string"},
		{name:"inGroup", type:"string", forFilter:true},
		{name:"inGroupId", type:"string"},
		{name:"newPassword", type:"string"},
		{name:"rpcResponse", type:"string"},
		{name:"withRole", type:"string", forFilter:true},
		{name:"withRoleId", type:"string"}
	]
});
