/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.WorkflowTask_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.WorkflowTask_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("workflowTask", Ext.create(atraxo.fmbas.ui.extjs.dc.WorkflowTask_Dc,{multiEdit: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnClaimWkfTaskRpc",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnClaimWkfTaskRpc,stateManager:[{ name:"selected_one", dc:"workflowTask", and: function(dc) {return (this.canClaimTask(dc));} }], scope:this})
		.addButton({name:"btnCompleteWkfTaskRpc",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnCompleteWkfTaskRpc,stateManager:[{ name:"selected_one", dc:"workflowTask", and: function(dc) {return (this.canCompleteTask(dc));} }], scope:this})
		.addButton({name:"btnAcceptTask",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnAcceptTask, scope:this})
		.addButton({name:"btnRejectTask",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnRejectTask, scope:this})
		.addDcFilterFormView("workflowTask", {name:"filter", xtype:"fmbas_WorkflowTask_Dc$Filter"})
		.addDcEditGridView("workflowTask", {name:"workflowTaskEdit", xtype:"fmbas_WorkflowTask_Dc$WorkFlowTaskForm", frame:true})
		.addDcFormView("workflowTask", {name:"workflowCompleteTaskEdit", xtype:"fmbas_WorkflowTask_Dc$EditCompleteTask"})
		.addWindow({name:"wdwCompleteTask", _hasTitle_:true, width:440, height:220, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("workflowCompleteTaskEdit")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnAcceptTask"), this._elems_.get("btnRejectTask")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["workflowTaskEdit"], ["center"])
		.addToolbarTo("workflowTaskEdit", "tlbWorkflowTaskForm");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbWorkflowTaskForm", {dc: "workflowTask"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnClaimWkfTaskRpc"),this._elems_.get("btnCompleteWkfTaskRpc")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnClaimWkfTaskRpc
	 */
	,onBtnClaimWkfTaskRpc: function() {
		var successFn = function() {
			this._getDc_("workflowTask").doReloadPage();
		};
		var o={
			name:"claimTask",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("workflowTask").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCompleteWkfTaskRpc
	 */
	,onBtnCompleteWkfTaskRpc: function() {
		this.openCompleteTaskWindow();
	}
	
	/**
	 * On-Click handler for button btnAcceptTask
	 */
	,onBtnAcceptTask: function() {
		this.completeFinalTask(true)
	}
	
	/**
	 * On-Click handler for button btnRejectTask
	 */
	,onBtnRejectTask: function() {
		this.completeFinalTask(false)
	}
	
	,completeFinalTask: function(accepted) {
		
						var dc = this._getDc_("workflowTask");
						var r = dc.getRecord();
						if (r) {
							r.set("workflowTaskAcceptance",accepted);
						}
						var reloadWdwFn = function(dc) {
							var w = this._getWindow_("wdwCompleteTask");
							w.close();
							dc.doReloadPage();
						}
						var o={
							name:"completeTask",
							modal:true,
							callbacks:{
								successFn: reloadWdwFn,
								failureFn: reloadWdwFn,
								successScope: this
							}
						};
						dc.doRpcData(o);
	}
	
	,openCompleteTaskWindow: function() {
		
						var label = "Please provide a note for completing this task.";
						var title = "Complete Task";
						this.setupHistoryWindow("wdwCompleteTask","workflowCompleteTaskEdit","btnAcceptTask","btnRejectTask" , label , title);				
	}
	
	,setupHistoryWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var remarksField = this._get_(theForm)._get_("remarks");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						var ctx = this;
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(true);
						
						remarksField.fieldLabel = label;
						remarksField.setValue("");
						window.title = title;
		
						window.show();
		
						window.on("close", function(field) {
						   	var dc = ctx._getDc_("workflowTask");
							dc.doCancel();
						});
		
						remarksField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
								cancelButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
								cancelButton.setDisabled(true);
						    }
						});
	}
	
	,canClaimTask: function(dc) {
		 
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							var user = getApplication().session.user;
							if(record && user && user.code === record.data.candidate ){
								return true;			
							}
						}
						return false;
	}
	
	,canCompleteTask: function(dc) {
		 
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							var user = getApplication().session.user;
							if(record && user && user.code === record.data.assignee ){
								return true;			
							}
						}
						return false;
	}
});
