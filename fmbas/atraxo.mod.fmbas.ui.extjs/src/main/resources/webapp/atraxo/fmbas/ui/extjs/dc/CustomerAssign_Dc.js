/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.CustomerAssign_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.Customers_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.Customers_Ds
});

/* ================= GRID: AssignList ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.CustomerAssign_Dc$AssignList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_CustomerAssign_Dc$AssignList",
	_noPaginator_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:50,  flex:1})
		.addTextColumn({ name:"name", dataIndex:"name", width:120,  flex:1})
		.addTextColumn({ name:"type", dataIndex:"type", width:70,  flex:1})
		.addTextColumn({ name:"status", dataIndex:"status", width:70,  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
