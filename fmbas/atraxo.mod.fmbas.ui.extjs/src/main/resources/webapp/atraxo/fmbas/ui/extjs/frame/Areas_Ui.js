/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.Areas_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Areas_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("area", Ext.create(atraxo.fmbas.ui.extjs.dc.Areas_Dc,{multiEdit: true, trackEditMode: true}))
		.addDc("locations", Ext.create(atraxo.fmbas.ui.extjs.dc.Locations_Dc,{}))
		.linkDc("locations", "area",{fetchMode:"auto",fields:[
					{childParam:"areaId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"assignLocations", disabled:false, handler: this.onAssignLocations, scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdw1",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw1, scope:this})
		.addButton({name:"btnCancelWithMenu",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"area", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", handler: this.onBtnCancelSelected, scope:this})
		.addButton({name:"btnCancelAll",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", _isDefaultHandler_:true, handler: this.onBtnCancelAll, scope:this})
		.addDcFilterFormView("area", {name:"aFilter", xtype:"fmbas_Areas_Dc$Filter"})
		.addDcEditGridView("area", {name:"aList", xtype:"fmbas_Areas_Dc$List", frame:true})
		.addDcFormView("area", {name:"aEdit", xtype:"fmbas_Areas_Dc$Edit"})
		.addDcGridView("locations", {name:"locList", height:400, xtype:"fmbas_Locations_Dc$AreasLocation"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["aList"], ["center"])
		.addChildrenTo("canvas2", ["aEdit", "locList"], ["north", "center"])
		.addToolbarTo("aList", "tlbCmpList")
		.addToolbarTo("aEdit", "tlbCmpEdit");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbCmpList", {dc: "area"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelWithMenu"),this._elems_.get("btnCancelSelected"),this._elems_.get("btnCancelAll"),this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbCmpEdit", {dc: "area"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("assignLocations"),this._elems_.get("helpWdw1")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button assignLocations
	 */
	,onAssignLocations: function() {
		this._showAsgnWindow_("atraxo.fmbas.ui.extjs.asgn.AreasLocations_Asgn$Ui" ,{dc: "area", objectIdField: "id"
		,listeners: {close: {scope: this, fn: function() { this._getDc_("locations").doReloadPage();
		}} }});
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdw1
	 */
	,onHelpWdw1: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnCancelSelected
	 */
	,onBtnCancelSelected: function() {
		this.fnCancelSelected();
	}
	
	/**
	 * On-Click handler for button btnCancelAll
	 */
	,onBtnCancelAll: function() {
		this.fnCancelAll();
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/Areas.html";
					window.open( url, "SONE_Help");
	}
	
	,fnCancelSelected: function() {
		
						var dc = this._getDc_("area");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAll: function() {
		
						var dc = this._getDc_("area");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isCancelEnabled(dc);
	}
});
