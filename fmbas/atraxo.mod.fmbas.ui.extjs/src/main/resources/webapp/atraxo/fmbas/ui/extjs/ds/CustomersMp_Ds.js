/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.CustomersMp_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_CustomersMp_Ds"
	},
	
	
	validators: {
		code: [{type: 'presence'}],
		name: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("business", "Supplier");
		this.set("isSubsidiary", true);
		this.set("status", "Prospect");
	},
	
	fields: [
		{name:"iataCode", type:"string"},
		{name:"icaoCode", type:"string"},
		{name:"name", type:"string"},
		{name:"code", type:"string"},
		{name:"status", type:"string"},
		{name:"type", type:"string"},
		{name:"phoneNo", type:"string"},
		{name:"dateOfInc", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"isCustomer", type:"boolean"},
		{name:"isSupplier", type:"boolean"},
		{name:"isSubsidiary", type:"boolean"},
		{name:"fuelSupplier", type:"boolean"},
		{name:"accountNumber", type:"string"},
		{name:"faxNo", type:"string"},
		{name:"website", type:"string"},
		{name:"officeStreet", type:"string"},
		{name:"officeCity", type:"string"},
		{name:"officePostalCode", type:"string"},
		{name:"billingStreet", type:"string"},
		{name:"billingCity", type:"string"},
		{name:"billingPostalCode", type:"string"},
		{name:"useForBilling", type:"boolean"},
		{name:"business", type:"string", noSort:true},
		{name:"buyerId", type:"string"},
		{name:"buyerName", type:"string"},
		{name:"buyerCode", type:"string"},
		{name:"officeCountryId", type:"int", allowNull:true},
		{name:"officeCountryCode", type:"string"},
		{name:"officeCountryName", type:"string"},
		{name:"officeStateId", type:"int", allowNull:true},
		{name:"officeStateCode", type:"string"},
		{name:"officeStateName", type:"string"},
		{name:"billingCountryId", type:"int", allowNull:true},
		{name:"billingCountryCode", type:"string"},
		{name:"billingCountryName", type:"string"},
		{name:"billingStateId", type:"int", allowNull:true},
		{name:"billingStateCode", type:"string"},
		{name:"billingStateName", type:"string"},
		{name:"contactName", type:"string", noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"dummyField", type:"string", noFilter:true, noSort:true},
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.CustomersMp_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"iataCode", type:"string"},
		{name:"icaoCode", type:"string"},
		{name:"name", type:"string"},
		{name:"code", type:"string"},
		{name:"status", type:"string"},
		{name:"type", type:"string"},
		{name:"phoneNo", type:"string"},
		{name:"dateOfInc", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"isCustomer", type:"boolean", allowNull:true},
		{name:"isSupplier", type:"boolean", allowNull:true},
		{name:"isSubsidiary", type:"boolean", allowNull:true},
		{name:"fuelSupplier", type:"boolean", allowNull:true},
		{name:"accountNumber", type:"string"},
		{name:"faxNo", type:"string"},
		{name:"website", type:"string"},
		{name:"officeStreet", type:"string"},
		{name:"officeCity", type:"string"},
		{name:"officePostalCode", type:"string"},
		{name:"billingStreet", type:"string"},
		{name:"billingCity", type:"string"},
		{name:"billingPostalCode", type:"string"},
		{name:"useForBilling", type:"boolean", allowNull:true},
		{name:"business", type:"string", noSort:true},
		{name:"buyerId", type:"string"},
		{name:"buyerName", type:"string"},
		{name:"buyerCode", type:"string"},
		{name:"officeCountryId", type:"int", allowNull:true},
		{name:"officeCountryCode", type:"string"},
		{name:"officeCountryName", type:"string"},
		{name:"officeStateId", type:"int", allowNull:true},
		{name:"officeStateCode", type:"string"},
		{name:"officeStateName", type:"string"},
		{name:"billingCountryId", type:"int", allowNull:true},
		{name:"billingCountryCode", type:"string"},
		{name:"billingCountryName", type:"string"},
		{name:"billingStateId", type:"int", allowNull:true},
		{name:"billingStateCode", type:"string"},
		{name:"billingStateName", type:"string"},
		{name:"contactName", type:"string", noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"dummyField", type:"string", noFilter:true, noSort:true},
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.fmbas.ui.extjs.ds.CustomersMp_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"contactEmail", type:"string"},
		{name:"contactFirstName", type:"string"},
		{name:"contactLastName", type:"string"},
		{name:"contactMobilePhone", type:"string"},
		{name:"customerLocationId", type:"int", forFilter:true, allowNull:true},
		{name:"remark", type:"string"}
	]
});
