/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.ExternalInterfacesHistory_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.ExternalInterfacesHistory_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.ExternalInterfacesHistory_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_ExternalInterfacesHistory_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateTimeField({name:"requestReceived", dataIndex:"requestReceived"})
			.addDateTimeField({name:"responseSent", dataIndex:"responseSent"})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __FMBAS_TYPE__.ExternalInterfacesHistoryStatus._SUCCESS_, __FMBAS_TYPE__.ExternalInterfacesHistoryStatus._FAILURE_, __FMBAS_TYPE__.ExternalInterfacesHistoryStatus._FAILED_TIMEOUT_, __FMBAS_TYPE__.ExternalInterfacesHistoryStatus._FAILED_COMMUNICATION_, __FMBAS_TYPE__.ExternalInterfacesHistoryStatus._FAILED_NEGATIVE_ACKNOWLEDGEMENT_]})
			.addTextField({ name:"requester", dataIndex:"requester", maxLength:1000})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ExternalInterfacesHistory_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_ExternalInterfacesHistory_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"requestReceived", dataIndex:"requestReceived", _mask_: Masks.DATETIME,  flex:1})
		.addDateColumn({ name:"responseSent", dataIndex:"responseSent", _mask_: Masks.DATETIME,  flex:1})
		.addTextColumn({ name:"status", dataIndex:"status", width:70,  flex:1})
		.addTextColumn({ name:"requester", dataIndex:"requester", hidden:true, width:200,  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Error ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ExternalInterfacesHistory_Dc$Error", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_ExternalInterfacesHistory_Dc$Error",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"msg", bind:"{d.result}", dataIndex:"result", noEdit:true , width:700, noLabel: true, height:400})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col"])
		.addChildrenTo("col", ["msg"]);
	}
});
