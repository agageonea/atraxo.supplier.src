/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.FinalCustomer_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.FinalCustomer_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.FinalCustomer_Ds
});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.FinalCustomer_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_FinalCustomer_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:100})
		.addTextColumn({ name:"name", dataIndex:"name", width:200})
		.addTextColumn({ name:"iata", dataIndex:"iataCode", width:100})
		.addTextColumn({ name:"icao", dataIndex:"icaoCode", width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.FinalCustomer_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_FinalCustomer_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", allowBlank:false, noLabel: true, maxLength:32})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, noLabel: true, maxLength:100})
		.addTextField({ name:"iata", bind:"{d.iataCode}", dataIndex:"iataCode", noLabel: true, enforceMaxLength:2, maxLength:2})
		.addTextField({ name:"icao", bind:"{d.icaoCode}", dataIndex:"icaoCode", noLabel: true, enforceMaxLength:3, maxLength:3})
		.addDisplayFieldText({ name:"codeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"nameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"iataCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"icaoCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"maineTable", layout: {type:"table"}})
		.addPanel({ name:"table1",  style:"margin-left: 20px", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["maineTable"])
		.addChildrenTo("maineTable", ["table1"])
		.addChildrenTo("table1", ["codeLabel", "code", "nameLabel", "name", "iataCodeLabel", "iata", "icaoCodeLabel", "icao"]);
	}
});
