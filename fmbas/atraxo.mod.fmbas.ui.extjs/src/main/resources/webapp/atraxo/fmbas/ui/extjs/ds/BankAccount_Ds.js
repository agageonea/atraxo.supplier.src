/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.BankAccount_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_BankAccount_Ds"
	},
	
	
	validators: {
		accountNumber: [{type: 'presence'}],
		currencyCode: [{type: 'presence'}],
		subsidiaryCode: [{type: 'presence'}]
	},
	
	fields: [
		{name:"accountNumber", type:"string"},
		{name:"accountHolder", type:"string"},
		{name:"bankName", type:"string"},
		{name:"bankAdress", type:"string"},
		{name:"branchCode", type:"string"},
		{name:"branchName", type:"string"},
		{name:"ibanCode", type:"string"},
		{name:"swiftCode", type:"string"},
		{name:"customerCodes", type:"string", noFilter:true, noSort:true},
		{name:"currencyCode", type:"string"},
		{name:"subsidiaryCode", type:"string"},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"subsidiaryId", type:"int", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.BankAccount_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"accountNumber", type:"string"},
		{name:"accountHolder", type:"string"},
		{name:"bankName", type:"string"},
		{name:"bankAdress", type:"string"},
		{name:"branchCode", type:"string"},
		{name:"branchName", type:"string"},
		{name:"ibanCode", type:"string"},
		{name:"swiftCode", type:"string"},
		{name:"customerCodes", type:"string", noFilter:true, noSort:true},
		{name:"currencyCode", type:"string"},
		{name:"subsidiaryCode", type:"string"},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"subsidiaryId", type:"int", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
