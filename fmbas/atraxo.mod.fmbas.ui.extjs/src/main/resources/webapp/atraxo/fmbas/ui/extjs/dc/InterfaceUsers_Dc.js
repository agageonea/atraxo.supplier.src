/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.InterfaceUsers_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.InterfaceUsers_Ds
});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.InterfaceUsers_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_InterfaceUsers_Dc$List",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"userName", dataIndex:"userName", width:200, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_UserNameLov_Lov", allowBlank:false, maxLength:255,
				retFieldMapping: [{lovField:"id", dsField: "userId"} ,{lovField:"email", dsField: "userEmail"} ,{lovField:"loginName", dsField: "loginName"} ]},  flex:1})
		.addTextColumn({name:"userEmail", dataIndex:"userEmail", width:100, noEdit: true, maxLength:128,  flex:1})
		.addComboColumn({name:"notificationType", dataIndex:"notificationType", width:70, allowBlank: false, 
			editor:{xtype:"combo", mode: 'local', allowBlank:false, triggerAction:'all', forceSelection:true, store:[ __FMBAS_TYPE__.NotificationType._EMAIL_, __FMBAS_TYPE__.NotificationType._EVENT_, __FMBAS_TYPE__.NotificationType._BOTH_]},  flex:1})
		.addComboColumn({name:"notificationCondition", dataIndex:"notificationCondition", width:70, allowBlank: false, 
			editor:{xtype:"combo", mode: 'local', allowBlank:false, triggerAction:'all', forceSelection:true, store:[ __FMBAS_TYPE__.InterfaceNotificationCondition._ON_FAILURE_, __FMBAS_TYPE__.InterfaceNotificationCondition._ON_SUCCESS_, __FMBAS_TYPE__.InterfaceNotificationCondition._ALL_EVENTS_]},  flex:1})
		.addBooleanColumn({name:"enabled", dataIndex:"enabled", width:120})
		.addTextColumn({name:"emailSubject", dataIndex:"emailSubject", width:50, maxLength:100,  flex:1, 
			editor: { xtype:"textfield", maxLength:100}})
		.addLov({name:"emailTemplate", dataIndex:"emailTemplateName", width:120, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_Templates_Lov", maxLength:100,
				retFieldMapping: [{lovField:"id", dsField: "emailTemplateId"} ,{lovField:"name", dsField: "emailTemplateName"} ],
				filterFieldMapping: [{lovField:"typeName", value: "e-mail body"} ]},  flex:1})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
