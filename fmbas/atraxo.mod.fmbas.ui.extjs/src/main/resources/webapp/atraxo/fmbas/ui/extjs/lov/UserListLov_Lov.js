/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.UserListLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_UserListLov_Lov",
	displayField: "userCode", 
	_columns_: ["id", "userCode", "userName"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{userCode}, {userName}, {active}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.fmbas.ui.extjs.ds.UsersLov_Ds
});
