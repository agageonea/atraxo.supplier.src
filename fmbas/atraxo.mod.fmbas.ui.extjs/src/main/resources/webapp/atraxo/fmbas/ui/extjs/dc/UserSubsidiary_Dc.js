/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.UserSubsidiary_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.UserSubsidiary_Ds
});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.UserSubsidiary_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_UserSubsidiary_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:100})
		.addTextColumn({ name:"name", dataIndex:"name", width:100})
		.addTextColumn({ name:"city", dataIndex:"officeStateName", width:100})
		.addTextColumn({ name:"country", dataIndex:"officeCountryName", width:100})
		.addBooleanColumn({ name:"default", dataIndex:"mainSubsidiary", width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
