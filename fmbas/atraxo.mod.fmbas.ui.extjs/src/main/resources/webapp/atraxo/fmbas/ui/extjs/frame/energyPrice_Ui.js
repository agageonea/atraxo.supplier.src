/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.energyPrice_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.energyPrice_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("energyPrice", Ext.create(atraxo.fmbas.ui.extjs.dc.Energy_Dc,{}))
		.addDc("timeSerieComp", Ext.create(atraxo.fmbas.ui.extjs.dc.CompositeTimeSerie_Dc,{multiEdit: true}))
		.addDc("timeSerieAverage", Ext.create(atraxo.fmbas.ui.extjs.dc.TimeSerieAverage_Dc,{multiEdit: true}))
		.addDc("timeSerieItem", Ext.create(atraxo.fmbas.ui.extjs.dc.TimeSerieItem_Dc,{multiEdit: true}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.linkDc("timeSerieComp", "energyPrice",{fetchMode:"auto",reloadChildrenOnParentUpdate:true,fields:[
					{childField:"timeSerieCompId", parentField:"id"}]})
				.linkDc("timeSerieAverage", "energyPrice",{fetchMode:"auto",reloadChildrenOnParentUpdate:true,fields:[
					{childField:"tserieId", parentField:"id"}]})
				.linkDc("timeSerieItem", "energyPrice",{fetchMode:"auto",reloadChildrenOnParentUpdate:true,fields:[
					{childField:"tserie", parentField:"id"}]})
				.linkDc("note", "energyPrice",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("history", "energyPrice",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnOnEditWdwClose",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnOnEditWdwClose, scope:this})
		.addButton({name:"btnSaveCloseWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseWdw, scope:this})
		.addButton({name:"btnSaveNewWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewWdw, scope:this})
		.addButton({name:"showenergyPrice",glyph:fp_asc.chart_glyph.glyph,iconCls: fp_asc.chart_glyph.css, disabled:true, handler: this.onShowenergyPrice,stateManager:[{ name:"selected_one", dc:"energyPrice"}], scope:this})
		.addButton({name:"btnDeleteSerieItem",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteSerieItem,stateManager:[{ name:"selected_one", dc:"timeSerieItem", and: function(dc) {return (this.canDeleteTimeSerieItem(dc));} }], scope:this})
		.addButton({name:"btnPublishSerie",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnPublishSerie,stateManager:[{ name:"selected_one_clean", dc:"timeSerieItem", and: function() {return (this._enablePublishBtn_());} }], scope:this})
		.addButton({name:"btnPublishSeries",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnPublishSeries,stateManager:[{ name:"selected_one_clean", dc:"energyPrice", and: function() {return (this._enablePublishBtn_());} }], scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdw1",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw1, scope:this})
		.addButton({name:"helpWdw2",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw2, scope:this})
		.addButton({name:"btnCancelWithMenu",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"timeSerieItem", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", handler: this.onBtnCancelSelected, scope:this})
		.addButton({name:"btnCancelAll",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", _isDefaultHandler_:true, handler: this.onBtnCancelAll, scope:this})
		.addButton({name:"btnCalculate",glyph:fp_asc.commit_glyph.glyph,iconCls: fp_asc.commit_glyph.css, disabled:true, handler: this.onBtnCalculate,stateManager:[{ name:"selected_not_zero", dc:"timeSerieComp", and: function() {return (this._compLessThanTwo_());} }], scope:this})
		.addButton({name:"btnSubmitForApprovalList",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitForApprovalList,stateManager:[{ name:"selected_one_clean", dc:"energyPrice", and: function(dc) {return (this.canSubmitForApproval(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnSubmitForApprovalEdit",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitForApprovalEdit,stateManager:[{ name:"selected_one_clean", dc:"energyPrice", and: function(dc) {return (this.canSubmitForApproval(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnSubmitForApprovalExchangeRates",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitForApprovalExchangeRates,stateManager:[{ name:"selected_one_clean", dc:"energyPrice", and: function(dc) {return (this.canSubmitForApproval(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnSaveCloseApprovalWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApprovalWdw, scope:this})
		.addButton({name:"btnCancelApprovalWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApprovalWdw, scope:this})
		.addButton({name:"btnApproveList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApproveList,stateManager:[{ name:"selected_not_zero", dc:"energyPrice", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysenergypriceapproval === 'true' })
		.addButton({name:"btnApproveEdit",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApproveEdit,stateManager:[{ name:"selected_one_clean", dc:"energyPrice", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysenergypriceapproval === 'true' })
		.addButton({name:"btnRejectList",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnRejectList,stateManager:[{ name:"selected_not_zero", dc:"energyPrice", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysenergypriceapproval === 'true' })
		.addButton({name:"btnRejectEdit",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnRejectEdit,stateManager:[{ name:"selected_one_clean", dc:"energyPrice", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: _SYSTEMPARAMETERS_.sysenergypriceapproval === 'true' })
		.addButton({name:"btnSaveCloseApproveWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApproveWdw, scope:this})
		.addButton({name:"btnSaveCloseRejectWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRejectWdw, scope:this})
		.addButton({name:"btnCancelApproveWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApproveWdw, scope:this})
		.addButton({name:"btnCancelRejectWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRejectWdw, scope:this})
		.addButton({name:"btnSaveTimeSeriesComponents",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveTimeSeriesComponents, scope:this})
		.addButton({name:"btnNewInlineTimeSeriesCommponents",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewInlineTimeSeriesCommponents, scope:this})
		.addButton({name:"btnCancelTimeSeriesComponents",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnCancelTimeSeriesComponents,stateManager:[{ name:"record_is_dirty", dc:"timeSerieComp"}], scope:this})
		.addButton({name:"btnDeleteTimeSeriesComponents",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onBtnDeleteTimeSeriesComponents, scope:this})
		.addDcFilterFormView("energyPrice", {name:"energyPriceFilter", xtype:"fmbas_Energy_Dc$Filter"})
		.addDcGridView("energyPrice", {name:"energyPriceList", xtype:"fmbas_Energy_Dc$List"})
		.addDcFormView("energyPrice", {name:"energyPriceEdit", xtype:"fmbas_Energy_Dc$Edit"})
		.addDcEditGridView("timeSerieItem", {name:"timeSerieItemList", xtype:"fmbas_TimeSerieItem_Dc$ListContext", frame:true})
		.addDcEditGridView("timeSerieAverage", {name:"timeSerieAverageList", _hasTitle_:true, xtype:"fmbas_TimeSerieAverage_Dc$List", frame:true})
		.addDcFormView("energyPrice", {name:"energyPriceDetails", xtype:"fmbas_Energy_Dc$EditDetails"})
		.addDcFormView("energyPrice", {name:"energyPriceQuotationsDetails", xtype:"fmbas_Energy_Dc$EditQuotationContext"})
		.addDcEditGridView("timeSerieComp", {name:"composite", _hasTitle_:true, xtype:"fmbas_CompositeTimeSerie_Dc$GridEnergyPrice", frame:true,  listeners:{beforeedit: {scope: this, fn: function(editor, ctx) {this._filterTsNameLov_(editor, ctx)}}}})
		.addDcGridView("note", {name:"NotesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcGridView("history", {name:"History", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("energyPrice", {name:"approvalNote", xtype:"fmbas_Energy_Dc$ApprovalNote"})
		.addDcFormView("energyPrice", {name:"approveNote", xtype:"fmbas_Energy_Dc$ApprovalNote"})
		.addDcFormView("energyPrice", {name:"rejectNote", xtype:"fmbas_Energy_Dc$ApprovalNote"})
		.addWindow({name:"wdwenergyPrice", _hasTitle_:true, width:680, height:330, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("energyPriceEdit")],  listeners:{ close:{fn:this.onWdwenergyPriceClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewWdw"), this._elems_.get("btnSaveCloseWdw"), this._elems_.get("btnOnEditWdwClose")]}]})
		.addWindow({name:"wdwApprovalNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("approvalNote")],  closable:true, listeners:{ show:{fn:this.onApprovalWdwShow, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseApprovalWdw"), this._elems_.get("btnCancelApprovalWdw")]}]})
		.addWindow({name:"wdwApproveNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("approveNote")],  closable:true, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseApproveWdw"), this._elems_.get("btnCancelApproveWdw")]}]})
		.addWindow({name:"wdwRejectNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("rejectNote")],  closable:true, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRejectWdw"), this._elems_.get("btnCancelRejectWdw")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas2", dcName:"energyPrice"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["energyPriceList"], ["center"])
		.addChildrenTo("canvas2", ["energyPriceDetails", "detailsTab"], ["north", "center"])
		.addChildrenTo("canvas3", ["energyPriceQuotationsDetails", "timeSerieItemList"], ["north", "center"])
		.addChildrenTo("detailsTab", ["timeSerieAverageList"])
		.addToolbarTo("energyPriceList", "tlbEnergyPriceList")
		.addToolbarTo("energyPriceDetails", "tlbEnergyPriceEdit")
		.addToolbarTo("timeSerieAverageList", "tlbtimeSerieAverage")
		.addToolbarTo("timeSerieItemList", "tlbtimeSerieItemList")
		.addToolbarTo("energyPriceQuotationsDetails", "tlbenergyPriceQuotations")
		.addToolbarTo("composite", "tlbComponents");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2", "canvas3"])
		.addChildrenTo2("detailsTab", ["composite", "History"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbEnergyPriceList", {dc: "energyPrice"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("showenergyPrice"),this._elems_.get("btnSubmitForApprovalList"),this._elems_.get("btnApproveList"),this._elems_.get("btnRejectList"),this._elems_.get("btnPublishSeries"),this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbEnergyPriceEdit", {dc: "energyPrice"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnSubmitForApprovalEdit"),this._elems_.get("helpWdw1")])
			.addReports()
		.end()
		.beginToolbar("tlbtimeSerieAverage", {dc: "timeSerieAverage"})
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbtimeSerieItemList", {dc: "timeSerieItem"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelWithMenu"),this._elems_.get("btnCancelSelected"),this._elems_.get("btnCancelAll"),this._elems_.get("btnDeleteSerieItem"),this._elems_.get("btnSubmitForApprovalExchangeRates"),this._elems_.get("btnApproveEdit"),this._elems_.get("btnRejectEdit"),this._elems_.get("btnPublishSerie")])
			.addReports()
		.end()
		.beginToolbar("tlbenergyPriceQuotations", {dc: "energyPrice"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw2")])
			.addReports()
		.end()
		.beginToolbar("tlbComponents", {dc: "timeSerieComp"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewInlineTimeSeriesCommponents"),this._elems_.get("btnSaveTimeSeriesComponents"),this._elems_.get("btnCancelTimeSeriesComponents"),this._elems_.get("btnDeleteTimeSeriesComponents"),this._elems_.get("btnCalculate")])
			.addReports()
		.end();
	}
	
	,_applyCustomStateManagers_: function(){
		this._getBuilder_().beginStateFnDef('tlbEnergyPriceList', {dc: 'energyPrice'})
		.setStateFn_Delete(this.canDeleteEnergyRate)
		.endStateFnSet();
		this._getBuilder_().beginStateFnDef('tlbtimeSerieItemList', {dc: 'timeSerieItem'})
		.setStateFn_New(this.canDoNewTimeSerieItem)
		.endStateFnSet();
	}
	

	
	/**
	 * On-Click handler for button btnOnEditWdwClose
	 */
	,onBtnOnEditWdwClose: function() {
		this.onWdwenergyPriceClose();
		this._getWindow_("wdwenergyPrice").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseWdw
	 */
	,onBtnSaveCloseWdw: function() {
		this.saveClose();
	}
	
	/**
	 * On-Click handler for button btnSaveNewWdw
	 */
	,onBtnSaveNewWdw: function() {
		this.saveNew();
	}
	
	/**
	 * On-Click handler for button showenergyPrice
	 */
	,onShowenergyPrice: function() {
		this.openTimeSerieItem()
	}
	
	/**
	 * On-Click handler for button btnDeleteSerieItem
	 */
	,onBtnDeleteSerieItem: function() {
		this.deleteSerieItem();
	}
	
	/**
	 * On-Click handler for button btnPublishSerie
	 */
	,onBtnPublishSerie: function() {
		var successFn = function() {
			this._getDc_("energyPrice").doReloadPage();
		};
		var o={
			name:"publish",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("energyPrice").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnPublishSeries
	 */
	,onBtnPublishSeries: function() {
		var successFn = function() {
			this._getDc_("energyPrice").doReloadPage();
		};
		var o={
			name:"publishElements",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("energyPrice").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdw1
	 */
	,onHelpWdw1: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdw2
	 */
	,onHelpWdw2: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnCancelSelected
	 */
	,onBtnCancelSelected: function() {
		this.fnCancelSelected();
	}
	
	/**
	 * On-Click handler for button btnCancelAll
	 */
	,onBtnCancelAll: function() {
		this.fnCancelAll();
	}
	
	/**
	 * On-Click handler for button btnCalculate
	 */
	,onBtnCalculate: function() {
		this._calculateEnergyPrice_();
	}
	
	/**
	 * On-Click handler for button btnSubmitForApprovalList
	 */
	,onBtnSubmitForApprovalList: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnSubmitForApprovalEdit
	 */
	,onBtnSubmitForApprovalEdit: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnSubmitForApprovalExchangeRates
	 */
	,onBtnSubmitForApprovalExchangeRates: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApprovalWdw
	 */
	,onBtnSaveCloseApprovalWdw: function() {
		var successFn = function() {
			this._getWindow_("wdwApprovalNote").close();
			this._getDc_("energyPrice").doEditOut();
			this._getDc_("energyPrice").doReloadPage();
		};
		var o={
			name:"submitForApproval",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("energyPrice").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCancelApprovalWdw
	 */
	,onBtnCancelApprovalWdw: function() {
		this._getWindow_("wdwApprovalNote").close();
		this._getDc_("energyPrice").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnApproveList
	 */
	,onBtnApproveList: function() {
		this.openApproveWindow();
	}
	
	/**
	 * On-Click handler for button btnApproveEdit
	 */
	,onBtnApproveEdit: function() {
		this.openApproveWindow();
	}
	
	/**
	 * On-Click handler for button btnRejectList
	 */
	,onBtnRejectList: function() {
		this.openRejectWindow();
	}
	
	/**
	 * On-Click handler for button btnRejectEdit
	 */
	,onBtnRejectEdit: function() {
		this.openRejectWindow();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApproveWdw
	 */
	,onBtnSaveCloseApproveWdw: function() {
		var successFn = function(dc,response) {
			this._getWindow_("wdwApproveNote").close();
			this.initFeedback(response)
			this._getDc_("energyPrice").doReloadPage();
		};
		var o={
			name:"approveList",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("energyPrice").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRejectWdw
	 */
	,onBtnSaveCloseRejectWdw: function() {
		var successFn = function(dc,response) {
			this._getWindow_("wdwRejectNote").close();
			this.initFeedback(response)
			this._getDc_("energyPrice").doReloadPage();
		};
		var o={
			name:"rejectList",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("energyPrice").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnCancelApproveWdw
	 */
	,onBtnCancelApproveWdw: function() {
		this._getWindow_("wdwApproveNote").close();
		this._getDc_("energyPrice").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCancelRejectWdw
	 */
	,onBtnCancelRejectWdw: function() {
		this._getWindow_("wdwRejectNote").close();
		this._getDc_("energyPrice").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSaveTimeSeriesComponents
	 */
	,onBtnSaveTimeSeriesComponents: function() {
		this.SaveTimeSeriesComponents();
	}
	
	/**
	 * On-Click handler for button btnNewInlineTimeSeriesCommponents
	 */
	,onBtnNewInlineTimeSeriesCommponents: function() {
		this._getDc_("timeSerieComp").doNew();
	}
	
	/**
	 * On-Click handler for button btnCancelTimeSeriesComponents
	 */
	,onBtnCancelTimeSeriesComponents: function() {
		this._getDc_("timeSerieComp").doCancel();
	}
	
	/**
	 * On-Click handler for button btnDeleteTimeSeriesComponents
	 */
	,onBtnDeleteTimeSeriesComponents: function() {
		this._getDc_("timeSerieComp").doDelete();
	}
	
	,showWindow: function() {	
		this._getWindow_("wdwenergyPrice").show();
	}
	
	,_when_called_from_dshboard_: function(id) {
		
						var dc = this._getDc_("energyPrice");
						dc.doCancel();
						dc.doClearAllFilters();
						dc.setFilterValue("id", id);				
						dc.doQuery({showEdit: true}); 
	}
	
	,canApproveReject: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var r = selectedRecords[i];
							var canBeApprovedRejected = r.data.approvalStatus === __FMBAS_TYPE__.TimeSeriesApprovalStatus._AWAITING_APPROVAL_ && r.data.canBeCompleted === true;
							if(canBeApprovedRejected === false) {
								return false;
							}
						}
						return true;
	}
	
	,openApproveWindow: function() {
		
						var label = "Please provide an approval reason.";
						var title = "Approve";
						this.setupPopUpWindow("wdwApproveNote","approveNote","btnSaveCloseApproveWdw","btnCancelApproveWdw" , label , title);				
	}
	
	,openRejectWindow: function() {
		
						var label = "Please provide a reason for rejection.";
						var title = "Reject";
						this.setupPopUpWindow("wdwRejectNote","rejectNote","btnSaveCloseRejectWdw","btnCancelRejectWdw" , label , title);				
	}
	
	,setupPopUpWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var remarksField = this._get_(theForm)._get_("remarks");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						
						saveButton.setDisabled(true);
						
						remarksField.fieldLabel = label;
		
						remarksField.setValue("");
						window.title = title;
		
						remarksField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
								cancelButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
						    }
						});
						window.show();
	}
	
	,_calculateEnergyPrice_: function() {
		
		
						var dc = this._getDc_("timeSerieComp");
						var energyPrice = this._getDc_("energyPrice");
						var store = dc.store;
						var weightSum = store.sum("weighting");
		
						var rpcFn = function() {
							var o={
								name:"calculate",
								modal:true
							};
							energyPrice.doRpcData(o);
						}
		
						if (weightSum > 100 || weightSum < 100) {
		
							Ext.Msg.show({
							    title: "Warning",
							    msg: "The sum of the weightings is different than 100.<br>Do you want to continue?",
							    buttons: Ext.MessageBox.YES + Ext.MessageBox.NO,
							    fn: function(btnId) {
							        if (btnId === "yes") {
							            rpcFn();
							        }
							    },
								buttonText: {
					                yes: "Continue", no: "Cancel"
					            },
							    scope: this
							});
						}
						else {
							rpcFn();
						}
		
						
	}
	
	,_compLessThanTwo_: function() {
		
						var dc = this._getDc_("energyPrice");
						var r = dc.getRecord();
						var result = false;
						if (r) {
							var componentsCount = r.get("componentsCount");
							if (componentsCount > 1) {
								result = true;
							}
						}
						return result;
	}
	
	,_filterTsNameLov_: function(editor,context) {
		
		
						var fieldName = context.field;
						var energyPrice = this._getDc_("energyPrice");
						var energyPriceRecord = energyPrice.getRecord();
						var store = "";
						if (fieldName === "timeSerieName") {
							store = context.column.getEditor().store;
						}
						if (!Ext.isEmpty(store) && energyPriceRecord) {
							context.column.getEditor().pageSize = 0;
							store.setPageSize(0);
							store.filterBy(function (record) {
								var serieType = "E";
								var curr = energyPriceRecord.get("currency1Code");
								var serieName = energyPriceRecord.get("serieName");
			                	if (record.get("serieType") === serieType && record.get("currCode") === curr && record.get("serieName") !== serieName && record.get("dataProvider") !== __FMBAS_TYPE__.DataProvider._CALCULATED_) return record;
			                });
						}
	}
	
	,_enablePublishBtn_: function() {
		
						var dc = this._getDc_("energyPrice");
						var r = dc.getRecord();
						var result = false;
						if (r) {
							var status = r.get("status");
							var hasTs = r.get("hasTimeSerieItems");
							var approvalStatus = r.get("approvalStatus");
							var approvalResult = (_SYSTEMPARAMETERS_.sysenergypriceapproval === "false" || (_SYSTEMPARAMETERS_.sysenergypriceapproval === "true"  && approvalStatus === __FMBAS_TYPE__.TimeSeriesApprovalStatus._APPROVED_))
							if (status === "work" && hasTs == true && approvalResult) {
								result = true;
							}
						}
						return result;
	}
	
	,fnCancelSelected: function() {
		
						var dc = this._getDc_("timeSerieItem");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAll: function() {
		
						var dc = this._getDc_("timeSerieItem");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isCancelEnabled(dc);
	}
	
	,SaveTimeSeriesComponents: function() {
		
						var dc = this._getDc_("timeSerieComp");
						var components = dc.store.data;
						var weightingSum = 0;
						for(var i=0 ; i<components.length ; i++) {
							
							weightingSum += components.items[i].get("weighting");
						}
						if(weightingSum <= 100){
							dc.doSave();
						} else {
								Ext.Msg.show({
								title : "Warning" ,
						        msg: Main.translate("applicationMsg","excededWeightingTimeSeriesComponennts_lbl"),
						        buttons: Ext.MessageBox.OK,
								scope: this,
						    }, this);
						}
	}
	
	,_setComponentsToolbarState_: function() {
		
						var view = this._get_("composite");
						if(view){
							var items = view.dockedItems.items;
							var toolbar = "";
							for (var i = 0; i < items.length; i++) {
								if (items[i].dock === "top" && items[i].xtype === "toolbar") {
									toolbar = items[i];
								}
							}
							if (!Ext.isEmpty(toolbar)) {
								var record = this._getDc_("energyPrice").getRecord();
								if (record) {
									var provider = record.get("dataProvider");
									if (provider === __FMBAS_TYPE__.DataProvider._CALCULATED_) {
										toolbar.setDisabled(false);
									}
									else {
										toolbar.setDisabled(true);
									}
								}
								else {
									toolbar.setDisabled(true);
								}
							}
						}
	}
	
	,canDoNewTimeSerieItem: function(dc) {
		
						var result = false;
						var record = dc._frame_._getDc_("energyPrice").getRecord();
						if (record) {
							var provider = record.get("dataProvider");
							result = (provider !== __FMBAS_TYPE__.DataProvider._CALCULATED_);
						}
						return result;
	}
	
	,_setCalaculateBtnState_: function() {
		
		
						var energy = this._getDc_("energyPrice");
						var dc = this._getDc_("timeSerieComp");
		
						var s = dc.store;
						var r = energy.getRecord();
						var energyStore = energy.store;
						var btn = this._get_("btnCalculate");
		
						s.load(function() {
							var count = s.getTotalCount();
							
							if (r) {
								r.set("componentsCount",count);
								energyStore.commitChanges();
							}
							if (count >= 2) {
								btn.setDisabled(false);
							}
							else {
								btn.setDisabled(true);
							}
						}, this);
					
	}
	
	,_afterLinkElementsPhase2_: function() {
		
						this._setComponentsToolbarState_();
	}
	
	,_afterDefineDcs_: function() {
		
		
					var energy = this._getDc_("energyPrice");
					var serieItem = this._getDc_("timeSerieItem");
					var timeSerieComp = this._getDc_("timeSerieComp");
					var avg = this._getDc_("timeSerieAverage");
		
					serieItem.on("afterDoDeleteSuccess", function() {
						energy.doReloadPage();
					}, this);
		
					energy.on("afterDoQuerySuccess", function(dc, ajaxResult) { 
						if (ajaxResult.options.showEdit == true) {
							this._showStackedViewElement_("main", "canvas3");
						}
					}, this);
		
					energy.on("recordReload", function() { 
						this._applyStateAllButtons_();
					}, this);
		
					serieItem.on("afterDoSaveSuccess", function() {
						energy.doReloadRecord();
					}, this);
		
					avg.on("afterDoSaveSuccess", function() {
						energy.doReloadRecord();
					}, this);
		
					serieItem.on("afterDoDeleteFailure", function() {
						serieItem.doCancel();
					}, this);
		
					timeSerieComp.on("afterDoSaveSuccess", function() {
						this._setCalaculateBtnState_();	
						energy.doReloadRecord();
					}, this);
		
					timeSerieComp.on("afterDoDeleteSuccess", function() {
						this._setCalaculateBtnState_();	
						energy.doReloadRecord();
					}, this);
		
					energy.on("recordChange", function(obj) {
						this._setComponentsToolbarState_();
						var recEnergyPrice = obj.dc.getRecord();
						var dp = null;
						if( recEnergyPrice ){
							dp = recEnergyPrice.data.dataProvider;
						}
						var timeSerieItem = this._getDc_("timeSerieItem");
						if(dp === null || dp === __FMBAS_TYPE__.DataProvider._CALCULATED_){
							timeSerieItem.readOnly = true;
						}else{
							timeSerieItem.readOnly = false;
						}
						if ( this.useWorkflowForApproval() ) {
							this._makeEnergyPriceReadOnlyOnAwaitingApproval_ (obj.dc);
						}
					}, this);
		
					energy.on("afterDoSaveSuccess", function () {
					    this._setComponentsToolbarState_();
					}, this);
		
					energy.on("afterDoNew", function (dc) {
					    var rec = dc.getRecord();
					    if (rec) {
					        rec.set("serieType", "E");
					        rec.set("factor", "1");
					        rec.set("activation", "y");
					        rec.set("supplyItemUpdate", "n");
					    }
						dc.setEditMode();
					    this.showWindow();
					}, this);
		
					energy.on("onEditOut", function (dc) {
					    dc.doReloadRecord();
					}, this);
		
					energy.on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwenergyPrice").close();
						}
					}, this);
		
					this._getDc_("timeSerieItem").on("afterDoSaveSuccess", function (dc) {
							dc.doReloadPage(); }, this);
	}
	
	,saveNew: function() {
		
						var energy = this._getDc_("energyPrice");
						energy.doSave({doNewAfterSave:true});
	}
	
	,saveClose: function() {
		
						var energy = this._getDc_("energyPrice");
						energy.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,openHelp: function() {
		
						var url = Main.urlHelp+"/EnergyPricesTimeSeries.html";
						window.open( url, "SONE_Help");
	}
	
	,onWdwenergyPriceClose: function() {
		
						var dc = this._getDc_("energyPrice");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,deleteSerieItem: function() {
		
						this._getDc_("timeSerieItem").doDelete();
	}
	
	,canDeleteEnergyRate: function(dc) {
		
						if ( _SYSTEMPARAMETERS_.sysenergypriceapproval === "true" ) {
							var selectedRecords = dc.selectedRecords;
							for(var i=0 ; i<selectedRecords.length ; ++i){
								var data = selectedRecords[i].data;
								if( data.approvalStatus === __FMBAS_TYPE__.TimeSeriesApprovalStatus._AWAITING_APPROVAL_ ){
									return false;
								}
							}
						}
						return true;
	}
	
	,useWorkflowForApproval: function() {
		
						return _SYSTEMPARAMETERS_.sysenergypriceapproval === "true" ;
	}
	
	,_makeEnergyPriceReadOnlyOnAwaitingApproval_: function(energyPrice) {
		
						var record = energyPrice.getRecord();
						if(record) {
							var setChildrenState = function(state) {
								var childrenDcs = energyPrice.children;
								for (var i = 0; i<childrenDcs.length; i++) {
									childrenDcs[i].readOnly = state;
								}
							}
							var approvalStatus = record.get("approvalStatus");
							if (approvalStatus === __FMBAS_TYPE__.TimeSeriesApprovalStatus._AWAITING_APPROVAL_) {
								energyPrice._maskEditable_ = true;
								setChildrenState(true);
							}
							else { 	
								energyPrice._maskEditable_ = false;					
								setChildrenState(false);
							}
						}
	}
	
	,canSubmitForApproval: function(dc) {
		
						var record = dc.getRecord();
						if(record) {
							var approvalStatus = record.data.approvalStatus;
							var status = record.data.status;
							if ( status === __FMBAS_TYPE__.TimeSeriesStatus._WORK_ && (approvalStatus === __FMBAS_TYPE__.TimeSeriesApprovalStatus._NEW_ 
									|| approvalStatus === __FMBAS_TYPE__.TimeSeriesApprovalStatus._REJECTED_)) {
									return true;
								}
						}
						return false;
	}
	
	,canDeleteTimeSerieItem: function(dc) {
		
						var rec = dc.getRecord();
						if (rec) {
							var calc = ( rec.data.calculated=="0" ); 
							var dcEnergyPrice = this._getDc_("energyPrice");
							var recEnergyPrice = dcEnergyPrice.record;
							if ( _SYSTEMPARAMETERS_.sysenergypriceapproval === "true" && recEnergyPrice) {
								var awaitingApproval = (recEnergyPrice.data.approvalStatus === __FMBAS_TYPE__.TimeSeriesApprovalStatus._AWAITING_APPROVAL_);
								if (awaitingApproval) {
									return false;
								}
							}
							var dp = recEnergyPrice.data.dataProvider;							
							if(dp === __FMBAS_TYPE__.DataProvider._CALCULATED_){
								return false;
							}
							return calc;
						}
						return false;
	}
	
	,openSubmitForApprovalWindow: function() {
		
						var label = "Add your note to be sent to your request to the approvers";
						var title = "Submit for approval";
						this.setupPopUpWindow("wdwApprovalNote","approvalNote","btnSaveCloseApprovalWdw","btnCancelApprovalWdw" , label , title);				
	}
	
	,_afterDefineElements_: function() {
		
						var approveNote = this._getElementConfig_("approveNote"); 
						var rejectNote = this._getElementConfig_("rejectNote");
		
						if( approveNote ){
							approveNote._shouldDisableIfDcIsMasked_ = false;
						}
		
						if( rejectNote ){
							rejectNote._shouldDisableIfDcIsMasked_ = false;
						}
	}
	
	,initFeedback: function(response) {
		
						var responseData = Ext.getResponseDataInJSON(response);
						Main.info(responseData.params.approveRejectResult); 
	}
	
	,openTimeSerieItem: function(dc) {
		
						var dc = this._getDc_("energyPrice");
						var record = dc.getRecord();
						if(record.get("dataProvider") === "Calculated") {
							this._getDc_("timeSerieItem").setReadOnly(true); 
						} else {
							this._getDc_("timeSerieItem").setReadOnly(false); 
						}
						this._showStackedViewElement_("main", "canvas3", this._getDc_("timeSerieItem").doReloadPage());
	}
});
