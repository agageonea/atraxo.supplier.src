/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ActionResultHistory_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_ActionResultHistory_Ds"
	},
	
	
	fields: [
		{name:"value", type:"int", allowNull:true},
		{name:"name", type:"string"},
		{name:"jobExId", type:"int", allowNull:true},
		{name:"startTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"endTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"status", type:"string"},
		{name:"exitCode", type:"string"},
		{name:"exitMessage", type:"string"},
		{name:"actionName", type:"string", noFilter:true, noSort:true},
		{name:"duration", type:"string", noFilter:true, noSort:true},
		{name:"msg", type:"string", noFilter:true, noSort:true},
		{name:"stacktrace", type:"string", noFilter:true, noSort:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ActionResultHistory_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"value", type:"int", allowNull:true},
		{name:"name", type:"string"},
		{name:"jobExId", type:"int", allowNull:true},
		{name:"startTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"endTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"status", type:"string"},
		{name:"exitCode", type:"string"},
		{name:"exitMessage", type:"string"},
		{name:"actionName", type:"string", noFilter:true, noSort:true},
		{name:"duration", type:"string", noFilter:true, noSort:true},
		{name:"msg", type:"string", noFilter:true, noSort:true},
		{name:"stacktrace", type:"string", noFilter:true, noSort:true}
	]
});
