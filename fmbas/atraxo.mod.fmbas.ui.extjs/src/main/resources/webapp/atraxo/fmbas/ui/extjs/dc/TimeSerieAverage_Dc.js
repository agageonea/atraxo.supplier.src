/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.TimeSerieAverage_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.TimeSerieAverage_Ds
});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.TimeSerieAverage_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_TimeSerieAverage_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"avgShortName", dataIndex:"avgCode", width:80, noUpdate: true, maxLength:32, 
			editor: { xtype:"textfield", noUpdate:true, maxLength:32}})
		.addTextColumn({name:"avgLongName", dataIndex:"avgName", width:200, noUpdate: true, maxLength:50, 
			editor: { xtype:"textfield", noUpdate:true, maxLength:50}})
		.addBooleanColumn({name:"active", dataIndex:"active", width:100})
		.addNumberColumn({name:"effectiveOffsetDay", dataIndex:"effectiveOffsetDay", hidden:true, width:120, maxLength:11, align:"right" })
		.addNumberColumn({name:"effectiveOffsetInterval", dataIndex:"effectiveOffsetInterval", hidden:true, width:150, maxLength:11, align:"right" })
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
