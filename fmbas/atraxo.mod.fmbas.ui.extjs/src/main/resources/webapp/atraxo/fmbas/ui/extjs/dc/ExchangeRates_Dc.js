/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.ExchangeRates_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.ExchangeRate_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.ExchangeRates_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_ExchangeRates_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"currency1Code", dataIndex:"currency1Code", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"code", dsField: "currency1Code"} ]}})
			.addLov({name:"currency2Code", dataIndex:"currency2Code", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"code", dsField: "currency2Code"} ]}})
			.addLov({name:"financialSourceCode", dataIndex:"financialSourceCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CodeLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "financialSourceId"} ]}})
			.addLov({name:"avgMethodIndicatorName", dataIndex:"avgMethodIndicatorName", maxLength:50,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.AvgMethLov_Lov", selectOnFocus:true, maxLength:50,
					retFieldMapping: [{lovField:"name", dsField: "avgMethodIndicatorName"} ]}})
			.addDateField({name:"validFrom", dataIndex:"firstUsage"})
			.addDateField({name:"validTo", dataIndex:"lastUsage"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ExchangeRates_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_ExchangeRates_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"fromCurrency", dataIndex:"currency1Code", width:100})
		.addTextColumn({ name:"toCurrency", dataIndex:"currency2Code", width:100})
		.addTextColumn({ name:"source", dataIndex:"financialSourceCode", width:100})
		.addTextColumn({ name:"averagingMethod", dataIndex:"avgMethodIndicatorName", width:150})
		.addDateColumn({ name:"validFrom", dataIndex:"firstUsage", width:100, _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"lastUsage", width:100, _mask_: Masks.DATE})
		.addTextColumn({ name:"description", dataIndex:"description", hidden:true, width:100})
		.addBooleanColumn({ name:"active", dataIndex:"active", hidden:true})
		.addTextColumn({ name:"name", dataIndex:"name", hidden:true, width:120})
		.addTextColumn({ name:"calculatedValue", dataIndex:"calcVal", hidden:true, width:120})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ExchangeRates_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_ExchangeRates_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"fromCurrency", bind:"{d.currency1Code}", dataIndex:"currency1Code", noUpdate:true, maxLength:3, labelWidth:120})
		.addTextField({ name:"toCurrency", bind:"{d.currency2Code}", dataIndex:"currency2Code", noUpdate:true, maxLength:3, labelWidth:120})
		.addTextField({ name:"source", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", noUpdate:true, maxLength:25, labelWidth:120})
		.addTextField({ name:"averagingMethod", bind:"{d.avgMethodIndicatorName}", dataIndex:"avgMethodIndicatorName", noUpdate:true, maxLength:50, labelWidth:120})
		.addDateField({name:"validFrom", bind:"{d.firstUsage}", dataIndex:"firstUsage", noUpdate:true})
		.addDateField({name:"validTo", bind:"{d.lastUsage}", dataIndex:"lastUsage", noUpdate:true})
		.add({name:"nameValf", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("fromCurrency"),this._getConfig_("validFrom")]})
		.add({name:"avgValf", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("toCurrency"),this._getConfig_("validTo")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:600, layout:"anchor"})
		.addPanel({ name:"col2", width:600, layout:"anchor"})
		.addPanel({ name:"col3", width:300, layout:"anchor"})
		.addPanel({ name:"col4", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4"])
		.addChildrenTo("col1", ["nameValf"])
		.addChildrenTo("col2", ["avgValf"])
		.addChildrenTo("col3", ["averagingMethod"])
		.addChildrenTo("col4", ["source"]);
	}
});
