/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Exposure_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_Exposure_Ds"
	},
	
	
	validators: {
		type: [{type: 'presence'}],
		actualUtilization: [{type: 'presence'}],
		lastAction: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("type", "Credit line");
		this.set("lastAction", "Debit");
	},
	
	fields: [
		{name:"type", type:"string"},
		{name:"actualUtilization", type:"float", allowNull:true},
		{name:"lastAction", type:"string"},
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerCode", type:"string"},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"currencyName", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Exposure_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"type", type:"string"},
		{name:"actualUtilization", type:"float", allowNull:true},
		{name:"lastAction", type:"string"},
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerCode", type:"string"},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"currencyName", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
