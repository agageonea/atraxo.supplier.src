/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.AvgMethCodeLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_AvgMethCodeLov_Lov",
	displayField: "code", 
	_columns_: ["name", "code"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{name}, {code}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.fmbas.ui.extjs.ds.AvgMthdLov_Ds
});
