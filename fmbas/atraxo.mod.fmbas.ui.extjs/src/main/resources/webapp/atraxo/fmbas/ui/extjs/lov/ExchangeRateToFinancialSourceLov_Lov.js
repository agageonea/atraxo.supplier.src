/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.ExchangeRateToFinancialSourceLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_ExchangeRateToFinancialSourceLov_Lov",
	displayField: "financialSourceCode", 
	_columns_: ["financialSourceCode", "financialSourceName"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{financialSourceCode}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.fmbas.ui.extjs.ds.ExchangeRateToFinancialSourceAndAvg_Ds
});
