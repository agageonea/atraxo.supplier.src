/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.IataPC_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.IataPC_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.IataPC_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_IataPC_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"code", dataIndex:"code", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.IataLov_Lov", selectOnFocus:true, maxLength:25}})
			.addTextField({ name:"name", dataIndex:"name", maxLength:100})
			.addCombo({ xtype:"combo", name:"use", dataIndex:"use", store:[ __FMBAS_TYPE__.Use._TAX_, __FMBAS_TYPE__.Use._PRODUCT_, __FMBAS_TYPE__.Use._FEE_]})
			.addCombo({ xtype:"combo", name:"source", dataIndex:"source", store:[ __FMBAS_TYPE__.ProductSource._STANDARD_, __FMBAS_TYPE__.ProductSource._CUSTOM_]})
			.addTextField({ name:"sourceNotes", dataIndex:"sourceNotes", maxLength:255})
			.addBooleanField({ name:"active", dataIndex:"active"})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.IataPC_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_IataPC_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"code", dataIndex:"code", width:100, allowBlank: false, maxLength:25, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:25}})
		.addTextColumn({name:"name", dataIndex:"name", width:200, maxLength:100, 
			editor: { xtype:"textfield", maxLength:100}})
		.addComboColumn({name:"use", dataIndex:"use", width:100, 
			editor:{xtype:"combo", mode: 'local', triggerAction:'all', forceSelection:true, store:[ __FMBAS_TYPE__.Use._TAX_, __FMBAS_TYPE__.Use._PRODUCT_, __FMBAS_TYPE__.Use._FEE_]}})
		.addComboColumn({name:"source", dataIndex:"source", width:100, allowBlank: false, 
			editor:{xtype:"combo", mode: 'local', allowBlank:false, triggerAction:'all', forceSelection:true, store:[ __FMBAS_TYPE__.ProductSource._STANDARD_, __FMBAS_TYPE__.ProductSource._CUSTOM_]}})
		.addTextColumn({name:"sourceNotes", dataIndex:"sourceNotes", width:200, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addBooleanColumn({name:"active", dataIndex:"active"})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
