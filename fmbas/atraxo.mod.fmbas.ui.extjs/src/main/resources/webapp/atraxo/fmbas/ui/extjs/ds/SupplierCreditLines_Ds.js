/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.SupplierCreditLines_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_SupplierCreditLines_Ds"
	},
	
	
	validators: {
		currencyId: [{type: 'presence'}],
		amount: [{type: 'presence'}],
		alertPercentage: [{type: 'presence'}],
		creditTerm: [{type: 'presence'}],
		validFrom: [{type: 'presence'}],
		validTo: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("creditTerm", "Prepayment");
		this.set("validFrom", Ext.Date.parse('01.01.2016', 'd.m.Y'));
		this.set("validTo", Ext.Date.parse('01.01.2020', 'd.m.Y'));
	},
	
	fields: [
		{name:"exposure", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"percent", type:"string", noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"currencyId", type:"int", allowNull:true, noFilter:true},
		{name:"currencyCode", type:"string"},
		{name:"currencyCodeGrid", type:"string"},
		{name:"currencyName", type:"string"},
		{name:"companyId", type:"int", allowNull:true, noFilter:true},
		{name:"companyCode", type:"string"},
		{name:"companyName", type:"string"},
		{name:"amount", type:"int", allowNull:true},
		{name:"availability", type:"int", allowNull:true},
		{name:"alertPercentage", type:"int", allowNull:true},
		{name:"creditTerm", type:"string"},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"changeReason", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.SupplierCreditLines_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"exposure", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"percent", type:"string", noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"currencyId", type:"int", allowNull:true, noFilter:true},
		{name:"currencyCode", type:"string"},
		{name:"currencyCodeGrid", type:"string"},
		{name:"currencyName", type:"string"},
		{name:"companyId", type:"int", allowNull:true, noFilter:true},
		{name:"companyCode", type:"string"},
		{name:"companyName", type:"string"},
		{name:"amount", type:"int", allowNull:true},
		{name:"availability", type:"int", allowNull:true},
		{name:"alertPercentage", type:"int", allowNull:true},
		{name:"creditTerm", type:"string"},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"changeReason", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
