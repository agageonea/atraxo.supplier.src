/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Widget_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Widget_Ds
});

/* ================= FILTER: WidgetFilter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Widget_Dc$WidgetFilter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Widget_Dc$WidgetFilter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", allowBlank:false, width:100, maxLength:100})
			.addTextField({ name:"description", dataIndex:"description", width:200, maxLength:1000})
		;
	}

});

/* ================= EDIT-GRID: WidgetList ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Widget_Dc$WidgetList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_Widget_Dc$WidgetList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"code", dataIndex:"code", width:50, allowBlank: false, maxLength:32,  flex:1, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:32}})
		.addTextColumn({name:"name", dataIndex:"name", width:120, allowBlank: false, maxLength:100,  flex:1, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:100}})
		.addTextColumn({name:"description", dataIndex:"description", width:200, maxLength:1000,  flex:1, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addTextColumn({name:"thumbPath", dataIndex:"thumbPath", width:120, allowBlank: false, maxLength:400,  flex:1, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:400}})
		.addTextColumn({name:"cfgPath", dataIndex:"cfgPath", width:120, allowBlank: false, maxLength:400,  flex:1, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:400}})
		.addTextColumn({name:"dsName", dataIndex:"dsName", width:120, allowBlank: false, maxLength:64,  flex:1, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:64}})
		.addTextColumn({name:"methodName", dataIndex:"methodName", width:120, allowBlank: false, maxLength:64,  flex:1, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:64}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
