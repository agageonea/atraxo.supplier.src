/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.Templates_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Templates_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("template", Ext.create(atraxo.fmbas.ui.extjs.dc.Templates_Dc,{trackEditMode: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnDownloadTemplateList",glyph:fp_asc.down_glyph.glyph, disabled:true, handler: this.onBtnDownloadTemplateList,stateManager:[{ name:"selected_one_clean", dc:"template"}], scope:this})
		.addButton({name:"btnHelpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onBtnHelpWdw, scope:this})
		.addButton({name:"btnSelectFile",glyph:fp_asc.export_glyph.glyph, disabled:false, handler: this.onBtnSelectFile, scope:this})
		.addButton({name:"btnCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancel, scope:this})
		.addButton({name:"btnSelectNewTemplate",glyph:fp_asc.export_glyph.glyph, disabled:false, handler: this.onBtnSelectNewTemplate, scope:this})
		.addButton({name:"btnCancelEdit",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelEdit, scope:this})
		.addButton({name:"btnSaveEditTemplate",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveEditTemplate, scope:this})
		.addButton({name:"btnEditTemplate",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnEditTemplate,stateManager:[{ name:"selected_one", dc:"template", and: function() {return (this._canDeleteEditTemplate_());} }], scope:this})
		.addButton({name:"btnDeleteTemplate",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteTemplate,stateManager:[{ name:"selected_one_clean", dc:"template", and: function() {return (this._canDeleteEditTemplate_());} }], scope:this})
		.addDcFilterFormView("template", {name:"filter", xtype:"fmbas_Templates_Dc$Filter"})
		.addDcGridView("template", {name:"list", xtype:"fmbas_Templates_Dc$List"})
		.addDcFormView("template", {name:"newTemplate", xtype:"fmbas_Templates_Dc$New"})
		.addDcFormView("template", {name:"editTemplate", xtype:"fmbas_Templates_Dc$Edit"})
		.addWindow({name:"wdwNew", _hasTitle_:true, width:370, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("newTemplate")],  listeners:{ close:{fn:this.doCancelNewTemplate, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSelectFile"), this._elems_.get("btnCancel")]}]})
		.addWindow({name:"wdwEdit", _hasTitle_:true, width:370, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("editTemplate")],  listeners:{ close:{fn:this.doCancelEditTemplate, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSelectNewTemplate"), this._elems_.get("btnSaveEditTemplate"), this._elems_.get("btnCancelEdit")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["list"], ["center"])
		.addToolbarTo("list", "tlbList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbList", {dc: "template"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnEditTemplate"),this._elems_.get("btnDeleteTemplate"),this._elems_.get("btnDownloadTemplateList"),this._elems_.get("btnHelpWdw")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnDownloadTemplateList
	 */
	,onBtnDownloadTemplateList: function() {
		var successFn = function() {
			this.showTemplateCopy();
		};
		var o={
			name:"downloadTemplate",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("template").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnHelpWdw
	 */
	,onBtnHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnSelectFile
	 */
	,onBtnSelectFile: function() {
		this.doSaveNewTemplate();
	}
	
	/**
	 * On-Click handler for button btnCancel
	 */
	,onBtnCancel: function() {
		this.doCancelNewTemplate();
	}
	
	/**
	 * On-Click handler for button btnSelectNewTemplate
	 */
	,onBtnSelectNewTemplate: function() {
		this.doSelectNewTemplate();
	}
	
	/**
	 * On-Click handler for button btnCancelEdit
	 */
	,onBtnCancelEdit: function() {
		this.doCancelEditTemplate();
	}
	
	/**
	 * On-Click handler for button btnSaveEditTemplate
	 */
	,onBtnSaveEditTemplate: function() {
		this.doSaveEditTemplate();
	}
	
	/**
	 * On-Click handler for button btnEditTemplate
	 */
	,onBtnEditTemplate: function() {
		this._getDc_("template").doEditIn();
	}
	
	/**
	 * On-Click handler for button btnDeleteTemplate
	 */
	,onBtnDeleteTemplate: function() {
		this._getDc_("template").doDelete();
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/FormatManager.html";
					window.open( url, "SONE_Help");
	}
	
	,_canDeleteEditTemplate_: function() {
		
					var dc = this._getDc_("template");
					var r = dc.getRecord();
					var isAdmin = false; 
		
					if (!getApplication().session.roles) {
						return;
					}
		
					getApplication().session.roles.forEach(function(element) {
						if (element === "ADMIN") {
							isAdmin = true;
						}
					});
		
					var isSystemUse = r.data.systemUse;
					if (!isSystemUse) {
						return true;
					} else {
						return isAdmin;
					}
	}
	
	,_afterDefineDcs_: function() {
		
		
					var template = this._getDc_("template");
					
					template.on("afterDoNew", function(dc) {
						console.log("After de new");
						var win = this._getWindow_("wdwNew");
						dc.setEditMode();
						win.show(undefined, function() {
							var r = dc.getRecord();
							if (r) {
								var appMenu = getApplication().menu;
		
								r.beginEdit();
								r.set("userCode", getApplication().session.user.code);
								r.set("userName", getApplication().session.user.name);
								r.set("subsidiaryCode", parent._ACTIVESUBSIDIARY_.code);
								r.set("subsidiaryId", parent._ACTIVESUBSIDIARY_.id);	
								r.set("uploadDate", new Date());
								r.endEdit();
		
								dc.setParamValue("isUpdate", false);
							}
						}, this);
					}, this);
		
					template.on("afterDoEditIn", function(dc) {
						console.log("After de edit");
						var win = this._getWindow_("wdwEdit");
						dc.setEditMode();
						win.show(undefined, function() {
							var r = dc.getRecord();
							if (r) {
								dc.setParamValue("isUpdate", true);
								dc.setParamValue("templateId", r.get("id"));
							}
						}, this);
					}, this);
		
					template.on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwNew").close();
						}
						if (ajaxResult.options.options.doCloseEditWindowAfterSave === true) {
							this._getWindow_("wdwEdit").close();
						}
					}, this);
		
	}
	
	,doSelectNewTemplate: function() {
		
					var editTemplate = this._get_("editTemplate");
					var dc = editTemplate._controller_;
					var r = dc.getRecord();
		
					if (r) {
						var name = r.get("name");
						var typeName = r.get("typeName");
						var file = r.get("file");
		
						if (!Ext.isEmpty(name) && !Ext.isEmpty(typeName) && !Ext.isEmpty(file)) {
							editTemplate._doUpdateFile_();
						}
						else {
							editTemplate._selectFile_();
						}
		
					} 
	}
	
	,doSaveEditTemplate: function() {
		
					var template = this._getDc_("template");
					template.doSave({doCloseEditWindowAfterSave: true}); 
	}
	
	,doSaveNewTemplate: function() {
		
					var newTemplate = this._get_("newTemplate");
					var dc = newTemplate._controller_;
					var r = dc.getRecord();
		
					if (r) {
						var name = r.get("name");
						var typeName = r.get("typeName");
						var file = newTemplate._get_("file").getValue();
						var fileRec = r.get("file");
		
						if (!Ext.isEmpty(name) && !Ext.isEmpty(typeName) && !Ext.isEmpty(file) && !Ext.isEmpty(fileRec)) {
							newTemplate._doUploadFile_();
						}
						else {
							newTemplate._selectFile_();
						}
		
					} 
					
	}
	
	,doCancelNewTemplate: function() {
		
					var newTemplate = this._get_("newTemplate");
					newTemplate._cancel_();
	}
	
	,doCancelEditTemplate: function() {
		
					var editTemplate = this._get_("editTemplate");
					editTemplate._cancel_();
	}
	
	,showTemplateCopy: function() {
		
		                var dc = this._getDc_("template");
		                var fn = dc.getParamValue("previewFile");
		                var r = window.open(Main.urlDownload + "/" + fn,"Preview");
		                r.focus();  
						
	}
});
