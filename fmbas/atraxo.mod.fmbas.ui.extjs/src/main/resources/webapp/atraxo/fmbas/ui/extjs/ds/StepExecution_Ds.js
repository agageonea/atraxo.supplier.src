/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.StepExecution_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_StepExecution_Ds"
	},
	
	
	fields: [
		{name:"jobExecStepExitCode", type:"string"},
		{name:"jobExecStepId", type:"int", allowNull:true},
		{name:"jobExecStepExitMessage", type:"string"},
		{name:"jobExecStepStatus", type:"string"},
		{name:"jobName", type:"string"},
		{name:"jobInstanceId", type:"int", allowNull:true},
		{name:"jobInstanceKey", type:"string"},
		{name:"commitCount", type:"int", allowNull:true},
		{name:"startTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"endTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"exitCode", type:"string"},
		{name:"exitMessage", type:"string"},
		{name:"readCount", type:"int", allowNull:true},
		{name:"rollbackCount", type:"int", allowNull:true},
		{name:"status", type:"string"},
		{name:"writeCount", type:"int", allowNull:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.StepExecution_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"jobExecStepExitCode", type:"string"},
		{name:"jobExecStepId", type:"int", allowNull:true},
		{name:"jobExecStepExitMessage", type:"string"},
		{name:"jobExecStepStatus", type:"string"},
		{name:"jobName", type:"string"},
		{name:"jobInstanceId", type:"int", allowNull:true},
		{name:"jobInstanceKey", type:"string"},
		{name:"commitCount", type:"int", allowNull:true},
		{name:"startTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"endTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"exitCode", type:"string"},
		{name:"exitMessage", type:"string"},
		{name:"readCount", type:"int", allowNull:true},
		{name:"rollbackCount", type:"int", allowNull:true},
		{name:"status", type:"string"},
		{name:"writeCount", type:"int", allowNull:true}
	]
});
