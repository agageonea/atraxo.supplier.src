/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.SystemParameter_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.SystemParameter_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.SystemParameter_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_SystemParameter_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"code", dataIndex:"code", maxLength:32})
			.addTextField({ name:"value", dataIndex:"value", maxLength:255})
			.addTextField({ name:"description", dataIndex:"description", maxLength:255})
			.addTextField({ name:"type", dataIndex:"type", maxLength:250})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.SystemParameter_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_SystemParameter_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"code", dataIndex:"code", width:200, maxLength:32, 
			editor: { xtype:"textfield", maxLength:32}})
		.addTextColumn({name:"value", dataIndex:"value", width:120, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"description", dataIndex:"description", width:500, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"type", dataIndex:"type", width:120, maxLength:250, 
			editor: { xtype:"textfield", maxLength:250}})
		.addTextColumn({name:"refValues", dataIndex:"refValues", width:250, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
