/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.UserNameLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_UserNameLov_Lov",
	displayField: "userName", 
	_columns_: ["id", "userCode", "userName", "loginName", "email"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{userName}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.fmbas.ui.extjs.ds.UsersLov_Ds
});
