/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Locations_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_Locations_Ds"
	},
	
	
	validators: {
		code: [{type: 'presence'}],
		isAirport: [{type: 'presence'}],
		countryCode: [{type: 'presence'}],
		validFrom: [{type: 'presence'}],
		validTo: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("isAirport", true);
	},
	
	fields: [
		{name:"countryId", type:"int", allowNull:true},
		{name:"countryCode", type:"string"},
		{name:"countryName", type:"string"},
		{name:"subCountryId", type:"int", allowNull:true},
		{name:"subCountryCode", type:"string"},
		{name:"timeId", type:"int", allowNull:true},
		{name:"timeName", type:"string"},
		{name:"id", type:"int", allowNull:true},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"iataCode", type:"string"},
		{name:"icaoCode", type:"string"},
		{name:"isAirport", type:"boolean"},
		{name:"latitude", type:"float", allowNull:true},
		{name:"longitude", type:"float", allowNull:true},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Locations_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"countryId", type:"int", allowNull:true},
		{name:"countryCode", type:"string"},
		{name:"countryName", type:"string"},
		{name:"subCountryId", type:"int", allowNull:true},
		{name:"subCountryCode", type:"string"},
		{name:"timeId", type:"int", allowNull:true},
		{name:"timeName", type:"string"},
		{name:"id", type:"int", allowNull:true},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"iataCode", type:"string"},
		{name:"icaoCode", type:"string"},
		{name:"isAirport", type:"boolean", allowNull:true},
		{name:"latitude", type:"float", allowNull:true},
		{name:"longitude", type:"float", allowNull:true},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.fmbas.ui.extjs.ds.Locations_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"areaId", type:"int", forFilter:true, allowNull:true}
	]
});
