/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.CustomerLocations_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_CustomerLocations_Ds"
	},
	
	
	validators: {
		code: [{type: 'presence'}],
		isAirport: [{type: 'presence'}],
		countryCode: [{type: 'presence'}]
	},
	
	fields: [
		{name:"countryId", type:"int", allowNull:true},
		{name:"countryCode", type:"string"},
		{name:"countryName", type:"string"},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"iataCode", type:"string"},
		{name:"icaoCode", type:"string"},
		{name:"isAirport", type:"boolean"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.CustomerLocations_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"countryId", type:"int", allowNull:true},
		{name:"countryCode", type:"string"},
		{name:"countryName", type:"string"},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"iataCode", type:"string"},
		{name:"icaoCode", type:"string"},
		{name:"isAirport", type:"boolean", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.fmbas.ui.extjs.ds.CustomerLocations_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"customerId", type:"int", forFilter:true, allowNull:true}
	]
});
