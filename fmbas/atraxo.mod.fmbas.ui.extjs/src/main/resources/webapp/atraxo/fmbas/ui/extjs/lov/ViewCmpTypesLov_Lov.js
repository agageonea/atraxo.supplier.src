/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.ViewCmpTypesLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_ViewCmpTypesLov_Lov",
	displayField: "cmpType", 
	_columns_: ["cmpType"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{cmpType}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.fmbas.ui.extjs.ds.ViewCmpLov_Ds
});
