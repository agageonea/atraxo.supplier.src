/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.AdvancedFilter_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.AdvancedFilter_Ds
});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.AdvancedFilter_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_AdvancedFilter_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"name", dataIndex:"name", width:100, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"cmp", dataIndex:"cmp", width:400, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"cmpType", dataIndex:"cmpType", width:100, maxLength:16, 
			editor: { xtype:"textfield", maxLength:16}})
		.addTextColumn({name:"standardFilterValue", dataIndex:"standardFilterValue", width:200, maxLength:4000, 
			editor: { xtype:"textfield", maxLength:4000}})
		.addTextColumn({name:"advancedFilterValue", dataIndex:"advancedFilterValue", width:200, maxLength:4000, 
			editor: { xtype:"textfield", maxLength:4000}})
		.addBooleanColumn({name:"isStandard", dataIndex:"isStandard", width:100})
		.addBooleanColumn({name:"defaultFilter", dataIndex:"defaultFilter", width:100})
		.addBooleanColumn({name:"active", dataIndex:"active", width:100})
		.addBooleanColumn({name:"availableSystemwide", dataIndex:"availableSystemwide", width:100})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.AdvancedFilter_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_AdvancedFilter_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", maxLength:255})
		.addTextField({ name:"cmp", bind:"{d.cmp}", dataIndex:"cmp", maxLength:255})
		.addTextField({ name:"cmpType", bind:"{d.cmpType}", dataIndex:"cmpType", maxLength:16})
		.addTextField({ name:"advancedFilterValue", bind:"{d.advancedFilterValue}", dataIndex:"advancedFilterValue", maxLength:4000})
		.addBooleanField({ name:"defaultFilter", bind:"{d.defaultFilter}", dataIndex:"defaultFilter"})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active"})
		.addBooleanField({ name:"availableSystemwide", bind:"{d.availableSystemwide}", dataIndex:"availableSystemwide", width:100})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["name", "cmp", "cmpType", "advancedFilterValue", "defaultFilter", "active", "availableSystemwide"]);
	}
});
