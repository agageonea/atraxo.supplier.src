/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.ActionResultHistory_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.ActionResultHistory_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.ActionResultHistory_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_ActionResultHistory_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"actionName", width:100, maxLength:32})
			.addDateField({name:"startTime", dataIndex:"startTime", width:120})
			.addDateField({name:"endTime", dataIndex:"endTime", width:120})
			.addTextField({ name:"duration", dataIndex:"duration", width:120, maxLength:32})
			.addTextField({ name:"status", dataIndex:"status", width:100, maxLength:10})
			.addTextField({ name:"exitCode", dataIndex:"exitCode", width:100, maxLength:2500})
			.addTextField({ name:"exitMessage", dataIndex:"msg", width:100, maxLength:100})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ActionResultHistory_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_ActionResultHistory_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"actionName", width:50,  flex:1})
		.addDateColumn({ name:"startTime", dataIndex:"startTime", _mask_: Masks.DATETIME,  flex:1})
		.addDateColumn({ name:"endTime", dataIndex:"endTime", _mask_: Masks.DATETIME,  flex:1})
		.addTextColumn({ name:"duration", dataIndex:"duration", width:50,  flex:1})
		.addTextColumn({ name:"status", dataIndex:"status", width:50,  flex:1})
		.addTextColumn({ name:"exitCode", dataIndex:"exitCode", width:50,  flex:1})
		.addTextColumn({ name:"msg", dataIndex:"msg", width:120,  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Error ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ActionResultHistory_Dc$Error", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_ActionResultHistory_Dc$Error",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"msg", bind:"{d.stacktrace}", dataIndex:"stacktrace", noEdit:true , width:700, noLabel: true, height:400})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col"])
		.addChildrenTo("col", ["msg"]);
	}
});
