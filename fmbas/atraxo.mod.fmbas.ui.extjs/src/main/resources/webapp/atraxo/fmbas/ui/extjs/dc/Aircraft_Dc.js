/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Aircraft_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Aircraft_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Aircraft_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Aircraft_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"registration", dataIndex:"registration", maxLength:10})
			.addLov({name:"acType", dataIndex:"acTypeCode", maxLength:4,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.AcTypesLov_Lov", selectOnFocus:true, maxLength:4,
					retFieldMapping: [{lovField:"id", dsField: "acTypeId"} ],
					filterFieldMapping: [{lovField:"active", value: "true"} ]}})
			.addDateField({name:"validFrom", dataIndex:"validFrom"})
			.addDateField({name:"validTo", dataIndex:"validTo"})
			.addLov({name:"owner", dataIndex:"ownerCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "ownerId"} ]}})
			.addLov({name:"operator", dataIndex:"operCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "operId"} ]}})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Aircraft_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_Aircraft_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"registration", dataIndex:"registration", width:100, allowBlank: false, maxLength:10, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:10}})
		.addLov({name:"acType", dataIndex:"acTypeCode", width:100, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_AcTypesLov_Lov", allowBlank:false, maxLength:4,
				retFieldMapping: [{lovField:"id", dsField: "acTypeId"} ],
				filterFieldMapping: [{lovField:"active", value: "true"} ]}})
		.addDateColumn({name:"validFrom", dataIndex:"validFrom", width:100, allowBlank: false, _mask_: Masks.DATE })
		.addDateColumn({name:"validTo", dataIndex:"validTo", width:100, allowBlank: false, _mask_: Masks.DATE })
		.addLov({name:"owner", dataIndex:"ownerCode", width:100, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "ownerId"} ]}})
		.addLov({name:"operator", dataIndex:"operCode", width:100, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "operId"} ]}})
		.addLov({name:"customer", dataIndex:"custCode", hidden:true, width:100, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "custId"} ]}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: New ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Aircraft_Dc$New", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Aircraft_Dc$New",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"registration", bind:"{d.registration}", dataIndex:"registration", allowBlank:false, width:100, noLabel: true, maxLength:10})
		.addLov({name:"acType", bind:"{d.acTypeCode}", dataIndex:"acTypeCode", allowBlank:false, width:100, noLabel: true, xtype:"fmbas_AcTypesLov_Lov", maxLength:4,
			retFieldMapping: [{lovField:"id", dsField: "acTypeId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addHiddenField({ name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", allowBlank:false, width:100})
		.addHiddenField({ name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", allowBlank:false, width:100})
		.addTextField({ name:"customer", bind:"{d.custCode}", dataIndex:"custCode", noEdit:true , width:100, noLabel: true, maxLength:32})
		.addHiddenField({ name:"custId", bind:"{d.custId}", dataIndex:"custId", allowBlank:false, width:100})
		.addHiddenField({ name:"operId", bind:"{d.operId}", dataIndex:"operId", allowBlank:false, width:100})
		.addHiddenField({ name:"ownerId", bind:"{d.ownerId}", dataIndex:"ownerId", allowBlank:false, width:100})
		.addDisplayFieldText({ name:"customerLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"registrationLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"acTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"table"}, padding:"0 30 5 0"})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left:50px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3",  style:"margin-left:50px", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["table1", "table2", "table3"])
		.addChildrenTo("table1", ["customerLabel", "customer", "validFrom", "validTo"])
		.addChildrenTo("table2", ["registrationLabel", "registration"])
		.addChildrenTo("table3", ["acTypeLabel", "acType"]);
	}
});
