/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.BatchJobParameter_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_BatchJobParameter_Ds"
	},
	
	
	fields: [
		{name:"batchJobId", type:"int", allowNull:true},
		{name:"batchJobName", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"value", type:"string"},
		{name:"type", type:"string"},
		{name:"assignType", type:"string"},
		{name:"refValues", type:"string"},
		{name:"refBusinessValues", type:"string"},
		{name:"readOnly", type:"boolean"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.BatchJobParameter_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"batchJobId", type:"int", allowNull:true},
		{name:"batchJobName", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"value", type:"string"},
		{name:"type", type:"string"},
		{name:"assignType", type:"string"},
		{name:"refValues", type:"string"},
		{name:"refBusinessValues", type:"string"},
		{name:"readOnly", type:"boolean", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
