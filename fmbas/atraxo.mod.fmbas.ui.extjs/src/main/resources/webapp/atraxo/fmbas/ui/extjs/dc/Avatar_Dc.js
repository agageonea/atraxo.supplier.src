/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Avatar_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Avatar_Ds,
		
			/* ================ Business functions ================ */
	
	doNew: function() {
		
					if (this._doNewWdw_ == null ) {
						this._doNewWdw_ = Ext.create("Ext.window.Window", {
							width:330, 
							height:210,
							title:"Upload avatar",
							closable: false,
							closeAction: "hide",
							resizable:true, 
							layout:"fit", 
							modal:true,
							items: {
								_controller_:this,
								xtype:"fmbas_Avatar_Dc$New"
							},
							tools: [{
				                type : "close",
								listeners: {
									click: {
										scope: this,
										fn: function() {
											this._doNewWdw_.close();
											this.doCancel();
										}
									}
								}
				            }]
						});
					}
					this._doNewWdw_.show();
					this.commands.doNew.execute();
	}

});

/* ================= FILTER FORM: Filter ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Avatar_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterForm",
	alias: "widget.fmbas_Avatar_Dc$Filter",

	/**
	 * Components definition
	 */	
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", maxLength:255})
		.addLov({name:"userName", bind:"{d.userName}", dataIndex:"userName", xtype:"fmbas_UserNameLov_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "userId"} ]})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active"})
		.addTextField({ name:"fileName", bind:"{d.fileName}", dataIndex:"fileName", width:130, maxLength:255})
		.addTextField({ name:"contentType", bind:"{d.contentType}", dataIndex:"contentType", width:130, maxLength:32})
		
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */				
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["name", "userName", "fileName", "contentType", "active"]);
	}
});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Avatar_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Avatar_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:160,  renderer:function(val, meta, record, rowIndex) { meta.style="cursor:pointer"; return '<span style="color: #0179B5; text-decoration:underline">'+record.data.name+'</span>'; }, listeners:{click: {scope:this, fn: function() {return this._openReport_();}}}})
		.addTextColumn({ name:"userName", dataIndex:"userName", width:130})
		.addTextColumn({ name:"fileName", dataIndex:"fileName", width:130})
		.addTextColumn({ name:"contentType", dataIndex:"contentType", width:130})
		.addBooleanColumn({ name:"active", dataIndex:"active"})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_openReport_: function() {
		
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var id = record.get("id");
						var contentType = record.get("contentType");
						var baseUrl = record.get("baseUrl");
		
						var url = baseUrl+"/"+id+"."+contentType;
		
						return window.open(url, "Avatar", "location=1,status=1,scrollbars=1,width=300,height=300");
		
	}
});

/* ================= EDIT FORM: New ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Avatar_Dc$New", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Avatar_Dc$New",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btnSelectFile", scope: this, handler: this._selectFile_, text: "Select file"})
		.addButton({name:"btnCancel", scope: this, handler: this._cancel_, text: "Cancel"})
		.addLov({name:"type", bind:"{d.type}", dataIndex:"type", allowBlank:false, xtype:"ad_AttachmentTypes_Lov", maxLength:255, listeners:{change: {scope: this,fn:function(e) {this._save_()}}},
			retFieldMapping: [{lovField:"id", dsField: "typeId"} ],
			filterFieldMapping: [{lovField:"targetAlias", value: "Avatar"} ]})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, maxLength:255})
		.addLov({name:"userName", bind:"{d.userName}", dataIndex:"userName", xtype:"fmbas_UserNameLov_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "userId"} ]})
		.addTextField({ name:"location", bind:"{d.location}", dataIndex:"location", _enableFn_: function(dc, rec) { return rec.data.category == 'link' ; } , maxLength:400})
		.addUploadField({ name:"file", bind:"{d.filePath}", dataIndex:"filePath", style:"display:none", listeners:{change: {scope: this,fn:function(e) {this._save_()}}}})
		.addHiddenField({ name:"typeId", bind:"{d.typeId}", dataIndex:"typeId"})
		.addHiddenField({ name:"userId", bind:"{d.userId}", dataIndex:"userId"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, buttons: [this._getConfig_("btnSelectFile"),this._getConfig_("btnCancel")],  xtype:"panel", buttonAlign:"left"})
		.addPanel({ name:"col", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col"])
		.addChildrenTo("col", ["name", "userName", "type", "file", "typeId", "userId"]);
	},
	/* ==================== Business functions ==================== */
	
	_selectFile_: function() {
		
						var fileUploadField = this._get_("file");
						var uploadButton = fileUploadField.button;
						var elemId = fileUploadField.button.fileInputEl.dom.getAttribute("id");
						var elem = document.getElementById(elemId);
		
					   if(elem && document.createEvent) {
					      var evt = document.createEvent("MouseEvents");
					      evt.initEvent("click", true, false);
					      elem.dispatchEvent(evt);
					   }
	},
	
	_save_: function() {
		
						var me = this;
						var fileUploadField = this._get_("file");
						var uploadFieldValue = fileUploadField.getValue();
		
						if (me.isValid() && !Ext.isEmpty(uploadFieldValue)) {
							this._doUploadFile_();
						}
	},
	
	_cancel_: function() {
		
						this.up("window").hide();
						this._controller_.doCancel();
	},
	
	_doUploadFile_: function() {
		
						var window = this._controller_._doNewWdw_;
						var form = window.items.get(0);
						var rd = this._controller_.record.data;
						var cfg = {
				                _handler_ : "uploadAvatar",
				                _succesCallbackScope_ : this._controller_,
								_succesCallbackFn_ : function() {
									
									this.store.commitChanges();
									this.doReloadPage();
									window.close();
								}
				        };
						Ext.apply(window,cfg);
						var formCfg = {
							fileUpload : true
						}
						Ext.apply(form,formCfg);
						if (form.getForm().isValid()) {
							form.getForm().submit(
									{
										url : Main.urlUpload + "/" + window._handler_,
										waitMsg : Main.translate("msg", "uploading"),
										scope : window,
										success : function(form, action) {
											try {
												Ext.Msg.hide();
											} catch (e) {
				
											}
											if (this._succesCallbackFn_ != null) {
												this._succesCallbackFn_.call(this._succesCallbackScope_ || this);
											}
										},
										failure : function(form, action) {
		
											try {
												Ext.Msg.hide();
											} catch (e) {
												
											}
											switch (action.failureType) {
											case Ext.form.Action.CLIENT_INVALID:
												Main.error("INVALID_FORM");
												break;
											case Ext.form.Action.CONNECT_FAILURE:
												Main.serverMessage(null, action.response);
												break;
											case Ext.form.Action.SERVER_INVALID:
												Main.serverMessage(null, action.response);
											}
										}
									});
						} else {
							Main.error("INVALID_FORM");
						}
		
	}
});
