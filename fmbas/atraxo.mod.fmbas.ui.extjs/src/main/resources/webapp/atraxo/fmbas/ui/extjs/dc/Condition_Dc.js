/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Condition_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Condition_Ds
});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Condition_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_Condition_Dc$List",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"fieldName", dataIndex:"fieldName", width:120, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_FieldLov_Lov", maxLength:250,
				retFieldMapping: [{lovField:"fieldName", dsField: "fieldName"} ],
				filterFieldMapping: [{lovField:"externalInterfaceId", dsField: "externalInterfaceId"} ]},  flex:1})
		.addComboColumn({name:"operator", dataIndex:"operator", width:70, 
			editor:{xtype:"combo", mode: 'local', triggerAction:'all', forceSelection:true, store:[ __FMBAS_TYPE__.DictionaryOperator._Equal_, __FMBAS_TYPE__.DictionaryOperator._NotEqual_]},  flex:1})
		.addTextColumn({name:"value", dataIndex:"value", width:120, maxLength:250,  flex:1, 
			editor: { xtype:"textfield", maxLength:250}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
