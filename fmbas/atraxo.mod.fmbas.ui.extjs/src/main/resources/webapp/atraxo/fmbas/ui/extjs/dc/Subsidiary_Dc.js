/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Subsidiary_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Subsidiary_Ds
});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Subsidiary_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Subsidiary_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:150})
		.addTextColumn({ name:"name", dataIndex:"name", width:150})
		.addTextColumn({ name:"officeCity", dataIndex:"officeCity", width:200})
		.addTextColumn({ name:"officeCountryName", dataIndex:"officeCountryName", width:200})
		.addTextColumn({ name:"assignedLocation", dataIndex:"assignedAreaName", width:200})
		.addTextColumn({ name:"officeStreet", dataIndex:"officeStreet", hidden:true, width:100})
		.addTextColumn({ name:"officeStateName", dataIndex:"officeStateName", hidden:true, width:100})
		.addTextColumn({ name:"officePostalCode", dataIndex:"officePostalCode", hidden:true, width:100})
		.addTextColumn({ name:"accountNumber", dataIndex:"accountNumber", hidden:true, width:100})
		.addTextColumn({ name:"timeZoneName", dataIndex:"timeZoneName", hidden:true, width:100})
		.addTextColumn({ name:"phoneNo", dataIndex:"phoneNo", hidden:true, width:100})
		.addTextColumn({ name:"faxNo", dataIndex:"faxNo", hidden:true, width:100})
		.addTextColumn({ name:"website", dataIndex:"website", hidden:true, width:100})
		.addTextColumn({ name:"email", dataIndex:"email", hidden:true, width:160})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Subsidiary_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Subsidiary_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"code", dataIndex:"code", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32}})
			.addTextField({ name:"name", dataIndex:"name", maxLength:100})
			.addTextField({ name:"accountNumber", dataIndex:"accountNumber", maxLength:32})
			.addLov({name:"timeZoneName", dataIndex:"timeZoneName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.TimeZoneLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "timeZoneId"} ]}})
			.addTextField({ name:"phoneNo", dataIndex:"phoneNo", maxLength:100})
			.addTextField({ name:"faxNo", dataIndex:"faxNo", maxLength:100})
			.addTextField({ name:"website", dataIndex:"website", maxLength:100})
			.addTextField({ name:"officeStreet", dataIndex:"officeStreet", maxLength:50})
			.addTextField({ name:"officeCity", dataIndex:"officeCity", maxLength:100})
			.addTextField({ name:"officePostalCode", dataIndex:"officePostalCode", maxLength:32})
			.addLov({name:"officeCountryName", dataIndex:"officeCountryName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CountryNameLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "officeCountryId"} ,{lovField:"name", dsField: "officeCountryName"} ,{lovField:"code", dsField: "officeCountryCode"} ]}})
			.addLov({name:"officeStateName", dataIndex:"officeStateName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CountryNameLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "officeStateId"} ,{lovField:"name", dsField: "officeStateName"} ,{lovField:"code", dsField: "officeStateCode"} ],
					filterFieldMapping: [{lovField:"countryId", dsField: "officeCountryId"} ]}})
		;
	}

});

/* ================= GRID: AssignList2 ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Subsidiary_Dc$AssignList2", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Subsidiary_Dc$AssignList2",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:50,  flex:1})
		.addTextColumn({ name:"name", dataIndex:"name", width:120,  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: NewAndEdit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Subsidiary_Dc$NewAndEdit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Subsidiary_Dc$NewAndEdit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", allowBlank:false, width:180, maxLength:32, hideLabel:"true"})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, width:180, maxLength:100, hideLabel:"true"})
		.addLov({name:"timeZoneName", bind:"{d.timeZoneName}", dataIndex:"timeZoneName", width:180, noLabel: true, xtype:"fmbas_TimeZoneLov_Lov", maxLength:100,
			retFieldMapping: [{lovField:"id", dsField: "timeZoneId"} ]})
		.addLov({name:"subsidiaryAssignedArea", bind:"{d.assignedAreaCode}", dataIndex:"assignedAreaCode", allowBlank:false, width:180, noLabel: true, xtype:"fmbas_AreasLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "assignedAreaId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addDisplayFieldText({ name:"separator", bind:"{d.separator}", dataIndex:"separator", maxLength:64, labelWidth:680, labelAlign:"center"})
		.addDisplayFieldText({ name:"separator1", bind:"{d.separator}", dataIndex:"separator", maxLength:64, labelWidth:680, labelAlign:"center"})
		.addTextField({ name:"accountNumber", bind:"{d.accountNumber}", dataIndex:"accountNumber", width:180, maxLength:32, hideLabel:"true"})
		.addTextField({ name:"phoneNo", bind:"{d.phoneNo}", dataIndex:"phoneNo", width:180, maxLength:100, hideLabel:"true"})
		.addTextField({ name:"email", bind:"{d.email}", dataIndex:"email", width:180, maxLength:250, hideLabel:"true"})
		.addTextField({ name:"website", bind:"{d.website}", dataIndex:"website", width:180, maxLength:100, hideLabel:"true"})
		.addDisplayFieldText({ name:"codeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"nameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"accountNumberLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"timeZoneLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"subsidiaryAssignedAreaLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"phoneNumberLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-left:50px"})
		.addDisplayFieldText({ name:"emailLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-left:50px"})
		.addDisplayFieldText({ name:"websiteLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-left:50px"})
		.addTextField({ name:"officeStreet", bind:"{d.officeStreet}", dataIndex:"officeStreet", width:180, noLabel: true, maxLength:50})
		.addTextField({ name:"officeCity", bind:"{d.officeCity}", dataIndex:"officeCity", width:180, noLabel: true, maxLength:100})
		.addTextField({ name:"officePostalCode", bind:"{d.officePostalCode}", dataIndex:"officePostalCode", width:180, noLabel: true, maxLength:32})
		.addLov({name:"officeStateName", bind:"{d.officeStateName}", dataIndex:"officeStateName", _enableFn_: function(dc, rec) { return !Ext.isEmpty(dc.record.data.officeCountryId); } , width:180, noLabel: true, xtype:"fmbas_CountryNameLov_Lov", maxLength:100,
			retFieldMapping: [{lovField:"id", dsField: "officeStateId"} ,{lovField:"name", dsField: "officeStateName"} ,{lovField:"code", dsField: "officeStateCode"} ],
			advFilter: [{"lovFieldName":"countryId","operation":"=","fieldName1":"officeCountryId"}]})
		.addLov({name:"officeCountryName", bind:"{d.officeCountryName}", dataIndex:"officeCountryName", width:180, noLabel: true, xtype:"fmbas_CountryNameLov_Lov", maxLength:100, _localCache_:true,
			retFieldMapping: [{lovField:"id", dsField: "officeCountryId"} ,{lovField:"name", dsField: "officeCountryName"} ,{lovField:"code", dsField: "officeCountryCode"} ],listeners:{
			fpchange:{scope:this, fn:this._clearOfficeStateValue_}
		}})
		.addLov({name:"subsidiaryCurrencyCode", bind:"{d.subsidiaryCurrencyCode}", dataIndex:"subsidiaryCurrencyCode", width:180, noLabel: true, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, _localCache_:true,
			retFieldMapping: [{lovField:"id", dsField: "subsidiaryCurrencyId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addDisplayFieldText({ name:"officeAddressLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-top:10px; padding-bottom: 10px", colspan:2, listeners:{afterrender: {scope: this, fn: function(el) { this._setBorder_(el) }}}})
		.addDisplayFieldText({ name:"dummyField", bind:"{d.dummyField}", dataIndex:"dummyField", maxLength:32, style:"padding-top:10px; padding-left:50px; padding-bottom: 10px", colspan:2, listeners:{afterrender: {scope: this, fn: function(el) { this._setBorder_(el) }}}})
		.addDisplayFieldText({ name:"streetLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"cityLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"stateProvinceLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"zipPostalCodeLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"countryLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"subsidiaryCurrencyLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addLov({name:"defaultVolumeUnitCode", bind:"{d.defaultVolumeUnitCode}", dataIndex:"defaultVolumeUnitCode", width:180, noLabel: true, xtype:"fmbas_UnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "defaultVolumeUnitId"} ],
			filterFieldMapping: [{lovField:"unitTypeInd", value: "Volume"} ]})
		.addLov({name:"defaultWeightUnitCode", bind:"{d.defaultWeightUnitCode}", dataIndex:"defaultWeightUnitCode", width:180, noLabel: true, xtype:"fmbas_UnitsLov_Lov", maxLength:2, style:"padding-bottom:10px",
			retFieldMapping: [{lovField:"id", dsField: "defaultWeightUnitId"} ],
			filterFieldMapping: [{lovField:"unitTypeInd", value: "Mass"} ]})
		.addDisplayFieldText({ name:"defaultVolumeUnitLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"defaultWeightUnitLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"blankDummyField1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"height:36px"})
		.addDisplayFieldText({ name:"blankDummyField2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  style:"padding:10px"})
		.addPanel({ name:"tableContainer1", layout: {type:"table"}})
		.addPanel({ name:"tableContainer2", layout: {type:"table", columns:2}})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3Title", layout: {type:"table"}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["tableContainer1"])
		.addChildrenTo("tableContainer1", ["table1", "table2"])
		.addChildrenTo("tableContainer2", ["table3Title"])
		.addChildrenTo("table1", ["codeLabel", "code", "nameLabel", "name", "accountNumberLabel", "accountNumber", "subsidiaryAssignedAreaLabel", "subsidiaryAssignedArea", "timeZoneLabel", "timeZoneName", "blankDummyField1", "blankDummyField2", "officeAddressLabel", "streetLabel1", "officeStreet", "cityLabel1", "officeCity", "countryLabel1", "officeCountryName", "zipPostalCodeLabel1", "officePostalCode", "stateProvinceLabel1", "officeStateName"])
		.addChildrenTo("table2", ["phoneNumberLabel", "phoneNo", "emailLabel", "email", "websiteLabel", "website", "subsidiaryCurrencyLabel", "subsidiaryCurrencyCode", "defaultVolumeUnitLabel", "defaultVolumeUnitCode", "defaultWeightUnitLabel", "defaultWeightUnitCode", "dummyField"])
		.addChildrenTo("table3Title", ["officeAddressLabel"]);
	},
	/* ==================== Business functions ==================== */
	
	_clearOfficeStateValue_: function() {
		
						var f = function(){ 
							var dc = this._controller_;
							var r = dc.getRecord();
							var officeCountryName = this._get_("officeCountryName").getValue();
							var officeStateName = this._get_("officeStateName");
			
							if (Ext.isEmpty(officeCountryName)) {
								officeStateName.setReadOnly(true);
								if (r) {
									r.set("officeStateId","");
									r.set("officeStateCode","");
									r.set("officeStateName","");
								}
							}
							else {
								officeStateName.setReadOnly(false);
								if (r && r.dirty == true) {
									if ("officeCountryId" in r.modified) {
										r.set("officeStateId","");
										r.set("officeStateCode","");
										r.set("officeStateName","");
									}
								}
							}
						};
						Ext.defer(f, 100, this);
	},
	
	_setBorder_: function(el) {
		
						var td = el.getEl().dom.parentNode; 
						td.style.borderTop = "1px solid #d0d0d0";
	}
});
