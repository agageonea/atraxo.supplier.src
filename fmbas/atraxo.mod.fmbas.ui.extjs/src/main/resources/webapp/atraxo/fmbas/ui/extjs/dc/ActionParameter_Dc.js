/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.ActionParameter_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.ActionParameter_Ds
});

/* ================= EDIT FORM: actionParameterForm ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ActionParameter_Dc$actionParameterForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_ActionParameter_Dc$actionParameterForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addUploadField({ name:"file", bind:"{d.filePath}", dataIndex:"filePath", listeners:{change: {scope: this,fn:function() {this._save_()}}}})
		.addHiddenField({ name:"id", bind:"{d.id}", dataIndex:"id"})
		/* =========== containers =========== */
		.addPanel({ name:"col", width:300, layout:"anchor"})
		.addPanel({ name:"main", autoScroll:true});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("col", ["file", "id"])
		.addChildrenTo("main", ["col"]);
	},
	/* ==================== Business functions ==================== */
	
	_selectFile_: function() {
		
						var fileUploadField = this._get_("file");
						var elemId = fileUploadField.button.fileInputEl.dom.getAttribute("id");
						var elem = document.getElementById(elemId);
		
					   if(elem && document.createEvent) {
					      var evt = document.createEvent("MouseEvents");
					      evt.initEvent("click", true, false);
					      elem.dispatchEvent(evt);
					   }
	},
	
	_save_: function() {
		
						var me = this;
						var fileUploadField = me._get_("file");
						var uploadFieldValue = fileUploadField.getValue();
		
						if (me.isValid() && !Ext.isEmpty(uploadFieldValue)) {
							me._doUploadFile_(uploadFieldValue);
						}
	},
	
	_doUploadFile_: function(fileName) {
		
						
						var form = this;
						var formCfg = {
							fileUpload : true,
			                _handler_ : "uploadSSHKey",
							_rec_ : this._controller_.getRecord(),
							_fileName_ : fileName,
			                _succesCallbackScope_ : form,
							_succesCallbackFn_ : function() {
								var fullPath = this._fileName_;
								var fileName = fullPath.split("\\").pop().split("/").pop();						
								this._rec_.set("value",fileName);
							}
						}
						Ext.apply(form,formCfg);
						if (form.getForm().isValid()) {
							form.getForm().submit(
									{
										url : Main.urlUpload + "/" + formCfg._handler_,
										waitMsg : Main.translate("msg", "uploading"),
										scope : form,
										success : function(a,b) {
											try {
												Ext.Msg.hide();
		
											} catch (e) {
												// nothing to do
											}
											if (this._succesCallbackFn_ != null) {
												var businessValue = b.result.PATH;
												this._rec_.set("businessValue", businessValue);	
												this._succesCallbackFn_.call(this._succesCallbackScope_ || this);
											}
										},
										failure : function(form, action) {
											try {
												Ext.Msg.hide();
												
											} catch (e) {
												// nothing to do
											}
											if( action.failureType === Ext.form.Action.CLIENT_INVALID ){
												Main.error("INVALID_FORM");
											} else {
												Main.serverMessage(null, action.response);
											}
											if (this._succesCallbackFn_ != null) {
												this._succesCallbackFn_.call(this._succesCallbackScope_ || this);
											}
										}
									});
						} else {
							Main.error("INVALID_FORM");
						}
	}
});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ActionParameter_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_ActionParameter_Dc$EditList",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"name", dataIndex:"name", width:50, noEdit: true, maxLength:32,  flex:1})
		.addTextColumn({name:"description", dataIndex:"description", width:120, noEdit: true, maxLength:255,  flex:1})
		.addTextColumn({name:"value", dataIndex:"value", width:50, maxLength:100,  flex:1, renderer:function(value,meta) {return this._injectAssignButton_(value,meta);}, 
			editor: { xtype:"textfield", maxLength:100}})
		.addTextColumn({name:"defaultValue", dataIndex:"defaultValue", width:50, noEdit: true, maxLength:50,  flex:1, renderer:function(val, meta, record) {return this._maskField_(val, meta, record);}})
		.addTextColumn({name:"type", dataIndex:"type", width:70, noEdit: true, maxLength:32,  flex:1})
		.addBooleanColumn({name:"readOnly", dataIndex:"readOnly", noEdit: true,  flex:1})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_injectAssignButton_: function(value,meta) {
		
		
						var id = Ext.id();
						meta.tdCls += "sone-cell-with-button";
						var ctx = this;
						var rec = meta.record;
						var type = rec.get("type");
						var maskedValue;
						var replaceCharacter = "*";
		
						if (type === __FMBAS_TYPE__.JobParamType._MASKED_) {
							maskedValue = value.split("");
							var i = 0, l = maskedValue.length;
							for (i; i < l; i++) {
								maskedValue[i] = replaceCharacter;
							}
							value = maskedValue.toString().replace(/,/g,"");
						}
		
						if (type === __FMBAS_TYPE__.JobParamType._DATE_) {	
							if(!Ext.isEmpty(value))	{			
								var date = new Date(value);
								value = ("0" + date.getDate()).slice(-2) + "." + ("0" + (date.getMonth() + 1)).slice(-2) + "." +  date.getFullYear();
							}
						}
		
						Ext.defer(function () {	
		
							if( document.getElementById(id) && (type === __FMBAS_TYPE__.JobParamType._ASSIGN_ || type === __FMBAS_TYPE__.JobParamType._KEY_)){
						        Ext.widget("button", {
						            renderTo: id,
									rec: rec,
						            glyph: "xf067@FontAwesome",		
									cls: "sone-grid-assign-btn",				
						            handler: function (b) {							
										b.focus(true,100); 
										ctx.__asgnBtnHandler__(b.rec);
									}
						        });
							} 
					    }, 50);	
		
						return value+"<div class='sone-assign-button' id="+id+"></div>";
					    
	},
	
	__asgnBtnHandler__: function(rec) {
		
						var ctrl = this._controller_;
		                ctrl.setSelectedRecords([rec]);
						var frame = ctrl.getFrame();
						var win;
		
		                if( rec.get("type") === __FMBAS_TYPE__.JobParamType._ASSIGN_ ){
							if(rec.get("assignType") === __FMBAS_TYPE__.JobParamAssignType._CUSTOMER_){
								win = frame._getWindow_("customerAsgnWdw");
							}
							if(rec.get("assignType") === __FMBAS_TYPE__.JobParamAssignType._SUBSIDIARY_){
								win = frame._getWindow_("subsidiaryAsgnWdw");
							}
							var f = function() {
								if( win ){
									win.show();
								}
							}
							Ext.defer(f, 200, this);
		                } else if ( rec.get("type") === __FMBAS_TYPE__.JobParamType._KEY_ ) {
							var form = frame._getElement_("actionParameterNew");
							form._selectFile_();
						}
	},
	
	_maskField_: function(val,meta,record) {
		
		
						var type = record.get("type");
						var maskedValue;
						var replaceCharacter = "*";
						if (type === "masked") {
							maskedValue = val.split("");
							var i = 0, l = maskedValue.length;
							for (i; i < l; i++) {
								maskedValue[i] = replaceCharacter;
							}
							val = maskedValue.toString().replace(/,/g,"");
						}
						return val;
	}
});
