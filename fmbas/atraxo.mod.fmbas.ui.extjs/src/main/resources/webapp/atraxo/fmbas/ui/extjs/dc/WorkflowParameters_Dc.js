/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.WorkflowParameters_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.WorkflowParameter_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.WorkflowParameter_Ds
});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.WorkflowParameters_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_WorkflowParameters_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"name", dataIndex:"name", width:200, noEdit: true, maxLength:255,  flex:1})
		.addTextColumn({name:"description", dataIndex:"description", width:200, noEdit: true, maxLength:1000,  flex:2})
		.addTextColumn({name:"paramValue", dataIndex:"paramValue", width:200, maxLength:1000,  renderer:function(value,meta) {return this._injectAssignButton_(value,meta);}, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addTextColumn({name:"defaultValue", dataIndex:"defaultValue", width:200, noEdit: true, maxLength:1000,  renderer:function(val, meta, record) {return this._maskField_(val, meta, record);}})
		.addComboColumn({name:"type", dataIndex:"type", width:70, noEdit: true,  flex:1})
		.addBooleanColumn({name:"mandatory", dataIndex:"mandatory", noEdit: true,  flex:1})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_injectAssignButton_: function(value,meta) {
		
		
						var id = Ext.id();
						meta.tdCls += "sone-cell-with-button";
						var ctx = this;				
						var rec = meta.record;
						var type = rec.get("type");
						var maskedValue;
						var replaceCharacter = "*";
		
						if (type === "masked") {
							maskedValue = value.split("");
							var i = 0, l = maskedValue.length;
							for (i; i < l; i++) {
								maskedValue[i] = replaceCharacter;
							}
							value = maskedValue.toString().replace(/,/g,"");
						}
		
						Ext.defer(function () {	
							if( document.getElementById(id) && rec.get("type") === __FMBAS_TYPE__.WorkflowParamType._ASSIGN_ ){
						        Ext.widget("button", {
						            renderTo: id,
									rec: rec,
						            glyph: "xf067@FontAwesome",	
									cls: "sone-grid-assign-btn",					
						            handler: function (b) {							
										b.focus(true,100); 
										ctx.__asgnBtnHandler__(b.rec);
									}
						        });
							}
					    }, 50);	
						return value+"<div class='sone-assign-button' id="+id+"></div>";
					    
	},
	
	__asgnColEnabled__: function(record,item) {
		
						if( record.get("type") === __FMBAS_TYPE__.WorkflowParamType._ASSIGN_ ){
							item.setText("...");
							item.removeCls("x-hidden-clip");
							return true;
						} else {
							item.addCls("x-hidden-clip");
							item.setText("");
							return false;
						}
	},
	
	__asgnBtnHandler__: function(rec) {
		
		
						var ctrl = this._controller_;
						var frame = ctrl.getFrame();
						var win = frame._getWindow_("rolesAsgnWdw");
		
		                if( rec.get("type") === __FMBAS_TYPE__.WorkflowParamType._ASSIGN_ ){
		                    ctrl.setSelectedRecords([rec]);			
							var f = function() {
								win.show();
							}
							Ext.defer(f, 100, this);
		                }
	},
	
	_maskField_: function(val,meta,record) {
		
		
						var type = record.get("type");
						var maskedValue;
						var replaceCharacter = "*";
						if (type === "masked") {
							maskedValue = val.split("");
							var i = 0, l = maskedValue.length;
							for (i; i < l; i++) {
								maskedValue[i] = replaceCharacter;
							}
							val = maskedValue.toString().replace(/,/g,"");
						}
						return val;
	}
});
