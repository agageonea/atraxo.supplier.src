/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.UserSubsidiary_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_UserSubsidiary_Ds"
	},
	
	
	initRecord: function() {
		this.set("mainSubsidiary", false);
	},
	
	fields: [
		{name:"mainSubsidiary", type:"boolean"},
		{name:"userCode", type:"string"},
		{name:"userName", type:"string"},
		{name:"userId", type:"string"},
		{name:"subsidiaryId", type:"int", allowNull:true},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"officeCountryName", type:"string"},
		{name:"officeStateName", type:"string"},
		{name:"defaultVolumeUnitId", type:"int", allowNull:true},
		{name:"defaultVolumeUnitCode", type:"string"},
		{name:"defaultWeightUnitId", type:"int", allowNull:true},
		{name:"defaultWeightUnitCode", type:"string"},
		{name:"defaultCurrencyId", type:"int", allowNull:true},
		{name:"defaultCurrencyCode", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.UserSubsidiary_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"mainSubsidiary", type:"boolean", allowNull:true},
		{name:"userCode", type:"string"},
		{name:"userName", type:"string"},
		{name:"userId", type:"string"},
		{name:"subsidiaryId", type:"int", allowNull:true},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"officeCountryName", type:"string"},
		{name:"officeStateName", type:"string"},
		{name:"defaultVolumeUnitId", type:"int", allowNull:true},
		{name:"defaultVolumeUnitCode", type:"string"},
		{name:"defaultWeightUnitId", type:"int", allowNull:true},
		{name:"defaultWeightUnitCode", type:"string"},
		{name:"defaultCurrencyId", type:"int", allowNull:true},
		{name:"defaultCurrencyCode", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
