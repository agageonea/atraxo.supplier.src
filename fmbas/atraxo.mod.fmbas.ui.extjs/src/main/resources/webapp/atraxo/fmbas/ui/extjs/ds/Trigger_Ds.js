/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Trigger_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_Trigger_Ds"
	},
	
	
	validators: {
		type: [{type: 'presence'}],
		startDate: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("active", true);
	},
	
	fields: [
		{name:"type", type:"string"},
		{name:"startDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"intInDays", type:"int", allowNull:true},
		{name:"intInWeeks", type:"int", allowNull:true},
		{name:"daysOfMonth", type:"string"},
		{name:"daysOfWeek", type:"string"},
		{name:"months", type:"string"},
		{name:"weekOfMonth", type:"string"},
		{name:"onMonthDays", type:"boolean"},
		{name:"onWeekDays", type:"boolean"},
		{name:"repeatInterval", type:"string"},
		{name:"repeatDuration", type:"string"},
		{name:"timeout", type:"string"},
		{name:"active", type:"boolean"},
		{name:"timeReference", type:"boolean"},
		{name:"details", type:"string"},
		{name:"jobChainId", type:"int", allowNull:true},
		{name:"temp", type:"string", noFilter:true, noSort:true},
		{name:"days", type:"string", noFilter:true, noSort:true},
		{name:"weeks", type:"string", noFilter:true, noSort:true},
		{name:"monday", type:"boolean", noFilter:true, noSort:true},
		{name:"tuesday", type:"boolean", noFilter:true, noSort:true},
		{name:"wednesday", type:"boolean", noFilter:true, noSort:true},
		{name:"thursday", type:"boolean", noFilter:true, noSort:true},
		{name:"friday", type:"boolean", noFilter:true, noSort:true},
		{name:"saturday", type:"boolean", noFilter:true, noSort:true},
		{name:"sunday", type:"boolean", noFilter:true, noSort:true},
		{name:"repChk", type:"boolean", noFilter:true, noSort:true},
		{name:"timeoutChk", type:"boolean", noFilter:true, noSort:true},
		{name:"enable", type:"string", noFilter:true, noSort:true},
		{name:"time", type:"string", noFilter:true, noSort:true},
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Trigger_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"type", type:"string"},
		{name:"startDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"intInDays", type:"int", allowNull:true},
		{name:"intInWeeks", type:"int", allowNull:true},
		{name:"daysOfMonth", type:"string"},
		{name:"daysOfWeek", type:"string"},
		{name:"months", type:"string"},
		{name:"weekOfMonth", type:"string"},
		{name:"onMonthDays", type:"boolean", allowNull:true},
		{name:"onWeekDays", type:"boolean", allowNull:true},
		{name:"repeatInterval", type:"string"},
		{name:"repeatDuration", type:"string"},
		{name:"timeout", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"timeReference", type:"boolean", allowNull:true},
		{name:"details", type:"string"},
		{name:"jobChainId", type:"int", allowNull:true},
		{name:"temp", type:"string", noFilter:true, noSort:true},
		{name:"days", type:"string", noFilter:true, noSort:true},
		{name:"weeks", type:"string", noFilter:true, noSort:true},
		{name:"monday", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"tuesday", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"wednesday", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"thursday", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"friday", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"saturday", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"sunday", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"repChk", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"timeoutChk", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"enable", type:"string", noFilter:true, noSort:true},
		{name:"time", type:"string", noFilter:true, noSort:true},
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.fmbas.ui.extjs.ds.Trigger_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"daysOfMonthCombo", type:"string"},
		{name:"daysOfWeekCombo", type:"string"},
		{name:"monthsCombo", type:"string"},
		{name:"weekOfMonthCombo", type:"string"}
	]
});
