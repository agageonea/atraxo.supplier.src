/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Field_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_Field_Ds"
	},
	
	
	fields: [
		{name:"externalInterfaceId", type:"int", allowNull:true},
		{name:"externalInterfaceName", type:"string"},
		{name:"fieldName", type:"string"}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Field_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"externalInterfaceId", type:"int", allowNull:true},
		{name:"externalInterfaceName", type:"string"},
		{name:"fieldName", type:"string"}
	]
});
