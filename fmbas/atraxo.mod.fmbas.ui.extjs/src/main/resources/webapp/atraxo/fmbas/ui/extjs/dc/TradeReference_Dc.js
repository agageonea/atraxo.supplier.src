/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.TradeReference_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.TradeReference_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.TradeReference_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_TradeReference_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"companyName", dataIndex:"companyName", maxLength:100})
			.addTextField({ name:"contactPerson", dataIndex:"contactPerson", maxLength:100})
			.addBooleanField({ name:"isVerified", dataIndex:"isVerified"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.TradeReference_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_TradeReference_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"companyName", dataIndex:"companyName", width:150})
		.addTextColumn({ name:"companyAddress", dataIndex:"companyAddress", width:150})
		.addTextColumn({ name:"contactPerson", dataIndex:"contactPerson", width:150})
		.addTextColumn({ name:"contactNumber", dataIndex:"contactNumber", width:150})
		.addTextColumn({ name:"fax", dataIndex:"fax", width:150})
		.addTextColumn({ name:"email", dataIndex:"email", width:150})
		.addBooleanColumn({ name:"isVerified", dataIndex:"isVerified", width:100})
		.addTextColumn({ name:"verifiedBy", dataIndex:"modifiedBy", width:150})
		.addTextColumn({ name:"verificationNotes", dataIndex:"verificationNotes", width:150})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.TradeReference_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_TradeReference_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"companyName", bind:"{d.companyName}", dataIndex:"companyName", maxLength:100, labelWidth:125})
		.addTextArea({ name:"companyAddress", bind:"{d.companyAddress}", dataIndex:"companyAddress", labelWidth:125})
		.addTextField({ name:"contactPerson", bind:"{d.contactPerson}", dataIndex:"contactPerson", maxLength:100, labelWidth:125})
		.addTextField({ name:"contactNumber", bind:"{d.contactNumber}", dataIndex:"contactNumber", maxLength:100, labelWidth:125})
		.addTextField({ name:"fax", bind:"{d.fax}", dataIndex:"fax", maxLength:100, labelWidth:125})
		.addTextField({ name:"email", bind:"{d.email}", dataIndex:"email", maxLength:100, labelWidth:125})
		.addBooleanField({ name:"isVerified", bind:"{d.isVerified}", dataIndex:"isVerified", labelWidth:125})
		.addTextArea({ name:"verificationNotes", bind:"{d.verificationNotes}", dataIndex:"verificationNotes", labelWidth:125})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active", labelWidth:125})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["companyName", "companyAddress", "contactPerson", "contactNumber", "fax", "email", "isVerified", "verificationNotes"]);
	}
});
