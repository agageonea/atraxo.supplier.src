/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Kpi_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Kpi_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Kpi_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Kpi_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"glyphCode", dataIndex:"glyphCode", width:100, maxLength:32})
			.addCombo({ xtype:"combo", name:"glyphFont", dataIndex:"glyphFont", width:100, store:[ __FMBAS_TYPE__.glyphFont._FONTAWESOME_, __FMBAS_TYPE__.glyphFont._FINANCESOLID_]})
			.addLov({name:"frameTitle", dataIndex:"frameTitle", maxLength:255,
				editor:{xtype:"atraxo.ad.ui.extjs.lov.MenuItemKpiLov_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "frameId"} ]}})
			.addTextField({ name:"dsName", dataIndex:"dsName", allowBlank:false, width:120, maxLength:64})
			.addTextField({ name:"methodName", dataIndex:"methodName", allowBlank:false, width:120, maxLength:64})
			.addBooleanField({ name:"reverse", dataIndex:"reverse", allowBlank:false})
			.addTextField({ name:"unit", dataIndex:"unit", allowBlank:false, width:120, maxLength:10})
			.addTextField({ name:"title", dataIndex:"title", width:150, maxLength:255})
			.addTextField({ name:"titleEn", dataIndex:"titleEn", maxLength:255})
			.addTextField({ name:"titleDe", dataIndex:"titleDe", maxLength:255})
			.addTextField({ name:"titleJp", dataIndex:"titleJp", maxLength:255})
			.addTextField({ name:"titleEs", dataIndex:"titleEs", maxLength:255})
			.addTextField({ name:"titleFr", dataIndex:"titleFr", maxLength:255})
			.addTextField({ name:"titleAr", dataIndex:"titleAr", maxLength:255})
			.addNumberField({name:"upLimit", dataIndex:"upLimit", maxLength:10})
			.addNumberField({name:"downLimit", dataIndex:"downLimit", maxLength:10})
			.addTextField({ name:"description", dataIndex:"description", maxLength:1000})
			.addNumberField({name:"kpiOrder", dataIndex:"kpiOrder", maxLength:2})
			.addBooleanField({ name:"active", dataIndex:"active"})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Kpi_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_Kpi_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"glyphCode", dataIndex:"glyphCode", width:100, allowBlank: false, maxLength:32, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:32}})
		.addComboColumn({name:"glyphFont", dataIndex:"glyphFont", width:100, allowBlank: false, 
			editor:{xtype:"combo", mode: 'local', allowBlank:false, triggerAction:'all', forceSelection:true, store:[ __FMBAS_TYPE__.glyphFont._FONTAWESOME_, __FMBAS_TYPE__.glyphFont._FINANCESOLID_]}})
		.addLov({name:"frameTitle", dataIndex:"frameTitle", width:200, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"ad_MenuItemKpiLov_Lov", allowBlank:false, maxLength:255,
				retFieldMapping: [{lovField:"id", dsField: "frameId"} ]}})
		.addTextColumn({name:"dsName", dataIndex:"dsName", width:120, allowBlank: false, maxLength:64, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:64}})
		.addTextColumn({name:"methodName", dataIndex:"methodName", width:120, allowBlank: false, maxLength:64, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:64}})
		.addTextColumn({name:"unit", dataIndex:"unit", width:120, allowBlank: false, maxLength:10, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:10}})
		.addTextColumn({name:"title", dataIndex:"title", width:150, allowBlank: false, maxLength:255, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:255}})
		.addTextColumn({name:"titleEn", dataIndex:"titleEn", width:120, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"titleDe", dataIndex:"titleDe", hidden:true, width:120, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"titleJp", dataIndex:"titleJp", hidden:true, width:120, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"titleEs", dataIndex:"titleEs", hidden:true, width:120, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"titleFr", dataIndex:"titleFr", hidden:true, width:120, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"titleAr", dataIndex:"titleAr", hidden:true, width:120, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addNumberColumn({name:"upLimit", dataIndex:"upLimit", maxLength:10, align:"right" })
		.addNumberColumn({name:"downLimit", dataIndex:"downLimit", maxLength:10, align:"right" })
		.addNumberColumn({name:"kpiOrder", dataIndex:"kpiOrder", maxLength:2, align:"right" })
		.addTextColumn({name:"description", dataIndex:"description", width:200, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addBooleanColumn({name:"reverse", dataIndex:"reverse", allowBlank: false})
		.addBooleanColumn({name:"active", dataIndex:"active", allowBlank: false})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
