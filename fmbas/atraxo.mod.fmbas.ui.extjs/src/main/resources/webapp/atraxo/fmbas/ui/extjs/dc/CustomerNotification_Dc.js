/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.CustomerNotification_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.CustomerNotification_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.CustomerNotification_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_CustomerNotification_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"notificationEventName", maxLength:255})
			.addTextField({ name:"description", dataIndex:"notificationEventDescription", maxLength:255})
			.addTextField({ name:"template", dataIndex:"templateName", maxLength:100})
			.addLov({name:"assignedArea", dataIndex:"assignedAreaCode", allowBlank:false, width:180, maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.AreasLov_Lov", selectOnFocus:true, allowBlank:false, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "assignedAreaId"} ],
					filterFieldMapping: [{lovField:"active", value: "true"} ]}})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.CustomerNotification_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_CustomerNotification_Dc$List",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"notificationType", dataIndex:"notificationEventName", width:200, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_NotificationEvents_Lov", allowBlank:false,listeners:{
				fpchange:{scope:this, fn:function() {this._resetTemplateLov_()}}, buffer: Main.viewConfig.GRID_EDITOR_CHANGE_EVT_BUFFER
			}, maxLength:255,
				retFieldMapping: [{lovField:"id", dsField: "notificationEventId"} ,{lovField:"description", dsField: "notificationEventDescription"} ]}})
		.addTextColumn({name:"notificationEventDescription", dataIndex:"notificationEventDescription", width:200, noEdit: true, maxLength:255})
		.addLov({name:"templateName", dataIndex:"templateName", width:200, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_Templates_Lov", allowBlank:false, maxLength:100,
				retFieldMapping: [{lovField:"id", dsField: "templateId"} ,{lovField:"name", dsField: "templateName"} ],
				filterFieldMapping: [{lovField:"typeId", dsField: "notificationEventId"} ]}})
		.addLov({name:"emailBody", dataIndex:"emailBody", width:200, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_Templates_Lov", allowBlank:false, maxLength:100,
				retFieldMapping: [{lovField:"id", dsField: "emailBodyId"} ,{lovField:"name", dsField: "emailBody"} ],
				filterFieldMapping: [{lovField:"typeName", value: "e-mail body"} ]}})
		.addLov({name:"assignedArea", dataIndex:"assignedAreaCode", width:180, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_AreasLov_Lov", allowBlank:false, maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "assignedAreaId"} ],
				filterFieldMapping: [{lovField:"active", value: "true"} ]}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_resetTemplateLov_: function() {
		
		
						var dc = this._controller_;
						var r = dc.getRecord();
		
						if (r) {
							r.set("templateName","");
							r.set("templateId","");
						}
	}
});
