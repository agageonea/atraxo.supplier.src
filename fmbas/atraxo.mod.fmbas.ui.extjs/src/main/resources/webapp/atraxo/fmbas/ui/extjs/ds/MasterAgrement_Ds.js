/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.MasterAgrement_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_MasterAgrement_Ds"
	},
	
	
	validators: {
		validFrom: [{type: 'presence'}],
		currencyCode: [{type: 'presence'}],
		period: [{type: 'presence'}],
		status: [{type: 'presence'}],
		financialSourceCode: [{type: 'presence'}],
		avgMthd: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("evergreen", true);
	},
	
	fields: [
		{name:"percent", type:"string", noFilter:true, noSort:true},
		{name:"days", type:"string", noFilter:true, noSort:true},
		{name:"generalTermsTitle", type:"string", noFilter:true, noSort:true},
		{name:"paymentTermsTitle", type:"string", noFilter:true, noSort:true},
		{name:"creditTermsTitle", type:"string", noFilter:true, noSort:true},
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"companyId", type:"int", allowNull:true, noFilter:true},
		{name:"company", type:"string"},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"evergreen", type:"boolean"},
		{name:"renewalReminder", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"status", type:"string"},
		{name:"curencyId", type:"int", allowNull:true, noFilter:true},
		{name:"currencyCode", type:"string"},
		{name:"period", type:"string"},
		{name:"paymentTerms", type:"int", allowNull:true},
		{name:"referenceTo", type:"string"},
		{name:"invoiceFrequency", type:"string"},
		{name:"invoiceType", type:"string"},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceCode", type:"string"},
		{name:"avgMthdId", type:"int", allowNull:true, noFilter:true},
		{name:"avgMthd", type:"string"},
		{name:"creditCard", type:"boolean"},
		{name:"exposure", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"forex", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.MasterAgrement_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"percent", type:"string", noFilter:true, noSort:true},
		{name:"days", type:"string", noFilter:true, noSort:true},
		{name:"generalTermsTitle", type:"string", noFilter:true, noSort:true},
		{name:"paymentTermsTitle", type:"string", noFilter:true, noSort:true},
		{name:"creditTermsTitle", type:"string", noFilter:true, noSort:true},
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"companyId", type:"int", allowNull:true, noFilter:true},
		{name:"company", type:"string"},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"evergreen", type:"boolean", allowNull:true},
		{name:"renewalReminder", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"status", type:"string"},
		{name:"curencyId", type:"int", allowNull:true, noFilter:true},
		{name:"currencyCode", type:"string"},
		{name:"period", type:"string"},
		{name:"paymentTerms", type:"int", allowNull:true},
		{name:"referenceTo", type:"string"},
		{name:"invoiceFrequency", type:"string"},
		{name:"invoiceType", type:"string"},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceCode", type:"string"},
		{name:"avgMthdId", type:"int", allowNull:true, noFilter:true},
		{name:"avgMthd", type:"string"},
		{name:"creditCard", type:"boolean", allowNull:true},
		{name:"exposure", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"forex", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
