/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Notification_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_Notification_Ds"
	},
	
	
	fields: [
		{name:"title", type:"string"},
		{name:"descripion", type:"string"},
		{name:"priority", type:"string"},
		{name:"category", type:"string"},
		{name:"eventDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"lifeTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"action", type:"string"},
		{name:"link", type:"string"},
		{name:"methodName", type:"string"},
		{name:"methodParam", type:"string"},
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"userCode", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Notification_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"title", type:"string"},
		{name:"descripion", type:"string"},
		{name:"priority", type:"string"},
		{name:"category", type:"string"},
		{name:"eventDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"lifeTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"action", type:"string"},
		{name:"link", type:"string"},
		{name:"methodName", type:"string"},
		{name:"methodParam", type:"string"},
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"userCode", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
