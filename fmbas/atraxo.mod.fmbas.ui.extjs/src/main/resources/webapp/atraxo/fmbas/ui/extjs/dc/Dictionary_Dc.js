/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Dictionary_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Dictionary_Ds
});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Dictionary_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_Dictionary_Dc$List",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"fieldName", dataIndex:"fieldName", width:120, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_FieldLov_Lov", maxLength:250,
				retFieldMapping: [{lovField:"fieldName", dsField: "fieldName"} ],
				filterFieldMapping: [{lovField:"externalInterfaceId", dsField: "externalInterfaceId"} ]},  flex:1})
		.addTextColumn({name:"originalValue", dataIndex:"originalValue", width:120, maxLength:100,  flex:1, 
			editor: { xtype:"textfield", maxLength:100}})
		.addTextColumn({name:"newValue", dataIndex:"newValue", width:120, maxLength:100,  flex:1, 
			editor: { xtype:"textfield", maxLength:100}})
		.addTextColumn({name:"criteria", dataIndex:"criteria", width:50, noEdit: true, maxLength:2500,  flex:1})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
