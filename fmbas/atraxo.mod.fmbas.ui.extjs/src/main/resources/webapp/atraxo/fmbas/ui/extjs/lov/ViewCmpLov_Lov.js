/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.ViewCmpLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_ViewCmpLov_Lov",
	displayField: "cmp", 
	_columns_: ["cmp"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{cmp}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.fmbas.ui.extjs.ds.ViewCmpLov_Ds
});
