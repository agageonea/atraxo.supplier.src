/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.QuotationValue_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_QuotationValue_Ds"
	},
	
	
	fields: [
		{name:"quotId", type:"int", allowNull:true},
		{name:"quotName", type:"string"},
		{name:"methodName", type:"string"},
		{name:"validFromDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validToDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"isProvisioned", type:"boolean"},
		{name:"rate", type:"float", allowNull:true},
		{name:"active", type:"boolean"},
		{name:"decimals", type:"int", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.QuotationValue_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"quotId", type:"int", allowNull:true},
		{name:"quotName", type:"string"},
		{name:"methodName", type:"string"},
		{name:"validFromDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validToDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"isProvisioned", type:"boolean", allowNull:true},
		{name:"rate", type:"float", allowNull:true},
		{name:"active", type:"boolean", allowNull:true},
		{name:"decimals", type:"int", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
