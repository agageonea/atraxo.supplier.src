/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.PriceCategories_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_PriceCategories_Ds"
	},
	
	
	validators: {
		name: [{type: 'presence'}],
		active: [{type: 'presence'}],
		type: [{type: 'presence'}],
		mainCode: [{type: 'presence'}],
		iataCode: [{type: 'presence'}],
		pricePer: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("active", true);
		this.set("isMovement", true);
		this.set("isCso", true);
		this.set("isTransport", true);
		this.set("isPeriodical", true);
	},
	
	fields: [
		{name:"iataId", type:"int", allowNull:true},
		{name:"iataCode", type:"string"},
		{name:"mainId", type:"int", allowNull:true},
		{name:"mainCode", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"type", type:"string"},
		{name:"pricePer", type:"string"},
		{name:"isIndexBased", type:"boolean"},
		{name:"isSys", type:"boolean"},
		{name:"isMovement", type:"boolean"},
		{name:"isTransport", type:"boolean"},
		{name:"isPeriodical", type:"boolean"},
		{name:"isCso", type:"boolean"},
		{name:"active", type:"boolean"},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.PriceCategories_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"iataId", type:"int", allowNull:true},
		{name:"iataCode", type:"string"},
		{name:"mainId", type:"int", allowNull:true},
		{name:"mainCode", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"type", type:"string"},
		{name:"pricePer", type:"string"},
		{name:"isIndexBased", type:"boolean", allowNull:true},
		{name:"isSys", type:"boolean", allowNull:true},
		{name:"isMovement", type:"boolean", allowNull:true},
		{name:"isTransport", type:"boolean", allowNull:true},
		{name:"isPeriodical", type:"boolean", allowNull:true},
		{name:"isCso", type:"boolean", allowNull:true},
		{name:"active", type:"boolean", allowNull:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
