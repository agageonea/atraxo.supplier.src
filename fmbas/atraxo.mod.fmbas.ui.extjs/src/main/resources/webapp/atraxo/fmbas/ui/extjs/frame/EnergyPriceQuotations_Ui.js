/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.EnergyPriceQuotations_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.EnergyPriceQuotations_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("energyPrice", Ext.create(atraxo.fmbas.ui.extjs.dc.EnergyPrice_Dc,{}))
		.addDc("quotVal", Ext.create(atraxo.fmbas.ui.extjs.dc.QuotationValue_Dc,{}))
		.linkDc("quotVal", "energyPrice",{fetchMode:"auto",fields:[
					{childField:"quotId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdw1",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw1, scope:this})
		.addDcFilterFormView("energyPrice", {name:"energyPriceFilter", xtype:"fmbas_EnergyPrice_Dc$Filter"})
		.addDcGridView("energyPrice", {name:"energyPriceList", xtype:"fmbas_EnergyPrice_Dc$List"})
		.addDcFormView("energyPrice", {name:"energyPriceEdit", xtype:"fmbas_EnergyPrice_Dc$Edit"})
		.addDcGridView("quotVal", {name:"quotValEdit", xtype:"fmbas_QuotationValue_Dc$List"})
		.addDcFilterFormView("quotVal", {name:"quoteValFilter", xtype:"fmbas_QuotationValue_Dc$Filter"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["energyPriceList"], ["center"])
		.addChildrenTo("canvas2", ["energyPriceEdit", "quotValEdit"], ["north", "center"])
		.addToolbarTo("energyPriceList", "tlbEnergyPriceList")
		.addToolbarTo("energyPriceEdit", "tlbEnergyPriceEdit");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbEnergyPriceList", {dc: "energyPrice"})
			.addButtons([])
			.addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbEnergyPriceEdit", {dc: "energyPrice"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw1")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdw1
	 */
	,onHelpWdw1: function() {
		this.openHelp();
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/EnergyQuotations.html";
					window.open( url, "SONE_Help");
	}
});
