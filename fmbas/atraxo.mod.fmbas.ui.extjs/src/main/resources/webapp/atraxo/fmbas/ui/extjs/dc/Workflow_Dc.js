/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Workflow_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.Workflow_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.Workflow_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Workflow_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Workflow_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:64})
			.addLov({name:"subsidiary", dataIndex:"customerCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SubsidiaryLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "customerId"} ]}})
			.addTextField({ name:"workflowFile", dataIndex:"workflowFile", maxLength:64})
			.addTextField({ name:"processKey", dataIndex:"processKey", maxLength:64})
			.addTextField({ name:"description", dataIndex:"description", maxLength:64})
			.addCombo({ xtype:"combo", name:"deployStatus", dataIndex:"deployStatus", store:[ __FMBAS_TYPE__.WorkflowDeployStatus._DEPLOYED_, __FMBAS_TYPE__.WorkflowDeployStatus._NEW_]})
			.addNumberField({name:"deployVersion", dataIndex:"deployVersion", maxLength:10})
		;
	}

});

/* ================= EDIT-GRID: WorkFlowForm ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Workflow_Dc$WorkFlowForm", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_Workflow_Dc$WorkFlowForm",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"name", dataIndex:"name", width:120, noEdit: true, maxLength:64,  flex:2})
		.addLov({name:"subsidiary", dataIndex:"customerCode", width:50, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_SubsidiaryLov_Lov", maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "customerId"} ]},  flex:1})
		.addTextColumn({name:"workflowFile", dataIndex:"workflowFile", width:120, noEdit: true, maxLength:64,  flex:2})
		.addTextColumn({name:"processKey", dataIndex:"processKey", width:120, noEdit: true, maxLength:64,  flex:2})
		.addTextColumn({name:"description", dataIndex:"description", width:120, maxLength:64,  flex:4, 
			editor: { xtype:"textfield", maxLength:64}})
		.addComboColumn({name:"deployStatus", dataIndex:"deployStatus", width:70, noEdit: true,  flex:1})
		.addNumberColumn({name:"deployVersion", dataIndex:"deployVersion", noEdit: true, maxLength:10, align:"right",  flex:1 })
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: SelectSubsidiary ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Workflow_Dc$SelectSubsidiary", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Workflow_Dc$SelectSubsidiary",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"subsidiary", bind:"{p.subsidiary}", paramIndex:"subsidiary", allowBlank:false, noLabel: true, xtype:"fmbas_SubsidiaryLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"code", dsParam: "subsidiary"} ]})
		.addDisplayFieldText({ name:"subsidiaryLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:250, style:"float:right"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"table", layout: {type:"table"}})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["table"])
		.addChildrenTo("table", ["table1"])
		.addChildrenTo("table1", ["subsidiaryLabel", "subsidiary"]);
	}
});
