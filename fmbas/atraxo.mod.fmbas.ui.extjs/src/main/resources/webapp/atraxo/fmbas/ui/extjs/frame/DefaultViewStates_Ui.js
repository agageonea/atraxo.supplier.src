/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.DefaultViewStates_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.DefaultViewStates_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("defaultViewState", Ext.create(atraxo.fmbas.ui.extjs.dc.DefaultViewState_Dc,{multiEdit: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addDcFilterFormView("defaultViewState", {name:"defaultViewStateFilter", xtype:"fmbas_DefaultViewState_Dc$Filter"})
		.addDcEditGridView("defaultViewState", {name:"defaultViewStateList", xtype:"fmbas_DefaultViewState_Dc$List", frame:true})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["defaultViewStateList"], ["center"])
		.addToolbarTo("defaultViewStateList", "tlbDefaultViewStateList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbDefaultViewStateList", {dc: "defaultViewState"})
			.addQuery({iconCls:fp_asc.load_glyph.css,glyph:fp_asc.load_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addCopy({iconCls:fp_asc.copy_glyph.css,glyph:fp_asc.copy_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addReports()
		.end();
	}
	

});
