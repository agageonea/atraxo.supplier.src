/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.DocumentNumberSeries_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.DocumentNumberSeries_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("documentNumberSeries", Ext.create(atraxo.fmbas.ui.extjs.dc.DocumentNumberSeries_Dc,{multiEdit: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"helpInvWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpInvWdw, scope:this})
		.addButton({name:"enableInvNumSerie",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onEnableInvNumSerie,stateManager:[{ name:"selected_one_clean", dc:"documentNumberSeries", and: function() {return (this._enableIfDisabled_());} }], scope:this})
		.addButton({name:"disableInvNumSerie",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onDisableInvNumSerie,stateManager:[{ name:"selected_one_clean", dc:"documentNumberSeries", and: function() {return (this._enableIfEnabled_());} }], scope:this})
		.addDcFilterFormView("documentNumberSeries", {name:"docNumSerieFilter", xtype:"fmbas_DocumentNumberSeries_Dc$Filter"})
		.addDcEditGridView("documentNumberSeries", {name:"docNumSerieList", xtype:"fmbas_DocumentNumberSeries_Dc$List", frame:true})
		.addPanel({name:"main", layout:"card", activeItem:0}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["docNumSerieList"])
		.addToolbarTo("docNumSerieList", "tlbInvNumSerieList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbInvNumSerieList", {dc: "documentNumberSeries"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("enableInvNumSerie"),this._elems_.get("disableInvNumSerie"),this._elems_.get("helpInvWdw")])
			.addReports()
		.end();
	},
			
			/**
			 * Frame monitoring list definition
			 */
			_defineFrameMonList_: function() {
				this._getBuilder_()
				.addFrameMon("Customers_Ui",{_dcName_:"customer", _eventName_:"afterDoSaveSuccess", _fn_:this._reloadDc_})
				;
			}
	

	
	/**
	 * On-Click handler for button helpInvWdw
	 */
	,onHelpInvWdw: function() {
		this.openInvHelp();
	}
	
	/**
	 * On-Click handler for button enableInvNumSerie
	 */
	,onEnableInvNumSerie: function() {
		this._showWarningOnEnable_();
	}
	
	/**
	 * On-Click handler for button disableInvNumSerie
	 */
	,onDisableInvNumSerie: function() {
		var o={
			name:"disable",
			modal:true
		};
		this._getDc_("documentNumberSeries").doRpcData(o);
	}
	
	,_afterDefineDcs_: function() {
		
						var invNS = this._getDc_("documentNumberSeries");
		
						invNS.on("afterDoSaveSuccess", function(dc) {				
							this._applyStateAllButtons_();
							dc.doReloadPage();
						} , this );
	}
	
	,_enableIfDisabled_: function() {
		
						var dc = this._getDc_("documentNumberSeries");
						var r = dc.getRecord();
						var result = true;
						if (r) {
							var enabled = r.get("enabled");
							if (enabled == true) {
								result = false;
							}
						}
						return result;
	}
	
	,_enableIfEnabled_: function() {
		
						var dc = this._getDc_("documentNumberSeries");
						var r = dc.getRecord();
						var result = true;
						if (r) {
							var enabled = r.get("enabled");
							if (enabled == false) {
								result = false;
							}
						}
						return result;
	}
	
	,_enableRpc_: function(dc) {
		
						var successFn = function(dc) {
							dc.doReloadPage();
						}
						var o={
							name:"enable",
							modal:true,
							callbacks:{
								successFn: successFn,
								successScope: this
							}
						};
						dc.doRpcData(o);
	}
	
	,_showWarningOnEnable_: function() {
		
						var ds = "fmbas_DocumentNumberSeries_Ds";
						var alreadyEnabledCounter = 0;
						Ext.Ajax.request({
			            	url: Main.dsAPI(ds, "json").read,
			            	method: "POST",
			            	scope: this,
			            	success: function(response, opts) {
								var data = Ext.getResponseDataInJSON(response).data;
								var dc = this._getDc_("documentNumberSeries");
								Ext.each(data, function(item) {
									if (item.enabled ==  true) {
										alreadyEnabledCounter ++;
									}
								}, this);
								if (alreadyEnabledCounter > 0) {
									var context = this;
									var execFn = function() {
										context._enableRpc_(dc);
									}
									Main.warning("Other number series of the same type will be automatically disabled", null, execFn); 
								}
								else {
									this._enableRpc_(dc);
								}
							}
						});				
	}
	
	,_reloadDc_: function() {
		
						var dc = this._getDc_("documentNumberSeries");
						dc.doReloadPage();
	}
	
	,openInvHelp: function() {
		
					var url = Main.urlHelp+"/DocumentNumberSeries.html";
					window.open( url, "SONE_Help");
	}
});
