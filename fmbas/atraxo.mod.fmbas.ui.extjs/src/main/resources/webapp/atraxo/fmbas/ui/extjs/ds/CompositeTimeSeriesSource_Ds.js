/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.CompositeTimeSeriesSource_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_CompositeTimeSeriesSource_Ds"
	},
	
	
	fields: [
		{name:"timeSerieCompId", type:"int", allowNull:true},
		{name:"timeSerieId", type:"int", allowNull:true},
		{name:"timeSerieName", type:"string"},
		{name:"timeSerieDescription", type:"string"},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceCode", type:"string"},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"weighting", type:"int", allowNull:true},
		{name:"factor", type:"float", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.CompositeTimeSeriesSource_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"timeSerieCompId", type:"int", allowNull:true},
		{name:"timeSerieId", type:"int", allowNull:true},
		{name:"timeSerieName", type:"string"},
		{name:"timeSerieDescription", type:"string"},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceCode", type:"string"},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"weighting", type:"int", allowNull:true},
		{name:"factor", type:"float", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
