/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Subsidiary_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_Subsidiary_Ds"
	},
	
	
	validators: {
		assignedAreaCode: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("transmissionStatus", "Not available");
		this.set("processedStatus", "Not available");
		this.set("creditUpdateRequestStatus", "New");
		this.set("defaultVolumeUnitCode", _SYSTEMPARAMETERS_.sysvol);
		this.set("defaultWeightUnitCode", _SYSTEMPARAMETERS_.sysweight);
		this.set("assignedAreaCode", "WW");
	},
	
	fields: [
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"officeCountryId", type:"int", allowNull:true},
		{name:"officeCountryCode", type:"string"},
		{name:"officeCountryName", type:"string"},
		{name:"officeStateId", type:"int", allowNull:true},
		{name:"officeStateCode", type:"string"},
		{name:"officeStateName", type:"string"},
		{name:"assignedAreaId", type:"int", allowNull:true},
		{name:"assignedAreaCode", type:"string"},
		{name:"assignedAreaName", type:"string"},
		{name:"defaultVolumeUnitId", type:"int", allowNull:true},
		{name:"defaultVolumeUnitCode", type:"string"},
		{name:"defaultWeightUnitId", type:"int", allowNull:true},
		{name:"defaultWeightUnitCode", type:"string"},
		{name:"officeStreet", type:"string"},
		{name:"officeCity", type:"string"},
		{name:"officePostalCode", type:"string"},
		{name:"isSubsidiary", type:"boolean"},
		{name:"isCustomer", type:"boolean"},
		{name:"isSupplier", type:"boolean"},
		{name:"name", type:"string"},
		{name:"code", type:"string"},
		{name:"natureOfBusiness", type:"string"},
		{name:"status", type:"string"},
		{name:"type", type:"string"},
		{name:"iplAgent", type:"boolean"},
		{name:"fuelSupplier", type:"boolean"},
		{name:"fixedBaseOperator", type:"boolean"},
		{name:"accountNumber", type:"string"},
		{name:"phoneNo", type:"string"},
		{name:"faxNo", type:"string"},
		{name:"website", type:"string"},
		{name:"email", type:"string"},
		{name:"bankName", type:"string"},
		{name:"bankAddress", type:"string"},
		{name:"bankAccountNo", type:"string"},
		{name:"bankIbanNo", type:"string"},
		{name:"bankSwiftNo", type:"string"},
		{name:"timeZoneId", type:"int", allowNull:true},
		{name:"timeZoneName", type:"string"},
		{name:"subsidiaryCurrencyId", type:"int", allowNull:true},
		{name:"subsidiaryCurrencyCode", type:"string"},
		{name:"subsidiaryCurrencyActive", type:"boolean"},
		{name:"transmissionStatus", type:"string"},
		{name:"processedStatus", type:"string"},
		{name:"creditUpdateRequestStatus", type:"string"},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"dummyField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Subsidiary_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"officeCountryId", type:"int", allowNull:true},
		{name:"officeCountryCode", type:"string"},
		{name:"officeCountryName", type:"string"},
		{name:"officeStateId", type:"int", allowNull:true},
		{name:"officeStateCode", type:"string"},
		{name:"officeStateName", type:"string"},
		{name:"assignedAreaId", type:"int", allowNull:true},
		{name:"assignedAreaCode", type:"string"},
		{name:"assignedAreaName", type:"string"},
		{name:"defaultVolumeUnitId", type:"int", allowNull:true},
		{name:"defaultVolumeUnitCode", type:"string"},
		{name:"defaultWeightUnitId", type:"int", allowNull:true},
		{name:"defaultWeightUnitCode", type:"string"},
		{name:"officeStreet", type:"string"},
		{name:"officeCity", type:"string"},
		{name:"officePostalCode", type:"string"},
		{name:"isSubsidiary", type:"boolean", allowNull:true},
		{name:"isCustomer", type:"boolean", allowNull:true},
		{name:"isSupplier", type:"boolean", allowNull:true},
		{name:"name", type:"string"},
		{name:"code", type:"string"},
		{name:"natureOfBusiness", type:"string"},
		{name:"status", type:"string"},
		{name:"type", type:"string"},
		{name:"iplAgent", type:"boolean", allowNull:true},
		{name:"fuelSupplier", type:"boolean", allowNull:true},
		{name:"fixedBaseOperator", type:"boolean", allowNull:true},
		{name:"accountNumber", type:"string"},
		{name:"phoneNo", type:"string"},
		{name:"faxNo", type:"string"},
		{name:"website", type:"string"},
		{name:"email", type:"string"},
		{name:"bankName", type:"string"},
		{name:"bankAddress", type:"string"},
		{name:"bankAccountNo", type:"string"},
		{name:"bankIbanNo", type:"string"},
		{name:"bankSwiftNo", type:"string"},
		{name:"timeZoneId", type:"int", allowNull:true},
		{name:"timeZoneName", type:"string"},
		{name:"subsidiaryCurrencyId", type:"int", allowNull:true},
		{name:"subsidiaryCurrencyCode", type:"string"},
		{name:"subsidiaryCurrencyActive", type:"boolean", allowNull:true},
		{name:"transmissionStatus", type:"string"},
		{name:"processedStatus", type:"string"},
		{name:"creditUpdateRequestStatus", type:"string"},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"dummyField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
