/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.StepExecution_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.StepExecution_Ds
});

/* ================= GRID: StepExecutionList ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.StepExecution_Dc$StepExecutionList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_StepExecution_Dc$StepExecutionList",
	_noExport_: true,
	_noPaginator_: true,
	selType: null,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"startTime", dataIndex:"startTime", _mask_: Masks.DATETIME,  flex:1})
		.addDateColumn({ name:"endTime", dataIndex:"endTime", _mask_: Masks.DATETIME,  flex:1})
		.addTextColumn({ name:"status", dataIndex:"status", width:50,  flex:1})
		.addTextColumn({ name:"exitCode", dataIndex:"exitCode", width:50,  flex:1})
		.addTextColumn({ name:"exitMessage", dataIndex:"exitMessage", width:50,  flex:1})
		.addNumberColumn({ name:"readCount", dataIndex:"readCount",  flex:1})
		.addNumberColumn({ name:"writeCount", dataIndex:"writeCount",  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
