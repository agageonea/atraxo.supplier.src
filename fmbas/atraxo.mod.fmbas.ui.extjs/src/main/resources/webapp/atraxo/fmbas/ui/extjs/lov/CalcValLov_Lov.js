/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.CalcValLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_CalcValLov_Lov",
	displayField: "calcVal", 
	_columns_: [],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{calcVal}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.fmbas.ui.extjs.ds.CalcValLov_Ds
});
