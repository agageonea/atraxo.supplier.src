/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.ExternalInterfacesMessageHistory_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.ExternalInterfaceMessageHistory_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.ExternalInterfaceMessageHistory_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.ExternalInterfacesMessageHistory_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_ExternalInterfacesMessageHistory_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"externalInterfaceName", dataIndex:"externalInterfaceName", maxLength:100})
			.addTextField({ name:"objectType", dataIndex:"objectType", maxLength:100})
			.addTextField({ name:"objectRefId", dataIndex:"objectRefId", maxLength:100})
			.addDateField({name:"executionTime", dataIndex:"executionTime"})
			.addTextField({ name:"msgId", dataIndex:"messageId", maxLength:100})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __FMBAS_TYPE__.ExternalInterfaceMessageHistoryStatus._EXPORTED_, __FMBAS_TYPE__.ExternalInterfaceMessageHistoryStatus._FAILED_, __FMBAS_TYPE__.ExternalInterfaceMessageHistoryStatus._ACKNOWLEDGED_, __FMBAS_TYPE__.ExternalInterfaceMessageHistoryStatus._FAILED_TIMEOUT_, __FMBAS_TYPE__.ExternalInterfaceMessageHistoryStatus._FAILED_COMMUNICATION_, __FMBAS_TYPE__.ExternalInterfaceMessageHistoryStatus._FAILED_NEGATIVE_ACKNOWLEDGEMENT_, __FMBAS_TYPE__.ExternalInterfaceMessageHistoryStatus._SENT_]})
			.addDateField({name:"responseMessageTime", dataIndex:"responseMessageTime"})
			.addTextField({ name:"responseMessageId", dataIndex:"responseMessageId", maxLength:100})
		;
	}

});

/* ================= EDIT FORM: MessageContent ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ExternalInterfacesMessageHistory_Dc$MessageContent", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_ExternalInterfacesMessageHistory_Dc$MessageContent",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"sentMessageContent", bind:"{p.messageContent}", paramIndex:"messageContent", noEdit:true , width:700, noLabel: true, height:400})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col"])
		.addChildrenTo("col", ["sentMessageContent"]);
	}
});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ExternalInterfacesMessageHistory_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_ExternalInterfacesMessageHistory_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"externalInterfaceName", dataIndex:"externalInterfaceName", width:160})
		.addTextColumn({ name:"objectType", dataIndex:"objectType", width:120})
		.addTextColumn({ name:"objectRefId", dataIndex:"objectRefId", width:120})
		.addDateColumn({ name:"executionTime", dataIndex:"executionTime", width:160, _mask_: Masks.DATETIME})
		.addTextColumn({ name:"msgId", dataIndex:"messageId", width:350})
		.addDateColumn({ name:"responseMessageTime", dataIndex:"responseMessageTime", width:160, _mask_: Masks.DATETIME})
		.addTextColumn({ name:"responseMessageId", dataIndex:"responseMessageId", width:350})
		.addTextColumn({ name:"status", dataIndex:"status", width:70})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
