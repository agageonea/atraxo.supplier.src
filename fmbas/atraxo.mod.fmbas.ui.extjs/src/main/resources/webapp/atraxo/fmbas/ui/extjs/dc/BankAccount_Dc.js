/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.BankAccount_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.BankAccount_Ds
});

/* ================= EDIT-GRID: BankAccountList ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.BankAccount_Dc$BankAccountList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_BankAccount_Dc$BankAccountList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"accountNumber", dataIndex:"accountNumber", width:150, allowBlank: false, maxLength:64, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:64}})
		.addTextColumn({name:"accountHolder", dataIndex:"accountHolder", width:200, maxLength:64, 
			editor: { xtype:"textfield", maxLength:64}})
		.addTextColumn({name:"bankName", dataIndex:"bankName", width:200, maxLength:64, 
			editor: { xtype:"textfield", maxLength:64}})
		.addTextColumn({name:"bankAdress", dataIndex:"bankAdress", width:200, maxLength:200, 
			editor: { xtype:"textfield", maxLength:200}})
		.addTextColumn({name:"branchCode", dataIndex:"branchCode", width:150, maxLength:15, 
			editor: { xtype:"textfield", maxLength:15}})
		.addTextColumn({name:"branchName", dataIndex:"branchName", width:150, maxLength:50, 
			editor: { xtype:"textfield", maxLength:50}})
		.addTextColumn({name:"iban", dataIndex:"ibanCode", width:150, maxLength:64, 
			editor: { xtype:"textfield", maxLength:64}})
		.addTextColumn({name:"swift", dataIndex:"swiftCode", width:150, maxLength:20, 
			editor: { xtype:"textfield", maxLength:20}})
		.addLov({name:"currency", dataIndex:"currencyCode", width:100, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_CurrenciesLov_Lov", allowBlank:false, maxLength:3,
				retFieldMapping: [{lovField:"id", dsField: "currencyId"} ],
				filterFieldMapping: [{lovField:"active", value: "true"} ]}})
		.addLov({name:"subsidiary", dataIndex:"subsidiaryCode", width:100, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_SubsidiaryLov_Lov", allowBlank:false, maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "subsidiaryId"} ]}})
		.addTextColumn({name:"customerCodes", dataIndex:"customerCodes", width:150, noEdit: true, maxLength:250,  flex:1, renderer:function(value,meta) {return this._injectAssignButton_(value,meta);}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_injectAssignButton_: function(value,meta) {
		
		
						var id = Ext.id();
						meta.tdCls += "sone-cell-with-button";
						var ctx = this;
						var rec = meta.record;				
						
							Ext.defer(function () {	
							if( document.getElementById(id)){
						        Ext.widget("button", {
						            renderTo: id,
									rec: rec,
						            glyph: "xf067@FontAwesome",		
									cls: "sone-grid-assign-btn",				
						            handler: function (b) {							
										b.focus(true,100); 
										ctx.__asgnBtnHandler__(b.rec);
									}
						      });}
							}, 50);	
						return value+"<div class='sone-assign-button' id="+id+"></div>";			    
	},
	
	__asgnBtnHandler__: function(rec) {
		
						var ctrl = this._controller_;
						var frame= ctrl.getFrame();
						var bankAccountDc = frame._getDc_("bankAccount");
						if(!bankAccountDc.isDirty()){
							frame._showAsgnWindow_("atraxo.fmbas.ui.extjs.asgn.Bankaccount_Customer_Asgn$Ui" ,{dc: "bankAccount", objectIdField: "id"
								,listeners: {close: {scope: frame, fn: function() { ctrl.doReloadPage();
							}}}},frame);
						}
		         
	}
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.BankAccount_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_BankAccount_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"accountNumber", dataIndex:"accountNumber", maxLength:64})
			.addTextField({ name:"accountHolder", dataIndex:"accountHolder", maxLength:64})
			.addTextField({ name:"bankName", dataIndex:"bankName", maxLength:64})
			.addTextField({ name:"bankAdress", dataIndex:"bankAdress", maxLength:200})
			.addTextField({ name:"branchCode", dataIndex:"branchCode", maxLength:15})
			.addTextField({ name:"branchName", dataIndex:"branchName", maxLength:50})
			.addTextField({ name:"iban", dataIndex:"ibanCode", maxLength:64})
			.addTextField({ name:"swift", dataIndex:"swiftCode", maxLength:20})
			.addLov({name:"currency", dataIndex:"currencyCode", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"id", dsField: "currencyId"} ],
					filterFieldMapping: [{lovField:"active", value: "true"} ]}})
			.addLov({name:"subsidiary", dataIndex:"subsidiaryCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SubsidiaryLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "subsidiaryId"} ]}})
			.addTextField({ name:"customerCodes", dataIndex:"customerCodes", maxLength:250})
		;
	}

});
