/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.CustomerGroupLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_CustomerGroupLov_Ds"
	},
	
	
	fields: [
		{name:"id", type:"int", allowNull:true},
		{name:"code", type:"string"},
		{name:"type", type:"string"},
		{name:"isCustomer", type:"boolean"},
		{name:"parentGroupId", type:"int", allowNull:true},
		{name:"parentGroupCode", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.CustomerGroupLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"id", type:"int", allowNull:true},
		{name:"code", type:"string"},
		{name:"type", type:"string"},
		{name:"isCustomer", type:"boolean", allowNull:true},
		{name:"parentGroupId", type:"int", allowNull:true},
		{name:"parentGroupCode", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.fmbas.ui.extjs.ds.CustomerGroupLov_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerParentId", type:"int", allowNull:true}
	]
});
