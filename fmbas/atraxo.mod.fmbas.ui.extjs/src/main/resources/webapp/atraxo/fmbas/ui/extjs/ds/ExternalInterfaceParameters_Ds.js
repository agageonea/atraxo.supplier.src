/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ExternalInterfaceParameters_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_ExternalInterfaceParameters_Ds"
	},
	
	
	fields: [
		{name:"externalInterfaceId", type:"int", allowNull:true},
		{name:"externalInterfaceName", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"currentValue", type:"string"},
		{name:"defaultValue", type:"string"},
		{name:"type", type:"string"},
		{name:"refValues", type:"string"},
		{name:"refBusinessValues", type:"string"},
		{name:"readOnly", type:"boolean"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ExternalInterfaceParameters_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"externalInterfaceId", type:"int", allowNull:true},
		{name:"externalInterfaceName", type:"string"},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"currentValue", type:"string"},
		{name:"defaultValue", type:"string"},
		{name:"type", type:"string"},
		{name:"refValues", type:"string"},
		{name:"refBusinessValues", type:"string"},
		{name:"readOnly", type:"boolean", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
