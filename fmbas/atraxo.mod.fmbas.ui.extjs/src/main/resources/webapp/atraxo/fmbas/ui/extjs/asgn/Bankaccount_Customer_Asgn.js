/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

/* model */
Ext.define("atraxo.fmbas.ui.extjs.asgn.Bankaccount_Customer_Asgn$Model", {
	extend: 'Ext.data.Model',
	statics: {
		ALIAS: "fmbas_Bankaccount_Customer_Asgn"
	},
	fields:  [
		{name:"id",type:"string"},
		{name:"name",type:"string"},
		{name:"code",type:"string"}
	]
});

/* lists */
Ext.define( "atraxo.fmbas.ui.extjs.asgn.Bankaccount_Customer_Asgn$List", {
	extend: "e4e.asgn.AbstractAsgnGrid",
	alias:[ "widget.fmbas_Bankaccount_Customer_Asgn$Left","widget.fmbas_Bankaccount_Customer_Asgn$Right" ],
	_defineColumns_: function () {
		this._getBuilder_()
		.addTextColumn({name:"id", dataIndex:"id", hidden:true})
		.addTextColumn({name:"name", dataIndex:"name", width:120})
		.addTextColumn({name:"code", dataIndex:"code", width:40})
	} 
});
	
/* ui-window */
Ext.define("atraxo.fmbas.ui.extjs.asgn.Bankaccount_Customer_Asgn$Ui", {
	extend: "e4e.asgn.AbstractAsgnUi",
	width:800,
	height:400,
	title:"Assign customers to bank accounts ",
	_filterFields_: [
		["name"],
		["code"]
	],
	_defaultFilterField_ : "name",

	_defineElements_: function () {
		this._getBuilder_()
			.addLeftGrid({ xtype:"fmbas_Bankaccount_Customer_Asgn$Left"})
			.addRightGrid({ xtype:"fmbas_Bankaccount_Customer_Asgn$Right"})
	}
});

/* ui-panel */
Ext.define("atraxo.fmbas.ui.extjs.asgn.Bankaccount_Customer_Asgn$AsgnPanel", {
	extend: "e4e.asgn.AbstractAsgnPanel",
	alias: "widget.fmbas_Bankaccount_Customer_Asgn$AsgnPanel",
	width:800,
	height:400,
	title:"Assign customers to bank accounts ",
	_filterFields_: [
		["name"],
		["code"]
	],
	_defaultFilterField_ : "name",

	_defineElements_: function () {
		this._getBuilder_()
			.addLeftGrid({ xtype:"fmbas_Bankaccount_Customer_Asgn$Left"})
			.addRightGrid({ xtype:"fmbas_Bankaccount_Customer_Asgn$Right"})
	}
});
