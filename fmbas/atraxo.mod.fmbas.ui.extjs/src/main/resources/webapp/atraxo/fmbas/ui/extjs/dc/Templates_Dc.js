/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Templates_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.Templates_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.Templates_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Templates_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Templates_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:100})
			.addTextField({ name:"description", dataIndex:"description", maxLength:1000})
			.addTextField({ name:"fileName", dataIndex:"fileName", maxLength:100})
			.addLov({name:"typeName", dataIndex:"typeName", maxLength:255,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.NotificationEvents_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "typeId"} ]}})
			.addLov({name:"userName", dataIndex:"userName", maxLength:255,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.UserNameLov_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "userId"} ]}})
			.addDateField({name:"uploadDate", dataIndex:"uploadDate"})
			.addTextField({ name:"subsidiaryCode", dataIndex:"subsidiaryCode", maxLength:32})
			.addBooleanField({ name:"systemUse", dataIndex:"systemUse"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Templates_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Templates_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:120,  flex:1})
		.addTextColumn({ name:"description", dataIndex:"description", width:350})
		.addTextColumn({ name:"fileName", dataIndex:"fileName", width:120,  flex:1})
		.addTextColumn({ name:"typeName", dataIndex:"typeName", width:120,  flex:1})
		.addTextColumn({ name:"userName", dataIndex:"userName", width:200,  flex:1})
		.addBooleanColumn({ name:"systemUse", dataIndex:"systemUse",  flex:1})
		.addDateColumn({ name:"uploadDate", dataIndex:"uploadDate", hidden:true, width:110, _mask_: Masks.DATE})
		.addTextColumn({ name:"subsidiaryCode", dataIndex:"subsidiaryCode", hidden:true, width:110})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: New ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Templates_Dc$New", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Templates_Dc$New",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, maxLength:100})
		.addTextArea({ name:"description", bind:"{d.description}", dataIndex:"description"})
		.addLov({name:"typeName", bind:"{d.typeName}", dataIndex:"typeName", allowBlank:false, xtype:"fmbas_NotificationEvents_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "typeId"} ]})
		.addUploadField({ name:"file", bind:"{d.file}", dataIndex:"file", style:"display:none", listeners:{change: {scope: this,fn:function(e) { this._save_() }}}})
		.addToggleField({ name:"systemUse", bind:"{d.systemUse}", dataIndex:"systemUse", _enableFn_: this._canEditSystemUse_})
		.addHiddenField({ name:"isUpdate", bind:"{p.isUpdate}", paramIndex:"isUpdate"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col"])
		.addChildrenTo("col", ["name", "description", "typeName", "file", "systemUse", "isUpdate"]);
	},
	/* ==================== Business functions ==================== */
	
	_selectFile_: function() {
		
						var fileUploadField = this._get_("file");
						var uploadButton = fileUploadField.button;
						var elemId = fileUploadField.button.fileInputEl.dom.getAttribute("id");
						var elem = document.getElementById(elemId);
		
						elem.setAttribute("accept", ".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.txt,.xls,.xlsx,.html");
		
					   if(elem && document.createEvent) {
					      var evt = document.createEvent("MouseEvents");
					      evt.initEvent("click", true, false);
					      elem.dispatchEvent(evt);
					   }
	},
	
	_canEditSystemUse_: function() {
		
						var dc = this._controller_;
						var result = false; 
						getApplication().session.roles.forEach(function(element) {
						    if (element === "ADMIN") {
								result = true;
							}
						});
						return result;
	},
	
	_save_: function() {
		
						var me = this;
						var fileUploadField = this._get_("file");
						var uploadFieldValue = fileUploadField.getValue();
						var dc = this._controller_;
						var r = dc.getRecord();
		
						if (r) {
							var fileName = document.getElementById(fileUploadField.getId()+"-button-fileInputEl").files[0].name; // <-- here is the dirty hack
							r.set("fileName", fileName);
						}
		
						if (me.isValid() && !Ext.isEmpty(uploadFieldValue)) {
							this._doUploadFile_();
						}
	},
	
	_cancel_: function() {
		
						this.up("window").hide();
						this._controller_.doCancel();
	},
	
	_doUploadFile_: function() {
		
						var frame = this._controller_.getFrame();
						var window = frame._getWindow_("wdwNew");
						var form = window.items.get(0);
						var rd = this._controller_.record.data;
						var cfg = {
				                _handler_ : "uploadTemplate",
				                _succesCallbackScope_ : this._controller_,
								_succesCallbackFn_ : function() {
									this.store.commitChanges();
									this.doReloadPage();
									window.close();
								}
				        };
						Ext.apply(window,cfg);
						var formCfg = {
							fileUpload : true
						}
						Ext.apply(form,formCfg);
						if (form.getForm().isValid()) {
							Main.working();
							form.getForm().submit(
									{
										url : Main.urlUpload + "/" + window._handler_,
										scope : window,
										success : function(form, action) {
											try {
												Main.workingEnd();
											} catch (e) {
				
											}
											if (this._succesCallbackFn_ != null) {
												this._succesCallbackFn_.call(this._succesCallbackScope_ || this);
											}
										},
										failure : function(form, action) {
		
											try {
												Main.workingEnd();
											} catch (e) {
												
											}
											switch (action.failureType) {
											case Ext.form.Action.CLIENT_INVALID:
												Main.error("INVALID_FORM");
												break;
											case Ext.form.Action.CONNECT_FAILURE:
												Main.serverMessage(null, action.response);
												break;
											case Ext.form.Action.SERVER_INVALID:
												Main.serverMessage(null, action.response);
											}
										}
									});
						} else {
							Main.error("INVALID_FORM");
						}
		
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Templates_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Templates_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, maxLength:100})
		.addTextArea({ name:"description", bind:"{d.description}", dataIndex:"description"})
		.addUploadField({ name:"file", bind:"{d.file}", dataIndex:"file", style:"display:none", listeners:{change: {scope: this,fn:function(e) { this._save_() }}}})
		.addLov({name:"typeName", bind:"{d.typeName}", dataIndex:"typeName", noEdit:true , allowBlank:false, xtype:"fmbas_NotificationEvents_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "typeId"} ]})
		.addTextField({ name:"fileName", bind:"{d.fileName}", dataIndex:"fileName", noEdit:true , maxLength:100})
		.addToggleField({ name:"systemUse", bind:"{d.systemUse}", dataIndex:"systemUse", _enableFn_: this._canEditSystemUse_})
		.addHiddenField({ name:"isUpdate", bind:"{p.isUpdate}", paramIndex:"isUpdate"})
		.addHiddenField({ name:"templateId", bind:"{p.templateId}", paramIndex:"templateId"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col"])
		.addChildrenTo("col", ["name", "description", "typeName", "fileName", "file", "systemUse", "isUpdate", "templateId"]);
	},
	/* ==================== Business functions ==================== */
	
	_selectFile_: function() {
		
						var fileUploadField = this._get_("file");
						var uploadButton = fileUploadField.button;
						var elemId = fileUploadField.button.fileInputEl.dom.getAttribute("id");
						var elem = document.getElementById(elemId);
		
						elem.setAttribute("accept", ".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.txt,.xls,.xlsx");
		
					   if(elem && document.createEvent) {
					      var evt = document.createEvent("MouseEvents");
					      evt.initEvent("click", true, false);
					      elem.dispatchEvent(evt);
					   }
	},
	
	_save_: function() {
		
						var me = this;
						var fileUploadField = this._get_("file");
						var uploadFieldValue = fileUploadField.getValue();
						var dc = this._controller_;
						var r = dc.getRecord();
		
						if (r) {
							var fileName = document.getElementById(fileUploadField.getId()+"-button-fileInputEl").files[0].name; // <-- here is the dirty hack
							r.set("fileName", fileName);
						}
		
						if (me.isValid() && !Ext.isEmpty(uploadFieldValue)) {
							this._doUpdateFile_();
						}
	},
	
	_cancel_: function() {
		
						this.up("window").hide();
						this._controller_.doCancel();
	},
	
	_doUpdateFile_: function() {
		
						var frame = this._controller_.getFrame();
						var window = frame._getWindow_("wdwEdit");
						var form = window.items.get(0);
						var rd = this._controller_.record.data;
						var cfg = {
				                _handler_ : "uploadTemplate",
				                _succesCallbackScope_ : this._controller_,
								_succesCallbackFn_ : function() {
									this.store.commitChanges();
									this.doReloadPage();
									window.close();
								}
				        };
						Ext.apply(window,cfg);
						var formCfg = {
							fileUpload : true
						}
						Ext.apply(form,formCfg);
						if (form.getForm().isValid()) {
							Main.working();
							form.getForm().submit(
									{
										url : Main.urlUpload + "/" + window._handler_,
										scope : window,
										success : function(form, action) {
											try {
												Main.workingEnd();
											} catch (e) {
				
											}
											if (this._succesCallbackFn_ != null) {
												this._succesCallbackFn_.call(this._succesCallbackScope_ || this);
											}
										},
										failure : function(form, action) {
		
											try {
												Main.workingEnd();
											} catch (e) {
												
											}
											switch (action.failureType) {
											case Ext.form.Action.CLIENT_INVALID:
												Main.error("INVALID_FORM");
												break;
											case Ext.form.Action.CONNECT_FAILURE:
												Main.serverMessage(null, action.response);
												break;
											case Ext.form.Action.SERVER_INVALID:
												Main.serverMessage(null, action.response);
											}
										}
									});
						} else {
							Main.error("INVALID_FORM");
						}
		
	},
	
	_canEditSystemUse_: function() {
		
						var dc = this._controller_;
						var result = false; 
						getApplication().session.roles.forEach(function(element) {
						    if (element === "ADMIN") {
								result = true;
							}
						});
						return result;
	}
});
