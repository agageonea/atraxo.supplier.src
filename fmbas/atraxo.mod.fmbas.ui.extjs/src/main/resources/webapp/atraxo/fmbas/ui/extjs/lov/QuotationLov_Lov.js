/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.QuotationLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_QuotationLov_Lov",
	displayField: "name", 
	_columns_: ["name", "unitCode", "avgMethodIndicatorCode", "avgMethodIndicatorName"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{name}, {avgMethodIndicatorCode}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.fmbas.ui.extjs.ds.QuotationLov_Ds
});
