/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Commodities_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Commoditie_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Commodities_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Commodities_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"code", dataIndex:"code", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CommoditiesLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"code", dsField: "code"} ]}})
			.addTextField({ name:"name", dataIndex:"name", maxLength:100})
			.addDateField({name:"validFrom", dataIndex:"validFrom"})
			.addDateField({name:"validTo", dataIndex:"validTo"})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Commodities_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_Commodities_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"code", dataIndex:"code", width:100, allowBlank: false, maxLength:25, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:25}})
		.addTextColumn({name:"name", dataIndex:"name", width:120, maxLength:100, 
			editor: { xtype:"textfield", maxLength:100}})
		.addDateColumn({name:"validFrom", dataIndex:"validFrom", allowBlank: false, _mask_: Masks.DATE })
		.addDateColumn({name:"validTo", dataIndex:"validTo", allowBlank: false, _mask_: Masks.DATE })
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
