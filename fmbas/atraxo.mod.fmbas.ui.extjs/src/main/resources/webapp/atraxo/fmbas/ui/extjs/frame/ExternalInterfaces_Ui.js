/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.ExternalInterfaces_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.ExternalInterfaces_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("externalInterfaces", Ext.create(atraxo.fmbas.ui.extjs.dc.ExternalInterfaces_Dc,{}))
		.addDc("externalInterfacesHistory", Ext.create(atraxo.fmbas.ui.extjs.dc.ExternalInterfacesHistory_Dc,{}))
		.addDc("interfaceUsers", Ext.create(atraxo.fmbas.ui.extjs.dc.InterfaceUsers_Dc,{multiEdit: true}))
		.addDc("externalInterfaceParameters", Ext.create(atraxo.fmbas.ui.extjs.dc.ExternalInterfaceParameters_Dc,{multiEdit: true}))
		.addDc("dictionary", Ext.create(atraxo.fmbas.ui.extjs.dc.Dictionary_Dc,{multiEdit: true}))
		.addDc("condition", Ext.create(atraxo.fmbas.ui.extjs.dc.Condition_Dc,{multiEdit: true}))
		.linkDc("externalInterfacesHistory", "externalInterfaces",{fetchMode:"auto",fields:[
					{childField:"externalInterfaceId", parentField:"id"}]})
				.linkDc("interfaceUsers", "externalInterfaces",{fetchMode:"auto",fields:[
					{childField:"interfaceId", parentField:"id"}]})
				.linkDc("externalInterfaceParameters", "externalInterfaces",{fetchMode:"auto",fields:[
					{childField:"externalInterfaceId", parentField:"id"}]})
				.linkDc("dictionary", "externalInterfaces",{fetchMode:"auto",fields:[
					{childField:"externalInterfaceId", parentField:"id"}]})
				.linkDc("condition", "dictionary",{fetchMode:"auto",fields:[
					{childField:"dictionaryId", parentField:"id"}, {childField:"externalInterfaceId", parentField:"externalInterfaceId"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnHelpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onBtnHelpWdw, scope:this})
		.addButton({name:"btnEnable",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnEnable,stateManager:[{ name:"record_is_clean", dc:"externalInterfaces", and: function(dc) {return (dc.record.get('enabled') != true);} }], scope:this})
		.addButton({name:"btnDisable",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnDisable,stateManager:[{ name:"record_is_clean", dc:"externalInterfaces", and: function(dc) {return (dc.record.get('enabled') == true);} }], scope:this})
		.addButton({name:"btnViewResultHistory",glyph:fp_asc.eye_glyph.glyph, disabled:true, handler: this.onBtnViewResultHistory,stateManager:[{ name:"selected_one_clean", dc:"externalInterfacesHistory"}], scope:this})
		.addButton({name:"btnCloseLog",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseLog, scope:this})
		.addButton({name:"btnDeleteHistory",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteHistory,stateManager:[{ name:"selected_one_clean", dc:"externalInterfacesHistory"}], scope:this})
		.addButton({name:"btnEmptyHistory",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnEmptyHistory,stateManager:[{ name:"list_not_empty", dc:"externalInterfacesHistory"}], scope:this})
		.addButton({name:"btnCancelWithMenu",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"externalInterfaceParameters", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", handler: this.onBtnCancelSelected, scope:this})
		.addButton({name:"btnCancelAll",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", _isDefaultHandler_:true, handler: this.onBtnCancelAll, scope:this})
		.addButton({name:"btnConditions",glyph:fp_asc.filter_glyph.glyph, disabled:true, handler: this.onBtnConditions,stateManager:[{ name:"selected_one_clean", dc:"dictionary"}], scope:this})
		.addButton({name:"btnCloseConditions",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseConditions, scope:this})
		.addDcGridView("externalInterfaces", {name:"list", height:300, xtype:"fmbas_ExternalInterfaces_Dc$List"})
		.addDcFilterFormView("externalInterfaces", {name:"filter", xtype:"fmbas_ExternalInterfaces_Dc$Filter"})
		.addDcFilterFormView("externalInterfacesHistory", {name:"historyFilter", xtype:"fmbas_ExternalInterfacesHistory_Dc$Filter"})
		.addDcGridView("externalInterfacesHistory", {name:"historyList", _hasTitle_:true, height:350, xtype:"fmbas_ExternalInterfacesHistory_Dc$List"})
		.addDcEditGridView("interfaceUsers", {name:"notificationReceiversList", _hasTitle_:true, height:350, xtype:"fmbas_InterfaceUsers_Dc$List", frame:true})
		.addDcFormView("externalInterfacesHistory", {name:"historyErrorMsg", xtype:"fmbas_ExternalInterfacesHistory_Dc$Error"})
		.addDcEditGridView("externalInterfaceParameters", {name:"parameterList", _hasTitle_:true, height:350, xtype:"fmbas_ExternalInterfaceParameters_Dc$List", frame:true,  listeners:{ beforeedit: { scope: this, fn: function(editor, ctx) { this._setEditor_(editor, ctx, this._get_('parameterList'))}}}})
		.addDcEditGridView("dictionary", {name:"dictionaryList", _hasTitle_:true, height:350, xtype:"fmbas_Dictionary_Dc$List", frame:true})
		.addDcEditGridView("condition", {name:"conditionList", xtype:"fmbas_Condition_Dc$List", frame:true})
		.addWindow({name:"wdwErrorLog", _hasTitle_:true, width:725, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("historyErrorMsg")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCloseLog")]}]})
		.addWindow({name:"wdwConditions", _hasTitle_:true, width:500, height:300, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("conditionList")],  listeners:{ close:{fn:this.onConditionWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCloseConditions")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", height:300, xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas1", dcName:"externalInterfaceParameters"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["list", "detailsTab"], ["center", "south"])
		.addChildrenTo("detailsTab", ["parameterList"])
		.addToolbarTo("list", "tlbList")
		.addToolbarTo("historyList", "tlbHistoryList")
		.addToolbarTo("notificationReceiversList", "tlbNotificationReceivers")
		.addToolbarTo("parameterList", "tlbParameterList")
		.addToolbarTo("dictionaryList", "tlbDictionaryList")
		.addToolbarTo("conditionList", "tlbConditionList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("detailsTab", ["historyList", "notificationReceiversList", "dictionaryList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbList", {dc: "externalInterfaces"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnEnable"),this._elems_.get("btnDisable"),this._elems_.get("btnHelpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbHistoryList", {dc: "externalInterfacesHistory"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnViewResultHistory"),this._elems_.get("btnDeleteHistory"),this._elems_.get("btnEmptyHistory")])
			.addReports()
		.end()
		.beginToolbar("tlbNotificationReceivers", {dc: "interfaceUsers"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbParameterList", {dc: "externalInterfaceParameters"})
			.addButtons([])
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelWithMenu"),this._elems_.get("btnCancelSelected"),this._elems_.get("btnCancelAll")])
			.addReports()
		.end()
		.beginToolbar("tlbDictionaryList", {dc: "dictionary"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnConditions")])
			.addReports()
		.end()
		.beginToolbar("tlbConditionList", {dc: "condition"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnHelpWdw
	 */
	,onBtnHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnEnable
	 */
	,onBtnEnable: function() {
		var o={
			name:"setEnable",
			modal:true
		};
		this._getDc_("externalInterfaces").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnDisable
	 */
	,onBtnDisable: function() {
		var o={
			name:"setDissable",
			modal:true
		};
		this._getDc_("externalInterfaces").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnViewResultHistory
	 */
	,onBtnViewResultHistory: function() {
		this._getWindow_("wdwErrorLog").show();
	}
	
	/**
	 * On-Click handler for button btnCloseLog
	 */
	,onBtnCloseLog: function() {
		this.closeWdwErrorLog();
	}
	
	/**
	 * On-Click handler for button btnDeleteHistory
	 */
	,onBtnDeleteHistory: function() {
		this._getDc_("externalInterfacesHistory").doDelete();
	}
	
	/**
	 * On-Click handler for button btnEmptyHistory
	 */
	,onBtnEmptyHistory: function() {
		var successFn = function() {
			this._getDc_("externalInterfacesHistory").doReloadPage();
		};
		var o={
			name:"emptyHistory",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("externalInterfaces").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCancelSelected
	 */
	,onBtnCancelSelected: function() {
		this.fnCancelSelected();
	}
	
	/**
	 * On-Click handler for button btnCancelAll
	 */
	,onBtnCancelAll: function() {
		this.fnCancelAll();
	}
	
	/**
	 * On-Click handler for button btnConditions
	 */
	,onBtnConditions: function() {
		this._getWindow_("wdwConditions").show();
	}
	
	/**
	 * On-Click handler for button btnCloseConditions
	 */
	,onBtnCloseConditions: function() {
		this._getWindow_("wdwConditions").close();
		this._getDc_("condition").doCancel();
		this._getDc_("dictionary").doReloadPage();
	}
	
	,closeWdwErrorLog: function() {	
		this._getWindow_("wdwErrorLog").close();
	}
	
	,_setEditor_: function(editor,ctx,view) {
		
		
					var fieldIndex = ctx.field;
					var col = ctx.column;
					var cfg = {};
					var textEditor = {xtype:"textfield"};	
					var numberEditor = {xtype:"numberfield", minValue:0};	
					var passwordEditor = {
						xtype:"textfield", 
						cls: "sone-masked-field",
						inputType: Ext.isChrome ? "text" : "password",
					};
					var field = col.field;
					var storeData = [];
					var dateFormat = "mm/dd/yyy";
					
					var frame = this;
		
					if (fieldIndex == "currentValue") {
						var r = ctx.record;
						if (r) {
		
							var type = r.get("type");
							var refValues = r.get("refValues");
							var readOnly = r.get("readOnly");
							if (!Ext.isEmpty(type)) {
		
								/* CASE 1 - Type is _ENUMERATION_ */
		
								if (type == __FMBAS_TYPE__.JobParamType._ENUMERATION_) {
									cfg = {
										xtype: "combo",
							            selectOnFocus: true,
							            queryMode: "local",
							            store: refValues.split(","),
										readOnly: readOnly == true ? true : false
									};
								}
		
								/* CASE 2 - Type is _BOOLEAN_ */
		
								else if (type == __FMBAS_TYPE__.JobParamType._BOOLEAN_) {
									cfg = {
										xtype: "combo",
							            selectOnFocus: true,
							            queryMode: "local",
							            store: ["true","false"],
										readOnly: readOnly == true ? true : false
									};
								}
		
								/* CASE 3 - Type is _INTEGER_ */
		
								else if (type == __FMBAS_TYPE__.JobParamType._INTEGER_) {
									numberEditor.readOnly = readOnly == true ? true : false;
									cfg = numberEditor;
								}
		
								/* CASE 4 - Type is _MASKED_ */
		
								else if (type == __FMBAS_TYPE__.JobParamType._MASKED_) {
									passwordEditor.readOnly = readOnly == true ? true : false;
									cfg = passwordEditor;
								}
		
								else {
									textEditor.readOnly = readOnly == true ? true : false;
									cfg = textEditor;
								}
								col.setEditor(cfg);
							}
						}
					}
	}
	
	,_afterDefineDcs_: function() {
		
					var dictionary = this._getDc_("dictionary");
					var interfaceUsers = this._getDc_("interfaceUsers");
					var params = this._getDc_("externalInterfaceParameters");
					dictionary.on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						dc.doReloadPage();
					},this);
		
					interfaceUsers.beforeDoSave = function () {
		                var r = interfaceUsers.getRecord();
						var emailTemplateName = r.get("emailTemplateName");
						if(!emailTemplateName) {
							r.set("emailTemplateId",null);
							console.log("sets the id as empty");
						}
		
		            };
		
		
					
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/ExternalInterfaces.html";
					window.open( url, "SONE_Help");
	}
	
	,onConditionWdwClose: function() {
		
						var dc = this._getDc_("condition");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
						this._getDc_("dictionary").doReloadPage();
	}
	
	,fnCancelSelected: function() {
		
						var dc = this._getDc_("externalInterfaceParameters");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAll: function() {
		
						var dc = this._getDc_("externalInterfaceParameters");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isCancelEnabled(dc);
	}
});
