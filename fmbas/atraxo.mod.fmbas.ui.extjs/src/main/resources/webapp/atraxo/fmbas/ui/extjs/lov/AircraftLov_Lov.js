/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.AircraftLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_AircraftLov_Lov",
	displayField: "registration", 
	_columns_: ["registration", "acTypeCode"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{registration}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.fmbas.ui.extjs.ds.AircraftsLov_Ds
});
