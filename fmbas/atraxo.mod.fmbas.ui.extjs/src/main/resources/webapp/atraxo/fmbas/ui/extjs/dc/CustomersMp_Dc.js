/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.CustomersMp_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.CustomersMp_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.CustomersMp_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.CustomersMp_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_CustomersMp_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"code", dataIndex:"code", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32}})
			.addTextField({ name:"name", dataIndex:"name", maxLength:100})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __FMBAS_TYPE__.CustomerStatus._ACTIVE_, __FMBAS_TYPE__.CustomerStatus._INACTIVE_, __FMBAS_TYPE__.CustomerStatus._PROSPECT_, __FMBAS_TYPE__.CustomerStatus._BLOCKED_]})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.CustomersMp_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_CustomersMp_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:150})
		.addTextColumn({ name:"name", dataIndex:"name", width:300})
		.addTextColumn({ name:"business", dataIndex:"business", width:120})
		.addTextColumn({ name:"status", dataIndex:"status", width:100})
		.addTextColumn({ name:"contactName", dataIndex:"contactName", width:200})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: New ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.CustomersMp_Dc$New", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_CustomersMp_Dc$New",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"businessLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addCombo({ xtype:"combo", name:"business", bind:"{d.business}", dataIndex:"business", allowBlank:false, noLabel: true, store:[ __FMBAS_TYPE__.BusinessType._SUPPLIER_, __FMBAS_TYPE__.BusinessType._AIRLINE_],listeners:{
			change:{scope:this, fn:this.changeBusinessType}
		}})
		.addDisplayFieldText({ name:"nameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, width:180, noLabel: true, maxLength:100})
		.addDisplayFieldText({ name:"codeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", allowBlank:false, width:180, noLabel: true, maxLength:32})
		.addDisplayFieldText({ name:"iataCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-left:50px"})
		.addTextField({ name:"iataCode", bind:"{d.iataCode}", dataIndex:"iataCode", width:180, noLabel: true, maxLength:4})
		.addDisplayFieldText({ name:"icaoCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-left:50px"})
		.addTextField({ name:"icaoCode", bind:"{d.icaoCode}", dataIndex:"icaoCode", width:180, noLabel: true, maxLength:3})
		.addDisplayFieldText({ name:"phoneNumberLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-left:50px"})
		.addTextField({ name:"phoneNo", bind:"{d.phoneNo}", dataIndex:"phoneNo", width:180, noLabel: true, maxLength:100})
		.addDisplayFieldText({ name:"contactFirstNameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addTextField({ name:"contactFirstName", bind:"{p.contactFirstName}", paramIndex:"contactFirstName", allowBlank:false, width:180, noLabel: true, maxLength:64})
		.addDisplayFieldText({ name:"contactLastNameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addTextField({ name:"contactLastName", bind:"{p.contactLastName}", paramIndex:"contactLastName", allowBlank:false, width:180, noLabel: true, maxLength:64})
		.addDisplayFieldText({ name:"contactPhoneLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-left:50px"})
		.addTextField({ name:"contactPhone", bind:"{p.contactMobilePhone}", paramIndex:"contactMobilePhone", width:180, noLabel: true, maxLength:100})
		.addDisplayFieldText({ name:"contactEmailLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-left:50px"})
		.addTextField({ name:"contactEmail", bind:"{p.contactEmail}", paramIndex:"contactEmail", allowBlank:false, width:180, noLabel: true, maxLength:100})
		.addDisplayFieldText({ name:"primaryContactLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-top:10px; padding-bottom: 10px", colspan:2})
		.addDisplayFieldText({ name:"dummyLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-top: 58px; padding-bottom: 10px", colspan:2})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  style:"padding:10px"})
		.addPanel({ name:"tableContainer1", layout: {type:"table"}})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["tableContainer1"])
		.addChildrenTo("tableContainer1", ["table1", "table2"])
		.addChildrenTo("table1", ["businessLabel", "business", "nameLabel", "name", "codeLabel", "code", "primaryContactLabel", "contactFirstNameLabel", "contactFirstName", "contactLastNameLabel", "contactLastName"])
		.addChildrenTo("table2", ["iataCodeLabel", "iataCode", "icaoCodeLabel", "icaoCode", "dummyLabel", "contactPhoneLabel", "contactPhone", "contactEmailLabel", "contactEmail"]);
	},
	/* ==================== Business functions ==================== */
	
	changeBusinessType: function(field,newValue,oldValue) {
		
					var business = field.value ;
					if (newValue != oldValue) {
						var r = this._controller_.record;
						if(business === __FMBAS_TYPE__.BusinessType._SUPPLIER_){
							r.set("isSupplier", "true");
							r.set("fuelSupplier", "true");
							r.set("isCustomer", "false");
						} else if(business === __FMBAS_TYPE__.BusinessType._AIRLINE_) {
							r.set("isCustomer", "true");
							r.set("isSupplier", "false");
							r.set("fuelSupplier", "false");
						}
					}
	}
});

/* ================= EDIT FORM: EditRemark ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.CustomersMp_Dc$EditRemark", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_CustomersMp_Dc$EditRemark",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.remark}", paramIndex:"remark", width:400, labelAlign:"top", fieldStyle:"margin-top: 5px"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:400, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
						remarks.setReadOnly(false);
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.CustomersMp_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_CustomersMp_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", maxLength:100, cls:"sone-flat-field sone-large-flat-field"})
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", allowBlank:false, maxLength:32, cls:"sone-flat-kpi"})
		.addDateField({name:"dateOfInc", bind:"{d.dateOfInc}", dataIndex:"dateOfInc", cls:"sone-flat-kpi", hideTrigger:true, listeners:{render: {scope: this, fn: function(el) {this._showPickerOnClick_(el)}}}})
		.addDisplayFieldText({ name:"status", bind:"{d.status}", dataIndex:"status", maxLength:32})
		.addDisplayFieldText({ name:"dummyField", bind:"{d.dummyField}", dataIndex:"dummyField", maxLength:32, style:"display:none"})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("dummyField"),this._getConfig_("name")]})
		.addDisplayFieldText({ name:"businessLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addCombo({ xtype:"combo", name:"business", bind:"{d.business}", dataIndex:"business", allowBlank:false, width:140, noLabel: true, store:[ __FMBAS_TYPE__.BusinessType._SUPPLIER_, __FMBAS_TYPE__.BusinessType._AIRLINE_],listeners:{
			change:{scope:this, fn:this.changeBusinessType}
		}})
		.addDisplayFieldText({ name:"buyerNameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addLov({name:"buyerName", bind:"{d.buyerName}", dataIndex:"buyerName", width:140, noLabel: true, xtype:"fmbas_UserListLov_Lov", maxLength:255,
			retFieldMapping: [{lovField:"userCode", dsField: "buyerCode"} ,{lovField:"id", dsField: "buyerId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addDisplayFieldText({ name:"accountNumberLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addTextField({ name:"accountNumber", bind:"{d.accountNumber}", dataIndex:"accountNumber", width:140, noLabel: true, maxLength:32})
		.addDisplayFieldText({ name:"phoneNumberLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addTextField({ name:"phoneNo", bind:"{d.phoneNo}", dataIndex:"phoneNo", width:140, noLabel: true, maxLength:100})
		.addDisplayFieldText({ name:"faxNumberLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addTextField({ name:"faxNo", bind:"{d.faxNo}", dataIndex:"faxNo", width:140, noLabel: true, maxLength:100})
		.addDisplayFieldText({ name:"websiteLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addTextField({ name:"website", bind:"{d.website}", dataIndex:"website", width:140, noLabel: true, maxLength:100})
		.addDisplayFieldText({ name:"iataCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addTextField({ name:"iataCode", bind:"{d.iataCode}", dataIndex:"iataCode", width:60, hideLabel:"true", maxLength:4, enforceMaxLength:true})
		.addDisplayFieldText({ name:"icaoCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addTextField({ name:"icaoCode", bind:"{d.icaoCode}", dataIndex:"icaoCode", width:60, hideLabel:"true", maxLength:3, enforceMaxLength:true})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"titleAndKpi",  style:"margin-bottom:10px", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"title", width:300,  cls:"sone-title-panel", layout:"anchor"})
		.addPanel({ name:"p3",  cls:"sone-kpi-panel", style:"float:right", layout: {type:"table"}})
		.addPanel({ name:"c1", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c2", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c3", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"mainTable1",  style:"margin-bottom:10px", layout: {type:"table"}})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left:30px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3",  style:"margin-left:30px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table4",  style:"margin-left:30px", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["titleAndKpi", "mainTable1"])
		.addChildrenTo("titleAndKpi", ["title", "p3"])
		.addChildrenTo("title", ["row1"])
		.addChildrenTo("p3", ["c1", "c2", "c3"])
		.addChildrenTo("c1", ["code"])
		.addChildrenTo("c2", ["dateOfInc"])
		.addChildrenTo("c3", ["status"])
		.addChildrenTo("mainTable1", ["table1", "table2", "table3", "table4"])
		.addChildrenTo("table1", ["businessLabel", "business", "buyerNameLabel", "buyerName"])
		.addChildrenTo("table2", ["accountNumberLabel", "accountNumber", "phoneNumberLabel", "phoneNo"])
		.addChildrenTo("table3", ["faxNumberLabel", "faxNo", "websiteLabel", "website"])
		.addChildrenTo("table4", ["iataCodeLabel", "iataCode", "icaoCodeLabel", "icaoCode"]);
	},
	/* ==================== Business functions ==================== */
	
	_showPickerOnClick_: function(el) {
		
						el.inputCell.on("click",function(){  
			                el.onTriggerClick();
			            });
	},
	
	_afterDefineElements_: function() {
		
						this._getBuilder_().change("titleAndKpi",{
							layout: {
						        type: "hbox",
						        align: "stretch"
						    },
						    defaults: {
						        flex: 1,
								height: 60
						    }
						});
	},
	
	changeBusinessType: function(field,newValue,oldValue) {
		
					var business = field.value ;
					if (newValue != oldValue) {
						var r = this._controller_.record;
						if(business === __FMBAS_TYPE__.BusinessType._SUPPLIER_){
							r.set("isSupplier", "true");
							r.set("fuelSupplier", "true");
							r.set("isCustomer", "false");
						} else if(business === __FMBAS_TYPE__.BusinessType._AIRLINE_) {
							r.set("isCustomer", "true");
							r.set("isSupplier", "false");
							r.set("fuelSupplier", "false");
						}
					}
	}
});

/* ================= EDIT FORM: Addresses ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.CustomersMp_Dc$Addresses", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_CustomersMp_Dc$Addresses",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"streetLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"cityLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"stateProvinceLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"zipPostalCodeLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"countryLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"streetLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"cityLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"stateProvinceLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"zipPostalCodeLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"countryLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addTextField({ name:"officeStreet", bind:"{d.officeStreet}", dataIndex:"officeStreet", width:180, noLabel: true, maxLength:50})
		.addTextField({ name:"officeCity", bind:"{d.officeCity}", dataIndex:"officeCity", width:180, noLabel: true, maxLength:100})
		.addLov({name:"officeStateName", bind:"{d.officeStateName}", dataIndex:"officeStateName", _enableFn_: function(dc, rec) { return !Ext.isEmpty(dc.record.data.officeCountryId); } , width:180, noLabel: true, xtype:"fmbas_CountryLov_Lov", maxLength:100, listeners:{expand: {scope: this, fn: function(el) {this._filterByOfficeCountry_(el)}}}, pageSize:0,
			retFieldMapping: [{lovField:"id", dsField: "officeStateId"} ,{lovField:"name", dsField: "officeStateName"} ,{lovField:"code", dsField: "officeStateCode"} ]})
		.addTextField({ name:"officePostalCode", bind:"{d.officePostalCode}", dataIndex:"officePostalCode", width:180, noLabel: true, maxLength:32})
		.addLov({name:"officeCountryName", bind:"{d.officeCountryName}", dataIndex:"officeCountryName", width:180, noLabel: true, xtype:"fmbas_CountryNameLov_Lov", maxLength:100, _localCache_:true,
			retFieldMapping: [{lovField:"id", dsField: "officeCountryId"} ,{lovField:"code", dsField: "officeCountryCode"} ,{lovField:"name", dsField: "officeCountryName"} ],listeners:{
			change:{scope:this, fn:this._clearOfficeStateValue_}
		}})
		.addTextField({ name:"billingStreet", bind:"{d.billingStreet}", dataIndex:"billingStreet", width:180, noLabel: true, maxLength:50})
		.addTextField({ name:"billingCity", bind:"{d.billingCity}", dataIndex:"billingCity", width:180, noLabel: true, maxLength:100})
		.addLov({name:"billingStateName", bind:"{d.billingStateName}", dataIndex:"billingStateName", _enableFn_: function(dc, rec) { return !Ext.isEmpty(dc.record.data.billingCountryId); } , width:180, noLabel: true, xtype:"fmbas_CountryLov_Lov", maxLength:100, listeners:{expand: {scope: this, fn: function(el) {this._filterByBillingCountry_(el)}}}, pageSize:0,
			retFieldMapping: [{lovField:"id", dsField: "billingStateId"} ,{lovField:"code", dsField: "billingStateCode"} ,{lovField:"name", dsField: "billingStateName"} ]})
		.addTextField({ name:"billingPostalCode", bind:"{d.billingPostalCode}", dataIndex:"billingPostalCode", width:180, noLabel: true, maxLength:32})
		.addLov({name:"billingCountryName", bind:"{d.billingCountryName}", dataIndex:"billingCountryName", width:180, noLabel: true, xtype:"fmbas_CountryNameLov_Lov", maxLength:100, _localCache_:true,
			retFieldMapping: [{lovField:"id", dsField: "billingCountryId"} ,{lovField:"code", dsField: "billingCountryCode"} ,{lovField:"name", dsField: "billingCountryName"} ],listeners:{
			change:{scope:this, fn:this._clearBillingStateValue_}
		}})
		.addDisplayFieldText({ name:"officeAddressLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-top:10px; padding-bottom: 10px", colspan:2})
		.addDisplayFieldText({ name:"billingAddressLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-top:10px; padding-bottom: 10px", colspan:2})
		.addBooleanField({ name:"useForBilling", bind:"{d.useForBilling}", dataIndex:"useForBilling", noLabel: true})
		.addDisplayFieldText({ name:"useForBillingLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		/* =========== containers =========== */
		.addPanel({ name:"officeAddressTable", layout: {type:"table", columns:2}})
		.addPanel({ name:"billingAddressTable",  style:"margin-left:50px", layout: {type:"table", columns:2}})
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"addressTabContainer", layout: {type:"table"}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("officeAddressTable", ["officeAddressLabel", "streetLabel1", "officeStreet", "cityLabel1", "officeCity", "countryLabel1", "officeCountryName", "zipPostalCodeLabel1", "officePostalCode", "stateProvinceLabel1", "officeStateName", "useForBillingLabel", "useForBilling"])
		.addChildrenTo("billingAddressTable", ["billingAddressLabel", "streetLabel2", "billingStreet", "cityLabel2", "billingCity", "countryLabel2", "billingCountryName", "zipPostalCodeLabel2", "billingPostalCode", "stateProvinceLabel2", "billingStateName"])
		.addChildrenTo("main", ["addressTabContainer"])
		.addChildrenTo("addressTabContainer", ["officeAddressTable", "billingAddressTable"]);
	},
	/* ==================== Business functions ==================== */
	
	_clearOfficeStateValue_: function() {
		
						var f = function(){ 
							var dc = this._controller_;
							var r = dc.getRecord();
							var officeCountryName = this._get_("officeCountryName").getValue();
							var officeStateName = this._get_("officeStateName");
			
							if (Ext.isEmpty(officeCountryName)) {
								officeStateName.setReadOnly(true);
								if (r) {
									r.set("officeStateId","");
									r.set("officeStateCode","");
									r.set("officeStateName","");
								}
							}
							else {
								officeStateName.setReadOnly(false);
								if (r && r.dirty == true) {
									if ("officeCountryId" in r.modified) {
										r.set("officeStateId","");
										r.set("officeStateCode","");
										r.set("officeStateName","");
									}
								}
							}
						};
						Ext.defer(f, 100, this);
	},
	
	_clearBillingStateValue_: function() {
		
		
						var f = function(){ 
							var dc = this._controller_;
							var r = dc.getRecord();
							var billingCountryName = this._get_("billingCountryName").getValue();
							var billingStateName = this._get_("billingStateName");
			
							if (Ext.isEmpty(billingCountryName)) {
								billingStateName.setReadOnly(true);
								if (r) {
									r.set("billingStateId","");
									r.set("billingStateCode","");
									r.set("billingStateName","");
								}
							}
							else {
								billingStateName.setReadOnly(false);
								if (r && r.dirty == true) {
									if ("billingCountryId" in r.modified) {
										r.set("billingStateId","");
										r.set("billingStateCode","");
										r.set("billingStateName","");
									}
								}
							}
						}
						Ext.defer(f, 100, this);
	},
	
	_filterByBillingCountry_: function(combo) {
		
		
						var dc = this._controller_;
						var r = dc.getRecord();
						var store = combo.store;
		
						store.clearFilter();
		
						if (r) {
							var billingCountryId = r.get("billingCountryId");
							if (!Ext.isEmpty(billingCountryId)) {
								store.filterBy(function (record) {
			                        if (record.get("countryId") == billingCountryId) {
										return record;
									}
			                    });
							}
						}
						
						
	},
	
	_filterByOfficeCountry_: function(combo) {
		
		
						var dc = this._controller_;
						var r = dc.getRecord();
						var store = combo.store;
		
						store.clearFilter();
		
						if (r) {
							var officeCountryId = r.get("officeCountryId");
							if (!Ext.isEmpty(officeCountryId)) {
								store.filterBy(function (record) {
			                        if (record.get("countryId") == officeCountryId) {
										return record;
									}
			                    });
							}
						}
						
						
	}
});
