/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.SystemParameters_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_SystemParameters_Ds"
	},
	
	
	fields: [
		{name:"code", type:"string"},
		{name:"value", type:"string"},
		{name:"sysUnits", type:"string", noFilter:true, noSort:true},
		{name:"sysLenght", type:"string", noFilter:true, noSort:true},
		{name:"sysVol", type:"string", noFilter:true, noSort:true},
		{name:"sysWeight", type:"string", noFilter:true, noSort:true},
		{name:"sysDensity", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"sysCurrency", type:"string", noFilter:true, noSort:true},
		{name:"sysAvgMthd", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.SystemParameters_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"code", type:"string"},
		{name:"value", type:"string"},
		{name:"sysUnits", type:"string", noFilter:true, noSort:true},
		{name:"sysLenght", type:"string", noFilter:true, noSort:true},
		{name:"sysVol", type:"string", noFilter:true, noSort:true},
		{name:"sysWeight", type:"string", noFilter:true, noSort:true},
		{name:"sysDensity", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"sysCurrency", type:"string", noFilter:true, noSort:true},
		{name:"sysAvgMthd", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
