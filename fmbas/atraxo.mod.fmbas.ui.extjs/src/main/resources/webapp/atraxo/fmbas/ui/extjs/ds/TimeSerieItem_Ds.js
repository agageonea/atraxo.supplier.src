/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.TimeSerieItem_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_TimeSerieItem_Ds"
	},
	
	
	validators: {
		tserie: [{type: 'presence'}],
		itemdate: [{type: 'presence'}],
		itemValue: [{type: 'presence'}],
		calculated: [{type: 'presence'}],
		transfred: [{type: 'presence'}],
		transferedItemUpdate: [{type: 'presence'}],
		update: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("active", true);
	},
	
	fields: [
		{name:"tserie", type:"int", allowNull:true},
		{name:"serieName", type:"string"},
		{name:"serieStatus", type:"string"},
		{name:"itemdate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"itemValue", type:"float", allowNull:true},
		{name:"calculated", type:"boolean"},
		{name:"transfred", type:"boolean"},
		{name:"transferedItemUpdate", type:"boolean"},
		{name:"update", type:"boolean"},
		{name:"active", type:"boolean"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.TimeSerieItem_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"tserie", type:"int", allowNull:true},
		{name:"serieName", type:"string"},
		{name:"serieStatus", type:"string"},
		{name:"itemdate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"itemValue", type:"float", allowNull:true},
		{name:"calculated", type:"boolean", allowNull:true},
		{name:"transfred", type:"boolean", allowNull:true},
		{name:"transferedItemUpdate", type:"boolean", allowNull:true},
		{name:"update", type:"boolean", allowNull:true},
		{name:"active", type:"boolean", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
