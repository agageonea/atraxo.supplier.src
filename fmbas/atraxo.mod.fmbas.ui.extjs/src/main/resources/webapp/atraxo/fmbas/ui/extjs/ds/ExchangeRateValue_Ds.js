/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ExchangeRateValue_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_ExchangeRateValue_Ds"
	},
	
	
	fields: [
		{name:"exchangeRateId", type:"int", allowNull:true},
		{name:"currency1Id", type:"int", allowNull:true},
		{name:"currency1Code", type:"string"},
		{name:"currency2Id", type:"int", allowNull:true},
		{name:"currency2Code", type:"string"},
		{name:"vldFrDtRef", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"vldToDtRef", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"isPrvsn", type:"boolean"},
		{name:"rate", type:"float", allowNull:true},
		{name:"postTypeInd", type:"int", allowNull:true},
		{name:"active", type:"boolean"},
		{name:"perRate", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"methodName", type:"string"},
		{name:"longMethodName", type:"string"},
		{name:"decimals", type:"int", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ExchangeRateValue_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"exchangeRateId", type:"int", allowNull:true},
		{name:"currency1Id", type:"int", allowNull:true},
		{name:"currency1Code", type:"string"},
		{name:"currency2Id", type:"int", allowNull:true},
		{name:"currency2Code", type:"string"},
		{name:"vldFrDtRef", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"vldToDtRef", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"isPrvsn", type:"boolean", allowNull:true},
		{name:"rate", type:"float", allowNull:true},
		{name:"postTypeInd", type:"int", allowNull:true},
		{name:"active", type:"boolean", allowNull:true},
		{name:"perRate", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"methodName", type:"string"},
		{name:"longMethodName", type:"string"},
		{name:"decimals", type:"int", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
