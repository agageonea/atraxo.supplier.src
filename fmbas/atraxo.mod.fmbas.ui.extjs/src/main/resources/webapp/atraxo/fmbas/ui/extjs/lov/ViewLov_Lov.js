/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.ViewLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_ViewLov_Lov",
	displayField: "name", 
	_columns_: ["name", "cmp"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{name}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.fmbas.ui.extjs.ds.ViewStateRt_Ds
});
