/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.PriceCategories_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.PriceCategories_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("priceCategory", Ext.create(atraxo.fmbas.ui.extjs.dc.PriceCategories_Dc,{trackEditMode: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnClosePriceWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnClosePriceWdw, scope:this})
		.addButton({name:"btnSaveClosePriceWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveClosePriceWdw, scope:this})
		.addButton({name:"btnSaveNewWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewWdw, scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addDcFilterFormView("priceCategory", {name:"priceCategoryFilter", xtype:"fmbas_PriceCategories_Dc$Filter"})
		.addDcGridView("priceCategory", {name:"priceCategoryList", xtype:"fmbas_PriceCategories_Dc$List"})
		.addDcFormView("priceCategory", {name:"priceCategoryNew", xtype:"fmbas_PriceCategories_Dc$EditForm"})
		.addDcFormView("priceCategory", {name:"priceCategoryEdit", xtype:"fmbas_PriceCategories_Dc$EditForm"})
		.addWindow({name:"wdwPriceCategory", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("priceCategoryNew")],  listeners:{ close:{fn:this.onWdwPriceCategoryClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveClosePriceWdw"), this._elems_.get("btnSaveNewWdw"), this._elems_.get("btnClosePriceWdw")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["priceCategoryList"], ["center"])
		.addChildrenTo("canvas2", ["priceCategoryEdit"], ["center"])
		.addToolbarTo("priceCategoryList", "tlbPriceCategoryList")
		.addToolbarTo("priceCategoryEdit", "tlbPriceCategoryContext");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbPriceCategoryList", {dc: "priceCategory"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbPriceCategoryContext", {dc: "priceCategory"})
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnClosePriceWdw
	 */
	,onBtnClosePriceWdw: function() {
		this.onWdwPriceCategoryClose();
		this._getWindow_("wdwPriceCategory").close();
	}
	
	/**
	 * On-Click handler for button btnSaveClosePriceWdw
	 */
	,onBtnSaveClosePriceWdw: function() {
		this.saveClosePrice();
		this._getDc_("priceCategory").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSaveNewWdw
	 */
	,onBtnSaveNewWdw: function() {
		this.saveNew();
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	,showPriceCategoryWdw: function() {	
		this._getWindow_("wdwPriceCategory").show();
	}
	
	,hidePriceCategoryWdw: function() {	
		this._getWindow_("wdwPriceCategory").close();
	}
	
	,_afterDefineDcs_: function() {
		 
					var priceCategory = this._getDc_("priceCategory");
					
					priceCategory.on("afterDoNew", function (dc) {
						dc.setEditMode();
						this.showPriceCategoryWdw();
					}, this);
		
					this._getDc_("priceCategory").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwPriceCategory").close();
						}
					}, this);
		
	}
	
	,saveClosePrice: function() {
		
					var priceCategory = this._getDc_("priceCategory");
					priceCategory.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveNew: function() {
		
					var btnSaveNew = this._getDc_("priceCategory");
					btnSaveNew.doSave({doNewAfterSave:true});
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/PriceCategories.html";
					window.open( url, "SONE_Help");
	}
	
	,onWdwPriceCategoryClose: function() {
		
						var dc = this._getDc_("priceCategory");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
});
