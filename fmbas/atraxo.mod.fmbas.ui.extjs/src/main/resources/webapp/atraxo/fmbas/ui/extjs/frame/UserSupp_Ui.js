/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.UserSupp_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.UserSupp_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("usr", Ext.create(atraxo.fmbas.ui.extjs.dc.User_Dc,{trackEditMode: true}))
		.addDc("rol", Ext.create(atraxo.fmbas.ui.extjs.dc.Role_Dc,{}))
		.addDc("userSubsidiary", Ext.create(atraxo.fmbas.ui.extjs.dc.UserSubsidiary_Dc,{}))
		.addDc("subsidiary", Ext.create(atraxo.fmbas.ui.extjs.dc.SubsidiaryAssignList_Dc,{multiEdit: true}))
		.linkDc("rol", "usr",{fetchMode:"auto",fields:[
					{childParam:"withUserId", parentField:"id"}]})
				.linkDc("userSubsidiary", "usr",{fetchMode:"auto",fields:[
					{childField:"userId", parentField:"id"}]})
				.linkDc("subsidiary", "usr",{fetchMode:"auto",fields:[
					{childField:"clientId", parentField:"clientId"}, {childParam:"userId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnSaveCloseWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseWdw, scope:this})
		.addButton({name:"btnSaveNewWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewWdw, scope:this})
		.addButton({name:"btnSaveEditWdw",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnSaveEditWdw, scope:this})
		.addButton({name:"btnCancelWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelWdw, scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdwEditor",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdwEditor, scope:this})
		.addButton({name:"btnChangePassword", disabled:false, handler: this.onBtnChangePassword,stateManager:[{ name:"record_is_clean", dc:"usr"}], scope:this})
		.addButton({name:"btnSavePassword", disabled:false, handler: this.onBtnSavePassword, scope:this})
		.addButton({name:"btnAsgnRoles", disabled:false, handler: this.onBtnAsgnRoles,stateManager:[{ name:"record_is_clean", dc:"usr"}], scope:this})
		.addButton({name:"btnDetailsRole", disabled:true, handler: this.onBtnDetailsRole,stateManager:[{ name:"selected_one", dc:"rol"}], scope:this})
		.addButton({name:"btnAssignSubsidiaryShowDialog",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnAssignSubsidiaryShowDialog, scope:this})
		.addButton({name:"btnSetDefault",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSetDefault,stateManager:[{ name:"selected_one_clean", dc:"userSubsidiary"}], scope:this})
		.addButton({name:"btnAssignSubsidiaries",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnAssignSubsidiaries, scope:this})
		.addButton({name:"btnCancelAssign",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelAssign, scope:this})
		.addButton({name:"btnRemoveSubsidiary",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onBtnRemoveSubsidiary,stateManager:[{ name:"record_is_clean", dc:"userSubsidiary", and: function(dc) {return (dc.record.data.mainSubsidiary == false);} }], scope:this})
		.addDcFilterFormView("usr", {name:"usrFilter", xtype:"fmbas_User_Dc$Filter"})
		.addDcGridView("usr", {name:"usrList", xtype:"fmbas_User_Dc$List"})
		.addDcFormView("usr", {name:"usrEdit", xtype:"fmbas_User_Dc$Header"})
		.addDcFormView("usr", {name:"usrTab", _hasTitle_:true, xtype:"fmbas_User_Dc$Tab"})
		.addDcFormView("usr", {name:"usrNew", xtype:"fmbas_User_Dc$New"})
		.addDcFormView("usr", {name:"canvasPassword", preventHeader:true, xtype:"fmbas_User_Dc$ChangePasswordForm"})
		.addDcGridView("rol", {name:"rolList", _hasTitle_:true, xtype:"fmbas_Role_Dc$List"})
		.addDcGridView("userSubsidiary", {name:"subsidiaryList", _hasTitle_:true, xtype:"fmbas_UserSubsidiary_Dc$List"})
		.addDcEditGridView("subsidiary", {name:"subsidiaryAssignList", xtype:"fmbas_SubsidiaryAssignList_Dc$List", frame:true})
		.addWindow({name:"wdwNewUser", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("usrNew")],  listeners:{ close:{fn:this.onWdwUserClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewWdw"), this._elems_.get("btnSaveCloseWdw"), this._elems_.get("btnSaveEditWdw"), this._elems_.get("btnCancelWdw")]}]})
		.addWindow({name:"wdwChangePassword", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("canvasPassword")],  listeners:{ show:{fn:function(win) { win.down('form')._isVisible_ = true; }, scope:this}, close:{fn:function(win) {this._onWdwwClose_(win);}, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSavePassword")]}]})
		.addWindow({name:"wdwSubsidiaries", _hasTitle_:true, width:662, height:350, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("subsidiaryAssignList")],  listeners:{ close:{fn:this.onwdwAssignClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnAssignSubsidiaries"), this._elems_.get("btnCancelAssign")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas2", dcName:"usr"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["usrList"], ["center"])
		.addChildrenTo("canvas2", ["usrEdit", "detailsTab"], ["north", "center"])
		.addChildrenTo("detailsTab", ["usrTab"])
		.addToolbarTo("usrList", "tlbUsrList")
		.addToolbarTo("usrEdit", "tlbUsrEdit")
		.addToolbarTo("rolList", "tlbRolList")
		.addToolbarTo("subsidiaryList", "tlbSubsidiary");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2"])
		.addChildrenTo2("detailsTab", ["rolList", "subsidiaryList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbUsrList", {dc: "usr"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbUsrEdit", {dc: "usr"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnChangePassword"),this._elems_.get("helpWdwEditor")])
			.addReports()
		.end()
		.beginToolbar("tlbRolList", {dc: "rol"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnAsgnRoles")])
			.addReports()
		.end()
		.beginToolbar("tlbSubsidiary", {dc: "userSubsidiary"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnAssignSubsidiaryShowDialog"),this._elems_.get("btnRemoveSubsidiary"),this._elems_.get("btnSetDefault")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnSaveCloseWdw
	 */
	,onBtnSaveCloseWdw: function() {
		this.saveClose();
	}
	
	/**
	 * On-Click handler for button btnSaveNewWdw
	 */
	,onBtnSaveNewWdw: function() {
		this.saveNew();
	}
	
	/**
	 * On-Click handler for button btnSaveEditWdw
	 */
	,onBtnSaveEditWdw: function() {
		this.saveEdit();
	}
	
	/**
	 * On-Click handler for button btnCancelWdw
	 */
	,onBtnCancelWdw: function() {
		this.onWdwUserClose();
		this._getWindow_("wdwNewUser").close();
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdwEditor
	 */
	,onHelpWdwEditor: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnChangePassword
	 */
	,onBtnChangePassword: function() {
		this._getWindow_("wdwChangePassword").show();
	}
	
	/**
	 * On-Click handler for button btnSavePassword
	 */
	,onBtnSavePassword: function() {
		var successFn = function() {
			this._closePassWdw_();
		};
		var o={
			name:"changePassword",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("usr").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnAsgnRoles
	 */
	,onBtnAsgnRoles: function() {
		this._showAsgnWindow_("atraxo.fmbas.ui.extjs.asgn.User_Role_Asgn$Ui" ,{dc: "usr", objectIdField: "id"
		,listeners: {close: {scope: this, fn: function() { this._getDc_("rol").doQuery();
		}} }});
	}
	
	/**
	 * On-Click handler for button btnDetailsRole
	 */
	,onBtnDetailsRole: function() {
		var bundle = "atraxo.mod.ad";
		var frame = "atraxo.ad.ui.extjs.frame.Role_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				id: this._getDc_("rol").getRecord().get("id"),
				code: this._getDc_("rol").getRecord().get("code")
			},
			callback: function (params) {
				this._when_called_for_details(params);
			}
		});
	}
	
	/**
	 * On-Click handler for button btnAssignSubsidiaryShowDialog
	 */
	,onBtnAssignSubsidiaryShowDialog: function() {
		this._showAssignWdw_();
	}
	
	/**
	 * On-Click handler for button btnSetDefault
	 */
	,onBtnSetDefault: function() {
		var successFn = function() {
			this._getDc_("userSubsidiary").doReloadPage();
		};
		var o={
			name:"setDefault",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("userSubsidiary").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnAssignSubsidiaries
	 */
	,onBtnAssignSubsidiaries: function() {
		var successFn = function() {
			this._getDc_("subsidiary").doCancel();
			this._getWindow_("wdwSubsidiaries").close();
			this._getDc_("userSubsidiary").doReloadPage();
		};
		var o={
			name:"assign",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("subsidiary").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnCancelAssign
	 */
	,onBtnCancelAssign: function() {
		this._getDc_("subsidiary").doCancel();
		this._getWindow_("wdwSubsidiaries").close();
	}
	
	/**
	 * On-Click handler for button btnRemoveSubsidiary
	 */
	,onBtnRemoveSubsidiary: function() {
		var successFn = function() {
			this._getDc_("userSubsidiary").doReloadPage();
		};
		var o={
			name:"removeSubsidiary",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("userSubsidiary").doRpcDataList(o);
	}
	
	,_closePassWdw_: function() {
		
						var win = this._getWindow_("wdwChangePassword");
						var user = this._getDc_("usr");
						var rol = this._getDc_("rol");
						var subsidiary = this._getDc_("userSubsidiary");
						win.close();
						user.doReloadPage();
						rol.doReloadPage(); 
						subsidiary.doReloadPage(); 
						
	}
	
	,_showAssignWdw_: function() {
		
						var win = this._getWindow_("wdwSubsidiaries");
						var grid = this._get_("subsidiaryAssignList");
						win.show(undefined, function() {
							var dc = this._getDc_("subsidiary");
							var s = dc.store;
							s.load({
							    callback: function (records, operation, success) {        
							        for (var i =0; i < records.length; i++) {
										var isSelected = records[i].get("isSelected");
										var rowIndex = grid.store.indexOf(records[i]);
										if (isSelected == true) {
											grid.getSelectionModel().select(rowIndex, true);
										}
										else {
											grid.getSelectionModel().deselect(rowIndex);
										}
									}
							    }
							})
							
						}, this);
	}
	
	,_afterDefineDcs_: function() {
		
					var user = this._getDc_("usr");
		
					user.on("afterDoNew", function (dc) {
						dc.setEditMode();
					    this.onShowWdw();
					}, this);
		
		
					user.on("afterDoSaveSuccess", function (dc, ajaxResult) {
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwNewUser").close();
						} else if (ajaxResult.options.options.doEditAfterSave === true) {
							this._getWindow_("wdwNewUser").close();
							this._showStackedViewElement_("main", "canvas2");	
						}
						this._refreshUserProfile_();
						this._applyStateAllButtons_(); 
					}, this);
		
	}
	
	,_refreshUserProfile_: function() {
		
						var user = this._getDc_("usr");
						var r = user.getRecord();
						if (r) {
							var title = r.get("title");
							var firstname = r.get("firstName"); 
							var lastname = r.get("lastName"); 
							var loginName = r.get("loginName");
							var sessionUserName = getApplication().session.user.loginName;
		
							if (loginName == sessionUserName) {
								getApplication().menu.fireEvent("updateProfilebtnText", Main.translate("appmenuitem", "user_hello__lbl")+" "+firstname);
							}
		
						}
	}
	
	,onShowWdw: function() {
		
						var win = this._getWindow_("wdwNewUser");
						win.show();
						var r = this._getDc_("usr").getRecord();
						if (r) {
							r.set("decimalSeparator", ",");
							r.set("thousandSeparator", ".");
							r.set("dateFormat", "dd.mm.yyyy");
						}
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/UserManagement.html";
					window.open( url, "SONE_Help");
	}
	
	,saveNew: function() {
		
					var dc = this._getDc_("usr");
					dc.doSave({doNewAfterSave:true});
	}
	
	,saveClose: function() {
		
					var dc = this._getDc_("usr");
					dc.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveEdit: function() {
		
					var dc = this._getDc_("usr");
					dc.doSave({doEditAfterSave:true});
	}
	
	,onWdwUserClose: function() {
		
						var dc = this._getDc_("usr");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,_onWdwwClose_: function(win) {
		
						var f = function(win) {
							var dc = this._getDc_("usr");
							dc.doCancel(); 
							win.down("form")._isVisible_ = false; 
							dc._changeState_ = 0;
						};
						Ext.defer(f, 300, this, [win]);
	}
	
	,onwdwAssignClose: function() {
		
						this._get_("subsidiaryAssignList").getSelectionModel().deselectAll();
						var dc = this._getDc_("subsidiary");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						} 
	}
});
