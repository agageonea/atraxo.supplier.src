/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.JobScheduler_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.JobScheduler_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("jobChain", Ext.create(atraxo.fmbas.ui.extjs.dc.JobChain_Dc,{multiEdit: true}))
		.addDc("trigger", Ext.create(atraxo.fmbas.ui.extjs.dc.Trigger_Dc,{}))
		.addDc("actions", Ext.create(atraxo.fmbas.ui.extjs.dc.Actions_Dc,{multiEdit: true}))
		.addDc("actionParameter", Ext.create(atraxo.fmbas.ui.extjs.dc.ActionParameter_Dc,{multiEdit: true}))
		.addDc("actionResultHistory", Ext.create(atraxo.fmbas.ui.extjs.dc.ActionResultHistory_Dc,{}))
		.addDc("jobChainUsers", Ext.create(atraxo.fmbas.ui.extjs.dc.JobChainUsers_Dc,{multiEdit: true}))
		.addDc("stepExecution", Ext.create(atraxo.fmbas.ui.extjs.dc.StepExecution_Dc,{}))
		.addDc("customer", Ext.create(atraxo.fmbas.ui.extjs.dc.CustomerAssign_Dc,{}))
		.addDc("subsidiary", Ext.create(atraxo.fmbas.ui.extjs.dc.Subsidiary_Dc,{}))
		.linkDc("trigger", "jobChain",{fetchMode:"auto",fields:[
					{childField:"jobChainId", parentField:"id"}]})
				.linkDc("actions", "jobChain",{fetchMode:"auto",fields:[
					{childField:"jobChainId", parentField:"id"}]})
				.linkDc("actionParameter", "actions",{fetchMode:"auto",fields:[
					{childField:"actionId", parentField:"id"}]})
				.linkDc("actionResultHistory", "jobChain",{fetchMode:"auto",fields:[
					{childField:"value", parentField:"id"}, {childField:"name", parentField:"paramIdentifier"}]})
				.linkDc("jobChainUsers", "jobChain",{fetchMode:"auto",fields:[
					{childField:"jobChainId", parentField:"id"}]})
				.linkDc("stepExecution", "actionResultHistory",{fetchMode:"auto",fields:[
					{childField:"jobExecStepId", parentField:"jobExId"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"runButton",glyph:fp_asc.gear_glyph.glyph, disabled:true, handler: this.onRunButton,stateManager:[{ name:"selected_one", dc:"jobChain", and: function(dc) {return (dc.record && dc.record.data.enabled === true);} }], scope:this})
		.addButton({name:"activateButton",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onActivateButton,stateManager:[{ name:"selected_one_clean", dc:"jobChain", and: function(dc) {return (dc.record && dc.record.data.enabled === false);} }], scope:this})
		.addButton({name:"deactivateButton",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onDeactivateButton,stateManager:[{ name:"selected_one_clean", dc:"jobChain", and: function(dc) {return (dc.record && dc.record.data.enabled === true);} }], scope:this})
		.addButton({name:"stopButton",glyph:fp_asc.stop_glyph.glyph,iconCls: fp_asc.stop_glyph.css,  disabled:true, handler: this.onStopButton, scope:this})
		.addButton({name:"helpjobChainButtons",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpjobChainButtons, scope:this})
		.addButton({name:"saveNewTrigger",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onSaveNewTrigger, scope:this})
		.addButton({name:"cancelNewTrigger",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onCancelNewTrigger, scope:this})
		.addButton({name:"updateJobParameter",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onUpdateJobParameter,stateManager:[{ name:"selected_not_zero", dc:"actionParameter", and: function(dc) {return (dc.isStoreDirty());} }], scope:this})
		.addButton({name:"cancelJobParameter",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onCancelJobParameter, scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"moveUpButton",glyph:fp_asc.up_glyph.glyph, disabled:false, handler: this.onMoveUpButton,stateManager:[{ name:"record_is_clean", dc:"actions", and: function() {return (this._isFirstIndex_() && this._jobIsNotEnabled_());} }], scope:this})
		.addButton({name:"moveDownButton",glyph:fp_asc.down_glyph.glyph, disabled:false, handler: this.onMoveDownButton,stateManager:[{ name:"record_is_clean", dc:"actions", and: function() {return (this._isLastIndex_() && this._jobIsNotEnabled_());} }], scope:this})
		.addButton({name:"paramButton",glyph:fp_asc.asterisk_glyph.glyph, disabled:false, handler: this.onParamButton, scope:this})
		.addButton({name:"btnCancelWithMenu1",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"jobChain", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected1",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu1", handler: this.onBtnCancelSelected1, scope:this})
		.addButton({name:"btnCancelAll1",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu1", _isDefaultHandler_:true, handler: this.onBtnCancelAll1, scope:this})
		.addButton({name:"btnCancelWithMenu2",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"actions", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected2",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu2", handler: this.onBtnCancelSelected2, scope:this})
		.addButton({name:"btnCancelAll2",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu2", _isDefaultHandler_:true, handler: this.onBtnCancelAll2, scope:this})
		.addButton({name:"viewLog",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onViewLog,stateManager:[{ name:"selected_one_clean", dc:"actionResultHistory"}], scope:this})
		.addButton({name:"btnDeleteHistory",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteHistory,stateManager:[{ name:"selected_one_clean", dc:"actionResultHistory"}], scope:this})
		.addButton({name:"btnEmptyHistory",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnEmptyHistory,stateManager:[{ name:"list_not_empty", dc:"actionResultHistory"}], scope:this})
		.addButton({name:"closeLog",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onCloseLog, scope:this})
		.addButton({name:"btnAddNotification",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnAddNotification, scope:this})
		.addButton({name:"btnSaveNotification",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSaveNotification,stateManager:[{ name:"record_is_dirty", dc:"jobChainUsers"}], scope:this})
		.addButton({name:"btnCancelNotification",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnCancelNotification,stateManager:[{ name:"record_is_dirty", dc:"jobChainUsers"}], scope:this})
		.addButton({name:"btnRemoveNotification",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnRemoveNotification,stateManager:[{ name:"selected_not_zero", dc:"jobChainUsers", and: function(dc) {return (dc.isDirty() == false);} }], scope:this})
		.addButton({name:"btnNewJob",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewJob, scope:this})
		.addButton({name:"btnSaveJob",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSaveJob,stateManager:[{ name:"record_is_dirty", dc:"jobChain", and: function(dc) {return (dc.record && (!Ext.isEmpty(dc.record.data.status) && dc.record.data.status != __FMBAS_TYPE__.JobStatus._RUNNING_));} }], scope:this})
		.addButton({name:"btnDeleteJob",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onBtnDeleteJob,stateManager:[{ name:"record_is_clean", dc:"jobChain", and: function(dc) {return (dc.record && (!Ext.isEmpty(dc.record.data.status) && dc.record.data.status != __FMBAS_TYPE__.JobStatus._RUNNING_));} }], scope:this})
		.addButton({name:"closeStepExecutionListWindow",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onCloseStepExecutionListWindow, scope:this})
		.addButton({name:"openStepExecutionListWindow",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onOpenStepExecutionListWindow,stateManager:[{ name:"selected_one_clean", dc:"actionResultHistory"}], scope:this})
		.addButton({name:"btnRefreshStepExecutionListWindow",glyph:fp_asc.refresh_glyph.glyph, disabled:false, handler: this.onBtnRefreshStepExecutionListWindow, scope:this})
		.addDcGridView("trigger", {name:"triggers", _hasTitle_:true, xtype:"fmbas_Trigger_Dc$List"})
		.addDcFormView("trigger", {name:"newTrigger", xtype:"fmbas_Trigger_Dc$New"})
		.addDcFormView("trigger", {name:"editTrigger", xtype:"fmbas_Trigger_Dc$New"})
		.addDcFilterFormView("jobChain", {name:"jobFilter", xtype:"fmbas_JobChain_Dc$Filter"})
		.addDcEditGridView("jobChain", {name:"jobList", height:300, xtype:"fmbas_JobChain_Dc$EditList", frame:true})
		.addDcEditGridView("actions", {name:"actions", _hasTitle_:true, xtype:"fmbas_Actions_Dc$EditList", frame:true})
		.addDcEditGridView("actionParameter", {name:"actionParametersList", width:400, height:300, xtype:"fmbas_ActionParameter_Dc$EditList", frame:true,  listeners:{ beforeedit: { scope: this, fn: function(editor, ctx) { this._setEditor_(editor, ctx, this._get_('actionParametersList'))}}}})
		.addDcFormView("actionParameter", {name:"actionParameterNew", height:1, xtype:"fmbas_ActionParameter_Dc$actionParameterForm"})
		.addDcFormView("trigger", {name:"settings", _hasTitle_:true, xtype:"fmbas_Trigger_Dc$Temp",  disabled:true})
		.addDcGridView("actionResultHistory", {name:"history", _hasTitle_:true, xtype:"fmbas_ActionResultHistory_Dc$List"})
		.addDcFilterFormView("actionResultHistory", {name:"historyFilter", xtype:"fmbas_ActionResultHistory_Dc$Filter"})
		.addDcFormView("actionResultHistory", {name:"historyErrorMsg", xtype:"fmbas_ActionResultHistory_Dc$Error"})
		.addDcEditGridView("jobChainUsers", {name:"notifications", _hasTitle_:true, height:350, xtype:"fmbas_JobChainUsers_Dc$List", frame:true})
		.addDcGridView("customer", {name:"customerAssignList", xtype:"fmbas_CustomerAssign_Dc$AssignList"})
		.addDcGridView("subsidiary", {name:"subsidiaryAssignList2", xtype:"fmbas_Subsidiary_Dc$AssignList2"})
		.addDcGridView("stepExecution", {name:"stepExecutionList", xtype:"fmbas_StepExecution_Dc$StepExecutionList"})
		.addWindow({name:"wdwNewTrigger", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("newTrigger")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("saveNewTrigger"), this._elems_.get("cancelNewTrigger")]}]})
		.addWindow({name:"wdwErrorLog", _hasTitle_:true, width:725, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("historyErrorMsg")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("closeLog")]}]})
		.addAsgnWindow({name:"customerAsgnWdw", _hasTitle_:true, width:800, height:400,_gridName_: "customerAssignList",
			_srcDcName_: "actionParameter", _srcField_: "value", _srcIdField_: "businessValue",
			_gridField_: "code"})
		.addAsgnWindow({name:"subsidiaryAsgnWdw", _hasTitle_:true, width:800, height:400,_gridName_: "subsidiaryAssignList2",
			_srcDcName_: "actionParameter", _srcField_: "value", _srcIdField_: "businessValue",
			_gridField_: "code"})
		.addWindow({name:"stepExecutionListWdw", _hasTitle_:true, width:1000, height:400, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("stepExecutionList")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnRefreshStepExecutionListWindow"), this._elems_.get("closeStepExecutionListWindow")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"actionParameter", layout:{ type: "border" }, defaults:{split:true}})
		.addWindow({name:"wdwJobParameters", _hasTitle_:true, width:816, height:400, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("actionParameter")],  listeners:{ close:{fn:this.onJobParametersWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("updateJobParameter"), this._elems_.get("cancelJobParameter")]}]})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas1", dcName:"jobChain"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["jobList", "detailsTab"], ["north", "center"])
		.addChildrenTo("detailsTab", ["triggers"])
		.addChildrenTo("actionParameter", ["actionParametersList", "actionParameterNew"], ["center", "south"])
		.addToolbarTo("jobList", "tlbJobChainEditableList")
		.addToolbarTo("triggers", "tlbTrigger")
		.addToolbarTo("editTrigger", "tlbTriggerEdit")
		.addToolbarTo("actions", "tlbActionsEditableList")
		.addToolbarTo("notifications", "tlbNotification")
		.addToolbarTo("history", "tlbHistory");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("detailsTab", ["actions", "settings", "history", "notifications"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbJobChainEditableList", {dc: "jobChain"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewJob"),this._elems_.get("btnSaveJob"),this._elems_.get("btnDeleteJob"),this._elems_.get("btnCancelWithMenu1"),this._elems_.get("btnCancelSelected1"),this._elems_.get("btnCancelAll1"),this._elems_.get("runButton"),this._elems_.get("activateButton"),this._elems_.get("deactivateButton"),this._elems_.get("stopButton"),this._elems_.get("helpjobChainButtons")])
			.addReports()
		.end()
		.beginToolbar("tlbTrigger", {dc: "trigger"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main"}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbTriggerEdit", {dc: "trigger"})
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbActionsEditableList", {dc: "actions"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelWithMenu2"),this._elems_.get("btnCancelSelected2"),this._elems_.get("btnCancelAll2"),this._elems_.get("moveUpButton"),this._elems_.get("moveDownButton"),this._elems_.get("paramButton")])
			.addReports()
		.end()
		.beginToolbar("tlbNotification", {dc: "jobChainUsers"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnAddNotification"),this._elems_.get("btnSaveNotification"),this._elems_.get("btnCancelNotification"),this._elems_.get("btnRemoveNotification")])
			.addReports()
		.end()
		.beginToolbar("tlbHistory", {dc: "actionResultHistory"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("viewLog"),this._elems_.get("btnDeleteHistory"),this._elems_.get("btnEmptyHistory"),this._elems_.get("openStepExecutionListWindow")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button runButton
	 */
	,onRunButton: function() {
		var successFn = function() {
			this._getDc_("jobChain").doReloadRecord();
		};
		var o={
			name:"runNow",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("jobChain").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button activateButton
	 */
	,onActivateButton: function() {
		var successFn = function() {
			this._getDc_("jobChain").doReloadPage();
		};
		var o={
			name:"activate",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("jobChain").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button deactivateButton
	 */
	,onDeactivateButton: function() {
		var successFn = function() {
			this._getDc_("jobChain").doReloadPage();
		};
		var o={
			name:"deactivate",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("jobChain").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button stopButton
	 */
	,onStopButton: function() {
		var o={
			name:"activate",
			modal:true
		};
		this._getDc_("jobChain").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button helpjobChainButtons
	 */
	,onHelpjobChainButtons: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button saveNewTrigger
	 */
	,onSaveNewTrigger: function() {
		this.saveClose();
	}
	
	/**
	 * On-Click handler for button cancelNewTrigger
	 */
	,onCancelNewTrigger: function() {
		this.onTriggerWdwClose();
		this._getWindow_("wdwNewTrigger").close();
	}
	
	/**
	 * On-Click handler for button updateJobParameter
	 */
	,onUpdateJobParameter: function() {
		this.saveCloseJobParameter();
	}
	
	/**
	 * On-Click handler for button cancelJobParameter
	 */
	,onCancelJobParameter: function() {
		this.onJobParametersWdwClose();
		this._getWindow_("wdwJobParameters").close();
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button moveUpButton
	 */
	,onMoveUpButton: function() {
		this.moveUp();
	}
	
	/**
	 * On-Click handler for button moveDownButton
	 */
	,onMoveDownButton: function() {
		this.moveDown();
	}
	
	/**
	 * On-Click handler for button paramButton
	 */
	,onParamButton: function() {
		this.showJobParamsWdw();
	}
	
	/**
	 * On-Click handler for button btnCancelSelected1
	 */
	,onBtnCancelSelected1: function() {
		this.fnCancelSelected1();
	}
	
	/**
	 * On-Click handler for button btnCancelAll1
	 */
	,onBtnCancelAll1: function() {
		this.fnCancelAll1();
	}
	
	/**
	 * On-Click handler for button btnCancelSelected2
	 */
	,onBtnCancelSelected2: function() {
		this.fnCancelSelected2();
	}
	
	/**
	 * On-Click handler for button btnCancelAll2
	 */
	,onBtnCancelAll2: function() {
		this.fnCancelAll2();
	}
	
	/**
	 * On-Click handler for button viewLog
	 */
	,onViewLog: function() {
		this._getWindow_("wdwErrorLog").show();
	}
	
	/**
	 * On-Click handler for button btnDeleteHistory
	 */
	,onBtnDeleteHistory: function() {
		var successFn = function() {
			this._getDc_("actionResultHistory").doReloadPage();
		};
		var o={
			name:"delete",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("actionResultHistory").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnEmptyHistory
	 */
	,onBtnEmptyHistory: function() {
		var successFn = function() {
			this._getDc_("actionResultHistory").doReloadPage();
		};
		var o={
			name:"empty",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("jobChain").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button closeLog
	 */
	,onCloseLog: function() {
		this.closeWdwErrorLog();
	}
	
	/**
	 * On-Click handler for button btnAddNotification
	 */
	,onBtnAddNotification: function() {
		this._getDc_("jobChainUsers").doNew();
	}
	
	/**
	 * On-Click handler for button btnSaveNotification
	 */
	,onBtnSaveNotification: function() {
		this._getDc_("jobChainUsers").doSave();
	}
	
	/**
	 * On-Click handler for button btnCancelNotification
	 */
	,onBtnCancelNotification: function() {
		this._getDc_("jobChainUsers").doCancel();
	}
	
	/**
	 * On-Click handler for button btnRemoveNotification
	 */
	,onBtnRemoveNotification: function() {
		this._getDc_("jobChainUsers").doDelete();
	}
	
	/**
	 * On-Click handler for button btnNewJob
	 */
	,onBtnNewJob: function() {
		this._getDc_("jobChain").doNew();
	}
	
	/**
	 * On-Click handler for button btnSaveJob
	 */
	,onBtnSaveJob: function() {
		this._getDc_("jobChain").doSave();
	}
	
	/**
	 * On-Click handler for button btnDeleteJob
	 */
	,onBtnDeleteJob: function() {
		this._getDc_("jobChain").doDelete();
	}
	
	/**
	 * On-Click handler for button closeStepExecutionListWindow
	 */
	,onCloseStepExecutionListWindow: function() {
		this._getWindow_("stepExecutionListWdw").close();
	}
	
	/**
	 * On-Click handler for button openStepExecutionListWindow
	 */
	,onOpenStepExecutionListWindow: function() {
		this._getWindow_("stepExecutionListWdw").show();
	}
	
	/**
	 * On-Click handler for button btnRefreshStepExecutionListWindow
	 */
	,onBtnRefreshStepExecutionListWindow: function() {
		this._getDc_("stepExecution").doReloadPage();
	}
	
	,closeWdwErrorLog: function() {	
		this._getWindow_("wdwErrorLog").close();
	}
	
	,_afterDefineDcs_: function() {
		
						var customer = this._getDc_("customer");
						customer.store.pageSize = 5000;
	}
	
	,_setEditor_: function(editor,ctx,view) {
		
					var dc = this._getDc_("actionParameter");
					var fieldIndex = ctx.field;
					var col = ctx.column;
					var cfg = {};
					var textEditor = {xtype:"textfield", maxLength:100};	
					var numberEditor = {xtype:"numberfield", maxLength:32};	
					var passwordEditor = {
						xtype:"textfield", 
						cls: "sone-masked-field",
						inputType: Ext.isChrome ? "text" : "password",
					};	
					var field = col.field;
					var storeData = [];
					var dateFormat = "mm/dd/yyy";
					
					var frame = this;
		
					if (fieldIndex == "value") {
						var r = ctx.record;
						if (r) {
		
							var type = r.get("type");
							var name = r.get("name");
							var refValues = r.get("refValues");
							var paramName = r.get("batchJobParamName");
							var test = r.get("businessValue");
							var readOnly = r.get("readOnly");
		
							if (!Ext.isEmpty(type)) {
		
								//////////// CASE 1 - Type is _ENUMERATION_ ////////////
		
								if (type == __FMBAS_TYPE__.JobParamType._ENUMERATION_) {
									cfg = {
										xtype: "combo",
							            selectOnFocus: true,
							            queryMode: "local",
							            store: refValues.split(","),
										readOnly: readOnly == true ? true : false
									};
									
								}
		
								//////////// CASE 2 - Type is _DATE_ ////////////
		
								else if (type == __FMBAS_TYPE__.JobParamType._DATE_) {							
									cfg = {
										xtype: "datefield",
										_initValue_ : this.value,
										format: Main.DATE_FORMAT,
										altFormats: Main.ALT_FORMATS,
										_mask_ : Main.DATE_FORMAT,
										submitFormat: Main.DATE_FORMAT,
										listeners: {						
											boxready: {
												scope: this,
												fn: function(el) {
													if (fieldIndex === "value") {
														var r = dc.getRecord();
														if (r) {
															var value = r.get("value");										
															if (!Ext.isEmpty(value)) {
																el.setValue(new Date(value));
															}											
														}
													}								
												}
											}
										}			
									};						
								}
		
								//////////// CASE 3 - Type is _LOV_ ////////////
		
								else if (type == __FMBAS_TYPE__.JobParamType._LOV_) {
		
									var retField = "name";
		
									if (paramName == "CUSTOMER" || paramName == "SUBSIDIARY") {
										retField = "code";
									}
		
									cfg = {
										_dcView_ : view, 
										xtype: refValues, 
										maxLength:32, 
										typeAhead: false,
										remoteFilter: true,
										remoteSort: true,
										forceSelection: true,
										readOnly: readOnly == true ? true : false,
										retFieldMapping: [{
											lovField:retField, 
											dsField: "value"}
										]
									};
								}
		
								//////////// CASE 4 - Type is _MASKED_ ////////////
		
								else if (type == __FMBAS_TYPE__.JobParamType._MASKED_) {
									passwordEditor.readOnly = readOnly == true ? true : false;
									cfg = passwordEditor;
								}
		
								else if (type == __FMBAS_TYPE__.JobParamType._ASSIGN_) {
									textEditor.readOnly = true;
									cfg = textEditor;
								}
		
								else {
									textEditor.readOnly = readOnly == true ? true : false;
									cfg = textEditor;
								}
		
								// Do not delete (example) : how to exclude certain numbers from input
		
		//						if (!Ext.isEmpty(name)) {
		//							if (name == "OFFSET") {
		//								cfg = numberEditor;
		//								cfg.enableKeyEvents = true;
		//								cfg.cls = "sone-number-no-spinner ";
		//								cfg.listeners = {
		//									keydown: {
		//										scope: this,
		//										fn: function(field, event) {
		//											var f = function() {
		//												console.log(field.getValue());
		//										        if (field.getValue() == 0) {
		//													field.setValue(null);
		//													event.stopPropagation();
		//										        }
		//											}
		//											Ext.defer(f,100,this);
		//											
		//										}
		//									}
		//								}
		//							}
		//						}
		
		
								col.setEditor(cfg);
		
							}
						}
					}
		
		
		
	}
	
	,_isLastIndex_: function() {
		
						var actions = this._get_("actions");
						var selection = actions.getSelectionModel().getSelection()[0];
						var result = false;
		
						// "actions.store.indexOf(selection) == -1" because sometimes the button is disable when it shouldn't be
		
						if (actions.store.indexOf(selection) == -1 || actions.store.indexOf(selection) != actions.getView().all.endIndex) {
							result = true;
						}
						return result;
	}
	
	,showJobParamsWdw: function() {
		
						var win = this._getWindow_("wdwJobParameters");
						win.show();
	}
	
	,_isFirstIndex_: function() {
		
						var actions = this._get_("actions");
						var selection = actions.getSelectionModel().getSelection()[0];
						var result = false;
						if (actions.store.indexOf(selection) > 0) {
							result = true;
						}
						return result;
	}
	
	,_jobIsNotEnabled_: function() {
		
						var jobChain = this._getDc_("jobChain");
						var record = jobChain.getRecord();
						var result = true;
						if (record) {
							var enabled = record.get("enabled");
							if (enabled == true) { 
								result = false;
							}
						}
						return result;
	}
	
	,_endDefine_: function() {
		
		
					var trigger= this._getDc_("trigger");
					var actions = this._getDc_("actions");
					var actionParams = this._getDc_("actionParameter");
					var jobChain = this._getDc_("jobChain");
					var jobChainUsers = this._getDc_("jobChainUsers");
		
					jobChain.on("recordChange", function () { 
						var record = jobChain.getRecord();
						if (record) {
							var enabled = record.get("enabled");
							if (enabled == true) { 
								trigger.setReadOnly(true); 
								actions.setReadOnly(true); 
							}
							else {
								trigger.setReadOnly(false);
								actions.setReadOnly(false);
							}
						}
					}, this);
		
					jobChainUsers.on("afterDoSaveSuccess", function (dc) { 
						dc.doReloadRecord();
					}, this);
		
					jobChain.on("afterDoSaveSuccess", function (dc) { 
						dc.doReloadRecord();
					}, this);
		
					actions.on("afterDoNew", function (dc, ajaxResult) { 
						var record = dc.getRecord();
						if (!record) {
							return;
						}
						var idx = dc.store.indexOf(record);
						var newIdx = idx+1;
		
						record.set("order", newIdx);
					}, this);
		
					actions.on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						this._applyStateAllButtons_();
						actionParams.doReloadPage();
					}, this);
		
					trigger.on("afterDoNew", function (dc) { 
						dc.isEditMode = true;
						this.showNewTriggerWdw(true);
					}, this);
		
					trigger.on("onEditIn", function (dc) {
						dc.isEditMode = true;
						this.showNewTriggerWdw();
					}, this);
		
					/*
					#############################################################################################
					# Start Dan: SONE-2328: Job Scheduler/Triggers: the design for "Monthly" triggers is broken
					#############################################################################################
					*/
		
					trigger.on("beforeDoSave", function (form) {
		
						var newTriggerView = this._get_("newTrigger");
						var monthsCombo = newTriggerView._get_("monthsCombo");
						var daysOfMonthCombo = newTriggerView._get_("daysOfMonthCombo");			
		
						monthsCombo.suspendEvents();
						form._controller_.setParamValue("monthsCombo", "");
						monthsCombo.resumeEvents();
		
						daysOfMonthCombo.suspendEvents();
						form._controller_.setParamValue("daysOfMonthCombo","");
						daysOfMonthCombo.resumeEvents();				
		
					}, this);
		
					/*
					#########################################################################################
					# End Dan: SONE-2328: Job Scheduler/Triggers: the design for "Monthly" triggers is broken
					#########################################################################################
					*/
		
					/*
					###################################################################################################################
					# SONE-2928: Job Scheduler/"Monthly" trigger: values from "Months" are erased if the repeat interval is not defined
					###################################################################################################################
					*/
		
					trigger.on("afterDoSaveFailure", function (dc, ajaxResult) {
		 
						var newTriggerView = this._get_("newTrigger");
						var monthsCombo = newTriggerView._get_("monthsCombo");
						var daysOfMonthCombo = newTriggerView._get_("daysOfMonthCombo");
						var record = dc.getRecord();
		
						if (record) {
		
							monthsCombo.suspendEvents();
							dc.setParamValue("monthsCombo",record.get("months"));
							monthsCombo.resumeEvents();
		
							daysOfMonthCombo.suspendEvents();
							dc.setParamValue("daysOfMonthCombo",record.get("days"));
							daysOfMonthCombo.resumeEvents();
						}
					}, this);
		
					trigger.on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwNewTrigger").close();
							dc.isEditMode = false; 
							dc.doReloadPage();
						}
					}, this);
		
					actionParams.on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwJobParameters").close();
						}
					}, this);
	}
	
	,moveUp: function() {
		
		
						var grid = this._get_("actions");
						var row = grid.getSelectionModel().getSelection()[0];
						var index = grid.store.indexOf(row);
		
						if(index < 1) return;
		                index--;
		                grid.store.remove(row, true);
		                grid.getStore().insert(index, row);
		
						var o={
							name:"moveUp",
							modal:true
						};
						this._getDc_("actions").doRpcData(o);
		
		
	}
	
	,moveDown: function() {
		
		
						var grid = this._get_("actions");
						var row = grid.getSelectionModel().getSelection()[0];
						var index = grid.store.indexOf(row);
						var totalCount = grid.store.getTotalCount();
		
						if(index >= grid.getView().all.endIndex)  return;
		                index++;
		                grid.store.remove(row, true);
		                grid.getStore().insert(index, row);
		
						var o={
							name:"moveDown",
							modal:true
						};
						this._getDc_("actions").doRpcData(o);
		
		
	}
	
	,showNewTriggerWdw: function(clearType) {
		
						var win = this._getWindow_("wdwNewTrigger");
						win.show(undefined, function() {
							var dc = this._getDc_("trigger");
							var r = dc.getRecord();
							if (r) {
								r.beginEdit();
								dc.setParamValue("monthsCombo", r.get("months"));
								dc.setParamValue("daysOfMonthCombo", r.get("daysOfMonth"));
								dc.setParamValue("weekOfMonthCombo", r.get("weekOfMonth"));
								if (r.get("type") == "Monthly") {
									dc.setParamValue("daysOfWeekCombo", r.get("daysOfWeek"));
								}
								if (r.get("type") == "Weekly") {
									r.set("daysOfWeekEnum", r.get("daysOfWeek"));
								}												
								r.endEdit();
							}
							if (clearType && clearType == true) {
								var view = this._get_("newTrigger");
								var typeField = view._get_("type");
								typeField.setValue(null);
							}
						}, this);
	}
	
	,saveClose: function() {
		
					var trigger = this._getDc_("trigger");
					trigger.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,onJobParametersWdwClose: function() {
		
						var dc = this._getDc_("actionParameter");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,saveCloseJobParameter: function() {
		 
					var actionParam = this._getDc_("actionParameter");
					actionParam.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,onTriggerWdwClose: function() {
		
						var dc = this._getDc_("trigger");
						if(dc.isDirty()){
							dc.doCancel();
						}
						dc.isEditMode = false;
						dc.doReloadPage();
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/JobScheduler.html";
					window.open( url, "SONE_Help");
	}
	
	,fnCancelSelected1: function() {
		
						var dc = this._getDc_("jobChain");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu1");
	}
	
	,fnCancelAll1: function() {
		
						var dc = this._getDc_("jobChain");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu1");
	}
	
	,fnCancelSelected2: function() {
		
						var dc = this._getDc_("actions");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu2");
	}
	
	,fnCancelAll2: function() {
		
						var dc = this._getDc_("actions");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu2");
	}
	
	,fnCancelEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isCancelEnabled(dc);
	}
	
	,_afterDefineElements_: function() {
		
						
						var builder = this._getBuilder_();
						this._customerSearchFieldId_ = Ext.id();
						builder.change("customerAssignList",this._injectSearchField_(this._customerSearchFieldId_,"customerAssignList","Search for account code/customer", 250, true));
		
						var x = this._getElementConfig_("actionParameter");
						Ext.apply(x,{defaults:{split:false}});
	}
	
	,_injectSearchField_: function(id,gridName,emptyLabelText,fieldWidth,showResetBtn) {
		
						return { tbar: [{ 
								xtype: "textfield",
								id: id,
							    width: fieldWidth ? fieldWidth : 200,
							    emptyText: emptyLabelText,
							    listeners: {
							        change: {
							            scope: this,
										buffer: 1000,
							            fn: function(field, newVal) {
		
							                var grid = this._get_(gridName);
							                if (newVal && newVal !== "*") {
		
												var af = [];
												var i;
												var dc = grid._controller_;
		
												for(i=0;i<grid.columns.length;i++){
													var operation ="like";
													if (grid.columns[i].hidden === false && grid.columns[i].dataIndex !== "type" && grid.columns[i].dataIndex !== "status") {
														var value1 = "%"+newVal+"%";
			
														var o = {
															"id":null,
															"fieldName":grid.columns[i].dataIndex,
															"operation":operation,
															"value1": value1,
															"groupOp":"OR1"
														};
		
														if (grid.columns[i].hidden === false) {
															af.push(o);
														}
													}		
												}
		
												dc.advancedFilter = af;
												dc.doQuery();
							                }
							            }
							        }
							    }
							},{
								glyph: "xf021@FontAwesome",
		                        xtype: "button",
								hidden: (showResetBtn === true) ? false : true,
								listeners: {
									click: {
										scope: this,
										fn: function() {
											var searchFieldId = Ext.getCmp(id);
											var grid = this._get_(gridName);
											var dc = grid._controller_;
											searchFieldId.setValue("");
		
											dc.advancedFilter = null;
											dc.doQuery();
										}
									}
								}}
							]}
	}
});
