__TYPES__ = {
		fuelRequests : {
				status : {
					nevv: "New",
					processed: "Processed",
					closed: "Closed",
					canceled: "Canceled",
					quoted: "Quoted"
				}
		},
		fuelQuoteLocation : {
			status: {
				expired: "Expired",
				submitted: "Submitted",
				closed: "Closed",
				canceled: "Canceled",
				ordered: "Ordered"
			}
			
		},
		flightEventEditSrc : {
			src: {
				fuelRequest: "FUEL_REQUEST",
				fuelQuote: "FUEL_QUOTATION"
			}
		},
		fuelQuotation : {
			status: {
				nevv: "New",
				submitted: "Submitted",
				negotiation: "Negotiating",
				ordered: "Ordered",
				closed: "Closed",
				canceled: "Canceled",
				expired: "Expired"
			},
			remarks: {
				negotiation: "Status changed to negotiation",
				submitted: "Submitted",
				created: "Created"
			},
			reports: {
				gridName: "List", 
				format: "pdf",
				reportCode : "FuelQuote"
			}
		},
		fuelEvent :{
			billStatus :{
				noBill : "No bill",
				checkFailed : "Check failed",
				awaitingApproval : "Awaiting approval",
				awaitingPayment : "Awaiting payment"
			},
			invoiceStatus :{
				notInvoiced : "Not invoiced",
				waitingForPayment : "Waiting for payment",
				paid : "Paid"
			}
		},
		fuelTicket : {
			ticketStatus: {
				original : "Original",
				updated : "Updated",
				canceled : "Canceled"
			},
			approvalStatus : {
				forApproval : "For approval",
				ok : "OK" ,
				recheck : "Recheck",
				rejected : "Rejected"
			},
			transmissionStatus : {
				nevv : "New",
				submitted : "Submitted",
				failed : "Failed",
				exported : "Exported",
				transmitted : "Transmitted"
			},
			fuelTicketSource :{
				mmanual : "Manual",
				iimport : "Import",
				sscan : "Scan"
			},
			invoiceStatus :{
				notInvoiced : "Not invoiced",
				awaitingPayment : "Awaiting payment",
				paid : "Paid"
			},
			billStatus : {
				notBilled : "Not billed",
				draft : "Draft",
				checkFailed : "Check failed",
				checkPassed : "Check passed",
				needsRecheck : "Needs recheck",
				awaitingApproval : "Awaiting approval",
				awaitingPayment : "Awaiting payment",
				paid : "Paid"
			}
			
		},
		fuelOrder : {
			status: {
				nevv : "New",
				confirmed : "Confirmed",
				released : "Released",
				rejected  : "Rejected",
				canceled : "Canceled"
			},
			remarks: {
				create : "New fuel order"
			},
			reports: {
				gridName: "List", 
				format: "pdf",
				reportCode : "FuelOrderRelease"
			}
		},
		fuelOrderLocation : {
			flightType :{
				domestic : "Domestic",
				international : "International",
				unspecified : "Unspecified"
			},
			operationalType :{
				privat : "Private",
				corporate : "Business",
				commercial : "Commercial"
			},
			product :{
				jetA : "Jet-A",
				jetA1 : "Jet-A1",
				ts1 : "TS-1",
				f34 : "F-34",
				jp8 : "JP-8",
				rp4 : "RP-4",
				t1 : "T1",
				a1Bio : "A1-BIO",
				cat2 : "CAT 2"
			},
			deliveryMethod :{
				hydrant : "Hydrant",
				refueller : "Refueller",
				unspecified : "Unspecified"
			},
			priceBasis :{
				index : "Index",
				market : "Market",
				government : "Government",
				fixed : "Fixed",
				variable : "Variable",
				exRefinery : "Ex-Refinery",
				trigger : "Trigger"
			},
			paymentRefDay :{
				daily : "Daily",
				perDelivery : "Per delivery",
				weekly : "Weekly",
				semimonthly : "Semimonthly",
				monthly : "Monthly",
				every10Days : "Every ten days",
				every3Days : "Every three days",
				every20Days : "Every 20 days"
			},
			invoiceFreq :{
				daily : "Daily",
				perDelivery : "Per delivery",
				weekly : "Weekly",
				semimonthly : "Semimonthly",
				monthly : "Monthly",
				every10Days : "Every ten days",
				every3Days : "Every three days",
				every20Days : "Every 20 days"
			}
		},
		flightEvent : {
			status :{
				scheduled : "Scheduled",
				fulfilled : "Fulfilled",
				canceled  : "Canceled",
				invoiced  : "Invoiced"
			},
			supplierInvoiceStatus:{
				noAccruals : "No Accruals",
				partially : "Partially",
				ok : "OK",
				openIssues : "Open Issues"
			},
			outGoingInvoiceStatus:{
				noAccruals : "No Accruals",
				partially : "Partially",
				ok : "OK",
				openIssues : "Open Issues"
			}
		},
		customers : {
			status : {
				prospect : "Prospect",
				approved : "Approved",
				blacklisted : "Blacklisted"
			},
			type :{
				group : "Group",
				groupMember : "Group member",
				independent : "Independent"
			}
		},
		exposure : {
			type:{
				creditLine : "Credit line",
				prepayment : "Prepayment",
				bankGuarantee : "Bank guarantee"
			},
			lastAction:{
				debit : "Debit",
				credit : "Credit",
				reconciliation : "Reconciliation"
			}
		},
		invoices:{
			status :{
				draft : "Draft",
				checkPassed : "Check passed",
				checkFailed : "Check failed",
				awaitingApproval : "Awaiting approval",
				awaitingPayment : "Awaiting payment",
				needsRecheck : "Needs recheck",
				noContract : "No Contract",
				paid : "Paid"
				
			},
			category :{
				productIntoPlane : "Product into plane",
				fuelingServiceIntoPlane : "Fueling service into plane",
				productIntoStorage : "Product into storage"
			},
			dealType :{
				buy : "Buy",
				sell : "Sell"
			},
			checkStatus :{
				ok : "OK",
				failed : "Failed"
			},
			refStatus :{
				bol : "Bill of landing",
				ctn : "Contract number",
				pin : "Previous invoice number",
				po : "Purchase order",
				so : "Sales order",
				wo : "Work order",
				ext : "External documents"
			},
			taxType :{
				unspecified : "Unspecified",
				bonded : "Bonded",
				domestic : "Domestic",
				foreignTradeZone : "Foreign trade zone",
				euQualified : "EU-qualified product",
				nonEuQualified : "Non EU-qualified product"
			},
			reports: {
				gridName: "InvoiceList",
				format: "pdf",
				reportCode : "SalesInvoice"
			}
		},
		invoiceLine:{
			vatStatus :{
				notChecked : "Not checked",
				ok : "OK",
				failed : "Failed",
				recheck : "Recheck"
			},
			flightType :{
				domestic : "Domestic",
				international : "International",
				unspecified : "Unspecified"
			}
		},
		contract:{
			status :{
				draft : "Draft",
				active : "Active",
				effective : "Effective",
				expired : "Expired"
			},
			reviewPeriod :{
				no : "No",
				weekly : "Weekly",
				semimonthly : "Semimonthly",
				monthly : "Monthly",
				quarterly : "Quarterly",
				semiannually : "Semiannually",
				annually : "Annually"
			},
			dealType :{
				buy : "Buy",
				sell : "Sell"
			},
			type :{
				product : "Product",
				fuelingService : "Fueling Service",
				generalService : "General Service",
				inspection : "Inspection",
				storage : "Storage",
				cso : "CSO"
			},
			subtype :{
				na : "",
				intoPlane : "Into Plane",
				exHydrant : "Ex-Hydrant",
				airlineSupply : "Airline Supply"
			},
			scope :{
				intoPlane : "Into plane",
				nonIntoPlane : "Non into plane"
			},
			limitedTo :{
				unspecified : "Unspecified",
				domestic : "Domestic",
				international : "International"
			},
			product :{
				jetA : "Jet-A",
				jetA1 : "Jet-A1",
				ts1 : "TS-1",
				f34 : "F-34",
				jp8 : "JP-8",
				rp4 : "RP-4",
				t1 : "T1",
				a1Bio : "A1-BIO",
				cat2 : "CAT 2"
			},
			tax :{
				unspecified : "Unspecified",
				bonded : "Bonded",
				domestic : "Domestic",
				foreignTrade : "Foreign trade zone",
				euQualified : "EU qualified",
				nonEuQualified : "Non-EU qualified"
			},
			quantityType :{
				netVolume : "Net volume",
				grossVolume : "Gross volume",
				period : "Period"
			},
			contractedVolumePeriod :{
				total : "Total",
				monthly : "Monthly"
			},
			pricingMethod :{
				index : "Index",
				fixed : "Fixed",
				market : "Market",
				government : "Government"
			},
			quotationOffset :{
				current : "Current",
				previous : "Previous",
				beforePrevious : "Before previous",
				next : "Next"
			},
			operator :{
				multiplication : "*",
				division : "/"
			},
			exchangeRateOffset :{
				current : "Current",
				previous : "Previous",
				beforePrevious : "Before previous",
				next : "Next"
			},
			invoiceFrequency :{
				daily : "Daily",
				perDelivery : "Per delivery",
				weekly : "Weekly",
				semimonthly : "Semimonthly",
				monthly : "Monthly",
				every10Days : "Every ten days",
				every3Days : "Every three days",
				every20Days : "Every 20 days"
			},
			creditTerms :{
				openCredit : "Open credit",
				creditLine : "Credit line",
				bankGuarantee : "Bank Guarantee",
				prepayment : "Prepayment"
			},
			invoiceType :{
				electronic : "electronic",
				paperToHQ : "Paper to Headquarter",
				paperToLocal : "Paper to Local"
			},
			invoicePayableBy :{
				headquarter : "Headquarter",
				localOffice : "Local office"
			},
			vatApplicability :{
				all : "All events",
				business : "Business event",
				commercial : "Commercial flight",
				domestic : "Domestic event",
				international : "International events",
				na : "Not applicable",
				privat : "Private flight"
			},
			eventType :{
				transport : "Transport",
				period : "Period",
				movement : "Movement",
				csoEvent : "CSO event"
			},
			priceCategory :{
				quantityType :{
					grossVolume : "Gross volume",
					netVolume : "Net volume",
					period : "Period"
				},
				vatApplicability :{
					all : "All events",
					business : "Business event",
					commercial : "Commercial flight",
					domestic : "Domestic event",
					international : "International events",
					na : "Not applicable",
					privat : "Private flight"
				}
			}
		},
		area:{
			indicator :{
				system : "System",
				user : "User"
			}
		},
		contact:{
			businessUnit :{
				operation : "Operation",
				comercial : "Comercial"
			},
			workOffice :{
				headOffice : "Head Office",
				branch : "Branch",
				airportOffice : "Airport Office"
			}
		},
		creditLine:{
			creditTerm :{
				creditLine : "Credit line",
				bankGuarantee : "Bank guarantee",
				prepaymanet : "Prepayment"
			}
		},
		fuelPriceLocationQuote:{
			flightType :{
				domestic : "Domestic",
				international : "International",
				unspecified : "Unspecified"
			},
			operationalType :{
				privat : "Private",
				corporate : "Business",
				commercial : "Commercial"
			},
			product :{
				jetA : "Jet-A",
				jetA1 : "Jet-A1",
				ts1 : "TS-1",
				f34 : "F-34",
				jp8 : "JP-8",
				rp4 : "RP-4",
				t1 : "T1",
				a1Bio : "A1-BIO",
				cat2 : "CAT 2"
			},
			deliveryMethod :{
				hydrant : "Hydrant",
				truck : "Truck",
				pipeline : "Pipeline",
				unspecified : "Unspecified"
			},
			paymentReferenceDate :{
				daily : "Daily",
				perDelivery : "Per delivery",
				weekly : "Weekly",
				semimonthly : "Semimonthly",
				monthly : "Monthly",
				every10Days : "Every ten days",
				every3Days : "Every three days",
				every20Days : "Every 20 days"
			},
			invoiceFrequency :{
				daily : "Daily",
				perDelivery : "Per delivery",
				weekly : "Weekly",
				semimonthly : "Semimonthly",
				monthly : "Monthly",
				every10Days : "Every ten days",
				every3Days : "Every three days",
				every20Days : "Every 20 days"
			}
		},
		fuelPriceQuote :{
			quotationType :{
				scheduled : "Scheduled",
				adHoc : "Ad-hoc"
			},
			scope :{
				intoPlane : "Into plane",
				exHydrant : "Ex-hydrant",
				intoStorage : "Into storage"
			},
			quoteStatus :{
				nevv : "New",
				submitted : "Submitted",
				negotiating : "Negotiating",
				ordered : "Ordered",
				closed : "Closed",
				canceled : "Canceled",
				expired : "Expired"
			},
			pricingCycle :{
				notice : "Notice",
				weekly : "Weekly",
				biWeekly : "Bi-Weekly",
				monthly : "Monthly"
			}
		},
		fuelPurchaseOrder :{
			orderStatus :{
				nevv : "New",
				cancel : "Cancel",
				rejected : "Rejected",
				confirmed : "Confirmed",
				released : "Released"
			},
			contractType :{
				scheduled : "Scheduled",
				adHoc : "Ad-hoc",
			},
			scope :{
				intoPlane : "Into plane",
				exHydrand : "Ex-hydrant",
				intoStorage : "Into storage"
			}
		},
		fuelRequestDetails :{
			eventType :{
				domestic : "Domestic",
				international : "International"
			},
			operationType :{
				privat : "Private",
				corporate : "Business",
				commercial : "Commercial"
			},
			product :{
				jetA : "Jet-A",
				jetA1 : "Jet-A1",
				ts1 : "TS-1",
				f34 : "F-34",
				jp8 : "JP-8",
				rp4 : "RP-4",
				t1 : "T1",
				a1Bio : "A1-BIO",
				cat2 : "CAT 2"
			}
		},
		masterAgreement :{
			creditTerm :{
				openCredit : "Open credit",
				bankGuarantee : "Bank guarantee",
				creditLine : "Credit line",
				prepayment : "Prepayment"
			},
			status :{
				negotiation : "Negotiation",
				effective : "Effective",
				onHold : "On hold",
				terminated : "Terminated"
			},
			period :{
				current : "Current period",
				previous : "Previous period",
				next : "Next period",
				beforePrevious : "Period before previous period"
			},
			referenceTo :{
				invoiceDate : "Invoice date",
				receivingDate : "Receiving date ofthe invoice",
				lastDelivery : "Last delivery date"
			},
			invoiceFreq :{
				perDelivery : "Per delivery",
				daily : "Daily",
				monthly : "Monthly",
				weekly : "Weekly",
				semimonthly : "Semimonthly",
				every10Days : "Every 10 days"
			},
			invoiceType :{
				electronic : "Electronic",
				paperToHQ : "Paper to headqurters",
				paperToLocal : "Paper to local"
			}
		},
		priceCategory :{
			type :{
				product : "Product",
				differential : "Differential",
				fee : "Fee",
				Tax: "Tax"
			},
			pricePer :{
				volume : "Volume",
				event : "Event",
				percentage : "Percentage",
				period : "Period"
			}
		}
		
}
