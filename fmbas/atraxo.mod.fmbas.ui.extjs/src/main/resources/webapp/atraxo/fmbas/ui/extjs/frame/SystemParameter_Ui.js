/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.SystemParameter_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.SystemParameter_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("sys", Ext.create(atraxo.fmbas.ui.extjs.dc.SystemParameter_Dc,{multiEdit: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"btnCancelWithMenu",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"sys", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", handler: this.onBtnCancelSelected, scope:this})
		.addButton({name:"btnCancelAll",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", _isDefaultHandler_:true, handler: this.onBtnCancelAll, scope:this})
		.addDcFilterFormView("sys", {name:"sysFilter", xtype:"fmbas_SystemParameter_Dc$Filter"})
		.addDcEditGridView("sys", {name:"sysList", xtype:"fmbas_SystemParameter_Dc$List", frame:true,  listeners:{ beforeedit: { scope: this, fn: function(editor, ctx) { this._setEditor_(editor, ctx, this._get_('sysList'))}}}})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["sysList"], ["center"])
		.addToolbarTo("sysList", "tlbList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbList", {dc: "sys"})
			.addButtons([])
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelWithMenu"),this._elems_.get("btnCancelSelected"),this._elems_.get("btnCancelAll"),this._elems_.get("helpWdw")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnCancelSelected
	 */
	,onBtnCancelSelected: function() {
		this.fnCancelSelected();
	}
	
	/**
	 * On-Click handler for button btnCancelAll
	 */
	,onBtnCancelAll: function() {
		this.fnCancelAll();
	}
	
	,_setEditor_: function(editor,ctx,view) {
		
		
					var fieldIndex = ctx.field;
					var col = ctx.column;
					var cfg = {};
					var textEditor = {xtype:"textfield", maxLength:32};	
					var numberEditor = {xtype:"numberfield", maxLength:32};	
					var field = col.field;
					var storeData = [];
					var dateFormat = "mm/dd/yyy";
					
					var frame = this;
		
					if (fieldIndex == "value") {
						var r = ctx.record;
						if (r) {
		
							var type = r.get("type");
							var refValues = r.get("refValues");
		
							if (!Ext.isEmpty(type)) {
		
								//////////// CASE 1 - Type is _ENUMERATION_ ////////////
		
								if (type == "enumeration") {
									cfg = {
										xtype: "combo",
							            selectOnFocus: true,
							            queryMode: "local",
							            store: refValues.split(","),
										validator: function(v) {
										    var store = this.getStore(),
										        index = store.findExact(this.displayField, v);
										
										    if (index === -1 && store.isLoading()) {
										        store.on("load", function() {
										            this.validate();
										        }, this, { single: true, delay: 100 });
										    }
										
										    return (index !== -1) ? true : false;
										}
									};
		
								}
		
								//////////// CASE 2 - Type is _LOV_ ////////////
		
								else if (type == "lov") {
		
									var retField = "code";
		
									cfg = {
										_dcView_ : view, 
										xtype: refValues, 
										maxLength:32, 
										typeAhead: false,
										remoteFilter: true,
										remoteSort: true,
										forceSelection: true,
										retFieldMapping: [{
											lovField:retField, 
											dsField: "value"}
										]
									};
								}
		
								//////////// CASE 3 - Type is boolean ////////////
		
								else if (type == "boolean") {
									cfg = {
										xtype: "combo",
							            selectOnFocus: true,
							            queryMode: "local",
							            store: ["true","false"]
									};
		
								}
		
								//////////// CASE 4 - Type is integer ////////////
		
								else if (type == "integer") {
									cfg = numberEditor;
		
								}
		
								else {
									cfg = textEditor;
								}
		
								col.setEditor(cfg);
		
							}
						}
					}
		
		
		
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/SystemParameters.html";
					window.open( url, "SONE_Help");
	}
	
	,fnCancelSelected: function() {
		
						var dc = this._getDc_("sys");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAll: function() {
		
						var dc = this._getDc_("sys");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isCancelEnabled(dc);
	}
});
