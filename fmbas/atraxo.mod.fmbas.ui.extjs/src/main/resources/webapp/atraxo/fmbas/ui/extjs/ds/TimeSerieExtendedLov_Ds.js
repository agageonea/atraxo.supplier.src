/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.TimeSerieExtendedLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_TimeSerieExtendedLov_Ds"
	},
	
	
	fields: [
		{name:"tsId", type:"int", allowNull:true},
		{name:"serieName", type:"string"},
		{name:"externalSerieName", type:"string"},
		{name:"description", type:"string"},
		{name:"arithmOper", type:"float", allowNull:true},
		{name:"convFctr", type:"string"},
		{name:"decimals", type:"int", allowNull:true},
		{name:"dataProvider", type:"string"},
		{name:"serieType", type:"string"},
		{name:"status", type:"string"},
		{name:"active", type:"boolean"},
		{name:"currency1iD", type:"int", allowNull:true},
		{name:"currency1Code", type:"string"},
		{name:"currency2Id", type:"int", allowNull:true},
		{name:"currency2Code", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"unitTypeInd", type:"string"},
		{name:"unit2Id", type:"int", allowNull:true},
		{name:"unit2Code", type:"string"},
		{name:"unit2TypeInd", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.TimeSerieExtendedLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"tsId", type:"int", allowNull:true},
		{name:"serieName", type:"string"},
		{name:"externalSerieName", type:"string"},
		{name:"description", type:"string"},
		{name:"arithmOper", type:"float", allowNull:true},
		{name:"convFctr", type:"string"},
		{name:"decimals", type:"int", allowNull:true},
		{name:"dataProvider", type:"string"},
		{name:"serieType", type:"string"},
		{name:"status", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"currency1iD", type:"int", allowNull:true},
		{name:"currency1Code", type:"string"},
		{name:"currency2Id", type:"int", allowNull:true},
		{name:"currency2Code", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"unitTypeInd", type:"string"},
		{name:"unit2Id", type:"int", allowNull:true},
		{name:"unit2Code", type:"string"},
		{name:"unit2TypeInd", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
