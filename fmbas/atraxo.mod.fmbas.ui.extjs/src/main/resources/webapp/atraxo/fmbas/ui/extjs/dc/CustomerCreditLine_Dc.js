/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.CustomerCreditLine_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.CustomerCreditLines_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.CustomerCreditLines_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.CustomerCreditLine_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_CustomerCreditLine_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateField({name:"validFrom", dataIndex:"validFrom"})
			.addDateField({name:"validTo", dataIndex:"validTo"})
			.addCombo({ xtype:"combo", name:"creditType", dataIndex:"creditTerm", store:[ __FMBAS_TYPE__.CreditType._CREDIT_LINE_, __FMBAS_TYPE__.CreditType._BANK_GUARANTEE_, __FMBAS_TYPE__.CreditType._PREPAYMENT_]})
			.addNumberField({name:"amount", dataIndex:"amount", sysDec:"dec_unit", maxLength:11})
			.addNumberField({name:"availability", dataIndex:"availability", sysDec:"dec_unit", maxLength:11})
			.addNumberField({name:"alertLimit", dataIndex:"alertPercentage", sysDec:"dec_prc", maxLength:3})
			.addLov({name:"currencyCode", dataIndex:"currencyCode", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"code", dsField: "currencyCodeGrid"} ,{lovField:"id", dsField: "currencyId"} ]}})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.CustomerCreditLine_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_CustomerCreditLine_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"validFrom", dataIndex:"validFrom", width:100, _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"validTo", width:100, _mask_: Masks.DATE})
		.addTextColumn({ name:"creditTerm", dataIndex:"creditTerm", width:120})
		.addNumberColumn({ name:"amount", dataIndex:"amount", width:100, sysDec:"dec_unit"})
		.addNumberColumn({ name:"availability", dataIndex:"availability", width:120, sysDec:"dec_unit"})
		.addTextColumn({ name:"currencyCode", dataIndex:"currencyCodeGrid", width:80})
		.addNumberColumn({ name:"alertLimit", dataIndex:"alertPercentage", width:100, sysDec:"dec_prc"})
		.addBooleanColumn({ name:"active", dataIndex:"active", width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.CustomerCreditLine_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_CustomerCreditLine_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"percent", bind:"{d.percent}", dataIndex:"percent", maxLength:1, labelWidth:20, xtype:"percent", style:"margin-left:5px"})
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", allowBlank:false, width:175, noLabel: true})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", allowBlank:false, width:175, noLabel: true})
		.addCombo({ xtype:"combo", name:"creditType", bind:"{d.creditTerm}", dataIndex:"creditTerm", allowBlank:false, width:175, noLabel: true, store:[ __FMBAS_TYPE__.CreditType._CREDIT_LINE_, __FMBAS_TYPE__.CreditType._BANK_GUARANTEE_, __FMBAS_TYPE__.CreditType._PREPAYMENT_],listeners:{
			blur:{scope:this, fn:function() {this._enableCreditType_(this._get_('creditType'))}}
		}})
		.addNumberField({name:"amount", bind:"{d.amount}", dataIndex:"amount", noEdit:true , allowBlank:false, width:100, noLabel: true, sysDec:"dec_unit", maxLength:11})
		.addLov({name:"currencyCode", bind:"{d.currencyCode}", dataIndex:"currencyCode", allowBlank:false, width:70, noLabel: true, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, style:"margin-left:5px",
			retFieldMapping: [{lovField:"code", dsField: "currencyCodeGrid"} ,{lovField:"id", dsField: "currencyId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ],listeners:{
			blur:{scope:this, fn:function() {this._setDisplayCurrency_(this._get_('currencyCode'))}}
		}})
		.addNumberField({name:"availability", bind:"{d.availability}", dataIndex:"availability", width:100, noLabel: true, sysDec:"dec_unit", maxLength:11})
		.addNumberField({name:"alertLimit", bind:"{d.alertPercentage}", dataIndex:"alertPercentage", allowBlank:false, width:60, noLabel: true, sysDec:"dec_prc", maxLength:3})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active", noLabel: true})
		.addTextField({ name:"currencyDisplay", bind:"{d.currencyDisplay}", dataIndex:"currencyDisplay", noEdit:true , width:70, noLabel: true, maxLength:20, style:"margin-left:5px"})
		.addDisplayFieldText({ name:"creditTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-left:20px; float:right"})
		.addDisplayFieldText({ name:"creditLimitLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-left:20px; float:right"})
		.addDisplayFieldText({ name:"availabilityLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-left:20px; float:right"})
		.addDisplayFieldText({ name:"alertLimitLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-left:20px; float:right"})
		.addDisplayFieldText({ name:"validFromLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-left:20px; float:right"})
		.addDisplayFieldText({ name:"validToLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-left:20px; float:right"})
		.addDisplayFieldText({ name:"activeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-left:20px; float:right"})
		.add({name:"creditLimitCurrency", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("amount"),this._getConfig_("currencyCode")]})
		.add({name:"alertLimitAndPercent", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("alertLimit"),this._getConfig_("percent")]})
		.add({name:"availabilityAndCurrency", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("availability"),this._getConfig_("currencyDisplay")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"tableContainer1", layout: {type:"table"}})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["tableContainer1"])
		.addChildrenTo("tableContainer1", ["table1"])
		.addChildrenTo("table1", ["creditTypeLabel", "creditType", "creditLimitLabel", "creditLimitCurrency", "availabilityLabel", "availabilityAndCurrency", "alertLimitLabel", "alertLimitAndPercent", "validFromLabel", "validFrom", "validToLabel", "validTo", "activeLabel", "active"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableCreditType_: function(el) {
		
						var val = el.getValue();
						var amount = this._get_("amount");
						if (val !== __FMBAS_TYPE__.CreditType._PREPAYMENT_) {
							amount.setValue("");
							amount._enable_();
						}
						else {
							amount.setValue(0);
							amount._disable_();
						}
	},
	
	_setDisplayCurrency_: function(el) {
		
						var val = el.getValue();
						var currencyDisplay = this._get_("currencyDisplay");
						currencyDisplay.setValue(val);
	}
});

/* ================= EDIT FORM: EditContext ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.CustomerCreditLine_Dc$EditContext", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_CustomerCreditLine_Dc$EditContext",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"percent", bind:"{d.percent}", dataIndex:"percent", maxLength:1, labelWidth:20, xtype:"percent", style:"margin-left:5px"})
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", allowBlank:false, width:175, noLabel: true})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", allowBlank:false, width:175, noLabel: true})
		.addCombo({ xtype:"combo", name:"creditType", bind:"{d.creditTerm}", dataIndex:"creditTerm", noEdit:true , allowBlank:false, width:175, noLabel: true, store:[ __FMBAS_TYPE__.CreditType._CREDIT_LINE_, __FMBAS_TYPE__.CreditType._BANK_GUARANTEE_, __FMBAS_TYPE__.CreditType._PREPAYMENT_]})
		.addNumberField({name:"amount", bind:"{d.amount}", dataIndex:"amount", _enableFn_: function(dc, rec) { return dc.record && dc.record.data.creditTerm != __FMBAS_TYPE__.CreditType._PREPAYMENT_; } , allowBlank:false, width:100, noLabel: true, sysDec:"dec_unit", maxLength:11})
		.addLov({name:"currencyCode", bind:"{d.currencyCode}", dataIndex:"currencyCode", allowBlank:false, width:70, noLabel: true, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, style:"margin-left:5px",
			retFieldMapping: [{lovField:"code", dsField: "currencyCodeGrid"} ,{lovField:"id", dsField: "currencyId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ],listeners:{
			fpchange:{scope:this, fn:function() {this._setDisplayCurrency_(this._get_('currencyCode'))}}
		}})
		.addNumberField({name:"availability", bind:"{d.availability}", dataIndex:"availability", width:100, noLabel: true, sysDec:"dec_unit", maxLength:11})
		.addNumberField({name:"alertLimit", bind:"{d.alertPercentage}", dataIndex:"alertPercentage", allowBlank:false, width:60, noLabel: true, sysDec:"dec_prc", maxLength:3})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active", noLabel: true})
		.addTextField({ name:"currencyDisplay", bind:"{d.currencyDisplay}", dataIndex:"currencyDisplay", noEdit:true , width:70, noLabel: true, maxLength:20, style:"margin-left:5px"})
		.addDisplayFieldText({ name:"creditTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-left:20px; float:right"})
		.addDisplayFieldText({ name:"creditLimitLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-left:20px; float:right"})
		.addDisplayFieldText({ name:"availabilityLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-left:20px; float:right"})
		.addDisplayFieldText({ name:"alertLimitLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-left:20px; float:right"})
		.addDisplayFieldText({ name:"validFromLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-left:20px; float:right"})
		.addDisplayFieldText({ name:"validToLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-left:20px; float:right"})
		.addDisplayFieldText({ name:"activeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-left:20px; float:right"})
		.add({name:"creditLimitCurrency", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("amount"),this._getConfig_("currencyCode")]})
		.add({name:"alertLimitAndPercent", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("alertLimit"),this._getConfig_("percent")]})
		.add({name:"availabilityAndCurrency", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("availability"),this._getConfig_("currencyDisplay")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"tableContainer1", layout: {type:"table"}})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["tableContainer1"])
		.addChildrenTo("tableContainer1", ["table1"])
		.addChildrenTo("table1", ["creditTypeLabel", "creditType", "creditLimitLabel", "creditLimitCurrency", "availabilityLabel", "availabilityAndCurrency", "alertLimitLabel", "alertLimitAndPercent", "validFromLabel", "validFrom", "validToLabel", "validTo", "activeLabel", "active"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableCreditType_: function(el) {
		
						var val = el.getValue();
						var amount = this._get_("amount");
						if (val !== __FMBAS_TYPE__.CreditType._PREPAYMENT_) {
							amount.setValue("");
							amount._enable_();
						}
						else {
							amount.setValue(0);
							amount._disable_();
						}
	},
	
	_setDisplayCurrency_: function(el) {
		
						var val = el.getValue();
						var currencyDisplay = this._get_("currencyDisplay");
						currencyDisplay.setValue(val);
	}
});

/* ================= EDIT FORM: CreditHistory ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.CustomerCreditLine_Dc$CreditHistory", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_CustomerCreditLine_Dc$CreditHistory",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"changeReason", bind:"{d.changeReason}", dataIndex:"changeReason", width:400, labelAlign:"top", fieldStyle:"margin-top: 5px"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:400, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["changeReason"]);
	}
});
