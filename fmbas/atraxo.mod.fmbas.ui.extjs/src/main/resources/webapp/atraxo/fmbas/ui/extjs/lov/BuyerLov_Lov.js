/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.BuyerLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_BuyerLov_Lov",
	displayField: "buyerName", 
	_columns_: ["buyerName"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{buyerName}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.fmbas.ui.extjs.ds.SupplierLov_Ds
});
