/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.CustomerGroupLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_CustomerGroupLov_Lov",
	displayField: "code", 
	_columns_: ["code"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{code}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	paramModel: atraxo.fmbas.ui.extjs.ds.CustomerGroupLov_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.CustomerGroupLov_Ds
});
