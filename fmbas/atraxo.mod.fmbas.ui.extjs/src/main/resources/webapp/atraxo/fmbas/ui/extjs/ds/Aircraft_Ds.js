/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Aircraft_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_Aircraft_Ds"
	},
	
	
	validators: {
		registration: [{type: 'presence'}],
		acTypeCode: [{type: 'presence'}],
		validFrom: [{type: 'presence'}],
		validTo: [{type: 'presence'}]
	},
	
	fields: [
		{name:"acTypeId", type:"int", allowNull:true},
		{name:"acTypeCode", type:"string"},
		{name:"custId", type:"int", allowNull:true},
		{name:"custCode", type:"string"},
		{name:"ownerId", type:"int", allowNull:true},
		{name:"ownerCode", type:"string"},
		{name:"operId", type:"int", allowNull:true},
		{name:"operCode", type:"string"},
		{name:"registration", type:"string"},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Aircraft_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"acTypeId", type:"int", allowNull:true},
		{name:"acTypeCode", type:"string"},
		{name:"custId", type:"int", allowNull:true},
		{name:"custCode", type:"string"},
		{name:"ownerId", type:"int", allowNull:true},
		{name:"ownerCode", type:"string"},
		{name:"operId", type:"int", allowNull:true},
		{name:"operCode", type:"string"},
		{name:"registration", type:"string"},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
