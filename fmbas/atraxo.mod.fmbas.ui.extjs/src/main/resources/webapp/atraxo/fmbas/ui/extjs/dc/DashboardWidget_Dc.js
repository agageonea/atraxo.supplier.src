/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.DashboardWidget_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.DashboardWidget_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.DashboardWidget_Ds
});

/* ================= FILTER: DashboardWidgetFilter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.DashboardWidget_Dc$DashboardWidgetFilter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_DashboardWidget_Dc$DashboardWidgetFilter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"widgetName", dataIndex:"widgetName", width:100, maxLength:100})
			.addTextField({ name:"layoutRegionId", dataIndex:"layoutRegionId", maxLength:100})
		;
	}

});

/* ================= EDIT-GRID: DashboardWidgetList ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.DashboardWidget_Dc$DashboardWidgetList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_DashboardWidget_Dc$DashboardWidgetList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"widgetName", dataIndex:"widgetName", width:100, noEdit: true, allowBlank: false, maxLength:100,  flex:1})
		.addTextColumn({name:"layoutRegionId", dataIndex:"layoutRegionId", width:200, noEdit: true, maxLength:100,  flex:1})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
