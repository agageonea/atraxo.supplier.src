/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ExchangeRateToFinancialSourceAndAvgFiltered_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_ExchangeRateToFinancialSourceAndAvgFiltered_Ds"
	},
	
	
	fields: [
		{name:"name", type:"string"},
		{name:"fromCurrencyCode", type:"string"},
		{name:"fromCurrencyId", type:"int", allowNull:true},
		{name:"toCurrencyCode", type:"string"},
		{name:"toCurrencyId", type:"int", allowNull:true},
		{name:"avgMthdId", type:"int", allowNull:true},
		{name:"avgMthdName", type:"string"},
		{name:"avgMthdCode", type:"string"},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceCode", type:"string"},
		{name:"financialSourceName", type:"string"},
		{name:"currencyId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ExchangeRateToFinancialSourceAndAvgFiltered_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"name", type:"string"},
		{name:"fromCurrencyCode", type:"string"},
		{name:"fromCurrencyId", type:"int", allowNull:true},
		{name:"toCurrencyCode", type:"string"},
		{name:"toCurrencyId", type:"int", allowNull:true},
		{name:"avgMthdId", type:"int", allowNull:true},
		{name:"avgMthdName", type:"string"},
		{name:"avgMthdCode", type:"string"},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceCode", type:"string"},
		{name:"financialSourceName", type:"string"},
		{name:"currencyId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
