/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.TimeSerieAvgMthdLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_TimeSerieAvgMthdLov_Ds"
	},
	
	
	fields: [
		{name:"active", type:"boolean"},
		{name:"tserieId", type:"int", allowNull:true},
		{name:"tserieName", type:"string"},
		{name:"averagingMethodId", type:"int", allowNull:true},
		{name:"averagingMethodCode", type:"string"},
		{name:"averagingMethodName", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.TimeSerieAvgMthdLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"active", type:"boolean", allowNull:true},
		{name:"tserieId", type:"int", allowNull:true},
		{name:"tserieName", type:"string"},
		{name:"averagingMethodId", type:"int", allowNull:true},
		{name:"averagingMethodCode", type:"string"},
		{name:"averagingMethodName", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
