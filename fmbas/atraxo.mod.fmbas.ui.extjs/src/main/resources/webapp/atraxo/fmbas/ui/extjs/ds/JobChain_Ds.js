/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.JobChain_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_JobChain_Ds"
	},
	
	
	initRecord: function() {
		this.set("paramIdentifier", "JOBCHAIN");
		this.set("status", "Ready");
	},
	
	fields: [
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"status", type:"string"},
		{name:"nextRunTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"lastRunTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"lastRunResult", type:"string"},
		{name:"enabled", type:"boolean"},
		{name:"paramIdentifier", type:"string", noFilter:true, noSort:true},
		{name:"trigers", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.JobChain_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"status", type:"string"},
		{name:"nextRunTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"lastRunTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"lastRunResult", type:"string"},
		{name:"enabled", type:"boolean", allowNull:true},
		{name:"paramIdentifier", type:"string", noFilter:true, noSort:true},
		{name:"trigers", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
