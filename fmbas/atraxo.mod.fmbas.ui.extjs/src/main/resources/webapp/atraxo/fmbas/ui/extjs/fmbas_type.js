/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
__FMBAS_TYPE__ = {
	ParamType : {
		_NOT_AVAILABLE_ : "Not available" , 
		_A_C_TYPE_ : "A/C type" , 
		_CURRENCY_ : "Currency" , 
		_SUPPLIER_ : "Supplier" , 
		_UPLIFT_STATUS_ : "Uplift status" , 
		_STORAGE_TYPE_ : "Storage type" 
	},
	ToleranceType : {
		_QUANTITY_ : "Quantity" , 
		_DIMENSIONLESS_NUMBER_ : "Dimensionless number" , 
		_PRICE_ : "Price" , 
		_COST_ : "Cost" , 
		_AMOUNT_ : "Amount" 
	},
	PostTypeInd : {
		_AMOUNT_ : "1 [Ref.Currency] = value [Currency]" , 
		_PRICE_ : "1 [Currency] = value [Ref.Currency]" 
	},
	DataProvider : {
		_IMPORT_ARGUS_ : "IMPORT/ARGUS" , 
		_IMPORT_DDS_ : "IMPORT/DDS" , 
		_IMPORT_OPIS_ : "IMPORT/OPIS" , 
		_IMPORT_SHARE_ : "IMPORT/SHARE" , 
		_IMPORT_PLATTS_ : "IMPORT/PLATTS" , 
		_IMPORT_ECB_ : "IMPORT/ECB" , 
		_IMPORT_BLOOMBERG_ : "IMPORT/BLOOMBERG" , 
		_USER_MAINTAINED_ : "User maintained" , 
		_CALCULATED_ : "Calculated" , 
		_OTHER_ : "Other" 
	},
	SerieFreqInd : {
		_DAILY_ : "Daily" , 
		_WEEKDAY_ : "Weekday" , 
		_WEEKLY_ : "Weekly" , 
		_SEMIMONTHLY_ : "Semimonthly" , 
		_MONTHLY_ : "Monthly" , 
		_QUARTERLY_ : "Quarterly" 
	},
	PriceInd : {
		_VOLUME_ : "Volume" , 
		_EVENT_ : "Event" , 
		_COMPOSITE_ : "Composite" , 
		_PERCENT_ : "Percent" 
	},
	UnitType : {
		_MASS_ : "Mass" , 
		_VOLUME_ : "Volume" , 
		_PERCENT_ : "Percent" , 
		_EVENT_ : "Event" , 
		_TEMPERATURE_ : "Temperature" , 
		_DURATION_ : "Duration" , 
		_LENGTH_ : "Length" 
	},
	Operator : {
		_MULTIPLY_ : "Multiply" , 
		_DIVIDE_ : "Divide" 
	},
	TolLimits : {
		_NARROWER_ : "Narrower" , 
		_WIDER_ : "Wider" 
	},
	Use : {
		_TAX_ : "Tax" , 
		_PRODUCT_ : "Product" , 
		_FEE_ : "Fee" 
	},
	Continents : {
		_AFRICA_ : "Africa" , 
		_ANTARCTICA_ : "Antarctica" , 
		_ASIA_ : "Asia" , 
		_OCEANIA_ : "Oceania" , 
		_EUROPE_ : "Europe" , 
		_NORTH_AMERICA_ : "North America" , 
		_SOUTH_AMERICA_ : "South America" 
	},
	Department : {
		_OPERATION_ : "Operation" , 
		_COMMERCIAL_ : "Commercial" 
	},
	Title : {
		_MR__ : "Mr." , 
		_MRS__ : "Mrs." , 
		_MS__ : "Ms." , 
		_MISS_ : "Miss" 
	},
	WorkOffice : {
		_HEAD_OFFICE_ : "Head office" , 
		_BRANCH_ : "Branch" , 
		_AIRPORT_OFFICE_ : "Airport office" 
	},
	CreditType : {
		_CREDIT_LINE_ : "Credit line" , 
		_BANK_GUARANTEE_ : "Bank guarantee" , 
		_PREPAYMENT_ : "Prepayment" 
	},
	ProductSource : {
		_STANDARD_ : "Standard" , 
		_CUSTOM_ : "Custom" 
	},
	PriceType : {
		_PRODUCT_ : "Product" , 
		_DIFFERENTIAL_ : "Differential" , 
		_INTO_PLANE_FEE_ : "Into Plane Fee" , 
		_OTHER_FEE_ : "Other fee" , 
		_TAX_ : "Tax" 
	},
	CustomerType : {
		_INDEPENDENT_ : "Independent" , 
		_GROUP_ : "Group" , 
		_GROUP_MEMBER_ : "Group Member" 
	},
	AreasType : {
		_USER_ : "User" , 
		_SYSTEM_ : "System" 
	},
	WidgetType : {
		_KPI_ : "KPI" , 
		_CHART_ : "Chart" , 
		_LIST_ : "List" 
	},
	UpliftedVolumePeriod : {
		_TODAY_ : "Today" , 
		_YESTERDAY_ : "Yesterday" , 
		_THIS_WEEK_ : "This week" , 
		_LAST_WEEK_ : "Last week" , 
		_THIS_MONTH_ : "This Month" , 
		_LAST_MONTH_ : "Last Month" , 
		_THIS_YEAR_ : "This Year" , 
		_LAST_YEAR_ : "Last Year" 
	},
	GeoType : {
		_LOCATION_ : "Location" , 
		_AREA_ : "Area" 
	},
	MasterAgreementsStatus : {
		_NEGOTIATION_ : "Negotiation" , 
		_EFFECTIVE_ : "Effective" , 
		_ON_HOLD_ : "On hold" , 
		_TERMINATED_ : "Terminated" 
	},
	MasterAgreementsPeriod : {
		_CURRENT_ : "Current" , 
		_PREVIOUS_ : "Previous" , 
		_CURRENT__2_ : "Current -2" , 
		_CURRENT__1_ : "Current +1" 
	},
	PaymentDay : {
		_INVOICE_DATE_ : "Invoice date" , 
		_INVOICE_RECEIVING_DATE_ : "Invoice receiving date" , 
		_LAST_DELIVERY_DATE_ : "Last delivery date" , 
		_FIRST_DELIVERY_DATE_ : "First Delivery Date" , 
		_MIDPOINT_DELIVERY_DATE_ : "Midpoint Delivery Date" 
	},
	InvoiceFreq : {
		_WEEKLY_ : "Weekly" , 
		_SEMIMONTHLY_ : "Semimonthly" , 
		_MONTHLY_ : "Monthly" , 
		_QUARTERLY_ : "Quarterly" , 
		_SEMIANNUALLY_ : "Semiannually" , 
		_ANNUALLY_ : "Annually" , 
		_PER_DELIVERY_ : "Per Delivery" , 
		_DAILY_ : "Daily" , 
		_EVERY_3_DAYS_ : "Every 3 days" , 
		_EVERY_10_DAYS_ : "Every 10 days" , 
		_EVERY_ : "Every" 
	},
	InvoiceType : {
		_XML_2_0_2_ : "XML_2_0_2" , 
		_XML_3_0_0_ : "XML_3_0_0" , 
		_XML_3_0_1_ : "XML_3_0_1" , 
		_XML_3_1_0_ : "XML_3_1_0" , 
		_EXCEL_ : "Excel" , 
		_PAPER_ : "Paper" , 
		_EDI_ : "EDI" , 
		_PDF_FORMAT_ : "PDF Format" , 
		_FISCAL_ : "Fiscal" , 
		_EXPORT_ : "Export" , 
		_REGULAR_ : "Regular" 
	},
	CreditTerm : {
		_OPEN_CREDIT_ : "Open credit" , 
		_CREDIT_LINE_ : "Credit line" , 
		_BANK_GUARANTEE_ : "Bank guarantee" , 
		_PREPAYMENT_ : "Prepayment" 
	},
	ExposureLastAction : {
		_DEBIT_ : "Debit" , 
		_CREDIT_ : "Credit" , 
		_RECONCILIATION_ : "Reconciliation" 
	},
	CustomerStatus : {
		_ACTIVE_ : "Active" , 
		_INACTIVE_ : "Inactive" , 
		_PROSPECT_ : "Prospect" , 
		_BLOCKED_ : "Blocked" 
	},
	TimeSeriesStatus : {
		_WORK_ : "work" , 
		_PUBLISHED_ : "published" 
	},
	TimeSeriesApprovalStatus : {
		_NEW_ : "New" , 
		_APPROVED_ : "Approved" , 
		_REJECTED_ : "Rejected" , 
		_AWAITING_APPROVAL_ : "Awaiting approval" 
	},
	AverageMethodCode : {
		_DC_ : "DC" , 
		_WC_ : "WC" , 
		_WT_ : "WT" , 
		_MC_ : "MC" , 
		_MT_ : "MT" , 
		_SC_ : "SC" , 
		_ST_ : "ST" , 
		_FC_ : "FC" , 
		_FT_ : "FT" 
	},
	DocumentNumberSeriesType : {
		_INVOICE_ : "Invoice" , 
		_CUSTOMER_ : "Customer" , 
		_CREDIT_MEMO_ : "Credit Memo" , 
		_CONSOLIDATED_STATEMENT_ : "Consolidated statement" 
	},
	ValidationMessageSeverity : {
		_INFO_ : "Info" , 
		_WARNING_ : "Warning" , 
		_ERROR_ : "Error" 
	},
	ValueType : {
		_HIGH_PRICE_ : "High price" , 
		_LOW_PRICE_ : "Low price" , 
		_MEAN_PRICE_ : "Mean price" , 
		_USER_CALCULATED_PRICE_ : "User calculated price" 
	},
	TriggerType : {
		_ONCE_ : "Once" , 
		_DAILY_ : "Daily" , 
		_WEEKLY_ : "Weekly" , 
		_MONTHLY_ : "Monthly" 
	},
	TimeInterval : {
		_5_MINUTES_ : "5 minutes" , 
		_10_MINUTES_ : "10 minutes" , 
		_15_MINUTES_ : "15 minutes" , 
		_30_MINUTES_ : "30 minutes" , 
		_1_HOUR_ : "1 hour" 
	},
	RepeatDuration : {
		_INDEFINITELY_ : "Indefinitely" , 
		_15_MINUTES_ : "15 minutes" , 
		_30_MINUTES_ : "30 minutes" , 
		_1_HOUR_ : "1 hour" , 
		_12_HOURS_ : "12 hours" , 
		_1_DAY_ : "1 day" 
	},
	TimeDuration : {
		_30_MINUTES_ : "30 minutes" , 
		_1_HOUR_ : "1 hour" , 
		_2_HOURS_ : "2 hours" , 
		_4_HOURS_ : "4 hours" , 
		_12_HOURS_ : "12 hours" , 
		_1_DAY_ : "1 day" , 
		_3_DAYS_ : "3 days" 
	},
	Months : {
		_ALL_ : "All" , 
		_JANUARY_ : "January" , 
		_FEBRUARY_ : "February" , 
		_MARCH_ : "March" , 
		_APRIL_ : "April" , 
		_MAY_ : "May" , 
		_JUNE_ : "June" , 
		_JULY_ : "July" , 
		_AUGUST_ : "August" , 
		_SEPTEMBER_ : "September" , 
		_OCTOBER_ : "October" , 
		_NOVEMBER_ : "November" , 
		_DECEMBER_ : "December" 
	},
	DaysOfWeek : {
		_MONDAY_ : "Monday" , 
		_TUESDAY_ : "Tuesday" , 
		_WEDNESDAY_ : "Wednesday" , 
		_THURSDAY_ : "Thursday" , 
		_FRIDAY_ : "Friday" , 
		_SATURDAY_ : "Saturday" , 
		_SUNDAY_ : "Sunday" 
	},
	WeekOfMonth : {
		_FIRST_ : "First" , 
		_SECOND_ : "Second" , 
		_THIRD_ : "Third" , 
		_FOURTH_ : "Fourth" , 
		_LAST_ : "Last" 
	},
	DaysOfMonth : {
		_ALL_ : "All" , 
		_1_ : "1" , 
		_2_ : "2" , 
		_3_ : "3" , 
		_4_ : "4" , 
		_5_ : "5" , 
		_6_ : "6" , 
		_7_ : "7" , 
		_8_ : "8" , 
		_9_ : "9" , 
		_10_ : "10" , 
		_11_ : "11" , 
		_12_ : "12" , 
		_13_ : "13" , 
		_14_ : "14" , 
		_15_ : "15" , 
		_16_ : "16" , 
		_17_ : "17" , 
		_18_ : "18" , 
		_19_ : "19" , 
		_20_ : "20" , 
		_21_ : "21" , 
		_22_ : "22" , 
		_23_ : "23" , 
		_24_ : "24" , 
		_25_ : "25" , 
		_26_ : "26" , 
		_27_ : "27" , 
		_28_ : "28" , 
		_29_ : "29" , 
		_30_ : "30" , 
		_31_ : "31" , 
		_LAST_ : "Last" 
	},
	JobStatus : {
		_READY_ : "Ready" , 
		_RUNNING_ : "Running" 
	},
	glyphFont : {
		_FONTAWESOME_ : "fontAwesome" , 
		_FINANCESOLID_ : "financeSolid" 
	},
	NatureOfBusiness : {
		_AIRLINE_ : "Airline" , 
		_RESELLER_ : "Reseller" , 
		_GENERAL_AVIATION_ : "General aviation" , 
		_GOVERNMENT_ : "Government" , 
		_MILITARY_ : "Military" , 
		_OTHERS_ : "Others" 
	},
	InvoiceCostToAccrual : {
		_ON_RELEASE_ : "On release" , 
		_ON_PAYMENT_ : "On payment" 
	},
	LastRequestStatus : {
		_UNKNOWN_ : "Unknown" , 
		_SUCCESS_ : "Success" , 
		_FAILURE_ : "Failure" 
	},
	ExternalInterfacesHistoryStatus : {
		_SUCCESS_ : "Success" , 
		_FAILURE_ : "Failure" , 
		_FAILED_TIMEOUT_ : "Failed-Timeout" , 
		_FAILED_COMMUNICATION_ : "Failed-Communication" , 
		_FAILED_NEGATIVE_ACKNOWLEDGEMENT_ : "Failed-Negative Acknowledgement" 
	},
	ExternalInterfaceMessageHistoryStatus : {
		_EXPORTED_ : "Exported" , 
		_FAILED_ : "Failed" , 
		_ACKNOWLEDGED_ : "Acknowledged" , 
		_FAILED_TIMEOUT_ : "Failed-Timeout" , 
		_FAILED_COMMUNICATION_ : "Failed-Communication" , 
		_FAILED_NEGATIVE_ACKNOWLEDGEMENT_ : "Failed-Negative Acknowledgement" , 
		_SENT_ : "Sent" 
	},
	InterfaceNotificationCondition : {
		_ON_FAILURE_ : "On failure" , 
		_ON_SUCCESS_ : "On success" , 
		_ALL_EVENTS_ : "All events" 
	},
	JobParamType : {
		_STRING_ : "string" , 
		_INTEGER_ : "integer" , 
		_BOOLEAN_ : "boolean" , 
		_DATE_ : "date" , 
		_ENUMERATION_ : "enumeration" , 
		_LOV_ : "lov" , 
		_MASKED_ : "masked" , 
		_ASSIGN_ : "assign" , 
		_KEY_ : "key" , 
		_TEXTAREA_ : "textarea" 
	},
	JobParamAssignType : {
		_CUSTOMER_ : "Customer" , 
		_SUBSIDIARY_ : "Subsidiary" , 
		_AREA_ : "Area" 
	},
	NotificationType : {
		_EMAIL_ : "E-mail message" , 
		_EVENT_ : "Notification event" , 
		_BOTH_ : "Both" 
	},
	ExternalInterfaceType : {
		_INBOUND_ : "Inbound" , 
		_OUTBOUND_ : "Outbound" 
	},
	JobRunResult : {
		_SUCCESS_ : "Success" , 
		_FAILURE_ : "Failure" , 
		_COMPLETED___FAILED_ : "Completed - Failed" , 
		_COMPLETED___SUCCESSFUL_ : "Completed - Successful" 
	},
	JobChainNotificationCondition : {
		_ON_FAILURE_ : "On failure" , 
		_ON_SUCCESS_ : "On success" , 
		_ALL_EVENTS_ : "All events" 
	},
	CustomerDataTransmissionStatus : {
		_NOT_AVAILABLE_ : "Not available" , 
		_IN_PROGRESS_ : "In progress" , 
		_SENT_ : "Sent" , 
		_DELIVERED_ : "Delivered" , 
		_PROCESSED_ : "Processed" , 
		_FAILED_ : "Failed" 
	},
	CustomerDataProcessedStatus : {
		_NOT_AVAILABLE_ : "Not available" , 
		_IN_PROGRESS_ : "In progress" , 
		_SUCCESS_ : "Success" , 
		_FAILED_ : "Failed" 
	},
	CreditUpdateRequestStatus : {
		_NEW_ : "New" , 
		_IN_PROGRESS_ : "In progress" , 
		_FAILED_ : "Failed" , 
		_TRANSMITTED_ : "Transmitted" 
	},
	DictionaryOperator : {
		_Equal_ : "=" , 
		_NotEqual_ : "!=" 
	},
	BusinessType : {
		_SUPPLIER_ : "Supplier" , 
		_AIRLINE_ : "Airline" 
	},
	TransportStatus : {
		_NEW_ : "New" , 
		_SENT_ : "Sent" , 
		_DELIVERED_ : "Delivered" , 
		_PROCESSED_ : "Processed" , 
		_FAILURE_ : "Failure" 
	},
	ProcessStatus : {
		_SUCCESS_ : "Success" , 
		_FAILURE_ : "Failure" 
	},
	WorkflowParamType : {
		_STRING_ : "string" , 
		_INTEGER_ : "integer" , 
		_BOOLEAN_ : "boolean" , 
		_DATE_ : "date" , 
		_ENUMERATION_ : "enumeration" , 
		_LOV_ : "lov" , 
		_MASKED_ : "masked" , 
		_ASSIGN_ : "assign" 
	},
	WorkflowInstanceStatus : {
		_IN_PROGRESS_ : "In progress" , 
		_SUCCESS_ : "Success" , 
		_FAILURE_ : "Failure" , 
		_TERMINATED_ : "Terminated" 
	},
	WorkflowDeployStatus : {
		_DEPLOYED_ : "Deployed" , 
		_NEW_ : "New" 
	},
	WorkflowGroup : {
		_CONTRACT_MANAGEMENT_ : "Contract Management" , 
		_FUEL_TICKETS_ : "Fuel Tickets" , 
		_ACCOUNTING_ : "Accounting" , 
		_PRICING_ : "Pricing" , 
		_TENDER___BID_MANAGEMENT_ : "Tender & Bid Management" 
	},
	WorkflowPeriodGroup : {
		_TODAY_ : "Today" , 
		_YESTERDAY_ : "Yesterday" , 
		_OLDER_ : "Older" 
	},
	FieldChangeType : {
		_INTEGER_ : "Integer" , 
		_BIGDECIMAL_ : "BigDecimal" , 
		_STRING_ : "String" , 
		_DATE_ : "Date" , 
		_LONG_ : "Long" , 
		_BOOLEAN_ : "Boolean" , 
		_BLOB_ : "Blob" 
	},
	FieldChangeName : {
		_VALIDFROM_ : "ValidFrom" , 
		_VALIDTO_ : "ValidTo" , 
		_VOLUME_ : "Volume" 
	},
	ContractChangeType : {
		_PERIOD_ : "Period" , 
		_SHIPTO_ : "ShipTo" , 
		_PRICE_ : "Price" , 
		_ATTACHMENT_ : "Attachment" 
	},
	AccountingRulesType : {
		_EXPORT_DETAILED_COMPOSITE_PRICE_ : "Export detailed composite price" , 
		_REPLACE_ON_INVOICE_EXPORT_THE_INVOICE_RECEIVER_WITH_SHIP_TO_ : "Replace on invoice export the invoice receiver with ship-to" 
	},
	TenderVolumeOffer : {
		_PER_AIRLINE_ : "Per airline" , 
		_PER_LOCATION_ : "Per location" , 
		_UNDEFINED_ : "Undefined" 
	},
	IncomingMessageStatus : {
		_NEW_ : "New" , 
		_PROCESSING_ : "Processing" 
	}
}
