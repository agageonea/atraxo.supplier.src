/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.ExternalInterfaces_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.ExternalInterfaces_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.ExternalInterfaces_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_ExternalInterfaces_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:100})
			.addTextField({ name:"description", dataIndex:"description", maxLength:1000})
			.addBooleanField({ name:"enabled", dataIndex:"enabled"})
			.addDateTimeField({name:"lastRequest", dataIndex:"lastRequest"})
			.addCombo({ xtype:"combo", name:"lastRequestStatus", dataIndex:"lastRequestStatus", store:[]})
			.addCombo({ xtype:"combo", name:"externalInterfaceType", dataIndex:"externalInterfaceType", store:[ __FMBAS_TYPE__.ExternalInterfaceType._INBOUND_, __FMBAS_TYPE__.ExternalInterfaceType._OUTBOUND_]})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ExternalInterfaces_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_ExternalInterfaces_Dc$List",
	_noPaginator_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:120,  flex:1})
		.addTextColumn({ name:"description", dataIndex:"description", width:350})
		.addTextColumn({ name:"externalInterfaceType", dataIndex:"externalInterfaceType", width:160})
		.addBooleanColumn({ name:"enabled", dataIndex:"enabled",  flex:1, renderer:function(val, meta, record, rowIndex) { return this._adjustStatus_(val, meta, record, rowIndex); }})
		.addDateColumn({ name:"lastRequest", dataIndex:"lastRequest", _mask_: Masks.DATETIME,  flex:1})
		.addTextColumn({ name:"lastRequestStatus", dataIndex:"lastRequestStatus", width:50,  flex:1, renderer:function(val, meta, record, rowIndex) { return this._adjustRequestStatus_(val, meta, record, rowIndex); }})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_adjustStatus_: function(val,meta,record,rowIndex) {
		
						var enabled = record.get("enabled");
						var newStatus;
						if (enabled == true) {
							newStatus = "Enabled";
						}
						else {
							newStatus = "Disabled";
						}
						return newStatus;
	},
	
	_adjustRequestStatus_: function(val,meta,record,rowIndex) {
		
						var lastRequestStatus = record.get("lastRequestStatus");
						var glyphColor = "#FFFFFF";
		
						if (lastRequestStatus == __FMBAS_TYPE__.LastRequestStatus._SUCCESS_) {
							glyphColor = "#52AA39";
						}
						else if (lastRequestStatus == __FMBAS_TYPE__.LastRequestStatus._FAILURE_) {
							glyphColor = "#F94F52";
						}
		
						var htmlWrapper = "<div style='display:table; table-layout: fixed'><div style='display:table-cell; width:100px; white-space: nowrap; vertical-align:middle'>"+lastRequestStatus+"</div><div style='display:table-cell; text-align: center; vertical-align:middle; padding-left:15px; '><i class='fa fa-circle' style='font-size:16px; line-height: 16px; color: "+glyphColor+"'></i></div></div>";
						return htmlWrapper;
	}
});
