/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.MainCategories_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.MainCategories_Ds,
		
			/* ================ Business functions ================ */
	
	canDoDelete: function() {
		
					var selRecords = this.selectedRecords;
		
					for (var i = 0; i < selRecords.length; i++) {
		               if (selRecords[i].data.isSys == true) {
						  return false;
					   }
					}
					return  true;
	}

});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.MainCategories_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_MainCategories_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"code", dataIndex:"code", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MainCategoriesLov_Lov", selectOnFocus:true, maxLength:25}})
			.addTextField({ name:"name", dataIndex:"name", maxLength:100})
			.addBooleanField({ name:"isProd", dataIndex:"isProd"})
			.addBooleanField({ name:"isSys", dataIndex:"isSys"})
			.addBooleanField({ name:"active", dataIndex:"active"})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.MainCategories_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_MainCategories_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"code", dataIndex:"code", width:150, allowBlank: false, _enableFn_: this._enableIfNotSys_, maxLength:25, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:25}})
		.addTextColumn({name:"name", dataIndex:"name", width:120, _enableFn_: this._enableIfNotSys_, maxLength:100, 
			editor: { xtype:"textfield", maxLength:100}})
		.addBooleanColumn({name:"isProd", dataIndex:"isProd", width:130, _enableFn_: this._enableIfNotSys_})
		.addBooleanColumn({name:"isSys", dataIndex:"isSys", width:150, noEdit: true})
		.addBooleanColumn({name:"active", dataIndex:"active"})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_enableIfNotSys_: function(dc,record) {
		
						if( record ){
							var isSys 	= record.get("isSys");
							return isSys !== true;
						}
						return false;
	}
});
