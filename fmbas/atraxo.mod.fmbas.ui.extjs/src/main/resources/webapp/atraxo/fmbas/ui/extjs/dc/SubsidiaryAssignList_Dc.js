/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.SubsidiaryAssignList_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.SubsidiaryAssignList_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.SubsidiaryAssignList_Ds
});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.SubsidiaryAssignList_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_SubsidiaryAssignList_Dc$List",
	selType: null,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addBooleanColumn({name:"isSelected", dataIndex:"isSelected", width:50,  hideLabel:"true", listeners:{checkchange: {scope: this, fn: function(el, rowIndex, checked, eOpts) {this._selectDeselectRow_( el, rowIndex, checked, eOpts);}}}})
		.addTextColumn({name:"code", dataIndex:"code", width:150, noEdit: true, maxLength:32})
		.addTextColumn({name:"name", dataIndex:"name", width:150, noEdit: true, maxLength:100})
		.addTextColumn({name:"city", dataIndex:"officeCity", width:150, noEdit: true, maxLength:100})
		.addTextColumn({name:"country", dataIndex:"officeCountryName", width:150, noEdit: true, maxLength:100})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_selectDeselectRow_: function(el,rowIndex,checked,eOpts) {
		
						var view = this.getView();
						if (checked == true) {
							this.getSelectionModel().select(rowIndex, true);
						}
						else {
							this.getSelectionModel().deselect(rowIndex, false);
						}
	}
});
