/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ActionParameter_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_ActionParameter_Ds"
	},
	
	
	fields: [
		{name:"actionId", type:"int", allowNull:true},
		{name:"jobUID", type:"string"},
		{name:"actionName", type:"string"},
		{name:"actionOrder", type:"int", allowNull:true},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"value", type:"string"},
		{name:"businessValue", type:"string"},
		{name:"defaultValue", type:"string"},
		{name:"type", type:"string"},
		{name:"assignType", type:"string"},
		{name:"refValues", type:"string"},
		{name:"refBusinessValues", type:"string"},
		{name:"readOnly", type:"boolean"},
		{name:"subsidiaryId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"customerId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"filePath", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ActionParameter_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"actionId", type:"int", allowNull:true},
		{name:"jobUID", type:"string"},
		{name:"actionName", type:"string"},
		{name:"actionOrder", type:"int", allowNull:true},
		{name:"name", type:"string"},
		{name:"description", type:"string"},
		{name:"value", type:"string"},
		{name:"businessValue", type:"string"},
		{name:"defaultValue", type:"string"},
		{name:"type", type:"string"},
		{name:"assignType", type:"string"},
		{name:"refValues", type:"string"},
		{name:"refBusinessValues", type:"string"},
		{name:"readOnly", type:"boolean", allowNull:true},
		{name:"subsidiaryId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"customerId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"filePath", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
