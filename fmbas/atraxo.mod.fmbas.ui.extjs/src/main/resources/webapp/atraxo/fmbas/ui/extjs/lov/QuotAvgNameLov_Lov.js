/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.QuotAvgNameLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_QuotAvgNameLov_Lov",
	displayField: "avgMethodIndicatorName", 
	_columns_: ["avgMethodIndicatorCode", "avgMethodIndicatorName", "name"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{avgMethodIndicatorName}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.fmbas.ui.extjs.ds.QuotationLov_Ds
});
