/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.WorkflowInstance_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.WorkflowInstance_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.WorkflowInstance_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.WorkflowInstance_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_WorkflowInstance_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:255})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __FMBAS_TYPE__.WorkflowInstanceStatus._IN_PROGRESS_, __FMBAS_TYPE__.WorkflowInstanceStatus._SUCCESS_, __FMBAS_TYPE__.WorkflowInstanceStatus._FAILURE_, __FMBAS_TYPE__.WorkflowInstanceStatus._TERMINATED_]})
			.addCombo({ xtype:"combo", name:"step", dataIndex:"step", store:[]})
			.addCombo({ xtype:"combo", name:"description", dataIndex:"description", store:[]})
			.addTextField({ name:"result", dataIndex:"result", maxLength:1000})
			.addNumberField({name:"workflowVersion", dataIndex:"workflowVersion", maxLength:10})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.WorkflowInstance_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_WorkflowInstance_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"name", dataIndex:"name", width:200, noEdit: true, maxLength:255,  flex:3})
		.addTextColumn({name:"status", dataIndex:"status", width:70, noEdit: true, maxLength:32,  flex:2})
		.addTextColumn({name:"step", dataIndex:"step", width:120, noEdit: true, maxLength:100,  flex:3})
		.addTextColumn({name:"description", dataIndex:"description", width:200, noEdit: true, maxLength:1000,  flex:6})
		.addTextColumn({name:"result", dataIndex:"result", width:200, noEdit: true, maxLength:1000,  flex:5})
		.addDateColumn({name:"startExecutionDate", dataIndex:"startExecutionDate", noEdit: true, _mask_: Masks.DATETIME,  flex:2 })
		.addTextColumn({name:"startExecutionUser", dataIndex:"startExecutionUser", width:200, noEdit: true, maxLength:255,  flex:2})
		.addNumberColumn({name:"workflowVersion", dataIndex:"workflowVersion", noEdit: true, maxLength:10, align:"right",  flex:1 })
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
