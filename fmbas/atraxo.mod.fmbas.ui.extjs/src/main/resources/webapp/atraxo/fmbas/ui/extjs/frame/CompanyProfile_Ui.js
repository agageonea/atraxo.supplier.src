/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.CompanyProfile_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.CompanyProfile_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("profile", Ext.create(atraxo.fmbas.ui.extjs.dc.CompanyProfile_Dc,{}))
		.addDc("bankAccount", Ext.create(atraxo.fmbas.ui.extjs.dc.BankAccount_Dc,{multiEdit: true}))
		.addDc("subsidiary", Ext.create(atraxo.fmbas.ui.extjs.dc.Subsidiary_Dc,{}))
		.addDc("accountingRules", Ext.create(atraxo.fmbas.ui.extjs.dc.AccountingRules_Dc,{multiEdit: true}))
		.linkDc("subsidiary", "profile",{fetchMode:"auto",fields:[
					{childField:"clientId", parentField:"clientId"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"btnSaveNewWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewWdw, scope:this})
		.addButton({name:"btnSaveCloseWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseWdw, scope:this})
		.addButton({name:"btnSaveCloseEditWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseEditWdw, scope:this})
		.addButton({name:"btnOnNewWdwClose",glyph:fp_asc.back_glyph.glyph,iconCls: fp_asc.back_glyph.css, disabled:false, handler: this.onBtnOnNewWdwClose, scope:this})
		.addButton({name:"btnOnEditWdwClose",glyph:fp_asc.back_glyph.glyph,iconCls: fp_asc.back_glyph.css, disabled:false, handler: this.onBtnOnEditWdwClose, scope:this})
		.addButton({name:"btnCancelWithMenu",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"bankAccount", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelWithMenuRules",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"accountingRules", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", handler: this.onBtnCancelSelected, scope:this})
		.addButton({name:"btnCancelSelectedRule",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenuRules", handler: this.onBtnCancelSelectedRule, scope:this})
		.addButton({name:"btnCancelAll",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", _isDefaultHandler_:true, handler: this.onBtnCancelAll, scope:this})
		.addButton({name:"btnCancelAllRules",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenuRules", _isDefaultHandler_:true, handler: this.onBtnCancelAllRules, scope:this})
		.addDcFormView("profile", {name:"Form", xtype:"fmbas_CompanyProfile_Dc$Form"})
		.addDcGridView("subsidiary", {name:"List", _hasTitle_:true, xtype:"fmbas_Subsidiary_Dc$List"})
		.addDcFilterFormView("subsidiary", {name:"Filter", xtype:"fmbas_Subsidiary_Dc$Filter"})
		.addDcFormView("subsidiary", {name:"New", xtype:"fmbas_Subsidiary_Dc$NewAndEdit"})
		.addDcFormView("subsidiary", {name:"Edit", xtype:"fmbas_Subsidiary_Dc$NewAndEdit"})
		.addDcEditGridView("bankAccount", {name:"BankAccount", _hasTitle_:true, xtype:"fmbas_BankAccount_Dc$BankAccountList", frame:true})
		.addDcFilterFormView("bankAccount", {name:"BankAccountFilter", xtype:"fmbas_BankAccount_Dc$Filter"})
		.addDcEditGridView("accountingRules", {name:"AccountingRules", _hasTitle_:true, xtype:"fmbas_AccountingRules_Dc$AccountingRulesList", frame:true})
		.addDcFilterFormView("accountingRules", {name:"AccountingRulesFilter", xtype:"fmbas_AccountingRules_Dc$Filter"})
		.addWindow({name:"wdwNew", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("New")],  listeners:{ close:{fn:this.onNewWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseWdw"), this._elems_.get("btnSaveNewWdw"), this._elems_.get("btnOnNewWdwClose")]}]})
		.addWindow({name:"wdwEdit", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("Edit")],  listeners:{ close:{fn:this.onNewWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseEditWdw"), this._elems_.get("btnOnEditWdwClose")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"stackedCanvas", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"stackedCanvas", containerPanelName:"canvas1", dcName:"profile"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["Form", "stackedCanvas"], ["north", "center"])
		.addChildrenTo("stackedCanvas", ["List"])
		.addToolbarTo("Form", "tlbForm")
		.addToolbarTo("List", "tlbSubsidiary")
		.addToolbarTo("BankAccount", "tlbBankAccount")
		.addToolbarTo("AccountingRules", "tlbAccountingRules");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("stackedCanvas", ["BankAccount", "AccountingRules"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbForm", {dc: "profile"})
			.addButtons([])
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbSubsidiary", {dc: "subsidiary"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbBankAccount", {dc: "bankAccount"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelWithMenu"),this._elems_.get("btnCancelSelected"),this._elems_.get("btnCancelAll")])
			.addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbAccountingRules", {dc: "accountingRules"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelWithMenuRules"),this._elems_.get("btnCancelSelectedRule"),this._elems_.get("btnCancelAllRules")])
			.addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnSaveNewWdw
	 */
	,onBtnSaveNewWdw: function() {
		this.saveNew();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseWdw
	 */
	,onBtnSaveCloseWdw: function() {
		this.saveClose();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseEditWdw
	 */
	,onBtnSaveCloseEditWdw: function() {
		this.saveCloseEdit();
	}
	
	/**
	 * On-Click handler for button btnOnNewWdwClose
	 */
	,onBtnOnNewWdwClose: function() {
		this.onNewWdwClose();
		this._getWindow_("wdwNew").close();
	}
	
	/**
	 * On-Click handler for button btnOnEditWdwClose
	 */
	,onBtnOnEditWdwClose: function() {
		this.onNewWdwClose();
		this._getWindow_("wdwEdit").close();
	}
	
	/**
	 * On-Click handler for button btnCancelSelected
	 */
	,onBtnCancelSelected: function() {
		this.fnCancelSelected();
	}
	
	/**
	 * On-Click handler for button btnCancelSelectedRule
	 */
	,onBtnCancelSelectedRule: function() {
		this.fnCancelSelectedRule();
	}
	
	/**
	 * On-Click handler for button btnCancelAll
	 */
	,onBtnCancelAll: function() {
		this.fnCancelAll();
	}
	
	/**
	 * On-Click handler for button btnCancelAllRules
	 */
	,onBtnCancelAllRules: function() {
		this.fnCancelAllRules();
	}
	
	,saveNew: function() {
		
						var dc = this._getDc_("subsidiary");
						dc.doSave({doNewAfterSave:true});
	}
	
	,saveClose: function() {
		
						var dc = this._getDc_("subsidiary");
						dc.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveCloseEdit: function() {
		
						var dc = this._getDc_("subsidiary");
						dc.doSave({doCloseEditWindowAfterSave: true });
	}
	
	,_afterDefineDcs_: function() {
		
		
						var profile = this._getDc_("profile");
						var subsidiary = this._getDc_("subsidiary");
						var bankAccount = this._getDc_("bankAccount");
						profile.doQuery();
						
						subsidiary.on("afterDoSaveSuccess", function (dc, ajaxResult) { 
							if (ajaxResult.options.options.doNewAfterSave === true) {
								dc.doNew(); 
							} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
								this._getWindow_("wdwNew").close();
							} else if (ajaxResult.options.options.doCloseEditWindowAfterSave === true) {
								this._getWindow_("wdwEdit").close();
							} else if (ajaxResult.options.options.doCloseEditAfterSave === true) {
								this._getWindow_("wdwNew").close();
								dc.doEditIn();
							} 
						}, this);
		
						subsidiary.on("afterDoNew", function (dc) {
							var rec = dc.getRecord();
						    if (rec) {
						        rec.set("isSubsidiary", "1");
								rec.set("isCustomer", "1");
								rec.set("isSupplier", "1");
								rec.set("status","Active");
								rec.set("type","Independent");
								rec.set("iplAgent",true);
								rec.set("fuelSupplier",true);
								rec.set("fixedBaseOperator",true);
						    }
						    this.onShowWdw();
						}, this);
		
						subsidiary.on("onEditIn", function (dc) {
							var win = this._getWindow_("wdwEdit");
							win.show();
						}, this);
		
		                subsidiary.on("afterDoDeleteSuccess", function (dc) {
							bankAccount.doReloadPage();
						}, this);
		
	}
	
	,onShowWdw: function() {
		
						var win = this._getWindow_("wdwNew");
						win.show();
	}
	
	,openHelp: function() {
		
						var url = Main.urlHelp+"/CompanyProfile.html";
						window.open( url, "SONE_Help");
	}
	
	,onNewWdwClose: function() {
		
						var dc = this._getDc_("subsidiary");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,fnCancelEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isCancelEnabled(dc);
	}
	
	,fnCancelSelected: function() {
		
						var dc = this._getDc_("bankAccount");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelSelectedRule: function() {
		
						var dc = this._getDc_("accountingRules");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenuRules");
	}
	
	,fnCancelAll: function() {
		
						var dc = this._getDc_("bankAccount");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAllRules: function() {
		
						var dc = this._getDc_("accountingRules");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenuRules");
	}
});
