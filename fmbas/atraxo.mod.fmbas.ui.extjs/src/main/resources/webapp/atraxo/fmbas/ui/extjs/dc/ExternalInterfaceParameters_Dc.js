/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.ExternalInterfaceParameters_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.ExternalInterfaceParameters_Ds
});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ExternalInterfaceParameters_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_ExternalInterfaceParameters_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"name", dataIndex:"name", width:120, noEdit: true, maxLength:100,  flex:1})
		.addTextColumn({name:"description", dataIndex:"description", width:200, noEdit: true, maxLength:1000,  flex:1})
		.addTextColumn({name:"currentValue", dataIndex:"currentValue", width:200, maxLength:1000,  flex:1, renderer:function(val, meta, record) {return this._maskField_(val, meta, record);}, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addTextColumn({name:"defaultValue", dataIndex:"defaultValue", width:200, noEdit: true, maxLength:1000,  flex:1, renderer:function(val, meta, record) {return this._maskField_(val, meta, record);}})
		.addTextColumn({name:"type", dataIndex:"type", width:70, noEdit: true, maxLength:32,  flex:1})
		.addBooleanColumn({name:"readOnly", dataIndex:"readOnly", noEdit: true,  flex:1})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_maskField_: function(val,meta,record) {
		
		
						var type = record.get("type");
						var maskedValue = val;
						var replaceCharacter = "*";
						if (type == "masked") {
							maskedValue = val.split("");
							var i = 0, l = maskedValue.length;
							for (i; i < l; i++) {
								maskedValue[i] = replaceCharacter;
							}
						}		
						return maskedValue.toString().replace(/,/g,"");
	}
});
