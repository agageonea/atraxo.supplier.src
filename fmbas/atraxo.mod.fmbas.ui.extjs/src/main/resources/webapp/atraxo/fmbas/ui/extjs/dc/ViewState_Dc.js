/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.ViewState_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.ViewState_Ds
});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ViewState_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_ViewState_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:100})
		.addTextColumn({ name:"cmp", dataIndex:"cmp", width:200})
		.addTextColumn({ name:"advancedFilterName", dataIndex:"advancedFilterName", width:100})
		.addTextColumn({ name:"cmpType", dataIndex:"cmpType", width:100})
		.addTextColumn({ name:"value", dataIndex:"value", width:400})
		.addTextColumn({ name:"unitCode", dataIndex:"unitCode", width:60})
		.addTextColumn({ name:"currencyCode", dataIndex:"currencyCode", width:60})
		.addNumberColumn({ name:"pageSize", dataIndex:"pageSize", width:100})
		.addBooleanColumn({ name:"totals", dataIndex:"totals", width:100})
		.addBooleanColumn({ name:"isStandard", dataIndex:"isStandard", width:100})
		.addBooleanColumn({ name:"defaultView", dataIndex:"defaultView", width:100})
		.addBooleanColumn({ name:"availableSystemwide", dataIndex:"availableSystemwide", width:130})
		.addBooleanColumn({ name:"active", dataIndex:"active", width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
