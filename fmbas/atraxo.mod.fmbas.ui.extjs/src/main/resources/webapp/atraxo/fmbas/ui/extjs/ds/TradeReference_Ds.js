/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.TradeReference_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_TradeReference_Ds"
	},
	
	
	initRecord: function() {
		this.set("active", true);
	},
	
	fields: [
		{name:"companyId", type:"int", allowNull:true},
		{name:"companyCode", type:"string"},
		{name:"companyAddress", type:"string"},
		{name:"active", type:"boolean"},
		{name:"clientId", type:"string"},
		{name:"companyName", type:"string"},
		{name:"contactPerson", type:"string"},
		{name:"contactNumber", type:"string"},
		{name:"fax", type:"string"},
		{name:"email", type:"string"},
		{name:"isVerified", type:"boolean"},
		{name:"verificationNotes", type:"string"},
		{name:"verifiedBy", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.TradeReference_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"companyId", type:"int", allowNull:true},
		{name:"companyCode", type:"string"},
		{name:"companyAddress", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"clientId", type:"string"},
		{name:"companyName", type:"string"},
		{name:"contactPerson", type:"string"},
		{name:"contactNumber", type:"string"},
		{name:"fax", type:"string"},
		{name:"email", type:"string"},
		{name:"isVerified", type:"boolean", allowNull:true},
		{name:"verificationNotes", type:"string"},
		{name:"verifiedBy", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
