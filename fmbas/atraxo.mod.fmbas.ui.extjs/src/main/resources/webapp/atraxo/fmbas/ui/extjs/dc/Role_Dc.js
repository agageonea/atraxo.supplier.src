/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Role_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.RoleSupp_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.RoleSupp_Ds
});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Role_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Role_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:120})
		.addTextColumn({ name:"name", dataIndex:"name", width:150})
		.addTextColumn({ name:"description", dataIndex:"description", width:400})
		.addBooleanColumn({ name:"active", dataIndex:"active"})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
