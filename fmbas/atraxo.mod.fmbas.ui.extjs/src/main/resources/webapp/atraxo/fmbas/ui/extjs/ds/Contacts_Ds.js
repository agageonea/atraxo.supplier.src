/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Contacts_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_Contacts_Ds"
	},
	
	
	validators: {
		firstName: [{type: 'presence'}],
		lastName: [{type: 'presence'}],
		countryCode: [{type: 'presence'}],
		email: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("active", true);
	},
	
	fields: [
		{name:"fullName", type:"string", noFilter:true, noSort:true},
		{name:"addressTitle", type:"string", noFilter:true, noSort:true},
		{name:"phoneEmails", type:"string", noFilter:true, noSort:true},
		{name:"countryId", type:"int", allowNull:true},
		{name:"countryName", type:"string"},
		{name:"countryCode", type:"string"},
		{name:"objectId", type:"int", allowNull:true, noFilter:true},
		{name:"objectType", type:"string", noFilter:true},
		{name:"jobTitle", type:"string"},
		{name:"title", type:"string"},
		{name:"firstName", type:"string"},
		{name:"lastName", type:"string"},
		{name:"businessUnit", type:"string"},
		{name:"businessPhone", type:"string"},
		{name:"mobilePhone", type:"string"},
		{name:"faxNumber", type:"string"},
		{name:"email", type:"string"},
		{name:"workOffice", type:"string"},
		{name:"city", type:"string"},
		{name:"street", type:"string"},
		{name:"state", type:"string"},
		{name:"zip", type:"string"},
		{name:"isPrimary", type:"boolean"},
		{name:"active", type:"boolean"},
		{name:"notes", type:"string"},
		{name:"fullNameField", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Contacts_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"fullName", type:"string", noFilter:true, noSort:true},
		{name:"addressTitle", type:"string", noFilter:true, noSort:true},
		{name:"phoneEmails", type:"string", noFilter:true, noSort:true},
		{name:"countryId", type:"int", allowNull:true},
		{name:"countryName", type:"string"},
		{name:"countryCode", type:"string"},
		{name:"objectId", type:"int", allowNull:true, noFilter:true},
		{name:"objectType", type:"string", noFilter:true},
		{name:"jobTitle", type:"string"},
		{name:"title", type:"string"},
		{name:"firstName", type:"string"},
		{name:"lastName", type:"string"},
		{name:"businessUnit", type:"string"},
		{name:"businessPhone", type:"string"},
		{name:"mobilePhone", type:"string"},
		{name:"faxNumber", type:"string"},
		{name:"email", type:"string"},
		{name:"workOffice", type:"string"},
		{name:"city", type:"string"},
		{name:"street", type:"string"},
		{name:"state", type:"string"},
		{name:"zip", type:"string"},
		{name:"isPrimary", type:"boolean", allowNull:true},
		{name:"active", type:"boolean", allowNull:true},
		{name:"notes", type:"string"},
		{name:"fullNameField", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
