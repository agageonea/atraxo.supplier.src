/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.Dashboard_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Dashboard_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("dashboard", Ext.create(atraxo.fmbas.ui.extjs.dc.Dashboard_Dc,{multiEdit: true}))
		.addDc("layout", Ext.create(atraxo.fmbas.ui.extjs.dc.Layout_Dc,{multiEdit: true}))
		.addDc("widget", Ext.create(atraxo.fmbas.ui.extjs.dc.Widget_Dc,{multiEdit: true}))
		.addDc("dashboardWidget", Ext.create(atraxo.fmbas.ui.extjs.dc.DashboardWidget_Dc,{multiEdit: true}))
		.addDc("widgetParameter", Ext.create(atraxo.fmbas.ui.extjs.dc.WidgetParameter_Dc,{multiEdit: true}))
		.addDc("assignedWidgetParameter", Ext.create(atraxo.fmbas.ui.extjs.dc.AssignedWidgetParameter_Dc,{multiEdit: true}))
		.linkDc("dashboardWidget", "dashboard",{fetchMode:"auto",fields:[
					{childField:"dashboardId", parentField:"id"}]})
				.linkDc("widgetParameter", "widget",{fetchMode:"auto",fields:[
					{childField:"widgetId", parentField:"id"}]})
				.linkDc("assignedWidgetParameter", "dashboardWidget",{fetchMode:"auto",fields:[
					{childField:"dashboardWidgetId", parentField:"id"}, {childField:"layoutRegionId", parentField:"layoutRegionId"}, {childField:"layoutId", parentField:"layoutId"}, {childField:"widgetId", parentField:"widgetId"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnWidgetParams",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnWidgetParams,stateManager:[{ name:"selected_one", dc:"assignedWidgetParameter"}], scope:this})
		.addButton({name:"updateParameter",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onUpdateParameter,stateManager:[{ name:"selected_not_zero", dc:"assignedWidgetParameter"}], scope:this})
		.addButton({name:"cancelParameter",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onCancelParameter,stateManager:[{ name:"selected_not_zero", dc:"assignedWidgetParameter"}], scope:this})
		.addDcEditGridView("dashboard", {name:"dashboardList", xtype:"fmbas_Dashboard_Dc$DashboardList", frame:true})
		.addDcEditGridView("layout", {name:"layoutList", xtype:"fmbas_Layout_Dc$LayoutList", frame:true})
		.addDcFilterFormView("dashboard", {name:"dashboardFilter", xtype:"fmbas_Dashboard_Dc$DashboardFilter"})
		.addDcFilterFormView("layout", {name:"layoutFilter", xtype:"fmbas_Layout_Dc$LayoutFilter"})
		.addDcFilterFormView("widget", {name:"widgetFilter", xtype:"fmbas_Widget_Dc$WidgetFilter"})
		.addDcEditGridView("widget", {name:"widgetList", xtype:"fmbas_Widget_Dc$WidgetList", frame:true})
		.addDcEditGridView("dashboardWidget", {name:"dashboardWidgetList", _hasTitle_:true, xtype:"fmbas_DashboardWidget_Dc$DashboardWidgetList", frame:true})
		.addDcFilterFormView("dashboardWidget", {name:"dashboardWidgetFilter", xtype:"fmbas_DashboardWidget_Dc$DashboardWidgetFilter"})
		.addDcEditGridView("widgetParameter", {name:"widgetParameterList", _hasTitle_:true, xtype:"fmbas_WidgetParameter_Dc$WidgetParameterList", frame:true})
		.addDcFilterFormView("widgetParameter", {name:"widgetParameterFilter", xtype:"fmbas_WidgetParameter_Dc$WidgetParameterFilter"})
		.addDcEditGridView("assignedWidgetParameter", {name:"assignedWidgetParameterList", xtype:"fmbas_AssignedWidgetParameter_Dc$AssignedWidgetParameterList", frame:true,  listeners:{ beforeedit: { scope: this, fn: function(editor, ctx) { this._setEditor_(editor, ctx, this._get_('assignedWidgetParameterList'))}}}})
		.addWindow({name:"wdwAssignedParams", _hasTitle_:true, width:816, height:400, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("assignedWidgetParameterList")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("updateParameter"), this._elems_.get("cancelParameter")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"TabbedPanel", xtype:"tabpanel", activeTab:0, plain:false,  cls:"sone-tab-two-levels", deferredRender:true})
		.addPanel({name:"Dashboards", _hasTitle_:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"Layouts", _hasTitle_:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"Widgets", _hasTitle_:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"DashboardsPanel", layout:"card", activeItem:0})
		.addPanel({name:"LayoutsPanel", layout:"card", activeItem:0})
		.addPanel({name:"WidgetsPanel", layout:"card", activeItem:0})
		.addPanel({name:"detailsTab", height:400, xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"widgetDetailsTab", height:400, xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"Dashboards", dcName:"dashboardWidget"})
		
		.addTabPanelForUserFields({tabPanelName:"widgetDetailsTab", containerPanelName:"Widgets", dcName:"widgetParameter"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["TabbedPanel"])
		.addChildrenTo("TabbedPanel", ["Dashboards"])
		.addChildrenTo("Dashboards", ["DashboardsPanel", "detailsTab"], ["center", "south"])
		.addChildrenTo("Layouts", ["LayoutsPanel"], ["center"])
		.addChildrenTo("Widgets", ["WidgetsPanel", "widgetDetailsTab"], ["center", "south"])
		.addChildrenTo("DashboardsPanel", ["dashboardList"])
		.addChildrenTo("LayoutsPanel", ["layoutList"])
		.addChildrenTo("WidgetsPanel", ["widgetList"])
		.addChildrenTo("detailsTab", ["dashboardWidgetList"])
		.addChildrenTo("widgetDetailsTab", ["widgetParameterList"])
		.addToolbarTo("dashboardList", "tlbDashboardList")
		.addToolbarTo("layoutList", "tlbLayoutList")
		.addToolbarTo("widgetList", "tlbWidgetList")
		.addToolbarTo("widgetParameterList", "tlbWidgetParameterList")
		.addToolbarTo("dashboardWidgetList", "tlbDashboardWidgetList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("TabbedPanel", ["Layouts", "Widgets"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbDashboardList", {dc: "dashboard"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbLayoutList", {dc: "layout"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbWidgetList", {dc: "widget"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbWidgetParameterList", {dc: "widgetParameter"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbDashboardWidgetList", {dc: "dashboardWidget"})
			.addButtons([])
			.addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnWidgetParams")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnWidgetParams
	 */
	,onBtnWidgetParams: function() {
		this.showAssignedWidgetParamsWdw();
	}
	
	/**
	 * On-Click handler for button updateParameter
	 */
	,onUpdateParameter: function() {
		this.saveParameter();
	}
	
	/**
	 * On-Click handler for button cancelParameter
	 */
	,onCancelParameter: function() {
		this.onParametersWdwClose();
		this._getWindow_("wdwAssignedParams").close();
	}
	
	,_setEditor_: function(editor,ctx,view) {
		
		
					var fieldIndex = ctx.field;
					var col = ctx.column;
					var cfg = {};
					var textEditor = {xtype:"textfield", maxLength:32};	
					var numberEditor = {xtype:"numberfield", maxLength:32};	
					var field = col.field;
					var storeData = [];
					var dateFormat = "mm/dd/yyy";
					
					var frame = this;
		
					if (fieldIndex == "value") {
						var r = ctx.record;
						if (r) {
		
							var type = r.get("type");
							var refValues = r.get("refValues");
		
							if (!Ext.isEmpty(type)) {
		
								//////////// CASE 1 - Type is _ENUMERATION_ ////////////
		
								if (type == "enumeration") {
									cfg = {
										xtype: "combo",
							            selectOnFocus: true,
							            queryMode: "local",
							            store: refValues.split(",")
									};
		
								}
		
								//////////// CASE 2 - Type is _LOV_ ////////////
		
								else if (type == "lov") {
		
									var retField = "code";
		
									cfg = {
										_dcView_ : view, 
										xtype: refValues, 
										maxLength:32, 
										typeAhead: false,
										remoteFilter: true,
										remoteSort: true,
										forceSelection: true,
										retFieldMapping: [{
											lovField:retField, 
											dsField: "value"}
										]
									};
								}
		
								//////////// CASE 3 - Type is boolean ////////////
		
								else if (type == "boolean") {
									cfg = {
										xtype: "combo",
							            selectOnFocus: true,
							            queryMode: "local",
							            store: ["true","false"]
									};
		
								}
		
								//////////// CASE 4 - Type is integer ////////////
		
								else if (type == "integer") {
									cfg = numberEditor;
		
								}
		
								else {
									cfg = textEditor;
								}
		
								col.setEditor(cfg);
		
							}
						}
					}
		
		
		
	}
	
	,saveParameter: function() {
		 
					var assignedWidgetParameter = this._getDc_("assignedWidgetParameter");
					assignedWidgetParameter.doSave({doCloseParamWindowAfterSave: true });
	}
	
	,onParametersWdwClose: function() {
		
						var dc = this._getDc_("assignedWidgetParameter");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,showAssignedWidgetParamsWdw: function() {
		
					var w = this._getWindow_("wdwAssignedParams");
					w.show();
	}
	
	,_endDefine_: function() {
		
					var paramsDc = this._getDc_("assignedWidgetParameter");
					paramsDc.on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doCloseParamWindowAfterSave === true) {
							this._getWindow_("wdwAssignedParams").close();
						}
					}, this);
	}
});
