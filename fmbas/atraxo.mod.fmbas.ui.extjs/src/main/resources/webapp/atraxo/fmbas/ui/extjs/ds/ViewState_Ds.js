/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ViewState_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_ViewState_Ds"
	},
	
	
	fields: [
		{name:"advancedFilterId", type:"int", allowNull:true},
		{name:"advancedFilterName", type:"string"},
		{name:"standardFilterValue", type:"string"},
		{name:"advancedFilterValue", type:"string"},
		{name:"defaultFilter", type:"boolean"},
		{name:"advancedFilterCmp", type:"string"},
		{name:"name", type:"string"},
		{name:"cmp", type:"string"},
		{name:"cmpType", type:"string"},
		{name:"description", type:"string"},
		{name:"value", type:"string"},
		{name:"notes", type:"string"},
		{name:"pageSize", type:"int", allowNull:true},
		{name:"totals", type:"boolean"},
		{name:"unitCode", type:"string"},
		{name:"currencyCode", type:"string"},
		{name:"isStandard", type:"boolean"},
		{name:"defaultView", type:"boolean"},
		{name:"availableSystemwide", type:"boolean"},
		{name:"active", type:"boolean"},
		{name:"createdBy", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ViewState_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"advancedFilterId", type:"int", allowNull:true},
		{name:"advancedFilterName", type:"string"},
		{name:"standardFilterValue", type:"string"},
		{name:"advancedFilterValue", type:"string"},
		{name:"defaultFilter", type:"boolean", allowNull:true},
		{name:"advancedFilterCmp", type:"string"},
		{name:"name", type:"string"},
		{name:"cmp", type:"string"},
		{name:"cmpType", type:"string"},
		{name:"description", type:"string"},
		{name:"value", type:"string"},
		{name:"notes", type:"string"},
		{name:"pageSize", type:"int", allowNull:true},
		{name:"totals", type:"boolean", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"currencyCode", type:"string"},
		{name:"isStandard", type:"boolean", allowNull:true},
		{name:"defaultView", type:"boolean", allowNull:true},
		{name:"availableSystemwide", type:"boolean", allowNull:true},
		{name:"active", type:"boolean", allowNull:true},
		{name:"createdBy", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
