/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Kpi_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_Kpi_Ds"
	},
	
	
	validators: {
		glyphCode: [{type: 'presence'}],
		glyphFont: [{type: 'presence'}],
		title: [{type: 'presence'}],
		active: [{type: 'presence'}],
		kpiOrder: [{type: 'presence'}]
	},
	
	fields: [
		{name:"glyphCode", type:"string"},
		{name:"glyphFont", type:"string"},
		{name:"title", type:"string"},
		{name:"titleEn", type:"string"},
		{name:"titleDe", type:"string"},
		{name:"titleJp", type:"string"},
		{name:"titleEs", type:"string"},
		{name:"titleFr", type:"string"},
		{name:"titleAr", type:"string"},
		{name:"active", type:"boolean"},
		{name:"downLimit", type:"int", allowNull:true},
		{name:"upLimit", type:"int", allowNull:true},
		{name:"description", type:"string"},
		{name:"createdBy", type:"string"},
		{name:"dsName", type:"string"},
		{name:"methodName", type:"string"},
		{name:"reverse", type:"boolean"},
		{name:"unit", type:"string"},
		{name:"kpiOrder", type:"int", allowNull:true},
		{name:"frameId", type:"string"},
		{name:"frameTitle", type:"string"},
		{name:"frameBundle", type:"string"},
		{name:"frameName", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Kpi_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"glyphCode", type:"string"},
		{name:"glyphFont", type:"string"},
		{name:"title", type:"string"},
		{name:"titleEn", type:"string"},
		{name:"titleDe", type:"string"},
		{name:"titleJp", type:"string"},
		{name:"titleEs", type:"string"},
		{name:"titleFr", type:"string"},
		{name:"titleAr", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"downLimit", type:"int", allowNull:true},
		{name:"upLimit", type:"int", allowNull:true},
		{name:"description", type:"string"},
		{name:"createdBy", type:"string"},
		{name:"dsName", type:"string"},
		{name:"methodName", type:"string"},
		{name:"reverse", type:"boolean", allowNull:true},
		{name:"unit", type:"string"},
		{name:"kpiOrder", type:"int", allowNull:true},
		{name:"frameId", type:"string"},
		{name:"frameTitle", type:"string"},
		{name:"frameBundle", type:"string"},
		{name:"frameName", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
