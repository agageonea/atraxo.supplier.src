/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.AssignedWidgetParameter_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.AssignedWidgetParameter_Ds
});

/* ================= EDIT-GRID: AssignedWidgetParameterList ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.AssignedWidgetParameter_Dc$AssignedWidgetParameterList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_AssignedWidgetParameter_Dc$AssignedWidgetParameterList",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"name", dataIndex:"name", width:50, noEdit: true, allowBlank: false, maxLength:32,  flex:1})
		.addComboColumn({name:"type", dataIndex:"type", width:70, noEdit: true, allowBlank: false,  flex:1})
		.addTextColumn({name:"value", dataIndex:"value", width:50, maxLength:100,  flex:1, 
			editor: { xtype:"textfield", maxLength:100}})
		.addTextColumn({name:"defaultValue", dataIndex:"defaultValue", width:50, noEdit: true, allowBlank: false, maxLength:50,  flex:1})
		.addBooleanColumn({name:"readOnly", dataIndex:"readOnly", noEdit: true, allowBlank: false,  flex:1})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
