/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.EnergyPrice_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Quotation_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.EnergyPrice_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_EnergyPrice_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"name", dataIndex:"name", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.QuotationLov_Lov", selectOnFocus:true, maxLength:100}})
			.addLov({name:"currency", dataIndex:"currency1Code", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3}})
			.addLov({name:"unit", dataIndex:"unitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MeasurmentUnitsLov_Lov", selectOnFocus:true, maxLength:2}})
			.addLov({name:"avgMethodIndicatorName", dataIndex:"avgMethodIndicatorName", maxLength:50,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.AvgMethLov_Lov", selectOnFocus:true, maxLength:50,
					retFieldMapping: [{lovField:"id", dsField: "avgId"} ,{lovField:"name", dsField: "avgMethodIndicatorName"} ]}})
			.addDateField({name:"validFrom", dataIndex:"firstUsage"})
			.addDateField({name:"validTo", dataIndex:"lastUsage"})
			.addCombo({ xtype:"combo", name:"valueType", dataIndex:"valueType", store:[ __FMBAS_TYPE__.ValueType._HIGH_PRICE_, __FMBAS_TYPE__.ValueType._LOW_PRICE_, __FMBAS_TYPE__.ValueType._MEAN_PRICE_, __FMBAS_TYPE__.ValueType._USER_CALCULATED_PRICE_]})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.EnergyPrice_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_EnergyPrice_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:150})
		.addTextColumn({ name:"currency", dataIndex:"currency1Code", width:120})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:100})
		.addTextColumn({ name:"avgMethod", dataIndex:"avgMethodIndicatorName", width:200})
		.addDateColumn({ name:"validFrom", dataIndex:"firstUsage", _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"lastUsage", _mask_: Masks.DATE})
		.addTextColumn({ name:"valueType", dataIndex:"valueType", width:70})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.EnergyPrice_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_EnergyPrice_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", noUpdate:true, maxLength:100, labelWidth:120})
		.addTextField({ name:"avgMethod", bind:"{d.avgMethodIndicatorName}", dataIndex:"avgMethodIndicatorName", noUpdate:true, maxLength:50, labelWidth:120})
		.addTextField({ name:"currency", bind:"{d.currency1Code}", dataIndex:"currency1Code", noUpdate:true, width:190, maxLength:3, labelWidth:120})
		.addTextField({ name:"unit", bind:"{d.unitCode}", dataIndex:"unitCode", noUpdate:true, width:60, noLabel: true, maxLength:2})
		.addDateField({name:"validFrom", bind:"{d.firstUsage}", dataIndex:"firstUsage", noUpdate:true})
		.addDateField({name:"validTo", bind:"{d.lastUsage}", dataIndex:"lastUsage", noUpdate:true})
		.addCombo({ xtype:"combo", name:"valueType", bind:"{d.valueType}", dataIndex:"valueType", noUpdate:true, store:[ __FMBAS_TYPE__.ValueType._HIGH_PRICE_, __FMBAS_TYPE__.ValueType._LOW_PRICE_, __FMBAS_TYPE__.ValueType._MEAN_PRICE_, __FMBAS_TYPE__.ValueType._USER_CALCULATED_PRICE_], labelWidth:145})
		.add({name:"nameValf", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("name"),this._getConfig_("validFrom")]})
		.add({name:"avgValf", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("avgMethod"),this._getConfig_("validTo")]})
		.add({name:"currUnit", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("currency"),this._getConfig_("unit"),this._getConfig_("valueType")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:600, layout:"anchor"})
		.addPanel({ name:"col2", width:600, layout:"anchor"})
		.addPanel({ name:"col3", width:600, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3"])
		.addChildrenTo("col1", ["nameValf"])
		.addChildrenTo("col2", ["avgValf"])
		.addChildrenTo("col3", ["currUnit"]);
	}
});
