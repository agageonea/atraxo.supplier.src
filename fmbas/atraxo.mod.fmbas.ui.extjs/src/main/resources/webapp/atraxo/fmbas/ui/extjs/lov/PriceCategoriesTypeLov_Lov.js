/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.PriceCategoriesTypeLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_PriceCategoriesTypeLov_Lov",
	displayField: "type", 
	_columns_: ["name"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{name}, {type}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.fmbas.ui.extjs.ds.PriceCategoriesLov_Ds
});
