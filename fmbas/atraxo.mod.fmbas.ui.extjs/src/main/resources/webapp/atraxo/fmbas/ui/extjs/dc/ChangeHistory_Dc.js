/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.ChangeHistory_Ds
});

/* ================= GRID: ListReason ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc$ListReason", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_ChangeHistory_Dc$ListReason",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"createdAt", dataIndex:"createdAt", width:120, _mask_: Masks.DATETIME})
		.addTextColumn({ name:"objectValue", dataIndex:"objectValue", width:200})
		.addTextColumn({ name:"remarks", dataIndex:"remarks", width:340})
		.addTextColumn({ name:"createdBy", dataIndex:"createdBy", width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_ChangeHistory_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{d.remarks}", dataIndex:"remarks", width:400, labelAlign:"top", fieldStyle:"margin-top: 5px"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:400, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
						remarks.setReadOnly(false);
	}
});

/* ================= EDIT FORM: EditRemark ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc$EditRemark", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_ChangeHistory_Dc$EditRemark",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{d.remarks}", dataIndex:"remarks", width:400, labelAlign:"top", fieldStyle:"margin-top: 5px"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:400, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
						remarks.setReadOnly(false);
	}
});
