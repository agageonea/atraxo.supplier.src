/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.SubsidiaryAssignList_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_SubsidiaryAssignList_Ds"
	},
	
	
	fields: [
		{name:"officeCountryId", type:"int", allowNull:true},
		{name:"officeCountryCode", type:"string"},
		{name:"officeCountryName", type:"string"},
		{name:"officeStateId", type:"int", allowNull:true},
		{name:"officeStateCode", type:"string"},
		{name:"officeStateName", type:"string"},
		{name:"officeCity", type:"string"},
		{name:"name", type:"string"},
		{name:"code", type:"string"},
		{name:"isSelected", type:"boolean", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.SubsidiaryAssignList_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"officeCountryId", type:"int", allowNull:true},
		{name:"officeCountryCode", type:"string"},
		{name:"officeCountryName", type:"string"},
		{name:"officeStateId", type:"int", allowNull:true},
		{name:"officeStateCode", type:"string"},
		{name:"officeStateName", type:"string"},
		{name:"officeCity", type:"string"},
		{name:"name", type:"string"},
		{name:"code", type:"string"},
		{name:"isSelected", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.fmbas.ui.extjs.ds.SubsidiaryAssignList_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"userCode", type:"string"},
		{name:"userId", type:"string"}
	]
});
