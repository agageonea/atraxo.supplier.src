/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.AirlinesLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_AirlinesLov_Ds"
	},
	
	
	fields: [
		{name:"id", type:"int", allowNull:true},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"natureOfBusiness", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.AirlinesLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	initFilter: function() {
		this.set("natureOfBusiness", "Airline");
	},
	
	fields: [
		{name:"id", type:"int", allowNull:true},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"natureOfBusiness", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
