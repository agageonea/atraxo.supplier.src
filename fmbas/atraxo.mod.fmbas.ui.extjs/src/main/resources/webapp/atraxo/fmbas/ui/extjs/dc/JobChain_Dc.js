/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.JobChain_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.JobChain_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.JobChain_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_JobChain_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:32})
			.addTextField({ name:"description", dataIndex:"description", maxLength:255})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __FMBAS_TYPE__.JobStatus._READY_, __FMBAS_TYPE__.JobStatus._RUNNING_]})
		;
	}

});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.JobChain_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_JobChain_Dc$EditList",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"name", dataIndex:"name", width:50, maxLength:32,  flex:1, 
			editor: { xtype:"textfield", maxLength:32}})
		.addTextColumn({name:"description", dataIndex:"description", width:300, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"status", dataIndex:"status", width:100, noEdit: true, maxLength:50,  flex:1})
		.addDateColumn({name:"nextRunTime", dataIndex:"nextRunTime", noEdit: true, _mask_: Masks.DATETIME,  flex:1 })
		.addDateColumn({name:"lastRunTime", dataIndex:"lastRunTime", noEdit: true, _mask_: Masks.DATETIME,  flex:1 })
		.addTextColumn({name:"lastRunResult", dataIndex:"lastRunResult", width:70, noEdit: true, maxLength:32,  flex:1, renderer:function(val, meta, record, rowIndex) { return this._formatResult_(val, meta, record, rowIndex); }})
		.addBooleanColumn({name:"enabled", dataIndex:"enabled", noEdit: true,  flex:1, renderer:function(val, meta, record, rowIndex) { return this._adjustStatus_(val, meta, record, rowIndex); }})
		.addTextColumn({name:"trigger", dataIndex:"trigers", hidden:true, width:50, maxLength:2500,  flex:1, 
			editor: { xtype:"textfield", maxLength:2500}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_adjustStatus_: function(val,meta,record,rowIndex) {
		
						var enabled = record.get("enabled");
						var newStatus;
						if (enabled == true) {
							newStatus = "Yes";
						}
						else {
							newStatus = "No";
						}
						//this.enableFinalCust(newStatus);
						return newStatus;
	},
	
	_formatResult_: function(val,meta,record,rowIndex) {
		
						var lastRunResult = record.get("lastRunResult");
						var glyphColor = "#FFFFFF";
			
						if (lastRunResult == __FMBAS_TYPE__.JobRunResult._SUCCESS_ || lastRunResult == __FMBAS_TYPE__.JobRunResult._COMPLETED___SUCCESSFUL_) {
							glyphColor = "#52AA39";
						}
						else if (lastRunResult == __FMBAS_TYPE__.JobRunResult._FAILURE_ || lastRunResult == __FMBAS_TYPE__.JobRunResult._COMPLETED___FAILED_ ) {
							glyphColor = "#F94F52";
						}
		
						var htmlWrapper = "<div style='display:table; table-layout: fixed'><div style='display:table-cell; text-align: center; vertical-align:middle; '><i class='fa fa-circle' style='font-size:16px; line-height: 16px; color: "+glyphColor+"'></i></div><div style='display:table-cell; width:100px; white-space: nowrap; padding-left:15px; vertical-align:middle'>"+lastRunResult+"</div></div>";
						return htmlWrapper;
	}
});
