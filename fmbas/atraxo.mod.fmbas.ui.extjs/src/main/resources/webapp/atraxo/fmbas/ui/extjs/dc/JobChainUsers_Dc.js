/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.JobChainUsers_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.JobChainUsers_Ds
});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.JobChainUsers_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_JobChainUsers_Dc$List",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"userName", dataIndex:"userName", width:200, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_UserNameLov_Lov", allowBlank:false, maxLength:255,
				retFieldMapping: [{lovField:"id", dsField: "userId"} ,{lovField:"email", dsField: "userEmail"} ,{lovField:"loginName", dsField: "loginName"} ]},  flex:1})
		.addTextColumn({name:"userEmail", dataIndex:"userEmail", width:100, noEdit: true, allowBlank: false, maxLength:128,  flex:1})
		.addComboColumn({name:"notificationType", dataIndex:"notificationType", width:70, allowBlank: false, 
			editor:{xtype:"combo", mode: 'local', allowBlank:false, triggerAction:'all', forceSelection:true, store:[ __FMBAS_TYPE__.NotificationType._EMAIL_, __FMBAS_TYPE__.NotificationType._EVENT_, __FMBAS_TYPE__.NotificationType._BOTH_]},  flex:1})
		.addComboColumn({name:"notificationCondition", dataIndex:"notificationCondition", width:70, allowBlank: false, 
			editor:{xtype:"combo", mode: 'local', allowBlank:false, triggerAction:'all', forceSelection:true, store:[ __FMBAS_TYPE__.JobChainNotificationCondition._ON_FAILURE_, __FMBAS_TYPE__.JobChainNotificationCondition._ON_SUCCESS_, __FMBAS_TYPE__.JobChainNotificationCondition._ALL_EVENTS_]},  flex:1})
		.addBooleanColumn({name:"enabled", dataIndex:"enabled", width:120})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
