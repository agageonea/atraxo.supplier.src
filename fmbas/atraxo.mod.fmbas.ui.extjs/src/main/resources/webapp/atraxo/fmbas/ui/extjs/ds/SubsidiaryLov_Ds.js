/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.SubsidiaryLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_SubsidiaryLov_Ds"
	},
	
	
	fields: [
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"type", type:"string"},
		{name:"refid", type:"string"},
		{name:"status", type:"string"},
		{name:"isCustomer", type:"boolean"},
		{name:"isSubsidiary", type:"boolean"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.SubsidiaryLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"type", type:"string"},
		{name:"refid", type:"string"},
		{name:"status", type:"string"},
		{name:"isCustomer", type:"boolean", allowNull:true},
		{name:"isSubsidiary", type:"boolean", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
