/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Dashboard_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Dashboard_Ds
});

/* ================= FILTER: DashboardFilter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Dashboard_Dc$DashboardFilter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Dashboard_Dc$DashboardFilter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", allowBlank:false, width:100, maxLength:100})
			.addTextField({ name:"description", dataIndex:"description", width:200, maxLength:1000})
		;
	}

});

/* ================= EDIT-GRID: DashboardList ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Dashboard_Dc$DashboardList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_Dashboard_Dc$DashboardList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"name", dataIndex:"name", width:200, allowBlank: false, maxLength:100, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:100}})
		.addTextColumn({name:"description", dataIndex:"description", width:200, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addLov({name:"layoutName", dataIndex:"layoutName", width:200, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_LayoutLov_Lov", maxLength:100,
				retFieldMapping: [{lovField:"id", dsField: "layoutId"} ]}})
		.addBooleanColumn({name:"isDefault", dataIndex:"isDefault", width:100})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
