/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Actions_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Action_Ds
});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Actions_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_Actions_Dc$EditList",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"batchJob", dataIndex:"batchJobName", width:200, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_BatchJobsLov_Lov", allowBlank:false, maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "batchJobId"} ]}})
		.addTextColumn({name:"description", dataIndex:"description", width:300, maxLength:255, 
			editor: { xtype:"textfield", maxLength:255}})
		.addTextColumn({name:"name", dataIndex:"name", hidden:true, width:200, maxLength:32, 
			editor: { xtype:"textfield", maxLength:32}})
		.addNumberColumn({name:"order", dataIndex:"order", hidden:true, maxLength:2, align:"right" })
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Actions_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Actions_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:32})
			.addTextField({ name:"description", dataIndex:"description", maxLength:255})
			.addLov({name:"batchJob", dataIndex:"batchJobName", allowBlank:false, maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.BatchJobsLov_Lov", selectOnFocus:true, allowBlank:false, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "batchJobId"} ]}})
		;
	}

});
