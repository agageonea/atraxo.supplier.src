/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Quotation_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_Quotation_Ds"
	},
	
	
	validators: {
		name: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("active", true);
	},
	
	fields: [
		{name:"convFctr", type:"string"},
		{name:"source", type:"string"},
		{name:"tsId", type:"int", allowNull:true, noFilter:true},
		{name:"tsName", type:"string"},
		{name:"arithmOper", type:"float", allowNull:true},
		{name:"currency1iD", type:"int", allowNull:true, noFilter:true},
		{name:"currency1Code", type:"string"},
		{name:"currency2Id", type:"int", allowNull:true, noFilter:true},
		{name:"currency2Code", type:"string"},
		{name:"avgId", type:"int", allowNull:true, noFilter:true},
		{name:"avgMethodIndicatorName", type:"string"},
		{name:"avgMethodIndicatorCode", type:"string"},
		{name:"name", type:"string"},
		{name:"active", type:"boolean"},
		{name:"unitId", type:"int", allowNull:true, noFilter:true},
		{name:"unitCode", type:"string"},
		{name:"unitTypeInd", type:"string"},
		{name:"unit2Id", type:"int", allowNull:true, noFilter:true},
		{name:"unit2Code", type:"string"},
		{name:"unit2TypeInd", type:"string"},
		{name:"calcVal", type:"string"},
		{name:"valueType", type:"string"},
		{name:"firstUsage", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"lastUsage", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Quotation_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"convFctr", type:"string"},
		{name:"source", type:"string"},
		{name:"tsId", type:"int", allowNull:true, noFilter:true},
		{name:"tsName", type:"string"},
		{name:"arithmOper", type:"float", allowNull:true},
		{name:"currency1iD", type:"int", allowNull:true, noFilter:true},
		{name:"currency1Code", type:"string"},
		{name:"currency2Id", type:"int", allowNull:true, noFilter:true},
		{name:"currency2Code", type:"string"},
		{name:"avgId", type:"int", allowNull:true, noFilter:true},
		{name:"avgMethodIndicatorName", type:"string"},
		{name:"avgMethodIndicatorCode", type:"string"},
		{name:"name", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"unitId", type:"int", allowNull:true, noFilter:true},
		{name:"unitCode", type:"string"},
		{name:"unitTypeInd", type:"string"},
		{name:"unit2Id", type:"int", allowNull:true, noFilter:true},
		{name:"unit2Code", type:"string"},
		{name:"unit2TypeInd", type:"string"},
		{name:"calcVal", type:"string"},
		{name:"valueType", type:"string"},
		{name:"firstUsage", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"lastUsage", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
