/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.Geography_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Geography_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("country", Ext.create(atraxo.fmbas.ui.extjs.dc.Country_Dc,{multiEdit: true}))
		.addDc("area", Ext.create(atraxo.fmbas.ui.extjs.dc.Areas_Dc,{multiEdit: true, trackEditMode: true}))
		.addDc("locations", Ext.create(atraxo.fmbas.ui.extjs.dc.Locations_Dc,{}))
		.addDc("location", Ext.create(atraxo.fmbas.ui.extjs.dc.Locations_Dc,{trackEditMode: true}))
		.addDc("contacts", Ext.create(atraxo.fmbas.ui.extjs.dc.Contacts_Dc,{}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.linkDc("locations", "area",{fetchMode:"auto",fields:[
					{childParam:"areaId", parentField:"id"}]})
				.linkDc("contacts", "location",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("note", "location",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"countriesHelpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onCountriesHelpWdw, scope:this})
		.addButton({name:"countriesBtnCancelWithMenu",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"country", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"countriesBtnCancelSelected",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"countriesBtnCancelWithMenu", handler: this.onCountriesBtnCancelSelected, scope:this})
		.addButton({name:"countriesBtnCancelAll",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"countriesBtnCancelWithMenu", _isDefaultHandler_:true, handler: this.onCountriesBtnCancelAll, scope:this})
		.addButton({name:"areasAssignLocations", disabled:false, handler: this.onAreasAssignLocations, scope:this})
		.addButton({name:"areasHelpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onAreasHelpWdw, scope:this})
		.addButton({name:"areasHelpWdw1",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onAreasHelpWdw1, scope:this})
		.addButton({name:"areasBtnCancelWithMenu",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"area", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"areasBtnCancelSelected",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"areasBtnCancelWithMenu", handler: this.onAreasBtnCancelSelected, scope:this})
		.addButton({name:"areasBtnCancelAll",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"areasBtnCancelWithMenu", _isDefaultHandler_:true, handler: this.onAreasBtnCancelAll, scope:this})
		.addButton({name:"btnOnEditWdwClose",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnOnEditWdwClose, scope:this})
		.addButton({name:"btnSaveCloseWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseWdw, scope:this})
		.addButton({name:"btnSaveNewWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewWdw, scope:this})
		.addButton({name:"btnSaveEditWdw",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnSaveEditWdw, scope:this})
		.addButton({name:"btnSaveNewContactWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewContactWdw, scope:this})
		.addButton({name:"btnSaveCloseContactWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseContactWdw, scope:this})
		.addButton({name:"btnSaveCloseNoteWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseNoteWdw, scope:this})
		.addButton({name:"btnCloseContactWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseContactWdw, scope:this})
		.addButton({name:"btnCloseNoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseNoteWdw, scope:this})
		.addButton({name:"helpLocationWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpLocationWdw, scope:this})
		.addButton({name:"helpLocationWdw1",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpLocationWdw1, scope:this})
		.addDcFilterFormView("country", {name:"cuFilter", xtype:"fmbas_Country_Dc$Filter"})
		.addDcEditGridView("country", {name:"cuList", _hasTitle_:true, xtype:"fmbas_Country_Dc$List", frame:true})
		.addDcFilterFormView("area", {name:"aFilter", xtype:"fmbas_Areas_Dc$Filter"})
		.addDcEditGridView("area", {name:"aList", xtype:"fmbas_Areas_Dc$List", frame:true})
		.addDcFormView("area", {name:"aEdit", xtype:"fmbas_Areas_Dc$Edit"})
		.addDcGridView("locations", {name:"locList", height:400, xtype:"fmbas_Locations_Dc$AreasLocation"})
		.addDcFilterFormView("location", {name:"locationFilter", xtype:"fmbas_Locations_Dc$Filter"})
		.addDcGridView("location", {name:"locationList", xtype:"fmbas_Locations_Dc$ListPopUp"})
		.addDcFormView("location", {name:"locationEdit", xtype:"fmbas_Locations_Dc$Edit"})
		.addDcFormView("location", {name:"locationEditDetails", xtype:"fmbas_Locations_Dc$Edit"})
		.addDcGridView("location", {name:"airportInfList", _hasTitle_:true, xtype:"fmbas_Locations_Dc$Temp",  disabled:true})
		.addDcGridView("location", {name:"notamList", _hasTitle_:true, xtype:"fmbas_Locations_Dc$Temp",  disabled:true})
		.addDcGridView("location", {name:"taxesFeesList", _hasTitle_:true, xtype:"fmbas_Locations_Dc$Temp",  disabled:true})
		.addDcFilterFormView("contacts", {name:"contactFilter", xtype:"fmbas_Contacts_Dc$Filter"})
		.addDcGridView("contacts", {name:"contactsList", _hasTitle_:true, xtype:"fmbas_Contacts_Dc$List"})
		.addDcFormView("contacts", {name:"contactEdit", xtype:"fmbas_Contacts_Dc$Edit"})
		.addDcFormView("contacts", {name:"contactEditContext", xtype:"fmbas_Contacts_Dc$Edit"})
		.addDcGridView("note", {name:"notesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcFormView("note", {name:"notesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false,  cls:"sone-tab-two-levels"})
		.addPanel({name:"areasCanvas", _hasTitle_:true, layout:"card", activeItem:0})
		.addPanel({name:"areasElement1", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"areasElement2", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"locationCanvas", _hasTitle_:true, layout:"card", activeItem:0})
		.addPanel({name:"locationElement1", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"locationElement2", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"locationElement3", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addWindow({name:"wdwLocation", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("locationEdit")],  listeners:{ close:{fn:this.onLocationWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewWdw"), this._elems_.get("btnSaveCloseWdw"), this._elems_.get("btnSaveEditWdw"), this._elems_.get("btnOnEditWdwClose")]}]})
		.addWindow({name:"noteWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("notesEdit")],  listeners:{ close:{fn:this.onNoteWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseNoteWdw"), this._elems_.get("btnCloseNoteWdw")]}]})
		.addWindow({name:"contactWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("contactEdit")],  listeners:{ close:{fn:this.onContactWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewContactWdw"), this._elems_.get("btnSaveCloseContactWdw"), this._elems_.get("btnCloseContactWdw")]}]})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"locationElement2", dcName:"location"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["cuList"])
		.addChildrenTo("areasCanvas", ["areasElement1", "areasElement2"])
		.addChildrenTo("areasElement1", ["aList"], ["center"])
		.addChildrenTo("areasElement2", ["aEdit", "locList"], ["north", "center"])
		.addChildrenTo("locationCanvas", ["locationElement1", "locationElement2", "locationElement3"])
		.addChildrenTo("locationElement1", ["locationList"], ["center"])
		.addChildrenTo("locationElement2", ["locationEditDetails", "detailsTab"], ["north", "center"])
		.addChildrenTo("locationElement3", ["contactEditContext"], ["center"])
		.addChildrenTo("detailsTab", ["contactsList"])
		.addToolbarTo("cuList", "tlbCountriesCmpList")
		.addToolbarTo("aList", "tlbAreasCmpList")
		.addToolbarTo("aEdit", "tlbAreasCmpEdit")
		.addToolbarTo("locationList", "tlbLocList")
		.addToolbarTo("locationEditDetails", "tlbLocEditList")
		.addToolbarTo("notesList", "tlbNoteList")
		.addToolbarTo("contactsList", "tlbContacts")
		.addToolbarTo("contactEditContext", "tlbContactContext");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("canvas1", ["locationCanvas", "areasCanvas"])
		.addChildrenTo2("detailsTab", ["notesList", "airportInfList", "notamList", "taxesFeesList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbCountriesCmpList", {dc: "country"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("countriesBtnCancelWithMenu"),this._elems_.get("countriesBtnCancelSelected"),this._elems_.get("countriesBtnCancelAll"),this._elems_.get("countriesHelpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbAreasCmpList", {dc: "area"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"areasElement2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("areasBtnCancelWithMenu"),this._elems_.get("areasBtnCancelSelected"),this._elems_.get("areasBtnCancelAll"),this._elems_.get("areasHelpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbAreasCmpEdit", {dc: "area"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("areasAssignLocations"),this._elems_.get("areasHelpWdw1")])
			.addReports()
		.end()
		.beginToolbar("tlbLocList", {dc: "location"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"locationElement2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpLocationWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbLocEditList", {dc: "location"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpLocationWdw1")])
			.addReports()
		.end()
		.beginToolbar("tlbNoteList", {dc: "note"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbContacts", {dc: "contacts"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas3"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbContactContext", {dc: "contacts"})
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas2"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button countriesHelpWdw
	 */
	,onCountriesHelpWdw: function() {
		this.openCountriesHelp();
	}
	
	/**
	 * On-Click handler for button countriesBtnCancelSelected
	 */
	,onCountriesBtnCancelSelected: function() {
		this.fnCountriesCancelSelected();
	}
	
	/**
	 * On-Click handler for button countriesBtnCancelAll
	 */
	,onCountriesBtnCancelAll: function() {
		this.fnCountriesCancelAll();
	}
	
	/**
	 * On-Click handler for button areasAssignLocations
	 */
	,onAreasAssignLocations: function() {
		this._showAsgnWindow_("atraxo.fmbas.ui.extjs.asgn.AreasLocations_Asgn$Ui" ,{dc: "area", objectIdField: "id"
		,listeners: {close: {scope: this, fn: function() { this._getDc_("locations").doReloadPage();
		}} }});
	}
	
	/**
	 * On-Click handler for button areasHelpWdw
	 */
	,onAreasHelpWdw: function() {
		this.openAreasHelp();
	}
	
	/**
	 * On-Click handler for button areasHelpWdw1
	 */
	,onAreasHelpWdw1: function() {
		this.openAreasHelp();
	}
	
	/**
	 * On-Click handler for button areasBtnCancelSelected
	 */
	,onAreasBtnCancelSelected: function() {
		this.fnAreasCancelSelected();
	}
	
	/**
	 * On-Click handler for button areasBtnCancelAll
	 */
	,onAreasBtnCancelAll: function() {
		this.fnAreasCancelAll();
	}
	
	/**
	 * On-Click handler for button btnOnEditWdwClose
	 */
	,onBtnOnEditWdwClose: function() {
		this.onLocationWdwClose();
		this._getWindow_("wdwLocation").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseWdw
	 */
	,onBtnSaveCloseWdw: function() {
		this.saveClose();
	}
	
	/**
	 * On-Click handler for button btnSaveNewWdw
	 */
	,onBtnSaveNewWdw: function() {
		this.saveNew();
	}
	
	/**
	 * On-Click handler for button btnSaveEditWdw
	 */
	,onBtnSaveEditWdw: function() {
		this.saveEdit();
	}
	
	/**
	 * On-Click handler for button btnSaveNewContactWdw
	 */
	,onBtnSaveNewContactWdw: function() {
		this.saveNewContact();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseContactWdw
	 */
	,onBtnSaveCloseContactWdw: function() {
		this.saveCloseContact();
		this._getDc_("contacts").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseNoteWdw
	 */
	,onBtnSaveCloseNoteWdw: function() {
		this.saveCloseNote();
		this._getDc_("note").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCloseContactWdw
	 */
	,onBtnCloseContactWdw: function() {
		this.onContactWdwClose();
		this._getWindow_("contactWdw").close();
	}
	
	/**
	 * On-Click handler for button btnCloseNoteWdw
	 */
	,onBtnCloseNoteWdw: function() {
		this.onNoteWdwClose();
		this._getWindow_("noteWdw").close();
	}
	
	/**
	 * On-Click handler for button helpLocationWdw
	 */
	,onHelpLocationWdw: function() {
		this.openLocationHelp();
	}
	
	/**
	 * On-Click handler for button helpLocationWdw1
	 */
	,onHelpLocationWdw1: function() {
		this.openLocationHelp();
	}
	
	,onShowWdw: function() {	
		this._getWindow_("wdwLocation").show();
	}
	
	,showContactWdw: function() {	
		this._getWindow_("contactWdw").show();
	}
	
	,showNoteWdw: function() {	
		this._getWindow_("noteWdw").show();
	}
	
	,openCountriesHelp: function() {
		
					var url = Main.urlHelp+"/Countries.html";
					window.open( url, "SONE_Help");
	}
	
	,fnCountriesCancelSelected: function() {
		
						var dc = this._getDc_("country");
						dc.doCancelSelected();
						this._applyStateButton_("countriesBtnCancelWithMenu");
	}
	
	,fnCountriesCancelAll: function() {
		
						var dc = this._getDc_("country");
						dc.doCancel();
						this._applyStateButton_("countriesBtnCancelWithMenu");
	}
	
	,openAreasHelp: function() {
		
					var url = Main.urlHelp+"/Areas.html";
					window.open( url, "SONE_Help");
	}
	
	,fnAreasCancelSelected: function() {
		
						var dc = this._getDc_("area");
						dc.doCancelSelected();
						this._applyStateButton_("areasBtnCancelWithMenu");
	}
	
	,fnAreasCancelAll: function() {
		
						var dc = this._getDc_("area");
						dc.doCancel();
						this._applyStateButton_("areasBtnCancelWithMenu");
	}
	
	,_afterDefineDcs_: function() {
		
		
					this._getDc_("location").on("afterDoNew", function (dc) {
						dc.setEditMode();
						this.onShowWdw();
					}, this);
		 	
					var contact = this._getDc_("contacts");
					var note = this._getDc_("note");
		
					note.on("afterDoQuerySuccess", function (dc) {
						var rec = dc.getRecord();
					    if (rec) {
							var date = rec.get("createdAt");
							var newDate = ("0" + date.getDate()).slice(-2)+"/"+("0" + (date.getMonth() + 1)).slice(-2)+"/"+date.getFullYear();
					    }
					}, this);
		
					note.on("afterDoNew", function (dc) {
					   this.showNoteWdw();
					}, this);
		
					contact.on("afterDoNew", function (dc) {
						dc.setEditMode();
						this.showContactWdw();
					}, this);
		
					note.on("afterDoEditIn", function (dc) {
						this._getWindow_("noteWdw").show();
					}, this);
		
					this._getDc_("contacts").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("contactWdw").close();
						}
					}, this);
		
					this._getDc_("note").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("noteWdw").close();
						}
					}, this);
					
					this._getDc_("location").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwLocation").close();
						} else if (ajaxResult.options.options.doCloseEditAfterSave === true) {
							this._getWindow_("wdwLocation").close();
							dc.doEditIn();
						}
					}, this);
	}
	
	,saveNew: function() {
		
					var loc = this._getDc_("location");
					loc.doSave({doNewAfterSave:true});
	}
	
	,saveClose: function() {
		
					var loc = this._getDc_("location");
					loc.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveEdit: function() {
		
					var loc = this._getDc_("location");
					loc.doSave({doCloseEditAfterSave: true });
	}
	
	,saveCloseContact: function() {
		
					var contact = this._getDc_("contacts");
					contact.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveCloseNote: function() {
		
					var note = this._getDc_("note");
					note.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveNewContact: function() {
		
					var cont = this._getDc_("contacts");
					cont.doSave({doNewAfterSave:true});
	}
	
	,openLocationHelp: function() {
		
					var url = Main.urlHelp+"/Locations.html";
					window.open( url, "SONE_Help");
	}
	
	,onContactWdwClose: function() {
		
						var dc = this._getDc_("contacts");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,onNoteWdwClose: function() {
		
						var dc = this._getDc_("note");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,onLocationWdwClose: function() {
		
						var dc = this._getDc_("location");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,fnCancelEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isCancelEnabled(dc);
	}
});
