/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.NotificationEvent_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.NotificationEvent_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("notificationEvent", Ext.create(atraxo.fmbas.ui.extjs.dc.NotificationEventList_Dc,{multiEdit: true, trackEditMode: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnHelpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onBtnHelpWdw, scope:this})
		.addDcGridView("notificationEvent", {name:"list", xtype:"fmbas_NotificationEventList_Dc$List"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["list"], ["center"])
		.addToolbarTo("list", "tlbList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbList", {dc: "notificationEvent"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnHelpWdw")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnHelpWdw
	 */
	,onBtnHelpWdw: function() {
		this.openHelp();
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/TemplateTypes.html";
					window.open( url, "SONE_Help");
	}
});
