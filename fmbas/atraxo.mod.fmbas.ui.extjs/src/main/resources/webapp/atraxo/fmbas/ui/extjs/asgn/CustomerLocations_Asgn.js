/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

/* model */
Ext.define("atraxo.fmbas.ui.extjs.asgn.CustomerLocations_Asgn$Model", {
	extend: 'Ext.data.Model',
	statics: {
		ALIAS: "fmbas_CustomerLocations_Asgn"
	},
	fields:  [
		{name:"id",type:"string"},
		{name:"name",type:"string"},
		{name:"code",type:"string"}
	]
});

/* lists */
Ext.define( "atraxo.fmbas.ui.extjs.asgn.CustomerLocations_Asgn$List", {
	extend: "e4e.asgn.AbstractAsgnGrid",
	alias:[ "widget.fmbas_CustomerLocations_Asgn$Left","widget.fmbas_CustomerLocations_Asgn$Right" ],
	_defineColumns_: function () {
		this._getBuilder_()
		.addTextColumn({name:"id", dataIndex:"id", hidden:true})
		.addTextColumn({name:"name", dataIndex:"name", width:120})
		.addTextColumn({name:"code", dataIndex:"code", width:40})
	} 
});
	
/* ui-window */
Ext.define("atraxo.fmbas.ui.extjs.asgn.CustomerLocations_Asgn$Ui", {
	extend: "e4e.asgn.AbstractAsgnUi",
	width:800,
	height:400,
	title:"Assign locations to customer ",
	_filterFields_: [
		["name"],
		["code"]
	],
	_defaultFilterField_ : "name",

	_defineElements_: function () {
		this._getBuilder_()
			.addLeftGrid({ xtype:"fmbas_CustomerLocations_Asgn$Left"})
			.addRightGrid({ xtype:"fmbas_CustomerLocations_Asgn$Right"})
	}
});

/* ui-panel */
Ext.define("atraxo.fmbas.ui.extjs.asgn.CustomerLocations_Asgn$AsgnPanel", {
	extend: "e4e.asgn.AbstractAsgnPanel",
	alias: "widget.fmbas_CustomerLocations_Asgn$AsgnPanel",
	width:800,
	height:400,
	title:"Assign locations to customer ",
	_filterFields_: [
		["name"],
		["code"]
	],
	_defaultFilterField_ : "name",

	_defineElements_: function () {
		this._getBuilder_()
			.addLeftGrid({ xtype:"fmbas_CustomerLocations_Asgn$Left"})
			.addRightGrid({ xtype:"fmbas_CustomerLocations_Asgn$Right"})
	}
});
