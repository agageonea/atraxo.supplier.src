/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.PriceCategories_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.PriceCategories_Ds,
		
			/* ================ Business functions ================ */
	
	canDoDelete: function() {
		
					var selRecords = this.selectedRecords;
		
					for (var i = 0; i < selRecords.length; i++) {
		               if (selRecords[i].data.isSys == true) {
						  return false;
					   }
					}
					return  true;
	}

});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.PriceCategories_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_PriceCategories_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"name", dataIndex:"name", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.PriceCategoriesLov_Lov", selectOnFocus:true, maxLength:32}})
			.addLov({name:"mainCode", dataIndex:"mainCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MainCategoriesLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "mainId"} ]}})
			.addLov({name:"iataCode", dataIndex:"iataCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.IataLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "iataId"} ]}})
			.addCombo({ xtype:"combo", name:"pricePer", dataIndex:"pricePer", store:[ __FMBAS_TYPE__.PriceInd._VOLUME_, __FMBAS_TYPE__.PriceInd._EVENT_, __FMBAS_TYPE__.PriceInd._COMPOSITE_, __FMBAS_TYPE__.PriceInd._PERCENT_]})
			.addCombo({ xtype:"combo", name:"type", dataIndex:"type", store:[ __FMBAS_TYPE__.PriceType._PRODUCT_, __FMBAS_TYPE__.PriceType._DIFFERENTIAL_, __FMBAS_TYPE__.PriceType._INTO_PLANE_FEE_, __FMBAS_TYPE__.PriceType._OTHER_FEE_, __FMBAS_TYPE__.PriceType._TAX_]})
			.addBooleanField({ name:"isIndexBased", dataIndex:"isIndexBased"})
			.addBooleanField({ name:"active", dataIndex:"active"})
			.addBooleanField({ name:"isSys", dataIndex:"isSys"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.PriceCategories_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_PriceCategories_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:160})
		.addTextColumn({ name:"mainCategory", dataIndex:"mainCode", width:140})
		.addTextColumn({ name:"iataCode", dataIndex:"iataCode", width:80})
		.addTextColumn({ name:"pricePer", dataIndex:"pricePer", width:100})
		.addTextColumn({ name:"type", dataIndex:"type", width:100})
		.addBooleanColumn({ name:"isIndexBased", dataIndex:"isIndexBased", width:100})
		.addBooleanColumn({ name:"active", dataIndex:"active", width:60})
		.addBooleanColumn({ name:"isSys", dataIndex:"isSys", width:60})
		.addTextColumn({ name:"description", dataIndex:"description", width:160})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: EditForm ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.PriceCategories_Dc$EditForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_PriceCategories_Dc$EditForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, noLabel: true, maxLength:32})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active", noLabel: true})
		.addCombo({ xtype:"combo", name:"type", bind:"{d.type}", dataIndex:"type", allowBlank:false, noLabel: true, store:[ __FMBAS_TYPE__.PriceType._PRODUCT_, __FMBAS_TYPE__.PriceType._DIFFERENTIAL_, __FMBAS_TYPE__.PriceType._INTO_PLANE_FEE_, __FMBAS_TYPE__.PriceType._OTHER_FEE_, __FMBAS_TYPE__.PriceType._TAX_]})
		.addLov({name:"mainCode", bind:"{d.mainCode}", dataIndex:"mainCode", allowBlank:false, noLabel: true, xtype:"fmbas_MainCategoriesLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"id", dsField: "mainId"} ]})
		.addLov({name:"iataCode", bind:"{d.iataCode}", dataIndex:"iataCode", allowBlank:false, noLabel: true, xtype:"fmbas_IataLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"id", dsField: "iataId"} ]})
		.addCombo({ xtype:"combo", name:"pricePer", bind:"{d.pricePer}", dataIndex:"pricePer", allowBlank:false, noLabel: true, store:[ __FMBAS_TYPE__.PriceInd._VOLUME_, __FMBAS_TYPE__.PriceInd._EVENT_, __FMBAS_TYPE__.PriceInd._COMPOSITE_, __FMBAS_TYPE__.PriceInd._PERCENT_]})
		.addTextArea({ name:"description", bind:"{d.description}", dataIndex:"description", width:300, noLabel: true})
		.addDisplayFieldText({ name:"nameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"typeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"mainCatLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"iataCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"pricePerLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"activeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"descLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["nameLabel", "name", "typeLabel", "type", "mainCatLabel", "mainCode", "iataCodeLabel", "iataCode", "pricePerLabel", "pricePer", "activeLabel", "active", "descLabel", "description"]);
	}
});
