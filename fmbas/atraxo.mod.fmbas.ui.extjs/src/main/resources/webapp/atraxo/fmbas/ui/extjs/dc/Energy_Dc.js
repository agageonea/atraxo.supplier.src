/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Energy_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.EnergyPriceTimeSerie_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.EnergyPriceTimeSerie_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Energy_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Energy_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"serieName", dataIndex:"serieName", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.TimeSerieLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"serieName", dsField: "serieName"} ],
					filterFieldMapping: [{lovField:"serieType", value: "E"} ]}})
			.addTextField({ name:"description", dataIndex:"description", maxLength:200})
			.addLov({name:"financialSourceCode", dataIndex:"financialSourceCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CodeLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "financialSource"} ,{lovField:"name", dsField: "financialSourceName"} ],
					filterFieldMapping: [{lovField:"active", value: "true"} ]}})
			.addLov({name:"commodityName", dataIndex:"commodityName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CommoditiesLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "commodityId"} ]}})
			.addCombo({ xtype:"combo", name:"serieFreqInd", dataIndex:"serieFreqInd", store:[ __FMBAS_TYPE__.SerieFreqInd._DAILY_, __FMBAS_TYPE__.SerieFreqInd._WEEKDAY_, __FMBAS_TYPE__.SerieFreqInd._WEEKLY_, __FMBAS_TYPE__.SerieFreqInd._SEMIMONTHLY_, __FMBAS_TYPE__.SerieFreqInd._MONTHLY_, __FMBAS_TYPE__.SerieFreqInd._QUARTERLY_]})
			.addDateField({name:"firstUsage", dataIndex:"firstUsage"})
			.addDateField({name:"lastUsage", dataIndex:"lastUsage"})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __FMBAS_TYPE__.TimeSeriesStatus._WORK_, __FMBAS_TYPE__.TimeSeriesStatus._PUBLISHED_]})
			.addCombo({ xtype:"combo", name:"approvalStatus", dataIndex:"approvalStatus", store:[ __FMBAS_TYPE__.TimeSeriesApprovalStatus._NEW_, __FMBAS_TYPE__.TimeSeriesApprovalStatus._APPROVED_, __FMBAS_TYPE__.TimeSeriesApprovalStatus._REJECTED_, __FMBAS_TYPE__.TimeSeriesApprovalStatus._AWAITING_APPROVAL_]})
			.addCombo({ xtype:"combo", name:"valueType", dataIndex:"valueType", store:[ __FMBAS_TYPE__.ValueType._HIGH_PRICE_, __FMBAS_TYPE__.ValueType._LOW_PRICE_, __FMBAS_TYPE__.ValueType._MEAN_PRICE_, __FMBAS_TYPE__.ValueType._USER_CALCULATED_PRICE_]})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Energy_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Energy_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"serieName", dataIndex:"serieName", width:150})
		.addTextColumn({ name:"description", dataIndex:"description", width:200})
		.addTextColumn({ name:"financialSourceCode", dataIndex:"financialSourceCode", width:120})
		.addTextColumn({ name:"commodityName", dataIndex:"commodityName", width:100})
		.addTextColumn({ name:"serieFreqInd", dataIndex:"serieFreqInd", width:100})
		.addTextColumn({ name:"measure", dataIndex:"measure", width:80})
		.addDateColumn({ name:"firstUsage", dataIndex:"firstUsage", _mask_: Masks.DATE})
		.addDateColumn({ name:"lastUsage", dataIndex:"lastUsage", _mask_: Masks.DATE})
		.addTextColumn({ name:"status", dataIndex:"status", width:80})
		.addTextColumn({ name:"approvalStatus", dataIndex:"approvalStatus", width:130})
		.addTextColumn({ name:"serieType", dataIndex:"serieType", hidden:true, width:60})
		.addTextColumn({ name:"code", dataIndex:"externalSerieName", hidden:true, width:80})
		.addTextColumn({ name:"provider", dataIndex:"dataProvider", hidden:true, width:100})
		.addTextColumn({ name:"valueType", dataIndex:"valueType", hidden:true, width:100})
		.addNumberColumn({ name:"decimals", dataIndex:"decimals", hidden:true, width:80})
		.addNumberColumn({ name:"factor", dataIndex:"factor", width:80, sysDec:"dec_prc"})
		.addTextColumn({ name:"convFactor", dataIndex:"convFctr", hidden:true, width:120})
		.addNumberColumn({ name:"by", dataIndex:"arithmOper", hidden:true, width:60,  decimals:4})
		.addTextColumn({ name:"to", dataIndex:"unit2Code", hidden:true, width:60})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_afterDefineElements_: function() {
		
						if(_SYSTEMPARAMETERS_.sysenergypriceapproval === "false"){
							this._columns_.getByKey("approvalStatus").hidden = true ;
						}
	}
});

/* ================= EDIT FORM: ApprovalNote ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Energy_Dc$ApprovalNote", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Energy_Dc$ApprovalNote",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.approvalNote}", paramIndex:"approvalNote", width:580, labelAlign:"top", labelStyle:"font-weight:bold", fieldStyle:"margin-top: 5px;"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:580})
		.addPanel({ name:"col1", width:580, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
		                       var remarks = this._get_("remarks");
		                       remarks.setRawValue("");
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Energy_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Energy_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"serieName", bind:"{d.serieName}", dataIndex:"serieName", allowBlank:false, maxLength:32, labelWidth:140})
		.addLov({name:"commodity", bind:"{d.commodityName}", dataIndex:"commodityName", allowBlank:false, xtype:"fmbas_CommoditiesLov_Lov", maxLength:100, labelWidth:120,
			retFieldMapping: [{lovField:"id", dsField: "commodityId"} ]})
		.addCombo({ xtype:"combo", name:"dataProvider", bind:"{d.dataProvider}", dataIndex:"dataProvider", allowBlank:false, store:[ __FMBAS_TYPE__.DataProvider._IMPORT_ARGUS_, __FMBAS_TYPE__.DataProvider._IMPORT_DDS_, __FMBAS_TYPE__.DataProvider._IMPORT_OPIS_, __FMBAS_TYPE__.DataProvider._IMPORT_SHARE_, __FMBAS_TYPE__.DataProvider._IMPORT_PLATTS_, __FMBAS_TYPE__.DataProvider._IMPORT_ECB_, __FMBAS_TYPE__.DataProvider._IMPORT_BLOOMBERG_, __FMBAS_TYPE__.DataProvider._USER_MAINTAINED_, __FMBAS_TYPE__.DataProvider._CALCULATED_, __FMBAS_TYPE__.DataProvider._OTHER_], labelWidth:120})
		.addTextField({ name:"description", bind:"{d.description}", dataIndex:"description", allowBlank:false, maxLength:200, labelWidth:140})
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", allowBlank:false, xtype:"fmbas_CodeLov_Lov", maxLength:25, labelWidth:120,
			retFieldMapping: [{lovField:"id", dsField: "financialSource"} ,{lovField:"name", dsField: "financialSourceName"} ]})
		.addTextField({ name:"externalSerieName", bind:"{d.externalSerieName}", dataIndex:"externalSerieName", allowBlank:false, maxLength:15, labelWidth:140})
		.addCombo({ xtype:"combo", name:"serieFreqInd", bind:"{d.serieFreqInd}", dataIndex:"serieFreqInd", allowBlank:false, store:[ __FMBAS_TYPE__.SerieFreqInd._DAILY_, __FMBAS_TYPE__.SerieFreqInd._WEEKDAY_, __FMBAS_TYPE__.SerieFreqInd._WEEKLY_, __FMBAS_TYPE__.SerieFreqInd._SEMIMONTHLY_, __FMBAS_TYPE__.SerieFreqInd._MONTHLY_, __FMBAS_TYPE__.SerieFreqInd._QUARTERLY_], labelWidth:120})
		.addCombo({ xtype:"combo", name:"valueType", bind:"{d.valueType}", dataIndex:"valueType", allowBlank:false, store:[ __FMBAS_TYPE__.ValueType._HIGH_PRICE_, __FMBAS_TYPE__.ValueType._LOW_PRICE_, __FMBAS_TYPE__.ValueType._MEAN_PRICE_, __FMBAS_TYPE__.ValueType._USER_CALCULATED_PRICE_], labelWidth:120})
		.addLov({name:"currency1Code", bind:"{d.currency1Code}", dataIndex:"currency1Code", allowBlank:false, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:140,
			retFieldMapping: [{lovField:"id", dsField: "currency1Id"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addNumberField({name:"decimals", bind:"{d.decimals}", dataIndex:"decimals", allowBlank:false, width:150, maxLength:3, labelWidth:120})
		.addLov({name:"unit", bind:"{d.unitCode}", dataIndex:"unitCode", allowBlank:false, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, labelWidth:140,
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ]})
		.addNumberField({name:"factor", bind:"{d.factor}", dataIndex:"factor", allowBlank:false, sysDec:"dec_prc", maxLength:19, labelWidth:140})
		.addNumberField({name:"oper", bind:"{d.arithmOper}", dataIndex:"arithmOper", allowBlank:false, width:100, maxLength:19, labelWidth:30, decimals:4})
		.addLov({name:"unit2", bind:"{d.unit2Code}", dataIndex:"unit2Code", allowBlank:false, width:100, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, labelWidth:30,
			retFieldMapping: [{lovField:"id", dsField: "unit2Id"} ]})
		.addCombo({ xtype:"combo", name:"convFactor", bind:"{d.convFctr}", dataIndex:"convFctr", allowBlank:false, store:[ __FMBAS_TYPE__.Operator._MULTIPLY_, __FMBAS_TYPE__.Operator._DIVIDE_], labelWidth:140})
		.add({name:"nameCommodity", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("serieName"),this._getConfig_("commodity")]})
		.add({name:"descriptionProvider", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("description"),this._getConfig_("dataProvider")]})
		.add({name:"codeSource", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("externalSerieName"),this._getConfig_("financialSourceCode")]})
		.add({name:"currencyFrequency", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("currency1Code"),this._getConfig_("serieFreqInd")]})
		.add({name:"unitValueType", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("unit"),this._getConfig_("valueType")]})
		.add({name:"factorDecimals", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("factor"),this._getConfig_("decimals")]})
		.add({name:"convUnitOper", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("convFactor"),this._getConfig_("oper"),this._getConfig_("unit2")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:620, layout:"anchor"})
		.addPanel({ name:"col2", width:620, layout:"anchor"})
		.addPanel({ name:"col3", width:620, layout:"anchor"})
		.addPanel({ name:"col4", width:620, layout:"anchor"})
		.addPanel({ name:"col5", width:620, layout:"anchor"})
		.addPanel({ name:"col6", width:620, layout:"anchor"})
		.addPanel({ name:"col7", width:600, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5", "col6", "col7"])
		.addChildrenTo("col1", ["nameCommodity"])
		.addChildrenTo("col2", ["descriptionProvider"])
		.addChildrenTo("col3", ["codeSource"])
		.addChildrenTo("col4", ["currencyFrequency"])
		.addChildrenTo("col5", ["unitValueType"])
		.addChildrenTo("col6", ["factorDecimals"])
		.addChildrenTo("col7", ["convUnitOper"]);
	}
});

/* ================= EDIT FORM: EditDetails ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Energy_Dc$EditDetails", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Energy_Dc$EditDetails",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"serieName", bind:"{d.serieName}", dataIndex:"serieName", allowBlank:false, maxLength:32, labelWidth:140})
		.addLov({name:"commodity", bind:"{d.commodityName}", dataIndex:"commodityName", allowBlank:false, xtype:"fmbas_CommoditiesLov_Lov", maxLength:100, labelWidth:120,
			retFieldMapping: [{lovField:"id", dsField: "commodityId"} ]})
		.addCombo({ xtype:"combo", name:"dataProvider", bind:"{d.dataProvider}", dataIndex:"dataProvider", _enableFn_: function(dc, rec) { return dc.record.data.hasComponents == false; } , allowBlank:false, store:[ __FMBAS_TYPE__.DataProvider._IMPORT_ARGUS_, __FMBAS_TYPE__.DataProvider._IMPORT_DDS_, __FMBAS_TYPE__.DataProvider._IMPORT_OPIS_, __FMBAS_TYPE__.DataProvider._IMPORT_SHARE_, __FMBAS_TYPE__.DataProvider._IMPORT_PLATTS_, __FMBAS_TYPE__.DataProvider._IMPORT_ECB_, __FMBAS_TYPE__.DataProvider._IMPORT_BLOOMBERG_, __FMBAS_TYPE__.DataProvider._USER_MAINTAINED_, __FMBAS_TYPE__.DataProvider._CALCULATED_, __FMBAS_TYPE__.DataProvider._OTHER_], labelWidth:120})
		.addTextField({ name:"description", bind:"{d.description}", dataIndex:"description", allowBlank:false, maxLength:200, labelWidth:140})
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", allowBlank:false, xtype:"fmbas_CodeLov_Lov", maxLength:25, labelWidth:120,
			retFieldMapping: [{lovField:"id", dsField: "financialSource"} ]})
		.addTextField({ name:"externalSerieName", bind:"{d.externalSerieName}", dataIndex:"externalSerieName", allowBlank:false, maxLength:15, labelWidth:140})
		.addCombo({ xtype:"combo", name:"serieFreqInd", bind:"{d.serieFreqInd}", dataIndex:"serieFreqInd", noEdit:true , allowBlank:false, store:[ __FMBAS_TYPE__.SerieFreqInd._DAILY_, __FMBAS_TYPE__.SerieFreqInd._WEEKDAY_, __FMBAS_TYPE__.SerieFreqInd._WEEKLY_, __FMBAS_TYPE__.SerieFreqInd._SEMIMONTHLY_, __FMBAS_TYPE__.SerieFreqInd._MONTHLY_, __FMBAS_TYPE__.SerieFreqInd._QUARTERLY_], labelWidth:120})
		.addLov({name:"currency1Code", bind:"{d.currency1Code}", dataIndex:"currency1Code", noUpdate:true, allowBlank:false, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:140,
			retFieldMapping: [{lovField:"id", dsField: "currency1Id"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addNumberField({name:"decimals", bind:"{d.decimals}", dataIndex:"decimals", allowBlank:false, width:150, maxLength:3, labelWidth:120})
		.addLov({name:"currency2Code", bind:"{d.currency2Code}", dataIndex:"currency2Code", allowBlank:false, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:140,
			retFieldMapping: [{lovField:"id", dsField: "currency2Id"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addLov({name:"unit", bind:"{d.unitCode}", dataIndex:"unitCode", noUpdate:true, allowBlank:false, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, labelWidth:140,
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ]})
		.addNumberField({name:"factor", bind:"{d.factor}", dataIndex:"factor", allowBlank:false, sysDec:"dec_prc", maxLength:19, labelWidth:140})
		.addNumberField({name:"oper", bind:"{d.arithmOper}", dataIndex:"arithmOper", allowBlank:false, width:100, maxLength:19, labelWidth:30, decimals:4})
		.addLov({name:"unit2", bind:"{d.unit2Code}", dataIndex:"unit2Code", allowBlank:false, width:100, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, labelWidth:30,
			retFieldMapping: [{lovField:"id", dsField: "unit2Id"} ]})
		.addCombo({ xtype:"combo", name:"convFactor", bind:"{d.convFctr}", dataIndex:"convFctr", allowBlank:false, store:[ __FMBAS_TYPE__.Operator._MULTIPLY_, __FMBAS_TYPE__.Operator._DIVIDE_], labelWidth:140})
		.addCombo({ xtype:"combo", name:"valueType", bind:"{d.valueType}", dataIndex:"valueType", _enableFn_: this._hasTimeSerieItems_, allowBlank:false, store:[ __FMBAS_TYPE__.ValueType._HIGH_PRICE_, __FMBAS_TYPE__.ValueType._LOW_PRICE_, __FMBAS_TYPE__.ValueType._MEAN_PRICE_, __FMBAS_TYPE__.ValueType._USER_CALCULATED_PRICE_], labelWidth:120})
		.addDateField({name:"firstUsage", bind:"{d.firstUsage}", dataIndex:"firstUsage", noEdit:true })
		.addDateField({name:"lastUsage", bind:"{d.lastUsage}", dataIndex:"lastUsage", noEdit:true })
		.addHiddenField({ name:"hasTimeSerieItems", bind:"{d.hasTimeSerieItems}", dataIndex:"hasTimeSerieItems"})
		.add({name:"nameCommodity", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("serieName"),this._getConfig_("commodity"),this._getConfig_("firstUsage")]})
		.add({name:"descriptionProvider", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("description"),this._getConfig_("dataProvider"),this._getConfig_("lastUsage")]})
		.add({name:"codeSource", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("externalSerieName"),this._getConfig_("financialSourceCode")]})
		.add({name:"currencyFrequency", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("currency1Code"),this._getConfig_("serieFreqInd")]})
		.add({name:"unitValueType", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("unit"),this._getConfig_("valueType")]})
		.add({name:"factorDecimals", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("factor"),this._getConfig_("decimals")]})
		.add({name:"convUnitOper", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("convFactor"),this._getConfig_("oper"),this._getConfig_("unit2")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:1000, layout:"anchor"})
		.addPanel({ name:"col2", width:1000, layout:"anchor"})
		.addPanel({ name:"col3", width:780, layout:"anchor"})
		.addPanel({ name:"col4", width:780, layout:"anchor"})
		.addPanel({ name:"col5", width:620, layout:"anchor"})
		.addPanel({ name:"col6", width:620, layout:"anchor"})
		.addPanel({ name:"col7", width:520, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5", "col6", "col7"])
		.addChildrenTo("col1", ["nameCommodity"])
		.addChildrenTo("col2", ["descriptionProvider"])
		.addChildrenTo("col3", ["codeSource"])
		.addChildrenTo("col4", ["currencyFrequency"])
		.addChildrenTo("col5", ["unitValueType"])
		.addChildrenTo("col6", ["factorDecimals"])
		.addChildrenTo("col7", ["convUnitOper", "hasTimeSerieItems"]);
	},
	/* ==================== Business functions ==================== */
	
	_hasTimeSerieItems_: function() {
		
						var result = true;
						var r = this._controller_.getRecord();
						var hasTimeSerieItems = r.get("hasTimeSerieItems");
						if (hasTimeSerieItems == true) {
							result = false;
						}
						return result;
		
	}
});

/* ================= EDIT FORM: EditQuotationContext ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Energy_Dc$EditQuotationContext", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Energy_Dc$EditQuotationContext",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"serieName", bind:"{d.serieName}", dataIndex:"serieName", noEdit:true , maxLength:32})
		.addCombo({ xtype:"combo", name:"dataProvider", bind:"{d.dataProvider}", dataIndex:"dataProvider", noEdit:true , store:[ __FMBAS_TYPE__.DataProvider._IMPORT_ARGUS_, __FMBAS_TYPE__.DataProvider._IMPORT_DDS_, __FMBAS_TYPE__.DataProvider._IMPORT_OPIS_, __FMBAS_TYPE__.DataProvider._IMPORT_SHARE_, __FMBAS_TYPE__.DataProvider._IMPORT_PLATTS_, __FMBAS_TYPE__.DataProvider._IMPORT_ECB_, __FMBAS_TYPE__.DataProvider._IMPORT_BLOOMBERG_, __FMBAS_TYPE__.DataProvider._USER_MAINTAINED_, __FMBAS_TYPE__.DataProvider._CALCULATED_, __FMBAS_TYPE__.DataProvider._OTHER_], labelWidth:100})
		.addTextField({ name:"description", bind:"{d.description}", dataIndex:"description", noEdit:true , maxLength:200})
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", noEdit:true , xtype:"fmbas_CodeLov_Lov", maxLength:25, labelWidth:100,
			retFieldMapping: [{lovField:"id", dsField: "financialSource"} ]})
		.addTextField({ name:"externalSerieName", bind:"{d.externalSerieName}", dataIndex:"externalSerieName", noEdit:true , maxLength:15, labelWidth:140})
		.addCombo({ xtype:"combo", name:"serieFreqInd", bind:"{d.serieFreqInd}", dataIndex:"serieFreqInd", noEdit:true , store:[ __FMBAS_TYPE__.SerieFreqInd._DAILY_, __FMBAS_TYPE__.SerieFreqInd._WEEKDAY_, __FMBAS_TYPE__.SerieFreqInd._WEEKLY_, __FMBAS_TYPE__.SerieFreqInd._SEMIMONTHLY_, __FMBAS_TYPE__.SerieFreqInd._MONTHLY_, __FMBAS_TYPE__.SerieFreqInd._QUARTERLY_], labelWidth:100})
		.addLov({name:"currency1Code", bind:"{d.currency1Code}", dataIndex:"currency1Code", noEdit:true , xtype:"fmbas_CurrenciesLov_Lov", maxLength:3,
			retFieldMapping: [{lovField:"id", dsField: "currency1Id"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addNumberField({name:"decimals", bind:"{d.decimals}", dataIndex:"decimals", noEdit:true , width:150, maxLength:3, labelWidth:100})
		.addLov({name:"unit", bind:"{d.unitCode}", dataIndex:"unitCode", noEdit:true , xtype:"fmbas_UnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ]})
		.addDateField({name:"firstUsage", bind:"{d.firstUsage}", dataIndex:"firstUsage", noEdit:true , labelWidth:100})
		.addDateField({name:"lastUsage", bind:"{d.lastUsage}", dataIndex:"lastUsage", noEdit:true , labelWidth:100})
		.add({name:"nameProviderQuotedFrom", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("serieName"),this._getConfig_("dataProvider"),this._getConfig_("firstUsage")]})
		.add({name:"descriptionSourceQuotedTo", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("description"),this._getConfig_("financialSourceCode"),this._getConfig_("lastUsage")]})
		.add({name:"codeFrequency", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("currency1Code"),this._getConfig_("serieFreqInd")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:900, layout:"anchor"})
		.addPanel({ name:"col2", width:900, layout:"anchor"})
		.addPanel({ name:"col3", width:620, layout:"anchor"})
		.addPanel({ name:"col5", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col5"])
		.addChildrenTo("col1", ["nameProviderQuotedFrom"])
		.addChildrenTo("col2", ["descriptionSourceQuotedTo"])
		.addChildrenTo("col3", ["codeFrequency"])
		.addChildrenTo("col5", ["unit"]);
	}
});
