/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.ExchangeRates_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.ExchangeRates_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("exchangeRate", Ext.create(atraxo.fmbas.ui.extjs.dc.ExchangeRates_Dc,{}))
		.addDc("exchangeRateValue", Ext.create(atraxo.fmbas.ui.extjs.dc.ExchangeRateVal_Dc,{}))
		.linkDc("exchangeRateValue", "exchangeRate",{fetchMode:"auto",fields:[
					{childField:"exchangeRateId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdw1",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw1, scope:this})
		.addDcFilterFormView("exchangeRate", {name:"exchangeRateFilter", xtype:"fmbas_ExchangeRates_Dc$Filter"})
		.addDcGridView("exchangeRate", {name:"exchangeRateList", xtype:"fmbas_ExchangeRates_Dc$List"})
		.addDcFormView("exchangeRate", {name:"exchangeRateEdit", xtype:"fmbas_ExchangeRates_Dc$Edit"})
		.addDcGridView("exchangeRateValue", {name:"exchangeRateValueList", xtype:"fmbas_ExchangeRateVal_Dc$List"})
		.addDcFilterFormView("exchangeRateValue", {name:"exRateValFilter", xtype:"fmbas_ExchangeRateVal_Dc$Filter"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["exchangeRateList"], ["center"])
		.addChildrenTo("canvas2", ["exchangeRateEdit", "exchangeRateValueList"], ["north", "center"])
		.addToolbarTo("exchangeRateList", "tlbExchangeRateList")
		.addToolbarTo("exchangeRateEdit", "tlbExchangeRateEdit");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbExchangeRateList", {dc: "exchangeRate"})
			.addButtons([])
			.addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbExchangeRateEdit", {dc: "exchangeRate"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw1")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdw1
	 */
	,onHelpWdw1: function() {
		this.openHelp();
	}
	
	,_afterDefineDcs_: function() {
		
				this._getDc_("exchangeRate").on("onEditIn", function (dc) {
						   this._calc_field_name_();
						}, this);
	}
	
	,_calc_field_name_: function() {
			
					var erDc = this._getDc_("exchangeRate");
					var exchangeRateValue = this._getDc_("exchangeRateValue");
					var erRec = erDc.getRecord();
			
					var cur1 = erRec.get("currency1Code");
					var cur2 = erRec.get("currency2Code");
			
					var newHeader = cur1+"/"+cur2;
					var newHeaderInverse = cur2+"/"+cur1;
			 
					this._get_("exchangeRateValueList").getView().getHeaderAtIndex(3).setText(newHeader);
					this._get_("exchangeRateValueList").getView().getHeaderAtIndex(4).setText(newHeaderInverse);
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/ExchangeRates.html";
					window.open( url, "SONE_Help");
	}
});
