/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ExchangeRateTimeSerie_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_ExchangeRateTimeSerie_Ds"
	},
	
	
	validators: {
		serieName: [{type: 'presence'}],
		description: [{type: 'presence'}],
		serieType: [{type: 'presence'}],
		decimals: [{type: 'presence'}],
		activation: [{type: 'presence'}],
		factor: [{type: 'presence'}],
		supplyItemUpdate: [{type: 'presence'}],
		currency1Code: [{type: 'presence'}],
		financialSourceName: [{type: 'presence'}],
		currency2Code: [{type: 'presence'}],
		postTypeInd: [{type: 'presence'}],
		externalSerieName: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("serieFreqInd", "Daily");
		this.set("decimals", 4);
		this.set("approvalStatus", "New");
		this.set("status", "work");
	},
	
	fields: [
		{name:"measure", type:"string", noFilter:true, noSort:true},
		{name:"serieName", type:"string"},
		{name:"description", type:"string"},
		{name:"serieType", type:"string"},
		{name:"serieFreqInd", type:"string"},
		{name:"decimals", type:"int", allowNull:true},
		{name:"dataProvider", type:"string"},
		{name:"externalSerieName", type:"string"},
		{name:"activation", type:"string"},
		{name:"firstUsage", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"lastUsage", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"factor", type:"float", allowNull:true},
		{name:"supplyItemUpdate", type:"string"},
		{name:"status", type:"string"},
		{name:"approvalStatus", type:"string"},
		{name:"valueType", type:"string"},
		{name:"currency1Id", type:"int", allowNull:true, noFilter:true},
		{name:"currency1Code", type:"string"},
		{name:"currency2Id", type:"int", allowNull:true, noFilter:true},
		{name:"currency2Code", type:"string"},
		{name:"commoditieId", type:"int", allowNull:true, noFilter:true},
		{name:"commodityName", type:"string"},
		{name:"commodityCode", type:"string"},
		{name:"postTypeInd", type:"string"},
		{name:"financialSource", type:"int", allowNull:true, noFilter:true},
		{name:"financialSourceCode", type:"string"},
		{name:"financialSourceName", type:"string"},
		{name:"unitId", type:"int", allowNull:true, noFilter:true},
		{name:"unitCode", type:"string"},
		{name:"convFctr", type:"string"},
		{name:"arithmOper", type:"float", allowNull:true},
		{name:"unit2Id", type:"int", allowNull:true, noFilter:true},
		{name:"unit2Code", type:"string"},
		{name:"active", type:"boolean"},
		{name:"hasTimeSerieItems", type:"boolean", noFilter:true, noSort:true},
		{name:"canBeCompleted", type:"boolean", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ExchangeRateTimeSerie_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"measure", type:"string", noFilter:true, noSort:true},
		{name:"serieName", type:"string"},
		{name:"description", type:"string"},
		{name:"serieType", type:"string"},
		{name:"serieFreqInd", type:"string"},
		{name:"decimals", type:"int", allowNull:true},
		{name:"dataProvider", type:"string"},
		{name:"externalSerieName", type:"string"},
		{name:"activation", type:"string"},
		{name:"firstUsage", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"lastUsage", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"factor", type:"float", allowNull:true},
		{name:"supplyItemUpdate", type:"string"},
		{name:"status", type:"string"},
		{name:"approvalStatus", type:"string"},
		{name:"valueType", type:"string"},
		{name:"currency1Id", type:"int", allowNull:true, noFilter:true},
		{name:"currency1Code", type:"string"},
		{name:"currency2Id", type:"int", allowNull:true, noFilter:true},
		{name:"currency2Code", type:"string"},
		{name:"commoditieId", type:"int", allowNull:true, noFilter:true},
		{name:"commodityName", type:"string"},
		{name:"commodityCode", type:"string"},
		{name:"postTypeInd", type:"string"},
		{name:"financialSource", type:"int", allowNull:true, noFilter:true},
		{name:"financialSourceCode", type:"string"},
		{name:"financialSourceName", type:"string"},
		{name:"unitId", type:"int", allowNull:true, noFilter:true},
		{name:"unitCode", type:"string"},
		{name:"convFctr", type:"string"},
		{name:"arithmOper", type:"float", allowNull:true},
		{name:"unit2Id", type:"int", allowNull:true, noFilter:true},
		{name:"unit2Code", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"hasTimeSerieItems", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"canBeCompleted", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.fmbas.ui.extjs.ds.ExchangeRateTimeSerie_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"approvalNote", type:"string"},
		{name:"approveRejectResult", type:"string"},
		{name:"submitForApprovalDescription", type:"string"},
		{name:"submitForApprovalResult", type:"boolean"}
	]
});
