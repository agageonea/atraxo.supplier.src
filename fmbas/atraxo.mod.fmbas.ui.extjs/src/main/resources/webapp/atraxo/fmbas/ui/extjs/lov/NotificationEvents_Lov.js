/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.NotificationEvents_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_NotificationEvents_Lov",
	displayField: "name", 
	_columns_: ["name"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{name}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	paramModel: atraxo.fmbas.ui.extjs.ds.NotificationEvent_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.NotificationEvent_Ds
});
