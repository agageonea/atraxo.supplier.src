/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.SysParams_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.SystemParameters_Ds
});

/* ================= EDIT FORM: FormParams ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.SysParams_Dc$FormParams", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_SysParams_Dc$FormParams",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"sysUnits", bind:"{d.sysUnits}", dataIndex:"sysUnits", noEdit:true , maxLength:32})
		.addTextField({ name:"sysLength", bind:"{d.sysLenght}", dataIndex:"sysLenght", noEdit:true , maxLength:4})
		.addTextField({ name:"sysVol", bind:"{d.sysVol}", dataIndex:"sysVol", noEdit:true , maxLength:4})
		.addTextField({ name:"sysWeight", bind:"{d.sysWeight}", dataIndex:"sysWeight", noEdit:true , maxLength:4})
		.addNumberField({name:"sysDensity", bind:"{d.sysDensity}", dataIndex:"sysDensity", noEdit:true , sysDec:"dec_prc", maxLength:19})
		.addTextField({ name:"sysCurrency", bind:"{d.sysCurrency}", dataIndex:"sysCurrency", noEdit:true , maxLength:4})
		.addTextField({ name:"sysAvgMthd", bind:"{d.sysAvgMthd}", dataIndex:"sysAvgMthd", noEdit:true , maxLength:4})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["sysUnits", "sysVol", "sysWeight", "sysDensity"]);
	}
});
