/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Tolerance_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Tolerance_Ds,
		
			/* ================ Business functions ================ */
	
	canDoDelete: function() {
		
					var selRecords = this.selectedRecords;
		
					for (var i = 0; i < selRecords.length; i++) {
		               if (selRecords[i].data.system == true) {
						  return false;
					   }
					}
					return  true;
	}

});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Tolerance_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Tolerance_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"name", dataIndex:"name", maxLength:64,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.ToleranceNameLov_Lov", selectOnFocus:true, maxLength:64}})
			.addBooleanField({ name:"system", dataIndex:"system"})
			.addTextField({ name:"toleranceFor", dataIndex:"toleranceFor", maxLength:32})
			.addNumberField({name:"absDevDown", dataIndex:"absDevDown", width:160, sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"absDevUp", dataIndex:"absDevUp", width:160, sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"relativeDevDown", dataIndex:"relativeDevDown", width:160, sysDec:"dec_prc", maxLength:19})
			.addNumberField({name:"relativeDevUp", dataIndex:"relativeDevUp", width:160, sysDec:"dec_prc", maxLength:19})
			.addCombo({ xtype:"combo", name:"tolLmtInd", dataIndex:"tolLmtInd", store:[ __FMBAS_TYPE__.TolLimits._NARROWER_, __FMBAS_TYPE__.TolLimits._WIDER_]})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Tolerance_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Tolerance_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:200})
		.addBooleanColumn({ name:"system", dataIndex:"system", width:80})
		.addTextColumn({ name:"toleranceFor", dataIndex:"toleranceFor", width:70})
		.addNumberColumn({ name:"absDevDown", dataIndex:"absDevDown", width:160, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"absDevUp", dataIndex:"absDevUp", width:160, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"relativeDevDown", dataIndex:"relativeDevDown", width:160, sysDec:"dec_prc"})
		.addNumberColumn({ name:"relativeDevUp", dataIndex:"relativeDevUp", width:160, sysDec:"dec_prc"})
		.addTextColumn({ name:"tolLmtInd", dataIndex:"tolLmtInd", width:100})
		.addTextColumn({ name:"parameterType", dataIndex:"parameterType", hidden:true, width:100})
		.addTextColumn({ name:"tolType", dataIndex:"toleranceType", hidden:true, width:100})
		.addTextColumn({ name:"curr", dataIndex:"currCode", hidden:true, width:80})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", hidden:true, width:80})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Tolerance_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Tolerance_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, maxLength:64, labelWidth:155})
		.addDisplayFieldText({ name:"percentDown", bind:"{d.percent}", dataIndex:"percent", maxLength:1, labelWidth:20, xtype:"percent"})
		.addDisplayFieldText({ name:"percentUp", bind:"{d.percent}", dataIndex:"percent", maxLength:1, labelWidth:20, xtype:"percent"})
		.addCombo({ xtype:"combo", name:"parameterType", bind:"{d.parameterType}", dataIndex:"parameterType", noUpdate:true, allowBlank:false, width:295, store:[ __FMBAS_TYPE__.ParamType._NOT_AVAILABLE_, __FMBAS_TYPE__.ParamType._A_C_TYPE_, __FMBAS_TYPE__.ParamType._CURRENCY_, __FMBAS_TYPE__.ParamType._SUPPLIER_, __FMBAS_TYPE__.ParamType._UPLIFT_STATUS_, __FMBAS_TYPE__.ParamType._STORAGE_TYPE_], labelWidth:155,listeners:{
			change:{scope:this, fn:this.afterParameterTypeChange}
		}})
		.addLov({name:"toleranceCurrency", bind:"{d.currCode}", dataIndex:"currCode", _visibleFn_: function(dc, rec) { return dc.record.data.parameterType == 'Currency' ; } , width:145, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:65,
			retFieldMapping: [{lovField:"id", dsField: "currId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addLov({name:"toleranceAcType", bind:"{d.toleranceFor}", dataIndex:"toleranceFor", width:145, xtype:"fmbas_AcTypesLov_Lov", maxLength:32, labelWidth:65})
		.addCombo({ xtype:"combo", name:"toleranceType", bind:"{d.toleranceType}", dataIndex:"toleranceType", noUpdate:true, allowBlank:false, width:295, store:[ __FMBAS_TYPE__.ToleranceType._QUANTITY_, __FMBAS_TYPE__.ToleranceType._DIMENSIONLESS_NUMBER_, __FMBAS_TYPE__.ToleranceType._PRICE_, __FMBAS_TYPE__.ToleranceType._COST_, __FMBAS_TYPE__.ToleranceType._AMOUNT_], labelWidth:155,listeners:{
			change:{scope:this, fn:this.afterToleranceTypeChange}
		}})
		.addLov({name:"unitQuantity", bind:"{d.unitCode}", dataIndex:"unitCode", _visibleFn_: function(dc, rec) { return dc.record.data.toleranceType == 'Quantity' ; } , width:140, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, labelWidth:65,
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ]})
		.addLov({name:"unitCurrency", bind:"{d.currCode}", dataIndex:"currCode", _visibleFn_: function(dc, rec) { return dc.record.data.toleranceType == 'Cost' ; } , width:140, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:65,
			retFieldMapping: [{lovField:"id", dsField: "currId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addLov({name:"unitPriceCurrency", bind:"{d.currCode}", dataIndex:"currCode", _visibleFn_: function(dc, rec) { return dc.record.data.toleranceType == 'Price' ; } , width:140, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:65,
			retFieldMapping: [{lovField:"id", dsField: "currId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addLov({name:"unitPrice", bind:"{d.unitCode}", dataIndex:"unitCode", _visibleFn_: function(dc, rec) { return dc.record.data.toleranceType == 'Price' ; } , width:115, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, labelWidth:40,
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ]})
		.addNumberField({name:"absDevDown", bind:"{d.absDevDown}", dataIndex:"absDevDown", width:225, sysDec:"dec_crncy", maxLength:19, labelWidth:155})
		.addNumberField({name:"absDevUp", bind:"{d.absDevUp}", dataIndex:"absDevUp", width:215, sysDec:"dec_crncy", maxLength:19, labelWidth:161})
		.addNumberField({name:"relativeDevDown", bind:"{d.relativeDevDown}", dataIndex:"relativeDevDown", width:225, sysDec:"dec_prc", maxLength:19, labelWidth:155})
		.addNumberField({name:"relativeDevUp", bind:"{d.relativeDevUp}", dataIndex:"relativeDevUp", width:190, sysDec:"dec_prc", maxLength:19, labelWidth:135})
		.addTextArea({ name:"description", bind:"{d.description}", dataIndex:"description", allowBlank:false, width:400, labelWidth:155})
		.addBooleanField({ name:"system", bind:"{d.system}", dataIndex:"system", noEdit:true , labelWidth:155})
		.addCombo({ xtype:"combo", name:"tolLmtInd", bind:"{d.tolLmtInd}", dataIndex:"tolLmtInd", allowBlank:false, width:295, store:[ __FMBAS_TYPE__.TolLimits._NARROWER_, __FMBAS_TYPE__.TolLimits._WIDER_], labelWidth:155})
		.add({name:"toleranceUnit", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("toleranceType")]})
		.add({name:"parameterTolerance", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("parameterType"),this._getConfig_("toleranceCurrency")]})
		.add({name:"absUpDown", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("absDevDown"),this._getConfig_("absDevUp"),this._getConfig_("unitQuantity"),this._getConfig_("unitCurrency"),this._getConfig_("unitPriceCurrency"),this._getConfig_("unitPrice")]})
		.add({name:"relativeUpDown", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("relativeDevDown"),this._getConfig_("percentDown"),this._getConfig_("relativeDevUp"),this._getConfig_("percentUp")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:550})
		.addPanel({ name:"col1", width:440, layout:"anchor"})
		.addPanel({ name:"col2", width:480, layout:"anchor"})
		.addPanel({ name:"col3", width:550, layout:"anchor"})
		.addPanel({ name:"col4", width:285, layout:"anchor"})
		.addPanel({ name:"col5", width:720, layout:"anchor"})
		.addPanel({ name:"col6", width:295, layout:"anchor"})
		.addPanel({ name:"col7", width:580, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5", "col6", "col7"])
		.addChildrenTo("col1", ["name"])
		.addChildrenTo("col2", ["parameterTolerance"])
		.addChildrenTo("col3", ["toleranceUnit"])
		.addChildrenTo("col4", ["system"])
		.addChildrenTo("col5", ["absUpDown", "relativeUpDown"])
		.addChildrenTo("col6", ["tolLmtInd"])
		.addChildrenTo("col7", ["description"]);
	},
	/* ==================== Business functions ==================== */
	
	afterParameterTypeChange: function(combo,newVal,oldVal) {
		
						//var parameterType = rec.get("parameterType");
						var parameterType = newVal;
		
						var lovList = ["toleranceCurrency"]; //, "toleranceAcType"];
						var totalLovs = lovList.length;
						
						if (parameterType == "Currency") {
						    var currentLovName = "toleranceCurrency";
		//				} else if (parameterType == "A/C type") {
		//				    var currentLovName = "toleranceAcType";
						} else if (parameterType == "Supplier" || parameterType == "Uplift status" || parameterType == "Storage type") {
							var currentLovName = "";
						    
						}
			
						var rec = this.getRecord();
						if(rec){
							var isDirty = this._controller_.isCurrentRecordDirty();
							if(isDirty){
								rec.set("toleranceFor","");
							}
						}
		
						for (var i = 0; i < totalLovs; i++) {
						    if (currentLovName!="" && currentLovName == lovList[i]) {
						        this._get_(currentLovName).show();
						    } else {
						        this._get_(lovList[i]).hide();
								if(isDirty){
									this._get_(lovList[i]).reset();
								}
						    }
						}
	},
	
	afterToleranceTypeChange: function(combo,newVal,oldVal) {
		
						//var toleranceType = rec.get("toleranceType");
						var toleranceType = newVal;
		
						var lovList = ["unitQuantity", "unitCurrency", "unitPriceCurrency" , "unitPrice"];
						var totalLovs = lovList.length;
		
		 				var currentLovName = "";
						var secondLovName = "";
						
						if (toleranceType == "Quantity") {
						    var currentLovName = "unitQuantity";
						}
						else if (toleranceType == "Cost") {
						    var currentLovName = "unitCurrency";
						} 
						else if (toleranceType == "Price") {
						    var currentLovName = "unitPriceCurrency";
							var secondLovName = "unitPrice";
						}
						else if (toleranceType == "Dimensionless number") {
							var currentLovName = "";
						    
						}
			
						var rec = this.getRecord();
						if(rec){
							var isDirty = this._controller_.isCurrentRecordDirty();
							if(isDirty){
								rec.set("unit","");
							}
						}
		
						for (var i = 0; i < totalLovs; i++) {
						    if (currentLovName!="" && currentLovName == lovList[i]) {
						        this._get_(currentLovName).show();
						    }
							else if (secondLovName!="" && secondLovName == lovList[i]) {
						        this._get_(secondLovName).show();
						    } 
							else {
						        this._get_(lovList[i]).hide();
								if(isDirty){
									this._get_(lovList[i]).reset();
								}
						    }
						}
	}
});
