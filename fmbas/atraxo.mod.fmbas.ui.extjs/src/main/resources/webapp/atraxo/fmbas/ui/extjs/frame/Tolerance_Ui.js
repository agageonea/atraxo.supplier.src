/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.Tolerance_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Tolerance_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("tolerance", Ext.create(atraxo.fmbas.ui.extjs.dc.Tolerance_Dc,{}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnOnEditWdwClose",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnOnEditWdwClose, scope:this})
		.addButton({name:"btnSaveCloseWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseWdw, scope:this})
		.addButton({name:"btnSaveNewWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewWdw, scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdw1",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw1, scope:this})
		.addDcFilterFormView("tolerance", {name:"Filter", xtype:"fmbas_Tolerance_Dc$Filter"})
		.addDcGridView("tolerance", {name:"List", xtype:"fmbas_Tolerance_Dc$List"})
		.addDcFormView("tolerance", {name:"PopUp", xtype:"fmbas_Tolerance_Dc$Edit"})
		.addDcFormView("tolerance", {name:"Edit", xtype:"fmbas_Tolerance_Dc$Edit"})
		.addWindow({name:"wdwTolerance", _hasTitle_:true, width:760, height:400, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("PopUp")],  listeners:{ close:{fn:this.onWdwToleranceClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewWdw"), this._elems_.get("btnSaveCloseWdw"), this._elems_.get("btnOnEditWdwClose")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["List"], ["center"])
		.addChildrenTo("canvas2", ["Edit"], ["center"])
		.addToolbarTo("List", "tlbList")
		.addToolbarTo("Edit", "tlbEdit");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbList", {dc: "tolerance"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbEdit", {dc: "tolerance"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw1")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnOnEditWdwClose
	 */
	,onBtnOnEditWdwClose: function() {
		this.onWdwToleranceClose();
		this._getWindow_("wdwTolerance").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseWdw
	 */
	,onBtnSaveCloseWdw: function() {
		this.saveClose();
		this._getDc_("tolerance").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSaveNewWdw
	 */
	,onBtnSaveNewWdw: function() {
		this.saveNew();
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdw1
	 */
	,onHelpWdw1: function() {
		this.openHelp();
	}
	
	,onShowWdw: function() {	
		this._getWindow_("wdwTolerance").show();
	}
	
	,_afterDefineDcs_: function() {
		
		
					var tolerance = this._getDc_("tolerance");
		
					tolerance.on("afterDoNew", function (dc) {
						dc.setEditMode();
					    this.onShowWdw();
					}, this);
		
					this._getDc_("tolerance").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwTolerance").close();
						}
					}, this);
		
	}
	
	,saveNew: function() {
		
					var tol = this._getDc_("tolerance");
					tol.doSave({doNewAfterSave:true});
	}
	
	,saveClose: function() {
		
					var tol = this._getDc_("tolerance");
					tol.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/Tolerances.html";
					window.open( url, "SONE_Help");
	}
	
	,onWdwToleranceClose: function() {
		
						var dc = this._getDc_("tolerance");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
});
