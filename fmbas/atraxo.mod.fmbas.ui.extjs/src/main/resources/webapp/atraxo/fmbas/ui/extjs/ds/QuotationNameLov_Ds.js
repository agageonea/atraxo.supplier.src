/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.QuotationNameLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_QuotationNameLov_Ds"
	},
	
	
	fields: [
		{name:"convFctr", type:"string"},
		{name:"tsId", type:"int", allowNull:true},
		{name:"externalSerieName", type:"string"},
		{name:"tsName", type:"string"},
		{name:"tsDescription", type:"string"},
		{name:"arithmOper", type:"float", allowNull:true},
		{name:"tsConvFctr", type:"string"},
		{name:"decimals", type:"int", allowNull:true},
		{name:"dataProvider", type:"string"},
		{name:"serieType", type:"string"},
		{name:"tsStatus", type:"string"},
		{name:"financialSourceName", type:"string"},
		{name:"financialSourceCode", type:"string"},
		{name:"currency1iD", type:"int", allowNull:true},
		{name:"currency1Code", type:"string"},
		{name:"currency2Id", type:"int", allowNull:true},
		{name:"currency2Code", type:"string"},
		{name:"avgId", type:"int", allowNull:true},
		{name:"avgMethodIndicatorName", type:"string"},
		{name:"avgMethodIndicatorCode", type:"string"},
		{name:"name", type:"string"},
		{name:"active", type:"boolean"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"unitTypeInd", type:"string"},
		{name:"unit2Id", type:"int", allowNull:true},
		{name:"unit2Code", type:"string"},
		{name:"unit2TypeInd", type:"string"},
		{name:"calcVal", type:"string"},
		{name:"priceValueType", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.QuotationNameLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"convFctr", type:"string"},
		{name:"tsId", type:"int", allowNull:true},
		{name:"externalSerieName", type:"string"},
		{name:"tsName", type:"string"},
		{name:"tsDescription", type:"string"},
		{name:"arithmOper", type:"float", allowNull:true},
		{name:"tsConvFctr", type:"string"},
		{name:"decimals", type:"int", allowNull:true},
		{name:"dataProvider", type:"string"},
		{name:"serieType", type:"string"},
		{name:"tsStatus", type:"string"},
		{name:"financialSourceName", type:"string"},
		{name:"financialSourceCode", type:"string"},
		{name:"currency1iD", type:"int", allowNull:true},
		{name:"currency1Code", type:"string"},
		{name:"currency2Id", type:"int", allowNull:true},
		{name:"currency2Code", type:"string"},
		{name:"avgId", type:"int", allowNull:true},
		{name:"avgMethodIndicatorName", type:"string"},
		{name:"avgMethodIndicatorCode", type:"string"},
		{name:"name", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"unitTypeInd", type:"string"},
		{name:"unit2Id", type:"int", allowNull:true},
		{name:"unit2Code", type:"string"},
		{name:"unit2TypeInd", type:"string"},
		{name:"calcVal", type:"string"},
		{name:"priceValueType", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
