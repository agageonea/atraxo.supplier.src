/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.CompanyProfile_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.CompanyProfile_Ds
});

/* ================= EDIT FORM: Form ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.CompanyProfile_Dc$Form", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_CompanyProfile_Dc$Form",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", noEdit:true , noLabel: true, maxLength:32})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", noLabel: true, maxLength:100})
		.addTextField({ name:"currency", bind:"{d.sysCurrency}", dataIndex:"sysCurrency", noEdit:true , noLabel: true, maxLength:4})
		.addLov({name:"financialSource", bind:"{d.sysFinancialSource}", dataIndex:"sysFinancialSource", noLabel: true, xtype:"fmbas_CodeLov_Lov", maxLength:32})
		.addTextField({ name:"unit", bind:"{d.sysUnits}", dataIndex:"sysUnits", noEdit:true , noLabel: true, maxLength:32})
		.addDisplayFieldText({ name:"codeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"nameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"currLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"fsLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"unitLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"table", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["table"])
		.addChildrenTo("table", ["codeLabel", "code", "nameLabel", "name", "currLabel", "currency", "fsLabel", "financialSource", "unitLabel", "unit"]);
	}
});
