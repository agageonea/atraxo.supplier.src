/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Workflow_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_Workflow_Ds"
	},
	
	
	initRecord: function() {
		this.set("deployStatus", "New");
		this.set("workflowGroup", "");
	},
	
	fields: [
		{name:"customerCode", type:"string"},
		{name:"customerId", type:"int", allowNull:true},
		{name:"name", type:"string"},
		{name:"code", type:"string"},
		{name:"workflowFile", type:"string"},
		{name:"processKey", type:"string"},
		{name:"description", type:"string"},
		{name:"deployStatus", type:"string"},
		{name:"deployVersion", type:"int", allowNull:true},
		{name:"workflowGroup", type:"string"},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Workflow_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"customerCode", type:"string"},
		{name:"customerId", type:"int", allowNull:true},
		{name:"name", type:"string"},
		{name:"code", type:"string"},
		{name:"workflowFile", type:"string"},
		{name:"processKey", type:"string"},
		{name:"description", type:"string"},
		{name:"deployStatus", type:"string"},
		{name:"deployVersion", type:"int", allowNull:true},
		{name:"workflowGroup", type:"string"},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.fmbas.ui.extjs.ds.Workflow_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"copyResultDescription", type:"string"},
		{name:"copyResultValue", type:"boolean"},
		{name:"subsidiary", type:"string"}
	]
});
