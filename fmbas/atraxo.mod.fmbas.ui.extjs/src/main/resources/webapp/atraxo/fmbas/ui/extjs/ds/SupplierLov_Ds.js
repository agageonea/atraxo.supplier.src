/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.SupplierLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_SupplierLov_Ds"
	},
	
	
	fields: [
		{name:"buyerId", type:"string"},
		{name:"buyerName", type:"string"},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"iataCode", type:"string"},
		{name:"iplAgent", type:"boolean"},
		{name:"fuelSupplier", type:"boolean"},
		{name:"fixedBaseOperator", type:"boolean"},
		{name:"isSupplier", type:"boolean"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.SupplierLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"buyerId", type:"string"},
		{name:"buyerName", type:"string"},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"iataCode", type:"string"},
		{name:"iplAgent", type:"boolean", allowNull:true},
		{name:"fuelSupplier", type:"boolean", allowNull:true},
		{name:"fixedBaseOperator", type:"boolean", allowNull:true},
		{name:"isSupplier", type:"boolean", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
