/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.FieldLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_FieldLov_Lov",
	displayField: "fieldName", 
	_columns_: [],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{fieldName}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.fmbas.ui.extjs.ds.Field_Ds
});
