/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.lov.QuotDescriptionLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.fmbas_QuotDescriptionLov_Lov",
	displayField: "tsDescription", 
	_columns_: ["tsDescription"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{tsDescription}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.fmbas.ui.extjs.ds.QuotationNameLov_Ds
});
