/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Country_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Country_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Country_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Country_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"codeX", dataIndex:"code", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CountryLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "id"} ,{lovField:"countryId", dsField: "text"} ]}})
			.addTextField({ name:"name", dataIndex:"name", maxLength:100})
			.addCombo({ xtype:"combo", name:"continent", dataIndex:"continent", store:[ __FMBAS_TYPE__.Continents._AFRICA_, __FMBAS_TYPE__.Continents._ANTARCTICA_, __FMBAS_TYPE__.Continents._ASIA_, __FMBAS_TYPE__.Continents._OCEANIA_, __FMBAS_TYPE__.Continents._EUROPE_, __FMBAS_TYPE__.Continents._NORTH_AMERICA_, __FMBAS_TYPE__.Continents._SOUTH_AMERICA_]})
			.addLov({name:"countryCode", dataIndex:"countryCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CountryLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "countryId"} ],
					filterFieldMapping: [{lovField:"id", dsField: "text"} ]}})
			.addBooleanField({ name:"active", dataIndex:"active"})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Country_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_Country_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"codeX", dataIndex:"code", width:50, allowBlank: false, maxLength:25, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:25}})
		.addTextColumn({name:"name", dataIndex:"name", width:150, allowBlank: false, maxLength:100, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:100}})
		.addComboColumn({name:"continent", dataIndex:"continent", width:100, 
			editor:{xtype:"combo", mode: 'local', triggerAction:'all', forceSelection:true, store:[ __FMBAS_TYPE__.Continents._AFRICA_, __FMBAS_TYPE__.Continents._ANTARCTICA_, __FMBAS_TYPE__.Continents._ASIA_, __FMBAS_TYPE__.Continents._OCEANIA_, __FMBAS_TYPE__.Continents._EUROPE_, __FMBAS_TYPE__.Continents._NORTH_AMERICA_, __FMBAS_TYPE__.Continents._SOUTH_AMERICA_]}})
		.addLov({name:"country", dataIndex:"countryCode", width:120, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_CountryLov_Lov", maxLength:25,
				retFieldMapping: [{lovField:"id", dsField: "countryId"} ]}})
		.addBooleanColumn({name:"active", dataIndex:"active"})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
