/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.TemplatesLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_TemplatesLov_Ds"
	},
	
	
	fields: [
		{name:"userId", type:"string"},
		{name:"userName", type:"string"},
		{name:"userCode", type:"string"},
		{name:"typeId", type:"int", allowNull:true},
		{name:"typeName", type:"string"},
		{name:"subsidiaryId", type:"int", allowNull:true},
		{name:"subsidiaryCode", type:"string"},
		{name:"name", type:"string"},
		{name:"fileName", type:"string"},
		{name:"uploadDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"fileReference", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.TemplatesLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"userId", type:"string"},
		{name:"userName", type:"string"},
		{name:"userCode", type:"string"},
		{name:"typeId", type:"int", allowNull:true},
		{name:"typeName", type:"string"},
		{name:"subsidiaryId", type:"int", allowNull:true},
		{name:"subsidiaryCode", type:"string"},
		{name:"name", type:"string"},
		{name:"fileName", type:"string"},
		{name:"uploadDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"fileReference", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
