/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Note_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.Note_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.Note_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Note_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Note_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateField({name:"createdAfter", paramIndex:"createdAfter"})
			.addDateField({name:"createdBefore", paramIndex:"createdBefore"})
			.addTextField({ name:"createdBy", dataIndex:"createdBy", maxLength:32})
			.addTextField({ name:"notes", dataIndex:"notes", maxLength:2000})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Note_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Note_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"createdAt", dataIndex:"createdAt", width:150, _mask_: Masks.DATETIME})
		.addTextColumn({ name:"createdBy", dataIndex:"createdBy", width:150})
		.addTextColumn({ name:"notes", dataIndex:"notes", width:500})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Note_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Note_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"notes", bind:"{d.notes}", dataIndex:"notes", allowBlank:false, width:400, labelWidth:45})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:420, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["notes"]);
	}
});
