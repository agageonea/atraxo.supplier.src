/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.AccountingRules_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.AccountingRules_Ds
});

/* ================= EDIT-GRID: AccountingRulesList ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.AccountingRules_Dc$AccountingRulesList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_AccountingRules_Dc$AccountingRulesList",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addComboColumn({name:"ruleType", dataIndex:"ruleType", width:500, allowBlank: false, 
			editor:{xtype:"combo", mode: 'local', allowBlank:false, triggerAction:'all', forceSelection:true, store:[ __FMBAS_TYPE__.AccountingRulesType._EXPORT_DETAILED_COMPOSITE_PRICE_, __FMBAS_TYPE__.AccountingRulesType._REPLACE_ON_INVOICE_EXPORT_THE_INVOICE_RECEIVER_WITH_SHIP_TO_]}})
		.addLov({name:"subsidiary", dataIndex:"subsidiaryCode", width:100, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_SubsidiaryLov_Lov", allowBlank:false, maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "subsidiaryId"} ]}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.AccountingRules_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_AccountingRules_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addCombo({ xtype:"combo", name:"ruleType", dataIndex:"ruleType", store:[ __FMBAS_TYPE__.AccountingRulesType._EXPORT_DETAILED_COMPOSITE_PRICE_, __FMBAS_TYPE__.AccountingRulesType._REPLACE_ON_INVOICE_EXPORT_THE_INVOICE_RECEIVER_WITH_SHIP_TO_]})
			.addLov({name:"subsidiary", dataIndex:"subsidiaryCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SubsidiaryLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "subsidiaryId"} ]}})
		;
	}

});
