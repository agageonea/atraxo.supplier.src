/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Units_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Units_Ds
});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Units_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_Units_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"code", dataIndex:"code", width:150, maxLength:2, 
			editor: { xtype:"textfield", maxLength:2}})
		.addTextColumn({name:"iataCode", dataIndex:"iataCode", width:150, noEdit: true, maxLength:3})
		.addNumberColumn({name:"factor", dataIndex:"factor", width:150, sysDec:"dec_prc", maxLength:19, align:"right" })
		.addTextColumn({name:"name", dataIndex:"name", width:200, maxLength:100, 
			editor: { xtype:"textfield", maxLength:100}})
		.addTextColumn({name:"unitTypeIndicator", dataIndex:"unittypeInd", hidden:true, width:150, maxLength:32, 
			editor: { xtype:"textfield", maxLength:32}})
		.addBooleanColumn({name:"active", dataIndex:"active", hidden:true, width:150})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
