/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Suppliers_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Suppliers_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Suppliers_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Suppliers_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"code", dataIndex:"code", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"code", dsField: "code"} ],
					filterFieldMapping: [{lovField:"isSupplier", value: "true"} ]}})
			.addLov({name:"name", dataIndex:"name", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierNameLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"name", dsField: "name"} ]}})
			.addLov({name:"buyerName", dataIndex:"buyerName", maxLength:255,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.BuyerLov_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"buyerName", dsField: "buyerName"} ]}})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Suppliers_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Suppliers_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:100})
		.addTextColumn({ name:"name", dataIndex:"name", width:150})
		.addTextColumn({ name:"roles", dataIndex:"roles", width:280})
		.addTextColumn({ name:"contactName", dataIndex:"contactName", width:130})
		.addTextColumn({ name:"contactPhone", dataIndex:"contactPhone", width:130})
		.addTextColumn({ name:"contactEmail", dataIndex:"contactEmail", width:200})
		.addNumberColumn({ name:"exposure", dataIndex:"exposure", width:120, sysDec:"dec_prc"})
		.addTextColumn({ name:"buyerName", dataIndex:"buyerName", width:120})
		.addBooleanColumn({ name:"fuelSupplier", dataIndex:"fuelSupplier", hidden:true, width:100})
		.addBooleanColumn({ name:"iplAgent", dataIndex:"iplAgent", hidden:true, width:80})
		.addBooleanColumn({ name:"fixedBaseOperator", dataIndex:"fixedBaseOperator", hidden:true, width:130})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Suppliers_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Suppliers_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"sectionTitle", bind:"{d.sectionTitle}", dataIndex:"sectionTitle", maxLength:64, labelWidth:150})
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", allowBlank:false, maxLength:32, labelWidth:120, style:"margin-top: 27px"})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, maxLength:100, labelWidth:120})
		.addLov({name:"buyerName", bind:"{d.buyerName}", dataIndex:"buyerName", xtype:"fmbas_UserListLov_Lov", maxLength:255, labelWidth:120,
			retFieldMapping: [{lovField:"id", dsField: "buyerId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addBooleanField({ name:"fuelSupplier", bind:"{d.fuelSupplier}", dataIndex:"fuelSupplier", labelWidth:120, style:"margin-left: 50px"})
		.addBooleanField({ name:"iplAgent", bind:"{d.iplAgent}", dataIndex:"iplAgent", labelWidth:120, style:"margin-left: 50px"})
		.addBooleanField({ name:"fixedBaseOperator", bind:"{d.fixedBaseOperator}", dataIndex:"fixedBaseOperator", labelWidth:120, style:"margin-left: 50px"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"})
		.addPanel({ name:"col2", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["code", "name", "buyerName"])
		.addChildrenTo("col2", ["sectionTitle", "fuelSupplier", "iplAgent", "fixedBaseOperator"]);
	}
});

/* ================= EDIT FORM: EditContext ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Suppliers_Dc$EditContext", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Suppliers_Dc$EditContext",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"sectionTitle", bind:"{d.sectionTitle}", dataIndex:"sectionTitle", maxLength:64, labelWidth:130})
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", allowBlank:false, maxLength:32, labelWidth:115, style:"margin-top: 27px"})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, width:300, maxLength:100, labelWidth:115})
		.addLov({name:"buyerName", bind:"{d.buyerName}", dataIndex:"buyerName", width:315, xtype:"fmbas_UserListLov_Lov", maxLength:255, labelWidth:110,
			retFieldMapping: [{lovField:"id", dsField: "buyerId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addBooleanField({ name:"fuelSupplier", bind:"{d.fuelSupplier}", dataIndex:"fuelSupplier", labelWidth:115})
		.addBooleanField({ name:"iplAgent", bind:"{d.iplAgent}", dataIndex:"iplAgent", labelWidth:115})
		.addBooleanField({ name:"fixedBaseOperator", bind:"{d.fixedBaseOperator}", dataIndex:"fixedBaseOperator", labelWidth:115})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"})
		.addPanel({ name:"col2", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["code", "name", "buyerName"])
		.addChildrenTo("col2", ["sectionTitle", "fuelSupplier", "iplAgent", "fixedBaseOperator"]);
	}
});

/* ================= GRID: Temp ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Suppliers_Dc$Temp", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Suppliers_Dc$Temp",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
