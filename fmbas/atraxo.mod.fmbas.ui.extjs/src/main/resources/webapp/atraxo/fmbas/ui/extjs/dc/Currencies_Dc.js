/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Currencies_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Currencies_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Currencies_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Currencies_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:100})
			.addLov({name:"code", dataIndex:"code", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"code", dsField: "code"} ]}})
			.addTextField({ name:"isoNo", dataIndex:"isoNo", maxLength:3})
			.addNumberField({name:"decCnt", dataIndex:"decCnt", maxLength:11})
			.addBooleanField({ name:"active", dataIndex:"active"})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Currencies_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_Currencies_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"name", dataIndex:"name", width:150, maxLength:100, 
			editor: { xtype:"textfield", maxLength:100}})
		.addTextColumn({name:"code", dataIndex:"code", width:120, allowBlank: false, maxLength:3, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:3}})
		.addTextColumn({name:"isoNo", dataIndex:"isoNo", width:100, allowBlank: false, maxLength:3, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:3}})
		.addNumberColumn({name:"decCnt", dataIndex:"decCnt", width:150, allowBlank: false, maxLength:11, align:"right" })
		.addBooleanColumn({name:"active", dataIndex:"active"})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
