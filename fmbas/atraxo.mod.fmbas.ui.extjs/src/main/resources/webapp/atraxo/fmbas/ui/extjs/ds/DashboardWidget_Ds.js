/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.DashboardWidget_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_DashboardWidget_Ds"
	},
	
	
	fields: [
		{name:"dashboardId", type:"int", allowNull:true},
		{name:"dashboardName", type:"string"},
		{name:"layoutId", type:"int", allowNull:true},
		{name:"layoutName", type:"string"},
		{name:"widgetId", type:"int", allowNull:true},
		{name:"widgetName", type:"string"},
		{name:"widgetDs", type:"string"},
		{name:"widgetMethod", type:"string"},
		{name:"id", type:"int", allowNull:true},
		{name:"layoutRegionId", type:"string"},
		{name:"widgetIndex", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.DashboardWidget_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"dashboardId", type:"int", allowNull:true},
		{name:"dashboardName", type:"string"},
		{name:"layoutId", type:"int", allowNull:true},
		{name:"layoutName", type:"string"},
		{name:"widgetId", type:"int", allowNull:true},
		{name:"widgetName", type:"string"},
		{name:"widgetDs", type:"string"},
		{name:"widgetMethod", type:"string"},
		{name:"id", type:"int", allowNull:true},
		{name:"layoutRegionId", type:"string"},
		{name:"widgetIndex", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.fmbas.ui.extjs.ds.DashboardWidget_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"rpcResponse", type:"string"}
	]
});
