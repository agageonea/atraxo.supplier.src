/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.DocumentNumberSeries_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.DocumentNumberSeries_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.DocumentNumberSeries_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_DocumentNumberSeries_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"prefix", dataIndex:"prefix", maxLength:64})
			.addNumberField({name:"startIndex", dataIndex:"startIndex", maxLength:11})
			.addNumberField({name:"endIndex", dataIndex:"endIndex", maxLength:11})
			.addNumberField({name:"currentIndex", dataIndex:"currentIndex", maxLength:11})
			.addBooleanField({ name:"enabled", dataIndex:"enabled"})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.DocumentNumberSeries_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_DocumentNumberSeries_Dc$List",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"prefix", dataIndex:"prefix", width:100, maxLength:64, 
			editor: { xtype:"textfield", maxLength:64}})
		.addNumberColumn({name:"startIndex", dataIndex:"startIndex", width:100, maxLength:11, align:"right" })
		.addNumberColumn({name:"endIndex", dataIndex:"endIndex", width:100, maxLength:11, align:"right" })
		.addNumberColumn({name:"currentIndex", dataIndex:"currentIndex", width:150, noEdit: true, maxLength:11, align:"right" })
		.addBooleanColumn({name:"enabled", dataIndex:"enabled", width:100, noEdit: true})
		.addComboColumn({name:"type", dataIndex:"type", width:100, 
			editor:{xtype:"combo", mode: 'local',listeners:{
				expand:{scope:this, fn:this._filterByNotInvoice_}, buffer: Main.viewConfig.GRID_EDITOR_CHANGE_EVT_BUFFER
			}, triggerAction:'all', forceSelection:true, store:[ __FMBAS_TYPE__.DocumentNumberSeriesType._INVOICE_, __FMBAS_TYPE__.DocumentNumberSeriesType._CUSTOMER_, __FMBAS_TYPE__.DocumentNumberSeriesType._CREDIT_MEMO_, __FMBAS_TYPE__.DocumentNumberSeriesType._CONSOLIDATED_STATEMENT_]}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_filterByNotInvoice_: function(el) {
		
						var s = el.store;
						s.filter(function(r) {
							var value = r.get("field1");
							if ( value == "Customer" ) {
								return value;
							}
		
						});
	}
});
