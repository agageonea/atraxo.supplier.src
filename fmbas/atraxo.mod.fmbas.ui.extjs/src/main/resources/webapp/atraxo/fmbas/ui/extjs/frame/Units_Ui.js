/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.Units_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Units_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("units", Ext.create(atraxo.fmbas.ui.extjs.dc.Units_Dc,{}))
		.addDc("sysParams", Ext.create(atraxo.fmbas.ui.extjs.dc.SysParams_Dc,{}))
		.addDc("umass", Ext.create(atraxo.fmbas.ui.extjs.dc.Units_Dc,{multiEdit: true}))
		.addDc("uvol", Ext.create(atraxo.fmbas.ui.extjs.dc.Units_Dc,{multiEdit: true}))
		.addDc("ulen", Ext.create(atraxo.fmbas.ui.extjs.dc.Units_Dc,{multiEdit: true}))
		.addDc("utime", Ext.create(atraxo.fmbas.ui.extjs.dc.Units_Dc,{multiEdit: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"btnCancelWithMenu1",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"umass", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected1",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu1", handler: this.onBtnCancelSelected1, scope:this})
		.addButton({name:"btnCancelAll1",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu1", _isDefaultHandler_:true, handler: this.onBtnCancelAll1, scope:this})
		.addButton({name:"btnCancelWithMenu2",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"uvol", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected2",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu2", handler: this.onBtnCancelSelected2, scope:this})
		.addButton({name:"btnCancelAll2",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu2", _isDefaultHandler_:true, handler: this.onBtnCancelAll2, scope:this})
		.addButton({name:"btnCancelWithMenu3",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"ulen", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected3",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu3", handler: this.onBtnCancelSelected3, scope:this})
		.addButton({name:"btnCancelAll3",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu3", _isDefaultHandler_:true, handler: this.onBtnCancelAll3, scope:this})
		.addButton({name:"btnCancelWithMenu4",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"utime", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected4",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu4", handler: this.onBtnCancelSelected4, scope:this})
		.addButton({name:"btnCancelAll4",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu4", _isDefaultHandler_:true, handler: this.onBtnCancelAll4, scope:this})
		.addDcFormView("sysParams", {name:"Form", xtype:"fmbas_SysParams_Dc$FormParams"})
		.addDcEditGridView("umass", {name:"Mass", _hasTitle_:true, xtype:"fmbas_Units_Dc$List", frame:true})
		.addDcEditGridView("uvol", {name:"Volume", _hasTitle_:true, xtype:"fmbas_Units_Dc$List", frame:true})
		.addDcEditGridView("ulen", {name:"Lenght", _hasTitle_:true, xtype:"fmbas_Units_Dc$List", frame:true})
		.addDcEditGridView("utime", {name:"Time", _hasTitle_:true, xtype:"fmbas_Units_Dc$List", frame:true})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas1", dcName:"sysParams"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["Form", "detailsTab"], ["north", "center"])
		.addChildrenTo("detailsTab", ["Mass"])
		.addToolbarTo("Form", "tlbForm")
		.addToolbarTo("Mass", "tlbList")
		.addToolbarTo("Volume", "tlbList1")
		.addToolbarTo("Lenght", "tlbList2")
		.addToolbarTo("Time", "tlbList3");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("detailsTab", ["Volume", "Lenght", "Time"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbForm", {dc: "units"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbList", {dc: "umass"})
			.addButtons([])
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelWithMenu1"),this._elems_.get("btnCancelSelected1"),this._elems_.get("btnCancelAll1")])
			.addReports()
		.end()
		.beginToolbar("tlbList1", {dc: "uvol"})
			.addButtons([])
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelWithMenu2"),this._elems_.get("btnCancelSelected2"),this._elems_.get("btnCancelAll2")])
			.addReports()
		.end()
		.beginToolbar("tlbList2", {dc: "ulen"})
			.addButtons([])
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelWithMenu3"),this._elems_.get("btnCancelSelected3"),this._elems_.get("btnCancelAll3")])
			.addReports()
		.end()
		.beginToolbar("tlbList3", {dc: "utime"})
			.addButtons([])
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelWithMenu4"),this._elems_.get("btnCancelSelected4"),this._elems_.get("btnCancelAll4")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnCancelSelected1
	 */
	,onBtnCancelSelected1: function() {
		this.fnCancelSelected1();
	}
	
	/**
	 * On-Click handler for button btnCancelAll1
	 */
	,onBtnCancelAll1: function() {
		this.fnCancelAll1();
	}
	
	/**
	 * On-Click handler for button btnCancelSelected2
	 */
	,onBtnCancelSelected2: function() {
		this.fnCancelSelected2();
	}
	
	/**
	 * On-Click handler for button btnCancelAll2
	 */
	,onBtnCancelAll2: function() {
		this.fnCancelAll2();
	}
	
	/**
	 * On-Click handler for button btnCancelSelected3
	 */
	,onBtnCancelSelected3: function() {
		this.fnCancelSelected3();
	}
	
	/**
	 * On-Click handler for button btnCancelAll3
	 */
	,onBtnCancelAll3: function() {
		this.fnCancelAll3();
	}
	
	/**
	 * On-Click handler for button btnCancelSelected4
	 */
	,onBtnCancelSelected4: function() {
		this.fnCancelSelected4();
	}
	
	/**
	 * On-Click handler for button btnCancelAll4
	 */
	,onBtnCancelAll4: function() {
		this.fnCancelAll4();
	}
	
	,_afterDefineDcs_: function() {
		
		
					var um = this._getDc_("umass");
					um.setFilterValue("unittypeInd", "Mass");
					um.doQuery();
		
					um.on("afterDoQuerySuccess", function() {
						this._getDc_("sysParams").doQuery();
					},this);
		
					var uv = this._getDc_("uvol");
					uv.setFilterValue("unittypeInd", "Volume");
					uv.doQuery();
		
					var ul = this._getDc_("ulen");
					ul.setFilterValue("unittypeInd", "Length");
					ul.doQuery();
		
					var ut = this._getDc_("utime");
					ut.setFilterValue("unittypeInd", "Duration");
					ut.doQuery();
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/UnitsOfMeasurement.html";
					window.open( url, "SONE_Help");
	}
	
	,fnCancelSelected1: function() {
		
						var dc = this._getDc_("umass");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAll1: function() {
		
						var dc = this._getDc_("umass");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelSelected2: function() {
		
						var dc = this._getDc_("uvol");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAll2: function() {
		
						var dc = this._getDc_("uvol");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelSelected3: function() {
		
						var dc = this._getDc_("ulen");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAll3: function() {
		
						var dc = this._getDc_("ulen");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelSelected4: function() {
		
						var dc = this._getDc_("utime");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAll4: function() {
		
						var dc = this._getDc_("utime");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isCancelEnabled(dc);
	}
});
