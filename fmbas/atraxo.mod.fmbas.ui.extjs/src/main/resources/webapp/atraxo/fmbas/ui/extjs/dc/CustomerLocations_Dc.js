/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.CustomerLocations_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.CustomerLocations_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.CustomerLocations_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.CustomerLocations_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_CustomerLocations_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"code", dataIndex:"code", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"code", dsField: "code"} ]}})
			.addTextField({ name:"name", dataIndex:"name", maxLength:100})
			.addBooleanField({ name:"isAirport", dataIndex:"isAirport"})
			.addLov({name:"countryCode", dataIndex:"countryCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CountryLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "countryId"} ],
					filterFieldMapping: [{lovField:"active", value: "true"} ]}})
		;
	}

});

/* ================= GRID: CustomerLocation ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.CustomerLocations_Dc$CustomerLocation", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_CustomerLocations_Dc$CustomerLocation",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"locationCode", dataIndex:"code", width:200})
		.addTextColumn({ name:"locationName", dataIndex:"name", width:200})
		.addTextColumn({ name:"country", dataIndex:"countryName", width:200})
		.addBooleanColumn({ name:"isAirport", dataIndex:"isAirport", width:200})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
