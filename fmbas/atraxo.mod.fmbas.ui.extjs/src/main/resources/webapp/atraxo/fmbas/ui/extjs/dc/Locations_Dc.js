/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Locations_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.Locations_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.Locations_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Locations_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Locations_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"code", dataIndex:"code", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"code", dsField: "code"} ]}})
			.addTextField({ name:"name", dataIndex:"name", maxLength:100})
			.addBooleanField({ name:"isAirport", dataIndex:"isAirport"})
			.addLov({name:"countryCode", dataIndex:"countryCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CountryLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "countryId"} ],
					filterFieldMapping: [{lovField:"active", value: "true"} ]}})
			.addDateField({name:"validFrom", dataIndex:"validFrom"})
			.addDateField({name:"validTo", dataIndex:"validTo"})
		;
	}

});

/* ================= GRID: ListPopUp ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Locations_Dc$ListPopUp", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Locations_Dc$ListPopUp",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:60})
		.addTextColumn({ name:"name", dataIndex:"name", width:150})
		.addTextColumn({ name:"country", dataIndex:"countryCode", width:80})
		.addBooleanColumn({ name:"airport", dataIndex:"isAirport", width:80})
		.addDateColumn({ name:"validFrom", dataIndex:"validFrom", width:100, _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"validTo", width:100, _mask_: Masks.DATE})
		.addTextColumn({ name:"iataCode", dataIndex:"iataCode", hidden:true, width:80})
		.addTextColumn({ name:"icaoCode", dataIndex:"icaoCode", hidden:true, width:80})
		.addTextColumn({ name:"timezoneName", dataIndex:"timeName", hidden:true, width:100})
		.addNumberColumn({ name:"latitude", dataIndex:"latitude", hidden:true,  decimals:6})
		.addNumberColumn({ name:"longitude", dataIndex:"longitude", hidden:true,  decimals:6})
		.addTextColumn({ name:"state", dataIndex:"subCountryCode", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= GRID: SourceList ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Locations_Dc$SourceList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Locations_Dc$SourceList",
	_noPaginator_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:50,  flex:1})
		.addTextColumn({ name:"name", dataIndex:"name", width:150})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Locations_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Locations_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", allowBlank:false, maxLength:25})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", maxLength:100})
		.addTextField({ name:"iataCode", bind:"{d.iataCode}", dataIndex:"iataCode", maxLength:3})
		.addTextField({ name:"icaoCode", bind:"{d.icaoCode}", dataIndex:"icaoCode", maxLength:4})
		.addLov({name:"country", bind:"{d.countryCode}", dataIndex:"countryCode", allowBlank:false, xtype:"fmbas_CountryLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"id", dsField: "countryId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addLov({name:"subCountry", bind:"{d.subCountryCode}", dataIndex:"subCountryCode", xtype:"fmbas_CountryLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"id", dsField: "subCountryId"} ],
			filterFieldMapping: [{lovField:"countryId", dsField: "countryId"} ]})
		.addBooleanField({ name:"isAirport", bind:"{d.isAirport}", dataIndex:"isAirport", allowBlank:false})
		.addLov({name:"timezoneName", bind:"{d.timeName}", dataIndex:"timeName", xtype:"fmbas_TimeZoneLov_Lov", maxLength:100})
		.addNumberField({name:"latitude", bind:"{d.latitude}", dataIndex:"latitude", maxLength:19, decimals:6})
		.addNumberField({name:"longitude", bind:"{d.longitude}", dataIndex:"longitude", maxLength:19, decimals:6})
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", allowBlank:false})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", allowBlank:false})
		.add({name:"codeAirport", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("code"),this._getConfig_("isAirport")]})
		.add({name:"nameTime", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("name"),this._getConfig_("timezoneName")]})
		.add({name:"IATALat", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("iataCode"),this._getConfig_("latitude")]})
		.add({name:"ICAOLong", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("icaoCode"),this._getConfig_("longitude")]})
		.add({name:"countryFrom", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("country"),this._getConfig_("validFrom")]})
		.add({name:"stateTo", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("subCountry"),this._getConfig_("validTo")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:600})
		.addPanel({ name:"col1", width:560, layout:"anchor"})
		.addPanel({ name:"col2", width:560, layout:"anchor"})
		.addPanel({ name:"col3", width:560, layout:"anchor"})
		.addPanel({ name:"col4", width:560, layout:"anchor"})
		.addPanel({ name:"col5", width:560, layout:"anchor"})
		.addPanel({ name:"col6", width:560, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5", "col6"])
		.addChildrenTo("col1", ["codeAirport"])
		.addChildrenTo("col2", ["nameTime"])
		.addChildrenTo("col3", ["IATALat"])
		.addChildrenTo("col4", ["ICAOLong"])
		.addChildrenTo("col5", ["countryFrom"])
		.addChildrenTo("col6", ["stateTo"]);
	}
});

/* ================= GRID: Temp ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Locations_Dc$Temp", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Locations_Dc$Temp",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= GRID: AreasLocation ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Locations_Dc$AreasLocation", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Locations_Dc$AreasLocation",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"locationCode", dataIndex:"code", width:200})
		.addTextColumn({ name:"locationName", dataIndex:"name", width:200})
		.addTextColumn({ name:"country", dataIndex:"countryName", width:200})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
