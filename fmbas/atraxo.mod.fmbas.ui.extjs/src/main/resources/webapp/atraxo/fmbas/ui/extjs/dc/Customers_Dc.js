/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Customers_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.Customers_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.Customers_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Customers_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Customers_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"code", dataIndex:"code", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerSimpleCodeLov_Lov", selectOnFocus:true, maxLength:32}})
			.addTextField({ name:"name", dataIndex:"name", maxLength:100})
			.addCombo({ xtype:"combo", name:"type", dataIndex:"type", store:[ __FMBAS_TYPE__.CustomerType._INDEPENDENT_, __FMBAS_TYPE__.CustomerType._GROUP_, __FMBAS_TYPE__.CustomerType._GROUP_MEMBER_]})
			.addTextField({ name:"parentName", dataIndex:"parentName", maxLength:100})
			.addTextField({ name:"buyerName", dataIndex:"buyerName", maxLength:255})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __FMBAS_TYPE__.CustomerStatus._ACTIVE_, __FMBAS_TYPE__.CustomerStatus._INACTIVE_, __FMBAS_TYPE__.CustomerStatus._PROSPECT_, __FMBAS_TYPE__.CustomerStatus._BLOCKED_]})
			.addCombo({ xtype:"combo", name:"transmissionStatus", dataIndex:"transmissionStatus", store:[ __FMBAS_TYPE__.CustomerDataTransmissionStatus._NOT_AVAILABLE_, __FMBAS_TYPE__.CustomerDataTransmissionStatus._IN_PROGRESS_, __FMBAS_TYPE__.CustomerDataTransmissionStatus._SENT_, __FMBAS_TYPE__.CustomerDataTransmissionStatus._DELIVERED_, __FMBAS_TYPE__.CustomerDataTransmissionStatus._PROCESSED_, __FMBAS_TYPE__.CustomerDataTransmissionStatus._FAILED_]})
			.addCombo({ xtype:"combo", name:"processedStatus", dataIndex:"processedStatus", store:[ __FMBAS_TYPE__.CustomerDataProcessedStatus._NOT_AVAILABLE_, __FMBAS_TYPE__.CustomerDataProcessedStatus._IN_PROGRESS_, __FMBAS_TYPE__.CustomerDataProcessedStatus._SUCCESS_, __FMBAS_TYPE__.CustomerDataProcessedStatus._FAILED_]})
			.addCombo({ xtype:"combo", name:"creditUpdateRequestStatus", dataIndex:"creditUpdateRequestStatus", store:[ __FMBAS_TYPE__.CreditUpdateRequestStatus._NEW_, __FMBAS_TYPE__.CreditUpdateRequestStatus._IN_PROGRESS_, __FMBAS_TYPE__.CreditUpdateRequestStatus._FAILED_, __FMBAS_TYPE__.CreditUpdateRequestStatus._TRANSMITTED_]})
			.addTextField({ name:"accountNumber", dataIndex:"accountNumber", maxLength:32})
			.addTextField({ name:"vatRegNumber", dataIndex:"vatRegNumber", maxLength:32})
			.addTextField({ name:"taxIdNumber", dataIndex:"taxIdNumber", maxLength:32})
			.addTextField({ name:"incomeTaxNumber", dataIndex:"incomeTaxNumber", maxLength:32})
			.addLov({name:"officeCountryName", dataIndex:"officeCountryName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CountryNameLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "officeCountryId"} ,{lovField:"name", dsField: "officeCountryName"} ,{lovField:"code", dsField: "officeCountryCode"} ]}})
			.addLov({name:"officeStateName", dataIndex:"officeStateName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CountryNameLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "officeStateId"} ,{lovField:"name", dsField: "officeStateName"} ,{lovField:"code", dsField: "officeStateCode"} ],
					filterFieldMapping: [{lovField:"countryId", dsField: "officeCountryId"} ]}})
			.addTextField({ name:"officeCity", dataIndex:"officeCity", maxLength:100})
			.addTextField({ name:"officeStreet", dataIndex:"officeStreet", maxLength:50})
			.addTextField({ name:"officePostalCode", dataIndex:"officePostalCode", maxLength:32})
			.addLov({name:"billingCountryName", dataIndex:"billingCountryName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CountryNameLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "billingCountryId"} ,{lovField:"code", dsField: "billingCountryCode"} ,{lovField:"name", dsField: "billingCountryName"} ]}})
			.addLov({name:"billingStateName", dataIndex:"billingStateName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CountryNameLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "billingStateId"} ,{lovField:"code", dsField: "billingStateCode"} ,{lovField:"name", dsField: "billingStateName"} ],
					filterFieldMapping: [{lovField:"countryId", dsField: "billingCountryId"} ]}})
			.addTextField({ name:"billingCity", dataIndex:"billingCity", maxLength:100})
			.addTextField({ name:"billingStreet", dataIndex:"billingStreet", maxLength:50})
			.addTextField({ name:"billingPostalCode", dataIndex:"billingPostalCode", maxLength:32})
			.addBooleanField({ name:"system", dataIndex:"system"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Customers_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Customers_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:110})
		.addTextColumn({ name:"name", dataIndex:"name", width:160})
		.addTextColumn({ name:"type", dataIndex:"type", width:110})
		.addTextColumn({ name:"parentName", dataIndex:"parentName", width:100})
		.addNumberColumn({ name:"creditUtil", dataIndex:"creditUtil", width:110, sysDec:"dec_prc",  renderer:function(val, meta, record, rowIndex) { return this._markUtilization_(val, meta, record, rowIndex); }})
		.addTextColumn({ name:"status", dataIndex:"status", width:100})
		.addTextColumn({ name:"buyerName", dataIndex:"buyerName", width:120})
		.addDateColumn({ name:"dateofInc", dataIndex:"dateOfInc", hidden:true, width:130, _mask_: Masks.DATE})
		.addTextColumn({ name:"natureOfBus", dataIndex:"natureOfBusiness", hidden:true, width:120})
		.addTextColumn({ name:"tradelic", dataIndex:"tradeLicense", hidden:true, width:100})
		.addDateColumn({ name:"tlValidFrom", dataIndex:"tlValidFrom", hidden:true, width:100, _mask_: Masks.DATE})
		.addDateColumn({ name:"tlValidTo", dataIndex:"tlValidTo", hidden:true, width:100, _mask_: Masks.DATE})
		.addTextColumn({ name:"aoc", dataIndex:"aoc", hidden:true, width:80})
		.addDateColumn({ name:"aocValidFrom", dataIndex:"aocValidFrom", hidden:true, width:100, _mask_: Masks.DATE})
		.addDateColumn({ name:"aocValidTo", dataIndex:"aocValidTo", hidden:true, width:100, _mask_: Masks.DATE})
		.addTextColumn({ name:"phoneNo", dataIndex:"phoneNo", hidden:true, width:100})
		.addTextColumn({ name:"faxNo", dataIndex:"faxNo", hidden:true, width:100})
		.addTextColumn({ name:"website", dataIndex:"website", hidden:true, width:100})
		.addTextColumn({ name:"email", dataIndex:"email", hidden:true, width:160})
		.addNumberColumn({ name:"alertLimit", dataIndex:"alertLimit", hidden:true, width:100})
		.addTextColumn({ name:"accountNumber", dataIndex:"accountNumber", hidden:true, width:100})
		.addTextColumn({ name:"vatRegNumber", dataIndex:"vatRegNumber", hidden:true, width:100})
		.addTextColumn({ name:"taxIdNumber", dataIndex:"taxIdNumber", hidden:true, width:100})
		.addTextColumn({ name:"incomeTaxNumber", dataIndex:"incomeTaxNumber", hidden:true, width:100})
		.addTextColumn({ name:"officeStreet", dataIndex:"officeStreet", hidden:true, width:100})
		.addTextColumn({ name:"officeCity", dataIndex:"officeCity", hidden:true, width:100})
		.addTextColumn({ name:"officeStateName", dataIndex:"officeStateName", hidden:true, width:100})
		.addTextColumn({ name:"officePostalCode", dataIndex:"officePostalCode", hidden:true, width:100})
		.addTextColumn({ name:"officeCountryName", dataIndex:"officeCountryName", hidden:true, width:100})
		.addTextColumn({ name:"transmissionStatus", dataIndex:"transmissionStatus", hidden:true, width:100})
		.addTextColumn({ name:"processedStatus", dataIndex:"processedStatus", hidden:true, width:100})
		.addTextColumn({ name:"creditUpdateRequestStatus", dataIndex:"creditUpdateRequestStatus", hidden:true, width:100})
		.addTextColumn({ name:"billingStreet", dataIndex:"billingStreet", hidden:true, width:100})
		.addTextColumn({ name:"billingCity", dataIndex:"billingCity", hidden:true, width:100})
		.addTextColumn({ name:"billingStateName", dataIndex:"billingStateName", hidden:true, width:100})
		.addTextColumn({ name:"billingPostalCode", dataIndex:"billingPostalCode", hidden:true, width:100})
		.addTextColumn({ name:"billingCountryName", dataIndex:"billingCountryName", hidden:true, width:100})
		.addBooleanColumn({ name:"system", dataIndex:"system", hidden:true})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_markUtilization_: function(value,meta,record,rowIndex) {
		
		
						var globalStyles = "border:0px !important";
						
						var redStyles = "background: #F94F52; color: #FFFFFF;"+globalStyles;
						var yellowStyles = "background: #F9B344; color: #FFFFFF;"+globalStyles;
						var greenStyles = "background: #52AA39; color: #FFFFFF;"+globalStyles;
		
						var alertLimit = record.get("alertLimit");
		
						if (!Ext.isEmpty(value)) {
							if(value <= alertLimit) {
					        	meta.style = greenStyles;
						    } else if (value > alertLimit && value <= 100) {
						        meta.style = yellowStyles;
						    } else if (value > alertLimit && value >= 100) {
						        meta.style = redStyles;
						    }
							return value+" %";
						}
	}
});

/* ================= EDIT FORM: NewCustomerPopup ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Customers_Dc$NewCustomerPopup", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Customers_Dc$NewCustomerPopup",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"generateCodeNumber", scope: this, handler: this._generateCode_, text: "<i class='fa fa-refresh'></i>"})
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", allowBlank:false, width:141, maxLength:32, hideLabel:"true"})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, width:180, maxLength:100, hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"type", bind:"{d.type}", dataIndex:"type", allowBlank:false, width:180, store:[ __FMBAS_TYPE__.CustomerType._INDEPENDENT_, __FMBAS_TYPE__.CustomerType._GROUP_, __FMBAS_TYPE__.CustomerType._GROUP_MEMBER_], hideLabel:"true",listeners:{
			fpchange:{scope:this, fn:this._setParentVisibility_}
		}})
		.addTextField({ name:"tradeLicense", bind:"{d.tradeLicense}", dataIndex:"tradeLicense", maxLength:100, labelWidth:125})
		.addDateField({name:"tlValidFrom", bind:"{d.tlValidFrom}", dataIndex:"tlValidFrom", width:210})
		.addDateField({name:"tlValidTo", bind:"{d.tlValidTo}", dataIndex:"tlValidTo", width:135, labelWidth:25})
		.addTextField({ name:"aoc", bind:"{d.aoc}", dataIndex:"aoc", maxLength:100, labelWidth:125})
		.addDateField({name:"aocValidFrom", bind:"{d.aocValidFrom}", dataIndex:"aocValidFrom", width:210})
		.addDateField({name:"aocValidTo", bind:"{d.aocValidTo}", dataIndex:"aocValidTo", width:135, labelWidth:25})
		.addCombo({ xtype:"combo", name:"natureOfBusiness", bind:"{d.natureOfBusiness}", dataIndex:"natureOfBusiness", width:180, store:[ __FMBAS_TYPE__.NatureOfBusiness._AIRLINE_, __FMBAS_TYPE__.NatureOfBusiness._RESELLER_, __FMBAS_TYPE__.NatureOfBusiness._GENERAL_AVIATION_, __FMBAS_TYPE__.NatureOfBusiness._GOVERNMENT_, __FMBAS_TYPE__.NatureOfBusiness._MILITARY_, __FMBAS_TYPE__.NatureOfBusiness._OTHERS_], hideLabel:"true"})
		.addDisplayFieldText({ name:"separator", bind:"{d.separator}", dataIndex:"separator", maxLength:64, labelWidth:680, labelAlign:"center"})
		.addDisplayFieldText({ name:"separator1", bind:"{d.separator}", dataIndex:"separator", maxLength:64, labelWidth:680, labelAlign:"center"})
		.addLov({name:"buyerName", bind:"{d.buyerName}", dataIndex:"buyerName", xtype:"fmbas_UserListLov_Lov", maxLength:255, labelWidth:125,
			retFieldMapping: [{lovField:"userCode", dsField: "buyerCode"} ,{lovField:"id", dsField: "buyerId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addTextField({ name:"accountNumber", bind:"{d.accountNumber}", dataIndex:"accountNumber", width:180, maxLength:32, hideLabel:"true"})
		.addLov({name:"parentGroupName", bind:"{d.parentName}", dataIndex:"parentName", width:180, noLabel: true, xtype:"fmbas_CustomerLov_Lov", maxLength:100, style:"padding-bottom:41px",
			retFieldMapping: [{lovField:"id", dsField: "parentId"} ,{lovField:"name", dsField: "parentName"} ],
			filterFieldMapping: [{lovField:"type", value: "Group"} ]})
		.addDateField({name:"dateOfInc", bind:"{d.dateOfInc}", dataIndex:"dateOfInc", width:180, hideLabel:"true"})
		.addTextField({ name:"phoneNo", bind:"{d.phoneNo}", dataIndex:"phoneNo", width:180, maxLength:100, hideLabel:"true"})
		.addTextField({ name:"faxNo", bind:"{d.faxNo}", dataIndex:"faxNo", width:180, maxLength:100, hideLabel:"true", style:"padding-bottom:10px"})
		.addTextField({ name:"website", bind:"{d.website}", dataIndex:"website", width:180, maxLength:100, hideLabel:"true"})
		.addTextField({ name:"email", bind:"{d.email}", dataIndex:"email", width:180, maxLength:250, hideLabel:"true"})
		.addBooleanField({ name:"useForBilling", bind:"{d.useForBilling}", dataIndex:"useForBilling", noLabel: true})
		.addDisplayFieldText({ name:"useForBillingLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"codeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"nameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"accountNumberLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"typeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"parentGroupLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-bottom:10px"})
		.addDisplayFieldText({ name:"natureOfBusinessLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-left:50px"})
		.addDisplayFieldText({ name:"dateOfIncLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-left:50px"})
		.addDisplayFieldText({ name:"phoneNumberLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-left:50px"})
		.addDisplayFieldText({ name:"faxNumberLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-left:50px; padding-bottom:10px;"})
		.addDisplayFieldText({ name:"websiteLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-left:50px;"})
		.addDisplayFieldText({ name:"emailLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right; padding-left:50px;"})
		.addTextField({ name:"officeStreet", bind:"{d.officeStreet}", dataIndex:"officeStreet", width:180, noLabel: true, maxLength:50})
		.addTextField({ name:"officeCity", bind:"{d.officeCity}", dataIndex:"officeCity", width:180, noLabel: true, maxLength:100})
		.addLov({name:"officeStateName", bind:"{d.officeStateName}", dataIndex:"officeStateName", _enableFn_: function(dc, rec) { return !Ext.isEmpty(dc.record.data.officeCountryId); } , width:180, noLabel: true, xtype:"fmbas_CountryNameLov_Lov", maxLength:100,
			retFieldMapping: [{lovField:"id", dsField: "officeStateId"} ,{lovField:"name", dsField: "officeStateName"} ,{lovField:"code", dsField: "officeStateCode"} ],
			advFilter: [{"lovFieldName":"countryId","operation":"=","fieldName1":"officeCountryId"}]})
		.addLov({name:"officeCountryName", bind:"{d.officeCountryName}", dataIndex:"officeCountryName", width:180, noLabel: true, xtype:"fmbas_CountryNameLov_Lov", maxLength:100, _localCache_:true,
			retFieldMapping: [{lovField:"id", dsField: "officeCountryId"} ,{lovField:"name", dsField: "officeCountryName"} ,{lovField:"code", dsField: "officeCountryCode"} ],listeners:{
			fpchange:{scope:this, fn:this._clearOfficeStateValue_}
		}})
		.addTextField({ name:"officePostalCode", bind:"{d.officePostalCode}", dataIndex:"officePostalCode", width:180, noLabel: true, maxLength:32})
		.addTextField({ name:"billingStreet", bind:"{d.billingStreet}", dataIndex:"billingStreet", width:180, noLabel: true, maxLength:50})
		.addTextField({ name:"billingCity", bind:"{d.billingCity}", dataIndex:"billingCity", width:180, noLabel: true, maxLength:100})
		.addLov({name:"billingStateName", bind:"{d.billingStateName}", dataIndex:"billingStateName", _enableFn_: function(dc, rec) { return !Ext.isEmpty(dc.record.data.billingCountryId); } , width:180, noLabel: true, xtype:"fmbas_CountryNameLov_Lov", maxLength:100,
			retFieldMapping: [{lovField:"id", dsField: "billingStateId"} ,{lovField:"code", dsField: "billingStateCode"} ,{lovField:"name", dsField: "billingStateName"} ],
			advFilter: [{"lovFieldName":"countryId","operation":"=","fieldName1":"billingCountryId"}]})
		.addLov({name:"billingCountryName", bind:"{d.billingCountryName}", dataIndex:"billingCountryName", width:180, noLabel: true, xtype:"fmbas_CountryNameLov_Lov", maxLength:100, _localCache_:true,
			retFieldMapping: [{lovField:"id", dsField: "billingCountryId"} ,{lovField:"code", dsField: "billingCountryCode"} ,{lovField:"name", dsField: "billingCountryName"} ],listeners:{
			fpchange:{scope:this, fn:this._clearBillingStateValue_}
		}})
		.addTextField({ name:"billingPostalCode", bind:"{d.billingPostalCode}", dataIndex:"billingPostalCode", width:180, noLabel: true, maxLength:32})
		.addDisplayFieldText({ name:"officeAddressLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-top:10px; padding-bottom: 10px", colspan:2, listeners:{afterrender: {scope: this, fn: function(el) { this._setBorder_(el) }}}})
		.addDisplayFieldText({ name:"billingAddressLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-top:10px; padding-left:50px; padding-bottom: 10px", colspan:2, listeners:{afterrender: {scope: this, fn: function(el) { this._setBorder_(el) }}}})
		.addDisplayFieldText({ name:"streetLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"cityLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"stateProvinceLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"zipPostalCodeLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"countryLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"streetLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"cityLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"stateProvinceLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"zipPostalCodeLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"countryLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.add({name:"codeNumberGenerator", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("code"),this._getConfig_("generateCodeNumber")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  style:"padding:10px"})
		.addPanel({ name:"tableContainer1", layout: {type:"table"}})
		.addPanel({ name:"tableContainer2", layout: {type:"table", columns:2}})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3Title", layout: {type:"table"}})
		.addPanel({ name:"table4Title", layout: {type:"table"}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["tableContainer1"])
		.addChildrenTo("tableContainer1", ["table1", "table2"])
		.addChildrenTo("tableContainer2", ["table3Title", "table4Title"])
		.addChildrenTo("table1", ["codeLabel", "codeNumberGenerator", "nameLabel", "name", "accountNumberLabel", "accountNumber", "typeLabel", "type", "parentGroupLabel", "parentGroupName", "officeAddressLabel", "streetLabel1", "officeStreet", "cityLabel1", "officeCity", "countryLabel1", "officeCountryName", "zipPostalCodeLabel1", "officePostalCode", "stateProvinceLabel1", "officeStateName", "useForBillingLabel", "useForBilling"])
		.addChildrenTo("table2", ["natureOfBusinessLabel", "natureOfBusiness", "dateOfIncLabel", "dateOfInc", "emailLabel", "email", "websiteLabel", "website", "phoneNumberLabel", "phoneNo", "faxNumberLabel", "faxNo", "billingAddressLabel", "streetLabel2", "billingStreet", "cityLabel2", "billingCity", "countryLabel2", "billingCountryName", "zipPostalCodeLabel2", "billingPostalCode", "stateProvinceLabel2", "billingStateName"])
		.addChildrenTo("table3Title", ["officeAddressLabel"])
		.addChildrenTo("table4Title", ["billingAddressLabel"]);
	},
	/* ==================== Business functions ==================== */
	
	_clearOfficeStateValue_: function() {
		
						var f = function(){ 
							var dc = this._controller_;
							var r = dc.getRecord();
							var officeCountryName = this._get_("officeCountryName").getValue();
							var officeStateName = this._get_("officeStateName");
			
							if (Ext.isEmpty(officeCountryName)) {
								officeStateName.setReadOnly(true);
								if (r) {
									r.set("officeStateId","");
									r.set("officeStateCode","");
									r.set("officeStateName","");
								}
							}
							else {
								officeStateName.setReadOnly(false);
								if (r && r.dirty == true) {
									if ("officeCountryId" in r.modified) {
										r.set("officeStateId","");
										r.set("officeStateCode","");
										r.set("officeStateName","");
									}
								}
							}
						};
						Ext.defer(f, 100, this);
	},
	
	_clearBillingStateValue_: function() {
		
		
						var f = function(){ 
							var dc = this._controller_;
							var r = dc.getRecord();
							var billingCountryName = this._get_("billingCountryName").getValue();
							var billingStateName = this._get_("billingStateName");
			
							if (Ext.isEmpty(billingCountryName)) {
								billingStateName.setReadOnly(true);
								if (r) {
									r.set("billingStateId","");
									r.set("billingStateCode","");
									r.set("billingStateName","");
								}
							}
							else {
								billingStateName.setReadOnly(false);
								if (r && r.dirty == true) {
									if ("billingCountryId" in r.modified) {
										r.set("billingStateId","");
										r.set("billingStateCode","");
										r.set("billingStateName","");
									}
								}
							}
						}
						Ext.defer(f, 100, this);
	},
	
	_generateCode_: function() {
		
		
						var dc = this._controller_;
						var o={
							name:"generateCustomerCode",
							modal:true
						};
						dc.doRpcData(o);
	},
	
	_setBorder_: function(el) {
		
						var td = el.getEl().dom.parentNode; 
						td.style.borderTop = "1px solid #d0d0d0";
	},
	
	_setParentVisibility_: function(combo,newVal,oldVal) {
		
						var parent = this._get_("parentGroupName");
						var parentLabel = this._get_("parentGroupLabel");
						var dc = this._controller_;
						var r = dc.getRecord();
		
						if (newVal=="Group Member") {
							parent.setReadOnly(false);
							
						} else {
							parent.setReadOnly(true);
							if (r) {
								r.set("parentName", null);
								r.set("parentId", null);
							}
						}
	},
	
	_endDefine_: function() {
		
						this._shouldDisableWhenDcIsReadOnly_ = false;
	}
});

/* ================= EDIT FORM: Addresses ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Customers_Dc$Addresses", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Customers_Dc$Addresses",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"streetLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"cityLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"stateProvinceLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"zipPostalCodeLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"countryLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"streetLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"cityLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"stateProvinceLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"zipPostalCodeLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"countryLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addTextField({ name:"officeStreet", bind:"{d.officeStreet}", dataIndex:"officeStreet", width:180, noLabel: true, maxLength:50})
		.addTextField({ name:"officeCity", bind:"{d.officeCity}", dataIndex:"officeCity", width:180, noLabel: true, maxLength:100})
		.addLov({name:"officeStateName", bind:"{d.officeStateName}", dataIndex:"officeStateName", _enableFn_: function(dc, rec) { return !Ext.isEmpty(dc.record.data.officeCountryId); } , width:180, noLabel: true, xtype:"fmbas_CountryNameLov_Lov", maxLength:100,
			retFieldMapping: [{lovField:"id", dsField: "officeStateId"} ,{lovField:"name", dsField: "officeStateName"} ,{lovField:"code", dsField: "officeStateCode"} ],
			advFilter: [{"lovFieldName":"countryId","operation":"=","fieldName1":"officeCountryId"}]})
		.addLov({name:"officeCountryName", bind:"{d.officeCountryName}", dataIndex:"officeCountryName", width:180, noLabel: true, xtype:"fmbas_CountryNameLov_Lov", maxLength:100, _localCache_:true,
			retFieldMapping: [{lovField:"id", dsField: "officeCountryId"} ,{lovField:"code", dsField: "officeCountryCode"} ,{lovField:"name", dsField: "officeCountryName"} ],listeners:{
			fpchange:{scope:this, fn:this._clearOfficeStateValue_}
		}})
		.addTextField({ name:"officePostalCode", bind:"{d.officePostalCode}", dataIndex:"officePostalCode", width:180, noLabel: true, maxLength:32})
		.addTextField({ name:"billingStreet", bind:"{d.billingStreet}", dataIndex:"billingStreet", width:180, noLabel: true, maxLength:50})
		.addTextField({ name:"billingCity", bind:"{d.billingCity}", dataIndex:"billingCity", width:180, noLabel: true, maxLength:100})
		.addLov({name:"billingStateName", bind:"{d.billingStateName}", dataIndex:"billingStateName", _enableFn_: function(dc, rec) { return !Ext.isEmpty(dc.record.data.billingCountryId); } , width:180, noLabel: true, xtype:"fmbas_CountryNameLov_Lov", maxLength:100,
			retFieldMapping: [{lovField:"id", dsField: "billingStateId"} ,{lovField:"code", dsField: "billingStateCode"} ,{lovField:"name", dsField: "billingStateName"} ],
			advFilter: [{"lovFieldName":"countryId","operation":"=","fieldName1":"billingCountryId"}]})
		.addLov({name:"billingCountryName", bind:"{d.billingCountryName}", dataIndex:"billingCountryName", width:180, noLabel: true, xtype:"fmbas_CountryNameLov_Lov", maxLength:100, _localCache_:true,
			retFieldMapping: [{lovField:"id", dsField: "billingCountryId"} ,{lovField:"code", dsField: "billingCountryCode"} ,{lovField:"name", dsField: "billingCountryName"} ],listeners:{
			fpchange:{scope:this, fn:this._clearBillingStateValue_}
		}})
		.addTextField({ name:"billingPostalCode", bind:"{d.billingPostalCode}", dataIndex:"billingPostalCode", width:180, noLabel: true, maxLength:32})
		.addDisplayFieldText({ name:"officeAddressLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-top:10px; padding-bottom: 10px", colspan:2})
		.addDisplayFieldText({ name:"billingAddressLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-top:10px; padding-bottom: 10px", colspan:2})
		.addBooleanField({ name:"useForBilling", bind:"{d.useForBilling}", dataIndex:"useForBilling", noLabel: true})
		.addDisplayFieldText({ name:"useForBillingLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		/* =========== containers =========== */
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left:50px", layout: {type:"table", columns:2}})
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"tableContainer1", layout: {type:"table"}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("table1", ["officeAddressLabel", "streetLabel1", "officeStreet", "cityLabel1", "officeCity", "countryLabel1", "officeCountryName", "zipPostalCodeLabel1", "officePostalCode", "stateProvinceLabel1", "officeStateName", "useForBillingLabel", "useForBilling"])
		.addChildrenTo("table2", ["billingAddressLabel", "streetLabel2", "billingStreet", "cityLabel2", "billingCity", "countryLabel2", "billingCountryName", "zipPostalCodeLabel2", "billingPostalCode", "stateProvinceLabel2", "billingStateName"])
		.addChildrenTo("main", ["tableContainer1"])
		.addChildrenTo("tableContainer1", ["table1", "table2"]);
	},
	/* ==================== Business functions ==================== */
	
	_clearOfficeStateValue_: function() {
		
						var f = function(){ 
							var dc = this._controller_;
							var r = dc.getRecord();
							var officeCountryName = this._get_("officeCountryName").getValue();
							var officeStateName = this._get_("officeStateName");
			
							if (Ext.isEmpty(officeCountryName)) {
								officeStateName.setReadOnly(true);
								if (r) {
									r.set("officeStateId","");
									r.set("officeStateCode","");
									r.set("officeStateName","");
								}
							}
							else {
								officeStateName.setReadOnly(false);
								if (r && r.dirty == true) {
									if ("officeCountryId" in r.modified) {
										r.set("officeStateId","");
										r.set("officeStateCode","");
										r.set("officeStateName","");
									}
								}
							}
						};
						Ext.defer(f, 100, this);
	},
	
	_clearBillingStateValue_: function() {
		
		
						var f = function(){ 
							var dc = this._controller_;
							var r = dc.getRecord();
							var billingCountryName = this._get_("billingCountryName").getValue();
							var billingStateName = this._get_("billingStateName");
			
							if (Ext.isEmpty(billingCountryName)) {
								billingStateName.setReadOnly(true);
								if (r) {
									r.set("billingStateId","");
									r.set("billingStateCode","");
									r.set("billingStateName","");
								}
							}
							else {
								billingStateName.setReadOnly(false);
								if (r && r.dirty == true) {
									if ("billingCountryId" in r.modified) {
										r.set("billingStateId","");
										r.set("billingStateCode","");
										r.set("billingStateName","");
									}
								}
							}
						}
						Ext.defer(f, 100, this);
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Customers_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Customers_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", allowBlank:false, maxLength:32, cls:"sone-flat-kpi"})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", maxLength:100, cls:"sone-flat-field"})
		.addCombo({ xtype:"combo", name:"type", bind:"{d.type}", dataIndex:"type", allowBlank:false, width:140, noLabel: true, store:[ __FMBAS_TYPE__.CustomerType._INDEPENDENT_, __FMBAS_TYPE__.CustomerType._GROUP_, __FMBAS_TYPE__.CustomerType._GROUP_MEMBER_],listeners:{
			fpchange:{scope:this, fn:this._setParentVisibility_}
		}})
		.addDisplayFieldText({ name:"status", bind:"{d.status}", dataIndex:"status", noEdit:true , maxLength:32})
		.addLov({name:"parentGroupName", bind:"{d.parentName}", dataIndex:"parentName", _enableFn_: function(dc, rec) { return dc.record.data.type=='Group Member'; } , width:140, noLabel: true, xtype:"fmbas_CustomerLov_Lov", maxLength:100,
			retFieldMapping: [{lovField:"id", dsField: "parentId"} ,{lovField:"name", dsField: "parentName"} ],
			filterFieldMapping: [{lovField:"type", value: "Group"} ]})
		.addTextField({ name:"tradeLicense", bind:"{d.tradeLicense}", dataIndex:"tradeLicense", width:140, noLabel: true, maxLength:100})
		.addDateField({name:"tlValidFrom", bind:"{d.tlValidFrom}", dataIndex:"tlValidFrom", width:120, noLabel: true})
		.addDateField({name:"tlValidTo", bind:"{d.tlValidTo}", dataIndex:"tlValidTo", width:120, noLabel: true})
		.addDisplayFieldNumber({ name:"creditLimit", bind:"{d.creditLimit}", dataIndex:"creditLimit", sysDec:"dec_amount", maxLength:19, style:"white-space:nowrap" })
		.addDisplayFieldNumber({ name:"creditUtil", bind:"{d.creditUtil}", dataIndex:"creditUtil", sysDec:"dec_prc", maxLength:19 })
		.addTextField({ name:"aoc", bind:"{d.aoc}", dataIndex:"aoc", width:140, noLabel: true, maxLength:100})
		.addDateField({name:"aocValidFrom", bind:"{d.aocValidFrom}", dataIndex:"aocValidFrom", width:120, noLabel: true})
		.addDateField({name:"aocValidTo", bind:"{d.aocValidTo}", dataIndex:"aocValidTo", width:120, noLabel: true})
		.addDateField({name:"dateOfInc", bind:"{d.dateOfInc}", dataIndex:"dateOfInc", cls:"sone-flat-kpi", hideTrigger:true, listeners:{render: {scope: this, fn: function(el) {this._showPickerOnClick_(el)}}}})
		.addCombo({ xtype:"combo", name:"natureOfBusiness", bind:"{d.natureOfBusiness}", dataIndex:"natureOfBusiness", width:140, noLabel: true, store:[ __FMBAS_TYPE__.NatureOfBusiness._AIRLINE_, __FMBAS_TYPE__.NatureOfBusiness._RESELLER_, __FMBAS_TYPE__.NatureOfBusiness._GENERAL_AVIATION_, __FMBAS_TYPE__.NatureOfBusiness._GOVERNMENT_, __FMBAS_TYPE__.NatureOfBusiness._MILITARY_, __FMBAS_TYPE__.NatureOfBusiness._OTHERS_], listeners:{change: {scope: this, fn: function(el,newVal,oldVal) {this._enableDisableFields_(el,newVal,oldVal)}}}})
		.addDisplayFieldText({ name:"separator", bind:"{d.separator}", dataIndex:"separator", maxLength:64, labelWidth:1440, labelAlign:"center"})
		.addLov({name:"buyerName", bind:"{d.buyerName}", dataIndex:"buyerName", width:140, noLabel: true, xtype:"fmbas_UserListLov_Lov", maxLength:255,
			retFieldMapping: [{lovField:"userCode", dsField: "buyerCode"} ,{lovField:"id", dsField: "buyerId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addDisplayFieldText({ name:"dummyField", bind:"{d.dummyField}", dataIndex:"dummyField", maxLength:32, style:"display:none"})
		.addTextField({ name:"accountNumber", bind:"{d.accountNumber}", dataIndex:"accountNumber", width:120, noLabel: true, maxLength:32})
		.addTextField({ name:"phoneNo", bind:"{d.phoneNo}", dataIndex:"phoneNo", width:140, maxLength:100, hideLabel:"true"})
		.addTextField({ name:"faxNo", bind:"{d.faxNo}", dataIndex:"faxNo", width:140, maxLength:100, hideLabel:"true"})
		.addTextField({ name:"website", bind:"{d.website}", dataIndex:"website", maxLength:100, hideLabel:"true"})
		.addTextField({ name:"email", bind:"{d.email}", dataIndex:"email", maxLength:250, hideLabel:"true"})
		.addTextField({ name:"iataCode", bind:"{d.iataCode}", dataIndex:"iataCode", _enableFn_: function(dc, rec) { return dc.record.data.natureOfBusiness == 'Airline'; } , width:60, hideLabel:"true", maxLength:4, enforceMaxLength:true})
		.addTextField({ name:"icaoCode", bind:"{d.icaoCode}", dataIndex:"icaoCode", _enableFn_: function(dc, rec) { return dc.record.data.natureOfBusiness == 'Airline'; } , width:60, hideLabel:"true", maxLength:3, enforceMaxLength:true})
		.addTextField({ name:"vatRegNumber", bind:"{d.vatRegNumber}", dataIndex:"vatRegNumber", width:140, noLabel: true, maxLength:32})
		.addTextField({ name:"taxIdNumber", bind:"{d.taxIdNumber}", dataIndex:"taxIdNumber", width:140, noLabel: true, maxLength:32})
		.addTextField({ name:"incomeTaxNumber", bind:"{d.incomeTaxNumber}", dataIndex:"incomeTaxNumber", width:140, noLabel: true, maxLength:32})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("dummyField"),this._getConfig_("name")]})
		.addDisplayFieldText({ name:"natureOfBusinessLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"buyerNameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"typeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"parentLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"accountNumberLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"phoneNumberLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"faxNumberLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"websiteLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"iataCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"icaoCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"emailLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"tradeLicenseLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"aocLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"validFromLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"validFromLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"toLabel1", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-left:10px"})
		.addDisplayFieldText({ name:"toLabel2", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"padding-left:10px"})
		.addDisplayFieldText({ name:"vatRegistrationLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"taxIdentification", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"incomeTaxLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"creditLimitAndCurrency", bind:"{d.creditLimitAndCurrency}", dataIndex:"creditLimitAndCurrency", sysDec:"dec_amount", maxLength:64})
		.addDisplayFieldText({ name:"creditUtilAndPercent", bind:"{d.creditUtilAndPercent}", dataIndex:"creditUtilAndPercent", maxLength:64, listeners:{change : {scope: this, fn: function(el, newVal, oldVal) {this._markUtilization_(el, newVal, oldVal)}}}})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"titleAndKpi",  style:"margin-bottom:10px", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"p3",  cls:"sone-kpi-panel", layout: {type:"table"}})
		.addPanel({ name:"mainTable1",  style:"margin-bottom:10px", layout: {type:"table"}})
		.addPanel({ name:"mainTable2",  style:"margin-bottom:10px", layout: {type:"table"}})
		.addPanel({ name:"c1", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c2", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c3", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c4", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c5", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"title", width:300,  cls:"sone-title-panel", layout:"anchor"})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left:30px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3",  style:"margin-left:30px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table4",  style:"margin-left:30px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table5",  style:"margin-left:30px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table6",  style:"margin-left:30px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table7",  style:"margin-left:30px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table8",  style:"margin-left:30px", layout: {type:"table", columns:4}})
		.addPanel({ name:"table9",  style:"margin-left:30px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table10",  style:"margin-left:30px", layout: {type:"table", columns:2}})
		.addPanel({ name:"panelSeparator", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["titleAndKpi", "mainTable1", "panelSeparator", "mainTable2"])
		.addChildrenTo("titleAndKpi", ["title", "p3"])
		.addChildrenTo("p3", ["c1", "c2", "c3", "c4", "c5"])
		.addChildrenTo("mainTable1", ["table1", "table2", "table3", "table4", "table5", "table6"])
		.addChildrenTo("mainTable2", ["table7", "table8", "table9", "table10"])
		.addChildrenTo("c1", ["code"])
		.addChildrenTo("c2", ["creditLimitAndCurrency"])
		.addChildrenTo("c3", ["creditUtilAndPercent"])
		.addChildrenTo("c4", ["dateOfInc"])
		.addChildrenTo("c5", ["status"])
		.addChildrenTo("title", ["row1"])
		.addChildrenTo("table1", ["natureOfBusinessLabel", "natureOfBusiness", "buyerNameLabel", "buyerName"])
		.addChildrenTo("table2", ["typeLabel", "type", "parentLabel", "parentGroupName"])
		.addChildrenTo("table3", ["accountNumberLabel", "accountNumber"])
		.addChildrenTo("table4", ["iataCodeLabel", "iataCode", "icaoCodeLabel", "icaoCode"])
		.addChildrenTo("table5", ["emailLabel", "email", "websiteLabel", "website"])
		.addChildrenTo("table6", ["phoneNumberLabel", "phoneNo", "faxNumberLabel", "faxNo"])
		.addChildrenTo("table7", ["tradeLicenseLabel", "tradeLicense", "aocLabel", "aoc"])
		.addChildrenTo("table8", ["validFromLabel1", "tlValidFrom", "toLabel1", "tlValidTo", "validFromLabel2", "aocValidFrom", "toLabel2", "aocValidTo"])
		.addChildrenTo("table9", ["vatRegistrationLabel", "vatRegNumber", "taxIdentification", "taxIdNumber"])
		.addChildrenTo("table10", ["incomeTaxLabel", "incomeTaxNumber"])
		.addChildrenTo("panelSeparator", ["separator"]);
	},
	/* ==================== Business functions ==================== */
	
	_hideCreditLimitIfEmpty_: function() {
		
		
						var dc = this._controller_;
						var r = dc.getRecord();
						
						if (r) {
							var creditLimitAndCurrency = r.get("creditLimitAndCurrency");
							var kpi = this._get_("c2");
							if (Ext.isEmpty(creditLimitAndCurrency)) {
								kpi.hide();
							}
							else {
								kpi.show();
							}
						}
		
	},
	
	_markUtilization_: function(el,newVal,oldVal) {
		
						
						var dc = this._controller_;
						var r = dc.getRecord();
						
						var redStyles = "#F94F52"
						var yellowStyles = "#F9B344";
						var greenStyles = "#52AA39";
		
						if (r) {
							var alertLimit = r.get("alertLimit");
							var currentVal = parseInt(newVal.split(" ")[0]);
		
							if (!Ext.isEmpty(newVal)) {
								if(currentVal <= alertLimit) {
						        	document.getElementById(el.inputId).style.color = greenStyles;
							    } else if (currentVal > alertLimit && currentVal <= 100) {
							        document.getElementById(el.inputId).style.color = yellowStyles;
							    } else if (currentVal > alertLimit && currentVal >= 100) {
									document.getElementById(el.inputId).style.color = redStyles;
							    }
							}
						}
		
	},
	
	_showPickerOnClick_: function(el) {
		
		
						el.inputCell.on("click",function(){  
			                el.onTriggerClick();
			            });
	},
	
	_afterDefineElements_: function() {
		
		
						this._getBuilder_().change("titleAndKpi",{
							layout: {
						        type: "hbox",
						        align: "stretch"
						    },
						    defaults: {
						        flex: 1,
								height: 60
						    }
						});
						
						this._controller_.on("afterDoServiceSuccess", function() {					
							this._applyStates_(this._controller_.getRecord());
						},this);
		
	},
	
	enableFinalCust: function(el,newVal,oldVal) {
		
						var dc = this._controller_;
						var frame = dc.getFrame();
						var view = frame._get_("finalCustList");
						var items = view.dockedItems.items;
						var toolbar = "";
						var dockedItem = items[i];
						for (var i = 0; i < items.length; i++) {
							if (items[i].dock == "top" && items[i].xtype == "toolbar") {
								toolbars = items[i];
								if (newVal != "Reseller") {
									toolbars.setDisabled(true);
								}
								else {
									toolbars.setDisabled(false);
								}
							}
						}
						
						
	},
	
	_enableDisableFields_: function(el,newVal,oldVal) {
		
						this.enableFinalCust(el, newVal, oldVal);
						var natureOfBusiness = this._get_("natureOfBusiness");
						var icaoCode = this._get_("icaoCode");
						var iataCode = this._get_("iataCode");
						var newVal = natureOfBusiness.getValue();
		
						if (newVal == "Airline") {
							icaoCode.setReadOnly(false);
							iataCode.setReadOnly(false);
						}
						else {
							icaoCode.setReadOnly(true);
							iataCode.setReadOnly(true);
							icaoCode.setValue("");
							iataCode.setValue("");
						}
	},
	
	_setParentVisibility_: function(combo,newVal,oldVal) {
		
						var parent = this._get_("parentGroupName");
						var dc = this._controller_;
						var r = dc.getRecord();
		
						if (newVal=="Group Member") {
							parent.setReadOnly(false);
							
						} else {
							parent.setReadOnly(true);
							if (r) {
								r.set("parentName", null);
								r.set("parentId", null);
							}
						}
	},
	
	_showAlert_: function() {
		
						var type = this.getRecord().get("type");
						
						if (type == "Group") {
							Main.error("If there are customers belonging to this group, the status for these customers will be automatically set to independent status!");
						}
	},
	
	_afterApplyStates_: function() {
		
						var field = this._get_("name");
						var inputElId = field.inputEl.dom.id;
						var originalTitleLabel = field.fieldLabel;
						var inputWidth = 700;
		
						field.labelEl.update("<i class='fa fa-user'></i> "+originalTitleLabel);
						document.getElementById(inputElId).style.width = inputWidth+"px";
						
						this._hideCreditLimitIfEmpty_();
						
	}
});

/* ================= EDIT FORM: EditRemark ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Customers_Dc$EditRemark", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Customers_Dc$EditRemark",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.remark}", paramIndex:"remark", width:400, labelAlign:"top", fieldStyle:"margin-top: 5px"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:400, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
						remarks.setReadOnly(false);
						//remarks.allowBlank=true;		
	}
});
