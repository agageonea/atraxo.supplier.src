/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.Country_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Country_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("country", Ext.create(atraxo.fmbas.ui.extjs.dc.Country_Dc,{multiEdit: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"btnCancelWithMenu",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"country", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", handler: this.onBtnCancelSelected, scope:this})
		.addButton({name:"btnCancelAll",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", _isDefaultHandler_:true, handler: this.onBtnCancelAll, scope:this})
		.addDcFilterFormView("country", {name:"cuFilter", xtype:"fmbas_Country_Dc$Filter"})
		.addDcEditGridView("country", {name:"cuList", xtype:"fmbas_Country_Dc$List", frame:true})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["cuList"], ["center"])
		.addToolbarTo("cuList", "tlbCmpList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbCmpList", {dc: "country"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelWithMenu"),this._elems_.get("btnCancelSelected"),this._elems_.get("btnCancelAll"),this._elems_.get("helpWdw")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnCancelSelected
	 */
	,onBtnCancelSelected: function() {
		this.fnCancelSelected();
	}
	
	/**
	 * On-Click handler for button btnCancelAll
	 */
	,onBtnCancelAll: function() {
		this.fnCancelAll();
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/Countries.html";
					window.open( url, "SONE_Help");
	}
	
	,fnCancelSelected: function() {
		
						var dc = this._getDc_("country");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAll: function() {
		
						var dc = this._getDc_("country");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isCancelEnabled(dc);
	}
});
