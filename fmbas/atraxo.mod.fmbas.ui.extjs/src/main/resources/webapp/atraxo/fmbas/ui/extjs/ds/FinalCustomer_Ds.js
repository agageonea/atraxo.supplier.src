/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.FinalCustomer_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_FinalCustomer_Ds"
	},
	
	
	validators: {
		code: [{type: 'presence'}],
		name: [{type: 'presence'}]
	},
	
	fields: [
		{name:"isThirdParty", type:"boolean"},
		{name:"name", type:"string"},
		{name:"code", type:"string"},
		{name:"iataCode", type:"string"},
		{name:"icaoCode", type:"string"},
		{name:"natureOfBusiness", type:"string"},
		{name:"status", type:"string"},
		{name:"type", type:"string"},
		{name:"transmissionStatus", type:"string"},
		{name:"processedStatus", type:"string"},
		{name:"creditUpdateRequestStatus", type:"string"},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.FinalCustomer_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"isThirdParty", type:"boolean", allowNull:true},
		{name:"name", type:"string"},
		{name:"code", type:"string"},
		{name:"iataCode", type:"string"},
		{name:"icaoCode", type:"string"},
		{name:"natureOfBusiness", type:"string"},
		{name:"status", type:"string"},
		{name:"type", type:"string"},
		{name:"transmissionStatus", type:"string"},
		{name:"processedStatus", type:"string"},
		{name:"creditUpdateRequestStatus", type:"string"},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.fmbas.ui.extjs.ds.FinalCustomer_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"customerId", type:"int", forFilter:true, allowNull:true}
	]
});
