/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ExternalInterfaceMessageHistory_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_ExternalInterfaceMessageHistory_Ds"
	},
	
	
	fields: [
		{name:"externalInterfaceId", type:"int", allowNull:true},
		{name:"externalInterfaceName", type:"string"},
		{name:"objectId", type:"int", allowNull:true},
		{name:"objectType", type:"string"},
		{name:"objectRefId", type:"string"},
		{name:"messageId", type:"string"},
		{name:"responseMessageId", type:"string"},
		{name:"executionTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"responseMessageTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"status", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ExternalInterfaceMessageHistory_DsFilter", {
	extend: 'Ext.data.Model',
	
	getFilterAttributes : function(){
		return ["executionTime_cutTime","responseMessageTime_cutTime"];
	},
	
	fields: [
		{name:"externalInterfaceId", type:"int", allowNull:true},
		{name:"externalInterfaceName", type:"string"},
		{name:"objectId", type:"int", allowNull:true},
		{name:"objectType", type:"string"},
		{name:"objectRefId", type:"string"},
		{name:"messageId", type:"string"},
		{name:"responseMessageId", type:"string"},
		{name:"executionTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"responseMessageTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"status", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.fmbas.ui.extjs.ds.ExternalInterfaceMessageHistory_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"messageContent", type:"string"}
	]
});
