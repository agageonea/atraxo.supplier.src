/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.Suppliers_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Suppliers_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("supplier", Ext.create(atraxo.fmbas.ui.extjs.dc.Suppliers_Dc,{trackEditMode: true}))
		.addDc("attachment", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("contact", Ext.create(atraxo.fmbas.ui.extjs.dc.Contacts_Dc,{}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.addDc("creditLine", Ext.create(atraxo.fmbas.ui.extjs.dc.SupplierCreditLine_Dc,{}))
		.addDc("creditLineHistory", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.linkDc("attachment", "supplier",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}, {childField:"targetType", value:"N/A"}]})
				.linkDc("contact", "supplier",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("note", "supplier",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("creditLine", "supplier",{fetchMode:"auto",fields:[
					{childField:"companyId", parentField:"id"}]})
				.linkDc("creditLineHistory", "creditLine",{fetchMode:"auto",reloadChildrenOnParentUpdate:true,fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnOnEditWdwClose",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnOnEditWdwClose, scope:this})
		.addButton({name:"btnCloseContactWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseContactWdw, scope:this})
		.addButton({name:"btnCloseNoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseNoteWdw, scope:this})
		.addButton({name:"btnCloseCreditWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseCreditWdw, scope:this})
		.addButton({name:"btnSaveCloseWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseWdw, scope:this})
		.addButton({name:"btnSaveCloseContactWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseContactWdw, scope:this})
		.addButton({name:"btnSaveCloseNoteWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseNoteWdw, scope:this})
		.addButton({name:"btnSaveCloseCreditWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseCreditWdw, scope:this})
		.addButton({name:"btnSaveCreditLine",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSaveCreditLine,stateManager:[{ name:"selected_not_zero", dc:"creditLine", and: function(dc) {return (this.fnSaveEnabled(dc));} }], scope:this})
		.addButton({name:"btnSaveCloseHistoryWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseHistoryWdw, scope:this})
		.addButton({name:"btnSaveNewWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewWdw, scope:this})
		.addButton({name:"btnSaveNewCreditWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewCreditWdw, scope:this})
		.addButton({name:"btnSaveNewContactWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewContactWdw, scope:this})
		.addButton({name:"btnSaveEditWdw",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnSaveEditWdw, scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdw1",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw1, scope:this})
		.addDcFilterFormView("supplier", {name:"supplierFilter", xtype:"fmbas_Suppliers_Dc$Filter"})
		.addDcGridView("supplier", {name:"supplierList", xtype:"fmbas_Suppliers_Dc$List"})
		.addDcFormView("supplier", {name:"supplierEdit", xtype:"fmbas_Suppliers_Dc$Edit"})
		.addDcFormView("supplier", {name:"supplierEditContext", xtype:"fmbas_Suppliers_Dc$Edit"})
		.addDcFilterFormView("contact", {name:"contactFilter", xtype:"fmbas_Contacts_Dc$Filter"})
		.addDcGridView("contact", {name:"contactsList", _hasTitle_:true, xtype:"fmbas_Contacts_Dc$List"})
		.addDcFormView("contact", {name:"contactEdit", xtype:"fmbas_Contacts_Dc$Edit"})
		.addDcFormView("contact", {name:"contactEditContext", xtype:"fmbas_Contacts_Dc$Edit"})
		.addDcGridView("note", {name:"notesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcFormView("note", {name:"notesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addDcGridView("supplier", {name:"activitiesList", _hasTitle_:true, xtype:"fmbas_Suppliers_Dc$Temp",  disabled:true})
		.addDcFilterFormView("creditLine", {name:"creditLineFilter", xtype:"fmbas_SupplierCreditLine_Dc$Filter"})
		.addDcGridView("creditLine", {name:"creditLineList", _hasTitle_:true, xtype:"fmbas_SupplierCreditLine_Dc$List"})
		.addDcFormView("creditLine", {name:"creditLineEdit", xtype:"fmbas_SupplierCreditLine_Dc$Edit"})
		.addDcFormView("creditLine", {name:"creditLineEditContext", xtype:"fmbas_SupplierCreditLine_Dc$Edit"})
		.addDcGridView("creditLineHistory", {name:"creditLineHistoryList", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("creditLine", {name:"creditLineHistoryEdit", xtype:"fmbas_SupplierCreditLine_Dc$CreditHistory"})
		.addDcGridView("supplier", {name:"invoicesList", _hasTitle_:true, xtype:"fmbas_Suppliers_Dc$Temp",  disabled:true})
		.addDcGridView("supplier", {name:"complaintsList", _hasTitle_:true, xtype:"fmbas_Suppliers_Dc$Temp",  disabled:true})
		.addDcGridView("attachment", {name:"attachmentList", _hasTitle_:true, xtype:"ad_Attachment_Dc$List"})
		.addDcFormView("attachment", {name:"attachmentEdit", xtype:"ad_Attachment_Dc$New"})
		.addWindow({name:"wdwSupplier", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("supplierEdit")],  listeners:{ close:{fn:this.onWdwSupplierClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewWdw"), this._elems_.get("btnSaveCloseWdw"), this._elems_.get("btnSaveEditWdw"), this._elems_.get("btnOnEditWdwClose")]}]})
		.addWindow({name:"contactWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("contactEdit")],  listeners:{ close:{fn:this.onContactWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewContactWdw"), this._elems_.get("btnSaveCloseContactWdw"), this._elems_.get("btnCloseContactWdw")]}]})
		.addWindow({name:"creditLineWdw", _hasTitle_:true, width:380, height:300, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("creditLineEdit")],  listeners:{ close:{fn:this.onCreditLineWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewCreditWdw"), this._elems_.get("btnSaveCloseCreditWdw"), this._elems_.get("btnCloseCreditWdw")]}]})
		.addWindow({name:"noteWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("notesEdit")],  listeners:{ close:{fn:this.onNoteWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseNoteWdw"), this._elems_.get("btnCloseNoteWdw")]}]})
		.addWindow({name:"creditLineHistoryWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("creditLineHistoryEdit")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseHistoryWdw")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas4", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas2", dcName:"supplier"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["supplierList"], ["center"])
		.addChildrenTo("canvas2", ["supplierEditContext", "detailsTab"], ["north", "center"])
		.addChildrenTo("canvas3", ["contactEditContext"], ["center"])
		.addChildrenTo("canvas4", ["creditLineEditContext", "creditLineHistoryList"], ["north", "center"])
		.addChildrenTo("detailsTab", ["contactsList"])
		.addToolbarTo("supplierList", "tlbSupplierList")
		.addToolbarTo("supplierEditContext", "tlbSupplierContext")
		.addToolbarTo("contactsList", "tlbContacts")
		.addToolbarTo("contactEditContext", "tlbContactContext")
		.addToolbarTo("notesList", "tlbNoteList")
		.addToolbarTo("creditLineList", "tlbCreditLineList")
		.addToolbarTo("creditLineEditContext", "tlbCreditLineEditContext")
		.addToolbarTo("attachmentList", "tlbAttachments");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2", "canvas3", "canvas4"])
		.addChildrenTo2("detailsTab", ["creditLineList", "notesList", "attachmentList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbSupplierList", {dc: "supplier"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbSupplierContext", {dc: "supplier"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw1")])
			.addReports()
		.end()
		.beginToolbar("tlbContacts", {dc: "contact"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas3"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbContactContext", {dc: "contact"})
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas2"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbNoteList", {dc: "note"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbCreditLineList", {dc: "creditLine"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas4"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbCreditLineEditContext", {dc: "creditLine"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas2"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnSaveCreditLine")])
			.addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addPrevRec({glyph:fp_asc.prev_glyph.glyph})
			.addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbAttachments", {dc: "attachment"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end();
	}
	
	,_applyTabLoadRestriction_: function(){
		this._getBuilder_()
		.addTabLoadRestriction("contact","detailsTab",["contactsList"])
		.addTabLoadRestriction("note","detailsTab",["notesList"])
		.addTabLoadRestriction("creditLine","detailsTab",["creditLineList"])
		.addTabLoadRestriction("attachment","detailsTab",["attachmentList"])
		;
	}

	
	/**
	 * On-Click handler for button btnOnEditWdwClose
	 */
	,onBtnOnEditWdwClose: function() {
		this.onWdwSupplierClose();
		this._getWindow_("wdwSupplier").close();
	}
	
	/**
	 * On-Click handler for button btnCloseContactWdw
	 */
	,onBtnCloseContactWdw: function() {
		this.onContactWdwClose();
		this._getWindow_("contactWdw").close();
	}
	
	/**
	 * On-Click handler for button btnCloseNoteWdw
	 */
	,onBtnCloseNoteWdw: function() {
		this._getDc_("note").doCancel();
		this._getWindow_("noteWdw").close();
	}
	
	/**
	 * On-Click handler for button btnCloseCreditWdw
	 */
	,onBtnCloseCreditWdw: function() {
		this.onCreditLineWdwClose();
		this._getWindow_("creditLineWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseWdw
	 */
	,onBtnSaveCloseWdw: function() {
		this.saveClose();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseContactWdw
	 */
	,onBtnSaveCloseContactWdw: function() {
		this.saveCloseContact();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseNoteWdw
	 */
	,onBtnSaveCloseNoteWdw: function() {
		this.saveCloseNote();
		this._getDc_("note").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseCreditWdw
	 */
	,onBtnSaveCloseCreditWdw: function() {
		this.saveCloseCredit();
		this._getDc_("creditLine").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSaveCreditLine
	 */
	,onBtnSaveCreditLine: function() {
		this._getWindow_("creditLineHistoryWdw").show();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseHistoryWdw
	 */
	,onBtnSaveCloseHistoryWdw: function() {
		this._getDc_("creditLine").doSave();
		this._getWindow_("creditLineHistoryWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveNewWdw
	 */
	,onBtnSaveNewWdw: function() {
		this.saveNew();
	}
	
	/**
	 * On-Click handler for button btnSaveNewCreditWdw
	 */
	,onBtnSaveNewCreditWdw: function() {
		this.saveNewCredit();
	}
	
	/**
	 * On-Click handler for button btnSaveNewContactWdw
	 */
	,onBtnSaveNewContactWdw: function() {
		this.saveNewContact();
	}
	
	/**
	 * On-Click handler for button btnSaveEditWdw
	 */
	,onBtnSaveEditWdw: function() {
		this.saveEdit();
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdw1
	 */
	,onHelpWdw1: function() {
		this.openHelp();
	}
	
	,onShowWdw: function() {	
		this._getWindow_("wdwSupplier").show();
	}
	
	,showContactWdw: function() {	
		this._getWindow_("contactWdw").show();
	}
	
	,showNoteWdw: function() {	
		this._getWindow_("noteWdw").show();
	}
	
	,showCreditLineWdw: function() {	
		this._getWindow_("creditLineWdw").show();
	}
	
	,onNoteWdwClose: function() {	
		this._getDc_("note").doCancel();
	}
	
	,onCreditLineHistoryWdwClose: function() {	
		this._getDc_("creditLineHistory").doCancel();
	}
	
	,_onTabChanged_: function() {
		
						this._getDc_("contract").doReloadPage();
	}
	
	,_when_called_for_details_: function(params) {
			
						var dc = this._getDc_("supplier");
						dc.setFilterValue("id", params.supplierId);
						dc.doQuery();
						this._showStackedViewElement_("main", "canvas2");
	}
	
	,_when_called_for_contracts_: function(params) {
			
						var detailsTab = this._get_("detailsTab");
						var contractsList = this._get_("contractsList");
		
						var dc = this._getDc_("supplier");
						dc.setFilterValue("id", params.supplierId);
						dc.doQuery();
						this._showStackedViewElement_("main", "canvas2");
		
						this.setActiveTabByTitle(detailsTab,contractsList);
	}
	
	,setActiveTabByTitle: function(tabPanel,tab) {
		
						tabPanel.setActiveTab(tab);
	}
	
	,_afterDefineDcs_: function() {
		
		
					var supplier = this._getDc_("supplier");
					var contact = this._getDc_("contact");
					var note = this._getDc_("note");
					var creditLine = this._getDc_("creditLine");
					var creditLineHistory = this._getDc_("creditLineHistory");
		
					note.on("afterDoQuerySuccess", function (dc) {
						var rec = dc.getRecord();
					    if (rec) {
							var date = rec.get("createdAt");
							var newDate = ("0" + date.getDate()).slice(-2)+"/"+("0" + (date.getMonth() + 1)).slice(-2)+"/"+date.getFullYear();
					    }
					}, this);
		
					note.on("afterDoNew", function (dc) {
		
					   this.showNoteWdw();
					}, this);
		
					supplier.on("afterDoNew", function (dc) {
						var rec = dc.getRecord();
					    if (rec) {
					        rec.set("isSupplier", "1");
					    }
						dc.setEditMode();
					    this.onShowWdw();
					}, this);
		
		
					contact.on("afterDoNew", function (dc) {
						dc.setEditMode();
						this.showContactWdw();
					}, this);
		
					note.on("afterDoEditIn", function (dc) {
						this._getWindow_("noteWdw").show();
					}, this);
		
					creditLine.on("afterDoNew", function (dc) {
						dc.setEditMode();
					    this.showCreditLineWdw();
					}, this);
		
					this._getDc_("supplier").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwSupplier").close();
						} else if (ajaxResult.options.options.doEditAfterSave === true) {
							this._getWindow_("wdwSupplier").close();
							this._showStackedViewElement_("main", "canvas2");	
							supplier.doEditIn();
						}
					}, this);
		
					this._getDc_("contact").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("contactWdw").close();					
						}
						dc.doReloadPage();
					}, this);
					
					this._getDc_("note").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("noteWdw").close();
						}
					}, this);
		
					this._getDc_("creditLine").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("creditLineWdw").close();
							dc.doReloadRecord();
						}
					}, this);
	}
	
	,saveNew: function() {
		
					var dc = this._getDc_("supplier");
					dc.doSave({doNewAfterSave:true});
	}
	
	,saveEdit: function() {
		
					var dc = this._getDc_("supplier");
					dc.doSave({doEditAfterSave:true});
	}
	
	,saveNewContact: function() {
		
					var dc = this._getDc_("contact");
					dc.doSave({doNewAfterSave:true});
	}
	
	,saveNewCredit: function() {
		
					var dc = this._getDc_("creditLine");
					dc.doSave({doNewAfterSave:true});
	}
	
	,saveClose: function() {
		
					var supplier = this._getDc_("supplier");
					supplier.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveCloseContact: function() {
		
					var contact = this._getDc_("contact");
					contact.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveCloseNote: function() {
		
					var note = this._getDc_("note");
					note.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveCloseCredit: function() {
		
					var creditLine = this._getDc_("creditLine");
					creditLine.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/Suppliers.html";
					window.open( url, "SONE_Help");
	}
	
	,onWdwSupplierClose: function() {
		
						var dc = this._getDc_("supplier");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,onContactWdwClose: function() {
		
						var dc = this._getDc_("contact");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,onCreditLineWdwClose: function() {
		
						var dc = this._getDc_("creditLine");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,fnSaveEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isSaveEnabled(dc);
	}
});
