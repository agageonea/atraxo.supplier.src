/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

/* model */
Ext.define("atraxo.fmbas.ui.extjs.asgn.FinalCustomerResseler_Asgn$Model", {
	extend: 'Ext.data.Model',
	statics: {
		ALIAS: "fmbas_FinalCustomerResseler_Asgn"
	},
	fields:  [
		{name:"id",type:"string"},
		{name:"name",type:"string"},
		{name:"code",type:"string"},
		{name:"iata",type:"string"},
		{name:"icao",type:"string"}
	]
});

/* lists */
Ext.define( "atraxo.fmbas.ui.extjs.asgn.FinalCustomerResseler_Asgn$List", {
	extend: "e4e.asgn.AbstractAsgnGrid",
	alias:[ "widget.fmbas_FinalCustomerResseler_Asgn$Left","widget.fmbas_FinalCustomerResseler_Asgn$Right" ],
	_defineColumns_: function () {
		this._getBuilder_()
		.addTextColumn({name:"id", dataIndex:"id", hidden:true})
		.addTextColumn({name:"name", dataIndex:"name", width:120})
		.addTextColumn({name:"code", dataIndex:"code", width:40})
		.addTextColumn({name:"iata", dataIndex:"iata", width:50})
		.addTextColumn({name:"icao", dataIndex:"icao", width:50})
	} 
});
	
/* ui-window */
Ext.define("atraxo.fmbas.ui.extjs.asgn.FinalCustomerResseler_Asgn$Ui", {
	extend: "e4e.asgn.AbstractAsgnUi",
	width:800,
	height:400,
	title:"Assign final customer to customer ",
	_filterFields_: [
		["name"],
		["code"],
		["iata"],
		["icao"]
	],
	_defaultFilterField_ : "name",

	_defineElements_: function () {
		this._getBuilder_()
			.addLeftGrid({ xtype:"fmbas_FinalCustomerResseler_Asgn$Left"})
			.addRightGrid({ xtype:"fmbas_FinalCustomerResseler_Asgn$Right"})
	}
});

/* ui-panel */
Ext.define("atraxo.fmbas.ui.extjs.asgn.FinalCustomerResseler_Asgn$AsgnPanel", {
	extend: "e4e.asgn.AbstractAsgnPanel",
	alias: "widget.fmbas_FinalCustomerResseler_Asgn$AsgnPanel",
	width:800,
	height:400,
	title:"Assign final customer to customer ",
	_filterFields_: [
		["name"],
		["code"],
		["iata"],
		["icao"]
	],
	_defaultFilterField_ : "name",

	_defineElements_: function () {
		this._getBuilder_()
			.addLeftGrid({ xtype:"fmbas_FinalCustomerResseler_Asgn$Left"})
			.addRightGrid({ xtype:"fmbas_FinalCustomerResseler_Asgn$Right"})
	}
});
