/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */

/* model */
Ext.define("atraxo.fmbas.ui.extjs.asgn.AreasLocations_Asgn$Model", {
	extend: 'Ext.data.Model',
	statics: {
		ALIAS: "fmbas_AreasLocations_Asgn"
	},
	fields:  [
		{name:"id",type:"string"},
		{name:"name",type:"string"},
		{name:"code",type:"string"},
		{name:"validFrom",type:"string"},
		{name:"validTo",type:"string"}
	]
});

/* lists */
Ext.define( "atraxo.fmbas.ui.extjs.asgn.AreasLocations_Asgn$List", {
	extend: "e4e.asgn.AbstractAsgnGrid",
	alias:[ "widget.fmbas_AreasLocations_Asgn$Left","widget.fmbas_AreasLocations_Asgn$Right" ],
	_defineColumns_: function () {
		this._getBuilder_()
		.addTextColumn({name:"id", dataIndex:"id", hidden:true})
		.addTextColumn({name:"name", dataIndex:"name", width:120})
		.addTextColumn({name:"code", dataIndex:"code", width:40})
		.addTextColumn({name:"validFrom", dataIndex:"validFrom", hidden:true})
		.addTextColumn({name:"validTo", dataIndex:"validTo", hidden:true})
	} 
});
	
/* ui-window */
Ext.define("atraxo.fmbas.ui.extjs.asgn.AreasLocations_Asgn$Ui", {
	extend: "e4e.asgn.AbstractAsgnUi",
	width:800,
	height:400,
	title:"Assign locations to area ",
	_filterFields_: [
		["name"],
		["code"]
	],
	_defaultFilterField_ : "name",

	_defineElements_: function () {
		this._getBuilder_()
			.addLeftGrid({ xtype:"fmbas_AreasLocations_Asgn$Left"})
			.addRightGrid({ xtype:"fmbas_AreasLocations_Asgn$Right"})
	}
});

/* ui-panel */
Ext.define("atraxo.fmbas.ui.extjs.asgn.AreasLocations_Asgn$AsgnPanel", {
	extend: "e4e.asgn.AbstractAsgnPanel",
	alias: "widget.fmbas_AreasLocations_Asgn$AsgnPanel",
	width:800,
	height:400,
	title:"Assign locations to area ",
	_filterFields_: [
		["name"],
		["code"]
	],
	_defaultFilterField_ : "name",

	_defineElements_: function () {
		this._getBuilder_()
			.addLeftGrid({ xtype:"fmbas_AreasLocations_Asgn$Left"})
			.addRightGrid({ xtype:"fmbas_AreasLocations_Asgn$Right"})
	}
});
