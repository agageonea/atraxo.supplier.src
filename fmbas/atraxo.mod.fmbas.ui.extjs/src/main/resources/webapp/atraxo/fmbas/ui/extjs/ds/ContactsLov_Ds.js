/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ContactsLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_ContactsLov_Ds"
	},
	
	
	fields: [
		{name:"firstName", type:"string"},
		{name:"lastName", type:"string"},
		{name:"fullNameField", type:"string"},
		{name:"email", type:"string"},
		{name:"businessPhone", type:"string"},
		{name:"objectId", type:"int", allowNull:true},
		{name:"objectType", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.ContactsLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"firstName", type:"string"},
		{name:"lastName", type:"string"},
		{name:"fullNameField", type:"string"},
		{name:"email", type:"string"},
		{name:"businessPhone", type:"string"},
		{name:"objectId", type:"int", allowNull:true},
		{name:"objectType", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
