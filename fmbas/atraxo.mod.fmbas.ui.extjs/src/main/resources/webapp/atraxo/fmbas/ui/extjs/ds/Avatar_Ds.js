/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Avatar_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_Avatar_Ds"
	},
	
	
	validators: {
		userId: [{type: 'presence'}],
		name: [{type: 'presence'}],
		active: [{type: 'presence'}],
		type: [{type: 'presence'}]
	},
	
	fields: [
		{name:"userName", type:"string"},
		{name:"userId", type:"string"},
		{name:"loginName", type:"string"},
		{name:"name", type:"string"},
		{name:"fileName", type:"string"},
		{name:"location", type:"string"},
		{name:"contentType", type:"string"},
		{name:"active", type:"boolean"},
		{name:"typeId", type:"string"},
		{name:"type", type:"string"},
		{name:"baseUrl", type:"string"},
		{name:"filePath", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Avatar_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"userName", type:"string"},
		{name:"userId", type:"string"},
		{name:"loginName", type:"string"},
		{name:"name", type:"string"},
		{name:"fileName", type:"string"},
		{name:"location", type:"string"},
		{name:"contentType", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"typeId", type:"string"},
		{name:"type", type:"string"},
		{name:"baseUrl", type:"string"},
		{name:"filePath", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
