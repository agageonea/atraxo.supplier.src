/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.UsersLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_UsersLov_Ds"
	},
	
	
	fields: [
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"userName", type:"string"},
		{name:"loginName", type:"string"},
		{name:"email", type:"string"},
		{name:"userCode", type:"string"},
		{name:"active", type:"boolean"}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.UsersLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"id", type:"string"},
		{name:"clientId", type:"string"},
		{name:"userName", type:"string"},
		{name:"loginName", type:"string"},
		{name:"email", type:"string"},
		{name:"userCode", type:"string"},
		{name:"active", type:"boolean", allowNull:true}
	]
});
