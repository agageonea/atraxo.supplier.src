/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.AcTypes_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.AcTypes_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.AcTypes_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_AcTypes_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"code", dataIndex:"code", maxLength:4,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.AcTypesLov_Lov", selectOnFocus:true, maxLength:4,
					retFieldMapping: [{lovField:"code", dsField: "code"} ]}})
			.addTextField({ name:"name", dataIndex:"name", maxLength:64})
			.addNumberField({name:"burnoffProH", dataIndex:"burnoffProH", maxLength:11})
			.addNumberField({name:"tankCapacity", dataIndex:"tankCapacity", maxLength:11})
			.addLov({name:"unit", dataIndex:"unitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MeasurmentUnitsLov_Lov", selectOnFocus:true, maxLength:2,
					retFieldMapping: [{lovField:"id", dsField: "unitId"} ]}})
			.addBooleanField({ name:"active", dataIndex:"active"})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.AcTypes_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_AcTypes_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"code", dataIndex:"code", width:100, allowBlank: false, maxLength:4, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:4}})
		.addTextColumn({name:"name", dataIndex:"name", width:160, maxLength:64, 
			editor: { xtype:"textfield", maxLength:64}})
		.addNumberColumn({name:"burnoffProH", dataIndex:"burnoffProH", width:150, allowBlank: false, maxLength:11, align:"right" })
		.addNumberColumn({name:"tankCapacity", dataIndex:"tankCapacity", width:150, allowBlank: false, maxLength:11, align:"right" })
		.addLov({name:"unit", dataIndex:"unitCode", width:60, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
				retFieldMapping: [{lovField:"id", dsField: "unitId"} ]}})
		.addBooleanColumn({name:"active", dataIndex:"active"})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
