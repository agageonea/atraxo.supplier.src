/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.WorkflowTask_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.WorkflowTask_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.WorkflowTask_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.WorkflowTask_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_WorkflowTask_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:64})
			.addTextField({ name:"description", dataIndex:"description", maxLength:255})
			.addTextField({ name:"candidate", dataIndex:"candidate", maxLength:64})
			.addTextField({ name:"assignee", dataIndex:"assignee", maxLength:64})
			.addTextField({ name:"owner", dataIndex:"owner", maxLength:64})
			.addTextField({ name:"process", dataIndex:"process", maxLength:64})
		;
	}

});

/* ================= EDIT-GRID: WorkFlowTaskForm ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.WorkflowTask_Dc$WorkFlowTaskForm", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_WorkflowTask_Dc$WorkFlowTaskForm",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"process", dataIndex:"process", width:120, noEdit: true, maxLength:64,  flex:1})
		.addTextColumn({name:"name", dataIndex:"name", width:120, noEdit: true, maxLength:64,  flex:1})
		.addTextColumn({name:"description", dataIndex:"description", width:120, noEdit: true, maxLength:255,  flex:2})
		.addTextColumn({name:"candidate", dataIndex:"candidate", width:120, noEdit: true, maxLength:64,  flex:1})
		.addTextColumn({name:"assignee", dataIndex:"assignee", width:120, noEdit: true, maxLength:64,  flex:1})
		.addTextColumn({name:"owner", dataIndex:"owner", width:120, noEdit: true, maxLength:64,  flex:2})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: EditCompleteTask ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.WorkflowTask_Dc$EditCompleteTask", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_WorkflowTask_Dc$EditCompleteTask",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{d.workflowTaskNote}", dataIndex:"workflowTaskNote", width:400, labelAlign:"top", fieldStyle:"margin-top: 5px"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:400, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
						
	}
});
