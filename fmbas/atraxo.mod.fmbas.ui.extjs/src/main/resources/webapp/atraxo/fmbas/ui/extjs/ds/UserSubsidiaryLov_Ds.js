/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.UserSubsidiaryLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_UserSubsidiaryLov_Ds"
	},
	
	
	fields: [
		{name:"mainSubsidiary", type:"boolean"},
		{name:"userCode", type:"string"},
		{name:"userId", type:"string"},
		{name:"subsidiaryId", type:"int", allowNull:true},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.UserSubsidiaryLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"mainSubsidiary", type:"boolean", allowNull:true},
		{name:"userCode", type:"string"},
		{name:"userId", type:"string"},
		{name:"subsidiaryId", type:"int", allowNull:true},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
