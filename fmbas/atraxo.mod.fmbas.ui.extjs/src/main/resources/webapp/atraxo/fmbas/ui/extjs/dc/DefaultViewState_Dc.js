/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.DefaultViewState_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.DefaultViewState_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.DefaultViewState_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_DefaultViewState_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:255})
			.addLov({name:"cmp", dataIndex:"cmp", maxLength:255,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.ViewCmpLov_Lov", selectOnFocus:true, maxLength:255}})
			.addLov({name:"cmpType", dataIndex:"cmpType", maxLength:16,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.ViewCmpTypesLov_Lov", selectOnFocus:true, maxLength:16}})
			.addLov({name:"unitCode", dataIndex:"unitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.UnitsLov_Lov", selectOnFocus:true, maxLength:2}})
			.addNumberField({name:"pageSize", dataIndex:"pageSize", maxLength:11})
			.addBooleanField({ name:"totals", dataIndex:"totals"})
			.addBooleanField({ name:"isStandard", dataIndex:"isStandard"})
			.addBooleanField({ name:"availableSystemwide", dataIndex:"availableSystemwide"})
			.addBooleanField({ name:"active", dataIndex:"active"})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.DefaultViewState_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_DefaultViewState_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"userName", dataIndex:"userName", width:110, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_UserNameLov_Lov", allowBlank:false, maxLength:255,
				retFieldMapping: [{lovField:"id", dsField: "userId"} ]}})
		.addLov({name:"cmp", dataIndex:"cmp", width:320, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_ViewCmpLov_Lov", allowBlank:false,listeners:{
				change:{scope:this, fn:function() {this._clearViewStateName_()}}, buffer: Main.viewConfig.GRID_EDITOR_CHANGE_EVT_BUFFER
			}, maxLength:255}})
		.addLov({name:"name", dataIndex:"name", width:130, allowBlank: false, _enableFn_: function(dc, rec, col, fld) { return !Ext.isEmpty(dc.record.data.cmp); } , xtype:"gridcolumn", 
			editor:{xtype:"fmbas_ViewLov_Lov", allowBlank:false, maxLength:255,
				retFieldMapping: [{lovField:"id", dsField: "viewId"} ],
				filterFieldMapping: [{lovField:"cmp", dsField: "cmp"} ]}})
		.addTextColumn({name:"cmpType", dataIndex:"cmpType", width:130, noEdit: true, maxLength:16})
		.addTextColumn({name:"value", dataIndex:"value", width:320, noEdit: true, maxLength:4000})
		.addNumberColumn({name:"pageSize", dataIndex:"pageSize", noEdit: true, maxLength:11, align:"right" })
		.addBooleanColumn({name:"totals", dataIndex:"totals", noEdit: true})
		.addTextColumn({name:"unitCode", dataIndex:"unitCode", width:110, noEdit: true, maxLength:2})
		.addTextColumn({name:"currencyCode", dataIndex:"currencyCode", width:110, noEdit: true, maxLength:3})
		.addBooleanColumn({name:"isStandard", dataIndex:"isStandard", width:110, noEdit: true})
		.addBooleanColumn({name:"availableSystemwide", dataIndex:"availableSystemwide", width:160, noEdit: true})
		.addBooleanColumn({name:"active", dataIndex:"active", width:110, noEdit: true})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_clearViewStateName_: function() {
		
						var rec = this._controller_.getRecord();
						if (rec) {
							rec.set("name","");
							rec.set("viewId","");
						}
	}
});
