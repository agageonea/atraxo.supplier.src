/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.CustomerMpLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_CustomerMpLov_Ds"
	},
	
	
	fields: [
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"type", type:"string"},
		{name:"refid", type:"string"},
		{name:"status", type:"string"},
		{name:"isCustomer", type:"boolean"},
		{name:"business", type:"string"},
		{name:"buyerId", type:"string"},
		{name:"buyerName", type:"string"},
		{name:"buyerCode", type:"string"},
		{name:"primaryContactId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"primaryContactName", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.CustomerMpLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"type", type:"string"},
		{name:"refid", type:"string"},
		{name:"status", type:"string"},
		{name:"isCustomer", type:"boolean", allowNull:true},
		{name:"business", type:"string"},
		{name:"buyerId", type:"string"},
		{name:"buyerName", type:"string"},
		{name:"buyerCode", type:"string"},
		{name:"primaryContactId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"primaryContactName", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
