/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Trigger_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.Trigger_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.Trigger_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Trigger_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Trigger_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addCombo({ xtype:"combo", name:"type", dataIndex:"type", store:[ __FMBAS_TYPE__.TriggerType._ONCE_, __FMBAS_TYPE__.TriggerType._DAILY_, __FMBAS_TYPE__.TriggerType._WEEKLY_, __FMBAS_TYPE__.TriggerType._MONTHLY_]})
			.addDateField({name:"startDate", dataIndex:"startDate"})
			.addTextField({ name:"details", dataIndex:"details", maxLength:255})
			.addBooleanField({ name:"active", dataIndex:"active"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Trigger_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Trigger_Dc$List",
	_noPaginator_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"type", dataIndex:"type", width:100,  flex:1})
		.addTextColumn({ name:"details", dataIndex:"details", width:120,  flex:1})
		.addDateColumn({ name:"startDate", dataIndex:"startDate", hidden:true, width:150, _mask_: Masks.DATETIME})
		.addBooleanColumn({ name:"active", dataIndex:"active",  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: New ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Trigger_Dc$New", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Trigger_Dc$New",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addCombo({ xtype:"combo", name:"type", bind:"{d.type}", dataIndex:"type", allowBlank:false, store:[ __FMBAS_TYPE__.TriggerType._ONCE_, __FMBAS_TYPE__.TriggerType._DAILY_, __FMBAS_TYPE__.TriggerType._WEEKLY_, __FMBAS_TYPE__.TriggerType._MONTHLY_], labelWidth:92, typeAhead:false,listeners:{
			change:{scope:this, fn:this._showHidePanels_},
			select:{scope:this, fn:this._resetFormValues_}
		}})
		.addDateTimeField({name:"startDate", bind:"{d.startDate}", dataIndex:"startDate", allowBlank:false,listeners:{
			change:{scope:this, fn:this._setSummary_}
		}})
		.addBooleanField({ name:"timeRef", bind:"{d.timeReference}", dataIndex:"timeReference", width:40, labelWidth:25})
		.addDisplayFieldText({ name:"timeLabel", bind:"{d.time}", dataIndex:"time", noEdit:true , maxLength:32, labelWidth:100})
		.addTextField({ name:"details", bind:"{d.details}", dataIndex:"details", noEdit:true , width:700, maxLength:255, cls:"sone-field-flat", labelWidth:83})
		.addTextArea({ name:"temp", bind:"{d.temp}", dataIndex:"temp"})
		.addNumberField({name:"intInDays", bind:"{d.intInDays}", dataIndex:"intInDays", width:130, maxLength:3, labelWidth:80,listeners:{
			change:{scope:this, fn:this._setSummary_}
		}})
		.addNumberField({name:"intInWeeks", bind:"{d.intInWeeks}", dataIndex:"intInWeeks", width:130, maxLength:3, labelWidth:80,listeners:{
			change:{scope:this, fn:this._setSummary_}
		}})
		.addDisplayFieldText({ name:"days", bind:"{d.days}", dataIndex:"days", maxLength:16, labelWidth:40})
		.addDisplayFieldText({ name:"weeks", bind:"{d.weeks}", dataIndex:"weeks", maxLength:16, labelWidth:70})
		.addHiddenField({ name:"daysOfMonth", bind:"{d.daysOfMonth}", dataIndex:"daysOfMonth", labelWidth:40})
		.addHiddenField({ name:"months", bind:"{d.months}", dataIndex:"months", labelWidth:70})
		.addHiddenField({ name:"weekOfMonth", bind:"{d.weekOfMonth}", dataIndex:"weekOfMonth", labelWidth:40})
		.addHiddenField({ name:"daysOfWeek", bind:"{d.daysOfWeek}", dataIndex:"daysOfWeek", labelWidth:70})
		.addCombo({ xtype:"combo", name:"daysOfMonthCombo", bind:"{p.daysOfMonthCombo}", paramIndex:"daysOfMonthCombo", _enableFn_: function(dc, rec) { return dc.record.data.onWeekDays == false; } , width:428, store:[ __FMBAS_TYPE__.DaysOfMonth._ALL_, __FMBAS_TYPE__.DaysOfMonth._1_, __FMBAS_TYPE__.DaysOfMonth._2_, __FMBAS_TYPE__.DaysOfMonth._3_, __FMBAS_TYPE__.DaysOfMonth._4_, __FMBAS_TYPE__.DaysOfMonth._5_, __FMBAS_TYPE__.DaysOfMonth._6_, __FMBAS_TYPE__.DaysOfMonth._7_, __FMBAS_TYPE__.DaysOfMonth._8_, __FMBAS_TYPE__.DaysOfMonth._9_, __FMBAS_TYPE__.DaysOfMonth._10_, __FMBAS_TYPE__.DaysOfMonth._11_, __FMBAS_TYPE__.DaysOfMonth._12_, __FMBAS_TYPE__.DaysOfMonth._13_, __FMBAS_TYPE__.DaysOfMonth._14_, __FMBAS_TYPE__.DaysOfMonth._15_, __FMBAS_TYPE__.DaysOfMonth._16_, __FMBAS_TYPE__.DaysOfMonth._17_, __FMBAS_TYPE__.DaysOfMonth._18_, __FMBAS_TYPE__.DaysOfMonth._19_, __FMBAS_TYPE__.DaysOfMonth._20_, __FMBAS_TYPE__.DaysOfMonth._21_, __FMBAS_TYPE__.DaysOfMonth._22_, __FMBAS_TYPE__.DaysOfMonth._23_, __FMBAS_TYPE__.DaysOfMonth._24_, __FMBAS_TYPE__.DaysOfMonth._25_, __FMBAS_TYPE__.DaysOfMonth._26_, __FMBAS_TYPE__.DaysOfMonth._27_, __FMBAS_TYPE__.DaysOfMonth._28_, __FMBAS_TYPE__.DaysOfMonth._29_, __FMBAS_TYPE__.DaysOfMonth._30_, __FMBAS_TYPE__.DaysOfMonth._31_, __FMBAS_TYPE__.DaysOfMonth._LAST_], labelWidth:30, multiSelect:true,listeners:{
			change:{scope:this, fn:this._setSummary_}
		}})
		.addCombo({ xtype:"combo", name:"monthsCombo", bind:"{p.monthsCombo}", paramIndex:"monthsCombo", width:725, store:[ __FMBAS_TYPE__.Months._ALL_, __FMBAS_TYPE__.Months._JANUARY_, __FMBAS_TYPE__.Months._FEBRUARY_, __FMBAS_TYPE__.Months._MARCH_, __FMBAS_TYPE__.Months._APRIL_, __FMBAS_TYPE__.Months._MAY_, __FMBAS_TYPE__.Months._JUNE_, __FMBAS_TYPE__.Months._JULY_, __FMBAS_TYPE__.Months._AUGUST_, __FMBAS_TYPE__.Months._SEPTEMBER_, __FMBAS_TYPE__.Months._OCTOBER_, __FMBAS_TYPE__.Months._NOVEMBER_, __FMBAS_TYPE__.Months._DECEMBER_], labelWidth:47, multiSelect:true,listeners:{
			change:{scope:this, fn:this._setSummary_}
		}})
		.addCombo({ xtype:"combo", name:"weekOfMonthCombo", bind:"{p.weekOfMonthCombo}", paramIndex:"weekOfMonthCombo", _enableFn_: function(dc, rec) { return dc.record.data.onMonthDays == false; } , width:227, store:[ __FMBAS_TYPE__.WeekOfMonth._FIRST_, __FMBAS_TYPE__.WeekOfMonth._SECOND_, __FMBAS_TYPE__.WeekOfMonth._THIRD_, __FMBAS_TYPE__.WeekOfMonth._FOURTH_, __FMBAS_TYPE__.WeekOfMonth._LAST_], labelWidth:30,listeners:{
			change:{scope:this, fn:this._setSummary_}
		}})
		.addCombo({ xtype:"combo", name:"daysOfWeekCombo", bind:"{p.daysOfWeekCombo}", paramIndex:"daysOfWeekCombo", _enableFn_: function(dc, rec) { return dc.record.data.onMonthDays == false; } , width:197, noLabel: true, store:[ __FMBAS_TYPE__.DaysOfWeek._MONDAY_, __FMBAS_TYPE__.DaysOfWeek._TUESDAY_, __FMBAS_TYPE__.DaysOfWeek._WEDNESDAY_, __FMBAS_TYPE__.DaysOfWeek._THURSDAY_, __FMBAS_TYPE__.DaysOfWeek._FRIDAY_, __FMBAS_TYPE__.DaysOfWeek._SATURDAY_, __FMBAS_TYPE__.DaysOfWeek._SUNDAY_], style:"margin-left:5px",listeners:{
			change:{scope:this, fn:this._setSummary_}
		}})
		.addHiddenField({ name:"daysOfWeekEnum", bind:"{d.daysOfWeek}", dataIndex:"daysOfWeek", width:197, noLabel: true, style:"margin-left:5px", multiSelect:true,listeners:{
			change:{scope:this, fn:this._setSummary_}
		}})
		.addBooleanField({ name:"onMonthDays", bind:"{d.onMonthDays}", dataIndex:"onMonthDays", noLabel: true, fieldCls:"x-form-radio",listeners:{
			change:{scope:this, fn:this._configWeekDays_}
		}})
		.addBooleanField({ name:"onWeekDays", bind:"{d.onWeekDays}", dataIndex:"onWeekDays", noLabel: true, fieldCls:"x-form-radio",listeners:{
			change:{scope:this, fn:this._configMonthDays_}
		}})
		.addBooleanField({ name:"repChk", bind:"{d.repChk}", dataIndex:"repChk", width:25, noLabel: true, style:"margin-left: 25px"})
		.addBooleanField({ name:"timeoutChk", bind:"{d.timeoutChk}", dataIndex:"timeoutChk", width:25, noLabel: true, style:"margin-left: 25px"})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active", width:25, noLabel: true, style:"margin-left: 25px"})
		.addDisplayFieldText({ name:"third", bind:"{d.enable}", dataIndex:"enable", noEdit:true , width:130, maxLength:32, labelWidth:130, labelAlign:"left"})
		.addCombo({ xtype:"combo", name:"repeatInterval", bind:"{d.repeatInterval}", dataIndex:"repeatInterval", width:245, store:[ __FMBAS_TYPE__.TimeInterval._5_MINUTES_, __FMBAS_TYPE__.TimeInterval._10_MINUTES_, __FMBAS_TYPE__.TimeInterval._15_MINUTES_, __FMBAS_TYPE__.TimeInterval._30_MINUTES_, __FMBAS_TYPE__.TimeInterval._1_HOUR_], labelAlign:"left"})
		.addCombo({ xtype:"combo", name:"repeatDur", bind:"{d.repeatDuration}", dataIndex:"repeatDuration", width:250, store:[ __FMBAS_TYPE__.RepeatDuration._INDEFINITELY_, __FMBAS_TYPE__.RepeatDuration._15_MINUTES_, __FMBAS_TYPE__.RepeatDuration._30_MINUTES_, __FMBAS_TYPE__.RepeatDuration._1_HOUR_, __FMBAS_TYPE__.RepeatDuration._12_HOURS_, __FMBAS_TYPE__.RepeatDuration._1_DAY_]})
		.addCombo({ xtype:"combo", name:"timeout", bind:"{d.timeout}", dataIndex:"timeout", width:335, store:[ __FMBAS_TYPE__.TimeDuration._30_MINUTES_, __FMBAS_TYPE__.TimeDuration._1_HOUR_, __FMBAS_TYPE__.TimeDuration._2_HOURS_, __FMBAS_TYPE__.TimeDuration._4_HOURS_, __FMBAS_TYPE__.TimeDuration._12_HOURS_, __FMBAS_TYPE__.TimeDuration._1_DAY_, __FMBAS_TYPE__.TimeDuration._3_DAYS_], labelWidth:175, labelAlign:"left"})
		.addBooleanField({ name:"Monday", bind:"{d.monday}", dataIndex:"monday", style:"margin-left: -8px",listeners:{
			change:{scope:this, fn:this._setupDaysArray_}
		}})
		.addBooleanField({ name:"Tuesday", bind:"{d.tuesday}", dataIndex:"tuesday",listeners:{
			change:{scope:this, fn:this._setupDaysArray_}
		}})
		.addBooleanField({ name:"Wednesday", bind:"{d.wednesday}", dataIndex:"wednesday",listeners:{
			change:{scope:this, fn:this._setupDaysArray_}
		}})
		.addBooleanField({ name:"Thursday", bind:"{d.thursday}", dataIndex:"thursday",listeners:{
			change:{scope:this, fn:this._setupDaysArray_}
		}})
		.addBooleanField({ name:"Friday", bind:"{d.friday}", dataIndex:"friday",listeners:{
			change:{scope:this, fn:this._setupDaysArray_}
		}})
		.addBooleanField({ name:"Saturday", bind:"{d.saturday}", dataIndex:"saturday",listeners:{
			change:{scope:this, fn:this._setupDaysArray_}
		}})
		.addBooleanField({ name:"Sunday", bind:"{d.sunday}", dataIndex:"sunday",listeners:{
			change:{scope:this, fn:this._setupDaysArray_}
		}})
		.add({name:"daysComp", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("onMonthDays"),this._getConfig_("daysOfMonth"),this._getConfig_("daysOfMonthCombo")]})
		.add({name:"weeksComp", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("onWeekDays"),this._getConfig_("weekOfMonth"),this._getConfig_("daysOfWeek"),this._getConfig_("weekOfMonthCombo"),this._getConfig_("daysOfWeekCombo")]})
		.add({name:"one", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("intInDays"),this._getConfig_("days")]})
		.add({name:"two", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("intInWeeks"),this._getConfig_("weeks"),this._getConfig_("daysOfWeekEnum")]})
		.add({name:"daysCheckboxes", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("Monday"),this._getConfig_("Tuesday"),this._getConfig_("Wednesday"),this._getConfig_("Thursday"),this._getConfig_("Friday"),this._getConfig_("Saturday"),this._getConfig_("Sunday")]})
		.add({name:"row", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("type"),this._getConfig_("startDate"),this._getConfig_("timeRef"),this._getConfig_("timeLabel")]})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("repChk"),this._getConfig_("repeatInterval"),this._getConfig_("repeatDur")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("timeoutChk"),this._getConfig_("timeout")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("active"),this._getConfig_("third")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col", width:700, layout:"anchor"})
		.addPanel({ name:"fieldsWrapper", height: 108,  style:"border:1px solid #D0D0D0 !important; margin-top:5px; margin-bottom:10px; padding:10px 10px 5px 10px; margin-left:25px; margin-right:25px"})
		.addPanel({ name:"daily", width:600,  hidden:true, layout:"anchor"})
		.addPanel({ name:"checkBoxGroup",  hidden:true, style:"border:1px solid #D0D0D0 !important; margin-top:5px; margin-bottom:10px; padding:10px 10px 5px 10px"})
		.addPanel({ name:"weekly", width:600,  hidden:true, cls:"sone-checkbox-group", layout:"anchor"})
		.addPanel({ name:"monthly", width:600,  hidden:true, layout:"anchor"})
		.addPanel({ name:"col1", width:800, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col", "fieldsWrapper", "col1"])
		.addChildrenTo("col", ["row"])
		.addChildrenTo("fieldsWrapper", ["daily", "weekly", "monthly"])
		.addChildrenTo("daily", ["one"])
		.addChildrenTo("checkBoxGroup", ["daily", "weekly", "monthly"])
		.addChildrenTo("weekly", ["two", "daysCheckboxes"])
		.addChildrenTo("monthly", ["months", "monthsCombo", "daysComp", "weeksComp"])
		.addChildrenTo("col1", ["row1", "row2", "row3", "details"]);
	},
	/* ==================== Business functions ==================== */
	
	_afterApplyStates_: function() {
		
						this.daysArray = [];
	},
	
	_resetFormValues_: function() {
		
						var items = this.getForm().getFields().items, i = 0, len = items.length;
						for(i; i < len; i++) {
						    var c = items[i];
						    if (c.name != "type" && c.name != "startDate") {
								c.setValue("");
							}
						}
	},
	
	_setSummary_: function() {
		
		
						var summaryField = this._get_("details");
						var type = this._get_("type");
						var typeVal = type.getValue();
						var dc = this._controller_;
		
						if (!Ext.isEmpty(typeVal) && dc.isEditMode == true) {
		
							switch (typeVal) {
							    case "Daily":
									this._setDailySummary_();
							        break;
							    case "Weekly":
							        this._setWeeklySummary_();
							        break;
							    case "Monthly":
							        this._setMonthlySummary_();
							        break;
								case "Once":
									this._setOnceSummary_();
							        break;
							}
						}
						
	},
	
	_setMonthlySummary_: function() {
		
		
						var summaryField = this._get_("details");
						var startDate = this._get_("startDate").getValue();
						var monthsEnum = "";
						var theString = "";
						var daysOfMonthEnum = "";
						var monthsEnumStr = "";
		
						var weekOfMonthCombo = this._get_("weekOfMonthCombo");
						var daysOfWeekCombo = this._get_("daysOfWeekCombo");
						var weekOfMonth = this._get_("weekOfMonth");
						var daysOfWeek = this._get_("daysOfWeek");
		
						var onMonthDays = this._get_("onMonthDays");
						var onWeekDays = this._get_("onWeekDays");
						var daysOfMonth = this._get_("daysOfMonthCombo");
						
						var months = this._get_("months");
						var days = this._get_("daysOfMonth");
		
						if (!Ext.isEmpty(startDate)) {
							var time = Ext.Date.format(startDate, "H:i");
							var date = Ext.Date.format(startDate, "d.m.Y");
		
							if (!Ext.isEmpty(this._get_("monthsCombo").getValue())) {
								monthsEnum = this._get_("monthsCombo").getValue().toString();
								monthsEnum = monthsEnum.indexOf( "," ) == 0 ? monthsEnum = monthsEnum.replace( ",", "" ) : monthsEnum;
								if (!Ext.isEmpty(monthsEnum)) {
									monthsEnumStr = Main.translate("jobTriggers", "each__lbl")+" "+monthsEnum+" ";
									months.setValue(monthsEnum);
								}
							}
							else {
								if (this._get_("monthsCombo").getValue().length > 0) {
									monthsEnum = this._get_("months").getValue();
								}
								else {
									months.setValue(null);
								}
								
							}
							if (onMonthDays.getValue() == true) {
								if (!Ext.isEmpty(daysOfMonth.getValue())) {
									daysOfMonthEnum = daysOfMonth.getValue().toString();
									daysOfMonthEnum = daysOfMonthEnum.indexOf( "," ) == 0 ? daysOfMonthEnum = daysOfMonthEnum.replace( ",", "" ) : daysOfMonthEnum;
									theString = Main.translate("jobTriggers", "onThe__lbl")+" "+daysOfMonthEnum+" "+Main.translate("jobTriggers", "ofTheMonth__lbl");
									days.setValue(daysOfMonthEnum);							
								}
								else {
									if (daysOfMonth.getValue().length > 0) {
										daysOfMonthEnum = days.getValue();
									}
									else {
										days.setValue(null);
									}
								}
							}
							if (onWeekDays.getValue() == true) {
		
								var s1 = weekOfMonth.getValue();
								var s2 = daysOfWeek.getValue();
		
								if (!Ext.isEmpty(weekOfMonthCombo.getValue())) {
									s1 = weekOfMonthCombo.getValue();
									weekOfMonth.setValue(s1);
								}
								if (!Ext.isEmpty(daysOfWeekCombo.getValue())) {
									s2 = daysOfWeekCombo.getValue();
									daysOfWeek.setValue(s2);
								}
								theString = "on the "+s1+" "+s2; 
							}
							summaryField.setValue(Main.translate("jobTriggers", "at__lbl")+" "+time+", "+theString+" "+monthsEnumStr+Main.translate("jobTriggers", "starting__lbl")+" "+date);
							
						}
	},
	
	_setWeeklySummary_: function() {
		
		
						var summaryField = this._get_("details");
						var startDate = this._get_("startDate").getValue();
						var numWeeks = 0;
						var daysEnum = "";
						var daysStr = "";
						var numWeeksStr = "";
		
						if (!Ext.isEmpty(startDate)) {
							var time = Ext.Date.format(startDate, "H:i");
							var date = Ext.Date.format(startDate, "d.m.Y");
							if (!Ext.isEmpty(this._get_("intInWeeks").getValue())) {
								numWeeks = this._get_("intInWeeks").getValue();
								numWeeksStr = Main.translate("jobTriggers", "ofEvery__lbl")+" "+numWeeks+" "+Main.translate("jobTriggers", "weeks__lbl")+" ";
							}
							if (!Ext.isEmpty(this._get_("daysOfWeekEnum").getValue())) {
								daysEnum = this._get_("daysOfWeekEnum").getValue();
								daysStr = Main.translate("jobTriggers", "every__lbl")+" "+daysEnum;
								
							}
							
							summaryField.setValue(Main.translate("jobTriggers", "at__lbl")+" "+time+" "+daysStr+" "+numWeeksStr+Main.translate("jobTriggers", "starting__lbl")+" "+date);
						}
	},
	
	_setOnceSummary_: function() {
		
		
						var summaryField = this._get_("details");
						var startDate = this._get_("startDate").getValue();
		
						if (!Ext.isEmpty(startDate)) {
							var time = Ext.Date.format(startDate, "H:i");
							var date = Ext.Date.format(startDate, "d.m.Y");
							summaryField.setValue(Main.translate("jobTriggers", "at__lbl")+" "+time+" "+Main.translate("jobTriggers", "on__lbl")+" "+date);
						}
						
	},
	
	_setDailySummary_: function() {
		
		
						var summaryField = this._get_("details");
						var startDate = this._get_("startDate").getValue();
						var numDays = 0;
		
						if (!Ext.isEmpty(startDate)) {
							var time = Ext.Date.format(startDate, "H:i");
							if (!Ext.isEmpty(this._get_("intInDays").getValue())) {
								numDays = this._get_("intInDays").getValue();
							}
							summaryField.setValue(Main.translate("jobTriggers", "at__lbl")+" "+time+" "+Main.translate("jobTriggers", "every__lbl")+" "+numDays+ " "+Main.translate("jobTriggers", "days__lbl"));
						}
						
	},
	
	_setupDaysArray_: function(el,newVal,oldVal,opts) {
		
						
								var enumField = this._get_("daysOfWeekEnum");
								var enumArray = enumField.getValue().split(",");
								var name = el.name;
								var daysArray = this.daysArray;
								
		//						if (this._controller_.isEditMode == true) {
									if (daysArray) {
										if (enumArray.length > daysArray.length) {
											daysArray = enumArray; 
										}
			
										if (newVal === true) {
											var n = daysArray.indexOf(name);
											if(n == -1) {
												daysArray.push(name);
											}
										}
										else {
											var i = daysArray.indexOf(name);
											if(i != -1) {
												daysArray.splice(i, 1);
											}
										}
										enumField.setValue(daysArray.toString());
		//							}
								}
								
		
	},
	
	_configWeekDays_: function(el,newVal,oldVal,opts) {
		
		
						// Dan: enable / disable the week / month fields based on checkbox values
		
						var weekDaysCombo = this._get_("onWeekDays");
						var weeksPanel = this._get_("weeksComp");
						var weeksPanelFields = weeksPanel.query("field");
		
						if (newVal == true) {
		
							Ext.each(weeksPanelFields, function(f) {
								if (f.xtype != "checkbox") {
									//f.setValue("");
									f.setReadOnly(true);
								}
								
							});					
							weekDaysCombo.setValue(false);
		
						}
						else {
		
							Ext.each(weeksPanelFields, function(f) {
								f.setReadOnly(false);
							});
							
		
						}
		
						this._setSummary_();
		
	},
	
	_configMonthDays_: function(el,newVal,oldVal,opts) {
		
		
						// Dan: enable / disable the week / month fields based on checkbox values
		
						var monthDaysCombo = this._get_("onMonthDays");
						var daysPanel = this._get_("daysComp");
						var daysPanelFields = daysPanel.query("field");
		
						if (newVal == true) {
		
							Ext.each(daysPanelFields, function(f) {
								if (f.xtype != "checkbox") {
									//f.setValue("");
									f.setReadOnly(true);
								}
								
							});					
							monthDaysCombo.setValue(false);
		
						}
						else {
							
							Ext.each(daysPanelFields, function(f) {
								f.setReadOnly(false);
							});
		
							
						}
		
						this._setSummary_();
		
	},
	
	_showHidePanels_: function(el,newVal,oldVal,opts) {
		
		
						// Dan: Show / hide the days, weeks, months panels based on the type selection
						
						var dailyPanel = this._get_("daily");
						var weeklyPanel = this._get_("weekly");
						var monthlyPanel = this._get_("monthly");
						var panels = [dailyPanel,weeklyPanel,monthlyPanel];
						var wrapper = this._get_("fieldsWrapper");
						var record = this._controller_.getRecord();				
		
						// Andras: enable/disable 2 rows if Once is selected
		
						var field = this._get_("type").getValue();
		
						if (field=="Once"){
							this._get_("repChk").setDisabled(true);
							this._get_("repeatInterval").setDisabled(true);
							this._get_("repeatDur").setDisabled(true);
							this._get_("timeoutChk").setDisabled(true);
							this._get_("timeout").setDisabled(true);
						} else {
							this._get_("repChk").setDisabled(false);
							this._get_("repeatInterval").setDisabled(false);
							this._get_("repeatDur").setDisabled(false);
							this._get_("timeoutChk").setDisabled(false);
							this._get_("timeout").setDisabled(false);
							
						}
		
		
						var configPanels = function(freq, panels, wrapper) {
							//wrapper.show();
							for (var i = 0; i < panels.length; i++) {
								if (panels[i].name != freq) {
									panels[i].hide();
									// cleanFields(panels[i]);
								}
								else {
									panels[i].show();
								}
							}
						};
		
						var hideWrapper = function(cmp) {
							dailyPanel.hide();
							weeklyPanel.hide();
							monthlyPanel.hide();
						}
		
						switch (newVal) {
						    case "Daily":
						        configPanels("daily", panels, wrapper);
						        break;
						    case "Weekly":
						        configPanels("weekly", panels, wrapper);
						        break;
						    case "Monthly":
						        configPanels("monthly", panels, wrapper);
						        break;
							case "Once":
						        hideWrapper(wrapper);
						        break;
							default:
								hideWrapper(wrapper);
						}
		
						this._setSummary_();
		
	}
});

/* ================= EDIT FORM: Temp ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Trigger_Dc$Temp", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Trigger_Dc$Temp",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"comingSoon", bind:"{d.temp}", dataIndex:"temp", maxLength:255, style:"margin-bottom: 20px"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col"])
		.addChildrenTo("col", ["comingSoon"]);
	}
});
