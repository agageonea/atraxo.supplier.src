/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Vat_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Vat_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Vat_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Vat_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"code", dataIndex:"code", maxLength:32})
			.addLov({name:"countryName", dataIndex:"countryName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CountryNameLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "countryId"} ]}})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Vat_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_Vat_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"code", dataIndex:"code", width:200, allowBlank: false, maxLength:32, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:32}})
		.addLov({name:"countryName", dataIndex:"countryName", width:200, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_CountryNameLov_Lov", maxLength:100,
				retFieldMapping: [{lovField:"id", dsField: "countryId"} ]}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
