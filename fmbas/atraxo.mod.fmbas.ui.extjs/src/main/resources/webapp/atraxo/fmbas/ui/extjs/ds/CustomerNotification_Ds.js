/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.CustomerNotification_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_CustomerNotification_Ds"
	},
	
	
	validators: {
		templateName: [{type: 'presence'}],
		notificationEventName: [{type: 'presence'}],
		templateId: [{type: 'presence'}],
		emailBody: [{type: 'presence'}],
		emailBodyId: [{type: 'presence'}],
		assignedAreaCode: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("assignedAreaCode", "WW");
	},
	
	fields: [
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerName", type:"string"},
		{name:"customerCode", type:"string"},
		{name:"templateId", type:"int", allowNull:true},
		{name:"templateName", type:"string"},
		{name:"emailBody", type:"string"},
		{name:"emailBodyId", type:"int", allowNull:true},
		{name:"subsidiaryCode", type:"string"},
		{name:"userCode", type:"string"},
		{name:"notificationEventId", type:"int", allowNull:true},
		{name:"notificationEventName", type:"string"},
		{name:"notificationEventDescription", type:"string"},
		{name:"assignedAreaId", type:"int", allowNull:true},
		{name:"assignedAreaCode", type:"string"},
		{name:"assignedAreaName", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.CustomerNotification_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerName", type:"string"},
		{name:"customerCode", type:"string"},
		{name:"templateId", type:"int", allowNull:true},
		{name:"templateName", type:"string"},
		{name:"emailBody", type:"string"},
		{name:"emailBodyId", type:"int", allowNull:true},
		{name:"subsidiaryCode", type:"string"},
		{name:"userCode", type:"string"},
		{name:"notificationEventId", type:"int", allowNull:true},
		{name:"notificationEventName", type:"string"},
		{name:"notificationEventDescription", type:"string"},
		{name:"assignedAreaId", type:"int", allowNull:true},
		{name:"assignedAreaCode", type:"string"},
		{name:"assignedAreaName", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
