/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.AdvancedFilters_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.AdvancedFilters_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("filter", Ext.create(atraxo.fmbas.ui.extjs.dc.AdvancedFilter_Dc,{multiEdit: true, trackEditMode: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addDcEditGridView("filter", {name:"filterList", xtype:"fmbas_AdvancedFilter_Dc$List", frame:true})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["filterList"], ["center"])
		.addToolbarTo("filterList", "tlbAdvancedFilterList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbAdvancedFilterList", {dc: "filter"})
			.addQuery({iconCls:fp_asc.load_glyph.css,glyph:fp_asc.load_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addCopy({iconCls:fp_asc.copy_glyph.css,glyph:fp_asc.copy_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addReports()
		.end();
	}
	

});
