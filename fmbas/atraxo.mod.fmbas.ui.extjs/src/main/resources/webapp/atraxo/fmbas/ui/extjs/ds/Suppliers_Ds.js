/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Suppliers_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "fmbas_Suppliers_Ds"
	},
	
	
	validators: {
		code: [{type: 'presence'}],
		name: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("isSupplier", true);
	},
	
	fields: [
		{name:"contactName", type:"string", noFilter:true, noSort:true},
		{name:"contactPhone", type:"string", noFilter:true, noSort:true},
		{name:"contactEmail", type:"string", noFilter:true, noSort:true},
		{name:"sectionTitle", type:"string", noFilter:true, noSort:true},
		{name:"buyerId", type:"string"},
		{name:"buyerName", type:"string"},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"iataCode", type:"string"},
		{name:"iplAgent", type:"boolean"},
		{name:"fuelSupplier", type:"boolean"},
		{name:"fixedBaseOperator", type:"boolean"},
		{name:"notes", type:"string"},
		{name:"isSupplier", type:"boolean"},
		{name:"roles", type:"string", noFilter:true, noSort:true},
		{name:"exposure", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.fmbas.ui.extjs.ds.Suppliers_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"contactName", type:"string", noFilter:true, noSort:true},
		{name:"contactPhone", type:"string", noFilter:true, noSort:true},
		{name:"contactEmail", type:"string", noFilter:true, noSort:true},
		{name:"sectionTitle", type:"string", noFilter:true, noSort:true},
		{name:"buyerId", type:"string"},
		{name:"buyerName", type:"string"},
		{name:"code", type:"string"},
		{name:"name", type:"string"},
		{name:"iataCode", type:"string"},
		{name:"iplAgent", type:"boolean", allowNull:true},
		{name:"fuelSupplier", type:"boolean", allowNull:true},
		{name:"fixedBaseOperator", type:"boolean", allowNull:true},
		{name:"notes", type:"string"},
		{name:"isSupplier", type:"boolean", allowNull:true},
		{name:"roles", type:"string", noFilter:true, noSort:true},
		{name:"exposure", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
