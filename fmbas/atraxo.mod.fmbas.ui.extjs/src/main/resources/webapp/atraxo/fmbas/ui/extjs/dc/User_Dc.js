/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.User_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.UserSupp_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.UserSupp_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.User_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_User_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"loginCode", dataIndex:"loginCode", maxLength:10,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.UserListLov_Lov", selectOnFocus:true, maxLength:10,
					retFieldMapping: [{lovField:"id", dsField: "id"} ]}})
			.addTextField({ name:"name", dataIndex:"name", maxLength:255})
			.addTextField({ name:"firstName", dataIndex:"firstName", maxLength:64})
			.addTextField({ name:"lastName", dataIndex:"lastName", maxLength:64})
			.addLov({name:"location", dataIndex:"locCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "locId"} ]}})
			.addBooleanField({ name:"active", dataIndex:"active"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.User_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_User_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"title", dataIndex:"title", hidden:true, width:70})
		.addTextColumn({ name:"first", dataIndex:"firstName", width:100})
		.addTextColumn({ name:"last", dataIndex:"lastName", width:100})
		.addTextColumn({ name:"login", dataIndex:"loginName", width:100})
		.addTextColumn({ name:"display", dataIndex:"name", width:100})
		.addTextColumn({ name:"userLanguage", dataIndex:"userLanguage", width:50})
		.addBooleanColumn({ name:"active", dataIndex:"active", width:100})
		.addTextColumn({ name:"location", dataIndex:"locCode", hidden:true, width:50})
		.addTextColumn({ name:"dateFormat", dataIndex:"dateFormat", hidden:true, width:200})
		.addTextColumn({ name:"decimalSeparator", dataIndex:"decimalSeparator", hidden:true, width:50})
		.addTextColumn({ name:"thousandSeparator", dataIndex:"thousandSeparator", hidden:true, width:50})
		.addTextColumn({ name:"notes", dataIndex:"notes", hidden:true, width:200})
		.addTextColumn({ name:"jobTitle", dataIndex:"jobTitle", hidden:true, width:120})
		.addTextColumn({ name:"department", dataIndex:"department", hidden:true, width:100})
		.addTextColumn({ name:"workOffice", dataIndex:"workOffice", hidden:true, width:70})
		.addTextColumn({ name:"email", dataIndex:"email", hidden:true, width:100})
		.addTextColumn({ name:"businessPhone", dataIndex:"businessPhone", hidden:true, width:120})
		.addTextColumn({ name:"mobilePhone", dataIndex:"mobilePhone", hidden:true, width:120})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Header ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.User_Dc$Header", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_User_Dc$Header",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, noLabel: true, maxLength:255})
		.addTextField({ name:"loginName", bind:"{d.loginName}", dataIndex:"loginName", noEdit:true , allowBlank:false, noLabel: true, maxLength:255})
		.addTextArea({ name:"notes", bind:"{d.notes}", dataIndex:"notes", noLabel: true})
		.addLov({name:"location", bind:"{d.locCode}", dataIndex:"locCode", noLabel: true, xtype:"fmbas_LocationsLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"id", dsField: "locId"} ]})
		.addCombo({ xtype:"combo", name:"userLanguage", bind:"{d.userLanguage}", dataIndex:"userLanguage", noLabel: true, store:[ __AD__.LanguageCodes._ENGLISH_, __AD__.LanguageCodes._GERMAN_, __AD__.LanguageCodes._FRENCH_, __AD__.LanguageCodes._SPANISH_, __AD__.LanguageCodes._PORTUGUESE_, __AD__.LanguageCodes._CHINESE_]})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active", noLabel: true})
		.addCombo({ xtype:"combo", name:"decimalSeparator", bind:"{d.decimalSeparator}", dataIndex:"decimalSeparator", allowBlank:false, noLabel: true, store:[ __AD__.TNumberSeparator._POINT_, __AD__.TNumberSeparator._COMMA_]})
		.addCombo({ xtype:"combo", name:"thousandSeparator", bind:"{d.thousandSeparator}", dataIndex:"thousandSeparator", allowBlank:false, noLabel: true, store:[ __AD__.TNumberSeparator._POINT_, __AD__.TNumberSeparator._COMMA_]})
		.addLov({name:"dateFormat", bind:"{d.dateFormat}", dataIndex:"dateFormat", noLabel: true, xtype:"ad_DateFormats_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "dateFormatId"} ]})
		.addDisplayFieldText({ name:"loginNameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"displayNameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"locLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"userLanguageLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"dateFLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"decimalFLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"sepLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"activeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"notesLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left: 40px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3",  style:"margin-left: 40px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table4",  style:"margin-left: 40px", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["table1", "table2", "table3", "table4"])
		.addChildrenTo("table1", ["loginNameLabel", "loginName", "displayNameLabel", "name", "locLabel", "location"])
		.addChildrenTo("table2", ["dateFLabel", "dateFormat", "decimalFLabel", "decimalSeparator", "sepLabel", "thousandSeparator"])
		.addChildrenTo("table3", ["userLanguageLabel", "userLanguage", "notesLabel", "notes"])
		.addChildrenTo("table4", ["activeLabel", "active"]);
	},
	/* ==================== Business functions ==================== */
	
	_preparePhoto_: function() {
		
						var Resizer = function() {
		
						    var maxWidth = 0,
						        maxHeight = 0;
						    var canvas = document.createElement("canvas");
						    var img = new Image();
						    var callback;
						
						    var isFileOk = function(file) {
						        if (!file || !file.type.match(/image.*/)) {
						            return false;
						        };
						        return true;
						    }
						
						    img.onload = function() {
						        var dimensions = getResizedDimensions(img.width, img.height);
						        canvas.width = dimensions.width;
						        canvas.height = dimensions.height;
						        var ctx = canvas.getContext("2d");
						        ctx.drawImage(img, 0, 0, dimensions.width, dimensions.height);
						        if (callback) {
						            callback(canvas.toDataURL());
						            img.src = "";
						            ctx.clearRect(0, 0, canvas.width, canvas.height);
						        }
						    }
						    if (window.opera) { 
						        img.onerror = img.onload; 
						    }
						
						    var createObjectURL = function(file) {
						        if (window.webkitURL) {
						            return window.webkitURL.createObjectURL(file);
						        } else if (window.URL && window.URL.createObjectURL) {
						            return window.URL.createObjectURL(file);
						        } else {
						            return null;
						        }
						    }
						
						    var getResizedDimensions = function(initW, initH) {
						        var resizedWidth = maxWidth,
						            resizedHeight = maxHeight,
						            initialWidth = initW,
						            initialHeight = initH;
						        if (initialWidth <= maxWidth && initialHeight <= maxHeight) {
						            resizedWidth = initialWidth;
						            resizedHeight = initialHeight;
						        } else {
						            if (initialWidth < initialHeight) {
						                var calcWidth = initialWidth * resizedHeight / initialHeight;
						                if (calcWidth <= maxWidth) {
						                    resizedWidth = calcWidth;
						                } else {
						                    resizedHeight = resizedHeight * resizedWidth / calcWidth;
						                }
						            } else {
						                if (initialWidth > initialHeight) {
						                    var calcHeight = initialHeight * resizedWidth / initialWidth;
						                    if (calcHeight <= maxHeight) {
						                        resizedHeight = calcHeight;
						                    } else {
						                        resizedWidth = resizedWidth * resizedHeight / calcHeight;
						                    }
						                } else {
						                    resizedWidth = Math.Min(maxHeight, maxWidth);
						                    resizedHeight = resizedWidth;
						                }
						            }
						        }
						        return {
						            width: resizedWidth,
						            height: resizedHeight
						        }
						    }
						
						    return {
						        scale: function(file, width, height, action) {
						            if (!isFileOk(file)) {
						                return;
						            }
						            maxWidth = width;
						            maxHeight = height;
						            callback = action;
						            img.src = createObjectURL(file);
						        },
						    }
						}
		
						var resizer = new Resizer();
		
		//			    var file = this._get_("pictureField").getEl().dom.files[0];
		//			    var picture = this._get_("picture");
		//			    var callback = function(scaledImg) {
		//			        picture.setValue(scaledImg);//				
		//			    }
		//			
		//			    resizer.scale(file, 320, 240, callback);
	}
});

/* ================= EDIT FORM: Tab ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.User_Dc$Tab", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_User_Dc$Tab",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addCombo({ xtype:"combo", name:"title", bind:"{d.title}", dataIndex:"title", noLabel: true, store:[ __FMBAS_TYPE__.Title._MR__, __FMBAS_TYPE__.Title._MRS__, __FMBAS_TYPE__.Title._MS__, __FMBAS_TYPE__.Title._MISS_]})
		.addTextField({ name:"firstName", bind:"{d.firstName}", dataIndex:"firstName", allowBlank:false, noLabel: true, maxLength:64})
		.addTextField({ name:"lastName", bind:"{d.lastName}", dataIndex:"lastName", allowBlank:false, noLabel: true, maxLength:64})
		.addTextField({ name:"jobTitle", bind:"{d.jobTitle}", dataIndex:"jobTitle", noLabel: true, maxLength:100})
		.addCombo({ xtype:"combo", name:"department", bind:"{d.department}", dataIndex:"department", noLabel: true, store:[ __FMBAS_TYPE__.Department._OPERATION_, __FMBAS_TYPE__.Department._COMMERCIAL_]})
		.addCombo({ xtype:"combo", name:"workOffice", bind:"{d.workOffice}", dataIndex:"workOffice", noLabel: true, store:[ __FMBAS_TYPE__.WorkOffice._HEAD_OFFICE_, __FMBAS_TYPE__.WorkOffice._BRANCH_, __FMBAS_TYPE__.WorkOffice._AIRPORT_OFFICE_]})
		.addTextField({ name:"email", bind:"{d.email}", dataIndex:"email", allowBlank:false, noLabel: true, maxLength:128})
		.addTextField({ name:"businessPhone", bind:"{d.businessPhone}", dataIndex:"businessPhone", noLabel: true, maxLength:100})
		.addTextField({ name:"mobilePhone", bind:"{d.mobilePhone}", dataIndex:"mobilePhone", noLabel: true, maxLength:100})
		.addDisplayFieldText({ name:"titleLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"firstNameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"lastNameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"jobTitleLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"depLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"worklabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"emailLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"businessPhoneLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"mobilePhoneLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left: 40px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3",  style:"margin-left: 40px", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["table1", "table2", "table3"])
		.addChildrenTo("table1", ["titleLabel", "title", "firstNameLabel", "firstName", "lastNameLabel", "lastName"])
		.addChildrenTo("table2", ["jobTitleLabel", "jobTitle", "depLabel", "department", "worklabel", "workOffice"])
		.addChildrenTo("table3", ["emailLabel", "email", "businessPhoneLabel", "businessPhone", "mobilePhoneLabel", "mobilePhone"]);
	}
});

/* ================= EDIT FORM: New ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.User_Dc$New", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_User_Dc$New",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addCombo({ xtype:"combo", name:"title", bind:"{d.title}", dataIndex:"title", noLabel: true, store:[ __FMBAS_TYPE__.Title._MR__, __FMBAS_TYPE__.Title._MRS__, __FMBAS_TYPE__.Title._MS__, __FMBAS_TYPE__.Title._MISS_]})
		.addTextField({ name:"firstName", bind:"{d.firstName}", dataIndex:"firstName", allowBlank:false, noLabel: true, maxLength:64})
		.addTextField({ name:"lastName", bind:"{d.lastName}", dataIndex:"lastName", allowBlank:false, noLabel: true, maxLength:64})
		.addTextField({ name:"displayName", bind:"{d.name}", dataIndex:"name", allowBlank:false, noLabel: true, maxLength:255})
		.addTextField({ name:"loginName", bind:"{d.loginName}", dataIndex:"loginName", allowBlank:false, noLabel: true, maxLength:255})
		.addTextField({ name:"email", bind:"{d.email}", dataIndex:"email", allowBlank:false, noLabel: true, maxLength:128})
		.addTextField({ name:"jobTitle", bind:"{d.jobTitle}", dataIndex:"jobTitle", noLabel: true, maxLength:100})
		.addCombo({ xtype:"combo", name:"department", bind:"{d.department}", dataIndex:"department", noLabel: true, store:[ __FMBAS_TYPE__.Department._OPERATION_, __FMBAS_TYPE__.Department._COMMERCIAL_]})
		.addCombo({ xtype:"combo", name:"workOffice", bind:"{d.workOffice}", dataIndex:"workOffice", noLabel: true, store:[ __FMBAS_TYPE__.WorkOffice._HEAD_OFFICE_, __FMBAS_TYPE__.WorkOffice._BRANCH_, __FMBAS_TYPE__.WorkOffice._AIRPORT_OFFICE_]})
		.addTextField({ name:"businessPhone", bind:"{d.businessPhone}", dataIndex:"businessPhone", noLabel: true, maxLength:100})
		.addTextField({ name:"mobilePhone", bind:"{d.mobilePhone}", dataIndex:"mobilePhone", noLabel: true, maxLength:100})
		.addLov({name:"location", bind:"{d.locCode}", dataIndex:"locCode", noLabel: true, xtype:"fmbas_LocationsLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"id", dsField: "locId"} ]})
		.addDisplayFieldText({ name:"titleLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"firstNameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"lastNameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"loginNameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"displayNameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"jobTitleLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"depLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"worklabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"emailLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"businessPhoneLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"mobilePhoneLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		.addDisplayFieldText({ name:"locLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:100, style:"float:right"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left : 40px", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["table1", "table2"])
		.addChildrenTo("table1", ["titleLabel", "title", "firstNameLabel", "firstName", "lastNameLabel", "lastName", "loginNameLabel", "loginName", "displayNameLabel", "displayName", "emailLabel", "email"])
		.addChildrenTo("table2", ["jobTitleLabel", "jobTitle", "depLabel", "department", "worklabel", "workOffice", "businessPhoneLabel", "businessPhone", "mobilePhoneLabel", "mobilePhone", "locLabel", "location"]);
	}
});

/* ================= EDIT FORM: ChangePasswordForm ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.User_Dc$ChangePasswordForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_User_Dc$ChangePasswordForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"newPassword", bind:"{p.newPassword}", paramIndex:"newPassword", allowBlank:false, maxLength:255, inputType:"password", labelWidth:120})
		.addTextField({ name:"confirmPassword", bind:"{p.confirmPassword}", paramIndex:"confirmPassword", allowBlank:false, maxLength:255, inputType:"password", labelWidth:120})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:350, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["newPassword", "confirmPassword"]);
	},
	/* ==================== Business functions ==================== */
	
	_shouldValidate_: function() {
		
						return this._isVisible_;
	}
});
