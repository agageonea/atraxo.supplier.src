/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.ViewStates_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.ViewStates_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("viewState", Ext.create(atraxo.fmbas.ui.extjs.dc.ViewState_Dc,{}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addDcGridView("viewState", {name:"viewStateList", xtype:"fmbas_ViewState_Dc$List"})
		.addPanel({name:"main", layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["viewStateList"], ["center"])
		.addToolbarTo("viewStateList", "tlbViewStateList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbViewStateList", {dc: "viewState"})
			.addQuery({iconCls:fp_asc.load_glyph.css,glyph:fp_asc.load_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addCopy({iconCls:fp_asc.copy_glyph.css,glyph:fp_asc.copy_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addReports()
		.end();
	}
	

});
