/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.RoleActive_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.RoleSuppActive_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.RoleSuppActive_Ds
});

/* ================= GRID: ListActive ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.RoleActive_Dc$ListActive", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_RoleActive_Dc$ListActive",
	_noPaginator_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"code", dataIndex:"code", width:165})
		.addTextColumn({ name:"name", dataIndex:"name", width:180})
		.addTextColumn({ name:"description", dataIndex:"description", width:400})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
