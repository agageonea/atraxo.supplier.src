/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.frame.ExternalInterfacesMessageHistory_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.ExternalInterfacesMessageHistory_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("externalInterfacesMessageHistory", Ext.create(atraxo.fmbas.ui.extjs.dc.ExternalInterfacesMessageHistory_Dc,{}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnHelpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onBtnHelpWdw, scope:this})
		.addButton({name:"btnViewObject",glyph:fp_asc.eye_glyph.glyph, disabled:true, handler: this.onBtnViewObject,stateManager:[{ name:"selected_one", dc:"externalInterfacesMessageHistory", and: function(dc) {return (!Ext.isEmpty(dc.record.get('objectRefId')));} }], scope:this})
		.addButton({name:"btnSentMessage",glyph:fp_asc.eye_glyph.glyph, disabled:true, handler: this.onBtnSentMessage,stateManager:[{ name:"selected_one_clean", dc:"externalInterfacesMessageHistory"}], scope:this})
		.addButton({name:"btnReceivedMessage",glyph:fp_asc.eye_glyph.glyph, disabled:true, handler: this.onBtnReceivedMessage,stateManager:[{ name:"selected_one_clean", dc:"externalInterfacesMessageHistory", and: function(dc) {return (dc.record.get('responseMessageId'));} }], scope:this})
		.addButton({name:"btnCloseMessage",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseMessage, scope:this})
		.addDcFilterFormView("externalInterfacesMessageHistory", {name:"filter", xtype:"fmbas_ExternalInterfacesMessageHistory_Dc$Filter"})
		.addDcGridView("externalInterfacesMessageHistory", {name:"list", xtype:"fmbas_ExternalInterfacesMessageHistory_Dc$List"})
		.addDcFormView("externalInterfacesMessageHistory", {name:"message", xtype:"fmbas_ExternalInterfacesMessageHistory_Dc$MessageContent"})
		.addWindow({name:"wdwSeeMessage", _hasTitle_:true, width:725, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("message")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCloseMessage")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["list"], ["center"])
		.addToolbarTo("list", "tlbList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_();
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbList", {dc: "externalInterfacesMessageHistory"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnViewObject"),this._elems_.get("btnSentMessage"),this._elems_.get("btnReceivedMessage"),this._elems_.get("btnHelpWdw")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnHelpWdw
	 */
	,onBtnHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnViewObject
	 */
	,onBtnViewObject: function() {
		this.openObject();
	}
	
	/**
	 * On-Click handler for button btnSentMessage
	 */
	,onBtnSentMessage: function() {
		this._readSentMessage_();
	}
	
	/**
	 * On-Click handler for button btnReceivedMessage
	 */
	,onBtnReceivedMessage: function() {
		this._readReceivedMessage_();
	}
	
	/**
	 * On-Click handler for button btnCloseMessage
	 */
	,onBtnCloseMessage: function() {
		this.closeWdwSentMessage();
	}
	
	,closeWdwSentMessage: function() {	
		this._getWindow_("wdwSeeMessage").close();
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/ExternalInterfaceMessageHistory.html";
					window.open( url, "SONE_Help");
	}
	
	,_readSentMessage_: function() {
		
						var dc = this._getDc_("externalInterfacesMessageHistory");
						var r = dc.getRecord();
						if(r) {
						var successFn = function() {
							this.showMessage();
						}
						var o={
							name:"readSentMessage",
								callbacks:{
									successFn: successFn,
									successScope: this
								},
							modal:true
						};
						}
						dc.doRpcData(o);
	}
	
	,_readReceivedMessage_: function() {
		
						var dc = this._getDc_("externalInterfacesMessageHistory");
						var r = dc.getRecord();
						if(r) {
						var successFn = function() {
							this.showMessage();
						}
						var o={
							name:"readReceivedMessage",
								callbacks:{
									successFn: successFn,
									successScope: this
								},
							modal:true
						};
						}
						dc.doRpcData(o);
	}
	
	,showMessage: function() {
		
		                var wdw = this._getWindow_("wdwSeeMessage");
						wdw.show();
						
						
	}
	
	,openObject: function() {
		
					var dc = this._getDc_("externalInterfacesMessageHistory");
					var r = dc.getRecord();
					if (r) {
						var ot = r.get("objectType");
						var oid = r.get("objectId");
						var frame = "";
						var bundle = "";
						if (ot == "Contract") {
						// If the objectType is "Contract" then open the Contract dialog
							var frame = "atraxo.cmm.ui.extjs.frame.ContractCustomer_Ui";
							var bundle = "atraxo.mod.cmm";
						}
						else if (ot == "FuelEvent" || ot == "FuelEvent-Purchase" || ot == "FuelEvent-Sale") {
						// If the objectType is "FuelEvent" then open the Fuel Event dialog
							var frame = "atraxo.acc.ui.extjs.frame.FuelEvents_Ui";
							var bundle = "atraxo.mod.acc";
						}
						else if (ot == "Customer") {
						// If the objectType is "Customer" than open the Outgoing Invoice Line dialog
							var frame = "atraxo.fmbas.ui.extjs.frame.Customers_Ui";
							var bundle = "atraxo.mod.fmbas";
						}
						else if (ot == "FuelTransaction") {
						// If the objectType is "FuelTransaction" than open the Outgoing Invoice Line dialog
							var frame = "atraxo.ops.ui.extjs.frame.FuelTicket_Ui";
							var bundle = "atraxo.mod.ops";
						}
						else if (ot == "Tender") {
						// If the objectType is "Tender" than open the Tender dialog
							var frame = "atraxo.cmm.ui.extjs.frame.TenderInvitation_Ui";
							var bundle = "atraxo.mod.cmm";
						}
						else if (ot == "Bid") {
						// If the objectType is "Bid" than open the Bid dialog
							var frame = "atraxo.cmm.ui.extjs.frame.TenderBid_Ui";
							var bundle = "atraxo.mod.cmm";
						}
						else {
							Main.info(Main.translate("applicationMsg", "externalInterfaceViewObject__lbl"));
							return;
						}
						getApplication().showFrame(frame,{
								url:Main.buildUiPath(bundle, frame, false),
								params: {
									objectId: oid
								},
								callback: function (params) {
									this._onShow_from_externalInterfaceMessageHistory_(params)
								}
							});
						
					}
	}
});
