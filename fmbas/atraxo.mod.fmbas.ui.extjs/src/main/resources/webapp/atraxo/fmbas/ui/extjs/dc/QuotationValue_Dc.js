/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.QuotationValue_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.QuotationValue_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.QuotationValue_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_QuotationValue_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateField({name:"validFrom", dataIndex:"validFromDate"})
			.addDateField({name:"validTo", dataIndex:"validToDate"})
			.addNumberField({name:"rate", dataIndex:"rate", sysDec:"dec_xr", maxLength:19})
			.addBooleanField({ name:"prov", dataIndex:"isProvisioned"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.QuotationValue_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_QuotationValue_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"validFrom", dataIndex:"validFromDate", _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"validToDate", _mask_: Masks.DATE})
		.addNumberColumn({ name:"rate", dataIndex:"rate", sysDec:"dec_xr"})
		.addBooleanColumn({ name:"prov", dataIndex:"isProvisioned", width:100})
		.addBooleanColumn({ name:"active", dataIndex:"active", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
