/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.MasterAgrement_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.MasterAgrement_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.MasterAgrement_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_MasterAgrement_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateField({name:"validFrom", dataIndex:"validFrom"})
			.addDateField({name:"validTo", dataIndex:"validTo"})
			.addBooleanField({ name:"evergreen", dataIndex:"evergreen"})
			.addLov({name:"financialSourceCode", dataIndex:"financialSourceCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CodeLov_Lov", selectOnFocus:true, maxLength:25}})
			.addLov({name:"avgMthd", dataIndex:"avgMthd", maxLength:50,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.AvgMethLov_Lov", selectOnFocus:true, maxLength:50}})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.MasterAgrement_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_MasterAgrement_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"validFrom", dataIndex:"validFrom", width:100, _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"validTo", width:100, _mask_: Masks.DATE})
		.addBooleanColumn({ name:"evergreen", dataIndex:"evergreen", width:80})
		.addTextColumn({ name:"currency", dataIndex:"currencyCode", width:120})
		.addDateColumn({ name:"renewalReminder", dataIndex:"renewalReminder", hidden:true, width:120, _mask_: Masks.DATE})
		.addTextColumn({ name:"status", dataIndex:"status", hidden:true, width:100})
		.addTextColumn({ name:"period", dataIndex:"period", hidden:true, width:100})
		.addNumberColumn({ name:"paymentTerms", dataIndex:"paymentTerms", hidden:true, width:120})
		.addTextColumn({ name:"referenceTo", dataIndex:"referenceTo", hidden:true, width:130})
		.addTextColumn({ name:"invoiceFrequency", dataIndex:"invoiceFrequency", hidden:true, width:120})
		.addTextColumn({ name:"invoiceType", dataIndex:"invoiceType", hidden:true, width:100})
		.addBooleanColumn({ name:"creditCard", dataIndex:"creditCard", hidden:true, width:80})
		.addTextColumn({ name:"avgMthd", dataIndex:"avgMthd", hidden:true, width:100})
		.addTextColumn({ name:"financialSourceCode", dataIndex:"financialSourceCode", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.MasterAgrement_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_MasterAgrement_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"generalTermsTitle", bind:"{d.generalTermsTitle}", dataIndex:"generalTermsTitle", maxLength:100, labelWidth:95})
		.addDisplayFieldText({ name:"paymentTermsTitle", bind:"{d.paymentTermsTitle}", dataIndex:"paymentTermsTitle", maxLength:100})
		.addDisplayFieldText({ name:"separator", bind:"{d.separator}", dataIndex:"separator", maxLength:16, labelWidth:800, labelAlign:"center"})
		.addDisplayFieldText({ name:"separator2", bind:"{d.separator}", dataIndex:"separator", maxLength:16, labelWidth:800, labelAlign:"center"})
		.addDisplayFieldText({ name:"percent", bind:"{d.percent}", dataIndex:"percent", noEdit:true , width:50, maxLength:1, labelWidth:20})
		.addDisplayFieldText({ name:"days", bind:"{d.days}", dataIndex:"days", noEdit:true , width:120, maxLength:4, labelWidth:30})
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", allowBlank:false, labelWidth:120})
		.addDateField({name:"validTo", bind:"{d.validTo}", dataIndex:"validTo", labelWidth:120})
		.addCombo({ xtype:"combo", name:"status", bind:"{d.status}", dataIndex:"status", allowBlank:false, store:[ __FMBAS_TYPE__.MasterAgreementsStatus._NEGOTIATION_, __FMBAS_TYPE__.MasterAgreementsStatus._EFFECTIVE_, __FMBAS_TYPE__.MasterAgreementsStatus._ON_HOLD_, __FMBAS_TYPE__.MasterAgreementsStatus._TERMINATED_], labelWidth:120})
		.addBooleanField({ name:"evergreenCheck", bind:"{d.evergreen}", dataIndex:"evergreen"})
		.addDateField({name:"renewalReminder", bind:"{d.renewalReminder}", dataIndex:"renewalReminder", labelWidth:120})
		.addLov({name:"invCurrency", bind:"{d.currencyCode}", dataIndex:"currencyCode", allowBlank:false, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:120,
			retFieldMapping: [{lovField:"id", dsField: "curencyId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ],listeners:{
			fpchange:{scope:this, fn:this._enableFs_}
		}})
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", _enableFn_: function(dc, rec) { return dc.record.data.curencyId != null; ; } , allowBlank:false, width:295, xtype:"fmbas_ExchangeRateToFinancialSourceAndAvgFilteredLov_Lov", maxLength:25, labelWidth:120,
			retFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"} ],
			filterFieldMapping: [{lovField:"currencyId", dsField: "curencyId"} ],listeners:{
			fpchange:{scope:this, fn:this._enableAvgMethods_},
			expand:{scope:this, fn:this._filterDistinct_}
		}})
		.addLov({name:"avgMethodName", bind:"{d.avgMthd}", dataIndex:"avgMthd", _enableFn_: this._enableIfFinancialSource_, allowBlank:false, width:295, xtype:"fmbas_ExchangeRateToAvgMthdFilteredLov_Lov", maxLength:50, labelWidth:120,
			retFieldMapping: [{lovField:"avgMthdId", dsField: "avgMthdId"} ],
			filterFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"}, {lovField:"currencyId", dsField: "curencyId"} ],listeners:{
			expand:{scope:this, fn:this._filterDistinctAvg_}
		}})
		.addCombo({ xtype:"combo", name:"period", bind:"{d.period}", dataIndex:"period", allowBlank:false, store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_], labelWidth:120})
		.addNumberField({name:"paymentTerms", bind:"{d.paymentTerms}", dataIndex:"paymentTerms", width:160, maxLength:11, labelWidth:120})
		.addCombo({ xtype:"combo", name:"referenceTo", bind:"{d.referenceTo}", dataIndex:"referenceTo", store:[ __FMBAS_TYPE__.PaymentDay._INVOICE_DATE_, __FMBAS_TYPE__.PaymentDay._INVOICE_RECEIVING_DATE_, __FMBAS_TYPE__.PaymentDay._LAST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._FIRST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._MIDPOINT_DELIVERY_DATE_], labelWidth:135})
		.addCombo({ xtype:"combo", name:"invoiceFrequency", bind:"{d.invoiceFrequency}", dataIndex:"invoiceFrequency", store:[ __FMBAS_TYPE__.InvoiceFreq._WEEKLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIMONTHLY_, __FMBAS_TYPE__.InvoiceFreq._MONTHLY_, __FMBAS_TYPE__.InvoiceFreq._QUARTERLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._ANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._PER_DELIVERY_, __FMBAS_TYPE__.InvoiceFreq._DAILY_, __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_], labelWidth:120,listeners:{
			expand:{scope:this, fn:this._filterInvoiceFreq_}
		}})
		.addCombo({ xtype:"combo", name:"invoiceType", bind:"{d.invoiceType}", dataIndex:"invoiceType", store:[ __FMBAS_TYPE__.InvoiceType._XML_2_0_2_, __FMBAS_TYPE__.InvoiceType._XML_3_0_0_, __FMBAS_TYPE__.InvoiceType._XML_3_0_1_, __FMBAS_TYPE__.InvoiceType._XML_3_1_0_, __FMBAS_TYPE__.InvoiceType._EXCEL_, __FMBAS_TYPE__.InvoiceType._PAPER_, __FMBAS_TYPE__.InvoiceType._EDI_, __FMBAS_TYPE__.InvoiceType._PDF_FORMAT_, __FMBAS_TYPE__.InvoiceType._FISCAL_, __FMBAS_TYPE__.InvoiceType._EXPORT_, __FMBAS_TYPE__.InvoiceType._REGULAR_], labelWidth:120})
		.add({name:"generalTitle", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("generalTermsTitle")]})
		.add({name:"vFromvToStatus", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("validFrom"),this._getConfig_("validTo"),this._getConfig_("status")]})
		.add({name:"evergreen", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("evergreenCheck")]})
		.add({name:"renewReminder", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("renewalReminder")]})
		.add({name:"paymentTitle", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("paymentTermsTitle")]})
		.add({name:"invCurExchRatePer", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("invCurrency"),this._getConfig_("financialSourceCode"),this._getConfig_("avgMethodName")]})
		.add({name:"payTermRefToIFrq", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("period"),this._getConfig_("paymentTerms"),this._getConfig_("days"),this._getConfig_("referenceTo")]})
		.add({name:"invoiceTypeC", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("invoiceFrequency"),this._getConfig_("invoiceType")]})
		.add({name:"separ", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("separator")]})
		.add({name:"separ2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("separator2")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:900, layout:"anchor"})
		.addPanel({ name:"col2", width:900, layout:"anchor"})
		.addPanel({ name:"col3", width:900, layout:"anchor"})
		.addPanel({ name:"col4", width:900, layout:"anchor"})
		.addPanel({ name:"col5", width:900, layout:"anchor"})
		.addPanel({ name:"col6", width:900, layout:"anchor"})
		.addPanel({ name:"col7", width:900, layout:"anchor"})
		.addPanel({ name:"col8", width:900, layout:"anchor"})
		.addPanel({ name:"col9", width:900, layout:"anchor"})
		.addPanel({ name:"col10", width:900, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col6", "col7", "col8", "col9"])
		.addChildrenTo("col1", ["generalTitle"])
		.addChildrenTo("col2", ["vFromvToStatus"])
		.addChildrenTo("col3", ["evergreen"])
		.addChildrenTo("col4", ["renewReminder"])
		.addChildrenTo("col5", ["separ"])
		.addChildrenTo("col6", ["paymentTitle"])
		.addChildrenTo("col7", ["invCurExchRatePer"])
		.addChildrenTo("col8", ["payTermRefToIFrq"])
		.addChildrenTo("col9", ["invoiceTypeC"])
		.addChildrenTo("col10", ["separ2"]);
	},
	/* ==================== Business functions ==================== */
	
	_filterDistinct_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("financialSourceName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });
						}, this);
	},
	
	_filterDistinctAvg_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("avgMthdName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	},
	
	_setMandatory_: function(field,newValue,oldValue) {
		
						var rec = this._get_("creditCurrencyCode");
						
						if(!Ext.isEmpty(field.getValue())&&rec.value==null) {
							rec.allowBlank=false;
						}
						else { 
							rec.allowBlank=true; 
						}
	},
	
	_setMandatory1_: function(field,newValue,oldValue) {
		
						var rec = this._get_("amount");
						
						if(Ext.isEmpty(field.getValue())&&rec.value!=null) {
							field.allowBlank=false;
						}
						else { 
							field.allowBlank=true; 
						}
	},
	
	_enableFs_: function(el) {
		
							if (el._fpRawValue_ != el._onFocusVal_) {
								var financialSourceCode = this._get_("financialSourceCode");
								financialSourceCode.setValue("");
								var elVal = el.getValue(); 
								if (!Ext.isEmpty(elVal)) {
									financialSourceCode.setReadOnly(false);
									financialSourceCode.setValue("");
								}
								else {
									financialSourceCode.setReadOnly(true);
								}
								financialSourceCode.setValue("");
							}
						
		
	},
	
	_enableAvgMethods_: function(el) {
		
							if (el._fpRawValue_ != el._onFocusVal_) {
								var avgMethodName = this._get_("avgMethodName");
								var elVal = el.getValue(); 
								if (!Ext.isEmpty(elVal)) {
									avgMethodName.setReadOnly(false);
								}
								else {
									avgMethodName.setReadOnly(true);
								}
								avgMethodName.setValue("");
							}
	},
	
	_enableIfFinancialSource_: function() {
		
						
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = false;
		
						if (record) {
							var financialSourceCode = record.get("financialSourceCode");
							if (!Ext.isEmpty(financialSourceCode)) {
								result = true;
							}
						}
		
						return result;
	},
	
	_filterInvoiceFreq_: function(el) {
		
						var s = el.store;
						s.filter(function(r) {
							var value = r.get("field1");
							if ( value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_ && value !== __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_) {
								return value;
							}
						});
	}
});
