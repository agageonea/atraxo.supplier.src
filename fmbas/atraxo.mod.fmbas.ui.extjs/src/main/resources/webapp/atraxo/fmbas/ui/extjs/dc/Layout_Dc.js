/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Layout_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Layout_Ds
});

/* ================= FILTER: LayoutFilter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Layout_Dc$LayoutFilter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Layout_Dc$LayoutFilter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", allowBlank:false, width:100, maxLength:100})
			.addTextField({ name:"description", dataIndex:"description", width:200, maxLength:1000})
		;
	}

});

/* ================= EDIT-GRID: LayoutList ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Layout_Dc$LayoutList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_Layout_Dc$LayoutList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"name", dataIndex:"name", width:100, allowBlank: false, maxLength:100, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:100}})
		.addTextColumn({name:"description", dataIndex:"description", width:200, maxLength:1000, 
			editor: { xtype:"textfield", maxLength:1000}})
		.addTextColumn({name:"thumbPath", dataIndex:"thumbPath", width:200, maxLength:400, 
			editor: { xtype:"textfield", maxLength:400}})
		.addTextColumn({name:"cfgPath", dataIndex:"cfgPath", width:200, maxLength:400, 
			editor: { xtype:"textfield", maxLength:400}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
