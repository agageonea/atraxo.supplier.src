/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Areas_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.fmbas.ui.extjs.ds.Area_DsParam,
	recordModel: atraxo.fmbas.ui.extjs.ds.Area_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Areas_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Areas_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"code", dataIndex:"code", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.AreasLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"code", dsField: "code"} ]}})
			.addTextField({ name:"name", dataIndex:"name", maxLength:100})
			.addCombo({ xtype:"combo", name:"indicator", dataIndex:"indicator", store:[ __FMBAS_TYPE__.AreasType._USER_, __FMBAS_TYPE__.AreasType._SYSTEM_]})
			.addBooleanField({ name:"active", dataIndex:"active"})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Areas_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_Areas_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"code", dataIndex:"code", width:150, allowBlank: false, maxLength:32, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:32}})
		.addTextColumn({name:"name", dataIndex:"name", width:150, allowBlank: false, maxLength:100, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:100}})
		.addTextColumn({name:"indicator", dataIndex:"indicator", width:150, noEdit: true, maxLength:32})
		.addBooleanColumn({name:"active", dataIndex:"active"})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Areas_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Areas_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", allowBlank:false, maxLength:32})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, maxLength:100})
		.addTextField({ name:"indicator", bind:"{d.indicator}", dataIndex:"indicator", noEdit:true , maxLength:32})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["code", "name", "indicator", "active"]);
	}
});
