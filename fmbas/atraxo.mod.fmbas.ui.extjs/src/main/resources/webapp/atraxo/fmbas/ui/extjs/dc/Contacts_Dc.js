/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.Contacts_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.Contacts_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.Contacts_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_Contacts_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"firstName", dataIndex:"firstName", maxLength:64})
			.addTextField({ name:"lastName", dataIndex:"lastName", maxLength:64})
			.addBooleanField({ name:"active", dataIndex:"active"})
			.addCombo({ xtype:"combo", name:"departament", dataIndex:"businessUnit", store:[ __FMBAS_TYPE__.Department._OPERATION_, __FMBAS_TYPE__.Department._COMMERCIAL_]})
			.addCombo({ xtype:"combo", name:"workOffice", dataIndex:"workOffice", store:[ __FMBAS_TYPE__.WorkOffice._HEAD_OFFICE_, __FMBAS_TYPE__.WorkOffice._BRANCH_, __FMBAS_TYPE__.WorkOffice._AIRPORT_OFFICE_]})
			.addTextField({ name:"city", dataIndex:"city", maxLength:100})
			.addLov({name:"countryCode", dataIndex:"countryCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CountryLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "countryId"} ]}})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Contacts_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.fmbas_Contacts_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"fullName", dataIndex:"fullName", width:150})
		.addTextColumn({ name:"jobTitle", dataIndex:"jobTitle", width:150})
		.addTextColumn({ name:"businessUnit", dataIndex:"businessUnit", width:150})
		.addTextColumn({ name:"workOffice", dataIndex:"workOffice", width:150})
		.addTextColumn({ name:"city", dataIndex:"city", width:150})
		.addTextColumn({ name:"businessPhone", dataIndex:"businessPhone", width:150})
		.addTextColumn({ name:"email", dataIndex:"email", width:150})
		.addBooleanColumn({ name:"isPrimary", dataIndex:"isPrimary", width:110})
		.addTextColumn({ name:"title", dataIndex:"title", hidden:true, width:70})
		.addTextColumn({ name:"firstName", dataIndex:"firstName", hidden:true, width:90})
		.addTextColumn({ name:"lastName", dataIndex:"lastName", hidden:true, width:90})
		.addTextColumn({ name:"mobilPhone", dataIndex:"mobilePhone", hidden:true, width:120})
		.addTextColumn({ name:"faxNumber", dataIndex:"faxNumber", hidden:true, width:120})
		.addTextColumn({ name:"streetAddress", dataIndex:"street", hidden:true, width:80})
		.addTextColumn({ name:"state", dataIndex:"state", hidden:true, width:100})
		.addTextColumn({ name:"zipCode", dataIndex:"zip", hidden:true, width:120})
		.addTextColumn({ name:"countryCode", dataIndex:"countryCode", hidden:true, width:100})
		.addTextColumn({ name:"countryName", dataIndex:"countryName", hidden:true, width:120})
		.addBooleanColumn({ name:"active", dataIndex:"active", hidden:true})
		.addTextColumn({ name:"notes", dataIndex:"notes", hidden:true, width:200})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Contacts_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Contacts_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addCombo({ xtype:"combo", name:"title", bind:"{d.title}", dataIndex:"title", store:[ __FMBAS_TYPE__.Title._MR__, __FMBAS_TYPE__.Title._MRS__, __FMBAS_TYPE__.Title._MS__, __FMBAS_TYPE__.Title._MISS_]})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active", labelWidth:125})
		.addTextField({ name:"firstName", bind:"{d.firstName}", dataIndex:"firstName", allowBlank:false, maxLength:64})
		.addTextField({ name:"lastName", bind:"{d.lastName}", dataIndex:"lastName", allowBlank:false, maxLength:64})
		.addTextField({ name:"jobTitle", bind:"{d.jobTitle}", dataIndex:"jobTitle", maxLength:32})
		.addBooleanField({ name:"isPrimary", bind:"{d.isPrimary}", dataIndex:"isPrimary", labelWidth:125})
		.addCombo({ xtype:"combo", name:"department", bind:"{d.businessUnit}", dataIndex:"businessUnit", store:[ __FMBAS_TYPE__.Department._OPERATION_, __FMBAS_TYPE__.Department._COMMERCIAL_], labelWidth:125})
		.addCombo({ xtype:"combo", name:"workOffice", bind:"{d.workOffice}", dataIndex:"workOffice", store:[ __FMBAS_TYPE__.WorkOffice._HEAD_OFFICE_, __FMBAS_TYPE__.WorkOffice._BRANCH_, __FMBAS_TYPE__.WorkOffice._AIRPORT_OFFICE_], labelWidth:125})
		.addDisplayFieldText({ name:"addressTitle", bind:"{d.addressTitle}", dataIndex:"addressTitle", maxLength:100, labelWidth:80, style:"margin-top: 6px"})
		.addDisplayFieldText({ name:"phoneEmails", bind:"{d.phoneEmails}", dataIndex:"phoneEmails", maxLength:100, labelWidth:380, style:"margin-top: 6px"})
		.addTextArea({ name:"street", bind:"{d.street}", dataIndex:"street"})
		.addTextField({ name:"city", bind:"{d.city}", dataIndex:"city", maxLength:100})
		.addTextField({ name:"zip", bind:"{d.zip}", dataIndex:"zip", maxLength:64})
		.addTextField({ name:"state", bind:"{d.state}", dataIndex:"state", maxLength:100})
		.addLov({name:"countryName", bind:"{d.countryCode}", dataIndex:"countryCode", allowBlank:false, xtype:"fmbas_CountryLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"id", dsField: "countryId"} ]})
		.addTextField({ name:"businessPhone", bind:"{d.businessPhone}", dataIndex:"businessPhone", maxLength:100})
		.addTextField({ name:"mobilePhone", bind:"{d.mobilePhone}", dataIndex:"mobilePhone", maxLength:100})
		.addTextField({ name:"faxNumber", bind:"{d.faxNumber}", dataIndex:"faxNumber", maxLength:100})
		.addTextField({ name:"email", bind:"{d.email}", dataIndex:"email", allowBlank:false, maxLength:100})
		.addTextArea({ name:"notes", bind:"{d.notes}", dataIndex:"notes", width:400})
		.add({name:"titleActive", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("title"),this._getConfig_("active")]})
		.add({name:"firstNamePrimary", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("firstName"),this._getConfig_("isPrimary")]})
		.add({name:"lastNameDepartment", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("lastName"),this._getConfig_("department")]})
		.add({name:"jobAndOffice", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("jobTitle"),this._getConfig_("workOffice")]})
		.add({name:"addressEmails", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("addressTitle"),this._getConfig_("phoneEmails")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:600, layout:"anchor"})
		.addPanel({ name:"col2", width:600, layout:"anchor"})
		.addPanel({ name:"col3", width:600, layout:"anchor"})
		.addPanel({ name:"col4", width:600, layout:"anchor"})
		.addPanel({ name:"col5", width:600, layout:"anchor"})
		.addPanel({ name:"col10", width:450, layout:"anchor"})
		.addPanel({ name:"col7", width:600, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"col8", width:300, layout:"anchor"})
		.addPanel({ name:"col9", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5", "col7", "col10"])
		.addChildrenTo("col1", ["titleActive"])
		.addChildrenTo("col2", ["firstNamePrimary"])
		.addChildrenTo("col3", ["lastNameDepartment"])
		.addChildrenTo("col4", ["jobAndOffice"])
		.addChildrenTo("col5", ["addressEmails"])
		.addChildrenTo("col10", ["notes"])
		.addChildrenTo("col7", ["col8", "col9"])
		.addChildrenTo("col8", ["street", "city", "zip", "state", "countryName"])
		.addChildrenTo("col9", ["businessPhone", "mobilePhone", "faxNumber", "email"]);
	}
});

/* ================= EDIT FORM: EditDetails ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Contacts_Dc$EditDetails", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Contacts_Dc$EditDetails",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addCombo({ xtype:"combo", name:"title", bind:"{d.title}", dataIndex:"title", store:[ __FMBAS_TYPE__.Title._MR__, __FMBAS_TYPE__.Title._MRS__, __FMBAS_TYPE__.Title._MS__, __FMBAS_TYPE__.Title._MISS_]})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active", labelWidth:125})
		.addTextField({ name:"firstName", bind:"{d.firstName}", dataIndex:"firstName", allowBlank:false, maxLength:64})
		.addTextField({ name:"lastName", bind:"{d.lastName}", dataIndex:"lastName", allowBlank:false, maxLength:64})
		.addTextField({ name:"jobTitle", bind:"{d.jobTitle}", dataIndex:"jobTitle", maxLength:32})
		.addBooleanField({ name:"isPrimary", bind:"{d.isPrimary}", dataIndex:"isPrimary", labelWidth:125})
		.addCombo({ xtype:"combo", name:"department", bind:"{d.businessUnit}", dataIndex:"businessUnit", store:[ __FMBAS_TYPE__.Department._OPERATION_, __FMBAS_TYPE__.Department._COMMERCIAL_], labelWidth:125})
		.addCombo({ xtype:"combo", name:"workOffice", bind:"{d.workOffice}", dataIndex:"workOffice", store:[ __FMBAS_TYPE__.WorkOffice._HEAD_OFFICE_, __FMBAS_TYPE__.WorkOffice._BRANCH_, __FMBAS_TYPE__.WorkOffice._AIRPORT_OFFICE_], labelWidth:125})
		.add({name:"titleActive", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("title"),this._getConfig_("active")]})
		.add({name:"firstNamePrimary", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("firstName"),this._getConfig_("isPrimary")]})
		.add({name:"lastNameDepartment", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("lastName"),this._getConfig_("department")]})
		.add({name:"jobAndOffice", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("jobTitle"),this._getConfig_("workOffice")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:600, layout:"anchor"})
		.addPanel({ name:"col2", width:600, layout:"anchor"})
		.addPanel({ name:"col3", width:600, layout:"anchor"})
		.addPanel({ name:"col4", width:600, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4"])
		.addChildrenTo("col1", ["titleActive"])
		.addChildrenTo("col2", ["firstNamePrimary"])
		.addChildrenTo("col3", ["lastNameDepartment"])
		.addChildrenTo("col4", ["jobAndOffice"]);
	}
});

/* ================= EDIT FORM: Tab ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.Contacts_Dc$Tab", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_Contacts_Dc$Tab",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"addressTitle", bind:"{d.addressTitle}", dataIndex:"addressTitle", maxLength:100, labelWidth:80, style:"margin-top: 6px"})
		.addDisplayFieldText({ name:"phoneEmails", bind:"{d.phoneEmails}", dataIndex:"phoneEmails", maxLength:100, labelWidth:380, style:"margin-top: 6px"})
		.addTextArea({ name:"street", bind:"{d.street}", dataIndex:"street"})
		.addTextField({ name:"city", bind:"{d.city}", dataIndex:"city", maxLength:100})
		.addTextField({ name:"zip", bind:"{d.zip}", dataIndex:"zip", maxLength:64})
		.addTextField({ name:"state", bind:"{d.state}", dataIndex:"state", maxLength:100})
		.addLov({name:"countryName", bind:"{d.countryCode}", dataIndex:"countryCode", allowBlank:false, xtype:"fmbas_CountryLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"id", dsField: "countryId"} ]})
		.addTextField({ name:"businessPhone", bind:"{d.businessPhone}", dataIndex:"businessPhone", maxLength:100})
		.addTextField({ name:"mobilePhone", bind:"{d.mobilePhone}", dataIndex:"mobilePhone", maxLength:100})
		.addTextField({ name:"faxNumber", bind:"{d.faxNumber}", dataIndex:"faxNumber", maxLength:100})
		.addTextField({ name:"email", bind:"{d.email}", dataIndex:"email", allowBlank:false, maxLength:100})
		.addTextArea({ name:"notes", bind:"{d.notes}", dataIndex:"notes", width:400})
		.add({name:"addressEmails", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("addressTitle"),this._getConfig_("phoneEmails")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col5", width:600, layout:"anchor"})
		.addPanel({ name:"col10", width:450, layout:"anchor"})
		.addPanel({ name:"col7", width:600, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"col8", width:300, layout:"anchor"})
		.addPanel({ name:"col9", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col5", "col7", "col10"])
		.addChildrenTo("col5", ["addressEmails"])
		.addChildrenTo("col10", ["notes"])
		.addChildrenTo("col7", ["col8", "col9"])
		.addChildrenTo("col8", ["street", "city", "zip", "state", "countryName"])
		.addChildrenTo("col9", ["businessPhone", "mobilePhone", "faxNumber", "email"]);
	}
});
