/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.fmbas.ui.extjs.dc.TimeSerieItem_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.fmbas.ui.extjs.ds.TimeSerieItem_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.fmbas.ui.extjs.dc.TimeSerieItem_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.fmbas_TimeSerieItem_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addNumberField({name:"tserie", dataIndex:"tserie", maxLength:11})
			.addDateField({name:"itemdate", dataIndex:"itemdate"})
			.addNumberField({name:"itemValue", dataIndex:"itemValue", sysDec:"dec_xr", maxLength:19})
			.addBooleanField({ name:"calculated", dataIndex:"calculated"})
			.addBooleanField({ name:"transfred", dataIndex:"transfred"})
			.addBooleanField({ name:"transferedItemUpdate", dataIndex:"transferedItemUpdate"})
			.addBooleanField({ name:"update", dataIndex:"update"})
			.addBooleanField({ name:"active", dataIndex:"active"})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.TimeSerieItem_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_TimeSerieItem_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addNumberColumn({name:"tserie", dataIndex:"tserie", width:70, maxLength:11, align:"right" })
		.addDateColumn({name:"itemdate", dataIndex:"itemdate", _mask_: Masks.DATE })
		.addNumberColumn({name:"itemValue", dataIndex:"itemValue", sysDec:"dec_xr", maxLength:19, align:"right" })
		.addBooleanColumn({name:"calculated", dataIndex:"calculated", noEdit: true})
		.addBooleanColumn({name:"transfred", dataIndex:"transfred"})
		.addBooleanColumn({name:"transferedItemUpdate", dataIndex:"transferedItemUpdate"})
		.addBooleanColumn({name:"update", dataIndex:"update"})
		.addBooleanColumn({name:"active", dataIndex:"active"})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT-GRID: ListContext ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.TimeSerieItem_Dc$ListContext", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.fmbas_TimeSerieItem_Dc$ListContext",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addDateColumn({name:"itemdate", dataIndex:"itemdate", noUpdate: true, _mask_: Masks.DATE })
		.addNumberColumn({name:"itemValue", dataIndex:"itemValue", width:100, sysDec:"dec_xr", maxLength:19, align:"right" })
		.addBooleanColumn({name:"calculated", dataIndex:"calculated", width:100, noEdit: true})
		.addTextColumn({name:"serieStatus", dataIndex:"serieStatus", hidden:true, width:70, maxLength:40, 
			editor: { xtype:"textfield", maxLength:40}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.fmbas.ui.extjs.dc.TimeSerieItem_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.fmbas_TimeSerieItem_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addNumberField({name:"tserie", bind:"{d.tserie}", dataIndex:"tserie", maxLength:11})
		.addDateField({name:"itemdate", bind:"{d.itemdate}", dataIndex:"itemdate"})
		.addNumberField({name:"itemValue", bind:"{d.itemValue}", dataIndex:"itemValue", sysDec:"dec_xr", maxLength:19})
		.addBooleanField({ name:"calculated", bind:"{d.calculated}", dataIndex:"calculated"})
		.addBooleanField({ name:"transfred", bind:"{d.transfred}", dataIndex:"transfred"})
		.addBooleanField({ name:"transferedItemUpdate", bind:"{d.transferedItemUpdate}", dataIndex:"transferedItemUpdate"})
		.addBooleanField({ name:"update", bind:"{d.update}", dataIndex:"update"})
		.addBooleanField({ name:"active", bind:"{d.active}", dataIndex:"active"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["tserie", "itemdate", "itemValue", "calculated", "transfred", "transferedItemUpdate", "update", "active"]);
	}
});
