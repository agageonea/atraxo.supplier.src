Ext.define("atraxo.fmbas.i18n.ds.JobChainUsers_Ds", {
	enabled__lbl: ".Enabled",
	enabled__tlp: ".Enabled?",
	jobChainId__lbl: ".Job Chain(ID)",
	jobChainName__lbl: ".Job Chain",
	loginName__lbl: ".Login Name",
	loginName__tlp: ".Login name",
	notificationCondition__lbl: ".Condition",
	notificationCondition__tlp: ".Condition",
	notificationType__lbl: ".Type",
	notificationType__tlp: ".Type",
	userEmail__lbl: ".eMail address",
	userEmail__tlp: ".Email",
	userId__lbl: ".User(ID)",
	userName__lbl: ".Notification receiver"
});
