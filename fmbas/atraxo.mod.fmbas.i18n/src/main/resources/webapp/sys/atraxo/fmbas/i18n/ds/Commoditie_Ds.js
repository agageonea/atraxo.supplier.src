Ext.define("atraxo.fmbas.i18n.ds.Commoditie_Ds", {
	code__lbl: ".Code",
	code__tlp: ".code",
	name__lbl: ".Name",
	name__tlp: ".name",
	validFrom__lbl: ".Valid from",
	validFrom__tlp: ".valid from",
	validTo__lbl: ".Valid to",
	validTo__tlp: ".valid to"
});
