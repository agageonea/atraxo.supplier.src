Ext.define("atraxo.fmbas.i18n.ds.ToleranceLov_Ds", {
	active__lbl: ".Active",
	active__tlp: ".Active",
	name__lbl: ".Name",
	name__tlp: ".Name",
	toleranceType__lbl: ".Tolerance type",
	toleranceType__tlp: ".Tolerance type"
});
