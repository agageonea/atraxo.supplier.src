Ext.define("atraxo.fmbas.i18n.frame.ExternalInterfacesMessageHistory_Ui", {
	/* view */
	wdwSeeMessage__ttl: ".Show Message",
	/* menu */
	/* button */
	btnCloseMessage__lbl: ".Close Window",
	btnCloseMessage__tlp: ".Close Sent Message Window",
	btnHelpWdw__lbl: ".Help",
	btnHelpWdw__tlp: ".Help",
	btnReceivedMessage__lbl: ".View received message",
	btnReceivedMessage__tlp: ".View received message",
	btnSentMessage__lbl: ".View sent message",
	btnSentMessage__tlp: ".View sent message",
	btnViewObject__lbl: ".View object",
	btnViewObject__tlp: ".View object",
	
	title: ".External Interfaces"
});
