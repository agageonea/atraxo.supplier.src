Ext.define("atraxo.fmbas.i18n.ds.Dictionary_Ds", {
	criteria__lbl: "",
	externalInterfaceId__lbl: ".External Interface(ID)",
	externalInterfaceName__lbl: ".External Interface",
	externalInterfaceName__tlp: ".Name",
	fieldName__lbl: ".Field name",
	fieldName__tlp: ".Field name",
	newValue__lbl: ".Translate into",
	newValue__tlp: ".Translate into",
	originalValue__lbl: ".Original value",
	originalValue__tlp: ".Original value"
});
