
Ext.define("atraxo.fmbas.i18n.dc.MasterAgrement_Dc$Edit", {
	avgMethodName__lbl: ".Averaging method",
	financialSourceCode__lbl: ".Financial source"
});

Ext.define("atraxo.fmbas.i18n.dc.MasterAgrement_Dc$Filter", {
	avgMthd__lbl: ".Average method",
	financialSourceCode__lbl: ".Financial source"
});

Ext.define("atraxo.fmbas.i18n.dc.MasterAgrement_Dc$List", {
	avgMthd__lbl: ".Averaging method",
	financialSourceCode__lbl: ".Financial source"
});
