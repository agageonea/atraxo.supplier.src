
Ext.define("atraxo.fmbas.i18n.dc.ExchangeRate_Dc$ApprovalNote", {
	remarks__lbl: ".Add your note to be sent with the request to approvers"
});

Ext.define("atraxo.fmbas.i18n.dc.ExchangeRate_Dc$GridV", {
	averagingMethod__lbl: ".Averaging method",
	fromCurrency__lbl: ".From currency",
	intoCurrency__lbl: ".Into currency",
	source__lbl: ".Source",
	validFrom__lbl: ".Valid from",
	validTo__lbl: ".Valid to"
});

Ext.define("atraxo.fmbas.i18n.dc.ExchangeRate_Dc$ListV", {
	averagingMethod__lbl: ".Averaging method",
	fromCurrency__lbl: ".From currency",
	intoCurrency__lbl: ".Into currency",
	source__lbl: ".Source",
	validFrom__lbl: ".Valid from",
	validTo__lbl: ".Valid to"
});
