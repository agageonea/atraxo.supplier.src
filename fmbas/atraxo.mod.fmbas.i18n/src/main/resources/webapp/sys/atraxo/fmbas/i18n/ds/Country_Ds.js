Ext.define("atraxo.fmbas.i18n.ds.Country_Ds", {
	active__lbl: ".Active",
	active__tlp: ".Active",
	code__lbl: ".Code",
	code__tlp: ".Code",
	continent__lbl: ".Continent",
	continent__tlp: ".Continent",
	countryCode__lbl: ".Country reference",
	countryCode__tlp: ".Country reference",
	countryId__lbl: ".Country(ID)",
	name__lbl: ".Name",
	name__tlp: ".Name",
	text__lbl: ""
});
