Ext.define("atraxo.fmbas.i18n.ds.Exposure_Ds", {
	actualUtilization__lbl: ".Actual utilization",
	actualUtilization__tlp: ".Actual utilization",
	currencyCode__lbl: ".Currency",
	currencyCode__tlp: ".Alphabetic code",
	currencyId__lbl: ".Currency(ID)",
	currencyName__lbl: ".Currency(Name)",
	currencyName__tlp: ".Name",
	customerCode__lbl: ".Customer",
	customerCode__tlp: ".Account code",
	customerId__lbl: ".Customer(ID)",
	lastAction__lbl: ".Last action",
	lastAction__tlp: ".Last action",
	type__lbl: ".Type",
	type__tlp: ".Type"
});
