Ext.define("atraxo.fmbas.i18n.ds.DashboardWidget_Ds", {
	dashboardId__lbl: ".Dashboard(ID)",
	dashboardName__lbl: ".Dashboard",
	dashboardName__tlp: ".Name",
	id__lbl: ".Id",
	layoutId__lbl: ".Layout(ID)",
	layoutName__lbl: ".Layout",
	layoutName__tlp: ".Name",
	layoutRegionId__lbl: ".Layout region",
	layoutRegionId__tlp: ".Layout region",
	rpcResponse__lbl: ".Rpc Response",
	widgetDs__lbl: ".Ds Name",
	widgetId__lbl: ".Widget(ID)",
	widgetIndex__lbl: ".Widget index",
	widgetIndex__tlp: ".Widget index",
	widgetMethod__lbl: ".Method Name",
	widgetName__lbl: ".Name",
	widgetName__tlp: ".Name"
});
