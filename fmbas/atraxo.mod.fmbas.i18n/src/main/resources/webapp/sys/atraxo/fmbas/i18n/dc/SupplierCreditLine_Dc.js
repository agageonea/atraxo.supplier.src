
Ext.define("atraxo.fmbas.i18n.dc.SupplierCreditLine_Dc$Edit", {
	alertLimitLabel__lbl: ".Alert limit",
	availabilityLabel__lbl: ".Available credit",
	creditLimitLabel__lbl: ".Credit limit",
	creditTypeLabel__lbl: ".Credit type",
	validFromLabel__lbl: ".Valid from",
	validToLabel__lbl: ".Valid to"
});
