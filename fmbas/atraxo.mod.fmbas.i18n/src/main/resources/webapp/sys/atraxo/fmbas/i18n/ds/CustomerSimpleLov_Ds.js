Ext.define("atraxo.fmbas.i18n.ds.CustomerSimpleLov_Ds", {
	code__lbl: ".Account code",
	code__tlp: ".Account code",
	isCustomer__lbl: ".Is customer?",
	isCustomer__tlp: ".Is customer?",
	name__lbl: ".Name",
	name__tlp: ".Name"
});
