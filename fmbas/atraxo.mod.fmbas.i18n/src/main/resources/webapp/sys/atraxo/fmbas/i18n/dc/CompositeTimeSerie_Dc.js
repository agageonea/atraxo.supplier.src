
Ext.define("atraxo.fmbas.i18n.dc.CompositeTimeSerie_Dc$GridEnergyPrice", {
	currency__lbl: ".Currency",
	description__lbl: ".Description",
	factor__lbl: ".Factor",
	source__lbl: ".Source",
	timeSerie__lbl: ".Time Series",
	unit__lbl: ".UoM",
	weighting__lbl: ".Weighting %"
});
