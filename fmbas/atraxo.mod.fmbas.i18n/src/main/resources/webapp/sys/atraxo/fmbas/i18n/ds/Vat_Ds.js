Ext.define("atraxo.fmbas.i18n.ds.Vat_Ds", {
	code__lbl: ".VAT Code",
	code__tlp: ".VAT code as per country of origin: TVA, GST, etc",
	countryCode__lbl: ".Country",
	countryCode__tlp: ".Code",
	countryId__lbl: ".Country(ID)",
	countryName__lbl: ".Country(Name)",
	countryName__tlp: ".Name"
});
