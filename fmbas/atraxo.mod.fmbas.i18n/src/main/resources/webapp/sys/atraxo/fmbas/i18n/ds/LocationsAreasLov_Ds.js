Ext.define("atraxo.fmbas.i18n.ds.LocationsAreasLov_Ds", {
	code__lbl: ".Code",
	code__tlp: ".Code",
	name__lbl: ".Name",
	name__tlp: ".Name",
	type__lbl: ".Type",
	type__tlp: ".Type"
});
