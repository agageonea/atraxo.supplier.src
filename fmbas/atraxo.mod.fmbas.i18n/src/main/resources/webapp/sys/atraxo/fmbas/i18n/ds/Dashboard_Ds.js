Ext.define("atraxo.fmbas.i18n.ds.Dashboard_Ds", {
	description__lbl: ".Description",
	description__tlp: ".Description",
	isDefault__lbl: ".Is default?",
	isDefault__tlp: ".Is default?",
	layoutId__lbl: ".Layout(ID)",
	layoutName__lbl: ".Layout",
	layoutName__tlp: ".Name",
	name__lbl: ".Name",
	name__tlp: ".Name"
});
