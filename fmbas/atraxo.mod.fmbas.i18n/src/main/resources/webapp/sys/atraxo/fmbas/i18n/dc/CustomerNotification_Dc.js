
Ext.define("atraxo.fmbas.i18n.dc.CustomerNotification_Dc$Filter", {
	assignedArea__lbl: ".Area"
});

Ext.define("atraxo.fmbas.i18n.dc.CustomerNotification_Dc$List", {
	assignedArea__lbl: ".Area",
	emailBody__lbl: ".E-mail template",
	notificationType__lbl: ".Notification Type",
	templateName__lbl: ".Document template"
});
