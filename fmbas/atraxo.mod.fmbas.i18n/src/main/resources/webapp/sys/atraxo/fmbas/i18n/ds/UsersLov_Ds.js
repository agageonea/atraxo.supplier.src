Ext.define("atraxo.fmbas.i18n.ds.UsersLov_Ds", {
	active__lbl: ".Active",
	clientId__lbl: ".Client Id",
	email__lbl: ".Email",
	email__tlp: ".Email",
	id__lbl: ".Id",
	loginName__lbl: ".Login Name",
	loginName__tlp: ".Login name",
	userCode__lbl: ".Code",
	userName__lbl: ".Name"
});
