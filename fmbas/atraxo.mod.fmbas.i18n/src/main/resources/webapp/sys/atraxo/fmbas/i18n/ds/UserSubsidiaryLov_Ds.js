Ext.define("atraxo.fmbas.i18n.ds.UserSubsidiaryLov_Ds", {
	code__lbl: ".Code",
	code__tlp: ".Code",
	mainSubsidiary__lbl: ".Default",
	mainSubsidiary__tlp: ".Default",
	name__lbl: ".Name",
	name__tlp: ".Name",
	subsidiaryId__lbl: ".Subsidiary(ID)",
	userCode__lbl: ".User",
	userId__lbl: ".User(ID)"
});
