Ext.define("atraxo.fmbas.i18n.ds.SystemParameter_Ds", {
	code__lbl: ".Name",
	code__tlp: ".Name",
	description__lbl: ".Description",
	description__tlp: ".Description",
	editable__lbl: ".Editable",
	editable__tlp: ".Editable",
	refValues__lbl: ".Reference values",
	refValues__tlp: ".Editable",
	type__lbl: ".Type",
	type__tlp: ".Type",
	value__lbl: ".Value",
	value__tlp: ".Value"
});
