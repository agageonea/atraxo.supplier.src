Ext.define("atraxo.fmbas.i18n.ds.VatRateLov_Ds", {
	note__lbl: ".Note",
	note__tlp: ".A note for the rate",
	validFrom__lbl: ".Valid from",
	validFrom__tlp: ".Starting date of the rate valability",
	validTo__lbl: ".Valid to",
	validTo__tlp: ".End date for the rate valability",
	vatCode__lbl: ".Vat",
	vatCode__tlp: ".VAT code as per country of origin: TVA, GST, etc",
	vatId__lbl: ".Vat(ID)"
});
