Ext.define("atraxo.fmbas.i18n.ds.ValidationMessage_Ds", {
	labelField__lbl: "",
	message__lbl: ".Validation Message",
	message__tlp: ".Validation Message",
	objectId__lbl: ".Object Reference Id",
	objectType__lbl: ".Object Type",
	severity__lbl: ".Severity",
	severity__tlp: ".Severity"
});
