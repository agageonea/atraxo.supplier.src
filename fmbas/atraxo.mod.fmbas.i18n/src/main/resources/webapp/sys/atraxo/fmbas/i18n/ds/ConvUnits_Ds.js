Ext.define("atraxo.fmbas.i18n.ds.ConvUnits_Ds", {
	active__lbl: ".Active",
	active__tlp: ".Active",
	convType__lbl: ".Conversion type",
	convType__tlp: ".Conversion type",
	factor__lbl: ".Factor",
	factor__tlp: ".Factor",
	unitFromCode__lbl: ".Unit From Id",
	unitFromCode__tlp: ".Code",
	unitFromId__lbl: ".Unit From Id(ID)",
	unitToCode__lbl: ".Unit To Id",
	unitToCode__tlp: ".Code",
	unitToId__lbl: ".Unit To Id(ID)"
});
