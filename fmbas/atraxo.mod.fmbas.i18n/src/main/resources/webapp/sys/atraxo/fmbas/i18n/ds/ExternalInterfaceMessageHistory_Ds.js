Ext.define("atraxo.fmbas.i18n.ds.ExternalInterfaceMessageHistory_Ds", {
	executionTime__lbl: ".Execution Time",
	externalInterfaceId__lbl: ".External Interface(ID)",
	externalInterfaceName__lbl: ".External Interface",
	externalInterfaceName__tlp: ".Name",
	messageContent__lbl: ".Message Content",
	messageId__lbl: ".Message ID",
	objectId__lbl: ".Object Id",
	objectRefId__lbl: ".Object Ref Id",
	objectType__lbl: ".Object Type",
	responseMessageId__lbl: ".Response Message ID",
	responseMessageTime__lbl: ".Response Message Time",
	status__lbl: ".Status",
	status__tlp: ".Indicates the status of the message history"
});
