Ext.define("atraxo.fmbas.i18n.ds.CustomerBlockedActiveLov_Ds", {
	buyerCode__lbl: ".Responsible Buyer",
	buyerId__lbl: ".Responsible Buyer(ID)",
	buyerName__lbl: ".Responsible Buyer(Name)",
	code__lbl: ".Account code",
	code__tlp: ".Account code",
	iataCode__lbl: ".IATA code",
	iataCode__tlp: ".IATA code",
	isCustomer__lbl: ".Is customer?",
	isCustomer__tlp: ".Is customer?",
	name__lbl: ".Name",
	name__tlp: ".Name",
	parentGroupCode__lbl: ".Parent",
	parentGroupCode__tlp: ".Account code",
	parentGroupId__lbl: ".Parent(ID)",
	primaryContactId__lbl: "",
	primaryContactName__lbl: "",
	refid__lbl: ".Refid",
	status__lbl: ".Status",
	status__tlp: ".Status",
	type__lbl: ".Type",
	type__tlp: ".Type"
});
