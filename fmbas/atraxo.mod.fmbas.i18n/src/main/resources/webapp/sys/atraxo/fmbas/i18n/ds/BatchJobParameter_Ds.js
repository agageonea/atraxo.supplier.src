Ext.define("atraxo.fmbas.i18n.ds.BatchJobParameter_Ds", {
	assignType__lbl: ".Assign Type",
	batchJobId__lbl: ".Batch Job(ID)",
	batchJobName__lbl: ".Batch Job",
	description__lbl: ".Description",
	name__lbl: ".Name",
	readOnly__lbl: ".Read Only",
	refBusinessValues__lbl: ".Ref Business Values",
	refValues__lbl: ".Possible values",
	type__lbl: ".Type",
	value__lbl: ".Value"
});
