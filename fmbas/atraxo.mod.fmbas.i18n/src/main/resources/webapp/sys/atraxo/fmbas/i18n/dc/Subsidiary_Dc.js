
Ext.define("atraxo.fmbas.i18n.dc.Subsidiary_Dc$List", {
	assignedLocation__lbl: ".Assigned Area",
	code__lbl: ".Code",
	name__lbl: ".Name",
	officeCity__lbl: ".City",
	officeCountryName__lbl: ".Country"
});

Ext.define("atraxo.fmbas.i18n.dc.Subsidiary_Dc$NewAndEdit", {
	accountNumberLabel__lbl: ".Account number",
	blankDummyField1__lbl: ".  ",
	blankDummyField2__lbl: ".  ",
	cityLabel1__lbl: ".City",
	codeLabel__lbl: ".Company code *",
	countryLabel1__lbl: ".Country",
	defaultVolumeUnitLabel__lbl: ".Default volume unit",
	defaultWeightUnitLabel__lbl: ".Default weight unit",
	dummyField__lbl: ". ",
	emailLabel__lbl: ".Email adress",
	nameLabel__lbl: ".Company name *",
	officeAddressLabel__lbl: ".<span style='font-weight:bold !important'>Office address</span>",
	phoneNumberLabel__lbl: ".Phone number",
	stateProvinceLabel1__lbl: ".State/Province",
	streetLabel1__lbl: ".Street",
	subsidiaryAssignedAreaLabel__lbl: ".Business region",
	subsidiaryCurrencyLabel__lbl: ".Accounting currency",
	timeZoneLabel__lbl: ".Timezone",
	websiteLabel__lbl: ".Website",
	zipPostalCodeLabel1__lbl: ".Zip/Postal Code"
});
