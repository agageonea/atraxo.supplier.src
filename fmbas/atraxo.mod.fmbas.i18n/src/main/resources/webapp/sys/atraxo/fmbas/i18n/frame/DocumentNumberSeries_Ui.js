Ext.define("atraxo.fmbas.i18n.frame.DocumentNumberSeries_Ui", {
	/* view */
	/* menu */
	/* button */
	disableInvNumSerie__lbl: ".Disable",
	disableInvNumSerie__tlp: ".Disable",
	enableInvNumSerie__lbl: ".Enable",
	enableInvNumSerie__tlp: ".Enable",
	helpInvWdw__lbl: ".Help",
	helpInvWdw__tlp: ".Help",
	
	title: ".Document Number Series"
});
