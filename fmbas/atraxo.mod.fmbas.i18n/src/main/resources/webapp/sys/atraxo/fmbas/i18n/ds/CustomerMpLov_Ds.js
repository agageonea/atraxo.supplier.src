Ext.define("atraxo.fmbas.i18n.ds.CustomerMpLov_Ds", {
	business__lbl: ".Business Type",
	buyerCode__lbl: ".Responsible Buyer",
	buyerId__lbl: ".Responsible Buyer(ID)",
	buyerName__lbl: ".Responsible Buyer(Name)",
	code__lbl: ".Account code",
	code__tlp: ".Account code",
	isCustomer__lbl: ".Is customer?",
	isCustomer__tlp: ".Is customer?",
	name__lbl: ".Name",
	name__tlp: ".Name",
	primaryContactId__lbl: "",
	primaryContactName__lbl: "",
	refid__lbl: ".Refid",
	status__lbl: ".Status",
	status__tlp: ".Status",
	type__lbl: ".Type",
	type__tlp: ".Type"
});
