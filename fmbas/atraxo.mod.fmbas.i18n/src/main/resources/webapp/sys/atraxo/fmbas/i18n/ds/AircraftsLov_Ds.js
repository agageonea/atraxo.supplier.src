Ext.define("atraxo.fmbas.i18n.ds.AircraftsLov_Ds", {
	acTypeCode__lbl: ".Ac Types",
	acTypeCode__tlp: ".IATA code",
	acTypeId__lbl: ".Ac Types(ID)",
	custCode__lbl: ".Customer",
	custCode__tlp: ".Account code",
	custId__lbl: ".Customer(ID)",
	registration__lbl: ".Registration",
	registration__tlp: ".Registration",
	validFrom__lbl: ".Valid from",
	validFrom__tlp: ".Valid from",
	validTo__lbl: ".Valid to",
	validTo__tlp: ".Valid to"
});
