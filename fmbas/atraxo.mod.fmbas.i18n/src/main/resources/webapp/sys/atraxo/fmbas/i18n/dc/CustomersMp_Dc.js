
Ext.define("atraxo.fmbas.i18n.dc.CustomersMp_Dc$Addresses", {
	billingAddressLabel__lbl: ".<span style='font-weight:bold !important'>Billing address</span>",
	cityLabel1__lbl: ".City",
	cityLabel2__lbl: ".City",
	countryLabel1__lbl: ".Country",
	countryLabel2__lbl: ".Country",
	officeAddressLabel__lbl: ".<span style='font-weight:bold !important'>Office address</span>",
	stateProvinceLabel1__lbl: ".State/Province",
	stateProvinceLabel2__lbl: ".State/Province",
	streetLabel1__lbl: ".Street",
	streetLabel2__lbl: ".Street",
	useForBillingLabel__lbl: ".Use for billing",
	zipPostalCodeLabel1__lbl: ".Zip/Postal Code",
	zipPostalCodeLabel2__lbl: ".Zip/Postal Code"
});

Ext.define("atraxo.fmbas.i18n.dc.CustomersMp_Dc$Edit", {
	accountNumberLabel__lbl: ".Account number",
	businessLabel__lbl: ".Business",
	buyerNameLabel__lbl: ".Account manager",
	code__lbl: ".Account Code",
	dateOfInc__lbl: ".Registered since",
	faxNumberLabel__lbl: ".Fax number",
	iataCodeLabel__lbl: ".IATA code",
	icaoCodeLabel__lbl: ".ICAO code",
	name__lbl: ".Company Name:",
	phoneNumberLabel__lbl: ".Phone number",
	websiteLabel__lbl: ".Website"
});

Ext.define("atraxo.fmbas.i18n.dc.CustomersMp_Dc$List", {
	business__lbl: ".Business Type"
});

Ext.define("atraxo.fmbas.i18n.dc.CustomersMp_Dc$New", {
	businessLabel__lbl: ".Business",
	codeLabel__lbl: ".Company code",
	contactEmailLabel__lbl: ".e-mail",
	contactFirstNameLabel__lbl: ".First Name",
	contactLastNameLabel__lbl: ".Last Name",
	contactPhoneLabel__lbl: ".Phone",
	dummyLabel__lbl: ".<span style='font-weight:bold !important' />",
	iataCodeLabel__lbl: ".IATA Code",
	icaoCodeLabel__lbl: ".ICAO Code",
	nameLabel__lbl: ".Company Name",
	phoneNumberLabel__lbl: ".Phone number",
	primaryContactLabel__lbl: ".<span style='font-weight:bold !important'>Primary Contact</span>"
});
