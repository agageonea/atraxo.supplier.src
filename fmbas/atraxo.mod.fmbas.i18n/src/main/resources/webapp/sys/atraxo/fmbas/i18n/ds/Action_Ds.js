Ext.define("atraxo.fmbas.i18n.ds.Action_Ds", {
	batchJobId__lbl: ".Batch Job(ID)",
	batchJobName__lbl: ".Batch Job",
	description__lbl: ".Description",
	jobChainId__lbl: ".Job Chain(ID)",
	name__lbl: ".Name",
	order__lbl: ".Order"
});
