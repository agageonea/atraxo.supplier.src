Ext.define("atraxo.fmbas.i18n.ds.CompositeTimeSeriesSource_Ds", {
	currCode__lbl: ".Currency1 Id",
	currCode__tlp: ".Alphabetic code",
	currencyId__lbl: ".Currency1 Id(ID)",
	factor__lbl: ".Factor",
	financialSourceCode__lbl: ".Financial Source",
	financialSourceCode__tlp: ".Code",
	financialSourceId__lbl: ".Financial Source(ID)",
	timeSerieCompId__lbl: ".Composite(ID)",
	timeSerieDescription__lbl: ".Description",
	timeSerieDescription__tlp: ".Description",
	timeSerieId__lbl: ".Source(ID)",
	timeSerieName__lbl: ".Series name",
	timeSerieName__tlp: ".Series name",
	unitCode__lbl: ".Unit Id",
	unitCode__tlp: ".Code",
	unitId__lbl: ".Unit Id(ID)",
	weighting__lbl: ".Weighting"
});
