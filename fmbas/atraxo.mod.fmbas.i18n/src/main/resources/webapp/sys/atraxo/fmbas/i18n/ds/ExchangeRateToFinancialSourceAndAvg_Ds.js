Ext.define("atraxo.fmbas.i18n.ds.ExchangeRateToFinancialSourceAndAvg_Ds", {
	avgMthdCode__lbl: ".Avg Method Indicator",
	avgMthdCode__tlp: ".Code",
	avgMthdId__lbl: ".Avg Method Indicator(ID)",
	avgMthdName__lbl: ".Avg Method Indicator(Name)",
	avgMthdName__tlp: ".Name",
	currencyCode__lbl: ".Currency1",
	currencyCode__tlp: ".Alphabetic code",
	currencyId__lbl: ".Currency1(ID)",
	financialSourceCode__lbl: ".Finsource",
	financialSourceCode__tlp: ".Code",
	financialSourceId__lbl: ".Finsource(ID)",
	financialSourceName__lbl: ".Finsource(Name)",
	financialSourceName__tlp: ".Name",
	name__lbl: ".Name",
	name__tlp: ".Name"
});
