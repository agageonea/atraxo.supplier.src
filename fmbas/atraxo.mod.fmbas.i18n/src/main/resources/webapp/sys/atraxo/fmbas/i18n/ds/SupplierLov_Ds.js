Ext.define("atraxo.fmbas.i18n.ds.SupplierLov_Ds", {
	buyerId__lbl: ".Responsible Buyer(ID)",
	buyerName__lbl: ".Responsible Buyer(Name)",
	code__lbl: ".Code",
	code__tlp: ".Code",
	fixedBaseOperator__lbl: ".Fixed base operator",
	fixedBaseOperator__tlp: ".Fixed base operator",
	fuelSupplier__lbl: ".Fuel provider",
	fuelSupplier__tlp: ".Fuel provider",
	iataCode__lbl: ".IATA code",
	iataCode__tlp: ".IATA code",
	iplAgent__lbl: ".IPL agent",
	iplAgent__tlp: ".IPL agent",
	isSupplier__lbl: ".Is supplier?",
	isSupplier__tlp: ".Is supplier?",
	name__lbl: ".Name",
	name__tlp: ".Name"
});
