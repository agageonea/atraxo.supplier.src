Ext.define("atraxo.fmbas.i18n.frame.energyPrice_Ui", {
	/* view */
	History__ttl: ".History",
	NotesList__ttl: ".Notes",
	composite__ttl: ".Components",
	timeSerieAverageList__ttl: ".Averaging Methods",
	wdwApprovalNote__ttl: ".Remark",
	wdwApproveNote__ttl: ".Remark",
	wdwRejectNote__ttl: ".Remark",
	wdwenergyPrice__ttl: ".New Energy Price Time Series",
	/* menu */
	/* button */
	btnApproveEdit__lbl: ".Approve",
	btnApproveEdit__tlp: ".Approve",
	btnApproveList__lbl: ".Approve",
	btnApproveList__tlp: ".Approve",
	btnCalculate__lbl: ".Calculate",
	btnCalculate__tlp: ".Calculate",
	btnCancelAll__lbl: ".Cancel all",
	btnCancelAll__tlp: ".Cancel all",
	btnCancelApprovalWdw__lbl: ".Cancel submission",
	btnCancelApprovalWdw__tlp: ".Cancel",
	btnCancelApproveWdw__lbl: ".Cancel",
	btnCancelApproveWdw__tlp: ".Cancel",
	btnCancelRejectWdw__lbl: ".Cancel",
	btnCancelRejectWdw__tlp: ".Cancel",
	btnCancelSelected__lbl: ".Cancel",
	btnCancelSelected__tlp: ".Cancel",
	btnCancelTimeSeriesComponents__lbl: ".Cancel",
	btnCancelTimeSeriesComponents__tlp: ".Cancel",
	btnCancelWithMenu__lbl: ".Cancel",
	btnCancelWithMenu__tlp: "",
	btnDeleteSerieItem__lbl: ".Delete",
	btnDeleteSerieItem__tlp: ".Delete",
	btnDeleteTimeSeriesComponents__lbl: ".Delete",
	btnDeleteTimeSeriesComponents__tlp: ".Delete",
	btnNewInlineTimeSeriesCommponents__lbl: ".New",
	btnNewInlineTimeSeriesCommponents__tlp: ".New",
	btnOnEditWdwClose__lbl: ".Cancel",
	btnOnEditWdwClose__tlp: ".Cancel",
	btnPublishSerie__lbl: ".Publish",
	btnPublishSerie__tlp: ".Publish",
	btnPublishSeries__lbl: ".Publish",
	btnPublishSeries__tlp: ".Publish",
	btnRejectEdit__lbl: ".Reject",
	btnRejectEdit__tlp: ".Reject",
	btnRejectList__lbl: ".Reject",
	btnRejectList__tlp: ".Reject",
	btnSaveCloseApprovalWdw__lbl: ".Submit to approver",
	btnSaveCloseApprovalWdw__tlp: ".Submit",
	btnSaveCloseApproveWdw__lbl: ".Save & Close",
	btnSaveCloseApproveWdw__tlp: ".Save & Close",
	btnSaveCloseRejectWdw__lbl: ".Save & Close",
	btnSaveCloseRejectWdw__tlp: ".Save & Close",
	btnSaveCloseWdw__lbl: ".Save & Close",
	btnSaveCloseWdw__tlp: ".Save & Close",
	btnSaveNewWdw__lbl: ".Save & New",
	btnSaveNewWdw__tlp: ".Save & New",
	btnSaveTimeSeriesComponents__lbl: ".Save",
	btnSaveTimeSeriesComponents__tlp: ".Save",
	btnSubmitForApprovalEdit__lbl: ".Submit for Approval",
	btnSubmitForApprovalEdit__tlp: ".Submit for Approval",
	btnSubmitForApprovalExchangeRates__lbl: ".Submit for Approval",
	btnSubmitForApprovalExchangeRates__tlp: ".Submit for Approval",
	btnSubmitForApprovalList__lbl: ".Submit for Approval",
	btnSubmitForApprovalList__tlp: ".Submit for Approval",
	helpWdw__lbl: ".Help",
	helpWdw__tlp: ".Help",
	helpWdw1__lbl: ".Help",
	helpWdw1__tlp: ".Help",
	helpWdw2__lbl: ".Help",
	helpWdw2__tlp: ".Help",
	showenergyPrice__lbl: ".Energy Prices",
	showenergyPrice__tlp: ".Energy Price",
	
	title: ".Energy Price Time Series"
});
