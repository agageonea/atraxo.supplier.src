Ext.define("atraxo.fmbas.i18n.ds.StepExecution_Ds", {
	commitCount__lbl: ".Commit Count",
	endTime__lbl: ".End Time",
	exitCode__lbl: ".Exit Code",
	exitMessage__lbl: ".Exit Message",
	jobExecStepExitCode__lbl: ".Exit Code",
	jobExecStepExitMessage__lbl: ".Exit Message",
	jobExecStepId__lbl: ".Job Exec Step(ID)",
	jobExecStepStatus__lbl: ".Status",
	jobInstanceId__lbl: ".Job Instance(ID)",
	jobInstanceKey__lbl: ".Job Key",
	jobName__lbl: ".Job Name",
	readCount__lbl: ".Read Count",
	rollbackCount__lbl: ".Rollback Count",
	startTime__lbl: ".Start Time",
	status__lbl: ".Status",
	writeCount__lbl: ".Write Count"
});
