Ext.define("atraxo.fmbas.i18n.frame.Areas_Ui", {
	/* view */
	/* menu */
	/* button */
	assignLocations__lbl: ".Assign/Deassign",
	assignLocations__tlp: ".Assign/Deassign location",
	btnCancelAll__lbl: ".Cancel all",
	btnCancelAll__tlp: ".Cancel all",
	btnCancelSelected__lbl: ".Cancel",
	btnCancelSelected__tlp: ".Cancel",
	btnCancelWithMenu__lbl: ".Cancel",
	btnCancelWithMenu__tlp: "",
	helpWdw__lbl: ".Help",
	helpWdw__tlp: ".Help",
	helpWdw1__lbl: ".Help",
	helpWdw1__tlp: ".Help",
	
	title: ".Areas"
});
