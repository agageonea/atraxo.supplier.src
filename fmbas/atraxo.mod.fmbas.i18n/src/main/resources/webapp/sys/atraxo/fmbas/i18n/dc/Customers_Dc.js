
Ext.define("atraxo.fmbas.i18n.dc.Customers_Dc$Addresses", {
	billingAddressLabel__lbl: ".<span style='font-weight:bold !important'>Billing address</span>",
	cityLabel1__lbl: ".City",
	cityLabel2__lbl: ".City",
	countryLabel1__lbl: ".Country",
	countryLabel2__lbl: ".Country",
	officeAddressLabel__lbl: ".<span style='font-weight:bold !important'>Office address</span>",
	stateProvinceLabel1__lbl: ".State/Province",
	stateProvinceLabel2__lbl: ".State/Province",
	streetLabel1__lbl: ".Street",
	streetLabel2__lbl: ".Street",
	useForBillingLabel__lbl: ".Use for billing",
	zipPostalCodeLabel1__lbl: ".Zip/Postal Code",
	zipPostalCodeLabel2__lbl: ".Zip/Postal Code"
});

Ext.define("atraxo.fmbas.i18n.dc.Customers_Dc$Edit", {
	accountNumberLabel__lbl: ".Account number",
	aocLabel__lbl: ".AOC",
	aocValidFrom__lbl: ".Valid from",
	aocValidTo__lbl: ".to",
	buyerNameLabel__lbl: ".Account manager",
	dateOfInc__lbl: ".Customer since",
	emailLabel__lbl: ".Email adress",
	faxNumberLabel__lbl: ".Fax number",
	iataCodeLabel__lbl: ".IATA code",
	icaoCodeLabel__lbl: ".ICAO code",
	incomeTaxLabel__lbl: ".Income tax",
	natureOfBusinessLabel__lbl: ".Nature of business",
	parentLabel__lbl: ".Parent group",
	phoneNumberLabel__lbl: ".Phone number",
	taxIdentification__lbl: ".Tax identification",
	tlValidFrom__lbl: ".Valid from",
	tlValidTo__lbl: ".to",
	toLabel1__lbl: ".to",
	toLabel2__lbl: ".to",
	tradeLicenseLabel__lbl: ".Trade license",
	typeLabel__lbl: ".Type",
	validFromLabel1__lbl: ".Valid from",
	validFromLabel2__lbl: ".Valid from",
	vatRegistrationLabel__lbl: ".VAT Registration",
	websiteLabel__lbl: ".Website"
});

Ext.define("atraxo.fmbas.i18n.dc.Customers_Dc$Filter", {
	parentName__lbl: ".Member of"
});

Ext.define("atraxo.fmbas.i18n.dc.Customers_Dc$List", {
	parentName__lbl: ".Member of"
});

Ext.define("atraxo.fmbas.i18n.dc.Customers_Dc$NewCustomerPopup", {
	accountNumberLabel__lbl: ".Account number",
	aocValidFrom__lbl: ".Valid from",
	aocValidTo__lbl: ".to",
	billingAddressLabel__lbl: ".<span style='font-weight:bold !important'>Billing address</span>",
	cityLabel1__lbl: ".City",
	cityLabel2__lbl: ".City",
	codeLabel__lbl: ".Account code",
	countryLabel1__lbl: ".Country",
	countryLabel2__lbl: ".Country",
	dateOfIncLabel__lbl: ".Customer since",
	emailLabel__lbl: ".Email address",
	faxNumberLabel__lbl: ".Fax number",
	generateCodeNumber__lbl: ".<i class='fa fa-refresh'></i>",
	nameLabel__lbl: ".Name",
	natureOfBusinessLabel__lbl: ".Nature of business",
	officeAddressLabel__lbl: ".<span style='font-weight:bold !important'>Office address</span>",
	parentGroupLabel__lbl: ".Parent group",
	phoneNumberLabel__lbl: ".Phone number",
	stateProvinceLabel1__lbl: ".State/Province",
	stateProvinceLabel2__lbl: ".State/Province",
	streetLabel1__lbl: ".Street",
	streetLabel2__lbl: ".Street",
	tlValidFrom__lbl: ".Valid from",
	tlValidTo__lbl: ".to",
	typeLabel__lbl: ".Type",
	useForBillingLabel__lbl: ".Use for billing",
	websiteLabel__lbl: ".Website",
	zipPostalCodeLabel1__lbl: ".Zip/Postal Code",
	zipPostalCodeLabel2__lbl: ".Zip/Postal Code"
});
