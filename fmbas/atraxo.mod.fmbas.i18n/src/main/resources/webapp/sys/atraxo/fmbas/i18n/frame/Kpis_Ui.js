Ext.define("atraxo.fmbas.i18n.frame.Kpis_Ui", {
	/* view */
	/* menu */
	/* button */
	btnCancelAll__lbl: ".Cancel all",
	btnCancelAll__tlp: ".Cancel all",
	btnCancelSelected__lbl: ".Cancel",
	btnCancelSelected__tlp: ".Cancel",
	btnCancelWithMenu__lbl: ".Cancel",
	btnCancelWithMenu__tlp: "",
	helpWdw__lbl: ".Help",
	helpWdw__tlp: ".Help",
	
	title: ".KPI"
});
