Ext.define("atraxo.fmbas.i18n.ds.JobChain_Ds", {
	description__lbl: ".Description",
	enabled__lbl: ".Enabled",
	enabled__tlp: ".Enabled",
	lastRunResult__lbl: ".Last run result",
	lastRunTime__lbl: ".Last run time",
	name__lbl: ".Name",
	nextRunTime__lbl: ".Next run time",
	paramIdentifier__lbl: "",
	status__lbl: ".Status",
	trigers__lbl: ".Triggers"
});
