Ext.define("atraxo.fmbas.i18n.ds.Currencies_Ds", {
	active__lbl: ".Active",
	active__tlp: ".Active",
	code__lbl: ".Alphabetic code",
	code__tlp: ".Alphabetic code",
	decCnt__lbl: ".Minor unit",
	decCnt__tlp: ".Minor unit",
	isoNo__lbl: ".Numeric code",
	isoNo__tlp: ".Numeric code",
	name__lbl: ".Name",
	name__tlp: ".Name"
});
