Ext.define("atraxo.fmbas.i18n.ds.IataPC_Ds", {
	active__lbl: ".Active",
	active__tlp: ".Active",
	code__lbl: ".IATA code",
	code__tlp: ".IATA code",
	name__lbl: ".Name",
	name__tlp: ".Name",
	sourceNotes__lbl: ".Source notes",
	sourceNotes__tlp: ".Source notes",
	source__lbl: ".Source",
	source__tlp: ".Source",
	use__lbl: ".Use",
	use__tlp: ".Use"
});
