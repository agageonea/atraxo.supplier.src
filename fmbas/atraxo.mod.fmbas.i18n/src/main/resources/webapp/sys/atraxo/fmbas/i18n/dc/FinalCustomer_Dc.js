
Ext.define("atraxo.fmbas.i18n.dc.FinalCustomer_Dc$Edit", {
	codeLabel__lbl: ".Account code",
	iataCodeLabel__lbl: ".IATA code",
	icaoCodeLabel__lbl: ".ICAO code",
	nameLabel__lbl: ".Name"
});
