Ext.define("atraxo.fmbas.i18n.frame.Templates_Ui", {
	/* view */
	wdwEdit__ttl: ".Edit template",
	wdwNew__ttl: ".Add new template",
	/* menu */
	/* button */
	btnCancel__lbl: ".Cancel",
	btnCancel__tlp: ".Cancel",
	btnCancelEdit__lbl: ".Cancel",
	btnCancelEdit__tlp: ".Cancel",
	btnDeleteTemplate__lbl: ".Delete",
	btnDeleteTemplate__tlp: ".Delete",
	btnDownloadTemplateList__lbl: ".Download",
	btnDownloadTemplateList__tlp: ".Download",
	btnEditTemplate__lbl: ".Edit",
	btnEditTemplate__tlp: ".Edit",
	btnHelpWdw__lbl: ".Help",
	btnHelpWdw__tlp: ".Help",
	btnSaveEditTemplate__lbl: ".Save",
	btnSaveEditTemplate__tlp: ".Save",
	btnSelectFile__lbl: ".Select file",
	btnSelectFile__tlp: ".Select file",
	btnSelectNewTemplate__lbl: ".Select file",
	btnSelectNewTemplate__tlp: ".Select file",
	
	title: ".Template Manager"
});
