Ext.define("atraxo.fmbas.i18n.ds.BankAccount_Ds", {
	accountHolder__lbl: ".Account holder",
	accountHolder__tlp: ".Account holder",
	accountNumber__lbl: ".Account number",
	accountNumber__tlp: ".Account number",
	bankAdress__lbl: ".Bank address",
	bankAdress__tlp: ".Bank address",
	bankName__lbl: ".Bank name",
	bankName__tlp: ".Bank name",
	branchCode__lbl: ".Branch code",
	branchCode__tlp: ".Branch code",
	branchName__lbl: ".Branch name",
	branchName__tlp: ".Branch name",
	currencyCode__lbl: ".Currency",
	currencyCode__tlp: ".Alphabetic code",
	currencyId__lbl: ".Currency(ID)",
	customerCodes__lbl: ".For customer",
	ibanCode__lbl: ".IBAN",
	ibanCode__tlp: ".IBAN",
	subsidiaryCode__lbl: ".Subsidiary",
	subsidiaryCode__tlp: ".Account code",
	subsidiaryId__lbl: ".Subsidiary(ID)",
	swiftCode__lbl: ".Swift",
	swiftCode__tlp: ".IBAN"
});
