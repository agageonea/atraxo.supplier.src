
Ext.define("atraxo.fmbas.i18n.dc.User_Dc$Filter", {
	loginCode__lbl: ".Code"
});

Ext.define("atraxo.fmbas.i18n.dc.User_Dc$Header", {
	activeLabel__lbl: ".Active",
	dateFLabel__lbl: ".Date format",
	decimalFLabel__lbl: ".Decimal format",
	displayNameLabel__lbl: ".Display name",
	locLabel__lbl: ".Base location",
	loginNameLabel__lbl: ".Login name",
	notesLabel__lbl: ".Notes",
	sepLabel__lbl: ".Thousands separator",
	userLanguageLabel__lbl: ".Application language"
});

Ext.define("atraxo.fmbas.i18n.dc.User_Dc$List", {
	display__lbl: ".Display name",
	first__lbl: ".First name",
	last__lbl: ".Last name",
	login__lbl: ".Login name",
	userLanguage__lbl: ".Application language"
});

Ext.define("atraxo.fmbas.i18n.dc.User_Dc$New", {
	businessPhoneLabel__lbl: ".Business phone",
	depLabel__lbl: ".Department",
	displayNameLabel__lbl: ".Display name",
	emailLabel__lbl: ".E-mail",
	firstNameLabel__lbl: ".First name",
	jobTitleLabel__lbl: ".Job title",
	lastNameLabel__lbl: ".Last name",
	locLabel__lbl: ".Base location",
	loginNameLabel__lbl: ".Login name",
	mobilePhoneLabel__lbl: ".Mobile phone",
	titleLabel__lbl: ".Title",
	worklabel__lbl: ".Work office"
});

Ext.define("atraxo.fmbas.i18n.dc.User_Dc$Tab", {
	businessPhoneLabel__lbl: ".Business phone",
	depLabel__lbl: ".Department",
	emailLabel__lbl: ".E-mail",
	firstNameLabel__lbl: ".First name",
	jobTitleLabel__lbl: ".Job title",
	lastNameLabel__lbl: ".Last name",
	mobilePhoneLabel__lbl: ".Mobile phone",
	titleLabel__lbl: ".Title",
	worklabel__lbl: ".Work office"
});
