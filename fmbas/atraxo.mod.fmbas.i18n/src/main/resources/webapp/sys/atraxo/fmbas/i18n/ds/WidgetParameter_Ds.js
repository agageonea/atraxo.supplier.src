Ext.define("atraxo.fmbas.i18n.ds.WidgetParameter_Ds", {
	defaultValue__lbl: ".Default Value",
	description__lbl: ".Description",
	description__tlp: ".Description",
	name__lbl: ".Name",
	name__tlp: ".Name",
	readOnly__lbl: ".Read Only",
	refBusinessValues__lbl: ".Ref Business Values",
	refValues__lbl: ".Possible values",
	type__lbl: ".Parameter type",
	value__lbl: ".Value",
	widgetCode__lbl: ".Widget",
	widgetCode__tlp: ".Name",
	widgetId__lbl: ".Widget(ID)",
	widgetName__lbl: ".Widget(Name)",
	widgetName__tlp: ".Name"
});
