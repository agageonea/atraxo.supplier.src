
Ext.define("atraxo.fmbas.i18n.dc.Aircraft_Dc$List", {
	acType__tlp: ".Aircraft type",
	customer__tlp: ".Reference to the company who is the customer of the A/C	",
	operator__tlp: ".Reference to the company who is the operator of the A/C	",
	owner__tlp: ".Reference to the company who is the owner of the A/C",
	registration__tlp: ".Aircraft registration number",
	validFrom__tlp: ".Starting validity date",
	validTo__tlp: ".Ending validity date"
});

Ext.define("atraxo.fmbas.i18n.dc.Aircraft_Dc$New", {
	acTypeLabel__lbl: ".Aircraft type",
	acType__tlp: ".Aircraft type",
	customerLabel__lbl: ".Customer",
	registrationLabel__lbl: ".Registration",
	registration__tlp: ".Aircraft registration number",
	validFrom__tlp: ".Starting validity date",
	validTo__tlp: ".Ending validity date"
});
