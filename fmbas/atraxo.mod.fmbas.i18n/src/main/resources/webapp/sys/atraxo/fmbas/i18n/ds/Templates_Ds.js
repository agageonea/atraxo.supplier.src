Ext.define("atraxo.fmbas.i18n.ds.Templates_Ds", {
	description__lbl: ".Description",
	description__tlp: ".Description",
	fileName__lbl: ".Source file name",
	fileName__tlp: ".File name",
	fileReference__lbl: ".File reference",
	fileReference__tlp: ".File reference",
	file__lbl: "",
	isUpdate__lbl: ".Is Update",
	name__lbl: ".Name",
	name__tlp: ".Name",
	previewFile__lbl: ".Preview File",
	subsidiaryCode__lbl: ".Subsidiary code",
	subsidiaryCode__tlp: ".Account code",
	subsidiaryId__lbl: ".Subsidiary(ID)",
	subsidiaryName__lbl: ".Subsidiary name",
	subsidiaryName__tlp: ".Name",
	systemUse__lbl: ".System use",
	systemUse__tlp: ".System use",
	templateId__lbl: ".Template Id",
	typeId__lbl: ".Type(ID)",
	typeName__lbl: ".Used for",
	typeName__tlp: ".Name",
	uploadDate__lbl: ".Upload date",
	uploadDate__tlp: ".Upload date",
	userCode__lbl: ".User",
	userId__lbl: ".User(ID)",
	userName__lbl: ".Loaded by"
});
