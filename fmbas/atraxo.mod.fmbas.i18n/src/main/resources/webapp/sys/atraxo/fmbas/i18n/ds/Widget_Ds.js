Ext.define("atraxo.fmbas.i18n.ds.Widget_Ds", {
	cfgPath__lbl: ".Path to config file",
	cfgPath__tlp: ".Path to config file",
	code__lbl: ".Code",
	code__tlp: ".Name",
	description__lbl: ".Description",
	description__tlp: ".Description",
	dsName__lbl: ".Ds Name",
	methodName__lbl: ".Method Name",
	name__lbl: ".Name",
	name__tlp: ".Name",
	thumbPath__lbl: ".Thumbnail path",
	thumbPath__tlp: ".Thumbnail path"
});
