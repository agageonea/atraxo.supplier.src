Ext.define("atraxo.fmbas.i18n.ds.CustomerGroupLov_Ds", {
	code__lbl: ".Account code",
	code__tlp: ".Account code",
	customerId__lbl: ".Customer Id",
	customerParentId__lbl: ".Customer Parent Id",
	id__lbl: ".Id",
	isCustomer__lbl: ".Is customer?",
	isCustomer__tlp: ".Is customer?",
	parentGroupCode__lbl: ".Parent",
	parentGroupCode__tlp: ".Account code",
	parentGroupId__lbl: ".Parent(ID)",
	type__lbl: ".Type",
	type__tlp: ".Type"
});
