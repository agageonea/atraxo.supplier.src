Ext.define("atraxo.fmbas.i18n.ds.SubsidiaryLov_Ds", {
	code__lbl: ".Account code",
	code__tlp: ".Account code",
	isCustomer__lbl: ".Is customer?",
	isCustomer__tlp: ".Is customer?",
	isSubsidiary__lbl: ".Is subsidiary?",
	name__lbl: ".Name",
	name__tlp: ".Name",
	refid__lbl: ".Refid",
	status__lbl: ".Status",
	status__tlp: ".Status",
	type__lbl: ".Type",
	type__tlp: ".Type"
});
