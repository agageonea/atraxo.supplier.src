Ext.define("atraxo.fmbas.i18n.ds.AveragingMethod_Ds", {
	active__lbl: ".Active",
	active__tlp: ".Active",
	code__lbl: ".Code",
	code__tlp: ".Code",
	name__lbl: ".Description",
	name__tlp: ".Description"
});
