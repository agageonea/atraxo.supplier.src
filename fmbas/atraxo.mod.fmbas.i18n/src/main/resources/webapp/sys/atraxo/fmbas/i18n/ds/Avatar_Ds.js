Ext.define("atraxo.fmbas.i18n.ds.Avatar_Ds", {
	active__lbl: ".Active",
	baseUrl__lbl: ".Base Url",
	contentType__lbl: ".Content Type",
	fileName__lbl: ".File Name",
	filePath__lbl: "",
	location__lbl: ".Location",
	loginName__lbl: ".Login Name",
	name__lbl: ".Name",
	typeId__lbl: ".Type(ID)",
	type__lbl: ".Type",
	userId__lbl: ".User(ID)",
	userName__lbl: ".User(Name)"
});
