Ext.define("atraxo.fmbas.i18n.ds.Area_Ds", {
	active__lbl: ".Active",
	active__tlp: ".Active",
	code__lbl: ".Area code",
	code__tlp: ".Area code",
	indicator__lbl: ".Type",
	indicator__tlp: ".Type",
	locationId__lbl: ".Location Id",
	name__lbl: ".Name",
	name__tlp: ".Name"
});
