
Ext.define("atraxo.fmbas.i18n.dc.PriceCategories_Dc$EditForm", {
	activeLabel__lbl: ".Active?",
	descLabel__lbl: ".Comments",
	iataCodeLabel__lbl: ".IATA code",
	mainCatLabel__lbl: ".Main category",
	nameLabel__lbl: ".Price category",
	pricePerLabel__lbl: ".Price per",
	typeLabel__lbl: ".Type"
});
