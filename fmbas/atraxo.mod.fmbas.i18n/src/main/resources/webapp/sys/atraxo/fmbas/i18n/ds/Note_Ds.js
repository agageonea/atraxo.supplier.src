Ext.define("atraxo.fmbas.i18n.ds.Note_Ds", {
	createdAfter__lbl: ".Created after",
	createdBefore__lbl: ".Created before",
	notes__lbl: ".Note",
	notes__tlp: ".Note",
	objectId__lbl: ".Object Id",
	objectType__lbl: ".Object Type"
});
