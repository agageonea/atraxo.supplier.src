Ext.define("atraxo.fmbas.i18n.ds.CurrenciesLov_Ds", {
	active__lbl: ".Active",
	active__tlp: ".Active",
	code__lbl: ".Alphabetic code",
	code__tlp: ".Alphabetic code",
	name__lbl: ".Name",
	name__tlp: ".Name"
});
