Ext.define("atraxo.fmbas.i18n.frame.WorkflowTask_Ui", {
	/* view */
	wdwCompleteTask__ttl: ".Note",
	/* menu */
	/* button */
	btnAcceptTask__lbl: ".Approve",
	btnAcceptTask__tlp: ".Accept",
	btnClaimWkfTaskRpc__lbl: ".Claim task",
	btnClaimWkfTaskRpc__tlp: ".Claim workflow task",
	btnCompleteWkfTaskRpc__lbl: ".Complete task",
	btnCompleteWkfTaskRpc__tlp: ".Complete workflow task",
	btnRejectTask__lbl: ".Reject",
	btnRejectTask__tlp: ".Reject",
	
	title: ".WorkflowTask"
});
