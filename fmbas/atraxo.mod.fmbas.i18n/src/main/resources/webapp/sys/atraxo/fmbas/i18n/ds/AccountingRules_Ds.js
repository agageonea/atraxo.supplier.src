Ext.define("atraxo.fmbas.i18n.ds.AccountingRules_Ds", {
	ruleType__lbl: ".Rule type",
	ruleType__tlp: ".Rule type",
	subsidiaryCode__lbl: ".Subsidiary",
	subsidiaryCode__tlp: ".Account code",
	subsidiaryId__lbl: ".Subsidiary(ID)"
});
