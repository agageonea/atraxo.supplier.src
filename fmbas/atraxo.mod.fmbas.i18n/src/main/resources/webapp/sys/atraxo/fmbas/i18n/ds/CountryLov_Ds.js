Ext.define("atraxo.fmbas.i18n.ds.CountryLov_Ds", {
	active__lbl: ".Active",
	active__tlp: ".Active",
	code__lbl: ".Code",
	code__tlp: ".Code",
	countryCode__lbl: ".Country",
	countryCode__tlp: ".Code",
	countryId__lbl: ".Country(ID)",
	id__lbl: ".Id",
	name__lbl: ".Name",
	name__tlp: ".Name"
});
