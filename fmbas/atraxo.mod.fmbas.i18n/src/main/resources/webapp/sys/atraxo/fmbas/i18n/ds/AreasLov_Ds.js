Ext.define("atraxo.fmbas.i18n.ds.AreasLov_Ds", {
	active__lbl: ".Active",
	active__tlp: ".Active",
	code__lbl: ".Area code",
	code__tlp: ".Area code",
	name__lbl: ".Name",
	name__tlp: ".Name"
});
