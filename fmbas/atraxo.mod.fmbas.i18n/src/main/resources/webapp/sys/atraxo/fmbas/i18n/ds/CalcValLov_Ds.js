Ext.define("atraxo.fmbas.i18n.ds.CalcValLov_Ds", {
	avgMthdId__lbl: ".Avg Method Indicator(ID)",
	calcVal__lbl: ".Using ForEx of",
	calcVal__tlp: ".Using ForEx of",
	financialSourceId__lbl: ".Finsource(ID)",
	name__lbl: ".Name",
	name__tlp: ".Name"
});
