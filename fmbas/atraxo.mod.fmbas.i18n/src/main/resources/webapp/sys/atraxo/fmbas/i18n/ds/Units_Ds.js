Ext.define("atraxo.fmbas.i18n.ds.Units_Ds", {
	active__lbl: ".Active",
	active__tlp: ".Active",
	code__lbl: ".Code",
	code__tlp: ".Code",
	factor__lbl: ".Factor",
	factor__tlp: ".Factor",
	iataCode__lbl: ".IATA Code",
	iataCode__tlp: ".IATA Code",
	id__lbl: ".Id",
	name__lbl: ".Name",
	name__tlp: ".Name",
	unittypeInd__lbl: ".Unit type indicator",
	unittypeInd__tlp: ".Unit type indicator"
});
