Ext.define("atraxo.fmbas.i18n.ds.FinancialSources_Ds", {
	active__lbl: ".Active",
	active__tlp: ".Active",
	code__lbl: ".Code",
	code__tlp: ".Code",
	description__lbl: ".Description",
	description__tlp: ".Description",
	isStdFlg__lbl: ".Standard X-Rate",
	isStdFlg__tlp: ".Standard X-Rate",
	name__lbl: ".Name",
	name__tlp: ".Name"
});
