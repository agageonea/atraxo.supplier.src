
Ext.define("atraxo.fmbas.i18n.dc.Trigger_Dc$List", {
	active__lbl: ".Status",
	details__lbl: ".Details",
	startDate__lbl: ".Start Date",
	type__lbl: ".Trigger type"
});

Ext.define("atraxo.fmbas.i18n.dc.Trigger_Dc$New", {
	daysOfMonthCombo__lbl: ".Days",
	details__lbl: ".Summary:",
	intInDays__lbl: ".Repeats every",
	intInWeeks__lbl: ".Repeats every",
	monthsCombo__lbl: ".Months",
	startDate__lbl: ".When: ",
	type__lbl: ".How Often",
	weekOfMonthCombo__lbl: ".On"
});

Ext.define("atraxo.fmbas.i18n.dc.Trigger_Dc$Temp", {
	comingSoon__lbl: ".<span style='font-weight:bold'>Coming soon...</span>"
});
