
Ext.define("atraxo.fmbas.i18n.dc.StepExecution_Dc$StepExecutionList", {
	endTime__lbl: ".Start time",
	exitCode__lbl: ".Exit code",
	exitMessage__lbl: ".Exit message",
	readCount__lbl: ".Read count",
	startTime__lbl: ".Start time",
	status__lbl: ".Status",
	writeCount__lbl: ".Write count"
});
