Ext.define("atraxo.fmbas.i18n.frame.ExternalInterfaces_Ui", {
	/* view */
	dictionaryList__ttl: ".Translation Dictionary",
	historyList__ttl: ".Execution History",
	notificationReceiversList__ttl: ".Notification Receivers",
	parameterList__ttl: ".Parameters",
	wdwConditions__ttl: ".Translation criteria",
	wdwErrorLog__ttl: ".Result",
	/* menu */
	/* button */
	btnCancelAll__lbl: ".Cancel all",
	btnCancelAll__tlp: ".Cancel all",
	btnCancelSelected__lbl: ".Cancel",
	btnCancelSelected__tlp: ".Cancel",
	btnCancelWithMenu__lbl: ".Cancel",
	btnCancelWithMenu__tlp: ".Cancel",
	btnCloseConditions__lbl: ".Close",
	btnCloseConditions__tlp: ".Close",
	btnCloseLog__lbl: ".Close",
	btnCloseLog__tlp: ".Close log window",
	btnConditions__lbl: ".Conditions",
	btnConditions__tlp: ".Conditions",
	btnDeleteHistory__lbl: ".Delete",
	btnDeleteHistory__tlp: ".Delete",
	btnDisable__lbl: ".Disable",
	btnDisable__tlp: ".Disable",
	btnEmptyHistory__lbl: ".Empty",
	btnEmptyHistory__tlp: ".Empty",
	btnEnable__lbl: ".Enable",
	btnEnable__tlp: ".Enable",
	btnHelpWdw__lbl: ".Help",
	btnHelpWdw__tlp: ".Help",
	btnViewResultHistory__lbl: ".View result",
	btnViewResultHistory__tlp: ".View result",
	
	title: ".External Interfaces"
});
