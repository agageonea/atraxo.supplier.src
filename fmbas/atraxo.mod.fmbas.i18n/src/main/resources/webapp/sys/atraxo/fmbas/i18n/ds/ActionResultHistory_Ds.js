Ext.define("atraxo.fmbas.i18n.ds.ActionResultHistory_Ds", {
	actionName__lbl: ".Action name",
	duration__lbl: ".Duration",
	endTime__lbl: ".Finished at",
	exitCode__lbl: ".Result",
	exitMessage__lbl: ".Exit Message",
	jobExId__lbl: ".Job Execution(ID)",
	msg__lbl: ".Result Message",
	name__lbl: ".Key Name",
	stacktrace__lbl: "",
	startTime__lbl: ".Started at",
	status__lbl: ".Status",
	value__lbl: ".Long Val"
});
