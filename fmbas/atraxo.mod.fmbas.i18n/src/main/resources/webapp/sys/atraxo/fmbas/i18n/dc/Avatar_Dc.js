
Ext.define("atraxo.fmbas.i18n.dc.Avatar_Dc$Filter", {
	contentType__lbl: ".Content type",
	fileName__lbl: ".File name",
	userName__lbl: ".User name"
});

Ext.define("atraxo.fmbas.i18n.dc.Avatar_Dc$List", {
	contentType__lbl: ".Content type",
	fileName__lbl: ".File name",
	userName__lbl: ".User name"
});

Ext.define("atraxo.fmbas.i18n.dc.Avatar_Dc$New", {
	btnCancel__lbl: ".Cancel",
	btnSelectFile__lbl: ".Select file",
	name__lbl: ".Display name",
	type__lbl: ".Document type"
});
