Ext.define("atraxo.fmbas.i18n.ds.ExchangeRateCalcVal_Ds", {
	avgMethodIndicatorCode__lbl: ".Averaging method",
	avgMethodIndicatorCode__tlp: ".Averaging method code",
	avgMethodIndicatorName__lbl: ".Averaging method",
	avgMethodIndicatorName__tlp: ".Averaging method name",
	avgMthdId__lbl: ".Avg Method Indicator(ID)",
	calcVal__lbl: ".Using ForEx of",
	calcVal__tlp: ".Using ForEx of",
	financialSourceCode__lbl: ".Source",
	financialSourceCode__tlp: ".Source",
	financialSourceId__lbl: ".Finsource(ID)"
});
