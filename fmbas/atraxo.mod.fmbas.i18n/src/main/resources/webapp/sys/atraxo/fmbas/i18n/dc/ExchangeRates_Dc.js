
Ext.define("atraxo.fmbas.i18n.dc.ExchangeRates_Dc$Edit", {
	averagingMethod__lbl: ".Averaging method",
	fromCurrency__lbl: ".From currency",
	toCurrency__lbl: ".To currency",
	validFrom__lbl: ".Valid from",
	validTo__lbl: ".Valid to"
});

Ext.define("atraxo.fmbas.i18n.dc.ExchangeRates_Dc$Filter", {
	avgMethodIndicatorName__lbl: ".Averaging method",
	currency1Code__lbl: ".From currency",
	currency2Code__lbl: ".Into currency",
	validFrom__lbl: ".Valid from",
	validTo__lbl: ".Valid to"
});

Ext.define("atraxo.fmbas.i18n.dc.ExchangeRates_Dc$List", {
	averagingMethod__lbl: ".Averaging method",
	fromCurrency__lbl: ".From currency",
	toCurrency__lbl: ".To currency",
	validFrom__lbl: ".Valid from",
	validTo__lbl: ".Valid to"
});
