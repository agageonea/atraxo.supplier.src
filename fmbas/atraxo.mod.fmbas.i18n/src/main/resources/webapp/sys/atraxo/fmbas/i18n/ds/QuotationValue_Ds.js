Ext.define("atraxo.fmbas.i18n.ds.QuotationValue_Ds", {
	active__lbl: ".Active",
	active__tlp: ".Active",
	decimals__lbl: ".Decimals",
	decimals__tlp: ".Decimals",
	isProvisioned__lbl: ".Provisional?",
	isProvisioned__tlp: ".Provisional?",
	methodName__lbl: ".Avg Method Indicator",
	methodName__tlp: ".Code",
	quotId__lbl: ".Quotation(ID)",
	quotName__lbl: ".Quotation",
	quotName__tlp: ".Name",
	rate__lbl: ".Value",
	rate__tlp: ".Value",
	validFromDate__lbl: ".Valid from",
	validFromDate__tlp: ".Valid from",
	validToDate__lbl: ".Valid to",
	validToDate__tlp: ".Valid to"
});
