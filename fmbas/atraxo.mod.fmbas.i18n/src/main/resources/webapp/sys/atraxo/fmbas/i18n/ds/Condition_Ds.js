Ext.define("atraxo.fmbas.i18n.ds.Condition_Ds", {
	dictionaryFieldName__lbl: ".Field name",
	dictionaryFieldName__tlp: ".Field name",
	dictionaryId__lbl: ".Dictionary(ID)",
	externalInterfaceId__lbl: ".External Interface(ID)",
	fieldName__lbl: ".Field name",
	fieldName__tlp: ".Field name",
	operator__lbl: ".Operator",
	operator__tlp: ".Operator",
	value__lbl: ".Value",
	value__tlp: ".Value"
});
