Ext.define("atraxo.fmbas.i18n.ds.Tolerance_Ds", {
	absDevDown__lbl: ".Absolute difference down",
	absDevDown__tlp: ".Absolute difference down",
	absDevUp__lbl: ".Absolute difference up",
	absDevUp__tlp: ".Absolute difference up",
	active__lbl: ".Active",
	active__tlp: ".Active",
	currCode__lbl: ".Currency",
	currCode__tlp: ".Currency",
	currId__lbl: ".Currency(ID)",
	description__lbl: ".Description",
	description__tlp: ".Description",
	name__lbl: ".Name",
	name__tlp: ".Name",
	parameterType__lbl: ".Parameter type",
	parameterType__tlp: ".Parameter type",
	percent__lbl: ".%",
	relativeDevDown__lbl: ".Relative difference down",
	relativeDevDown__tlp: ".Relative difference down",
	relativeDevUp__lbl: ".Relative difference up",
	relativeDevUp__tlp: ".Relative difference up",
	system__lbl: ".Is system?",
	system__tlp: ".Is system?",
	tolLmtInd__lbl: ".Limits",
	tolLmtInd__tlp: ".Limits",
	toleranceFor__lbl: ".For",
	toleranceFor__tlp: ".Tolerance for",
	toleranceType__lbl: ".Tolerance type",
	toleranceType__tlp: ".Tolerance type",
	unitCode__lbl: ".Unit",
	unitCode__tlp: ".Unit",
	unitId__lbl: ".Ref Unit(ID)"
});
