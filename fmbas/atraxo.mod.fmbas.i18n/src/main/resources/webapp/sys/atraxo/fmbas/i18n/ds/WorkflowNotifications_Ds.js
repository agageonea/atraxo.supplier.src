Ext.define("atraxo.fmbas.i18n.ds.WorkflowNotifications_Ds", {
	enabled__lbl: ".Enabled",
	enabled__tlp: ".Enabled?",
	notificationCondition__lbl: ".Condition",
	notificationCondition__tlp: ".Condition",
	notificationType__lbl: ".Type",
	notificationType__tlp: ".Type",
	userEmail__lbl: ".eMail address",
	userEmail__tlp: ".Email",
	userId__lbl: ".User(ID)",
	userName__lbl: ".Notification receiver",
	workflowId__lbl: ".Workflow(ID)",
	workflowName__lbl: ".Workflow(Name)",
	workflowName__tlp: ".Workflow name"
});
