Ext.define("atraxo.fmbas.i18n.ds.ChangeHistory_Ds", {
	objectId__lbl: ".Object id",
	objectId__tlp: ".Object id",
	objectType__lbl: ".Object type",
	objectType__tlp: ".Object type",
	objectValue__lbl: ".Object value",
	objectValue__tlp: ".Object value",
	reasonField__lbl: "",
	remarks__lbl: ".Explanation of changes",
	remarks__tlp: ".Explanation of changes"
});
