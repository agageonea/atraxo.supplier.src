/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.exchangerate;

import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ExchangeRateValue} domain entity.
 */
public interface IExchangeRateValueService
		extends
			IEntityService<ExchangeRateValue> {

	/**
	 * Custom service getExchangeRateValue
	 *
	 * @return ExchangeRateValue
	 */
	public ExchangeRateValue getExchangeRateValue(ExchangeRate exchangeRate,
			Date date, Boolean strict) throws BusinessException;

	/**
	 * Custom service getExchangeRateValuesByBusinessKey
	 *
	 * @return ExchangeRateValue
	 */
	public ExchangeRateValue getExchangeRateValuesByBusinessKey(
			ExchangeRateValue exchangeRateValue) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return ExchangeRateValue
	 */
	public ExchangeRateValue findByBusinessKey(ExchangeRate exchRate,
			Date vldFrDtRef, Date vldToDtRef);

	/**
	 * Find by unique key
	 *
	 * @return ExchangeRateValue
	 */
	public ExchangeRateValue findByBusinessKey(Long exchRateId,
			Date vldFrDtRef, Date vldToDtRef);

	/**
	 * Find by unique key
	 *
	 * @return ExchangeRateValue
	 */
	public ExchangeRateValue findByBusiness(Integer id);

	/**
	 * Find by reference: exchRate
	 *
	 * @param exchRate
	 * @return List<ExchangeRateValue>
	 */
	public List<ExchangeRateValue> findByExchRate(ExchangeRate exchRate);

	/**
	 * Find by ID of reference: exchRate.id
	 *
	 * @param exchRateId
	 * @return List<ExchangeRateValue>
	 */
	public List<ExchangeRateValue> findByExchRateId(Integer exchRateId);
}
