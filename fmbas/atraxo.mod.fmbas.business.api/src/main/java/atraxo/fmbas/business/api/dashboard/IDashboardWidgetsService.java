/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.dashboard;

import atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;
import atraxo.fmbas.domain.impl.dashboard.Dashboards;
import atraxo.fmbas.domain.impl.dashboard.Layouts;
import atraxo.fmbas.domain.impl.dashboard.Widgets;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link DashboardWidgets} domain entity.
 */
public interface IDashboardWidgetsService
		extends
			IEntityService<DashboardWidgets> {

	/**
	 * Find by unique key
	 *
	 * @return DashboardWidgets
	 */
	public DashboardWidgets findByBusiness(Integer id);

	/**
	 * Find by reference: dashboard
	 *
	 * @param dashboard
	 * @return List<DashboardWidgets>
	 */
	public List<DashboardWidgets> findByDashboard(Dashboards dashboard);

	/**
	 * Find by ID of reference: dashboard.id
	 *
	 * @param dashboardId
	 * @return List<DashboardWidgets>
	 */
	public List<DashboardWidgets> findByDashboardId(Integer dashboardId);

	/**
	 * Find by reference: layout
	 *
	 * @param layout
	 * @return List<DashboardWidgets>
	 */
	public List<DashboardWidgets> findByLayout(Layouts layout);

	/**
	 * Find by ID of reference: layout.id
	 *
	 * @param layoutId
	 * @return List<DashboardWidgets>
	 */
	public List<DashboardWidgets> findByLayoutId(Integer layoutId);

	/**
	 * Find by reference: widget
	 *
	 * @param widget
	 * @return List<DashboardWidgets>
	 */
	public List<DashboardWidgets> findByWidget(Widgets widget);

	/**
	 * Find by ID of reference: widget.id
	 *
	 * @param widgetId
	 * @return List<DashboardWidgets>
	 */
	public List<DashboardWidgets> findByWidgetId(Integer widgetId);

	/**
	 * Find by reference: assignedWidgetParameters
	 *
	 * @param assignedWidgetParameters
	 * @return List<DashboardWidgets>
	 */
	public List<DashboardWidgets> findByAssignedWidgetParameters(
			AssignedWidgetParameters assignedWidgetParameters);

	/**
	 * Find by ID of reference: assignedWidgetParameters.id
	 *
	 * @param assignedWidgetParametersId
	 * @return List<DashboardWidgets>
	 */
	public List<DashboardWidgets> findByAssignedWidgetParametersId(
			Integer assignedWidgetParametersId);
}
