/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.timeserie;

import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieAverage;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link TimeSerieAverage} domain entity.
 */
public interface ITimeSerieAverageService
		extends
			IEntityService<TimeSerieAverage> {

	/**
	 * Find by unique key
	 *
	 * @return TimeSerieAverage
	 */
	public TimeSerieAverage findByTserieAvg(TimeSerie tserie,
			AverageMethod averagingMethod);

	/**
	 * Find by unique key
	 *
	 * @return TimeSerieAverage
	 */
	public TimeSerieAverage findByTserieAvg(Long tserieId,
			Long averagingMethodId);

	/**
	 * Find by unique key
	 *
	 * @return TimeSerieAverage
	 */
	public TimeSerieAverage findByBusiness(Integer id);

	/**
	 * Find by reference: tserie
	 *
	 * @param tserie
	 * @return List<TimeSerieAverage>
	 */
	public List<TimeSerieAverage> findByTserie(TimeSerie tserie);

	/**
	 * Find by ID of reference: tserie.id
	 *
	 * @param tserieId
	 * @return List<TimeSerieAverage>
	 */
	public List<TimeSerieAverage> findByTserieId(Integer tserieId);

	/**
	 * Find by reference: averagingMethod
	 *
	 * @param averagingMethod
	 * @return List<TimeSerieAverage>
	 */
	public List<TimeSerieAverage> findByAveragingMethod(
			AverageMethod averagingMethod);

	/**
	 * Find by ID of reference: averagingMethod.id
	 *
	 * @param averagingMethodId
	 * @return List<TimeSerieAverage>
	 */
	public List<TimeSerieAverage> findByAveragingMethodId(
			Integer averagingMethodId);
}
