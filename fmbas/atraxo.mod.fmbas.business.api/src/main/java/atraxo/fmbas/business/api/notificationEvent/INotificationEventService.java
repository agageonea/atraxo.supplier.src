/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.notificationEvent;

import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link NotificationEvent} domain entity.
 */
public interface INotificationEventService
		extends
			IEntityService<NotificationEvent> {

	/**
	 * Find by unique key
	 *
	 * @return NotificationEvent
	 */
	public NotificationEvent findByName(String name);

	/**
	 * Find by unique key
	 *
	 * @return NotificationEvent
	 */
	public NotificationEvent findByBusiness(Integer id);

	/**
	 * Find by reference: contact
	 *
	 * @param contact
	 * @return List<NotificationEvent>
	 */
	public List<NotificationEvent> findByContact(Contacts contact);

	/**
	 * Find by ID of reference: contact.id
	 *
	 * @param contactId
	 * @return List<NotificationEvent>
	 */
	public List<NotificationEvent> findByContactId(Integer contactId);
}
