/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.job;

import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.ActionParameter;
import atraxo.fmbas.domain.impl.job.BatchJobParameter;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ActionParameter} domain entity.
 */
public interface IActionParameterService
		extends
			IEntityService<ActionParameter> {

	/**
	 * Find by unique key
	 *
	 * @return ActionParameter
	 */
	public ActionParameter findByBusiness(Integer id);

	/**
	 * Find by reference: jobAction
	 *
	 * @param jobAction
	 * @return List<ActionParameter>
	 */
	public List<ActionParameter> findByJobAction(Action jobAction);

	/**
	 * Find by ID of reference: jobAction.id
	 *
	 * @param jobActionId
	 * @return List<ActionParameter>
	 */
	public List<ActionParameter> findByJobActionId(Integer jobActionId);

	/**
	 * Find by reference: batchJobParameter
	 *
	 * @param batchJobParameter
	 * @return List<ActionParameter>
	 */
	public List<ActionParameter> findByBatchJobParameter(
			BatchJobParameter batchJobParameter);

	/**
	 * Find by ID of reference: batchJobParameter.id
	 *
	 * @param batchJobParameterId
	 * @return List<ActionParameter>
	 */
	public List<ActionParameter> findByBatchJobParameterId(
			Integer batchJobParameterId);
}
