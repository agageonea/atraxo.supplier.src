/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.validationMessage;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.ValidationMessageSeverity;
import atraxo.fmbas.domain.impl.validationMessage.ValidationMessage;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ValidationMessage} domain entity.
 */
public interface IValidationMessageService
		extends
			IEntityService<ValidationMessage> {

	/**
	 * Custom service deleteValidationMessage
	 *
	 * @return void
	 */
	public void deleteValidationMessage(Integer id, Class<?> c)
			throws BusinessException;

	/**
	 * Custom service deleteValidationMessages
	 *
	 * @return void
	 */
	public void deleteValidationMessages(List ids, Class<?> c)
			throws BusinessException;

	/**
	 * Custom service deleteByEntities
	 *
	 * @return void
	 */
	public void deleteByEntities(List<? extends AbstractEntity> entities,
			Class<?> c) throws BusinessException;

	/**
	 * Custom service createValidationMessage
	 *
	 * @return ValidationMessage
	 */
	public ValidationMessage createValidationMessage(AbstractEntity entity,
			String message, ValidationMessageSeverity severity)
			throws BusinessException;

	/**
	 * Custom service getInvalidObjectIds
	 *
	 * @return List<String>
	 */
	public List<String> getInvalidObjectIds(String objectTye,
			List<? extends AbstractEntity> entities) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return ValidationMessage
	 */
	public ValidationMessage findByBusiness(Integer id);
}
