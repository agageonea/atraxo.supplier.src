/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.ac;

import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link AcTypes} domain entity.
 */
public interface IAcTypesService extends IEntityService<AcTypes> {

	/**
	 * Find by unique key
	 *
	 * @return AcTypes
	 */
	public AcTypes findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return AcTypes
	 */
	public AcTypes findByBusiness(Integer id);

	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<AcTypes>
	 */
	public List<AcTypes> findByUnit(Unit unit);

	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<AcTypes>
	 */
	public List<AcTypes> findByUnitId(Integer unitId);
}
