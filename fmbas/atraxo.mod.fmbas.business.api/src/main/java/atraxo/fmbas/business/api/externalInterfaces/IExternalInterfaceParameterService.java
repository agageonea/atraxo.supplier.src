/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.externalInterfaces;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceParameter;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ExternalInterfaceParameter} domain entity.
 */
public interface IExternalInterfaceParameterService
		extends
			IEntityService<ExternalInterfaceParameter> {

	/**
	 * Find by unique key
	 *
	 * @return ExternalInterfaceParameter
	 */
	public ExternalInterfaceParameter findByKey(
			ExternalInterface externalInterface, String name);

	/**
	 * Find by unique key
	 *
	 * @return ExternalInterfaceParameter
	 */
	public ExternalInterfaceParameter findByKey(Long externalInterfaceId,
			String name);

	/**
	 * Find by unique key
	 *
	 * @return ExternalInterfaceParameter
	 */
	public ExternalInterfaceParameter findByBusiness(Integer id);

	/**
	 * Find by reference: externalInterface
	 *
	 * @param externalInterface
	 * @return List<ExternalInterfaceParameter>
	 */
	public List<ExternalInterfaceParameter> findByExternalInterface(
			ExternalInterface externalInterface);

	/**
	 * Find by ID of reference: externalInterface.id
	 *
	 * @param externalInterfaceId
	 * @return List<ExternalInterfaceParameter>
	 */
	public List<ExternalInterfaceParameter> findByExternalInterfaceId(
			Integer externalInterfaceId);
}
