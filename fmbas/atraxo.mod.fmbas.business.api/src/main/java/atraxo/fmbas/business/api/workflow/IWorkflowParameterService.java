/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.workflow;

import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link WorkflowParameter} domain entity.
 */
public interface IWorkflowParameterService
		extends
			IEntityService<WorkflowParameter> {

	/**
	 * Find by unique key
	 *
	 * @return WorkflowParameter
	 */
	public WorkflowParameter findByKey(Workflow workflow, String name);

	/**
	 * Find by unique key
	 *
	 * @return WorkflowParameter
	 */
	public WorkflowParameter findByKey(Long workflowId, String name);

	/**
	 * Find by unique key
	 *
	 * @return WorkflowParameter
	 */
	public WorkflowParameter findByBusiness(Integer id);

	/**
	 * Find by reference: workflow
	 *
	 * @param workflow
	 * @return List<WorkflowParameter>
	 */
	public List<WorkflowParameter> findByWorkflow(Workflow workflow);

	/**
	 * Find by ID of reference: workflow.id
	 *
	 * @param workflowId
	 * @return List<WorkflowParameter>
	 */
	public List<WorkflowParameter> findByWorkflowId(Integer workflowId);
}
