/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.aircraft;

import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.customer.Customer;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Aircraft} domain entity.
 */
public interface IAircraftService extends IEntityService<Aircraft> {

	/**
	 * Find by unique key
	 *
	 * @return Aircraft
	 */
	public Aircraft findByReg(Customer customer, String registration);

	/**
	 * Find by unique key
	 *
	 * @return Aircraft
	 */
	public Aircraft findByReg(Long customerId, String registration);

	/**
	 * Find by unique key
	 *
	 * @return Aircraft
	 */
	public Aircraft findByBusiness(Integer id);

	/**
	 * Find by reference: acTypes
	 *
	 * @param acTypes
	 * @return List<Aircraft>
	 */
	public List<Aircraft> findByAcTypes(AcTypes acTypes);

	/**
	 * Find by ID of reference: acTypes.id
	 *
	 * @param acTypesId
	 * @return List<Aircraft>
	 */
	public List<Aircraft> findByAcTypesId(Integer acTypesId);

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<Aircraft>
	 */
	public List<Aircraft> findByCustomer(Customer customer);

	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<Aircraft>
	 */
	public List<Aircraft> findByCustomerId(Integer customerId);

	/**
	 * Find by reference: owner
	 *
	 * @param owner
	 * @return List<Aircraft>
	 */
	public List<Aircraft> findByOwner(Customer owner);

	/**
	 * Find by ID of reference: owner.id
	 *
	 * @param ownerId
	 * @return List<Aircraft>
	 */
	public List<Aircraft> findByOwnerId(Integer ownerId);

	/**
	 * Find by reference: operator
	 *
	 * @param operator
	 * @return List<Aircraft>
	 */
	public List<Aircraft> findByOperator(Customer operator);

	/**
	 * Find by ID of reference: operator.id
	 *
	 * @param operatorId
	 * @return List<Aircraft>
	 */
	public List<Aircraft> findByOperatorId(Integer operatorId);
}
