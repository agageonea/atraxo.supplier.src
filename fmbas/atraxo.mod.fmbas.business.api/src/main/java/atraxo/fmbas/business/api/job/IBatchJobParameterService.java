/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.job;

import atraxo.fmbas.domain.impl.job.BatchJob;
import atraxo.fmbas.domain.impl.job.BatchJobParameter;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link BatchJobParameter} domain entity.
 */
public interface IBatchJobParameterService
		extends
			IEntityService<BatchJobParameter> {

	/**
	 * Find by unique key
	 *
	 * @return BatchJobParameter
	 */
	public BatchJobParameter findByName(BatchJob batchJob, String name);

	/**
	 * Find by unique key
	 *
	 * @return BatchJobParameter
	 */
	public BatchJobParameter findByName(Long batchJobId, String name);

	/**
	 * Find by unique key
	 *
	 * @return BatchJobParameter
	 */
	public BatchJobParameter findByBusiness(Integer id);

	/**
	 * Find by reference: batchJob
	 *
	 * @param batchJob
	 * @return List<BatchJobParameter>
	 */
	public List<BatchJobParameter> findByBatchJob(BatchJob batchJob);

	/**
	 * Find by ID of reference: batchJob.id
	 *
	 * @param batchJobId
	 * @return List<BatchJobParameter>
	 */
	public List<BatchJobParameter> findByBatchJobId(Integer batchJobId);
}
