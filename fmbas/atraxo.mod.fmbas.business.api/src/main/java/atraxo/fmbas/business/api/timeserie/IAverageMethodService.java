/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.timeserie;

import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link AverageMethod} domain entity.
 */
public interface IAverageMethodService extends IEntityService<AverageMethod> {

	/**
	 * Custom service getDefaultMethod
	 *
	 * @return AverageMethod
	 */
	public AverageMethod getDefaultMethod() throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return AverageMethod
	 */
	public AverageMethod findByName(String code);

	/**
	 * Find by unique key
	 *
	 * @return AverageMethod
	 */
	public AverageMethod findByBusiness(Integer id);
}
