/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.trade;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.trade.TradeReference;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link TradeReference} domain entity.
 */
public interface ITradeReferenceService extends IEntityService<TradeReference> {

	/**
	 * Find by unique key
	 *
	 * @return TradeReference
	 */
	public TradeReference findByBusiness(Integer id);

	/**
	 * Find by reference: company
	 *
	 * @param company
	 * @return List<TradeReference>
	 */
	public List<TradeReference> findByCompany(Customer company);

	/**
	 * Find by ID of reference: company.id
	 *
	 * @param companyId
	 * @return List<TradeReference>
	 */
	public List<TradeReference> findByCompanyId(Integer companyId);
}
