/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.dictionary;

import atraxo.fmbas.domain.impl.dictionary.Condition;
import atraxo.fmbas.domain.impl.dictionary.Dictionary;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Condition} domain entity.
 */
public interface IConditionService extends IEntityService<Condition> {

	/**
	 * Find by unique key
	 *
	 * @return Condition
	 */
	public Condition findByBusiness(Integer id);

	/**
	 * Find by reference: dictionary
	 *
	 * @param dictionary
	 * @return List<Condition>
	 */
	public List<Condition> findByDictionary(Dictionary dictionary);

	/**
	 * Find by ID of reference: dictionary.id
	 *
	 * @param dictionaryId
	 * @return List<Condition>
	 */
	public List<Condition> findByDictionaryId(Integer dictionaryId);
}
