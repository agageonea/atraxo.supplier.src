package atraxo.fmbas.business.api.ext.ws;

import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public interface IIncomingMessageProcessor {
	/**
	 * @param request
	 * @param beanName
	 * @param syncMethod
	 * @return
	 */
	MSG process(MSG request, String beanName, String syncMethod) throws BusinessException;
}
