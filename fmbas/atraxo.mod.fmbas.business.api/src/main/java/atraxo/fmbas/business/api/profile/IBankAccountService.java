/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.profile;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.profile.BankAccount;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link BankAccount} domain entity.
 */
public interface IBankAccountService extends IEntityService<BankAccount> {

	/**
	 * Find by unique key
	 *
	 * @return BankAccount
	 */
	public BankAccount findByBankAccount(String accountNumber,
			Currencies currency, Customer subsidiary);

	/**
	 * Find by unique key
	 *
	 * @return BankAccount
	 */
	public BankAccount findByBankAccount(String accountNumber, Long currencyId,
			Long subsidiaryId);

	/**
	 * Find by unique key
	 *
	 * @return BankAccount
	 */
	public BankAccount findByBusiness(Integer id);

	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<BankAccount>
	 */
	public List<BankAccount> findByCurrency(Currencies currency);

	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<BankAccount>
	 */
	public List<BankAccount> findByCurrencyId(Integer currencyId);

	/**
	 * Find by reference: subsidiary
	 *
	 * @param subsidiary
	 * @return List<BankAccount>
	 */
	public List<BankAccount> findBySubsidiary(Customer subsidiary);

	/**
	 * Find by ID of reference: subsidiary.id
	 *
	 * @param subsidiaryId
	 * @return List<BankAccount>
	 */
	public List<BankAccount> findBySubsidiaryId(Integer subsidiaryId);

	/**
	 * Find by reference: customers
	 *
	 * @param customers
	 * @return List<BankAccount>
	 */
	public List<BankAccount> findByCustomers(Customer customers);

	/**
	 * Find by ID of reference: customers.id
	 *
	 * @param customersId
	 * @return List<BankAccount>
	 */
	public List<BankAccount> findByCustomersId(Integer customersId);
}
