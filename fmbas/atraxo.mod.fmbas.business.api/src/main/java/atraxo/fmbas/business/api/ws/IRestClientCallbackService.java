package atraxo.fmbas.business.api.ws;

import java.util.Collection;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public interface IRestClientCallbackService<E extends AbstractEntity> {

	/**
	 * @param result
	 * @param list
	 */
	void finish(WSExecutionResult result, Collection<E> list) throws BusinessException;
}
