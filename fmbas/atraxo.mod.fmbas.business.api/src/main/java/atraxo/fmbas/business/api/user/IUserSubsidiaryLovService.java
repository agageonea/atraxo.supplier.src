/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.user;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.user.UserSubsidiaryLov;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link UserSubsidiaryLov} domain entity.
 */
public interface IUserSubsidiaryLovService
		extends
			IEntityService<UserSubsidiaryLov> {

	/**
	 * Find by unique key
	 *
	 * @return UserSubsidiaryLov
	 */
	public UserSubsidiaryLov findByKey(UserSupp user, Customer subsidiary);

	/**
	 * Find by unique key
	 *
	 * @return UserSubsidiaryLov
	 */
	public UserSubsidiaryLov findByKey(Long userId, Long subsidiaryId);

	/**
	 * Find by unique key
	 *
	 * @return UserSubsidiaryLov
	 */
	public UserSubsidiaryLov findByBusiness(Integer id);

	/**
	 * Find by reference: user
	 *
	 * @param user
	 * @return List<UserSubsidiaryLov>
	 */
	public List<UserSubsidiaryLov> findByUser(UserSupp user);

	/**
	 * Find by ID of reference: user.id
	 *
	 * @param userId
	 * @return List<UserSubsidiaryLov>
	 */
	public List<UserSubsidiaryLov> findByUserId(String userId);

	/**
	 * Find by reference: subsidiary
	 *
	 * @param subsidiary
	 * @return List<UserSubsidiaryLov>
	 */
	public List<UserSubsidiaryLov> findBySubsidiary(Customer subsidiary);

	/**
	 * Find by ID of reference: subsidiary.id
	 *
	 * @param subsidiaryId
	 * @return List<UserSubsidiaryLov>
	 */
	public List<UserSubsidiaryLov> findBySubsidiaryId(Integer subsidiaryId);
}
