/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.uom;

import atraxo.fmbas.domain.impl.uom.FisLov;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FisLov} domain entity.
 */
public interface IFisLovService extends IEntityService<FisLov> {

	/**
	 * Find by unique key
	 *
	 * @return FisLov
	 */
	public FisLov findById(Integer id);

	/**
	 * Find by unique key
	 *
	 * @return FisLov
	 */
	public FisLov findByBusiness(Integer id);
}
