/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.soneDefaultViewState;

import atraxo.ad.domain.impl.security.User;
import atraxo.fmbas.domain.impl.soneDefaultViewState.SoneDefaultViewState;
import atraxo.fmbas.domain.impl.soneViewState.SoneViewState;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link SoneDefaultViewState} domain entity.
 */
public interface ISoneDefaultViewStateService
		extends
			IEntityService<SoneDefaultViewState> {

	/**
	 * Custom service getDefaultViewStates
	 *
	 * @return List<SoneDefaultViewState>
	 */
	public List<SoneDefaultViewState> getDefaultViewStates(String cmp)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return SoneDefaultViewState
	 */
	public SoneDefaultViewState findByBusiness(Integer id);

	/**
	 * Find by reference: user
	 *
	 * @param user
	 * @return List<SoneDefaultViewState>
	 */
	public List<SoneDefaultViewState> findByUser(User user);

	/**
	 * Find by ID of reference: user.id
	 *
	 * @param userId
	 * @return List<SoneDefaultViewState>
	 */
	public List<SoneDefaultViewState> findByUserId(String userId);

	/**
	 * Find by reference: view
	 *
	 * @param view
	 * @return List<SoneDefaultViewState>
	 */
	public List<SoneDefaultViewState> findByView(SoneViewState view);

	/**
	 * Find by ID of reference: view.id
	 *
	 * @param viewId
	 * @return List<SoneDefaultViewState>
	 */
	public List<SoneDefaultViewState> findByViewId(Integer viewId);
}
