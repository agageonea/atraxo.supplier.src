/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.customer;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.customer.CustomerNotification;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import atraxo.fmbas.domain.impl.templates.Template;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link CustomerNotification} domain entity.
 */
public interface ICustomerNotificationService
		extends
			IEntityService<CustomerNotification> {

	/**
	 * Find by unique key
	 *
	 * @return CustomerNotification
	 */
	public CustomerNotification findByKey(Customer customer,
			NotificationEvent notificationEvent, Area assignedArea);

	/**
	 * Find by unique key
	 *
	 * @return CustomerNotification
	 */
	public CustomerNotification findByKey(Long customerId,
			Long notificationEventId, Long assignedAreaId);

	/**
	 * Find by unique key
	 *
	 * @return CustomerNotification
	 */
	public CustomerNotification findByBusiness(Integer id);

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByCustomer(Customer customer);

	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByCustomerId(Integer customerId);

	/**
	 * Find by reference: template
	 *
	 * @param template
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByTemplate(Template template);

	/**
	 * Find by ID of reference: template.id
	 *
	 * @param templateId
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByTemplateId(Integer templateId);

	/**
	 * Find by reference: emailBody
	 *
	 * @param emailBody
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByEmailBody(Template emailBody);

	/**
	 * Find by ID of reference: emailBody.id
	 *
	 * @param emailBodyId
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByEmailBodyId(Integer emailBodyId);

	/**
	 * Find by reference: notificationEvent
	 *
	 * @param notificationEvent
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByNotificationEvent(
			NotificationEvent notificationEvent);

	/**
	 * Find by ID of reference: notificationEvent.id
	 *
	 * @param notificationEventId
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByNotificationEventId(
			Integer notificationEventId);

	/**
	 * Find by reference: assignedArea
	 *
	 * @param assignedArea
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByAssignedArea(Area assignedArea);

	/**
	 * Find by ID of reference: assignedArea.id
	 *
	 * @param assignedAreaId
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByAssignedAreaId(
			Integer assignedAreaId);
}
