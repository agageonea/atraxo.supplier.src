/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.workflow;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowNotification;
import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Workflow} domain entity.
 */
public interface IWorkflowService extends IEntityService<Workflow> {

	/**
	 * Custom service findByNameAndSubsidiary
	 *
	 * @return Workflow
	 */
	public Workflow findByNameAndSubsidiary(String workflowName,
			String subsidiaryCode) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Workflow
	 */
	public Workflow findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return Workflow
	 */
	public Workflow findByBusiness(Integer id);

	/**
	 * Find by reference: subsidiary
	 *
	 * @param subsidiary
	 * @return List<Workflow>
	 */
	public List<Workflow> findBySubsidiary(Customer subsidiary);

	/**
	 * Find by ID of reference: subsidiary.id
	 *
	 * @param subsidiaryId
	 * @return List<Workflow>
	 */
	public List<Workflow> findBySubsidiaryId(Integer subsidiaryId);

	/**
	 * Find by reference: notifications
	 *
	 * @param notifications
	 * @return List<Workflow>
	 */
	public List<Workflow> findByNotifications(WorkflowNotification notifications);

	/**
	 * Find by ID of reference: notifications.id
	 *
	 * @param notificationsId
	 * @return List<Workflow>
	 */
	public List<Workflow> findByNotificationsId(Integer notificationsId);

	/**
	 * Find by reference: instances
	 *
	 * @param instances
	 * @return List<Workflow>
	 */
	public List<Workflow> findByInstances(WorkflowInstance instances);

	/**
	 * Find by ID of reference: instances.id
	 *
	 * @param instancesId
	 * @return List<Workflow>
	 */
	public List<Workflow> findByInstancesId(Integer instancesId);

	/**
	 * Find by reference: params
	 *
	 * @param params
	 * @return List<Workflow>
	 */
	public List<Workflow> findByParams(WorkflowParameter params);

	/**
	 * Find by ID of reference: params.id
	 *
	 * @param paramsId
	 * @return List<Workflow>
	 */
	public List<Workflow> findByParamsId(Integer paramsId);
}
