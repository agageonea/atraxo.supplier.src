/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.abstracts;

import atraxo.fmbas.domain.impl.abstracts.DummyTypeWithSubSidiary;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link DummyTypeWithSubSidiary} domain entity.
 */
public interface IDummyTypeWithSubSidiaryService
		extends
			IEntityService<DummyTypeWithSubSidiary> {

	/**
	 * Find by unique key
	 *
	 * @return DummyTypeWithSubSidiary
	 */
	public DummyTypeWithSubSidiary findByBusiness(Integer id);
}
