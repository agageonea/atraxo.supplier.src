/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.job;

import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.ActionParameter;
import atraxo.fmbas.domain.impl.job.BatchJob;
import atraxo.fmbas.domain.impl.job.JobChain;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Action} domain entity.
 */
public interface IActionService extends IEntityService<Action> {

	/**
	 * Custom service move
	 *
	 * @return void
	 */
	public void move(Action e, int step) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Action
	 */
	public Action findByBusiness(Integer id);

	/**
	 * Find by reference: jobChain
	 *
	 * @param jobChain
	 * @return List<Action>
	 */
	public List<Action> findByJobChain(JobChain jobChain);

	/**
	 * Find by ID of reference: jobChain.id
	 *
	 * @param jobChainId
	 * @return List<Action>
	 */
	public List<Action> findByJobChainId(Integer jobChainId);

	/**
	 * Find by reference: nextAction
	 *
	 * @param nextAction
	 * @return List<Action>
	 */
	public List<Action> findByNextAction(Action nextAction);

	/**
	 * Find by ID of reference: nextAction.id
	 *
	 * @param nextActionId
	 * @return List<Action>
	 */
	public List<Action> findByNextActionId(Integer nextActionId);

	/**
	 * Find by reference: batchJob
	 *
	 * @param batchJob
	 * @return List<Action>
	 */
	public List<Action> findByBatchJob(BatchJob batchJob);

	/**
	 * Find by ID of reference: batchJob.id
	 *
	 * @param batchJobId
	 * @return List<Action>
	 */
	public List<Action> findByBatchJobId(Integer batchJobId);

	/**
	 * Find by reference: actionParameters
	 *
	 * @param actionParameters
	 * @return List<Action>
	 */
	public List<Action> findByActionParameters(ActionParameter actionParameters);

	/**
	 * Find by ID of reference: actionParameters.id
	 *
	 * @param actionParametersId
	 * @return List<Action>
	 */
	public List<Action> findByActionParametersId(Integer actionParametersId);
}
