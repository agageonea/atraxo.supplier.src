/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.externalInterfaces;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ExternalInterfaceMessageHistory} domain entity.
 */
public interface IExternalInterfaceMessageHistoryService
		extends
			IEntityService<ExternalInterfaceMessageHistory> {

	/**
	 * Custom service findByObjIdType
	 *
	 * @return List<ExternalInterfaceMessageHistory>
	 */
	public List<ExternalInterfaceMessageHistory> findByObjIdType(
			Integer objectId, String objectType) throws BusinessException;

	/**
	 * Custom service findByMessageId
	 *
	 * @return List<ExternalInterfaceMessageHistory>
	 */
	public List<ExternalInterfaceMessageHistory> findByMessageId(
			String messageId) throws BusinessException;

	/**
	 * Custom service findByResponseMessageId
	 *
	 * @return List<ExternalInterfaceMessageHistory>
	 */
	public List<ExternalInterfaceMessageHistory> findByResponseMessageId(
			String messageId) throws BusinessException;

	/**
	 * Custom service isLastMessage
	 *
	 * @return Boolean
	 */
	public Boolean isLastMessage(Integer objectId, String objectType,
			String messageId) throws BusinessException;

	/**
	 * Custom service findLastMessage
	 *
	 * @return ExternalInterfaceMessageHistory
	 */
	public ExternalInterfaceMessageHistory findLastMessage(Integer objectId,
			String objectType) throws BusinessException;

	/**
	 * Custom service updateMessageHistory
	 *
	 * @return void
	 */
	public void updateMessageHistory(MSG msg,
			ExternalInterfaceMessageHistoryStatus status)
			throws BusinessException;

	/**
	 * Custom service uploadToS3
	 *
	 * @return void
	 */
	public void uploadToS3(String message, String messageId, Boolean sent)
			throws BusinessException;

	/**
	 * Custom service readFromS3
	 *
	 * @return String
	 */
	public String readFromS3(String msdId, Boolean sent)
			throws BusinessException;

	/**
	 * Custom service getOldestExportedMessage
	 *
	 * @return ExternalInterfaceMessageHistory
	 */
	public ExternalInterfaceMessageHistory getOldestExportedMessage()
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return ExternalInterfaceMessageHistory
	 */
	public ExternalInterfaceMessageHistory findByBusiness(Integer id);

	/**
	 * Find by index key
	 *
	 * @return List<ExternalInterfaceMessageHistory>
	 */
	public List<ExternalInterfaceMessageHistory> findByIdxMessage_id(
			String messageId);

	/**
	 * Find by reference: externalInterface
	 *
	 * @param externalInterface
	 * @return List<ExternalInterfaceMessageHistory>
	 */
	public List<ExternalInterfaceMessageHistory> findByExternalInterface(
			ExternalInterface externalInterface);

	/**
	 * Find by ID of reference: externalInterface.id
	 *
	 * @param externalInterfaceId
	 * @return List<ExternalInterfaceMessageHistory>
	 */
	public List<ExternalInterfaceMessageHistory> findByExternalInterfaceId(
			Integer externalInterfaceId);
}
