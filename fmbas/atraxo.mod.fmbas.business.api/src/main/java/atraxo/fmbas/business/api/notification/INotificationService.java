/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.notification;

import atraxo.fmbas.domain.impl.fmbas_type.NotificationType;
import atraxo.fmbas.domain.impl.notification.Notification;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import java.util.Map;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Notification} domain entity.
 */
public interface INotificationService extends IEntityService<Notification> {

	/**
	 * Custom service findForUser
	 *
	 * @return List<Notification>
	 */
	public List<Notification> findForUser(UserSupp user)
			throws BusinessException;

	/**
	 * Custom service findForUser
	 *
	 * @return List<Notification>
	 */
	public List<Notification> findForUser(String code) throws BusinessException;

	/**
	 * Custom service deleteForUser
	 *
	 * @return void
	 */
	public void deleteForUser(String code) throws BusinessException;

	/**
	 * Custom service sendAndSave
	 *
	 * @return void
	 */
	public void sendAndSave(Notification e, List<UserSupp> emailUsers,
			Map objectMap) throws BusinessException;

	/**
	 * Custom service addUsersToNotification
	 *
	 * @return void
	 */
	public void addUsersToNotification(Notification e, UserSupp u,
			NotificationType type, List<UserSupp> emailUsers)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Notification
	 */
	public Notification findByBusiness(Integer id);

	/**
	 * Find by reference: users
	 *
	 * @param users
	 * @return List<Notification>
	 */
	public List<Notification> findByUsers(UserSupp users);

	/**
	 * Find by ID of reference: users.id
	 *
	 * @param usersId
	 * @return List<Notification>
	 */
	public List<Notification> findByUsersId(String usersId);
}
