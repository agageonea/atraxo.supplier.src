/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.timeserie;

import atraxo.fmbas.domain.impl.timeserie.RawTimeSerieItem;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link RawTimeSerieItem} domain entity.
 */
public interface IRawTimeSerieItemService
		extends
			IEntityService<RawTimeSerieItem> {

	/**
	 * Custom service insertForComposite
	 *
	 * @return void
	 */
	public void insertForComposite(List<RawTimeSerieItem> list)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return RawTimeSerieItem
	 */
	public RawTimeSerieItem findByTsi_business(TimeSerie tserie, Date itemDate);

	/**
	 * Find by unique key
	 *
	 * @return RawTimeSerieItem
	 */
	public RawTimeSerieItem findByTsi_business(Long tserieId, Date itemDate);

	/**
	 * Find by unique key
	 *
	 * @return RawTimeSerieItem
	 */
	public RawTimeSerieItem findByBusiness(Integer id);

	/**
	 * Find by reference: tserie
	 *
	 * @param tserie
	 * @return List<RawTimeSerieItem>
	 */
	public List<RawTimeSerieItem> findByTserie(TimeSerie tserie);

	/**
	 * Find by ID of reference: tserie.id
	 *
	 * @param tserieId
	 * @return List<RawTimeSerieItem>
	 */
	public List<RawTimeSerieItem> findByTserieId(Integer tserieId);
}
