/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.financialSources;

import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FinancialSources} domain entity.
 */
public interface IFinancialSourcesService
		extends
			IEntityService<FinancialSources> {

	/**
	 * Custom service getStandardFinancialSource
	 *
	 * @return FinancialSources
	 */
	public FinancialSources getStandardFinancialSource()
			throws BusinessException;

	/**
	 * Custom service setStandardFinancialSource
	 *
	 * @return void
	 */
	public void setStandardFinancialSource(String fsCode)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return FinancialSources
	 */
	public FinancialSources findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return FinancialSources
	 */
	public FinancialSources findByBusiness(Integer id);
}
