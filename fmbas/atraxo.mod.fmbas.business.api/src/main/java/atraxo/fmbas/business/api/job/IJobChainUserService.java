/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.job;

import atraxo.fmbas.domain.impl.job.JobChain;
import atraxo.fmbas.domain.impl.job.JobChainUser;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link JobChainUser} domain entity.
 */
public interface IJobChainUserService extends IEntityService<JobChainUser> {

	/**
	 * Find by unique key
	 *
	 * @return JobChainUser
	 */
	public JobChainUser findByKey(JobChain jobChain, UserSupp user);

	/**
	 * Find by unique key
	 *
	 * @return JobChainUser
	 */
	public JobChainUser findByKey(Long jobChainId, Long userId);

	/**
	 * Find by unique key
	 *
	 * @return JobChainUser
	 */
	public JobChainUser findByBusiness(Integer id);

	/**
	 * Find by reference: jobChain
	 *
	 * @param jobChain
	 * @return List<JobChainUser>
	 */
	public List<JobChainUser> findByJobChain(JobChain jobChain);

	/**
	 * Find by ID of reference: jobChain.id
	 *
	 * @param jobChainId
	 * @return List<JobChainUser>
	 */
	public List<JobChainUser> findByJobChainId(Integer jobChainId);

	/**
	 * Find by reference: user
	 *
	 * @param user
	 * @return List<JobChainUser>
	 */
	public List<JobChainUser> findByUser(UserSupp user);

	/**
	 * Find by ID of reference: user.id
	 *
	 * @param userId
	 * @return List<JobChainUser>
	 */
	public List<JobChainUser> findByUserId(String userId);
}
