/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.workflow;

import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.ad.domain.impl.security.User;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link WorkflowInstance} domain entity.
 */
public interface IWorkflowInstanceService
		extends
			IEntityService<WorkflowInstance> {

	/**
	 * Custom service createAttachment
	 *
	 * @return Attachment
	 */
	public Attachment createAttachment(WorkflowInstance instance, String name,
			AttachmentType type, String targetAlias, String notes,
			String extension, Boolean active) throws BusinessException;

	/**
	 * Custom service insertAndPersist
	 *
	 * @return void
	 */
	public void insertAndPersist(WorkflowInstance instance)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return WorkflowInstance
	 */
	public WorkflowInstance findByKey(Workflow workflow, String name);

	/**
	 * Find by unique key
	 *
	 * @return WorkflowInstance
	 */
	public WorkflowInstance findByKey(Long workflowId, String name);

	/**
	 * Find by unique key
	 *
	 * @return WorkflowInstance
	 */
	public WorkflowInstance findByBusiness(Integer id);

	/**
	 * Find by reference: workflow
	 *
	 * @param workflow
	 * @return List<WorkflowInstance>
	 */
	public List<WorkflowInstance> findByWorkflow(Workflow workflow);

	/**
	 * Find by ID of reference: workflow.id
	 *
	 * @param workflowId
	 * @return List<WorkflowInstance>
	 */
	public List<WorkflowInstance> findByWorkflowId(Integer workflowId);

	/**
	 * Find by reference: startExecutionUser
	 *
	 * @param startExecutionUser
	 * @return List<WorkflowInstance>
	 */
	public List<WorkflowInstance> findByStartExecutionUser(
			User startExecutionUser);

	/**
	 * Find by ID of reference: startExecutionUser.id
	 *
	 * @param startExecutionUserId
	 * @return List<WorkflowInstance>
	 */
	public List<WorkflowInstance> findByStartExecutionUserId(
			String startExecutionUserId);

	/**
	 * Find by reference: entitites
	 *
	 * @param entitites
	 * @return List<WorkflowInstance>
	 */
	public List<WorkflowInstance> findByEntitites(
			WorkflowInstanceEntity entitites);

	/**
	 * Find by ID of reference: entitites.id
	 *
	 * @param entititesId
	 * @return List<WorkflowInstance>
	 */
	public List<WorkflowInstance> findByEntititesId(Integer entititesId);
}
