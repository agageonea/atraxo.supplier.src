/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.masteragreements;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.masteragreements.MasterAgreement;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link MasterAgreement} domain entity.
 */
public interface IMasterAgreementService
		extends
			IEntityService<MasterAgreement> {

	/**
	 * Find by unique key
	 *
	 * @return MasterAgreement
	 */
	public MasterAgreement findByBusiness(Integer id);

	/**
	 * Find by reference: company
	 *
	 * @param company
	 * @return List<MasterAgreement>
	 */
	public List<MasterAgreement> findByCompany(Customer company);

	/**
	 * Find by ID of reference: company.id
	 *
	 * @param companyId
	 * @return List<MasterAgreement>
	 */
	public List<MasterAgreement> findByCompanyId(Integer companyId);

	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<MasterAgreement>
	 */
	public List<MasterAgreement> findByCurrency(Currencies currency);

	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<MasterAgreement>
	 */
	public List<MasterAgreement> findByCurrencyId(Integer currencyId);

	/**
	 * Find by reference: financialsource
	 *
	 * @param financialsource
	 * @return List<MasterAgreement>
	 */
	public List<MasterAgreement> findByFinancialsource(
			FinancialSources financialsource);

	/**
	 * Find by ID of reference: financialsource.id
	 *
	 * @param financialsourceId
	 * @return List<MasterAgreement>
	 */
	public List<MasterAgreement> findByFinancialsourceId(
			Integer financialsourceId);

	/**
	 * Find by reference: averageMethod
	 *
	 * @param averageMethod
	 * @return List<MasterAgreement>
	 */
	public List<MasterAgreement> findByAverageMethod(AverageMethod averageMethod);

	/**
	 * Find by ID of reference: averageMethod.id
	 *
	 * @param averageMethodId
	 * @return List<MasterAgreement>
	 */
	public List<MasterAgreement> findByAverageMethodId(Integer averageMethodId);
}
