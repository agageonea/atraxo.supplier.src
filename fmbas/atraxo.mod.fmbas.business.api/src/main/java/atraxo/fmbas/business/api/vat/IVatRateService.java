/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.vat;

import atraxo.fmbas.domain.impl.vat.Vat;
import atraxo.fmbas.domain.impl.vat.VatRate;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link VatRate} domain entity.
 */
public interface IVatRateService extends IEntityService<VatRate> {

	/**
	 * Custom service findVatRateByDate
	 *
	 * @return VatRate
	 */
	public VatRate findVatRateByDate(Date date, Vat vat)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return VatRate
	 */
	public VatRate findByBusiness(Integer id);

	/**
	 * Find by reference: vat
	 *
	 * @param vat
	 * @return List<VatRate>
	 */
	public List<VatRate> findByVat(Vat vat);

	/**
	 * Find by ID of reference: vat.id
	 *
	 * @param vatId
	 * @return List<VatRate>
	 */
	public List<VatRate> findByVatId(Integer vatId);
}
