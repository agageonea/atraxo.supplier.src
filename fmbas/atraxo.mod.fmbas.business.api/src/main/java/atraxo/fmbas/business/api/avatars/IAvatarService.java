/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.avatars;

import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.ad.domain.impl.security.User;
import atraxo.fmbas.domain.impl.avatars.Avatar;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Avatar} domain entity.
 */
public interface IAvatarService extends IEntityService<Avatar> {

	/**
	 * Find by unique key
	 *
	 * @return Avatar
	 */
	public Avatar findById(String id);

	/**
	 * Find by reference: user
	 *
	 * @param user
	 * @return List<Avatar>
	 */
	public List<Avatar> findByUser(User user);

	/**
	 * Find by ID of reference: user.id
	 *
	 * @param userId
	 * @return List<Avatar>
	 */
	public List<Avatar> findByUserId(String userId);

	/**
	 * Find by reference: type
	 *
	 * @param type
	 * @return List<Avatar>
	 */
	public List<Avatar> findByType(AttachmentType type);

	/**
	 * Find by ID of reference: type.id
	 *
	 * @param typeId
	 * @return List<Avatar>
	 */
	public List<Avatar> findByTypeId(String typeId);
}
