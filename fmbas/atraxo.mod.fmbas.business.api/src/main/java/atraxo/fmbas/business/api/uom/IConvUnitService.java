/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.uom;

import atraxo.fmbas.domain.impl.uom.ConvUnit;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ConvUnit} domain entity.
 */
public interface IConvUnitService extends IEntityService<ConvUnit> {

	/**
	 * Find by unique key
	 *
	 * @return ConvUnit
	 */
	public ConvUnit findByUnitsConvFact(Unit unitFromId, Unit unitToId,
			Integer convType);

	/**
	 * Find by unique key
	 *
	 * @return ConvUnit
	 */
	public ConvUnit findByUnitsConvFact(Long unitFromIdId, Long unitToIdId,
			Integer convType);

	/**
	 * Find by unique key
	 *
	 * @return ConvUnit
	 */
	public ConvUnit findByBusiness(Integer id);

	/**
	 * Find by reference: unitFromId
	 *
	 * @param unitFromId
	 * @return List<ConvUnit>
	 */
	public List<ConvUnit> findByUnitFromId(Unit unitFromId);

	/**
	 * Find by ID of reference: unitFromId.id
	 *
	 * @param unitFromIdId
	 * @return List<ConvUnit>
	 */
	public List<ConvUnit> findByUnitFromIdId(Integer unitFromIdId);

	/**
	 * Find by reference: unitToId
	 *
	 * @param unitToId
	 * @return List<ConvUnit>
	 */
	public List<ConvUnit> findByUnitToId(Unit unitToId);

	/**
	 * Find by ID of reference: unitToId.id
	 *
	 * @param unitToIdId
	 * @return List<ConvUnit>
	 */
	public List<ConvUnit> findByUnitToIdId(Integer unitToIdId);
}
