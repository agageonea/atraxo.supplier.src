/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.externalInterfaces;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.IncommingMessage;
import atraxo.fmbas.domain.impl.fmbas_type.IncomingMessageStatus;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link IncommingMessage} domain entity.
 */
public interface IIncommingMessageService
		extends
			IEntityService<IncommingMessage> {

	/**
	 * Custom service getNextMessage
	 *
	 * @return IncommingMessage
	 */
	public IncommingMessage getNextMessage(ExternalInterface extInt)
			throws BusinessException;

	/**
	 * Custom service findByMessageId
	 *
	 * @return List<IncommingMessage>
	 */
	public List<IncommingMessage> findByMessageId(String msgId)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return IncommingMessage
	 */
	public IncommingMessage findByBusiness(Integer id);

	/**
	 * Find by index key
	 *
	 * @return List<IncommingMessage>
	 */
	public List<IncommingMessage> findByIdxStatus_interace_id_clientid_created_at(
			IncomingMessageStatus status, ExternalInterface externalInterface,
			Date createdAt);

	/**
	 * Find by index key
	 *
	 * @return List<IncommingMessage>
	 */
	public List<IncommingMessage> findByIdxStatus_interace_id_clientid_created_at(
			IncomingMessageStatus status, Long externalInterfaceId,
			Date createdAt);

	/**
	 * Find by reference: externalInterface
	 *
	 * @param externalInterface
	 * @return List<IncommingMessage>
	 */
	public List<IncommingMessage> findByExternalInterface(
			ExternalInterface externalInterface);

	/**
	 * Find by ID of reference: externalInterface.id
	 *
	 * @param externalInterfaceId
	 * @return List<IncommingMessage>
	 */
	public List<IncommingMessage> findByExternalInterfaceId(
			Integer externalInterfaceId);
}
