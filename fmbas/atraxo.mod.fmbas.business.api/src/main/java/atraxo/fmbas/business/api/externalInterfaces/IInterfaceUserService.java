/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.externalInterfaces;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.InterfaceUser;
import atraxo.fmbas.domain.impl.templates.Template;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link InterfaceUser} domain entity.
 */
public interface IInterfaceUserService extends IEntityService<InterfaceUser> {

	/**
	 * Find by unique key
	 *
	 * @return InterfaceUser
	 */
	public InterfaceUser findByKey(ExternalInterface externalInterface,
			UserSupp user);

	/**
	 * Find by unique key
	 *
	 * @return InterfaceUser
	 */
	public InterfaceUser findByKey(Long externalInterfaceId, Long userId);

	/**
	 * Find by unique key
	 *
	 * @return InterfaceUser
	 */
	public InterfaceUser findByBusiness(Integer id);

	/**
	 * Find by reference: externalInterface
	 *
	 * @param externalInterface
	 * @return List<InterfaceUser>
	 */
	public List<InterfaceUser> findByExternalInterface(
			ExternalInterface externalInterface);

	/**
	 * Find by ID of reference: externalInterface.id
	 *
	 * @param externalInterfaceId
	 * @return List<InterfaceUser>
	 */
	public List<InterfaceUser> findByExternalInterfaceId(
			Integer externalInterfaceId);

	/**
	 * Find by reference: user
	 *
	 * @param user
	 * @return List<InterfaceUser>
	 */
	public List<InterfaceUser> findByUser(UserSupp user);

	/**
	 * Find by ID of reference: user.id
	 *
	 * @param userId
	 * @return List<InterfaceUser>
	 */
	public List<InterfaceUser> findByUserId(String userId);

	/**
	 * Find by reference: emailTemplate
	 *
	 * @param emailTemplate
	 * @return List<InterfaceUser>
	 */
	public List<InterfaceUser> findByEmailTemplate(Template emailTemplate);

	/**
	 * Find by ID of reference: emailTemplate.id
	 *
	 * @param emailTemplateId
	 * @return List<InterfaceUser>
	 */
	public List<InterfaceUser> findByEmailTemplateId(Integer emailTemplateId);
}
