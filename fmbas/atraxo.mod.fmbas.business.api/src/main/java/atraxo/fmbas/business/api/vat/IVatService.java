/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.vat;

import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.vat.Vat;
import atraxo.fmbas.domain.impl.vat.VatRate;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Vat} domain entity.
 */
public interface IVatService extends IEntityService<Vat> {

	/**
	 * Custom service getVat
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getVat(Date date, Country country)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Vat
	 */
	public Vat findByCode(String code, Country country);

	/**
	 * Find by unique key
	 *
	 * @return Vat
	 */
	public Vat findByCode(String code, Long countryId);

	/**
	 * Find by unique key
	 *
	 * @return Vat
	 */
	public Vat findByBusiness(Integer id);

	/**
	 * Find by reference: country
	 *
	 * @param country
	 * @return List<Vat>
	 */
	public List<Vat> findByCountry(Country country);

	/**
	 * Find by ID of reference: country.id
	 *
	 * @param countryId
	 * @return List<Vat>
	 */
	public List<Vat> findByCountryId(Integer countryId);

	/**
	 * Find by reference: vatRate
	 *
	 * @param vatRate
	 * @return List<Vat>
	 */
	public List<Vat> findByVatRate(VatRate vatRate);

	/**
	 * Find by ID of reference: vatRate.id
	 *
	 * @param vatRateId
	 * @return List<Vat>
	 */
	public List<Vat> findByVatRateId(Integer vatRateId);
}
