/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.suppliers;

import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Suppliers} domain entity.
 */
public interface ISuppliersService extends IEntityService<Suppliers> {

	/**
	 * Custom service getPrimaryContact
	 *
	 * @return Contacts
	 */
	public Contacts getPrimaryContact(Suppliers supplier)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Suppliers
	 */
	public Suppliers findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return Suppliers
	 */
	public Suppliers findByBusiness(Integer id);

	/**
	 * Find by reference: responsibleBuyer
	 *
	 * @param responsibleBuyer
	 * @return List<Suppliers>
	 */
	public List<Suppliers> findByResponsibleBuyer(UserSupp responsibleBuyer);

	/**
	 * Find by ID of reference: responsibleBuyer.id
	 *
	 * @param responsibleBuyerId
	 * @return List<Suppliers>
	 */
	public List<Suppliers> findByResponsibleBuyerId(String responsibleBuyerId);
}
