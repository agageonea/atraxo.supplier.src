/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.geo;

import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.geo.Locations;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Area} domain entity.
 */
public interface IAreaService extends IEntityService<Area> {

	/**
	 * Custom service findActive
	 *
	 * @return List<Area>
	 */
	public List<Area> findActive() throws BusinessException;

	/**
	 * Custom service getWW
	 *
	 * @return Area
	 */
	public Area getWW() throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Area
	 */
	public Area findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return Area
	 */
	public Area findByBusiness(Integer id);

	/**
	 * Find by reference: locations
	 *
	 * @param locations
	 * @return List<Area>
	 */
	public List<Area> findByLocations(Locations locations);

	/**
	 * Find by ID of reference: locations.id
	 *
	 * @param locationsId
	 * @return List<Area>
	 */
	public List<Area> findByLocationsId(Integer locationsId);
}
