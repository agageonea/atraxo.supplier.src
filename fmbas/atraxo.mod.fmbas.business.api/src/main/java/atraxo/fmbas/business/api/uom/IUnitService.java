/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.uom;

import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.math.BigDecimal;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Unit} domain entity.
 */
public interface IUnitService extends IEntityService<Unit> {

	/**
	 * Custom service getBaseUnit
	 *
	 * @return Unit
	 */
	public Unit getBaseUnit(UnitType indicator, BigDecimal factor)
			throws BusinessException;

	/**
	 * Custom service convert
	 *
	 * @return BigDecimal
	 */
	public BigDecimal convert(Unit fromUnit, Unit toUnit, BigDecimal value,
			Double density) throws BusinessException;

	/**
	 * Custom service convert
	 *
	 * @return BigDecimal
	 */
	public BigDecimal convert(Unit fromUnit, Unit toUnit, BigDecimal value,
			Double density, Unit dFromUnit, Unit fToUnit)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Unit
	 */
	public Unit findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return Unit
	 */
	public Unit findByBusiness(Integer id);
}
