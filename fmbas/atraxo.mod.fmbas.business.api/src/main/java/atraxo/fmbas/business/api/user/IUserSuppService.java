/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.user;

import atraxo.ad.domain.impl.system.DateFormat;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.notification.Notification;
import atraxo.fmbas.domain.impl.user.RoleSupp;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link UserSupp} domain entity.
 */
public interface IUserSuppService extends IEntityService<UserSupp> {

	/**
	 * Custom service doChangePassword
	 *
	 * @return void
	 */
	public void doChangePassword(String userId, String newPassword)
			throws BusinessException;

	/**
	 * Custom service updateUserSessionProfile
	 *
	 * @return void
	 */
	public void updateUserSessionProfile(Object obj) throws BusinessException;

	/**
	 * Custom service createSessionUser
	 *
	 * @return void
	 */
	public void createSessionUser(String clientCode) throws BusinessException;

	/**
	 * Custom service createSessionUser
	 *
	 * @return void
	 */
	public void createSessionUser(String clientCode, String userCode,
			boolean useSubsidiaries) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return UserSupp
	 */
	public UserSupp findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return UserSupp
	 */
	public UserSupp findByLogin(String loginName);

	/**
	 * Find by reference: dateFormat
	 *
	 * @param dateFormat
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findByDateFormat(DateFormat dateFormat);

	/**
	 * Find by ID of reference: dateFormat.id
	 *
	 * @param dateFormatId
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findByDateFormatId(String dateFormatId);

	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findByLocation(Locations location);

	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findByLocationId(Integer locationId);

	/**
	 * Find by reference: roles
	 *
	 * @param roles
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findByRoles(RoleSupp roles);

	/**
	 * Find by ID of reference: roles.id
	 *
	 * @param rolesId
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findByRolesId(String rolesId);

	/**
	 * Find by reference: subsidiaries
	 *
	 * @param subsidiaries
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findBySubsidiaries(UserSubsidiary subsidiaries);

	/**
	 * Find by ID of reference: subsidiaries.id
	 *
	 * @param subsidiariesId
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findBySubsidiariesId(Integer subsidiariesId);

	/**
	 * Find by reference: notifications
	 *
	 * @param notifications
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findByNotifications(Notification notifications);

	/**
	 * Find by ID of reference: notifications.id
	 *
	 * @param notificationsId
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findByNotificationsId(Integer notificationsId);
}
