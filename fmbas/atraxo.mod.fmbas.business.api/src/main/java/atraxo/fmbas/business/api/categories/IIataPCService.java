/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.categories;

import atraxo.fmbas.domain.impl.categories.IataPC;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link IataPC} domain entity.
 */
public interface IIataPCService extends IEntityService<IataPC> {

	/**
	 * Find by unique key
	 *
	 * @return IataPC
	 */
	public IataPC findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return IataPC
	 */
	public IataPC findByBusiness(Integer id);
}
