/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.workflow;

import atraxo.fmbas.domain.impl.fmbas_type.WorkflowInstanceStatus;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowNotification;
import java.util.List;
import java.util.Map;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link WorkflowNotification} domain entity.
 */
public interface IWorkflowNotificationService
		extends
			IEntityService<WorkflowNotification> {

	/**
	 * Custom service sendNotification
	 *
	 * @return void
	 */
	public void sendNotification(WorkflowInstance wkfInstance,
			WorkflowInstanceStatus status, String instanceResult)
			throws BusinessException;

	/**
	 * Custom service sendNotification
	 *
	 * @return void
	 */
	public void sendNotification(WorkflowInstance wkfInstance,
			WorkflowInstanceStatus status, String instanceResult, Map params)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return WorkflowNotification
	 */
	public WorkflowNotification findByKey(Workflow workflow, UserSupp user);

	/**
	 * Find by unique key
	 *
	 * @return WorkflowNotification
	 */
	public WorkflowNotification findByKey(Long workflowId, Long userId);

	/**
	 * Find by unique key
	 *
	 * @return WorkflowNotification
	 */
	public WorkflowNotification findByBusiness(Integer id);

	/**
	 * Find by reference: workflow
	 *
	 * @param workflow
	 * @return List<WorkflowNotification>
	 */
	public List<WorkflowNotification> findByWorkflow(Workflow workflow);

	/**
	 * Find by ID of reference: workflow.id
	 *
	 * @param workflowId
	 * @return List<WorkflowNotification>
	 */
	public List<WorkflowNotification> findByWorkflowId(Integer workflowId);

	/**
	 * Find by reference: user
	 *
	 * @param user
	 * @return List<WorkflowNotification>
	 */
	public List<WorkflowNotification> findByUser(UserSupp user);

	/**
	 * Find by ID of reference: user.id
	 *
	 * @param userId
	 * @return List<WorkflowNotification>
	 */
	public List<WorkflowNotification> findByUserId(String userId);
}
