/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.history;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.history.ChangeHistory;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ChangeHistory} domain entity.
 */
public interface IChangeHistoryService extends IEntityService<ChangeHistory> {

	/**
	 * Custom service deleteHistory
	 *
	 * @return void
	 */
	public void deleteHistory(Integer id, Class<?> c) throws BusinessException;

	/**
	 * Custom service deleteHistory
	 *
	 * @return void
	 */
	public void deleteHistory(List ids, Class<?> c) throws BusinessException;

	/**
	 * Custom service saveChangeHistory
	 *
	 * @return void
	 */
	public void saveChangeHistory(AbstractEntity obj, String action,
			String message) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return ChangeHistory
	 */
	public ChangeHistory findByBusiness(Integer id);
}
