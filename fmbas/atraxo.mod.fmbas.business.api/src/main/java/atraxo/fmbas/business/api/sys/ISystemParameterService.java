/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.sys;

import atraxo.fmbas.domain.impl.fmbas_type.InvoiceCostToAccrual;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link SystemParameter} domain entity.
 */
public interface ISystemParameterService
		extends
			IEntityService<SystemParameter> {

	/**
	 * Custom service getDensity
	 *
	 * @return String
	 */
	public String getDensity() throws BusinessException;

	/**
	 * Custom service getDecimalsForPercentage
	 *
	 * @return Integer
	 */
	public Integer getDecimalsForPercentage() throws BusinessException;

	/**
	 * Custom service getDecimalsForCurrency
	 *
	 * @return Integer
	 */
	public Integer getDecimalsForCurrency() throws BusinessException;

	/**
	 * Custom service getDecimalsForUnit
	 *
	 * @return Integer
	 */
	public Integer getDecimalsForUnit() throws BusinessException;

	/**
	 * Custom service getDecimalsForAmount
	 *
	 * @return Integer
	 */
	public Integer getDecimalsForAmount() throws BusinessException;

	/**
	 * Custom service getSysVol
	 *
	 * @return String
	 */
	public String getSysVol() throws BusinessException;

	/**
	 * Custom service getSysWeight
	 *
	 * @return String
	 */
	public String getSysWeight() throws BusinessException;

	/**
	 * Custom service getSysCurrency
	 *
	 * @return String
	 */
	public String getSysCurrency() throws BusinessException;

	/**
	 * Custom service getInvoiceApprovalWorkflow
	 *
	 * @return String
	 */
	public String getInvoiceApprovalWorkflow() throws BusinessException;

	/**
	 * Custom service getSalesInvoiceApprovalWorkflow
	 *
	 * @return Boolean
	 */
	public Boolean getSalesInvoiceApprovalWorkflow() throws BusinessException;

	/**
	 * Custom service getAccrualEvaluationDate
	 *
	 * @return String
	 */
	public String getAccrualEvaluationDate() throws BusinessException;

	/**
	 * Custom service getSysAverageMethod
	 *
	 * @return String
	 */
	public String getSysAverageMethod() throws BusinessException;

	/**
	 * Custom service getInvoiceCostToAccruals
	 *
	 * @return InvoiceCostToAccrual
	 */
	public InvoiceCostToAccrual getInvoiceCostToAccruals()
			throws BusinessException;

	/**
	 * Custom service getAccrualAutoClose
	 *
	 * @return Boolean
	 */
	public Boolean getAccrualAutoClose() throws BusinessException;

	/**
	 * Custom service getSysUnit
	 *
	 * @return String
	 */
	public String getSysUnit() throws BusinessException;

	/**
	 * Custom service getNoReplyAddress
	 *
	 * @return String
	 */
	public String getNoReplyAddress() throws BusinessException;

	/**
	 * Custom service getDefaultFuelEventReceivedStatus
	 *
	 * @return String
	 */
	public String getDefaultFuelEventReceivedStatus() throws BusinessException;

	/**
	 * Custom service updateSessionSystemParameters
	 *
	 * @return void
	 */
	public void updateSessionSystemParameters(Object obj)
			throws BusinessException;

	/**
	 * Custom service getRestrictBlacklisted
	 *
	 * @return Boolean
	 */
	public Boolean getRestrictBlacklisted() throws BusinessException;

	/**
	 * Custom service getCreditMemoApprovalWorkflow
	 *
	 * @return Boolean
	 */
	public Boolean getCreditMemoApprovalWorkflow() throws BusinessException;

	/**
	 * Custom service getPriceUpdateApprovalWorkflow
	 *
	 * @return Boolean
	 */
	public Boolean getPriceUpdateApprovalWorkflow() throws BusinessException;

	/**
	 * Custom service getSellContractApprovalWorkflow
	 *
	 * @return Boolean
	 */
	public Boolean getSellContractApprovalWorkflow() throws BusinessException;

	/**
	 * Custom service getPurchaseContractApprovalWorkflow
	 *
	 * @return Boolean
	 */
	public Boolean getPurchaseContractApprovalWorkflow()
			throws BusinessException;

	/**
	 * Custom service getBidApprovalWorkflow
	 *
	 * @return Boolean
	 */
	public Boolean getBidApprovalWorkflow() throws BusinessException;

	/**
	 * Custom service getFuelTicketApprovalWorkflow
	 *
	 * @return Boolean
	 */
	public Boolean getFuelTicketApprovalWorkflow() throws BusinessException;

	/**
	 * Custom service getEnergyPriceApprovalWorkflow
	 *
	 * @return Boolean
	 */
	public Boolean getEnergyPriceApprovalWorkflow() throws BusinessException;

	/**
	 * Custom service getExchangeRateApprovalWorkflow
	 *
	 * @return Boolean
	 */
	public Boolean getExchangeRateApprovalWorkflow() throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return SystemParameter
	 */
	public SystemParameter findBySysparam(String code);

	/**
	 * Find by unique key
	 *
	 * @return SystemParameter
	 */
	public SystemParameter findByBusiness(Integer id);
}
