/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.externalInterfaces;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceHistory;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.time.StopWatch;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ExternalInterfaceHistory} domain entity.
 */
public interface IExternalInterfaceHistoryService
		extends
			IEntityService<ExternalInterfaceHistory> {

	/**
	 * Custom service empty
	 *
	 * @return void
	 */
	public void empty(Integer externalInterfaceId) throws BusinessException;

	/**
	 * Custom service findByMsgId
	 *
	 * @return ExternalInterfaceHistory
	 */
	public ExternalInterfaceHistory findByMsgId(String msgId)
			throws BusinessException;

	/**
	 * Custom service updateHistoryOutboundFromACK
	 *
	 * @return void
	 */
	public void updateHistoryOutboundFromACK(Boolean success, String messageId,
			boolean canHaveNegativeACK) throws BusinessException;

	/**
	 * Custom service findLastEntry
	 *
	 * @return ExternalInterfaceHistory
	 */
	public ExternalInterfaceHistory findLastEntry(Integer externalInterfaceId)
			throws BusinessException;

	/**
	 * Custom service saveInterfaceHistory
	 *
	 * @return void
	 */
	public void saveInterfaceHistory(StopWatch stopWatch,
			WSExecutionResult result, ExternalInterface externalInterface)
			throws BusinessException;

	/**
	 * Custom service sendNotification
	 *
	 * @return void
	 */
	public void sendNotification(ExternalInterface inter, Map objectMap)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return ExternalInterfaceHistory
	 */
	public ExternalInterfaceHistory findByBusiness(Integer id);

	/**
	 * Find by index key
	 *
	 * @return List<ExternalInterfaceHistory>
	 */
	public List<ExternalInterfaceHistory> findByIdxMessage_id(String msgId);

	/**
	 * Find by reference: externalInterface
	 *
	 * @param externalInterface
	 * @return List<ExternalInterfaceHistory>
	 */
	public List<ExternalInterfaceHistory> findByExternalInterface(
			ExternalInterface externalInterface);

	/**
	 * Find by ID of reference: externalInterface.id
	 *
	 * @param externalInterfaceId
	 * @return List<ExternalInterfaceHistory>
	 */
	public List<ExternalInterfaceHistory> findByExternalInterfaceId(
			Integer externalInterfaceId);
}
