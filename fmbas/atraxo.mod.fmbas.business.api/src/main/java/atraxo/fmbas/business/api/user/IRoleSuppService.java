/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.user;

import atraxo.fmbas.domain.impl.user.RoleSupp;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link RoleSupp} domain entity.
 */
public interface IRoleSuppService extends IEntityService<RoleSupp> {

	/**
	 * Find by unique key
	 *
	 * @return RoleSupp
	 */
	public RoleSupp findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return RoleSupp
	 */
	public RoleSupp findByName(String name);

	/**
	 * Find by reference: users
	 *
	 * @param users
	 * @return List<RoleSupp>
	 */
	public List<RoleSupp> findByUsers(UserSupp users);

	/**
	 * Find by ID of reference: users.id
	 *
	 * @param usersId
	 * @return List<RoleSupp>
	 */
	public List<RoleSupp> findByUsersId(String usersId);
}
