/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.timeserie;

import atraxo.fmbas.domain.impl.commodities.Commoditie;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesApprovalStatus;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesStatus;
import atraxo.fmbas.domain.impl.timeserie.CompositeTimeSeriesSource;
import atraxo.fmbas.domain.impl.timeserie.RawTimeSerieItem;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieAverage;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link TimeSerie} domain entity.
 */
public interface ITimeSerieService extends IEntityService<TimeSerie> {

	/**
	 * Custom service publish
	 *
	 * @return void
	 */
	public void publish(List<TimeSerie> timeSeries) throws BusinessException;

	/**
	 * Custom service updateStatus
	 *
	 * @return void
	 */
	public void updateStatus(TimeSerie e, TimeSeriesStatus status)
			throws BusinessException;

	/**
	 * Custom service calculate
	 *
	 * @return void
	 */
	public void calculate(Integer id) throws BusinessException;

	/**
	 * Custom service calculate
	 *
	 * @return void
	 */
	public void calculate(TimeSerie e) throws BusinessException;

	/**
	 * Custom service getTimeSerie
	 *
	 * @return TimeSerie
	 */
	public TimeSerie getTimeSerie(List<Integer> usedIds)
			throws BusinessException;

	/**
	 * Custom service submitForApproval
	 *
	 * @return void
	 */
	public void submitForApproval(TimeSerie e, TimeSeriesApprovalStatus status,
			String action, String reason) throws BusinessException;

	/**
	 * Custom service approve
	 *
	 * @return void
	 */
	public void approve(TimeSerie e, String action, String reason)
			throws BusinessException;

	/**
	 * Custom service reject
	 *
	 * @return void
	 */
	public void reject(TimeSerie e, String action, String reason)
			throws BusinessException;

	/**
	 * Custom service countRawTimeSerieItems
	 *
	 * @return Long
	 */
	public Long countRawTimeSerieItems(TimeSerie e) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return TimeSerie
	 */
	public TimeSerie findByName(String serieName);

	/**
	 * Find by unique key
	 *
	 * @return TimeSerie
	 */
	public TimeSerie findByCode(String externalSerieName);

	/**
	 * Find by unique key
	 *
	 * @return TimeSerie
	 */
	public TimeSerie findByBusiness(Integer id);

	/**
	 * Find by reference: currency1Id
	 *
	 * @param currency1Id
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByCurrency1Id(Currencies currency1Id);

	/**
	 * Find by ID of reference: currency1Id.id
	 *
	 * @param currency1IdId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByCurrency1IdId(Integer currency1IdId);

	/**
	 * Find by reference: currency2Id
	 *
	 * @param currency2Id
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByCurrency2Id(Currencies currency2Id);

	/**
	 * Find by ID of reference: currency2Id.id
	 *
	 * @param currency2IdId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByCurrency2IdId(Integer currency2IdId);

	/**
	 * Find by reference: commoditieId
	 *
	 * @param commoditieId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByCommoditieId(Commoditie commoditieId);

	/**
	 * Find by ID of reference: commoditieId.id
	 *
	 * @param commoditieIdId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByCommoditieIdId(Integer commoditieIdId);

	/**
	 * Find by reference: unitId
	 *
	 * @param unitId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByUnitId(Unit unitId);

	/**
	 * Find by ID of reference: unitId.id
	 *
	 * @param unitIdId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByUnitIdId(Integer unitIdId);

	/**
	 * Find by reference: unit2Id
	 *
	 * @param unit2Id
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByUnit2Id(Unit unit2Id);

	/**
	 * Find by ID of reference: unit2Id.id
	 *
	 * @param unit2IdId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByUnit2IdId(Integer unit2IdId);

	/**
	 * Find by reference: financialSource
	 *
	 * @param financialSource
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByFinancialSource(
			FinancialSources financialSource);

	/**
	 * Find by ID of reference: financialSource.id
	 *
	 * @param financialSourceId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByFinancialSourceId(Integer financialSourceId);

	/**
	 * Find by reference: rawTimeserieItems
	 *
	 * @param rawTimeserieItems
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByRawTimeserieItems(
			RawTimeSerieItem rawTimeserieItems);

	/**
	 * Find by ID of reference: rawTimeserieItems.id
	 *
	 * @param rawTimeserieItemsId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByRawTimeserieItemsId(Integer rawTimeserieItemsId);

	/**
	 * Find by reference: timeserieItems
	 *
	 * @param timeserieItems
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByTimeserieItems(TimeSerieItem timeserieItems);

	/**
	 * Find by ID of reference: timeserieItems.id
	 *
	 * @param timeserieItemsId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByTimeserieItemsId(Integer timeserieItemsId);

	/**
	 * Find by reference: timeserieAvg
	 *
	 * @param timeserieAvg
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByTimeserieAvg(TimeSerieAverage timeserieAvg);

	/**
	 * Find by ID of reference: timeserieAvg.id
	 *
	 * @param timeserieAvgId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByTimeserieAvgId(Integer timeserieAvgId);

	/**
	 * Find by reference: compositeTimeSeries
	 *
	 * @param compositeTimeSeries
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByCompositeTimeSeries(
			CompositeTimeSeriesSource compositeTimeSeries);

	/**
	 * Find by ID of reference: compositeTimeSeries.id
	 *
	 * @param compositeTimeSeriesId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByCompositeTimeSeriesId(
			Integer compositeTimeSeriesId);

	/**
	 * Find by reference: sourceTimeSeries
	 *
	 * @param sourceTimeSeries
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findBySourceTimeSeries(
			CompositeTimeSeriesSource sourceTimeSeries);

	/**
	 * Find by ID of reference: sourceTimeSeries.id
	 *
	 * @param sourceTimeSeriesId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findBySourceTimeSeriesId(Integer sourceTimeSeriesId);
}
