package atraxo.fmbas.business.api.ws;

import seava.j4e.api.exceptions.BusinessException;


/**
 * @author vhojda
 * @param <E>
 */
public interface IRestClientTimeoutService<E> {

	/**
	 * Performs different operations on an entity (modifies it) after a specific timeout has expired
	 *
	 * @param entity
	 * @returns true if modified, false otherwise
	 * @throws BusinessException
	 */
	boolean afterTimeout(E entity) throws BusinessException;

}
