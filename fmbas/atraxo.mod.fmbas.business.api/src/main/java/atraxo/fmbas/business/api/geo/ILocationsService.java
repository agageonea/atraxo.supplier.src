/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.geo;

import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.geo.TimeZone;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Locations} domain entity.
 */
public interface ILocationsService extends IEntityService<Locations> {

	/**
	 * Custom service findByCodeAndClientId
	 *
	 * @return Locations
	 */
	public Locations findByCodeAndClientId(String code, String clientId)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Locations
	 */
	public Locations findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return Locations
	 */
	public Locations findByBusiness(Integer id);

	/**
	 * Find by reference: country
	 *
	 * @param country
	 * @return List<Locations>
	 */
	public List<Locations> findByCountry(Country country);

	/**
	 * Find by ID of reference: country.id
	 *
	 * @param countryId
	 * @return List<Locations>
	 */
	public List<Locations> findByCountryId(Integer countryId);

	/**
	 * Find by reference: subCountry
	 *
	 * @param subCountry
	 * @return List<Locations>
	 */
	public List<Locations> findBySubCountry(Country subCountry);

	/**
	 * Find by ID of reference: subCountry.id
	 *
	 * @param subCountryId
	 * @return List<Locations>
	 */
	public List<Locations> findBySubCountryId(Integer subCountryId);

	/**
	 * Find by reference: timezone
	 *
	 * @param timezone
	 * @return List<Locations>
	 */
	public List<Locations> findByTimezone(TimeZone timezone);

	/**
	 * Find by ID of reference: timezone.id
	 *
	 * @param timezoneId
	 * @return List<Locations>
	 */
	public List<Locations> findByTimezoneId(Integer timezoneId);

	/**
	 * Find by reference: areas
	 *
	 * @param areas
	 * @return List<Locations>
	 */
	public List<Locations> findByAreas(Area areas);

	/**
	 * Find by ID of reference: areas.id
	 *
	 * @param areasId
	 * @return List<Locations>
	 */
	public List<Locations> findByAreasId(Integer areasId);
}
