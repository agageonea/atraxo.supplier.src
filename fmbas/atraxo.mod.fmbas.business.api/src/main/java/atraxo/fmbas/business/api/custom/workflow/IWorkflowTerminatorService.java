package atraxo.fmbas.business.api.custom.workflow;

import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import seava.j4e.api.exceptions.BusinessException;

public interface IWorkflowTerminatorService {
	/**
	 * @param workflowInstance
	 * @param reason
	 * @throws BusinessException
	 */
	public void terminateWorkflow(WorkflowInstance workflowInstance, String reason) throws BusinessException;
}
