package atraxo.fmbas.business.api.ext.bpm;

/**
 * @author vhojda
 */
public class WorkflowStartResult {

	private static final String DEFAULT_REASON = "Unknown reason";

	private boolean started;
	private String reason;
	private int numberOfSuccesInstances;

	/**
	 *
	 */
	public WorkflowStartResult() {
		super();
		this.started = false;
		this.reason = DEFAULT_REASON;
	}

	public boolean isStarted() {
		return this.started;
	}

	public int getNumberOfSuccesInstances() {
		return this.numberOfSuccesInstances;
	}

	public void setNumberOfSuccesInstances(int numberOfSuccesInstances) {
		this.numberOfSuccesInstances = numberOfSuccesInstances;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public String toString() {
		return "WorkflowStartResult [started=" + this.started + ", reason=" + this.reason + ", numberOfSuccesInstances="
				+ this.numberOfSuccesInstances + "]";
	}

}
