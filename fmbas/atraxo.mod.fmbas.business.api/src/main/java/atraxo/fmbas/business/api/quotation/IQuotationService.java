/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.quotation;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Quotation} domain entity.
 */
public interface IQuotationService extends IEntityService<Quotation> {

	/**
	 * Custom service getByTimeseriesAndAvgMethod
	 *
	 * @return Quotation
	 */
	public Quotation getByTimeseriesAndAvgMethod(Integer tserieId,
			AverageMethod avgMethod) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Quotation
	 */
	public Quotation findByName(String name);

	/**
	 * Find by unique key
	 *
	 * @return Quotation
	 */
	public Quotation findByBusiness(Integer id);

	/**
	 * Find by reference: timeseries
	 *
	 * @param timeseries
	 * @return List<Quotation>
	 */
	public List<Quotation> findByTimeseries(TimeSerie timeseries);

	/**
	 * Find by ID of reference: timeseries.id
	 *
	 * @param timeseriesId
	 * @return List<Quotation>
	 */
	public List<Quotation> findByTimeseriesId(Integer timeseriesId);

	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<Quotation>
	 */
	public List<Quotation> findByCurrency(Currencies currency);

	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<Quotation>
	 */
	public List<Quotation> findByCurrencyId(Integer currencyId);

	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<Quotation>
	 */
	public List<Quotation> findByUnit(Unit unit);

	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<Quotation>
	 */
	public List<Quotation> findByUnitId(Integer unitId);

	/**
	 * Find by reference: avgMethodIndicator
	 *
	 * @param avgMethodIndicator
	 * @return List<Quotation>
	 */
	public List<Quotation> findByAvgMethodIndicator(
			AverageMethod avgMethodIndicator);

	/**
	 * Find by ID of reference: avgMethodIndicator.id
	 *
	 * @param avgMethodIndicatorId
	 * @return List<Quotation>
	 */
	public List<Quotation> findByAvgMethodIndicatorId(
			Integer avgMethodIndicatorId);

	/**
	 * Find by reference: quotationValues
	 *
	 * @param quotationValues
	 * @return List<Quotation>
	 */
	public List<Quotation> findByQuotationValues(QuotationValue quotationValues);

	/**
	 * Find by ID of reference: quotationValues.id
	 *
	 * @param quotationValuesId
	 * @return List<Quotation>
	 */
	public List<Quotation> findByQuotationValuesId(Integer quotationValuesId);
}
