/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.kpi;

import atraxo.ad.domain.impl.security.MenuItem;
import atraxo.fmbas.domain.impl.kpi.Kpi;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Kpi} domain entity.
 */
public interface IKpiService extends IEntityService<Kpi> {

	/**
	 * Find by unique key
	 *
	 * @return Kpi
	 */
	public Kpi findByKpi(String dsName, String methodName);

	/**
	 * Find by unique key
	 *
	 * @return Kpi
	 */
	public Kpi findByBusiness(Integer id);

	/**
	 * Find by reference: menuItem
	 *
	 * @param menuItem
	 * @return List<Kpi>
	 */
	public List<Kpi> findByMenuItem(MenuItem menuItem);

	/**
	 * Find by ID of reference: menuItem.id
	 *
	 * @param menuItemId
	 * @return List<Kpi>
	 */
	public List<Kpi> findByMenuItemId(String menuItemId);
}
