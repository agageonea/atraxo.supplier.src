/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.customer;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.customer.CustomerLocations;
import atraxo.fmbas.domain.impl.geo.Country;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link CustomerLocations} domain entity.
 */
public interface ICustomerLocationsService
		extends
			IEntityService<CustomerLocations> {

	/**
	 * Find by unique key
	 *
	 * @return CustomerLocations
	 */
	public CustomerLocations findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return CustomerLocations
	 */
	public CustomerLocations findByBusiness(Integer id);

	/**
	 * Find by reference: country
	 *
	 * @param country
	 * @return List<CustomerLocations>
	 */
	public List<CustomerLocations> findByCountry(Country country);

	/**
	 * Find by ID of reference: country.id
	 *
	 * @param countryId
	 * @return List<CustomerLocations>
	 */
	public List<CustomerLocations> findByCountryId(Integer countryId);

	/**
	 * Find by reference: customers
	 *
	 * @param customers
	 * @return List<CustomerLocations>
	 */
	public List<CustomerLocations> findByCustomers(Customer customers);

	/**
	 * Find by ID of reference: customers.id
	 *
	 * @param customersId
	 * @return List<CustomerLocations>
	 */
	public List<CustomerLocations> findByCustomersId(Integer customersId);
}
