/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.notes;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.notes.Notes;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Notes} domain entity.
 */
public interface INotesService extends IEntityService<Notes> {

	/**
	 * Custom service deleteNote
	 *
	 * @return void
	 */
	public void deleteNote(Integer id, Class<?> c) throws BusinessException;

	/**
	 * Custom service deleteNotes
	 *
	 * @return void
	 */
	public void deleteNotes(List ids, Class<?> c) throws BusinessException;

	/**
	 * Custom service deleteByEntities
	 *
	 * @return void
	 */
	public void deleteByEntities(List<? extends AbstractEntity> entities,
			Class<?> c) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Notes
	 */
	public Notes findById(Integer id);

	/**
	 * Find by unique key
	 *
	 * @return Notes
	 */
	public Notes findByBusiness(Integer id);
}
