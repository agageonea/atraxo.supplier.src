/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.documentNumberSeries;

import atraxo.fmbas.domain.impl.documentNumberSeries.DocumentNumberSeries;
import atraxo.fmbas.domain.impl.fmbas_type.DocumentNumberSeriesType;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link DocumentNumberSeries} domain entity.
 */
public interface IDocumentNumberSeriesService
		extends
			IEntityService<DocumentNumberSeries> {

	/**
	 * Custom service getNextDocumentNumber
	 *
	 * @return String
	 */
	public String getNextDocumentNumber(DocumentNumberSeriesType type)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return DocumentNumberSeries
	 */
	public DocumentNumberSeries findByBusiness(Integer id);
}
