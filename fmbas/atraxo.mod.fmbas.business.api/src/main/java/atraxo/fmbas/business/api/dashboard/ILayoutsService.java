/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.dashboard;

import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;
import atraxo.fmbas.domain.impl.dashboard.Layouts;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Layouts} domain entity.
 */
public interface ILayoutsService extends IEntityService<Layouts> {

	/**
	 * Find by unique key
	 *
	 * @return Layouts
	 */
	public Layouts findByName(String name);

	/**
	 * Find by unique key
	 *
	 * @return Layouts
	 */
	public Layouts findByBusiness(Integer id);

	/**
	 * Find by reference: dashboardWidgets
	 *
	 * @param dashboardWidgets
	 * @return List<Layouts>
	 */
	public List<Layouts> findByDashboardWidgets(
			DashboardWidgets dashboardWidgets);

	/**
	 * Find by ID of reference: dashboardWidgets.id
	 *
	 * @param dashboardWidgetsId
	 * @return List<Layouts>
	 */
	public List<Layouts> findByDashboardWidgetsId(Integer dashboardWidgetsId);
}
