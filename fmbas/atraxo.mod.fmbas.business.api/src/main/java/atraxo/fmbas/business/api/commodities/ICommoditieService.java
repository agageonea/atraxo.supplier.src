/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.commodities;

import atraxo.fmbas.domain.impl.commodities.Commoditie;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Commoditie} domain entity.
 */
public interface ICommoditieService extends IEntityService<Commoditie> {

	/**
	 * Find by unique key
	 *
	 * @return Commoditie
	 */
	public Commoditie findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return Commoditie
	 */
	public Commoditie findByBusiness(Integer id);
}
