/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.dashboard;

import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;
import atraxo.fmbas.domain.impl.dashboard.Dashboards;
import atraxo.fmbas.domain.impl.dashboard.Layouts;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Dashboards} domain entity.
 */
public interface IDashboardsService extends IEntityService<Dashboards> {

	/**
	 * Find by unique key
	 *
	 * @return Dashboards
	 */
	public Dashboards findByBusiness(Integer id);

	/**
	 * Find by reference: layout
	 *
	 * @param layout
	 * @return List<Dashboards>
	 */
	public List<Dashboards> findByLayout(Layouts layout);

	/**
	 * Find by ID of reference: layout.id
	 *
	 * @param layoutId
	 * @return List<Dashboards>
	 */
	public List<Dashboards> findByLayoutId(Integer layoutId);

	/**
	 * Find by reference: dashboardWidgets
	 *
	 * @param dashboardWidgets
	 * @return List<Dashboards>
	 */
	public List<Dashboards> findByDashboardWidgets(
			DashboardWidgets dashboardWidgets);

	/**
	 * Find by ID of reference: dashboardWidgets.id
	 *
	 * @param dashboardWidgetsId
	 * @return List<Dashboards>
	 */
	public List<Dashboards> findByDashboardWidgetsId(Integer dashboardWidgetsId);
}
