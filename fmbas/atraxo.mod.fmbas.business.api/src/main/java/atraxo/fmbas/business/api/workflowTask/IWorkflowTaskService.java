/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.workflowTask;

import atraxo.fmbas.domain.impl.workflowTask.WorkflowTask;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link WorkflowTask} domain entity.
 */
public interface IWorkflowTaskService extends IEntityService<WorkflowTask> {

	/**
	 * Find by unique key
	 *
	 * @return WorkflowTask
	 */
	public WorkflowTask findByBusiness(Integer id);
}
