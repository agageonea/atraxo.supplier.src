/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.contacts;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Contacts} domain entity.
 */
public interface IContactsService extends IEntityService<Contacts> {

	/**
	 * Custom service deleteContact
	 *
	 * @return void
	 */
	public void deleteContact(Integer id, Class<?> c) throws BusinessException;

	/**
	 * Custom service deleteByEntities
	 *
	 * @return void
	 */
	public void deleteByEntities(List<? extends AbstractEntity> entities,
			Class<?> c) throws BusinessException;

	/**
	 * Custom service findByObjectIdObjectType
	 *
	 * @return List<Contacts>
	 */
	public List<Contacts> findByObjectIdObjectType(Integer objectId,
			String objectType) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Contacts
	 */
	public Contacts findById(Integer id);

	/**
	 * Find by unique key
	 *
	 * @return Contacts
	 */
	public Contacts findByBusiness(Integer id);

	/**
	 * Find by reference: country
	 *
	 * @param country
	 * @return List<Contacts>
	 */
	public List<Contacts> findByCountry(Country country);

	/**
	 * Find by ID of reference: country.id
	 *
	 * @param countryId
	 * @return List<Contacts>
	 */
	public List<Contacts> findByCountryId(Integer countryId);

	/**
	 * Find by reference: notification
	 *
	 * @param notification
	 * @return List<Contacts>
	 */
	public List<Contacts> findByNotification(NotificationEvent notification);

	/**
	 * Find by ID of reference: notification.id
	 *
	 * @param notificationId
	 * @return List<Contacts>
	 */
	public List<Contacts> findByNotificationId(Integer notificationId);
}
