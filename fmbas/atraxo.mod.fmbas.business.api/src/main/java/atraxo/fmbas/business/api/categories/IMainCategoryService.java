/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.categories;

import atraxo.fmbas.domain.impl.categories.MainCategory;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link MainCategory} domain entity.
 */
public interface IMainCategoryService extends IEntityService<MainCategory> {

	/**
	 * Find by unique key
	 *
	 * @return MainCategory
	 */
	public MainCategory findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return MainCategory
	 */
	public MainCategory findByBusiness(Integer id);
}
