/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.categories;

import atraxo.fmbas.domain.impl.categories.IataPC;
import atraxo.fmbas.domain.impl.categories.MainCategory;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link PriceCategory} domain entity.
 */
public interface IPriceCategoryService extends IEntityService<PriceCategory> {

	/**
	 * Custom service findbyName
	 *
	 * @return PriceCategory
	 */
	public PriceCategory findbyName(String name) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return PriceCategory
	 */
	public PriceCategory findByName(String name);

	/**
	 * Find by unique key
	 *
	 * @return PriceCategory
	 */
	public PriceCategory findByBusiness(Integer id);

	/**
	 * Find by reference: mainCategory
	 *
	 * @param mainCategory
	 * @return List<PriceCategory>
	 */
	public List<PriceCategory> findByMainCategory(MainCategory mainCategory);

	/**
	 * Find by ID of reference: mainCategory.id
	 *
	 * @param mainCategoryId
	 * @return List<PriceCategory>
	 */
	public List<PriceCategory> findByMainCategoryId(Integer mainCategoryId);

	/**
	 * Find by reference: iata
	 *
	 * @param iata
	 * @return List<PriceCategory>
	 */
	public List<PriceCategory> findByIata(IataPC iata);

	/**
	 * Find by ID of reference: iata.id
	 *
	 * @param iataId
	 * @return List<PriceCategory>
	 */
	public List<PriceCategory> findByIataId(Integer iataId);
}
