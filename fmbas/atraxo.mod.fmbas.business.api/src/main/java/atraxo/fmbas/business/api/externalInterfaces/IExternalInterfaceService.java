/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.externalInterfaces;

import atraxo.fmbas.domain.impl.dictionary.Dictionary;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceParameter;
import atraxo.fmbas.domain.impl.externalInterfaces.InterfaceUser;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ExternalInterface} domain entity.
 */
public interface IExternalInterfaceService
		extends
			IEntityService<ExternalInterface> {

	/**
	 * Custom service setEnableDissableStatus
	 *
	 * @return void
	 */
	public void setEnableDissableStatus(Integer id, boolean status)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return ExternalInterface
	 */
	public ExternalInterface findByName(String name);

	/**
	 * Find by unique key
	 *
	 * @return ExternalInterface
	 */
	public ExternalInterface findByBusiness(Integer id);

	/**
	 * Find by reference: users
	 *
	 * @param users
	 * @return List<ExternalInterface>
	 */
	public List<ExternalInterface> findByUsers(InterfaceUser users);

	/**
	 * Find by ID of reference: users.id
	 *
	 * @param usersId
	 * @return List<ExternalInterface>
	 */
	public List<ExternalInterface> findByUsersId(Integer usersId);

	/**
	 * Find by reference: params
	 *
	 * @param params
	 * @return List<ExternalInterface>
	 */
	public List<ExternalInterface> findByParams(
			ExternalInterfaceParameter params);

	/**
	 * Find by ID of reference: params.id
	 *
	 * @param paramsId
	 * @return List<ExternalInterface>
	 */
	public List<ExternalInterface> findByParamsId(Integer paramsId);

	/**
	 * Find by reference: dictionaries
	 *
	 * @param dictionaries
	 * @return List<ExternalInterface>
	 */
	public List<ExternalInterface> findByDictionaries(Dictionary dictionaries);

	/**
	 * Find by ID of reference: dictionaries.id
	 *
	 * @param dictionariesId
	 * @return List<ExternalInterface>
	 */
	public List<ExternalInterface> findByDictionariesId(Integer dictionariesId);
}
