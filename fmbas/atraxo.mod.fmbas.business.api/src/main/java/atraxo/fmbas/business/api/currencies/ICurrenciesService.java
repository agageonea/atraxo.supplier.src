/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.currencies;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Currencies} domain entity.
 */
public interface ICurrenciesService extends IEntityService<Currencies> {

	/**
	 * Custom service getSystemCurrency
	 *
	 * @return Currencies
	 */
	public Currencies getSystemCurrency() throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Currencies
	 */
	public Currencies findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return Currencies
	 */
	public Currencies findByBusiness(Integer id);
}
