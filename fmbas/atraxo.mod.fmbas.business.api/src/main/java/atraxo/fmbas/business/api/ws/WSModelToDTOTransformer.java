package atraxo.fmbas.business.api.ws;

import java.util.Collection;
import java.util.Map;

import javax.xml.bind.JAXBException;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.ws.dto.WSPayloadDataDTO;
import atraxo.fmbas.domain.ws.dto.WSTransportDataType;

/**
 * Transformer used to transform a model domain object in DTO used for sending data through an existing WS
 *
 * @author vhojda
 */
public interface WSModelToDTOTransformer<E extends AbstractEntity> {

	/**
	 * Transforms more model entities of type <code>AbstractEntity</code> together with some header-meta <code>WSHeaderData</code> to simple
	 * <code>WSPayloadDataDTO</code> in order to send it via a WS
	 *
	 * @param modelEntities <code>Collection</code> of<code>AbstractEntity</code>
	 * @param paramMap a <code>Map<String, String></code> containg different params
	 * @returns the <code>WSPayloadDataDTO</code>
	 * @throws JAXBException
	 */
	public WSPayloadDataDTO transformModelToDTO(Collection<E> modelEntities, Map<String, String> paramMap) throws JAXBException;

	/**
	 * Gets the type of data that need to be transported (eg. contracts)
	 *
	 * @returns the data type that needs to be transported (imported/exported)
	 */
	public WSTransportDataType getTransportDataType();
}
