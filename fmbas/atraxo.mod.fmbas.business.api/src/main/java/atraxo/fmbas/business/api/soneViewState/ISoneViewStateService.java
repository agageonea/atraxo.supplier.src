/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.soneViewState;

import atraxo.fmbas.domain.impl.soneAdvancedFilter.SoneAdvancedFilter;
import atraxo.fmbas.domain.impl.soneViewState.SoneViewState;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link SoneViewState} domain entity.
 */
public interface ISoneViewStateService extends IEntityService<SoneViewState> {

	/**
	 * Find by unique key
	 *
	 * @return SoneViewState
	 */
	public SoneViewState findById(Integer id);

	/**
	 * Find by unique key
	 *
	 * @return SoneViewState
	 */
	public SoneViewState findByBusiness(Integer id);

	/**
	 * Find by reference: advancedFilter
	 *
	 * @param advancedFilter
	 * @return List<SoneViewState>
	 */
	public List<SoneViewState> findByAdvancedFilter(
			SoneAdvancedFilter advancedFilter);

	/**
	 * Find by ID of reference: advancedFilter.id
	 *
	 * @param advancedFilterId
	 * @return List<SoneViewState>
	 */
	public List<SoneViewState> findByAdvancedFilterId(Integer advancedFilterId);
}
