/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.job;

import atraxo.fmbas.domain.impl.job.BatchJob;
import atraxo.fmbas.domain.impl.job.BatchJobParameter;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link BatchJob} domain entity.
 */
public interface IBatchJobService extends IEntityService<BatchJob> {

	/**
	 * Find by unique key
	 *
	 * @return BatchJob
	 */
	public BatchJob findByName(String name);

	/**
	 * Find by unique key
	 *
	 * @return BatchJob
	 */
	public BatchJob findByBusiness(Integer id);

	/**
	 * Find by reference: jobParameters
	 *
	 * @param jobParameters
	 * @return List<BatchJob>
	 */
	public List<BatchJob> findByJobParameters(BatchJobParameter jobParameters);

	/**
	 * Find by ID of reference: jobParameters.id
	 *
	 * @param jobParametersId
	 * @return List<BatchJob>
	 */
	public List<BatchJob> findByJobParametersId(Integer jobParametersId);
}
