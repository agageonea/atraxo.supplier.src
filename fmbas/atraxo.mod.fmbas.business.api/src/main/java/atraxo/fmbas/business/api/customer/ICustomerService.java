/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.customer;

import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.customer.CustomerLocations;
import atraxo.fmbas.domain.impl.customer.CustomerNotification;
import atraxo.fmbas.domain.impl.exposure.Exposure;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.geo.TimeZone;
import atraxo.fmbas.domain.impl.masteragreements.MasterAgreement;
import atraxo.fmbas.domain.impl.profile.AccountingRules;
import atraxo.fmbas.domain.impl.profile.BankAccount;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Customer} domain entity.
 */
public interface ICustomerService extends IEntityService<Customer> {

	/**
	 * Custom service getValidEffectiveMasterAgreement
	 *
	 * @return MasterAgreement
	 */
	public MasterAgreement getValidEffectiveMasterAgreement(Customer customer,
			Date date) throws BusinessException;

	/**
	 * Custom service getValidMasterAgreement
	 *
	 * @return MasterAgreement
	 */
	public MasterAgreement getValidMasterAgreement(Customer customer, Date date)
			throws BusinessException;

	/**
	 * Custom service getValidCreditLine
	 *
	 * @return CreditLines
	 */
	public CreditLines getValidCreditLine(Customer customer, Date date)
			throws BusinessException;

	/**
	 * Custom service getValidCreditLineWithoutException
	 *
	 * @return CreditLines
	 */
	public CreditLines getValidCreditLineWithoutException(Customer customer,
			Date date) throws BusinessException;

	/**
	 * Custom service getPrimaryContact
	 *
	 * @return Contacts
	 */
	public Contacts getPrimaryContact(Customer customer)
			throws BusinessException;

	/**
	 * Custom service approveCustomers
	 *
	 * @return void
	 */
	public void approveCustomers(List<Customer> customers)
			throws BusinessException;

	/**
	 * Custom service deactivateCustomers
	 *
	 * @return void
	 */
	public void deactivateCustomers(Customer customers, String reason)
			throws BusinessException;

	/**
	 * Custom service blacklistCustomer
	 *
	 * @return void
	 */
	public void blacklistCustomer(Customer customer, String reason)
			throws BusinessException;

	/**
	 * Custom service approveCustomer
	 *
	 * @return void
	 */
	public void approveCustomer(Customer customer, String reason)
			throws BusinessException;

	/**
	 * Custom service convertCreditLimit
	 *
	 * @return BigDecimal
	 */
	public BigDecimal convertCreditLimit(Integer creditLimit,
			String creditCurrencyCode, String currencyCode)
			throws BusinessException;

	/**
	 * Custom service getAllocatedCredit
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getAllocatedCredit() throws BusinessException;

	/**
	 * Custom service getCreditUtilizationRation
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getCreditUtilizationRation() throws BusinessException;

	/**
	 * Custom service promoteFinalCustomers
	 *
	 * @return void
	 */
	public void promoteFinalCustomers(List<Customer> customers)
			throws BusinessException;

	/**
	 * Custom service findSubsidiaries
	 *
	 * @return List<Customer>
	 */
	public List<Customer> findSubsidiaries() throws BusinessException;

	/**
	 * Custom service findCustomer
	 *
	 * @return List<Customer>
	 */
	public List<Customer> findCustomer(List<Integer> ids)
			throws BusinessException;

	/**
	 * Custom service getCustomer
	 *
	 * @return List<Customer>
	 */
	public List<Customer> getCustomer() throws BusinessException;

	/**
	 * Custom service requestCreditStatus
	 *
	 * @return void
	 */
	public void requestCreditStatus(Customer customers)
			throws BusinessException;

	/**
	 * Custom service updateJobSessionUserProfile
	 *
	 * @return void
	 */
	public void updateJobSessionUserProfile(Object obj)
			throws BusinessException;

	/**
	 * Custom service findByIdFetchLocations
	 *
	 * @return Customer
	 */
	public Customer findByIdFetchLocations(Integer id) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Customer
	 */
	public Customer findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return Customer
	 */
	public Customer findByAreas(Area assignedArea, Boolean isSubsidiary);

	/**
	 * Find by unique key
	 *
	 * @return Customer
	 */
	public Customer findByAreas(Long assignedAreaId, Boolean isSubsidiary);

	/**
	 * Find by unique key
	 *
	 * @return Customer
	 */
	public Customer findByBusiness(Integer id);

	/**
	 * Find by reference: parent
	 *
	 * @param parent
	 * @return List<Customer>
	 */
	public List<Customer> findByParent(Customer parent);

	/**
	 * Find by ID of reference: parent.id
	 *
	 * @param parentId
	 * @return List<Customer>
	 */
	public List<Customer> findByParentId(Integer parentId);

	/**
	 * Find by reference: responsibleBuyer
	 *
	 * @param responsibleBuyer
	 * @return List<Customer>
	 */
	public List<Customer> findByResponsibleBuyer(UserSupp responsibleBuyer);

	/**
	 * Find by ID of reference: responsibleBuyer.id
	 *
	 * @param responsibleBuyerId
	 * @return List<Customer>
	 */
	public List<Customer> findByResponsibleBuyerId(String responsibleBuyerId);

	/**
	 * Find by reference: officeCountry
	 *
	 * @param officeCountry
	 * @return List<Customer>
	 */
	public List<Customer> findByOfficeCountry(Country officeCountry);

	/**
	 * Find by ID of reference: officeCountry.id
	 *
	 * @param officeCountryId
	 * @return List<Customer>
	 */
	public List<Customer> findByOfficeCountryId(Integer officeCountryId);

	/**
	 * Find by reference: officeState
	 *
	 * @param officeState
	 * @return List<Customer>
	 */
	public List<Customer> findByOfficeState(Country officeState);

	/**
	 * Find by ID of reference: officeState.id
	 *
	 * @param officeStateId
	 * @return List<Customer>
	 */
	public List<Customer> findByOfficeStateId(Integer officeStateId);

	/**
	 * Find by reference: billingCountry
	 *
	 * @param billingCountry
	 * @return List<Customer>
	 */
	public List<Customer> findByBillingCountry(Country billingCountry);

	/**
	 * Find by ID of reference: billingCountry.id
	 *
	 * @param billingCountryId
	 * @return List<Customer>
	 */
	public List<Customer> findByBillingCountryId(Integer billingCountryId);

	/**
	 * Find by reference: billingState
	 *
	 * @param billingState
	 * @return List<Customer>
	 */
	public List<Customer> findByBillingState(Country billingState);

	/**
	 * Find by ID of reference: billingState.id
	 *
	 * @param billingStateId
	 * @return List<Customer>
	 */
	public List<Customer> findByBillingStateId(Integer billingStateId);

	/**
	 * Find by reference: timeZone
	 *
	 * @param timeZone
	 * @return List<Customer>
	 */
	public List<Customer> findByTimeZone(TimeZone timeZone);

	/**
	 * Find by ID of reference: timeZone.id
	 *
	 * @param timeZoneId
	 * @return List<Customer>
	 */
	public List<Customer> findByTimeZoneId(Integer timeZoneId);

	/**
	 * Find by reference: subsidiaryCurrency
	 *
	 * @param subsidiaryCurrency
	 * @return List<Customer>
	 */
	public List<Customer> findBySubsidiaryCurrency(Currencies subsidiaryCurrency);

	/**
	 * Find by ID of reference: subsidiaryCurrency.id
	 *
	 * @param subsidiaryCurrencyId
	 * @return List<Customer>
	 */
	public List<Customer> findBySubsidiaryCurrencyId(
			Integer subsidiaryCurrencyId);

	/**
	 * Find by reference: defaultVolumeUnit
	 *
	 * @param defaultVolumeUnit
	 * @return List<Customer>
	 */
	public List<Customer> findByDefaultVolumeUnit(Unit defaultVolumeUnit);

	/**
	 * Find by ID of reference: defaultVolumeUnit.id
	 *
	 * @param defaultVolumeUnitId
	 * @return List<Customer>
	 */
	public List<Customer> findByDefaultVolumeUnitId(Integer defaultVolumeUnitId);

	/**
	 * Find by reference: defaultWeightUnit
	 *
	 * @param defaultWeightUnit
	 * @return List<Customer>
	 */
	public List<Customer> findByDefaultWeightUnit(Unit defaultWeightUnit);

	/**
	 * Find by ID of reference: defaultWeightUnit.id
	 *
	 * @param defaultWeightUnitId
	 * @return List<Customer>
	 */
	public List<Customer> findByDefaultWeightUnitId(Integer defaultWeightUnitId);

	/**
	 * Find by reference: assignedArea
	 *
	 * @param assignedArea
	 * @return List<Customer>
	 */
	public List<Customer> findByAssignedArea(Area assignedArea);

	/**
	 * Find by ID of reference: assignedArea.id
	 *
	 * @param assignedAreaId
	 * @return List<Customer>
	 */
	public List<Customer> findByAssignedAreaId(Integer assignedAreaId);

	/**
	 * Find by reference: bankAccounts
	 *
	 * @param bankAccounts
	 * @return List<Customer>
	 */
	public List<Customer> findByBankAccounts(BankAccount bankAccounts);

	/**
	 * Find by ID of reference: bankAccounts.id
	 *
	 * @param bankAccountsId
	 * @return List<Customer>
	 */
	public List<Customer> findByBankAccountsId(Integer bankAccountsId);

	/**
	 * Find by reference: accountingRules
	 *
	 * @param accountingRules
	 * @return List<Customer>
	 */
	public List<Customer> findByAccountingRules(AccountingRules accountingRules);

	/**
	 * Find by ID of reference: accountingRules.id
	 *
	 * @param accountingRulesId
	 * @return List<Customer>
	 */
	public List<Customer> findByAccountingRulesId(Integer accountingRulesId);

	/**
	 * Find by reference: locations
	 *
	 * @param locations
	 * @return List<Customer>
	 */
	public List<Customer> findByLocations(CustomerLocations locations);

	/**
	 * Find by ID of reference: locations.id
	 *
	 * @param locationsId
	 * @return List<Customer>
	 */
	public List<Customer> findByLocationsId(Integer locationsId);

	/**
	 * Find by reference: creditLines
	 *
	 * @param creditLines
	 * @return List<Customer>
	 */
	public List<Customer> findByCreditLines(CreditLines creditLines);

	/**
	 * Find by ID of reference: creditLines.id
	 *
	 * @param creditLinesId
	 * @return List<Customer>
	 */
	public List<Customer> findByCreditLinesId(Integer creditLinesId);

	/**
	 * Find by reference: customerNotification
	 *
	 * @param customerNotification
	 * @return List<Customer>
	 */
	public List<Customer> findByCustomerNotification(
			CustomerNotification customerNotification);

	/**
	 * Find by ID of reference: customerNotification.id
	 *
	 * @param customerNotificationId
	 * @return List<Customer>
	 */
	public List<Customer> findByCustomerNotificationId(
			Integer customerNotificationId);

	/**
	 * Find by reference: reseller
	 *
	 * @param reseller
	 * @return List<Customer>
	 */
	public List<Customer> findByReseller(Customer reseller);

	/**
	 * Find by ID of reference: reseller.id
	 *
	 * @param resellerId
	 * @return List<Customer>
	 */
	public List<Customer> findByResellerId(Integer resellerId);

	/**
	 * Find by reference: finalCustomer
	 *
	 * @param finalCustomer
	 * @return List<Customer>
	 */
	public List<Customer> findByFinalCustomer(Customer finalCustomer);

	/**
	 * Find by ID of reference: finalCustomer.id
	 *
	 * @param finalCustomerId
	 * @return List<Customer>
	 */
	public List<Customer> findByFinalCustomerId(Integer finalCustomerId);

	/**
	 * Find by reference: exposure
	 *
	 * @param exposure
	 * @return List<Customer>
	 */
	public List<Customer> findByExposure(Exposure exposure);

	/**
	 * Find by ID of reference: exposure.id
	 *
	 * @param exposureId
	 * @return List<Customer>
	 */
	public List<Customer> findByExposureId(Integer exposureId);

	/**
	 * Find by reference: masterAgreement
	 *
	 * @param masterAgreement
	 * @return List<Customer>
	 */
	public List<Customer> findByMasterAgreement(MasterAgreement masterAgreement);

	/**
	 * Find by ID of reference: masterAgreement.id
	 *
	 * @param masterAgreementId
	 * @return List<Customer>
	 */
	public List<Customer> findByMasterAgreementId(Integer masterAgreementId);

	/**
	 * Find by reference: aircraft
	 *
	 * @param aircraft
	 * @return List<Customer>
	 */
	public List<Customer> findByAircraft(Aircraft aircraft);

	/**
	 * Find by ID of reference: aircraft.id
	 *
	 * @param aircraftId
	 * @return List<Customer>
	 */
	public List<Customer> findByAircraftId(Integer aircraftId);
}
