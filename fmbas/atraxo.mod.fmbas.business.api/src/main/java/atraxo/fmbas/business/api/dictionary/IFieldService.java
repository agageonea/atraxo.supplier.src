/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.dictionary;

import atraxo.fmbas.domain.impl.dictionary.Field;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Field} domain entity.
 */
public interface IFieldService extends IEntityService<Field> {

	/**
	 * Find by unique key
	 *
	 * @return Field
	 */
	public Field findByCode(Integer externalInterfaceId, String fieldName);

	/**
	 * Find by unique key
	 *
	 * @return Field
	 */
	public Field findByBusiness(Integer id);
}
