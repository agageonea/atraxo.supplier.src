package atraxo.fmbas.business.api.ext.history;

import seava.j4e.api.exceptions.BusinessException;

public interface IMessageFormatterService {
	public Object formatArguments(Object object) throws BusinessException;
}
