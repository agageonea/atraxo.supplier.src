package atraxo.fmbas.business.api.ext.subsidiary;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public interface ISubsidiaryService {

	/**
	 * @return
	 * @throws BusinessException
	 */
	Unit getDefaultVolumeUnit() throws BusinessException;

	/**
	 * @return
	 * @throws BusinessException
	 */
	Currencies getDefaultCurrency() throws BusinessException;

	/**
	 * @return
	 * @throws BusinessException
	 */
	FinancialSources getDefaultFinancialSource() throws BusinessException;

	/**
	 * @return
	 * @throws BusinessException
	 */
	AverageMethod getDefaultAverageMethod() throws BusinessException;
}
