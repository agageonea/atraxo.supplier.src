/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.dictionary;

import atraxo.fmbas.domain.impl.dictionary.Condition;
import atraxo.fmbas.domain.impl.dictionary.Dictionary;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Dictionary} domain entity.
 */
public interface IDictionaryService extends IEntityService<Dictionary> {

	/**
	 * Custom service translate
	 *
	 * @return void
	 */
	public void translate(ExternalInterface externalInterface, Object entity)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Dictionary
	 */
	public Dictionary findByBusiness(Integer id);

	/**
	 * Find by reference: externalInterface
	 *
	 * @param externalInterface
	 * @return List<Dictionary>
	 */
	public List<Dictionary> findByExternalInterface(
			ExternalInterface externalInterface);

	/**
	 * Find by ID of reference: externalInterface.id
	 *
	 * @param externalInterfaceId
	 * @return List<Dictionary>
	 */
	public List<Dictionary> findByExternalInterfaceId(
			Integer externalInterfaceId);

	/**
	 * Find by reference: conditions
	 *
	 * @param conditions
	 * @return List<Dictionary>
	 */
	public List<Dictionary> findByConditions(Condition conditions);

	/**
	 * Find by ID of reference: conditions.id
	 *
	 * @param conditionsId
	 * @return List<Dictionary>
	 */
	public List<Dictionary> findByConditionsId(Integer conditionsId);
}
