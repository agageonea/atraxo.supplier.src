/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.profile;

import atraxo.fmbas.domain.impl.profile.CompanyProfile;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link CompanyProfile} domain entity.
 */
public interface ICompanyProfileService extends IEntityService<CompanyProfile> {

	/**
	 * Custom service create
	 *
	 * @return void
	 */
	public void create(Object obj) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return CompanyProfile
	 */
	public CompanyProfile findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return CompanyProfile
	 */
	public CompanyProfile findByBusiness(Integer id);
}
