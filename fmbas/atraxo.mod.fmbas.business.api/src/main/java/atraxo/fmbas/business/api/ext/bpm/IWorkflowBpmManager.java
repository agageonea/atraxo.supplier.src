/**
 *
 */
package atraxo.fmbas.business.api.ext.bpm;

import java.util.List;
import java.util.Map;

import org.activiti.engine.runtime.ProcessInstance;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public interface IWorkflowBpmManager {

	/**
	 *
	 */
	void redeployWorkflows();

	/**
	 * @param wkfInstanceID
	 * @return
	 */
	boolean canComplete(Integer wkfInstanceID);

	/**
	 * @param wkfInstanceID
	 * @return
	 */
	ProcessInstance getProcessInstanceByWorkflowInstance(Integer wkfInstanceID);

	/**
	 * @param processInstanceId
	 * @param varName
	 * @return
	 */
	Object getProcessInstanceVariable(String processInstanceId, String varName);

	/**
	 * @param wkfInstanceID
	 * @param workflowTaskID
	 * @param accepted
	 * @param acceptanceNote
	 * @return
	 * @throws BusinessException
	 */
	boolean completeTask(Integer wkfInstanceID, String workflowTaskID, Boolean accepted, String acceptanceNote) throws BusinessException;

	/**
	 * @param wkfInstanceID
	 * @param workflowTaskID
	 * @return
	 * @throws BusinessException
	 */
	boolean claimTask(Integer wkfInstanceID, String workflowTaskID) throws BusinessException;

	/**
	 * @param wkfInstanceID
	 * @param accepted
	 * @param acceptanceNote
	 * @return
	 * @throws BusinessException
	 */
	boolean claimCompleteTask(Integer wkfInstanceID, boolean accepted, String acceptanceNote) throws BusinessException;

	/**
	 * @param wkfInstanceID
	 * @param terminationReason
	 * @return
	 * @throws BusinessException
	 */
	boolean terminateWorkflow(Integer wkfInstanceID, String terminationReason) throws BusinessException;

	/**
	 * @param wkfInstanceIDList
	 * @param terminationReason
	 * @return
	 * @throws BusinessException
	 */
	Integer terminateWorkflow(List<Integer> wkfInstanceIDList, String terminationReason) throws BusinessException;

	/**
	 * @param wkfInstanceID
	 * @return
	 * @throws BusinessException
	 */
	boolean resumeWorkflow(Integer wkfInstanceID) throws BusinessException;

	/**
	 * @param workflowName
	 * @param wkfVariables
	 * @param subsidiaryID
	 * @param entities
	 * @return
	 * @throws BusinessException
	 */
	WorkflowStartResult startWorkflow(String workflowName, Map<String, Object> wkfVariables, String subsidiaryID, AbstractEntity... entities)
			throws BusinessException;

	/**
	 * @param workflowId
	 * @param workflowResourceName
	 * @return
	 * @throws BusinessException
	 */
	boolean deployWorkflow(Integer workflowId, String workflowResourceName) throws BusinessException;

	/**
	 * @param wkfName
	 * @param objectId
	 * @param objectType
	 * @return
	 */
	WorkflowInstance getWorkflowInstanceByEntity(WorkflowNames wkfName, Integer objectId, String objectType);

}
