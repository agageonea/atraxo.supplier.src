/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.job;

import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.JobChain;
import atraxo.fmbas.domain.impl.job.JobChainUser;
import atraxo.fmbas.domain.impl.job.Trigger;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link JobChain} domain entity.
 */
public interface IJobChainService extends IEntityService<JobChain> {

	/**
	 * Custom service activate
	 *
	 * @return void
	 */
	public void activate(Integer id) throws BusinessException;

	/**
	 * Custom service deactivate
	 *
	 * @return void
	 */
	public void deactivate(Integer id) throws BusinessException;

	/**
	 * Custom service runNow
	 *
	 * @return void
	 */
	public void runNow(Integer id) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return JobChain
	 */
	public JobChain findByBusiness(Integer id);

	/**
	 * Find by reference: triggers
	 *
	 * @param triggers
	 * @return List<JobChain>
	 */
	public List<JobChain> findByTriggers(Trigger triggers);

	/**
	 * Find by ID of reference: triggers.id
	 *
	 * @param triggersId
	 * @return List<JobChain>
	 */
	public List<JobChain> findByTriggersId(Integer triggersId);

	/**
	 * Find by reference: actions
	 *
	 * @param actions
	 * @return List<JobChain>
	 */
	public List<JobChain> findByActions(Action actions);

	/**
	 * Find by ID of reference: actions.id
	 *
	 * @param actionsId
	 * @return List<JobChain>
	 */
	public List<JobChain> findByActionsId(Integer actionsId);

	/**
	 * Find by reference: users
	 *
	 * @param users
	 * @return List<JobChain>
	 */
	public List<JobChain> findByUsers(JobChainUser users);

	/**
	 * Find by ID of reference: users.id
	 *
	 * @param usersId
	 * @return List<JobChain>
	 */
	public List<JobChain> findByUsersId(Integer usersId);
}
