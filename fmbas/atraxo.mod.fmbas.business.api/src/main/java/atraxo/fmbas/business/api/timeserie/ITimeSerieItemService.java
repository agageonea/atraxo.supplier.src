/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.timeserie;

import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link TimeSerieItem} domain entity.
 */
public interface ITimeSerieItemService extends IEntityService<TimeSerieItem> {

	/**
	 * Custom service getAllElementsBetweenDatesWithMargins
	 *
	 * @return List<TimeSerieItem>
	 */
	public List<TimeSerieItem> getAllElementsBetweenDatesWithMargins(
			Date startDate, Date endDate, Integer serieId, Boolean calculated)
			throws BusinessException;

	/**
	 * Custom service findNextNotCalculatedItem
	 *
	 * @return TimeSerieItem
	 */
	public TimeSerieItem findNextNotCalculatedItem(TimeSerieItem item)
			throws BusinessException;

	/**
	 * Custom service findPrevNotCalculatedItem
	 *
	 * @return TimeSerieItem
	 */
	public TimeSerieItem findPrevNotCalculatedItem(TimeSerieItem item)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return TimeSerieItem
	 */
	public TimeSerieItem findByTsi_business(TimeSerie tserie, Date itemDate);

	/**
	 * Find by unique key
	 *
	 * @return TimeSerieItem
	 */
	public TimeSerieItem findByTsi_business(Long tserieId, Date itemDate);

	/**
	 * Find by unique key
	 *
	 * @return TimeSerieItem
	 */
	public TimeSerieItem findByBusiness(Integer id);

	/**
	 * Find by reference: tserie
	 *
	 * @param tserie
	 * @return List<TimeSerieItem>
	 */
	public List<TimeSerieItem> findByTserie(TimeSerie tserie);

	/**
	 * Find by ID of reference: tserie.id
	 *
	 * @param tserieId
	 * @return List<TimeSerieItem>
	 */
	public List<TimeSerieItem> findByTserieId(Integer tserieId);
}
