/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.profile;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.AccountingRulesType;
import atraxo.fmbas.domain.impl.profile.AccountingRules;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link AccountingRules} domain entity.
 */
public interface IAccountingRulesService
		extends
			IEntityService<AccountingRules> {

	/**
	 * Find by unique key
	 *
	 * @return AccountingRules
	 */
	public AccountingRules findByAccountingRules(AccountingRulesType ruleType,
			Customer subsidiary);

	/**
	 * Find by unique key
	 *
	 * @return AccountingRules
	 */
	public AccountingRules findByAccountingRules(AccountingRulesType ruleType,
			Long subsidiaryId);

	/**
	 * Find by unique key
	 *
	 * @return AccountingRules
	 */
	public AccountingRules findByBusiness(Integer id);

	/**
	 * Find by reference: subsidiary
	 *
	 * @param subsidiary
	 * @return List<AccountingRules>
	 */
	public List<AccountingRules> findBySubsidiary(Customer subsidiary);

	/**
	 * Find by ID of reference: subsidiary.id
	 *
	 * @param subsidiaryId
	 * @return List<AccountingRules>
	 */
	public List<AccountingRules> findBySubsidiaryId(Integer subsidiaryId);
}
