/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.soneAdvancedFilter;

import atraxo.fmbas.domain.impl.soneAdvancedFilter.SoneAdvancedFilter;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link SoneAdvancedFilter} domain entity.
 */
public interface ISoneAdvancedFilterService
		extends
			IEntityService<SoneAdvancedFilter> {

	/**
	 * Custom service getDefaultFilters
	 *
	 * @return List<SoneAdvancedFilter>
	 */
	public List<SoneAdvancedFilter> getDefaultFilters(String cmp)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return SoneAdvancedFilter
	 */
	public SoneAdvancedFilter findById(Integer id);

	/**
	 * Find by unique key
	 *
	 * @return SoneAdvancedFilter
	 */
	public SoneAdvancedFilter findByName_cmp(String name, String cmp);

	/**
	 * Find by unique key
	 *
	 * @return SoneAdvancedFilter
	 */
	public SoneAdvancedFilter findByBusiness(Integer id);
}
