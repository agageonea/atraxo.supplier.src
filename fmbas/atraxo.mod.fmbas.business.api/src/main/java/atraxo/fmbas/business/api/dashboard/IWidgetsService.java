/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.dashboard;

import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;
import atraxo.fmbas.domain.impl.dashboard.WidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.Widgets;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Widgets} domain entity.
 */
public interface IWidgetsService extends IEntityService<Widgets> {

	/**
	 * Find by unique key
	 *
	 * @return Widgets
	 */
	public Widgets findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return Widgets
	 */
	public Widgets findByBusiness(Integer id);

	/**
	 * Find by reference: widgetParams
	 *
	 * @param widgetParams
	 * @return List<Widgets>
	 */
	public List<Widgets> findByWidgetParams(WidgetParameters widgetParams);

	/**
	 * Find by ID of reference: widgetParams.id
	 *
	 * @param widgetParamsId
	 * @return List<Widgets>
	 */
	public List<Widgets> findByWidgetParamsId(Integer widgetParamsId);

	/**
	 * Find by reference: dashboardWidgets
	 *
	 * @param dashboardWidgets
	 * @return List<Widgets>
	 */
	public List<Widgets> findByDashboardWidgets(
			DashboardWidgets dashboardWidgets);

	/**
	 * Find by ID of reference: dashboardWidgets.id
	 *
	 * @param dashboardWidgetsId
	 * @return List<Widgets>
	 */
	public List<Widgets> findByDashboardWidgetsId(Integer dashboardWidgetsId);
}
