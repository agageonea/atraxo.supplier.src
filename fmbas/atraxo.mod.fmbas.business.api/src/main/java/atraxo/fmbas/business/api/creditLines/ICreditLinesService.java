/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.creditLines;

import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.ExposureLastAction;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link CreditLines} domain entity.
 */
public interface ICreditLinesService extends IEntityService<CreditLines> {

	/**
	 * Custom service activate
	 *
	 * @return void
	 */
	public void activate(CreditLines e, String changeReason, String action)
			throws BusinessException;

	/**
	 * Custom service deActivate
	 *
	 * @return void
	 */
	public void deActivate(CreditLines e, String changeReason, String action)
			throws BusinessException;

	/**
	 * Custom service insert
	 *
	 * @return void
	 */
	public void insert(CreditLines e, String changeReason)
			throws BusinessException;

	/**
	 * Custom service update
	 *
	 * @return void
	 */
	public void update(CreditLines e, String changeReason)
			throws BusinessException;

	/**
	 * Custom service updateCreditTriggerExposure
	 *
	 * @return void
	 */
	public void updateCreditTriggerExposure(CreditLines e,
			ExposureLastAction lastAction, String changeReason)
			throws BusinessException;

	/**
	 * Custom service insertCreditTriggerExposure
	 *
	 * @return void
	 */
	public void insertCreditTriggerExposure(CreditLines e,
			ExposureLastAction lastAction, String changeReason)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return CreditLines
	 */
	public CreditLines findByBusiness(Integer id);

	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<CreditLines>
	 */
	public List<CreditLines> findByCurrency(Currencies currency);

	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<CreditLines>
	 */
	public List<CreditLines> findByCurrencyId(Integer currencyId);

	/**
	 * Find by reference: supplier
	 *
	 * @param supplier
	 * @return List<CreditLines>
	 */
	public List<CreditLines> findBySupplier(Suppliers supplier);

	/**
	 * Find by ID of reference: supplier.id
	 *
	 * @param supplierId
	 * @return List<CreditLines>
	 */
	public List<CreditLines> findBySupplierId(Integer supplierId);

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<CreditLines>
	 */
	public List<CreditLines> findByCustomer(Customer customer);

	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<CreditLines>
	 */
	public List<CreditLines> findByCustomerId(Integer customerId);
}
