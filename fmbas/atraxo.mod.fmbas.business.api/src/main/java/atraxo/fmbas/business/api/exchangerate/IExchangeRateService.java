/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.exchangerate;

import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ExchangeRate} domain entity.
 */
public interface IExchangeRateService extends IEntityService<ExchangeRate> {

	/**
	 * Custom service getByTimeseriesAndAvgMethod
	 *
	 * @return ExchangeRate
	 */
	public ExchangeRate getByTimeseriesAndAvgMethod(Integer timeSeriesNo,
			AverageMethod avgMethodIndicator) throws BusinessException;

	/**
	 * Custom service getExchangeRate
	 *
	 * @return ExchangeRate
	 */
	public ExchangeRate getExchangeRate(Currencies fromCurrency,
			Currencies toCurrency, FinancialSources financialSource,
			AverageMethod averageMethod) throws BusinessException;

	/**
	 * Custom service convertExchangeRate
	 *
	 * @return ConversionResult
	 */
	public ConversionResult convertExchangeRate(Currencies fromCurrency,
			Currencies toCurrency, Date date, BigDecimal price, Boolean strict)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return ExchangeRate
	 */
	public ExchangeRate findByBusiness(Integer id);

	/**
	 * Find by reference: tserie
	 *
	 * @param tserie
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByTserie(TimeSerie tserie);

	/**
	 * Find by ID of reference: tserie.id
	 *
	 * @param tserieId
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByTserieId(Integer tserieId);

	/**
	 * Find by reference: currency1
	 *
	 * @param currency1
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByCurrency1(Currencies currency1);

	/**
	 * Find by ID of reference: currency1.id
	 *
	 * @param currency1Id
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByCurrency1Id(Integer currency1Id);

	/**
	 * Find by reference: currency2
	 *
	 * @param currency2
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByCurrency2(Currencies currency2);

	/**
	 * Find by ID of reference: currency2.id
	 *
	 * @param currency2Id
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByCurrency2Id(Integer currency2Id);

	/**
	 * Find by reference: finsource
	 *
	 * @param finsource
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByFinsource(FinancialSources finsource);

	/**
	 * Find by ID of reference: finsource.id
	 *
	 * @param finsourceId
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByFinsourceId(Integer finsourceId);

	/**
	 * Find by reference: avgMethodIndicator
	 *
	 * @param avgMethodIndicator
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByAvgMethodIndicator(
			AverageMethod avgMethodIndicator);

	/**
	 * Find by ID of reference: avgMethodIndicator.id
	 *
	 * @param avgMethodIndicatorId
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByAvgMethodIndicatorId(
			Integer avgMethodIndicatorId);

	/**
	 * Find by reference: exchangeRateValue
	 *
	 * @param exchangeRateValue
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByExchangeRateValue(
			ExchangeRateValue exchangeRateValue);

	/**
	 * Find by ID of reference: exchangeRateValue.id
	 *
	 * @param exchangeRateValueId
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByExchangeRateValueId(
			Integer exchangeRateValueId);
}
