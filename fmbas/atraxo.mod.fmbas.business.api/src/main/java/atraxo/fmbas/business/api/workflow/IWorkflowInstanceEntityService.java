/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.workflow;

import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link WorkflowInstanceEntity} domain entity.
 */
public interface IWorkflowInstanceEntityService
		extends
			IEntityService<WorkflowInstanceEntity> {

	/**
	 * Custom service getEntitiesByWorkflowAndObjectType
	 *
	 * @return List<WorkflowInstanceEntity>
	 */
	public List<WorkflowInstanceEntity> getEntitiesByWorkflowAndObjectType(
			String workflowName, String className) throws BusinessException;

	/**
	 * Custom service getEntityesByObjectIdObjectType
	 *
	 * @return List<WorkflowInstanceEntity>
	 */
	public List<WorkflowInstanceEntity> getEntityesByObjectIdObjectType(
			Integer objectId, String objectType) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return WorkflowInstanceEntity
	 */
	public WorkflowInstanceEntity findByBusinessKey(String name,
			Integer objectId, String objectType);

	/**
	 * Find by unique key
	 *
	 * @return WorkflowInstanceEntity
	 */
	public WorkflowInstanceEntity findByBusiness(Integer id);

	/**
	 * Find by reference: workflowInstance
	 *
	 * @param workflowInstance
	 * @return List<WorkflowInstanceEntity>
	 */
	public List<WorkflowInstanceEntity> findByWorkflowInstance(
			WorkflowInstance workflowInstance);

	/**
	 * Find by ID of reference: workflowInstance.id
	 *
	 * @param workflowInstanceId
	 * @return List<WorkflowInstanceEntity>
	 */
	public List<WorkflowInstanceEntity> findByWorkflowInstanceId(
			Integer workflowInstanceId);
}
