/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.geo;

import atraxo.fmbas.domain.impl.geo.Country;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Country} domain entity.
 */
public interface ICountryService extends IEntityService<Country> {

	/**
	 * Find by unique key
	 *
	 * @return Country
	 */
	public Country findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return Country
	 */
	public Country findByBusiness(Integer id);

	/**
	 * Find by reference: country
	 *
	 * @param country
	 * @return List<Country>
	 */
	public List<Country> findByCountry(Country country);

	/**
	 * Find by ID of reference: country.id
	 *
	 * @param countryId
	 * @return List<Country>
	 */
	public List<Country> findByCountryId(Integer countryId);
}
