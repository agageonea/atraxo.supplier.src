/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.timeserie;

import atraxo.fmbas.domain.impl.timeserie.CompositeTimeSeriesSource;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link CompositeTimeSeriesSource} domain entity.
 */
public interface ICompositeTimeSeriesSourceService
		extends
			IEntityService<CompositeTimeSeriesSource> {

	/**
	 * Find by unique key
	 *
	 * @return CompositeTimeSeriesSource
	 */
	public CompositeTimeSeriesSource findByBusiness(Integer id);

	/**
	 * Find by reference: composite
	 *
	 * @param composite
	 * @return List<CompositeTimeSeriesSource>
	 */
	public List<CompositeTimeSeriesSource> findByComposite(TimeSerie composite);

	/**
	 * Find by ID of reference: composite.id
	 *
	 * @param compositeId
	 * @return List<CompositeTimeSeriesSource>
	 */
	public List<CompositeTimeSeriesSource> findByCompositeId(Integer compositeId);

	/**
	 * Find by reference: source
	 *
	 * @param source
	 * @return List<CompositeTimeSeriesSource>
	 */
	public List<CompositeTimeSeriesSource> findBySource(TimeSerie source);

	/**
	 * Find by ID of reference: source.id
	 *
	 * @param sourceId
	 * @return List<CompositeTimeSeriesSource>
	 */
	public List<CompositeTimeSeriesSource> findBySourceId(Integer sourceId);
}
