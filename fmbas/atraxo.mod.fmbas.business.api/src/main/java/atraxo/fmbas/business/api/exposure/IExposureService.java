/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.exposure;

import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.exposure.Exposure;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.fmbas_type.ExposureLastAction;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Exposure} domain entity.
 */
public interface IExposureService extends IEntityService<Exposure> {

	/**
	 * Custom service getExposure
	 *
	 * @return Exposure
	 */
	public Exposure getExposure(CreditLines creditLine)
			throws BusinessException;

	/**
	 * Custom service createExposure
	 *
	 * @return void
	 */
	public void createExposure(CreditLines creditLine) throws BusinessException;

	/**
	 * Custom service updateExposure
	 *
	 * @return void
	 */
	public void updateExposure(CreditLines creditLine,
			ExposureLastAction lastAction) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Exposure
	 */
	public Exposure findByKey(Customer customer, CreditTerm type);

	/**
	 * Find by unique key
	 *
	 * @return Exposure
	 */
	public Exposure findByKey(Long customerId, CreditTerm type);

	/**
	 * Find by unique key
	 *
	 * @return Exposure
	 */
	public Exposure findByBusiness(Integer id);

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<Exposure>
	 */
	public List<Exposure> findByCustomer(Customer customer);

	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<Exposure>
	 */
	public List<Exposure> findByCustomerId(Integer customerId);

	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<Exposure>
	 */
	public List<Exposure> findByCurrency(Currencies currency);

	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<Exposure>
	 */
	public List<Exposure> findByCurrencyId(Integer currencyId);
}
