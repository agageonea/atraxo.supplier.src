/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.job;

import atraxo.fmbas.domain.impl.job.JobChain;
import atraxo.fmbas.domain.impl.job.Trigger;
import java.util.Date;
import java.util.List;
import org.quartz.JobListener;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Trigger} domain entity.
 */
public interface ITriggerService extends IEntityService<Trigger> {

	/**
	 * Custom service scheduleTrigger
	 *
	 * @return Date
	 */
	public Date scheduleTrigger(Trigger e, JobListener... listeners)
			throws BusinessException;

	/**
	 * Custom service unscheduleTrigger
	 *
	 * @return void
	 */
	public void unscheduleTrigger(Trigger e) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Trigger
	 */
	public Trigger findByBusiness(Integer id);

	/**
	 * Find by reference: jobChain
	 *
	 * @param jobChain
	 * @return List<Trigger>
	 */
	public List<Trigger> findByJobChain(JobChain jobChain);

	/**
	 * Find by ID of reference: jobChain.id
	 *
	 * @param jobChainId
	 * @return List<Trigger>
	 */
	public List<Trigger> findByJobChainId(Integer jobChainId);
}
