package atraxo.fmbas.business.api.ext.bpm;

import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Enum Constants for all the Existing Workflows (system)
 *
 * @author vhojda
 * @author aradu
 */
public enum WorkflowNames {

	BID_APPROVAL("Bid Approval", "bidApprovalWorkflowTerminator"),
	CREDIT_MEMO_APPROVAL("Credit Memo Approval", "creditMemoApprovalWorkflowTerminator"),
	FUEL_TICKET_APPROVAL("Fuel Ticket Approval", "fuelTicketApprovalWorkflowTerminator"),
	FUEL_TICKET_RELEASE("Fuel Ticket Release", "fuelTicketReleaseWorkflowTerminator"),
	FUEL_TICKET_CANCEL("Fuel Ticket Cancel", "fuelTicketCancelWorkflowTerminator"),
	PRICE_UPDATE_APPROVAL("Price Update Approval", "priceUpdateApprovalWorkflowTerminator"),
	SELL_CONTRACT_APPROVAL("Sell Contract Approval", "contractApprovalWorkflowTerminator"),
	PURCHASE_CONTRACT_APPROVAL("Purchase Contract Approval", "contractApprovalWorkflowTerminator"),
	ENERGY_PRICE_TIME_SERIES_APPROVAL("Energy Price Time Series Approval", "energyPriceTimeSeriesApprovalWorkflowTerminator"),
	EXCHANGE_RATE_TIME_SERIES_APPROVAL("Exchange Rate Time Series Approval", "exchangeRateTimeSeriesApprovalWorkflowTerminator"),
	SALE_CONTRACT_PERIOD_UPDATE_APPROVAL("Sale Contract Period Update Approval", "periodUpdateApprovalWorkflowTerminator"),
	SALE_CONTRACT_SHIPTO_UPDATE_APPROVAL("Sale Contract ShipTo Update Approval", "shipToUpdateApprovalWorkflowTerminator"),
	SALE_CONTRACT_PRICE_CATEGORY_APPROVAL("Sale Contract Price Category Approval", "priceCategoryApprovalWorkflowTerminator"),
	SEND_INVOICE_NOTIFICATIONS("Send Invoice Notifications", "sendInvoiceNotificationsWorkflowTerminator"),
	UPLOAD_INVOICE_RICOH_XML("Upload Invoice Ricoh XML", "uploadInvoiceRicohXMLWorkflowTerminator");

	private String workflowName;
	private String workflowTerminatorName;

	private WorkflowNames(String workflowName, String workflowTerminatorName) {
		this.workflowName = workflowName;
		this.workflowTerminatorName = workflowTerminatorName;
	}

	public static WorkflowNames getByName(String name) {
		for (WorkflowNames wName : values()) {
			if (wName.getWorkflowName().equalsIgnoreCase(name)) {
				return wName;
			}
		}
		throw new InvalidEnumException("Inexistent WorkflowNames with name: " + name);
	}

	public String getWorkflowName() {
		return this.workflowName;
	}

	public String getWorkflowTerminatorName() {
		return this.workflowTerminatorName;
	}

}
