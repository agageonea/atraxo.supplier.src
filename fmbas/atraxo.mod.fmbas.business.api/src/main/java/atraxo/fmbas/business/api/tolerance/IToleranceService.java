/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.tolerance;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Tolerance} domain entity.
 */
public interface IToleranceService extends IEntityService<Tolerance> {

	/**
	 * Custom service getVolumeTolerance
	 *
	 * @return Tolerance
	 */
	public Tolerance getVolumeTolerance() throws BusinessException;

	/**
	 * Custom service getEventTolerance
	 *
	 * @return Tolerance
	 */
	public Tolerance getEventTolerance() throws BusinessException;

	/**
	 * Custom service getPercentageTolerance
	 *
	 * @return Tolerance
	 */
	public Tolerance getPercentageTolerance() throws BusinessException;

	/**
	 * Custom service getMarkUpTolerance
	 *
	 * @return Tolerance
	 */
	public Tolerance getMarkUpTolerance() throws BusinessException;

	/**
	 * Custom service getTankCapacityTolerance
	 *
	 * @return Tolerance
	 */
	public Tolerance getTankCapacityTolerance() throws BusinessException;

	/**
	 * Custom service getAccrualAutoCloseTolerance
	 *
	 * @return Tolerance
	 */
	public Tolerance getAccrualAutoCloseTolerance() throws BusinessException;

	/**
	 * Custom service getConversionFactorTolerance
	 *
	 * @return Tolerance
	 */
	public Tolerance getConversionFactorTolerance() throws BusinessException;

	/**
	 * Custom service getTolerance
	 *
	 * @return Tolerance
	 */
	public Tolerance getTolerance(String name) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Tolerance
	 */
	public Tolerance findByName(String name);

	/**
	 * Find by unique key
	 *
	 * @return Tolerance
	 */
	public Tolerance findByBusiness(Integer id);

	/**
	 * Find by reference: refUnit
	 *
	 * @param refUnit
	 * @return List<Tolerance>
	 */
	public List<Tolerance> findByRefUnit(Unit refUnit);

	/**
	 * Find by ID of reference: refUnit.id
	 *
	 * @param refUnitId
	 * @return List<Tolerance>
	 */
	public List<Tolerance> findByRefUnitId(Integer refUnitId);

	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<Tolerance>
	 */
	public List<Tolerance> findByCurrency(Currencies currency);

	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<Tolerance>
	 */
	public List<Tolerance> findByCurrencyId(Integer currencyId);
}
