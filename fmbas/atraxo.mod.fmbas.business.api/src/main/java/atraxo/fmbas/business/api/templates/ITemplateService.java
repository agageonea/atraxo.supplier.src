/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.templates;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.customer.CustomerNotification;
import atraxo.fmbas.domain.impl.externalInterfaces.InterfaceUser;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import atraxo.fmbas.domain.impl.templates.Template;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Template} domain entity.
 */
public interface ITemplateService extends IEntityService<Template> {

	/**
	 * Custom service insertTemplate
	 *
	 * @return void
	 */
	public void insertTemplate(byte[] file, String fileName,
			String description, String name, String typeName, Boolean systemUse)
			throws BusinessException;

	/**
	 * Custom service updateTemplate
	 *
	 * @return void
	 */
	public void updateTemplate(byte[] file, String fileName, Template template,
			String name, String description, Boolean systemUse)
			throws BusinessException;

	/**
	 * Custom service getPreview
	 *
	 * @return byte[]
	 */
	public byte[] getPreview(Template template, String jsonData)
			throws BusinessException;

	/**
	 * Custom service generateTemplate
	 *
	 * @return Template
	 */
	public Template generateTemplate(String description, String name,
			String typeName, String fileName, Boolean systemUse)
			throws BusinessException;

	/**
	 * Custom service generateTemplate
	 *
	 * @return Template
	 */
	public Template generateTemplate(UserSupp user, String description,
			String name, String typeName, String fileName, Boolean systemUse)
			throws BusinessException;

	/**
	 * Custom service loadTemplateFromDisk
	 *
	 * @return byte[]
	 */
	public byte[] loadTemplateFromDisk(Template e) throws BusinessException;

	/**
	 * Custom service getTemplateForExport
	 *
	 * @return Template
	 */
	public Template getTemplateForExport() throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Template
	 */
	public Template findByKey(UserSupp user, Customer subsidiary, String name);

	/**
	 * Find by unique key
	 *
	 * @return Template
	 */
	public Template findByKey(Long userId, Long subsidiaryId, String name);

	/**
	 * Find by unique key
	 *
	 * @return Template
	 */
	public Template findByBusiness(Integer id);

	/**
	 * Find by reference: type
	 *
	 * @param type
	 * @return List<Template>
	 */
	public List<Template> findByType(NotificationEvent type);

	/**
	 * Find by ID of reference: type.id
	 *
	 * @param typeId
	 * @return List<Template>
	 */
	public List<Template> findByTypeId(Integer typeId);

	/**
	 * Find by reference: user
	 *
	 * @param user
	 * @return List<Template>
	 */
	public List<Template> findByUser(UserSupp user);

	/**
	 * Find by ID of reference: user.id
	 *
	 * @param userId
	 * @return List<Template>
	 */
	public List<Template> findByUserId(String userId);

	/**
	 * Find by reference: subsidiary
	 *
	 * @param subsidiary
	 * @return List<Template>
	 */
	public List<Template> findBySubsidiary(Customer subsidiary);

	/**
	 * Find by ID of reference: subsidiary.id
	 *
	 * @param subsidiaryId
	 * @return List<Template>
	 */
	public List<Template> findBySubsidiaryId(Integer subsidiaryId);

	/**
	 * Find by reference: interfaceUser
	 *
	 * @param interfaceUser
	 * @return List<Template>
	 */
	public List<Template> findByInterfaceUser(InterfaceUser interfaceUser);

	/**
	 * Find by ID of reference: interfaceUser.id
	 *
	 * @param interfaceUserId
	 * @return List<Template>
	 */
	public List<Template> findByInterfaceUserId(Integer interfaceUserId);

	/**
	 * Find by reference: customerNotification
	 *
	 * @param customerNotification
	 * @return List<Template>
	 */
	public List<Template> findByCustomerNotification(
			CustomerNotification customerNotification);

	/**
	 * Find by ID of reference: customerNotification.id
	 *
	 * @param customerNotificationId
	 * @return List<Template>
	 */
	public List<Template> findByCustomerNotificationId(
			Integer customerNotificationId);
}
