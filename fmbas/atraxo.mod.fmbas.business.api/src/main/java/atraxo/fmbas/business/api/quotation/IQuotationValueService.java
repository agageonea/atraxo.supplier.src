/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.quotation;

import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link QuotationValue} domain entity.
 */
public interface IQuotationValueService extends IEntityService<QuotationValue> {

	/**
	 * Custom service getQuotationValuesByBusinessKey
	 *
	 * @return QuotationValue
	 */
	public QuotationValue getQuotationValuesByBusinessKey(
			QuotationValue quotationValue) throws BusinessException;

	/**
	 * Custom service getQuotationValueByDate
	 *
	 * @return QuotationValue
	 */
	public QuotationValue getQuotationValueByDate(Quotation quotation, Date date)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return QuotationValue
	 */
	public QuotationValue findByBusinessKey(Quotation quotation,
			Date validFromDate, Date validToDate);

	/**
	 * Find by unique key
	 *
	 * @return QuotationValue
	 */
	public QuotationValue findByBusinessKey(Long quotationId,
			Date validFromDate, Date validToDate);

	/**
	 * Find by unique key
	 *
	 * @return QuotationValue
	 */
	public QuotationValue findByBusiness(Integer id);

	/**
	 * Find by reference: quotation
	 *
	 * @param quotation
	 * @return List<QuotationValue>
	 */
	public List<QuotationValue> findByQuotation(Quotation quotation);

	/**
	 * Find by ID of reference: quotation.id
	 *
	 * @param quotationId
	 * @return List<QuotationValue>
	 */
	public List<QuotationValue> findByQuotationId(Integer quotationId);
}
