/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.user;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link UserSubsidiary} domain entity.
 */
public interface IUserSubsidiaryService extends IEntityService<UserSubsidiary> {

	/**
	 * Custom service setDefaultSubsidiary
	 *
	 * @return void
	 */
	public void setDefaultSubsidiary(Integer id) throws BusinessException;

	/**
	 * Custom service getDefaultSubsidiary
	 *
	 * @return UserSubsidiary
	 */
	public UserSubsidiary getDefaultSubsidiary(UserSupp user)
			throws BusinessException;

	/**
	 * Custom service removeSubsidiary
	 *
	 * @return void
	 */
	public void removeSubsidiary(List<UserSubsidiary> usList)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return UserSubsidiary
	 */
	public UserSubsidiary findByKey(UserSupp user, Customer subsidiary);

	/**
	 * Find by unique key
	 *
	 * @return UserSubsidiary
	 */
	public UserSubsidiary findByKey(Long userId, Long subsidiaryId);

	/**
	 * Find by unique key
	 *
	 * @return UserSubsidiary
	 */
	public UserSubsidiary findByBusiness(Integer id);

	/**
	 * Find by reference: user
	 *
	 * @param user
	 * @return List<UserSubsidiary>
	 */
	public List<UserSubsidiary> findByUser(UserSupp user);

	/**
	 * Find by ID of reference: user.id
	 *
	 * @param userId
	 * @return List<UserSubsidiary>
	 */
	public List<UserSubsidiary> findByUserId(String userId);

	/**
	 * Find by reference: subsidiary
	 *
	 * @param subsidiary
	 * @return List<UserSubsidiary>
	 */
	public List<UserSubsidiary> findBySubsidiary(Customer subsidiary);

	/**
	 * Find by ID of reference: subsidiary.id
	 *
	 * @param subsidiaryId
	 * @return List<UserSubsidiary>
	 */
	public List<UserSubsidiary> findBySubsidiaryId(Integer subsidiaryId);
}
