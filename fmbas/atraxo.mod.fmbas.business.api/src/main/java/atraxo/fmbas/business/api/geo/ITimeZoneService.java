/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.geo;

import atraxo.fmbas.domain.impl.geo.TimeZone;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link TimeZone} domain entity.
 */
public interface ITimeZoneService extends IEntityService<TimeZone> {

	/**
	 * Find by unique key
	 *
	 * @return TimeZone
	 */
	public TimeZone findByName(String name);

	/**
	 * Find by unique key
	 *
	 * @return TimeZone
	 */
	public TimeZone findByBusiness(Integer id);
}
