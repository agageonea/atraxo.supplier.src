/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.dashboard;

import atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;
import atraxo.fmbas.domain.impl.dashboard.WidgetParameters;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link AssignedWidgetParameters} domain entity.
 */
public interface IAssignedWidgetParametersService
		extends
			IEntityService<AssignedWidgetParameters> {

	/**
	 * Custom service getPeriod
	 *
	 * @return String
	 */
	public String getPeriod(
			List<AssignedWidgetParameters> assignedWidgetParameter)
			throws BusinessException;

	/**
	 * Custom service getUnit
	 *
	 * @return String
	 */
	public String getUnit(List<AssignedWidgetParameters> assignedWidgetParameter)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return AssignedWidgetParameters
	 */
	public AssignedWidgetParameters findByBusiness(Integer id);

	/**
	 * Find by reference: dashboardWidgets
	 *
	 * @param dashboardWidgets
	 * @return List<AssignedWidgetParameters>
	 */
	public List<AssignedWidgetParameters> findByDashboardWidgets(
			DashboardWidgets dashboardWidgets);

	/**
	 * Find by ID of reference: dashboardWidgets.id
	 *
	 * @param dashboardWidgetsId
	 * @return List<AssignedWidgetParameters>
	 */
	public List<AssignedWidgetParameters> findByDashboardWidgetsId(
			Integer dashboardWidgetsId);

	/**
	 * Find by reference: widgetParameters
	 *
	 * @param widgetParameters
	 * @return List<AssignedWidgetParameters>
	 */
	public List<AssignedWidgetParameters> findByWidgetParameters(
			WidgetParameters widgetParameters);

	/**
	 * Find by ID of reference: widgetParameters.id
	 *
	 * @param widgetParametersId
	 * @return List<AssignedWidgetParameters>
	 */
	public List<AssignedWidgetParameters> findByWidgetParametersId(
			Integer widgetParametersId);
}
