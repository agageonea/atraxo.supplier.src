/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.api.dashboard;

import atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.WidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.Widgets;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link WidgetParameters} domain entity.
 */
public interface IWidgetParametersService
		extends
			IEntityService<WidgetParameters> {

	/**
	 * Custom service getPeriod
	 *
	 * @return String
	 */
	public String getPeriod(Widgets widget) throws BusinessException;

	/**
	 * Custom service getUnit
	 *
	 * @return String
	 */
	public String getUnit(Widgets widget) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return WidgetParameters
	 */
	public WidgetParameters findByName(String name);

	/**
	 * Find by unique key
	 *
	 * @return WidgetParameters
	 */
	public WidgetParameters findByBusiness(Integer id);

	/**
	 * Find by reference: widget
	 *
	 * @param widget
	 * @return List<WidgetParameters>
	 */
	public List<WidgetParameters> findByWidget(Widgets widget);

	/**
	 * Find by ID of reference: widget.id
	 *
	 * @param widgetId
	 * @return List<WidgetParameters>
	 */
	public List<WidgetParameters> findByWidgetId(Integer widgetId);

	/**
	 * Find by reference: assignedWidgetParameters
	 *
	 * @param assignedWidgetParameters
	 * @return List<WidgetParameters>
	 */
	public List<WidgetParameters> findByAssignedWidgetParameters(
			AssignedWidgetParameters assignedWidgetParameters);

	/**
	 * Find by ID of reference: assignedWidgetParameters.id
	 *
	 * @param assignedWidgetParametersId
	 * @return List<WidgetParameters>
	 */
	public List<WidgetParameters> findByAssignedWidgetParametersId(
			Integer assignedWidgetParametersId);
}
