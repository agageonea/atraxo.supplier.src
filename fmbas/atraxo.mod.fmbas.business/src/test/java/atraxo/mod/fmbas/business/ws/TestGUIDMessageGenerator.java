package atraxo.mod.fmbas.business.ws;

import static org.junit.Assert.assertFalse;

import org.junit.Test;

import atraxo.fmbas.business.ws.GUIDMessageGenerator;

public class TestGUIDMessageGenerator {

	@Test
	public void testExtractClientIdentifier() {
		String dev = "DEV";
		String response = GUIDMessageGenerator.extractClientIdentifier(dev + "-321");
		assertFalse(!response.equals(dev));
		response = GUIDMessageGenerator.extractClientIdentifier(dev);
		assertFalse(!response.equals(dev));
	}

}
