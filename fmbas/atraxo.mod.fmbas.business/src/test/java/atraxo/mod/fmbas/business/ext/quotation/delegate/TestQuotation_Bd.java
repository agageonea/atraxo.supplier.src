package atraxo.mod.fmbas.business.ext.quotation.delegate;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import atraxo.fmbas.business.api.quotation.IQuotationService;
import atraxo.fmbas.business.api.quotation.IQuotationValueService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieAverageService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieItemService;
import atraxo.fmbas.business.ext.calculator.Method;
import atraxo.fmbas.business.ext.quotation.delegate.Quotation_Bd;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieAverage;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.Session;
import seava.j4e.commons.security.AppClient;
import seava.j4e.commons.security.AppUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:/META-INF/spring/jee/fmbas-business-test-context.xml" })
public class TestQuotation_Bd {

	private static final Logger LOGGER = LoggerFactory.getLogger(TestQuotation_Bd.class);

	@Autowired
	private Quotation_Bd quotationBd;

	@Autowired
	private ITimeSerieAverageService timeSerieAvgService;
	@Autowired
	private IQuotationService quotationService;
	@Autowired
	private ITimeSerieItemService tsiService;
	@Autowired
	private IQuotationValueService quotationValueService;

	@BeforeClass
	public static void beforeClass() {
		AppClient client = new AppClient("id", "code", "name");
		IUser user = new AppUser("code", "name", "loginName", "password", null, null, client, null, null, null, false);
		Session.user.set(user);
	}

	@Before
	public void setUp() {
		EasyMock.reset(this.timeSerieAvgService, this.quotationService, this.tsiService, this.quotationValueService);
	}

	@Test
	public void testCalculateQuotationForStartPeriod() {
		this.assertServices();

		TimeSerie timeSerie = new TimeSerie();
		timeSerie.setId(1);
		timeSerie.addToTimeserieItems(this.buildTimeSerieItem(timeSerie, -1));
		TimeSerieItem tsi = this.buildTimeSerieItem(timeSerie, 0);
		timeSerie.addToTimeserieItems(tsi);

		ArrayList<TimeSerieAverage> tSeriesAverages = new ArrayList<TimeSerieAverage>();

		AverageMethod avgMethod = new AverageMethod();
		avgMethod.setCode(Method.WC.name());
		this.buildTimeSeriesAverage(timeSerie, tSeriesAverages, avgMethod);

		Quotation quotation = new Quotation();
		quotation.setAvgMethodIndicator(avgMethod);
		List<Quotation> quotations = new ArrayList<>();

		try {
			EasyMock.expect(this.quotationService.findByTimeseries(EasyMock.anyObject(TimeSerie.class))).andReturn(quotations).once();
			EasyMock.expect(this.quotationService.getByTimeseriesAndAvgMethod(EasyMock.anyInt(), EasyMock.anyObject(AverageMethod.class)))
					.andReturn(quotation).once();
			this.quotationService.delete(EasyMock.<List<Quotation>> anyObject());
			this.quotationService.update(EasyMock.<List<Quotation>> anyObject());
			EasyMock.expectLastCall();
			this.replayAll();
			this.quotationBd.updateAverages(timeSerie);
		} catch (BusinessException e) {
			e.printStackTrace();
			LOGGER.error("Business exception:", e);
		}
		this.verifyAll();

	}

	private void verifyAll() {
		EasyMock.verify(this.timeSerieAvgService);
		EasyMock.verify(this.quotationService);
		EasyMock.verify(this.tsiService);
		EasyMock.verify(this.quotationValueService);
	}

	private void replayAll() {
		EasyMock.replay(this.timeSerieAvgService);
		EasyMock.replay(this.quotationService);
		EasyMock.replay(this.tsiService);
		EasyMock.replay(this.quotationValueService);
	}

	private void buildTimeSeriesAverage(TimeSerie timeSerie, ArrayList<TimeSerieAverage> tSeriesAverages, AverageMethod avgMethod) {
		TimeSerieAverage timeSerieAvg = new TimeSerieAverage();
		timeSerieAvg.setActive(true);
		timeSerieAvg.setAveragingMethod(avgMethod);
		timeSerie.addToTimeserieAvg(timeSerieAvg);
	}

	private TimeSerieItem buildTimeSerieItem(TimeSerie timeSerie, int days) {
		TimeSerieItem timeSerieItem = new TimeSerieItem();
		timeSerieItem.setItemDate(DateUtils.addDays(Calendar.getInstance().getTime(), days));
		timeSerieItem.setItemValue(BigDecimal.valueOf(1));
		timeSerieItem.setCalculated(false);
		timeSerieItem.setTserie(timeSerie);
		return timeSerieItem;
	}

	private void assertServices() {
		assertNotNull(this.quotationBd);
		assertNotNull(this.timeSerieAvgService);
		assertNotNull(this.tsiService);
		assertNotNull(this.quotationService);
		assertNotNull(this.quotationValueService);
	}
}
