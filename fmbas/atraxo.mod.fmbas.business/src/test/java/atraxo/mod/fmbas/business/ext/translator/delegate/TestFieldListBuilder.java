package atraxo.mod.fmbas.business.ext.translator.delegate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.ext.dictionary.delegate.FieldListBuilder;
import seava.j4e.business.utils.BigDecimalFormater;

public class TestFieldListBuilder {

	private static final Logger LOGGER = LoggerFactory.getLogger(TestFieldListBuilder.class);

	@Test
	public void verify() {
		List<String> listStr = FieldListBuilder.build(DummyData.class, "");
		System.out.println(listStr);
		Assert.assertTrue(!CollectionUtils.isEmpty(listStr));
	}

	@Test
	public void testGetValue() {
		List<DummyListData> list = new ArrayList<>();
		DummyData dm = new DummyData(list, "test1", 1, BigDecimal.ZERO);
		try {
			Object value = FieldListBuilder.getValue(DummyData.class, dm, "str");

			Assert.assertTrue("test1".equals(value));
		} catch (NoSuchFieldException e) {
			LOGGER.error("Exception:", e);
			Assert.assertFalse(true);
		} catch (SecurityException e) {
			LOGGER.error("Exception:", e);
			Assert.assertFalse(true);
		} catch (IllegalArgumentException e) {
			LOGGER.error("Exception:", e);
			Assert.assertFalse(true);
		} catch (IllegalAccessException e) {
			LOGGER.error("Exception:", e);
			Assert.assertFalse(true);
		}
	}

	@Test
	public void testBigDecimalFormater() {
		List<DummyListData> list = new ArrayList<>();
		DummyListData dld1 = new DummyListData("a1", "a2", null, TestEnum._EMPTY_);
		DummyListData dld2 = new DummyListData("b1", "b2", BigDecimal.ONE, TestEnum._EMPTY_);
		list.add(dld1);
		list.add(dld2);
		DummyData dm = new DummyData(list, "test1", 1, BigDecimal.TEN);
		try {
			BigDecimalFormater.format(dm, 6);
			Assert.assertTrue(dm.getNum().toString().split("\\.")[1].length() == 6);
		} catch (IllegalArgumentException e) {
			LOGGER.error("Exception:", e);
			e.printStackTrace();
			Assert.assertFalse(true);
		} catch (IllegalAccessException e) {
			LOGGER.error("Exception:", e);
			e.printStackTrace();
			Assert.assertFalse(true);
		}
	}

	@Test
	public void testChange() {
		List<DummyListData> list = new ArrayList<>();
		DummyListData dld1 = new DummyListData("a1", "a2", BigDecimal.ZERO, TestEnum._EMPTY_);
		DummyListData dld2 = new DummyListData("b1", "b2", BigDecimal.ZERO, TestEnum._EMPTY_);
		list.add(dld1);
		list.add(dld2);
		DummyData dm = new DummyData(list, "test1", 1, BigDecimal.ZERO);
		try {
			FieldListBuilder.setValue(DummyData.class, dm, "list/name1", "name3", "a1");
			Assert.assertTrue(list.get(0).getName1().equals("name3"));
		} catch (NoSuchFieldException e) {
			LOGGER.error("Exception:", e);
			Assert.assertFalse(true);
		} catch (SecurityException e) {
			LOGGER.error("Exception:", e);
			Assert.assertFalse(true);
		} catch (IllegalArgumentException e) {
			LOGGER.error("Exception:", e);
			Assert.assertFalse(true);
		} catch (IllegalAccessException e) {
			LOGGER.error("Exception:", e);
			Assert.assertFalse(true);
		}
	}

	protected class DummyData {

		List<DummyListData> list;
		String str;
		int criteria;
		BigDecimal num;

		public DummyData(List<DummyListData> list, String str, int criteria, BigDecimal num) {
			super();
			this.list = list;
			this.str = str;
			this.criteria = criteria;
			this.num = num;
		}

		public List<DummyListData> getList() {
			return this.list;
		}

		public void setList(List<DummyListData> list) {
			this.list = list;
		}

		public String getStr() {
			return this.str;
		}

		public void setStr(String str) {
			this.str = str;
		}

		public int getCriteria() {
			return this.criteria;
		}

		public void setCriteria(int criteria) {
			this.criteria = criteria;
		}

		public BigDecimal getNum() {
			return this.num;
		}

		public void setNum(BigDecimal num) {
			this.num = num;
		}
	}

	protected enum TestEnum {
		_EMPTY_("", ""), _IMPORT_("IMPORT", "IMP");

		private String name;
		private String code;

		private TestEnum(String name, String code) {
			this.name = name;
			this.code = code;
		}
	}

	protected class DummyListData {
		String name1;
		String name2;
		BigDecimal num;
		TestEnum enum1;

		public DummyListData(String name1, String name2, BigDecimal num, TestEnum enum1) {
			super();
			this.name1 = name1;
			this.name2 = name2;
			this.num = num;
			this.enum1 = enum1;
		}

		public String getName1() {
			return this.name1;
		}

		public void setName1(String name1) {
			this.name1 = name1;
		}

		public String getName2() {
			return this.name2;
		}

		public void setName2(String name2) {
			this.name2 = name2;
		}

		public BigDecimal getNum() {
			return this.num;
		}

		public void setNum(BigDecimal num) {
			this.num = num;
		}

		public TestEnum getEnum1() {
			return this.enum1;
		}

		public void setEnum1(TestEnum enum1) {
			this.enum1 = enum1;
		}

	}
}
