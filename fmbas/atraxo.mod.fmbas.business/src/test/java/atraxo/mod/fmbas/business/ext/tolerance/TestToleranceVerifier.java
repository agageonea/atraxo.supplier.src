package atraxo.mod.fmbas.business.ext.tolerance;

import java.math.BigDecimal;
import java.util.Date;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.business.ext.tolerance.ToleranceResult;
import atraxo.fmbas.business.ext.tolerance.ToleranceVerifier;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.fmbas_type.TolLimits;
import atraxo.fmbas.domain.impl.fmbas_type.ToleranceType;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;

public class TestToleranceVerifier {

	private static final Density DENSITY = new Density(new Unit(), new Unit(), 1);

	@Autowired
	private UnitConverterService unitConverterService;
	@Autowired
	private ExchangeRate_Bd exchangeRateBd;

	private ToleranceVerifier verifier;

	@Before
	public void before() {
		this.unitConverterService = EasyMock.createMock(UnitConverterService.class);
		this.exchangeRateBd = EasyMock.createMock(ExchangeRate_Bd.class);

		this.verifier = new ToleranceVerifier(this.unitConverterService, this.exchangeRateBd, DENSITY);
	}

	@Test
	public void testCheckToleranceQuantity() throws BusinessException {
		Unit fromUnit = new Unit();
		Unit toUnit = new Unit();
		BigDecimal oldValue = new BigDecimal(1);
		BigDecimal refValue = new BigDecimal(2);
		Tolerance tolerance = new Tolerance();
		tolerance.setAbsDevDown(new BigDecimal(5));
		tolerance.setAbsDevUp(new BigDecimal(5));
		tolerance.setToleranceType(ToleranceType._QUANTITY_);
		tolerance.setRefUnit(toUnit);
		tolerance.setTolLmtInd(TolLimits._NARROWER_);
		Date date = new Date(System.currentTimeMillis());

		EasyMock.expect(this.unitConverterService.convert(fromUnit, toUnit, refValue, DENSITY)).andReturn(new BigDecimal(6)).once();
		EasyMock.replay(this.unitConverterService);
		ToleranceResult result = this.verifier.checkTolerance(oldValue, refValue, fromUnit, toUnit, null, null, tolerance, date);
		EasyMock.reset(this.unitConverterService);

		Assert.assertEquals(ToleranceResult.ResultType.MARGIN, result.getType());

		refValue = new BigDecimal(3);
		EasyMock.expect(this.unitConverterService.convert(fromUnit, toUnit, refValue, DENSITY)).andReturn(new BigDecimal(5)).once();
		EasyMock.replay(this.unitConverterService);
		result = this.verifier.checkTolerance(oldValue, refValue, fromUnit, toUnit, null, null, tolerance, date);
		Assert.assertEquals(ToleranceResult.ResultType.WITHIN, result.getType());
		EasyMock.reset(this.unitConverterService);

		refValue = new BigDecimal(6);
		EasyMock.expect(this.unitConverterService.convert(fromUnit, toUnit, refValue, DENSITY)).andReturn(new BigDecimal(7)).once();
		EasyMock.replay(this.unitConverterService);
		result = this.verifier.checkTolerance(oldValue, refValue, fromUnit, toUnit, null, null, tolerance, date);
		Assert.assertEquals(ToleranceResult.ResultType.NOT_WITHIN, result.getType());
		EasyMock.verify(this.unitConverterService);

	}

	@Test
	public void testCheckToleranceCosts() throws BusinessException {
		Currencies fromCurrency = new Currencies();
		Currencies toCurrency = new Currencies();
		BigDecimal refValue = new BigDecimal(1);
		BigDecimal newValue = new BigDecimal(50);
		Tolerance tolerance = new Tolerance();
		tolerance.setAbsDevDown(new BigDecimal(5));
		tolerance.setAbsDevUp(new BigDecimal(5));
		tolerance.setToleranceType(ToleranceType._COST_);
		tolerance.setCurrency(toCurrency);
		tolerance.setTolLmtInd(TolLimits._NARROWER_);
		Date date = new Date(System.currentTimeMillis());
		ConversionResult convResult = new ConversionResult();
		convResult.setValue(new BigDecimal(6));
		EasyMock.expect(this.exchangeRateBd.convert(fromCurrency, toCurrency, date, newValue, false)).andReturn(convResult).once();
		EasyMock.replay(this.exchangeRateBd);

		ToleranceResult result = this.verifier.checkTolerance(refValue, newValue, null, null, fromCurrency, toCurrency, tolerance, date);

		Assert.assertEquals(ToleranceResult.ResultType.MARGIN, result.getType());
		EasyMock.verify(this.exchangeRateBd);

		EasyMock.reset(this.exchangeRateBd);
		newValue = new BigDecimal(3);
		convResult = new ConversionResult();
		convResult.setValue(new BigDecimal(5));
		EasyMock.expect(this.exchangeRateBd.convert(fromCurrency, toCurrency, date, newValue, false)).andReturn(convResult).once();
		EasyMock.replay(this.exchangeRateBd);
		result = this.verifier.checkTolerance(refValue, newValue, null, null, fromCurrency, toCurrency, tolerance, date);
		Assert.assertEquals(ToleranceResult.ResultType.WITHIN, result.getType());
		EasyMock.verify(this.exchangeRateBd);

		EasyMock.reset(this.exchangeRateBd);
		newValue = new BigDecimal(6);
		convResult = new ConversionResult();
		convResult.setValue(new BigDecimal(7));
		EasyMock.expect(this.exchangeRateBd.convert(fromCurrency, toCurrency, date, newValue, false)).andReturn(convResult).once();
		EasyMock.replay(this.exchangeRateBd);
		result = this.verifier.checkTolerance(refValue, newValue, null, null, fromCurrency, toCurrency, tolerance, date);
		Assert.assertEquals(ToleranceResult.ResultType.NOT_WITHIN, result.getType());

		EasyMock.verify(this.exchangeRateBd);
	}

	@Test
	public void testCheckTolerancePrice() throws BusinessException {
		Unit fromUnit = new Unit();
		Unit toUnit = new Unit();

		Currencies fromCurrency = new Currencies();
		Currencies toCurrency = new Currencies();
		BigDecimal oldValue = new BigDecimal(1);
		BigDecimal refValue = new BigDecimal(50);
		Tolerance tolerance = new Tolerance();
		tolerance.setAbsDevDown(new BigDecimal(5));
		tolerance.setAbsDevUp(new BigDecimal(5));
		tolerance.setToleranceType(ToleranceType._PRICE_);
		tolerance.setCurrency(toCurrency);
		tolerance.setRefUnit(toUnit);
		tolerance.setTolLmtInd(TolLimits._NARROWER_);
		Date date = new Date(System.currentTimeMillis());
		ConversionResult convResult = new ConversionResult();
		convResult.setValue(new BigDecimal(6));

		EasyMock.expect(this.exchangeRateBd.convert(fromCurrency, toCurrency, date, refValue, false)).andReturn(convResult).once();
		EasyMock.expect(this.unitConverterService.convert(fromUnit, toUnit, new BigDecimal(6), DENSITY)).andReturn(new BigDecimal(6)).once();

		EasyMock.replay(this.unitConverterService, this.exchangeRateBd);

		ToleranceResult result = this.verifier.checkTolerance(oldValue, refValue, fromUnit, toUnit, fromCurrency, toCurrency, tolerance, date);

		Assert.assertEquals(ToleranceResult.ResultType.MARGIN, result.getType());
		EasyMock.reset(this.unitConverterService, this.exchangeRateBd);

		refValue = new BigDecimal(5);
		convResult = new ConversionResult();
		convResult.setValue(new BigDecimal(5));
		EasyMock.expect(this.exchangeRateBd.convert(fromCurrency, toCurrency, date, refValue, false)).andReturn(convResult).once();
		EasyMock.expect(this.unitConverterService.convert(fromUnit, toUnit, new BigDecimal(5), DENSITY)).andReturn(new BigDecimal(5)).once();
		EasyMock.replay(this.unitConverterService, this.exchangeRateBd);

		result = this.verifier.checkTolerance(oldValue, refValue, fromUnit, toUnit, fromCurrency, toCurrency, tolerance, date);

		Assert.assertEquals(ToleranceResult.ResultType.WITHIN, result.getType());

		EasyMock.reset(this.exchangeRateBd, this.unitConverterService);

		refValue = new BigDecimal(6);
		convResult = new ConversionResult();
		convResult.setValue(new BigDecimal(10));

		EasyMock.expect(this.exchangeRateBd.convert(fromCurrency, toCurrency, date, refValue, false)).andReturn(convResult).once();
		EasyMock.expect(this.unitConverterService.convert(fromUnit, toUnit, new BigDecimal(10), DENSITY)).andReturn(new BigDecimal(10)).once();
		EasyMock.replay(this.unitConverterService, this.exchangeRateBd);

		result = this.verifier.checkTolerance(oldValue, refValue, fromUnit, toUnit, fromCurrency, toCurrency, tolerance, date);

		Assert.assertEquals(ToleranceResult.ResultType.NOT_WITHIN, result.getType());
		EasyMock.verify(this.unitConverterService, this.exchangeRateBd);
	}
}
