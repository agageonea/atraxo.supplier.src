package atraxo.mod.fmbas.business.ext.externalInterfaces.service;

import static org.junit.Assert.assertFalse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistory_Service;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;

public class TestExternalInterfaceMessageHistoryService {

	@Test
	public void testSortByDate() throws ParseException {

		List<ExternalInterfaceMessageHistory> list = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
		ExternalInterfaceMessageHistory e1 = new ExternalInterfaceMessageHistory();
		e1.setExecutionTime(sdf.parse("2016.08.15"));
		list.add(e1);
		ExternalInterfaceMessageHistory e2 = new ExternalInterfaceMessageHistory();
		e2.setExecutionTime(sdf.parse("2016.08.16"));
		list.add(e2);
		list = ExternalInterfaceMessageHistory_Service.sortByDate(list);
		assertFalse(list.get(0).getExecutionTime().compareTo(e1.getExecutionTime()) == 0);
	}

}
