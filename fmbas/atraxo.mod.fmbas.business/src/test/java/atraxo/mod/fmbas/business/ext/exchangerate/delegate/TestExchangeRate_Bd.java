package atraxo.mod.fmbas.business.ext.exchangerate.delegate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import atraxo.fmbas.business.api.exchangerate.IExchangeRateService;
import atraxo.fmbas.business.api.exchangerate.IExchangeRateValueService;
import atraxo.fmbas.business.api.financialSources.IFinancialSourcesService;
import atraxo.fmbas.business.api.timeserie.IAverageMethodService;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.PostTypeInd;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import seava.j4e.api.exceptions.BusinessException;

// @RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:META-INF/spring/jee/fmbas-business-test-context.xml" })
public class TestExchangeRate_Bd extends AbstractTransactionalJUnit4SpringContextTests {

	private static final Logger LOGGER = LoggerFactory.getLogger(TestExchangeRate_Bd.class);

	@Autowired
	private ExchangeRate_Bd exchangeRateBd;
	@Autowired
	private IAverageMethodService avgMethodService;
	@Autowired
	private IFinancialSourcesService finSourceService;
	@Autowired
	private IExchangeRateService exchRateService;
	@Autowired
	private IExchangeRateValueService exchRateValueService;

	@Test
	public void testConvert() {
		Assert.assertNotNull(this.exchangeRateBd);
		Assert.assertNotNull(this.avgMethodService);
		Assert.assertNotNull(this.finSourceService);
		Assert.assertNotNull(this.exchRateService);
		Assert.assertNotNull(this.exchRateValueService);

		Currencies fromCurrency = new Currencies();
		fromCurrency.setCode("EUR");
		Currencies toCurrency = new Currencies();
		toCurrency.setCode("USD");
		Date date = GregorianCalendar.getInstance().getTime();
		BigDecimal value = new BigDecimal(3);
		boolean strict = false;

		try {
			AverageMethod defaultAvgMethod = new AverageMethod();
			FinancialSources stdFinancialSource = new FinancialSources();
			ExchangeRate exchRate = new ExchangeRate();
			exchRate.setCurrency1(fromCurrency);
			exchRate.setCurrency2(toCurrency);
			ExchangeRateValue exchRateValue = new ExchangeRateValue();
			exchRateValue.setPostTypeInd(PostTypeInd._AMOUNT_.ordinal());
			exchRateValue.setRate(new BigDecimal(2));

			EasyMock.expect(this.avgMethodService.getDefaultMethod()).andReturn(defaultAvgMethod).once();
			EasyMock.expect(this.finSourceService.getStandardFinancialSource()).andReturn(stdFinancialSource).once();
			EasyMock.expect(this.exchRateService.getExchangeRate(fromCurrency, toCurrency, stdFinancialSource, defaultAvgMethod)).andReturn(exchRate)
					.once();

			EasyMock.expect(this.exchRateValueService.getExchangeRateValue(exchRate, date, strict)).andReturn(exchRateValue).once();
			EasyMock.replay(this.avgMethodService, this.finSourceService, this.exchRateService, this.exchRateValueService);
			BigDecimal convertedValue = this.exchangeRateBd.convert(fromCurrency, toCurrency, date, value, strict).getValue();
			Assert.assertEquals("1.5", convertedValue.toString());
			EasyMock.verify(this.avgMethodService, this.finSourceService, this.exchRateService, this.exchRateValueService);
		} catch (BusinessException e) {
			e.printStackTrace();
			LOGGER.error("Business exception:", e);
		}
	}
}
