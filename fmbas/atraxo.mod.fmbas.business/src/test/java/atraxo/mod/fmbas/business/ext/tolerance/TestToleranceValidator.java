package atraxo.mod.fmbas.business.ext.tolerance;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.tolerance.ToleranceException;
import atraxo.fmbas.business.ext.exceptions.tolerance.ToleranceLimitsException;
import atraxo.fmbas.business.ext.tolerance.ToleranceValidator;
import atraxo.fmbas.domain.impl.fmbas_type.ToleranceType;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;

public class TestToleranceValidator {

	private static final Logger LOGGER = LoggerFactory.getLogger(TestToleranceValidator.class);

	private ToleranceValidator validator;

	@Before
	public void setUp() {
		this.validator = new ToleranceValidator();
	}

	@Test(expected = ToleranceException.class)
	public void testNullValues() throws ToleranceException {
		Assert.assertNotNull(this.validator);
		this.validator.validateValues(null, null);

	}

	@Test
	public void testNullOldValue() {
		Assert.assertNotNull(this.validator);
		try {
			this.validator.validateValues(null, BigDecimal.ONE);
		} catch (ToleranceException e) {
			LOGGER.error("Tolerance exception:", e);
			Assert.assertEquals(BusinessErrorCode.INVALID_TOLERANCE_VALUE, e.getErrorCode());
		}
	}

	@Test
	public void testNullReferenceValue() {
		Assert.assertNotNull(this.validator);
		try {
			this.validator.validateValues(BigDecimal.ONE, null);
		} catch (ToleranceException e) {
			LOGGER.error("Invalid tolerance exception:", e);
			Assert.assertEquals(BusinessErrorCode.INVALID_TOLERANCE_VALUE, e.getErrorCode());
		}
	}

	@Test(expected = ToleranceLimitsException.class)
	public void testToleranceDownValues() throws ToleranceException {
		Tolerance tolerance = new Tolerance();
		tolerance.setAbsDevUp(BigDecimal.ONE);
		this.validator.validateTolerance(tolerance);
	}

	@Test(expected = ToleranceLimitsException.class)
	public void testToleranceUpValues() throws ToleranceException {
		Tolerance tolerance = new Tolerance();
		tolerance.setAbsDevDown(BigDecimal.ONE);

		this.validator.validateTolerance(tolerance);
	}

	@Test(expected = ToleranceException.class)
	public void testToleranceTypeCost() throws ToleranceException {
		Tolerance tolerance = new Tolerance();
		tolerance.setToleranceType(ToleranceType._COST_);
		this.validator.validateTolerance(tolerance);

	}

	@Test(expected = ToleranceException.class)
	public void testToleranceTypeQuantity() throws ToleranceException {
		Tolerance tolerance = new Tolerance();
		tolerance.setToleranceType(ToleranceType._QUANTITY_);
		this.validator.validateTolerance(tolerance);

	}

	@Test(expected = ToleranceException.class)
	public void testToleranceTypePrice() throws ToleranceException {
		Tolerance tolerance = new Tolerance();
		tolerance.setToleranceType(ToleranceType._PRICE_);
		this.validator.validateTolerance(tolerance);

	}
}
