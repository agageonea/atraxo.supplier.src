package atraxo.mod.fmbas.business.ext.uom.delegate;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:/META-INF/spring/jee/fmbas-business-test-context.xml" })

public class TestUnit_Bd {

	private static final Logger LOGGER = LoggerFactory.getLogger(TestUnit_Bd.class);

	@Autowired
	private UnitConverterService unitConverterService;

	@Autowired
	private IUnitService unitService;

	@After
	public void tearDown() {
		EasyMock.reset(this.unitService);

	}

	@Test
	public void testConvertMassToVol() {

		Unit fromMassUnit = this.buildUnit(UnitType._MASS_, new BigDecimal("1000"));
		Unit toVolUnit = this.buildUnit(UnitType._VOLUME_, new BigDecimal("1000"));
		Unit massBaseUnit = this.buildUnit(UnitType._MASS_, BigDecimal.ONE);
		Unit volBaseUnit = this.buildUnit(UnitType._VOLUME_, BigDecimal.ONE);

		BigDecimal value = new BigDecimal(3);
		double density = 0.8;

		try {
			EasyMock.expect(this.unitService.getBaseUnit(EasyMock.anyObject(UnitType.class), EasyMock.anyObject(BigDecimal.class)))
					.andReturn(massBaseUnit).once();
			EasyMock.expect(this.unitService.getBaseUnit(EasyMock.anyObject(UnitType.class), EasyMock.anyObject(BigDecimal.class)))
					.andReturn(volBaseUnit).once();
			EasyMock.replay(this.unitService);

			BigDecimal convertedValue = this.unitConverterService.convert(fromMassUnit, toVolUnit, value, density);

			Assert.assertEquals("3.750000", convertedValue.setScale(6, RoundingMode.HALF_UP).toString());

			EasyMock.verify(this.unitService);
			EasyMock.reset(this.unitService);
		} catch (BusinessException e) {
			LOGGER.error("Business Exception:", e);
			e.printStackTrace();
		}
	}

	@Test
	public void testConvertMassToVolDifBaseUnit() {
		Unit fromMassUnit = this.buildUnit(UnitType._MASS_, new BigDecimal("1000"));
		Unit toVolUnit = this.buildUnit(UnitType._VOLUME_, new BigDecimal("100"));
		Unit dFromUnit = this.buildUnit(UnitType._MASS_, new BigDecimal("2"));
		Unit dToUnit = this.buildUnit(UnitType._VOLUME_, new BigDecimal("10"));

		BigDecimal value = new BigDecimal(3);
		double density = 0.8;
		try {
			BigDecimal convertedValue = this.unitConverterService.convert(fromMassUnit, toVolUnit, value, new Density(dFromUnit, dToUnit, density));

			Assert.assertEquals("187.500000", convertedValue.setScale(6, RoundingMode.HALF_UP).toString());
		} catch (BusinessException e) {
			LOGGER.error("Business Exception:", e);
			e.printStackTrace();
		}
	}

	@Test
	public void testConvertVolToMass() {

		Unit toMassUnit = this.buildUnit(UnitType._MASS_, new BigDecimal("1000"));
		Unit fromVolUnit = this.buildUnit(UnitType._VOLUME_, new BigDecimal("100"));
		Unit massBaseUnit = this.buildUnit(UnitType._MASS_, BigDecimal.ONE);
		Unit volBaseUnit = this.buildUnit(UnitType._VOLUME_, BigDecimal.ONE);

		BigDecimal value = new BigDecimal(3);
		double density = 0.8;

		try {
			EasyMock.expect(this.unitService.getBaseUnit(EasyMock.anyObject(UnitType.class), EasyMock.anyObject(BigDecimal.class)))
					.andReturn(massBaseUnit).once();
			EasyMock.expect(this.unitService.getBaseUnit(EasyMock.anyObject(UnitType.class), EasyMock.anyObject(BigDecimal.class)))
					.andReturn(volBaseUnit).once();
			EasyMock.replay(this.unitService);

			BigDecimal convertedValue = this.unitConverterService.convert(fromVolUnit, toMassUnit, value, density);

			Assert.assertEquals("0.240000", convertedValue.setScale(6, RoundingMode.HALF_UP).toString());

			EasyMock.verify(this.unitService);
			EasyMock.reset(this.unitService);
		} catch (BusinessException e) {
			LOGGER.error("Business Exception:", e);
			e.printStackTrace();
		}
	}

	@Test
	public void testConvertVolToMassDifBaseUnit() {

		Unit toMassUnit = this.buildUnit(UnitType._MASS_, new BigDecimal("1000"));
		Unit fromVolUnit = this.buildUnit(UnitType._VOLUME_, new BigDecimal("100"));
		Unit dFromUnit = this.buildUnit(UnitType._MASS_, new BigDecimal("100"));
		Unit dToUnit = this.buildUnit(UnitType._VOLUME_, new BigDecimal("2"));

		BigDecimal value = new BigDecimal(3);
		double density = 0.8;

		try {
			BigDecimal convertedValue = this.unitConverterService.convert(fromVolUnit, toMassUnit, value, new Density(dFromUnit, dToUnit, density));

			Assert.assertEquals("12.000000", convertedValue.setScale(6, RoundingMode.HALF_UP).toString());

		} catch (BusinessException e) {
			LOGGER.error("Business Exception:", e);
			e.printStackTrace();
		}
	}

	@Test
	public void testConvertWithFactorFromVolToMass() {
		Unit toMassUnit = this.buildUnit(UnitType._MASS_, new BigDecimal("1000"));
		Unit fromVolUnit = this.buildUnit(UnitType._VOLUME_, new BigDecimal("100"));
		Unit fFromUnit = this.buildUnit(UnitType._MASS_, new BigDecimal("100"));
		Unit fToUnit = this.buildUnit(UnitType._VOLUME_, new BigDecimal("2"));

		BigDecimal value = new BigDecimal(3);
		BigDecimal factor = new BigDecimal(3);
		try {
			BigDecimal convertedValue = this.unitConverterService.convert(fromVolUnit, toMassUnit, value, factor, fFromUnit, fToUnit);
			Assert.assertEquals("5.000000", convertedValue.setScale(6, RoundingMode.HALF_UP).toString());
		} catch (BusinessException e) {
			LOGGER.error("Business Exception:", e);
			e.printStackTrace();
		}
	}

	@Test
	public void testConvertWithFactorFromMassToVol() {
		Unit fromMassUnit = this.buildUnit(UnitType._MASS_, new BigDecimal("1000"));
		Unit toVolUnit = this.buildUnit(UnitType._VOLUME_, new BigDecimal("100"));
		Unit fFromUnit = this.buildUnit(UnitType._MASS_, new BigDecimal("100"));
		Unit fToUnit = this.buildUnit(UnitType._VOLUME_, new BigDecimal("2"));

		BigDecimal value = new BigDecimal(3);
		BigDecimal factor = new BigDecimal(3);
		try {
			BigDecimal convertedValue = this.unitConverterService.convert(fromMassUnit, toVolUnit, value, factor, fFromUnit, fToUnit);
			Assert.assertEquals("1.800000", convertedValue.setScale(6, RoundingMode.HALF_UP).toString());
		} catch (BusinessException e) {
			LOGGER.error("Business Exception:", e);
			e.printStackTrace();
		}
	}

	@Test
	public void testConversionMassToMass() {

		Unit fromUnit = this.buildUnit(UnitType._MASS_, new BigDecimal("1000"));
		Unit toUnit = this.buildUnit(UnitType._MASS_, new BigDecimal("1"));
		Unit massBaseUnit = this.buildUnit(UnitType._MASS_, BigDecimal.ONE);
		Unit volBaseUnit = this.buildUnit(UnitType._VOLUME_, BigDecimal.ONE);
		BigDecimal value = new BigDecimal(3);
		double density = 1.5;

		try {
			EasyMock.expect(this.unitService.getBaseUnit(EasyMock.anyObject(UnitType.class), EasyMock.anyObject(BigDecimal.class)))
					.andReturn(massBaseUnit).once();
			EasyMock.expect(this.unitService.getBaseUnit(EasyMock.anyObject(UnitType.class), EasyMock.anyObject(BigDecimal.class)))
					.andReturn(volBaseUnit).once();
			EasyMock.replay(this.unitService);

			BigDecimal convertedValue = this.unitConverterService.convert(fromUnit, toUnit, value, density);

			Assert.assertEquals("3000", convertedValue.toString());
			EasyMock.verify(this.unitService);
		} catch (BusinessException e) {
			LOGGER.error("Business Exception:", e);
			e.printStackTrace();
		}
	}

	@Test
	public void testConversionVolToVol() {

		Unit fromUnit = this.buildUnit(UnitType._VOLUME_, new BigDecimal("1000"));
		Unit toUnit = this.buildUnit(UnitType._VOLUME_, new BigDecimal("1"));
		Unit massBaseUnit = this.buildUnit(UnitType._MASS_, BigDecimal.ONE);
		Unit volBaseUnit = this.buildUnit(UnitType._VOLUME_, BigDecimal.ONE);
		BigDecimal value = new BigDecimal(3);
		double density = 1.5;

		try {
			EasyMock.expect(this.unitService.getBaseUnit(EasyMock.anyObject(UnitType.class), EasyMock.anyObject(BigDecimal.class)))
					.andReturn(massBaseUnit).once();
			EasyMock.expect(this.unitService.getBaseUnit(EasyMock.anyObject(UnitType.class), EasyMock.anyObject(BigDecimal.class)))
					.andReturn(volBaseUnit).once();
			EasyMock.replay(this.unitService);

			BigDecimal convertedValue = this.unitConverterService.convert(fromUnit, toUnit, value, density);

			Assert.assertEquals("3000", convertedValue.toString());
			EasyMock.verify(this.unitService);

		} catch (BusinessException e) {
			LOGGER.error("Business Exception:", e);
			e.printStackTrace();
		}
	}

	@Test
	public void testConversionTemp() throws BusinessException {
		Unit fromUnit = this.buildUnit(UnitType._TEMPERATURE_, new BigDecimal("1000"));
		Unit toUnit = this.buildUnit(UnitType._MASS_, new BigDecimal("3785.41"));
		Unit massBaseUnit = this.buildUnit(UnitType._MASS_, BigDecimal.ONE);
		Unit volBaseUnit = this.buildUnit(UnitType._VOLUME_, BigDecimal.ONE);
		BigDecimal value = new BigDecimal(3);
		double density = 1.5;
		EasyMock.expect(this.unitService.getBaseUnit(EasyMock.anyObject(UnitType.class), EasyMock.anyObject(BigDecimal.class)))
				.andReturn(massBaseUnit).once();
		EasyMock.expect(this.unitService.getBaseUnit(EasyMock.anyObject(UnitType.class), EasyMock.anyObject(BigDecimal.class))).andReturn(volBaseUnit)
				.once();
		EasyMock.replay(this.unitService);
		BigDecimal convertedValue = this.unitConverterService.convert(fromUnit, toUnit, value, density);
		Assert.assertEquals(value, convertedValue);
		EasyMock.verify(this.unitService);

	}

	private Unit buildUnit(UnitType typeInd, BigDecimal factor) {
		Unit unit = new Unit();
		unit.setId((int) Math.round(Math.random() * 1000));
		unit.setUnittypeInd(typeInd);
		unit.setFactor(factor);
		return unit;
	}
}
