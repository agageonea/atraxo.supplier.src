/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.trade;

import atraxo.fmbas.business.api.trade.ITradeReferenceService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.trade.TradeReference;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link TradeReference} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class TradeReference_Service
		extends
			AbstractEntityService<TradeReference>
		implements
			ITradeReferenceService {

	/**
	 * Public constructor for TradeReference_Service
	 */
	public TradeReference_Service() {
		super();
	}

	/**
	 * Public constructor for TradeReference_Service
	 * 
	 * @param em
	 */
	public TradeReference_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<TradeReference> getEntityClass() {
		return TradeReference.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return TradeReference
	 */
	public TradeReference findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(TradeReference.NQ_FIND_BY_BUSINESS,
							TradeReference.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TradeReference", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TradeReference", "id"), nure);
		}
	}

	/**
	 * Find by reference: company
	 *
	 * @param company
	 * @return List<TradeReference>
	 */
	public List<TradeReference> findByCompany(Customer company) {
		return this.findByCompanyId(company.getId());
	}
	/**
	 * Find by ID of reference: company.id
	 *
	 * @param companyId
	 * @return List<TradeReference>
	 */
	public List<TradeReference> findByCompanyId(Integer companyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TradeReference e where e.clientId = :clientId and e.company.id = :companyId",
						TradeReference.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("companyId", companyId).getResultList();
	}
}
