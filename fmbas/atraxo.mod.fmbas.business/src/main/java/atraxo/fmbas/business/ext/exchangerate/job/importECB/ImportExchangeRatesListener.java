package atraxo.fmbas.business.ext.exchangerate.job.importECB;

import java.util.List;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;

import seava.j4e.scheduler.batch.listener.DefaultActionListener;

/**
 * @author zspeter
 */
public class ImportExchangeRatesListener extends DefaultActionListener {

	@Override
	public void afterJob(JobExecution jobExecution) {
		super.afterJob(jobExecution);
		ExitStatus status = jobExecution.getExitStatus();
		StringBuilder sb = new StringBuilder(status.getExitDescription());
		List<StepExecution> stepExecution = (List<StepExecution>) jobExecution.getStepExecutions();

		for (StepExecution se : stepExecution) {
			ExecutionContext executionContext = se.getExecutionContext();
			if (executionContext.containsKey(ImportExchangeRatesItemProcessor.LOG)) {
				sb.append(executionContext.getString(ImportExchangeRatesItemProcessor.LOG)).append(ImportExchangeRatesItemProcessor.CRLF);
			}
		}
		status = new ExitStatus(status.getExitCode(), sb.toString());
		jobExecution.setExitStatus(status);
	}
}
