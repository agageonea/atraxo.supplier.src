package atraxo.fmbas.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;

/**
 *
 * Exception used when exchange rate is zero.
 *
 * @author zspeter
 *
 */
public class ExchangeFactorException extends BusinessException {

	private static final long serialVersionUID = -8298117689547629990L;

	public ExchangeFactorException() {
		super(BusinessErrorCode.EXCHANGE_FACTOR, BusinessErrorCode.EXCHANGE_FACTOR.getErrMsg());
	}

	public ExchangeFactorException(Throwable e) {
		super(BusinessErrorCode.EXCHANGE_FACTOR, BusinessErrorCode.EXCHANGE_FACTOR.getErrMsg(), e);

	}

	public ExchangeFactorException(Throwable e, String msg) {
		super(BusinessErrorCode.EXCHANGE_FACTOR, msg, e);

	}
}
