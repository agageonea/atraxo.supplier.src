/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.externalInterfaces.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.ad.domain.impl.ad.Category;
import atraxo.ad.domain.impl.ad.NotificationAction;
import atraxo.ad.domain.impl.ad.Priority;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceHistoryService;
import atraxo.fmbas.business.api.notification.INotificationService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.bpm.delegate.exchangeRate.approval.ExchangeRateTimeSeriesApprovalDelegate;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceHistory;
import atraxo.fmbas.domain.impl.externalInterfaces.InterfaceUser;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfacesHistoryStatus;
import atraxo.fmbas.domain.impl.fmbas_type.LastRequestStatus;
import atraxo.fmbas.domain.impl.notification.Notification;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link ExternalInterfaceHistory} domain entity.
 */
public class ExternalInterfaceHistory_Service extends atraxo.fmbas.business.impl.externalInterfaces.ExternalInterfaceHistory_Service
		implements IExternalInterfaceHistoryService {

	private static final Logger LOG = LoggerFactory.getLogger(ExchangeRateTimeSeriesApprovalDelegate.class);

	@Autowired
	private INotificationService srv;
	@Autowired
	private IUserSuppService userSrv;

	@Override
	protected void postInsert(ExternalInterfaceHistory e) throws BusinessException {
		if (!e.getExternalInterface().getName().equals(("Fuel Tender Invitation"))) {
			this.sendNotification(e.getExternalInterface(), null);
		}
		super.postInsert(e);
	}

	@Override
	@Transactional
	public void empty(Integer externalInterfaceId) throws BusinessException {
		List<ExternalInterfaceHistory> eihList = this.findByExternalInterfaceId(externalInterfaceId);
		List<Object> ids = new ArrayList<>();
		for (ExternalInterfaceHistory eih : eihList) {
			ids.add(eih.getId());
		}
		this.deleteByIds(ids);
	}

	@Override
	public ExternalInterfaceHistory findByMsgId(String msgId) throws BusinessException {
		Map<String, Object> identificationParams = new HashMap<>();
		identificationParams.put("msgId", msgId);
		return this.findEntityByAttributes(ExternalInterfaceHistory.class, identificationParams);
	}

	@Override
	@Transactional
	public void updateHistoryOutboundFromACK(Boolean success, String messageId, boolean canHaveNegativeACK) throws BusinessException {
		ExternalInterfaceHistory history = this.findByMsgId(messageId);
		ExternalInterfacesHistoryStatus externalInterfacesHistoryStatus = canHaveNegativeACK
				? ExternalInterfacesHistoryStatus._FAILED_NEGATIVE_ACKNOWLEDGEMENT_
				: ExternalInterfacesHistoryStatus._FAILURE_;
		history.setStatus(success ? ExternalInterfacesHistoryStatus._SUCCESS_ : externalInterfacesHistoryStatus);
		this.update(history);

	}

	@Override
	public ExternalInterfaceHistory findLastEntry(Integer externalInterfaceId) throws BusinessException {
		try {
			return this.getEntityManager().createQuery(
					"select e from ExternalInterfaceHistory e where e.clientId = :clientId and e.externalInterface.id = :externalInterface ORDER BY e.requestReceived DESC",
					ExternalInterfaceHistory.class).setParameter("clientId", Session.user.get().getClient().getId())
					.setParameter("externalInterface", externalInterfaceId).setMaxResults(1).getSingleResult();
		} catch (Exception e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
			return null;
		}
	}

	@Override
	public void sendNotification(ExternalInterface inter, Map objectMap) throws BusinessException {
		LastRequestStatus status = this.getLastRequestStatus(inter);
		Notification e = this.buildNotification(inter, status);
		if (CollectionUtils.isEmpty(inter.getUsers())) {
			return;
		}
		List<UserSupp> emailUsers = new ArrayList<>();
		for (InterfaceUser iuser : inter.getUsers()) {
			if (!iuser.getEnabled()) {
				continue;
			}
			UserSupp u = this.userSrv.findById(iuser.getUser().getId());
			if (iuser.getEmailSubject() != null && !iuser.getEmailSubject().isEmpty()) {
				u.setEmailSubject(iuser.getEmailSubject());
			}
			if (iuser.getEmailTemplate() != null) {
				u.setEmailTemplateRefId(iuser.getEmailTemplate().getRefid());
			}
			switch (iuser.getNotificationCondition()) {
			case _ALL_EVENTS_:
				this.addUsersToNotification(e, u, iuser, emailUsers);
				break;
			case _ON_SUCCESS_:
				if (status.equals(LastRequestStatus._SUCCESS_)) {
					this.addUsersToNotification(e, u, iuser, emailUsers);
				}
				break;
			case _ON_FAILURE_:
				if (status.equals(LastRequestStatus._FAILURE_)) {
					this.addUsersToNotification(e, u, iuser, emailUsers);
				}
				break;

			default:
				break;
			}
		}
		this.srv.sendAndSave(e, emailUsers, objectMap);
	}

	/**
	 * Get's last request status from an external interface based on the latest External Interface History
	 *
	 * @param inter
	 * @return
	 * @throws BusinessException
	 */
	private LastRequestStatus getLastRequestStatus(ExternalInterface inter) throws BusinessException {
		ExternalInterfaceHistory e = this.findLastEntry(inter.getId());
		if (e != null) {
			if (ExternalInterfacesHistoryStatus._SUCCESS_.equals(e.getStatus())) {
				return LastRequestStatus._SUCCESS_;
			} else {
				return LastRequestStatus._FAILURE_;
			}
		}
		return LastRequestStatus._EMPTY_;
	}

	private void addUsersToNotification(Notification e, UserSupp u, InterfaceUser iuser, List<UserSupp> emailUsers) {
		if (u.getNotifications() == null) {
			u.setNotifications(new ArrayList<Notification>());
		}
		switch (iuser.getNotificationType()) {
		case _BOTH_:
			u.getNotifications().add(e);
			e.getUsers().add(u);
			emailUsers.add(u);
			break;
		case _EMAIL_:
			emailUsers.add(u);
			break;
		case _EVENT_:
			u.getNotifications().add(e);
			e.getUsers().add(u);
			break;
		default:
			break;
		}
	}

	private Notification buildNotification(ExternalInterface extInterface, LastRequestStatus status) throws BusinessException {
		Notification e = new Notification();
		e.setEventDate(extInterface.getLastRequest());
		e.setLifeTime(extInterface.getLastRequest());
		e.setTitle(extInterface.getName());
		e.setAction(NotificationAction._NOACTION_);
		e.setLink("link");
		e.setEventDate(GregorianCalendar.getInstance().getTime());
		e.setLifeTime(GregorianCalendar.getInstance().getTime());
		if (LastRequestStatus._SUCCESS_.equals(status)) {
			e.setCategory(Category._JOB_);
			e.setDescripion(String.format(BusinessErrorCode.INTERFACE_NOTIFICATION_SUCCESS.getErrMsg(), extInterface.getName()));
			e.setPriority(Priority._DEFAULT_);
		} else {
			e.setCategory(Category._ERROR_);
			e.setDescripion(String.format(BusinessErrorCode.INTERFACE_NOTIFICATION_FAILURE.getErrMsg(), extInterface.getName()));
			e.setPriority(Priority._MAX_);
		}
		e.setStatus(status.getName());
		e.setResult(status.getName());
		e.setUsers(new ArrayList<UserSupp>());
		return e;
	}

	@Transactional
	@Override
	public void saveInterfaceHistory(StopWatch stopWatch, WSExecutionResult result, ExternalInterface externalInterface) throws BusinessException {
		ExternalInterfaceHistory history = new ExternalInterfaceHistory();
		history.setExternalInterface(externalInterface);
		history.setRequester(Session.user.get().getClientCode());
		history.setRequestReceived(new Date(stopWatch.getStartTime()));
		history.setResponseSent(new Date(stopWatch.getStartTime() + stopWatch.getTime()));
		history.setStatus(result.isSuccess() ? ExternalInterfacesHistoryStatus._SUCCESS_ : ExternalInterfacesHistoryStatus._FAILURE_);
		history.setResult(result.getTrimmedExecutionData());
		this.insert(history);
	}

}
