package atraxo.fmbas.business.ext.ftp.workflow;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.integration.file.remote.session.SessionFactory;

import atraxo.fmbas.business.ext.ftp.AbstractFtpBean;
import atraxo.fmbas.business.ext.ftp.FtpCommunicationResult;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import seava.j4e.api.exceptions.BusinessException;

public class FtpWriteFiles extends AbstractFtpBean implements InitializingBean {

	public FtpWriteFiles(SessionFactory<?> sessionFactory, String remoteDirectory, String fileNamePattern) {
		super(sessionFactory, remoteDirectory, fileNamePattern);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T extends AbstractEntity> FtpCommunicationResult execute(T... list) throws IOException, InterruptedException, BusinessException {
		return null;
	}

	public void execute(Map<String, String> files) throws BusinessException {
		for (Map.Entry<String, String> entry : files.entrySet()) {
			this.writeToFTP(IOUtils.toInputStream(entry.getValue()), entry.getKey());
		}
	}

}
