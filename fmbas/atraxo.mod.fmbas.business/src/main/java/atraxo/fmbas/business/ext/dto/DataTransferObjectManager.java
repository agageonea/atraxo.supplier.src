package atraxo.fmbas.business.ext.dto;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDataDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;
import seava.j4e.api.annotation.DtoField;

/**
 * @author mbotorogea
 */
public class DataTransferObjectManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(DataTransferObjectManager.class);

	private static final String FIELD_METHOD_GETTER_PREFIX = "get";
	private static final String FIELD_METHOD_SETTER_PREFIX = "set";

	/**
	 * Transform a given object to a simple POJO (DTO) thru reflexion. Will populate all the dto plain properties, nested properties and nested
	 * objects. All the fields to be populated must bee annotated with DtoField and specify the path. After the properties are copied it will invoke
	 * the custom method ("addCustomValues") from the service.
	 *
	 * @param object
	 * @param dtoClass
	 * @param dtoService
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <T, D extends AbstractDataDto> D objectToCustomDto(T object, Class dtoClass, AbstractDataDtoService dtoService) throws Exception {

		// Copy properties
		D customDto = this.objectToDto(object, dtoClass);

		// Apply custom values
		dtoService.addCustomValues(object, customDto);

		return customDto;
	}

	/**
	 * Transform a given object to a simple POJO (DTO) thru reflexion. Will populate all the dto plain properties, nested properties and nested
	 * objects. All the fields to be populated must bee annotated with DtoField and specify the path.
	 *
	 * @param object
	 * @param dtoClass
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T, D extends AbstractDataDto> D objectToDto(T object, Class dtoClass) throws Exception {
		StandardEvaluationContext context = new StandardEvaluationContext(object);
		SpelExpressionParser localParser = new SpelExpressionParser();

		// Create a new dto instance
		D dto = (D) BeanUtils.instantiate(dtoClass);

		// Get object class and fields
		Class objectClass = object.getClass();
		List<Field> objectFields = Arrays.asList(objectClass.getDeclaredFields());

		// Get all the DtoFields (exclue lists and collections)
		List<Field> dtoFields = Arrays.asList(dtoClass.getDeclaredFields()).stream().filter(f -> f.isAnnotationPresent(DtoField.class))
				.collect(Collectors.toList());

		// Copy properties
		for (Field dtoField : dtoFields) {
			String path = this.getDtoFieldPath(dtoField);
			Field objectField = objectFields.stream().filter(f -> f.getName().equals(path)).findFirst().orElse(null);

			// Identify the root and nested elements
			boolean isRootElement = dtoField.getAnnotation(DtoField.class).rootElement();
			boolean isNestedElement = objectField != null ? objectField.isAnnotationPresent(ManyToOne.class)
					|| (!objectField.getType().isPrimitive() && objectField.getType().getPackage().getName().contains("atraxo.mod")) : false;
			boolean isCollection = objectField != null ? objectField.isAnnotationPresent(OneToMany.class)
					|| objectField.getType().equals(Collection.class) || objectField.getType().equals(List.class) : false;

			Method setMethod = this.findFieldMethod(dtoClass, dtoField, FIELD_METHOD_SETTER_PREFIX);

			if (isRootElement || isNestedElement) {
				Object nestedObject = object;
				AbstractDto nestedDto = null;

				if (isNestedElement) {
					Method getMethod = this.findFieldMethod(objectClass, objectField, FIELD_METHOD_GETTER_PREFIX);
					nestedObject = getMethod.invoke(object);
				}

				if (nestedObject != null) {
					nestedDto = this.objectToDto(nestedObject, dtoField.getType());
				}

				setMethod.invoke(dto, nestedDto);
			}

			if (isCollection) {
				// Extract the object class from the object collection
				ParameterizedType dtoCollectionType = (ParameterizedType) dtoField.getGenericType();
				Class dtoCollectionClass = (Class) (dtoCollectionType.getActualTypeArguments()[0]);

				Method getMethod = this.findFieldMethod(objectClass, objectField, FIELD_METHOD_GETTER_PREFIX);
				List<Object> objectCollection = (List<Object>) getMethod.invoke(object);

				List<AbstractDataDto> dtoCollection = new ArrayList<>();
				for (Object e : objectCollection) {
					dtoCollection.add(this.objectToDto(e, dtoCollectionClass));
				}

				setMethod.invoke(dto, dtoCollection);

				continue;
			}

			try {
				setMethod.invoke(dto, localParser.parseExpression(path).getValue(context));
			} catch (Exception exc) {
				if (LOGGER.isTraceEnabled()) {
					LOGGER.trace("Could not set object property to dto , will continue to next property !", exc);
				}
			}
		}

		// Set default values
		dto.setDefaultValues();

		return dto;
	}

	@SuppressWarnings({ "rawtypes" })
	private Method findFieldMethod(Class objectClass, Field field, String methodType) throws NoSuchMethodException {
		List<Method> methods = Arrays.asList(objectClass.getMethods());
		String methodName = methodType + field.getName();

		return methods.stream().filter(m -> m.getName().toUpperCase().equals(methodName.toUpperCase())).findFirst().orElse(null);
	}

	private String getDtoFieldPath(Field dtoField) {
		return dtoField.getAnnotation(DtoField.class).path().isEmpty() ? dtoField.getName() : dtoField.getAnnotation(DtoField.class).path();
	}

}
