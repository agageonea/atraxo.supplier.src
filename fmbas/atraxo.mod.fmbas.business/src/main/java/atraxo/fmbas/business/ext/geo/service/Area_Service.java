/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.geo.service;

import java.util.List;
import java.util.Map;

import atraxo.fmbas.business.api.geo.IAreaService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.fmbas_type.AreasType;
import atraxo.fmbas.domain.impl.geo.Area;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link Area} domain entity.
 */
public class Area_Service extends atraxo.fmbas.business.impl.geo.Area_Service implements IAreaService {

	private static final String BASE_AREA = "WW";
	private static final String SYSTEM_AREA = "System";

	@Override
	public List<Area> findActive() throws BusinessException {
		return this.getEntityManager().createQuery("select e from Area e where e.clientId = :clientId and e.active = true", Area.class)
				.setParameter("clientId", Session.user.get().getClient().getId()).getResultList();
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {

		super.preDeleteByIds(ids, context);

		for (Object id : ids) {
			Area a = this.findById(id);
			if (this.isWWAreas(a)) {
				throw new BusinessException(BusinessErrorCode.AREA_WW, BusinessErrorCode.AREA_WW.getErrMsg());
			}
			if (AreasType._SYSTEM_.equals(a.getIndicator())) {
				throw new BusinessException(BusinessErrorCode.AREA_SYSTEM, BusinessErrorCode.AREA_SYSTEM.getErrMsg());
			}
		}
	}

	/**
	 * Verify if is the system's maintained area.
	 *
	 * @param a
	 * @return
	 */
	private boolean isWWAreas(Area a) {
		if (a.getCode().matches(Area_Service.BASE_AREA)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Area getWW() throws BusinessException {
		return this.findByCode(BASE_AREA);
	}

}