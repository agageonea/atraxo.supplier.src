package atraxo.fmbas.business.ext.bpm.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.workflow.IWorkflowInstanceService;
import atraxo.fmbas.business.api.workflow.IWorkflowNotificationService;
import atraxo.fmbas.business.ext.bpm.WorkflowMsgConstants;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowInstanceStatus;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;

/**
 * WorkflowInstanceResultStatusListener is a listener of an workflow task. It sets the success status and the result of an instance.
 *
 * @author abolindu
 */
public class WorkflowInstanceResultStatusListener implements ExecutionListener {

	private static final long serialVersionUID = -2777383490046822943L;

	@Autowired
	private IWorkflowInstanceService workflowInstanceService;
	@Autowired
	private IWorkflowNotificationService workflowNotificationService;

	@Override
	public void notify(DelegateExecution execution) throws Exception {

		Object wkfInstanceId = execution.getVariable(WorkflowVariablesConstants.VAR_WKF_INSTANCE_ID);
		WorkflowInstance wkfInstance = this.workflowInstanceService.findByBusiness((Integer) wkfInstanceId);

		boolean hasError = execution.hasVariable(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_RELEASE);

		Boolean approved = false;
		if (wkfInstance != null && !hasError && !wkfInstance.getStatus().equals(WorkflowInstanceStatus._FAILURE_)
				&& !wkfInstance.getStatus().equals(WorkflowInstanceStatus._TERMINATED_)) {
			if (execution.hasVariable(WorkflowVariablesConstants.TASK_ACCEPTANCE_VAR)) {
				approved = (Boolean) execution.getVariable(WorkflowVariablesConstants.TASK_ACCEPTANCE_VAR);
			}

			String note = null;
			if (execution.hasVariable(WorkflowVariablesConstants.TASK_NOTE_VAR)) {
				note = (String) execution.getVariable(WorkflowVariablesConstants.TASK_NOTE_VAR);
			}

			StringBuilder instanceResult = new StringBuilder(
					approved ? WorkflowMsgConstants.WKF_RESULT_APPROVED : WorkflowMsgConstants.WKF_RESULT_REJECTED);

			wkfInstance.setStatus(WorkflowInstanceStatus._SUCCESS_);
			wkfInstance.setResult(instanceResult.append(": ").append(note).toString());
			wkfInstance.setEntitites(null);
			this.workflowInstanceService.update(wkfInstance);

			// send notification
			this.workflowNotificationService.sendNotification(wkfInstance, WorkflowInstanceStatus._SUCCESS_, instanceResult.toString(),
					execution.getVariablesLocal());
		}
	}
}
