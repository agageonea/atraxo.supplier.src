/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.notificationEvent;

import atraxo.fmbas.business.api.notificationEvent.INotificationEventService;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link NotificationEvent} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class NotificationEvent_Service
		extends
			AbstractEntityService<NotificationEvent>
		implements
			INotificationEventService {

	/**
	 * Public constructor for NotificationEvent_Service
	 */
	public NotificationEvent_Service() {
		super();
	}

	/**
	 * Public constructor for NotificationEvent_Service
	 * 
	 * @param em
	 */
	public NotificationEvent_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<NotificationEvent> getEntityClass() {
		return NotificationEvent.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return NotificationEvent
	 */
	public NotificationEvent findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(NotificationEvent.NQ_FIND_BY_NAME,
							NotificationEvent.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"NotificationEvent", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"NotificationEvent", "name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return NotificationEvent
	 */
	public NotificationEvent findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(NotificationEvent.NQ_FIND_BY_BUSINESS,
							NotificationEvent.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"NotificationEvent", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"NotificationEvent", "id"), nure);
		}
	}

	/**
	 * Find by reference: contact
	 *
	 * @param contact
	 * @return List<NotificationEvent>
	 */
	public List<NotificationEvent> findByContact(Contacts contact) {
		return this.findByContactId(contact.getId());
	}
	/**
	 * Find by ID of reference: contact.id
	 *
	 * @param contactId
	 * @return List<NotificationEvent>
	 */
	public List<NotificationEvent> findByContactId(Integer contactId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from NotificationEvent e, IN (e.contact) c where e.clientId = :clientId and c.id = :contactId",
						NotificationEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contactId", contactId).getResultList();
	}
}
