/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.soneDefaultViewState;

import atraxo.ad.domain.impl.security.User;
import atraxo.fmbas.domain.impl.soneDefaultViewState.SoneDefaultViewState;
import atraxo.fmbas.domain.impl.soneViewState.SoneViewState;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link SoneDefaultViewState} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class SoneDefaultViewState_Service
		extends
			AbstractEntityService<SoneDefaultViewState> {

	/**
	 * Public constructor for SoneDefaultViewState_Service
	 */
	public SoneDefaultViewState_Service() {
		super();
	}

	/**
	 * Public constructor for SoneDefaultViewState_Service
	 * 
	 * @param em
	 */
	public SoneDefaultViewState_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<SoneDefaultViewState> getEntityClass() {
		return SoneDefaultViewState.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return SoneDefaultViewState
	 */
	public SoneDefaultViewState findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(SoneDefaultViewState.NQ_FIND_BY_BUSINESS,
							SoneDefaultViewState.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"SoneDefaultViewState", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"SoneDefaultViewState", "id"), nure);
		}
	}

	/**
	 * Find by reference: user
	 *
	 * @param user
	 * @return List<SoneDefaultViewState>
	 */
	public List<SoneDefaultViewState> findByUser(User user) {
		return this.findByUserId(user.getId());
	}
	/**
	 * Find by ID of reference: user.id
	 *
	 * @param userId
	 * @return List<SoneDefaultViewState>
	 */
	public List<SoneDefaultViewState> findByUserId(String userId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from SoneDefaultViewState e where e.clientId = :clientId and e.user.id = :userId",
						SoneDefaultViewState.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("userId", userId).getResultList();
	}
	/**
	 * Find by reference: view
	 *
	 * @param view
	 * @return List<SoneDefaultViewState>
	 */
	public List<SoneDefaultViewState> findByView(SoneViewState view) {
		return this.findByViewId(view.getId());
	}
	/**
	 * Find by ID of reference: view.id
	 *
	 * @param viewId
	 * @return List<SoneDefaultViewState>
	 */
	public List<SoneDefaultViewState> findByViewId(Integer viewId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from SoneDefaultViewState e where e.clientId = :clientId and e.view.id = :viewId",
						SoneDefaultViewState.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("viewId", viewId).getResultList();
	}
}
