/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.profile;

import atraxo.fmbas.business.api.profile.IAccountingRulesService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.AccountingRulesType;
import atraxo.fmbas.domain.impl.profile.AccountingRules;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link AccountingRules} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class AccountingRules_Service
		extends
			AbstractEntityService<AccountingRules>
		implements
			IAccountingRulesService {

	/**
	 * Public constructor for AccountingRules_Service
	 */
	public AccountingRules_Service() {
		super();
	}

	/**
	 * Public constructor for AccountingRules_Service
	 * 
	 * @param em
	 */
	public AccountingRules_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<AccountingRules> getEntityClass() {
		return AccountingRules.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return AccountingRules
	 */
	public AccountingRules findByAccountingRules(AccountingRulesType ruleType,
			Customer subsidiary) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							AccountingRules.NQ_FIND_BY_ACCOUNTINGRULES,
							AccountingRules.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("ruleType", ruleType)
					.setParameter("subsidiary", subsidiary).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"AccountingRules", "ruleType, subsidiary"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"AccountingRules", "ruleType, subsidiary"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return AccountingRules
	 */
	public AccountingRules findByAccountingRules(AccountingRulesType ruleType,
			Long subsidiaryId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							AccountingRules.NQ_FIND_BY_ACCOUNTINGRULES_PRIMITIVE,
							AccountingRules.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("ruleType", ruleType)
					.setParameter("subsidiaryId", subsidiaryId)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"AccountingRules", "ruleType, subsidiaryId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"AccountingRules", "ruleType, subsidiaryId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return AccountingRules
	 */
	public AccountingRules findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(AccountingRules.NQ_FIND_BY_BUSINESS,
							AccountingRules.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"AccountingRules", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"AccountingRules", "id"), nure);
		}
	}

	/**
	 * Find by reference: subsidiary
	 *
	 * @param subsidiary
	 * @return List<AccountingRules>
	 */
	public List<AccountingRules> findBySubsidiary(Customer subsidiary) {
		return this.findBySubsidiaryId(subsidiary.getId());
	}
	/**
	 * Find by ID of reference: subsidiary.id
	 *
	 * @param subsidiaryId
	 * @return List<AccountingRules>
	 */
	public List<AccountingRules> findBySubsidiaryId(Integer subsidiaryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from AccountingRules e where e.clientId = :clientId and e.subsidiary.id = :subsidiaryId",
						AccountingRules.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("subsidiaryId", subsidiaryId).getResultList();
	}
}
