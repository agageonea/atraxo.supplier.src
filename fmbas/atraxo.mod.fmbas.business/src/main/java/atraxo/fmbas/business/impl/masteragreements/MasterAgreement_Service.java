/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.masteragreements;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.masteragreements.MasterAgreement;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link MasterAgreement} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class MasterAgreement_Service
		extends
			AbstractEntityService<MasterAgreement> {

	/**
	 * Public constructor for MasterAgreement_Service
	 */
	public MasterAgreement_Service() {
		super();
	}

	/**
	 * Public constructor for MasterAgreement_Service
	 * 
	 * @param em
	 */
	public MasterAgreement_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<MasterAgreement> getEntityClass() {
		return MasterAgreement.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return MasterAgreement
	 */
	public MasterAgreement findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(MasterAgreement.NQ_FIND_BY_BUSINESS,
							MasterAgreement.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"MasterAgreement", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"MasterAgreement", "id"), nure);
		}
	}

	/**
	 * Find by reference: company
	 *
	 * @param company
	 * @return List<MasterAgreement>
	 */
	public List<MasterAgreement> findByCompany(Customer company) {
		return this.findByCompanyId(company.getId());
	}
	/**
	 * Find by ID of reference: company.id
	 *
	 * @param companyId
	 * @return List<MasterAgreement>
	 */
	public List<MasterAgreement> findByCompanyId(Integer companyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from MasterAgreement e where e.clientId = :clientId and e.company.id = :companyId",
						MasterAgreement.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("companyId", companyId).getResultList();
	}
	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<MasterAgreement>
	 */
	public List<MasterAgreement> findByCurrency(Currencies currency) {
		return this.findByCurrencyId(currency.getId());
	}
	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<MasterAgreement>
	 */
	public List<MasterAgreement> findByCurrencyId(Integer currencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from MasterAgreement e where e.clientId = :clientId and e.currency.id = :currencyId",
						MasterAgreement.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currencyId", currencyId).getResultList();
	}
	/**
	 * Find by reference: financialsource
	 *
	 * @param financialsource
	 * @return List<MasterAgreement>
	 */
	public List<MasterAgreement> findByFinancialsource(
			FinancialSources financialsource) {
		return this.findByFinancialsourceId(financialsource.getId());
	}
	/**
	 * Find by ID of reference: financialsource.id
	 *
	 * @param financialsourceId
	 * @return List<MasterAgreement>
	 */
	public List<MasterAgreement> findByFinancialsourceId(
			Integer financialsourceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from MasterAgreement e where e.clientId = :clientId and e.financialsource.id = :financialsourceId",
						MasterAgreement.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("financialsourceId", financialsourceId)
				.getResultList();
	}
	/**
	 * Find by reference: averageMethod
	 *
	 * @param averageMethod
	 * @return List<MasterAgreement>
	 */
	public List<MasterAgreement> findByAverageMethod(AverageMethod averageMethod) {
		return this.findByAverageMethodId(averageMethod.getId());
	}
	/**
	 * Find by ID of reference: averageMethod.id
	 *
	 * @param averageMethodId
	 * @return List<MasterAgreement>
	 */
	public List<MasterAgreement> findByAverageMethodId(Integer averageMethodId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from MasterAgreement e where e.clientId = :clientId and e.averageMethod.id = :averageMethodId",
						MasterAgreement.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("averageMethodId", averageMethodId)
				.getResultList();
	}
}
