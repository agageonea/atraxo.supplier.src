package atraxo.fmbas.business.ext.aop.history;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotations used to be able to log history changes.
 *
 * @author zspeter
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
public @interface History {

	/**
	 * @return
	 */
	String value() default "";

	/**
	 * @return
	 */
	Class<?> type();

}
