/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.user;

import atraxo.fmbas.business.api.user.IRoleSuppService;
import atraxo.fmbas.domain.impl.user.RoleSupp;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link RoleSupp} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class RoleSupp_Service extends AbstractEntityService<RoleSupp>
		implements
			IRoleSuppService {

	/**
	 * Public constructor for RoleSupp_Service
	 */
	public RoleSupp_Service() {
		super();
	}

	/**
	 * Public constructor for RoleSupp_Service
	 * 
	 * @param em
	 */
	public RoleSupp_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<RoleSupp> getEntityClass() {
		return RoleSupp.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return RoleSupp
	 */
	public RoleSupp findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(RoleSupp.NQ_FIND_BY_CODE, RoleSupp.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"RoleSupp", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"RoleSupp", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return RoleSupp
	 */
	public RoleSupp findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(RoleSupp.NQ_FIND_BY_NAME, RoleSupp.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"RoleSupp", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"RoleSupp", "name"), nure);
		}
	}

	/**
	 * Find by reference: users
	 *
	 * @param users
	 * @return List<RoleSupp>
	 */
	public List<RoleSupp> findByUsers(UserSupp users) {
		return this.findByUsersId(users.getId());
	}
	/**
	 * Find by ID of reference: users.id
	 *
	 * @param usersId
	 * @return List<RoleSupp>
	 */
	public List<RoleSupp> findByUsersId(String usersId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from RoleSupp e, IN (e.users) c where e.clientId = :clientId and c.id = :usersId",
						RoleSupp.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("usersId", usersId).getResultList();
	}
}
