/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.dashboard;

import atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.WidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.Widgets;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link WidgetParameters} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class WidgetParameters_Service
		extends
			AbstractEntityService<WidgetParameters> {

	/**
	 * Public constructor for WidgetParameters_Service
	 */
	public WidgetParameters_Service() {
		super();
	}

	/**
	 * Public constructor for WidgetParameters_Service
	 * 
	 * @param em
	 */
	public WidgetParameters_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<WidgetParameters> getEntityClass() {
		return WidgetParameters.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return WidgetParameters
	 */
	public WidgetParameters findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(WidgetParameters.NQ_FIND_BY_NAME,
							WidgetParameters.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"WidgetParameters", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"WidgetParameters", "name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return WidgetParameters
	 */
	public WidgetParameters findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(WidgetParameters.NQ_FIND_BY_BUSINESS,
							WidgetParameters.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"WidgetParameters", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"WidgetParameters", "id"), nure);
		}
	}

	/**
	 * Find by reference: widget
	 *
	 * @param widget
	 * @return List<WidgetParameters>
	 */
	public List<WidgetParameters> findByWidget(Widgets widget) {
		return this.findByWidgetId(widget.getId());
	}
	/**
	 * Find by ID of reference: widget.id
	 *
	 * @param widgetId
	 * @return List<WidgetParameters>
	 */
	public List<WidgetParameters> findByWidgetId(Integer widgetId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from WidgetParameters e where e.clientId = :clientId and e.widget.id = :widgetId",
						WidgetParameters.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("widgetId", widgetId).getResultList();
	}
	/**
	 * Find by reference: assignedWidgetParameters
	 *
	 * @param assignedWidgetParameters
	 * @return List<WidgetParameters>
	 */
	public List<WidgetParameters> findByAssignedWidgetParameters(
			AssignedWidgetParameters assignedWidgetParameters) {
		return this.findByAssignedWidgetParametersId(assignedWidgetParameters
				.getId());
	}
	/**
	 * Find by ID of reference: assignedWidgetParameters.id
	 *
	 * @param assignedWidgetParametersId
	 * @return List<WidgetParameters>
	 */
	public List<WidgetParameters> findByAssignedWidgetParametersId(
			Integer assignedWidgetParametersId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from WidgetParameters e, IN (e.assignedWidgetParameters) c where e.clientId = :clientId and c.id = :assignedWidgetParametersId",
						WidgetParameters.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("assignedWidgetParametersId",
						assignedWidgetParametersId).getResultList();
	}
}
