/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.contacts;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Contacts} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Contacts_Service extends AbstractEntityService<Contacts> {

	/**
	 * Public constructor for Contacts_Service
	 */
	public Contacts_Service() {
		super();
	}

	/**
	 * Public constructor for Contacts_Service
	 * 
	 * @param em
	 */
	public Contacts_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Contacts> getEntityClass() {
		return Contacts.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Contacts
	 */
	public Contacts findById(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Contacts.NQ_FIND_BY_ID, Contacts.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Contacts", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Contacts", "id"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Contacts
	 */
	public Contacts findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Contacts.NQ_FIND_BY_BUSINESS,
							Contacts.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Contacts", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Contacts", "id"), nure);
		}
	}

	/**
	 * Find by reference: country
	 *
	 * @param country
	 * @return List<Contacts>
	 */
	public List<Contacts> findByCountry(Country country) {
		return this.findByCountryId(country.getId());
	}
	/**
	 * Find by ID of reference: country.id
	 *
	 * @param countryId
	 * @return List<Contacts>
	 */
	public List<Contacts> findByCountryId(Integer countryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Contacts e where e.clientId = :clientId and e.country.id = :countryId",
						Contacts.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("countryId", countryId).getResultList();
	}
	/**
	 * Find by reference: notification
	 *
	 * @param notification
	 * @return List<Contacts>
	 */
	public List<Contacts> findByNotification(NotificationEvent notification) {
		return this.findByNotificationId(notification.getId());
	}
	/**
	 * Find by ID of reference: notification.id
	 *
	 * @param notificationId
	 * @return List<Contacts>
	 */
	public List<Contacts> findByNotificationId(Integer notificationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Contacts e, IN (e.notification) c where e.clientId = :clientId and c.id = :notificationId",
						Contacts.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("notificationId", notificationId).getResultList();
	}
}
