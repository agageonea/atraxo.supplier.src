package atraxo.fmbas.business.ext.timeserie.job.importPlatts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.timeserie.IRawTimeSerieItemService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.domain.ext.platts.PlattsData;
import atraxo.fmbas.domain.ext.platts.Symbol;
import atraxo.fmbas.domain.impl.fmbas_type.DataProvider;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesApprovalStatus;
import atraxo.fmbas.domain.impl.timeserie.RawTimeSerieItem;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;

/**
 * @author zspeter
 */
public class PlattsFileItemWriter implements ItemWriter<PlattsData> {

	private static final Logger LOG = LoggerFactory.getLogger(PlattsFileItemWriter.class);

	@Autowired
	private ITimeSerieService tsService;
	@Autowired
	private IRawTimeSerieItemService tsiService;
	@Autowired
	private ISystemParameterService systemParameterService;

	private ExecutionContext executionContext;

	public enum Params {
		IMPORT_LIST, UPDATE_LIST, OVERWRITE, NO, NAN
	}

	@BeforeStep
	public void storeExecutionContext(StepExecution stepExecution) {
		this.executionContext = stepExecution.getJobExecution().getExecutionContext();
	}

	@Override
	public void write(List<? extends PlattsData> items) throws Exception {
		Map<String, TimeSerie> tsMap = new HashMap<>();
		Map<String, List<RawTimeSerieItem>> tsiMap = new HashMap<>();
		List<RawTimeSerieItem> updList = new ArrayList<>();
		List<RawTimeSerieItem> insList = new ArrayList<>();
		Set<String> updCodeList = new HashSet<>();
		Set<String> insCodeList = new HashSet<>();
		List<Object> delListIds = new ArrayList<>();

		for (PlattsData data : items) {
			Date importDate = DateUtils.parseDate(data.getDate(), new String[] { "yyyyMMddHHmm" });
			if (!tsMap.containsKey(data.getSymbol())) {
				try {
					TimeSerie timeSeries = this.tsService.findByCode(data.getSymbol());
					if (this.executionContext.containsKey(Params.OVERWRITE.toString())) {
						if (!timeSeries.getDataProvider().equals(DataProvider._IMPORT_PLATTS_)) {
							continue;
						}
					}
					tsMap.put(data.getSymbol(), timeSeries);
					tsiMap.put(data.getSymbol(), this.tsiService.findByTserie(timeSeries));
				} catch (Exception e) {
					// time series not found or cannot be determined by code
					LOG.warn("Tserie with symbol/code/external serie name \"" + data.getSymbol()
							+ "\" wasn't found in database. Data for this timeseries won't be imported.", e);
					continue;
				}
			}

			List<RawTimeSerieItem> tsiList = tsiMap.get(data.getSymbol());
			tsiList.sort((o1, o2) -> o1.getItemDate().compareTo(o2.getItemDate()));

			if (tsiList.isEmpty() && !data.getClassification().equalsIgnoreCase(Symbol.D.name())
					&& !data.getValue().equalsIgnoreCase(Params.NAN.toString())) {
				insList.add(this.createTsi(tsMap.get(data.getSymbol()), data, importDate));
				insCodeList.add(data.getSymbol());
			} else {
				boolean found = this.dealWithExistingData(updList, delListIds, data, importDate, tsiList, updCodeList, insCodeList);
				if (!found && !data.getClassification().equalsIgnoreCase(Symbol.D.name())
						&& !data.getValue().equalsIgnoreCase(Params.NAN.toString())) {
					insList.add(this.createTsi(tsMap.get(data.getSymbol()), data, importDate));
					insCodeList.add(data.getSymbol());
				}
			}
		}

		if (!updList.isEmpty()) {
			this.tsiService.update(updList);
		}

		if (this.executionContext.containsKey(Params.UPDATE_LIST.toString())) {
			@SuppressWarnings("unchecked")
			Set<String> update = (Set<String>) this.executionContext.get(Params.UPDATE_LIST.toString());
			update.addAll(updCodeList);
			this.executionContext.put(Params.UPDATE_LIST.toString(), update);
		} else {
			this.executionContext.put(Params.UPDATE_LIST.toString(), updCodeList);
		}

		if (!insList.isEmpty()) {
			this.tsiService.insert(insList);
		}

		if (this.executionContext.containsKey(Params.IMPORT_LIST.toString())) {
			@SuppressWarnings("unchecked")
			Set<String> insert = (Set<String>) this.executionContext.get(Params.IMPORT_LIST.toString());
			insert.addAll(insCodeList);
			this.executionContext.put(Params.IMPORT_LIST.toString(), insert);
		} else {
			this.executionContext.put(Params.IMPORT_LIST.toString(), insCodeList);
		}

		if (!delListIds.isEmpty()) {
			this.tsiService.deleteByIds(delListIds);
		}

		// also make sure to set the status of the TimeSeries as Approved after job runs if the approval workflow is enabled
		if (this.systemParameterService.getEnergyPriceApprovalWorkflow()) {
			Set<TimeSerie> modifiedTimeSeriesSet = new HashSet<>();
			updList.forEach(c -> modifiedTimeSeriesSet.add(c.getTserie()));
			insList.forEach(c -> modifiedTimeSeriesSet.add(c.getTserie()));
			modifiedTimeSeriesSet.forEach(c -> c.setApprovalStatus(TimeSeriesApprovalStatus._APPROVED_));
			this.tsService.update(new ArrayList<>(modifiedTimeSeriesSet));
		}
	}

	private boolean dealWithExistingData(List<RawTimeSerieItem> updList, List<Object> delListIds, PlattsData data, Date importDate,
			List<RawTimeSerieItem> tsiList, Set<String> updCodeList, Set<String> insCodeList) {

		boolean found = false;
		for (RawTimeSerieItem tsi : tsiList) {
			if (tsi.getItemDate().equals(importDate)) {
				if (data.getClassification().equalsIgnoreCase(Symbol.D.name())) {
					// add to delete list
					delListIds.add(tsi.getId());
					found = true;
				} else if (!data.getValue().equalsIgnoreCase(Params.NAN.toString())) {
					// add to update list
					if (!tsi.getCalculated()) {
						if (this.executionContext.containsKey(Params.OVERWRITE.toString())
								&& this.executionContext.getString(Params.OVERWRITE.toString()).equalsIgnoreCase(Params.NO.toString())) {
							return true;
						}
						updCodeList.add(data.getSymbol());
					} else {
						insCodeList.add(data.getSymbol());
					}
					tsi.setItemValue(tsi.getTserie().getFactor().multiply(new BigDecimal(data.getValue())));
					tsi.setCalculated(false);
					tsi.setUpdated(true);
					updList.add(tsi);
					found = true;
				}
			}
		}
		return found;
	}

	private RawTimeSerieItem createTsi(TimeSerie timeSerie, PlattsData data, Date importDate) {
		// create for insert
		RawTimeSerieItem tsItem = new RawTimeSerieItem();
		tsItem.setTserie(timeSerie);
		tsItem.setItemDate(importDate);
		tsItem.setItemValue(timeSerie.getFactor().multiply(new BigDecimal(data.getValue())));
		tsItem.setCalculated(false);
		tsItem.setActive(true);
		tsItem.setTransfered(false);
		tsItem.setTransferedItemUpdate(false);
		tsItem.setUpdated(false);
		return tsItem;
	}

}
