package atraxo.fmbas.business.ext.exceptions;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public class NoEntityException extends BusinessException {

	/**
	 *
	 */
	private static final long serialVersionUID = -3322464786472986222L;

	/**
	 * @param clazz - entity class.
	 * @param fields - A string with a list of "-" sperated field names that generates the exception.
	 */
	public NoEntityException(Class<? extends AbstractEntity> clazz, String fields) {
		super(BusinessErrorCode.NO_ENTITY, String.format(BusinessErrorCode.NO_ENTITY.getErrMsg(), clazz.getSimpleName(), fields));
	}

}
