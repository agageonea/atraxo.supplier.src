/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.masteragreements.service;

import java.util.List;

import atraxo.fmbas.business.api.masteragreements.IMasterAgreementService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.masteragreements.MasterAgreement;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link CreditLines} domain entity.
 */
public class MasterAgreement_Service extends atraxo.fmbas.business.impl.masteragreements.MasterAgreement_Service implements IMasterAgreementService {

	@Override
	protected void preInsert(MasterAgreement e) throws BusinessException {
		super.preInsert(e);
		this.checkValidTo(e);
		this.checkValidDate(e);
	}

	@Override
	protected void preUpdate(MasterAgreement e) throws BusinessException {
		super.preUpdate(e);
		this.checkValidTo(e);
		this.checkValidDate(e);
	}

	/**
	 * @param ma
	 * @throws BusinessException
	 */
	private void checkValidDate(MasterAgreement ma) throws BusinessException {
		List<MasterAgreement> maList = this.findByCompany(ma.getCompany());
		for (MasterAgreement masterAgreement : maList) {
			if (masterAgreement.getId().equals(ma.getId())) {
				continue;
			}
			if (masterAgreement.getEvergreen() && (ma.getEvergreen() || masterAgreement.getValidFrom().compareTo(ma.getValidTo()) <= 0
					|| ma.getValidFrom().compareTo(masterAgreement.getValidTo()) <= 0)) {
				throw new BusinessException(BusinessErrorCode.MASTER_AGREEMENT_OVERLAPPING,
						BusinessErrorCode.MASTER_AGREEMENT_OVERLAPPING.getErrMsg());
			}

			if (this.isOverlapping(ma, masterAgreement)) {
				throw new BusinessException(BusinessErrorCode.MASTER_AGREEMENT_OVERLAPPING,
						BusinessErrorCode.MASTER_AGREEMENT_OVERLAPPING.getErrMsg());
			}
		}
	}

	private boolean isOverlapping(MasterAgreement ma1, MasterAgreement ma2) {
		boolean b = !ma1.getEvergreen() && ma1.getValidTo().compareTo(ma2.getValidFrom()) >= 0 && ma1.getValidFrom().compareTo(ma2.getValidTo()) <= 0;
		boolean c = !ma2.getEvergreen() && ma2.getValidTo().compareTo(ma1.getValidFrom()) >= 0 && ma2.getValidFrom().compareTo(ma1.getValidTo()) <= 0;
		return b || c;
	}

	/**
	 * Check the evergreen status and if is false, valid to can not be null.
	 *
	 * @param ma
	 * @throws BusinessException
	 */
	private void checkValidTo(MasterAgreement ma) throws BusinessException {
		if (!ma.getEvergreen() && ma.getValidTo() == null) {
			throw new BusinessException(BusinessErrorCode.MASTER_AGREEMENT_EVERGREEN, BusinessErrorCode.MASTER_AGREEMENT_EVERGREEN.getErrMsg());
		}
		if (ma.getValidTo() != null && ma.getValidFrom().after(ma.getValidTo())) {
			throw new BusinessException(BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
					BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO.getErrMsg());
		}
	}
}