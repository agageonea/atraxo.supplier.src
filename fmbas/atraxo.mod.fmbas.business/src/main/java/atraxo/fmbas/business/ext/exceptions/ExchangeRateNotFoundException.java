package atraxo.fmbas.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;

/**
 * Exception used when can't determine exchange rate.
 *
 * @author zspeter
 *
 */
public class ExchangeRateNotFoundException extends BusinessException {

	private static final long serialVersionUID = -2252342695598455498L;

	public ExchangeRateNotFoundException() {
		super(BusinessErrorCode.NO_EXCHANGE_RATE, BusinessErrorCode.NO_EXCHANGE_RATE.getErrMsg());
	}

	public ExchangeRateNotFoundException(String msg, Throwable e) {
		super(BusinessErrorCode.NO_EXCHANGE_RATE, msg, e);
	}

	public ExchangeRateNotFoundException(Throwable e) {
		super(BusinessErrorCode.NO_EXCHANGE_RATE, BusinessErrorCode.NO_EXCHANGE_RATE.getErrMsg(), e);
	}

	public ExchangeRateNotFoundException(String msg) {
		super(BusinessErrorCode.NO_EXCHANGE_RATE, msg);
	}
}
