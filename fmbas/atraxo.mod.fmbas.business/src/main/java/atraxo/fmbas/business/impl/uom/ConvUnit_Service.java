/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.uom;

import atraxo.fmbas.business.api.uom.IConvUnitService;
import atraxo.fmbas.domain.impl.uom.ConvUnit;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ConvUnit} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ConvUnit_Service extends AbstractEntityService<ConvUnit>
		implements
			IConvUnitService {

	/**
	 * Public constructor for ConvUnit_Service
	 */
	public ConvUnit_Service() {
		super();
	}

	/**
	 * Public constructor for ConvUnit_Service
	 * 
	 * @param em
	 */
	public ConvUnit_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ConvUnit> getEntityClass() {
		return ConvUnit.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ConvUnit
	 */
	public ConvUnit findByUnitsConvFact(Unit unitFromId, Unit unitToId,
			Integer convType) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ConvUnit.NQ_FIND_BY_UNITSCONVFACT,
							ConvUnit.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("unitFromId", unitFromId)
					.setParameter("unitToId", unitToId)
					.setParameter("convType", convType).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ConvUnit", "unitFromId, unitToId, convType"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ConvUnit", "unitFromId, unitToId, convType"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return ConvUnit
	 */
	public ConvUnit findByUnitsConvFact(Long unitFromIdId, Long unitToIdId,
			Integer convType) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							ConvUnit.NQ_FIND_BY_UNITSCONVFACT_PRIMITIVE,
							ConvUnit.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("unitFromIdId", unitFromIdId)
					.setParameter("unitToIdId", unitToIdId)
					.setParameter("convType", convType).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ConvUnit", "unitFromIdId, unitToIdId, convType"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ConvUnit", "unitFromIdId, unitToIdId, convType"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return ConvUnit
	 */
	public ConvUnit findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ConvUnit.NQ_FIND_BY_BUSINESS,
							ConvUnit.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ConvUnit", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ConvUnit", "id"), nure);
		}
	}

	/**
	 * Find by reference: unitFromId
	 *
	 * @param unitFromId
	 * @return List<ConvUnit>
	 */
	public List<ConvUnit> findByUnitFromId(Unit unitFromId) {
		return this.findByUnitFromIdId(unitFromId.getId());
	}
	/**
	 * Find by ID of reference: unitFromId.id
	 *
	 * @param unitFromIdId
	 * @return List<ConvUnit>
	 */
	public List<ConvUnit> findByUnitFromIdId(Integer unitFromIdId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ConvUnit e where e.clientId = :clientId and e.unitFromId.id = :unitFromIdId",
						ConvUnit.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitFromIdId", unitFromIdId).getResultList();
	}
	/**
	 * Find by reference: unitToId
	 *
	 * @param unitToId
	 * @return List<ConvUnit>
	 */
	public List<ConvUnit> findByUnitToId(Unit unitToId) {
		return this.findByUnitToIdId(unitToId.getId());
	}
	/**
	 * Find by ID of reference: unitToId.id
	 *
	 * @param unitToIdId
	 * @return List<ConvUnit>
	 */
	public List<ConvUnit> findByUnitToIdId(Integer unitToIdId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ConvUnit e where e.clientId = :clientId and e.unitToId.id = :unitToIdId",
						ConvUnit.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitToIdId", unitToIdId).getResultList();
	}
}
