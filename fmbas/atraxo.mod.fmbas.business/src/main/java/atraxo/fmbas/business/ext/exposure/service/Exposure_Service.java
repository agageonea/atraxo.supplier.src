/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.exposure.service;

import java.math.BigDecimal;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.exposure.IExposureService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.exposure.Exposure;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.fmbas_type.CreditType;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ExposureLastAction;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;

/**
 * Business extensions specific for {@link Exposure} domain entity.
 */
public class Exposure_Service extends atraxo.fmbas.business.impl.exposure.Exposure_Service implements IExposureService {

	@Autowired
	private ICustomerService customerSrv;

	@Override
	protected void postUpdate(Exposure e) throws BusinessException {
		this.checkCreditLimit(e);
		super.postUpdate(e);
	}

	private void checkCreditLimit(Exposure e) throws BusinessException {
		CreditLines cl = this.customerSrv.getValidCreditLine(e.getCustomer(), e.getModifiedAt());

		if (cl.getAvailability() >= 0) {
			e.getCustomer().setStatus(CustomerStatus._ACTIVE_);
			this.customerSrv.update(e.getCustomer());
		} else {
			this.customerSrv.blacklistCustomer(e.getCustomer(), "Credit line limit exceeded");
		}

	}

	@Override
	public Exposure getExposure(CreditLines cl) throws BusinessException {
		Customer customer = cl.getCustomer();
		CreditTerm creditTerm = this.getCreditLine(customer);
		try {
			return this.findByKey(customer, creditTerm);
		} catch (ApplicationException e) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(e.getErrorCode())) {
				throw e;
			}
			throw new BusinessException(BusinessErrorCode.NO_ENTITY,
					String.format(BusinessErrorCode.NO_ENTITY.getErrMsg(), Exposure.class.getSimpleName(), creditTerm), e);
		}
	}

	private CreditTerm getCreditLine(Customer customer) throws BusinessException {
		try {
			CreditLines cl = this.customerSrv.getValidCreditLine(customer, GregorianCalendar.getInstance().getTime());
			if (cl.getCreditTerm() != null
					&& (CreditType._CREDIT_LINE_.equals(cl.getCreditTerm()) || CreditType._PREPAYMENT_.equals(cl.getCreditTerm()))
					|| CreditType._BANK_GUARANTEE_.equals(cl.getCreditTerm())) {
				return CreditTerm.getByName(cl.getCreditTerm().getName());
			}
		} catch (BusinessException e) {
			if (!e.getErrorCode().equals(BusinessErrorCode.NO_CREDIT_LINE)) {
				throw e;
			}
		}
		return CreditTerm._OPEN_CREDIT_;
	}

	@Override
	public void createExposure(CreditLines creditLines) throws BusinessException {
		Exposure exposure = new Exposure();
		exposure.setType(CreditTerm.getByName(creditLines.getCreditTerm().getName()));
		exposure.setActualUtilization(new BigDecimal(this.calculateActualUtilization(creditLines.getAmount(), creditLines.getAvailability())));
		exposure.setLastAction(ExposureLastAction._EMPTY_);
		exposure.setCustomer(creditLines.getCustomer());
		exposure.setCurrency(creditLines.getCurrency());
		this.insert(exposure);
	}

	@Override
	public void updateExposure(CreditLines creditLines, ExposureLastAction lastAction) throws BusinessException {
		List<Exposure> exposures = this.findByCustomer(creditLines.getCustomer());
		for (Exposure exposure : exposures) {
			Integer actualUtilization = this.calculateActualUtilization(creditLines.getAmount(), creditLines.getAvailability());
			exposure.setActualUtilization(new BigDecimal(actualUtilization));
			exposure.setLastAction(lastAction);
			this.update(exposure);
		}
	}

	private Integer calculateActualUtilization(Integer amount, Integer availability) {
		return amount > 0 ? ((amount - availability) * 100) / amount : 0;
	}
}
