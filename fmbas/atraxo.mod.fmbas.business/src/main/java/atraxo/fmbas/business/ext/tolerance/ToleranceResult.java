package atraxo.fmbas.business.ext.tolerance;

import java.math.BigDecimal;

public class ToleranceResult {

	public enum ResultType {
		WITHIN, NOT_WITHIN, MARGIN;
	};

	private BigDecimal difference;
	private ResultType type;

	public ToleranceResult(ResultType type, BigDecimal difference) {
		this.type = type;
		this.difference = difference;
	}

	public BigDecimal getDifference() {
		return this.difference;
	}

	public ResultType getType() {
		return this.type;
	}

}
