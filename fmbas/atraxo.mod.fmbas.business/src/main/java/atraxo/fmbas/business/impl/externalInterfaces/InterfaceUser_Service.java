/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.externalInterfaces;

import atraxo.fmbas.business.api.externalInterfaces.IInterfaceUserService;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.InterfaceUser;
import atraxo.fmbas.domain.impl.templates.Template;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link InterfaceUser} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class InterfaceUser_Service extends AbstractEntityService<InterfaceUser>
		implements
			IInterfaceUserService {

	/**
	 * Public constructor for InterfaceUser_Service
	 */
	public InterfaceUser_Service() {
		super();
	}

	/**
	 * Public constructor for InterfaceUser_Service
	 * 
	 * @param em
	 */
	public InterfaceUser_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<InterfaceUser> getEntityClass() {
		return InterfaceUser.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return InterfaceUser
	 */
	public InterfaceUser findByKey(ExternalInterface externalInterface,
			UserSupp user) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(InterfaceUser.NQ_FIND_BY_KEY,
							InterfaceUser.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("externalInterface", externalInterface)
					.setParameter("user", user).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"InterfaceUser", "externalInterface, user"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"InterfaceUser", "externalInterface, user"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return InterfaceUser
	 */
	public InterfaceUser findByKey(Long externalInterfaceId, Long userId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(InterfaceUser.NQ_FIND_BY_KEY_PRIMITIVE,
							InterfaceUser.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("externalInterfaceId", externalInterfaceId)
					.setParameter("userId", userId).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"InterfaceUser", "externalInterfaceId, userId"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"InterfaceUser", "externalInterfaceId, userId"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return InterfaceUser
	 */
	public InterfaceUser findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(InterfaceUser.NQ_FIND_BY_BUSINESS,
							InterfaceUser.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"InterfaceUser", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"InterfaceUser", "id"), nure);
		}
	}

	/**
	 * Find by reference: externalInterface
	 *
	 * @param externalInterface
	 * @return List<InterfaceUser>
	 */
	public List<InterfaceUser> findByExternalInterface(
			ExternalInterface externalInterface) {
		return this.findByExternalInterfaceId(externalInterface.getId());
	}
	/**
	 * Find by ID of reference: externalInterface.id
	 *
	 * @param externalInterfaceId
	 * @return List<InterfaceUser>
	 */
	public List<InterfaceUser> findByExternalInterfaceId(
			Integer externalInterfaceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from InterfaceUser e where e.clientId = :clientId and e.externalInterface.id = :externalInterfaceId",
						InterfaceUser.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("externalInterfaceId", externalInterfaceId)
				.getResultList();
	}
	/**
	 * Find by reference: user
	 *
	 * @param user
	 * @return List<InterfaceUser>
	 */
	public List<InterfaceUser> findByUser(UserSupp user) {
		return this.findByUserId(user.getId());
	}
	/**
	 * Find by ID of reference: user.id
	 *
	 * @param userId
	 * @return List<InterfaceUser>
	 */
	public List<InterfaceUser> findByUserId(String userId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from InterfaceUser e where e.clientId = :clientId and e.user.id = :userId",
						InterfaceUser.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("userId", userId).getResultList();
	}
	/**
	 * Find by reference: emailTemplate
	 *
	 * @param emailTemplate
	 * @return List<InterfaceUser>
	 */
	public List<InterfaceUser> findByEmailTemplate(Template emailTemplate) {
		return this.findByEmailTemplateId(emailTemplate.getId());
	}
	/**
	 * Find by ID of reference: emailTemplate.id
	 *
	 * @param emailTemplateId
	 * @return List<InterfaceUser>
	 */
	public List<InterfaceUser> findByEmailTemplateId(Integer emailTemplateId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from InterfaceUser e where e.clientId = :clientId and e.emailTemplate.id = :emailTemplateId",
						InterfaceUser.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("emailTemplateId", emailTemplateId)
				.getResultList();
	}
}
