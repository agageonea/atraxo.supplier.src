package atraxo.fmbas.business.ext.timeserie.job.publishTimeSeries;

import java.util.List;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import seava.j4e.scheduler.batch.listener.DefaultActionListener;

public class PublishTimeSerieListener extends DefaultActionListener {

	private static final String NEW_LINE = "\n";
	private static final String QUOTATION = "quotation";
	private static final String EXCHANGE_RATE = "exchangeRate";

	@SuppressWarnings("unchecked")
	@Override
	public void afterJob(JobExecution jobExecution) {
		super.afterJob(jobExecution);
		ExitStatus status = jobExecution.getExitStatus();
		StringBuilder sb = new StringBuilder(status.getExitDescription());
		List<StepExecution> stepExecution = (List<StepExecution>) jobExecution.getStepExecutions();

		for (StepExecution se : stepExecution) {
			ExecutionContext executionContext = se.getExecutionContext();

			if (executionContext.containsKey(QUOTATION)) {
				List<String> quotations = (List<String>) executionContext.get(QUOTATION);
				sb.append(String.format(BusinessErrorCode.PUBLISH_JOB_QUOTATION_NUMBER.getErrMsg(), quotations.size())).append(": " + NEW_LINE);
				for (String q : quotations) {
					sb.append(" - ").append(q).append("; " + NEW_LINE);
				}
			} else {
				sb.append(String.format(BusinessErrorCode.PUBLISH_JOB_QUOTATION_NUMBER.getErrMsg(), 0)).append(NEW_LINE);
			}

			if (executionContext.containsKey(EXCHANGE_RATE)) {
				List<String> exchangeRates = (List<String>) executionContext.get(EXCHANGE_RATE);
				sb.append(String.format(BusinessErrorCode.PUBLISH_JOB_EXCHANGE_RATE_NUMBER.getErrMsg(), exchangeRates.size()))
						.append(": " + NEW_LINE);
				for (String er : exchangeRates) {
					sb.append(" - ").append(er).append("; " + NEW_LINE);
				}
			} else {
				sb.append(String.format(BusinessErrorCode.PUBLISH_JOB_EXCHANGE_RATE_NUMBER.getErrMsg(), 0)).append(NEW_LINE);
			}
		}
		status = new ExitStatus(status.getExitCode(), sb.toString());
		jobExecution.setExitStatus(status);
	}
}
