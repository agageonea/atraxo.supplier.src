package atraxo.fmbas.business.ext.mailMerge.delegate;

import com.fuelplus.mailmerge.ws.ClientContext;

public class ClientContextManager {

	public static final String ORGANIZATION_NAME = "Atraxo1";

	public static ClientContext getClientContext(String cliendId) {
		ClientContext clientContext = new ClientContext();
		clientContext.setApplicationCode(cliendId);
		clientContext.setOrganizationName(ORGANIZATION_NAME);
		clientContext.setClientID(2060L);
		return clientContext;
	}
}
