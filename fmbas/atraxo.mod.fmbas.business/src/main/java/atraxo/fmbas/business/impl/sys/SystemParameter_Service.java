/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.sys;

import atraxo.fmbas.domain.impl.sys.SystemParameter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link SystemParameter} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class SystemParameter_Service
		extends
			AbstractEntityService<SystemParameter> {

	/**
	 * Public constructor for SystemParameter_Service
	 */
	public SystemParameter_Service() {
		super();
	}

	/**
	 * Public constructor for SystemParameter_Service
	 * 
	 * @param em
	 */
	public SystemParameter_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<SystemParameter> getEntityClass() {
		return SystemParameter.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return SystemParameter
	 */
	public SystemParameter findBySysparam(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(SystemParameter.NQ_FIND_BY_SYSPARAM,
							SystemParameter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"SystemParameter", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"SystemParameter", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return SystemParameter
	 */
	public SystemParameter findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(SystemParameter.NQ_FIND_BY_BUSINESS,
							SystemParameter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"SystemParameter", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"SystemParameter", "id"), nure);
		}
	}

}
