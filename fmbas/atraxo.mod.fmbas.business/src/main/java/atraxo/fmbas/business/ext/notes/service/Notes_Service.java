/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.notes.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import seava.j4e.api.exceptions.BusinessException;
import atraxo.fmbas.business.api.notes.INotesService;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.notes.Notes;

/**
 * Business extensions specific for {@link Notes} domain entity.
 */
public class Notes_Service extends atraxo.fmbas.business.impl.notes.Notes_Service implements INotesService {

	@Override
	public void deleteNote(Integer id, Class<?> c) throws BusinessException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("objectId", id);
		params.put("objectType", c.getSimpleName());
		List<Notes> notes = this.findEntitiesByAttributes(params);
		List<Object> ids = new ArrayList<>();
		for (Notes ch : notes) {
			ids.add(ch.getId());
		}
		this.deleteByIds(ids);
	}

	@Override
	public void deleteNotes(List ids, Class<?> c) throws BusinessException {
		for (Object id : ids) {
			this.deleteNote((Integer) id, c);
		}
	}

	@Override
	public void deleteByEntities(List<? extends AbstractEntity> entities, Class<?> c) throws BusinessException {
		for(AbstractEntity abstractEntity : entities) 
					this.deleteNote(abstractEntity.getId(), c);
	}
}
