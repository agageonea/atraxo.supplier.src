/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.quotation.service;

import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import atraxo.fmbas.business.api.quotation.IQuotationValueService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link QuotationValue} domain entity.
 */
public class QuotationValue_Service extends atraxo.fmbas.business.impl.quotation.QuotationValue_Service implements IQuotationValueService {

	@Override
	public QuotationValue getQuotationValuesByBusinessKey(QuotationValue quotationValue) {
		StringBuilder sbSQL = new StringBuilder();
		sbSQL.append("select e from ").append(QuotationValue.class.getSimpleName()).append(
				" e where e.clientId=:clientId and e.quotation.id = :quotationId and e.validFromDate = :validFromDate and e.validToDate = :validToDate");
		List<QuotationValue> resultList = this.getEntityManager().createQuery(sbSQL.toString(), QuotationValue.class)
				.setParameter("clientId", Session.user.get().getClient().getId()).setParameter("quotationId", quotationValue.getQuotation().getId())
				.setParameter("validFromDate", quotationValue.getValidFromDate()).setParameter("validToDate", quotationValue.getValidToDate())
				.getResultList();
		if (resultList.size() == 0) {
			return null;
		}
		return resultList.get(0);
	}

	@Override
	public QuotationValue getQuotationValueByDate(Quotation quotation, Date date) throws BusinessException {
		StringBuilder sbSQL = new StringBuilder();
		sbSQL.append("select e from ").append(QuotationValue.class.getSimpleName())
				.append(" e where e.clientId=:clientId and e.quotation.id = :quotationId and e.validFromDate <= :date and e.validToDate >= :date");
		try {
			return this.getEntityManager().createQuery(sbSQL.toString(), QuotationValue.class)
					.setParameter("clientId", Session.user.get().getClient().getId()).setParameter("quotationId", quotation.getId())
					.setParameter("date", date).getSingleResult();
		} catch (NoResultException | NonUniqueResultException e) {
			throw new BusinessException(BusinessErrorCode.NO_QUOTATION_VALUE_FOR_DATE, BusinessErrorCode.NO_QUOTATION_VALUE_FOR_DATE.getErrMsg(), e);
		}
	}
}
