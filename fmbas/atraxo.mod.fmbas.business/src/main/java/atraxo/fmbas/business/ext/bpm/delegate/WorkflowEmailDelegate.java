/**
 *
 */
package atraxo.fmbas.business.ext.bpm.delegate;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.WebServiceException;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fuelplus.mailmerge.ws.FileAttachment;

import atraxo.ad.business.api.attachment.IAttachmentService;
import atraxo.ad.domain.impl.ad.TAttachmentType;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.templates.ITemplateService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.mailMerge.MailMergeManager;
import atraxo.fmbas.business.ext.mailMerge.service.MailMerge_Service;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.impl.templates.Template;
import seava.j4e.api.enums.DateFormatAttribute;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Spring Java Bean/ Activiti Delegate with the role of actually sending an e-mail.
 *
 * @author vhojda
 */
public class WorkflowEmailDelegate extends AbstractJavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowEmailDelegate.class);

	private static final String DEFAULT_NO_REPLY_ADDRESS = "no_reply_address";

	private static final int DEFAULT_MAX_ALLOWED_ATTACHMENT_SIZE_MB = 10;
	private static final int MB_BYTES_SIZE = 1048576;

	private String maxAttachmentsSize;

	private Expression template;

	private Expression email;

	@Autowired
	private ITemplateService templateSrv;

	@Autowired
	private MailMerge_Service mailSrv;

	@Autowired
	private IAttachmentService attachmentService;

	@Autowired
	private ISystemParameterService sysParamSrv;

	/**
	 * @param maxAttachmentsSize
	 */
	public WorkflowEmailDelegate(String maxAttachmentsSize) {
		this.maxAttachmentsSize = maxAttachmentsSize;
	}

	/**
	 * @throws Exception
	 */
	@Override
	public void execute() throws Exception {
		// check for parameters existence (if correctly configured from bpm20 file)
		if (this.template != null && this.email != null) {

			String templateName = null;
			EmailDto emailDto = null;

			try {
				templateName = (String) this.template.getValue(this.execution);
				emailDto = (EmailDto) this.email.getValue(this.execution);
			} catch (Exception e) {
				LOGGER.error("ERROR: Could not extract parameters from workflow when sending email!", e);
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_MAIL,
						"Could not extract parameters from workflow when sending email", e);
			}

			// send the email
			if (templateName != null && emailDto != null) {
				boolean sent = this.sendEmail(this.execution, emailDto, templateName);
				LOGGER.info("Mail " + (sent ? "sent" : "not sent") + " for template " + templateName);
			}

		} else {
			LOGGER.error("ERROR: the workflow does not have the email sender components parameters correct filled in !");
			this.workflowBpmErrorHandler.handleError(this.execution, WorkflowVariablesConstants.ERROR_DEFAULT_MAIL,
					"Workflow email components incorrect configured ", null);
		}

	}

	/**
	 * Attempts to send an email and catches all the possible existing errors.
	 *
	 * @param execution
	 * @param dto
	 * @param templateMail
	 * @throws BusinessException
	 * @returns true if mail was sent, false otherwise
	 */
	private boolean sendEmail(DelegateExecution execution, EmailDto dto, String templateMail) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START sendEmail()");
		}
		boolean sent = false;

		// set the "from" before sending the email
		try {
			dto.setFrom(this.sysParamSrv.getNoReplyAddress());
		} catch (Exception e) {
			LOGGER.warn("WARNING:could not find the system param for 'no reply' adress, will use the default one " + DEFAULT_NO_REPLY_ADDRESS, e);
			dto.setFrom(DEFAULT_NO_REPLY_ADDRESS);
		}

		// load the Template
		Map<String, Object> params = new HashMap<>();
		params.put("name", templateMail);
		Template localTemplate = null;
		try {
			localTemplate = this.templateSrv.findEntityByAttributes(params);
		} catch (Exception e) {
			LOGGER.error("ERROR:could not find a template for name '" + templateMail + "'", e);
			this.workflowBpmErrorHandler.handleError(execution, WorkflowVariablesConstants.ERROR_DEFAULT_MAIL,
					"Could not find a template for name '" + templateMail + "'", e);
		}

		// if template exists try to send email
		if (localTemplate != null) {

			// actually send the e-mail
			try {
				String dateFormat = Session.user.get().getSettings().getDateFormat(DateFormatAttribute.JAVA_DATE_FORMAT).toPattern();

				if (this.mailSentIsReminder()) {
					dto.setSubject("Reminder:" + dto.getSubject());
				}

				String json = MailMergeManager.getDataJson(null, "", dto, dateFormat);
				Map<String, String> emailMap;
				if (dto.getAttachments() != null && !dto.getAttachments().isEmpty()) {
					int size = 0;

					List<FileAttachment> fas = new ArrayList<>();
					for (String attach : dto.getAttachments()) {

						Attachment attachment = this.attachmentService.findById(attach);
						FileAttachment fa = new FileAttachment();

						if (attachment.getType().getCategory().equals(TAttachmentType._UPLOAD_)) {
							InputStream is = this.attachmentService.download(attachment.getRefid());
							byte[] bytes = IOUtils.toByteArray(is);
							size += bytes.length;
							fa.setFilecontent(bytes);
							is.close();
						}
						fa.setFilename(!StringUtils.isEmpty(attachment.getFileName()) ? attachment.getFileName() : attachment.getName());
						fas.add(fa);

					}

					if (size > this.getMaximumAllowedAttachmentsSizeBytes()) {
						this.workflowBpmErrorHandler.handleError(execution, WorkflowVariablesConstants.ERROR_DEFAULT_MAIL,
								"The total size of attachments should be under " + this.getMaximumAllowedAttachmentsSizeMB() + " MB !", null);
						emailMap = this.mailSrv.sendMail(localTemplate.getFileReference(), json);
					} else {
						emailMap = this.mailSrv.sendMail(localTemplate.getFileReference(), json, fas);
					}
				} else {
					emailMap = this.mailSrv.sendMail(localTemplate.getFileReference(), json);
				}

				// also, increment the counter representing the number of times the email was sent
				Integer emailCounter = (Integer) this.execution.getVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_SEND_COUNTER);
				emailCounter++;
				this.execution.setVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_SEND_COUNTER, emailCounter);
				this.execution.setVariable(WorkflowVariablesConstants.TASK_EMAILS, emailMap);
				this.execution.setVariable(WorkflowVariablesConstants.TASK_EMAIL_SUBJECT, dto.getSubject());

			} catch (WebServiceException e) {
				if (!this.continueIfMailErrors()) {
					LOGGER.error("ERROR:could not send the email because the Server Mail is unavailable", e);
					this.workflowBpmErrorHandler.handleError(execution, WorkflowVariablesConstants.ERROR_DEFAULT_MAIL,
							"Could not send the email because the Server Mail is not available!", e);
				} else {
					LOGGER.warn("WARN:could not send the email because the Server Mail is unavailable, will continue with the Workflow !", e);
				}

			} catch (Exception e) {
				if (!this.continueIfMailErrors()) {
					LOGGER.error("ERROR:could not send the e-mail for template '" + localTemplate.getName() + "'", e);
					this.workflowBpmErrorHandler.handleError(execution, WorkflowVariablesConstants.ERROR_DEFAULT_MAIL,
							"Could not send the e-mail for template '" + localTemplate.getName() + "'", e);
				} else {
					LOGGER.warn("WARN:could not send the e-mail for template '" + localTemplate.getName() + "', will continue with the Workflow!", e);
				}
			}
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END sendEmail()");
		}
		return sent;
	}

	private boolean continueIfMailErrors() {
		return Boolean.valueOf(this.execution.getVariable(WorkflowVariablesConstants.VAR_CONTINUE_WKF_IF_MAIL_FAIL).toString());
	}

	/**
	 * Checks if the sent mail (if any) should be a reminder or not
	 *
	 * @return
	 */
	private boolean mailSentIsReminder() {
		boolean reminder = false;
		Object counterObj = this.execution.getVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_SEND_COUNTER);
		if (counterObj != null && counterObj instanceof Integer) {
			int emailCounter = ((Integer) counterObj).intValue();
			if (emailCounter > 0) {
				reminder = true;
			}
		}
		return reminder;
	}

	/**
	 * @return
	 */
	private int getMaximumAllowedAttachmentsSizeBytes() {
		return this.getMaximumAllowedAttachmentsSizeMB() * MB_BYTES_SIZE;
	}

	/**
	 * @return
	 */
	private int getMaximumAllowedAttachmentsSizeMB() {
		Integer maxMb = DEFAULT_MAX_ALLOWED_ATTACHMENT_SIZE_MB;
		try {
			maxMb = Integer.parseInt(this.maxAttachmentsSize);
		} catch (NumberFormatException e) {
			LOGGER.warn("WARNING:could not retrieve valid number of MB from parameter " + this.maxAttachmentsSize + "! Will use the default one "
					+ DEFAULT_MAX_ALLOWED_ATTACHMENT_SIZE_MB, e);
		}
		return maxMb;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#getSessionUserCode()
	 */
	@Override
	public String getSessionUserCode() {
		return null;
	}

}
