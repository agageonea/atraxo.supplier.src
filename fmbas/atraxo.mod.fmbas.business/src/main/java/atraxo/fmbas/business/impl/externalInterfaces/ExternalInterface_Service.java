/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.externalInterfaces;

import atraxo.fmbas.domain.impl.dictionary.Dictionary;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceParameter;
import atraxo.fmbas.domain.impl.externalInterfaces.InterfaceUser;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ExternalInterface} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ExternalInterface_Service
		extends
			AbstractEntityService<ExternalInterface> {

	/**
	 * Public constructor for ExternalInterface_Service
	 */
	public ExternalInterface_Service() {
		super();
	}

	/**
	 * Public constructor for ExternalInterface_Service
	 * 
	 * @param em
	 */
	public ExternalInterface_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ExternalInterface> getEntityClass() {
		return ExternalInterface.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ExternalInterface
	 */
	public ExternalInterface findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ExternalInterface.NQ_FIND_BY_NAME,
							ExternalInterface.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ExternalInterface", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ExternalInterface", "name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return ExternalInterface
	 */
	public ExternalInterface findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ExternalInterface.NQ_FIND_BY_BUSINESS,
							ExternalInterface.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ExternalInterface", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ExternalInterface", "id"), nure);
		}
	}

	/**
	 * Find by reference: users
	 *
	 * @param users
	 * @return List<ExternalInterface>
	 */
	public List<ExternalInterface> findByUsers(InterfaceUser users) {
		return this.findByUsersId(users.getId());
	}
	/**
	 * Find by ID of reference: users.id
	 *
	 * @param usersId
	 * @return List<ExternalInterface>
	 */
	public List<ExternalInterface> findByUsersId(Integer usersId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from ExternalInterface e, IN (e.users) c where e.clientId = :clientId and c.id = :usersId",
						ExternalInterface.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("usersId", usersId).getResultList();
	}
	/**
	 * Find by reference: params
	 *
	 * @param params
	 * @return List<ExternalInterface>
	 */
	public List<ExternalInterface> findByParams(
			ExternalInterfaceParameter params) {
		return this.findByParamsId(params.getId());
	}
	/**
	 * Find by ID of reference: params.id
	 *
	 * @param paramsId
	 * @return List<ExternalInterface>
	 */
	public List<ExternalInterface> findByParamsId(Integer paramsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from ExternalInterface e, IN (e.params) c where e.clientId = :clientId and c.id = :paramsId",
						ExternalInterface.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("paramsId", paramsId).getResultList();
	}
	/**
	 * Find by reference: dictionaries
	 *
	 * @param dictionaries
	 * @return List<ExternalInterface>
	 */
	public List<ExternalInterface> findByDictionaries(Dictionary dictionaries) {
		return this.findByDictionariesId(dictionaries.getId());
	}
	/**
	 * Find by ID of reference: dictionaries.id
	 *
	 * @param dictionariesId
	 * @return List<ExternalInterface>
	 */
	public List<ExternalInterface> findByDictionariesId(Integer dictionariesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from ExternalInterface e, IN (e.dictionaries) c where e.clientId = :clientId and c.id = :dictionariesId",
						ExternalInterface.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("dictionariesId", dictionariesId).getResultList();
	}
}
