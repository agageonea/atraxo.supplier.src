package atraxo.fmbas.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;

/**
 *
 * @author zspeter
 *
 */
public class FTPUploadException extends BusinessException {

	/**
	 *
	 * @param exception
	 */
	public FTPUploadException(Throwable exception) {
		super(BusinessErrorCode.FTP_UPLOAD_ERROR, BusinessErrorCode.FTP_UPLOAD_ERROR.getErrMsg(), exception);
	}

}
