/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.quotation;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Quotation} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Quotation_Service extends AbstractEntityService<Quotation> {

	/**
	 * Public constructor for Quotation_Service
	 */
	public Quotation_Service() {
		super();
	}

	/**
	 * Public constructor for Quotation_Service
	 * 
	 * @param em
	 */
	public Quotation_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Quotation> getEntityClass() {
		return Quotation.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Quotation
	 */
	public Quotation findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Quotation.NQ_FIND_BY_NAME,
							Quotation.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Quotation", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Quotation", "name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Quotation
	 */
	public Quotation findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Quotation.NQ_FIND_BY_BUSINESS,
							Quotation.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Quotation", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Quotation", "id"), nure);
		}
	}

	/**
	 * Find by reference: timeseries
	 *
	 * @param timeseries
	 * @return List<Quotation>
	 */
	public List<Quotation> findByTimeseries(TimeSerie timeseries) {
		return this.findByTimeseriesId(timeseries.getId());
	}
	/**
	 * Find by ID of reference: timeseries.id
	 *
	 * @param timeseriesId
	 * @return List<Quotation>
	 */
	public List<Quotation> findByTimeseriesId(Integer timeseriesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Quotation e where e.clientId = :clientId and e.timeseries.id = :timeseriesId",
						Quotation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("timeseriesId", timeseriesId).getResultList();
	}
	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<Quotation>
	 */
	public List<Quotation> findByCurrency(Currencies currency) {
		return this.findByCurrencyId(currency.getId());
	}
	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<Quotation>
	 */
	public List<Quotation> findByCurrencyId(Integer currencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Quotation e where e.clientId = :clientId and e.currency.id = :currencyId",
						Quotation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currencyId", currencyId).getResultList();
	}
	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<Quotation>
	 */
	public List<Quotation> findByUnit(Unit unit) {
		return this.findByUnitId(unit.getId());
	}
	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<Quotation>
	 */
	public List<Quotation> findByUnitId(Integer unitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Quotation e where e.clientId = :clientId and e.unit.id = :unitId",
						Quotation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitId", unitId).getResultList();
	}
	/**
	 * Find by reference: avgMethodIndicator
	 *
	 * @param avgMethodIndicator
	 * @return List<Quotation>
	 */
	public List<Quotation> findByAvgMethodIndicator(
			AverageMethod avgMethodIndicator) {
		return this.findByAvgMethodIndicatorId(avgMethodIndicator.getId());
	}
	/**
	 * Find by ID of reference: avgMethodIndicator.id
	 *
	 * @param avgMethodIndicatorId
	 * @return List<Quotation>
	 */
	public List<Quotation> findByAvgMethodIndicatorId(
			Integer avgMethodIndicatorId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Quotation e where e.clientId = :clientId and e.avgMethodIndicator.id = :avgMethodIndicatorId",
						Quotation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("avgMethodIndicatorId", avgMethodIndicatorId)
				.getResultList();
	}
	/**
	 * Find by reference: quotationValues
	 *
	 * @param quotationValues
	 * @return List<Quotation>
	 */
	public List<Quotation> findByQuotationValues(QuotationValue quotationValues) {
		return this.findByQuotationValuesId(quotationValues.getId());
	}
	/**
	 * Find by ID of reference: quotationValues.id
	 *
	 * @param quotationValuesId
	 * @return List<Quotation>
	 */
	public List<Quotation> findByQuotationValuesId(Integer quotationValuesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Quotation e, IN (e.quotationValues) c where e.clientId = :clientId and c.id = :quotationValuesId",
						Quotation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("quotationValuesId", quotationValuesId)
				.getResultList();
	}
}
