package atraxo.fmbas.business.ext.exceptions;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import seava.j4e.api.exceptions.BusinessException;

public class PrimaryContactCannotDetermineException extends BusinessException {

	/**
	 *
	 */
	private static final long serialVersionUID = 7974849015295479820L;

	/**
	 * @param clazz - entity class.
	 * @param e
	 * @param fields - A string with a list of "-" sperated field names that generates the exception.
	 */
	public PrimaryContactCannotDetermineException(Class<? extends AbstractEntity> clazz, Throwable e) {
		super(BusinessErrorCode.PRIMARY_CONTACT_CANNOT_DETERMINE,
				String.format(BusinessErrorCode.PRIMARY_CONTACT_CANNOT_DETERMINE.getErrMsg(), clazz.getSimpleName()), e);
	}

}
