/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.kpi;

import atraxo.ad.domain.impl.security.MenuItem;
import atraxo.fmbas.business.api.kpi.IKpiService;
import atraxo.fmbas.domain.impl.kpi.Kpi;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Kpi} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Kpi_Service extends AbstractEntityService<Kpi>
		implements
			IKpiService {

	/**
	 * Public constructor for Kpi_Service
	 */
	public Kpi_Service() {
		super();
	}

	/**
	 * Public constructor for Kpi_Service
	 * 
	 * @param em
	 */
	public Kpi_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Kpi> getEntityClass() {
		return Kpi.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Kpi
	 */
	public Kpi findByKpi(String dsName, String methodName) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Kpi.NQ_FIND_BY_KPI, Kpi.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("dsName", dsName)
					.setParameter("methodName", methodName).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(), "Kpi",
							"dsName, methodName"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Kpi", "dsName, methodName"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Kpi
	 */
	public Kpi findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Kpi.NQ_FIND_BY_BUSINESS, Kpi.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(), "Kpi",
							"id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Kpi", "id"), nure);
		}
	}

	/**
	 * Find by reference: menuItem
	 *
	 * @param menuItem
	 * @return List<Kpi>
	 */
	public List<Kpi> findByMenuItem(MenuItem menuItem) {
		return this.findByMenuItemId(menuItem.getId());
	}
	/**
	 * Find by ID of reference: menuItem.id
	 *
	 * @param menuItemId
	 * @return List<Kpi>
	 */
	public List<Kpi> findByMenuItemId(String menuItemId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Kpi e where e.clientId = :clientId and e.menuItem.id = :menuItemId",
						Kpi.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("menuItemId", menuItemId).getResultList();
	}
}
