package atraxo.fmbas.business.ext.externalInterfaces.timeout;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceMessageHistoryService;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceParameters;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceParameter;

/**
 * Batch job reader used to check external interface message history records and in case set them with status failed.
 *
 * @author zspeter
 */
public class ExternalInterfaceMessageHistoryReader implements ItemReader<ExternalInterfaceMessageHistory> {

	@Autowired
	private IExternalInterfaceMessageHistoryService srv;

	@Override
	public ExternalInterfaceMessageHistory read() throws Exception {
		ExternalInterfaceMessageHistory e = this.srv.getOldestExportedMessage();
		if (e == null) {
			return null;
		}
		Date date = e.getExecutionTime();
		for (ExternalInterfaceParameter param : e.getExternalInterface().getParams()) {
			if (ExtInterfaceParameters.TIMEOUT_INTTERVAL.getDisplayName().equalsIgnoreCase(param.getName())) {
				int delta = param.getCurrentValue() != null ? Integer.parseInt(param.getCurrentValue()) : Integer.parseInt(param.getDefaultValue());
				date = DateUtils.addMinutes(date, delta);
				break;
			}
		}
		if (date.before(Calendar.getInstance().getTime())) {
			return e;
		}
		return null;
	}
}
