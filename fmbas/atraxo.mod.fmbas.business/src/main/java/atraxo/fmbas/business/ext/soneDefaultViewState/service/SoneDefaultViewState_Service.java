/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.soneDefaultViewState.service;

import java.util.List;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import atraxo.fmbas.business.api.soneDefaultViewState.ISoneDefaultViewStateService;
import atraxo.fmbas.domain.impl.soneDefaultViewState.SoneDefaultViewState;
import atraxo.fmbas.domain.impl.soneViewState.SoneViewState;

/**
 * Business extensions specific for {@link SoneDefaultViewState} domain entity.
 * 
 */
public class SoneDefaultViewState_Service extends atraxo.fmbas.business.impl.soneDefaultViewState.SoneDefaultViewState_Service 
	implements ISoneDefaultViewStateService{

	// Dan: getDefaultViewStates
	
	@Override
	public List<SoneDefaultViewState> getDefaultViewStates(String cmp) throws BusinessException {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT e FROM ").append(SoneDefaultViewState.class.getSimpleName()).append(" e WHERE ");
		sb.append("e.clientId =:clientId AND e.view.id IN ( SELECT v.id FROM ").append(SoneViewState.class.getSimpleName()).append(" v WHERE v.cmp LIKE '%").append(cmp).append("%') AND e.user.code =:userCode");		

		@SuppressWarnings("unchecked")
		List<SoneDefaultViewState> soneDefaultViewState = this.getEntityManager().createQuery(sb.toString()).setParameter("clientId", Session.user.get().getClientId())
				.setParameter("userCode", Session.user.get().getCode()).getResultList();

		return soneDefaultViewState;
	}
	
	

}
