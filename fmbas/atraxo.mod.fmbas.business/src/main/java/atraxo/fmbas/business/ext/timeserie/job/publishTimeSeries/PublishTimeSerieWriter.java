package atraxo.fmbas.business.ext.timeserie.job.publishTimeSeries;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemWriter;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.support.MessageBuilder;

import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import seava.j4e.business.AbstractApplicationContextAware;

public class PublishTimeSerieWriter extends AbstractApplicationContextAware implements ItemWriter<TimeSerie> {

	private static final String PUBLISH_TIME_SERIES_CHANNEL = "publishTimeSeriesChannel";
	private static final Logger LOG = LoggerFactory.getLogger(PublishTimeSerieWriter.class);
	private StepExecution stepExecution;

	@BeforeStep
	public void storeExecutionContext(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}

	@Override
	public void write(List<? extends TimeSerie> items) throws Exception {
		LOG.info("Write time series.");
		if (!CollectionUtils.isEmpty(items)) {
			TimeSerie ts = items.iterator().next();

			ExitStatus exitStatus = this.getExitStatus(ts);
			this.sendMessage(PUBLISH_TIME_SERIES_CHANNEL, ts.getId());

			this.stepExecution.setExitStatus(exitStatus);
		}
	}

	private ExitStatus getExitStatus(TimeSerie ts) {
		StringBuilder sb = new StringBuilder();
		sb.append(this.stepExecution.getExitStatus().getExitCode());
		sb.append(" w:");
		sb.append(ts.getSerieName()).append(" ");
		return new ExitStatus(sb.toString(), sb.toString());
	}

	protected void sendMessage(String to, Object content) {
		Message<Object> message = MessageBuilder.withPayload(content).build();
		this.getApplicationContext().getBean(to, MessageChannel.class).send(message);
	}

}
