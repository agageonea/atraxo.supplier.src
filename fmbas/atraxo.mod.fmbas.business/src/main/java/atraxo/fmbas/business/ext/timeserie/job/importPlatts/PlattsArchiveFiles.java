package atraxo.fmbas.business.ext.timeserie.job.importPlatts;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.StorageException;
import seava.j4e.business.service.storage.StorageService;

/**
 * Spring bach tasklet to archive platts processed files.
 *
 * @author aradu
 */

public class PlattsArchiveFiles implements Tasklet {

	private static final String FAILED = "Failed";
	private static final String PROCESSED = "Processed";
	private static final String PLATTS_FILE = "PlattsFile";
	private static final String READ_WITH_ERRORS = " - Read with errors";
	private static final String SUCCESFULLY_READ = " - Succesfully read";
	private static final String FILES_INFO_DETAILS = "Files info details: \n";
	private static final String FILES_NAME_INFO = "filesNameInfo";
	public static final String SONE_PLATTS = "platts";

	@Autowired
	private StorageService storageService;

	private static final Logger logger = LoggerFactory.getLogger(PlattsArchiveFiles.class);

	StringBuilder logMessageBuilder;

	@SuppressWarnings("unchecked")
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		JobExecution jobExecution = chunkContext.getStepContext().getStepExecution().getJobExecution();
		ExecutionContext jobExecutionContext = jobExecution.getExecutionContext();
		Date date = jobExecution.getCreateTime();
		this.logMessageBuilder = new StringBuilder(FILES_INFO_DETAILS);

		if (jobExecutionContext.containsKey(FILES_NAME_INFO)) {
			HashMap<File, Boolean> filesNameInfo = (HashMap<File, Boolean>) jobExecutionContext.get(FILES_NAME_INFO);

			for (Map.Entry<File, Boolean> entry : filesNameInfo.entrySet()) {
				this.logMessageBuilder.append(entry.getKey().getPath()).append(entry.getValue() ? SUCCESFULLY_READ : READ_WITH_ERRORS)
						.append(System.lineSeparator());
				this.uploadToS3(entry.getKey(), entry.getValue(), entry.getKey().getName(), date);
				if (entry.getKey().exists()) {
					FileUtils.forceDelete(entry.getKey());
				}
			}
		}
		jobExecutionContext.put(FILES_NAME_INFO, this.logMessageBuilder.toString());
		return RepeatStatus.FINISHED;
	}

	private void uploadToS3(File plattsFile, Boolean isSuccesfull, String fileName, Date executionDate) throws BusinessException {
		try (FileInputStream inputStream = FileUtils.openInputStream(plattsFile)) {
			StringBuilder fullKeyBuilder = this.createFullArchiveKey(isSuccesfull, executionDate);
			fullKeyBuilder.append(File.separator).append(fileName);
			this.storageService.uploadFile(inputStream, fullKeyBuilder.toString(), PLATTS_FILE);
		} catch (IOException e) {
			this.logMessageBuilder.append("Failure to open the file input stream: ").append(plattsFile.getName());
			logger.error("Error", e);
		} catch (StorageException e) {
			this.logMessageBuilder.append("Failure to upload to s3 the following file: ").append(plattsFile.getName());
			logger.error("Error", e);
		}
	}

	private StringBuilder createFullArchiveKey(Boolean isSuccesfull, Date executionDate) throws BusinessException {
		String formatedDate = this.formatDate(executionDate);
		StringBuilder fullPathBuilder = new StringBuilder(SONE_PLATTS).append(File.separator).append(formatedDate).append(File.separator)
				.append(isSuccesfull ? PROCESSED : FAILED);
		return fullPathBuilder;
	}

	private String formatDate(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd-hh-mm-ss");
		return dateFormat.format(date);
	}
}
