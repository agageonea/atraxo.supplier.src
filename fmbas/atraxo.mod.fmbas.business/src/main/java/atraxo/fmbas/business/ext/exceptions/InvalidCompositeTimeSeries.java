package atraxo.fmbas.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.IErrorCode;

public class InvalidCompositeTimeSeries extends BusinessException {

	public InvalidCompositeTimeSeries(IErrorCode errorCode) {
		super(errorCode, errorCode.getErrMsg());
	}

}
