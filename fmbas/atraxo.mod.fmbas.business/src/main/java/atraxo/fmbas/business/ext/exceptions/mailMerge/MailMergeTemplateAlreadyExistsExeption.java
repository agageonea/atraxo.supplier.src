package atraxo.fmbas.business.ext.exceptions.mailMerge;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author apetho
 */
public class MailMergeTemplateAlreadyExistsExeption extends BusinessException {

	private static final long serialVersionUID = -2068925235828611123L;

	/**
	 * @param exception
	 */
	public MailMergeTemplateAlreadyExistsExeption(Throwable exception) {
		super(BusinessErrorCode.TEMPLATE_ALREADY_EXISTS, BusinessErrorCode.TEMPLATE_ALREADY_EXISTS.getErrMsg(), exception);
	}

}
