/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.exposure;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.exposure.Exposure;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Exposure} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Exposure_Service extends AbstractEntityService<Exposure> {

	/**
	 * Public constructor for Exposure_Service
	 */
	public Exposure_Service() {
		super();
	}

	/**
	 * Public constructor for Exposure_Service
	 * 
	 * @param em
	 */
	public Exposure_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Exposure> getEntityClass() {
		return Exposure.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Exposure
	 */
	public Exposure findByKey(Customer customer, CreditTerm type) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Exposure.NQ_FIND_BY_KEY, Exposure.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("customer", customer)
					.setParameter("type", type).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Exposure", "customer, type"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Exposure", "customer, type"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Exposure
	 */
	public Exposure findByKey(Long customerId, CreditTerm type) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Exposure.NQ_FIND_BY_KEY_PRIMITIVE,
							Exposure.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("customerId", customerId)
					.setParameter("type", type).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Exposure", "customerId, type"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Exposure", "customerId, type"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Exposure
	 */
	public Exposure findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Exposure.NQ_FIND_BY_BUSINESS,
							Exposure.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Exposure", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Exposure", "id"), nure);
		}
	}

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<Exposure>
	 */
	public List<Exposure> findByCustomer(Customer customer) {
		return this.findByCustomerId(customer.getId());
	}
	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<Exposure>
	 */
	public List<Exposure> findByCustomerId(Integer customerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Exposure e where e.clientId = :clientId and e.customer.id = :customerId",
						Exposure.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerId", customerId).getResultList();
	}
	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<Exposure>
	 */
	public List<Exposure> findByCurrency(Currencies currency) {
		return this.findByCurrencyId(currency.getId());
	}
	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<Exposure>
	 */
	public List<Exposure> findByCurrencyId(Integer currencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Exposure e where e.clientId = :clientId and e.currency.id = :currencyId",
						Exposure.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currencyId", currencyId).getResultList();
	}
}
