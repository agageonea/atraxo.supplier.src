package atraxo.fmbas.business.ext.ftp.job.bean;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.file.remote.session.SessionFactory;

import atraxo.fmbas.business.ext.exceptions.FTPUploadException;
import atraxo.fmbas.business.ext.ftp.AbstractFtpBean;
import atraxo.fmbas.business.ext.ftp.FtpCommunicationResult;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.StorageException;
import seava.j4e.business.service.storage.StorageService;

/**
 * Uploads a list of files from S3 into an ftp server.
 *
 * @author zspeter
 */
public class FtpPutFilesBean extends AbstractFtpBean implements InitializingBean {

	private static final Logger LOGGER = LoggerFactory.getLogger(FtpPutFilesBean.class);

	@Autowired
	private StorageService storageService;

	/**
	 * @param sessionFactory
	 * @param remoteDirectory
	 * @param fileNamePattern
	 * @param paths
	 */
	public FtpPutFilesBean(SessionFactory<?> sessionFactory, String remoteDirectory, String fileNamePattern) {
		super(sessionFactory, remoteDirectory, fileNamePattern);
	}

	@Override
	public FtpCommunicationResult execute(AbstractEntity... list) throws IOException, StorageException {
		StringBuilder sbLog = new StringBuilder();
		List<AbstractEntity> successList = new ArrayList<>();
		List<AbstractEntity> failureList = new ArrayList<>();

		InputStream inputStream = null;
		for (AbstractEntity entity : list) {
			try {
				inputStream = this.storageService.downloadFile(entity.getRefid());
				String fileName = this.storageService.getFileName(entity.getRefid());
				if (Pattern.matches(wildcardToRegex(this.getFileNamePattern()), fileName)) {
					this.manageFile(inputStream, fileName);
					successList.add(entity);
					sbLog.append("File \"" + fileName + "\" has been uploaded \n");
				}
			} catch (StorageException se) {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(se.getMessage(), se);
				}
				failureList.add(entity);
				sbLog.append("File for entity with refId " + entity.getRefid() + " was not find.");
			} catch (BusinessException e) {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(e.getMessage(), e);
				}
				failureList.add(entity);
				sbLog.append(e.getMessage()).append("\r\n");
			} finally {
				if (inputStream != null) {
					try {
						inputStream.close();
					} catch (IOException e) {
						if (LOGGER.isDebugEnabled()) {
							LOGGER.debug("failed to close session ", e);
						}
					}
				}
			}
		}

		Map<String, List<AbstractEntity>> map = new HashMap<>();
		map.put("SUCCESS", successList);
		map.put("FAILURE", failureList);
		FtpCommunicationResult result = new FtpCommunicationResult();
		result.setMap(map);
		result.setMessage(sbLog.toString());
		return result;
	}

	private void manageFile(InputStream fileContent, String fileName) throws BusinessException {
		int attempt = 1;
		do {
			try {
				this.writeToFTP(fileContent, this.getRemoteDirectory().concat(fileName));
				attempt = this.getRetryAttempts() + 1;
			} catch (FTPUploadException e) {
				attempt++;
				if (attempt > this.getRetryAttempts()) {
					throw e;
				}
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("failed to write file to ftp. retry for " + attempt + " time.", e);
				}
			}
		} while (attempt <= this.getRetryAttempts());

	}

	/**
	 * special method that converts wildCard pattern to Regex pattern
	 *
	 * @param wildcard
	 * @return
	 */
	public static String wildcardToRegex(String wildcard) {
		StringBuilder s = new StringBuilder(wildcard.length());
		s.append('^');
		for (int i = 0, is = wildcard.length(); i < is; i++) {
			char c = wildcard.charAt(i);
			switch (c) {
			case '*':
				s.append(".*");
				break;
			case '?':
				s.append(".");
				break;
			// escape special regexp-characters
			case '(':
			case ')':
			case '[':
			case ']':
			case '$':
			case '^':
			case '.':
			case '{':
			case '}':
			case '|':
			case '\\':
				s.append("\\");
				s.append(c);
				break;
			default:
				s.append(c);
				break;
			}
		}
		s.append('$');
		return s.toString();
	}

}
