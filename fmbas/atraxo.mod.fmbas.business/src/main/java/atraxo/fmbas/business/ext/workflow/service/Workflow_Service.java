/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.workflow.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.workflow.IWorkflowInstanceService;
import atraxo.fmbas.business.api.workflow.IWorkflowNotificationService;
import atraxo.fmbas.business.api.workflow.IWorkflowParameterService;
import atraxo.fmbas.business.api.workflow.IWorkflowService;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;

/**
 * Business extensions specific for {@link Workflow} domain entity.
 */
public class Workflow_Service extends atraxo.fmbas.business.impl.workflow.Workflow_Service implements IWorkflowService {

	private static final Logger LOGGER = LoggerFactory.getLogger(Workflow_Service.class);

	@Autowired
	private IWorkflowParameterService workflowParameterService;

	@Autowired
	private IWorkflowInstanceService workflowInstanceService;

	@Autowired
	private IWorkflowNotificationService workflowNotificationService;

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.business.service.entity.AbstractEntityWriteService#preDeleteByIds(java.util.List, java.util.Map)
	 */
	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);

		List<Object> params = new ArrayList<>();
		List<Object> instances = new ArrayList<>();
		List<Object> notifications = new ArrayList<>();

		// also get all the workflows
		List<Workflow> workflows = new ArrayList<>();

		for (Object id : ids) {
			Workflow wkf = this.findById(id);
			params.addAll(this.collectIds(new ArrayList<>(wkf.getParams())));
			instances.addAll(this.collectIds(new ArrayList<>(wkf.getInstances())));
			notifications.addAll(this.collectIds(new ArrayList<>(wkf.getNotifications())));
			workflows.add(wkf);
		}

		// generate a MAP with key = workflow name and value = workflows with same name
		Map<String, List<Workflow>> workflowMap = new HashMap<>();
		for (Workflow wkf : workflows) {
			String wkfName = wkf.getName();
			if (!workflowMap.containsKey(wkfName)) {
				workflowMap.put(wkfName, new ArrayList<Workflow>());
			}
			workflowMap.get(wkfName).add(wkf);
		}

		// iterate the map and check to see if maybe all the workflows of same type/name will be deleted
		StringBuilder errorMessage = new StringBuilder();
		for (Iterator<Entry<String, List<Workflow>>> it = workflowMap.entrySet().iterator(); it.hasNext();) {
			Entry<String, List<Workflow>> entry = it.next();
			String key = entry.getKey();
			List<Workflow> lst = entry.getValue();

			// get from the DB all the workflows for a specific name
			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put("name", key);
			List<Workflow> wkfList = this.findEntitiesByAttributes(paramMap);

			// compare the number of workflows from DB with the one from the MAP(the ones sent for delete)
			if (lst.size() == wkfList.size()) {
				errorMessage.append("There must be at least one workflow of type '" + key + "'.");
			}
		}

		// checks if the workflows can be deleted
		if (errorMessage.length() != 0) {
			// this means there was an "error"
			throw new BusinessException(ErrorCode.DB_DELETE_ERROR, "Not allowed to delete all workflows." + errorMessage);
		} else {
			// delete the workflows
			this.workflowParameterService.deleteByIds(params);
			this.workflowInstanceService.deleteByIds(instances);
			this.workflowNotificationService.deleteByIds(notifications);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.api.workflow.IWorkflowService#findByNameAndSubsidiary(java.lang.String, java.lang.String)
	 */
	@Override
	public Workflow findByNameAndSubsidiary(String workflowName, String subsidiaryCode) throws BusinessException {
		String wkfCode = this.generateWorkflowCode(workflowName, subsidiaryCode);
		Workflow wkf = this.findByCode(wkfCode);
		return wkf;
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.business.service.entity.AbstractEntityWriteService#preInsert(java.lang.Object)
	 */
	@Override
	protected void preInsert(Workflow e) throws BusinessException {
		super.preInsert(e);
		e.setCode(this.generateWorkflowCode(e));
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.business.service.entity.AbstractEntityWriteService#preUpdate(java.lang.Object)
	 */
	@Override
	protected void preUpdate(Workflow e) throws BusinessException {
		super.preUpdate(e);
		e.setCode(this.generateWorkflowCode(e));
	}

	private String generateWorkflowCode(Workflow e) {
		return this.generateWorkflowCode(e.getName(), e.getSubsidiary() != null ? e.getSubsidiary().getCode() : null);
	}

	private String generateWorkflowCode(String workflowName, String subsidiaryCode) {
		String wkfCode = workflowName;
		if (!StringUtils.isEmpty(subsidiaryCode)) {
			wkfCode = wkfCode + " " + subsidiaryCode;
		}
		wkfCode = wkfCode.replaceAll(" ", "_").toUpperCase();
		return wkfCode;
	}

}
