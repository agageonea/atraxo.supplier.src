/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.externalInterfaces.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceMessageHistoryService;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.storage.StorageService;
import seava.j4e.commons.utils.XMLUtils;

/**
 * Business extensions specific for {@link ExternalInterfaceMessageHistory} domain entity.
 */

public class ExternalInterfaceMessageHistory_Service extends atraxo.fmbas.business.impl.externalInterfaces.ExternalInterfaceMessageHistory_Service
		implements IExternalInterfaceMessageHistoryService {

	private static final Logger LOG = LoggerFactory.getLogger(ExternalInterfaceMessageHistory_Service.class);

	@Autowired
	private StorageService storageService;
	@Autowired
	private ExternalInterfaceMessageHistoryFacade messageFacade;

	@Override
	@Transactional
	public void updateMessageHistory(MSG message, ExternalInterfaceMessageHistoryStatus status) throws BusinessException {
		List<ExternalInterfaceMessageHistory> list = this.findByMessageId(message.getHeaderCommon().getCorrelationID());
		Calendar cal = GregorianCalendar.getInstance();
		for (ExternalInterfaceMessageHistory messageHistory : list) {
			messageHistory.setStatus(status);
			messageHistory.setResponseTime(cal.getTime());
			messageHistory.setResponseMessageId(message.getHeaderCommon().getMsgID());
		}
		this.update(list);
		try {
			this.messageFacade.uploadToS3(XMLUtils.toString(message, MSG.class, true, true), message.getHeaderCommon().getMsgID(), false);
		} catch (JAXBException e) {
			LOG.warn("Upload to webdav unsuccess.", e);
		}
	}

	@Override
	public List<ExternalInterfaceMessageHistory> findByObjIdType(Integer objectId, String objectType) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("objectId", objectId);
		params.put("objectType", objectType);
		return this.findEntitiesByAttributes(ExternalInterfaceMessageHistory.class, params);
	}

	@Override
	public List<ExternalInterfaceMessageHistory> findByMessageId(String messageId) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("messageId", messageId);
		return this.findEntitiesByAttributes(ExternalInterfaceMessageHistory.class, params);
	}

	@Override
	public Boolean isLastMessage(Integer objectId, String objectType, String messageId) throws BusinessException {
		ExternalInterfaceMessageHistory history = this.findLastMessage(objectId, objectType);
		return history != null && history.getMessageId().equals(messageId);
	}

	@Override
	public ExternalInterfaceMessageHistory findLastMessage(Integer objectId, String objectType) throws BusinessException {
		List<ExternalInterfaceMessageHistory> list = this.findByObjIdType(objectId, objectType);
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}
		sortByDate(list);
		return list.get(0);
	}

	public static List<ExternalInterfaceMessageHistory> sortByDate(List<ExternalInterfaceMessageHistory> list) {

		Collections.sort(list, (o1, o2) -> -1 * o1.getExecutionTime().compareTo(o2.getExecutionTime()));
		return list;

	}

	@Override
	public void uploadToS3(String message, String messageId, Boolean sent) throws BusinessException {
		StringBuilder dataType = new StringBuilder("ExternalInterfaces");
		if (sent) {
			dataType.append("Sent");
		} else {
			dataType.append("Received");
		}
		String objectKey = new StringBuilder(messageId).append(".xml").toString();
		if (this.storageService.existsResource(objectKey)) {
			return;
		}
		byte[] byteMessage = message.getBytes();
		this.storageService.uploadFile(byteMessage, objectKey, dataType.toString());

	}

	@Override
	public String readFromS3(String msdId, Boolean sent) throws BusinessException {
		String fullPath = new StringBuilder(msdId).append(".xml").toString();
		if (this.storageService.existsResource(fullPath)) {
			InputStream inputStreamMessage = this.storageService.downloadFile(fullPath);
			String message = "";
			try {
				message = IOUtils.toString(inputStreamMessage);
			} catch (IOException e) {
				LOG.warn("Read from webdav unsuccess.", e);
			}
			return message;
		} else {
			return "The file could not be found on the server.";
		}
	}

	@Override
	public ExternalInterfaceMessageHistory getOldestExportedMessage() throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("status", ExternalInterfaceMessageHistoryStatus._EXPORTED_);
		List<ExternalInterfaceMessageHistory> list = this.findEntitiesByAttributes(params, 1);
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}
		Collections.sort(list, (e1, e2) -> e1.getExecutionTime().compareTo(e2.getExecutionTime()));
		return list.get(0);
	}

	@Override
	public List<ExternalInterfaceMessageHistory> findByResponseMessageId(String messageId) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("responseMessageId", messageId);
		return this.findEntitiesByAttributes(ExternalInterfaceMessageHistory.class, params);
	}

}
