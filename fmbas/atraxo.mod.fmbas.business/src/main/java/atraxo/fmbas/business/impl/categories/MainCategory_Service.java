/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.categories;

import atraxo.fmbas.business.api.categories.IMainCategoryService;
import atraxo.fmbas.domain.impl.categories.MainCategory;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link MainCategory} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class MainCategory_Service extends AbstractEntityService<MainCategory>
		implements
			IMainCategoryService {

	/**
	 * Public constructor for MainCategory_Service
	 */
	public MainCategory_Service() {
		super();
	}

	/**
	 * Public constructor for MainCategory_Service
	 * 
	 * @param em
	 */
	public MainCategory_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<MainCategory> getEntityClass() {
		return MainCategory.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return MainCategory
	 */
	public MainCategory findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(MainCategory.NQ_FIND_BY_CODE,
							MainCategory.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"MainCategory", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"MainCategory", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return MainCategory
	 */
	public MainCategory findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(MainCategory.NQ_FIND_BY_BUSINESS,
							MainCategory.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"MainCategory", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"MainCategory", "id"), nure);
		}
	}

}
