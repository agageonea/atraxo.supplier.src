package atraxo.fmbas.business.ext.calculator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import atraxo.fmbas.business.api.timeserie.ITimeSerieItemService;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Calculator implementation for weekly trading days average calculation.
 *
 * @author zspeter
 */
public class WTCalculator extends AbstractWeeklyCalculator implements Calculator {

	@Override
	public List<TimeSerieItem> getItemsForCalculation(List<TimeSerieItem> timeSerieItems, ITimeSerieItemService service) throws BusinessException {
		Collections.sort(timeSerieItems, new TimeSerieItemDateComparator());
		List<TimeSerieItem> allTimeSeriesItems = new ArrayList<>();
		for (TimeSerieItem timeSerieItem : timeSerieItems) {
			if (!timeSerieItem.getCalculated()) {
				allTimeSeriesItems.add(timeSerieItem);
			}
		}

		return allTimeSeriesItems;
	}
}
