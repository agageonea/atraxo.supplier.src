/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.uom;

import atraxo.fmbas.business.api.uom.IFisLovService;
import atraxo.fmbas.domain.impl.uom.FisLov;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FisLov} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FisLov_Service extends AbstractEntityService<FisLov>
		implements
			IFisLovService {

	/**
	 * Public constructor for FisLov_Service
	 */
	public FisLov_Service() {
		super();
	}

	/**
	 * Public constructor for FisLov_Service
	 * 
	 * @param em
	 */
	public FisLov_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FisLov> getEntityClass() {
		return FisLov.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FisLov
	 */
	public FisLov findById(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FisLov.NQ_FIND_BY_ID, FisLov.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FisLov", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FisLov", "id"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FisLov
	 */
	public FisLov findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FisLov.NQ_FIND_BY_BUSINESS, FisLov.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FisLov", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FisLov", "id"), nure);
		}
	}

}
