/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.profile.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atraxo.fmbas.business.api.profile.IBankAccountService;
import atraxo.fmbas.business.ext.exceptions.DuplicateEntityException;
import atraxo.fmbas.domain.impl.profile.BankAccount;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link BankAccount} domain entity.
 */
public class BankAccount_Service extends atraxo.fmbas.business.impl.profile.BankAccount_Service implements IBankAccountService {

	@Override
	protected void preInsert(BankAccount e) throws BusinessException {
		super.preInsert(e);
		this.checkBusinessKey(e);
	}

	@Override
	protected void preUpdate(BankAccount e) throws BusinessException {
		super.preUpdate(e);
		this.checkBusinessKey(e);
	}

	private void checkBusinessKey(BankAccount e) throws DuplicateEntityException {
		Map<String, Object> params = new HashMap<>();
		params.put("accountNumber", e.getAccountNumber());
		params.put("currency", e.getCurrency());
		params.put("subsidiary", e.getSubsidiary());

		List<BankAccount> bankAccounts = this.findEntitiesByAttributes(params);
		if ((!bankAccounts.isEmpty() && e.getId() == null) || bankAccounts.size() > 1) {
			throw new DuplicateEntityException(BankAccount.class,
					e.getAccountNumber() + " - " + e.getCurrency().getCode() + " - " + e.getSubsidiary().getCode());
		}
	}

}
