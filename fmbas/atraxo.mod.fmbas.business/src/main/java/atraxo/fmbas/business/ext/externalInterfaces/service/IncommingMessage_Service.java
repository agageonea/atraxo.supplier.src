/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.externalInterfaces.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import atraxo.fmbas.business.api.externalInterfaces.IIncommingMessageService;
import atraxo.fmbas.business.ext.exceptions.NoEntityException;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.IncommingMessage;
import atraxo.fmbas.domain.impl.fmbas_type.IncomingMessageStatus;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link IncommingMessage} domain entity.
 */
public class IncommingMessage_Service extends atraxo.fmbas.business.impl.externalInterfaces.IncommingMessage_Service
		implements IIncommingMessageService {

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public IncommingMessage getNextMessage(ExternalInterface extInt) throws BusinessException {
		String qlString = "SELECT e FROM IncommingMessage e WHERE e.status = :status AND e.externalInterface = :extInt AND e.clientId = :clientId ORDER BY e.createdAt";
		List<IncommingMessage> list = this.getEntityManager().createQuery(qlString, this.getEntityClass()).setParameter("extInt", extInt)
				.setParameter("status", IncomingMessageStatus._NEW_).setParameter("clientId", Session.user.get().getClientId()).setMaxResults(1)
				.getResultList();

		if (list.isEmpty()) {
			throw new NoEntityException(IncommingMessage.class, "(no filter)");
		}

		IncommingMessage msg = list.get(0);
		msg.setStatus(IncomingMessageStatus._PROCESSING_);
		this.update(msg);

		return msg;
	}

	@Override
	public List<IncommingMessage> findByMessageId(String msgId) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("messageId", msgId);
		return this.findEntitiesByAttributes(params);
	}

}
