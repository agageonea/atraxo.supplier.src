/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.geo;

import atraxo.fmbas.business.api.geo.ICountryService;
import atraxo.fmbas.domain.impl.geo.Country;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Country} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Country_Service extends AbstractEntityService<Country>
		implements
			ICountryService {

	/**
	 * Public constructor for Country_Service
	 */
	public Country_Service() {
		super();
	}

	/**
	 * Public constructor for Country_Service
	 * 
	 * @param em
	 */
	public Country_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Country> getEntityClass() {
		return Country.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Country
	 */
	public Country findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Country.NQ_FIND_BY_CODE, Country.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Country", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Country", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Country
	 */
	public Country findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Country.NQ_FIND_BY_BUSINESS,
							Country.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Country", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Country", "id"), nure);
		}
	}

	/**
	 * Find by reference: country
	 *
	 * @param country
	 * @return List<Country>
	 */
	public List<Country> findByCountry(Country country) {
		return this.findByCountryId(country.getId());
	}
	/**
	 * Find by ID of reference: country.id
	 *
	 * @param countryId
	 * @return List<Country>
	 */
	public List<Country> findByCountryId(Integer countryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Country e where e.clientId = :clientId and e.country.id = :countryId",
						Country.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("countryId", countryId).getResultList();
	}
}
