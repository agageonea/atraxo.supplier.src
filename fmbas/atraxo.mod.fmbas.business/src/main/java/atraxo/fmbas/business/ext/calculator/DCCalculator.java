package atraxo.fmbas.business.ext.calculator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.timeserie.ITimeSerieItemService;
import atraxo.fmbas.business.ext.calculator.model.Result;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Calculator for Daily averages.
 *
 * @author zspeter
 */
public class DCCalculator implements Calculator {

	private static final Logger LOGGER = LoggerFactory.getLogger(DCCalculator.class);

	@Override
	public List<Result> calculate(List<TimeSerieItem> timeSerieItem, List<TimeSerieItem> itemsForProvisioning) {
		List<Result> resultList = new ArrayList<Result>();
		for (TimeSerieItem item : timeSerieItem) {
			resultList.add(this.transferItemToResult(item, itemsForProvisioning));
		}
		return resultList;
	}

	private Result transferItemToResult(TimeSerieItem item, List<TimeSerieItem> itemsForProvisioning) {
		Result result = new Result();
		result.setValidFromDate(item.getItemDate());
		result.setValidToDate(item.getItemDate());
		result.setProvisioned(this.isProvisioned(item, itemsForProvisioning));
		result.setRate(item.getItemValue());

		return result;
	}

	/**
	 * Determine provisioned flag for the from time series item <code>timeSerieItem</code>. If time series item is on week-end than return the
	 * previous calculated value.
	 *
	 * @param timeSerieItem
	 * @param provisioned
	 * @return
	 */
	private boolean isProvisioned(TimeSerieItem timeSerieItem, List<TimeSerieItem> itemList) {
		boolean provisioned = true;
		Calendar cal = Calendar.getInstance();
		Date endDate = timeSerieItem.getItemDate();
		cal.setTime(endDate);
		int counter = 3;
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			counter = 4;
		}
		cal.setTime(timeSerieItem.getItemDate());
		if (endDate.before(new Date())) {
			provisioned = false;
		} else {
			Date provDetEndDate = DateUtils.addDays(endDate, counter);
			for (TimeSerieItem tsi : itemList) {
				if (counter <= 0) {
					break;
				}
				if (tsi.getItemDate().after(endDate) && tsi.getItemDate().before(provDetEndDate)) {
					if (!tsi.getCalculated()) {
						provisioned = false;
						break;
					} else {
						counter--;
					}
				}
			}
		}
		return provisioned;
	}

	@Override
	public List<TimeSerieItem> getItemsForCalculation(List<TimeSerieItem> timeSerieItems, ITimeSerieItemService service) throws BusinessException {
		Collections.sort(timeSerieItems, new TimeSerieItemDateComparator());
		// Set<TimeSerieItem> allTimeSeriesItems = new HashSet<TimeSerieItem>();
		// for (TimeSerieItem timeSerieItem : timeSerieItems) {
		// TimeSerieItem nextItem = service.findNextNotCalculatedItem(timeSerieItem);
		// if (nextItem == null) {
		// TimeSerieItem prevItem = service.findPrevNotCalculatedItem(timeSerieItem);
		// if (prevItem == null) {
		// allTimeSeriesItems.add(timeSerieItem);
		// } else {
		// allTimeSeriesItems.addAll(this.getItemsBetweenElementsWithCompleteDays(prevItem, timeSerieItem, service));
		// }
		// } else {
		// allTimeSeriesItems.addAll(this.getItemsBetweenElementsWithCompleteDays(timeSerieItem, nextItem, service));
		// }
		// }
		return new ArrayList<TimeSerieItem>(timeSerieItems);
	}

	/**
	 * Get time series items between last Monday before <code>fromItem</code> item date and first Sunday after <code>toItem</code>.
	 *
	 * @param fromItem
	 * @param toItem
	 * @param service
	 * @return
	 * @throws BusinessException
	 */
	private Collection<? extends TimeSerieItem> getItemsBetweenElementsWithCompleteDays(TimeSerieItem fromItem, TimeSerieItem toItem,
			ITimeSerieItemService service) throws BusinessException {
		Date startDate = fromItem.getItemDate();
		Date endDate = toItem.getItemDate();
		Calendar endCal = Calendar.getInstance();
		endCal.setTime(endDate);
		if (endCal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
			endCal.add(Calendar.DAY_OF_YEAR, 2);
			endDate = endCal.getTime();
		} else if (endCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			endCal.add(Calendar.DAY_OF_YEAR, 1);
			endDate = endCal.getTime();
		}
		return service.getAllElementsBetweenDatesWithMargins(startDate, endDate, toItem.getTserie().getId(), null);
	}

	@Override
	public List<TimeSerieItem> getItemsForProvisioning(List<TimeSerieItem> tsiList, ITimeSerieItemService service) throws BusinessException {
		Set<TimeSerieItem> tsiSet = new HashSet<TimeSerieItem>(tsiList);
		Date now = new Date();
		Collections.sort(tsiList, new TimeSerieItemDateComparator());
		try {
			TimeSerieItem lastItem = tsiList.get(tsiList.size() - 1);
			// item date is not in the past
			if (!lastItem.getItemDate().before(now)) {
				Date endDate = lastItem.getItemDate();
				tsiSet.addAll(service.getAllElementsBetweenDatesWithMargins(endDate, this.getProvisioningIntervalEndDate(endDate),
						lastItem.getTserie().getId(), false));
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			// tsiList is empty occurs when all elements for a calculation
			// periods are deleted
			LOGGER.info("Got an ArrayIndexOutOfBoundsException because all elements for a calculation periods are deleted, will do nothing!", e);
		}
		return new ArrayList<TimeSerieItem>(tsiSet);
	}

	private Date getProvisioningIntervalEndDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			cal.add(Calendar.DAY_OF_MONTH, 4);
		} else {
			cal.add(Calendar.DAY_OF_MONTH, 3);
		}
		return cal.getTime();
	}
}
