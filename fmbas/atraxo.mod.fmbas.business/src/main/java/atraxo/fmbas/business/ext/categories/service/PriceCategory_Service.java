/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.categories.service;

import java.util.List;

import atraxo.fmbas.business.api.categories.IPriceCategoryService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link PriceCategory} domain entity.
 */
public class PriceCategory_Service extends atraxo.fmbas.business.impl.categories.PriceCategory_Service implements IPriceCategoryService {

	@Override
	public PriceCategory findbyName(String name) throws BusinessException {
		StringBuilder sbSql = new StringBuilder();
		sbSql.append("SELECT e ").append("from ").append(PriceCategory.class.getSimpleName()).append(" e ")
				.append("WHERE e.clientId =:clientId and e.name = :name");
		List<PriceCategory> priceCategories = this.getEntityManager().createQuery(sbSql.toString(), PriceCategory.class)
				.setParameter("clientId", Session.user.get().getClient().getId()).setParameter("name", name).getResultList();
		if (!priceCategories.isEmpty()) {
			return priceCategories.get(0);
		}
		throw new BusinessException(BusinessErrorCode.NO_ENTITY, "Cannot find price category by name: " + name);
	}

	@Override
	protected void preInsert(PriceCategory e) throws BusinessException {
		super.preInsert(e);
		checkComposite(e);
	}

	@Override
	protected void preUpdate(PriceCategory e) throws BusinessException {
		super.preUpdate(e);
		checkComposite(e);
	}

	private void checkComposite(PriceCategory pc) throws BusinessException {
		if (PriceInd._COMPOSITE_.equals(pc.getPricePer()) && (!PriceType._PRODUCT_.equals(pc.getType()) || !pc.getMainCategory().getIsProd())) {
			throw new BusinessException(BusinessErrorCode.PRICE_CATEGORY_COMPOSITE_PRODUCT,
					BusinessErrorCode.PRICE_CATEGORY_COMPOSITE_PRODUCT.getErrMsg());
		}
	}
}
