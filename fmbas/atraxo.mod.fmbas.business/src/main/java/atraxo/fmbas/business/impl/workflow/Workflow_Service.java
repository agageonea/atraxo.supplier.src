/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.workflow;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowNotification;
import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Workflow} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Workflow_Service extends AbstractEntityService<Workflow> {

	/**
	 * Public constructor for Workflow_Service
	 */
	public Workflow_Service() {
		super();
	}

	/**
	 * Public constructor for Workflow_Service
	 * 
	 * @param em
	 */
	public Workflow_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Workflow> getEntityClass() {
		return Workflow.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Workflow
	 */
	public Workflow findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Workflow.NQ_FIND_BY_CODE, Workflow.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Workflow", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Workflow", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Workflow
	 */
	public Workflow findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Workflow.NQ_FIND_BY_BUSINESS,
							Workflow.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Workflow", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Workflow", "id"), nure);
		}
	}

	/**
	 * Find by reference: subsidiary
	 *
	 * @param subsidiary
	 * @return List<Workflow>
	 */
	public List<Workflow> findBySubsidiary(Customer subsidiary) {
		return this.findBySubsidiaryId(subsidiary.getId());
	}
	/**
	 * Find by ID of reference: subsidiary.id
	 *
	 * @param subsidiaryId
	 * @return List<Workflow>
	 */
	public List<Workflow> findBySubsidiaryId(Integer subsidiaryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Workflow e where e.clientId = :clientId and e.subsidiary.id = :subsidiaryId",
						Workflow.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("subsidiaryId", subsidiaryId).getResultList();
	}
	/**
	 * Find by reference: notifications
	 *
	 * @param notifications
	 * @return List<Workflow>
	 */
	public List<Workflow> findByNotifications(WorkflowNotification notifications) {
		return this.findByNotificationsId(notifications.getId());
	}
	/**
	 * Find by ID of reference: notifications.id
	 *
	 * @param notificationsId
	 * @return List<Workflow>
	 */
	public List<Workflow> findByNotificationsId(Integer notificationsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Workflow e, IN (e.notifications) c where e.clientId = :clientId and c.id = :notificationsId",
						Workflow.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("notificationsId", notificationsId)
				.getResultList();
	}
	/**
	 * Find by reference: instances
	 *
	 * @param instances
	 * @return List<Workflow>
	 */
	public List<Workflow> findByInstances(WorkflowInstance instances) {
		return this.findByInstancesId(instances.getId());
	}
	/**
	 * Find by ID of reference: instances.id
	 *
	 * @param instancesId
	 * @return List<Workflow>
	 */
	public List<Workflow> findByInstancesId(Integer instancesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Workflow e, IN (e.instances) c where e.clientId = :clientId and c.id = :instancesId",
						Workflow.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("instancesId", instancesId).getResultList();
	}
	/**
	 * Find by reference: params
	 *
	 * @param params
	 * @return List<Workflow>
	 */
	public List<Workflow> findByParams(WorkflowParameter params) {
		return this.findByParamsId(params.getId());
	}
	/**
	 * Find by ID of reference: params.id
	 *
	 * @param paramsId
	 * @return List<Workflow>
	 */
	public List<Workflow> findByParamsId(Integer paramsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Workflow e, IN (e.params) c where e.clientId = :clientId and c.id = :paramsId",
						Workflow.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("paramsId", paramsId).getResultList();
	}
}
