package atraxo.fmbas.business.ext.exchangerate.job.importBloomberg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;

import seava.j4e.scheduler.batch.listener.DefaultActionListener;

/**
 * @author vhojda
 */
public class ImportExchangeRatesListener extends DefaultActionListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(ImportExchangeRatesListener.class);

	@Override
	public void afterJob(JobExecution jobExecution) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START ImportExchangeRatesListener()");
		}
		super.afterJob(jobExecution);

		// remake the exit status (if the case)
		ExitStatus currentExistingStatus = jobExecution.getExitStatus();

		if (jobExecution.getExecutionContext().containsKey(ImportExchangeRatesTasklet.KEY_LOG)) {
			String description = (String) jobExecution.getExecutionContext().get(ImportExchangeRatesTasklet.KEY_LOG);

			String exitCode = jobExecution.getExecutionContext().containsKey(ImportExchangeRatesTasklet.KEY_NOT_OK) ? ExitStatus.FAILED.getExitCode()
					: currentExistingStatus.getExitCode();

			currentExistingStatus = new ExitStatus(exitCode, description);
			jobExecution.setExitStatus(currentExistingStatus);
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END ImportExchangeRatesListener()");
		}
	}

}
