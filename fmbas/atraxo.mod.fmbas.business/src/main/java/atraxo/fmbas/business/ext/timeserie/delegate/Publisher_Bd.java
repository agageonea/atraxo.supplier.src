package atraxo.fmbas.business.ext.timeserie.delegate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.timeserie.ITimeSerieAverageService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieItemService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.business.ext.abstracts.ModifiedAtComparator;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.business.ext.quotation.delegate.Quotation_Bd;
import atraxo.fmbas.domain.ext.types.timeseries.TimeSeriesType;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.DataProvider;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesStatus;
import atraxo.fmbas.domain.impl.timeserie.CompositeTimeSeriesSource;
import atraxo.fmbas.domain.impl.timeserie.RawTimeSerieItem;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieAverage;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class Publisher_Bd extends AbstractBusinessDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(Publisher_Bd.class);

	private final class DataProviderComparator implements Comparator<TimeSerie> {
		@Override
		public int compare(TimeSerie o1, TimeSerie o2) {
			if (!o1.getDataProvider().equals(o2.getDataProvider())) {
				if (o1.getDataProvider().equals(DataProvider._CALCULATED_)) {
					return 1;
				} else if (o2.getDataProvider().equals(DataProvider._CALCULATED_)) {
					return -1;
				} else {
					return 0;
				}
			} else {
				return 0;
			}
		}
	}

	/**
	 * @param tsList
	 * @param notify
	 * @throws Exception
	 */
	public void publish(List<TimeSerie> tsList) throws BusinessException {
		ITimeSerieService tsService = (ITimeSerieService) this.findEntityService(TimeSerie.class);
		ITimeSerieAverageService tsAverageService = (ITimeSerieAverageService) this.findEntityService(TimeSerieAverage.class);
		Collections.sort(tsList, new DataProviderComparator());
		for (TimeSerie timeSerie : tsList) {
			LOG.info("Start publish time serie with name:" + timeSerie.getSerieName());
			Boolean hasActiveAverage = false;
			for (TimeSerieAverage tsAverage : tsAverageService.findByTserie(timeSerie)) {
				if (tsAverage.getActive()) {
					hasActiveAverage = true;
					break;
				}
			}

			if (hasActiveAverage) {
				if (TimeSeriesStatus._WORK_.equals(timeSerie.getStatus())) {
					this.internalPublish(tsService, timeSerie);
				}
			} else {
				throw new BusinessException(BusinessErrorCode.NO_AVERAGE_METHOD, BusinessErrorCode.NO_AVERAGE_METHOD.getErrMsg());
			}
		}

	}

	private void internalPublish(ITimeSerieService tsService, TimeSerie timeSerie) throws BusinessException {
		if (timeSerie.getDataProvider().equals(DataProvider._CALCULATED_)) {
			tsService.calculate(timeSerie);
		}

		List<RawTimeSerieItem> devTsiList = new ArrayList<>(timeSerie.getRawTimeserieItems());
		this.merge(devTsiList, timeSerie);

		timeSerie.setStatus(TimeSeriesStatus._PUBLISHED_);
		tsService.updateStatus(timeSerie, timeSerie.getStatus());
		this.updateAverages(timeSerie);
		if (!CollectionUtils.isEmpty(timeSerie.getSourceTimeSeries())) {
			this.calculateTimeSerie(timeSerie, tsService);
		}
	}

	private void calculateTimeSerie(TimeSerie timeSerie, ITimeSerieService tsService) {
		for (CompositeTimeSeriesSource ts : timeSerie.getSourceTimeSeries()) {
			try {
				tsService.calculate(ts.getComposite().getId());
			} catch (BusinessException e) {
				LOG.info("Can not calculate time serie:" + timeSerie.getSerieName(), e);
			}
		}
	}

	private void updateAverages(TimeSerie timeSerie) throws BusinessException {
		if (timeSerie.getSerieType().equalsIgnoreCase(TimeSeriesType.ENERGYQUOTATION.getCode())) {
			Quotation_Bd bd = this.getBusinessDelegate(Quotation_Bd.class);
			bd.updateAverages(timeSerie);
		} else if (timeSerie.getSerieType().equalsIgnoreCase(TimeSeriesType.EXCHANGERATE.getCode())) {
			ExchangeRate_Bd bd = this.getBusinessDelegate(ExchangeRate_Bd.class);
			bd.updateAverages(timeSerie);
		}

	}

	private void merge(List<RawTimeSerieItem> devTsiList, TimeSerie ts) throws BusinessException {
		List<TimeSerieItem> tsiList = new ArrayList<>(ts.getTimeserieItems());
		List<TimeSerieItem> insList = new ArrayList<>();
		List<TimeSerieItem> updList = new ArrayList<>();
		Comparator<AbstractEntity> comp = Collections.reverseOrder(new ModifiedAtComparator());
		Collections.sort(devTsiList, comp);
		Collections.sort(tsiList, comp);
		if (!tsiList.isEmpty()) {
			this.handleTimeSeriesItemsUpdate(devTsiList, ts, tsiList, insList, updList);
		} else {
			for (RawTimeSerieItem devTsi : devTsiList) {
				TimeSerieItem tsi = this.buildTimeSeriesItem(devTsi);
				insList.add(tsi);
				ts.addToTimeserieItems(tsi);
			}
		}
		ITimeSerieItemService tsiService = (ITimeSerieItemService) this.findEntityService(TimeSerieItem.class);
		if (tsiList.size() > devTsiList.size()) {
			this.deleteItems(devTsiList, tsiList, ts);
		}
		tsiService.insert(insList);
		tsiService.update(updList);
	}

	private void handleTimeSeriesItemsUpdate(List<RawTimeSerieItem> devTsiList, TimeSerie ts, List<TimeSerieItem> tsiList,
			List<TimeSerieItem> insList, List<TimeSerieItem> updList) {
		TimeSerieItem lastModifiedItem = tsiList.get(0);
		for (RawTimeSerieItem devTsi : devTsiList) {
			TimeSerieItem tsi = this.buildTimeSeriesItem(devTsi);
			if (tsi.getModifiedAt().after(lastModifiedItem.getModifiedAt())) {
				if (!tsiList.contains(tsi)) {
					insList.add(tsi);
					ts.addToTimeserieItems(tsi);
				} else {
					TimeSerieItem updItem = tsiList.get(tsiList.indexOf(tsi));
					updItem.setItemValue(tsi.getItemValue());
					updItem.setCalculated(tsi.getCalculated());
					updList.add(updItem);
				}
			} else {
				break;
			}
		}
	}

	private List<TimeSerieItem> deleteItems(List<RawTimeSerieItem> devTsiList, List<TimeSerieItem> tsiList, TimeSerie ts) {
		List<Object> delIdList = new ArrayList<>();
		List<TimeSerieItem> delList = new ArrayList<>();
		if (devTsiList.isEmpty()) {
			for (TimeSerieItem i : tsiList) {
				delIdList.add(i.getId());
				delList.add(i);
				ts.getTimeserieItems().remove(i);
			}
		} else {
			this.delTsiItems(devTsiList, tsiList, ts, delIdList, delList);
		}
		return delList;
	}

	private void delTsiItems(List<RawTimeSerieItem> devTsiList, List<TimeSerieItem> tsiList, TimeSerie ts, List<Object> delIdList,
			List<TimeSerieItem> delList) {

		tsiList.sort((o1, o2) -> o1.getItemDate().compareTo(o2.getItemDate()));
		devTsiList.sort((o1, o2) -> o1.getItemDate().compareTo(o2.getItemDate()));

		RawTimeSerieItem devItem = devTsiList.get(0);
		for (TimeSerieItem i : tsiList) {
			if (i.getItemDate().before(devItem.getItemDate())) {
				delIdList.add(i.getId());
				delList.add(i);
				ts.getTimeserieItems().remove(i);
			} else {
				break;
			}
		}
		devItem = devTsiList.get(devTsiList.size() - 1);
		Collections.reverse(tsiList);
		for (TimeSerieItem i : tsiList) {
			if (i.getItemDate().after(devItem.getItemDate())) {
				delIdList.add(i.getId());
				delList.add(i);
				ts.getTimeserieItems().remove(i);
			} else {
				break;
			}
		}
	}

	private TimeSerieItem buildTimeSeriesItem(RawTimeSerieItem devTsi) {
		TimeSerieItem tsi = new TimeSerieItem();
		tsi.setActive(devTsi.getActive());
		tsi.setCalculated(devTsi.getCalculated());
		tsi.setClientId(devTsi.getClientId());
		tsi.setCreatedAt(devTsi.getCreatedAt());
		tsi.setCreatedBy(devTsi.getCreatedBy());
		tsi.setEntityAlias(devTsi.getEntityAlias());
		tsi.setEntityFqn(devTsi.getEntityFqn());
		tsi.setItemDate(devTsi.getItemDate());
		tsi.setItemValue(devTsi.getItemValue());
		tsi.setModifiedAt(devTsi.getModifiedAt());
		tsi.setModifiedBy(devTsi.getModifiedBy());
		tsi.setRefid(devTsi.getRefid());
		tsi.setTransfered(devTsi.getTransfered());
		tsi.setTransferedItemUpdate(devTsi.getTransferedItemUpdate());
		tsi.setTserie(devTsi.getTserie());
		tsi.setUpdated(devTsi.getUpdated());
		tsi.setVersion(devTsi.getVersion());
		return tsi;
	}
}
