package atraxo.fmbas.business.ws;

import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import atraxo.fmbas.domain.ws.dto.WSTransportDataType;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;

/**
 * @author vhojda
 */
public interface ExportDataWS_Service {

	/**
	 * Exports the data contained in a <code>MSG</code>, given also a specific <code>WSTransportDataType</code>
	 *
	 * @param msg
	 * @param dataType
	 * @return a <code>WSExecutionResult</code> with the response
	 */
	public WSExecutionResult exportData(MSG msg, WSTransportDataType dataType);
}
