/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.timeserie;

import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link AverageMethod} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class AverageMethod_Service extends AbstractEntityService<AverageMethod> {

	/**
	 * Public constructor for AverageMethod_Service
	 */
	public AverageMethod_Service() {
		super();
	}

	/**
	 * Public constructor for AverageMethod_Service
	 * 
	 * @param em
	 */
	public AverageMethod_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<AverageMethod> getEntityClass() {
		return AverageMethod.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return AverageMethod
	 */
	public AverageMethod findByName(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(AverageMethod.NQ_FIND_BY_NAME,
							AverageMethod.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"AverageMethod", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"AverageMethod", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return AverageMethod
	 */
	public AverageMethod findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(AverageMethod.NQ_FIND_BY_BUSINESS,
							AverageMethod.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"AverageMethod", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"AverageMethod", "id"), nure);
		}
	}

}
