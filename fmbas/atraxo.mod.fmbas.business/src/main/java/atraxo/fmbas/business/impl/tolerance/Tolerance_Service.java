/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.tolerance;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Tolerance} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Tolerance_Service extends AbstractEntityService<Tolerance> {

	/**
	 * Public constructor for Tolerance_Service
	 */
	public Tolerance_Service() {
		super();
	}

	/**
	 * Public constructor for Tolerance_Service
	 * 
	 * @param em
	 */
	public Tolerance_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Tolerance> getEntityClass() {
		return Tolerance.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Tolerance
	 */
	public Tolerance findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Tolerance.NQ_FIND_BY_NAME,
							Tolerance.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Tolerance", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Tolerance", "name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Tolerance
	 */
	public Tolerance findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Tolerance.NQ_FIND_BY_BUSINESS,
							Tolerance.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Tolerance", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Tolerance", "id"), nure);
		}
	}

	/**
	 * Find by reference: refUnit
	 *
	 * @param refUnit
	 * @return List<Tolerance>
	 */
	public List<Tolerance> findByRefUnit(Unit refUnit) {
		return this.findByRefUnitId(refUnit.getId());
	}
	/**
	 * Find by ID of reference: refUnit.id
	 *
	 * @param refUnitId
	 * @return List<Tolerance>
	 */
	public List<Tolerance> findByRefUnitId(Integer refUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Tolerance e where e.clientId = :clientId and e.refUnit.id = :refUnitId",
						Tolerance.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("refUnitId", refUnitId).getResultList();
	}
	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<Tolerance>
	 */
	public List<Tolerance> findByCurrency(Currencies currency) {
		return this.findByCurrencyId(currency.getId());
	}
	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<Tolerance>
	 */
	public List<Tolerance> findByCurrencyId(Integer currencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Tolerance e where e.clientId = :clientId and e.currency.id = :currencyId",
						Tolerance.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currencyId", currencyId).getResultList();
	}
}
