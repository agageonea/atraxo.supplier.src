package atraxo.fmbas.business.ext.tolerance;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Date;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.tolerance.ToleranceException;
import atraxo.fmbas.business.ext.exceptions.tolerance.ToleranceLimitsException;
import atraxo.fmbas.business.ext.exceptions.tolerance.ToleranceTypeException;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.fmbas_type.TolLimits;
import atraxo.fmbas.domain.impl.fmbas_type.ToleranceType;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business methods for tolerance verification.
 *
 * @author zspeter
 */
public class ToleranceVerifier {

	private static final BigDecimal ONE_PERCENT = BigDecimal.valueOf(0.01);
	private UnitConverterService unitConverterService;
	private ExchangeRate_Bd exchangeRateBd;
	private final Density unitDensity;

	public ToleranceVerifier(UnitConverterService unitConverterService, ExchangeRate_Bd exchangeRateBd, Density unitDensity) {
		this.unitConverterService = unitConverterService;
		this.unitDensity = unitDensity;
		this.exchangeRateBd = exchangeRateBd;
	}

	/**
	 * @param newValue
	 * @param refValue
	 * @param fromUnit
	 * @param toUnit
	 * @param tolerance
	 * @param date
	 * @return
	 */
	public ToleranceResult checkTolerance(BigDecimal refValue, BigDecimal newValue, Unit fromUnit, Unit toUnit, Currencies fromCurrency,
			Currencies toCurrency, Tolerance tolerance, Date date) throws BusinessException {
		this.validateTolerance(refValue, newValue, tolerance);
		// TODO rename or remove
		// this.convertTolerance(tolerance, toUnit, toCurrency, date);
		BigDecimal convertedValue = this.convertValues(fromUnit, toUnit, fromCurrency, toCurrency, tolerance, newValue, date);
		BigDecimal diff = convertedValue.subtract(refValue);
		EffectiveLimits limits = this.getEffectiveLimits(tolerance, refValue);
		if ((convertedValue.compareTo(limits.getMin()) > 0) && (convertedValue.compareTo(limits.getMax()) < 0)) {
			return new ToleranceResult(ToleranceResult.ResultType.WITHIN, diff);
		} else if ((convertedValue.compareTo(limits.getMin()) == 0) || (convertedValue.compareTo(limits.getMax()) == 0)) {
			return new ToleranceResult(ToleranceResult.ResultType.MARGIN, diff);
		} else {
			return new ToleranceResult(ToleranceResult.ResultType.NOT_WITHIN, diff);
		}
	}

	public ToleranceResult checkTolerance(BigDecimal refValue, BigDecimal newValue, Currencies fromCurrency, Currencies toCurrency,
			Tolerance tolerance, Date date) throws BusinessException {
		return this.checkTolerance(refValue, newValue, null, null, fromCurrency, toCurrency, tolerance, date);
	}

	public ToleranceResult checkTolerance(BigDecimal refValue, BigDecimal newValue, Unit fromUnit, Unit toUnit, Tolerance tolerance, Date date)
			throws BusinessException {
		return this.checkTolerance(refValue, newValue, fromUnit, toUnit, null, null, tolerance, date);
	}

	public ToleranceResult checkTolerance(BigDecimal refValue, BigDecimal newValue, Tolerance tolerance) throws BusinessException {
		return this.checkTolerance(refValue, newValue, null, null, null, null, tolerance, null);
	}

	private void validateTolerance(BigDecimal refValue, BigDecimal newValue, Tolerance tolerance) throws ToleranceException, ToleranceTypeException {
		ToleranceValidator validator = new ToleranceValidator();
		validator.validateValues(newValue, refValue);
		validator.validateTolerance(tolerance);
	}

	public ToleranceResult checkLowerToleranceLimit(BigDecimal refValue, BigDecimal newValue, Unit fromUnit, Unit toUnit, Currencies fromCurrency,
			Currencies toCurrency, Tolerance tolerance, Date date) throws BusinessException {
		BigDecimal convertedValue = this.convertValues(fromUnit, toUnit, fromCurrency, toCurrency, tolerance, newValue, date);
		BigDecimal diff = convertedValue.subtract(refValue);
		EffectiveLimits limits = this.getEffectiveLimits(tolerance, refValue);
		if (convertedValue.compareTo(limits.getMin()) > 0) {
			return new ToleranceResult(ToleranceResult.ResultType.WITHIN, diff);
		} else if (convertedValue.compareTo(limits.getMin()) == 0) {
			return new ToleranceResult(ToleranceResult.ResultType.MARGIN, diff);
		} else {
			return new ToleranceResult(ToleranceResult.ResultType.NOT_WITHIN, diff);
		}

	}

	public ToleranceResult checkHigherToleranceLimit(BigDecimal refValue, BigDecimal newValue, Unit refUnit, Unit newUnit, Currencies fromCurrency,
			Currencies toCurrency, Tolerance tolerance, Date date) throws BusinessException {
		BigDecimal convertedValue = this.convertValues(newUnit, refUnit, fromCurrency, toCurrency, tolerance, newValue, date);
		BigDecimal diff = convertedValue.subtract(refValue);
		EffectiveLimits limits = this.getEffectiveLimits(tolerance, refValue);
		if (convertedValue.compareTo(limits.getMax()) < 0) {
			return new ToleranceResult(ToleranceResult.ResultType.WITHIN, diff);
		} else if (convertedValue.compareTo(limits.getMax()) == 0) {
			return new ToleranceResult(ToleranceResult.ResultType.MARGIN, diff);
		} else {
			return new ToleranceResult(ToleranceResult.ResultType.NOT_WITHIN, diff);
		}
	}

	private EffectiveLimits getEffectiveLimits(Tolerance tolerance, BigDecimal refValue) throws ToleranceLimitsException {
		TolLimits limitIndicator = tolerance.getTolLmtInd();
		EffectiveLimits limits = new EffectiveLimits();
		if (tolerance.getAbsDevDown() != null) {
			limits.setMin(refValue.subtract(tolerance.getAbsDevDown()));
		}
		if (tolerance.getAbsDevUp() != null) {
			limits.setMax(refValue.add(tolerance.getAbsDevUp()));
		}
		switch (limitIndicator) {
		case _NARROWER_:
			if (tolerance.getRelativeDevDown() != null) {
				BigDecimal tmp = refValue.subtract(
						tolerance.getRelativeDevDown().multiply(ONE_PERCENT, MathContext.DECIMAL64).multiply(refValue, MathContext.DECIMAL64));
				if (limits.getMin() == null || tmp.compareTo(limits.getMin()) > 0) {
					limits.setMin(tmp);
				}
			}
			if (tolerance.getRelativeDevUp() != null) {
				BigDecimal tmp = refValue
						.add(tolerance.getRelativeDevUp().multiply(ONE_PERCENT, MathContext.DECIMAL64).multiply(refValue, MathContext.DECIMAL64));
				if (limits.getMax() == null || tmp.compareTo(limits.getMax()) < 0) {
					limits.setMax(tmp);
				}
			}
			break;
		case _WIDER_:
			if (tolerance.getRelativeDevDown() != null) {
				BigDecimal tmp = refValue.subtract(
						tolerance.getRelativeDevDown().multiply(ONE_PERCENT, MathContext.DECIMAL64).multiply(refValue, MathContext.DECIMAL64));
				if (limits.getMin() == null || tmp.compareTo(limits.getMin()) < 0) {
					limits.setMin(tmp);
				}
			}
			if (tolerance.getRelativeDevUp() != null) {
				BigDecimal tmp = refValue
						.add(tolerance.getRelativeDevUp().multiply(ONE_PERCENT, MathContext.DECIMAL64).multiply(refValue, MathContext.DECIMAL64));
				if (limits.getMax() == null || tmp.compareTo(limits.getMax()) > 0) {
					limits.setMax(tmp);
				}
			}
			break;
		default:
		}
		return limits;
	}

	private BigDecimal convertValues(Unit fromUnit, Unit toUnit, Currencies fromCurrency, Currencies toCurrency, Tolerance tolerance,
			BigDecimal value, Date date) throws BusinessException {
		ToleranceType type = tolerance.getToleranceType();
		BigDecimal convertedValue;
		switch (type) {
		case _COST_:
			convertedValue = this.exchangeRateBd.convert(fromCurrency, toCurrency, date, value, false).getValue();
			break;
		case _PRICE_:
			convertedValue = this.exchangeRateBd.convert(fromCurrency, toCurrency, date, value, false).getValue();
			convertedValue = this.unitConverterService.convert(fromUnit, toUnit, convertedValue, this.unitDensity);
			break;
		case _QUANTITY_:
			convertedValue = this.unitConverterService.convert(fromUnit, toUnit, value, this.unitDensity);
			break;
		case _DIMENSIONLESS_NUMBER_:
			convertedValue = value;
			break;
		default:
			throw new ToleranceException(BusinessErrorCode.INVALID_TOLERANCE_TYPE, "Tolerance with type " + type.toString() + " cannot be converted");
		}
		return convertedValue;
	}
}
