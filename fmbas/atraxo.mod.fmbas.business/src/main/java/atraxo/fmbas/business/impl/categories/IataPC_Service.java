/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.categories;

import atraxo.fmbas.business.api.categories.IIataPCService;
import atraxo.fmbas.domain.impl.categories.IataPC;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link IataPC} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class IataPC_Service extends AbstractEntityService<IataPC>
		implements
			IIataPCService {

	/**
	 * Public constructor for IataPC_Service
	 */
	public IataPC_Service() {
		super();
	}

	/**
	 * Public constructor for IataPC_Service
	 * 
	 * @param em
	 */
	public IataPC_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<IataPC> getEntityClass() {
		return IataPC.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return IataPC
	 */
	public IataPC findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(IataPC.NQ_FIND_BY_CODE, IataPC.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"IataPC", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"IataPC", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return IataPC
	 */
	public IataPC findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(IataPC.NQ_FIND_BY_BUSINESS, IataPC.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"IataPC", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"IataPC", "id"), nure);
		}
	}

}
