/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.soneAdvancedFilter;

import atraxo.fmbas.domain.impl.soneAdvancedFilter.SoneAdvancedFilter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link SoneAdvancedFilter} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class SoneAdvancedFilter_Service
		extends
			AbstractEntityService<SoneAdvancedFilter> {

	/**
	 * Public constructor for SoneAdvancedFilter_Service
	 */
	public SoneAdvancedFilter_Service() {
		super();
	}

	/**
	 * Public constructor for SoneAdvancedFilter_Service
	 * 
	 * @param em
	 */
	public SoneAdvancedFilter_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<SoneAdvancedFilter> getEntityClass() {
		return SoneAdvancedFilter.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return SoneAdvancedFilter
	 */
	public SoneAdvancedFilter findById(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(SoneAdvancedFilter.NQ_FIND_BY_ID,
							SoneAdvancedFilter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"SoneAdvancedFilter", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"SoneAdvancedFilter", "id"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return SoneAdvancedFilter
	 */
	public SoneAdvancedFilter findByName_cmp(String name, String cmp) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(SoneAdvancedFilter.NQ_FIND_BY_NAME_CMP,
							SoneAdvancedFilter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).setParameter("cmp", cmp)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"SoneAdvancedFilter", "name, cmp"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"SoneAdvancedFilter", "name, cmp"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return SoneAdvancedFilter
	 */
	public SoneAdvancedFilter findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(SoneAdvancedFilter.NQ_FIND_BY_BUSINESS,
							SoneAdvancedFilter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"SoneAdvancedFilter", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"SoneAdvancedFilter", "id"), nure);
		}
	}

}
