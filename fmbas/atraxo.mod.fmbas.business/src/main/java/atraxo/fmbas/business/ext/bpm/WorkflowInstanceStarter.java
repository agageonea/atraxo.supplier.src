package atraxo.fmbas.business.ext.bpm;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.activiti.engine.runtime.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.ext.bpm.WorkflowStartResult;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceService;
import atraxo.fmbas.business.api.workflow.IWorkflowNotificationService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowInstanceStatus;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.bpm.ActivitiBpmService;

/**
 * WorkflowInstanceStarter is used to start each WorkflowInstance on different thread.
 *
 * @author abolindu
 */
public class WorkflowInstanceStarter implements Callable<WorkflowStartResult> {

	private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowInstanceStarter.class);

	private Workflow wkf;
	private WorkflowInstance wkfInstance;
	private Map<String, Object> variables;

	private ActivitiBpmService activitiBpmService;

	private IWorkflowInstanceService workflowInstanceService;

	private IWorkflowNotificationService workflowNotificationService;

	public WorkflowInstanceStarter(Workflow wkf, WorkflowInstance wkfInstance, Map<String, Object> variables, ActivitiBpmService activitiBpmService,
			IWorkflowInstanceService workflowInstanceService, IWorkflowNotificationService workflowNotificationService) {
		this.wkf = wkf;
		this.wkfInstance = wkfInstance;
		this.variables = variables;
		this.activitiBpmService = activitiBpmService;
		this.workflowInstanceService = workflowInstanceService;
		this.workflowNotificationService = workflowNotificationService;
	}

	/**
	 * Starts the workflow by starting a separate <code>Thread</code> that initiates the starting actions.
	 *
	 * @return
	 * @throws BusinessException
	 */
	public WorkflowStartResult start() throws BusinessException {
		WorkflowStartResult result = new WorkflowStartResult();

		ExecutorService srv = Executors.newSingleThreadExecutor();
		Future<WorkflowStartResult> future = srv.submit(this);

		try {
			result = future.get(2, TimeUnit.MINUTES);
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			String reason = BusinessErrorCode.THREAD_START_NO_RESOURCES.getErrMsg() + ":" + e.getLocalizedMessage();
			result.setReason(reason);
			LOGGER.error(reason, e);
			srv.shutdown();
			this.failWorkflowInstance(reason);
		} finally {
			srv.shutdown();
		}

		return result;
	}

	@Override
	public WorkflowStartResult call() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START call()");
		}
		WorkflowStartResult result = new WorkflowStartResult();

		// try starting the Activiti process
		try {
			ProcessInstance instance = this.activitiBpmService.getRuntimeService().startProcessInstanceByKeyAndTenantId(this.wkf.getProcessKey(),
					this.wkfInstance.getId() + "", this.variables, this.wkf.getClientId());
			result.setStarted(true);
			LOGGER.info("STARTED WORKFLOW with instance ID " + instance.getId());
		} catch (Exception e) {
			LOGGER.error("ERROR:could not start workflow!", e);
			result.setReason("Could not start Activiti process instance with the given key " + this.wkf.getProcessKey() + " ["
					+ e.getLocalizedMessage() + "]");

			this.failWorkflowInstance(e.getLocalizedMessage());
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END call()");
		}
		return result;
	}

	/**
	 * Sets the workflow instance in a fail state, because of specified reason.
	 *
	 * @param reason
	 * @throws BusinessException
	 */
	private void failWorkflowInstance(String reason) throws BusinessException {
		// this means the process could not be started
		this.wkfInstance = this.workflowInstanceService.findById(this.wkfInstance.getId());
		this.wkfInstance.setStatus(WorkflowInstanceStatus._FAILURE_);
		this.wkfInstance.setResult(reason);
		this.workflowInstanceService.update(this.wkfInstance);

		// send failure notification
		this.workflowNotificationService.sendNotification(this.wkfInstance, WorkflowInstanceStatus._FAILURE_,
				WorkflowMsgConstants.WKF_RESULT_APPROVE_FAILED);
	}
}
