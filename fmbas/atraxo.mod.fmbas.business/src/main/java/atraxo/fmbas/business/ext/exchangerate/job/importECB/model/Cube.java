package atraxo.fmbas.business.ext.exchangerate.job.importECB.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author zspeter
 */
@XmlRootElement(name = "Cube", namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref")
public class Cube {

	private List<Cube> cube;
	private Date time;
	private String currency;
	private BigDecimal rate;

	@XmlElement(name = "Cube", required = false, namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref")
	public List<Cube> getCube() {
		return this.cube;
	}

	public void setCube(List<Cube> cube) {
		this.cube = cube;
	}

	@XmlAttribute(name = "time", required = false)
	public Date getTime() {
		return this.time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@XmlAttribute(name = "currency", required = false)
	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@XmlAttribute(name = "rate", required = false)
	public BigDecimal getRate() {
		return this.rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

}
