package atraxo.fmbas.business.ext.abstracts;

import java.util.Comparator;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;

/**
 * Compare any abstract entities based on modified date.
 * 
 * @author zspeter
 */
public class ModifiedAtComparator implements Comparator<AbstractEntity> {

	@Override
	public int compare(AbstractEntity o1, AbstractEntity o2) {
		if (o1.getModifiedAt() == null || o2.getModifiedAt() == null) {
			return 0;
		}
		return o1.getModifiedAt().compareTo(o2.getModifiedAt());
	}
}
