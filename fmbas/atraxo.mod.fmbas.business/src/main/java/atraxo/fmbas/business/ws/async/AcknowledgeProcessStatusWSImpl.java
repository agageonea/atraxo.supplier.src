package atraxo.fmbas.business.ws.async;

import java.io.IOException;
import java.util.List;

import javax.jws.WebService;
import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.xml.sax.SAXException;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceMessageHistoryService;
import atraxo.fmbas.business.api.externalInterfaces.IIncommingMessageService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ws.async.model.StatusRequest;
import atraxo.fmbas.business.ws.async.model.StatusResponse;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import atraxo.fmbas.domain.impl.externalInterfaces.IncommingMessage;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
@WebService
public class AcknowledgeProcessStatusWSImpl implements AcknowledgeProcessStatusWS {
	private static final Logger LOG = LoggerFactory.getLogger(AcknowledgeProcessStatusWSImpl.class);
	@Autowired
	private IIncommingMessageService msgSrv;
	@Autowired
	private IExternalInterfaceMessageHistoryService historySrv;
	@Autowired
	private IUserSuppService userSrv;

	@Override
	public StatusResponse checkStatus(StatusRequest request) throws JAXBException, SAXException, IOException, BusinessException {
		StatusResponse response = new StatusResponse();
		response.setMessageId(request.getMessageId());

		try {
			this.userSrv.createSessionUser(request.getClientCode());
		} catch (Exception e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Cannot create session user for client " + request.getClientCode(), e);
			}
			response.setMessageStatus("Invalid client");
		}
		List<IncommingMessage> list = this.msgSrv.findByMessageId(request.getMessageId());
		if (!list.isEmpty()) {
			list.sort((o1, o2) -> o2.getCreatedAt().compareTo(o1.getCreatedAt()));
			IncommingMessage msg = list.get(0);
			response.setMessageStatus(msg.getStatus().getName());
		} else {
			List<ExternalInterfaceMessageHistory> historyList = this.historySrv.findByResponseMessageId(request.getMessageId());
			if (!historyList.isEmpty()) {
				historyList.sort((o1, o2) -> o2.getCreatedAt().compareTo(o1.getCreatedAt()));
				ExternalInterfaceMessageHistory history = historyList.get(0);
				response.setMessageStatus(history.getStatus().getName());
			} else {
				response.setMessageId("Not found");
			}
		}
		return response;
	}

}
