/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.dashboard.service;

import java.util.List;

import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.dashboard.IAssignedWidgetParametersService;
import atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link AssignedWidgetParameters} domain entity.
 */
public class AssignedWidgetParameters_Service extends atraxo.fmbas.business.impl.dashboard.AssignedWidgetParameters_Service
		implements IAssignedWidgetParametersService {

	private static final String THIS_YEAR = "This Year";
	private static final String PERIOD = "PERIOD";

	@Override
	public String getPeriod(List<AssignedWidgetParameters> assignedWidgetParameter) throws BusinessException {
		if (CollectionUtils.isEmpty(assignedWidgetParameter)) {
			return THIS_YEAR;
		}
		for (AssignedWidgetParameters wp : assignedWidgetParameter) {
			if (PERIOD.equalsIgnoreCase(wp.getWidgetParameters().getName())) {
				return wp.getValue() == null ? wp.getDefaultValue() : wp.getValue();
			}
		}
		return THIS_YEAR;
	}

	@Override
	public String getUnit(List<AssignedWidgetParameters> assignedWidgetParameter) throws BusinessException {
		if (CollectionUtils.isEmpty(assignedWidgetParameter)) {
			return "UG";
		}
		for (AssignedWidgetParameters wp : assignedWidgetParameter) {
			if ("UNIT OF MEASURE".equalsIgnoreCase(wp.getWidgetParameters().getName())) {
				return wp.getValue() == null ? wp.getDefaultValue() : wp.getValue();
			}
		}
		return "UG";
	}

}
