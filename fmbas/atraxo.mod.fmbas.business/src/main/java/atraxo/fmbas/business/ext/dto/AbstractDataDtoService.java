package atraxo.fmbas.business.ext.dto;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDataDto;

public abstract class AbstractDataDtoService<E, D extends AbstractDataDto> {

	public abstract void addCustomValues(E object, D dto);

}
