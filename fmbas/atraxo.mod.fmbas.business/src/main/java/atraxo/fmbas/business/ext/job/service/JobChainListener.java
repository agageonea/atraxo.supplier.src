package atraxo.fmbas.business.ext.job.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;

import atraxo.fmbas.business.api.job.IJobChainService;
import atraxo.fmbas.domain.impl.fmbas_type.JobRunResult;
import atraxo.fmbas.domain.impl.fmbas_type.JobStatus;
import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.JobChain;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.domain.batchjob.JobParameter;

/**
 * @author zspeter
 */
public class JobChainListener implements JobListener {
	private static final String MSG_PART = "Job : ";
	private static final Logger LOG = LoggerFactory.getLogger(JobChainListener.class);
	private static final String LISTENER_NAME = "SONE_JOBCHAIN_LISTENER";

	private IJobChainService srv;

	/**
	 * @param srv
	 */
	public JobChainListener(IJobChainService srv) {
		this.srv = srv;
	}

	@Override
	public String getName() {
		return LISTENER_NAME;
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext context) {
		String jobName = context.getJobDetail().getKey().toString();
		LOG.info(MSG_PART + jobName + " is going to start...");
		Object jobChainId = context.getJobDetail().getJobDataMap().get(JobParameter.JOBCHAIN.name());
		JobChain jobChain = this.srv.findById(jobChainId);
		jobChain.setLastRunTime(GregorianCalendar.getInstance().getTime());
		jobChain.setStatus(JobStatus._RUNNING_);
		try {
			this.srv.update(jobChain);
		} catch (BusinessException e) {
			LOG.error(e.getMessage(), e);
		}

	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext context) {
		String jobName = context.getJobDetail().getKey().toString();
		LOG.info("jobExecutionVetoed");
		LOG.info(MSG_PART + jobName + " isVetoed.");

	}

	@Override
	public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {

		String jobName = context.getJobDetail().getKey().toString();
		LOG.info(MSG_PART + jobName + " is finished...");
		Object jobChainId = context.getJobDetail().getJobDataMap().get(JobParameter.JOBCHAIN.name());
		Object actionId = context.getJobDetail().getJobDataMap().get(JobParameter.ACTION.name());
		JobChain jobChain = this.srv.findById(jobChainId);
		List<Action> list = new ArrayList<>(jobChain.getActions());
		for (Action a : list) {
			if (a.getId().equals(actionId) && a.getOrder() < list.size()) {
				return;
			}
		}
		jobChain.setStatus(JobStatus._READY_);

		jobChain.setLastRunResult(JobRunResult._SUCCESS_);

		if (jobChain.getNextRunTime() != null && context.getNextFireTime() != null
				&& (jobChain.getNextRunTime().before(Calendar.getInstance().getTime())
						|| jobChain.getNextRunTime().after(context.getNextFireTime()))) {
			jobChain.setNextRunTime(context.getNextFireTime());
		}
		if (context.getResult() instanceof JobExecution) {
			JobExecution jobExecution = (JobExecution) context.getResult();

			if (jobExecution.getExitStatus().getExitCode().equals(JobRunResult._COMPLETED___FAILED_.toString())) {
				jobChain.setLastRunResult(JobRunResult._COMPLETED___FAILED_);
			} else if (jobExecution.getExitStatus().getExitCode().equals(JobRunResult._COMPLETED___SUCCESSFUL_.toString())) {
				jobChain.setLastRunResult(JobRunResult._COMPLETED___SUCCESSFUL_);
			}

			for (Throwable t : jobExecution.getAllFailureExceptions()) {
				LOG.info("Exception thrown by: " + jobName + " Exception: " + t.getMessage());
				jobChain.setLastRunResult(JobRunResult._FAILURE_);
			}

		}

		try {
			this.srv.update(jobChain);
		} catch (BusinessException e) {
			LOG.error("Jobchain " + jobName + " cannot be finished", e);
		}
	}
}
