package atraxo.fmbas.business.ext.calculator;

import java.util.Comparator;
import java.util.List;

import seava.j4e.api.exceptions.BusinessException;
import atraxo.fmbas.business.api.timeserie.ITimeSerieItemService;
import atraxo.fmbas.business.ext.calculator.model.Result;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;

/**
 * Average methods calculators common interface.
 *
 * @author zspeter
 *
 */
public interface Calculator {

	/**
	 * Comparing time series by item date.
	 *
	 * @author zspeter
	 *
	 */
	final class TimeSerieItemDateComparator implements Comparator<TimeSerieItem> {
		@Override
		public int compare(TimeSerieItem o1, TimeSerieItem o2) {
			return o1.getItemDate().compareTo(o2.getItemDate());
		}
	}

	/**
	 * Calculate average values for the time series items
	 * <code>timeSerieItems</code>. The time series items from
	 * <code>inputList</code> are used to be merged with
	 * <code>timesSerieItems</code>. In practice are cases when input values are
	 * different from persisted values (ex. in case of delete).
	 *
	 * @param timeSerieItems
	 *            List of time series items.
	 * @param itemsForProvisioning
	 *            TODO
	 * @return A List of calculated average values.
	 * @throws BusinessException
	 *             In case of any business exception occurred.
	 */
	List<Result> calculate(List<TimeSerieItem> timeSerieItems, List<TimeSerieItem> itemsForProvisioning)
			throws BusinessException;

	/**
	 * Get time series items that will be used for average calculation.
	 *
	 * @param timeSerieItems
	 *            Introduced or modified time series items.
	 * @param service
	 *            Spring bean used to retrieve time series items.
	 * @return A list of time series items.
	 * @throws BusinessException
	 *             In case of any business exception occurred.
	 */
	List<TimeSerieItem> getItemsForCalculation(List<TimeSerieItem> timeSerieItems, ITimeSerieItemService service)
			throws BusinessException;

	/**
	 * Get time series items for calculated average provisioning flag
	 * determination.
	 *
	 * @param tsiList
	 * @param service
	 * @return
	 */
	List<TimeSerieItem> getItemsForProvisioning(List<TimeSerieItem> tsiList, ITimeSerieItemService service)
			throws BusinessException;

}
