/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.job;

import atraxo.fmbas.domain.impl.job.JobChain;
import atraxo.fmbas.domain.impl.job.Trigger;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import org.quartz.JobListener;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Trigger} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Trigger_Service extends AbstractEntityService<Trigger> {

	/**
	 * Public constructor for Trigger_Service
	 */
	public Trigger_Service() {
		super();
	}

	/**
	 * Public constructor for Trigger_Service
	 * 
	 * @param em
	 */
	public Trigger_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Trigger> getEntityClass() {
		return Trigger.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Trigger
	 */
	public Trigger findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Trigger.NQ_FIND_BY_BUSINESS,
							Trigger.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Trigger", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Trigger", "id"), nure);
		}
	}

	/**
	 * Find by reference: jobChain
	 *
	 * @param jobChain
	 * @return List<Trigger>
	 */
	public List<Trigger> findByJobChain(JobChain jobChain) {
		return this.findByJobChainId(jobChain.getId());
	}
	/**
	 * Find by ID of reference: jobChain.id
	 *
	 * @param jobChainId
	 * @return List<Trigger>
	 */
	public List<Trigger> findByJobChainId(Integer jobChainId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Trigger e where e.clientId = :clientId and e.jobChain.id = :jobChainId",
						Trigger.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("jobChainId", jobChainId).getResultList();
	}
}
