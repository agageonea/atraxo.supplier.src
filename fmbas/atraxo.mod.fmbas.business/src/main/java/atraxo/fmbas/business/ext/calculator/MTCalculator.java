package atraxo.fmbas.business.ext.calculator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import seava.j4e.api.exceptions.BusinessException;
import atraxo.fmbas.business.api.timeserie.ITimeSerieItemService;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;

/**
 * Specific methods for monthly trading average calculations.
 *
 * @author zspeter
 */
public class MTCalculator extends AbstractMonthlyCalculator implements Calculator {

	@Override
	public List<TimeSerieItem> getItemsForCalculation(List<TimeSerieItem> timeSerieItems, ITimeSerieItemService service) throws BusinessException {
		Collections.sort(timeSerieItems, new TimeSerieItemDateComparator());
		Set<TimeSerieItem> allTimeSeriesItems = new HashSet<TimeSerieItem>();
		for (TimeSerieItem timeSerieItem : timeSerieItems) {
			if (!timeSerieItem.getCalculated()) {
				allTimeSeriesItems.add(timeSerieItem);
			}
			// TimeSerieItem nextItem = service.findNextNotCalculatedItem(timeSerieItem);
			// if (nextItem == null) {
			// TimeSerieItem prevItem = service.findPrevNotCalculatedItem(timeSerieItem);
			// if (prevItem == null) {
			// allTimeSeriesItems.add(timeSerieItem);
			// } else {
			// allTimeSeriesItems.addAll(this.getItemsBetweenElementsWithCompleteMonths(prevItem, timeSerieItem,
			// service));
			// }
			// } else {
			// allTimeSeriesItems.addAll(this.getItemsBetweenElementsWithCompleteMonths(timeSerieItem, nextItem,
			// service));
			// }
		}
		return new ArrayList<TimeSerieItem>(allTimeSeriesItems);
	}

	/**
	 * Get time series items between last Monday before <code>fromItem</code> item date and first Sunday after <code>toItem</code>.
	 *
	 * @param fromItem
	 * @param toItem
	 * @param service
	 * @return
	 * @throws BusinessException
	 */
	private Collection<? extends TimeSerieItem> getItemsBetweenElementsWithCompleteMonths(TimeSerieItem fromItem, TimeSerieItem toItem,
			ITimeSerieItemService service) throws BusinessException {
		Date startDate = fromItem.getItemDate();
		Date endDate = toItem.getItemDate();
		Calendar cal = Calendar.getInstance();

		cal.setTime(startDate);
		this.changeCalToSpecifiedMonthDay(cal, 1);
		startDate = cal.getTime();

		cal.setTime(toItem.getItemDate());
		this.changeCalToSpecifiedMonthDay(cal, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		endDate = cal.getTime();
		return service.getAllElementsBetweenDatesWithMargins(startDate, endDate, toItem.getTserie().getId(), false);
	}

}
