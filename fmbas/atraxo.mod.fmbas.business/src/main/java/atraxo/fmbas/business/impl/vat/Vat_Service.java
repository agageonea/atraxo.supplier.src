/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.vat;

import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.vat.Vat;
import atraxo.fmbas.domain.impl.vat.VatRate;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Vat} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Vat_Service extends AbstractEntityService<Vat> {

	/**
	 * Public constructor for Vat_Service
	 */
	public Vat_Service() {
		super();
	}

	/**
	 * Public constructor for Vat_Service
	 * 
	 * @param em
	 */
	public Vat_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Vat> getEntityClass() {
		return Vat.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Vat
	 */
	public Vat findByCode(String code, Country country) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Vat.NQ_FIND_BY_CODE, Vat.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code)
					.setParameter("country", country).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(), "Vat",
							"code, country"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Vat", "code, country"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Vat
	 */
	public Vat findByCode(String code, Long countryId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Vat.NQ_FIND_BY_CODE_PRIMITIVE, Vat.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code)
					.setParameter("countryId", countryId).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(), "Vat",
							"code, countryId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Vat", "code, countryId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Vat
	 */
	public Vat findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Vat.NQ_FIND_BY_BUSINESS, Vat.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(), "Vat",
							"id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Vat", "id"), nure);
		}
	}

	/**
	 * Find by reference: country
	 *
	 * @param country
	 * @return List<Vat>
	 */
	public List<Vat> findByCountry(Country country) {
		return this.findByCountryId(country.getId());
	}
	/**
	 * Find by ID of reference: country.id
	 *
	 * @param countryId
	 * @return List<Vat>
	 */
	public List<Vat> findByCountryId(Integer countryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Vat e where e.clientId = :clientId and e.country.id = :countryId",
						Vat.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("countryId", countryId).getResultList();
	}
	/**
	 * Find by reference: vatRate
	 *
	 * @param vatRate
	 * @return List<Vat>
	 */
	public List<Vat> findByVatRate(VatRate vatRate) {
		return this.findByVatRateId(vatRate.getId());
	}
	/**
	 * Find by ID of reference: vatRate.id
	 *
	 * @param vatRateId
	 * @return List<Vat>
	 */
	public List<Vat> findByVatRateId(Integer vatRateId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Vat e, IN (e.vatRate) c where e.clientId = :clientId and c.id = :vatRateId",
						Vat.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("vatRateId", vatRateId).getResultList();
	}
}
