package atraxo.fmbas.business.ext.timeserie.job.publishTimeSeries;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;

public class PublishTimeSerieReader implements ItemReader<TimeSerie> {

	private static final Logger LOG = LoggerFactory.getLogger(PublishTimeSerieReader.class);
	private static final String IDS = "ids";
	private static final String EXEXCUTIN_EXIT_CODE = "EXECUTING";

	@Autowired
	private ITimeSerieService service;

	private StepExecution stepExecution;
	private ExecutionContext executionContext;

	@BeforeStep
	public void storeExecutionContext(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
		this.executionContext = stepExecution.getExecutionContext();
	}

	@SuppressWarnings("unchecked")
	@Override
	public TimeSerie read() throws Exception {
		LOG.info("Started reading time serie");
		List<Integer> ids = this.executionContext.containsKey(IDS) ? (ArrayList<Integer>) this.executionContext.get(IDS) : new ArrayList<>();
		TimeSerie timeSerie = this.service.getTimeSerie(ids);
		if (timeSerie != null) {

			StringBuilder sb = new StringBuilder();
			sb.append(this.getStepExecutionExitCode());
			sb.append(timeSerie.getSerieName()).append(" ");
			ids.add(timeSerie.getId());

			this.stepExecution.setExitStatus(new ExitStatus(sb.toString(), sb.toString()));

			if (!this.executionContext.containsKey(IDS)) {
				this.executionContext.put(IDS, ids);
			}
		}
		return timeSerie;
	}

	private String getStepExecutionExitCode() {
		StringBuilder sb = new StringBuilder();
		if (this.stepExecution.getExitStatus() != null && !EXEXCUTIN_EXIT_CODE.equalsIgnoreCase(this.stepExecution.getExitStatus().getExitCode())) {
			String statusStr = this.stepExecution.getExitStatus().getExitCode();
			if (statusStr.length() > 1800) {
				if (statusStr.indexOf("\\|") != -1) {
					statusStr = statusStr.substring(statusStr.indexOf("\\|"));
				} else {
					statusStr = "";
				}
			}
			sb.append(statusStr).append(" | ");
		}
		sb.append("R:");
		return sb.toString();
	}

}
