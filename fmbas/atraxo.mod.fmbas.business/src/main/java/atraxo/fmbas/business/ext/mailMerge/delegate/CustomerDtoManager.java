package atraxo.fmbas.business.ext.mailMerge.delegate;

import java.io.IOException;
import java.text.SimpleDateFormat;

import org.codehaus.jackson.map.ObjectMapper;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.ext.mailmerge.dto.CustomerDto;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public class CustomerDtoManager {

	/**
	 * Generate Only the data without the header.
	 *
	 * @param customer
	 * @return
	 * @throws BusinessException
	 */

	private CustomerDtoManager() {
		// private constructor
	}

	/**
	 * @param customer
	 * @param dateFormat
	 * @return
	 * @throws BusinessException
	 */
	public static String getCustomerContractJsonData(CustomerDto customer, String dateFormat) throws BusinessException {
		ObjectMapper mapper = new ObjectMapper();
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		mapper.setDateFormat(sdf);
		try {
			return mapper.writeValueAsString(customer);
		} catch (IOException e) {
			throw new BusinessException(BusinessErrorCode.JSON_PARSE_ERROR, BusinessErrorCode.JSON_PARSE_ERROR.getErrMsg(), e);
		}
	}

}
