package atraxo.fmbas.business.ext.calculator;

import java.util.Calendar;
import java.util.Date;

/**
 * @author vhojda
 */
public abstract class AbstractFortnightCalculator extends AbstractCalculator implements Calculator {

	@Override
	protected Date calculateAveragePeriodStartDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		Calendar compareCal1 = (Calendar) cal.clone();
		compareCal1.set(Calendar.DAY_OF_MONTH, 11);

		Calendar compareCal2 = (Calendar) cal.clone();
		compareCal2.set(Calendar.DAY_OF_MONTH, 26);

		if (cal.before(compareCal1)) {
			cal.add(Calendar.MONTH, -1);
			cal.set(Calendar.DAY_OF_MONTH, 26);
		} else if (cal.before(compareCal2)) {
			cal.set(Calendar.DAY_OF_MONTH, 11);
		} else {
			cal.set(Calendar.DAY_OF_MONTH, 26);
		}

		return cal.getTime();
	}

	@Override
	protected Date calculateAveragePeriodEndDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		Calendar compareCal1 = (Calendar) cal.clone();
		compareCal1.set(Calendar.DAY_OF_MONTH, 11);

		Calendar compareCal2 = (Calendar) cal.clone();
		compareCal2.set(Calendar.DAY_OF_MONTH, 26);

		if (cal.before(compareCal1)) {
			cal.set(Calendar.DAY_OF_MONTH, 10);
		} else if (cal.before(compareCal2)) {
			cal.set(Calendar.DAY_OF_MONTH, 25);
		} else {
			cal.add(Calendar.MONTH, 1);
			cal.set(Calendar.DAY_OF_MONTH, 10);
		}

		return cal.getTime();
	}

	@Override
	protected Date getEndDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		Calendar compareCal1 = (Calendar) cal.clone();
		compareCal1.set(Calendar.DAY_OF_MONTH, 11);

		Calendar compareCal2 = (Calendar) cal.clone();
		compareCal2.set(Calendar.DAY_OF_MONTH, 26);

		if (cal.before(compareCal1)) {
			cal.set(Calendar.DAY_OF_MONTH, 10);
		} else if (cal.before(compareCal2)) {
			cal.set(Calendar.DAY_OF_MONTH, 25);
		} else {
			cal.add(Calendar.MONTH, 1);
			cal.set(Calendar.DAY_OF_MONTH, 10);
		}

		return cal.getTime();
	}
}
