package atraxo.fmbas.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;

public class ExchangeRateValueNotFoundException extends BusinessException {

	private static final long serialVersionUID = -2252342695598455498L;

	public ExchangeRateValueNotFoundException() {
		super(BusinessErrorCode.NO_EXCHANGE_RATE_VALUE, BusinessErrorCode.NO_EXCHANGE_RATE_VALUE.getErrMsg());
	}

	public ExchangeRateValueNotFoundException(String msg, Throwable e) {
		super(BusinessErrorCode.NO_EXCHANGE_RATE_VALUE, msg, e);
	}

	public ExchangeRateValueNotFoundException(Throwable e) {
		super(BusinessErrorCode.NO_EXCHANGE_RATE_VALUE, BusinessErrorCode.NO_EXCHANGE_RATE_VALUE.getErrMsg(), e);
	}

	public ExchangeRateValueNotFoundException(String msg) {
		super(BusinessErrorCode.NO_EXCHANGE_RATE_VALUE, msg);
	}
}
