/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.user.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import atraxo.fmbas.business.api.user.IUserSubsidiaryService;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;

/**
 * Business extensions specific for {@link UserSubsidiary} domain entity.
 */
public class UserSubsidiary_Service extends atraxo.fmbas.business.impl.user.UserSubsidiary_Service implements IUserSubsidiaryService {

	private static final Logger LOG = LoggerFactory.getLogger(UserSubsidiary_Service.class);

	@Override
	@Transactional
	public void setDefaultSubsidiary(Integer id) throws BusinessException {
		UserSubsidiary us = this.findById(id);
		UserSubsidiary defaultUs = this.getDefaultSubsidiary(us.getUser());
		if (defaultUs == null || !us.getId().equals(defaultUs.getId())) {
			us.setMainSubsidiary(true);
			this.update(us);
			if (defaultUs != null) {
				defaultUs.setMainSubsidiary(false);
				this.update(defaultUs);
			}
		}
	}

	@Override
	public UserSubsidiary getDefaultSubsidiary(UserSupp user) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("mainSubsidiary", true);
		params.put("user", user);
		try {
			return this.findEntityByAttributes(params);
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw nre;
			}
			LOG.warn("Could not find a subsidiary, will return null !", nre);
			return null;
		}
	}

	@Override
	@Transactional
	public void removeSubsidiary(List<UserSubsidiary> usList) throws BusinessException {
		List<Object> ids = new ArrayList<>();
		for (UserSubsidiary us : usList) {
			ids.add(us.getId());
		}
		this.deleteByIds(ids);
	}

}
