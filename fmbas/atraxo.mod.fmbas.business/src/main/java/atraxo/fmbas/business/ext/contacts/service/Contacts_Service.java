/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.contacts.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atraxo.fmbas.business.api.contacts.IContactsService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.util.EmailUtil;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link Contacts} domain entity.
 */
public class Contacts_Service extends atraxo.fmbas.business.impl.contacts.Contacts_Service implements IContactsService {

	@Override
	protected void preUpdate(Contacts e) throws BusinessException {
		super.preUpdate(e);
		this.checkEmail(e);
	}

	@Override
	protected void postUpdate(Contacts e) throws BusinessException {
		super.preUpdate(e);
		this.updatePrimary(e);
	}

	@Override
	protected void preInsert(Contacts e) throws BusinessException {
		super.preInsert(e);
		this.checkEmail(e);
	}

	@Override
	protected void postInsert(Contacts e) throws BusinessException {
		super.preInsert(e);
		this.updatePrimary(e);
	}

	/**
	 * Check if the e-mail address in the input-list is valid
	 *
	 * @param list
	 * @throws BusinessException
	 */
	private void checkEmail(Contacts contact) throws BusinessException {
		if (contact.getEmail() != null && !contact.getEmail().matches("") && !EmailUtil.validateEmail(contact.getEmail())) {
			throw new BusinessException(BusinessErrorCode.CONTACT_EMAIL, BusinessErrorCode.CONTACT_EMAIL.getErrMsg());
		}
	}

	private void updatePrimary(Contacts e) throws BusinessException {
		if (!e.getIsPrimary()) {
			return;
		}
		Integer objectId = e.getObjectId();
		String objectType = e.getObjectType();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("objectId", objectId);
		params.put("objectType", objectType);
		params.put("isPrimary", true);
		List<Contacts> contacts = this.findEntitiesByAttributes(params);
		List<Contacts> updateList = new ArrayList<Contacts>();
		if (contacts != null) {
			for (Contacts contact : contacts) {
				if (!contact.getId().equals(e.getId())) {
					contact.setIsPrimary(false);
					updateList.add(contact);
				}
			}
		}
		this.update(updateList);
	}

	@Override
	public void deleteContact(Integer id, Class<?> c) throws BusinessException {
		List<Contacts> contacts = this.findByObjectIdObjectType(id, c.getSimpleName());
		List<Object> ids = new ArrayList<>();
		for (Contacts ch : contacts) {
			ids.add(ch.getId());
		}
		this.deleteByIds(ids);
	}

	@Override
	public void deleteByEntities(List<? extends AbstractEntity> entities, Class<?> c) throws BusinessException {
		for (AbstractEntity abstractEntity : entities) {
			this.deleteContact(abstractEntity.getId(), c);
		}
	}

	@Override
	public List<Contacts> findByObjectIdObjectType(Integer objectId, String objectType) throws BusinessException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("objectId", objectId);
		params.put("objectType", objectType);
		List<Contacts> contacts = this.findEntitiesByAttributes(params);
		return contacts;
	}

	// @Override
	// @Transactional
	// public void removeNotificationEvent(List<NotificationEvent> neList) throws BusinessException {
	// List<Object> ids = new ArrayList<>();
	// for (NotificationEvent ne : neList) {
	// ids.add(ne.getId());
	// }
	// this.deleteByIds(ids);
	// }

}
