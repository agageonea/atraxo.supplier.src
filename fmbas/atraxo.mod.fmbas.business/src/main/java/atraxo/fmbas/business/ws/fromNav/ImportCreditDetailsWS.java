package atraxo.fmbas.business.ws.fromNav;

import java.io.IOException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;

@WebService(targetNamespace = "http://fromNav.ws.business.fmbas.atraxo/", endpointInterface = "ImportCreditDetailsEndpoint")
public interface ImportCreditDetailsWS {

	/**
	 * @param request
	 * @return
	 * @throws JAXBException
	 * @throws SAXException
	 * @throws IOException
	 * @throws BusinessException
	 */
	@WebMethod(operationName = "importCreditDetails", action = "importCreditDetails")
	@WebResult(targetNamespace = "http://fromNav.ws.business.fmbas.atraxo/")
	MSG importCreditDetails(@WebParam(name = "request", targetNamespace = "http://fromNav.ws.business.fmbas.atraxo/") MSG request)
			throws JAXBException, SAXException, IOException, BusinessException;

}
