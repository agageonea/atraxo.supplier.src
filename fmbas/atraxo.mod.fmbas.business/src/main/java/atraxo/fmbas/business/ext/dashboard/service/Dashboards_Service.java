/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.dashboard.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.dashboard.IDashboardWidgetsService;
import atraxo.fmbas.business.api.dashboard.IDashboardsService;
import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;
import atraxo.fmbas.domain.impl.dashboard.Dashboards;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link Dashboards} domain entity.
 */
public class Dashboards_Service extends atraxo.fmbas.business.impl.dashboard.Dashboards_Service implements IDashboardsService {

	@Autowired
	IDashboardWidgetsService dashboardService;

	@Override
	protected void preInsert(Dashboards e) throws BusinessException {
		super.preInsert(e);
		this.updateDefaultDashboard(e);
	}

	@Override
	protected void preUpdate(Dashboards e) throws BusinessException {
		super.preUpdate(e);
		this.updateDefaultDashboard(e);
	}

	private void updateDefaultDashboard(Dashboards e) throws BusinessException {
		if (e.getIsDefault() == null || !e.getIsDefault()) {
			return;
		}

		StringBuilder sb = new StringBuilder();
		sb.append("select e from ").append(Dashboards.class.getSimpleName());
		sb.append(" e where e.clientId =:clientId and e.isDefault = :isDefault and e.createdBy = :createdBy");

		TypedQuery<Dashboards> query = this.getEntityManager().createQuery(sb.toString(), Dashboards.class).setParameter("clientId",
				Session.user.get().getClient().getId());
		query.setParameter("isDefault", e.getIsDefault());
		query.setParameter("createdBy", Session.user.get().getCode());

		List<Dashboards> dashboardList = query.getResultList();
		for (Dashboards dashboard : dashboardList) {
			dashboard.setIsDefault(false);
		}
		this.update(dashboardList);
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		List<Dashboards> dashboards = this.findByIds(ids, Dashboards.class);
		List<DashboardWidgets> dwList = new ArrayList<>();
		for (Dashboards dashboard : dashboards) {
			dwList.addAll(dashboard.getDashboardWidgets());
		}
		List<Object> dwDeleteIds = this.collectIds(dwList);
		if (!dwDeleteIds.isEmpty()) {
			this.dashboardService.deleteByIds(dwDeleteIds);
		}
	}

}
