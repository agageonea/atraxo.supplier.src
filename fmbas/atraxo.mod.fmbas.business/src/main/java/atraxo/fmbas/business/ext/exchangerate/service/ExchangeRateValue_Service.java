/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.exchangerate.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import atraxo.fmbas.business.api.exchangerate.IExchangeRateValueService;
import atraxo.fmbas.business.ext.exceptions.ExchangeRateValueNotFoundException;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;

/**
 * Business extensions specific for {@link ExchangeRateValue} domain entity.
 */
public class ExchangeRateValue_Service extends atraxo.fmbas.business.impl.exchangerate.ExchangeRateValue_Service implements IExchangeRateValueService {

	@Override
	public ExchangeRateValue getExchangeRateValuesByBusinessKey(ExchangeRateValue exchangeRateValue) {
		StringBuilder sbSQL = new StringBuilder();
		sbSQL.append("select e from ")
				.append(ExchangeRateValue.class.getSimpleName())
				.append(" e where e.clientId=:clientId and e.exchRate.id = :exchangeRateId and e.vldFrDtRef = :validFromDate and e.vldToDtRef = :validToDate");
		List<ExchangeRateValue> resultList = this.getEntityManager().createQuery(sbSQL.toString(), ExchangeRateValue.class)
				.setParameter("clientId", Session.user.get().getClient().getId())
				.setParameter("exchangeRateId", exchangeRateValue.getExchRate().getId())
				.setParameter("validFromDate", exchangeRateValue.getVldFrDtRef()).setParameter("validToDate", exchangeRateValue.getVldToDtRef())
				.getResultList();
		if (resultList.size() == 0) {
			return null;
		}
		return resultList.get(0);
	}

	@Override
	public ExchangeRateValue getExchangeRateValue(ExchangeRate exchangeRate, Date date, Boolean strict) throws BusinessException {
		ExchangeRateValue result = null;
		List<ExchangeRateValue> resultList = this.findByExchRate(exchangeRate);
		Date truncatedDate = DateUtils.truncate(date, Calendar.DAY_OF_MONTH);
		for (ExchangeRateValue exchangeRateValue : resultList) {
			if ((truncatedDate.compareTo(exchangeRateValue.getVldFrDtRef()) >= 0)
					&& (truncatedDate.compareTo(exchangeRateValue.getVldToDtRef()) <= 0)) {
				result = exchangeRateValue;
				break;
			}
		}
		if (result == null && !strict) {
			Collections.sort(resultList, new Comparator<ExchangeRateValue>() {
				// order result list by valid to date reference in descending
				// order
				@Override
				public int compare(ExchangeRateValue o1, ExchangeRateValue o2) {
					return o2.getVldToDtRef().compareTo(o1.getVldToDtRef());
				}
			});
			for (ExchangeRateValue exchangeRateValue : resultList) {
				if (exchangeRateValue.getVldToDtRef().before(date)) {
					result = exchangeRateValue;
					break;
				}
			}
		}
		if (result == null) {
			throw new ExchangeRateValueNotFoundException("Cannot find exchange rate value for exchange rate " + exchangeRate.getName()
					+ " and date: " + new SimpleDateFormat().format(truncatedDate));
		}
		return result;
	}
}
