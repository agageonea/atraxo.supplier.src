/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.abstracts;

import atraxo.fmbas.business.api.abstracts.IDummyTypeWithSubSidiaryService;
import atraxo.fmbas.domain.impl.abstracts.DummyTypeWithSubSidiary;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link DummyTypeWithSubSidiary} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class DummyTypeWithSubSidiary_Service
		extends
			AbstractEntityService<DummyTypeWithSubSidiary>
		implements
			IDummyTypeWithSubSidiaryService {

	/**
	 * Public constructor for DummyTypeWithSubSidiary_Service
	 */
	public DummyTypeWithSubSidiary_Service() {
		super();
	}

	/**
	 * Public constructor for DummyTypeWithSubSidiary_Service
	 * 
	 * @param em
	 */
	public DummyTypeWithSubSidiary_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<DummyTypeWithSubSidiary> getEntityClass() {
		return DummyTypeWithSubSidiary.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return DummyTypeWithSubSidiary
	 */
	public DummyTypeWithSubSidiary findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							DummyTypeWithSubSidiary.NQ_FIND_BY_BUSINESS,
							DummyTypeWithSubSidiary.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DummyTypeWithSubSidiary", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DummyTypeWithSubSidiary", "id"), nure);
		}
	}

}
