/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.workflow;

import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link WorkflowInstanceEntity} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class WorkflowInstanceEntity_Service
		extends
			AbstractEntityService<WorkflowInstanceEntity> {

	/**
	 * Public constructor for WorkflowInstanceEntity_Service
	 */
	public WorkflowInstanceEntity_Service() {
		super();
	}

	/**
	 * Public constructor for WorkflowInstanceEntity_Service
	 * 
	 * @param em
	 */
	public WorkflowInstanceEntity_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<WorkflowInstanceEntity> getEntityClass() {
		return WorkflowInstanceEntity.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return WorkflowInstanceEntity
	 */
	public WorkflowInstanceEntity findByBusinessKey(String name,
			Integer objectId, String objectType) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							WorkflowInstanceEntity.NQ_FIND_BY_BUSINESSKEY,
							WorkflowInstanceEntity.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name)
					.setParameter("objectId", objectId)
					.setParameter("objectType", objectType).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"WorkflowInstanceEntity",
							"name, objectId, objectType"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"WorkflowInstanceEntity",
							"name, objectId, objectType"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return WorkflowInstanceEntity
	 */
	public WorkflowInstanceEntity findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							WorkflowInstanceEntity.NQ_FIND_BY_BUSINESS,
							WorkflowInstanceEntity.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"WorkflowInstanceEntity", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"WorkflowInstanceEntity", "id"), nure);
		}
	}

	/**
	 * Find by reference: workflowInstance
	 *
	 * @param workflowInstance
	 * @return List<WorkflowInstanceEntity>
	 */
	public List<WorkflowInstanceEntity> findByWorkflowInstance(
			WorkflowInstance workflowInstance) {
		return this.findByWorkflowInstanceId(workflowInstance.getId());
	}
	/**
	 * Find by ID of reference: workflowInstance.id
	 *
	 * @param workflowInstanceId
	 * @return List<WorkflowInstanceEntity>
	 */
	public List<WorkflowInstanceEntity> findByWorkflowInstanceId(
			Integer workflowInstanceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from WorkflowInstanceEntity e where e.clientId = :clientId and e.workflowInstance.id = :workflowInstanceId",
						WorkflowInstanceEntity.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("workflowInstanceId", workflowInstanceId)
				.getResultList();
	}
}
