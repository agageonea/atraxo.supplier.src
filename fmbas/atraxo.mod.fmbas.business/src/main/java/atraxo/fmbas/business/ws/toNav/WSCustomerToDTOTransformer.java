/**
 *
 */
package atraxo.fmbas.business.ws.toNav;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import atraxo.fmbas.business.api.contacts.IContactsService;
import atraxo.fmbas.business.api.ws.WSModelToDTOTransformer;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.ws.dto.WSPayloadDataDTO;
import atraxo.fmbas.domain.ws.dto.WSTransportDataType;
import atraxo.fmbas.domain.ws.fuelPlusCustomerAccount.Any;
import atraxo.fmbas.domain.ws.fuelPlusCustomerAccount.Any.FuelPlusCustomerAccount;
import seava.j4e.business.service.AbstractBusinessBaseService;
import seava.j4e.business.utils.DateUtils;
import seava.j4e.commons.utils.XMLUtils;

/**
 * @author vhojda
 */
public class WSCustomerToDTOTransformer extends AbstractBusinessBaseService implements WSModelToDTOTransformer<Customer> {

	final static Logger logger = LoggerFactory.getLogger(WSCustomerToDTOTransformer.class);

	private static final String XML_CUSTOMERS_TAG = "FuelPlusCustomerAccounts";//$NON-NLS-1$

	private static final String DEFAULT_IATA_CODE = "999";
	private static final String DEFAULT_ICAO_CODE = "999";

	@Autowired
	private IContactsService contactsService;

	/**
	 *
	 */
	public WSCustomerToDTOTransformer() {

	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.api.ws.WSModelToDTOTransformer#transformModelToDTO(java.util.Collection, java.util.Map)
	 */
	@Override
	public WSPayloadDataDTO transformModelToDTO(Collection<Customer> modelEntities, Map<String, String> paramMap) throws JAXBException {
		if (logger.isDebugEnabled()) {
			logger.debug("START transformModelToDTO()");
		}

		WSPayloadDataDTO dto = new WSPayloadDataDTO();

		if (!modelEntities.isEmpty()) {
			if (modelEntities.size() == 1) {
				AbstractEntity entity = modelEntities.iterator().next();

				Customer customerModel = null;
				try {
					customerModel = (Customer) entity;
				} catch (ClassCastException e) {
					logger.error("ERROR: could not cast " + entity.getClass() + " to " + Customer.class + " !", e);
					throw e;
				}

				if (customerModel != null) {
					Any customerAccountAny = this.toCustomerAccount(customerModel);
					dto.setPayload(XMLUtils.toXML(customerAccountAny, customerAccountAny.getClass()));
				}

			} else {
				Element rootEl = XMLUtils.createRootElement(XML_CUSTOMERS_TAG);
				for (Customer entity : modelEntities) {
					Customer customerModel = null;
					try {
						customerModel = entity;
					} catch (ClassCastException e) {
						logger.error("ERROR: could not cast " + entity.getClass() + " to " + Customer.class + " !", e);
						throw e;
					}
					if (customerModel != null) {
						Any customerAccountAny = this.toCustomerAccount(customerModel);
						Element customerElement = (XMLUtils.toXML(customerAccountAny, customerAccountAny.getClass()));
						Node importedNode = rootEl.getOwnerDocument().importNode(customerElement, true);
						rootEl.appendChild(importedNode);
					}
				}
				dto.setPayload(rootEl);
			}
		}

		if (logger.isDebugEnabled()) {
			logger.debug("END transformModelToDTO()");
		}
		return dto;
	}

	/**
	 * @param customerModel
	 * @return
	 */
	private Any toCustomerAccount(Customer customerModel) {
		Any anyAccount = new Any();
		FuelPlusCustomerAccount account = new FuelPlusCustomerAccount();
		anyAccount.setFuelPlusCustomerAccount(account);

		FuelPlusCustomerAccount.Customer customer = new FuelPlusCustomerAccount.Customer();

		customer.setFPEntryNo(customerModel.getId() + "");
		customer.setCustomerAccountNumber(customerModel.getCode());
		customer.setErpNumber(customerModel.getAccountNumber());
		customer.setIATACode(!StringUtils.isEmpty(customerModel.getIataCode()) ? customerModel.getIataCode() : DEFAULT_IATA_CODE);
		customer.setICAOCode(!StringUtils.isEmpty(customerModel.getIcaoCode()) ? customerModel.getIcaoCode() : DEFAULT_ICAO_CODE);
		customer.setNewEntry(customerModel.getStatus() != null ? customerModel.getStatus().equals(CustomerStatus._PROSPECT_) + "" : "");
		customer.setName(customerModel.getName() != null ? customerModel.getName() : "");
		customer.setAddress(customerModel.getOfficeStreet() != null ? customerModel.getOfficeStreet() : "");
		customer.setCity(customerModel.getOfficeCity() != null ? customerModel.getOfficeCity() : "");
		customer.setPhoneNo(customerModel.getPhoneNo() != null ? customerModel.getPhoneNo() : "");
		customer.setCountryRegionCode(customerModel.getOfficeCountry() != null ? customerModel.getOfficeCountry().getCode() : "");
		customer.setCurrencyCode("");
		customer.setLocationCode("");
		customer.setFaxNo(customerModel.getFaxNo() != null ? customerModel.getFaxNo() : "");
		customer.setVATRegistrationNo(customerModel.getVatRegNumber() != null ? customerModel.getVatRegNumber() : "");
		customer.setCounty(customerModel.getOfficeState() != null ? customerModel.getOfficeState().getName() : "");
		customer.setHomePage(customerModel.getWebsite() != null ? customerModel.getWebsite() : "");
		customer.setSegment(customerModel.getNatureOfBusiness() != null ? customerModel.getNatureOfBusiness().getName() : "");
		customer.setIncometaxnumber(customerModel.getIncomeTaxNumber() != null ? customerModel.getIncomeTaxNumber() : "");
		customer.setTaxIdentificationNo(customerModel.getTaxIdNumber() != null ? customerModel.getTaxIdNumber() : "");
		customer.setCustomerSince(DateUtils.toXMLGregorianCalendar(customerModel.getDateOfInc()));
		customer.setLastDateModified(DateUtils.toXMLGregorianCalendar(customerModel.getModifiedAt()));
		customer.setModificationTime(DateUtils.toXMLGregorianCalendar(customerModel.getModifiedAt()));
		customer.setModificationBy(customerModel.getModifiedBy());

		Contacts primaryContact = this.getPrimaryContactForCustomer(customerModel);
		if (primaryContact != null) {
			customer.setContact(primaryContact.getFullNameField());
			customer.setEMail(primaryContact.getEmail());
		} else {
			customer.setContact("");
			customer.setEMail("");
		}

		customer.setNewEntry(Boolean.toString(StringUtils.isEmpty(customerModel.getAccountNumber())));

		account.setCustomer(customer);

		return anyAccount;
	}

	@Override
	public WSTransportDataType getTransportDataType() {
		return WSTransportDataType.CUSTOMERS;
	}

	/**
	 * @param cust
	 * @return
	 */
	private Contacts getPrimaryContactForCustomer(Customer cust) {
		Contacts primaryContact = null;

		Map<String, Object> params = new HashMap<>();
		params.put("objectId", cust.getId());
		params.put("objectType", Customer.class.getSimpleName());
		params.put("isPrimary", true);
		List<Contacts> contacts = this.contactsService.findEntitiesByAttributes(params);
		if (contacts != null && !contacts.isEmpty()) {
			primaryContact = contacts.get(0);// get the first one
		}
		return primaryContact;
	}

}
