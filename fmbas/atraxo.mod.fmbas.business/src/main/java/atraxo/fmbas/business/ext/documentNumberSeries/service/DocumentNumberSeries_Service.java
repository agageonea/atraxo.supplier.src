/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.documentNumberSeries.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import atraxo.fmbas.business.api.documentNumberSeries.IDocumentNumberSeriesService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.DocumentSeriesOutOfBoundException;
import atraxo.fmbas.domain.impl.documentNumberSeries.DocumentNumberSeries;
import atraxo.fmbas.domain.impl.fmbas_type.DocumentNumberSeriesType;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link DocumentNumberSeries} domain entity.
 */
public class DocumentNumberSeries_Service extends atraxo.fmbas.business.impl.documentNumberSeries.DocumentNumberSeries_Service
		implements IDocumentNumberSeriesService {

	private static final Logger LOG = LoggerFactory.getLogger(DocumentNumberSeries_Service.class);

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String getNextDocumentNumber(DocumentNumberSeriesType type) throws BusinessException {
		Map<String, Object> params = this.createParamsMap(type);
		try {
			DocumentNumberSeries serie = this.findEntityByAttributes(params);
			int currentDocumentNumber = serie.getCurrentIndex();
			if (currentDocumentNumber >= serie.getEndIndex()) {
				this.throwDocumentSeriesOutOfBoundException(type);
			}
			serie.setCurrentIndex(currentDocumentNumber + 1);
			this.update(serie);
			return serie.getPrefix() + currentDocumentNumber;
		} catch (BusinessException e) {
			throw e;
		} catch (Exception e) {
			return this.handleInvalidSeries(type, e);
		}
	}

	private Map<String, Object> createParamsMap(DocumentNumberSeriesType type) {
		Map<String, Object> params = new HashMap<>();
		params.put("enabled", true);
		params.put("type", type);
		return params;
	}

	private void throwDocumentSeriesOutOfBoundException(DocumentNumberSeriesType type) throws DocumentSeriesOutOfBoundException {
		throw new DocumentSeriesOutOfBoundException(BusinessErrorCode.DOCUMENT_NO_SERIES_OVERDUE,
				String.format(BusinessErrorCode.DOCUMENT_NO_SERIES_OVERDUE.getErrMsg(), type.getName()));
	}

	private String handleInvalidSeries(DocumentNumberSeriesType type, Exception e) throws BusinessException {
		LOG.warn("Document number series get next document ", e);

		switch (type) {

		case _INVOICE_: {
			throw new BusinessException(BusinessErrorCode.INVALID_INVOICE_SERIES, BusinessErrorCode.INVALID_INVOICE_SERIES.getErrMsg());
		}
		case _CREDIT_MEMO_: {
			throw new BusinessException(BusinessErrorCode.INVALID_CREDITMEMO_SERIES, BusinessErrorCode.INVALID_CREDITMEMO_SERIES.getErrMsg());
		}
		case _CUSTOMER_: {
			LOG.warn("There is no document number series defined for customer");
			return null;
		}
		default: {
			throw new BusinessException(BusinessErrorCode.INVALID_SERIES, BusinessErrorCode.INVALID_SERIES.getErrMsg());
		}
		}
	}

}
