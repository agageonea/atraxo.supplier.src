package atraxo.fmbas.business.ext.exceptions.tolerance;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.IErrorCode;

/**
 * Base exception for any exception related to tolerance calculation.
 *
 * @author zspeter
 *
 */
public class ToleranceException extends BusinessException {

	private static final long serialVersionUID = -1206128618533704436L;

	public ToleranceException(IErrorCode errorCode, String errorDetails) {
		super(errorCode, errorDetails);
	}

}
