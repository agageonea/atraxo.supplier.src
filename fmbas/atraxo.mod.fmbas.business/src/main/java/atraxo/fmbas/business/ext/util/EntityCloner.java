package atraxo.fmbas.business.ext.util;

import java.lang.reflect.Field;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.BeanUtils;

import atraxo.abstracts.domain.impl.tenant.AbstractAuditable;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;

public class EntityCloner {

	private EntityCloner() {
		// empty constructor
	}

	@SuppressWarnings("unchecked")
	public static <T extends AbstractEntity> T cloneEntity(T entity) {
		T retEntity = (T) BeanUtils.instantiate(entity.getClass());
		BeanUtils.copyProperties(entity, retEntity, getAbstractIgnoredFields(AbstractEntity.class.getDeclaredFields()));
		return retEntity;
	}

	@SuppressWarnings("unchecked")
	public static <T extends AbstractEntity> T cloneEntityWithAbstractFields(T entity) {
		T retEntity = (T) BeanUtils.instantiate(entity.getClass());
		BeanUtils.copyProperties(entity, retEntity);
		return retEntity;
	}

	public static <T> T cloneEntity(T entity) {
		@SuppressWarnings("unchecked")
		T retEntity = (T) BeanUtils.instantiate(entity.getClass());
		BeanUtils.copyProperties(entity, retEntity);
		return retEntity;
	}

	@SuppressWarnings("unchecked")
	public static <T extends AbstractEntity> T cloneEntity(T entity, String[] ignoreProperties) {
		T retEntity = (T) BeanUtils.instantiate(entity.getClass());
		return cloneEntity(entity, retEntity, ignoreProperties);
	}

	/**
	 * @param from - from entity
	 * @param to - to entity
	 * @param ignoreProperties - This fields will be ignored.
	 * @return
	 */
	public static <T extends AbstractEntity> T cloneEntity(T from, T to, List<Field> ignoreProperties) {
		String[] ignoreList = new String[ignoreProperties.size()];
		for (int i = 0; i < ignoreProperties.size(); ++i) {
			ignoreList[i] = ignoreProperties.get(i).getName();
		}
		String[] finalList = (String[]) ArrayUtils.addAll(ignoreList, getAbstractIgnoredFields(AbstractEntity.class.getDeclaredFields()));
		BeanUtils.copyProperties(from, to, finalList);
		return to;
	}

	public static <T extends AbstractEntity> T cloneEntity(T from, T to, String[] ignoreProperties) {
		String[] ignoreList = (String[]) ArrayUtils.addAll(ignoreProperties, getAbstractIgnoredFields(AbstractEntity.class.getDeclaredFields()));
		BeanUtils.copyProperties(from, to, ignoreList);
		return to;
	}

	@SuppressWarnings("unchecked")
	public static <T extends AbstractAuditable> T cloneEntity(T entity) {
		T retEntity = (T) BeanUtils.instantiate(entity.getClass());
		BeanUtils.copyProperties(entity, retEntity, getAbstractIgnoredFields(AbstractAuditable.class.getDeclaredFields()));
		return retEntity;
	}

	@SuppressWarnings("unchecked")
	public static <T extends AbstractAuditable> T cloneEntity(T entity, String[] ignoreProperties) {
		T retEntity = (T) BeanUtils.instantiate(entity.getClass());
		String[] ignoreList = (String[]) ArrayUtils.addAll(ignoreProperties, getAbstractIgnoredFields(AbstractAuditable.class.getDeclaredFields()));
		BeanUtils.copyProperties(entity, retEntity, ignoreList);
		return retEntity;
	}

	private static String[] getAbstractIgnoredFields(Field[] fields) {
		if (fields == null) {
			return new String[0];
		}
		String[] strArray = new String[fields.length];
		for (int i = 0; i < fields.length; ++i) {
			fields[i].setAccessible(true);
			strArray[i] = fields[i].getName();
		}
		return strArray;
	}
}
