package atraxo.fmbas.business.ext.mailMerge;

import java.io.IOException;
import java.text.SimpleDateFormat;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializerProvider;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.FormatDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.HeaderDto;
import seava.j4e.api.exceptions.BusinessException;

public class MailMergeManager {

	/**
	 * @param data
	 * @param fileExtension
	 * @param emailDto
	 * @param dateFormat
	 * @return
	 * @throws BusinessException
	 */
	public static String getDataJson(AbstractDto data, String fileExtension, EmailDto emailDto, String dateFormat) throws BusinessException {
		try {
			ObjectMapper mapper = new ObjectMapper();

			FormatDto format = new FormatDto();
			format.setFormat(fileExtension);

			HeaderDto header = new HeaderDto();
			header.setData(data);
			header.setEmail(emailDto);
			header.setHeader(format);

			if (dateFormat != null && !dateFormat.isEmpty()) {
				// see SimpleDateFormat documentation for date formating
				SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
				mapper.setDateFormat(sdf);
			}

			mapper.getSerializerProvider().setNullValueSerializer(new JsonSerializer<Object>() {
				@Override
				public void serialize(Object arg0, JsonGenerator arg1, SerializerProvider arg2) throws IOException {
					arg1.writeString("");
				}
			});

			return mapper.writeValueAsString(header);
		} catch (Exception e) {
			throw new BusinessException(BusinessErrorCode.JSON_PARSE_ERROR, BusinessErrorCode.JSON_PARSE_ERROR.getErrMsg(), e);
		}
	}
}
