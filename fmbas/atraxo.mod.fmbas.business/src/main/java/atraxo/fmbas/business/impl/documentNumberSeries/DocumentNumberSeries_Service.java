/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.documentNumberSeries;

import atraxo.fmbas.domain.impl.documentNumberSeries.DocumentNumberSeries;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link DocumentNumberSeries} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class DocumentNumberSeries_Service
		extends
			AbstractEntityService<DocumentNumberSeries> {

	/**
	 * Public constructor for DocumentNumberSeries_Service
	 */
	public DocumentNumberSeries_Service() {
		super();
	}

	/**
	 * Public constructor for DocumentNumberSeries_Service
	 * 
	 * @param em
	 */
	public DocumentNumberSeries_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<DocumentNumberSeries> getEntityClass() {
		return DocumentNumberSeries.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return DocumentNumberSeries
	 */
	public DocumentNumberSeries findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DocumentNumberSeries.NQ_FIND_BY_BUSINESS,
							DocumentNumberSeries.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DocumentNumberSeries", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DocumentNumberSeries", "id"), nure);
		}
	}

}
