package atraxo.fmbas.business.ext.subsidiary;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.ext.subsidiary.ISubsidiaryService;
import atraxo.fmbas.business.api.financialSources.IFinancialSourcesService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.timeserie.IAverageMethodService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

public class SubsidiaryService implements ISubsidiaryService {

	@Autowired
	private IUnitService unitService;
	@Autowired
	private ICurrenciesService currencyService;
	@Autowired
	private ISystemParameterService systemParamsService;
	@Autowired
	private IFinancialSourcesService financialSourceService;
	@Autowired
	private IAverageMethodService averageMethodService;
	@Autowired
	private ICustomerService customerService;

	@Override
	public Unit getDefaultVolumeUnit() throws BusinessException {
		Customer subsidiary = this.getActiveSubsidiary();

		Unit settlementUnit;
		if (subsidiary.getDefaultVolumeUnit() != null) {
			settlementUnit = subsidiary.getDefaultVolumeUnit();
		} else {
			settlementUnit = this.unitService.findByCode(this.systemParamsService.getSysVol());
		}
		return settlementUnit;
	}

	@Override
	public Currencies getDefaultCurrency() throws BusinessException {
		Customer subsidiary = this.getActiveSubsidiary();

		Currencies currency;
		if (subsidiary.getSubsidiaryCurrency() != null) {
			currency = subsidiary.getSubsidiaryCurrency();
		} else {
			currency = this.currencyService.findByCode(this.systemParamsService.getSysCurrency());
		}
		return currency;
	}

	@Override
	public FinancialSources getDefaultFinancialSource() throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("isStdFlg", Boolean.TRUE);
		params.put("active", Boolean.TRUE);

		return this.financialSourceService.findEntityByAttributes(params);

	}

	@Override
	public AverageMethod getDefaultAverageMethod() throws BusinessException {
		return this.averageMethodService.findByName(this.systemParamsService.getSysAverageMethod());
	}

	private Customer getActiveSubsidiary() {
		Map<String, Object> subsidiaryParams = new HashMap<>();
		subsidiaryParams.put("refid", Session.user.get().getClient().getActiveSubsidiaryId());

		return this.customerService.findEntityByAttributes(Customer.class, subsidiaryParams);
	}
}
