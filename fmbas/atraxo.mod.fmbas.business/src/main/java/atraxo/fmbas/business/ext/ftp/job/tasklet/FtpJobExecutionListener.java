package atraxo.fmbas.business.ext.ftp.job.tasklet;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.item.ExecutionContext;

public class FtpJobExecutionListener implements JobExecutionListener {

	@Override
	public void beforeJob(JobExecution jobExecution) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		ExecutionContext jobExecutionContext = jobExecution.getExecutionContext();
		jobExecutionContext.put("plattsInputFile", "file:d:/platts/dc2015072401.pga");

	}

}
