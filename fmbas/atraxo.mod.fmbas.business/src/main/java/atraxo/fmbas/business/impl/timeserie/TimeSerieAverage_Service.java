/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.timeserie;

import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieAverage;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link TimeSerieAverage} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class TimeSerieAverage_Service
		extends
			AbstractEntityService<TimeSerieAverage> {

	/**
	 * Public constructor for TimeSerieAverage_Service
	 */
	public TimeSerieAverage_Service() {
		super();
	}

	/**
	 * Public constructor for TimeSerieAverage_Service
	 * 
	 * @param em
	 */
	public TimeSerieAverage_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<TimeSerieAverage> getEntityClass() {
		return TimeSerieAverage.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return TimeSerieAverage
	 */
	public TimeSerieAverage findByTserieAvg(TimeSerie tserie,
			AverageMethod averagingMethod) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(TimeSerieAverage.NQ_FIND_BY_TSERIEAVG,
							TimeSerieAverage.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("tserie", tserie)
					.setParameter("averagingMethod", averagingMethod)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TimeSerieAverage", "tserie, averagingMethod"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TimeSerieAverage", "tserie, averagingMethod"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return TimeSerieAverage
	 */
	public TimeSerieAverage findByTserieAvg(Long tserieId,
			Long averagingMethodId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							TimeSerieAverage.NQ_FIND_BY_TSERIEAVG_PRIMITIVE,
							TimeSerieAverage.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("tserieId", tserieId)
					.setParameter("averagingMethodId", averagingMethodId)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TimeSerieAverage", "tserieId, averagingMethodId"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TimeSerieAverage", "tserieId, averagingMethodId"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return TimeSerieAverage
	 */
	public TimeSerieAverage findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(TimeSerieAverage.NQ_FIND_BY_BUSINESS,
							TimeSerieAverage.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TimeSerieAverage", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TimeSerieAverage", "id"), nure);
		}
	}

	/**
	 * Find by reference: tserie
	 *
	 * @param tserie
	 * @return List<TimeSerieAverage>
	 */
	public List<TimeSerieAverage> findByTserie(TimeSerie tserie) {
		return this.findByTserieId(tserie.getId());
	}
	/**
	 * Find by ID of reference: tserie.id
	 *
	 * @param tserieId
	 * @return List<TimeSerieAverage>
	 */
	public List<TimeSerieAverage> findByTserieId(Integer tserieId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TimeSerieAverage e where e.clientId = :clientId and e.tserie.id = :tserieId",
						TimeSerieAverage.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("tserieId", tserieId).getResultList();
	}
	/**
	 * Find by reference: averagingMethod
	 *
	 * @param averagingMethod
	 * @return List<TimeSerieAverage>
	 */
	public List<TimeSerieAverage> findByAveragingMethod(
			AverageMethod averagingMethod) {
		return this.findByAveragingMethodId(averagingMethod.getId());
	}
	/**
	 * Find by ID of reference: averagingMethod.id
	 *
	 * @param averagingMethodId
	 * @return List<TimeSerieAverage>
	 */
	public List<TimeSerieAverage> findByAveragingMethodId(
			Integer averagingMethodId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TimeSerieAverage e where e.clientId = :clientId and e.averagingMethod.id = :averagingMethodId",
						TimeSerieAverage.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("averagingMethodId", averagingMethodId)
				.getResultList();
	}
}
