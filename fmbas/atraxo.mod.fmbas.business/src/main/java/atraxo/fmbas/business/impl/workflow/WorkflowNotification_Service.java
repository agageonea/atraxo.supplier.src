/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.workflow;

import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowNotification;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link WorkflowNotification} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class WorkflowNotification_Service
		extends
			AbstractEntityService<WorkflowNotification> {

	/**
	 * Public constructor for WorkflowNotification_Service
	 */
	public WorkflowNotification_Service() {
		super();
	}

	/**
	 * Public constructor for WorkflowNotification_Service
	 * 
	 * @param em
	 */
	public WorkflowNotification_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<WorkflowNotification> getEntityClass() {
		return WorkflowNotification.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return WorkflowNotification
	 */
	public WorkflowNotification findByKey(Workflow workflow, UserSupp user) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(WorkflowNotification.NQ_FIND_BY_KEY,
							WorkflowNotification.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("workflow", workflow)
					.setParameter("user", user).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"WorkflowNotification", "workflow, user"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"WorkflowNotification", "workflow, user"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return WorkflowNotification
	 */
	public WorkflowNotification findByKey(Long workflowId, Long userId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							WorkflowNotification.NQ_FIND_BY_KEY_PRIMITIVE,
							WorkflowNotification.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("workflowId", workflowId)
					.setParameter("userId", userId).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"WorkflowNotification", "workflowId, userId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"WorkflowNotification", "workflowId, userId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return WorkflowNotification
	 */
	public WorkflowNotification findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(WorkflowNotification.NQ_FIND_BY_BUSINESS,
							WorkflowNotification.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"WorkflowNotification", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"WorkflowNotification", "id"), nure);
		}
	}

	/**
	 * Find by reference: workflow
	 *
	 * @param workflow
	 * @return List<WorkflowNotification>
	 */
	public List<WorkflowNotification> findByWorkflow(Workflow workflow) {
		return this.findByWorkflowId(workflow.getId());
	}
	/**
	 * Find by ID of reference: workflow.id
	 *
	 * @param workflowId
	 * @return List<WorkflowNotification>
	 */
	public List<WorkflowNotification> findByWorkflowId(Integer workflowId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from WorkflowNotification e where e.clientId = :clientId and e.workflow.id = :workflowId",
						WorkflowNotification.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("workflowId", workflowId).getResultList();
	}
	/**
	 * Find by reference: user
	 *
	 * @param user
	 * @return List<WorkflowNotification>
	 */
	public List<WorkflowNotification> findByUser(UserSupp user) {
		return this.findByUserId(user.getId());
	}
	/**
	 * Find by ID of reference: user.id
	 *
	 * @param userId
	 * @return List<WorkflowNotification>
	 */
	public List<WorkflowNotification> findByUserId(String userId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from WorkflowNotification e where e.clientId = :clientId and e.user.id = :userId",
						WorkflowNotification.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("userId", userId).getResultList();
	}
}
