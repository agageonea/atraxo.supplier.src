package atraxo.fmbas.business.ext.exchangerate.job.importBloomberg.model;

/**
 * @author vhojda
 */
public enum BloombergExchangeRatePricingFieldEnum {

	PRICING_FIELD_PREV_DAY("Closing price previous day", "PX_YEST_CLOSE", "reference", 1),
	PRICING_FIELD_HISTORY("90 days history", "PX_LAST", "history", 90);

	private String paramName;
	private String bloombergValue;
	private String urlWord;
	private int nrProcessedDays;

	/**
	 * @param paramName
	 * @param bloombergValue
	 * @param urlWord
	 */
	private BloombergExchangeRatePricingFieldEnum(String paramName, String bloombergValue, String urlWord, int nrProcessedDays) {
		this.paramName = paramName;
		this.bloombergValue = bloombergValue;
		this.urlWord = urlWord;
		this.nrProcessedDays = nrProcessedDays;
	}

	public static BloombergExchangeRatePricingFieldEnum getByValue(String value) {
		BloombergExchangeRatePricingFieldEnum param = null;
		for (BloombergExchangeRatePricingFieldEnum pn : values()) {
			if (pn.getParamName().equals(value)) {
				param = pn;
				break;
			}
		}
		return param;
	}

	public String getParamName() {
		return this.paramName;
	}

	public String getBloombergValue() {
		return this.bloombergValue;
	}

	public String getUrlWord() {
		return this.urlWord;
	}

	public int getNrProcessedDays() {
		return this.nrProcessedDays;
	}

}
