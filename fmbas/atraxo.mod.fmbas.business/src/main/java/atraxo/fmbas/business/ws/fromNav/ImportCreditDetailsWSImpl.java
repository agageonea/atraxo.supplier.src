/**
 *
 */
package atraxo.fmbas.business.ws.fromNav;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import atraxo.fmbas.business.api.creditLines.ICreditLinesService;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceParameterService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ws.ExternalInterfaceWSHelper;
import atraxo.fmbas.business.ws.GUIDMessageGenerator;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceHistory;
import atraxo.fmbas.domain.impl.fmbas_type.CreditType;
import atraxo.fmbas.domain.impl.fmbas_type.CreditUpdateRequestStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ExposureLastAction;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfacesHistoryStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ProcessStatus;
import atraxo.fmbas.domain.impl.fmbas_type.TransportStatus;
import atraxo.fmbas.domain.ws.commonResponse.Any;
import atraxo.fmbas.domain.ws.commonResponse.Any.Acknowledgement;
import atraxo.fmbas.domain.ws.commonResponse.Any.Acknowledgement.DataSets;
import atraxo.fmbas.domain.ws.commonResponse.Any.Acknowledgement.DataSets.DataSet;
import atraxo.fmbas.domain.ws.creditInformation.Any.CustomerCreditInformation.CreditDetails.CreditDetail;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import atraxo.fmbas.domain.ws.dto.WSExecutionType;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import atraxo.fmbas.domain.ws.messgeXSD.MSG.HeaderCommon;
import atraxo.fmbas.domain.ws.messgeXSD.MSG.Payload;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.business.utils.DateUtils;
import seava.j4e.commons.utils.XMLUtils;

/**
 * @author vhojda
 */
@WebService
public class ImportCreditDetailsWSImpl implements ImportCreditDetailsWS {

	private static final Logger LOGGER = LoggerFactory.getLogger(AcknowledgeExportCustomersWSImpl.class);

	private static final String DEFAULT_BLOCKED_VALUE_TRUE = "yes";// I hate this, but these are the specs
	private static final String DEFAULT_BLOCKED_VALUE_FALSE = "no";// I hate this, but these are the specs

	/**
	 * Generic Interfaces Beans
	 */
	@Autowired
	private IExternalInterfaceService externalInterfaceService;

	@Autowired
	private IExternalInterfaceParameterService externalInterfaceParamService;

	@Autowired
	private ExternalInterfaceHistoryFacade historyFacade;

	@Autowired
	private ICurrenciesService currencyService;

	@Autowired
	private IUserSuppService userSrv;

	@Autowired
	private ICustomerService customerService;

	@Autowired
	private ICreditLinesService crediLinesService;

	private ExternalInterface importCreditLinesInterface;

	private ExternalInterfaceWSHelper helper;

	/**
	 * Temporal MAP for holding the curencies
	 */
	private Map<String, Currencies> tempCurrenciesMap;

	/**
	 *
	 */
	public ImportCreditDetailsWSImpl() {

	}

	/**
	 * Initializez current bean data members
	 */
	@PostConstruct
	public void init() {
		this.helper = new ExternalInterfaceWSHelper(this.externalInterfaceParamService);
		this.tempCurrenciesMap = new HashMap<>();
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ws.fromNav.ImportCreditDetailsWS#importCreditDetails(atraxo.fmbas.domain.ws.MSG)
	 */

	/**
	 * @author aradu private class that encapsulates a currency object, a customer object, an amount object, a data set and an arrayList of errors
	 */
	private class ProcessedCreditData {

		private static final String ERROR_SEPARATOR = "|";//$NON-NLS-1$

		private Currencies currency;
		private Customer customer;
		private Integer amount;
		private Integer availability;
		private DataSet ackDataSet;
		private List<String> errors;

		/**
		 * Default constructor
		 */
		public ProcessedCreditData() {
			this.errors = new ArrayList<>();
		}

		/**
		 * @return return a boolean that indicates if the array contains any elements ( errors )
		 */
		public boolean hasErrors() {
			return !this.errors.isEmpty();
		}

		public DataSet getAckDataSet() {
			return this.ackDataSet;
		}

		/**
		 * @adds an error to the arrayList for debugging and info purposes
		 * @param error a string that indicates an error
		 */
		public void addError(String error) {
			this.errors.add(error);
		}

		public Currencies getCurrency() {
			return this.currency;
		}

		public void setCurrency(Currencies currency) {
			this.currency = currency;
		}

		public Customer getCustomer() {
			return this.customer;
		}

		public void setCustomer(Customer customer) {
			this.customer = customer;
		}

		public Integer getAmount() {
			return this.amount;
		}

		public void setAmount(Integer amount) {
			this.amount = amount;
		}

		public List getErrors() {
			return this.errors;
		}

		public void setAckDataSet(DataSet ackDataSet) {
			this.ackDataSet = ackDataSet;
		}

		/**
		 * Returns the errors in a text format
		 *
		 * @return
		 */
		public String getErrorsConcatenated() {
			StringBuilder errorsBuilder = new StringBuilder("");
			for (Iterator<String> errorIt = this.errors.iterator(); errorIt.hasNext();) {
				String error = errorIt.next();
				errorsBuilder.append(error);
				if (errorIt.hasNext()) {
					errorsBuilder.append(ERROR_SEPARATOR);
				}
			}
			return errorsBuilder.toString();
		}

		public Integer getAvailability() {
			return this.availability;
		}

		public void setAvailability(Integer availability) {
			this.availability = availability;
		}

	}

	@Override
	public MSG importCreditDetails(MSG request) throws JAXBException, SAXException, IOException, BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START importCreditDetails()");
		}
		MSG response = new MSG();

		HeaderCommon header = request.getHeaderCommon();
		response.setHeaderCommon(header);

		// start constructing the interface execution result
		WSExecutionResult execResult = new WSExecutionResult();
		execResult.setExecutionType(WSExecutionType.IMPORT_DATA);
		execResult.setSuccess(false);// assume false

		// first, try to set up the client Session (even if disabled)
		boolean foundErrors = false;
		String clientIdentifier = null;
		try {
			clientIdentifier = header.getSourceCompany();
			this.userSrv.createSessionUser(clientIdentifier);
		} catch (Exception e) {
			foundErrors = true;
			LOGGER.error("ERROR: could not process received WS Client ID !!", e);
			this.fillHeaderErrorData(String.format(BusinessErrorCode.INTERFACE_NO_CLIENT_IDENTIFIER.getErrMsg(), clientIdentifier, clientIdentifier),
					header, execResult, e);
		}

		if (!foundErrors) {
			// then try to find the interface
			try {
				this.importCreditLinesInterface = this.externalInterfaceService.findByName(ExtInterfaceNames.CUSTOMER_CREDIT_LINE_UPDATE.getValue());
			} catch (Exception e) {
				foundErrors = true;
				LOGGER.error("ERROR:could not locate an external interface with name " + ExtInterfaceNames.CUSTOMER_CREDIT_LINE_UPDATE.getValue(), e);
				this.fillHeaderErrorData(String.format(BusinessErrorCode.INTERFACE_NO_INTERFACE_WITH_NAME_AND_CLIENT_IDENTIFIER.getErrMsg(),
						ExtInterfaceNames.CUSTOMER_CREDIT_LINE_UPDATE.getValue(), clientIdentifier), header, execResult, e);
			}
		}

		// if no errors so far proceed with operations
		if (!foundErrors) {

			// if interface is active proceed with operations
			if (this.importCreditLinesInterface.getEnabled()) {

				// extract the CustomerCreditInformation from received XML
				Element creditLineElement = request.getPayload().getAny();
				atraxo.fmbas.domain.ws.creditInformation.Any creditLineInformationAny = null;
				try {
					creditLineInformationAny = (atraxo.fmbas.domain.ws.creditInformation.Any) XMLUtils.toObject(creditLineElement,
							atraxo.fmbas.domain.ws.creditInformation.Any.class);
				} catch (Exception e) {
					LOGGER.error("ERROR:could not extract CustomerCreditInformation from received XML !", e);
					this.fillHeaderErrorData(BusinessErrorCode.INTERFACE_IMPORT_CREDIT_NO_CREDIT.getErrMsg(), header, execResult, e);
				}

				// if Credit Line Information is received then proceed to parse it
				if (creditLineInformationAny != null) {

					// set the XML as received data
					String xmlMSG = XMLUtils.toString(request, MSG.class, true, true);
					execResult.setExecutionData(xmlMSG);

					// initialize the payload data sets
					DataSets dataSets = new DataSets();

					// extract data and update data accordingly
					if (creditLineInformationAny != null && creditLineInformationAny.getCustomerCreditInformation() != null
							&& creditLineInformationAny.getCustomerCreditInformation().getCreditDetails() != null
							&& creditLineInformationAny.getCustomerCreditInformation().getCreditDetails().getCreditDetail() != null
							&& !creditLineInformationAny.getCustomerCreditInformation().getCreditDetails().getCreditDetail().isEmpty()) {

						List<CreditDetail> creditDetails = creditLineInformationAny.getCustomerCreditInformation().getCreditDetails()
								.getCreditDetail();

						// iterate through the received customer credit lines
						for (CreditDetail credit : creditDetails) {

							// create a new ProcessedCreditData from a CreditDetail
							ProcessedCreditData creditData = new ProcessedCreditData();
							this.setCustomerObjectForCredit(credit, creditData);
							this.setCurrencyObjectForCredit(credit, creditData);
							this.setAmountObjectForCredit(credit, creditData);
							DataSet ackDataSet = this.generateAckDataSet(creditData, credit);
							creditData.setAckDataSet(ackDataSet);
							dataSets.getDataSet().add(ackDataSet);

							// update the customer with credit info (if no errors before)
							if (!creditData.hasErrors()) {

								// change the status of the Update Request to transmitted
								creditData.customer.setCreditUpdateRequestStatus(CreditUpdateRequestStatus._TRANSMITTED_);

								// block/unblock
								Boolean updated = this.processBlockedCustomer(credit, creditData);

								// update the customer if not updated so far
								if (!updated) {
									this.customerService.update(creditData.getCustomer());
								}

								// change credit lines
								CreditType creditType = this.getCreditTypeFromCredit(credit);

								// update/create existing/new credit lines
								CreditLines creditLines = this.getActiveCreditLinesForCustomer(creditData.customer, creditType);
								if (creditLines != null) {// update
									creditLines.setAmount(creditData.getAmount());
									creditLines.setCurrency(creditData.getCurrency());
									creditLines.setAvailability(creditData.getAvailability());

									this.crediLinesService.updateCreditTriggerExposure(creditLines, ExposureLastAction._RECONCILIATION_,
											ExternalInterfaceWSHelper.DEFAULT_SYSTEM_REQUEST_REASON);
								} else {// create
									CreditLines newCreditLines = this.createNewCreditLine(creditType, creditData.getAmount(),
											creditData.getAvailability(), creditData.getCustomer(), creditData.getCurrency());
									creditData.customer.getCreditLines().add(newCreditLines);

									this.crediLinesService.insertCreditTriggerExposure(newCreditLines, ExposureLastAction._RECONCILIATION_,
											ExternalInterfaceWSHelper.DEFAULT_SYSTEM_REQUEST_REASON);
								}

								// the result is a success if at least one result is OK
								execResult.setSuccess(true);
							} else {
								Customer customer = creditData.getCustomer();
								if (customer != null) {
									customer.setCreditUpdateRequestStatus(CreditUpdateRequestStatus._FAILED_);
									this.customerService.update(customer);
								}
							}

						}// end iterate

					} else {
						this.fillHeaderErrorData(BusinessErrorCode.INTERFACE_IMPORT_CREDIT_EMPTY_CREDIT_LINES.getErrMsg(), header, execResult, null);
					}

					// put the payload
					Any ackAny = new Any();
					Acknowledgement ack = new Acknowledgement();
					ackAny.setAcknowledgement(ack);
					ack.setAckType("");
					ack.setDataSets(dataSets);
					Payload payload = new Payload();
					payload.setAny(XMLUtils.toXML(ackAny, ackAny.getClass()));
					response.setPayload(payload);
					response.setStatusPayload("");

				}// received Credit Line Information

				// save history regardless what happened (except the case when I can't create a session client)
				this.saveHistory(execResult);

			} else {
				this.fillHeaderErrorData(BusinessErrorCode.INTERFACE_DISABLED.getErrMsg(), header, execResult, null);
			}

			// set up the header data
			this.fillHeaderMessageData(header, execResult);
		}

		// set the header statuses
		header.setTransportStatus(TransportStatus._PROCESSED_.getName());
		header.setProcessStatus(execResult.isSuccess() ? ProcessStatus._SUCCESS_.getName() : ProcessStatus._FAILURE_.getName());

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END importCreditDetails()");
		}
		return response;
	}

	/**
	 * Processed the blocked information from the credit detail data and blocks/unblocks/approves/blacklists the customer;
	 *
	 * @param credit
	 * @param creditData
	 * @return
	 * @throws BusinessException
	 */
	private Boolean processBlockedCustomer(CreditDetail credit, ProcessedCreditData creditData) throws BusinessException {
		boolean customerUpdated = false;
		// block/unblock customer, depending on received flags
		boolean includedInStopList = credit.isIncludedInStoplist();
		if (creditData.getCustomer().getStatus().equals(CustomerStatus._ACTIVE_)
				|| creditData.getCustomer().getStatus().equals(CustomerStatus._INACTIVE_)
				|| creditData.getCustomer().getStatus().equals(CustomerStatus._BLOCKED_)) {
			if (includedInStopList) {
				if (!creditData.getCustomer().getStatus().equals(CustomerStatus._BLOCKED_)) {
					this.customerService.blacklistCustomer(creditData.getCustomer(), ExternalInterfaceWSHelper.DEFAULT_SYSTEM_REQUEST_REASON);
					customerUpdated = true;
				}
			} else {
				boolean blocked = this.extractBlockedValueFromCredit(credit);
				if (blocked) {
					if (!creditData.getCustomer().getStatus().equals(CustomerStatus._INACTIVE_)) {
						this.customerService.deactivateCustomers(creditData.getCustomer(), ExternalInterfaceWSHelper.DEFAULT_SYSTEM_REQUEST_REASON);
						customerUpdated = true;
					}
				} else {
					if (!creditData.getCustomer().getStatus().equals(CustomerStatus._ACTIVE_)) {
						this.customerService.approveCustomer(creditData.getCustomer(), ExternalInterfaceWSHelper.DEFAULT_SYSTEM_REQUEST_REASON);
						customerUpdated = true;
					}
				}
			}
		}
		return customerUpdated;
	}

	/**
	 * @param credit
	 * @return
	 */
	private boolean extractBlockedValueFromCredit(CreditDetail credit) {
		boolean blocked = false;// default is FALSE
		if (credit.getBlocked().equalsIgnoreCase(DEFAULT_BLOCKED_VALUE_TRUE)) {
			blocked = true;
		} else if (credit.getBlocked().equalsIgnoreCase(DEFAULT_BLOCKED_VALUE_FALSE)) {
			blocked = false;
		} else {
			LOGGER.warn("WARNING:could not recognize blocked value from " + credit.getBlocked()
					+ "! Expected values are yes/no. Will use the default:  blocked = false !");
		}
		return blocked;
	}

	/**
	 * @param credit
	 * @return
	 */
	private CreditType getCreditTypeFromCredit(CreditDetail credit) {
		CreditType creditType = null;
		if (credit.getCreditAvailability() != 0 && credit.getCreditLimit() == 0) {
			creditType = CreditType._PREPAYMENT_;
		} else if (credit.getCreditAvailability() == 0 && credit.getCreditLimit() != 0) {
			creditType = CreditType._CREDIT_LINE_;
		} else if (credit.getCreditAvailability() != 0 && credit.getCreditLimit() != 0) {
			creditType = CreditType._CREDIT_LINE_;
		} else if (credit.getCreditAvailability() == 0 && credit.getCreditLimit() == 0) {
			creditType = CreditType._PREPAYMENT_;
		}
		return creditType;
	}

	/**
	 * @param credit
	 * @return
	 */
	private void setCurrencyObjectForCredit(CreditDetail credit, ProcessedCreditData pcd) {

		String currencyCode = credit.getCurrency();
		if (!StringUtils.isEmpty(currencyCode)) {
			try {
				if (this.tempCurrenciesMap.containsKey(currencyCode)) {
					pcd.currency = this.tempCurrenciesMap.get(currencyCode);
				} else {
					pcd.currency = this.currencyService.findByCode(currencyCode);
					this.tempCurrenciesMap.put(currencyCode, pcd.currency);
				}
			} catch (Exception e) {
				LOGGER.info("Received invalid currency code : could not locate a currency for code " + currencyCode + "! Will do nothing !", e);
				pcd.addError(String.format(BusinessErrorCode.INTERFACE_INVALID_CURRENCY.getErrMsg(), currencyCode));
			}
		} else {
			pcd.addError(String.format(BusinessErrorCode.INTERFACE_INVALID_CURRENCY.getErrMsg(), currencyCode));
		}

	}

	/**
	 * @param pcd
	 * @param credit
	 * @return
	 */
	private DataSet generateAckDataSet(ProcessedCreditData pcd, CreditDetail credit) {
		DataSet ackDataSet = new DataSet();
		ackDataSet.setLogEntryNo(ExternalInterfaceWSHelper.DEFAULT_ACK_LOG_ENTRY_NO);
		ackDataSet.setName(ExternalInterfaceWSHelper.DEFAULT_CREDIT_ACK_NAME);
		ackDataSet.setDateTimeStamp(DateUtils.toXMLGregorianCalendar(new Date()));

		// set customer
		if (pcd.getCustomer() != null) {
			Customer customer = pcd.getCustomer();
			ackDataSet.setCustomerAccountNumber(customer.getCode());
			ackDataSet.setErpNumber(customer.getAccountNumber());
			ackDataSet.setTransactionID(customer.getRefid());
		} else {
			ackDataSet.setCustomerAccountNumber(credit.getCustomerAccountNumber());
			ackDataSet.setErpNumber(credit.getErpNumber());
			ackDataSet.setTransactionID("");// default value for TransactionID
		}

		// set error code (concatenated)
		ackDataSet.setErrorCode(pcd.getErrorsConcatenated());

		ackDataSet.setDelivered(true);
		ackDataSet.setProcessed(!pcd.hasErrors());
		ackDataSet.setDescription(pcd.hasErrors() ? ExternalInterfaceWSHelper.DEFAULT_ACK_DESCRIPTION_SUCCESS : ackDataSet.getErrorCode());

		return ackDataSet;
	}

	/**
	 * @param errorMsg
	 * @param header
	 * @param execResult
	 * @param e
	 */
	private void fillHeaderErrorData(String errorMsg, HeaderCommon header, WSExecutionResult execResult, Exception e) {
		header.setErrorDescription(errorMsg);
		header.setProcessingErrorDescription(e != null ? e.getLocalizedMessage() : "");
		execResult.appendExecutionData(
				header.getErrorDescription() + (e != null ? header.getProcessingErrorDescription() + "::" + ExceptionUtils.getStackTrace(e) : ""));
	}

	/**
	 * Fills in the data for the response Message Header
	 *
	 * @param header
	 * @param result
	 */
	private void fillHeaderMessageData(HeaderCommon header, WSExecutionResult result) {
		// make sure to set up the MSG Ids (they depend on the session)
		header.setCorrelationID(header.getMsgID());
		header.setMsgID(GUIDMessageGenerator.generateMessageID());

		String interfaceType = this.helper.retrieveStringParameterValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_INTERFACE_TYPE, result,
				this.importCreditLinesInterface);
		header.setInterfaceType(interfaceType);

		String sourceGroupCompany = this.helper.retrieveStringParameterValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_SOURCE_GROUP_COMPANY, result,
				this.importCreditLinesInterface);
		header.setSourceGroupCompany(sourceGroupCompany);

		String destGroupComapany = this.helper.retrieveStringParameterValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_DESTINATION_GROUP_COMPANY,
				result, this.importCreditLinesInterface);
		header.setDestinationGroupCompany(destGroupComapany);

		String sourceCompany = this.helper.retrieveStringParameterValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_SOURCE_COMPANY, result,
				this.importCreditLinesInterface);
		header.setSourceCompany(sourceCompany);

		header.setUpdateSourceOnReceive(false);
		header.setUpdateDestinationOnDelivery(true);
		if (header.getProcessingErrorDescription() == null) {
			header.setProcessingErrorDescription("");
		}
		header.setDestinationUpdateStatusAddress("");
		header.setDestinationAddress("");
	}

	/**
	 * @param credit
	 * @return
	 */
	private void setAmountObjectForCredit(CreditDetail credit, ProcessedCreditData pcd) {
		try {
			int amount = Double.valueOf(credit.getCreditLimit()).intValue();
			pcd.setAmount(Integer.valueOf(amount));

			int availability = Double.valueOf(credit.getCreditAvailability()).intValue();
			pcd.setAvailability(Integer.valueOf(availability));
		} catch (Exception e) {
			LOGGER.error("ERROR:could not parse received amounts !", e);
			pcd.addError(BusinessErrorCode.INTERFACE_INVALID_AMOUNT.getErrMsg());
		}
	}

	/**
	 * @param creditTerm
	 * @param amount
	 * @param availability
	 * @param customer
	 * @param currency
	 * @return
	 */
	private CreditLines createNewCreditLine(CreditType creditTerm, Integer amount, Integer availability, Customer customer, Currencies currency) {
		CreditLines newCreditLines = new CreditLines();
		newCreditLines.setAlertPercentage(ExternalInterfaceWSHelper.DEFAUL_CREDIT_ALERT_PERCENTAGE);
		newCreditLines.setActive(Boolean.TRUE);
		newCreditLines.setAmount(amount);
		newCreditLines.setAvailability(availability);
		newCreditLines.setCreatedAt(new Date());
		newCreditLines.setCreditTerm(creditTerm);
		newCreditLines.setCurrency(currency);
		newCreditLines.setCustomer(customer);
		newCreditLines.setChangeReason(BusinessErrorCode.CREDIT_LINE_UPDATE.getErrMsg());
		Calendar cal = Calendar.getInstance();
		newCreditLines.setValidFrom(cal.getTime());
		cal.add(Calendar.YEAR, 100);
		newCreditLines.setValidTo(cal.getTime());
		return newCreditLines;
	}

	/**
	 * @param credit
	 * @return
	 */
	private void setCustomerObjectForCredit(CreditDetail credit, ProcessedCreditData pcd) {
		// check customer code
		String customerCode = credit.getCustomerAccountNumber();
		if (StringUtils.isEmpty(customerCode)) {
			pcd.addError(BusinessErrorCode.INTERFACE_IMPORT_CREDIT_EMPTY_ACCOUNT_NUMBER.getErrMsg());
		}

		// check account number
		String accountNumber = credit.getErpNumber();
		if (StringUtils.isEmpty(credit.getErpNumber())) {
			pcd.addError(BusinessErrorCode.INTERFACE_IMPORT_CREDIT_EMPTY_ERP_NUMBER.getErrMsg());
		}

		// find customer given the code
		Customer customer = null;
		try {
			customer = this.customerService.findByCode(customerCode);
			pcd.setCustomer(customer);
		} catch (Exception e) {
			LOGGER.warn("WARNING: could not find Customer for CODE " + customerCode, e);
			pcd.addError(String.format(BusinessErrorCode.INTERFACE_FIND_CUSTOMER_INVALID_CODE.getErrMsg(), customerCode));
		}

		if (customer != null && !accountNumber.equals(customer.getAccountNumber())) {
			pcd.addError(String.format(BusinessErrorCode.INTERFACE_CUSTOMERS_DIFFERENT_ACCOUNT_NUMBER.getErrMsg(), customerCode, accountNumber,
					customer.getAccountNumber()));
		}
	}

	/**
	 * Saves history
	 *
	 * @param result
	 * @param success
	 * @throws BusinessException
	 */
	private void saveHistory(WSExecutionResult result) throws BusinessException {
		ExternalInterfaceHistory history = new ExternalInterfaceHistory();
		history.setExternalInterface(this.importCreditLinesInterface);
		history.setRequester(Session.user.get().getClientCode());
		history.setRequestReceived(new Date());
		history.setResponseSent(new Date());
		history.setStatus(
				result.isSuccess() ? ExternalInterfacesHistoryStatus._SUCCESS_ : ExternalInterfacesHistoryStatus._FAILED_NEGATIVE_ACKNOWLEDGEMENT_);
		history.setResult(result.getTrimmedExecutionData());
		this.historyFacade.insert(history);
	}

	/**
	 * @param customer
	 * @param creditType
	 * @return
	 */
	private CreditLines getActiveCreditLinesForCustomer(Customer customer, CreditType creditType) {
		CreditLines returnedActiveCreditLine = null;
		for (CreditLines line : customer.getCreditLines()) {
			if (line.getActive() && line.getCreditTerm().equals(creditType)) {
				returnedActiveCreditLine = line;
				break;
			}
		}
		return returnedActiveCreditLine;
	}

}
