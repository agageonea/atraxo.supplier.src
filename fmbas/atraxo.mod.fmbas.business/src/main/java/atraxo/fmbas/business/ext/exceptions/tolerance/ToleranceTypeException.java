package atraxo.fmbas.business.ext.exceptions.tolerance;

import seava.j4e.api.exceptions.IErrorCode;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;

/**
 * Exception occurs on invalid tolerance type.
 *
 * @author zspeter
 *
 */
public class ToleranceTypeException extends ToleranceException {

	private static final long serialVersionUID = 4552139528983350683L;

	public ToleranceTypeException(IErrorCode errorCode, String errorDetails) {
		super(errorCode, errorDetails);
	}

	public ToleranceTypeException(String errorDetails) {
		super(BusinessErrorCode.INVALID_TOLERANCE_TYPE, errorDetails);
	}

}
