package atraxo.fmbas.business.ext.calculator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import atraxo.fmbas.business.api.timeserie.ITimeSerieItemService;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author vhojda
 */
public class FTCalculator extends AbstractFortnightCalculator implements Calculator {

	@Override
	public List<TimeSerieItem> getItemsForCalculation(List<TimeSerieItem> timeSerieItems, ITimeSerieItemService service) throws BusinessException {
		Collections.sort(timeSerieItems, new TimeSerieItemDateComparator());
		List<TimeSerieItem> allTimeSeriesItems = new ArrayList<TimeSerieItem>();
		for (TimeSerieItem timeSerieItem : timeSerieItems) {
			if (!timeSerieItem.getCalculated()) {
				allTimeSeriesItems.add(timeSerieItem);
			}
		}
		return allTimeSeriesItems;
	}

}
