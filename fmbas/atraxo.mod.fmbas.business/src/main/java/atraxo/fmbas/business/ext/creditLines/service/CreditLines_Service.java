/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.creditLines.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.creditLines.ICreditLinesService;
import atraxo.fmbas.business.api.exposure.IExposureService;
import atraxo.fmbas.business.ext.aop.history.Action;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.exposure.Exposure;
import atraxo.fmbas.domain.impl.fmbas_type.ExposureLastAction;
import seava.j4e.api.annotation.Reason;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;

/**
 * Business extensions specific for {@link CreditLines} domain entity.
 */
public class CreditLines_Service extends atraxo.fmbas.business.impl.creditLines.CreditLines_Service implements ICreditLinesService {

	private static final String DE_ACTIVATE = "De-activate";
	private static final String ACTIVATE = "Activate";
	private static final Logger LOG = LoggerFactory.getLogger(CreditLines_Service.class);

	@Override
	protected void preInsert(CreditLines e) throws BusinessException {
		super.preInsert(e);
		this.checkAvailability(e);
		this.checkValidity(e);
	}

	@Override
	protected void preUpdate(CreditLines e) throws BusinessException {
		super.preUpdate(e);
		this.checkValidity(e);
	}

	@Override
	@History(type = CreditLines.class, value = "Update")
	@Transactional
	public void update(List<CreditLines> list) throws BusinessException {
		super.update(list);
	}

	@Override
	protected void postUpdate(CreditLines e) throws BusinessException {
		super.postUpdate(e);
		this.deActivateOthers(e);
		this.manageExposure(e);
	}

	@Override
	protected void postInsert(CreditLines e) throws BusinessException {
		super.postInsert(e);
		this.deActivateOthers(e);
		this.manageExposure(e);
	}

	private void manageExposure(CreditLines e) throws BusinessException {
		if (e.getCustomer() == null) {
			return;
		}
		if (e.getActive()) {
			IExposureService exposureService = (IExposureService) this.findEntityService(Exposure.class);
			List<Exposure> exposureList = exposureService.findByCustomer(e.getCustomer());
			if (exposureList.isEmpty()) {
				exposureService.createExposure(e);
			} else {
				exposureService.updateExposure(e, ExposureLastAction._RECONCILIATION_);
			}
		}
	}

	private void deActivateOthers(CreditLines e, String reason) throws BusinessException {
		if (e.getCustomer() == null || !e.getActive()) {
			return;
		}
		IExposureService exposureService = (IExposureService) this.findEntityService(Exposure.class);
		List<Exposure> exposureList = exposureService.findByCustomer(e.getCustomer());
		exposureService.delete(exposureList);
		Map<String, Object> params = new HashMap<>();
		params.put("customer", e.getCustomer());
		params.put("active", true);
		List<CreditLines> list = this.findEntitiesByAttributes(params);
		ICreditLinesService srv = (ICreditLinesService) this.findEntityService(CreditLines.class);
		if (CollectionUtils.isEmpty(list)) {
			return;
		}
		for (CreditLines cl : list) {
			if (!cl.getId().equals(e.getId())) {
				srv.deActivate(cl, reason, DE_ACTIVATE);
			}
		}
	}

	private void deActivateOthers(CreditLines e) throws BusinessException {
		this.deActivateOthers(e, BusinessErrorCode.CREDIT_LINE_UPDATE.getErrMsg());
	}

	/**
	 * @param e
	 * @param changeReason
	 * @throws BusinessException
	 */
	@Transactional
	@History(type = CreditLines.class)
	@Override
	public void activate(CreditLines e, @Reason String changeReason, @Action String action) throws BusinessException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Credit line is " + action + " with reason" + changeReason);
		}
		e.setActive(true);
		this.onUpdate(e);
	}

	/**
	 * @param e
	 * @param changeReason
	 * @throws BusinessException
	 */
	@Transactional
	@History(type = CreditLines.class)
	@Override
	public void deActivate(CreditLines e, @Reason String changeReason, @Action String action) throws BusinessException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Credit line is " + action + " with reason" + changeReason);
		}
		e.setActive(false);
		this.onUpdate(e);
	}

	/**
	 * Checks if the availability has a value and if the case sets it as the credi limit(default)
	 *
	 * @param e
	 */
	private void checkAvailability(CreditLines e) {
		if (e.getAvailability() == null) {
			e.setAvailability(e.getAmount());
		}
	}

	/**
	 * Check if the the current Credit Line is valid.
	 *
	 * @param cl
	 * @throws BusinessException
	 */
	private void checkValidity(CreditLines cl) throws BusinessException {
		if (cl.getSupplier() == null) {
			return;
		}

		if (cl.getValidFrom() != null && cl.getValidTo() != null && cl.getValidFrom().after(cl.getValidTo())) {
			throw new BusinessException(BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
					BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO.getErrMsg());
		}
		Map<String, Object> params = new HashMap<>();
		params.put("creditTerm", cl.getCreditTerm());
		this.setCompany(cl, params);
		List<CreditLines> clList = this.findEntitiesByAttributes(params);

		for (CreditLines creditLines : clList) {
			if (!creditLines.getId().equals(cl.getId()) && (this.isInPeriod(creditLines.getValidFrom(), creditLines.getValidTo(), cl.getValidFrom())
					|| this.isInPeriod(creditLines.getValidFrom(), creditLines.getValidTo(), cl.getValidTo()))) {
				throw new BusinessException(BusinessErrorCode.CREDIT_LINE_OVERLAPING, BusinessErrorCode.CREDIT_LINE_OVERLAPING.getErrMsg());
			}
		}
	}

	private void setCompany(CreditLines cl, Map<String, Object> params) throws BusinessException {
		if (cl.getSupplier() == null && cl.getCustomer() == null) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, ErrorCode.G_RUNTIME_ERROR.getErrMsg());
		}
		if (cl.getSupplier() != null) {
			params.put("supplier", cl.getSupplier());
		} else if (cl.getCustomer() != null) {
			params.put("customer", cl.getCustomer());
		}
	}

	/**
	 * Verify if the date is between start and the end day.
	 *
	 * @param startPeriod
	 * @param endPeriod
	 * @param date
	 * @return
	 */
	private boolean isInPeriod(Date startPeriod, Date endPeriod, Date date) {
		return date.after(startPeriod) && date.before(endPeriod);
	}

	@Transactional
	@History(type = CreditLines.class, value = "Update")
	@Override
	public void update(CreditLines e, @Reason String changeReason) throws BusinessException {
		this.update(e);
	}

	@Override
	@Transactional
	public void insert(CreditLines e, String changeReason) throws BusinessException {
		this.onInsert(e);
		this.deActivateOthers(e, changeReason);
	}

	@Transactional
	@History(type = CreditLines.class, value = "Update")
	@Override
	public void updateCreditTriggerExposure(CreditLines e, ExposureLastAction lastAction, @Reason String changeReason) throws BusinessException {
		this.update(e);
		IExposureService exposureService = (IExposureService) this.findEntityService(Exposure.class);
		exposureService.updateExposure(e, lastAction);
	}

	@Transactional
	@History(type = CreditLines.class, value = "Insert")
	@Override
	public void insertCreditTriggerExposure(CreditLines e, ExposureLastAction lastAction, @Reason String changeReason) throws BusinessException {
		this.insert(e, changeReason);
		IExposureService exposureService = (IExposureService) this.findEntityService(Exposure.class);
		exposureService.createExposure(e);
	}
}
