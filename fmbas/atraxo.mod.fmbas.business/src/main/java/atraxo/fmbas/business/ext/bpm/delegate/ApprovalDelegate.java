package atraxo.fmbas.business.ext.bpm.delegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.user.IRoleSuppService;
import atraxo.fmbas.business.api.workflow.IWorkflowService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.user.RoleSupp;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.Workflow;

/**
 * Abstract <code>AbstractJavaDelegate</code> class with the scope of offering common behavior for delegates that calculate if a certain approval step
 * should be taken or not, within an existing running workflow
 *
 * @author vhojda
 */
public abstract class ApprovalDelegate extends AbstractJavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApprovalDelegate.class);

	@Autowired
	protected IRoleSuppService roleService;
	@Autowired
	protected IWorkflowService workflowService;

	/**
	 * Extracts a list of users for specific Role code(s) given as parameter;this method will stop the process in case the Roles based on the received
	 * codes are not well defined;
	 *
	 * @param variableApproverRoles
	 * @return
	 * @throws Exception
	 */
	protected List<UserSupp> extractApprovers(String variableApproverRoles) throws Exception {
		return this.extractApprovers(variableApproverRoles, true);
	}

	/**
	 * Extracts a list of users for specific Role code(s) given as parameter; this method can stop the process in case the Roles based on the received
	 * codes are not well defined;
	 *
	 * @param variableApproverRoles
	 *            the code(s) for the approver role(s)
	 * @param throwErrorIfRoleNotDefined
	 *            true if the system should stop the process and throw an error informing the user that the Role(s) is/are not well defined (the role
	 *            was removed, the role doesn't exist or there are no users defined for that role(s)); false - otherwise
	 * @return
	 * @throws Exception
	 */
	protected List<UserSupp> extractApprovers(String variableApproverRoles, boolean throwErrorIfRoleNotDefined) throws Exception {
		List<UserSupp> usersList = null;

		String approverRoleCodes = (String) this.execution.getVariable(variableApproverRoles);

		if (!StringUtils.isEmpty(approverRoleCodes)) {

			usersList = new ArrayList<>();
			Object workflowId = this.execution.getVariable(WorkflowVariablesConstants.VAR_WKF_ID);
			Workflow wkf = this.workflowService.findById(workflowId);
			Customer subsidiary = wkf.getSubsidiary();

			StringTokenizer tokenizer = new StringTokenizer(approverRoleCodes, ",");
			while (tokenizer.hasMoreTokens()) {
				String roleCode = tokenizer.nextToken();

				// first, get the roles
				RoleSupp roleLevel = null;
				try {
					roleLevel = this.roleService.findByCode(roleCode);

					// get the users for each role
					Collection<UserSupp> usersCollection = roleLevel.getUsers();
					for (UserSupp userSupp : usersCollection) {
						boolean add = false;
						if (subsidiary != null) {
							for (UserSubsidiary userSub : userSupp.getSubsidiaries()) {
								if (userSub.getSubsidiary().equals(subsidiary)) {
									add = true;
									break;
								}
							}
						}
						if (subsidiary != null) {
							if (add) {
								this.addUserToList(userSupp, usersList);
							}
						} else {
							this.addUserToList(userSupp, usersList);
						}
					}

				} catch (Exception e1) {
					if (throwErrorIfRoleNotDefined) {
						usersList = null;
						LOGGER.error("ERROR:could not find a role by code ", approverRoleCodes);
						this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
								String.format(BusinessErrorCode.ROLE_BY_CODE_NOT_EXISTING.getErrMsg(), roleCode), e1);
						break;
					} else {
						LOGGER.warn("ERROR:could not find a role by code ", roleCode);
					}
				}

			}

			if (usersList != null && usersList.isEmpty() && throwErrorIfRoleNotDefined) {
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						String.format(BusinessErrorCode.ROLES_NO_USERS_ERROR.getErrMsg(), approverRoleCodes));
			}

		} else {
			if (throwErrorIfRoleNotDefined) {
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						BusinessErrorCode.ROLES_CODE_EMPTY_ERROR.getErrMsg());
			} else {
				LOGGER.warn("WARNING: The approver role codes are empty !", approverRoleCodes);
			}
		}

		return usersList;
	}

	/**
	 * Resets the variables needed by the "send e-mail component" (counter and continue_send_email)
	 */
	protected void resetMailComponentVariables() {
		// put static variables used by the email component
		this.execution.setVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_SEND_COUNTER, Integer.valueOf(0));
	}

	/**
	 * @param userSupp
	 * @param usersList
	 */
	private void addUserToList(UserSupp userSupp, List<UserSupp> usersList) {
		boolean found = false;
		for (UserSupp uSup : usersList) {
			if (uSup.getId().equals(userSupp.getId())) {
				found = true;
				break;
			}
		}
		if (!found) {
			usersList.add(userSupp);
		}
	}

}
