/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.notification.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.notification.INotificationService;
import atraxo.fmbas.domain.impl.fmbas_type.NotificationType;
import atraxo.fmbas.domain.impl.notification.Notification;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link Notification} domain entity.
 */
public class Notification_Service extends atraxo.fmbas.business.impl.notification.Notification_Service implements INotificationService {

	@Autowired
	private NotificationSenderService sender;

	@Override
	public List<Notification> findForUser(UserSupp user) throws BusinessException {
		return this.findForUser(user.getCode());
	}

	@Override
	public List<Notification> findForUser(String code) throws BusinessException {
		Map<String, Object> params = Collections.emptyMap();
		List<Notification> list = this.findEntitiesByAttributes(params);
		List<Notification> usersNotification = new ArrayList<>();
		for (Notification e : list) {
			if (this.isAssignable(code, e)) {
				usersNotification.add(e);
			}
		}
		return usersNotification;
	}

	/**
	 * @param e
	 * @param emailUsers
	 * @throws BusinessException
	 */
	@Transactional
	@Override
	public void sendAndSave(Notification e, List<UserSupp> emailUsers, Map objectMap) throws BusinessException {
		if (!CollectionUtils.isEmpty(e.getUsers())) {
			this.insert(e);
		}
		this.sendNotificationToClient(e, emailUsers, objectMap);

	}

	private void sendNotificationToClient(Notification e, List<UserSupp> users, Map objectMap) throws BusinessException {
		this.sender.send(e, NotificationType._EMPTY_, users, objectMap);
	}

	/**
	 * Notification <code>e</code> is assignable to user with code <code>code</code>.
	 *
	 * @param code
	 * @param e
	 * @return
	 */
	private boolean isAssignable(String code, Notification e) {
		Collection<UserSupp> users = e.getUsers();
		if (CollectionUtils.isEmpty(users)) {
			return true;
		} else {
			if (this.getUserByCode(users, code) != null) {
				return true;
			}
		}
		return false;
	}

	private UserSupp getUserByCode(Collection<UserSupp> users, String code) {
		UserSupp user = null;
		for (UserSupp u : users) {
			if (u.getCode().equals(code)) {
				user = u;
			}
		}

		return user;
	}

	@Override
	@Transactional
	public void deleteForUser(String code) throws BusinessException {
		List<Notification> list = this.findForUser(code);
		List<Notification> delList = new ArrayList<>();
		List<Notification> updList = new ArrayList<>();
		for (Notification e : list) {
			if (this.isAssignable(code, e)) {
				if (e.getUsers().size() <= 1) {
					delList.add(e);
				} else {
					e.getUsers().remove(this.getUserByCode(e.getUsers(), code));
					updList.add(e);
				}
			}
		}
		this.delete(delList);
		this.update(updList);
	}

	@Override
	public void addUsersToNotification(Notification e, UserSupp u, NotificationType type, List<UserSupp> emailUsers) throws BusinessException {
		if (u.getNotifications() == null) {
			u.setNotifications(new ArrayList<Notification>());
		}
		switch (type) {
		case _BOTH_:
			e.getUsers().add(u);
			emailUsers.add(u);
			break;
		case _EMAIL_:
			emailUsers.add(u);
			break;
		case _EVENT_:
			e.getUsers().add(u);
			break;
		default:
			break;
		}
	}
}
