/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.customer.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import atraxo.fmbas.business.api.contacts.IContactsService;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.customer.ICustomerNotificationService;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.notes.INotesService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.ws.IRestClientCallbackService;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.PrimaryContactCannotDetermineException;
import atraxo.fmbas.business.ext.exceptions.ResellerChangeException;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.business.ext.util.EmailUtil;
import atraxo.fmbas.business.ws.ExternalInterfaceWSHelper;
import atraxo.fmbas.business.ws.client.PumaRestClient;
import atraxo.fmbas.business.ws.toNav.WSCustomerRequestToDTOTransformer;
import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.fmbas_type.CreditUpdateRequestStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataProcessedStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataTransmissionStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerType;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsStatus;
import atraxo.fmbas.domain.impl.fmbas_type.NatureOfBusiness;
import atraxo.fmbas.domain.impl.masteragreements.MasterAgreement;
import atraxo.fmbas.domain.impl.notes.Notes;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import seava.j4e.api.annotation.Reason;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.IOrganization;
import seava.j4e.api.session.IUserProfile;
import seava.j4e.api.session.Session;
import seava.j4e.commons.security.Organization;

/**
 * Business extensions specific for {@link Customers} domain entity.
 */
public class Customer_Service extends atraxo.fmbas.business.impl.customer.Customer_Service
		implements ICustomerService, IRestClientCallbackService<Customer> {

	private static final String STATUS = "status";

	private static final Logger LOG = LoggerFactory.getLogger(Customer_Service.class);

	@Autowired
	private ICustomerNotificationService customerNotificationService;

	@Autowired
	private IExternalInterfaceService extInterSrv;
	@Autowired
	private ExternalInterfaceHistoryFacade historyFacade;
	@Autowired
	private ExternalInterfaceMessageHistoryFacade messageFacade;
	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;

	@Override
	@Transactional
	public void finish(WSExecutionResult result, Collection<Customer> list) throws BusinessException {
		if (!result.isSuccess()) {
			List<Customer> toUpdate = new ArrayList<>();
			for (Customer customer : list) {
				try {
					Customer c = this.findByCode(customer.getCode());
					c.setTransmissionStatus(CustomerDataTransmissionStatus._FAILED_);
					c.setProcessedStatus(CustomerDataProcessedStatus._FAILED_);
					toUpdate.add(c);
				} catch (ApplicationException e) {
					if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(e.getErrorCode())) {
						throw e;
					}
					// cutomer not found do nothing
					LOG.warn("Could not retrieve a customer by code " + customer.getCode() + " ! Will do nothing !", e);
				}
			}
			this.update(toUpdate);
		}
	}

	@Override
	protected void preInsert(List<Customer> list) throws BusinessException {
		super.preInsert(list);
		for (Customer cust : list) {
			this.validateTypeSelected(cust);
			this.checkGroupMembers(cust);
			if (cust.getEmail() != null) {
				this.checkEmail(cust);
			}

			this.checkStreet(cust);

			if (cust.getIsSubsidiary() == null) {
				cust.setIsSubsidiary(false);
			}
			// check for assigned area only for subsidiaries (it is mandatory only in this case)
			if (cust.getIsSubsidiary() && this.checkSubsidiaryBusinessKey(cust)) {
				throw new BusinessException(BusinessErrorCode.AREA_ALREADY_ASSIGNED, BusinessErrorCode.AREA_ALREADY_ASSIGNED.getErrMsg());
			}

			if (cust.getName().trim().length() == 0 || cust.getCode().trim().length() == 0) {
				throw new BusinessException(BusinessErrorCode.CODE_NAME_CANT_BE_EMPTY, BusinessErrorCode.CODE_NAME_CANT_BE_EMPTY.getErrMsg());
			}
		}
	}

	@Override
	protected void preUpdate(List<Customer> list) throws BusinessException {
		super.preUpdate(list);
		for (Customer cust : list) {
			this.canChangeReseller(cust);
			this.validateTypeSelected(cust);
			this.checkGroupMembers(cust);
			if (cust.getEmail() != null) {
				this.checkEmail(cust);
			}
			this.checkStreet(cust);
			if (cust.getIsSubsidiary() == null) {
				cust.setIsSubsidiary(false);
			}
			// check for assigned area only for subsidiaries (it is mandatory only in this case)
			if (cust.getIsSubsidiary() && this.checkSubsidiaryBusinessKey(cust)) {
				throw new BusinessException(BusinessErrorCode.AREA_ALREADY_ASSIGNED, BusinessErrorCode.AREA_ALREADY_ASSIGNED.getErrMsg());
			}

			if (cust.getName().trim().length() == 0 || cust.getCode().trim().length() == 0) {
				throw new BusinessException(BusinessErrorCode.CODE_NAME_CANT_BE_EMPTY, BusinessErrorCode.CODE_NAME_CANT_BE_EMPTY.getErrMsg());
			}
		}
	}

	private boolean checkSubsidiaryBusinessKey(Customer e) {
		List<Customer> customers = this.findByAssignedArea(e.getAssignedArea());
		if (CollectionUtils.isEmpty(customers)) {
			return false;
		}
		for (Customer c : customers) {
			if (!c.getId().equals(e.getId())) {
				return true;
			}
		}
		return false;
	}

	@Override
	@Transactional
	public void deleteByIds(List<Object> ids) throws BusinessException {
		List<Customer> list = this.findByIds(ids);
		this.delete(list);
	}

	@Override
	@Transactional
	public void deleteById(Object id) throws BusinessException {
		Customer customer = this.findById(id);
		this.delete(customer);
	}

	@Override
	protected void preDelete(Customer customer) throws BusinessException {
		super.preDelete(customer);
		CustomerType customerType = customer.getType();
		if (CustomerType._GROUP_.equals(customerType)) {
			List<Customer> customersChild = this.findByParentId(customer.getId());
			if (!CollectionUtils.isEmpty(customersChild)) {
				throw new BusinessException(BusinessErrorCode.CUSTOMER_CANNOT_DELETE, BusinessErrorCode.CUSTOMER_CANNOT_DELETE.getErrMsg());
			}
		}
		if (customer.getIsThirdParty() != null && customer.getIsThirdParty()) {
			Collection<Customer> resellers = customer.getReseller();
			if (resellers.size() > 1) {
				throw new BusinessException(BusinessErrorCode.ASSIGNED_TO_ANOTHER_CUSTOMER,
						BusinessErrorCode.ASSIGNED_TO_ANOTHER_CUSTOMER.getErrMsg());
			}
		}
		this.customerNotificationService.delete(new ArrayList<>(customer.getCustomerNotification()));
	}

	@Override
	protected void preDelete(List<Customer> list) throws BusinessException {
		super.preDelete(list);
		INotesService notesService = (INotesService) this.findEntityService(Notes.class);
		IContactsService contactService = (IContactsService) this.findEntityService(Contacts.class);
		notesService.deleteByEntities(list, Customer.class);
		contactService.deleteByEntities(list, Customer.class);
	}

	private void checkEmail(Customer customer) throws BusinessException {
		if (customer.getEmail() != null && !customer.getEmail().matches("") && !EmailUtil.validateEmail(customer.getEmail())) {
			throw new BusinessException(BusinessErrorCode.CONTACT_EMAIL, BusinessErrorCode.CONTACT_EMAIL.getErrMsg());
		}
	}

	/**
	 * Checks if the inserted street adress is between the limits of 50 characters. If it's not it throws an error.
	 *
	 * @param customer
	 * @throws BusinessException
	 */
	private void checkStreet(Customer customer) throws BusinessException {
		if (customer.getBillingStreet() != null && customer.getBillingStreet().length() > 50) {
			throw new BusinessException(BusinessErrorCode.BILLING_STREET_NAME_TOO_LONG, BusinessErrorCode.BILLING_STREET_NAME_TOO_LONG.getErrMsg());
		}
		if (customer.getOfficeStreet() != null && customer.getOfficeStreet().length() > 50) {
			throw new BusinessException(BusinessErrorCode.OFFICE_STREET_NAME_TOO_LONG, BusinessErrorCode.OFFICE_STREET_NAME_TOO_LONG.getErrMsg());
		}
	}

	/**
	 * @param customer
	 * @throws ResellerChangeException
	 */
	private void canChangeReseller(Customer customer) throws ResellerChangeException {
		if (!(NatureOfBusiness._RESELLER_.equals(customer.getNatureOfBusiness()) || customer.getFinalCustomer().isEmpty())) {
			throw new ResellerChangeException();
		}
	}

	/**
	 * @param customer
	 * @throws BusinessException
	 */
	private void validateTypeSelected(Customer customer) throws BusinessException {
		CustomerType customerType = customer.getType();
		if (CustomerType._GROUP_MEMBER_.equals(customerType) && customer.getParent() == null) {
			throw new BusinessException(BusinessErrorCode.CUSTOMER_PARENT_ACCOUNT, BusinessErrorCode.CUSTOMER_PARENT_ACCOUNT.getErrMsg());
		}
		if (customer.getParent() != null && customer.getId() == customer.getParent().getId()) {
			throw new BusinessException(BusinessErrorCode.CUSTOMER_SELECTED_HIMSELF, BusinessErrorCode.CUSTOMER_SELECTED_HIMSELF.getErrMsg());
		}
	}

	/**
	 * @param customer
	 * @throws BusinessException
	 */
	private void checkGroupMembers(Customer customer) throws BusinessException {
		CustomerType customerType = customer.getType();
		if ((CustomerType._GROUP_.equals(customerType) || CustomerType._INDEPENDENT_.equals(customerType)) && customer.getParent() != null) {
			customer.setParent(null);
		}
		if (CustomerType._GROUP_MEMBER_.equals(customerType) || CustomerType._INDEPENDENT_.equals(customerType)) {
			List<Customer> customers = this.findByParentId(customer.getId());
			for (Customer cust : customers) {
				cust.setParent(null);
				cust.setType(CustomerType._INDEPENDENT_);
			}
			this.update(customers);
		}
	}

	@Override
	public MasterAgreement getValidMasterAgreement(Customer customer, Date date) throws BusinessException {
		Collection<MasterAgreement> agreements = customer.getMasterAgreement();
		if (agreements == null) {
			throw new BusinessException(BusinessErrorCode.NO_MASTER_AGREEMENT,
					BusinessErrorCode.NO_MASTER_AGREEMENT.getErrMsg() + SimpleDateFormat.getDateInstance().format(date));
		}
		Iterator<MasterAgreement> iter = agreements.iterator();
		while (iter.hasNext()) {
			MasterAgreement agreement = iter.next();
			if (agreement.getEvergreen() || this.isValidFor(date, agreement.getValidFrom(), agreement.getValidTo())) {
				return agreement;
			}
		}
		throw new BusinessException(BusinessErrorCode.NO_MASTER_AGREEMENT,
				BusinessErrorCode.NO_MASTER_AGREEMENT.getErrMsg() + SimpleDateFormat.getDateInstance().format(date));
	}

	@Override
	public MasterAgreement getValidEffectiveMasterAgreement(Customer customer, Date date) throws BusinessException {
		Collection<MasterAgreement> agreements = customer.getMasterAgreement();

		if (agreements == null) {
			throw new BusinessException(BusinessErrorCode.NO_MASTER_AGREEMENT,
					BusinessErrorCode.NO_MASTER_AGREEMENT.getErrMsg() + SimpleDateFormat.getDateInstance().format(date));
		}

		Iterator<MasterAgreement> iter = agreements.iterator();
		while (iter.hasNext()) {
			MasterAgreement agreement = iter.next();
			if (agreement.getStatus().equals(MasterAgreementsStatus._EFFECTIVE_)
					&& (agreement.getEvergreen() || this.isValidFor(date, agreement.getValidFrom(), agreement.getValidTo()))) {
				return agreement;
			}
		}

		throw new BusinessException(BusinessErrorCode.NO_MASTER_AGREEMENT,
				BusinessErrorCode.NO_MASTER_AGREEMENT.getErrMsg() + SimpleDateFormat.getDateInstance().format(date));
	}

	@Override
	public CreditLines getValidCreditLine(Customer customer, Date date) throws BusinessException {
		Collection<CreditLines> creditLines = customer.getCreditLines();
		if (CollectionUtils.isEmpty(creditLines)) {
			throw new BusinessException(BusinessErrorCode.NO_CREDIT_LINE,
					BusinessErrorCode.NO_CREDIT_LINE.getErrMsg() + SimpleDateFormat.getDateInstance().format(date));
		}
		Iterator<CreditLines> iter = creditLines.iterator();
		while (iter.hasNext()) {
			CreditLines cl = iter.next();
			if (cl.getActive() != null && cl.getActive() && this.isValidFor(date, cl.getValidFrom(), cl.getValidTo())) {
				return cl;
			}
		}
		throw new BusinessException(BusinessErrorCode.NO_CREDIT_LINE,
				BusinessErrorCode.NO_CREDIT_LINE.getErrMsg() + SimpleDateFormat.getDateInstance().format(date));
	}

	@Override
	public CreditLines getValidCreditLineWithoutException(Customer customer, Date date) throws BusinessException {
		Collection<CreditLines> creditLines = customer.getCreditLines();
		if (CollectionUtils.isEmpty(creditLines)) {
			return null;
		}
		Iterator<CreditLines> iter = creditLines.iterator();
		while (iter.hasNext()) {
			CreditLines cl = iter.next();
			if (cl.getActive() != null && cl.getActive() && this.isValidFor(date, cl.getValidFrom(), cl.getValidTo())) {
				return cl;
			}
		}
		return null;
	}

	private boolean isValidFor(Date date, Date from, Date to) {
		return from.compareTo(date) <= 0 && to.compareTo(date) >= 0;
	}

	@Override
	public Contacts getPrimaryContact(Customer customer) throws BusinessException {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("isPrimary", true);
			params.put("objectType", Customer.class.getSimpleName());
			params.put("objectId", customer.getId());
			return this.findEntityByAttributes(Contacts.class, params);
		} catch (ApplicationException e) {
			throw new PrimaryContactCannotDetermineException(Customer.class, e);
		}
	}

	@Override
	@Transactional
	@History(value = "Activated", type = Customer.class)
	public void approveCustomers(List<Customer> customers) throws BusinessException {
		for (Customer cust : customers) {
			try {
				if (!StringUtils.isEmpty(cust.getAccountNumber())) {
					cust.setStatus(CustomerStatus._ACTIVE_);
				} else {
					throw new BusinessException(BusinessErrorCode.NO_ACCOUNT_NUMBER_SET, BusinessErrorCode.NO_ACCOUNT_NUMBER_SET.getErrMsg());
				}
			} catch (Exception e) {
				throw new BusinessException(BusinessErrorCode.NO_ACCOUNT_NUMBER_SET, BusinessErrorCode.NO_ACCOUNT_NUMBER_SET.getErrMsg(), e);
			}

		}
		this.update(customers);
	}

	@Override
	@Transactional
	@History(value = "Activated", type = Customer.class)
	public void approveCustomer(Customer customer, @Reason String reason) throws BusinessException {
		try {
			if (!customer.getAccountNumber().isEmpty()) {
				customer.setStatus(CustomerStatus._ACTIVE_);
			} else {
				throw new BusinessException(BusinessErrorCode.NO_ACCOUNT_NUMBER_SET, BusinessErrorCode.NO_ACCOUNT_NUMBER_SET.getErrMsg());
			}
		} catch (Exception e) {
			throw new BusinessException(BusinessErrorCode.NO_ACCOUNT_NUMBER_SET, BusinessErrorCode.NO_ACCOUNT_NUMBER_SET.getErrMsg(), e);
		}

		this.update(customer);
	}

	@Override
	@Transactional
	@History(value = "Deactivated ", type = Customer.class)
	public void deactivateCustomers(Customer customer, @Reason String reason) throws BusinessException {
		customer.setStatus(CustomerStatus._INACTIVE_);
		this.update(customer);
	}

	@Override
	@Transactional
	@History(value = "Blocked", type = Customer.class)
	public void blacklistCustomer(Customer customer, @Reason String reason) throws BusinessException {
		customer.setStatus(CustomerStatus._BLOCKED_);
		this.update(customer);
	}

	@Override
	public BigDecimal convertCreditLimit(Integer creditLimit, String creditCurrencyCode, String currencyCode) throws BusinessException {
		ExchangeRate_Bd bd = this.getBusinessDelegate(ExchangeRate_Bd.class);
		ICurrenciesService service = (ICurrenciesService) this.findEntityService(Currencies.class);
		Currencies creditCurrency = service.findByCode(creditCurrencyCode);
		Currencies currency = service.findByCode(currencyCode);
		Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH);
		ConversionResult result = bd.convert(creditCurrency, currency, date, BigDecimal.valueOf(creditLimit), false);
		return result.getValue();
	}

	@Override
	public BigDecimal getAllocatedCredit() throws BusinessException {
		BigDecimal result = BigDecimal.ZERO;
		Map<String, Object> params = new HashMap<>();
		params.put(STATUS, CustomerStatus._ACTIVE_);
		List<Customer> list = this.findEntitiesByAttributes(params);
		ISystemParameterService srv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		String sysCurrencyCode = srv.getSysCurrency();
		Date today = GregorianCalendar.getInstance().getTime();
		for (Customer customer : list) {
			for (CreditLines cl : customer.getCreditLines()) {
				if (cl.getActive() != null && cl.getActive() && this.isValidFor(today, cl.getValidFrom(), cl.getValidTo())) {
					try {
						result = result.add(this.convertCreditLimit(cl.getAmount(), cl.getCurrency().getCode(), sysCurrencyCode));
					} catch (BusinessException be) {
						LOG.warn("Business exception occoured on convert credit limit. ", be);
					}
				}
			}
		}
		return result;

	}

	@Override
	public BigDecimal getCreditUtilizationRation() throws BusinessException {
		BigDecimal result = BigDecimal.ZERO;
		BigDecimal credit = BigDecimal.ZERO;
		HashMap<String, Object> params = new HashMap<>();
		params.put(STATUS, CustomerStatus._ACTIVE_);
		List<Customer> list = this.findEntitiesByAttributes(params);
		ISystemParameterService srv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		String sysCurrencyCode = srv.getSysCurrency();
		Date today = GregorianCalendar.getInstance().getTime();
		for (Customer customer : list) {
			for (CreditLines cl : customer.getCreditLines()) {
				try {
					if (cl.getActive() != null && cl.getActive() && this.isValidFor(today, cl.getValidFrom(), cl.getValidTo())
							&& cl.getCurrency() != null && cl.getAmount() != null) {
						// TODO exposure calculation
						// Exposure exposure = service.getExposure(ma);
						// result = result.add(exposure.getActualUtilization());
						credit = credit.add(this.convertCreditLimit(cl.getAmount(), cl.getCurrency().getCode(), sysCurrencyCode));
					}
				} catch (BusinessException be) {
					// do nothing exposure not found
					LOG.info(be.getMessage(), be);
				}
			}

		}
		if (credit.equals(BigDecimal.ZERO)) {
			return BigDecimal.ZERO;
		}
		result = result.multiply(BigDecimal.valueOf(100)).divide(credit, MathContext.DECIMAL64);
		result = result.setScale(2, BigDecimal.ROUND_CEILING);
		return result;

	}

	@Override
	@Transactional
	public void promoteFinalCustomers(List<Customer> customers) throws BusinessException {
		for (Customer customer : customers) {
			customer.setIsCustomer(true);
			customer.setIsThirdParty(false);
			customer.setType(CustomerType._INDEPENDENT_);
			customer.setSystem(false);
			this.onUpdate(customer);
		}
	}

	@Override
	public List<Customer> findSubsidiaries() throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("isSubsidiary", true);
		return this.findEntitiesByAttributes(params);
	}

	@Override
	public List<Customer> findCustomer(List<Integer> ids) throws BusinessException {
		return this.getEntityManager()
				.createQuery("select e from Customer e where e.clientId = :clientId and e.status = :status and e.id not in :id ", Customer.class)
				.setParameter("clientId", Session.user.get().getClient().getId()).setParameter(STATUS, CustomerStatus._ACTIVE_)
				.setParameter("id", ids).getResultList();
	}

	@Override
	public List<Customer> getCustomer() throws BusinessException {
		List<Customer> customers = new ArrayList<>();
		Map<String, Object> params = new HashMap<>();
		params.put(STATUS, CustomerStatus._ACTIVE_);
		customers.addAll(this.findEntitiesByAttributes(params));
		return !customers.isEmpty() ? customers : null;
	}

	@Override
	@Transactional
	public void requestCreditStatus(Customer customer) throws BusinessException {

		ExternalInterface extInterface = this.extInterSrv.findByName(ExtInterfaceNames.CLIENT_UPDATE_REQUEST.getValue());
		ExtInterfaceParameters params = new ExtInterfaceParameters(extInterface.getParams());
		WSCustomerRequestToDTOTransformer transformer = new WSCustomerRequestToDTOTransformer();
		params.put(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_DESTINATION_ADDRESS, "");
		params.put(ExternalInterfaceWSHelper.CAN_MODIFY_DESTINATION_ADDRESS, "FALSE");
		PumaRestClient<Customer> client = new PumaRestClient<>(this.taskExecutor, extInterface, params, Arrays.asList(customer), this.historyFacade,
				transformer, this.messageFacade);

		client.start(3);
		customer.setCreditUpdateRequestStatus(CreditUpdateRequestStatus._IN_PROGRESS_);
		this.update(customer);
	}

	@Override
	public void updateJobSessionUserProfile(Object obj) throws BusinessException {
		List<Customer> list = this.findSubsidiaries();
		for (Customer c : list) {
			IOrganization org = new Organization(c.getCode(), c.getName(), false, c.getRefid());
			IUserProfile profile = Session.user.get().getProfile();
			profile.addOrganisation(org);
		}
	}

	@Override
	@Deprecated
	public Customer findByIdFetchLocations(Integer id) {
		// TODO need refactoring
		return this.getEntityManager()
				.createQuery("select distinct e from Customer e join fetch e.locations " + "where e.clientId = :clientId and e.id = :id",
						Customer.class)
				.setParameter("clientId", Session.user.get().getClient().getId()).setParameter("id", id).getSingleResult();
	}

}
