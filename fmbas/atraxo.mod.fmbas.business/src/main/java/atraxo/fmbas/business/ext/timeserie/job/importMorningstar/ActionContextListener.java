package atraxo.fmbas.business.ext.timeserie.job.importMorningstar;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.net.util.Base64;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import atraxo.fmbas.business.api.job.IActionService;
import atraxo.fmbas.business.ext.timeserie.job.importPlatts.PlattsFileItemWriter.Params;
import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.ActionParameter;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.domain.batchjob.JobParameter;
import seava.j4e.scheduler.batch.listener.DefaultActionListener;

/**
 * @author abolindu
 */
public class ActionContextListener extends DefaultActionListener {

	@Autowired
	private IActionService service;
	private final RestTemplate restTemplate = new RestTemplate();
	Map<ParamName, String> paramMap = new EnumMap<>(ParamName.class);
	private final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");

	private enum ParamName {
		BASEURL, LISTNAME, OFFSET, USERNAME, PASSWORD, OVERWRITE
	}

	@Override
	public void beforeJob(JobExecution jobExecution) {
		Long actionId = jobExecution.getJobParameters().getLong(JobParameter.ACTION.name());
		Action action = this.service.findById(actionId.intValue());
		for (ActionParameter param : action.getActionParameters()) {
			this.paramMap.put(ParamName.valueOf(param.getName()), param.getValue() != null ? param.getValue() : param.getDefaultValue());
		}
		jobExecution.getExecutionContext().put(ParamName.OVERWRITE.toString(), this.paramMap.get(ParamName.OVERWRITE));
	}

	/**
	 * Authentification HTTP request
	 *
	 * @return
	 * @throws BusinessException
	 */
	public Resource authentificate() throws BusinessException {
		HttpEntity<String> request = new HttpEntity<>(
				this.createHeaders(this.paramMap.get(ParamName.USERNAME), this.paramMap.get(ParamName.PASSWORD)));
		ResponseEntity<Resource> response = this.restTemplate.exchange(this.buildUrl(), HttpMethod.GET, request, Resource.class);

		return response.getBody();
	}

	/**
	 * @return
	 */
	private String buildUrl() {
		Integer offsetVal = Integer.valueOf(this.paramMap.get(ParamName.OFFSET));
		Calendar calendar = Calendar.getInstance();
		StringBuilder url = new StringBuilder(this.paramMap.get(ParamName.BASEURL));
		url.append(this.paramMap.get(ParamName.LISTNAME));
		if (offsetVal != 0) {
			url.append("/content?fromDateTime=");
			calendar.add(Calendar.DAY_OF_MONTH, offsetVal > 0 ? -offsetVal : offsetVal);
			url.append(this.SDF.format(calendar.getTime()));
			url.append("&toDateTime=");
			calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			url.append(this.SDF.format(calendar.getTime()));
		} else {
			url.append("/content?fromDateTime=");
			url.append(this.SDF.format(calendar.getTime()));
		}
		return url.toString();
	}

	/**
	 * @param username
	 * @param password
	 * @return
	 */
	private HttpHeaders createHeaders(final String username, final String password) {
		HttpHeaders headers = new HttpHeaders();
		String auth = username + ":" + password;
		byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
		String authHeader = "Basic " + new String(encodedAuth);
		headers.add("Authorization", authHeader);
		return headers;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void afterJob(JobExecution jobExecution) {
		super.afterJob(jobExecution);
		List<Throwable> errorsList = jobExecution.getAllFailureExceptions();
		if (!errorsList.isEmpty()) {
			for (Throwable error : errorsList) {
				Throwable httpError = error.getCause().getCause();
				if (httpError instanceof HttpClientErrorException) {
					ExitStatus status = jobExecution.getExitStatus();
					StringBuilder sb = new StringBuilder();
					if (((HttpClientErrorException) httpError).getStatusCode().value() == 404) {
						sb.append("The provided URL is not valid.");
					} else if (((HttpClientErrorException) httpError).getStatusCode().value() == 401) {
						sb.append("Wrong USERNAME or PASSWORD.");
					}
					status = new ExitStatus(status.getExitCode(), sb.toString());
					jobExecution.setExitStatus(status);
				} else if (httpError instanceof HttpServerErrorException) {
					ExitStatus status = jobExecution.getExitStatus();
					StringBuilder sb = new StringBuilder();
					sb.append("Wrong credentials.");
					status = new ExitStatus(status.getExitCode(), sb.toString());
					jobExecution.setExitStatus(status);
				}

			}
			return;
		}
		ExitStatus status = jobExecution.getExitStatus();
		StringBuilder sb = new StringBuilder();
		if (jobExecution.getExecutionContext().containsKey(Params.IMPORT_LIST.toString())) {
			Set<String> imported = (Set<String>) jobExecution.getExecutionContext().get(Params.IMPORT_LIST.toString());
			sb.append("Number of imported TimeSeries: ").append(imported.size()).append("\n");
			for (String ts : imported) {
				sb.append("\t - ").append(ts).append("\n");
			}
		}
		if (jobExecution.getExecutionContext().containsKey(Params.UPDATE_LIST.toString())) {
			Set<String> updated = (Set<String>) jobExecution.getExecutionContext().get(Params.UPDATE_LIST.toString());
			sb.append("Number of updated TimeSeries: ").append(updated.size()).append("\n");
			for (String ts : updated) {
				sb.append("\t - ").append(ts).append("\n");
			}
		}
		if (!jobExecution.getExecutionContext().containsKey(Params.IMPORT_LIST.toString())
				&& !jobExecution.getExecutionContext().containsKey(Params.UPDATE_LIST.toString())) {
			sb.append("There are no TimeSeries with \"Morningstar\" provider or there are no new values to insert.");
		}
		status = new ExitStatus(status.getExitCode(), sb.toString());
		jobExecution.setExitStatus(status);
	}
}