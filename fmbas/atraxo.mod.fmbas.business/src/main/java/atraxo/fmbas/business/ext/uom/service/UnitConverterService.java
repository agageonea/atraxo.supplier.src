package atraxo.fmbas.business.ext.uom.service;

import java.math.BigDecimal;
import java.math.MathContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.conversion.InconvertibleUnitTypesException;
import atraxo.fmbas.business.ext.exceptions.conversion.UnitConversionException;
import atraxo.fmbas.business.ext.exceptions.conversion.UnitTyeException;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

public class UnitConverterService extends AbstractBusinessBaseService {

	private static final Logger LOG = LoggerFactory.getLogger(UnitConverterService.class);

	@Autowired
	private IUnitService unitService;

	/**
	 * Convert an unit to another in case they are convertible.
	 *
	 * @param fromUnit
	 * @param toUnit
	 * @param value
	 * @param density
	 * @return
	 * @throws BusinessException
	 */
	public BigDecimal convert(Unit fromUnit, Unit toUnit, BigDecimal value, double density) throws BusinessException {
		Unit dFromUnit = this.unitService.getBaseUnit(UnitType._MASS_, BigDecimal.ONE);
		Unit dToUnit = this.unitService.getBaseUnit(UnitType._VOLUME_, BigDecimal.ONE);
		return this.convert(fromUnit, toUnit, value, new Density(dFromUnit, dToUnit, density));
	}

	/**
	 * @param fromUnit
	 * @param toUnit
	 * @param value
	 * @param density
	 * @return
	 * @throws BusinessException
	 */
	public BigDecimal convert(Unit fromUnit, Unit toUnit, BigDecimal value, Density density) throws BusinessException {
		BigDecimal convertedValue = null;
		if (this.isConvertible(fromUnit, toUnit)) {
			convertedValue = value.multiply(this.getConversionFactor(fromUnit, toUnit), MathContext.DECIMAL64);
		}
		if (convertedValue == null) {
			if (this.isConvertible(fromUnit, toUnit, false)) {
				UnitType fromUnitTypeInd = fromUnit.getUnittypeInd();
				if (Double.compare(density.getDensity(), 0d) == 0) {
					throw new UnitConversionException(BusinessErrorCode.ZERO_DENSITY);
				}
				if (UnitType._VOLUME_.equals(density.getFromUnit().getUnittypeInd())) {
					// density must be defined in Mass/Volume
					density = new Density(density.getToUnit(), density.getFromUnit(), 1 / density.getDensity());
				}
				switch (fromUnitTypeInd) {
				case _VOLUME_:
					convertedValue = value.multiply(this.getConversionFactor(fromUnit, density.getToUnit()), MathContext.DECIMAL64);
					convertedValue = convertedValue.multiply(BigDecimal.valueOf(density.getDensity()), MathContext.DECIMAL64);
					convertedValue = convertedValue.multiply(this.getConversionFactor(density.getFromUnit(), toUnit));
					break;
				case _MASS_:
					convertedValue = value.multiply(this.getConversionFactor(fromUnit, density.getFromUnit()), MathContext.DECIMAL64);
					convertedValue = convertedValue.divide(BigDecimal.valueOf(density.getDensity()), MathContext.DECIMAL64);
					convertedValue = convertedValue.multiply(this.getConversionFactor(density.getToUnit(), toUnit));
					break;
				default:
					throw new UnitConversionException(BusinessErrorCode.INCONVERTIBLE_UNITS);
				}
			} else {
				LOG.warn("Cannot convert from " + fromUnit.getUnittypeInd() + " into " + toUnit.getUnittypeInd()
						+ " . Unconverted value will be returned.");
				convertedValue = value;
			}
		}
		return convertedValue;

	}

	/**
	 * Convert <code>value</code> from <code>fromUnit</code> to <code>toUnit</code> using the conversion <code>factor</code> between
	 * <code>fromUnit</code> and <code>tmpUnit</code>.
	 *
	 * @param fromUnit - Unit in which the <code>value</code> is defined.
	 * @param toUnit - Unit in which the <code>value</code> must be converted.
	 * @param value - The value that will be converted.
	 * @param factor - Conversion factor between <code>factorFromUnit</code> and <code>factorToUnit</code>
	 * @param factorFromUnit
	 * @param factorToUnit
	 * @return The <code>value</code> converted into <code>toUnit</code>.
	 * @throws BusinessException
	 */
	public BigDecimal convert(Unit fromUnit, Unit toUnit, BigDecimal value, BigDecimal factor, Unit factorFromUnit, Unit factorToUnit)
			throws BusinessException {
		BigDecimal convertedValue = null;
		if (this.isConvertible(fromUnit, toUnit)) {
			convertedValue = value.multiply(this.getConversionFactor(fromUnit, toUnit), MathContext.DECIMAL64);
		}
		if (convertedValue == null) {
			if (this.isConvertible(fromUnit, toUnit, false)) {
				if (this.isConvertible(fromUnit, factorToUnit)) {
					factor = BigDecimal.ONE.divide(factor, MathContext.DECIMAL64);
					Unit tmpUnit = factorFromUnit;
					factorFromUnit = factorToUnit;
					factorToUnit = tmpUnit;
				}

				convertedValue = value.multiply(this.getConversionFactor(fromUnit, factorFromUnit), MathContext.DECIMAL64);
				convertedValue = convertedValue.multiply(factor, MathContext.DECIMAL64);
				convertedValue = convertedValue.multiply(this.getConversionFactor(factorToUnit, toUnit), MathContext.DECIMAL64);
			} else {
				LOG.warn("Cannot convert from " + fromUnit + " to " + toUnit + ". Input value is returned.");
				throw new InconvertibleUnitTypesException(fromUnit, toUnit);
			}
		}
		return convertedValue;
	}

	private boolean isConvertible(Unit unit1, Unit unit2, boolean strictConversion) throws UnitTyeException {
		UnitType unitTypeInd1 = unit1.getUnittypeInd();
		UnitType unitTypeInd2 = unit2.getUnittypeInd();
		if (unit1.getId().equals(unit2.getId())) {
			return true;
		}
		if (unitTypeInd1.equals(UnitType._TEMPERATURE_) || unitTypeInd2.equals(UnitType._TEMPERATURE_)) {
			// TEMP unit type is not convertible
			return false;
		}
		if (!strictConversion && this.isConvertibleFromMassToVolume(unitTypeInd1, unitTypeInd2)) {
			// we can convert from mass to volume and vice versa
			return true;
		}
		return unitTypeInd1.equals(unitTypeInd2);
	}

	private boolean isConvertibleFromMassToVolume(UnitType unitTypeInd1, UnitType unitTypeInd2) {
		return (unitTypeInd1.equals(UnitType._VOLUME_) && unitTypeInd2.equals(UnitType._MASS_))
				|| (unitTypeInd1.equals(UnitType._MASS_) && unitTypeInd2.equals(UnitType._VOLUME_));
	}

	private boolean isConvertible(Unit unit1, Unit unit2) throws UnitTyeException {
		return this.isConvertible(unit1, unit2, true);
	}

	private BigDecimal getConversionFactor(Unit unit1, Unit unit2) throws UnitConversionException, UnitTyeException {
		if (this.isConvertible(unit1, unit2) && !unit2.getFactor().equals(BigDecimal.ZERO)) {
			return unit1.getFactor().divide(unit2.getFactor(), MathContext.DECIMAL64);
		}
		throw new UnitConversionException(BusinessErrorCode.UNIT_CONVERSION_FACTOR);
	}

}
