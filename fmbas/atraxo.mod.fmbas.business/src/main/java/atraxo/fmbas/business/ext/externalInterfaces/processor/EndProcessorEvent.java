/**
 *
 */
package atraxo.fmbas.business.ext.externalInterfaces.processor;

import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ApplicationContextEvent;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;

/**
 * @author zspeter
 */
public class EndProcessorEvent extends ApplicationContextEvent {

	/**
	 *
	 */
	private static final long serialVersionUID = -5241289436712423373L;
	private ExternalInterface extInt;
	private String clientCode;

	/**
	 * @param source
	 */
	public EndProcessorEvent(ApplicationContext source, ExternalInterface extInt, String clientCode) {
		super(source);
		this.extInt = extInt;
		this.clientCode = clientCode;
	}

	public ExternalInterface getExtInt() {
		return this.extInt;
	}

	public String getClientCode() {
		return this.clientCode;
	}

}
