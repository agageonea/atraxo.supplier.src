package atraxo.fmbas.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.IErrorCode;

public class TimeSeriesException extends BusinessException {

	private static final long serialVersionUID = 8083010050552272464L;

	public TimeSeriesException(IErrorCode code) {
		super(code, "Quotation is in use.");
	}

}
