/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.categories;

import atraxo.fmbas.domain.impl.categories.IataPC;
import atraxo.fmbas.domain.impl.categories.MainCategory;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link PriceCategory} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class PriceCategory_Service extends AbstractEntityService<PriceCategory> {

	/**
	 * Public constructor for PriceCategory_Service
	 */
	public PriceCategory_Service() {
		super();
	}

	/**
	 * Public constructor for PriceCategory_Service
	 * 
	 * @param em
	 */
	public PriceCategory_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<PriceCategory> getEntityClass() {
		return PriceCategory.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return PriceCategory
	 */
	public PriceCategory findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(PriceCategory.NQ_FIND_BY_NAME,
							PriceCategory.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"PriceCategory", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"PriceCategory", "name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return PriceCategory
	 */
	public PriceCategory findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(PriceCategory.NQ_FIND_BY_BUSINESS,
							PriceCategory.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"PriceCategory", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"PriceCategory", "id"), nure);
		}
	}

	/**
	 * Find by reference: mainCategory
	 *
	 * @param mainCategory
	 * @return List<PriceCategory>
	 */
	public List<PriceCategory> findByMainCategory(MainCategory mainCategory) {
		return this.findByMainCategoryId(mainCategory.getId());
	}
	/**
	 * Find by ID of reference: mainCategory.id
	 *
	 * @param mainCategoryId
	 * @return List<PriceCategory>
	 */
	public List<PriceCategory> findByMainCategoryId(Integer mainCategoryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceCategory e where e.clientId = :clientId and e.mainCategory.id = :mainCategoryId",
						PriceCategory.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("mainCategoryId", mainCategoryId).getResultList();
	}
	/**
	 * Find by reference: iata
	 *
	 * @param iata
	 * @return List<PriceCategory>
	 */
	public List<PriceCategory> findByIata(IataPC iata) {
		return this.findByIataId(iata.getId());
	}
	/**
	 * Find by ID of reference: iata.id
	 *
	 * @param iataId
	 * @return List<PriceCategory>
	 */
	public List<PriceCategory> findByIataId(Integer iataId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PriceCategory e where e.clientId = :clientId and e.iata.id = :iataId",
						PriceCategory.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("iataId", iataId).getResultList();
	}
}
