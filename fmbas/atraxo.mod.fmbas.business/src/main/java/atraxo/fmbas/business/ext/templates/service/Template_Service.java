/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.templates.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import atraxo.fmbas.business.api.notificationEvent.INotificationEventService;
import atraxo.fmbas.business.api.templates.ITemplateService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.mailMerge.service.MailMerge_Service;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import atraxo.fmbas.domain.impl.templates.Template;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.ISettings;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link Template} domain entity.
 */
public class Template_Service extends atraxo.fmbas.business.impl.templates.Template_Service implements ITemplateService {

	private static final Logger LOG = LoggerFactory.getLogger(Template_Service.class);
	@Autowired
	private MailMerge_Service mailMergeService;
	@Autowired
	private INotificationEventService notificationEventService;
	@Autowired
	private ISettings setting;

	@Override
	@Transactional
	public void insertTemplate(byte[] file, String fileName, String description, String name, String typeName, Boolean systemUse)
			throws BusinessException {
		Template template = this.generateTemplate(description, name, typeName, fileName, systemUse);
		this.checkForSystemTemplate(template, false);
		this.onInsert(template);
		this.mailMergeService.uploadTemplate(file, template.getFileReference(), template.getDescription());
	}

	@Override
	protected void preUpdate(Template e) throws BusinessException {
		super.preUpdate(e);
		this.checkForSystemTemplate(e, true);
	}

	@Override
	protected void preInsert(Template e) throws BusinessException {
		super.preInsert(e);
		this.checkForDuplicateTemplates(e);
		this.checkForSystemTemplate(e, false);
		e.setFileReference(UUID.randomUUID().toString() + "." + this.getExtension(e.getFileName()));

	}

	private void checkForSystemTemplate(Template e, boolean onUpdate) throws BusinessException {
		if (e.getSystemUse()) {
			this.checkUserAdminRole();
			Map<String, Object> params = new HashMap<>();
			params.put("type", e.getType());
			params.put("systemUse", e.getSystemUse());
			List<Template> list = this.findEntitiesByAttributes(params);
			if (onUpdate) {
				list.remove(this.findById(e.getId()));
			}
			if (!list.isEmpty()) {
				throw new BusinessException(BusinessErrorCode.EXISTENT_SYSTEM_USE_TYPE, BusinessErrorCode.EXISTENT_SYSTEM_USE_TYPE.getErrMsg());
			}

		}
	}

	private void checkUserAdminRole() throws BusinessException {
		IUser user = Session.user.get();

		if (!user.getProfile().isAdministrator()) {
			throw new BusinessException(BusinessErrorCode.NO_ADMIN_ROLE, BusinessErrorCode.NO_ADMIN_ROLE.getErrMsg());
		}
	}

	/**
	 * @param e
	 * @throws BusinessException
	 * @author aradu Checks for duplicate templates at client update.
	 */
	private void checkForDuplicateTemplates(Template e) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("name", e.getName());
		params.put("user", null);
		params.put("subsidiary", null);
		List<Template> templates = this.findEntitiesByAttributes(params);
		if (!templates.isEmpty()) {
			throw new BusinessException(BusinessErrorCode.DUPLICATE_TEMPLATE, BusinessErrorCode.DUPLICATE_TEMPLATE.getErrMsg());
		}

	}

	@Override
	protected void postInsert(Template e) throws BusinessException {
		super.postInsert(e);
		byte[] data = this.loadTemplateFromDisk(e);
		this.mailMergeService.uploadTemplate(data, e.getFileReference(), e.getDescription());
	}

	@Override
	public byte[] loadTemplateFromDisk(Template e) throws BusinessException {
		// String workspacePath = Session.user.get().getWorkspace().getWorkspacePath();
		StringBuilder sb = new StringBuilder();// workspacePath);
		String indexUrl = this.setting.getInitFileLocation();
		// indexUrl.substring(0, indexUrl.lastIndexOf('/')).concat("templates/");
		sb.append(indexUrl.substring(0, indexUrl.lastIndexOf('/')).concat("/templates/"));
		sb.append(e.getFileName());
		Path path = Paths.get(sb.toString());
		try {
			return Files.readAllBytes(path);
		} catch (IOException ioe) {
			throw new BusinessException(ErrorCode.G_FILE_NOT_FOUND, ioe);
		}
	}

	@Override
	public Template generateTemplate(String description, String name, String typeName, String fileName, Boolean systemUse) throws BusinessException {
		UserSupp user = this.getUser();
		return this.generateTemplate(user, description, name, typeName, fileName, systemUse);
	}

	@Override
	public Template generateTemplate(UserSupp user, String description, String name, String typeName, String fileName, Boolean systemUse)
			throws BusinessException {
		INotificationEventService notificationEventService = (INotificationEventService) this.findEntityService(NotificationEvent.class);
		NotificationEvent notificationEvent = notificationEventService.findByName(typeName);
		Template template = new Template();
		template.setDescription(description);
		template.setFileName(fileName);
		template.setType(notificationEvent);
		template.setName(name);
		template.setUser(user);
		template.setUploadDate(GregorianCalendar.getInstance().getTime());
		template.setSystemUse(systemUse);
		Customer subsidiary = this.getActiveSubsidiary(user);
		template.setSubsidiary(subsidiary);
		try {
			if (subsidiary != null) {
				this.findByKey(user, subsidiary, name);
			}
		} catch (ApplicationException e) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(e.getErrorCode())) {
				throw new BusinessException(BusinessErrorCode.TEMPLATE_ALREADY_EXISTS, BusinessErrorCode.TEMPLATE_ALREADY_EXISTS.getErrMsg());
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("Template not found.", e);
			}
		}
		template.setFileReference(UUID.randomUUID().toString() + "." + this.getExtension(fileName));
		return template;
	}

	private UserSupp getUser() throws BusinessException {
		String userCode = Session.user.get().getCode();
		IUserSuppService userService = (IUserSuppService) this.findEntityService(UserSupp.class);
		return userService.findByCode(userCode);
	}

	private Customer getActiveSubsidiary(UserSupp user) {
		Collection<UserSubsidiary> userSubsidiaries = user.getSubsidiaries();
		for (UserSubsidiary userSubsidiary : userSubsidiaries) {
			if (userSubsidiary.getMainSubsidiary()) {
				return userSubsidiary.getSubsidiary();
			}
		}
		return null;
	}

	@Override
	public byte[] getPreview(Template template, String jsonData) throws BusinessException {
		byte[] file = this.mailMergeService.generateDocument(template.getFileReference(), jsonData);
		if (file == null) {
			throw new BusinessException(BusinessErrorCode.TEMPLATE_NULL_PREVIEW, BusinessErrorCode.TEMPLATE_NULL_PREVIEW.getErrMsg());
		}
		return file;
	}

	@Override
	@Transactional
	public void updateTemplate(byte[] file, String fileName, Template template, String name, String description, Boolean systemUse)
			throws BusinessException {
		this.mailMergeService.updateTemplate(file, template.getFileReference(), template.getDescription());
		template.setFileName(fileName);
		template.setName(name);
		template.setDescription(description);
		this.update(template);
	}

	private String getExtension(String fileName) {
		return FilenameUtils.getExtension(fileName);
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		List<Template> templates = this.findByIds(ids);
		for (Template template : templates) {
			if (!template.getCustomerNotification().isEmpty()) {
				throw new BusinessException(BusinessErrorCode.CAN_NOT_DELETE_ACTIVE_TEMPLATE,
						BusinessErrorCode.CAN_NOT_DELETE_ACTIVE_TEMPLATE.getErrMsg());
			}
		}
	}

	/*
	 * Returns the newest template used for Bid export (non-Javadoc)
	 * @see atraxo.fmbas.business.api.templates.ITemplateService#getTemplateForExport()
	 */
	@Override
	public Template getTemplateForExport() throws BusinessException {
		try {
			NotificationEvent notificationEvent = this.notificationEventService.findByName("Bid export");
			List<Template> templatesList = this.findByType(notificationEvent);
			templatesList.sort((o1, o2) -> o2.getModifiedAt().compareTo(o1.getModifiedAt()));
			return templatesList.get(0);
		} catch (IndexOutOfBoundsException | ApplicationException e) {
			throw new BusinessException(BusinessErrorCode.TEMPLATE_NOT_FOUND, BusinessErrorCode.TEMPLATE_NOT_FOUND.getErrMsg(), e);
		}
	}
}
