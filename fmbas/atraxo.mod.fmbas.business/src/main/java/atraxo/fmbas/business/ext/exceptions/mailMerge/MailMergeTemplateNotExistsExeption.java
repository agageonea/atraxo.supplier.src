package atraxo.fmbas.business.ext.exceptions.mailMerge;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author abolindu
 */
public class MailMergeTemplateNotExistsExeption extends BusinessException {

	private static final long serialVersionUID = -5767641033619332327L;

	/**
	 * @param exception
	 */
	public MailMergeTemplateNotExistsExeption(Throwable exception) {
		super(BusinessErrorCode.TEMPLATE_NOT_EXISTS, BusinessErrorCode.TEMPLATE_NOT_EXISTS.getErrMsg(), exception);
	}

	/**
	 * Template not exists exception.
	 */
	public MailMergeTemplateNotExistsExeption() {
		super(BusinessErrorCode.TEMPLATE_NOT_EXISTS, BusinessErrorCode.TEMPLATE_NOT_EXISTS.getErrMsg());
	}

}
