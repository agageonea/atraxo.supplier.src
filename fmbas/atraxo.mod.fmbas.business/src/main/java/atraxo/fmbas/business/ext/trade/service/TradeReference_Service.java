/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.trade.service;

import atraxo.fmbas.business.api.trade.ITradeReferenceService;
import atraxo.fmbas.domain.impl.trade.TradeReference;

/**
 * Business extensions specific for {@link TradeReference} domain entity.
 */
public class TradeReference_Service extends atraxo.fmbas.business.impl.trade.TradeReference_Service implements ITradeReferenceService {

	// Implement me

}
