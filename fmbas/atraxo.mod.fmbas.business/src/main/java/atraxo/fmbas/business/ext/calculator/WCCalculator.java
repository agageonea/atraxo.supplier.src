package atraxo.fmbas.business.ext.calculator;

import java.util.List;

import atraxo.fmbas.business.api.timeserie.ITimeSerieItemService;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Calculator implementation to calculate weekly averages.
 *
 * @author zspeter
 */
public class WCCalculator extends AbstractWeeklyCalculator implements Calculator {

	@Override
	public List<TimeSerieItem> getItemsForCalculation(List<TimeSerieItem> timeSerieItems, ITimeSerieItemService service) throws BusinessException {
		return timeSerieItems;

	}

}
