/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.workflow;

import atraxo.ad.domain.impl.security.User;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link WorkflowInstance} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class WorkflowInstance_Service
		extends
			AbstractEntityService<WorkflowInstance> {

	/**
	 * Public constructor for WorkflowInstance_Service
	 */
	public WorkflowInstance_Service() {
		super();
	}

	/**
	 * Public constructor for WorkflowInstance_Service
	 * 
	 * @param em
	 */
	public WorkflowInstance_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<WorkflowInstance> getEntityClass() {
		return WorkflowInstance.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return WorkflowInstance
	 */
	public WorkflowInstance findByKey(Workflow workflow, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(WorkflowInstance.NQ_FIND_BY_KEY,
							WorkflowInstance.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("workflow", workflow)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"WorkflowInstance", "workflow, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"WorkflowInstance", "workflow, name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return WorkflowInstance
	 */
	public WorkflowInstance findByKey(Long workflowId, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							WorkflowInstance.NQ_FIND_BY_KEY_PRIMITIVE,
							WorkflowInstance.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("workflowId", workflowId)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"WorkflowInstance", "workflowId, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"WorkflowInstance", "workflowId, name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return WorkflowInstance
	 */
	public WorkflowInstance findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(WorkflowInstance.NQ_FIND_BY_BUSINESS,
							WorkflowInstance.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"WorkflowInstance", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"WorkflowInstance", "id"), nure);
		}
	}

	/**
	 * Find by reference: workflow
	 *
	 * @param workflow
	 * @return List<WorkflowInstance>
	 */
	public List<WorkflowInstance> findByWorkflow(Workflow workflow) {
		return this.findByWorkflowId(workflow.getId());
	}
	/**
	 * Find by ID of reference: workflow.id
	 *
	 * @param workflowId
	 * @return List<WorkflowInstance>
	 */
	public List<WorkflowInstance> findByWorkflowId(Integer workflowId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from WorkflowInstance e where e.clientId = :clientId and e.workflow.id = :workflowId",
						WorkflowInstance.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("workflowId", workflowId).getResultList();
	}
	/**
	 * Find by reference: startExecutionUser
	 *
	 * @param startExecutionUser
	 * @return List<WorkflowInstance>
	 */
	public List<WorkflowInstance> findByStartExecutionUser(
			User startExecutionUser) {
		return this.findByStartExecutionUserId(startExecutionUser.getId());
	}
	/**
	 * Find by ID of reference: startExecutionUser.id
	 *
	 * @param startExecutionUserId
	 * @return List<WorkflowInstance>
	 */
	public List<WorkflowInstance> findByStartExecutionUserId(
			String startExecutionUserId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from WorkflowInstance e where e.clientId = :clientId and e.startExecutionUser.id = :startExecutionUserId",
						WorkflowInstance.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("startExecutionUserId", startExecutionUserId)
				.getResultList();
	}
	/**
	 * Find by reference: entitites
	 *
	 * @param entitites
	 * @return List<WorkflowInstance>
	 */
	public List<WorkflowInstance> findByEntitites(
			WorkflowInstanceEntity entitites) {
		return this.findByEntititesId(entitites.getId());
	}
	/**
	 * Find by ID of reference: entitites.id
	 *
	 * @param entititesId
	 * @return List<WorkflowInstance>
	 */
	public List<WorkflowInstance> findByEntititesId(Integer entititesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from WorkflowInstance e, IN (e.entitites) c where e.clientId = :clientId and c.id = :entititesId",
						WorkflowInstance.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("entititesId", entititesId).getResultList();
	}
}
