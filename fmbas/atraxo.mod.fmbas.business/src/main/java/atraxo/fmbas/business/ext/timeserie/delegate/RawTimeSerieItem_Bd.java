package atraxo.fmbas.business.ext.timeserie.delegate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.BeanUtils;

import atraxo.fmbas.domain.impl.timeserie.RawTimeSerieItem;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class RawTimeSerieItem_Bd extends AbstractBusinessDelegate {
	/**
	 * Currently processed item
	 */
	private RawTimeSerieItem crt;
	/**
	 * Previous existing item
	 */
	private RawTimeSerieItem prev;
	/**
	 * Next existing item
	 */
	private RawTimeSerieItem next;
	private RawTimeSerieItem tSerieItemFirst;
	private RawTimeSerieItem tSerieItemLast;

	/**
	 * Fill the gap between two RawTimeSerieItem.
	 *
	 * @param RawTimeSerieItem
	 * @param TimeSerie
	 * @return List<RawTimeSerieItem> list of generated RawTimeSerieItem
	 * @throws BusinessException
	 */
	// public List<RawTimeSerieItem> fillTheGap(RawTimeSerieItem tsi, TimeSerie ts) throws BusinessException {
	// // Get the last used positions
	// Date firstUsage = ts.getFirstUsage();
	// Date lastUsage = ts.getLastUsage();
	// Date itemDate = tsi.getItemDate();
	// List<RawTimeSerieItem> listTSI = new ArrayList<RawTimeSerieItem>();
	//
	// // Check if exists smaller and bigger item, if no the time series first
	// // and the last element will point to the actual item.
	// if (firstUsage == null) {
	// ts.setFirstUsage(itemDate);
	// firstUsage = itemDate;
	// }
	// if (lastUsage == null) {
	// ts.setLastUsage(itemDate);
	// lastUsage = itemDate;
	// }
	// if (this.tSerieItemLast != null && this.tSerieItemLast.getTserie() == null) {
	// this.tSerieItemLast.setTserie(ts);
	// }
	// if (firstUsage.after(itemDate)) {// this.isFirstBigger(firstUsage,
	// // itemDate)) {
	// ts.setFirstUsage(itemDate);
	// return this.generateListOfItem(tsi, DateUtils.addDays(firstUsage, -1));
	// } else if (itemDate.after(lastUsage)) {// this.isFirstBigger(itemDate,
	// // lastUsage)) {
	// ts.setLastUsage(itemDate);
	// return this.generateListOfItem(this.tSerieItemLast, DateUtils.addDays(itemDate, -1));
	// }
	// return listTSI;
	// }

	/**
	 * Check if the first date is bigger
	 *
	 * @param date1
	 * @param date2
	 * @return true or false;
	 */
	// @Deprecated
	// public boolean isFirstBigger(Date date1, Date date2) {
	// Calendar cal1 = Calendar.getInstance();
	// Calendar cal2 = Calendar.getInstance();
	// cal1.setTime(date1);
	// cal2.setTime(date2);
	//
	// return cal1.after(cal2);
	//
	// }

	/**
	 * Compare two dates if are equals
	 *
	 * @param date1
	 * @param date2
	 * @return True or false
	 */
	// @Deprecated
	// public boolean isDatesEqual(Date date1, Date date2) {
	//
	// Calendar cal1 = Calendar.getInstance();
	// Calendar cal2 = Calendar.getInstance();
	//
	// cal1.setTime(date1);
	// cal2.setTime(date2);
	//
	// return cal1.equals(cal2);
	// }

	/**
	 * If the given item is on a Friday, return the new elements for Saturday and Sunday
	 *
	 * @return List of generated item if is Friday
	 */
	// public List<RawTimeSerieItem> processFriday() {
	//
	// RawTimeSerieItem c = this.crt;
	// return this.processFriday(c);
	// }

	/**
	 * If the given item is on a Friday, return the new elements for Saturday and Sunday
	 *
	 * @return List<RawTimeSerieItem>
	 */
	public List<RawTimeSerieItem> processFriday(RawTimeSerieItem c, Map<Date, RawTimeSerieItem> existingItems) {
		List<RawTimeSerieItem> result = new ArrayList<RawTimeSerieItem>();
		RawTimeSerieItem newItem = null;
		if (this.isThisWeekDay(c.getItemDate(), Calendar.FRIDAY)) {
			Date saturday = DateUtils.addDays(c.getItemDate(), 1);
			if (!existingItems.containsKey(saturday)) {
				newItem = this.cloneItem(c, saturday);
				result.add(newItem);
			}

			Date sunday = DateUtils.addDays(c.getItemDate(), 2);
			if (!existingItems.containsKey(sunday)) {
				newItem = this.cloneItem(c, sunday);
				result.add(newItem);
			}
		}

		if (this.isThisWeekDay(c.getItemDate(), Calendar.SATURDAY)) {
			Date sunday = DateUtils.addDays(c.getItemDate(), 1);

			if (!existingItems.containsKey(sunday)) {
				newItem = this.cloneItem(c, sunday);
				result.add(newItem);
			}
		}

		return result;
	}

	/**
	 * Create a clone (new instance) of the given entity
	 *
	 * @param date
	 * @param RawTimeSerieItem
	 * @return The pramater's clone
	 */
	public RawTimeSerieItem cloneItem(RawTimeSerieItem s, Date date) {
		RawTimeSerieItem t = new RawTimeSerieItem();
		BeanUtils.copyProperties(s, t);
		t.setId(null);
		t.setItemDate(date);
		t.setCalculated(true);
		t.setActive(true);
		return t;
	}

	/**
	 * @param Time
	 *            Series item witch was clone
	 * @param The
	 *            data to clone
	 * @return List of clones between the time serie item date and the given date
	 */
	// public List<RawTimeSerieItem> generateListOfItem(RawTimeSerieItem s, Date upTo) {
	//
	// return this.generateListOfItem(s, s.getItemDate(), upTo);
	//
	// }

	/**
	 * Generate a list of elements between dates
	 *
	 * @param s
	 * @param fromTo
	 * @param upTo
	 * @return
	 */
	public List<RawTimeSerieItem> generateListOfItem(RawTimeSerieItem s, Date from, Date upTo) {

		Calendar calUpTo = Calendar.getInstance();
		Calendar calItemDate = Calendar.getInstance();

		Date loopDate = (Date) from.clone();

		calUpTo.setTime(upTo);
		calItemDate.setTime(loopDate);
		List<RawTimeSerieItem> list = new ArrayList<RawTimeSerieItem>();

		while ((calUpTo.get(Calendar.YEAR) >= calItemDate.get(Calendar.YEAR)
				&& calUpTo.get(Calendar.DAY_OF_YEAR) > calItemDate.get(Calendar.DAY_OF_YEAR))
				|| calUpTo.get(Calendar.YEAR) > calItemDate.get(Calendar.YEAR)) {

			loopDate = DateUtils.addDays(loopDate, 1);
			RawTimeSerieItem tempTimeSerieItem = this.cloneItem(s, loopDate);
			calItemDate.setTime(loopDate);
			list.add(tempTimeSerieItem);
		}
		return list;
	}

	/**
	 * Verifies if <code>date</code> is the same week day as it is marked by <code>day</code>.
	 *
	 * @param date
	 * @param day
	 * @return
	 */
	public Boolean isThisWeekDay(Date date, int day) {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);
		if (cal1.get(Calendar.DAY_OF_WEEK) == day) {
			return true;
		}
		return false;
	}

	/**
	 * Change the item value for each element
	 *
	 * @param tsiList
	 * @param value
	 * @return List<RawTimeSerieItem>
	 */
	public void changeItemValue(List<RawTimeSerieItem> tsiList, BigDecimal value) {
		for (RawTimeSerieItem tsi : tsiList) {
			tsi.setItemValue(value);
			tsi.setCalculated(true);
		}
	}

	/**
	 * @return
	 */
	public RawTimeSerieItem getCrt() {
		return this.crt;
	}

	/**
	 * @param crt
	 */
	public void setCrt(RawTimeSerieItem crt) {
		this.crt = crt;
	}

	/**
	 * @return
	 */
	public RawTimeSerieItem getPrev() {
		return this.prev;
	}

	/**
	 * @param prev
	 */
	public void setPrev(RawTimeSerieItem prev) {
		this.prev = prev;
	}

	/**
	 * @return
	 */
	public RawTimeSerieItem getNext() {
		return this.next;
	}

	public void setNext(RawTimeSerieItem next) {
		this.next = next;
	}

	/**
	 * @return the tSerieItemLast
	 */
	public RawTimeSerieItem gettSerieItemLast() {
		return this.tSerieItemLast;
	}

	/**
	 * @param tSerieItemLast
	 *            the tSerieItemLast to set
	 */
	public void settSerieItemLast(RawTimeSerieItem tSerieItemLast) {
		this.tSerieItemLast = tSerieItemLast;
	}

	/**
	 * @return the tSerieItemFirst
	 */
	public RawTimeSerieItem gettSerieItemFirst() {
		return this.tSerieItemFirst;
	}

	/**
	 * @param tSerieItemFirst
	 *            the tSerieItemFirst to set
	 */
	public void settSerieItemFirst(RawTimeSerieItem tSerieItemFirst) {
		this.tSerieItemFirst = tSerieItemFirst;
	}

}
