/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.commodities;

import atraxo.fmbas.domain.impl.commodities.Commoditie;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Commoditie} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Commoditie_Service extends AbstractEntityService<Commoditie> {

	/**
	 * Public constructor for Commoditie_Service
	 */
	public Commoditie_Service() {
		super();
	}

	/**
	 * Public constructor for Commoditie_Service
	 * 
	 * @param em
	 */
	public Commoditie_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Commoditie> getEntityClass() {
		return Commoditie.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Commoditie
	 */
	public Commoditie findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Commoditie.NQ_FIND_BY_CODE,
							Commoditie.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Commoditie", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Commoditie", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Commoditie
	 */
	public Commoditie findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Commoditie.NQ_FIND_BY_BUSINESS,
							Commoditie.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Commoditie", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Commoditie", "id"), nure);
		}
	}

}
