/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.quotation;

import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link QuotationValue} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class QuotationValue_Service
		extends
			AbstractEntityService<QuotationValue> {

	/**
	 * Public constructor for QuotationValue_Service
	 */
	public QuotationValue_Service() {
		super();
	}

	/**
	 * Public constructor for QuotationValue_Service
	 * 
	 * @param em
	 */
	public QuotationValue_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<QuotationValue> getEntityClass() {
		return QuotationValue.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return QuotationValue
	 */
	public QuotationValue findByBusinessKey(Quotation quotation,
			Date validFromDate, Date validToDate) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(QuotationValue.NQ_FIND_BY_BUSINESSKEY,
							QuotationValue.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("quotation", quotation)
					.setParameter("validFromDate", validFromDate)
					.setParameter("validToDate", validToDate).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"QuotationValue",
							"quotation, validFromDate, validToDate"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"QuotationValue",
							"quotation, validFromDate, validToDate"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return QuotationValue
	 */
	public QuotationValue findByBusinessKey(Long quotationId,
			Date validFromDate, Date validToDate) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							QuotationValue.NQ_FIND_BY_BUSINESSKEY_PRIMITIVE,
							QuotationValue.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("quotationId", quotationId)
					.setParameter("validFromDate", validFromDate)
					.setParameter("validToDate", validToDate).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"QuotationValue",
							"quotationId, validFromDate, validToDate"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"QuotationValue",
							"quotationId, validFromDate, validToDate"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return QuotationValue
	 */
	public QuotationValue findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(QuotationValue.NQ_FIND_BY_BUSINESS,
							QuotationValue.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"QuotationValue", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"QuotationValue", "id"), nure);
		}
	}

	/**
	 * Find by reference: quotation
	 *
	 * @param quotation
	 * @return List<QuotationValue>
	 */
	public List<QuotationValue> findByQuotation(Quotation quotation) {
		return this.findByQuotationId(quotation.getId());
	}
	/**
	 * Find by ID of reference: quotation.id
	 *
	 * @param quotationId
	 * @return List<QuotationValue>
	 */
	public List<QuotationValue> findByQuotationId(Integer quotationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from QuotationValue e where e.clientId = :clientId and e.quotation.id = :quotationId",
						QuotationValue.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("quotationId", quotationId).getResultList();
	}
}
