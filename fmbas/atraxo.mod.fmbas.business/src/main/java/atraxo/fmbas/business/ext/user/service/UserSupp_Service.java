/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.user.service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.transaction.annotation.Transactional;

import atraxo.ad.business.api.system.IClientService;
import atraxo.ad.domain.impl.system.Client;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.util.EmailUtil;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.enums.SysParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.exceptions.InvalidConfiguration;
import seava.j4e.api.security.IPasswordValidator;
import seava.j4e.api.service.IClientInfoProvider;
import seava.j4e.api.session.IClient;
import seava.j4e.api.session.IOrganization;
import seava.j4e.api.session.ISessionUser;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.IUserProfile;
import seava.j4e.api.session.IUserSettings;
import seava.j4e.api.session.IWorkspace;
import seava.j4e.api.session.Session;
import seava.j4e.commons.security.AppClient;
import seava.j4e.commons.security.AppUser;
import seava.j4e.commons.security.AppUserProfile;
import seava.j4e.commons.security.AppUserSettings;
import seava.j4e.commons.security.Organization;

/**
 * Business extensions specific for {@link UserSupp} domain entity.
 */
public class UserSupp_Service extends atraxo.fmbas.business.impl.user.UserSupp_Service implements IUserSuppService {

	@Override
	public void updateUserSessionProfile(Object obj) throws BusinessException {
		if (obj instanceof ISessionUser) {
			ISessionUser su = (ISessionUser) obj;
			AppClient client = (AppClient) su.getUser().getClient();
			UserSupp u = this.findByCode(su.getUser().getCode());
			Collection<UserSubsidiary> subsidiaries = u.getSubsidiaries();
			boolean hasDefault = false;
			if (subsidiaries != null) {
				for (UserSubsidiary subsidiary : subsidiaries) {
					if (!hasDefault) {
						hasDefault = subsidiary.getMainSubsidiary();
					}
					if (subsidiary.getMainSubsidiary()) {
						client.setActiveSubsidiaryId(subsidiary.getSubsidiary().getRefid());
					}
					IOrganization org = new Organization(subsidiary.getSubsidiary().getCode(), subsidiary.getSubsidiary().getName(),
							subsidiary.getMainSubsidiary(), subsidiary.getSubsidiary().getRefid());
					su.getUser().getProfile().addOrganisation(org);
				}
			}
			ICustomerService srv = (ICustomerService) this.findEntityService(Customer.class);
			Customer customer = srv.findByCode(su.getUser().getClientCode());
			// IOrganization org = new Organization(customer.getCode(), customer.getName(), !hasDefault, customer.getRefid());
			// su.getUser().getProfile().addOrganisation(org);
		}
	}

	@Override
	protected void preInsert(UserSupp e) throws BusinessException {
		if (e.getPassword() == null || "".equals(e.getPassword())) {
			e.setPassword(this.encryptPassword(e.getLoginName()));
		}
		e.setCode(e.getLoginName());
		this.checkEmail(e);
	}

	@Override
	protected void preUpdate(UserSupp e) throws BusinessException {
		e.setCode(e.getLoginName());
		this.checkEmail(e);
	}

	private void checkEmail(UserSupp user) throws BusinessException {
		if (user.getEmail() != null && !user.getEmail().matches("") && !EmailUtil.validateEmail(user.getEmail())) {
			throw new BusinessException(BusinessErrorCode.CONTACT_EMAIL, BusinessErrorCode.CONTACT_EMAIL.getErrMsg());
		}
	}

	@Override
	@Transactional
	public void doChangePassword(String userId, String newPassword) throws BusinessException {

		UserSupp u = this.findById(userId);
		if (!u.getClientId().equals(Session.user.get().getClientId())) {
			throw new BusinessException(BusinessErrorCode.ACCES_TO_DIFFERENT_CLIENT, BusinessErrorCode.ACCES_TO_DIFFERENT_CLIENT.getErrMsg());
		}
		try {
			this.getApplicationContext().getBean(IPasswordValidator.class).validate(newPassword);
		} catch (Exception e) {
			throw new BusinessException(BusinessErrorCode.PASSWORD_VALIDATION, BusinessErrorCode.PASSWORD_VALIDATION.getErrMsg(), e);
		}
		u.setPassword(this.encryptPassword(newPassword));
		this.getEntityManager().merge(u);

	}

	private String encryptPassword(String password) throws BusinessException {
		MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new BusinessException(BusinessErrorCode.NO_ALGORITH_FOUND, BusinessErrorCode.NO_ALGORITH_FOUND.getErrMsg(), e);
		}
		messageDigest.update(password.getBytes(), 0, password.length());
		String hashedPass = new BigInteger(1, messageDigest.digest()).toString(16);
		if (hashedPass.length() < 32) {
			hashedPass = "0" + hashedPass;
		}
		return hashedPass;
	}

	@Override
	public void createSessionUser(String clientCode) throws BusinessException {
		try {
			String userCode = this.getSettings().getParam(SysParam.CORE_JOB_USER.name());
			IClientService clientSrv = (IClientService) this.findEntityService(Client.class);
			Client c = clientSrv.findByCode(clientCode);
			IClient client = new AppClient(c.getId(), clientCode, "");
			IUserSettings settings;
			settings = AppUserSettings.newInstance(this.getSettings());
			IUserProfile profile = new AppUserProfile(true, null, false, false, false);

			// create an incomplete user first
			IUser user = new AppUser(userCode, userCode, null, userCode, null, null, client, settings, profile, null, true);
			Session.user.set(user);

			// get the client workspace info
			IWorkspace ws = this.getApplicationContext().getBean(IClientInfoProvider.class).getClientWorkspace();
			user = new AppUser(userCode, userCode, null, userCode, null, null, client, settings, profile, ws, true);
			Session.user.set(user);
		} catch (InvalidConfiguration e) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}
	}

	@Override
	public void createSessionUser(String clientCode, String userCode, boolean useSubsidiaries) throws BusinessException {
		try {
			IClientService clientSrv = (IClientService) this.findEntityService(Client.class);
			Client c = clientSrv.findByCode(clientCode);
			IClient client = new AppClient(c.getId(), clientCode, "");
			IUserSettings settings;
			settings = AppUserSettings.newInstance(this.getSettings());
			IUserProfile profile = new AppUserProfile(true, null, false, false, false);

			// create an incomplete user first
			IUser user = new AppUser(userCode, userCode, null, userCode, null, null, client, settings, profile, null, true);
			Session.user.set(user);

			// get the client workspace info
			IWorkspace ws = this.getApplicationContext().getBean(IClientInfoProvider.class).getClientWorkspace();
			user = new AppUser(userCode, userCode, null, userCode, null, null, client, settings, profile, ws, true);
			Session.user.set(user);

			Message<IUser> message = MessageBuilder.withPayload(user).build();
			this.getApplicationContext().getBean("updateJobSessionChannel", MessageChannel.class).send(message);

		} catch (InvalidConfiguration e) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}
	}

}
