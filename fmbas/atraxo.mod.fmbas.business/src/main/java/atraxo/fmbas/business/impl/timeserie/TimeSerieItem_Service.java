/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.timeserie;

import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link TimeSerieItem} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class TimeSerieItem_Service extends AbstractEntityService<TimeSerieItem> {

	/**
	 * Public constructor for TimeSerieItem_Service
	 */
	public TimeSerieItem_Service() {
		super();
	}

	/**
	 * Public constructor for TimeSerieItem_Service
	 * 
	 * @param em
	 */
	public TimeSerieItem_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<TimeSerieItem> getEntityClass() {
		return TimeSerieItem.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return TimeSerieItem
	 */
	public TimeSerieItem findByTsi_business(TimeSerie tserie, Date itemDate) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(TimeSerieItem.NQ_FIND_BY_TSI_BUSINESS,
							TimeSerieItem.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("tserie", tserie)
					.setParameter("itemDate", itemDate).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TimeSerieItem", "tserie, itemDate"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TimeSerieItem", "tserie, itemDate"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return TimeSerieItem
	 */
	public TimeSerieItem findByTsi_business(Long tserieId, Date itemDate) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							TimeSerieItem.NQ_FIND_BY_TSI_BUSINESS_PRIMITIVE,
							TimeSerieItem.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("tserieId", tserieId)
					.setParameter("itemDate", itemDate).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TimeSerieItem", "tserieId, itemDate"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TimeSerieItem", "tserieId, itemDate"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return TimeSerieItem
	 */
	public TimeSerieItem findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(TimeSerieItem.NQ_FIND_BY_BUSINESS,
							TimeSerieItem.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TimeSerieItem", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TimeSerieItem", "id"), nure);
		}
	}

	/**
	 * Find by reference: tserie
	 *
	 * @param tserie
	 * @return List<TimeSerieItem>
	 */
	public List<TimeSerieItem> findByTserie(TimeSerie tserie) {
		return this.findByTserieId(tserie.getId());
	}
	/**
	 * Find by ID of reference: tserie.id
	 *
	 * @param tserieId
	 * @return List<TimeSerieItem>
	 */
	public List<TimeSerieItem> findByTserieId(Integer tserieId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TimeSerieItem e where e.clientId = :clientId and e.tserie.id = :tserieId",
						TimeSerieItem.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("tserieId", tserieId).getResultList();
	}
}
