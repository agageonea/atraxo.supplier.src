package atraxo.fmbas.business.ext.exchangerate.job.importBloomberg.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vhojda
 */
public class ExchangeRateResponseDTO {

	private List<ExchangeRateDateDTO> rates;

	private String securityExceptionMessage;

	private String communicationExceptionMessage;

	/**
	 *
	 */
	public ExchangeRateResponseDTO() {
		this.rates = new ArrayList<>();
	}

	public List<ExchangeRateDateDTO> getRates() {
		return this.rates;
	}

	public void setRates(List<ExchangeRateDateDTO> rates) {
		this.rates = rates;
	}

	public void addRate(ExchangeRateDateDTO dto) {
		this.rates.add(dto);
	}

	public String getSecurityExceptionMessage() {
		return this.securityExceptionMessage;
	}

	public void setSecurityExceptionMessage(String securityExceptionMessage) {
		this.securityExceptionMessage = securityExceptionMessage;
	}

	public String getCommunicationExceptionMessage() {
		return this.communicationExceptionMessage;
	}

	public void setCommunicationExceptionMessage(String communicationExceptionMessage) {
		this.communicationExceptionMessage = communicationExceptionMessage;
	}

	@Override
	public String toString() {
		return "ExchangeRateResponseDTO [rates=" + this.rates + ", securityExceptionMessage=" + this.securityExceptionMessage
				+ ", communicationExceptionMessage=" + this.communicationExceptionMessage + "]";
	}

}
