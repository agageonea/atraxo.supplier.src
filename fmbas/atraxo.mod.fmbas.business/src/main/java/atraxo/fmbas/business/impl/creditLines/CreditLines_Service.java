/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.creditLines;

import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link CreditLines} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class CreditLines_Service extends AbstractEntityService<CreditLines> {

	/**
	 * Public constructor for CreditLines_Service
	 */
	public CreditLines_Service() {
		super();
	}

	/**
	 * Public constructor for CreditLines_Service
	 * 
	 * @param em
	 */
	public CreditLines_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<CreditLines> getEntityClass() {
		return CreditLines.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return CreditLines
	 */
	public CreditLines findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(CreditLines.NQ_FIND_BY_BUSINESS,
							CreditLines.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"CreditLines", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"CreditLines", "id"), nure);
		}
	}

	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<CreditLines>
	 */
	public List<CreditLines> findByCurrency(Currencies currency) {
		return this.findByCurrencyId(currency.getId());
	}
	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<CreditLines>
	 */
	public List<CreditLines> findByCurrencyId(Integer currencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from CreditLines e where e.clientId = :clientId and e.currency.id = :currencyId",
						CreditLines.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currencyId", currencyId).getResultList();
	}
	/**
	 * Find by reference: supplier
	 *
	 * @param supplier
	 * @return List<CreditLines>
	 */
	public List<CreditLines> findBySupplier(Suppliers supplier) {
		return this.findBySupplierId(supplier.getId());
	}
	/**
	 * Find by ID of reference: supplier.id
	 *
	 * @param supplierId
	 * @return List<CreditLines>
	 */
	public List<CreditLines> findBySupplierId(Integer supplierId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from CreditLines e where e.clientId = :clientId and e.supplier.id = :supplierId",
						CreditLines.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("supplierId", supplierId).getResultList();
	}
	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<CreditLines>
	 */
	public List<CreditLines> findByCustomer(Customer customer) {
		return this.findByCustomerId(customer.getId());
	}
	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<CreditLines>
	 */
	public List<CreditLines> findByCustomerId(Integer customerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from CreditLines e where e.clientId = :clientId and e.customer.id = :customerId",
						CreditLines.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerId", customerId).getResultList();
	}
}
