package atraxo.fmbas.business.ext.externalInterfaces.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceHistoryService;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceHistory;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * @author zspeter
 */
public class ExternalInterfaceHistoryFacade extends AbstractBusinessBaseService {
	@Autowired
	private IExternalInterfaceHistoryService srv;

	/**
	 * @param e
	 * @throws BusinessException
	 */
	public void insert(ExternalInterfaceHistory e) throws BusinessException {
		this.srv.insert(e);
	}

	/**
	 * @param list
	 * @throws BusinessException
	 */
	public void insert(List<ExternalInterfaceHistory> list) throws BusinessException {
		this.srv.insert(list);
	}

	/**
	 * @param e
	 * @throws BusinessException
	 */
	public synchronized void update(ExternalInterfaceHistory e) throws BusinessException {
		this.srv.update(e);
	}

	/**
	 * @param list
	 * @throws BusinessException
	 */
	public synchronized void update(List<ExternalInterfaceHistory> list) throws BusinessException {
		this.srv.update(list);
	}

	/**
	 * @param id
	 * @return
	 */
	public ExternalInterfaceHistory findById(Object id) {
		return this.srv.findById(id);
	}

	/**
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	public ExternalInterfaceHistory findByMsgId(String msgId) throws BusinessException {
		return this.srv.findByMsgId(msgId);
	}

	/**
	 * @param success
	 * @param messageId
	 * @throws BusinessException
	 */
	public void updateHistoryOutboundFromACK(Boolean success, String messageId, Boolean canHaveNegativeACK) throws BusinessException {
		this.srv.updateHistoryOutboundFromACK(success, messageId, canHaveNegativeACK);
	}

}
