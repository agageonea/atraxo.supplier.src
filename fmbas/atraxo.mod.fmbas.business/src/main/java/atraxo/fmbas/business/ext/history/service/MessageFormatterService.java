package atraxo.fmbas.business.ext.history.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.ext.history.IMessageFormatterService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import seava.j4e.api.exceptions.BusinessException;

public class MessageFormatterService implements IMessageFormatterService {

	private static final String ENTITY = "entityFqn";

	@Autowired
	private ISystemParameterService sysParameterService;

	@Override
	@SuppressWarnings("rawtypes")
	public Object formatArguments(Object object) throws BusinessException {
		if (object instanceof LinkedHashMap) {

			return ((LinkedHashMap) object).get("code");

		} else if (object.getClass().equals(Date.class)) {

			return this.formatDate((Date) object);

		} else if (object.getClass().equals(BigDecimal.class)) {

			return this.formatBigDecimal((BigDecimal) object);

		} else if (object.getClass().equals(Long.class)) {

			return this.convertToDate((Long) object);
		}
		return object.toString();
	}

	private String formatDate(Date date) {
		String format = "dd MMM yyyy";
		return new SimpleDateFormat(format).format(date);
	}

	private String formatBigDecimal(BigDecimal number) throws BusinessException {
		int decimalsNumber = this.sysParameterService.getDecimalsForAmount();
		return number.setScale(decimalsNumber, RoundingMode.HALF_UP).toPlainString();
	}

	private String convertToDate(Long timestamp) {
		Calendar mydate = Calendar.getInstance();
		mydate.setTimeInMillis(timestamp);
		return this.formatDate(mydate.getTime());
	}
}
