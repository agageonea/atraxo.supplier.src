/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.workflow;

import atraxo.fmbas.business.api.workflow.IWorkflowParameterService;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link WorkflowParameter} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class WorkflowParameter_Service
		extends
			AbstractEntityService<WorkflowParameter>
		implements
			IWorkflowParameterService {

	/**
	 * Public constructor for WorkflowParameter_Service
	 */
	public WorkflowParameter_Service() {
		super();
	}

	/**
	 * Public constructor for WorkflowParameter_Service
	 * 
	 * @param em
	 */
	public WorkflowParameter_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<WorkflowParameter> getEntityClass() {
		return WorkflowParameter.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return WorkflowParameter
	 */
	public WorkflowParameter findByKey(Workflow workflow, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(WorkflowParameter.NQ_FIND_BY_KEY,
							WorkflowParameter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("workflow", workflow)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"WorkflowParameter", "workflow, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"WorkflowParameter", "workflow, name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return WorkflowParameter
	 */
	public WorkflowParameter findByKey(Long workflowId, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							WorkflowParameter.NQ_FIND_BY_KEY_PRIMITIVE,
							WorkflowParameter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("workflowId", workflowId)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"WorkflowParameter", "workflowId, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"WorkflowParameter", "workflowId, name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return WorkflowParameter
	 */
	public WorkflowParameter findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(WorkflowParameter.NQ_FIND_BY_BUSINESS,
							WorkflowParameter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"WorkflowParameter", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"WorkflowParameter", "id"), nure);
		}
	}

	/**
	 * Find by reference: workflow
	 *
	 * @param workflow
	 * @return List<WorkflowParameter>
	 */
	public List<WorkflowParameter> findByWorkflow(Workflow workflow) {
		return this.findByWorkflowId(workflow.getId());
	}
	/**
	 * Find by ID of reference: workflow.id
	 *
	 * @param workflowId
	 * @return List<WorkflowParameter>
	 */
	public List<WorkflowParameter> findByWorkflowId(Integer workflowId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from WorkflowParameter e where e.clientId = :clientId and e.workflow.id = :workflowId",
						WorkflowParameter.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("workflowId", workflowId).getResultList();
	}
}
