package atraxo.fmbas.business.ext.bpm.delegate.exchangeRate.approval;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.custom.workflow.IWorkflowTerminatorService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * Terminator class for Exchange Rate Time Series approval workflow
 *
 * @author aradu
 */
public class ExchangeRateTimeSeriesApprovalWorkflowTerminator extends AbstractBusinessBaseService implements IWorkflowTerminatorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExchangeRateTimeSeriesApprovalWorkflowTerminator.class);

	private static final String APPROVAL_WORKFLOW_TERMINATED_COMMENT = "Approval workflow terminated.";
	private static final String REJECTED_COMMENT = "Rejected";

	@Autowired
	private ITimeSerieService timeSeriesService;

	@Override
	public void terminateWorkflow(WorkflowInstance workflowInstance, String reason) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}

		List<WorkflowInstanceEntity> entities = new ArrayList<>(workflowInstance.getEntitites());

		if (!CollectionUtils.isEmpty(entities)) {
			for (WorkflowInstanceEntity entity : entities) {
				// set the Exchange Rate time series status to rejected (if found)
				TimeSerie exchangeRateTimeSeries = null;
				try {
					exchangeRateTimeSeries = this.timeSeriesService.findByBusiness(entity.getObjectId());
				} catch (Exception e) {
					LOGGER.warn("WARNING: could not retrieve an exchange rate time serie for ID " + entity.getObjectId()
							+ " ! Will not update the entity since it doesn't exist !", e);
				}
				if (exchangeRateTimeSeries != null) {
					this.timeSeriesService.reject(exchangeRateTimeSeries, REJECTED_COMMENT,
							reason.isEmpty() ? APPROVAL_WORKFLOW_TERMINATED_COMMENT : reason);
				}
			}
		}
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
	}
}
