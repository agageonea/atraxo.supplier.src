package atraxo.fmbas.business.ext.externalInterfaces.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceMessageHistoryService;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * @author zspeter
 */
public class ExternalInterfaceMessageHistoryFacade extends AbstractBusinessBaseService {

	@Autowired
	private IExternalInterfaceMessageHistoryService srv;

	/**
	 * @param msgId
	 * @param status
	 * @throws BusinessException
	 */
	public synchronized void updateMessageHistory(MSG message, ExternalInterfaceMessageHistoryStatus status) throws BusinessException {
		this.srv.updateMessageHistory(message, status);
	}

	/**
	 * @param e
	 * @throws BusinessException
	 */
	public void insert(ExternalInterfaceMessageHistory e) throws BusinessException {
		this.srv.insert(e);
	}

	/**
	 * @param list
	 * @throws BusinessException
	 */
	public void insert(List<ExternalInterfaceMessageHistory> list) throws BusinessException {
		this.srv.insert(list);
	}

	/**
	 * @param e
	 * @throws BusinessException
	 */
	public synchronized void update(ExternalInterfaceMessageHistory e) throws BusinessException {
		this.srv.update(e);
	}

	/**
	 * @param list
	 * @throws BusinessException
	 */
	public synchronized void update(List<ExternalInterfaceMessageHistory> list) throws BusinessException {
		this.srv.update(list);
	}

	/**
	 * @param messageId
	 * @return
	 * @throws BusinessException
	 */
	public List<ExternalInterfaceMessageHistory> findByMessageId(String messageId) throws BusinessException {
		return this.srv.findByMessageId(messageId);
	}

	/**
	 * @param id
	 * @return
	 */
	public ExternalInterfaceMessageHistory findById(Object id) {
		return this.srv.findById(id);
	}

	/**
	 * @param id
	 * @return
	 * @return
	 * @throws BusinessException
	 */
	public void uploadToS3(String message, String messageId, Boolean sent) throws BusinessException {
		this.srv.uploadToS3(message, messageId, sent);
	}

}
