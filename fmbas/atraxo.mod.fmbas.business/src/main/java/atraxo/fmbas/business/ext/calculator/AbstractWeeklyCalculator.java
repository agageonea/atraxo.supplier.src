package atraxo.fmbas.business.ext.calculator;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;

/**
 * Abstract class for weekly average calculations.
 *
 * @author zspeter
 */
public abstract class AbstractWeeklyCalculator extends AbstractCalculator implements Calculator {

	@Override
	protected Date calculateAveragePeriodEndDate(Date startDate) {
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.setTime(startDate);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.calculator.AbstractCalculator#shouldCalculateAverage(java.util.Date,
	 * atraxo.fmbas.domain.impl.timeserie.TimeSerieItem)
	 */
	@Override
	protected boolean shouldCalculateAverage(Date endDate, TimeSerieItem timeSerieItem) {
		Date itemDate = timeSerieItem.getItemDate();
		itemDate = DateUtils.addDays(itemDate, 1);
		return endDate.before(itemDate);
	}

	@Override
	protected Date calculateAveragePeriodStartDate(Date date) {
		Date startDate = null;
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.setTime(date);
		if (cal.get(Calendar.DAY_OF_WEEK) == cal.getFirstDayOfWeek()) {
			cal.add(Calendar.DAY_OF_MONTH, 1);
			startDate = cal.getTime();
		}
		if (startDate == null) {
			cal.setTime(date);
			cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
			cal.add(Calendar.DAY_OF_MONTH, 1);
			startDate = cal.getTime();

		}
		return startDate;
	}

	protected void changeCalToSpecifiedWeekDay(Calendar cal, int weekDay) {
		if (cal.get(Calendar.DAY_OF_WEEK) != weekDay) {
			cal.set(Calendar.DAY_OF_WEEK, weekDay);
		}
	}

	@Override
	protected Date getEndDate(Date itemDate) {
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.setTime(itemDate);
		this.changeCalToSpecifiedWeekDay(cal, Calendar.SUNDAY);
		return cal.getTime();
	}

}
