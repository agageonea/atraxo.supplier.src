package atraxo.fmbas.business.ext.exceptions;

import java.util.Locale;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import seava.j4e.api.exceptions.IErrorCode;
import seava.j4e.api.session.Session;

public enum BusinessErrorCode implements IErrorCode {
	/* Tolerance error codes */
	INVALID_TOLERANCE_LIMITS(3001, "Tolerance limits are not defined.", "Error"),
	INVALID_TOLERANCE_TYPE(3002, "Invalid tolerance type.", "Error"),
	INVALID_TOLERANCE_VALUE(3003, "Invalid tolerance value.", "Error"),
	INVALID_TOLERANCE_CURRENCY(3004, "Invalid tolerance currency.", "Error"),
	INVALID_TOLERANCE_UNIT(3005, "Invalid tolerance unit.", "Error"),
	TOLERANCE_ABOSLUTE_RELATIVE_DIFFERENCE(3006, "You need to define an absolute or relative difference.", "Error"),
	TOLERANCE_ABSOLUTE_DIFFERENCE_INCOMPLETE(3007, "You need to define both down and up absolute differences.", "Error"),
	TOLERANCE_RELATIVE_DIFFERENCE_INCOMPLETE(3008, "You need to define both down and up relative differences.", "Error"),

	/* Unit error codes */
	UNIT_CONVERSION_FACTOR(3010, "Cannot determine conversion factor between units.", "Error"),
	INCONVERTIBLE_UNITS(3011, "Cannot convert units.", "Error"),
	ZERO_DENSITY(3012, "Density cannot be zero.", "Error"),
	INVALID_UNIT_TYPE(3013, "Cannot determine unit type.", "Error"),
	INVALID_DENSITY(3014, "Cannot convert from %s unit to %s unit using a density between %s and %s.", "Error"),
	TIME_SERIE_UNITS(3018, "Units must have different type.", "Error"),
	CONVERSION_FACTOR_VALIDATION(3019, "The indicated conversion factor is outside the %s %% tolerance.", "Error"),

	/* Entity related error codes */
	NO_UNIQUE_ENITY(3020, "Entity should be unique but cannot establish it.", "Error"),
	NO_ENTITY(3021, "Entity %s not found based on %s fields combination.", "Error"),
	DUPLICATE_ENTITY(3022, "There is a duplicate %s entity with the same %s field combination.", "Error"),
	ENTITY_CAN_NOT_DELETED(3023, "Can not delete entity %s because it is used by other business entities.", "Error"),
	ACCCOUNTING_RULE_ONCE_SUBSIDIARY(3024, "The same accounting rule can only be defined once for a subsidiary.", "Error"),

	/* Exchange related error codes */
	EXCHANGE_FACTOR(3030, "Exchange rate is zero.", "Error"),
	NO_EXCHANGE_RATE(3031, "No Exchange rate.", "Error"),
	NO_EXCHANGE_RATE_VALUE(3032, "No Exchange rate value.", "Error"),
	NO_AVERAGE_METHOD(3033, "No average method is activated for this time series.", "Error"),
	NO_EXCHANGE_RATE_ON_DATE(3034, "Exchange rate not found from %s  to %s  using financial source %s  and average method %s on %s.", "Error"),
	NO_EXCHANGE_RATE_FOUND(3035, "Exchange rate not found using financial source %s and average method %s.", "Error"),
	NO_DEFAULT_AVERAGE_METHOD(3036, "No default averaging method defined for this time series.", "Error"),

	TS_USED(3040, "Quotation is use.", "Error"),
	TSI_NOT_VALID(3041, "Your item %s is not valid.", "Error"),
	TSI_CANNOT_DELETE(3042, "Item at %s is calculated. You cannot delete.", "Error"),
	TS_DUPLICATE_ENTITY(3043, "Duplicate entity %s based on %s field.", "Error"),
	DUPLICATE_ENTITY_GENERAL(3044, "Duplicate entity.", "Error"),
	THREAD_START_NO_RESOURCES(3045, "Could not start new Thread, not enough resources.", "Error"),

	PRICE_CATEGORY_DUPLICATE_ENTRY(3046, "The name of the price category must be unique.", "Error"),
	PRICE_CATEGORY_COMPOSITE_PRODUCT(3047, "The composite price category type must be product!", "Error"),

	NO_MASTER_AGREEMENT(3050, "Master agreement doesn't exist.", "Error"),

	SUPPLIER_COMPANY_ROLE(3055, "At least one company role has to be selected.", "Error"),

	SUPPLIER_CONTRACT_NOT_FOUND(3060, "Supplier contract not found.", "Error"),
	PRIMARY_CONTACT_CANNOT_DETERMINE(3061, "Primary contact for %s not found or is not unique.", "Error"),
	PRICE_COMPONENT_NOT_FOUND_FOR_PRICE_CATEGORY(3062, "Price category %s doesn't has price components.", "Error"),

	ACCES_TO_DIFFERENT_CLIENT(3065, "Access to a different client is not allowed.", "Error"),

	PASSWORD_VALIDATION(3066, "Password validation failed.", "Error"),

	NO_ALGORITH_FOUND(3067, "No `MD5` algorithm found.", "Error"),

	CONTACT_EMAIL(3070, "The E-Mail address is not valid.", "Error"),

	CONTACT_EMPTY(3071, "There is no item to update.", "Error"),

	CREDIT_LINE_OVERLAPING(3075, "The system found overlapping credit line.", "Error"),
	CREDIT_LINE_UPDATE(3076, "User demand.", "Error"),
	NO_CREDIT_LINE(3077, "Credit Line doesn't exist.", "Error"),

	CUSTOMER_CANNOT_DELETE(3080, "The customer group cannot be deleted because there are customers belonging to this group.", "Error"),
	CUSTOMER_PARENT_ACCOUNT(3081, "A parent account must be selected.", "Error"),
	CUSTOMER_SELECTED_HIMSELF(3082, "You have selected himself.", "Error"),
	FINAL_CUSTOMER_DELETE_ERROR(
			3083,
			"Cannot delete final customers because following customers are assigned to other resellers or they are resellers too: %s.",
			"Error"),
	RESELLER_CHANGE(3084, "Cannot change the Nature of BUsiness for a customer with attached Final Customers.", "Error"),
	AREA_WW(3085, "The WW Area can not be deleted or modified.", "Error"),
	AREA_SYSTEM(3086, "The system Area can not be deleted or modified.", "Error"),
	AREA_ALREADY_ASSIGNED(3087, "The business area you have chosen is already linked to another subsidiary.", "Error"),
	OFFICE_STREET_NAME_TOO_LONG(3088, "The office street adress is too long. Maximum 50 characters", "Error"),
	BILLING_STREET_NAME_TOO_LONG(3089, "The billing street adress is too long. Maximum 50 characters", "Error"),
	LOCATION_LATITUDE(3090, "Latitude must be between -90 and 90.", "Error"),
	LOCATION_LONGITUDE(3091, "Longitude must be between -180 and 180.", "Error"),
	MASTER_AGREEMENT_OVERLAPPING(3094, "The system found overlapping master agreement.", "Error"),
	MASTER_AGREEMENT_EVERGREEN(3095, "Valid to can not be null when Evergreen is unchecked.", "Error"),
	CODE_NAME_CANT_BE_EMPTY(3096, "You can't have a customer with an empty code/name.", "Error"),
	NO_SYS_CRNCY_DEFFINED(3097, "There is no defined system currency.", "Error"),

	/* Duplicate advanced filter */

	DUPLICATE_FILTER(3100, "A filter with this name already exists!", "Error"),

	/* Duplicate view */

	DUPLICATE_VIEW(3110, "A view with this name already exists!", "Error"),

	VAT_RATE_OVERLAPPING(3120, "The period for this new VAT rate overlaps with other VAT rate period (%s-%s).", "Error"),

	/* Valid from must be smaller than valid to */

	VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO(3130, "Valid from can not be greater than valid to.", "Error"),

	VALID_TO_NULL(3131, "Valid to must be not null.", "Error"),

	VALID_FROM_NULL(3132, "Valid from must be not null.", "Error"),

	VALID_FROM_TO_NULL(3133, "Valid from and valid to must not be null.", "Error"),

	CONTACT_NULL(3134, "Contact tag must not be null!", "Error"),

	INVOICE_LINE_AMOUNT_BALANCE_OUT_OF_TOLERANCE(3140, "The amount exceeds the CHECK_BILL_AMOUNT defined tolerance.", "Error"),
	INVOICE_LINE_QUANTITY_BALANCE_OUT_OF_TOLERANCE(3141, "The quantity exceeds the defined tolerance CHECK_INVOICE_QUANTITY_AUTO_BALANCE.", "Error"),

	/* Jobs & Triggers */
	INVALID_PORT_NUMBER(3144, "Port number is invalid, make sure it's a number between 1024 and 65535.", "Validation"),
	RUNNING_JOB_CHAIN(3145, "Job chain is already running.", "Validation"),
	DAILY_TRIGGER_NOT_VALID(3150, "Please select repeat interval.", "Validation"),
	WEEKLY_TRIGGER_MISSING_DAYS(3151, "Please select at least one day.", "Validation"),
	WEEKLY_TRIGGER_MISSING_REPEAT_INTERVAL(3152, "Please select repeat interval.", "Validation"),
	MONTHLY_TRIGGER_MISSING_REPEAT_DEFINIITON(3153, "Please select a repeat definition.", "Validation"),
	MONTHLY_TRIGGER_MISSING_WEEKS(3154, "Please select at least a week day and a week in the month.", "Validation"),
	MONTHLY_TRIGGER_MISSING_DAYS_OF_MONTH(3155, "Please select at least one day.", "Validation"),
	MONTHLY_TRIGGER_MISSING_MONTHS(3156, "Please select at least one month.", "Validation"),
	INVALID_JOB_CHAIN(3157, "The job chain must contain at least one trigger and one action.", "Validation"),
	IATA_FT_JOB_SKIPED(3158, "Skiped count: %s", "Validation"),
	IATA_FT_JOB_READ(3159, "Read count: %s", "Validation"),

	/* Final customers delete */

	ASSIGNED_TO_ANOTHER_CUSTOMER(3160, "Can not delete the final customer because is assigned to another customer.", "Error"),
	CUSTOMER_TYPE_COMPANY(3161, "Can not delete the final customer because is company.", "Error"),
	ICAO_NOT_EXISTS(3163, "ICAO code must be set.", "Error"),
	IATA_NOT_EXISTS(3164, "IATA code must be set.", "Error"),

	COMP_TIME_SERIES_NULL_FIELDS(3170, "Weighting and Factor fields are mandatory.", "Error"),
	COMP_TIME_SERIES_INV_WEIGHTING(3171, "Weighting value must be between 1 and 99.", "Error"),
	COMP_TIME_SERIES_DUPLICATE_COMPONENT(3172, "You can add a time serie as a component once. Detected duplicates: %s", "Error"),

	PUBLISH_JOB_QUOTATION_NUMBER(3180, "Number of updated Quotations: %s", "Validation"),
	PUBLISH_JOB_EXCHANGE_RATE_NUMBER(3181, "Number of updated ExchangeRates: %s", "Validation"),

	PRICE_UPDATE_NOTIFICATION_JOB_SENT(3182, "Notification were sent to: ", "Validation"),
	PRICE_UPDATE_NOTIFICATION_JOB_NO_TEMPLATE(3183, "No template defined for: ", "Error"),
	PRICE_UPDATE_NOTIFICATION_JOB_NO_CONTACTS(3184, "No e-mail recipients found for: ", "Error"),
	PRICE_UPDATE_NOTIFICATION_JOB_SUBJECT(3185, "Price Notification", "Validation"),

	NO_ACCOUNT_NUMBER_SET(3186, "In order to approve a customer you need to define an account number.", "Error"),

	/* Mail merge */

	TEMPLATE_ALREADY_EXISTS(3200, "This template is already exists", "Error"),
	MAIL_MERGE_CLIENT_NOT_EXISTS(3201, "Mail merge client not exists", "Error"),
	TEMPLATE_NOT_EXISTS(3202, "This template does not exist on mail merge server.", "Error"),
	CAN_NOT_DELETE_ACTIVE_TEMPLATE(3203, "Cannot delete an active template.", "Error"),
	WRONG_FILE_FORMAT(3204, "Wrong format, you can add only xlsx, docx and txt", "Error"),
	CAN_NOT_BUILD_JSON_OBJECT(3205, "Can not build json object.", "Error"),
	TEMPLATE_NULL_PREVIEW(3206, "The generated template document is unreachable.", "Error"),
	MAIL_MERGE_APPLICATION_FAULT(3207, "Notification service fault.", "Error"),
	MAIL_MERGE_VALIDATION_FAULT(3208, "Notification service validation fault.", "Error"),
	JSON_PARSE_ERROR(3209, "Convert object to json string conversion problem", "Error"),
	MAIL_MERGE_SERVER_DOWN(3210, "The notification server is not accessible!", "Error"),
	EXISTENT_SYSTEM_USE_TYPE(3211, "There is already a template in system use of the same type.", "Error"),
	NO_ADMIN_ROLE(3212, "You must have the Application Administrator role to perform this operation.", "Error"),
	/* Customer Notifications */

	SAME_NOTIFICATION_TYPE(3300, "You can't assign the same notification type or area more than once per customer.", "Error"),

	CUSTOMER_RPC_ACTION_ERROR(
			3310,
			"Action cannot be executed on the following Customer(s) because they are not synchronized with database: %s The records have been reloaded. <br>Please try again.",
			"Validation"),

	/* Exchange Rate */

	USED_BY_ANOTHER_ENTITY_QUOTATION(3400, "This quotation cannot be deleted because it is used by another entity.", "Error"),

	USED_BY_ANOTHER_ENTITY_EXCHANGE_RATE(3401, "This exchange rate cannot be deleted because it is used by another entity.", "Error"),
	NO_QUOTATION_VALUE_FOR_DATE(3402, "There is not defined quotation value for the given date", "Error"),

	/* import ECB Exchange Rate job */

	IMPORT_ER_NOT_DEFINED(3402, "No Time Series defined for data import.", "Validation"),
	IMPORT_ER_DAYS_PROCESSED(3403, "Days processed: ", "Validation"),
	IMPORT_ER_TOTAL_PROCESSED(3404, "Total records processed: ", "Validation"),
	IMPORT_ER_IMPORTED(3405, "Imported records: ", "Validation"),
	IMPORT_ER_UPDATED(3406, "Updated records: ", "Validation"),
	IMPORT_ER_SKIPPED(3407, "Skipped records: ", "Validation"),
	IMPORT_ER_EXCEPTION(3408, "Exception occured!\r\nStacktrace:\r\n", "Error"),
	IMPORT_ER_COMMUNICATION(3409, "Exception calling the Bloomberg API:", "Validation"),
	IMPORT_ER_API(3410, "Exception returned by the Bloomberg API:", "Validation"),
	OPTIMISTIC_LOCK_TIME_SERIE_ECB_JOB(3411, "The following job resulted in failure due to a concurency error on the: %s |", "Error"),

	/* Document Series */

	DOCUMENT_NO_SERIES_OVERDUE(3500, "%s number series overdue. Please verify.", "Error"),
	INVALID_SERIES(
			3501,
			"Can not determine the document number series. Please verify that a document number series exists for this generator.",
			"Error"),

	INVALID_INDEX(3502, "Start index must be lower than end index.", "Error"),
	INVALID_START_INDEX(3503, "Start index cannot be lower than the next document number.", "Error"),
	INVALID_INVOICE_SERIES(
			3504,
			"There is no Invoice Number Serie generator. Please check that one exists and is enabled in the Accounting Settings -> Invoice Number Series.",
			"Error"),
	INVALID_CREDITMEMO_SERIES(
			3508,
			"The system automatically created a credit memo number as there is no valid document number series defined for credit memos. Please configure a document number series for credit memos in the Accounting settings.",
			"Error"),
	DOCUMENT_NO_SERIES_SYNC_ERROR(3509, "The document number series of type %s has suffered a concurrency error. Please try again.", "Error"),
	INVALID_AIRCRAFT_REGISTRATION_NUMBER(3510, "Invalid aircraft registration number.", "Error"),

	INTERFACE_NOTIFICATION_SUCCESS(3520, "%s was completed successfully", "Info"),
	INTERFACE_NOTIFICATION_FAILURE(3521, "%s failed. Check job result for details.", "Info"),
	INTERFACE_NOTIFICATION_SUBJECT(3522, "You have a new service notification.", "Info"),
	INTERFACE_NOTIFICATION_FAILURE_WKF(3523, "%s failed. Check workflow instance result for details.", "Error"),
	INTERFACE_NOTIFICATION_TERMINATE_WFK(3524, "%s has been terminated. Check workflow instance result for details.", "Info"),

	INTERFACE_CUSTOM_NOTIFICATION_SUCCESS_WKF(3525, "%s was completed successfully for %s %s.", "Info"),
	INTERFACE_CUSTOM_NOTIFICATION_FAILURE_WKF(3526, "%s failed for %s %s. Check workflow instance result for details.", "Info"),
	INTERFACE_CUSTOM_NOTIFICATION_TERMINATE_WFK(3527, "%s has been terminated for %s %s. Check workflow instance result for details.", "Info"),

	INTERFACE_EXPORT_CUSTOMERS_RETRY_ATTEMPT(
			3530,
			"Could not export customer(s) with account code(s) %s ! The system will attempt to resend the data %s times! ",
			"Error"),
	INTERFACE_EXPORT_CUSTOMERS_SUCCESS(3531, "Exported with success the customer(s) with account code(s) %s", "Info"),
	INTERFACE_NO_SOURCE(3532, "Internal error: could not build a new MSG from source !", "Error"),
	INTERFACE_PARAM_NOT_LOCATED(3533, "Could not locate parameter %s!", "Validation"),
	INTERFACE_PARAM_NEGATIVE(3534, "Parameter %s should not be negative !", "Validation"),
	INTERFACE_PARAM_NOT_VALID(3535, "Parameter %s has no valid value definition !", "Validation"),
	INTERFACE_RETRY_ATTEMPT_NR(3536, "Retry attempt nr.%s/%s for interface %s.", "Error"),
	INTERFACE_EXPORT_CUSTOMERS_INCORRECT_OPTION(3537, "Could not read export option for '%s'!", "Error"),
	INTERFACE_EXPORT_CUSTOMERS_NO_CUSTOMERS(3538, "There are no customers to export !", "Validation"),
	INTERFACE_DISABLED(3539, "The interface is disabled.", "Validation"),

	INTERFACE_FIND_CUSTOMER_INVALID_CODE(3540, "Could not find a customer for received code(s) %s", "Error"),
	INTERFACE_NO_INTERFACE_WITH_NAME_AND_CLIENT_IDENTIFIER(3541, "No interface with name '%s' and client identifier '%s' exists !", "Error"),
	INTERFACE_ACK_EXPORT_CUSTOMERS_NO_ACK(3542, "Could not extract Acknowledgement from received XML !", ""),
	INTERFACE_ACK_EXPORT_CUSTOMERS_INVALID_TRANSMISSION_STATUS(3543, "Could not extract transmission status from '%s'!", "Error"),
	INTERFACE_ACK_EXPORT_CUSTOMERS_INVALID_PROCESSED_STATUS(3544, "Could not extract processed status from '%s'!", "Error"),
	INTERFACE_ACK_EXPORT_CUSTOMERS_SAME_ACCOUNT_NUMBER(3545, "There are multiple customers with same account number :'%s'!", "Error"),
	INTERFACE_NO_CLIENT_IDENTIFIER(3546, "Could not process received WS Client Identifier extracted from CorrelationID '%s'!", "Error"),
	INTERFACE_CUSTOMERS_DIFFERENT_ACCOUNT_NUMBER(
			3547,
			"For customer (%s) the received ERP number (%s) is different from current ERP number (%s)",
			"Error"),

	INTERFACE_EXPORT_CUSTOMERS_START(3548, "%s interface is started. For status please check %s interface execution history.", "Error"),

	INTERFACE_IMPORT_CREDIT_NO_CREDIT(3550, "Could not extract customer credit information from received XML !", "Error"),
	INTERFACE_INVALID_CURRENCY(3551, "Received invalid currency code : could not locate a currency for code '%s' !", "Error"),
	INTERFACE_INVALID_CURRENCY_EMPTY(3552, "Received empty currency code!", "Error"),
	INTERFACE_IMPORT_CREDIT_MULTIPLE_ACCOUNT_NUMBER(3553, "There are multiple customers for customer account number '%s' !", "Error"),
	INTERFACE_IMPORT_CREDIT_INVALID_ACCOUNT_NUMBER(
			3554,
			"Received invalid account number : could not locate a customer for customer account number '%s' !",
			"Error"),
	INTERFACE_IMPORT_CREDIT_EMPTY_ACCOUNT_NUMBER(3555, "Received empty customer account number !", "Error"),
	INTERFACE_INVALID_AMOUNT(3556, "Could not parse received amounts !", "Error"),
	INTERFACE_IMPORT_CREDIT_EMPTY_CREDIT_LINES(3557, "Received empty credit line details !", "Error"),
	INTERFACE_IMPORT_CREDIT_EMPTY_ERP_NUMBER(3558, "Received empty ERP Number !", "Error"),

	INTERFACE_EXPORT_FUEL_EVENTS_NO_FUEL_EVENTS(3560, "There are no fuel events to export !", "Validation"),
	INTERFACE_EXPORT_FUEL_EVENTS_INCORRECT_OPTION(3561, "Could not read export option for '%s'!", "Error"),
	INTERFACE_EXPORT_FUEL_EVENTS_DETAILS(
			3562,
			"Initiated export operation with success to %s fuel events from total of %s. %s interface is started. For status please check %s interface execution history.",
			"Error"),

	INTERFACE_EXPORT_OUT_INVOICES_NO_INVOICES(3563, "There are no outgoing invoices to export !", "Validation"),
	INTERFACE_EXPORT_OUT_INVOICES_INCORRECT_OPTION(3564, "Could not read export option for '%s'!", "Error"),
	INTERFACE_EXPORT_OUT_INVOICES_DISABLED(3565, "Interface '%s' is disabled.", "Error"),
	INTERFACE_EXPORT_OUT_INVOICES_DETAILS_OUT_INVOICE(
			3566,
			"Interface '%s' started : exported %s from total of %s outgoing invoices . For status please check '%s' interface execution history.",
			"Error"),
	INTERFACE_EXPORT_OUT_INVOICES_DETAILS_CREDIT_MEMO(
			3567,
			"Interface '%s' started : exported %s from total of %s credit memos . For status please check '%s' interface execution history.",
			"Error"),
	INTERFACE_EXPORT_NO_ENTITIES(3568, "The list of entities for saving the message/constructing the payload history is empty.", "Error"),

	/* Templates */
	DUPLICATE_TEMPLATE(3600, "This template already exists in the database", "Error"),

	INVALID_UNIT_OF_MEASURE(3610, "Invalid unit of measure!", "Error"),

	OUT_INTERFACE_DISABLED_WITH_DETAILS(3611, "The interface %s is disabled. The data will not be sent!", "Validation"),

	IN_INTERFACE_DISABLED_WITH_DETAILS(3612, "The interface %s is disabled. The data will not be processed!", "Validation"),

	TEMPLATE_NOT_FOUND(3700, "No Template or too many templates found for export, please upload one.", "Error"),

	WORKFLOW_UNKNOWN_ERROR(3800, "Unknown workflow error.", "Error"),
	WORKFLOW_NO_PROCESS_INSTANCE(3801, "No running process instance exists.", "Error"),
	WORKFLOW_INSTANCE_ALREADY_STARTED(
			3802,
			"Workflow Instance already started for current entity. It has to be terminated before another one is started for the same entity.",
			"Error"),
	WORKFLOW_CLAIMED(3803, "Cannot complete. Task has been already claimed by user %s.", "Error"),
	WORKFLOW_FAILED_NUMBER(3804, "Failed to approve %s from %s.", "Error"),
	INSTANCE_TERMINATION_FAILURE_RESULT(3805, "Number of not terminated workflows instances: %d", "Error"),
	INSTANCE_TERMINATION_RESON(3806, "Terminated by %s", "Info"),

	FTP_UPLOAD_ERROR(3820, "Failed to upload files to ftp server.", "Error"),
	FTP_CONNECTION_ERROR(3821, "Failed FTP/sFTP session. Please check connection parameters.", "Error"),
	FTP_KEY_NOT_FOUND(3822, "Failed to find key for sFTP.", "Error"),

	// Exchange Rate Time Series Workflow Business Error Codes

	EXCHANGE_RATE_TIME_SERIE_WORKFLOW_NO_SYS_PARAM(
			3830,
			"Can not submit for approval, please check the 'Enable workflow approval process for exchange rate time series' system parameter.",
			"Error"),
	TIME_SERIE_WORKFLOW_ENTITY_NOT_EXISTING(3831, "Could not retrieve time series for ID %s", "Error"),
	EXCHANGE_RATE_TIME_SERIE_NOT_FIND(3832, "The selected exchange rate does not exist in database. Please refresh your page.", "Error"),
	TIME_SERIE_WORKFLOW_NOT_START_REASON(3833, "Approval process could not be started. Reason : ", "Error"),

	WORKFLOW_DEPLOY_ERROR(
			3834,
			"Workflow '%s' could not be automatically deployed before starting it. Please contact the system administrator.",
			"Error"),
	WORKFLOW_COULD_NOT_RETRIEVE_FOR_SUBSIDIARY_ERROR(3835, "Could not retrieve a corresponding workflow for '%s' and '%s' subsidiary.", "Error"),

	ENERGY_RATE_TIME_SERIE_WORKFLOW_NO_SYS_PARAM(
			3836,
			"Can not submit for approval, please check the 'Enable workflow approval process for energy price time series' system parameter.",
			"Error"),
	ENERGY_RATE_TIME_SERIE_WORKFLOW_ENTITY_NOT_EXISTING(3837, "Could not retrieve an energy rate for ID %s", "Error"),
	ENERGY_RATE_TIME_SERIE_NOT_FIND(3838, "The selected energy rate does not exist in database. Please refresh your page.", "Error"),
	ENERGY_RATE_TIME_SERIE_WORKFLOW_NOT_START_REASON(3839, "Approval process could not be started. Reason : ", "Error"),
	TIME_SERIE_NO_AVERAGE_METHOD_ACTIVE(3841, "There is no active average method defined for the current time serie.", "Error"),

	WORKFLOW_COULD_NOT_FIND_AN_EXCHANGE_RATE_FOR_ID(3842, "Could not find an Exchange Rate for ID '%s'", "Error"),
	WORKFLOW_COULD_NOT_PARSE_THE_EXCHANGE_RATE_ID(3843, "Could not parse the Exchange Rate ID '%s'", "Error"),
	WORKFLOW_EXCHANGE_RATE_TIME_SERIE_EMPTY_ID(3844, "The id for the Exchange Rate time series is empty !", "Error"),
	WORKFLOW_EXCHANGE_RATE_TIME_SERIE_VARIABLE_NOT_FOUND(
			3845,
			"The variable for the id of the Exchange Rate time serie could not be found !",
			"Error"),

	WORKFLOW_COULD_NOT_FIND_AN_ENERGY_PRICE_FOR_ID(3846, "Could not find an Energy Price for ID '%s'", "Error"),
	WORKFLOW_COULD_NOT_PARSE_THE_ENERGY_PRICE_ID(3847, "Could not parse the Energy Price ID '%s'", "Error"),
	WORKFLOW_ENERGY_PRICE_TIME_SERIE_EMPTY_ID(3848, "The id for the Energy Price time series is empty !", "Error"),
	WORKFLOW_ENERGY_PRICE_TIME_SERIE_VARIABLE_NOT_FOUND(
			3849,
			"The variable for the id of the Energy Price time series could not be found !",
			"Error"),

	TIME_SERIE_NO_RAW_ITEMS_EXIST(3850, "There are no time serie items defined for the current time serie.", "Error"),
	TIME_SERIE_X_APPROVAL_REQUEST_FOR_Y(3851, "%s approval request for %s.", "Info"),

	DTO_CANT_EXTRACT_DATA_FROM_ENTITY_WORKFLOW(3860, "Could not extract data from the '%s' '%s' in order to send the mail !", "Error"),

	SUCCESFULLY_APPROVED_X_FROM_Y_TIMESERIES(3870, "Succesfully approved %s out of %s time series\n", "Info"),
	SUCCESFULLY_REJECTED_X_FROM_Y_TIMESERIES(3871, "Succesfully rejected %s out of %s time series\n", "Info"),

	TIME_SERIE_RPC_ACTION_ERROR(
			3900,
			"Action cannot be executed on the following Time Serie(s) because they have been changed or deleted by another user: <br/> %s <br/> The records have been reloaded. <br>Please try again and if you still cannot proceed close the dialog and reprocess.",
			"Validation"),
	TIME_SERIE_RPC_ACTION_ERROR_SHORT(3901, "The following Time Serie has been changed or deleted by another user.", "Validation"),
	TIME_SERIE_WORKFLOW_IDENTIFICATION_ERROR(
			3902,
			"Workflow instance identification error. Time serie is reset, workflow instance is terminated.",
			"Error"),

	FIELD_CHANGE_VALUE_ERROR(3950, "Could not get the value from field change !", "Error"),
	PLATTS_CANT_FIND_FILE_NAME(3951, "Can't determine the file path for the read platts file", "Error"),

	ROLE_BY_CODE_NOT_EXISTING(3810, "Could not find a role by code '%s'.", "Error"),
	ROLES_NO_USERS_ERROR(3811, "Could not find any users for roles '%s'.", "Error"),
	ROLES_CODE_EMPTY_ERROR(3812, "The approver role codes are empty !", "Error"),

	JOBS_DATE_FIELDS_ARE_NOT_FILLED(3890, "Please fill in both fields related to the price validity: Price valid to and Price valid from.", "Error"),
	JOBS_VALID_FROM_GREATER_THAN_VALID_TO(3891, "The Price effective from value cannot be greater than the Price valid to value.", "Error"),

	COULD_NOT_FIND_SERVICE(3900, "Could not find the service named: %s", "Error");

	private final Logger LOG = LoggerFactory.getLogger(BusinessErrorCode.class);

	private ResourceBundle boundle;

	private final int errNo;
	private final String errMsg;
	private final String errGroup;

	private BusinessErrorCode(int errNo, String errMsg, String errGroup) {
		this.errNo = errNo;
		this.errMsg = errMsg;
		this.errGroup = errGroup;
	}

	@Override
	public String getErrGroup() {
		return this.errGroup;
	}

	@Override
	public int getErrNo() {
		return this.errNo;
	}

	@Override
	public String getErrMsg() {
		try {
			String language = Session.user.get().getSettings().getLanguage();
			if (language.equals("sys")) {
				this.boundle = ResourceBundle.getBundle("locale.sone.fmbas.error.messages");
			} else {
				this.boundle = ResourceBundle.getBundle("locale.sone.fmbas.error.messages", new Locale(language));
			}
		} catch (NullPointerException e) {
			this.LOG.trace("No session while testing", e);
			this.boundle = ResourceBundle.getBundle("locale.sone.fmbas.error.messages");
		}
		return this.boundle.containsKey(Integer.toString(this.errNo)) ? this.boundle.getString(Integer.toString(this.errNo)) : this.errMsg;
	}
}
