package atraxo.fmbas.business.ws.toNav;

import java.util.Collection;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.ws.WSModelToDTOTransformer;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.ws.creditRequest.Any;
import atraxo.fmbas.domain.ws.creditRequest.Any.RequestCustomerCreditInformation;
import atraxo.fmbas.domain.ws.dto.WSPayloadDataDTO;
import atraxo.fmbas.domain.ws.dto.WSTransportDataType;
import seava.j4e.business.service.AbstractBusinessBaseService;
import seava.j4e.business.utils.DateUtils;
import seava.j4e.commons.utils.XMLUtils;

public class WSCustomerRequestToDTOTransformer extends AbstractBusinessBaseService implements WSModelToDTOTransformer<Customer> {
	
	final static Logger logger = LoggerFactory.getLogger(WSCustomerRequestToDTOTransformer.class);

	@Override
	public WSPayloadDataDTO transformModelToDTO(Collection<Customer> modelEntities, Map<String, String> paramMap) throws JAXBException {
		if (logger.isDebugEnabled()) {
			logger.debug("START transformModelToDTO()");
		}
		WSPayloadDataDTO dto = new WSPayloadDataDTO();
		
		if(!modelEntities.isEmpty()) {
				AbstractEntity entity = modelEntities.iterator().next();
				
				Customer customerModel = null;
				
				try {
					customerModel = (Customer) entity;
				} catch (ClassCastException e) {
					logger.error("ERROR: could not cast " + entity.getClass() + " to " + Customer.class + " !", e);
					throw e;
				}
				
				if (customerModel != null) {
					Any customerAccountAny = this.toCustomerAccount(customerModel);
					dto.setPayload(XMLUtils.toXML(customerAccountAny, customerAccountAny.getClass()));
				}			
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug("END transformModelToDTO()");
		}
		return dto;
	}
	private Any toCustomerAccount(Customer customerModel) {
		Any anyAccount = new Any();
		RequestCustomerCreditInformation account = new RequestCustomerCreditInformation();
		anyAccount.setRequestCustomerCreditInformation(account);
		
		account.setCustomerAccountNumber(customerModel.getCode());
		account.setErpNumber(customerModel.getAccountNumber());

		return anyAccount;
	}
	
	@Override
	public WSTransportDataType getTransportDataType() {
		return WSTransportDataType.CUSTOMERS;
	}

}
