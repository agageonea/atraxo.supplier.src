package atraxo.fmbas.business.ext.bpm;

public enum WorkflowEntities {
	BID("atraxo.mod.cmm", "atraxo.cmm.ui.extjs.frame.TenderBid_Ui"),
	SELL_CONTRACT("atraxo.mod.cmm", "atraxo.cmm.ui.extjs.frame.ContractCustomer_Ui"),
	PURCHASE_CONTRACT("atraxo.mod.cmm", "atraxo.cmm.ui.extjs.frame.Contract_Ui"),
	INVOICE("atraxo.mod.acc", "atraxo.acc.ui.extjs.frame.OutgoingInvoices_Ui"),
	PRICE_UPDATE("atraxo.mod.cmm", "atraxo.cmm.ui.extjs.frame.PriceUpdate_Ui"),
	FUEL_TICKET("atraxo.mod.ops", "atraxo.ops.ui.extjs.frame.FuelTicket_Ui"),
	EXCHANGE_RATE("atraxo.mod.fmbas", "atraxo.fmbas.ui.extjs.frame.ExchangeRate_Ui"),
	ENERGY_PRICE("atraxo.mod.fmbas", "atraxo.fmbas.ui.extjs.frame.energyPrice_Ui");

	private String frame;
	private String module;

	private WorkflowEntities(String frame, String module) {
		this.frame = frame;
		this.module = module;
	}

	public String getFrame() {
		return this.frame;
	}

	public String getModule() {
		return this.module;
	}

}
