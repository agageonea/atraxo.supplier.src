/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.dictionary;

import atraxo.fmbas.domain.impl.dictionary.Condition;
import atraxo.fmbas.domain.impl.dictionary.Dictionary;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Dictionary} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Dictionary_Service extends AbstractEntityService<Dictionary> {

	/**
	 * Public constructor for Dictionary_Service
	 */
	public Dictionary_Service() {
		super();
	}

	/**
	 * Public constructor for Dictionary_Service
	 * 
	 * @param em
	 */
	public Dictionary_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Dictionary> getEntityClass() {
		return Dictionary.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Dictionary
	 */
	public Dictionary findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Dictionary.NQ_FIND_BY_BUSINESS,
							Dictionary.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Dictionary", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Dictionary", "id"), nure);
		}
	}

	/**
	 * Find by reference: externalInterface
	 *
	 * @param externalInterface
	 * @return List<Dictionary>
	 */
	public List<Dictionary> findByExternalInterface(
			ExternalInterface externalInterface) {
		return this.findByExternalInterfaceId(externalInterface.getId());
	}
	/**
	 * Find by ID of reference: externalInterface.id
	 *
	 * @param externalInterfaceId
	 * @return List<Dictionary>
	 */
	public List<Dictionary> findByExternalInterfaceId(
			Integer externalInterfaceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Dictionary e where e.clientId = :clientId and e.externalInterface.id = :externalInterfaceId",
						Dictionary.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("externalInterfaceId", externalInterfaceId)
				.getResultList();
	}
	/**
	 * Find by reference: conditions
	 *
	 * @param conditions
	 * @return List<Dictionary>
	 */
	public List<Dictionary> findByConditions(Condition conditions) {
		return this.findByConditionsId(conditions.getId());
	}
	/**
	 * Find by ID of reference: conditions.id
	 *
	 * @param conditionsId
	 * @return List<Dictionary>
	 */
	public List<Dictionary> findByConditionsId(Integer conditionsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Dictionary e, IN (e.conditions) c where e.clientId = :clientId and c.id = :conditionsId",
						Dictionary.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("conditionsId", conditionsId).getResultList();
	}
}
