/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.validationMessage;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.validationMessage.ValidationMessage;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ValidationMessage} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ValidationMessage_Service
		extends
			AbstractEntityService<ValidationMessage> {

	/**
	 * Public constructor for ValidationMessage_Service
	 */
	public ValidationMessage_Service() {
		super();
	}

	/**
	 * Public constructor for ValidationMessage_Service
	 * 
	 * @param em
	 */
	public ValidationMessage_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ValidationMessage> getEntityClass() {
		return ValidationMessage.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ValidationMessage
	 */
	public ValidationMessage findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ValidationMessage.NQ_FIND_BY_BUSINESS,
							ValidationMessage.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ValidationMessage", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ValidationMessage", "id"), nure);
		}
	}

}
