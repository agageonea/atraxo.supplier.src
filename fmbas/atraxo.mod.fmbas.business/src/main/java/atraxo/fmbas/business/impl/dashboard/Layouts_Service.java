/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.dashboard;

import atraxo.fmbas.business.api.dashboard.ILayoutsService;
import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;
import atraxo.fmbas.domain.impl.dashboard.Layouts;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Layouts} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Layouts_Service extends AbstractEntityService<Layouts>
		implements
			ILayoutsService {

	/**
	 * Public constructor for Layouts_Service
	 */
	public Layouts_Service() {
		super();
	}

	/**
	 * Public constructor for Layouts_Service
	 * 
	 * @param em
	 */
	public Layouts_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Layouts> getEntityClass() {
		return Layouts.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Layouts
	 */
	public Layouts findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Layouts.NQ_FIND_BY_NAME, Layouts.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Layouts", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Layouts", "name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Layouts
	 */
	public Layouts findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Layouts.NQ_FIND_BY_BUSINESS,
							Layouts.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Layouts", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Layouts", "id"), nure);
		}
	}

	/**
	 * Find by reference: dashboardWidgets
	 *
	 * @param dashboardWidgets
	 * @return List<Layouts>
	 */
	public List<Layouts> findByDashboardWidgets(
			DashboardWidgets dashboardWidgets) {
		return this.findByDashboardWidgetsId(dashboardWidgets.getId());
	}
	/**
	 * Find by ID of reference: dashboardWidgets.id
	 *
	 * @param dashboardWidgetsId
	 * @return List<Layouts>
	 */
	public List<Layouts> findByDashboardWidgetsId(Integer dashboardWidgetsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Layouts e, IN (e.dashboardWidgets) c where e.clientId = :clientId and c.id = :dashboardWidgetsId",
						Layouts.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("dashboardWidgetsId", dashboardWidgetsId)
				.getResultList();
	}
}
