package atraxo.fmbas.business.ext.ftp.job.tasklet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.EnumMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import atraxo.fmbas.business.api.job.IActionService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.ftp.SessionFactoryBuilder;
import atraxo.fmbas.business.ext.ftp.job.bean.FtpGetRemoteFilesBean;
import atraxo.fmbas.business.ws.GUIDMessageGenerator;
import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.ActionParameter;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.business.service.storage.StorageService;
import seava.j4e.domain.batchjob.JobParameter;

/**
 * Spring bach tasklet to get files from a remote ftp/sftp server.
 *
 * @author zspeter
 */
public class FtpServerReadTasklet implements Tasklet, ApplicationContextAware {

	/**
	 * Parameter names required by the current tasklet.
	 *
	 * @author zspeter
	 */
	private enum ParamName {
	HOST, USERNAME, PASSWORD, LOCALDIR, REMOTEDIR, FILEPATTERN, SECURED, KEYNAME, PORT
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(FtpGetRemoteFilesBean.class);

	@Autowired
	private IActionService actionSrv;
	@Autowired
	private StorageService storageService;

	private ApplicationContext appContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.appContext = applicationContext;

	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		Map<ParamName, String> paramMap = this.getActionParameters(chunkContext);
		Resource resource = null;
		if (paramMap.containsKey(ParamName.SECURED) && !StringUtils.isEmpty(paramMap.get(ParamName.SECURED))) {
			String securedValue = paramMap.get(ParamName.SECURED);
			boolean isSecured = Boolean.parseBoolean(securedValue);
			if (isSecured && paramMap.containsKey(ParamName.KEYNAME) && !StringUtils.isEmpty(paramMap.get(ParamName.KEYNAME))) {
				String refId = this.getActionParameterRefId(chunkContext, ParamName.KEYNAME);
				resource = this.createPrivateKeyResource(refId);
			}
		}
		FtpGetRemoteFilesBean bean = this.getFTPGetRemoteFilesBean(paramMap, resource);
		bean.execute();
		return RepeatStatus.FINISHED;
	}

	private Resource createPrivateKeyResource(String refId) throws BusinessException {
		final Path destination = Paths.get(
				seava.j4e.api.session.Session.user.get().getWorkspace().getTempPath() + File.separator + GUIDMessageGenerator.generateMessageID());
		try (final InputStream io = this.storageService.downloadFile(refId);) {
			if (destination.toFile().exists()) {
				Files.delete(destination);
			}
			Files.copy(io, destination);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}
		return new FileSystemResource(destination.toFile());
	}

	/**
	 * Using configured service read parameters for current action.
	 *
	 * @param chunkContext
	 * @return
	 */
	private Map<ParamName, String> getActionParameters(ChunkContext chunkContext) {
		Long actionId = (Long) chunkContext.getStepContext().getJobParameters().get(JobParameter.ACTION.name());
		Action action = this.actionSrv.findById(actionId.intValue());
		Map<ParamName, String> paramMap = new EnumMap<>(ParamName.class);

		for (ActionParameter param : action.getActionParameters()) {
			paramMap.put(ParamName.valueOf(param.getName()), !StringUtils.isEmpty(param.getBusinessValue()) ? param.getBusinessValue()
					: !StringUtils.isEmpty(param.getValue()) ? param.getValue() : param.getDefaultValue());
		}
		return paramMap;
	}

	private String getActionParameterRefId(ChunkContext chunkContext, ParamName name) throws BusinessException {
		Long actionId = (Long) chunkContext.getStepContext().getJobParameters().get(JobParameter.ACTION.name());
		Action action = this.actionSrv.findById(actionId.intValue());
		for (ActionParameter param : action.getActionParameters()) {
			if (name.toString().equals(param.getName())) {
				return param.getRefid();
			}
		}
		throw new BusinessException(BusinessErrorCode.FTP_KEY_NOT_FOUND, BusinessErrorCode.FTP_KEY_NOT_FOUND.getErrMsg());
	}

	private FtpGetRemoteFilesBean getFTPGetRemoteFilesBean(Map<ParamName, String> paramMap, Resource resource) {
		return (FtpGetRemoteFilesBean) this.appContext.getBean("ftpGetRemoteFiles",
				SessionFactoryBuilder.getInstance().buildSessionFactory(paramMap.get(ParamName.HOST), paramMap.get(ParamName.USERNAME),
						paramMap.get(ParamName.PASSWORD), paramMap.get(ParamName.PORT), Boolean.parseBoolean(paramMap.get(ParamName.SECURED)),
						resource),
				new File(paramMap.get(ParamName.LOCALDIR)), paramMap.get(ParamName.REMOTEDIR), paramMap.get(ParamName.FILEPATTERN));
	}

}
