/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.geo;

import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.geo.TimeZone;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Locations} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Locations_Service extends AbstractEntityService<Locations> {

	/**
	 * Public constructor for Locations_Service
	 */
	public Locations_Service() {
		super();
	}

	/**
	 * Public constructor for Locations_Service
	 * 
	 * @param em
	 */
	public Locations_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Locations> getEntityClass() {
		return Locations.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Locations
	 */
	public Locations findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Locations.NQ_FIND_BY_CODE,
							Locations.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Locations", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Locations", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Locations
	 */
	public Locations findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Locations.NQ_FIND_BY_BUSINESS,
							Locations.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Locations", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Locations", "id"), nure);
		}
	}

	/**
	 * Find by reference: country
	 *
	 * @param country
	 * @return List<Locations>
	 */
	public List<Locations> findByCountry(Country country) {
		return this.findByCountryId(country.getId());
	}
	/**
	 * Find by ID of reference: country.id
	 *
	 * @param countryId
	 * @return List<Locations>
	 */
	public List<Locations> findByCountryId(Integer countryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Locations e where e.clientId = :clientId and e.country.id = :countryId",
						Locations.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("countryId", countryId).getResultList();
	}
	/**
	 * Find by reference: subCountry
	 *
	 * @param subCountry
	 * @return List<Locations>
	 */
	public List<Locations> findBySubCountry(Country subCountry) {
		return this.findBySubCountryId(subCountry.getId());
	}
	/**
	 * Find by ID of reference: subCountry.id
	 *
	 * @param subCountryId
	 * @return List<Locations>
	 */
	public List<Locations> findBySubCountryId(Integer subCountryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Locations e where e.clientId = :clientId and e.subCountry.id = :subCountryId",
						Locations.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("subCountryId", subCountryId).getResultList();
	}
	/**
	 * Find by reference: timezone
	 *
	 * @param timezone
	 * @return List<Locations>
	 */
	public List<Locations> findByTimezone(TimeZone timezone) {
		return this.findByTimezoneId(timezone.getId());
	}
	/**
	 * Find by ID of reference: timezone.id
	 *
	 * @param timezoneId
	 * @return List<Locations>
	 */
	public List<Locations> findByTimezoneId(Integer timezoneId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Locations e where e.clientId = :clientId and e.timezone.id = :timezoneId",
						Locations.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("timezoneId", timezoneId).getResultList();
	}
	/**
	 * Find by reference: areas
	 *
	 * @param areas
	 * @return List<Locations>
	 */
	public List<Locations> findByAreas(Area areas) {
		return this.findByAreasId(areas.getId());
	}
	/**
	 * Find by ID of reference: areas.id
	 *
	 * @param areasId
	 * @return List<Locations>
	 */
	public List<Locations> findByAreasId(Integer areasId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Locations e, IN (e.areas) c where e.clientId = :clientId and c.id = :areasId",
						Locations.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("areasId", areasId).getResultList();
	}
}
