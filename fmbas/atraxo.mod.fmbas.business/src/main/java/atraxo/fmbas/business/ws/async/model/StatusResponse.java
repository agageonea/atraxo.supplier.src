package atraxo.fmbas.business.ws.async.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "response")
public class StatusResponse implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2651309903781948003L;
	private String messageId;
	private String messageStatus;

	@XmlElement(name = "messageId", required = true)
	public String getMessageId() {
		return this.messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	@XmlElement(name = "messageStatus", required = true)
	public String getMessageStatus() {
		return this.messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

}
