/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.timeserie;

import atraxo.fmbas.business.api.timeserie.ICompositeTimeSeriesSourceService;
import atraxo.fmbas.domain.impl.timeserie.CompositeTimeSeriesSource;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link CompositeTimeSeriesSource} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class CompositeTimeSeriesSource_Service
		extends
			AbstractEntityService<CompositeTimeSeriesSource>
		implements
			ICompositeTimeSeriesSourceService {

	/**
	 * Public constructor for CompositeTimeSeriesSource_Service
	 */
	public CompositeTimeSeriesSource_Service() {
		super();
	}

	/**
	 * Public constructor for CompositeTimeSeriesSource_Service
	 * 
	 * @param em
	 */
	public CompositeTimeSeriesSource_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<CompositeTimeSeriesSource> getEntityClass() {
		return CompositeTimeSeriesSource.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return CompositeTimeSeriesSource
	 */
	public CompositeTimeSeriesSource findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							CompositeTimeSeriesSource.NQ_FIND_BY_BUSINESS,
							CompositeTimeSeriesSource.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"CompositeTimeSeriesSource", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"CompositeTimeSeriesSource", "id"), nure);
		}
	}

	/**
	 * Find by reference: composite
	 *
	 * @param composite
	 * @return List<CompositeTimeSeriesSource>
	 */
	public List<CompositeTimeSeriesSource> findByComposite(TimeSerie composite) {
		return this.findByCompositeId(composite.getId());
	}
	/**
	 * Find by ID of reference: composite.id
	 *
	 * @param compositeId
	 * @return List<CompositeTimeSeriesSource>
	 */
	public List<CompositeTimeSeriesSource> findByCompositeId(Integer compositeId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from CompositeTimeSeriesSource e where e.clientId = :clientId and e.composite.id = :compositeId",
						CompositeTimeSeriesSource.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("compositeId", compositeId).getResultList();
	}
	/**
	 * Find by reference: source
	 *
	 * @param source
	 * @return List<CompositeTimeSeriesSource>
	 */
	public List<CompositeTimeSeriesSource> findBySource(TimeSerie source) {
		return this.findBySourceId(source.getId());
	}
	/**
	 * Find by ID of reference: source.id
	 *
	 * @param sourceId
	 * @return List<CompositeTimeSeriesSource>
	 */
	public List<CompositeTimeSeriesSource> findBySourceId(Integer sourceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from CompositeTimeSeriesSource e where e.clientId = :clientId and e.source.id = :sourceId",
						CompositeTimeSeriesSource.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("sourceId", sourceId).getResultList();
	}
}
