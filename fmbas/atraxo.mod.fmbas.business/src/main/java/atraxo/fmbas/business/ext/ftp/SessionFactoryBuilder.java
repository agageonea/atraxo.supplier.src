package atraxo.fmbas.business.ext.ftp;

import org.springframework.core.io.Resource;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.ftp.session.DefaultFtpSessionFactory;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;

/**
 * @author zspeter
 */
public class SessionFactoryBuilder {

	private static SessionFactoryBuilder instance = null;

	private SessionFactoryBuilder() {
		// Exists only to defeat instantiation.
	}

	public static synchronized SessionFactoryBuilder getInstance() {
		if (instance == null) {
			instance = new SessionFactoryBuilder();
		}
		return instance;
	}

	/**
	 * Builds a session factory.
	 *
	 * @param host
	 * @param username
	 * @param password
	 * @param port
	 * @param secured
	 * @param privateKey
	 * @return
	 */
	public SessionFactory<?> buildSessionFactory(String host, String username, String password, String port, boolean secured, Resource privateKey) {

		return secured ? this.buildSFtpSessionFactory(host, username, password, port, privateKey)
				: this.buildFtpSessionFactory(host, username, password, port);
	}

	/**
	 * Create an ftp session factory.
	 *
	 * @param host
	 * @param username
	 * @param password
	 * @return
	 */
	private DefaultFtpSessionFactory buildFtpSessionFactory(String host, String username, String password, String port) {
		DefaultFtpSessionFactory ftpSessionFactory = new DefaultFtpSessionFactory();
		ftpSessionFactory.setHost(host);
		ftpSessionFactory.setClientMode(0);
		ftpSessionFactory.setFileType(0);
		ftpSessionFactory.setPort(port.isEmpty() ? 21 : Integer.parseInt(port));
		ftpSessionFactory.setUsername(username);
		ftpSessionFactory.setPassword(password);

		return ftpSessionFactory;
	}

	/**
	 * Creates an sftp session factory.
	 *
	 * @param host
	 * @param username
	 * @param password
	 * @return
	 */
	private DefaultSftpSessionFactory buildSFtpSessionFactory(String host, String username, String password, String port, Resource privateKey) {
		DefaultSftpSessionFactory ftpSessionFactory = new DefaultSftpSessionFactory();
		if (privateKey != null) {
			ftpSessionFactory.setPrivateKey(privateKey);
			ftpSessionFactory.setPrivateKeyPassphrase(password);
		} else {
			ftpSessionFactory.setPassword(password);
		}
		ftpSessionFactory.setHost(host);
		ftpSessionFactory.setUser(username);
		ftpSessionFactory.setPort(port.isEmpty() ? 22 : Integer.parseInt(port));

		return ftpSessionFactory;
	}

}
