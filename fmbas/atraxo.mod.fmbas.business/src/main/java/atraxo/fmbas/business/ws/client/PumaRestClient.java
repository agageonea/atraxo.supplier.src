package atraxo.fmbas.business.ws.client;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.SSLSession;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StringUtils;
import org.xml.sax.SAXException;

import atraxo.fmbas.business.api.ws.IRestClientCallbackService;
import atraxo.fmbas.business.api.ws.WSModelToDTOTransformer;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.business.ws.ExternalInterfaceWSHelper;
import atraxo.fmbas.business.ws.GUIDMessageGenerator;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceHistory;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfacesHistoryStatus;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import atraxo.fmbas.domain.ws.dto.WSPayloadDataDTO;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import atraxo.fmbas.domain.ws.messgeXSD.MSG.HeaderCommon;
import atraxo.fmbas.domain.ws.messgeXSD.MSG.Payload;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.Session;
import seava.j4e.commons.utils.XMLUtils;

/**
 * @author zspeter
 */
public class PumaRestClient<E extends AbstractEntity> implements Callable<WSExecutionResult> {

	// private static final String FALSE = "false";

	private static final Logger LOGGER = LoggerFactory.getLogger(PumaRestClient.class);

	private ExternalInterface extInterface;
	private Collection<E> list;
	private ExternalInterfaceHistoryFacade historyFacade;

	private IRestClientCallbackService<E> callbackSrv;
	private ExternalInterfaceMessageHistoryFacade messageHistoryFacade;

	private WSModelToDTOTransformer<E> transformer;
	private ExtInterfaceParameters params;

	private IUser user;

	private Map<Integer, Integer> listHistoryMap;

	private DateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
	private DateFormat sdfTime = new SimpleDateFormat("hhmmss");

	private String sourceCompany;

	private ThreadPoolTaskExecutor taskExecutor;

	/**
	 * Default constructor with <code>IEntityService</code> used for timeout and retry mechanism; the timeout operations wil be performed (or at least
	 * an atempt to do so) if the "timeout" parameter will be present in the parameter Map
	 *
	 * @param extInterface the external interface called for operation with third party REST Client
	 * @param params the full <code>ExtInterfaceParameters</code> map with parameters (these parameters will be put on the Header of the message)
	 * @param list the <code>Collection</code> with the entities to be transported in the Payload of the message
	 * @param historyFacade the history <code>IEntityService</code> used for storing the history operations
	 * @param transformer a <code>WSModelToDTOTransformer</code> used for transforming the objects in XML
	 * @param entityService the <code>IEntityService</code> used for updating information regarding the exported entitites
	 * @param messageHistoryFacade
	 */
	public PumaRestClient(ThreadPoolTaskExecutor taskExecutor, ExternalInterface extInterface, ExtInterfaceParameters params, Collection<E> list,
			ExternalInterfaceHistoryFacade historyFacade, WSModelToDTOTransformer<E> transformer,
			ExternalInterfaceMessageHistoryFacade messageHistoryFacade) {

		this.taskExecutor = taskExecutor;
		this.extInterface = extInterface;
		this.historyFacade = historyFacade;
		this.list = list;
		this.transformer = transformer;
		this.params = params;
		this.user = Session.user.get();
		this.messageHistoryFacade = messageHistoryFacade;

		this.listHistoryMap = new HashMap<>();
	}

	/**
	 * @param seconds - Define the number of seconds to wait for external interface response.
	 * @return
	 * @throws BusinessException
	 */
	public WSExecutionResult start(int seconds) throws BusinessException {
		if (!this.extInterface.getEnabled()) {
			throw new BusinessException(BusinessErrorCode.INTERFACE_DISABLED, BusinessErrorCode.INTERFACE_DISABLED.getErrMsg());
		}

		Future<WSExecutionResult> future = this.taskExecutor.submit(this);
		try {
			return future.get(seconds, TimeUnit.SECONDS);
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		} catch (TimeoutException e) {
			LOGGER.warn("External interface execution takes more than 10 seconds. " + e.getMessage(), e);
			return null;
		}

	}

	@Override
	public WSExecutionResult call() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START call()");
		}

		WSExecutionResult result = new WSExecutionResult();
		StopWatch watch = new StopWatch();
		watch.start();
		ExternalInterfaceHistory eiHistory = null;
		MSG msg = null;
		Session.user.set(this.user);
		String messageID = GUIDMessageGenerator.generateMessageID();
		try {

			WSPayloadDataDTO payloadDto = this.transformer.transformModelToDTO(this.list, this.params.getParamMap());
			msg = this.buildMSG(payloadDto, messageID);
			Entity<MSG> entityMsg = Entity.xml(msg);
			this.validate(entityMsg);

			Invocation.Builder builder = this.createInvocationBuilder();

			int retryNr = Integer.parseInt(this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_NR_RETRIES));
			long retryInt = Long.parseLong(this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_RETRIES_INTERVAL));
			int retries = 0; // set as -1 just to enter the cycle (and call REST Client) at least once
			do {
				watch.reset();
				watch.start();
				retries++;
				try {
					Response response = builder.post(entityMsg);

					watch.stop();

					this.handleResponse(result, watch, msg,
							String.format(BusinessErrorCode.INTERFACE_RETRY_ATTEMPT_NR.getErrMsg(), retries, retryNr, this.extInterface.getName()),
							response);
					if (response.getStatus() != Response.Status.OK.getStatusCode()) {
						this.sleep(retryNr, retryInt, retries);
					} else {
						// end while
						retries = retryNr + 1;
					}
				} catch (ProcessingException e) {
					watch.stop();
					LOGGER.info("Procession exception: " + e.getMessage(), e);
					result.setSuccess(false);
					result.setExecutionData(XMLUtils.toString(msg, MSG.class, true, true));
					result.appendExecutionData("Procession exception: " + e.getMessage());
					result.appendExecutionData(
							String.format(BusinessErrorCode.INTERFACE_RETRY_ATTEMPT_NR.getErrMsg(), retries, retryNr, this.extInterface.getName()));

					this.sleep(retryNr, retryInt, retries);
				} finally {
					eiHistory = this.saveHistory(watch, result, eiHistory, messageID);
				}
			} while (retries <= retryNr);

			// execute finishing operations (if the case)
			if (this.callbackSrv != null) {
				this.callbackSrv.finish(result, this.list);
			}
			this.saveMessage(msg,
					result.isSuccess() ? ExternalInterfaceMessageHistoryStatus._SENT_ : ExternalInterfaceMessageHistoryStatus._FAILED_COMMUNICATION_);

			this.messageHistoryFacade.uploadToS3(XMLUtils.toString(msg, MSG.class, true, true), messageID, true);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			// save history, even if error
			result.setExecutionData(e.getLocalizedMessage() + "::" + ExceptionUtils.getStackTrace(e));

			this.saveHistory(watch, result, eiHistory, messageID);
		}
		Session.user.set(null);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END call()");
		}
		return result;
	}

	private void sleep(int retryNr, long retryInt, int retries) throws InterruptedException {
		if (retryNr == 0) {// this is needed because there needs to be a delay before setting it to Failed (first is In progress)
			Thread.sleep(3l * 1000);
		} else if (retries != retryNr) {// for the last cycle don't wait up to perform finishing operations
			Thread.sleep(retryInt * 60 * 1000);
		}
	}

	private void handleResponse(WSExecutionResult result, StopWatch watch, MSG msg, String retryMsg, Response response)
			throws JAXBException, BusinessException {
		result.setExecutionData(XMLUtils.toString(msg, MSG.class, true, true));
		if (response.getStatus() == Response.Status.OK.getStatusCode()) {
			LOGGER.info("RESPONSE OK !");
			result.setSuccess(true);
			// break;
		} else {
			LOGGER.info("RESPONSE NOT OK !");
			result.setSuccess(false);
			result.appendExecutionData(
					"Response code:" + response.getStatusInfo().getStatusCode() + ";response message:" + response.getStatusInfo().getReasonPhrase());
			result.appendExecutionData(retryMsg);
		}
	}

	private void validate(Entity<MSG> entityMsg) throws SAXException, IOException {
		try {
			String validationXsd = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_VALIDATION_XSD);
			SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = factory.newSchema(ClassPathResource.class.getResource(validationXsd));
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(entityMsg.toString()));
		} catch (IllegalArgumentException e) {
			LOGGER.warn("Output message can't be validated. Validation XSD parameter is not set.", e);
		}
	}

	/**
	 * Builds the <code>MSG</code> containing the Header and the Payload
	 *
	 * @param payloadDto
	 * @param result
	 * @return
	 */
	private MSG buildMSG(WSPayloadDataDTO payloadDto, String messageID) {
		MSG msg = new MSG();

		// first, try to get the parameters
		String interfaceType = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_INTERFACE_TYPE);
		String sourceGroupCompany = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_SOURCE_GROUP_COMPANY);
		String destinationGroupComapany = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_DESTINATION_GROUP_COMPANY);
		String sourceCompanyParam = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_SOURCE_COMPANY);

		String contractHolder = StringUtils.isEmpty(sourceCompanyParam) ? this.sourceCompany : sourceCompanyParam;

		String destinationCompany = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_DESTINATION_COMPANY);
		String sourceResponseAddress = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_SOURCE_RESPONSE_ADDRESS);
		String sourceUpdateStatusAddress = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_SOURCE_UPDATE_STATUS_ADDRESS);
		String destinationUpdateStatusAddress = this.params
				.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_DESTINATION_UPDATE_STATUS_ADDRESS);
		String middlewareUrlForPush = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_MIDDLEWARE_URL_FOR_PUSH);
		String emailNotification = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_EMAIL_NOTIFICATION);
		Boolean continueOnError = Boolean.valueOf(this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_CONTINUE_ON_ERROR));
		Boolean comprehensiveLogging = Boolean.valueOf(this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_COMPREHENSIVE_LOGGING));
		String transportStatus = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_VALUE_WS_TRANSPORT_STATUS);
		Boolean updateSourceOnReceive = Boolean
				.valueOf(this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_UPDATE_SOURCE_ON_RECEIVE));
		Boolean updateSourceOnDelivery = Boolean
				.valueOf(this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_UPDATE_SOURCE_ON_DELIVERY));
		Boolean updateSourceAfterProcessing = Boolean
				.valueOf(this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_UPDATE_SOURCE_AFTER_PROCESSING));
		Boolean updateDestinationOnDelivery = Boolean
				.valueOf(this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_UPDATE_DESTINATION_ON_DELIVERY));
		Boolean callDestinationForProcessing = Boolean
				.valueOf(this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_CALL_DESTINATION_FOR_PROCESSING));
		String objectType = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_OBJECT_TYPE);
		String objectName = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_OBJECT_NAME);
		String communicationType = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_VALUE_WS_COMMUNICATION_TYPE);
		String messageIDExtension = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_FILE_EXTENSION);
		String messageIDPrefix = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_MESSAGE_ID_PREFIX);

		HeaderCommon header = new HeaderCommon();
		header.setMsgID(messageID);
		header.setInterfaceType(interfaceType);
		header.setSourceGroupCompany(sourceGroupCompany);
		header.setDestinationGroupCompany(destinationGroupComapany);
		header.setSourceCompany(contractHolder);
		header.setDestinationCompany(destinationCompany);
		header.setSourceResponseAddress(sourceResponseAddress);
		header.setSourceUpdateStatusAddress(sourceUpdateStatusAddress);
		header.setDestinationUpdateStatusAddress(destinationUpdateStatusAddress);
		header.setMiddlewareUrlForPush(middlewareUrlForPush);
		header.setEmailNotification(emailNotification);
		header.setObjectName(objectName);
		header.setTransportStatus(transportStatus);
		header.setCommunicationType(communicationType);
		header.setContinueOnError(continueOnError);
		header.setComprehensiveLogging(comprehensiveLogging);
		header.setUpdateSourceOnReceive(updateSourceOnReceive);
		header.setUpdateDestinationOnDelivery(updateDestinationOnDelivery);
		header.setCallDestinationForProcessing(callDestinationForProcessing);
		header.setCorrelationID("");
		header.setErrorCode("");
		header.setErrorDescription("");
		header.setProcessingErrorDescription("");
		header.setProcessStatus("");
		header.setObjectType(objectType);

		header.setDestinationAddress(
				this.buildDestinationAddress(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_DESTINATION_ADDRESS, messageIDExtension, messageIDPrefix));
		header.setUpdateSourceOnDelivery(updateSourceOnDelivery);
		header.setUpdateSourceAfterProcessing(updateSourceAfterProcessing);
		msg.setHeaderCommon(header);

		Payload payload = new Payload();
		payload.setAny(payloadDto.getPayload());
		msg.setPayload(payload);
		msg.setStatusPayload("");
		return msg;
	}

	/**
	 * Saves history
	 *
	 * @param stopWatch
	 * @param result
	 * @throws BusinessException
	 */
	private ExternalInterfaceHistory saveHistory(StopWatch stopWatch, WSExecutionResult result, ExternalInterfaceHistory history, String msgId)
			throws BusinessException {
		ExternalInterfaceHistory modifiedHistory = new ExternalInterfaceHistory();
		if (history != null) {
			modifiedHistory = history;
		}
		modifiedHistory.setExternalInterface(this.extInterface);
		modifiedHistory.setRequester(Session.user.get().getClientCode());
		modifiedHistory.setRequestReceived(new Date(stopWatch.getStartTime()));
		modifiedHistory.setResponseSent(new Date(stopWatch.getStartTime() + stopWatch.getTime()));
		modifiedHistory
				.setStatus(result.isSuccess() ? ExternalInterfacesHistoryStatus._SUCCESS_ : ExternalInterfacesHistoryStatus._FAILED_COMMUNICATION_);
		modifiedHistory.setResult(result.getTrimmedExecutionData());
		modifiedHistory.setMsgId(msgId);
		if (history != null) {
			this.historyFacade.update(modifiedHistory);
			modifiedHistory = this.historyFacade.findById(modifiedHistory.getId());
		} else {
			this.historyFacade.insert(modifiedHistory);
		}
		return modifiedHistory;
	}

	/**
	 * @param messageId
	 * @param status
	 * @throws BusinessException
	 * @throws JAXBException
	 */
	private void saveMessage(MSG message, ExternalInterfaceMessageHistoryStatus status) throws BusinessException {
		List<ExternalInterfaceMessageHistory> insertList = new ArrayList<>();
		if (!this.list.isEmpty()) {
			// gets the (first element/only element) from the list
			AbstractEntity e = this.list.iterator().next();
			String objectTypeValue = this.params.getParamMap().containsKey(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_OBJECT_TYPE)
					? this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_OBJECT_TYPE)
					: e.getEntityAlias();

			ExternalInterfaceMessageHistory eimh = new ExternalInterfaceMessageHistory();
			eimh.setMessageId(message.getHeaderCommon().getMsgID());
			eimh.setExternalInterface(this.extInterface);
			eimh.setExecutionTime(GregorianCalendar.getInstance().getTime());
			eimh.setObjectRefId(this.list.size() == 1 ? e.getRefid() : "");
			eimh.setObjectId(this.list.size() == 1 ? e.getId() : 0); // Puts an id of 0, 0 being an invalid id
			eimh.setObjectType(objectTypeValue);
			eimh.setStatus(status);
			insertList.add(eimh);
			this.messageHistoryFacade.insert(eimh);

			// make sure to put in the history map the transmitted entity + history message entry
			this.listHistoryMap.put(e.getId(), eimh.getId());
		} else {
			throw new BusinessException(BusinessErrorCode.INTERFACE_EXPORT_NO_ENTITIES, BusinessErrorCode.INTERFACE_EXPORT_NO_ENTITIES.getErrMsg());
		}
	}

	/**
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws IOException
	 * @throws KeyStoreException
	 */
	private Invocation.Builder createInvocationBuilder() throws NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
		ClientBuilder clientBuilder = ClientBuilder.newBuilder();

		InputStream fileInputStreamJKS = null;
		try {
			char[] password = this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_KEY_STORE_PWD).toCharArray();

			KeyStore trustStore = KeyStore.getInstance(this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_KEY_STORE_TYPE));
			fileInputStreamJKS = this.getClass().getClassLoader()
					.getResourceAsStream(this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_KEY_STORE));
			trustStore.load(fileInputStreamJKS, password);
			clientBuilder.trustStore(trustStore);
			clientBuilder.keyStore(trustStore, password);
			clientBuilder.hostnameVerifier((String hostName, SSLSession session) -> hostName.equalsIgnoreCase(session.getPeerHost()));

		} catch (IllegalArgumentException e) {
			LOGGER.warn(e.getMessage(), e);
			LOGGER.warn("Can't set trustore to client builder. Application will try to create an unsecure connection.");
		} finally {
			if (fileInputStreamJKS != null) {
				fileInputStreamJKS.close();
			}
		}
		Client client = clientBuilder.build();
		WebTarget target = client.target(this.params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_RS_URL));
		return target.request(MediaType.APPLICATION_XML);
	}

	/**
	 * @param paramName
	 * @return
	 */
	private String buildDestinationAddress(String paramName, String messageIDExtension, String messageIDPrefix) {
		StringBuilder sb = new StringBuilder();
		if (this.params.hasParam(ExternalInterfaceWSHelper.CAN_MODIFY_DESTINATION_ADDRESS)
				&& !this.params.getParamValue(ExternalInterfaceWSHelper.CAN_MODIFY_DESTINATION_ADDRESS).isEmpty()
				&& Boolean.FALSE.toString().equalsIgnoreCase(this.params.getParamValue(ExternalInterfaceWSHelper.CAN_MODIFY_DESTINATION_ADDRESS))) {
			sb.append(this.params.getParamValue(paramName));
			return sb.toString();
		}
		Date date = new Date();
		if (this.params.hasParam(paramName) && !this.params.getParamValue(paramName).isEmpty()) {
			sb.append(this.params.getParamValue(paramName));
			sb.append(this.sdfDate.format(date));
			sb.append("T");
			sb.append(this.sdfTime.format(date));
		} else {
			sb.append(messageIDPrefix);
			sb.append(StringUtils.isEmpty(messageIDPrefix) ? "" : "_");
			sb.append(this.sdfDate.format(date));
			sb.append("T");
			sb.append(this.sdfTime.format(date));
			sb.append(StringUtils.isEmpty(messageIDExtension) ? "" : "." + messageIDExtension);
			return sb.toString();
		}
		return sb.toString();
	}

	/**
	 * @param callbackSrv
	 */
	public void setCallbackSrv(IRestClientCallbackService<E> callbackSrv) {
		this.callbackSrv = callbackSrv;
	}

	/**
	 * @param sourceCompany
	 */
	public void setSourceCompany(String sourceCompany) {
		this.sourceCompany = sourceCompany;
	}

}
