/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.externalInterfaces.service;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceParameterService;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceParameter;

/**
 * Business extensions specific for {@link ExternalInterfaceParameter} domain entity.
 * 
 */
public class ExternalInterfaceParameter_Service extends atraxo.fmbas.business.impl.externalInterfaces.ExternalInterfaceParameter_Service 
	implements IExternalInterfaceParameterService{

	//Implement me

}
