/**
 *
 */
package atraxo.fmbas.business.ws.fromNav;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;
import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.CollectionUtils;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceMessageHistoryService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.business.ws.ExternalInterfaceWSHelper;
import atraxo.fmbas.business.ws.GUIDMessageGenerator;
import atraxo.fmbas.business.ws.client.PumaRestClient;
import atraxo.fmbas.business.ws.toEbits.WSCustomerToDTOTransformer;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceHistory;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataProcessedStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataTransmissionStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfacesHistoryStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ProcessStatus;
import atraxo.fmbas.domain.impl.fmbas_type.TransportStatus;
import atraxo.fmbas.domain.ws.commonResponse.Any;
import atraxo.fmbas.domain.ws.commonResponse.Any.Acknowledgement.DataSets.DataSet;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import atraxo.fmbas.domain.ws.dto.WSExecutionType;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import atraxo.fmbas.domain.ws.messgeXSD.MSG.HeaderCommon;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.commons.utils.XMLUtils;

/**
 * @author vhojda
 * @author abolindu
 */
@WebService
public class AcknowledgeExportCustomersWSImpl implements AcknowledgeExportCustomersWS {

	private static final Logger LOGGER = LoggerFactory.getLogger(AcknowledgeExportCustomersWSImpl.class);

	/**
	 * Generic Interfaces Beans
	 */
	@Autowired
	private IExternalInterfaceService externalInterfaceService;
	@Autowired
	private ExternalInterfaceHistoryFacade historyFacade;
	@Autowired
	private IUserSuppService userSrv;
	@Autowired
	private ICustomerService customerService;
	@Autowired
	private IExternalInterfaceMessageHistoryService messageService;
	@Autowired
	private ExternalInterfaceMessageHistoryFacade messageFacade;
	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;

	@Override
	public MSG acknowledgeExportCustomersEBits(MSG request)
			throws JAXBException, SAXException, IOException, BusinessException, InstantiationException, IllegalAccessException {
		MSG response = new MSG();

		HeaderCommon header = request.getHeaderCommon();
		response.setHeaderCommon(header);

		// BEGIN - Workaround for copying header and message request
		MSG msgCopy = request.getClass().newInstance();
		BeanUtils.copyProperties(request, msgCopy);
		HeaderCommon headerCpy = header.getClass().newInstance();
		BeanUtils.copyProperties(header, headerCpy);
		msgCopy.setHeaderCommon(headerCpy);
		// END - Workaround

		// start constructing the interface execution result
		WSExecutionResult execResult = new WSExecutionResult();
		execResult.setExecutionType(WSExecutionType.EXPORT_DATA);
		execResult.setSuccess(false);// assume false

		// first, try to set up the client Session (even if disabled)
		boolean foundErrors = false;
		String clientIdentifier = null;
		try {
			clientIdentifier = GUIDMessageGenerator.extractClientIdentifier(header.getCorrelationID());
			this.userSrv.createSessionUser(clientIdentifier);
		} catch (Exception e) {
			foundErrors = true;
			LOGGER.error("ERROR: could not process received WS Client ID !!", e);
			this.fillHeaderErrorData(String.format(BusinessErrorCode.INTERFACE_NO_CLIENT_IDENTIFIER.getErrMsg(), clientIdentifier), header,
					execResult, e);
		}

		// if no errors so far and interface is active proceed with operations
		if (!foundErrors) {
			ExternalInterface ackExportCustomersExternalInterface = this.externalInterfaceService
					.findByName(ExtInterfaceNames.ACK_EXPORT_CUSTOMER_DATA_EBITS.getValue());

			if (ackExportCustomersExternalInterface.getEnabled()) {
				// set the XML as received data
				String xmlMSG = XMLUtils.toString(request, MSG.class, true, true);
				execResult.setExecutionData(xmlMSG);

				// result is the one from the "TransportStatus" element
				execResult.setSuccess(header.getTransportStatus().equals(TransportStatus._SENT_.getName()));

				// save history
				this.saveHistory(execResult, ackExportCustomersExternalInterface);
				this.historyFacade.updateHistoryOutboundFromACK(execResult.isSuccess(), msgCopy.getHeaderCommon().getCorrelationID(), false);

			} else {
				this.fillHeaderErrorData(BusinessErrorCode.INTERFACE_DISABLED.getErrMsg(), header, execResult, null);
			}

			// set up MSG Ids (they depend on the session)
			header.setCorrelationID(header.getMsgID());
			header.setMsgID(GUIDMessageGenerator.generateMessageID());
		}

		// set the header statuses
		header.setTransportStatus(TransportStatus._PROCESSED_.getName());
		header.setProcessStatus(execResult.isSuccess() ? ProcessStatus._SUCCESS_.getName() : ProcessStatus._FAILURE_.getName());

		this.updateMessageHistory(msgCopy, execResult.isSuccess());

		return response;

	}

	/**
	 * @param messageId
	 * @param success
	 * @throws BusinessException
	 * @throws JAXBException
	 */
	private void updateMessageHistory(MSG request, boolean success) throws BusinessException, JAXBException {
		Map<String, Object> params = new HashMap<>();
		params.put("messageId", request.getHeaderCommon().getCorrelationID());
		List<ExternalInterfaceMessageHistory> messagesHistory = this.messageService.findEntitiesByAttributes(params);
		if (messagesHistory != null && !messagesHistory.isEmpty()) {
			for (ExternalInterfaceMessageHistory message : messagesHistory) {
				message.setStatus(success ? ExternalInterfaceMessageHistoryStatus._ACKNOWLEDGED_
						: ExternalInterfaceMessageHistoryStatus._FAILED_NEGATIVE_ACKNOWLEDGEMENT_);
				message.setResponseTime(new Date());
				message.setResponseMessageId(request.getHeaderCommon().getMsgID());
			}
			this.messageFacade.update(messagesHistory);
			this.messageFacade.uploadToS3(XMLUtils.toString(request, MSG.class, true, true), request.getHeaderCommon().getMsgID(), false);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ws.fromNav.AcknowledgeExportCustomersWS#acknowledgeExportCustomers(atraxo.fmbas.domain.ws.Acknowledgement)
	 */
	@Override
	public MSG acknowledgeExportCustomers(MSG request)
			throws JAXBException, SAXException, IOException, BusinessException, InstantiationException, IllegalAccessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START acknowledgeExportCustomers()");
		}
		MSG response = new MSG();

		HeaderCommon header = request.getHeaderCommon();
		response.setHeaderCommon(header);

		// BEGIN - Workaround for copying header and message request
		MSG msgCopy = request.getClass().newInstance();
		BeanUtils.copyProperties(request, msgCopy);
		HeaderCommon headerCpy = header.getClass().newInstance();
		BeanUtils.copyProperties(header, headerCpy);
		msgCopy.setHeaderCommon(headerCpy);
		// END - Workaround

		// start constructing the interface execution result
		WSExecutionResult execResult = new WSExecutionResult();
		execResult.setExecutionType(WSExecutionType.EXPORT_DATA);
		execResult.setSuccess(false);// assume false

		// first, try to set up the client Session (even if disabled)
		boolean foundErrors = false;
		String clientIdentifier = null;
		try {
			clientIdentifier = GUIDMessageGenerator.extractClientIdentifier(header.getCorrelationID());
			this.userSrv.createSessionUser(clientIdentifier);
		} catch (Exception e) {
			foundErrors = true;
			LOGGER.error("ERROR: could not process received WS Client ID !!", e);
			this.fillHeaderErrorData(String.format(BusinessErrorCode.INTERFACE_NO_CLIENT_IDENTIFIER.getErrMsg(), clientIdentifier), header,
					execResult, e);
		}

		// if no errors so far and interface is active proceed with operations
		if (!foundErrors) {
			ExternalInterface ackExportCustomersExternalInterface = this.externalInterfaceService
					.findByName(ExtInterfaceNames.ACK_EXPORT_CUSTOMER_DATA_NAV.getValue());

			if (ackExportCustomersExternalInterface.getEnabled()) {

				// extract the Acknowledgement from received XML
				Element acknowledgementAnyElement = request.getPayload().getAny();
				Any acknowledgementAny = null;
				try {
					acknowledgementAny = (Any) XMLUtils.toObject(acknowledgementAnyElement, Any.class);
				} catch (Exception e) {
					LOGGER.error("ERROR:could not extract Acknowledgement from received XML !", e);
					this.fillHeaderErrorData(BusinessErrorCode.INTERFACE_ACK_EXPORT_CUSTOMERS_NO_ACK.getErrMsg(), header, execResult, e);
				}

				// if ACK is received then proceed to parse it
				if (acknowledgementAny != null && acknowledgementAny.getAcknowledgement() != null
						&& acknowledgementAny.getAcknowledgement().getDataSets() != null
						&& !CollectionUtils.isEmpty(acknowledgementAny.getAcknowledgement().getDataSets().getDataSet())) {

					DataSet dataSet = acknowledgementAny.getAcknowledgement().getDataSets().getDataSet().get(0);
					// set the XML as received data
					String xmlMSG = XMLUtils.toString(request, MSG.class, true, true);
					execResult.setExecutionData(xmlMSG);

					String customerCode = dataSet.getCustomerAccountNumber();
					Customer customer = this.findCustomerByCode(customerCode, header, execResult);
					if (customer != null) {

						// also check for valid ERP No
						if (!StringUtils.isEmpty(dataSet.getErpNumber()) && customer.getStatus().equals(CustomerStatus._PROSPECT_)) {
							String erpNumber = dataSet.getErpNumber();
							if (!customer.getAccountNumber().equalsIgnoreCase(erpNumber)) {
								String msg = String.format(BusinessErrorCode.INTERFACE_CUSTOMERS_DIFFERENT_ACCOUNT_NUMBER.getErrMsg(), customerCode,
										erpNumber, customer.getAccountNumber());
								execResult.appendExecutionData(msg);
								LOGGER.warn(msg);
							}
							customer.setAccountNumber(erpNumber);
							customer.setStatus(CustomerStatus._ACTIVE_);
						}

						// set the new transmission status
						customer.setTransmissionStatus(
								dataSet.isDelivered() ? CustomerDataTransmissionStatus._PROCESSED_ : CustomerDataTransmissionStatus._FAILED_);
						// set the new processed status
						customer.setProcessedStatus(
								dataSet.isProcessed() ? CustomerDataProcessedStatus._SUCCESS_ : CustomerDataProcessedStatus._FAILED_);

						// update the customer
						this.customerService.update(customer);

						// if the customer is processed, it will be sent to eBits
						if (dataSet.isProcessed()) {
							this.exportCustomersToEBits(customer);
						}

						// the result is a success if at least one result is OK
						if (dataSet.isProcessed()) {
							execResult.setSuccess(true);
						}
					}
				} else {
					LOGGER.error("ERROR:could not extract Acknowledgement from received XML !");
					this.fillHeaderErrorData(BusinessErrorCode.INTERFACE_ACK_EXPORT_CUSTOMERS_NO_ACK.getErrMsg(), header, execResult, null);
				}

				// save history
				this.saveHistory(execResult, ackExportCustomersExternalInterface);
				this.historyFacade.updateHistoryOutboundFromACK(execResult.isSuccess(), msgCopy.getHeaderCommon().getCorrelationID(), true);

			} else {
				this.fillHeaderErrorData(BusinessErrorCode.INTERFACE_DISABLED.getErrMsg(), header, execResult, null);
			}

			// set up MSG Ids (they depend on the session)
			header.setCorrelationID(header.getMsgID());
			header.setMsgID(GUIDMessageGenerator.generateMessageID());
		}

		this.updateMessageHistory(msgCopy, execResult.isSuccess());

		// set the header statuses
		header.setTransportStatus(TransportStatus._PROCESSED_.getName());
		header.setProcessStatus(execResult.isSuccess() ? ProcessStatus._SUCCESS_.getName() : ProcessStatus._FAILURE_.getName());

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END acknowledgeExportCustomers()");
		}
		return response;
	}

	/**
	 * @param customerCode
	 * @param header
	 * @param execResult
	 * @return
	 */
	private Customer findCustomerByCode(String customerCode, HeaderCommon header, WSExecutionResult execResult) {
		Customer customer = null;

		if (!StringUtils.isEmpty(customerCode)) {
			try {
				customer = this.customerService.findByCode(customerCode);
				if (customer == null) {
					this.fillHeaderErrorData(String.format(BusinessErrorCode.INTERFACE_FIND_CUSTOMER_INVALID_CODE.getErrMsg(), customerCode), header,
							execResult, null);
				}
			} catch (Exception e) {
				LOGGER.error("ERROR: received a bad customer code : " + customerCode, e);
				this.fillHeaderErrorData(String.format(BusinessErrorCode.INTERFACE_FIND_CUSTOMER_INVALID_CODE.getErrMsg(), customerCode), header,
						execResult, e);
			}
		} else {
			LOGGER.error("ERROR: received an empty customer code : " + customerCode);
			this.fillHeaderErrorData(String.format(BusinessErrorCode.INTERFACE_FIND_CUSTOMER_INVALID_CODE.getErrMsg(), customerCode), header,
					execResult, null);
		}

		return customer;
	}

	/**
	 * Saves history
	 *
	 * @param result
	 * @param success
	 * @throws BusinessException
	 */
	private void saveHistory(WSExecutionResult result, ExternalInterface ackExportCustomersExternalInterface) throws BusinessException {
		ExternalInterfaceHistory history = new ExternalInterfaceHistory();
		history.setExternalInterface(ackExportCustomersExternalInterface);
		history.setRequester(Session.user.get().getClientCode());
		history.setRequestReceived(new Date());
		history.setResponseSent(new Date());
		history.setStatus(
				result.isSuccess() ? ExternalInterfacesHistoryStatus._SUCCESS_ : ExternalInterfacesHistoryStatus._FAILED_NEGATIVE_ACKNOWLEDGEMENT_);
		history.setResult(result.getTrimmedExecutionData());
		this.historyFacade.insert(history);
	}

	/**
	 * @param errorMsg
	 * @param header
	 * @param execResult
	 * @param e
	 */
	private void fillHeaderErrorData(String errorMsg, HeaderCommon header, WSExecutionResult execResult, Exception e) {
		header.setErrorDescription(errorMsg);
		if (e != null) {
			header.setProcessingErrorDescription(e.getLocalizedMessage());
		}
		execResult.appendExecutionData(
				header.getErrorDescription() + (e != null ? header.getProcessingErrorDescription() + "::" + ExceptionUtils.getStackTrace(e) : ""));
	}

	/**
	 * Send Customer to eBits after it gets an acknoledgement from Nav
	 *
	 * @param id
	 * @throws BusinessException
	 */
	private void exportCustomersToEBits(Customer customer) throws BusinessException {
		List<Customer> customers = new ArrayList<>();
		customers.add(customer);

		if (!customers.isEmpty()) {
			ExternalInterface extInterface = this.externalInterfaceService.findByName(ExtInterfaceNames.EXPORT_CUSTOMERS_TO_EBITS.getValue());
			if (extInterface.getEnabled()) {
				ExtInterfaceParameters params = new ExtInterfaceParameters(extInterface.getParams());
				params.getParamMap().remove(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_DESTINATION_ADDRESS);
				PumaRestClient<Customer> client = new PumaRestClient<>(this.taskExecutor, extInterface, params, customers, this.historyFacade,
						new WSCustomerToDTOTransformer(), this.messageFacade);
				client.start(1);
			}
		}
	}
}
