/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.vat.service;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import atraxo.fmbas.business.api.vat.IVatRateService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.vat.Vat;
import atraxo.fmbas.domain.impl.vat.VatRate;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link VatRate} domain entity.
 */
public class VatRate_Service extends atraxo.fmbas.business.impl.vat.VatRate_Service implements IVatRateService {

	@Override
	protected void preInsert(VatRate e) throws BusinessException {
		super.preInsert(e);
		this.checkOverlapping(e);
	}

	@Override
	protected void preUpdate(VatRate e) throws BusinessException {
		super.preUpdate(e);
		this.checkOverlapping(e);
	}

	private void checkOverlapping(VatRate vr) throws BusinessException {
		Collection<VatRate> vatRates = vr.getVat().getVatRate();
		for (VatRate v : vatRates) {
			if (vr.getId() == null || !vr.getId().equals(v.getId())) {
				if ((this.isBetweenDates(vr.getValidFrom(), v.getValidFrom(), v.getValidTo())
						|| this.isBetweenDates(vr.getValidTo(), v.getValidFrom(), v.getValidTo()))
						|| this.isBetweenDates(v.getValidFrom(), vr.getValidFrom(), vr.getValidTo())
						|| this.isBetweenDates(v.getValidTo(), vr.getValidFrom(), vr.getValidTo())) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					throw new BusinessException(BusinessErrorCode.VAT_RATE_OVERLAPPING, String
							.format(BusinessErrorCode.VAT_RATE_OVERLAPPING.getErrMsg(), sdf.format(v.getValidFrom()), sdf.format(v.getValidTo())));
				}
			}
		}
	}

	private boolean isBetweenDates(Date date, Date from, Date to) {
		return date.compareTo(from) > -1 && date.compareTo(to) < 1;
	}

	@Override
	public VatRate findVatRateByDate(Date date, Vat vat) throws BusinessException {
		Collection<VatRate> rates = vat.getVatRate();
		for (VatRate vr : rates) {
			if (vr.getValidFrom().compareTo(date) < 1 && vr.getValidTo().compareTo(date) > -1) {
				return vr;
			}
		}
		return null;
	}

}
