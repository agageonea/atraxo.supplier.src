/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.job;

import atraxo.fmbas.business.api.job.IJobChainUserService;
import atraxo.fmbas.domain.impl.job.JobChain;
import atraxo.fmbas.domain.impl.job.JobChainUser;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link JobChainUser} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class JobChainUser_Service extends AbstractEntityService<JobChainUser>
		implements
			IJobChainUserService {

	/**
	 * Public constructor for JobChainUser_Service
	 */
	public JobChainUser_Service() {
		super();
	}

	/**
	 * Public constructor for JobChainUser_Service
	 * 
	 * @param em
	 */
	public JobChainUser_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<JobChainUser> getEntityClass() {
		return JobChainUser.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return JobChainUser
	 */
	public JobChainUser findByKey(JobChain jobChain, UserSupp user) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(JobChainUser.NQ_FIND_BY_KEY,
							JobChainUser.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("jobChain", jobChain)
					.setParameter("user", user).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"JobChainUser", "jobChain, user"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"JobChainUser", "jobChain, user"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return JobChainUser
	 */
	public JobChainUser findByKey(Long jobChainId, Long userId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(JobChainUser.NQ_FIND_BY_KEY_PRIMITIVE,
							JobChainUser.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("jobChainId", jobChainId)
					.setParameter("userId", userId).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"JobChainUser", "jobChainId, userId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"JobChainUser", "jobChainId, userId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return JobChainUser
	 */
	public JobChainUser findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(JobChainUser.NQ_FIND_BY_BUSINESS,
							JobChainUser.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"JobChainUser", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"JobChainUser", "id"), nure);
		}
	}

	/**
	 * Find by reference: jobChain
	 *
	 * @param jobChain
	 * @return List<JobChainUser>
	 */
	public List<JobChainUser> findByJobChain(JobChain jobChain) {
		return this.findByJobChainId(jobChain.getId());
	}
	/**
	 * Find by ID of reference: jobChain.id
	 *
	 * @param jobChainId
	 * @return List<JobChainUser>
	 */
	public List<JobChainUser> findByJobChainId(Integer jobChainId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from JobChainUser e where e.clientId = :clientId and e.jobChain.id = :jobChainId",
						JobChainUser.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("jobChainId", jobChainId).getResultList();
	}
	/**
	 * Find by reference: user
	 *
	 * @param user
	 * @return List<JobChainUser>
	 */
	public List<JobChainUser> findByUser(UserSupp user) {
		return this.findByUserId(user.getId());
	}
	/**
	 * Find by ID of reference: user.id
	 *
	 * @param userId
	 * @return List<JobChainUser>
	 */
	public List<JobChainUser> findByUserId(String userId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from JobChainUser e where e.clientId = :clientId and e.user.id = :userId",
						JobChainUser.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("userId", userId).getResultList();
	}
}
