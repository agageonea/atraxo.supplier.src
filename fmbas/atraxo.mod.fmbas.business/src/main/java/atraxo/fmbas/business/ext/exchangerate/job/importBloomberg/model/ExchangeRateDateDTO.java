package atraxo.fmbas.business.ext.exchangerate.job.importBloomberg.model;

import java.math.BigDecimal;
import java.util.Date;

public class ExchangeRateDateDTO {

	private BigDecimal rate;

	private Date date;

	/**
	 * 
	 */
	public void DataDTO() {

	}

	public BigDecimal getRate() {
		return this.rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
