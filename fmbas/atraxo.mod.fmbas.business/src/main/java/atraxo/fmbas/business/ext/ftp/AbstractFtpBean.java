package atraxo.fmbas.business.ext.ftp;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.NestedIOException;
import org.springframework.integration.file.remote.session.Session;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.util.Assert;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.FTPUploadException;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Abstract base class for ftp communication.
 *
 * @author zspeter
 */
public abstract class AbstractFtpBean implements InitializingBean {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractFtpBean.class);

	private SessionFactory<?> sessionFactory;

	private String fileNamePattern;

	private String remoteDirectory;
	private int retryAttempts = 12;

	private long retryIntervalMilliseconds = 300000;

	/**
	 * @param sessionFactory
	 * @param remoteDirectory
	 * @param fileNamePattern
	 */
	public AbstractFtpBean(SessionFactory<?> sessionFactory, String remoteDirectory, String fileNamePattern) {
		super();
		this.sessionFactory = sessionFactory;
		this.remoteDirectory = remoteDirectory;
		this.fileNamePattern = fileNamePattern;
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(this.sessionFactory, "sessionFactory attribute cannot be null");
		Assert.notNull(this.remoteDirectory, "remoteDirectory attribute cannot be null");
		Assert.notNull(this.fileNamePattern, "fileNamePattern attribute cannot be null");

	}

	/**
	 * @param list
	 * @return
	 * @throws InterruptedException
	 * @throws BusinessException
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public abstract <T extends AbstractEntity> FtpCommunicationResult execute(T... list) throws IOException, InterruptedException, BusinessException;

	/**
	 * @param inputStream
	 * @param fileName - absolute file name
	 * @throws BusinessException
	 */
	protected void writeToFTP(InputStream inputStream, String fileName) throws BusinessException {
		Session<?> session = null;
		try {
			session = this.getSessionFactory().getSession();
			session.write(inputStream, fileName);
		} catch (NestedIOException nio) {
			this.debugMessage(nio);
			throw new BusinessException(BusinessErrorCode.FTP_CONNECTION_ERROR, BusinessErrorCode.FTP_CONNECTION_ERROR.getErrMsg(), nio);
		} catch (IOException e) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("failed to upload to ftp", e);
			}
			throw new FTPUploadException(e);
		} catch (IllegalStateException e1) {
			this.debugMessage(e1);
			throw new BusinessException(BusinessErrorCode.FTP_CONNECTION_ERROR, BusinessErrorCode.FTP_CONNECTION_ERROR.getErrMsg(), e1);
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception ignored) {
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("failed to close Session", ignored);
					}
				}
			}
		}
	}

	protected void debugMessage(Exception e) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(e.getMessage(), e);
		}
	}

	/**
	 * @return the fileNamePattern
	 */
	public String getFileNamePattern() {
		return this.fileNamePattern;
	}

	/**
	 * @return the remoteDirectory
	 */
	public String getRemoteDirectory() {
		return this.remoteDirectory;
	}

	/**
	 * @return the retryIntervalMilliseconds
	 */
	public long getRetryIntervalMilliseconds() {
		return this.retryIntervalMilliseconds;
	}

	/**
	 * @return the sessionFactory
	 */
	public SessionFactory<?> getSessionFactory() {
		return this.sessionFactory;
	}

	/**
	 * @return the sftp
	 */
	public boolean isSftp() {
		return this.sessionFactory instanceof DefaultSftpSessionFactory;
	}

	/**
	 * @param fileNamePattern the fileNamePattern to set
	 */
	public void setFileNamePattern(String fileNamePattern) {
		this.fileNamePattern = fileNamePattern;
	}

	/**
	 * @param remoteDirectory the remoteDirectory to set
	 */
	public void setRemoteDirectory(String remoteDirectory) {
		this.remoteDirectory = remoteDirectory;
	}

	/**
	 * @param sessionFactory the sessionFactory to set
	 */
	public void setSessionFactory(SessionFactory<?> sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * @param downloadFileAttempts the downloadFileAttempts to set
	 */
	public void setRetryAttempts(int retryAttempts) {
		this.retryAttempts = retryAttempts;
	}

	/**
	 * @return the downloadFileAttempts
	 */
	public int getRetryAttempts() {
		return this.retryAttempts;
	}

}
