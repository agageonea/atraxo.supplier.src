/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.dictionary;

import atraxo.fmbas.business.api.dictionary.IFieldService;
import atraxo.fmbas.domain.impl.dictionary.Field;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Field} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Field_Service extends AbstractEntityService<Field>
		implements
			IFieldService {

	/**
	 * Public constructor for Field_Service
	 */
	public Field_Service() {
		super();
	}

	/**
	 * Public constructor for Field_Service
	 * 
	 * @param em
	 */
	public Field_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Field> getEntityClass() {
		return Field.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Field
	 */
	public Field findByCode(Integer externalInterfaceId, String fieldName) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Field.NQ_FIND_BY_CODE, Field.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("externalInterfaceId", externalInterfaceId)
					.setParameter("fieldName", fieldName).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Field", "externalInterfaceId, fieldName"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Field", "externalInterfaceId, fieldName"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Field
	 */
	public Field findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Field.NQ_FIND_BY_BUSINESS, Field.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Field", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Field", "id"), nure);
		}
	}

}
