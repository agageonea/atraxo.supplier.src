/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.aircraft.service;

import java.util.Date;

import atraxo.fmbas.business.api.aircraft.IAircraftService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link Aircraft} domain entity.
 */
public class Aircraft_Service extends atraxo.fmbas.business.impl.aircraft.Aircraft_Service implements IAircraftService {

	@Override
	protected void preInsert(Aircraft e) throws BusinessException {
		super.preInsert(e);
		this.verificateDates(e.getValidFrom(), e.getValidTo());
	}

	@Override
	protected void preUpdate(Aircraft e) throws BusinessException {
		super.preUpdate(e);
		this.verificateDates(e.getValidFrom(), e.getValidTo());
	}

	/**
	 * @param from
	 * @param to
	 * @throws BusinessException
	 */
	public void verificateDates(Date from, Date to) throws BusinessException {
		if (from != null && to != null && from.after(to)) {
			throw new BusinessException(BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
					BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO.getErrMsg());
		}
	}

}
