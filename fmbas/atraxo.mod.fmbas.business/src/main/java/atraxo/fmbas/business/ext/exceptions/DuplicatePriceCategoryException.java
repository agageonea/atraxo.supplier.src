package atraxo.fmbas.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;

public class DuplicatePriceCategoryException extends BusinessException {

	private static final long serialVersionUID = 1883919533638405683L;

	public DuplicatePriceCategoryException(String entity, String field) {
		super(BusinessErrorCode.DUPLICATE_ENTITY, String.format(BusinessErrorCode.DUPLICATE_ENTITY.getErrMsg(), entity, field));
	}
}
