/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.externalInterfaces;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceHistory;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ExternalInterfaceHistory} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ExternalInterfaceHistory_Service
		extends
			AbstractEntityService<ExternalInterfaceHistory> {

	/**
	 * Public constructor for ExternalInterfaceHistory_Service
	 */
	public ExternalInterfaceHistory_Service() {
		super();
	}

	/**
	 * Public constructor for ExternalInterfaceHistory_Service
	 * 
	 * @param em
	 */
	public ExternalInterfaceHistory_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ExternalInterfaceHistory> getEntityClass() {
		return ExternalInterfaceHistory.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ExternalInterfaceHistory
	 */
	public ExternalInterfaceHistory findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							ExternalInterfaceHistory.NQ_FIND_BY_BUSINESS,
							ExternalInterfaceHistory.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ExternalInterfaceHistory", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ExternalInterfaceHistory", "id"), nure);
		}
	}

	/**
	 * Find by index key
	 *
	 * @return List<ExternalInterfaceHistory>
	 */
	public List<ExternalInterfaceHistory> findByIdxMessage_id(String msgId) {
		return this
				.getEntityManager()
				.createNamedQuery(
						ExternalInterfaceHistory.NQ_IDX_FIND_BY_MESSAGE_ID,
						ExternalInterfaceHistory.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("msgId", msgId).getResultList();
	}

	/**
	 * Find by reference: externalInterface
	 *
	 * @param externalInterface
	 * @return List<ExternalInterfaceHistory>
	 */
	public List<ExternalInterfaceHistory> findByExternalInterface(
			ExternalInterface externalInterface) {
		return this.findByExternalInterfaceId(externalInterface.getId());
	}
	/**
	 * Find by ID of reference: externalInterface.id
	 *
	 * @param externalInterfaceId
	 * @return List<ExternalInterfaceHistory>
	 */
	public List<ExternalInterfaceHistory> findByExternalInterfaceId(
			Integer externalInterfaceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ExternalInterfaceHistory e where e.clientId = :clientId and e.externalInterface.id = :externalInterfaceId",
						ExternalInterfaceHistory.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("externalInterfaceId", externalInterfaceId)
				.getResultList();
	}
}
