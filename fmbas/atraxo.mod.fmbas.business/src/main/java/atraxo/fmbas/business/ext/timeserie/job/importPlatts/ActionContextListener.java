package atraxo.fmbas.business.ext.timeserie.job.importPlatts;

import java.util.Set;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;

import atraxo.fmbas.business.ext.timeserie.job.importPlatts.PlattsFileItemWriter.Params;
import seava.j4e.scheduler.batch.listener.DefaultActionListener;

/**
 * Platts job listener that sets the job execution exit status and job after process message.
 * 
 * @author zspeter
 */
public class ActionContextListener extends DefaultActionListener {

	@Override
	@SuppressWarnings("unchecked")
	public void afterJob(JobExecution jobExecution) {
		super.afterJob(jobExecution);
		if (!jobExecution.getAllFailureExceptions().isEmpty()) {
			return;
		}
		ExitStatus status = jobExecution.getExitStatus();
		StringBuilder sb = new StringBuilder();
		if (jobExecution.getExecutionContext().containsKey(Params.IMPORT_LIST.toString())) {
			Set<String> imported = (Set<String>) jobExecution.getExecutionContext().get(Params.IMPORT_LIST.toString());
			sb.append("Number of imported TimeSeries: ").append(imported.size()).append("\n");
			for (String ts : imported) {
				sb.append("\t - ").append(ts).append("\n");
			}
		}
		if (jobExecution.getExecutionContext().containsKey(Params.UPDATE_LIST.toString())) {
			Set<String> updated = (Set<String>) jobExecution.getExecutionContext().get(Params.UPDATE_LIST.toString());
			sb.append("Number of updated TimeSeries: ").append(updated.size()).append("\n");
			for (String ts : updated) {
				sb.append("\t - ").append(ts).append("\n");
			}
		}
		if (!jobExecution.getExecutionContext().containsKey(Params.IMPORT_LIST.toString())
				&& !jobExecution.getExecutionContext().containsKey(Params.UPDATE_LIST.toString())) {
			sb.append("There are no TimeSeries with \"Platts\" provider or there are no new values to insert.");
		}
		if (jobExecution.getExecutionContext().containsKey("filesNameInfo")) {
			sb.append(jobExecution.getExecutionContext().get("filesNameInfo"));
		}
		status = new ExitStatus(status.getExitCode(), sb.toString());
		jobExecution.setExitStatus(status);
	}
}
