/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.validationMessage.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.validationMessage.IValidationMessageService;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.fmbas_type.ValidationMessageSeverity;
import atraxo.fmbas.domain.impl.validationMessage.ValidationMessage;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link ValidationMessage} domain entity.
 */
public class ValidationMessage_Service extends atraxo.fmbas.business.impl.validationMessage.ValidationMessage_Service
		implements IValidationMessageService {

	private static final String CONTRACT_CLASS_SIMPLE_NAME = "Contract";

	@Override
	public List<String> getInvalidObjectIds(String objectTye, List<? extends AbstractEntity> entities) throws BusinessException {
		List<String> refIds = entities.stream().map(AbstractEntity::getRefid).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(refIds)) {
			return Collections.emptyList();
		}

		IValidationMessageService service = (IValidationMessageService) this.findEntityService(ValidationMessage.class);
		String qlString = "SELECT e FROM " + ValidationMessage.class.getSimpleName()
				+ " e WHERE e.objectType = :objectType and e.objectId IN  :objectIds";
		List<ValidationMessage> validationMessages = service.getEntityManager().createQuery(qlString, ValidationMessage.class)
				.setParameter("objectType", CONTRACT_CLASS_SIMPLE_NAME).setParameter("objectIds", refIds).getResultList();
		Set<String> invalidIds = new HashSet<>();
		for (ValidationMessage message : validationMessages) {
			invalidIds.add(message.getObjectId());
		}
		return new ArrayList<>(invalidIds);
	}

	@Override
	public void deleteValidationMessage(Integer id, Class<?> c) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("objectId", id);
		params.put("objectType", c.getSimpleName());
		List<ValidationMessage> validationMessages = this.findEntitiesByAttributes(params);
		List<Object> ids = new ArrayList<>();
		for (ValidationMessage ch : validationMessages) {
			ids.add(ch.getId());
		}
		this.deleteByIds(ids);

	}

	@Override
	public void deleteValidationMessages(List ids, Class<?> c) throws BusinessException {
		for (Object id : ids) {
			this.deleteValidationMessage((Integer) id, c);
		}

	}

	@Override
	public void deleteByEntities(List<? extends AbstractEntity> entities, Class<?> c) throws BusinessException {
		for (AbstractEntity abstractEntity : entities) {
			this.deleteValidationMessage(abstractEntity.getId(), c);
		}

	}

	@Override
	public ValidationMessage createValidationMessage(AbstractEntity entity, String message, ValidationMessageSeverity severity)
			throws BusinessException {
		ValidationMessage validationMessage = this.create();
		validationMessage.setObjectId(entity.getRefid());
		validationMessage.setObjectType(entity.getClass().getSimpleName());
		validationMessage.setMessage(message);
		validationMessage.setSeverity(severity);
		return validationMessage;

	}

}
