package atraxo.fmbas.business.ext.exceptions.mailMerge;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author apetho
 */
public class MailMergeValidationFaultExeption extends BusinessException {

	private static final long serialVersionUID = -1131795040962972565L;

	/**
	 * @param exception
	 */
	public MailMergeValidationFaultExeption(Throwable exception) {
		super(BusinessErrorCode.MAIL_MERGE_VALIDATION_FAULT, BusinessErrorCode.MAIL_MERGE_VALIDATION_FAULT.getErrMsg(), exception);
	}

}
