/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.job.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import atraxo.fmbas.business.api.job.IActionParameterService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.job.ActionParameter;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link ActionParameter} domain entity.
 */
public class ActionParameter_Service extends atraxo.fmbas.business.impl.job.ActionParameter_Service implements IActionParameterService {

	private static final Logger logger = LoggerFactory.getLogger(ActionParameter_Service.class);

	private static final String PORT = "PORT";

	@Override
	protected void preUpdate(ActionParameter e) throws BusinessException {
		super.preUpdate(e);
		this.checkPortParameter(e);
		String value = e.getValue();
		if ((e.getRefValues() != null) && (!e.getRefBusinessValues().isEmpty())) {
			String[] refValues = e.getRefValues().split(",");
			for (int i = 0; i < refValues.length; i++) {
				if (refValues[i].contains(value) && e.getRefBusinessValues() != null) {
					String[] refBusinessValues = e.getRefBusinessValues().split(",");
					e.setBusinessValue(refBusinessValues[i]);
				}
			}
		}
		if (value == null || value.isEmpty()) {
			e.setBusinessValue("");
		}
	}

	@Override
	protected void preUpdate(List<ActionParameter> list) throws BusinessException {
		try {
			this.checkPeriod(list);
		} catch (ParseException ex) {
			logger.warn("Invalid parameter value, type date", ex);
		}
	}

	private void checkPortParameter(ActionParameter e) throws BusinessException {
		if (e.getName().equals(PORT) && !e.getValue().isEmpty()) {
			try {
				int portNumber = Integer.parseInt(e.getValue());
				if ((portNumber != 21 && portNumber != 22 && portNumber < 1024) || (portNumber > 65535)) {
					throw new BusinessException(BusinessErrorCode.INVALID_PORT_NUMBER, BusinessErrorCode.INVALID_PORT_NUMBER.getErrMsg());
				}
			} catch (NumberFormatException error) {
				throw new BusinessException(BusinessErrorCode.INVALID_PORT_NUMBER, BusinessErrorCode.INVALID_PORT_NUMBER.getErrMsg());
			}
		}
	}

	private void checkPeriod(List<ActionParameter> actionParameters) throws ParseException, BusinessException {
		if (!CollectionUtils.isEmpty(actionParameters) && actionParameters.get(0).getJobAction() != null
				&& "Price Update Notification".equalsIgnoreCase(actionParameters.get(0).getJobAction().getBatchJob().getName())) {
			Collection<ActionParameter> parameters = actionParameters.get(0).getJobAction().getActionParameters();
			Date validFrom = null;
			Date validTo = null;
			SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd yyyy hh:mm:ss");
			for (ActionParameter ap : parameters) {
				if ("PRICES EFFECTIVE FROM".equalsIgnoreCase(ap.getName()) && !StringUtils.isEmpty(ap.getValue())) {
					validFrom = sdf.parse(ap.getValue());
				}
				if ("PRICES VALID TO".equalsIgnoreCase(ap.getName()) && !StringUtils.isEmpty(ap.getValue())) {
					validTo = sdf.parse(ap.getValue());
				}
			}
			for (ActionParameter ap : actionParameters) {
				if ("PRICES EFFECTIVE FROM".equalsIgnoreCase(ap.getName()) && !StringUtils.isEmpty(ap.getValue())) {
					validFrom = sdf.parse(ap.getValue());
				}
				if ("PRICES VALID TO".equalsIgnoreCase(ap.getName()) && !StringUtils.isEmpty(ap.getValue())) {
					validTo = sdf.parse(ap.getValue());
				}
			}
			if ((validFrom != null && validTo == null) || (validFrom == null && validTo != null)) {
				throw new BusinessException(BusinessErrorCode.JOBS_DATE_FIELDS_ARE_NOT_FILLED,
						BusinessErrorCode.JOBS_DATE_FIELDS_ARE_NOT_FILLED.getErrMsg());
			}
			if (validFrom != null && validTo != null && validFrom.compareTo(validTo) > 0) {
				throw new BusinessException(BusinessErrorCode.JOBS_VALID_FROM_GREATER_THAN_VALID_TO,
						BusinessErrorCode.JOBS_VALID_FROM_GREATER_THAN_VALID_TO.getErrMsg());
			}
		}
	}
}
