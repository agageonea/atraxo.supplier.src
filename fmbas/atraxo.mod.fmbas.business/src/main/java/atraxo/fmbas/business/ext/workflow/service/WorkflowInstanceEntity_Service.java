/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.workflow.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link WorkflowInstanceEntity} domain entity.
 */
public class WorkflowInstanceEntity_Service extends atraxo.fmbas.business.impl.workflow.WorkflowInstanceEntity_Service
		implements IWorkflowInstanceEntityService {

	@Override
	public List<WorkflowInstanceEntity> getEntitiesByWorkflowAndObjectType(String workflowName, String className) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("name", workflowName);
		params.put("objectType", className);
		List<WorkflowInstanceEntity> entities = this.findEntitiesByAttributes(params);
		if (!CollectionUtils.isEmpty(entities)) {
			return entities;
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public List<WorkflowInstanceEntity> getEntityesByObjectIdObjectType(Integer objectId, String objectType) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("objectId", objectId);
		params.put("objectType", objectType);
		List<WorkflowInstanceEntity> entities = this.findEntitiesByAttributes(params);
		if (!CollectionUtils.isEmpty(entities)) {
			return entities;
		} else {
			return new ArrayList<>();
		}
	}
}