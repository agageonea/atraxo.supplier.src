package atraxo.fmbas.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;

public class DuplicateEntityException extends BusinessException {

	/**
	 *
	 */
	private static final long serialVersionUID = 6593504668265301618L;

	/**
	 * @param clazz
	 *            - entity class.
	 * @param fields
	 *            - A string with a list of "-" sperated field names that generates the exception.
	 */
	public DuplicateEntityException(Class<? extends AbstractEntity> clazz, String fields) {
		super(BusinessErrorCode.DUPLICATE_ENTITY, String.format(BusinessErrorCode.DUPLICATE_ENTITY.getErrMsg(), clazz.getSimpleName(), fields));
	}

}
