/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.externalInterfaces.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.fmbas_type.LastRequestStatus;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link ExternalInterface} domain entity.
 */
public class ExternalInterface_Service extends atraxo.fmbas.business.impl.externalInterfaces.ExternalInterface_Service
		implements IExternalInterfaceService {

	@Autowired
	private ExternalInterfaceHistoryFacade interfaceHistoryFacade;

	@Override
	protected void preInsert(ExternalInterface e) throws BusinessException {
		if (e.getLastRequestStatus() == null) {
			e.setLastRequestStatus(LastRequestStatus._UNKNOWN_);
		}
	}

	@Override
	@Transactional
	public void setEnableDissableStatus(Integer externalInterfaceId, boolean status) throws BusinessException {
		ExternalInterface ei = this.findById(externalInterfaceId);
		ei.setEnabled(status);
		this.update(ei);
	}

}
