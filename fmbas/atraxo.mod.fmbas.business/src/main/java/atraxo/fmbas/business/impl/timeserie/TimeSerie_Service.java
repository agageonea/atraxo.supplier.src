/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.timeserie;

import atraxo.fmbas.domain.impl.commodities.Commoditie;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.timeserie.CompositeTimeSeriesSource;
import atraxo.fmbas.domain.impl.timeserie.RawTimeSerieItem;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieAverage;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link TimeSerie} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class TimeSerie_Service extends AbstractEntityService<TimeSerie> {

	/**
	 * Public constructor for TimeSerie_Service
	 */
	public TimeSerie_Service() {
		super();
	}

	/**
	 * Public constructor for TimeSerie_Service
	 * 
	 * @param em
	 */
	public TimeSerie_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<TimeSerie> getEntityClass() {
		return TimeSerie.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return TimeSerie
	 */
	public TimeSerie findByName(String serieName) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(TimeSerie.NQ_FIND_BY_NAME,
							TimeSerie.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("serieName", serieName).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TimeSerie", "serieName"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TimeSerie", "serieName"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return TimeSerie
	 */
	public TimeSerie findByCode(String externalSerieName) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(TimeSerie.NQ_FIND_BY_CODE,
							TimeSerie.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("externalSerieName", externalSerieName)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TimeSerie", "externalSerieName"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TimeSerie", "externalSerieName"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return TimeSerie
	 */
	public TimeSerie findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(TimeSerie.NQ_FIND_BY_BUSINESS,
							TimeSerie.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TimeSerie", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TimeSerie", "id"), nure);
		}
	}

	/**
	 * Find by reference: currency1Id
	 *
	 * @param currency1Id
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByCurrency1Id(Currencies currency1Id) {
		return this.findByCurrency1IdId(currency1Id.getId());
	}
	/**
	 * Find by ID of reference: currency1Id.id
	 *
	 * @param currency1IdId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByCurrency1IdId(Integer currency1IdId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TimeSerie e where e.clientId = :clientId and e.currency1Id.id = :currency1IdId",
						TimeSerie.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currency1IdId", currency1IdId).getResultList();
	}
	/**
	 * Find by reference: currency2Id
	 *
	 * @param currency2Id
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByCurrency2Id(Currencies currency2Id) {
		return this.findByCurrency2IdId(currency2Id.getId());
	}
	/**
	 * Find by ID of reference: currency2Id.id
	 *
	 * @param currency2IdId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByCurrency2IdId(Integer currency2IdId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TimeSerie e where e.clientId = :clientId and e.currency2Id.id = :currency2IdId",
						TimeSerie.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currency2IdId", currency2IdId).getResultList();
	}
	/**
	 * Find by reference: commoditieId
	 *
	 * @param commoditieId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByCommoditieId(Commoditie commoditieId) {
		return this.findByCommoditieIdId(commoditieId.getId());
	}
	/**
	 * Find by ID of reference: commoditieId.id
	 *
	 * @param commoditieIdId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByCommoditieIdId(Integer commoditieIdId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TimeSerie e where e.clientId = :clientId and e.commoditieId.id = :commoditieIdId",
						TimeSerie.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("commoditieIdId", commoditieIdId).getResultList();
	}
	/**
	 * Find by reference: unitId
	 *
	 * @param unitId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByUnitId(Unit unitId) {
		return this.findByUnitIdId(unitId.getId());
	}
	/**
	 * Find by ID of reference: unitId.id
	 *
	 * @param unitIdId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByUnitIdId(Integer unitIdId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TimeSerie e where e.clientId = :clientId and e.unitId.id = :unitIdId",
						TimeSerie.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitIdId", unitIdId).getResultList();
	}
	/**
	 * Find by reference: unit2Id
	 *
	 * @param unit2Id
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByUnit2Id(Unit unit2Id) {
		return this.findByUnit2IdId(unit2Id.getId());
	}
	/**
	 * Find by ID of reference: unit2Id.id
	 *
	 * @param unit2IdId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByUnit2IdId(Integer unit2IdId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TimeSerie e where e.clientId = :clientId and e.unit2Id.id = :unit2IdId",
						TimeSerie.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unit2IdId", unit2IdId).getResultList();
	}
	/**
	 * Find by reference: financialSource
	 *
	 * @param financialSource
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByFinancialSource(
			FinancialSources financialSource) {
		return this.findByFinancialSourceId(financialSource.getId());
	}
	/**
	 * Find by ID of reference: financialSource.id
	 *
	 * @param financialSourceId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByFinancialSourceId(Integer financialSourceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from TimeSerie e where e.clientId = :clientId and e.financialSource.id = :financialSourceId",
						TimeSerie.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("financialSourceId", financialSourceId)
				.getResultList();
	}
	/**
	 * Find by reference: rawTimeserieItems
	 *
	 * @param rawTimeserieItems
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByRawTimeserieItems(
			RawTimeSerieItem rawTimeserieItems) {
		return this.findByRawTimeserieItemsId(rawTimeserieItems.getId());
	}
	/**
	 * Find by ID of reference: rawTimeserieItems.id
	 *
	 * @param rawTimeserieItemsId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByRawTimeserieItemsId(Integer rawTimeserieItemsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from TimeSerie e, IN (e.rawTimeserieItems) c where e.clientId = :clientId and c.id = :rawTimeserieItemsId",
						TimeSerie.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("rawTimeserieItemsId", rawTimeserieItemsId)
				.getResultList();
	}
	/**
	 * Find by reference: timeserieItems
	 *
	 * @param timeserieItems
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByTimeserieItems(TimeSerieItem timeserieItems) {
		return this.findByTimeserieItemsId(timeserieItems.getId());
	}
	/**
	 * Find by ID of reference: timeserieItems.id
	 *
	 * @param timeserieItemsId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByTimeserieItemsId(Integer timeserieItemsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from TimeSerie e, IN (e.timeserieItems) c where e.clientId = :clientId and c.id = :timeserieItemsId",
						TimeSerie.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("timeserieItemsId", timeserieItemsId)
				.getResultList();
	}
	/**
	 * Find by reference: timeserieAvg
	 *
	 * @param timeserieAvg
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByTimeserieAvg(TimeSerieAverage timeserieAvg) {
		return this.findByTimeserieAvgId(timeserieAvg.getId());
	}
	/**
	 * Find by ID of reference: timeserieAvg.id
	 *
	 * @param timeserieAvgId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByTimeserieAvgId(Integer timeserieAvgId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from TimeSerie e, IN (e.timeserieAvg) c where e.clientId = :clientId and c.id = :timeserieAvgId",
						TimeSerie.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("timeserieAvgId", timeserieAvgId).getResultList();
	}
	/**
	 * Find by reference: compositeTimeSeries
	 *
	 * @param compositeTimeSeries
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByCompositeTimeSeries(
			CompositeTimeSeriesSource compositeTimeSeries) {
		return this.findByCompositeTimeSeriesId(compositeTimeSeries.getId());
	}
	/**
	 * Find by ID of reference: compositeTimeSeries.id
	 *
	 * @param compositeTimeSeriesId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findByCompositeTimeSeriesId(
			Integer compositeTimeSeriesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from TimeSerie e, IN (e.compositeTimeSeries) c where e.clientId = :clientId and c.id = :compositeTimeSeriesId",
						TimeSerie.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("compositeTimeSeriesId", compositeTimeSeriesId)
				.getResultList();
	}
	/**
	 * Find by reference: sourceTimeSeries
	 *
	 * @param sourceTimeSeries
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findBySourceTimeSeries(
			CompositeTimeSeriesSource sourceTimeSeries) {
		return this.findBySourceTimeSeriesId(sourceTimeSeries.getId());
	}
	/**
	 * Find by ID of reference: sourceTimeSeries.id
	 *
	 * @param sourceTimeSeriesId
	 * @return List<TimeSerie>
	 */
	public List<TimeSerie> findBySourceTimeSeriesId(Integer sourceTimeSeriesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from TimeSerie e, IN (e.sourceTimeSeries) c where e.clientId = :clientId and c.id = :sourceTimeSeriesId",
						TimeSerie.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("sourceTimeSeriesId", sourceTimeSeriesId)
				.getResultList();
	}
}
