/**
 *
 */
package atraxo.fmbas.business.ext.bpm.delegate.timeseries.approval;

import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public class ApproveTimeSeriesDelegate extends AbstractUserActionTimeSeriesDelegate {

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.energyprice.approval.AbstractUserActionTimeSeriesDelegate#executeAction()
	 */
	@Override
	protected void executeAction(TimeSerie timeSerie) throws BusinessException {
		this.timeSerieService.approve(timeSerie, "Approved", this.getNote());

	}

}
