/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.profile;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.profile.BankAccount;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link BankAccount} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class BankAccount_Service extends AbstractEntityService<BankAccount> {

	/**
	 * Public constructor for BankAccount_Service
	 */
	public BankAccount_Service() {
		super();
	}

	/**
	 * Public constructor for BankAccount_Service
	 * 
	 * @param em
	 */
	public BankAccount_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<BankAccount> getEntityClass() {
		return BankAccount.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return BankAccount
	 */
	public BankAccount findByBankAccount(String accountNumber,
			Currencies currency, Customer subsidiary) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(BankAccount.NQ_FIND_BY_BANKACCOUNT,
							BankAccount.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("accountNumber", accountNumber)
					.setParameter("currency", currency)
					.setParameter("subsidiary", subsidiary).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"BankAccount",
							"accountNumber, currency, subsidiary"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"BankAccount",
							"accountNumber, currency, subsidiary"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return BankAccount
	 */
	public BankAccount findByBankAccount(String accountNumber, Long currencyId,
			Long subsidiaryId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							BankAccount.NQ_FIND_BY_BANKACCOUNT_PRIMITIVE,
							BankAccount.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("accountNumber", accountNumber)
					.setParameter("currencyId", currencyId)
					.setParameter("subsidiaryId", subsidiaryId)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"BankAccount",
							"accountNumber, currencyId, subsidiaryId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"BankAccount",
							"accountNumber, currencyId, subsidiaryId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return BankAccount
	 */
	public BankAccount findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(BankAccount.NQ_FIND_BY_BUSINESS,
							BankAccount.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"BankAccount", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"BankAccount", "id"), nure);
		}
	}

	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<BankAccount>
	 */
	public List<BankAccount> findByCurrency(Currencies currency) {
		return this.findByCurrencyId(currency.getId());
	}
	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<BankAccount>
	 */
	public List<BankAccount> findByCurrencyId(Integer currencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from BankAccount e where e.clientId = :clientId and e.currency.id = :currencyId",
						BankAccount.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currencyId", currencyId).getResultList();
	}
	/**
	 * Find by reference: subsidiary
	 *
	 * @param subsidiary
	 * @return List<BankAccount>
	 */
	public List<BankAccount> findBySubsidiary(Customer subsidiary) {
		return this.findBySubsidiaryId(subsidiary.getId());
	}
	/**
	 * Find by ID of reference: subsidiary.id
	 *
	 * @param subsidiaryId
	 * @return List<BankAccount>
	 */
	public List<BankAccount> findBySubsidiaryId(Integer subsidiaryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from BankAccount e where e.clientId = :clientId and e.subsidiary.id = :subsidiaryId",
						BankAccount.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("subsidiaryId", subsidiaryId).getResultList();
	}
	/**
	 * Find by reference: customers
	 *
	 * @param customers
	 * @return List<BankAccount>
	 */
	public List<BankAccount> findByCustomers(Customer customers) {
		return this.findByCustomersId(customers.getId());
	}
	/**
	 * Find by ID of reference: customers.id
	 *
	 * @param customersId
	 * @return List<BankAccount>
	 */
	public List<BankAccount> findByCustomersId(Integer customersId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from BankAccount e, IN (e.customers) c where e.clientId = :clientId and c.id = :customersId",
						BankAccount.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customersId", customersId).getResultList();
	}
}
