package atraxo.fmbas.business.ext.externalInterfaces.timeout;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceMessageHistoryService;
import atraxo.fmbas.business.api.ws.IRestClientTimeoutService;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * @author zspeter
 */
public class ExternalInterfaceMessageHistoryWriter implements ItemWriter<ExternalInterfaceMessageHistory>, ApplicationContextAware {

	@Autowired
	private IExternalInterfaceMessageHistoryService srv;

	private ApplicationContext applicationContext;

	@Override
	public void write(List<? extends ExternalInterfaceMessageHistory> list) throws Exception {
		List<ExternalInterfaceMessageHistory> updList = new ArrayList<>();
		for (ExternalInterfaceMessageHistory externalInterfaceMessageHistory : list) {
			if (externalInterfaceMessageHistory.getStatus().equals(ExternalInterfaceMessageHistoryStatus._FAILED_TIMEOUT_)) {
				this.updateEntity(externalInterfaceMessageHistory);
				updList.add(externalInterfaceMessageHistory);
			}
		}
		this.srv.update(updList);
	}

	@SuppressWarnings("unchecked")
	private <E> void updateEntity(ExternalInterfaceMessageHistory e) throws ClassNotFoundException, BusinessException {
		IEntityService<E> entitySrv = (IEntityService<E>) this.applicationContext.getBean(e.getObjectType());
		if (entitySrv instanceof IRestClientTimeoutService) {
			IRestClientTimeoutService<E> timeoutSrv = (IRestClientTimeoutService<E>) entitySrv;
			E entity = entitySrv.findById(e.getObjectId());
			if (timeoutSrv.afterTimeout(entity)) {
				entitySrv.update(entity);
			}
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

}
