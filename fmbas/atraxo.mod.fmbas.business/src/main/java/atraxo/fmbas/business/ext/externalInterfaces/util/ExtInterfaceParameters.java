package atraxo.fmbas.business.ext.externalInterfaces.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.util.StringUtils;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceParameter;

/**
 * Utility class to manage external interface parameters;
 *
 * @author zspeter
 */
public class ExtInterfaceParameters {
	private Map<String, String> paramMap;

	/**
	 * @param list
	 */
	public ExtInterfaceParameters(Collection<ExternalInterfaceParameter> list) {
		this.paramMap = new HashMap<>();
		for (ExternalInterfaceParameter param : list) {
			this.paramMap.put(param.getName(), StringUtils.isEmpty(param.getCurrentValue()) ? param.getDefaultValue() : param.getCurrentValue());
		}
	}

	/**
	 * @param paramName
	 * @return
	 */
	public String getNotEmptyParamValue(String paramName) {
		String value = "";
		if (this.paramMap.containsKey(paramName)) {
			String paramValue = this.paramMap.get(paramName);
			// make sure to send empty string instead of null
			if (paramValue != null) {
				value = paramValue;
			}
		}
		return value;
	}

	/**
	 * @param paramName
	 * @return
	 */
	public boolean hasParam(String paramName) {
		return this.paramMap.containsKey(paramName);
	}

	/**
	 * @param paramName
	 * @return
	 */
	public String getParamValue(String paramName) {
		if (this.paramMap.containsKey(paramName)) {
			return this.paramMap.get(paramName);
		}
		throw new IllegalArgumentException("Param with name " + paramName + " not found!");
	}

	/**
	 * @return
	 */
	public Map<String, String> getParamMap() {
		return this.paramMap;
	}

	/**
	 * @see Map#put(Object, Object)
	 * @param key
	 * @param value
	 */
	public void put(String key, String value) {
		this.paramMap.put(key, value);
	}
}
