/**
 *
 */
package atraxo.fmbas.business.ext.bpm.delegate.timeseries.approval;

import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public class RejectTimeSeriesDelegate extends AbstractUserActionTimeSeriesDelegate {

	@Override
	protected void executeAction(TimeSerie timeSerie) throws BusinessException {
		this.timeSerieService.reject(timeSerie, "Rejected", this.getNote());

	}

}
