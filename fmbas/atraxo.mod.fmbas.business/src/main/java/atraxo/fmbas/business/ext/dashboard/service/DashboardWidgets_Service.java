/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.dashboard.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import seava.j4e.api.exceptions.BusinessException;
import atraxo.fmbas.business.api.dashboard.IAssignedWidgetParametersService;
import atraxo.fmbas.business.api.dashboard.IDashboardWidgetsService;
import atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;

/**
 * Business extensions specific for {@link DashboardWidgets} domain entity.
 */
public class DashboardWidgets_Service extends atraxo.fmbas.business.impl.dashboard.DashboardWidgets_Service implements IDashboardWidgetsService {

	@Autowired
	IAssignedWidgetParametersService awpService;

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		// TODO Auto-generated method stub
		super.preDeleteByIds(ids, context);
		List<DashboardWidgets> dwList = this.findByIds(ids);
		List<Object> deleteList = new ArrayList<Object>();
		for (DashboardWidgets dw : dwList) {
			for (AssignedWidgetParameters awp : dw.getAssignedWidgetParameters()) {
				deleteList.add(awp.getId());
			}
		}
		if (!CollectionUtils.isEmpty(deleteList)) {
			this.awpService.deleteByIds(deleteList);
		}
	}

}
