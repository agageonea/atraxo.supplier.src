/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.notification;

import atraxo.fmbas.domain.impl.notification.Notification;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Notification} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Notification_Service extends AbstractEntityService<Notification> {

	/**
	 * Public constructor for Notification_Service
	 */
	public Notification_Service() {
		super();
	}

	/**
	 * Public constructor for Notification_Service
	 * 
	 * @param em
	 */
	public Notification_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Notification> getEntityClass() {
		return Notification.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Notification
	 */
	public Notification findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Notification.NQ_FIND_BY_BUSINESS,
							Notification.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Notification", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Notification", "id"), nure);
		}
	}

	/**
	 * Find by reference: users
	 *
	 * @param users
	 * @return List<Notification>
	 */
	public List<Notification> findByUsers(UserSupp users) {
		return this.findByUsersId(users.getId());
	}
	/**
	 * Find by ID of reference: users.id
	 *
	 * @param usersId
	 * @return List<Notification>
	 */
	public List<Notification> findByUsersId(String usersId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Notification e, IN (e.users) c where e.clientId = :clientId and c.id = :usersId",
						Notification.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("usersId", usersId).getResultList();
	}
}
