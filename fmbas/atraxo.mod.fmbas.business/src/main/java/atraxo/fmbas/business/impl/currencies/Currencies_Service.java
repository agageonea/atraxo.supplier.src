/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.currencies;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Currencies} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Currencies_Service extends AbstractEntityService<Currencies> {

	/**
	 * Public constructor for Currencies_Service
	 */
	public Currencies_Service() {
		super();
	}

	/**
	 * Public constructor for Currencies_Service
	 * 
	 * @param em
	 */
	public Currencies_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Currencies> getEntityClass() {
		return Currencies.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Currencies
	 */
	public Currencies findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Currencies.NQ_FIND_BY_CODE,
							Currencies.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Currencies", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Currencies", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Currencies
	 */
	public Currencies findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Currencies.NQ_FIND_BY_BUSINESS,
							Currencies.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Currencies", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Currencies", "id"), nure);
		}
	}

}
