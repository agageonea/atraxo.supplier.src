/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.job;

import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.ActionParameter;
import atraxo.fmbas.domain.impl.job.BatchJob;
import atraxo.fmbas.domain.impl.job.JobChain;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Action} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Action_Service extends AbstractEntityService<Action> {

	/**
	 * Public constructor for Action_Service
	 */
	public Action_Service() {
		super();
	}

	/**
	 * Public constructor for Action_Service
	 * 
	 * @param em
	 */
	public Action_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Action> getEntityClass() {
		return Action.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Action
	 */
	public Action findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Action.NQ_FIND_BY_BUSINESS, Action.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Action", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Action", "id"), nure);
		}
	}

	/**
	 * Find by reference: jobChain
	 *
	 * @param jobChain
	 * @return List<Action>
	 */
	public List<Action> findByJobChain(JobChain jobChain) {
		return this.findByJobChainId(jobChain.getId());
	}
	/**
	 * Find by ID of reference: jobChain.id
	 *
	 * @param jobChainId
	 * @return List<Action>
	 */
	public List<Action> findByJobChainId(Integer jobChainId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Action e where e.clientId = :clientId and e.jobChain.id = :jobChainId",
						Action.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("jobChainId", jobChainId).getResultList();
	}
	/**
	 * Find by reference: nextAction
	 *
	 * @param nextAction
	 * @return List<Action>
	 */
	public List<Action> findByNextAction(Action nextAction) {
		return this.findByNextActionId(nextAction.getId());
	}
	/**
	 * Find by ID of reference: nextAction.id
	 *
	 * @param nextActionId
	 * @return List<Action>
	 */
	public List<Action> findByNextActionId(Integer nextActionId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Action e where e.clientId = :clientId and e.nextAction.id = :nextActionId",
						Action.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("nextActionId", nextActionId).getResultList();
	}
	/**
	 * Find by reference: batchJob
	 *
	 * @param batchJob
	 * @return List<Action>
	 */
	public List<Action> findByBatchJob(BatchJob batchJob) {
		return this.findByBatchJobId(batchJob.getId());
	}
	/**
	 * Find by ID of reference: batchJob.id
	 *
	 * @param batchJobId
	 * @return List<Action>
	 */
	public List<Action> findByBatchJobId(Integer batchJobId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Action e where e.clientId = :clientId and e.batchJob.id = :batchJobId",
						Action.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("batchJobId", batchJobId).getResultList();
	}
	/**
	 * Find by reference: actionParameters
	 *
	 * @param actionParameters
	 * @return List<Action>
	 */
	public List<Action> findByActionParameters(ActionParameter actionParameters) {
		return this.findByActionParametersId(actionParameters.getId());
	}
	/**
	 * Find by ID of reference: actionParameters.id
	 *
	 * @param actionParametersId
	 * @return List<Action>
	 */
	public List<Action> findByActionParametersId(Integer actionParametersId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Action e, IN (e.actionParameters) c where e.clientId = :clientId and c.id = :actionParametersId",
						Action.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("actionParametersId", actionParametersId)
				.getResultList();
	}
}
