/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.job;

import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.JobChain;
import atraxo.fmbas.domain.impl.job.JobChainUser;
import atraxo.fmbas.domain.impl.job.Trigger;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link JobChain} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class JobChain_Service extends AbstractEntityService<JobChain> {

	/**
	 * Public constructor for JobChain_Service
	 */
	public JobChain_Service() {
		super();
	}

	/**
	 * Public constructor for JobChain_Service
	 * 
	 * @param em
	 */
	public JobChain_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<JobChain> getEntityClass() {
		return JobChain.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return JobChain
	 */
	public JobChain findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(JobChain.NQ_FIND_BY_BUSINESS,
							JobChain.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"JobChain", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"JobChain", "id"), nure);
		}
	}

	/**
	 * Find by reference: triggers
	 *
	 * @param triggers
	 * @return List<JobChain>
	 */
	public List<JobChain> findByTriggers(Trigger triggers) {
		return this.findByTriggersId(triggers.getId());
	}
	/**
	 * Find by ID of reference: triggers.id
	 *
	 * @param triggersId
	 * @return List<JobChain>
	 */
	public List<JobChain> findByTriggersId(Integer triggersId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from JobChain e, IN (e.triggers) c where e.clientId = :clientId and c.id = :triggersId",
						JobChain.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("triggersId", triggersId).getResultList();
	}
	/**
	 * Find by reference: actions
	 *
	 * @param actions
	 * @return List<JobChain>
	 */
	public List<JobChain> findByActions(Action actions) {
		return this.findByActionsId(actions.getId());
	}
	/**
	 * Find by ID of reference: actions.id
	 *
	 * @param actionsId
	 * @return List<JobChain>
	 */
	public List<JobChain> findByActionsId(Integer actionsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from JobChain e, IN (e.actions) c where e.clientId = :clientId and c.id = :actionsId",
						JobChain.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("actionsId", actionsId).getResultList();
	}
	/**
	 * Find by reference: users
	 *
	 * @param users
	 * @return List<JobChain>
	 */
	public List<JobChain> findByUsers(JobChainUser users) {
		return this.findByUsersId(users.getId());
	}
	/**
	 * Find by ID of reference: users.id
	 *
	 * @param usersId
	 * @return List<JobChain>
	 */
	public List<JobChain> findByUsersId(Integer usersId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from JobChain e, IN (e.users) c where e.clientId = :clientId and c.id = :usersId",
						JobChain.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("usersId", usersId).getResultList();
	}
}
