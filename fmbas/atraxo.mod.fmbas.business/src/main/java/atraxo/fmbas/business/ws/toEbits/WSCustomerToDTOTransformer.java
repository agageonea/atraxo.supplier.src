/**
 *
 */
package atraxo.fmbas.business.ws.toEbits;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.ws.WSModelToDTOTransformer;
import atraxo.fmbas.business.ws.ExternalInterfaceWSHelper;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.ws.customer.CUSTOMERDETAILS;
import atraxo.fmbas.domain.ws.customer.CUSTOMERDETAILS.CUSTOMERS;
import atraxo.fmbas.domain.ws.customer.CUSTOMERDETAILS.CUSTOMERS.CUSTOMER;
import atraxo.fmbas.domain.ws.customer.CUSTOMERDETAILS.HEADER;
import atraxo.fmbas.domain.ws.dto.WSPayloadDataDTO;
import atraxo.fmbas.domain.ws.dto.WSTransportDataType;
import seava.j4e.commons.utils.XMLUtils;

/**
 * @author vhojda
 */
public class WSCustomerToDTOTransformer implements WSModelToDTOTransformer<Customer> {

	private static final String DEFAULT_IATA_CODE = "999";
	final static Logger logger = LoggerFactory.getLogger(WSCustomerToDTOTransformer.class);

	/**
	 *
	 */
	public WSCustomerToDTOTransformer() {

	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.api.ws.WSModelToDTOTransformer#transformModelToDTO(java.util.Collection, java.util.Map)
	 */
	@Override
	public WSPayloadDataDTO transformModelToDTO(Collection<Customer> modelEntities, Map<String, String> paramMap) throws JAXBException {
		if (logger.isDebugEnabled()) {
			logger.debug("START transformModelToDTO()");
		}

		WSPayloadDataDTO dto = new WSPayloadDataDTO();
		CUSTOMERDETAILS customerDetails = new CUSTOMERDETAILS();
		CUSTOMERS customers = new CUSTOMERS();
		customerDetails.setHEADER(this.toHEADER(modelEntities.size(), paramMap));
		customerDetails.setCUSTOMERS(customers);
		int batchId = 1;
		for (Customer entity : modelEntities) {
			Customer customerModel = null;
			try {
				customerModel = entity;
			} catch (ClassCastException e) {
				logger.error("ERROR: could not cast " + entity.getClass() + " to " + Customer.class + " !", e);
				throw e;
			}

			if (customerModel != null) {
				customers.getCUSTOMER().add(this.toCUSTOMER(customerModel, batchId));
				batchId++;
			}
		}
		dto.setPayload(XMLUtils.toXML(customerDetails, customerDetails.getClass()));

		if (logger.isDebugEnabled()) {
			logger.debug("END transformModelToDTO()");
		}
		return dto;
	}

	/**
	 * @param customerModel
	 * @return
	 */
	private CUSTOMER toCUSTOMER(Customer customerModel, int batchId) {
		CUSTOMER customer = new CUSTOMER();
		customer.setCUSTOMERNAME(customerModel.getName());
		customer.setCUSTOMERNUMBER(customerModel.getAccountNumber());
		customer.setCUSTOMERSEGMENTCODE(customerModel.getNatureOfBusiness().toString());
		customer.setDELETIONINDICATOR("N");
		customer.setIATACODE(!StringUtils.isEmpty(customerModel.getIataCode()) ? customerModel.getIataCode() : DEFAULT_IATA_CODE);
		customer.setCOUNTRYCODE((customerModel.getOfficeCountry() != null) ? customerModel.getOfficeCountry().getCode() : "");
		customer.setPARTNERCUSTOMERNUMBER(customerModel.getAccountNumber());
		customer.setBATCHID(String.valueOf(batchId));
		return customer;
	}

	/**
	 * @param headerData
	 * @return
	 */
	private HEADER toHEADER(int entitiesSize, Map<String, String> paramMap) {
		HEADER header = new HEADER();
		header.setCOUNT(BigInteger.valueOf(entitiesSize));
		header.setENV(paramMap.get(ExternalInterfaceWSHelper.EXTERNAL_PARAM_ENVIRONMENT));
		header.setINTERFACEID("E-BITS_MASTER");
		header.setINTERFACENAME("MASTERDATA");
		header.setMESSAGEID(paramMap.get(ExternalInterfaceWSHelper.EXTERNAL_PARAM_MESSAGE_ID));
		header.setSOURCESYSTEM("supplierONE");
		header.setTARGETSYSTEM("eBits AVReFueling");
		return header;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.api.ws.WSModelToDTOTransformer#getTransportDataType ()
	 */
	@Override
	public WSTransportDataType getTransportDataType() {
		return WSTransportDataType.CUSTOMERS;
	}

}
