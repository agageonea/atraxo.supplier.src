package atraxo.fmbas.business.ext.exceptions.conversion;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.IErrorCode;

/**
 * Execption used when cannot convert a unit to an other for different reasons.
 *
 * @author zspeter
 */
public class UnitConversionException extends BusinessException {

	private static final long serialVersionUID = -7465680626268478204L;

	public UnitConversionException(IErrorCode errorCode) {
		super(errorCode, "");
	}

	public UnitConversionException(IErrorCode errorCode, Object... fields) {
		super(errorCode, String.format(errorCode.getErrMsg(), fields));
	}

}
