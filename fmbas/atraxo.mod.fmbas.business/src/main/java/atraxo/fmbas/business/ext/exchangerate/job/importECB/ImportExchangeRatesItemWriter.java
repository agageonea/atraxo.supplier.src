package atraxo.fmbas.business.ext.exchangerate.job.importECB;

import java.util.List;

import org.springframework.batch.item.ItemWriter;

import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;

/**
 * @author zspeter
 */
public class ImportExchangeRatesItemWriter implements ItemWriter<List<ExchangeRate>> {

	@Override
	public void write(List<? extends List<ExchangeRate>> items) throws Exception {
		// do nothing
	}
}
