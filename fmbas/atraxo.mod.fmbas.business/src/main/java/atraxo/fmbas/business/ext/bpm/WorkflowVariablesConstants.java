package atraxo.fmbas.business.ext.bpm;

/**
 * Class with constants used in the Activiti workflows. THE NAMES ARE THE ONES USED IN TEH ACTIVITI PROCESSES SO DO NOT CHANGE THEM WITHOUT UPDATING
 * THE ACTIVITI PROCESS DEFINITION!!!
 *
 * @author vhojda
 */
public class WorkflowVariablesConstants {

	// core variables
	public static final String VAR_APP_CLIENT_CODE = "app_client_code";
	public static final String VAR_APP_CLIENT_ID = "app_client_id";

	public static final String VAR_WKF_ID = "wkf_id";
	public static final String VAR_WKF_NAME = "wkf_name";
	public static final String VAR_WKF_ENTITY_DESCRIPTION = "wkf_entity_description";
	public static final String VAR_WKF_INSTANCE_ID = "wkf_instance_id";

	public static final String VAR_AUTOMATIC_TERMINATE_FAILED_WORKFLOWS = "auto_terminate_failed_workflows";
	public static final String VAR_CONTINUE_WKF_IF_MAIL_FAIL = "continue_wkf_if_mail_fail";

	// email used variables
	public static final String VAR_WKF_EMAIL_SEND_COUNTER = "wkf_email_send_counter";
	public static final String VAR_WKF_EMAIL_RESEND_PERIOD = "wkf_email_resend_period";
	public static final String VAR_WKF_EMAIL_RESEND_ATTEMPTS = "wkf_email_resend_attempts";
	public static final String VAR_WKF_EMAIL_DTO = "email_dto";
	public static final String VAR_WKF_EMAIL_ATTACHMENTS = "email_attachments";

	// error codes for workflows
	public static final String ERROR_DEFAULT_CODE_APPROVE = "APPROVE_STAGE_ERROR";
	public static final String ERROR_DEFAULT_MAIL = "MAIL_STAGE_ERROR";
	public static final String ERROR_DEFAULT_CODE_RELEASE = "RELEASE_STAGE_ERROR";

	// Credit memo approval workflow
	public static final String INVOICE_ID_VAR = "invoice_id";
	public static final String APPROVER_ROLE = "approver_role";
	public static final String INVOICE_RICOH_XML = "ricoh_xml";
	public static final String INVOICE_RICOH_FILE_NAME = "ricoh_file_name";

	// Approval roles
	public static final String APPROVER_ROLES_LVL1 = "approver_role_lvl1";
	public static final String APPROVER_ROLES_LVL2 = "approver_role_lvl2";
	public static final String APPROVER_ROLES_LVL3 = "approver_role_lvl3";
	public static final String APPROVER_ROLES_LVL4 = "approver_role_lvl4";

	// Bid approval workflow
	public static final String BID_ID_VAR = "bid_id";
	public static final String BID_OVER_VOLUME = "bid_over_volume";
	public static final String THRESHOLD_VOLUME = "threshold_volume";
	public static final String THRESHOLD_VOLUME_UNIT = "threshold_volume_unit";

	// Price update workflow
	public static final String PRICE_UPDATE_ID_VAR = "price_update_id";
	public static final String NEXT_APPROVAL_STAGE_REQUIRED = "next_approval_stage_required";

	// Fuel Ticket approval workflow
	public static final String FUEL_TICKET_ID_VAR = "fuel_ticket_id";
	public static final String BLOCKED_CUSTOMER_APPROVER_ROLE = "approver_role_blocked_customer";
	public static final String SECOND_APPROVAL_STAGE_REQUIRED = "second_approval_stage_required";

	// Fuel Ticket Release Workflow
	public static final String CHECK_NET_QUANTITY = "check_net_quantity";

	// Contract approval workflow
	public static final String CONTRACT_ID_VAR = "contract_id";
	public static final String CONTRACT_OVER_LOWER_VOLUME = "contract_over_lower_volume";
	public static final String CONTRACT_OVER_UPPER_VOLUME = "contract_over_upper_volume";
	public static final String THRESHOLD_VOLUME_LOWER = "threshold_lower_volume";
	public static final String THRESHOLD_VOLUME_UPPER = "threshold_upper_volume";
	public static final String CONTRACT_VOLUME_DEFINED = "contract_volume_defined";

	// Time Series a approval workflows
	public static final String TIME_SERIE_ID = "exchangeRateTimeSerie_id";
	public static final String HAS_USERS_TO_APPROVE = "has_users_to_approve";
	public static final String TIME_SERIE_TYPE = "time_series_type";

	// common workflows
	public static final String FULLNAME_REQUESTER_VAR = "fullname_of_requester";
	public static final String APPROVE_NOTE = "approve_note";

	public static final String TASK_ACCEPTANCE_VAR = "task_acceptance";
	public static final String TASK_NOTE_VAR = "task_note";
	public static final String TASK_NOTE_APPROVER_NAME_VAR = "task_note_approver_name";
	public static final String TASK_NOTE_APPROVER_USERNAME = "task_note_approver_username";

	public static final String TASK_NOTE_VAR_LVL1 = "task_note_lvl1";
	public static final String TASK_NOTE_APPROVER_NAME_VAR_LVL1 = "task_note_approver_name_lvl1";
	public static final String TASK_NOTE_VAR_LVL2 = "task_note_lvl2";
	public static final String TASK_NOTE_APPROVER_NAME_VAR_LVL2 = "task_note_approver_name_lvl2";
	public static final String TASK_NOTE_VAR_LVL3 = "task_note_lvl3";
	public static final String TASK_NOTE_APPROVER_NAME_VAR_LVL3 = "task_note_approver_name_lvl3";
	public static final String TASK_EMAILS = "task_emails";
	public static final String TASK_EMAIL_SUBJECT = "task_email_subject";

	// notification
	public static final String NOTIFICATION_TITLE = "notification_title";
	public static final String NOTIFICATION_DETAILS = "notification_details";
	public static final String NOTIFICATION_DETAILS_ENTITY_NAME = "notification_details_entity_name";
	public static final String NOTIFICATION_DETAILS_ENTITY_CODE = "notification_details_entity_code";
	public static final String NOTIFICATION_METHOD_NAME = "notification_method_name";
	public static final String NOTIFICATION_METHOD_PARAM = "notification_method_param";

	// invoice notification
	public static final String SKIP_TO_NOTIFICATIONS = "skip_to_notifications";
	public static final String SKIP_TO_ZIP = "skip_to_zip";
	public static final String ERROR_NOTIFICATION = "error_notification";
	public static final String MAP_NOTIFICATIONS = "map_notifications";
	public static final String MAP_REPORTS = "map_reports";
	public static final String MAP_INVOICES_OFFSET = "map_invoices_offset";
	public static final String ATTACHMENT_TYPE = "attachment_type";
	public static final String REPORT_ID = "report_id";
	public static final String REPORT_NAME = "report_name";
	public static final String SKIP_TO_GENERATE_REPORTS = "skip_to_generate_reports";
	public static final String ZIP_REFID = "zip_refid";
	public static final String ZIP_NAME = "zip_name";

	public static final String USER = "user";

	// ftp params
	public static final String FTP_SERVER = "wkf_ftp_server";
	public static final String FTP_SERVER_PORT = "wkf_ftp_server_port";
	public static final String FTP_REMOTE_FOLDER = "wkf_ftp_remote_folder";
	public static final String FTP_SERVER_PASSWORD = "wkf_ftp_server_password";
	public static final String FTP_SERVER_USERNAME = "wkf_ftp_server_username";

	/**
	 * Private default constructor
	 */
	private WorkflowVariablesConstants() {

	}
}
