/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.customer;

import atraxo.fmbas.business.api.customer.ICustomerNotificationService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.customer.CustomerNotification;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import atraxo.fmbas.domain.impl.templates.Template;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link CustomerNotification} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class CustomerNotification_Service
		extends
			AbstractEntityService<CustomerNotification>
		implements
			ICustomerNotificationService {

	/**
	 * Public constructor for CustomerNotification_Service
	 */
	public CustomerNotification_Service() {
		super();
	}

	/**
	 * Public constructor for CustomerNotification_Service
	 * 
	 * @param em
	 */
	public CustomerNotification_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<CustomerNotification> getEntityClass() {
		return CustomerNotification.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return CustomerNotification
	 */
	public CustomerNotification findByKey(Customer customer,
			NotificationEvent notificationEvent, Area assignedArea) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(CustomerNotification.NQ_FIND_BY_KEY,
							CustomerNotification.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("customer", customer)
					.setParameter("notificationEvent", notificationEvent)
					.setParameter("assignedArea", assignedArea)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"CustomerNotification",
							"customer, notificationEvent, assignedArea"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"CustomerNotification",
							"customer, notificationEvent, assignedArea"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return CustomerNotification
	 */
	public CustomerNotification findByKey(Long customerId,
			Long notificationEventId, Long assignedAreaId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							CustomerNotification.NQ_FIND_BY_KEY_PRIMITIVE,
							CustomerNotification.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("customerId", customerId)
					.setParameter("notificationEventId", notificationEventId)
					.setParameter("assignedAreaId", assignedAreaId)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"CustomerNotification",
							"customerId, notificationEventId, assignedAreaId"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"CustomerNotification",
							"customerId, notificationEventId, assignedAreaId"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return CustomerNotification
	 */
	public CustomerNotification findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(CustomerNotification.NQ_FIND_BY_BUSINESS,
							CustomerNotification.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"CustomerNotification", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"CustomerNotification", "id"), nure);
		}
	}

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByCustomer(Customer customer) {
		return this.findByCustomerId(customer.getId());
	}
	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByCustomerId(Integer customerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from CustomerNotification e where e.clientId = :clientId and e.customer.id = :customerId",
						CustomerNotification.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerId", customerId).getResultList();
	}
	/**
	 * Find by reference: template
	 *
	 * @param template
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByTemplate(Template template) {
		return this.findByTemplateId(template.getId());
	}
	/**
	 * Find by ID of reference: template.id
	 *
	 * @param templateId
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByTemplateId(Integer templateId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from CustomerNotification e where e.clientId = :clientId and e.template.id = :templateId",
						CustomerNotification.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("templateId", templateId).getResultList();
	}
	/**
	 * Find by reference: emailBody
	 *
	 * @param emailBody
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByEmailBody(Template emailBody) {
		return this.findByEmailBodyId(emailBody.getId());
	}
	/**
	 * Find by ID of reference: emailBody.id
	 *
	 * @param emailBodyId
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByEmailBodyId(Integer emailBodyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from CustomerNotification e where e.clientId = :clientId and e.emailBody.id = :emailBodyId",
						CustomerNotification.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("emailBodyId", emailBodyId).getResultList();
	}
	/**
	 * Find by reference: notificationEvent
	 *
	 * @param notificationEvent
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByNotificationEvent(
			NotificationEvent notificationEvent) {
		return this.findByNotificationEventId(notificationEvent.getId());
	}
	/**
	 * Find by ID of reference: notificationEvent.id
	 *
	 * @param notificationEventId
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByNotificationEventId(
			Integer notificationEventId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from CustomerNotification e where e.clientId = :clientId and e.notificationEvent.id = :notificationEventId",
						CustomerNotification.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("notificationEventId", notificationEventId)
				.getResultList();
	}
	/**
	 * Find by reference: assignedArea
	 *
	 * @param assignedArea
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByAssignedArea(Area assignedArea) {
		return this.findByAssignedAreaId(assignedArea.getId());
	}
	/**
	 * Find by ID of reference: assignedArea.id
	 *
	 * @param assignedAreaId
	 * @return List<CustomerNotification>
	 */
	public List<CustomerNotification> findByAssignedAreaId(
			Integer assignedAreaId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from CustomerNotification e where e.clientId = :clientId and e.assignedArea.id = :assignedAreaId",
						CustomerNotification.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("assignedAreaId", assignedAreaId).getResultList();
	}
}
