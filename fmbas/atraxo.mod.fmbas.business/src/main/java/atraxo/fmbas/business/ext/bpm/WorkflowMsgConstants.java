package atraxo.fmbas.business.ext.bpm;

/**
 * @author vhojda
 */
public class WorkflowMsgConstants {

	/**
	 *
	 */
	public WorkflowMsgConstants() {

	}

	public static final String MSG_STARTED_FOR_APPROVAL_OK = "Submitted for approval";
	public static final String MSG_STARTED_FOR_APPROVAL_NOT_OK = "Failure in approval workflow";

	public static final String MSG_STARTED_FOR_PERIOD_APPROVAL_OK = "Submitted for period approval";
	public static final String MSG_STARTED_FOR_PERIOD_APPROVAL_NOT_OK = "Failure in period approval workflow";

	public static final String MSG_STARTED_FOR_PRICE_APPROVAL_OK = "Submitted for price approval";
	public static final String MSG_STARTED_FOR_PRICE_APPROVAL_NOT_OK = "Failure in price approval workflow";

	public static final String MSG_STARTED_FOR_SHIP_TO_APPROVAL_OK = "Submitted for ship-to approval";
	public static final String MSG_STARTED_FOR_SHIP_TO_APPROVAL_NOT_OK = "Failure in ship-to approval workflow";

	public static final String WKF_RESULT_APPROVED = "Approved";
	public static final String WKF_RESULT_REJECTED = "Rejected";
	public static final String WKF_RESULT_APPROVE_FAILED = "Approve failed";
	public static final String APPROVAL_WORKFLOW_TERMINATED_COMMENT = "Approval workflow terminated.";

	public static final String APPROVAL_PRICE_WORKFLOW_TERMINATED_COMMENT = "Approval price workflow terminated.";
	public static final String APPROVAL_PERIOD_WORKFLOW_TERMINATED_COMMENT = "Approval period workflow terminated.";
	public static final String APPROVAL_SHIP_TO_WORKFLOW_TERMINATED_COMMENT = "Approval ship-to workflow terminated.";

	public static final String SUBMIT_FOR_APPROVAL_FAILED = "Submit for approval - Failed";
	public static final String RELEASE_FAILED = "Release - Failed";
}
