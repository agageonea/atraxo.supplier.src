/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.financialSources;

import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FinancialSources} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FinancialSources_Service
		extends
			AbstractEntityService<FinancialSources> {

	/**
	 * Public constructor for FinancialSources_Service
	 */
	public FinancialSources_Service() {
		super();
	}

	/**
	 * Public constructor for FinancialSources_Service
	 * 
	 * @param em
	 */
	public FinancialSources_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FinancialSources> getEntityClass() {
		return FinancialSources.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FinancialSources
	 */
	public FinancialSources findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FinancialSources.NQ_FIND_BY_CODE,
							FinancialSources.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FinancialSources", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FinancialSources", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FinancialSources
	 */
	public FinancialSources findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FinancialSources.NQ_FIND_BY_BUSINESS,
							FinancialSources.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FinancialSources", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FinancialSources", "id"), nure);
		}
	}

}
