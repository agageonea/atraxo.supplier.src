package atraxo.fmbas.business.ext.calculator;

import java.util.List;

import seava.j4e.api.exceptions.BusinessException;
import atraxo.fmbas.business.api.timeserie.ITimeSerieItemService;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;

public class SCCalculator extends AbstractSemiMonthlyCalculator implements Calculator {

	@Override
	public List<TimeSerieItem> getItemsForCalculation(List<TimeSerieItem> timeSerieItems, ITimeSerieItemService service) throws BusinessException {
		return timeSerieItems;
	}
}
