package atraxo.fmbas.business.ext.job.delegate;

import java.util.ArrayList;
import java.util.List;

import atraxo.fmbas.business.api.job.IActionService;
import atraxo.fmbas.business.api.job.ITriggerService;
import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.Trigger;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class JobChainCleaner_Bd extends AbstractBusinessDelegate {

	public void cleanChilds(List<Object> ids) throws BusinessException {
		IActionService actionSrv = (IActionService) this.findEntityService(Action.class);
		ITriggerService triggerSrv = (ITriggerService) this.findEntityService(Trigger.class);
		List<Action> actions = new ArrayList<>();
		List<Trigger> triggers = new ArrayList<>();
		for (Object id : ids) {
			actions.addAll(actionSrv.findByJobChainId((Integer) id));
			triggers.addAll(triggerSrv.findByJobChainId((Integer) id));
		}
		actionSrv.delete(actions);
		triggerSrv.delete(triggers);

	}
}
