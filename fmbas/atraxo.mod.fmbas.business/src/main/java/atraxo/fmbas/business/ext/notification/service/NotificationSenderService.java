package atraxo.fmbas.business.ext.notification.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.templates.ITemplateService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.mailMerge.MailMergeTemplateAlreadyExistsExeption;
import atraxo.fmbas.business.ext.mailMerge.MailMergeManager;
import atraxo.fmbas.business.ext.mailMerge.service.MailMerge_Service;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailAddressesDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDataDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.TenderInvitationDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.WebServiceDto;
import atraxo.fmbas.domain.impl.fmbas_type.NotificationType;
import atraxo.fmbas.domain.impl.notification.Notification;
import atraxo.fmbas.domain.impl.templates.Template;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.enums.DateFormatAttribute;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.model.AppNotification;
import seava.j4e.api.service.notification.IAppNotificationService;
import seava.j4e.api.session.Session;

/**
 * @author zspeter
 */
public class NotificationSenderService {

	private static final Logger LOG = LoggerFactory.getLogger(NotificationSenderService.class);
	@Autowired
	private MailMerge_Service mailSrv;
	@Autowired
	private IAppNotificationService appNotifsrv;
	@Autowired
	private ITemplateService templateSrv;
	@Autowired
	private ISystemParameterService sysParamSrv;

	private static final String EMAIL_BODY = "Web service execution e-mail body.txt";

	/**
	 * @param e
	 * @param type
	 * @param emailTemplateRefId
	 * @param users
	 *            - A list of users who will receive email notification.
	 * @throws BusinessException
	 */
	public void send(Notification e, NotificationType type, List<UserSupp> users, Map objectMap) throws BusinessException {
		switch (type) {
		case _EMAIL_:
			this.email(e, users, objectMap);
			break;
		case _EVENT_:
			this.notify(e);
			break;
		case _BOTH_:
			this.notify(e);
			this.email(e, users, objectMap);
			break;
		case _EMPTY_:
			if (!CollectionUtils.isEmpty(e.getUsers())) {
				this.notify(e);
			}
			this.email(e, users, objectMap);
			break;
		default:
			break;
		}
	}

	private void email(Notification n, List<UserSupp> users, Map objectMap) throws BusinessException {
		for (UserSupp u : users) {
			String fileReference;
			// this is for the default one that doesn't get imported
			if (u.getEmailTemplateRefId() == null) {
				Template template = this.templateSrv.generateTemplate(u, n.getTitle(), n.getTitle(), "e-mail body", EMAIL_BODY, true);
				byte[] file = this.templateSrv.loadTemplateFromDisk(template);
				try {
					this.mailSrv.uploadTemplate(file, template.getFileReference(), template.getDescription());
				} catch (MailMergeTemplateAlreadyExistsExeption e) {
					LOG.debug(e.getMessage(), e);
				}
				fileReference = template.getFileReference();
			} else {
				fileReference = this.templateSrv.findByRefid(u.getEmailTemplateRefId()).getFileReference();
			}
			EmailDto email = this.getEmailData(u, n, objectMap);
			String dateFormat = Session.user.get().getSettings().getDateFormat(DateFormatAttribute.JAVA_DATETIME_FORMAT).toPattern();
			String json = !email.getTo().isEmpty() ? MailMergeManager.getDataJson(new EmailDataDto(), "", email, dateFormat) : null;

			this.mailSrv.sendMail(fileReference, json);
			LOG.info("An email using the template with refId: " + u.getEmailTemplateRefId() + " was sent.");
		}
	}

	private EmailDto getEmailData(UserSupp u, Notification n, Map tenderInvitationMap) throws BusinessException {

		EmailDataDto data = null;
		TenderInvitationDto tenderData = null;
		Boolean mailIsForTender = false;

		// there is now support only for tender object map, in the future it will be nice to extend it somehow to other entities
		if (tenderInvitationMap != null && !tenderInvitationMap.isEmpty()) {
			tenderData = this.tenderInvitationDataCreator(u, tenderInvitationMap);
			mailIsForTender = true;
		} else {
			data = this.genericEmailDataCreator(u);
		}

		WebServiceDto wsDto = new WebServiceDto();
		wsDto.setName(n.getTitle());
		wsDto.setDescription(n.getDescripion());
		wsDto.setLastRequestTime(n.getEventDate());
		wsDto.setStatus(n.getStatus());
		wsDto.setResult(n.getResult());

		if (mailIsForTender) {
			tenderData.setWebService(wsDto);
		} else {
			data.setWebService(wsDto);
		}

		EmailAddressesDto address = new EmailAddressesDto();
		if (mailIsForTender) {
			address.setData(tenderData);
		} else {
			address.setData(data);
		}

		address.setName(u.getFirstName() + " " + u.getLastName());
		address.setTitle(u.getTitle().getName());
		address.setAddress(u.getEmail());

		List<EmailAddressesDto> list = new ArrayList<>();
		list.add(address);

		EmailDto email = new EmailDto();
		email.setFrom(this.sysParamSrv.getNoReplyAddress());
		email.setSubject((u.getEmailSubject() != null && !u.getEmailSubject().isEmpty()) ? u.getEmailSubject()
				: BusinessErrorCode.INTERFACE_NOTIFICATION_SUBJECT.getErrMsg());
		email.setTo(list);
		return email;
	}

	private EmailDataDto genericEmailDataCreator(UserSupp u) {
		EmailDataDto data;
		data = new EmailDataDto();
		data.setEmail(u.getEmail());
		data.setFirstName(u.getFirstName());
		data.setLastName(u.getLastName());
		data.setTitle(u.getTitle().getName());
		data.setFullName(u.getFirstName() + " " + u.getLastName());
		return data;
	}

	private TenderInvitationDto tenderInvitationDataCreator(UserSupp u, Map tenderInvitationMap) {
		TenderInvitationDto tenderData;
		tenderData = new TenderInvitationDto();
		tenderData.setEmail(u.getEmail());
		tenderData.setFirstName(u.getFirstName());
		tenderData.setLastName(u.getLastName());
		tenderData.setTitle(u.getTitle().getName());
		tenderData.setFullName(u.getFirstName() + " " + u.getLastName());
		tenderData.setTenderCode((String) tenderInvitationMap.get("tender_code"));
		tenderData.setTenderIssuer((String) tenderInvitationMap.get("tender_issuer"));
		tenderData.setTenderName((String) tenderInvitationMap.get("tender_name"));
		tenderData.setTenderVersion((String) tenderInvitationMap.get("tender_version"));
		tenderData.setContactPerson((String) tenderInvitationMap.get("contact_person"));
		tenderData.setContactEmail((String) tenderInvitationMap.get("contact_email"));
		tenderData.setBiddingFrom((String) (tenderInvitationMap.get("bidding_from")));
		tenderData.setBiddingTo((String) (tenderInvitationMap.get("bidding_to")));
		return tenderData;
	}

	private void notify(Notification e) {
		AppNotification appNotification = new AppNotification(e.getId(), e.getTitle(), e.getDescripion(), Session.user.get().getClientId());
		appNotification.setAction(e.getAction().getName());
		appNotification.setCategory(e.getCategory().getName());
		appNotification.setEventDate(e.getEventDate());
		appNotification.setLifeTime(e.getLifeTime());
		appNotification.setLink(e.getLink());
		appNotification.setPriority(e.getPriority().getName());
		appNotification.setMethodName(e.getMethodName());
		appNotification.setMethodParam(e.getMethodParam());

		for (UserSupp u : e.getUsers()) {
			appNotification.addToUsers(u.getCode());
		}
		this.appNotifsrv.add(appNotification);
	}

}
