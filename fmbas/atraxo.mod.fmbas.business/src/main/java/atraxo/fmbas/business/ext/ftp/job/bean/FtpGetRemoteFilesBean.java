package atraxo.fmbas.business.ext.ftp.job.bean;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.integration.file.filters.SimplePatternFileListFilter;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.file.remote.synchronizer.AbstractInboundFileSynchronizer;
import org.springframework.integration.ftp.filters.FtpSimplePatternFileListFilter;
import org.springframework.integration.ftp.inbound.FtpInboundFileSynchronizer;
import org.springframework.integration.sftp.filters.SftpSimplePatternFileListFilter;
import org.springframework.integration.sftp.inbound.SftpInboundFileSynchronizer;

import com.jcraft.jsch.ChannelSftp.LsEntry;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.ftp.AbstractFtpBean;
import atraxo.fmbas.business.ext.ftp.FtpCommunicationResult;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Download files from an ftp server.
 *
 * @author zspeter
 */
public class FtpGetRemoteFilesBean extends AbstractFtpBean implements InitializingBean {

	private static final Logger LOGGER = LoggerFactory.getLogger(FtpGetRemoteFilesBean.class);

	private File localDirectory;

	private AbstractInboundFileSynchronizer<?> ftpInboundFileSynchronizer;

	private boolean autoCreateLocalDirectory = true;

	private boolean deleteLocalFiles = true;

	private boolean retryIfNotFound = false;

	/**
	 * @param sessionFactory
	 * @param localDirectory
	 * @param remoteDirectory
	 * @param fileNamePattern
	 */
	public FtpGetRemoteFilesBean(SessionFactory<?> sessionFactory, File localDirectory, String remoteDirectory, String fileNamePattern) {
		super(sessionFactory, remoteDirectory, fileNamePattern);
		this.localDirectory = localDirectory;
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {

		this.setupFileSynchronizer();

		if (!this.localDirectory.exists()) {
			if (this.autoCreateLocalDirectory) {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("The '" + this.localDirectory + "' directory doesn't exist; Will create.");
				}
				this.localDirectory.mkdirs();
			} else {
				throw new FileNotFoundException(this.localDirectory.getName());
			}
		}
	}

	/**
	 * @throws InterruptedException
	 * @throws FileNotFoundException
	 * @throws BusinessException
	 */
	@Override
	public FtpCommunicationResult execute(AbstractEntity... list) throws InterruptedException, FileNotFoundException, BusinessException {
		this.deleteLocalFiles();
		FtpCommunicationResult result = new FtpCommunicationResult();
		try {
			this.ftpInboundFileSynchronizer.synchronizeToLocalDirectory(this.localDirectory);
		} catch (IllegalStateException e) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(e.getMessage(), e);
			}
			result.setMessage(BusinessErrorCode.FTP_CONNECTION_ERROR.getErrMsg());
			throw new BusinessException(BusinessErrorCode.FTP_CONNECTION_ERROR, BusinessErrorCode.FTP_CONNECTION_ERROR.getErrMsg(), e);

		}

		if (this.retryIfNotFound) {
			SimplePatternFileListFilter filter = new SimplePatternFileListFilter(this.getFileNamePattern());
			int attemptCount = 1;
			while (filter.filterFiles(this.localDirectory.listFiles()).isEmpty() && attemptCount <= this.getRetryAttempts()) {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("File(s) matching " + this.getFileNamePattern() + " not found on remote site.  Attempt " + attemptCount + " out of "
							+ this.getRetryAttempts());
				}
				Thread.sleep(this.getRetryIntervalMilliseconds());
				this.ftpInboundFileSynchronizer.synchronizeToLocalDirectory(this.localDirectory);
				attemptCount++;
			}

			if (attemptCount >= this.getRetryAttempts() && filter.filterFiles(this.localDirectory.listFiles()).isEmpty()) {
				throw new FileNotFoundException(
						"Could not find remote file(s) matching " + this.getFileNamePattern() + " after " + this.getRetryAttempts() + " attempts.");
			}
		}
		return result;
	}

	/**
	 * @return the localDirectory
	 */
	public File getLocalDirectory() {
		return this.localDirectory;
	}

	/**
	 * @return the autoCreateLocalDirectory
	 */
	public boolean isAutoCreateLocalDirectory() {
		return this.autoCreateLocalDirectory;
	}

	/**
	 * @return the deleteLocalFiles
	 */
	public boolean isDeleteLocalFiles() {
		return this.deleteLocalFiles;
	}

	/**
	 * @return the retryIfNotFound
	 */
	public boolean isRetryIfNotFound() {
		return this.retryIfNotFound;
	}

	/**
	 * @param autoCreateLocalDirectory the autoCreateLocalDirectory to set
	 */
	public void setAutoCreateLocalDirectory(boolean autoCreateLocalDirectory) {
		this.autoCreateLocalDirectory = autoCreateLocalDirectory;
	}

	/**
	 * @param deleteLocalFiles the deleteLocalFiles to set
	 */
	public void setDeleteLocalFiles(boolean deleteLocalFiles) {
		this.deleteLocalFiles = deleteLocalFiles;
	}

	/**
	 * @param localDirectory the localDirectory to set
	 */
	public void setLocalDirectory(File localDirectory) {
		this.localDirectory = localDirectory;
	}

	/**
	 * @param retryIfNotFound the retryIfNotFound to set
	 */
	public void setRetryIfNotFound(boolean retryIfNotFound) {
		this.retryIfNotFound = retryIfNotFound;
	}

	@SuppressWarnings("unchecked")
	private void setupFileSynchronizer() {
		if (this.isSftp()) {
			this.ftpInboundFileSynchronizer = new SftpInboundFileSynchronizer((SessionFactory<LsEntry>) this.getSessionFactory());
			((SftpInboundFileSynchronizer) this.ftpInboundFileSynchronizer).setFilter(new SftpSimplePatternFileListFilter(this.getFileNamePattern()));
		} else {
			this.ftpInboundFileSynchronizer = new FtpInboundFileSynchronizer((SessionFactory<FTPFile>) this.getSessionFactory());
			((FtpInboundFileSynchronizer) this.ftpInboundFileSynchronizer).setFilter(new FtpSimplePatternFileListFilter(this.getFileNamePattern()));
		}
		this.ftpInboundFileSynchronizer.setRemoteDirectory(this.getRemoteDirectory());
	}

	private void deleteLocalFiles() {
		if (this.deleteLocalFiles) {
			SimplePatternFileListFilter filter = new SimplePatternFileListFilter(this.getFileNamePattern());
			List<File> matchingFiles = filter.filterFiles(this.localDirectory.listFiles());
			if (CollectionUtils.isNotEmpty(matchingFiles)) {
				for (File file : matchingFiles) {
					FileUtils.deleteQuietly(file);
				}
			}
		}
	}

}
