/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.profile.service;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.geo.IAreaService;
import atraxo.fmbas.business.api.profile.ICompanyProfileService;
import atraxo.fmbas.business.api.user.IUserSubsidiaryService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CreditUpdateRequestStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataProcessedStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerDataTransmissionStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerType;
import atraxo.fmbas.domain.impl.fmbas_type.NatureOfBusiness;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.profile.CompanyProfile;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;

/**
 * Business extensions specific for {@link CompanyProfile} domain entity.
 */
public class CompanyProfile_Service extends atraxo.fmbas.business.impl.profile.CompanyProfile_Service implements ICompanyProfileService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CompanyProfile_Service.class);

	@Autowired
	private ICustomerService customerSrv;
	@Autowired
	private IUserSuppService userSrv;
	@Autowired
	private IUserSubsidiaryService subsidiarySrv;

	@Override
	public void create(Object obj) throws BusinessException {
		@SuppressWarnings("unchecked")
		Map<String, String> map = (Map<String, String>) obj;
		CompanyProfile profile = new CompanyProfile();
		String code = map.get("clientCode");
		String userCode = map.get("userCode");
		profile.setCode(code);
		profile.setName(code);
		this.insert(profile);
		this.assignCustomerToSessionUser(code, userCode);
	}

	private void assignCustomerToSessionUser(String code, String userCode) throws BusinessException {
		Customer customer = this.customerSrv.findByCode(code);
		UserSupp user = this.userSrv.findByCode(userCode);
		UserSubsidiary subsidiary = new UserSubsidiary();
		subsidiary.setSubsidiary(customer);
		subsidiary.setUser(user);
		subsidiary.setMainSubsidiary(true);
		this.subsidiarySrv.insert(subsidiary);

	}

	@Override
	protected void postInsert(CompanyProfile e) throws BusinessException {
		super.postInsert(e);
		this.createCustomer(e);
	}

	@Override
	protected void postUpdate(CompanyProfile e) throws BusinessException {
		super.postUpdate(e);
		try {
			Customer c = this.customerSrv.findByCode(e.getCode());
			c.setName(e.getName());
			this.customerSrv.update(c);
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw nre;
			}
			LOGGER.info("Could not find a customer, will create a new one !", nre);
			this.createCustomer(e);
		}
	}

	private void createCustomer(CompanyProfile e) throws BusinessException {
		IAreaService areaSrv = (IAreaService) this.findEntityService(Area.class);
		Customer c = new Customer();
		c.setCode(e.getCode());
		c.setName(e.getName());
		c.setIsCustomer(true);
		c.setIsSupplier(true);
		c.setIplAgent(true);
		c.setFuelSupplier(true);
		c.setFixedBaseOperator(true);
		c.setUseForBilling(false);
		c.setIsThirdParty(false);
		c.setIsSubsidiary(true);
		c.setAssignedArea(areaSrv.getWW());
		c.setType(CustomerType._INDEPENDENT_);
		c.setNatureOfBusiness(NatureOfBusiness._EMPTY_);
		c.setStatus(CustomerStatus._ACTIVE_);
		c.setTransmissionStatus(CustomerDataTransmissionStatus._NOT_AVAILABLE_);
		c.setProcessedStatus(CustomerDataProcessedStatus._NOT_AVAILABLE_);
		c.setCreditUpdateRequestStatus(CreditUpdateRequestStatus._NEW_);
		this.customerSrv.insert(c);
	}

}
