/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.workflowTask;

import atraxo.fmbas.business.api.workflowTask.IWorkflowTaskService;
import atraxo.fmbas.domain.impl.workflowTask.WorkflowTask;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link WorkflowTask} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class WorkflowTask_Service extends AbstractEntityService<WorkflowTask>
		implements
			IWorkflowTaskService {

	/**
	 * Public constructor for WorkflowTask_Service
	 */
	public WorkflowTask_Service() {
		super();
	}

	/**
	 * Public constructor for WorkflowTask_Service
	 * 
	 * @param em
	 */
	public WorkflowTask_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<WorkflowTask> getEntityClass() {
		return WorkflowTask.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return WorkflowTask
	 */
	public WorkflowTask findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(WorkflowTask.NQ_FIND_BY_BUSINESS,
							WorkflowTask.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"WorkflowTask", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"WorkflowTask", "id"), nure);
		}
	}

}
