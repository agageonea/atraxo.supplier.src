/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.job;

import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.ActionParameter;
import atraxo.fmbas.domain.impl.job.BatchJobParameter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ActionParameter} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ActionParameter_Service
		extends
			AbstractEntityService<ActionParameter> {

	/**
	 * Public constructor for ActionParameter_Service
	 */
	public ActionParameter_Service() {
		super();
	}

	/**
	 * Public constructor for ActionParameter_Service
	 * 
	 * @param em
	 */
	public ActionParameter_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ActionParameter> getEntityClass() {
		return ActionParameter.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ActionParameter
	 */
	public ActionParameter findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ActionParameter.NQ_FIND_BY_BUSINESS,
							ActionParameter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ActionParameter", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ActionParameter", "id"), nure);
		}
	}

	/**
	 * Find by reference: jobAction
	 *
	 * @param jobAction
	 * @return List<ActionParameter>
	 */
	public List<ActionParameter> findByJobAction(Action jobAction) {
		return this.findByJobActionId(jobAction.getId());
	}
	/**
	 * Find by ID of reference: jobAction.id
	 *
	 * @param jobActionId
	 * @return List<ActionParameter>
	 */
	public List<ActionParameter> findByJobActionId(Integer jobActionId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ActionParameter e where e.clientId = :clientId and e.jobAction.id = :jobActionId",
						ActionParameter.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("jobActionId", jobActionId).getResultList();
	}
	/**
	 * Find by reference: batchJobParameter
	 *
	 * @param batchJobParameter
	 * @return List<ActionParameter>
	 */
	public List<ActionParameter> findByBatchJobParameter(
			BatchJobParameter batchJobParameter) {
		return this.findByBatchJobParameterId(batchJobParameter.getId());
	}
	/**
	 * Find by ID of reference: batchJobParameter.id
	 *
	 * @param batchJobParameterId
	 * @return List<ActionParameter>
	 */
	public List<ActionParameter> findByBatchJobParameterId(
			Integer batchJobParameterId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ActionParameter e where e.clientId = :clientId and e.batchJobParameter.id = :batchJobParameterId",
						ActionParameter.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("batchJobParameterId", batchJobParameterId)
				.getResultList();
	}
}
