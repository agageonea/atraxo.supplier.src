/**
 *
 */
package atraxo.fmbas.business.ext.util;

import java.math.BigDecimal;
import java.util.Date;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.fmbas_type.FieldChangeName;
import atraxo.fmbas.domain.impl.fmbas_type.FieldChangeType;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Utility class for "working" with <code>FieldChangeType</code> and <code>FieldChangeName</code>
 *
 * @author vhojda
 */
public class TemporaryChangeUtil {

	/**
	 * @param fieldName
	 * @return
	 */
	public static FieldChangeType getType(FieldChangeName fieldName) {
		if (fieldName.equals(FieldChangeName._VALIDFROM_) || fieldName.equals(FieldChangeName._VALIDTO_)) {
			return FieldChangeType._DATE_;
		} else {
			return FieldChangeType._EMPTY_;
		}
	}

	/**
	 * Gets the value based on the <code>FieldChangeType</code>
	 *
	 * @param value
	 * @param type
	 * @return
	 */
	public static Object getValueByType(String stringValue, FieldChangeType type) throws BusinessException {
		Object value = null;
		if (type.equals(FieldChangeType._BIGDECIMAL_)) {
			try {
				value = new BigDecimal(stringValue);
			} catch (NumberFormatException e) {
				throw new BusinessException(BusinessErrorCode.FIELD_CHANGE_VALUE_ERROR, e);
			}

		} else if (type.equals(FieldChangeType._BLOB_)) {
			// TODO ?!
		} else if (type.equals(FieldChangeType._BOOLEAN_)) {
			value = Boolean.valueOf(stringValue);
		} else if (type.equals(FieldChangeType._DATE_)) {
			try {
				long longDate = Long.parseLong(stringValue);
				value = new Date(longDate);
			} catch (NumberFormatException e) {
				throw new BusinessException(BusinessErrorCode.FIELD_CHANGE_VALUE_ERROR, e);
			}
		} else if (type.equals(FieldChangeType._INTEGER_)) {
			try {
				value = Integer.parseInt(stringValue);
			} catch (NumberFormatException e) {
				throw new BusinessException(BusinessErrorCode.FIELD_CHANGE_VALUE_ERROR, e);
			}
		} else if (type.equals(FieldChangeType._LONG_)) {
			try {
				value = Long.parseLong(stringValue);
			} catch (NumberFormatException e) {
				throw new BusinessException(BusinessErrorCode.FIELD_CHANGE_VALUE_ERROR, e);
			}

		} else if (type.equals(FieldChangeType._STRING_)) {
			value = stringValue;
		} else if (type.equals(FieldChangeType._EMPTY_)) {
			value = "";
		} else {
			throw new BusinessException(BusinessErrorCode.FIELD_CHANGE_VALUE_ERROR, BusinessErrorCode.FIELD_CHANGE_VALUE_ERROR.getErrMsg());
		}

		return value;
	}
}
