package atraxo.fmbas.business.ext.calculator.model;

import java.math.BigDecimal;
import java.util.Date;

import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;
import atraxo.fmbas.domain.impl.fmbas_type.PostTypeInd;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.quotation.QuotationValue;

public class Result {

	private Date validFromDate;
	private Date validToDate;
	private Boolean provisioned;
	private BigDecimal rate;

	public Date getValidFromDate() {
		return this.validFromDate;
	}

	public void setValidFromDate(Date validFromDate) {
		this.validFromDate = validFromDate;
	}

	public Date getValidToDate() {
		return this.validToDate;
	}

	public void setValidToDate(Date validToDate) {
		this.validToDate = validToDate;
	}

	public Boolean getProvisioned() {
		return this.provisioned;
	}

	public void setProvisioned(Boolean provisioned) {
		this.provisioned = provisioned;
	}

	public BigDecimal getRate() {
		return this.rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public QuotationValue getResultAsQuotationValue(Quotation quotation) {
		QuotationValue quotationValue = new QuotationValue();
		quotationValue.setQuotation(quotation);
		quotationValue.setIsProvisioned(this.provisioned);
		quotationValue.setRate(this.rate);
		quotationValue.setValidFromDate(this.validFromDate);
		quotationValue.setValidToDate(this.validToDate);
		return quotationValue;
	}

	public ExchangeRateValue getResultAsExchangeRateValue(ExchangeRate exchangeRate) {

		ExchangeRateValue exchangeRateValue = new ExchangeRateValue();
		exchangeRateValue.setExchRate(exchangeRate);
		exchangeRateValue.setIsPrvsn(this.provisioned);
		exchangeRateValue.setRate(this.rate);
		exchangeRateValue.setVldFrDtRef(this.validFromDate);
		exchangeRateValue.setVldToDtRef(this.validToDate);

		// TODO
		PostTypeInd postingType = exchangeRate.getTserie().getPostTypeInd();
		exchangeRateValue.setPostTypeInd(postingType.ordinal());
		return exchangeRateValue;
	}
}
