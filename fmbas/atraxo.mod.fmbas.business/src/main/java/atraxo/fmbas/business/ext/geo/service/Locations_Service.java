/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.geo.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import atraxo.fmbas.business.api.geo.ILocationsService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.fmbas_type.AreasType;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.geo.Locations;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link Locations} domain entity.
 */
/**
 * @author apetho
 */
public class Locations_Service extends atraxo.fmbas.business.impl.geo.Locations_Service implements ILocationsService {

	private static final String BASE_AREA = "WW";

	@Override
	protected void preInsert(Locations e) throws BusinessException {
		super.preInsert(e);

		if (e.getValidTo().getTime() < e.getValidFrom().getTime()) {
			throw new BusinessException(BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
					BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO.getErrMsg());
		}

		if (e.getLatitude() != null && e.getLatitude().abs().compareTo(new BigDecimal(90)) == 1) {
			throw new BusinessException(BusinessErrorCode.LOCATION_LATITUDE, BusinessErrorCode.LOCATION_LATITUDE.getErrMsg());
		}

		if (e.getLongitude() != null && e.getLongitude().abs().compareTo(new BigDecimal(180)) == 1) {
			throw new BusinessException(BusinessErrorCode.LOCATION_LONGITUDE, BusinessErrorCode.LOCATION_LONGITUDE.getErrMsg());
		}

	}

	@Override
	protected void postInsert(List<Locations> list) throws BusinessException {

		super.postInsert(list);
		IEntityService<Area> areasService = this.findEntityService(Area.class);
		List<Area> areasList = this.findAreaByCode(BASE_AREA);
		if (areasList != null && areasList.size() > 0) {
			Area wwArea = areasList.get(0);
			Collection<Locations> locCollection = wwArea.getLocations();
			for (Locations location : list) {
				locCollection.add(location);
				this.updateAreasLocation(location, areasService);
			}
			areasService.update(wwArea);
		}

	}

	/**
	 * Update area's location list.
	 *
	 * @param loc
	 * @param areasService
	 * @throws BusinessException
	 */
	private void updateAreasLocation(Locations loc, IEntityService<Area> areasService) throws BusinessException {
		String countryCode = loc.getCountry().getCode();
		String areaCode = "C-" + countryCode;
		List<Area> areaList = this.findAreaByCode(areaCode);

		if (areaList != null) {
			if (areaList.size() == 1) {
				Area a = areaList.get(0);
				a.getLocations().add(loc);
				areasService.update(a);
			} else if (areaList.size() == 0) {
				Area a = new Area();
				a.setCode(areaCode);
				a.setActive(true);
				a.setIndicator(AreasType._SYSTEM_);
				a.setName(loc.getCountry().getName());
				a.setLocations(new HashSet<Locations>());
				a.getLocations().add(loc);
				areasService.insert(a);
			}
		}
	}

	/**
	 * Delete location from assignment.
	 *
	 * @param loc
	 * @param areasService
	 * @throws BusinessException
	 */
	private void deleteAreasLocation(Locations loc, IEntityService<Area> areasService) throws BusinessException {
		Locations location = this.findById(loc.getId());
		String countryCode = location.getCountry().getCode();
		String areaCode = "C-" + countryCode;
		List<Area> areaList = this.findAreaByCode(areaCode);

		if (areaList != null) {
			if (areaList.size() == 1) {
				Area a = areaList.get(0);
				a.getLocations().remove(location);
				areasService.update(a);
			}
		}
	}

	@Override
	protected void preUpdate(Locations e) throws BusinessException {
		super.preUpdate(e);
		IEntityService<Area> areasService = this.findEntityService(Area.class);
		this.deleteAreasLocation(e, areasService);

		if (e.getValidTo() == null) {
			throw new BusinessException(BusinessErrorCode.VALID_TO_NULL, BusinessErrorCode.VALID_TO_NULL.getErrMsg());
		}

		if (e.getValidFrom() == null) {
			throw new BusinessException(BusinessErrorCode.VALID_FROM_NULL, BusinessErrorCode.VALID_FROM_NULL.getErrMsg());
		}

		if (e.getValidTo().getTime() < e.getValidFrom().getTime()) {
			throw new BusinessException(BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
					BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO.getErrMsg());
		}

		if (e.getLatitude() != null && e.getLatitude().abs().compareTo(new BigDecimal(90)) == 1) {
			throw new BusinessException(BusinessErrorCode.LOCATION_LATITUDE, BusinessErrorCode.LOCATION_LATITUDE.getErrMsg());
		}

		if (e.getLongitude() != null && e.getLongitude().abs().compareTo(new BigDecimal(180)) == 1) {
			throw new BusinessException(BusinessErrorCode.LOCATION_LONGITUDE, BusinessErrorCode.LOCATION_LONGITUDE.getErrMsg());
		}

	}

	@Override
	protected void postDelete(List<Locations> list) throws BusinessException {

		super.postDelete(list);
		IEntityService<Area> areasService = this.findEntityService(Area.class);
		List<Area> areasList = this.findAreaByCode(BASE_AREA);
		if (areasList != null && areasList.size() > 0) {
			Area wwArea = areasList.get(0);
			Collection<Locations> locCollection = wwArea.getLocations();
			for (Locations location : list) {
				locCollection.remove(location);
				this.deleteAreasLocation(location, areasService);
			}
			areasService.update(wwArea);
		}
	}

	private List<Area> findAreaByCode(String code) throws BusinessException {
		Session.user.get().getClient().getId();
		IEntityService<Area> areasService = this.findEntityService(Area.class);
		String sql = "select e from " + Area.class.getSimpleName() + " e where e.clientId=:clientId and e.code =:code";
		return areasService.getEntityManager().createQuery(sql, Area.class).setParameter("clientId", Session.user.get().getClient().getId())
				.setParameter("code", code).getResultList();
	}

	@Override
	protected void postUpdate(List<Locations> list) throws BusinessException {

		super.postUpdate(list);
		IEntityService<Area> areasService = this.findEntityService(Area.class);
		for (Locations location : list) {

			this.updateAreasLocation(location, areasService);
		}
	}

	@Override
	public Locations findByCodeAndClientId(String code, String clientId) throws BusinessException {
		String hql = "select e from " + Locations.class.getSimpleName() + " e where e.clientId = :clientId and e.code = :code";
		List<Locations> locations = this.getEntityManager().createQuery(hql, Locations.class).setParameter("clientId", clientId)
				.setParameter("code", code).getResultList();
		if (locations != null && locations.size() == 1) {
			return locations.get(0);
		} else {
			return null;
		}
	}
}
