/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.vat.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.vat.IVatRateService;
import atraxo.fmbas.business.api.vat.IVatService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.vat.Vat;
import atraxo.fmbas.domain.impl.vat.VatRate;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.utils.DateUtils;

/**
 * Business extensions specific for {@link Vat} domain entity.
 */
public class Vat_Service extends atraxo.fmbas.business.impl.vat.Vat_Service implements IVatService {

	@Autowired
	private IVatRateService vatRateService;

	@Override
	protected void postDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.postDeleteByIds(ids, context);

		IVatRateService service = (IVatRateService) this.findEntityService(VatRate.class);
		for (Object o : ids) {
			List<VatRate> vatRates = service.findByVatId((Integer) o);
			service.delete(vatRates);
		}

	}

	@Override
	public BigDecimal getVat(Date date, Country country) throws BusinessException {
		List<Vat> vats = this.findByCountry(country);
		if (!CollectionUtils.isEmpty(vats) && vats.size() == 1) {
			VatRate rate = this.vatRateService.findVatRateByDate(DateUtils.removeTime(date), vats.get(0));
			return rate != null ? rate.getRate() : BigDecimal.ZERO;
		}
		return BigDecimal.ZERO;
	}

	@Override
	protected void preInsert(Vat e) throws BusinessException {
		super.preInsert(e);
		this.checkUniqueKey(e);
	}

	@Override
	protected void preUpdate(Vat e) throws BusinessException {
		super.preUpdate(e);
		this.checkUniqueKey(e);
	}

	private void checkUniqueKey(Vat e) throws BusinessException {

		List<Vat> v = this.findByCountry(e.getCountry());
		if (v.size() > 1) {
			throw new BusinessException(BusinessErrorCode.DUPLICATE_ENTITY,
					String.format(BusinessErrorCode.DUPLICATE_ENTITY.getErrMsg(), Vat.class.getSimpleName(), "Contry"));
		} else if (v.size() == 1 && !v.get(0).getId().equals(e.getId())) {
			throw new BusinessException(BusinessErrorCode.DUPLICATE_ENTITY,
					String.format(BusinessErrorCode.DUPLICATE_ENTITY.getErrMsg(), Vat.class.getSimpleName(), "Contry"));
		}

	}
}
