package atraxo.fmbas.business.ext.exceptions.mailMerge;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author apetho
 */
public class MailMergeApplicationFaulExeption extends BusinessException {

	private static final long serialVersionUID = 2753442921451694259L;

	/**
	 * @param exception
	 */
	public MailMergeApplicationFaulExeption(Throwable exception) {
		super(BusinessErrorCode.MAIL_MERGE_APPLICATION_FAULT, BusinessErrorCode.MAIL_MERGE_APPLICATION_FAULT.getErrMsg(), exception);
	}

}
