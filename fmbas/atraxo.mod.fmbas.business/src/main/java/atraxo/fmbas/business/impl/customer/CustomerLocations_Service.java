/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.customer;

import atraxo.fmbas.business.api.customer.ICustomerLocationsService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.customer.CustomerLocations;
import atraxo.fmbas.domain.impl.geo.Country;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link CustomerLocations} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class CustomerLocations_Service
		extends
			AbstractEntityService<CustomerLocations>
		implements
			ICustomerLocationsService {

	/**
	 * Public constructor for CustomerLocations_Service
	 */
	public CustomerLocations_Service() {
		super();
	}

	/**
	 * Public constructor for CustomerLocations_Service
	 * 
	 * @param em
	 */
	public CustomerLocations_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<CustomerLocations> getEntityClass() {
		return CustomerLocations.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return CustomerLocations
	 */
	public CustomerLocations findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(CustomerLocations.NQ_FIND_BY_CODE,
							CustomerLocations.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"CustomerLocations", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"CustomerLocations", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return CustomerLocations
	 */
	public CustomerLocations findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(CustomerLocations.NQ_FIND_BY_BUSINESS,
							CustomerLocations.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"CustomerLocations", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"CustomerLocations", "id"), nure);
		}
	}

	/**
	 * Find by reference: country
	 *
	 * @param country
	 * @return List<CustomerLocations>
	 */
	public List<CustomerLocations> findByCountry(Country country) {
		return this.findByCountryId(country.getId());
	}
	/**
	 * Find by ID of reference: country.id
	 *
	 * @param countryId
	 * @return List<CustomerLocations>
	 */
	public List<CustomerLocations> findByCountryId(Integer countryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from CustomerLocations e where e.clientId = :clientId and e.country.id = :countryId",
						CustomerLocations.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("countryId", countryId).getResultList();
	}
	/**
	 * Find by reference: customers
	 *
	 * @param customers
	 * @return List<CustomerLocations>
	 */
	public List<CustomerLocations> findByCustomers(Customer customers) {
		return this.findByCustomersId(customers.getId());
	}
	/**
	 * Find by ID of reference: customers.id
	 *
	 * @param customersId
	 * @return List<CustomerLocations>
	 */
	public List<CustomerLocations> findByCustomersId(Integer customersId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from CustomerLocations e, IN (e.customers) c where e.clientId = :clientId and c.id = :customersId",
						CustomerLocations.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customersId", customersId).getResultList();
	}
}
