package atraxo.fmbas.business.ext.bpm;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.annotation.Transactional;

import atraxo.ad.business.api.security.IRoleService;
import atraxo.ad.business.api.security.IUserService;
import atraxo.ad.business.api.system.IClientService;
import atraxo.ad.domain.impl.security.Role;
import atraxo.ad.domain.impl.security.User;
import atraxo.ad.domain.impl.system.Client;
import atraxo.fmbas.business.api.custom.workflow.IWorkflowTerminatorService;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.ext.bpm.WorkflowStartResult;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceService;
import atraxo.fmbas.business.api.workflow.IWorkflowNotificationService;
import atraxo.fmbas.business.api.workflow.IWorkflowService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.workflow.ClaimedWorkflowException;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowDeployStatus;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowInstanceStatus;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowParamType;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;
import seava.j4e.api.ISettings;
import seava.j4e.api.enums.SysParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.exceptions.InvalidConfiguration;
import seava.j4e.api.service.IClientInfoProvider;
import seava.j4e.api.session.IClient;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.IUserProfile;
import seava.j4e.api.session.IUserSettings;
import seava.j4e.api.session.IWorkspace;
import seava.j4e.api.session.Session;
import seava.j4e.business.bpm.ActivitiBpmService;
import seava.j4e.business.service.AbstractBusinessDelegate;
import seava.j4e.commons.security.AppClient;
import seava.j4e.commons.security.AppUser;
import seava.j4e.commons.security.AppUserProfile;
import seava.j4e.commons.security.AppUserSettings;

/**
 * @author vhojda
 */
public class WorkflowBpmManager extends AbstractBusinessDelegate implements ApplicationListener<ContextRefreshedEvent>, IWorkflowBpmManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowBpmManager.class);

	private static final String FOLDER_WORKFLOW = "workflow";

	public static final String DATE_FORMAT = "dd.MM.yyyy";

	@Autowired
	private IWorkflowService workflowService;

	@Autowired
	private IUserService userService;
	@Autowired
	private IRoleService roleService;
	@Autowired
	private IUserSuppService userSuppService;
	@Autowired
	private IWorkflowInstanceService workflowInstanceService;
	@Autowired
	private IWorkflowInstanceEntityService workflowInstanceEntityService;
	@Autowired
	private IWorkflowNotificationService workflowNotificationService;
	@Autowired
	private ActivitiBpmService activitiBpmService;
	@Autowired
	private ICustomerService customerSevice;

	@Autowired
	private ISettings settings;

	@Autowired
	private IClientService clientService;

	/*
	 * (non-Javadoc)
	 * @see org.springframework.context.ApplicationListener#onApplicationEvent(org.springframework.context.ApplicationEvent)
	 */
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		this.redeployWorkflows();
	}

	/**
	 * Redeploys the workflows that need redeployment.
	 */
	@Override
	public void redeployWorkflows() {
		// deploy the workflows that need to be re-deployed
		try {
			String userCode = this.getUserCode();
			List<Client> clients = this.clientService.findEntitiesByAttributes(new HashMap<>());
			for (Client c : clients) {
				// create a new session user for each existing client code
				this.createSessionUser(c.getCode(), userCode);

				// get the workflows that need redeploy
				Map<String, Object> params = new HashMap<>();
				params.put("redeploy", true);
				List<Workflow> wkfs = this.workflowService.findEntitiesByAttributes(params);
				for (Workflow wkf : wkfs) {
					this.deployWorkflow(wkf.getId(), wkf.getWorkflowFile());
				}
			}
		} catch (BusinessException e) {
			LOGGER.error("ERROR:could not redeploy workflows !", e);
		}
	}

	/**
	 * @return
	 */
	private String getUserCode() {
		String userCode = "SYS_USER";
		try {
			userCode = this.settings.getParam(SysParam.CORE_WORKFLOW_USER.name());
		} catch (InvalidConfiguration e1) {
			LOGGER.warn("Could not retrieve system parameter for core workflow user, will use the default one " + userCode, e1);
		}
		return userCode;
	}

	/**
	 * Creates a custom session user
	 *
	 * @param clientCode
	 * @param userCode
	 * @throws BusinessException
	 */
	private void createSessionUser(String clientCode, String userCode) throws BusinessException {
		try {
			IClientService clientSrv = (IClientService) this.findEntityService(Client.class);
			Client c = clientSrv.findByCode(clientCode);
			IClient client = new AppClient(c.getId(), clientCode, "");
			IUserSettings userSettings = AppUserSettings.newInstance(this.getSettings());
			IUserProfile profile = new AppUserProfile(true, null, false, false, false);

			// create an incomplete user first
			IUser user = new AppUser(userCode, userCode, null, userCode, null, null, client, userSettings, profile, null, true);
			Session.user.set(user);

			// get the client workspace info
			IWorkspace ws = this.getApplicationContext().getBean(IClientInfoProvider.class).getClientWorkspace();
			user = new AppUser(userCode, userCode, null, userCode, null, null, client, userSettings, profile, ws, true);
			Session.user.set(user);

			Message<IUser> message = MessageBuilder.withPayload(user).build();
			this.getApplicationContext().getBean("updateJobSessionChannel", MessageChannel.class).send(message);

		} catch (InvalidConfiguration e) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}
	}

	/**
	 * @param wkfName
	 * @param objectId
	 * @param objectType
	 * @return
	 */
	@Override
	public WorkflowInstance getWorkflowInstanceByEntity(WorkflowNames wkfName, Integer objectId, String objectType) {
		WorkflowInstanceEntity instanceEntity = null;
		Map<String, Object> params = new HashMap<>();
		params.put("name", wkfName.getWorkflowName());
		params.put("objectId", objectId);
		params.put("objectType", objectType);
		instanceEntity = this.workflowInstanceEntityService.findEntityByAttributes(params);
		return instanceEntity.getWorkflowInstance();
	}

	/**
	 * @param workflowResourceName
	 * @param workflowId
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public boolean deployWorkflow(Integer workflowId, String workflowResourceName) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START deployWorkflow()");
		}
		boolean deployed = false;

		InputStream is = this.getStream(workflowResourceName);
		this.activitiBpmService.getRepositoryService().createDeployment().tenantId(Session.user.get().getClientId())
				.addInputStream(workflowResourceName, is).deploy();

		try {
			is.close();
		} catch (IOException e) {
			LOGGER.warn("WARNING: Could not close the InputStream for workflow resource " + workflowResourceName, e);
		}

		// also update the workflow status
		Workflow workflow = this.workflowService.findByBusiness(workflowId);
		workflow.setDeployStatus(WorkflowDeployStatus._DEPLOYED_);
		workflow.setDeployVersion(workflow.getDeployVersion() != null ? workflow.getDeployVersion().intValue() + 1 : 1);
		workflow.setRedeploy(false);
		this.workflowService.update(workflow);

		// if execution arrives here then everything is OK
		deployed = true;

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END deployWorkflow()");
		}
		return deployed;
	}

	/**
	 * @param workflowResourceName
	 * @return
	 * @throws BusinessException
	 */
	private InputStream getStream(String workflowResourceName) throws BusinessException {
		InputStream is = this.getClass().getResourceAsStream("/" + FOLDER_WORKFLOW + "/" + workflowResourceName);
		if (is == null) {
			is = this.getClass().getResourceAsStream(FOLDER_WORKFLOW + "/" + workflowResourceName);
		}
		if (is == null) {
			String errorDetails = "Could not locate the file for workflow resource '" + workflowResourceName + "' !";
			LOGGER.error(errorDetails);
			throw new BusinessException(ErrorCode.G_FILE_NOT_FOUND, errorDetails);
		}
		return is;
	}

	/**
	 * Checks if a specific workflow is actually deployed in Activiti.
	 *
	 * @param wkf
	 * @return
	 */
	private boolean isDeployedInActiviti(Workflow wkf) {
		long procDefs = this.activitiBpmService.getRepositoryService().createProcessDefinitionQuery().processDefinitionName(wkf.getName())
				.processDefinitionTenantId(Session.user.get().getClientId()).count();
		return procDefs != 0;
	}

	/**
	 * @param workflowName
	 * @param wkfVariables
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public WorkflowStartResult startWorkflow(String workflowName, Map<String, Object> wkfVariables, String subsidiaryID, AbstractEntity... entities)
			throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START startWorkflow()");
		}
		WorkflowStartResult result = new WorkflowStartResult();
		result.setStarted(false);

		Workflow wkf;
		String subsidiaryCode = "";
		if (!StringUtils.isEmpty(subsidiaryID)) {
			Customer subsidiary = this.customerSevice.findByRefid(subsidiaryID);
			subsidiaryCode = subsidiary.getCode();
		}
		wkf = this.findWorkflowForSubsidiary(workflowName, subsidiaryCode);

		if (wkf != null) {
			String validationError = this.validateParams(wkf.getParams());
			boolean valid = validationError == null;

			// if valid attempt to start a process instance
			if (valid) {
				// check deployment
				boolean deployValid = true;
				if (!this.isDeployedInActiviti(wkf)) {
					deployValid = this.deployWorkflow(wkf.getId(), wkf.getWorkflowFile());
				}
				if (deployValid) {
					// create the Variables MAP for the process with the needed params
					Map<String, Object> variables = new HashMap<>();
					variables.put(WorkflowVariablesConstants.VAR_WKF_ID, wkf.getId());
					variables.put(WorkflowVariablesConstants.VAR_WKF_NAME, wkf.getName());

					variables.put(WorkflowVariablesConstants.VAR_APP_CLIENT_CODE, Session.user.get().getClientCode());
					variables.put(WorkflowVariablesConstants.VAR_APP_CLIENT_ID, Session.user.get().getClientId());

					// also put the params from the workflow parameters
					for (WorkflowParameter wkfParam : wkf.getParams()) {
						this.putParamValue(wkfParam, variables);
					}

					// put the variables from received map
					if (wkfVariables != null) {
						variables.putAll(wkfVariables);
					}

					// create a WorkflowInstance prior to starting the Activiti process (because we need the ID -used later as business key for
					// Activiti
					// process instance)
					WorkflowInstance wkfInstance = new WorkflowInstance();
					wkfInstance.setName(wkf.getName() + "_" + System.currentTimeMillis());// could be done better ???
					wkfInstance.setStatus(WorkflowInstanceStatus._IN_PROGRESS_);
					wkfInstance.setWorkflow(wkf);
					wkfInstance.setWorkflowVersion(wkf.getDeployVersion());
					wkfInstance.setStartExecutionDate(new Date());
					wkfInstance.setStartExecutionUser(this.getCurrentSessionUser());
					if (wkfVariables != null) {
						Object wkfEntityDescriptionObj = wkfVariables.get(WorkflowVariablesConstants.VAR_WKF_ENTITY_DESCRIPTION);
						wkfInstance.setDescription(wkfEntityDescriptionObj != null ? wkfEntityDescriptionObj.toString() : "");
					}
					List<AbstractEntity> list = Arrays.asList(entities);
					for (AbstractEntity e : list) {
						WorkflowInstanceEntity entity = new WorkflowInstanceEntity();
						entity.setName(wkf.getName());
						entity.setObjectId(e.getId());
						entity.setObjectType(e.getClass().getSimpleName());
						wkfInstance.addToEntitites(entity);
					}

					try {
						this.workflowInstanceService.insertAndPersist(wkfInstance);
					} catch (TransactionSystemException e) {
						throw new BusinessException(BusinessErrorCode.WORKFLOW_INSTANCE_ALREADY_STARTED,
								BusinessErrorCode.WORKFLOW_INSTANCE_ALREADY_STARTED.getErrMsg(), e);
					}
					variables.put(WorkflowVariablesConstants.VAR_WKF_INSTANCE_ID, wkfInstance.getId());

					// try starting the Activiti process
					WorkflowInstanceStarter workflowInstanceManager = new WorkflowInstanceStarter(wkf, wkfInstance, variables,
							this.activitiBpmService, this.workflowInstanceService, this.workflowNotificationService);
					result = workflowInstanceManager.start();
				} else {
					result.setReason(String.format(BusinessErrorCode.WORKFLOW_DEPLOY_ERROR.getErrMsg(), wkf.getName()));
					LOGGER.warn("WARNING: Could not start new workflow {} since the system could not deploy it before starting it!", wkf.getName());
				}
			} else {
				result.setReason(validationError);
				LOGGER.warn("WARNING: Could not start new workflow {} since is not valid!", wkf.getName());
			}

		} else {
			result.setReason(
					String.format(BusinessErrorCode.WORKFLOW_COULD_NOT_RETRIEVE_FOR_SUBSIDIARY_ERROR.getErrMsg(), workflowName, subsidiaryCode));
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END startWorkflow");
		}
		return result;
	}

	/**
	 * @return
	 */
	private User getCurrentSessionUser() {
		User user = null;
		try {
			user = this.userService.findByLogin(Session.user.get().getLoginName());
		} catch (Exception e) {
			LOGGER.warn("WARNING:could not retrieve user for current session !", e);
		}
		return user;
	}

	/**
	 * @return
	 */
	private String getCurrentSessionUserFullName() {
		String fullName = "";
		try {
			UserSupp user = this.userSuppService.findByLogin(Session.user.get().getLoginName());
			fullName = user.getFirstName() + " " + user.getLastName();
		} catch (Exception e) {
			LOGGER.warn("Could not retrieve current user fullname from current Session!", e);
			fullName = Session.user.get().getCode();
		}
		return fullName;
	}

	/**
	 * Searches for a specific workflow, given an ID of an entity that extends <code>AbstractSubsidiary</code>
	 *
	 * @param workflowName
	 * @param subsidiaryCode
	 * @return
	 */
	private Workflow findWorkflowForSubsidiary(String workflowName, String subsidiaryCode) {
		Workflow wkf = null;

		// try to find a workflow, given the subsidiary of the Object
		try {
			wkf = this.workflowService.findByNameAndSubsidiary(workflowName, subsidiaryCode);
		} catch (Exception e) {
			LOGGER.warn("ERROR:could not find a Workflow for workflow name " + workflowName + " ! ", e);
		}

		return wkf;
	}

	/**
	 * Resumes a <code>WorkflowInstance</code>, given its ID
	 *
	 * @param wkfInstanceID
	 * @returns true if resumed, false otherwise
	 * @throws BusinessException
	 */
	@Override
	public boolean resumeWorkflow(Integer wkfInstanceID) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START resumeWorkflow()");
		}
		boolean resumed = true;

		// update the workflow instance
		WorkflowInstance instance = this.workflowInstanceService.findByBusiness(wkfInstanceID);
		instance.setStatus(WorkflowInstanceStatus._IN_PROGRESS_);
		instance.setResult("");
		this.workflowInstanceService.update(instance);

		// terminate Activiti process instance
		ProcessInstance processInstance = this.getProcessInstanceByWorkflowInstance(wkfInstanceID);
		if (processInstance != null) {
			if (processInstance.isSuspended()) {
				// "resume" the process instance if it is suspended only
				this.activitiBpmService.getRuntimeService().activateProcessInstanceById(processInstance.getId());
			}
		} else {
			LOGGER.error("WARNING:could not locate a ProcessInstance for process instance business key {} !", wkfInstanceID);
			throw new BusinessException(BusinessErrorCode.WORKFLOW_NO_PROCESS_INSTANCE, BusinessErrorCode.WORKFLOW_NO_PROCESS_INSTANCE.getErrMsg());
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END resumeWorkflow()");
		}
		return resumed;
	}

	/**
	 * Terminates a specific list of <code>WorkflowInstance</code>, given a reason
	 *
	 * @param wkfInstanceID
	 * @param terminationReason
	 * @throws BusinessException
	 * @returns true if all terminated with success, false otherwise
	 */
	@Override
	public Integer terminateWorkflow(List<Integer> wkfInstanceIDList, String terminationReason) throws BusinessException {
		Integer result = 0;
		for (Integer wkfInstanceID : wkfInstanceIDList) {
			try {
				result += this.terminateWorkflow(wkfInstanceID, terminationReason) ? 1 : 0;
			} catch (BusinessException e) {
				LOGGER.debug("Error while terminating workflow instance with id " + wkfInstanceID.toString(), e);
			}
		}
		return result;
	}

	/**
	 * Terminates a specific <code>WorkflowInstance</code>, given a reason
	 *
	 * @param wkfInstanceID
	 * @param terminationReason
	 * @throws BusinessException
	 * @returns true if terminated with success, false otherwise
	 */
	@Override
	public boolean terminateWorkflow(Integer wkfInstanceID, String terminationReason) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START terminateWorkflow()");
		}

		boolean terminated = true;
		// get the workflow instance
		WorkflowInstance instance = this.workflowInstanceService.findByBusiness(wkfInstanceID);

		this.getWorkflowTerminator(instance.getWorkflow()).terminateWorkflow(instance, terminationReason);

		// update the workflow instance
		instance.setStatus(WorkflowInstanceStatus._TERMINATED_);
		instance.setResult(terminationReason);
		instance.setEntitites(null);
		this.workflowInstanceService.update(instance);

		// terminate Activiti process instance (if it exists)
		ProcessInstance processInstance = this.getProcessInstanceByWorkflowInstance(wkfInstanceID);

		if (processInstance != null) {
			this.activitiBpmService.getRuntimeService().deleteProcessInstance(processInstance.getId() + "", terminationReason);
		} else {
			LOGGER.warn("WARNING:could not locate a ProcessInstance for process instance business key {} !", wkfInstanceID);
		}

		this.workflowNotificationService.sendNotification(instance, WorkflowInstanceStatus._TERMINATED_, terminationReason);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END terminateWorkflow()");
		}
		return terminated;
	}

	/**
	 * @param wkfInstanceID
	 * @param accepted
	 * @param acceptanceNote
	 * @return
	 * @throws BusinessException
	 */
	@Transactional
	@Override
	public boolean claimCompleteTask(Integer wkfInstanceID, boolean accepted, String acceptanceNote) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START claimCompleteTask()");
		}
		String username = Session.user.get().getLoginName();

		Task task = this.activitiBpmService.getTaskService().createTaskQuery().processInstanceBusinessKey(wkfInstanceID.toString()).singleResult();

		// claim in Activiti
		String workflowTaskID = task.getId();
		if (task.getAssignee() == null) {
			this.activitiBpmService.getTaskService().claim(workflowTaskID, username);
		} else if (!username.equals(task.getAssignee())) {
			throw new ClaimedWorkflowException(task.getAssignee());
		}

		this.activitiBpmService.getTaskService().setVariable(workflowTaskID, WorkflowVariablesConstants.TASK_NOTE_VAR, acceptanceNote);
		this.activitiBpmService.getTaskService().setVariable(workflowTaskID, WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR,
				this.getCurrentSessionUserFullName());
		this.activitiBpmService.getTaskService().setVariable(workflowTaskID, WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME, username);
		this.activitiBpmService.getTaskService().setVariable(workflowTaskID, WorkflowVariablesConstants.TASK_ACCEPTANCE_VAR, accepted);

		// complete in Activiti
		this.activitiBpmService.getTaskService().complete(workflowTaskID);

		// update the workflow instance
		WorkflowInstance instance = this.workflowInstanceService.findByBusiness(wkfInstanceID);
		instance.setResult((accepted ? "Approved by " : "Rejected by ") + username + ":" + acceptanceNote);
		this.workflowInstanceService.update(instance);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END claimCompleteTask()");
		}

		return true;

	}

	/**
	 * @param wkfInstanceID
	 * @param workflowTaskID
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public boolean claimTask(Integer wkfInstanceID, String workflowTaskID) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START claimTask()");
		}
		boolean claimed = true;

		String username = Session.user.get().getLoginName();

		// claim in Activiti
		this.activitiBpmService.getTaskService().claim(workflowTaskID, username);

		// update the workflow instance
		WorkflowInstance instance = this.workflowInstanceService.findByBusiness(wkfInstanceID);
		instance.setStatus(WorkflowInstanceStatus._IN_PROGRESS_);
		instance.setResult("Claimed by " + username);
		this.workflowInstanceService.update(instance);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END claimTask()");
		}
		return claimed;
	}

	/**
	 * @param wkfInstanceID
	 * @param workflowTaskID
	 * @param accepted
	 * @param acceptanceNote
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public boolean completeTask(Integer wkfInstanceID, String workflowTaskID, Boolean accepted, String acceptanceNote) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START completeTask()");
		}
		boolean completed = true;

		String username = Session.user.get().getLoginName();

		this.activitiBpmService.getTaskService().setVariable(workflowTaskID, WorkflowVariablesConstants.TASK_NOTE_VAR, acceptanceNote);
		this.activitiBpmService.getTaskService().setVariable(workflowTaskID, WorkflowVariablesConstants.TASK_NOTE_APPROVER_NAME_VAR,
				this.getCurrentSessionUserFullName());
		this.activitiBpmService.getTaskService().setVariable(workflowTaskID, WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME, username);
		this.activitiBpmService.getTaskService().setVariable(workflowTaskID, WorkflowVariablesConstants.TASK_ACCEPTANCE_VAR, accepted);

		// update the workflow instance
		WorkflowInstance instance = this.workflowInstanceService.findByBusiness(wkfInstanceID);
		instance.setResult((accepted ? "Approved by " : "Rejected by ") + username + ":" + acceptanceNote);
		this.workflowInstanceService.update(instance);

		// complete in Activiti
		this.activitiBpmService.getTaskService().complete(workflowTaskID);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END completeTask()");
		}
		return completed;
	}

	/**
	 * @param wkfParam
	 * @param variables
	 */
	private void putParamValue(WorkflowParameter wkfParam, Map<String, Object> variables) {
		// make sure to put something if not null
		variables.put(wkfParam.getName(), !StringUtils.isEmpty(wkfParam.getParamValue()) ? wkfParam.getParamValue() : wkfParam.getDefaultValue());

		// make sure to transform the value in corresponding type (if the case)
		Object paramValue = variables.get(wkfParam.getName());
		if (paramValue != null) {
			if (wkfParam.getType().equals(WorkflowParamType._INTEGER_)) {
				variables.put(wkfParam.getName(), Integer.valueOf(paramValue.toString()));
			} else if (wkfParam.getType().equals(WorkflowParamType._BOOLEAN_)) {
				variables.put(wkfParam.getName(), Boolean.valueOf(paramValue.toString()));
			} else if (wkfParam.getType().equals(WorkflowParamType._STRING_)) {
				variables.put(wkfParam.getName(), paramValue.toString());
			} else if (wkfParam.getType().equals(WorkflowParamType._DATE_)) {
				SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
				try {
					Date date = sdf.parse(paramValue.toString());
					variables.put(wkfParam.getName(), date);
				} catch (ParseException e) {
					LOGGER.error("Could not parse received date : {}.", paramValue);
				}
			} else if (wkfParam.getType().equals(WorkflowParamType._ENUMERATION_)) {
				// do nothing
			} else if (wkfParam.getType().equals(WorkflowParamType._LOV_)) {
				// do nothing
			} else if (wkfParam.getType().equals(WorkflowParamType._MASKED_)) {
				// do nothing
			}
		}

	}

	/**
	 * @param params
	 * @return
	 */
	private String validateParams(Collection<WorkflowParameter> params) {
		String validationError = null;

		// sort params, so that the order makes sense
		List<WorkflowParameter> paramList = new ArrayList<>();
		paramList.addAll(params);

		// sort by name
		paramList.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));

		for (WorkflowParameter param : paramList) {
			String value = !StringUtils.isEmpty(param.getParamValue()) ? param.getParamValue() : param.getDefaultValue();

			// check for mandatory
			boolean invalid = false;
			if (param.getMandatory() && StringUtils.isEmpty(value)) {
				validationError = "No value filled in for mandatory parameter '" + param.getName() + "'.";
				invalid = true;
			}
			switch (param.getType()) {
			case _BOOLEAN_:
				if (!(value.equals("true") || value.equals("false"))) {
					validationError = "The parameter value for '" + param.getName() + "' is not a valid boolean value (true/false) !";
					invalid = true;
				}
				break;
			case _DATE_:
				SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
				try {
					sdf.parse(value);
				} catch (ParseException e) {
					validationError = "The parameter value for '" + param.getName() + "' is not a valid date value of format " + DATE_FORMAT + " !";
					invalid = true;
				}

				break;
			case _INTEGER_:
				try {
					int number = Integer.parseInt(value);
					if (number < 0) {
						validationError = "The parameter value for '" + param.getName() + "' must be a positive number !";
					}
				} catch (NumberFormatException e) {
					validationError = "The parameter value for '" + param.getName() + "' is not a valid integer value !";
					invalid = true;
				}
				// check for special scenarios that apply to all workflows
				if (param.getName().equals(WorkflowVariablesConstants.VAR_WKF_EMAIL_RESEND_PERIOD)) {
					// check if greater than 1
					int number = Integer.parseInt(value);
					if (number == 0) {
						validationError = "The parameter value for '" + param.getName() + "' must not be zero !";
					}

				}
				break;
			case _STRING_:
			case _LOV_:
			case _MASKED_:
			default:
				// do nothing
				break;
			}
			if (invalid) {
				break;
			}

		}
		return validationError;

	}

	/**
	 * @param processInstanceId
	 * @param varName
	 * @return
	 */
	@Override
	public Object getProcessInstanceVariable(String processInstanceId, String varName) {
		return this.activitiBpmService.getRuntimeService().getVariable(processInstanceId, varName);
	}

	/**
	 * Retrieves a process instance given the workflow instance; this process instance can be null (if Activiti process does no longer exist)
	 *
	 * @param wkfInstanceID
	 * @return
	 */
	@Override
	public ProcessInstance getProcessInstanceByWorkflowInstance(Integer wkfInstanceID) {
		return this.activitiBpmService.getRuntimeService().createProcessInstanceQuery().processInstanceBusinessKey(wkfInstanceID + "").singleResult();
	}

	/**
	 * Check if the workflow can be completed by the logged in user.
	 *
	 * @param wkfInstanceID
	 * @return
	 */
	@Override
	public boolean canComplete(Integer wkfInstanceID) {

		String username = Session.user.get().getLoginName();

		User user = this.userService.findByLogin(username);
		List<String> rolesCodes = new ArrayList<>();
		List<Role> roles = this.roleService.findByUsers(user);
		for (Role role : roles) {
			rolesCodes.add(role.getCode());
		}

		Task t = this.activitiBpmService.getTaskService().createTaskQuery().processInstanceBusinessKey(wkfInstanceID.toString()).singleResult();
		List<IdentityLink> list = this.activitiBpmService.getTaskService().getIdentityLinksForTask(t.getId());
		for (IdentityLink e : list) {
			if (username.equals(e.getUserId()) || rolesCodes.contains(e.getGroupId().toUpperCase())) {
				return true;
			}
		}
		return false;
	}

	private IWorkflowTerminatorService getWorkflowTerminator(Workflow workflow) {
		String workflowName = workflow.getName();
		return (IWorkflowTerminatorService) this.getApplicationContext().getBean(WorkflowNames.getByName(workflowName).getWorkflowTerminatorName());
	}

}
