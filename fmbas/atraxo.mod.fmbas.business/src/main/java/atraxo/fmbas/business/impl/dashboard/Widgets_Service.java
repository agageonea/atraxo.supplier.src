/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.dashboard;

import atraxo.fmbas.business.api.dashboard.IWidgetsService;
import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;
import atraxo.fmbas.domain.impl.dashboard.WidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.Widgets;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Widgets} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Widgets_Service extends AbstractEntityService<Widgets>
		implements
			IWidgetsService {

	/**
	 * Public constructor for Widgets_Service
	 */
	public Widgets_Service() {
		super();
	}

	/**
	 * Public constructor for Widgets_Service
	 * 
	 * @param em
	 */
	public Widgets_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Widgets> getEntityClass() {
		return Widgets.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Widgets
	 */
	public Widgets findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Widgets.NQ_FIND_BY_CODE, Widgets.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Widgets", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Widgets", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Widgets
	 */
	public Widgets findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Widgets.NQ_FIND_BY_BUSINESS,
							Widgets.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Widgets", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Widgets", "id"), nure);
		}
	}

	/**
	 * Find by reference: widgetParams
	 *
	 * @param widgetParams
	 * @return List<Widgets>
	 */
	public List<Widgets> findByWidgetParams(WidgetParameters widgetParams) {
		return this.findByWidgetParamsId(widgetParams.getId());
	}
	/**
	 * Find by ID of reference: widgetParams.id
	 *
	 * @param widgetParamsId
	 * @return List<Widgets>
	 */
	public List<Widgets> findByWidgetParamsId(Integer widgetParamsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Widgets e, IN (e.widgetParams) c where e.clientId = :clientId and c.id = :widgetParamsId",
						Widgets.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("widgetParamsId", widgetParamsId).getResultList();
	}
	/**
	 * Find by reference: dashboardWidgets
	 *
	 * @param dashboardWidgets
	 * @return List<Widgets>
	 */
	public List<Widgets> findByDashboardWidgets(
			DashboardWidgets dashboardWidgets) {
		return this.findByDashboardWidgetsId(dashboardWidgets.getId());
	}
	/**
	 * Find by ID of reference: dashboardWidgets.id
	 *
	 * @param dashboardWidgetsId
	 * @return List<Widgets>
	 */
	public List<Widgets> findByDashboardWidgetsId(Integer dashboardWidgetsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Widgets e, IN (e.dashboardWidgets) c where e.clientId = :clientId and c.id = :dashboardWidgetsId",
						Widgets.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("dashboardWidgetsId", dashboardWidgetsId)
				.getResultList();
	}
}
