/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.dashboard;

import atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;
import atraxo.fmbas.domain.impl.dashboard.WidgetParameters;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link AssignedWidgetParameters} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class AssignedWidgetParameters_Service
		extends
			AbstractEntityService<AssignedWidgetParameters> {

	/**
	 * Public constructor for AssignedWidgetParameters_Service
	 */
	public AssignedWidgetParameters_Service() {
		super();
	}

	/**
	 * Public constructor for AssignedWidgetParameters_Service
	 * 
	 * @param em
	 */
	public AssignedWidgetParameters_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<AssignedWidgetParameters> getEntityClass() {
		return AssignedWidgetParameters.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return AssignedWidgetParameters
	 */
	public AssignedWidgetParameters findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							AssignedWidgetParameters.NQ_FIND_BY_BUSINESS,
							AssignedWidgetParameters.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"AssignedWidgetParameters", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"AssignedWidgetParameters", "id"), nure);
		}
	}

	/**
	 * Find by reference: dashboardWidgets
	 *
	 * @param dashboardWidgets
	 * @return List<AssignedWidgetParameters>
	 */
	public List<AssignedWidgetParameters> findByDashboardWidgets(
			DashboardWidgets dashboardWidgets) {
		return this.findByDashboardWidgetsId(dashboardWidgets.getId());
	}
	/**
	 * Find by ID of reference: dashboardWidgets.id
	 *
	 * @param dashboardWidgetsId
	 * @return List<AssignedWidgetParameters>
	 */
	public List<AssignedWidgetParameters> findByDashboardWidgetsId(
			Integer dashboardWidgetsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from AssignedWidgetParameters e where e.clientId = :clientId and e.dashboardWidgets.id = :dashboardWidgetsId",
						AssignedWidgetParameters.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("dashboardWidgetsId", dashboardWidgetsId)
				.getResultList();
	}
	/**
	 * Find by reference: widgetParameters
	 *
	 * @param widgetParameters
	 * @return List<AssignedWidgetParameters>
	 */
	public List<AssignedWidgetParameters> findByWidgetParameters(
			WidgetParameters widgetParameters) {
		return this.findByWidgetParametersId(widgetParameters.getId());
	}
	/**
	 * Find by ID of reference: widgetParameters.id
	 *
	 * @param widgetParametersId
	 * @return List<AssignedWidgetParameters>
	 */
	public List<AssignedWidgetParameters> findByWidgetParametersId(
			Integer widgetParametersId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from AssignedWidgetParameters e where e.clientId = :clientId and e.widgetParameters.id = :widgetParametersId",
						AssignedWidgetParameters.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("widgetParametersId", widgetParametersId)
				.getResultList();
	}
}
