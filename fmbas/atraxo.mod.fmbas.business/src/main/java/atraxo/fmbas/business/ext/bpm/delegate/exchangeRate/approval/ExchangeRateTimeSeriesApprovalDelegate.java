package atraxo.fmbas.business.ext.bpm.delegate.exchangeRate.approval;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.ApprovalDelegate;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailAddressesDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.ExchangeRateTimeSerieApprovalDto;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.user.UserSupp;

/**
 * Delegate class used for the approval process of an Exchange Rate Time Series
 *
 * @author aradu
 */
public class ExchangeRateTimeSeriesApprovalDelegate extends ApprovalDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExchangeRateTimeSeriesApprovalDelegate.class);

	@Autowired
	private ITimeSerieService timeSerieService;

	@Override
	public void execute() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}

		this.setHasUsersToApproveVariable(true);

		List<UserSupp> users = this.extractApprovers(WorkflowVariablesConstants.APPROVER_ROLE);
		if (!CollectionUtils.isEmpty(users)) {
			TimeSerie exchangeRateTimeSerie = this.extractExchangeRate();
			if (exchangeRateTimeSerie != null) {
				EmailDto emailDTO = this.extractEmailDto(exchangeRateTimeSerie, users);
				if (emailDTO != null) {
					this.execution.setVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_DTO, emailDTO);
				}
			}
		} else {
			this.setHasUsersToApproveVariable(false);
		}

		this.resetMailComponentVariables();

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
	}

	/**
	 * Method that extracts the exchange rate time series that was submitted for approval
	 *
	 * @return
	 * @throws Exception
	 */
	private TimeSerie extractExchangeRate() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}
		TimeSerie exchangeRate = null;
		Object exchangeRateIdObj = this.execution.getVariable(WorkflowVariablesConstants.TIME_SERIE_ID);
		if (exchangeRateIdObj != null) {
			String exchangeRateIdString = exchangeRateIdObj.toString();
			if (!"".equals(exchangeRateIdString)) {
				try {
					Integer exchangeRateTimeSerieId = Integer.parseInt(exchangeRateIdString);
					try {
						exchangeRate = this.timeSerieService.findByBusiness(exchangeRateTimeSerieId);
					} catch (Exception e) {
						LOGGER.error("ERROR:could not find an Exchange Rate time serie for ID " + exchangeRateTimeSerieId, e);
						this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
								String.format(BusinessErrorCode.WORKFLOW_COULD_NOT_FIND_AN_EXCHANGE_RATE_FOR_ID.getErrMsg(), exchangeRateTimeSerieId),
								e);
					}
				} catch (NumberFormatException e) {
					LOGGER.error("ERROR:could not parse the received Exchange Rate time serie ID ", exchangeRateIdObj);
					this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
							String.format(BusinessErrorCode.WORKFLOW_COULD_NOT_PARSE_THE_EXCHANGE_RATE_ID.getErrMsg(), exchangeRateIdObj), e);
				}
			} else {
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						String.format(BusinessErrorCode.WORKFLOW_EXCHANGE_RATE_TIME_SERIE_EMPTY_ID.getErrMsg(), exchangeRateIdString));
			}
		} else {
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
					BusinessErrorCode.WORKFLOW_EXCHANGE_RATE_TIME_SERIE_VARIABLE_NOT_FOUND.getErrMsg());
		}
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
		return exchangeRate;
	}

	/**
	 * Build the DTO from an exchangeRateTimeSerie
	 *
	 * @param exchangeRateTimeSerie
	 * @param users
	 * @return
	 * @throws Exception
	 */
	private EmailDto extractEmailDto(TimeSerie exchangeRateTimeSerie, List<UserSupp> users) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}
		EmailDto emailDTO = new EmailDto();
		String timeSeriesName = null;
		try {
			timeSeriesName = exchangeRateTimeSerie.getSerieName();
		} catch (Exception e) {
			LOGGER.error("ERROR:could not extract data from the Exchange Rate time serie  " + exchangeRateTimeSerie.getId()
					+ " in order to send the mail ! ", e);
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
					String.format(BusinessErrorCode.DTO_CANT_EXTRACT_DATA_FROM_ENTITY_WORKFLOW.getErrMsg(), "Exchange Rate Time Serie",
							exchangeRateTimeSerie.getId()));
			emailDTO = null;
		}

		if (emailDTO != null) {
			// set the subject
			emailDTO.setSubject("An exchange rate change for " + exchangeRateTimeSerie.getSerieName() + " is awaiting your approval.");

			// set the email content
			List<EmailAddressesDto> emailAddressesDtoList = new ArrayList<>();
			for (UserSupp user : users) {
				// extract email dto data from Exchange Rate time series and user
				ExchangeRateTimeSerieApprovalDto dto = new ExchangeRateTimeSerieApprovalDto();
				dto.setApprovalRequestNote(this.getVariableFromWorkflow(WorkflowVariablesConstants.APPROVE_NOTE));
				dto.setFullNameOfRequester(this.getVariableFromWorkflow(WorkflowVariablesConstants.FULLNAME_REQUESTER_VAR));
				dto.setTimeSeries(timeSeriesName);
				dto.setFullName(user.getFirstName() + " " + user.getLastName());
				dto.setTitle(user.getTitle().getName());

				EmailAddressesDto emailAddressesDto = new EmailAddressesDto();
				emailAddressesDto.setAddress(user.getEmail());
				emailAddressesDto.setData(dto);
				emailAddressesDtoList.add(emailAddressesDto);
			}
			emailDTO.setTo(emailAddressesDtoList);
		}
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
		return emailDTO;
	}

	@Override
	public String getSessionUserCode() {
		return null;
	}

	/**
	 * @param required
	 */
	private void setHasUsersToApproveVariable(boolean required) {
		this.execution.setVariable(WorkflowVariablesConstants.HAS_USERS_TO_APPROVE, required);
	}

}
