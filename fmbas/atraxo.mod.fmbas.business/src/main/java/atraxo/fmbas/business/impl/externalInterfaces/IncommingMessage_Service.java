/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.externalInterfaces;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.IncommingMessage;
import atraxo.fmbas.domain.impl.fmbas_type.IncomingMessageStatus;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link IncommingMessage} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class IncommingMessage_Service
		extends
			AbstractEntityService<IncommingMessage> {

	/**
	 * Public constructor for IncommingMessage_Service
	 */
	public IncommingMessage_Service() {
		super();
	}

	/**
	 * Public constructor for IncommingMessage_Service
	 * 
	 * @param em
	 */
	public IncommingMessage_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<IncommingMessage> getEntityClass() {
		return IncommingMessage.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return IncommingMessage
	 */
	public IncommingMessage findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(IncommingMessage.NQ_FIND_BY_BUSINESS,
							IncommingMessage.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"IncommingMessage", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"IncommingMessage", "id"), nure);
		}
	}

	/**
	 * Find by index key
	 *
	 * @return List<IncommingMessage>
	 */
	public List<IncommingMessage> findByIdxStatus_interace_id_clientid_created_at(
			IncomingMessageStatus status, ExternalInterface externalInterface,
			Date createdAt) {
		return this
				.getEntityManager()
				.createNamedQuery(
						IncommingMessage.NQ_IDX_FIND_BY_STATUS_INTERACE_ID_CLIENTID_CREATED_AT,
						IncommingMessage.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("status", status)
				.setParameter("externalInterface", externalInterface)
				.setParameter("createdAt", createdAt).getResultList();
	}
	/**
	 * Find by index key
	 *
	 * @return List<IncommingMessage>
	 */
	public List<IncommingMessage> findByIdxStatus_interace_id_clientid_created_at(
			IncomingMessageStatus status, Long externalInterfaceId,
			Date createdAt) {
		return this
				.getEntityManager()
				.createNamedQuery(
						IncommingMessage.NQ_IDX_FIND_BY_STATUS_INTERACE_ID_CLIENTID_CREATED_AT_PRIMITIVE,
						IncommingMessage.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("status", status)
				.setParameter("externalInterfaceId", externalInterfaceId)
				.setParameter("createdAt", createdAt).getResultList();
	}

	/**
	 * Find by reference: externalInterface
	 *
	 * @param externalInterface
	 * @return List<IncommingMessage>
	 */
	public List<IncommingMessage> findByExternalInterface(
			ExternalInterface externalInterface) {
		return this.findByExternalInterfaceId(externalInterface.getId());
	}
	/**
	 * Find by ID of reference: externalInterface.id
	 *
	 * @param externalInterfaceId
	 * @return List<IncommingMessage>
	 */
	public List<IncommingMessage> findByExternalInterfaceId(
			Integer externalInterfaceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from IncommingMessage e where e.clientId = :clientId and e.externalInterface.id = :externalInterfaceId",
						IncommingMessage.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("externalInterfaceId", externalInterfaceId)
				.getResultList();
	}
}
