package atraxo.fmbas.business.ext.timeserie.job.publishTimeSeries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import atraxo.fmbas.business.api.exchangerate.IExchangeRateService;
import atraxo.fmbas.business.api.quotation.IQuotationService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;

@Component
public class PublishTimeSerieProcessor implements ItemProcessor<TimeSerie, TimeSerie> {

	private static final Logger LOG = LoggerFactory.getLogger(PublishTimeSerieReader.class);
	private static final String QUOTATION = "quotation";
	private static final String EXCHANGE_RATE = "exchangeRate";

	@Autowired
	private ITimeSerieService timeSerieService;
	@Autowired
	private IQuotationService quotationService;
	@Autowired
	private IExchangeRateService exchangeRateService;

	private ExecutionContext executionContext;

	@BeforeStep
	public void storeExecutionContext(StepExecution stepExecution) {
		this.executionContext = stepExecution.getExecutionContext();
	}

	@Override
	public TimeSerie process(TimeSerie timeSeries) throws Exception {
		LOG.info("Start processing time serie!");
		this.timeSerieService.publish(Arrays.asList(timeSeries));

		this.setExchangeRateContext(timeSeries);
		this.setQuotationContext(timeSeries);
		return timeSeries;
	}

	private void setExchangeRateContext(TimeSerie timeSeries) {
		List<ExchangeRate> exchangeRates = new ArrayList<>();
		List<String> erNames = new ArrayList<>();
		exchangeRates.addAll(this.exchangeRateService.findByTserie(timeSeries));
		for (ExchangeRate er : exchangeRates) {
			erNames.add(er.getName());
		}
		@SuppressWarnings("unchecked")
		List<String> erNamesContext = (List<String>) this.executionContext.get(EXCHANGE_RATE);
		if (erNamesContext == null) {
			erNamesContext = erNames;
		} else {
			erNamesContext.addAll(erNames);
		}
		this.executionContext.put(EXCHANGE_RATE, erNamesContext);
	}

	private void setQuotationContext(TimeSerie timeSeries) {

		List<Quotation> quotations = this.quotationService.findByTimeseries(timeSeries);
		List<String> quotationNames = new ArrayList<>();
		for (Quotation q : quotations) {
			quotationNames.add(q.getName());
		}
		@SuppressWarnings("unchecked")
		List<String> quotationsContext = (List<String>) this.executionContext.get(QUOTATION);
		if (quotationsContext == null) {
			quotationsContext = quotationNames;
		} else {
			quotationsContext.addAll(quotationNames);
		}
		this.executionContext.put(QUOTATION, quotationsContext);
	}
}
