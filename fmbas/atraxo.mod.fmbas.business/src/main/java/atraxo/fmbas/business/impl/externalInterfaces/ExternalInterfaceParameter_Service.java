/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.externalInterfaces;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceParameterService;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceParameter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ExternalInterfaceParameter} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ExternalInterfaceParameter_Service
		extends
			AbstractEntityService<ExternalInterfaceParameter>
		implements
			IExternalInterfaceParameterService {

	/**
	 * Public constructor for ExternalInterfaceParameter_Service
	 */
	public ExternalInterfaceParameter_Service() {
		super();
	}

	/**
	 * Public constructor for ExternalInterfaceParameter_Service
	 * 
	 * @param em
	 */
	public ExternalInterfaceParameter_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ExternalInterfaceParameter> getEntityClass() {
		return ExternalInterfaceParameter.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ExternalInterfaceParameter
	 */
	public ExternalInterfaceParameter findByKey(
			ExternalInterface externalInterface, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							ExternalInterfaceParameter.NQ_FIND_BY_KEY,
							ExternalInterfaceParameter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("externalInterface", externalInterface)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ExternalInterfaceParameter",
							"externalInterface, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ExternalInterfaceParameter",
							"externalInterface, name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return ExternalInterfaceParameter
	 */
	public ExternalInterfaceParameter findByKey(Long externalInterfaceId,
			String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							ExternalInterfaceParameter.NQ_FIND_BY_KEY_PRIMITIVE,
							ExternalInterfaceParameter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("externalInterfaceId", externalInterfaceId)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ExternalInterfaceParameter",
							"externalInterfaceId, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ExternalInterfaceParameter",
							"externalInterfaceId, name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return ExternalInterfaceParameter
	 */
	public ExternalInterfaceParameter findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							ExternalInterfaceParameter.NQ_FIND_BY_BUSINESS,
							ExternalInterfaceParameter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ExternalInterfaceParameter", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ExternalInterfaceParameter", "id"), nure);
		}
	}

	/**
	 * Find by reference: externalInterface
	 *
	 * @param externalInterface
	 * @return List<ExternalInterfaceParameter>
	 */
	public List<ExternalInterfaceParameter> findByExternalInterface(
			ExternalInterface externalInterface) {
		return this.findByExternalInterfaceId(externalInterface.getId());
	}
	/**
	 * Find by ID of reference: externalInterface.id
	 *
	 * @param externalInterfaceId
	 * @return List<ExternalInterfaceParameter>
	 */
	public List<ExternalInterfaceParameter> findByExternalInterfaceId(
			Integer externalInterfaceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ExternalInterfaceParameter e where e.clientId = :clientId and e.externalInterface.id = :externalInterfaceId",
						ExternalInterfaceParameter.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("externalInterfaceId", externalInterfaceId)
				.getResultList();
	}
}
