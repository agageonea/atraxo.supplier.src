/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.history;

import atraxo.fmbas.domain.impl.history.ChangeHistory;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ChangeHistory} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ChangeHistory_Service extends AbstractEntityService<ChangeHistory> {

	/**
	 * Public constructor for ChangeHistory_Service
	 */
	public ChangeHistory_Service() {
		super();
	}

	/**
	 * Public constructor for ChangeHistory_Service
	 * 
	 * @param em
	 */
	public ChangeHistory_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ChangeHistory> getEntityClass() {
		return ChangeHistory.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ChangeHistory
	 */
	public ChangeHistory findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ChangeHistory.NQ_FIND_BY_BUSINESS,
							ChangeHistory.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ChangeHistory", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ChangeHistory", "id"), nure);
		}
	}

}
