package atraxo.fmbas.business.ext.externalInterfaces.processor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import javax.xml.bind.JAXBException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextStoppedEvent;

import atraxo.fmbas.business.api.externalInterfaces.IIncommingMessageService;
import atraxo.fmbas.business.ext.exceptions.NoEntityException;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.IncommingMessage;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.Session;
import seava.j4e.commons.utils.XMLUtils;

/**
 * Runnable to process buffered incoming messages.
 *
 * @author zspeter
 */
public class BufferedMessageProcessor implements Runnable, ApplicationListener<ContextStoppedEvent> {

	private static final Logger LOG = LoggerFactory.getLogger(BufferedMessageProcessor.class);

	private IIncommingMessageService msgSrv;

	private ApplicationContext ctx;
	private ExternalInterface extInt;
	private ApplicationEventPublisher publisher;
	private IUser user;

	private volatile boolean stop = false;

	public BufferedMessageProcessor(IIncommingMessageService msgSrv, ApplicationContext ctx, ApplicationEventPublisher publisher,
			ExternalInterface extInt) {
		this.msgSrv = msgSrv;
		this.ctx = ctx;
		this.publisher = publisher;
		this.extInt = extInt;
		this.user = Session.user.get();
	}

	@Override
	public void run() {
		LOG.info("Start thread to process messages received on interface {}.", this.extInt.getName());
		Session.user.set(this.user);
		while (!this.stop) {
			try {
				IncommingMessage msg = this.getNextMessage();
				this.process(msg);
				this.delete(msg);
			} catch (NoEntityException e) {
				if (LOG.isDebugEnabled()) {
					LOG.debug(e.getMessage(), e);
				}
				LOG.info("No message found for {}. Will check for the next entity in half an hour.", this.extInt.getName());
				this.requestStop();
			} catch (BusinessException e) {
				LOG.error(e.getMessage(), e);
				LOG.info("Business exception occured on {} interface . Will check for the next entity in half an hour.", this.extInt.getName());
				this.requestStop();
			}
		}
		this.publisher.publishEvent(new EndProcessorEvent(this.ctx, this.extInt, Session.user.get().getClientCode()));
	}

	public void requestStop() {
		this.stop = true;
	}

	private void delete(IncommingMessage msg) throws BusinessException {
		this.msgSrv.deleteByIds(Arrays.asList(msg.getId()));

	}

	private void process(IncommingMessage msg) {
		Object obj = this.ctx.getBean(msg.getBeanName());
		Method m;
		try {
			m = obj.getClass().getMethod(msg.getMethodName(), MSG.class);
			m.invoke(obj, XMLUtils.toObject(msg.getMessage(), MSG.class, true));
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | JAXBException | XMLStreamException
				| FactoryConfigurationError e) {
			LOG.error("Stupid programmer. " + e.getMessage(), e);
			LOG.error("Message: {}", msg.getMessage());
		} catch (InvocationTargetException e) {
			LOG.error(e.getMessage(), e);
			LOG.error("Message: {}", msg.getMessage());

		}

	}

	private IncommingMessage getNextMessage() throws BusinessException {
		return this.msgSrv.getNextMessage(this.extInt);
	}

	@Override
	public void onApplicationEvent(ContextStoppedEvent event) {
		this.requestStop();
	}

}
