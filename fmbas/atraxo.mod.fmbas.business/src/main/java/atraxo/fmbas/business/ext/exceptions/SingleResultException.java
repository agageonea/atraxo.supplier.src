package atraxo.fmbas.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.IErrorCode;

/**
 * Exception used when cannot determine uniquely an entity that should be unique
 * in the system.
 *
 * @author zspeter
 *
 */
public class SingleResultException extends BusinessException {

	private static final long serialVersionUID = 2286561716092118398L;

	public SingleResultException(IErrorCode errorCode, String errorMessage) {
		super(errorCode, errorMessage);
	}

	public SingleResultException(IErrorCode errorCode, String errorMessage, Throwable throwable) {
		super(errorCode, errorMessage, throwable);
	}

	public SingleResultException(String message, Throwable throwable) {
		super(BusinessErrorCode.NO_UNIQUE_ENITY, message, throwable);
	}

	public SingleResultException(String errorMessage) {
		super(BusinessErrorCode.NO_UNIQUE_ENITY, errorMessage);
	}

}
