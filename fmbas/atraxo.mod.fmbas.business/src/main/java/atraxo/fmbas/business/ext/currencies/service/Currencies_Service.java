/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.currencies.service;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.ext.types.FmbasSysParam;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link Currencies} domain entity.
 */
public class Currencies_Service extends atraxo.fmbas.business.impl.currencies.Currencies_Service implements ICurrenciesService {

	@Override
	public Currencies getSystemCurrency() throws BusinessException {

		String sql = "select e from " + Currencies.class.getSimpleName() + " e, " + SystemParameter.class.getSimpleName()
				+ " sys where e.clientId=:clientId and e.clientId = sys.clientId and e.code = sys.value and sys.code = :code";
		try {
			return this.getEntityManager().createQuery(sql, Currencies.class).setParameter("clientId", Session.user.get().getClient().getId())
					.setParameter("code", FmbasSysParam.SYSCRNCY.name()).getSingleResult();
		} catch (NoResultException | NonUniqueResultException nre) {
			throw new BusinessException(BusinessErrorCode.NO_SYS_CRNCY_DEFFINED, BusinessErrorCode.NO_SYS_CRNCY_DEFFINED.getErrMsg(), nre);
		}
	}
}
