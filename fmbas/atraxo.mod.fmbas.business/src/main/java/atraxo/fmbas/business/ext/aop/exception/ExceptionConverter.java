package atraxo.fmbas.business.ext.aop.exception;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
public @interface ExceptionConverter {

	/**
	 * Target exception.
	 */
	Class<? extends BusinessException> target();

}
