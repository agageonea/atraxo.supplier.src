package atraxo.fmbas.business.ext.tolerance;

import java.math.BigDecimal;

public class EffectiveLimits {

	private BigDecimal min;
	private BigDecimal max;

	public BigDecimal getMin() {
		return this.min;
	}

	public void setMin(BigDecimal min) {
		this.min = min;
	}

	public BigDecimal getMax() {
		return this.max;
	}

	public void setMax(BigDecimal max) {
		this.max = max;
	}

}
