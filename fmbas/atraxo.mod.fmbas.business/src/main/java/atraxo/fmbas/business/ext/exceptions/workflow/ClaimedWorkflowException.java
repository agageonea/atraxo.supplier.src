package atraxo.fmbas.business.ext.exceptions.workflow;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import seava.j4e.api.exceptions.BusinessException;

public class ClaimedWorkflowException extends BusinessException {


	public ClaimedWorkflowException(Object... params) {
		super(BusinessErrorCode.WORKFLOW_CLAIMED, String.format(BusinessErrorCode.WORKFLOW_CLAIMED.getErrMsg(), params));
	}
}
