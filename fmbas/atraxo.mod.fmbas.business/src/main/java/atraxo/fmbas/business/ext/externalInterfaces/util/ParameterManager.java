package atraxo.fmbas.business.ext.externalInterfaces.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.util.StringUtils;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceParameter;

/**
 * Utility class to manage external interface parameters;
 *
 * @author zspeter
 */
public class ParameterManager {
	private Map<String, String> paramMap;

	/**
	 * @param list
	 */
	public ParameterManager(Collection<ExternalInterfaceParameter> list) {
		this.paramMap = new HashMap<>();
		for (ExternalInterfaceParameter param : list) {
			this.paramMap.put(param.getName(), StringUtils.isEmpty(param.getCurrentValue()) ? param.getDefaultValue() : param.getCurrentValue());
		}
	}

	/**
	 * @param paramName
	 * @return
	 */
	public String getParamValue(String paramName) {
		if (this.paramMap.containsKey(paramName)) {
			String value = this.paramMap.get(paramName);
			value = (value == null) ? "" : value;// make sure to send empty string instead of null
			return this.paramMap.get(paramName);
		}
		throw new IllegalArgumentException("Param with name " + paramName + " not found!");
	}

}
