package atraxo.fmbas.business.ext.bpm;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceService;
import atraxo.fmbas.business.api.workflow.IWorkflowNotificationService;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowInstanceStatus;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import seava.j4e.api.ISettings;
import seava.j4e.api.enums.SysParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.InvalidConfiguration;
import seava.j4e.api.session.Session;

/**
 * This class has the role of handling different errors that can appear when executing workflows. It has the responsability of closing all the loose
 * ends if any Exception ocured (for example updating the <code>WorkflowInstance</code> accordingly).
 *
 * @author vhojda
 */
public class WorkflowBpmErrorHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowBpmErrorHandler.class);

	private static final String SYS_USER = "SYS USER";

	@Autowired
	private IWorkflowInstanceService workflowInstanceService;

	@Autowired
	private IWorkflowNotificationService workflowNotificationService;

	@Autowired
	private IUserSuppService userSrv;

	@Autowired
	private IWorkflowBpmManager workflowBpmManager;

	@Autowired
	private ISettings settings;

	/**
	 * @param execution
	 * @param errorCode
	 * @param errorDescription
	 * @param e
	 * @throws InvalidConfiguration
	 * @throws BusinessException
	 * @throws Exception
	 */
	public void handleError(DelegateExecution execution, String errorCode, String errorDescription, Exception e) {
		execution.setVariable(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_RELEASE, errorCode);

		// propagate the error in the workflow
		String description = (e == null) ? errorDescription : (errorDescription + " : " + e.getLocalizedMessage());

		String clientCode = Session.user.get().getClient().getCode();
		String userCode = SYS_USER;
		try {
			userCode = this.settings.getParam(SysParam.CORE_WORKFLOW_USER.name());
		} catch (InvalidConfiguration e1) {
			LOGGER.warn("Could not retrieve system parameter forcore workflow user, will use the default one " + userCode, e1);
		}

		Boolean autoTerminate = Boolean
				.valueOf(execution.getVariable(WorkflowVariablesConstants.VAR_AUTOMATIC_TERMINATE_FAILED_WORKFLOWS).toString());

		// treat error in different Thread
		ErrorBpmHandlerThread thread = new ErrorBpmHandlerThread(
				String.valueOf(execution.getVariable(WorkflowVariablesConstants.VAR_WKF_INSTANCE_ID)), autoTerminate, this.workflowInstanceService,
				description, clientCode, userCode, this.userSrv, this.workflowBpmManager);
		thread.start();

		if (!autoTerminate) {
			this.sendFailureNotification(execution);
		}
	}

	/**
	 * Send a failure notification
	 *
	 * @param execution
	 */
	private void sendFailureNotification(DelegateExecution execution) {
		try {
			WorkflowInstance wkfInstance = this.workflowInstanceService.findByBusiness(Integer.parseInt(execution.getProcessBusinessKey()));
			this.workflowNotificationService.sendNotification(wkfInstance, WorkflowInstanceStatus._FAILURE_,
					WorkflowMsgConstants.WKF_RESULT_REJECTED);
		} catch (Exception e) {
			LOGGER.error("ERROR:could not send failure noitification !", e);
		}
	}
}

class ErrorBpmHandlerThread extends Thread {

	private static final Logger LOGGER = LoggerFactory.getLogger(ErrorBpmHandlerThread.class);

	private static final String AUTOMATIC_TERMINATION_COMMENT = "Automatic termination";

	private boolean automaticTermination;
	private String processBussinessKey;
	private String errorDescription;
	private String clientCode;
	private String userCode;
	private IWorkflowInstanceService workflowInstanceService;
	private IUserSuppService userSrv;
	private IWorkflowBpmManager workflowBpmManager;

	/**
	 * @param processBussinessKey
	 * @param automaticTermination
	 * @param workflowInstanceService
	 * @param errorDescription
	 * @param clientCode
	 * @param userCode
	 * @param userSrv
	 * @param workflowBpmManager2
	 */
	public ErrorBpmHandlerThread(String processBussinessKey, boolean automaticTermination, IWorkflowInstanceService workflowInstanceService,
			String errorDescription, String clientCode, String userCode, IUserSuppService userSrv, IWorkflowBpmManager workflowBpmManager2) {
		super();
		this.processBussinessKey = processBussinessKey;
		this.automaticTermination = automaticTermination;
		this.workflowInstanceService = workflowInstanceService;
		this.errorDescription = errorDescription;
		this.clientCode = clientCode;
		this.userCode = userCode;
		this.userSrv = userSrv;
		this.workflowBpmManager = workflowBpmManager2;
	}

	@Override
	public void run() {

		try {
			// first create the session
			this.userSrv.createSessionUser(this.clientCode, this.userCode, true);

			// update the process instance
			WorkflowInstance wkfInstance = this.workflowInstanceService.findByBusiness(Integer.parseInt(this.processBussinessKey));

			if (wkfInstance != null) {
				// determine if automatic terminate workflows is the case here
				if (this.automaticTermination) {
					// this is needed so that Activiti completes different operations (adding history entries) before deleting the ProcessInstance (by
					// terminating the Workflow)
					Thread.sleep(2000);

					// terminate workflow
					this.workflowBpmManager.terminateWorkflow(wkfInstance.getId(),
							AUTOMATIC_TERMINATION_COMMENT + " [" + this.errorDescription + "]");
				} else {
					// if the workflow should not be terminated then update it as a failure
					wkfInstance.setStatus(WorkflowInstanceStatus._FAILURE_);
					wkfInstance.setResult(this.errorDescription);
					this.workflowInstanceService.update(wkfInstance);
				}

			}
		} catch (NumberFormatException | BusinessException | InterruptedException e) {
			LOGGER.error("ERROR:Could not run the Error Handler Thread !", e);
		}

	}

}
