/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.history.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.history.ChangeHistory;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link ChangeHistory} domain entity.
 */
public class ChangeHistory_Service extends atraxo.fmbas.business.impl.history.ChangeHistory_Service implements IChangeHistoryService {

	private static final String OBJECT_ID = "objectId";
	private static final String OBJECT_TYPE = "objectType";

	@Override
	public void deleteHistory(Integer id, Class<?> c) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put(OBJECT_ID, id);
		params.put(OBJECT_TYPE, c.getSimpleName());
		List<ChangeHistory> histories = this.findEntitiesByAttributes(params);
		List<Object> ids = this.collectIds(histories);
		this.deleteByIds(ids);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void deleteHistory(List ids, Class<?> c) throws BusinessException {
		for (Object id : ids) {
			this.deleteHistory((Integer) id, c);
		}
	}

	@Override
	@Transactional
	public void saveChangeHistory(AbstractEntity obj, String action, String message) throws BusinessException {
		ChangeHistory changeHistory = new ChangeHistory();
		changeHistory.setObjectId(obj.getId());
		changeHistory.setObjectType(obj.getClass().getSimpleName());
		changeHistory.setObjectValue(action);
		changeHistory.setRemarks(message);
		this.insert(changeHistory);
	}
}
