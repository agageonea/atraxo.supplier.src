/**
 *
 */
package atraxo.fmbas.business.ext.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;

/**
 * Utility class for streams.
 *
 * @author vhojda
 */
public class StreamUtil {

	/**
	 * Hidden constructor
	 */
	private StreamUtil() {

	}

	/**
	 * @param in
	 * @param zos
	 * @param reportName
	 * @throws BusinessException
	 */
	public static void addToZip(InputStream in, ZipOutputStream zos, String reportName) throws BusinessException {
		try {
			ZipEntry zipEntry = new ZipEntry(reportName);
			zos.putNextEntry(zipEntry);

			byte[] bytes = new byte[1024];
			int length;
			while ((length = in.read(bytes)) >= 0) {
				zos.write(bytes, 0, length);
			}
			zos.closeEntry();
		} catch (IOException e) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e.getMessage(), e);
		}
	}

}
