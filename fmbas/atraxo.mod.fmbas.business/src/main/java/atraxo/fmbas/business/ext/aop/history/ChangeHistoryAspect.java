package atraxo.fmbas.business.ext.aop.history;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.util.StringUtils;

import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.history.ChangeHistory;
import seava.j4e.api.annotation.Reason;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;

/**
 * Aspect to log history changes.
 *
 * @author zspeter
 */
@Aspect
@Order(-2)
public class ChangeHistoryAspect {

	@Autowired
	private IChangeHistoryService service;

	/**
	 * @param jp
	 * @throws BusinessException
	 */
	@AfterReturning(pointcut = "@annotation(History)")
	public void logHistory(JoinPoint jp) throws BusinessException {
		Method method = this.getMethod(jp);
		Annotation[][] parameterAnnotations = method.getParameterAnnotations();
		int reasonIdx = this.getParameterIdx(parameterAnnotations, Reason.class);
		int actionIdx = this.getParameterIdx(parameterAnnotations, Action.class);

		History history = method.getAnnotation(History.class);
		String reason = "";
		String action = "";
		List<AbstractEntity> entities = new ArrayList<>();
		for (int i = 0; i < jp.getArgs().length; i++) {
			Object value = jp.getArgs()[i];

			if (i == reasonIdx) {
				reason = value.toString();
			} else if (i == actionIdx) {
				action = value.toString();
			}

			if (value instanceof AbstractEntity) {
				entities.add((AbstractEntity) value);
				reason = this.getReasonFromObject(value, reason);
			}
			reason = this.parseListArguments(entities, value, reason);
		}
		this.saveHistory(history, entities, action, reason);
	}

	private String getReasonFromObject(Object value, String reason) throws BusinessException {
		if (!StringUtils.isEmpty(reason)) {
			return reason;
		}
		Method[] declaredMethods = value.getClass().getDeclaredMethods();
		for (Method m : declaredMethods) {
			if (m.getAnnotation(Reason.class) != null) {
				try {
					return (String) m.invoke(value);
				} catch (InvocationTargetException | IllegalAccessException | IllegalArgumentException e) {
					throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
				}
			}
		}
		return "";
	}

	private String parseListArguments(List<AbstractEntity> entities, Object value, String reason) throws BusinessException {
		String msg = reason;
		if (value instanceof List<?>) {
			List<?> list = (List<?>) value;
			for (Object obj : list) {
				if (obj instanceof AbstractEntity) {
					entities.add((AbstractEntity) obj);
					msg = this.getReasonFromObject(obj, reason);
				} else {
					break;
				}
			}
		}
		return msg;
	}

	private Method getMethod(JoinPoint jp) throws BusinessException {
		MethodSignature signature = (MethodSignature) jp.getSignature();
		Method method = signature.getMethod();
		if (method.getDeclaringClass().isInterface()) {
			try {
				method = jp.getTarget().getClass().getDeclaredMethod(jp.getSignature().getName(), method.getParameterTypes());
			} catch (final SecurityException | NoSuchMethodException exception) {
				throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, exception);
			}
		}
		return method;
	}

	private int getParameterIdx(Annotation[][] parameterAnnotations, Class<?> clazz) {
		int idx = -1;
		for (int i = 0; i < parameterAnnotations.length; i++) {
			Annotation[] annotations = parameterAnnotations[i];
			for (Annotation annotation : annotations) {
				if (clazz.isInstance(annotation)) {
					idx = i;
				}
			}
		}
		return idx;
	}

	private void saveHistory(History history, List<AbstractEntity> entities, String action, String reason) throws BusinessException {
		for (AbstractEntity entity : entities) {
			ChangeHistory changeHistory = new ChangeHistory();
			changeHistory.setObjectId(entity.getId());
			changeHistory.setObjectType(history.type().getSimpleName());
			String value = action.isEmpty() ? history.value() : action;
			changeHistory.setObjectValue(value);
			changeHistory.setRemarks(reason);
			this.service.insert(changeHistory);
		}
	}
}
