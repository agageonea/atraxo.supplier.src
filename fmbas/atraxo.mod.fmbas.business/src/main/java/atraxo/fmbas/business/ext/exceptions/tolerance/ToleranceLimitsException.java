package atraxo.fmbas.business.ext.exceptions.tolerance;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;

/**
 * Exception occures on tolerance limit problems.
 *
 * @author zspeter
 */
public class ToleranceLimitsException extends ToleranceException {

	private static final long serialVersionUID = 8730546933137561078L;

	public ToleranceLimitsException() {
		super(BusinessErrorCode.INVALID_TOLERANCE_LIMITS, BusinessErrorCode.INVALID_TOLERANCE_LIMITS.getErrMsg());
	}

	public ToleranceLimitsException(String erroDetails) {
		super(BusinessErrorCode.INVALID_TOLERANCE_LIMITS, erroDetails);
	}
}
