package atraxo.fmbas.business.ws.async;

import java.io.IOException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

import atraxo.fmbas.business.ws.async.model.StatusRequest;
import atraxo.fmbas.business.ws.async.model.StatusResponse;
import seava.j4e.api.exceptions.BusinessException;

@WebService(targetNamespace = "http://async.ws.business.fmbas.atraxo/", endpointInterface = "AcknowledgeProcessEndpoint")
public interface AcknowledgeProcessStatusWS {

	@WebMethod(operationName = "checkStatus", action = "checkStatus")
	@WebResult(targetNamespace = "http://async.ws.business.fmbas.atraxo/")
	StatusResponse checkStatus(@WebParam(name = "request", targetNamespace = "http://async.ws.business.fmbas.atraxo/") StatusRequest request)
			throws JAXBException, SAXException, IOException, BusinessException;

}
