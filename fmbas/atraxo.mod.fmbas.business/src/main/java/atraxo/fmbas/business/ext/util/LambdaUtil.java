package atraxo.fmbas.business.ext.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class LambdaUtil {

	private LambdaUtil() {
		// private constructor because this class is an utility class and it doesn't need to be created objects out of it
	}

	/**
	 * Method to filter enums based on criteria <pre> {@code
	* List<BidApprovalStatus> submited = LambdaUtil.filterEnum(BidApprovalStatus.class, a -> !BidApprovalStatus._NEW_.equals(a));
	* } </pre>
	 *
	 * @param enumClass
	 * @param criteria
	 * @return List of enumClass
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> List<T> filterEnum(Class enumClass, Predicate<T> criteria) {
		List<T> filteredEnum = new ArrayList<>();

		if (enumClass.isEnum()) {
			filteredEnum = (List<T>) Arrays.asList(enumClass.getEnumConstants()).stream().filter((Predicate<? super Object>) criteria)
					.collect(Collectors.toList());
		}

		return filteredEnum;
	}
}
