/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.job.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import atraxo.ad.business.api.scheduler.IJobExecutionService;
import atraxo.fmbas.business.api.job.IJobChainService;
import atraxo.fmbas.business.api.job.ITriggerService;
import atraxo.fmbas.business.api.notification.INotificationService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.job.delegate.JobChainCleaner_Bd;
import atraxo.fmbas.domain.impl.fmbas_type.JobStatus;
import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.ActionParameter;
import atraxo.fmbas.domain.impl.job.JobChain;
import atraxo.fmbas.domain.impl.job.Trigger;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.domain.batchjob.ActionDetail;
import seava.j4e.scheduler.quartz.JobLauncherDetails;
import seava.j4e.scheduler.quartz.PersistentJobScheduler;

/**
 * Business extensions specific for {@link JobChain} domain entity.
 */
public class JobChain_Service extends atraxo.fmbas.business.impl.job.JobChain_Service implements IJobChainService {

	private static final Logger logger = LoggerFactory.getLogger(JobChain_Service.class);

	@Autowired
	private PersistentJobScheduler jobScheduler;
	@Autowired
	private INotificationService notificationSrv;
	@Autowired
	private IUserSuppService userSrv;
	@Autowired
	private ITriggerService service;

	@Autowired
	IJobExecutionService jobExecutionSrv;

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		this.getBusinessDelegate(JobChainCleaner_Bd.class).cleanChilds(ids);
	}

	@Override
	@Transactional
	public void activate(Integer id) throws BusinessException {
		JobChain jobChain = this.findById(id);
		Collection<Action> actions = jobChain.getActions();
		Collection<Trigger> triggers = jobChain.getTriggers();
		if (triggers.isEmpty() || actions.isEmpty()) {
			throw new BusinessException(BusinessErrorCode.INVALID_JOB_CHAIN, BusinessErrorCode.INVALID_JOB_CHAIN.getErrMsg());
		}
		boolean hasActiveTrigger = false;
		Date nextFireTime = null;
		for (Iterator<Trigger> iter = jobChain.getTriggers().iterator(); iter.hasNext();) {
			Trigger trigger = iter.next();
			if (trigger.getActive()) {
				Date fireTime = this.service.scheduleTrigger(trigger, this.buildJobListeners(jobChain));
				if (nextFireTime == null || fireTime.before(nextFireTime)) {
					nextFireTime = fireTime;
				}
				hasActiveTrigger = true;
			}
		}
		if (hasActiveTrigger) {
			IJobChainService jobChainService = (IJobChainService) this.findEntityService(JobChain.class);
			jobChain.setEnabled(true);
			jobChain.setNextRunTime(nextFireTime);
			jobChainService.update(jobChain);
		} else {
			throw new BusinessException(BusinessErrorCode.INVALID_JOB_CHAIN, BusinessErrorCode.INVALID_JOB_CHAIN.getErrMsg());
		}
	}

	@Override
	@Transactional
	public void deactivate(Integer id) throws BusinessException {
		JobChain jobChain = this.findById(id);
		if (jobChain.getTriggers().isEmpty() || jobChain.getActions().isEmpty()) {
			throw new BusinessException(BusinessErrorCode.INVALID_JOB_CHAIN, BusinessErrorCode.INVALID_JOB_CHAIN.getErrMsg());
		}
		for (Iterator<Trigger> iter = jobChain.getTriggers().iterator(); iter.hasNext();) {
			Trigger trigger = iter.next();
			if (trigger.getActive()) {
				this.service.unscheduleTrigger(trigger);
			}
		}
		IJobChainService jobChainService = (IJobChainService) this.findEntityService(JobChain.class);
		jobChain.setEnabled(false);
		jobChainService.update(jobChain);
	}

	@Override
	public void runNow(Integer id) throws BusinessException {
		JobChain jobChain = this.findById(id);
		if (jobChain.getTriggers().isEmpty() || jobChain.getActions().isEmpty()) {
			throw new BusinessException(BusinessErrorCode.INVALID_JOB_CHAIN, BusinessErrorCode.INVALID_JOB_CHAIN.getErrMsg());
		}
		if (!JobStatus._READY_.equals(jobChain.getStatus())) {
			throw new BusinessException(BusinessErrorCode.RUNNING_JOB_CHAIN, BusinessErrorCode.RUNNING_JOB_CHAIN.getErrMsg());
		}
		List<Action> actions = new ArrayList<>(jobChain.getActions());
		actions.sort((o1, o2) -> o1.getOrder().compareTo(o2.getOrder()));
		LinkedHashMap<String, ActionDetail> batchJobs = new LinkedHashMap<>();
		for (Action action : actions) {
			String actionName = StringUtils.isEmpty(action.getName()) ? action.getBatchJob().getName() : action.getName();
			ActionDetail actionDetail = new ActionDetail(action.getId(), actionName, id, action.getBatchJob().getBeanName());
			for (ActionParameter param : action.getActionParameters()) {
				String value;
				if (!StringUtils.isEmpty(param.getBusinessValue())) {
					value = param.getBusinessValue();
				} else if (!StringUtils.isEmpty(param.getValue())) {
					value = param.getValue();
				} else {
					value = param.getDefaultValue();
				}
				actionDetail.putToProperties(param.getName(), value);
			}
			batchJobs.put(action.getJobUID(), actionDetail);
		}
		this.jobScheduler.runNow(JobLauncherDetails.class, jobChain.getName(), jobChain.getClientId(), Session.user.get().getClientCode(), batchJobs,
				this.buildJobListeners(jobChain));
	}

	private JobListener[] buildJobListeners(JobChain jobchain) throws BusinessException {
		List<JobListener> list = new ArrayList<>();
		list.add(new JobChainListener((IJobChainService) this.findEntityService(JobChain.class)));

		if (!CollectionUtils.isEmpty(jobchain.getUsers())) {
			list.add(new NotificationListener(this.notificationSrv, this.userSrv, this, jobchain));
		}
		return list.toArray(new JobListener[list.size()]);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void delete(List<JobChain> list) throws BusinessException {
		super.delete(list);
		try {
			List<Long> jobExecutionIds = new ArrayList<>();
			for (JobChain item : list) {
				jobExecutionIds.addAll(this.jobExecutionSrv.findByJobExecutionParams(item.getId()));
			}
			if (!jobExecutionIds.isEmpty()) {
				this.jobExecutionSrv.deleteJobExecution(jobExecutionIds);
			}
		} catch (BusinessException e) {
			logger.debug(e.getMessage(), e);
		}
	}
}
