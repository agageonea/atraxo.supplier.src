package atraxo.fmbas.business.ext.exceptions;

public class ExceptionErrorGroup {

	public static final String ERROR = "Error";
	public static final String VALIDATION = "Validation";

	private ExceptionErrorGroup() {
		super();
	}

}
