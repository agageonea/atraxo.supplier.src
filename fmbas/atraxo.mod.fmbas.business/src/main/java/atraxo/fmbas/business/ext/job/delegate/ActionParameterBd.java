package atraxo.fmbas.business.ext.job.delegate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.ActionParameter;
import atraxo.fmbas.domain.impl.job.BatchJob;
import atraxo.fmbas.domain.impl.job.BatchJobParameter;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

/**
 * @author zspeter
 */
public class ActionParameterBd extends AbstractBusinessDelegate {

	/**
	 * Add action batch job parameters to the action.
	 *
	 * @param e
	 * @throws BusinessException
	 */
	public void addActionParameters(Action e) throws BusinessException {
		List<ActionParameter> parameters = new ArrayList<>();
		if (e.getActionParameters() != null) {
			parameters.addAll(e.getActionParameters());
		}
		if (!parameters.isEmpty()) {
			BatchJob batchJob = parameters.get(0).getBatchJobParameter().getBatchJob();
			if (batchJob.getBeanName().equalsIgnoreCase(e.getBatchJob().getBeanName())) {
				return;
			}
			e.setActionParameters(null);
		}
		Collection<BatchJobParameter> jobParameters = e.getBatchJob().getJobParameters();
		for (Iterator<BatchJobParameter> iterator = jobParameters.iterator(); iterator.hasNext();) {
			BatchJobParameter batchJobParameter = iterator.next();
			ActionParameter parameter = this.buildActionParameter(batchJobParameter);
			parameter.setJobAction(e);
			e.addToActionParameters(parameter);

		}
	}

	private ActionParameter buildActionParameter(BatchJobParameter batchJobParameter) {
		ActionParameter parameter = new ActionParameter();
		parameter.setDefaultValue(batchJobParameter.getValue());
		if (batchJobParameter.getRefBusinessValues() != null) {
			String[] refBusinessValues = batchJobParameter.getRefBusinessValues().split(",");
			parameter.setBusinessValue(refBusinessValues[0]);
		}
		parameter.setDescription(batchJobParameter.getDescription());
		parameter.setName(batchJobParameter.getName());
		parameter.setType(batchJobParameter.getType());
		parameter.setAssignType(batchJobParameter.getAssignType());
		parameter.setRefBusinessValues(batchJobParameter.getRefBusinessValues());
		parameter.setBatchJobParameter(batchJobParameter);
		parameter.setRefValues(batchJobParameter.getRefValues());
		return parameter;
	}

}
