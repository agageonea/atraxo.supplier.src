package atraxo.fmbas.business.ws;

import org.apache.commons.lang.RandomStringUtils;

import seava.j4e.api.session.Session;

/**
 * Generator for unique Message IDs
 *
 * @author vhojda
 */
public class GUIDMessageGenerator {

	private static final String PREFIX = "GUID";
	private static final String SEPARATOR = "-";
	private static final int ROOT_COUNT = 20;

	private static boolean USE_PREFIX = false;

	/**
	 * @return
	 */
	public static String generateMessageID() {
		StringBuilder sb = new StringBuilder();
		if (USE_PREFIX) {
			sb.append(PREFIX);
			sb.append(SEPARATOR);
		}
		sb.append(Session.user.get().getClient().getCode());
		sb.append(SEPARATOR);
		sb.append(System.currentTimeMillis());
		sb.append(SEPARATOR);
		sb.append(RandomStringUtils.randomAlphanumeric(ROOT_COUNT));
		return sb.toString();
	}

	/**
	 * @param message
	 * @return
	 */
	public static String extractClientIdentifier(String message) {
		String id = null;
		int firstSeparatorIndex = USE_PREFIX ? message.indexOf(SEPARATOR) + 1 : 0;
		int secondSeparatorIndex = message.indexOf(SEPARATOR, firstSeparatorIndex);
		id = message.substring(firstSeparatorIndex, secondSeparatorIndex != -1 ? secondSeparatorIndex : message.length());
		return id;
	}
}
