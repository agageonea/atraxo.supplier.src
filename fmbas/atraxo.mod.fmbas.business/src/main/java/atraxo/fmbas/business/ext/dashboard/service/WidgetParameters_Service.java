/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.dashboard.service;

import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.dashboard.IWidgetParametersService;
import atraxo.fmbas.domain.impl.dashboard.WidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.Widgets;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link WidgetParameters} domain entity.
 */
public class WidgetParameters_Service extends atraxo.fmbas.business.impl.dashboard.WidgetParameters_Service implements IWidgetParametersService {

	private static final String THIS_YEAR = "This Year";
	private static final String PERIOD = "PERIOD";

	@Override
	public String getPeriod(Widgets widget) throws BusinessException {
		if (CollectionUtils.isEmpty(widget.getWidgetParams())) {
			return THIS_YEAR;
		}
		for (WidgetParameters wp : widget.getWidgetParams()) {
			if (PERIOD.equalsIgnoreCase(wp.getName())) {
				return wp.getValue() == null ? wp.getDefaultValue() : wp.getValue();
			}
		}
		return THIS_YEAR;
	}

	@Override
	public String getUnit(Widgets widget) throws BusinessException {
		if (CollectionUtils.isEmpty(widget.getWidgetParams())) {
			return "UG";
		}
		for (WidgetParameters wp : widget.getWidgetParams()) {
			if ("UNIT OF MEASURE".equalsIgnoreCase(wp.getName())) {
				return wp.getValue() == null ? wp.getDefaultValue() : wp.getValue();
			}
		}
		return "UG";
	}
}
