/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.financialSources.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import atraxo.fmbas.business.api.financialSources.IFinancialSourcesService;
import atraxo.fmbas.business.ext.exceptions.SingleResultException;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link FinancialSources} domain entity.
 */
public class FinancialSources_Service extends atraxo.fmbas.business.impl.financialSources.FinancialSources_Service
		implements IFinancialSourcesService {

	/**
	 * Return standard financial source if it can be uniquely determined, otherwise throws exception.
	 *
	 * @return
	 */
	@Override
	public FinancialSources getStandardFinancialSource() throws SingleResultException {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("isStdFlg", true);
			params.put("active", true);
			FinancialSources defaultAverageMethod = this.findEntityByAttributes(params);
			return defaultAverageMethod;
		} catch (ApplicationException nre) {
			throw new SingleResultException(nre.getMessage(), nre);
		} catch (Exception e) {
			throw new SingleResultException(e.getMessage(), e);
		}
	}

	@Override
	@Transactional
	public void setStandardFinancialSource(String fsCode) throws BusinessException {
		FinancialSources oldFs = this.getStandardFinancialSource();
		if (!oldFs.getCode().equals(fsCode)) {
			oldFs.setIsStdFlg(false);
			FinancialSources finSource = this.findByCode(fsCode);
			finSource.setIsStdFlg(true);
			this.update(finSource);
			this.update(oldFs);
		}

	}
}