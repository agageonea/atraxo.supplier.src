/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.suppliers;

import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Suppliers} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Suppliers_Service extends AbstractEntityService<Suppliers> {

	/**
	 * Public constructor for Suppliers_Service
	 */
	public Suppliers_Service() {
		super();
	}

	/**
	 * Public constructor for Suppliers_Service
	 * 
	 * @param em
	 */
	public Suppliers_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Suppliers> getEntityClass() {
		return Suppliers.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Suppliers
	 */
	public Suppliers findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Suppliers.NQ_FIND_BY_CODE,
							Suppliers.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Suppliers", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Suppliers", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Suppliers
	 */
	public Suppliers findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Suppliers.NQ_FIND_BY_BUSINESS,
							Suppliers.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Suppliers", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Suppliers", "id"), nure);
		}
	}

	/**
	 * Find by reference: responsibleBuyer
	 *
	 * @param responsibleBuyer
	 * @return List<Suppliers>
	 */
	public List<Suppliers> findByResponsibleBuyer(UserSupp responsibleBuyer) {
		return this.findByResponsibleBuyerId(responsibleBuyer.getId());
	}
	/**
	 * Find by ID of reference: responsibleBuyer.id
	 *
	 * @param responsibleBuyerId
	 * @return List<Suppliers>
	 */
	public List<Suppliers> findByResponsibleBuyerId(String responsibleBuyerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Suppliers e where e.clientId = :clientId and e.responsibleBuyer.id = :responsibleBuyerId",
						Suppliers.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("responsibleBuyerId", responsibleBuyerId)
				.getResultList();
	}
}
