/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.sys.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.ext.types.FmbasSysParam;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceCostToAccrual;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.commons.security.UserCache;

/**
 * Business extensions specific for {@link SystemParameter} domain entity.
 */
public class SystemParameter_Service extends atraxo.fmbas.business.impl.sys.SystemParameter_Service implements ISystemParameterService {

	private final static Logger LOG = LoggerFactory.getLogger(SystemParameter_Service.class);

	@Override
	public String getDensity() throws BusinessException {
		return this.getSystemParameterValue(FmbasSysParam.SYSDENS);
	}

	@Override
	public void updateSessionSystemParameters(Object obj) throws BusinessException {
		Map<String, Object> params = Collections.emptyMap();
		List<SystemParameter> list = this.findEntitiesByAttributes(params);
		Map<String, String> paramMap = new HashMap<>();
		for (SystemParameter param : list) {
			paramMap.put(param.getCode(), param.getValue());
		}
		Session.user.get().getProfile().setCache(new UserCache(paramMap));
	}

	private String getSystemParameterValue(FmbasSysParam code) throws BusinessException {
		try {
			String qlString = "select e from " + SystemParameter.class.getSimpleName() + " e where e.clientId =:clientId and e.code =:code";
			SystemParameter sysParam = this.getEntityManager().createQuery(qlString, SystemParameter.class)
					.setParameter("clientId", Session.user.get().getClient().getId()).setParameter("code", code.toString()).getSingleResult();
			return sysParam.getValue();
		} catch (NonUniqueResultException e) {
			throw new BusinessException(BusinessErrorCode.NO_UNIQUE_ENITY, BusinessErrorCode.NO_UNIQUE_ENITY.getErrMsg(), e);
		} catch (NoResultException e) {
			throw new BusinessException(BusinessErrorCode.NO_ENTITY,
					String.format(BusinessErrorCode.NO_ENTITY.getErrMsg(), SystemParameter.class.getSimpleName(), code.toString()), e);
		}

	}

	@Override
	public Integer getDecimalsForPercentage() throws BusinessException {
		return Integer.parseInt(this.getSystemParameterValue(FmbasSysParam.DEC_PRC));
	}

	@Override
	public Integer getDecimalsForCurrency() throws BusinessException {
		return Integer.parseInt(this.getSystemParameterValue(FmbasSysParam.DEC_CRNCY));
	}

	@Override
	public Integer getDecimalsForUnit() throws BusinessException {
		return Integer.parseInt(this.getSystemParameterValue(FmbasSysParam.DEC_UNIT));
	}

	@Override
	public Integer getDecimalsForAmount() throws BusinessException {
		return Integer.parseInt(this.getSystemParameterValue(FmbasSysParam.DEC_AMOUNT));
	}

	@Override
	public String getSysVol() throws BusinessException {
		return this.getSystemParameterValue(FmbasSysParam.SYSVOL);
	}

	@Override
	public String getSysWeight() throws BusinessException {
		return this.getSystemParameterValue(FmbasSysParam.SYSWEIGHT);
	}

	@Override
	public String getSysCurrency() throws BusinessException {
		return this.getSystemParameterValue(FmbasSysParam.SYSCRNCY);
	}

	@Override
	public String getSysAverageMethod() throws BusinessException {
		return this.getSystemParameterValue(FmbasSysParam.SYSAVGMTHD);
	}

	@Override
	public String getInvoiceApprovalWorkflow() throws BusinessException {
		return this.getSystemParameterValue(FmbasSysParam.SYSINVAPPROVAL);
	}

	@Override
	public String getAccrualEvaluationDate() throws BusinessException {
		return this.getSystemParameterValue(FmbasSysParam.ACCRUALEVALDATE);
	}

	@Override
	public Boolean getSalesInvoiceApprovalWorkflow() throws BusinessException {
		return Boolean.valueOf(this.getSystemParameterValue(FmbasSysParam.SYSSELLINVAPPR));
	}

	@Override
	public InvoiceCostToAccrual getInvoiceCostToAccruals() throws BusinessException {
		return InvoiceCostToAccrual.getByName(this.getSystemParameterValue(FmbasSysParam.INVCOSTACCRUAL));
	}

	@Override
	public Boolean getAccrualAutoClose() throws BusinessException {
		return Boolean.valueOf(this.getSystemParameterValue(FmbasSysParam.ACCRUALCLOSE));
	}

	@Override
	public String getSysUnit() throws BusinessException {
		return this.getSystemParameterValue(FmbasSysParam.SYSUNITS);
	}

	@Override
	public String getNoReplyAddress() throws BusinessException {
		return this.getSystemParameterValue(FmbasSysParam.NO_REPLY);
	}

	@Override
	public String getDefaultFuelEventReceivedStatus() throws BusinessException {
		return this.getSystemParameterValue(FmbasSysParam.DEFAULT_FUEL_EVENT_STATUS);
	}

	@Override
	public Boolean getRestrictBlacklisted() throws BusinessException {
		return Boolean.valueOf(this.getSystemParameterValue(FmbasSysParam.RESTRICTBLOCKED));
	}

	@Override
	public Boolean getCreditMemoApprovalWorkflow() throws BusinessException {
		return Boolean.valueOf(this.getSystemParameterValue(FmbasSysParam.SYSCREDITMEMOAPPROVAL));
	}

	@Override
	public Boolean getPriceUpdateApprovalWorkflow() throws BusinessException {
		try {
			return Boolean.valueOf(this.getSystemParameterValue(FmbasSysParam.SYSPRICEUPDATEAPPROVAL));
		} catch (BusinessException e) {
			LOG.warn(e.getMessage(), e);
			return false;
		}
	}

	@Override
	public Boolean getSellContractApprovalWorkflow() throws BusinessException {
		return Boolean.valueOf(this.getSystemParameterValue(FmbasSysParam.SYSSALESCONTRACTAPPROVAL));
	}

	@Override
	public Boolean getPurchaseContractApprovalWorkflow() throws BusinessException {
		return Boolean.valueOf(this.getSystemParameterValue(FmbasSysParam.SYSPURCHASECONTRACTAPPROVAL));
	}

	@Override
	public Boolean getBidApprovalWorkflow() throws BusinessException {
		return Boolean.valueOf(this.getSystemParameterValue(FmbasSysParam.SYSBIDAPPROVAL));
	}

	@Override
	public Boolean getFuelTicketApprovalWorkflow() throws BusinessException {
		return Boolean.valueOf(this.getSystemParameterValue(FmbasSysParam.SYSFUELTICKETAPPROVAL));
	}

	@Override
	public Boolean getEnergyPriceApprovalWorkflow() throws BusinessException {
		return Boolean.valueOf(this.getSystemParameterValue(FmbasSysParam.SYSENERGYPRICEAPPROVAL));
	}

	@Override
	public Boolean getExchangeRateApprovalWorkflow() throws BusinessException {
		return Boolean.valueOf(this.getSystemParameterValue(FmbasSysParam.SYSEXCHANGERATEAPPROVAL));
	}
}
