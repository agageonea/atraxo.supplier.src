package atraxo.fmbas.business.ext.exceptions;

import seava.j4e.api.exceptions.IErrorCode;

/**
 * Base class for service exception that have a specific error code;
 *
 * @author zspeter
 *
 */
public class SONEException extends RuntimeException {

	/**
	 * Error code for exception.
	 */
	private IErrorCode errorCode;

	/**
	 * Specific error message for the exception.
	 */
	private String errorMesage;

	public SONEException(IErrorCode errorCode) {
		super();
		this.errorCode = errorCode;
	}

	public SONEException(IErrorCode errorCode, String errorMessage) {
		super(errorMessage);
		this.errorCode = errorCode;
		this.errorMesage = errorMessage;
	}

	public SONEException(IErrorCode errorCode, String message, Throwable throwable) {
		super(message, throwable);
		this.errorCode = errorCode;
		this.errorMesage = message;
	}

	public SONEException(String errorMesage) {
		super(errorMesage);
		this.errorMesage = errorMesage;
	}

	public SONEException() {
		super();
	}

	public SONEException(String message, Throwable throwable) {
		super(message, throwable);
		this.errorMesage = message;
	}

	public SONEException(Throwable throwable) {
		super(throwable);
	}

	@Override
	public String getMessage() {
		if (this.getCause() instanceof SONEException) {
			return this.getCause().getMessage();
		}
		if (this.errorMesage != null) {
			return this.errorMesage;
		}
		if (this.errorCode != null) {
			return this.errorCode.getErrMsg();
		}
		return super.getMessage();
	}

}
