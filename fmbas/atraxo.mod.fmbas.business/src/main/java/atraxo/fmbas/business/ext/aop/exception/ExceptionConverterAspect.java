package atraxo.fmbas.business.ext.aop.exception;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.exceptions.IErrorCode;

/**
 * @author zspeter
 */
@Aspect
@Order(-2)
public class ExceptionConverterAspect {

	/**
	 * @param pjp
	 * @param ex
	 * @throws BusinessException
	 */
	@AfterThrowing(pointcut = "@annotation(ExceptionConverter)", throwing = "ex")
	public void convert(JoinPoint pjp, Throwable ex) throws BusinessException {
		MethodSignature sign = (MethodSignature) pjp.getSignature();
		Method method = sign.getMethod();
		for (Method m : pjp.getTarget().getClass().getDeclaredMethods()) {
			if (m.getName().equalsIgnoreCase(method.getName())) {
				ExceptionConverter converter = m.getAnnotation(ExceptionConverter.class);
				Class<? extends BusinessException> target = converter.target();
				try {
					throw target.getConstructor(IErrorCode.class).newInstance(BusinessErrorCode.TS_USED);
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
				}

			}
		}
	}
}
