/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.workflow.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.ad.domain.impl.ad.Category;
import atraxo.ad.domain.impl.ad.NotificationAction;
import atraxo.ad.domain.impl.ad.Priority;
import atraxo.fmbas.business.api.notification.INotificationService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.api.workflow.IWorkflowNotificationService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowInstanceStatus;
import atraxo.fmbas.domain.impl.notification.Notification;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowNotification;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link WorkflowNotification} domain entity.
 */
public class WorkflowNotification_Service extends atraxo.fmbas.business.impl.workflow.WorkflowNotification_Service
		implements IWorkflowNotificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowNotification_Service.class);

	@Autowired
	private IUserSuppService userSrv;
	@Autowired
	private INotificationService notifySrv;

	private Map<String, Object> params = new HashMap<>();

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void sendNotification(WorkflowInstance wkfInstance, WorkflowInstanceStatus status, String instanceResult, Map params)
			throws BusinessException {

		this.params.clear();
		this.params.putAll(params);
		this.sendNotification(wkfInstance, status, instanceResult);
	}

	@Override
	public void sendNotification(WorkflowInstance wkfInstance, WorkflowInstanceStatus status, String instanceResult) {
		try {
			Workflow wkf = wkfInstance.getWorkflow();
			if (CollectionUtils.isEmpty(wkf.getNotifications())) {
				return;
			}

			Notification notification = this.buildNotification(wkf, wkfInstance, status, instanceResult);
			List<UserSupp> emailUsers = new ArrayList<>();
			for (WorkflowNotification wfkNotification : wkf.getNotifications()) {
				if (!wfkNotification.getEnabled()) {
					continue;
				}
				UserSupp user = this.userSrv.findById(wfkNotification.getUser().getId());
				switch (wfkNotification.getNotificationCondition()) {
				case _ALL_EVENTS_:
					this.notifySrv.addUsersToNotification(notification, user, wfkNotification.getNotificationType(), emailUsers);
					break;
				case _ON_SUCCESS_:
					if (status.equals(WorkflowInstanceStatus._SUCCESS_)) {
						this.notifySrv.addUsersToNotification(notification, user, wfkNotification.getNotificationType(), emailUsers);
					}
					break;
				case _ON_FAILURE_:
					if (status.equals(WorkflowInstanceStatus._FAILURE_)) {
						this.notifySrv.addUsersToNotification(notification, user, wfkNotification.getNotificationType(), emailUsers);
					}
					break;
				default:
					break;
				}
			}
			this.notifySrv.sendAndSave(notification, emailUsers, Collections.emptyMap());
		} catch (Exception notificationException) {
			LOGGER.error(notificationException.getMessage(), notificationException);
		}
	}

	/**
	 * @param wkf
	 * @param wkfInstance
	 * @param status
	 * @return
	 * @throws BusinessException
	 */
	private Notification buildNotification(Workflow wkf, WorkflowInstance wkfInstance, WorkflowInstanceStatus status, String instanceResult) {
		Notification notification = new Notification();
		notification.setTitle(wkf.getName());
		notification.setLink("link");
		notification.setAction(NotificationAction._NOACTION_);
		notification.setEventDate(GregorianCalendar.getInstance().getTime());
		notification.setLifeTime(GregorianCalendar.getInstance().getTime());

		String description;
		Boolean hasDetails = (Boolean) this.params.getOrDefault(WorkflowVariablesConstants.NOTIFICATION_DETAILS, Boolean.FALSE);
		String entityName = (String) this.params.getOrDefault(WorkflowVariablesConstants.NOTIFICATION_DETAILS_ENTITY_NAME, StringUtils.EMPTY);
		String entityCode = (String) this.params.getOrDefault(WorkflowVariablesConstants.NOTIFICATION_DETAILS_ENTITY_CODE, StringUtils.EMPTY);

		if (WorkflowInstanceStatus._SUCCESS_.equals(status)) {
			description = hasDetails
					? String.format(BusinessErrorCode.INTERFACE_CUSTOM_NOTIFICATION_SUCCESS_WKF.getErrMsg(), wkf.getName(), entityName, entityCode)
					: String.format(BusinessErrorCode.INTERFACE_NOTIFICATION_SUCCESS.getErrMsg(), wkf.getName());

			notification.setDescripion(description);
			notification.setPriority(Priority._DEFAULT_);
			notification.setCategory(Category._ACTIVITY_);
		} else if (WorkflowInstanceStatus._FAILURE_.equals(status)) {
			description = hasDetails
					? String.format(BusinessErrorCode.INTERFACE_CUSTOM_NOTIFICATION_FAILURE_WKF.getErrMsg(), wkf.getName(), entityName, entityCode)
					: String.format(BusinessErrorCode.INTERFACE_NOTIFICATION_FAILURE_WKF.getErrMsg(), wkf.getName());

			notification.setDescripion(description);
			notification.setPriority(Priority._MAX_);
			notification.setCategory(Category._ERROR_);
		} else {
			description = hasDetails
					? String.format(BusinessErrorCode.INTERFACE_CUSTOM_NOTIFICATION_TERMINATE_WFK.getErrMsg(), wkf.getName(), entityName, entityCode)
					: String.format(BusinessErrorCode.INTERFACE_NOTIFICATION_TERMINATE_WFK.getErrMsg(), wkf.getName());

			notification.setDescripion(description);
			notification.setCategory(Category._ERROR_);
			notification.setPriority(Priority._DEFAULT_);
		}

		notification.setStatus(status.getName());
		notification.setResult(instanceResult);
		notification.setUsers(new ArrayList<UserSupp>());

		Map<String, Object> map = new HashMap<>();
		map.put(WorkflowVariablesConstants.VAR_WKF_INSTANCE_ID, wkfInstance.getId());
		map.put(WorkflowVariablesConstants.VAR_WKF_ID, wkf.getId());

		// If custom notification then use the params else use the default values
		String methodName = (String) this.params.getOrDefault(WorkflowVariablesConstants.NOTIFICATION_METHOD_NAME, "openWorkflowInstanceDialog");
		String methodParam = (String) this.params.getOrDefault(WorkflowVariablesConstants.NOTIFICATION_METHOD_PARAM,
				map.toString().replace('=', ':'));

		notification.setMethodName(methodName);
		notification.setMethodParam(methodParam);

		return notification;
	}

}
