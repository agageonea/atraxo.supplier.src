package atraxo.fmbas.business.ext.exceptions.conversion;

import seava.j4e.api.exceptions.BusinessException;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.uom.Unit;

public class InconvertibleUnitTypesException extends BusinessException {

	private static final long serialVersionUID = -7204396805888310951L;

	public InconvertibleUnitTypesException(Unit unit1, Unit unit2) {
		super(BusinessErrorCode.INCONVERTIBLE_UNITS, "Can't convert from type " + unit1.getUnittypeInd() + " to type "
				+ unit2.getUnittypeInd() + ".");
	}

}
