/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.avatars.service;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.fmbas.business.api.avatars.IAvatarService;
import atraxo.fmbas.domain.impl.avatars.Avatar;

/**
 * Business extensions specific for {@link Avatar} domain entity.
 */
public class Avatar_Service extends atraxo.fmbas.business.impl.avatars.Avatar_Service implements IAvatarService {

	@Override
	protected void preInsert(Avatar e) throws BusinessException {
		String name = e.getName();

		if (name == null || "".equals(name)) {
			AttachmentType type = e.getType();
			if (type == null) {
				throw new BusinessException(ErrorCode.G_NULL_FIELD_TYPE, "No type specified for attachment.");
			}
			e.setName(e.getFileName());
		}
		super.preInsert(e);
	}

}
