package atraxo.fmbas.business.ext.dictionary.delegate;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author apetho
 */
public class FieldListBuilder {

	private static final Logger LOG = LoggerFactory.getLogger(FieldListBuilder.class);

	private static final String SEPARATOR = "/";

	private FieldListBuilder() {
	}

	/**
	 * @param clazz
	 * @param path - initial value is recommended to be empty string.
	 * @return
	 */
	public static List<String> build(Class<?> clazz, String path) {
		String packageStr = clazz.getPackage().getName();
		List<String> nameList = new ArrayList<>();

		for (Field field : clazz.getDeclaredFields()) {
			field.setAccessible(true);
			if (java.lang.reflect.Modifier.isStatic(field.getModifiers()) || field.getType().equals(clazz) || String.class.equals(clazz)) {
				continue;
			}
			if (field.getType().equals(List.class)) {
				ParameterizedType listType = (ParameterizedType) field.getGenericType();
				Class<?> listClass = (Class<?>) listType.getActualTypeArguments()[0];
				List<String> subpackageStrList = FieldListBuilder.build(listClass,
						!path.isEmpty() ? path + SEPARATOR + listClass.getSimpleName() : "" + listClass.getSimpleName());
				nameList.addAll(subpackageStrList);
			}
			if (field.getType().equals(String.class) || field.getType().isEnum()) {
				nameList.add("".equalsIgnoreCase(path) ? field.getName() : path + SEPARATOR + field.getName());
			} else {
				if (!field.getType().isPrimitive() && field.getType().getPackage().getName().equals(packageStr)) {
					String fieldName = field.getType().isEnum() ? field.getName() : field.getType().getSimpleName();
					List<String> subpackageStrList = FieldListBuilder.build(field.getType(),
							!path.isEmpty() ? path + SEPARATOR + fieldName : "" + fieldName);
					nameList.addAll(subpackageStrList);
				}
			}
		}
		return nameList;
	}

	/**
	 * @param clazz
	 * @param entity
	 * @param fieldName
	 * @return
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	public static <T> Object getValue(Class<?> clazz, T entity, String fieldName) throws NoSuchFieldException, IllegalAccessException {
		String[] strArray = fieldName.split(SEPARATOR);
		Class<?> clazzTemp = clazz;
		Object obj = entity;
		for (int i = 0; i < strArray.length; ++i) {
			if (i == strArray.length - 1) {
				Field field = clazzTemp.getDeclaredField(strArray[i]);
				field.setAccessible(true);
				if (field.getType().equals(String.class)) {
					return field.get(obj);
				}
				if (field.getType().isEnum()) {
					return field.get(obj);
				}
			} else {
				Field field = getField(strArray[i], clazzTemp);
				field.setAccessible(true);
				clazzTemp = field.getType();
				obj = field.get(obj);
				if (clazzTemp.isAssignableFrom(List.class)) {
					clazzTemp = (Class<?>) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
					obj = ((List<?>) obj).get(0);
				}
				if (obj == null) {
					return null;
				}
			}
		}
		return "";
	}

	private static Field getField(String fieldName, Class<?> clazz) throws NoSuchFieldException {
		try {
			return clazz.getDeclaredField(fieldName.substring(0, 1).toLowerCase() + fieldName.substring(1));
		} catch (NoSuchFieldException e) {
			LOG.info("No field, wil do nothing !", e);
			for (Field field : clazz.getDeclaredFields()) {
				if (fieldName.equalsIgnoreCase(field.getName())) {
					return field;
				}
			}
		}
		throw new NoSuchFieldException();
	}

	/**
	 * @param clazz
	 * @param entity
	 * @param fieldName
	 * @param value
	 * @param originalValue
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 */
	public static <T> void setValue(Class<?> clazz, T entity, String fieldName, String value, String originalValue)
			throws NoSuchFieldException, IllegalAccessException {
		iterateOnField(clazz, entity, fieldName, value, originalValue);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static final <T> void iterateOnField(Class clazz, T entity, String fieldName, String value, String originalValue)
			throws NoSuchFieldException, IllegalAccessException {
		if (fieldName.isEmpty() || entity == null) {
			return;
		}
		String[] strArray = fieldName.split(SEPARATOR);
		Field field = getField(strArray[0], clazz);
		field.setAccessible(true);
		if (strArray.length == 1) {
			if (field.getType().equals(String.class) && field.get(entity).equals(originalValue)) {
				field.set(entity, value);
			} else if (field.getType().isEnum() && field.get(entity).equals(Enum.valueOf((Class) field.getType(), originalValue))) {
				field.set(entity, Enum.valueOf((Class) field.getType(), value));
			}
		} else {
			if (field.getType().equals(List.class)) {
				List list = (List) field.get(entity);
				if (!CollectionUtils.isEmpty(list)) {
					for (int i = 0; i < list.size(); ++i) {
						iterateOnField(list.get(i).getClass(), list.get(i), fieldName.substring(strArray[0].length() + 1), value, originalValue);
					}
				}
			} else {
				iterateOnField(field.getType(), field.get(entity), fieldName.substring(strArray[0].length() + 1), value, originalValue);
			}
		}
	}

}
