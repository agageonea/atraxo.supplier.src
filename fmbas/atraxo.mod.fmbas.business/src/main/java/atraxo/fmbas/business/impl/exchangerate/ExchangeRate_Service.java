/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.exchangerate;

import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ExchangeRate} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ExchangeRate_Service extends AbstractEntityService<ExchangeRate> {

	/**
	 * Public constructor for ExchangeRate_Service
	 */
	public ExchangeRate_Service() {
		super();
	}

	/**
	 * Public constructor for ExchangeRate_Service
	 * 
	 * @param em
	 */
	public ExchangeRate_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ExchangeRate> getEntityClass() {
		return ExchangeRate.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ExchangeRate
	 */
	public ExchangeRate findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ExchangeRate.NQ_FIND_BY_BUSINESS,
							ExchangeRate.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ExchangeRate", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ExchangeRate", "id"), nure);
		}
	}

	/**
	 * Find by reference: tserie
	 *
	 * @param tserie
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByTserie(TimeSerie tserie) {
		return this.findByTserieId(tserie.getId());
	}
	/**
	 * Find by ID of reference: tserie.id
	 *
	 * @param tserieId
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByTserieId(Integer tserieId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ExchangeRate e where e.clientId = :clientId and e.tserie.id = :tserieId",
						ExchangeRate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("tserieId", tserieId).getResultList();
	}
	/**
	 * Find by reference: currency1
	 *
	 * @param currency1
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByCurrency1(Currencies currency1) {
		return this.findByCurrency1Id(currency1.getId());
	}
	/**
	 * Find by ID of reference: currency1.id
	 *
	 * @param currency1Id
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByCurrency1Id(Integer currency1Id) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ExchangeRate e where e.clientId = :clientId and e.currency1.id = :currency1Id",
						ExchangeRate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currency1Id", currency1Id).getResultList();
	}
	/**
	 * Find by reference: currency2
	 *
	 * @param currency2
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByCurrency2(Currencies currency2) {
		return this.findByCurrency2Id(currency2.getId());
	}
	/**
	 * Find by ID of reference: currency2.id
	 *
	 * @param currency2Id
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByCurrency2Id(Integer currency2Id) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ExchangeRate e where e.clientId = :clientId and e.currency2.id = :currency2Id",
						ExchangeRate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currency2Id", currency2Id).getResultList();
	}
	/**
	 * Find by reference: finsource
	 *
	 * @param finsource
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByFinsource(FinancialSources finsource) {
		return this.findByFinsourceId(finsource.getId());
	}
	/**
	 * Find by ID of reference: finsource.id
	 *
	 * @param finsourceId
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByFinsourceId(Integer finsourceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ExchangeRate e where e.clientId = :clientId and e.finsource.id = :finsourceId",
						ExchangeRate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("finsourceId", finsourceId).getResultList();
	}
	/**
	 * Find by reference: avgMethodIndicator
	 *
	 * @param avgMethodIndicator
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByAvgMethodIndicator(
			AverageMethod avgMethodIndicator) {
		return this.findByAvgMethodIndicatorId(avgMethodIndicator.getId());
	}
	/**
	 * Find by ID of reference: avgMethodIndicator.id
	 *
	 * @param avgMethodIndicatorId
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByAvgMethodIndicatorId(
			Integer avgMethodIndicatorId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ExchangeRate e where e.clientId = :clientId and e.avgMethodIndicator.id = :avgMethodIndicatorId",
						ExchangeRate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("avgMethodIndicatorId", avgMethodIndicatorId)
				.getResultList();
	}
	/**
	 * Find by reference: exchangeRateValue
	 *
	 * @param exchangeRateValue
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByExchangeRateValue(
			ExchangeRateValue exchangeRateValue) {
		return this.findByExchangeRateValueId(exchangeRateValue.getId());
	}
	/**
	 * Find by ID of reference: exchangeRateValue.id
	 *
	 * @param exchangeRateValueId
	 * @return List<ExchangeRate>
	 */
	public List<ExchangeRate> findByExchangeRateValueId(
			Integer exchangeRateValueId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from ExchangeRate e, IN (e.exchangeRateValue) c where e.clientId = :clientId and c.id = :exchangeRateValueId",
						ExchangeRate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("exchangeRateValueId", exchangeRateValueId)
				.getResultList();
	}
}
