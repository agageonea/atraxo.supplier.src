/**
 *
 */
package atraxo.fmbas.business.ext.bpm.delegate;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.support.MessageBuilder;

import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceService;
import atraxo.fmbas.business.api.workflow.IWorkflowService;
import atraxo.fmbas.business.ext.bpm.WorkflowBpmErrorHandler;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import seava.j4e.api.ISettings;
import seava.j4e.api.enums.SysParam;
import seava.j4e.api.exceptions.InvalidConfiguration;
import seava.j4e.api.service.IClientInfoProvider;
import seava.j4e.api.session.IClient;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.IUserProfile;
import seava.j4e.api.session.IUserSettings;
import seava.j4e.api.session.IWorkspace;
import seava.j4e.api.session.Session;
import seava.j4e.commons.security.AppClient;
import seava.j4e.commons.security.AppUser;
import seava.j4e.commons.security.AppUserProfile;
import seava.j4e.commons.security.AppUserSettings;

/**
 * Abstract <code>JavaDelegate</code> class, this is the starting point for any custom delegate, it incorporates common behavior and also offers
 * mandatory abstract methods that need to be implemented by all the future delegates
 *
 * @author vhojda
 */
public abstract class AbstractJavaDelegate implements JavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractJavaDelegate.class);

	protected DelegateExecution execution;
	protected Integer workflowId;
	protected String workflowName;

	@Autowired
	protected IWorkflowService workflowService;

	@Autowired
	protected IWorkflowInstanceService workflowInstanceService;

	@Autowired
	protected IWorkflowBpmManager workflowBpmManager;

	@Autowired
	protected WorkflowBpmErrorHandler workflowBpmErrorHandler;

	@Autowired
	protected ISystemParameterService systemParamsService;

	@Autowired
	protected ApplicationContext applicationContext;

	@Autowired
	private ISettings settings;

	/**
	 * Executes the code.
	 */
	public abstract void execute() throws Exception;

	/**
	 * This methods sets up the code of the <code>User</code> for whom a <code>Session</code> will be created; the entire code of the
	 * <code>JavaDelegate</code> will use that <code>User</code> code for executing instructions. If no code is provided the default one will be used
	 * (the CORE WORKFLOW user);
	 *
	 * @returns the code of the <code>User</code>
	 */
	public abstract String getSessionUserCode();

	/*
	 * (non-Javadoc)
	 * @see org.activiti.engine.delegate.JavaDelegate#execute(org.activiti.engine.delegate.DelegateExecution)
	 */
	@Override
	public void execute(DelegateExecution execution) throws Exception {
		this.execution = execution;

		this.createSessionUser();

		this.extractWorkflowData();
		this.execute();
	}

	private void createSessionUser() throws InvalidConfiguration {
		if (Session.user.get() != null) {
			// I have a session user don't need to create an other one
			return;
		}
		String userCode = this.getSessionUserCode();
		if (!StringUtils.isEmpty(userCode)) {
			this.createSession(userCode);
		} else {
			// attempt to use the default one (the CORE)
			String defaultUserCode = this.getCoreUserCode();
			if (!StringUtils.isEmpty(defaultUserCode)) {
				this.createSession(defaultUserCode);
			} else {
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						"The workflow session is not well configured. Please contact the administrator.");
			}
		}
	}

	/**
	 * Gets the code for the "Core Workflow User" from settings parameters
	 *
	 * @return
	 * @throws InvalidConfiguration
	 */
	protected String getCoreUserCode() throws InvalidConfiguration {
		return this.settings.getParam(SysParam.CORE_WORKFLOW_USER.name());
	}

	/**
	 * Creates a <code>Session</code> for a specific user code
	 *
	 * @param userCode teh code of the <code>User</code>
	 * @throws InvalidConfiguration
	 */
	private void createSession(String userCode) throws InvalidConfiguration {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START createSession()");
		}

		String clientId = (String) this.execution.getVariable(WorkflowVariablesConstants.VAR_APP_CLIENT_ID);
		String cliectCode = (String) this.execution.getVariable(WorkflowVariablesConstants.VAR_APP_CLIENT_CODE);

		IClient client = new AppClient(clientId, cliectCode, "");
		IUserSettings userSettings = AppUserSettings.newInstance(this.settings);
		IUserProfile profile = new AppUserProfile(true, null, false, false, false);

		// create an incomplete user first
		IUser user = new AppUser(userCode, userCode, null, userCode, null, null, client, userSettings, profile, null, true);
		Session.user.set(user);

		// get the client workspace info
		IWorkspace ws = this.applicationContext.getBean(IClientInfoProvider.class).getClientWorkspace();
		user = new AppUser(userCode, userCode, null, userCode, null, null, client, userSettings, profile, ws, true);
		Session.user.set(user);

		Message<IUser> message = MessageBuilder.withPayload(user).build();
		this.applicationContext.getBean("updateJobSessionChannel", MessageChannel.class).send(message);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END createSession()");
		}
	}

	/**
	 * Extracts and sets common workflow variables
	 */
	private void extractWorkflowData() {
		this.workflowId = (Integer) this.execution.getVariable(WorkflowVariablesConstants.VAR_WKF_ID);
		this.workflowName = (String) this.execution.getVariable(WorkflowVariablesConstants.VAR_WKF_NAME);
	}

	/**
	 * Used only for Approve/Reject delegates
	 *
	 * @return
	 */
	public String getNote() {
		Object note = this.execution.getVariable(WorkflowVariablesConstants.TASK_NOTE_VAR);
		return note != null ? note.toString() : "UNKNOWN";
	}

	/**
	 * @param errorCode
	 * @param errorDescription
	 * @param e
	 */
	protected void throwBpmErrorForWorkflow(String errorCode, String errorDescription, Exception e) {
		// handle the error
		this.workflowBpmErrorHandler.handleError(this.execution, errorCode, errorDescription, e);
	}

	/**
	 * @param errorCode
	 * @param errorDescription
	 */
	protected void throwBpmErrorForWorkflow(String errorCode, String errorDescription) {
		this.throwBpmErrorForWorkflow(errorCode, errorDescription, null);
	}

	/**
	 * Gets a specific variable from the workflow variable map as <code>String</code>;
	 *
	 * @param varName
	 * @returns the value converted to <code>String</code> if it exists, empty <code>String</code> otherwise
	 */
	protected String getVariableFromWorkflow(String varName) {
		String value = "";
		Object valueObj = this.execution.getVariable(varName);
		if (valueObj != null) {
			value = valueObj.toString();
		}
		return value;
	}

}
