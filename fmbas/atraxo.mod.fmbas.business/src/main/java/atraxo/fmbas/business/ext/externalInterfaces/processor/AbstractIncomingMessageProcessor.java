package atraxo.fmbas.business.ext.externalInterfaces.processor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.ext.ws.IIncomingMessageProcessor;
import atraxo.fmbas.business.api.externalInterfaces.IIncommingMessageService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ws.GUIDMessageGenerator;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.IncommingMessage;
import atraxo.fmbas.domain.impl.fmbas_type.IncomingMessageStatus;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import atraxo.fmbas.domain.ws.messgeXSD.MSG.HeaderCommon;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.business.AbstractApplicationContextAware;
import seava.j4e.commons.utils.XMLUtils;

/**
 * Base class for services that need to process incoming messages asynchronously.
 *
 * @author zspeter
 */
public abstract class AbstractIncomingMessageProcessor extends AbstractApplicationContextAware implements IIncomingMessageProcessor {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractIncomingMessageProcessor.class);

	@Autowired
	private IUserSuppService userSrv;
	@Autowired
	private IIncommingMessageService msgSrv;

	@Override
	public MSG process(MSG request, String beanName, String syncMethod) throws BusinessException {
		try {
			this.initSession(request);
			ExternalInterface extInt = this.getExternalInterface();
			this.verifyInterfaceStatus(extInt);
			if (extInt.getAsync()) {
				this.buffer(request, beanName, syncMethod, extInt);
			} else {
				Object obj = this.getApplicationContext().getBean(beanName);
				Method m = obj.getClass().getMethod(syncMethod, MSG.class);
				m.invoke(this, request);
			}
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			LOG.warn("Stupid developer", e);
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, "Stupid developer/configuratior.");
		}
		return request;
	}

	/**
	 * Buffer current request.
	 *
	 * @param request
	 * @return
	 * @throws BusinessException
	 */
	private Integer buffer(MSG request, String beanName, String syncMethod, ExternalInterface externalInterface) throws BusinessException {
		try {
			IncommingMessage msg = new IncommingMessage();
			msg.setMessage(XMLUtils.toString(request, MSG.class, true, false));
			msg.setBeanName(beanName);
			msg.setStatus(IncomingMessageStatus._NEW_);
			msg.setMethodName(syncMethod);
			msg.setMessageId(request.getHeaderCommon().getMsgID());
			msg.setExternalInterface(externalInterface);
			this.msgSrv.insert(msg);
			return msg.getId();
		} catch (JAXBException e) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e.getMessage(), e);
		}
	}

	/**
	 * Abstract method to get the specific external interface.
	 *
	 * @return
	 */
	protected abstract ExternalInterface getExternalInterface();

	/**
	 * @param externalInterface
	 * @throws BusinessException
	 */
	private void verifyInterfaceStatus(ExternalInterface externalInterface) throws BusinessException {
		if (!externalInterface.getEnabled()) {
			LOG.info("{} (Interface) is disabled.", externalInterface.getName());
			throw new BusinessException(BusinessErrorCode.INTERFACE_DISABLED, BusinessErrorCode.INTERFACE_DISABLED.getErrMsg());
		}
	}

	/**
	 * @param request
	 * @throws BusinessException
	 */
	private void initSession(MSG request) throws BusinessException {
		HeaderCommon headerCommon = request.getHeaderCommon();

		String clientIdentifier = GUIDMessageGenerator.extractClientIdentifier(headerCommon.getCorrelationID());
		if (LOG.isDebugEnabled()) {
			LOG.debug("Create session user for client: {}", clientIdentifier);
		}
		this.userSrv.createSessionUser(clientIdentifier);

	}

}
