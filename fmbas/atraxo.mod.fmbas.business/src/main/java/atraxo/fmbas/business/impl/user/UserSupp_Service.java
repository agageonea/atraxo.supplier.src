/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.user;

import atraxo.ad.domain.impl.system.DateFormat;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.notification.Notification;
import atraxo.fmbas.domain.impl.user.RoleSupp;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link UserSupp} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class UserSupp_Service extends AbstractEntityService<UserSupp> {

	/**
	 * Public constructor for UserSupp_Service
	 */
	public UserSupp_Service() {
		super();
	}

	/**
	 * Public constructor for UserSupp_Service
	 * 
	 * @param em
	 */
	public UserSupp_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<UserSupp> getEntityClass() {
		return UserSupp.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return UserSupp
	 */
	public UserSupp findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(UserSupp.NQ_FIND_BY_CODE, UserSupp.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"UserSupp", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"UserSupp", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return UserSupp
	 */
	public UserSupp findByLogin(String loginName) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(UserSupp.NQ_FIND_BY_LOGIN, UserSupp.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("loginName", loginName).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"UserSupp", "loginName"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"UserSupp", "loginName"), nure);
		}
	}

	/**
	 * Find by reference: dateFormat
	 *
	 * @param dateFormat
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findByDateFormat(DateFormat dateFormat) {
		return this.findByDateFormatId(dateFormat.getId());
	}
	/**
	 * Find by ID of reference: dateFormat.id
	 *
	 * @param dateFormatId
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findByDateFormatId(String dateFormatId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from UserSupp e where e.clientId = :clientId and e.dateFormat.id = :dateFormatId",
						UserSupp.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("dateFormatId", dateFormatId).getResultList();
	}
	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findByLocation(Locations location) {
		return this.findByLocationId(location.getId());
	}
	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findByLocationId(Integer locationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from UserSupp e where e.clientId = :clientId and e.location.id = :locationId",
						UserSupp.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("locationId", locationId).getResultList();
	}
	/**
	 * Find by reference: roles
	 *
	 * @param roles
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findByRoles(RoleSupp roles) {
		return this.findByRolesId(roles.getId());
	}
	/**
	 * Find by ID of reference: roles.id
	 *
	 * @param rolesId
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findByRolesId(String rolesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from UserSupp e, IN (e.roles) c where e.clientId = :clientId and c.id = :rolesId",
						UserSupp.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("rolesId", rolesId).getResultList();
	}
	/**
	 * Find by reference: subsidiaries
	 *
	 * @param subsidiaries
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findBySubsidiaries(UserSubsidiary subsidiaries) {
		return this.findBySubsidiariesId(subsidiaries.getId());
	}
	/**
	 * Find by ID of reference: subsidiaries.id
	 *
	 * @param subsidiariesId
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findBySubsidiariesId(Integer subsidiariesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from UserSupp e, IN (e.subsidiaries) c where e.clientId = :clientId and c.id = :subsidiariesId",
						UserSupp.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("subsidiariesId", subsidiariesId).getResultList();
	}
	/**
	 * Find by reference: notifications
	 *
	 * @param notifications
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findByNotifications(Notification notifications) {
		return this.findByNotificationsId(notifications.getId());
	}
	/**
	 * Find by ID of reference: notifications.id
	 *
	 * @param notificationsId
	 * @return List<UserSupp>
	 */
	public List<UserSupp> findByNotificationsId(Integer notificationsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from UserSupp e, IN (e.notifications) c where e.clientId = :clientId and c.id = :notificationsId",
						UserSupp.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("notificationsId", notificationsId)
				.getResultList();
	}
}
