/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.externalInterfaces;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ExternalInterfaceMessageHistory} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ExternalInterfaceMessageHistory_Service
		extends
			AbstractEntityService<ExternalInterfaceMessageHistory> {

	/**
	 * Public constructor for ExternalInterfaceMessageHistory_Service
	 */
	public ExternalInterfaceMessageHistory_Service() {
		super();
	}

	/**
	 * Public constructor for ExternalInterfaceMessageHistory_Service
	 * 
	 * @param em
	 */
	public ExternalInterfaceMessageHistory_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ExternalInterfaceMessageHistory> getEntityClass() {
		return ExternalInterfaceMessageHistory.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ExternalInterfaceMessageHistory
	 */
	public ExternalInterfaceMessageHistory findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							ExternalInterfaceMessageHistory.NQ_FIND_BY_BUSINESS,
							ExternalInterfaceMessageHistory.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ExternalInterfaceMessageHistory", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ExternalInterfaceMessageHistory", "id"), nure);
		}
	}

	/**
	 * Find by index key
	 *
	 * @return List<ExternalInterfaceMessageHistory>
	 */
	public List<ExternalInterfaceMessageHistory> findByIdxMessage_id(
			String messageId) {
		return this
				.getEntityManager()
				.createNamedQuery(
						ExternalInterfaceMessageHistory.NQ_IDX_FIND_BY_MESSAGE_ID,
						ExternalInterfaceMessageHistory.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("messageId", messageId).getResultList();
	}

	/**
	 * Find by reference: externalInterface
	 *
	 * @param externalInterface
	 * @return List<ExternalInterfaceMessageHistory>
	 */
	public List<ExternalInterfaceMessageHistory> findByExternalInterface(
			ExternalInterface externalInterface) {
		return this.findByExternalInterfaceId(externalInterface.getId());
	}
	/**
	 * Find by ID of reference: externalInterface.id
	 *
	 * @param externalInterfaceId
	 * @return List<ExternalInterfaceMessageHistory>
	 */
	public List<ExternalInterfaceMessageHistory> findByExternalInterfaceId(
			Integer externalInterfaceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ExternalInterfaceMessageHistory e where e.clientId = :clientId and e.externalInterface.id = :externalInterfaceId",
						ExternalInterfaceMessageHistory.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("externalInterfaceId", externalInterfaceId)
				.getResultList();
	}
}
