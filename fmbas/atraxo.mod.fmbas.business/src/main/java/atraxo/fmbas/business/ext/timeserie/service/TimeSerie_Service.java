/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.timeserie.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.exchangerate.IExchangeRateService;
import atraxo.fmbas.business.api.quotation.IQuotationService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.timeserie.IRawTimeSerieItemService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieAverageService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieItemService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.business.ext.aop.exception.ExceptionConverter;
import atraxo.fmbas.business.ext.aop.history.Action;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.TimeSeriesException;
import atraxo.fmbas.business.ext.timeserie.delegate.CompositeCalculator_Bd;
import atraxo.fmbas.business.ext.timeserie.delegate.Publisher_Bd;
import atraxo.fmbas.domain.ext.types.timeseries.TimeSeriesType;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.fmbas_type.DataProvider;
import atraxo.fmbas.domain.impl.fmbas_type.SerieFreqInd;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesApprovalStatus;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesStatus;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.CompositeTimeSeriesSource;
import atraxo.fmbas.domain.impl.timeserie.RawTimeSerieItem;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieAverage;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;
import seava.j4e.api.annotation.Reason;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link TimeSerie} domain entity.
 */
public class TimeSerie_Service extends atraxo.fmbas.business.impl.timeserie.TimeSerie_Service implements ITimeSerieService {

	private static final Logger LOG = LoggerFactory.getLogger(TimeSerie.class);

	@Autowired
	private ISystemParameterService sysParamService;
	@Autowired
	private ITimeSerieAverageService timeSerieAverageService;
	@Autowired
	private IExchangeRateService exchangeRateService;
	@Autowired
	private IQuotationService quotationService;
	@Autowired
	private ITimeSerieItemService timeSerieItemService;
	@Autowired
	private IRawTimeSerieItemService rawTimeSerieItem;

	@Override
	public void updateStatus(TimeSerie e, TimeSeriesStatus status) throws BusinessException {
		e.setStatus(status);
		this.onUpdate(e);
		this.updateCompositeTimeSeries(e);
	}

	@Override
	@Transactional
	public void calculate(Integer id) throws BusinessException {
		TimeSerie e = this.findById(id);
		this.calculate(e);
		this.update(e);
	}

	@Override
	@Transactional
	public void calculate(TimeSerie e) throws BusinessException {
		LOG.info("Calculate time serie:" + e.getSerieName());
		this.updateUsageDate(e);
		this.getBusinessDelegate(CompositeCalculator_Bd.class).calculate(e);
	}

	private void updateCompositeTimeSeries(TimeSerie e) throws BusinessException {
		for (CompositeTimeSeriesSource src : e.getSourceTimeSeries()) {
			TimeSerie ts = src.getComposite();
			this.updateStatus(ts, TimeSeriesStatus._WORK_);
		}
	}

	private void updateUsageDate(TimeSerie e) throws BusinessException {
		List<CompositeTimeSeriesSource> list = new ArrayList<>(e.getCompositeTimeSeries());
		Date firstUsage = null;
		Date lastUsage = null;
		for (CompositeTimeSeriesSource src : list) {
			if (firstUsage == null || (src.getSource().getFirstUsage() != null && firstUsage.after(src.getSource().getFirstUsage()))) {
				firstUsage = src.getSource().getFirstUsage();
			}
			if (lastUsage == null || (src.getSource().getLastUsage() != null && lastUsage.before(src.getSource().getLastUsage()))) {
				lastUsage = src.getSource().getLastUsage();
			}
		}
		e.setStatus(TimeSeriesStatus._WORK_);
		e.setFirstUsage(firstUsage);
		e.setLastUsage(lastUsage);
	}

	/**
	 * TODO method to long please refactor and comment.
	 */
	@Override
	protected void postInsert(List<TimeSerie> list) throws BusinessException {
		super.postInsert(list);

		List<TimeSerieAverage> tsaList = new ArrayList<>();

		for (TimeSerie ts : list) {
			SerieFreqInd freqInd = ts.getSerieFreqInd();
			for (AverageMethod tsavg : this.getAllAvgMethod()) {

				String avgCode = tsavg.getCode();
				TimeSerieAverage tsa = new TimeSerieAverage();
				tsa.setActive(false);

				if (SerieFreqInd._DAILY_.equals(freqInd)) {
					tsa.setAveragingMethod(tsavg);
				}

				else if (SerieFreqInd._WEEKDAY_.equals(freqInd)) {
					// TODO complicate to understand the business of this test.
					// Please comment it for a better understanding.
					if ("WT".equals(avgCode) || "ST".equals(avgCode) || "XT".equals(avgCode) || "MT".equals(avgCode) || "NT".equals(avgCode)
							|| "FT".equals(avgCode) || "QT".equals(avgCode)) {
						tsa.setAveragingMethod(tsavg);
					}
				}

				else if (SerieFreqInd._WEEKLY_.equals(freqInd)) {
					if ("WT".equals(avgCode) || "ST".equals(avgCode) || "XT".equals(avgCode) || "MT".equals(avgCode) || "QT".equals(avgCode)) {
						tsa.setAveragingMethod(tsavg);
					}
				}

				else if (SerieFreqInd._SEMIMONTHLY_.equals(freqInd)) {
					if ("SC".equals(avgCode) || "MC".equals(avgCode) || "QC".equals(avgCode)) {
						tsa.setAveragingMethod(tsavg);
					}
				}

				else if (SerieFreqInd._MONTHLY_.equals(freqInd)) {
					if ("MT".equals(avgCode) || "QT".equals(avgCode)) {
						tsa.setAveragingMethod(tsavg);
					}
				}

				else if (SerieFreqInd._QUARTERLY_.equals(freqInd)) {
					if ("QT".equals(avgCode)) {
						tsa.setAveragingMethod(tsavg);
					}
				}

				if (tsa.getAveragingMethod() != null) {
					tsa.setTserie(ts);
					tsaList.add(tsa);
				}
			}
		}
		this.timeSerieAverageService.insert(tsaList);
	}

	@Override
	protected void postUpdate(TimeSerie e) throws BusinessException {
		super.postUpdate(e);
		if (e.getSerieType().equalsIgnoreCase(TimeSeriesType.ENERGYQUOTATION.getCode())) {
			this.updateEnergyQuotation(e);
		} else {
			this.updateExchangeRate(e);
		}
		this.updateCompositeTimeSeries(e);
	}

	private void updateExchangeRate(TimeSerie e) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("tserie", e);
		List<ExchangeRate> list = this.exchangeRateService.findEntitiesByAttributes(ExchangeRate.class, params);
		if (CollectionUtils.isEmpty(e.getRawTimeserieItems())) {
			this.exchangeRateService.delete(list);
		}
	}

	private void updateEnergyQuotation(TimeSerie e) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("timeseries", e);
		List<Quotation> list = this.quotationService.findEntitiesByAttributes(params);
		if (CollectionUtils.isEmpty(e.getRawTimeserieItems())) {
			this.quotationService.delete(list);
		}
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		for (Object id : ids) {
			TimeSerie ts = this.findById(id);
			this.deleteTimeSerieAverage(ts);
			this.deleteTimeSerieRawItems(ts);
			this.deleteTimeSerieItems(ts);
			if (ts.getSerieType().equalsIgnoreCase(TimeSeriesType.ENERGYQUOTATION.getCode())) {
				this.deleteQuotations((Integer) id);
			} else if (ts.getSerieType().equalsIgnoreCase(TimeSeriesType.EXCHANGERATE.getCode())) {
				this.deleteExchangeRates((Integer) id);
			}

		}
	}

	private void deleteTimeSerieRawItems(TimeSerie ts) throws BusinessException {
		this.rawTimeSerieItem.delete(new ArrayList<RawTimeSerieItem>(ts.getRawTimeserieItems()));
	}

	private void deleteTimeSerieItems(TimeSerie ts) throws BusinessException {
		this.timeSerieItemService.delete(new ArrayList<TimeSerieItem>(ts.getTimeserieItems()));
	}

	private void deleteExchangeRates(Integer id) throws BusinessException {
		List<ExchangeRate> exchangeRates = this.exchangeRateService.findByTserieId(id);
		this.exchangeRateService.delete(exchangeRates);
	}

	private void deleteQuotations(Integer id) throws BusinessException {
		List<Quotation> quotations = this.quotationService.findByTimeseriesId(id);
		this.quotationService.delete(quotations);
	}

	// TODO business is already done in IEntitityService.delete(list);
	private void deleteTimeSerieAverage(TimeSerie ts) throws BusinessException {
		this.timeSerieAverageService.delete(new ArrayList<TimeSerieAverage>(ts.getTimeserieAvg()));
	}

	private List<AverageMethod> getAllAvgMethod() throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("active", Boolean.TRUE);
		return this.findEntitiesByAttributes(AverageMethod.class, params);
	}

	@Override
	protected void preInsert(TimeSerie e) throws BusinessException {
		super.preInsert(e);
		this.checkAtInsertTimeSerieKey(e);
		this.validateTimeSerieUnit(e);
	}

	@Override
	protected void preUpdate(TimeSerie e) throws BusinessException {
		super.preUpdate(e);
		this.checkTimeSerieBusinessKey(e);
		this.validateTimeSerieUnit(e);
	}

	private void validateTimeSerieUnit(TimeSerie ts) throws BusinessException {
		if (ts.getUnitId() != null && ts.getUnit2Id() != null && ts.getUnitId().getUnittypeInd().equals(ts.getUnit2Id().getUnittypeInd())) {
			throw new BusinessException(BusinessErrorCode.TIME_SERIE_UNITS, BusinessErrorCode.TIME_SERIE_UNITS.getErrMsg());
		}
	}

	private void checkAtInsertTimeSerieKey(TimeSerie timeSerie) throws BusinessException {
		try {
			TimeSerie ts = this.findByName(timeSerie.getSerieName());
			if ((timeSerie.getId() == null) || (timeSerie.getSerieName().equals(ts.getSerieName()))) {
				throw new BusinessException(BusinessErrorCode.TS_DUPLICATE_ENTITY,
						String.format(BusinessErrorCode.TS_DUPLICATE_ENTITY.getErrMsg(), TimeSerie.class.getSimpleName(), "Serie name #"));
			}
		} catch (ApplicationException e) {
			if (e.getErrorCode() == J4eErrorCode.DB_NO_RESULT) {
				LOG.info("Time serie check key " + e.getMessage(), e);
			} else {
				throw e;
			}
		}
	}

	private void checkTimeSerieBusinessKey(TimeSerie timeSerie) throws BusinessException {
		try {
			TimeSerie ts = this.findByName(timeSerie.getSerieName());
			if ((timeSerie.getId() == null) || (timeSerie.getSerieName().equals(ts.getSerieName()) && !timeSerie.getId().equals(ts.getId()))) {
				throw new BusinessException(BusinessErrorCode.TS_DUPLICATE_ENTITY,
						String.format(BusinessErrorCode.TS_DUPLICATE_ENTITY.getErrMsg(), TimeSerie.class.getSimpleName(), "Serie name #"));
			}
		} catch (ApplicationException e) {
			if (e.getErrorCode() == J4eErrorCode.DB_NO_RESULT) {
				LOG.info("Time serie check key " + e.getMessage(), e);
			} else {
				throw e;
			}
		}
	}

	@Override
	@Transactional
	public void publish(List<TimeSerie> timeSeriesList) throws BusinessException {
		Publisher_Bd bd = this.getBusinessDelegate(Publisher_Bd.class);
		bd.publish(timeSeriesList);
	}

	/**
	 * Delete exchange rates for which the average method has been deactivated.
	 *
	 * @param timeSerie
	 * @throws BusinessException
	 */

	@Override
	@Transactional
	@ExceptionConverter(target = TimeSeriesException.class)
	public void deleteByIds(List<Object> ids) throws BusinessException {
		super.deleteByIds(ids);
	}

	@Override
	public TimeSerie getTimeSerie(List<Integer> usedIds) throws BusinessException {
		List<TimeSerie> timeSeries = this.getTimeseries(false);
		TimeSerie ts = this.filter(usedIds, timeSeries);
		if (ts == null) {
			timeSeries = this.getTimeseries(true);
			ts = this.filter(usedIds, timeSeries);
		}
		return ts;
	}

	private TimeSerie filter(List<Integer> usedIds, List<TimeSerie> timeSeries) throws BusinessException {
		for (TimeSerie timeSerie : timeSeries) {
			if (!usedIds.contains(timeSerie.getId()) && this.canPublish(timeSerie)) {
				return timeSerie;
			}
		}
		return null;
	}

	private List<TimeSerie> getTimeseries(boolean calculated) {
		String hql = "select e from %s e where e.clientId=:clientId and e.status=:status and e.dataProvider %s :provider";
		hql = calculated ? hql : hql + " and e.firstUsage IS NOT NULL and e.lastUsage IS NOT NULL";
		hql = String.format(hql, TimeSerie.class.getSimpleName(), calculated ? "=" : "!=");

		List<TimeSerie> timeSeries = this.getEntityManager().createQuery(hql, TimeSerie.class).setParameter("status", TimeSeriesStatus._WORK_)
				.setParameter("provider", DataProvider._CALCULATED_).setParameter("clientId", Session.user.get().getClientId()).getResultList();
		if (timeSeries.isEmpty()) {
			return Collections.emptyList();
		} else {
			return timeSeries;
		}
	}

	private boolean canPublish(TimeSerie ts) throws BusinessException {
		boolean useWorkflowExchangeRate = this.sysParamService.getExchangeRateApprovalWorkflow();
		boolean useWorkflowEnergyRate = this.sysParamService.getEnergyPriceApprovalWorkflow();

		if (!this.hasAveragingMethod(ts.getTimeserieAvg())) {
			return false;
		} else if (ts.getSerieType().equalsIgnoreCase(TimeSeriesType.ENERGYQUOTATION.getCode()) && useWorkflowEnergyRate
				&& !TimeSeriesApprovalStatus._APPROVED_.equals(ts.getApprovalStatus())) {
			return false;
		} else {
			return !(ts.getSerieType().equalsIgnoreCase(TimeSeriesType.EXCHANGERATE.getCode()) && useWorkflowExchangeRate
					&& !TimeSeriesApprovalStatus._APPROVED_.equals(ts.getApprovalStatus()));
		}

	}

	private boolean hasAveragingMethod(Collection<TimeSerieAverage> timeSerieAvg) {
		for (TimeSerieAverage avg : timeSerieAvg) {
			if (avg.getActive()) {
				return true;
			}
		}
		return false;
	}

	@Override
	@Transactional
	@History(type = TimeSerie.class)
	public void submitForApproval(TimeSerie e, TimeSeriesApprovalStatus status, @Action String action, @Reason String reason)
			throws BusinessException {
		e.setApprovalStatus(status);
		this.update(e);
	}

	@Override
	@Transactional
	@History(type = TimeSerie.class)
	public void approve(TimeSerie timeSerie, @Action String action, @Reason String reason) throws BusinessException {
		timeSerie.setApprovalStatus(TimeSeriesApprovalStatus._APPROVED_);
		this.update(timeSerie);

	}

	@Override
	@Transactional
	@History(type = TimeSerie.class)
	public void reject(TimeSerie timeSerie, @Action String action, @Reason String reason) throws BusinessException {
		timeSerie.setApprovalStatus(TimeSeriesApprovalStatus._REJECTED_);
		this.update(timeSerie);
	}

	@Override
	public Long countRawTimeSerieItems(TimeSerie e) throws BusinessException {
		String rawSql = "SELECT COUNT(e.id) FROM " + RawTimeSerieItem.class.getSimpleName()
				+ " e where e.clientId=:clientId and e.tserie.id=:timeSerieItem";
		return (Long) this.getEntityManager().createQuery(rawSql).setParameter("clientId", Session.user.get().getClient().getId())
				.setParameter("timeSerieItem", e.getId()).getSingleResult();
	}

}
