package atraxo.fmbas.business.ext.exceptions.mailMerge;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import seava.j4e.api.exceptions.BusinessException;

public class MailMergeServerDownException extends BusinessException {

	public MailMergeServerDownException(Throwable exception) {
		super(BusinessErrorCode.MAIL_MERGE_SERVER_DOWN, BusinessErrorCode.MAIL_MERGE_SERVER_DOWN.getErrMsg(), exception);

	}

}
