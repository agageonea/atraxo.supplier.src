package atraxo.fmbas.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;

public class NoAverageMethodException extends BusinessException {

	private static final long serialVersionUID = 8602108246915124723L;

	public NoAverageMethodException(String msg) {
		super(BusinessErrorCode.NO_AVERAGE_METHOD, msg);
	}
}
