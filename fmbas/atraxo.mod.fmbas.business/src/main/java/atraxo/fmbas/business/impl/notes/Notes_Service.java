/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.notes;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.notes.Notes;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Notes} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Notes_Service extends AbstractEntityService<Notes> {

	/**
	 * Public constructor for Notes_Service
	 */
	public Notes_Service() {
		super();
	}

	/**
	 * Public constructor for Notes_Service
	 * 
	 * @param em
	 */
	public Notes_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Notes> getEntityClass() {
		return Notes.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Notes
	 */
	public Notes findById(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Notes.NQ_FIND_BY_ID, Notes.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Notes", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Notes", "id"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Notes
	 */
	public Notes findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Notes.NQ_FIND_BY_BUSINESS, Notes.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Notes", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Notes", "id"), nure);
		}
	}

}
