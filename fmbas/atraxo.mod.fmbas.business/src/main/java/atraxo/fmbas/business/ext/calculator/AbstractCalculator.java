package atraxo.fmbas.business.ext.calculator;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.timeserie.ITimeSerieItemService;
import atraxo.fmbas.business.ext.calculator.model.Result;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Abstract calculator. Contains general methods available for all type of calculators.
 *
 * @author zspeter
 */
public abstract class AbstractCalculator implements Calculator {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCalculator.class);

	@Override
	public List<TimeSerieItem> getItemsForProvisioning(List<TimeSerieItem> tsiList, ITimeSerieItemService service) throws BusinessException {
		Set<TimeSerieItem> tsiSet = new HashSet<>(tsiList);
		Date now = new Date();
		Collections.sort(tsiList, new TimeSerieItemDateComparator());
		try {
			TimeSerieItem lastItem = tsiList.get(tsiList.size() - 1);
			// item date is not in the past
			if (!lastItem.getItemDate().before(now)) {
				Date endDate = this.getEndDate(lastItem.getItemDate());
				tsiSet.addAll(service.getAllElementsBetweenDatesWithMargins(endDate, this.getProvisioningIntervalEndDate(endDate),
						lastItem.getTserie().getId(), false));
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			// tsiList is empty occurs when all elements for a calculation
			// periods are deleted
			LOGGER.info("Got an ArrayIndexOutOfBoundsException because all elements for a calculation periods are deleted, will do nothing!", e);
		}
		return new ArrayList<>(tsiSet);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Result> calculate(List<TimeSerieItem> timeSerieItems, List<TimeSerieItem> itemsForProvisioning) throws BusinessException {
		Collections.sort(timeSerieItems, new TimeSerieItemDateComparator());
		return this.calculateAverages(timeSerieItems, itemsForProvisioning);
	}

	/**
	 * For all values from <code>timeSerieItems</code> calculates calculates average method period and average for the calculates period.
	 *
	 * @param timeSerieItems
	 * @return
	 */
	private List<Result> calculateAverages(List<TimeSerieItem> timeSerieItems, List<TimeSerieItem> itemsForProvisioning) {
		List<Result> resultList = new ArrayList<>();
		List<BigDecimal> values = new ArrayList<>();

		Date startDate = null;
		Date endDate = null;
		TimeSerie timeserie = null;
		boolean provisioned = true;
		TimeSerieItem tsiLast = null;
		for (TimeSerieItem timeSerieItem : timeSerieItems) {
			if (timeserie == null) {
				timeserie = timeSerieItem.getTserie();
			}
			if (endDate != null && this.shouldCalculateAverage(endDate, timeSerieItem)) {
				provisioned = this.isProvisioned(tsiLast, itemsForProvisioning);
				resultList.add(this.calculateAverage(startDate, endDate, values, provisioned));
				values.clear();
			}
			if (values.isEmpty()) {
				startDate = this.calculateAveragePeriodStartDate(timeSerieItem.getItemDate());
				endDate = this.calculateAveragePeriodEndDate(startDate);

			}
			Calendar cal = Calendar.getInstance();
			cal.setTime(timeSerieItem.getItemDate());
			values.add(timeSerieItem.getItemValue());
			tsiLast = timeSerieItem;
		}
		if (!values.isEmpty()) {
			provisioned = this.isProvisioned(tsiLast, itemsForProvisioning);
			resultList.add(this.calculateAverage(startDate, endDate, values, provisioned));
			values.clear();

		}
		return resultList;
	}

	/**
	 * @param endDate
	 * @param timeSerieItem
	 * @return
	 */
	protected boolean shouldCalculateAverage(Date endDate, TimeSerieItem timeSerieItem) {
		return endDate.before(timeSerieItem.getItemDate());
	}

	/**
	 * Determine provisioned flag for the from time series item <code>timeSerieItem</code>. If time series item is on week-end than return the
	 * previous calculated value.
	 *
	 * @param timeSerieItem
	 * @param provisioned
	 * @return
	 */
	private boolean isProvisioned(TimeSerieItem timeSerieItem, List<TimeSerieItem> itemList) {
		boolean provisioned = true;
		Calendar cal = Calendar.getInstance();
		Date endDate = this.getEndDate(timeSerieItem.getItemDate());
		cal.setTime(endDate);
		int counter = 3;
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			counter = 4;
		}
		cal.setTime(timeSerieItem.getItemDate());
		if (this.isIntervalCompleted(endDate)) {
			provisioned = false;
		} else {
			if (this.isLastItemNotCalculated(timeSerieItem, itemList, endDate)) {
				provisioned = false;
			} else {
				Date provDetEndDate = DateUtils.addDays(endDate, counter);
				for (TimeSerieItem tsi : itemList) {
					if (counter <= 0) {
						break;
					}
					if (tsi.getItemDate().after(endDate) && tsi.getItemDate().before(provDetEndDate)) {
						if (!tsi.getCalculated()) {
							provisioned = false;
							break;
						} else {
							counter--;
						}
					}
				}
			}
		}
		return provisioned;
	}

	/**
	 * @param tsi Last time series item took in consideration by the calculator for average calculation.
	 * @param itemList List of time series item.
	 * @param date Calculation period end date.
	 * @return true - if the last time series item <code>tsi</code> took in consideration is not calculated. Or if last time series item date is on
	 *         week-end the item from the previous Friday is took in consideration, false otherwise.
	 */
	private boolean isLastItemNotCalculated(TimeSerieItem tsi, List<TimeSerieItem> itemList, Date date) {
		Calendar dateCal = Calendar.getInstance();
		dateCal.setTime(date);
		if ((dateCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) || (dateCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)) {
			this.moveToPrevFriday(dateCal);
		}
		if (DateUtils.isSameDay(tsi.getItemDate(), dateCal.getTime())) {
			return !tsi.getCalculated();
		}
		if (tsi.getItemDate().after(dateCal.getTime())) {
			return this.isTimeSerieItemForPrevFridayNotCalculated(itemList, dateCal.getTime());
		}
		return false;
	}

	private boolean isTimeSerieItemForPrevFridayNotCalculated(List<TimeSerieItem> itemList, Date date) {
		for (TimeSerieItem timeSerieItem : itemList) {
			if (DateUtils.isSameDay(date, timeSerieItem.getItemDate())) {
				return !timeSerieItem.getCalculated();
			}
		}
		return false;
	}

	private void moveToPrevFriday(Calendar dateCal) {
		do {
			dateCal.add(Calendar.DAY_OF_MONTH, -1);
		} while (dateCal.get(Calendar.DAY_OF_WEEK) != Calendar.FRIDAY);
	}

	/**
	 * Verifies if the interval for which and average will be calculated is completed or not.
	 *
	 * @param date
	 * @return true if endDate is before current date minus 3 days or minus 4 days if current date is Saturday, false otherwise.
	 */
	private boolean isIntervalCompleted(Date date) {
		Date endDate = this.getProvisioningIntervalEndDate(date);
		return new Date().after(endDate);
	}

	private Date getProvisioningIntervalEndDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			cal.add(Calendar.DAY_OF_MONTH, 4);
		} else {
			cal.add(Calendar.DAY_OF_MONTH, 3);
		}
		return cal.getTime();
	}

	/**
	 * Determine from time series item <code>timeSerieItem</code> calculator period end date.
	 *
	 * @param timeSerieItem
	 * @return
	 */
	protected abstract Date getEndDate(Date itemDate);

	/**
	 * Calculate average for <code>values</code> for the specified period by <code>fromDate</code> and <code>toDate</code>.
	 *
	 * @param fromDate
	 * @param toDate
	 * @param values
	 * @param provisioned
	 * @return
	 */
	private Result calculateAverage(Date fromDate, Date toDate, List<BigDecimal> values, boolean provisioned) {
		BigDecimal sum = BigDecimal.ZERO;
		int itemNumbers = 0;
		for (BigDecimal value : values) {
			sum = sum.add(value);
			itemNumbers++;
		}
		BigDecimal avg = sum.divide(BigDecimal.valueOf(itemNumbers), MathContext.DECIMAL64);
		Result result = new Result();
		result.setRate(avg.setScale(6, RoundingMode.HALF_UP));
		result.setValidFromDate((Date) fromDate.clone());
		result.setValidToDate((Date) toDate.clone());
		result.setProvisioned(provisioned);
		return result;
	}

	/**
	 * Abstract method to calculate average calculator start date.
	 *
	 * @param date
	 * @return
	 */
	protected abstract Date calculateAveragePeriodStartDate(Date date);

	/**
	 * Abstract method to calculate average calculator end date.
	 *
	 * @param startDate
	 * @param timeSerie TODO
	 * @return
	 */
	protected abstract Date calculateAveragePeriodEndDate(Date startDate);

}
