package atraxo.fmbas.business.ext.externalInterfaces.processor;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import atraxo.ad.business.api.system.IClientService;
import atraxo.ad.domain.impl.system.Client;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.externalInterfaces.IIncommingMessageService;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceType;
import seava.j4e.api.ISettings;
import seava.j4e.api.enums.SysParam;
import seava.j4e.api.exceptions.InvalidConfiguration;
import seava.j4e.api.service.IClientInfoProvider;
import seava.j4e.api.session.IClient;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.IUserProfile;
import seava.j4e.api.session.IUserSettings;
import seava.j4e.api.session.IWorkspace;
import seava.j4e.api.session.Session;
import seava.j4e.business.AbstractApplicationContextAware;
import seava.j4e.commons.security.AppClient;
import seava.j4e.commons.security.AppUser;
import seava.j4e.commons.security.AppUserProfile;
import seava.j4e.commons.security.AppUserSettings;

/**
 * Start a separate thread for each inbounds asynchronous interface.
 *
 * @author zspeter
 */
@EnableScheduling
public class AsyncMessageProcessorManager extends AbstractApplicationContextAware implements ApplicationListener<ApplicationContextEvent> {

	private static final Logger LOG = LoggerFactory.getLogger(AsyncMessageProcessorManager.class);

	@Autowired
	private IClientService clientSrv;
	@Autowired
	private IExternalInterfaceService extIntSrv;
	@Autowired
	private IIncommingMessageService msgSrv;
	@Autowired
	private TaskExecutor taskExecutor;
	@Autowired
	private ApplicationEventPublisher publisher;

	private Set<String> startedThreads = new HashSet<>();

	// 1000 * 60 * 30 = 1/2 hour
	// @Scheduled(fixedRate = 1800000)
	@Scheduled(fixedRate = 30000)
	public void initIt() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Start threads for asynchronous processing.");
		}
		List<Client> clients = this.clientSrv.findEntitiesByAttributes(Collections.emptyMap());
		Map<String, Object> params = new HashMap<>();
		params.put("enabled", true);
		params.put("externalInterfaceType", ExternalInterfaceType._INBOUND_);
		try {
			for (Client client : clients) {
				this.createSessionUser(client.getId(), client.getCode());
				List<ExternalInterface> extInterfaces = this.extIntSrv.findEntitiesByAttributes(params);
				for (ExternalInterface extInt : extInterfaces) {
					synchronized (this.startedThreads) {
						if (extInt.getAsync() && !this.startedThreads.contains(client.getCode() + extInt.getName())) {
							if (LOG.isDebugEnabled()) {
								LOG.debug("start threads for asynchronous processing for interface {}.", extInt.getName());
							}
							BufferedMessageProcessor messageProcessor = new BufferedMessageProcessor(this.msgSrv, this.getApplicationContext(),
									this.publisher, extInt);
							this.taskExecutor.execute(messageProcessor);
							this.startedThreads.add(client.getCode() + extInt.getName());
						}
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Stupid programmer", e);
		}
	}

	private void createSessionUser(String clientId, String clientCode) throws InvalidConfiguration {
		ISettings settings = this.getApplicationContext().getBean(ISettings.class);
		String userCode = settings.getParam(SysParam.CORE_JOB_USER.name());
		//
		IClient client = new AppClient(clientId, clientCode, "");
		IUserSettings userSettings = AppUserSettings.newInstance(settings);
		IUserProfile profile = new AppUserProfile(true, null, false, false, false);

		// create an incomplete user first
		IUser user = new AppUser(userCode, userCode, null, userCode, null, null, client, userSettings, profile, null, true);
		Session.user.set(user);

		// get the client workspace info
		IWorkspace ws = this.getApplicationContext().getBean(IClientInfoProvider.class).getClientWorkspace();
		user = new AppUser(userCode, userCode, null, userCode, null, null, client, userSettings, profile, ws, true);
		Session.user.set(user);
	}

	@Override
	public void onApplicationEvent(ApplicationContextEvent event) {
		if (event instanceof ContextClosedEvent) {
			((ThreadPoolTaskExecutor) this.taskExecutor).shutdown();
		}
		if (event instanceof ContextStartedEvent) {
			this.initIt();
		}
		if (event instanceof EndProcessorEvent) {
			EndProcessorEvent endProcessorEvent = (EndProcessorEvent) event;
			synchronized (this.startedThreads) {
				this.startedThreads.remove(endProcessorEvent.getClientCode() + endProcessorEvent.getExtInt().getName());
			}
		}

	}

}
