/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.aircraft;

import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.customer.Customer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Aircraft} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Aircraft_Service extends AbstractEntityService<Aircraft> {

	/**
	 * Public constructor for Aircraft_Service
	 */
	public Aircraft_Service() {
		super();
	}

	/**
	 * Public constructor for Aircraft_Service
	 * 
	 * @param em
	 */
	public Aircraft_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Aircraft> getEntityClass() {
		return Aircraft.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Aircraft
	 */
	public Aircraft findByReg(Customer customer, String registration) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Aircraft.NQ_FIND_BY_REG, Aircraft.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("customer", customer)
					.setParameter("registration", registration)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Aircraft", "customer, registration"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Aircraft", "customer, registration"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Aircraft
	 */
	public Aircraft findByReg(Long customerId, String registration) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Aircraft.NQ_FIND_BY_REG_PRIMITIVE,
							Aircraft.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("customerId", customerId)
					.setParameter("registration", registration)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Aircraft", "customerId, registration"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Aircraft", "customerId, registration"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Aircraft
	 */
	public Aircraft findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Aircraft.NQ_FIND_BY_BUSINESS,
							Aircraft.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Aircraft", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Aircraft", "id"), nure);
		}
	}

	/**
	 * Find by reference: acTypes
	 *
	 * @param acTypes
	 * @return List<Aircraft>
	 */
	public List<Aircraft> findByAcTypes(AcTypes acTypes) {
		return this.findByAcTypesId(acTypes.getId());
	}
	/**
	 * Find by ID of reference: acTypes.id
	 *
	 * @param acTypesId
	 * @return List<Aircraft>
	 */
	public List<Aircraft> findByAcTypesId(Integer acTypesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Aircraft e where e.clientId = :clientId and e.acTypes.id = :acTypesId",
						Aircraft.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("acTypesId", acTypesId).getResultList();
	}
	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<Aircraft>
	 */
	public List<Aircraft> findByCustomer(Customer customer) {
		return this.findByCustomerId(customer.getId());
	}
	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<Aircraft>
	 */
	public List<Aircraft> findByCustomerId(Integer customerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Aircraft e where e.clientId = :clientId and e.customer.id = :customerId",
						Aircraft.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerId", customerId).getResultList();
	}
	/**
	 * Find by reference: owner
	 *
	 * @param owner
	 * @return List<Aircraft>
	 */
	public List<Aircraft> findByOwner(Customer owner) {
		return this.findByOwnerId(owner.getId());
	}
	/**
	 * Find by ID of reference: owner.id
	 *
	 * @param ownerId
	 * @return List<Aircraft>
	 */
	public List<Aircraft> findByOwnerId(Integer ownerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Aircraft e where e.clientId = :clientId and e.owner.id = :ownerId",
						Aircraft.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("ownerId", ownerId).getResultList();
	}
	/**
	 * Find by reference: operator
	 *
	 * @param operator
	 * @return List<Aircraft>
	 */
	public List<Aircraft> findByOperator(Customer operator) {
		return this.findByOperatorId(operator.getId());
	}
	/**
	 * Find by ID of reference: operator.id
	 *
	 * @param operatorId
	 * @return List<Aircraft>
	 */
	public List<Aircraft> findByOperatorId(Integer operatorId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Aircraft e where e.clientId = :clientId and e.operator.id = :operatorId",
						Aircraft.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("operatorId", operatorId).getResultList();
	}
}
