package atraxo.fmbas.business.ext.calculator;

import java.util.Calendar;
import java.util.Date;

public abstract class AbstractSemiMonthlyCalculator extends AbstractCalculator implements Calculator {

	@Override
	protected Date calculateAveragePeriodEndDate(Date startDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		Calendar compareCal = (Calendar) cal.clone();
		compareCal.set(Calendar.DAY_OF_MONTH, 16);
		if (cal.before(compareCal)) {
			cal.set(Calendar.DAY_OF_MONTH, 15);
		} else {
			Integer lastDayOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			cal.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);
		}
		return cal.getTime();
	}

	@Override
	protected Date calculateAveragePeriodStartDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		Calendar compareCal = (Calendar) cal.clone();
		compareCal.set(Calendar.DAY_OF_MONTH, 16);
		if (cal.before(compareCal)) {
			cal.set(Calendar.DAY_OF_MONTH, 1);
		} else {
			cal.set(Calendar.DAY_OF_MONTH, 16);
		}
		return cal.getTime();
	}

	/**
	 * @param cal
	 * @param monthDay
	 */
	protected void changeCalToSpecifiedMonthDay(Calendar cal, int monthDay) {
		if (cal.get(Calendar.DAY_OF_MONTH) != monthDay) {
			cal.set(Calendar.DAY_OF_MONTH, monthDay);
		}
	}

	@Override
	protected Date getEndDate(Date itemDate) {
		Calendar cal = Calendar.getInstance();
		Calendar compareCal = (Calendar) cal.clone();
		compareCal.set(Calendar.DAY_OF_MONTH, 16);
		cal.setTime(itemDate);
		if (cal.before(compareCal)) {
			cal.set(Calendar.DAY_OF_MONTH, 15);
		} else {
			this.changeCalToSpecifiedMonthDay(cal, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		}
		return cal.getTime();
	}

}
