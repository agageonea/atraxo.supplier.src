package atraxo.fmbas.business.ext.exchangerate.job.importECB;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.persistence.OptimisticLockException;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.timeserie.IRawTimeSerieItemService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exchangerate.job.importECB.model.Cube;
import atraxo.fmbas.domain.ext.job.JobParamName;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.fmbas_type.DataProvider;
import atraxo.fmbas.domain.impl.fmbas_type.PostTypeInd;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesApprovalStatus;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesStatus;
import atraxo.fmbas.domain.impl.timeserie.RawTimeSerieItem;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author abolindu
 */
public class ImportExchangeRatesItemProcessor implements ItemProcessor<Cube, List<ExchangeRate>> {

	private static final String TAB = "\t";
	static final String LOG = "LOG";
	static final String CRLF = "\r\n";
	@Autowired
	private ITimeSerieService timeSerieService;
	@Autowired
	private ICurrenciesService currenciesService;
	@Autowired
	private IRawTimeSerieItemService rawTimeSerieItemService;

	private StepExecution stepExecution;

	private SimpleDateFormat ft = new SimpleDateFormat("MMM d, yyyy");
	private StringBuilder logBuilder = new StringBuilder();

	/**
	 * Store execution context;
	 *
	 * @param stepExecution
	 */
	@BeforeStep
	public void storeExecutionContext(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}

	@Override
	public List<ExchangeRate> process(Cube item) throws Exception {
		Currencies currency = this.currenciesService.findByCode(JobParamName.CURRENCY.getDbName());
		Map<String, Object> params = new HashMap<>();
		params.put("dataProvider", DataProvider._IMPORT_ECB_);
		List<TimeSerie> timeSeries = this.timeSerieService.findEntitiesByAttributes(params);

		String jobParameter = this.stepExecution.getJobParameters().getString(JobParamName.OVERWRITE.getDbName());

		if (timeSeries.isEmpty()) {
			this.stepExecution.getExecutionContext().put(JobParamName.NO_TIME_SERIES.getDbName(), "");
			return Collections.emptyList();
		}

		List<Cube> timeCubes = item.getCube();
		// order DESC by date
		Collections.sort(timeCubes, (o1, o2) -> o2.getTime().compareTo(o1.getTime()));

		for (TimeSerie timeSerie : timeSeries) {
			if (timeSerie.getPostTypeInd().equals(PostTypeInd._AMOUNT_) && timeSerie.getCurrency2Id().equals(currency)) {
				this.processTimeSerie(timeSerie, timeSerie.getCurrency1Id(), timeCubes, jobParameter);
			} else if (timeSerie.getPostTypeInd().equals(PostTypeInd._PRICE_) && timeSerie.getCurrency1Id().equals(currency)) {
				this.processTimeSerie(timeSerie, timeSerie.getCurrency2Id(), timeCubes, jobParameter);
			}
		}
		if (this.stepExecution.getExecutionContext().containsKey(LOG)) {
			StringBuilder sb = new StringBuilder(this.stepExecution.getExecutionContext().getString(LOG));
			sb.append(CRLF);
			sb.append(this.logBuilder.toString());
			this.stepExecution.getExecutionContext().put(LOG, sb.toString());

		} else {
			this.stepExecution.getExecutionContext().put(LOG, this.logBuilder.toString());
		}

		return Collections.emptyList();
	}

	/**
	 * Imports ExchangeRates for TimeSeries which Provider is ECB.
	 *
	 * @param timeSerie
	 * @param calculatedCurrency - reference currency
	 * @param timeCubes
	 * @param jobParameter - update/skip
	 * @throws BusinessException
	 */
	private void processTimeSerie(TimeSerie timeSerie, Currencies calculatedCurrency, List<Cube> timeCubes, String jobParameter)
			throws BusinessException {
		List<Cube> cubeList = new ArrayList<>(timeCubes);
		this.logBuilder.append(timeSerie.getSerieName()).append(CRLF);
		this.logProcessedDays(timeCubes);

		List<RawTimeSerieItem> rawTimeSerieItems = (List<RawTimeSerieItem>) timeSerie.getRawTimeserieItems();
		// order DESC by date
		Collections.sort(rawTimeSerieItems, (o1, o2) -> o2.getItemDate().compareTo(o1.getItemDate()));

		this.logBuilder.append(TAB).append(BusinessErrorCode.IMPORT_ER_TOTAL_PROCESSED.getErrMsg()).append(timeCubes.size()).append(CRLF);
		this.update(cubeList, rawTimeSerieItems, calculatedCurrency, jobParameter, timeSerie);
		this.insert(cubeList, calculatedCurrency, timeSerie);

		timeSerie.setApprovalStatus(TimeSeriesApprovalStatus._APPROVED_);
		this.timeSerieService.updateStatus(timeSerie, TimeSeriesStatus._WORK_);
	}

	/**
	 * Updates or skip (depending on jobParameter) values of RawTimeSerieItem which allready exists. Values for RawTimeSerieItems are from cubeList.
	 *
	 * @param cubeList
	 * @param rawTimeSerieItems
	 * @param calculatedCurrency - reference currency
	 * @param jobParameter - update/skip
	 * @throws BusinessException
	 */
	private void update(List<Cube> cubeList, List<RawTimeSerieItem> rawTimeSerieItems, Currencies calculatedCurrency, String jobParameter,
			TimeSerie timeSerie) throws BusinessException {
		List<RawTimeSerieItem> updateList = new ArrayList<>();
		List<RawTimeSerieItem> skipedList = new ArrayList<>();

		ListIterator<Cube> iterator = cubeList.listIterator();
		while (iterator.hasNext()) {
			Cube timeCube = iterator.next();
			for (RawTimeSerieItem rawTimeSerieItem : rawTimeSerieItems) {
				if (rawTimeSerieItem.getItemDate().after(timeCube.getTime())) {
					continue;
				}
				if (timeCube.getTime().equals(rawTimeSerieItem.getItemDate())) {
					this.proceeeCurrencyCube(calculatedCurrency, jobParameter, updateList, skipedList, timeCube, rawTimeSerieItem);
					iterator.remove();
				}
			}
		}

		if (!rawTimeSerieItems.isEmpty()) {
			try {
				this.rawTimeSerieItemService.update(updateList);
			} catch (OptimisticLockException e) {
				throw new BusinessException(BusinessErrorCode.OPTIMISTIC_LOCK_TIME_SERIE_ECB_JOB,
						String.format(BusinessErrorCode.OPTIMISTIC_LOCK_TIME_SERIE_ECB_JOB.getErrMsg(), timeSerie.getSerieName()), e);
			}
		}
		this.logBuilder.append(TAB).append(BusinessErrorCode.IMPORT_ER_SKIPPED.getErrMsg()).append(skipedList.size()).append(CRLF);
		this.logBuilder.append(TAB).append(BusinessErrorCode.IMPORT_ER_UPDATED.getErrMsg()).append(updateList.size()).append(CRLF);
	}

	private void proceeeCurrencyCube(Currencies calculatedCurrency, String jobParameter, List<RawTimeSerieItem> updateList,
			List<RawTimeSerieItem> skipedList, Cube timeCube, RawTimeSerieItem rawTimeSerieItem) {
		for (Cube currencyCube : timeCube.getCube()) {
			if (currencyCube.getCurrency().equals(calculatedCurrency.getCode())) {
				if (jobParameter.equalsIgnoreCase(JobParamName.UPDATE.getDbName())) {
					rawTimeSerieItem.setItemValue(currencyCube.getRate());
					updateList.add(rawTimeSerieItem);
				} else {
					skipedList.add(rawTimeSerieItem);
				}
			}
		}
	}

	/**
	 * Inserts RawTimeSerieItems for specified TimeSerie. Values for RawTimeSerieItems are from cubeList.
	 *
	 * @param cubeList
	 * @param calculatedCurrency - reference currency
	 * @param timeSerie
	 * @throws BusinessException
	 */
	private void insert(List<Cube> cubeList, Currencies calculatedCurrency, TimeSerie timeSerie) throws BusinessException {
		List<RawTimeSerieItem> insertList = new ArrayList<>();

		for (Cube timeCube : cubeList) {
			for (Cube currencyCube : timeCube.getCube()) {
				if (currencyCube.getCurrency().equals(calculatedCurrency.getCode())) {
					RawTimeSerieItem exchangeRate = new RawTimeSerieItem();
					exchangeRate.setTserie(timeSerie);
					exchangeRate.setItemDate(timeCube.getTime());
					exchangeRate.setItemValue(currencyCube.getRate());
					exchangeRate.setCalculated(false);
					exchangeRate.setActive(true);
					exchangeRate.setTransfered(false);
					exchangeRate.setTransferedItemUpdate(false);
					exchangeRate.setUpdated(false);
					insertList.add(exchangeRate);
				}
			}
		}

		if (!insertList.isEmpty()) {
			this.rawTimeSerieItemService.insert(insertList);
		}
		this.logBuilder.append(TAB).append(BusinessErrorCode.IMPORT_ER_IMPORTED.getErrMsg()).append(insertList.size()).append(CRLF);

	}

	/**
	 * Logs in job history the period of processed and imported RawTimeSerieItems.
	 *
	 * @param timeCubes
	 */
	private void logProcessedDays(List<Cube> timeCubes) {
		this.logBuilder.append(TAB).append(BusinessErrorCode.IMPORT_ER_DAYS_PROCESSED.getErrMsg());
		if (timeCubes.size() > 1) {
			this.logBuilder.append(this.ft.format(timeCubes.get(timeCubes.size() - 1).getTime())).append(" - ")
					.append(this.ft.format(timeCubes.get(0).getTime()));
		} else {
			this.logBuilder.append(this.ft.format(timeCubes.get(0).getTime()));
		}
		this.logBuilder.append(CRLF);
	}

}
