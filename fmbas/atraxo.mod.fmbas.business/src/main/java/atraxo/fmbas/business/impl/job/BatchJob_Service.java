/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.job;

import atraxo.fmbas.business.api.job.IBatchJobService;
import atraxo.fmbas.domain.impl.job.BatchJob;
import atraxo.fmbas.domain.impl.job.BatchJobParameter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link BatchJob} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class BatchJob_Service extends AbstractEntityService<BatchJob>
		implements
			IBatchJobService {

	/**
	 * Public constructor for BatchJob_Service
	 */
	public BatchJob_Service() {
		super();
	}

	/**
	 * Public constructor for BatchJob_Service
	 * 
	 * @param em
	 */
	public BatchJob_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<BatchJob> getEntityClass() {
		return BatchJob.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return BatchJob
	 */
	public BatchJob findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(BatchJob.NQ_FIND_BY_NAME, BatchJob.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"BatchJob", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"BatchJob", "name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return BatchJob
	 */
	public BatchJob findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(BatchJob.NQ_FIND_BY_BUSINESS,
							BatchJob.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"BatchJob", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"BatchJob", "id"), nure);
		}
	}

	/**
	 * Find by reference: jobParameters
	 *
	 * @param jobParameters
	 * @return List<BatchJob>
	 */
	public List<BatchJob> findByJobParameters(BatchJobParameter jobParameters) {
		return this.findByJobParametersId(jobParameters.getId());
	}
	/**
	 * Find by ID of reference: jobParameters.id
	 *
	 * @param jobParametersId
	 * @return List<BatchJob>
	 */
	public List<BatchJob> findByJobParametersId(Integer jobParametersId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from BatchJob e, IN (e.jobParameters) c where e.clientId = :clientId and c.id = :jobParametersId",
						BatchJob.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("jobParametersId", jobParametersId)
				.getResultList();
	}
}
