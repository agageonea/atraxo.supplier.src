/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.user;

import atraxo.fmbas.business.api.user.IUserSubsidiaryLovService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.user.UserSubsidiaryLov;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link UserSubsidiaryLov} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class UserSubsidiaryLov_Service
		extends
			AbstractEntityService<UserSubsidiaryLov>
		implements
			IUserSubsidiaryLovService {

	/**
	 * Public constructor for UserSubsidiaryLov_Service
	 */
	public UserSubsidiaryLov_Service() {
		super();
	}

	/**
	 * Public constructor for UserSubsidiaryLov_Service
	 * 
	 * @param em
	 */
	public UserSubsidiaryLov_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<UserSubsidiaryLov> getEntityClass() {
		return UserSubsidiaryLov.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return UserSubsidiaryLov
	 */
	public UserSubsidiaryLov findByKey(UserSupp user, Customer subsidiary) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(UserSubsidiaryLov.NQ_FIND_BY_KEY,
							UserSubsidiaryLov.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("user", user)
					.setParameter("subsidiary", subsidiary).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"UserSubsidiaryLov", "user, subsidiary"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"UserSubsidiaryLov", "user, subsidiary"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return UserSubsidiaryLov
	 */
	public UserSubsidiaryLov findByKey(Long userId, Long subsidiaryId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							UserSubsidiaryLov.NQ_FIND_BY_KEY_PRIMITIVE,
							UserSubsidiaryLov.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("userId", userId)
					.setParameter("subsidiaryId", subsidiaryId)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"UserSubsidiaryLov", "userId, subsidiaryId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"UserSubsidiaryLov", "userId, subsidiaryId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return UserSubsidiaryLov
	 */
	public UserSubsidiaryLov findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(UserSubsidiaryLov.NQ_FIND_BY_BUSINESS,
							UserSubsidiaryLov.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"UserSubsidiaryLov", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"UserSubsidiaryLov", "id"), nure);
		}
	}

	/**
	 * Find by reference: user
	 *
	 * @param user
	 * @return List<UserSubsidiaryLov>
	 */
	public List<UserSubsidiaryLov> findByUser(UserSupp user) {
		return this.findByUserId(user.getId());
	}
	/**
	 * Find by ID of reference: user.id
	 *
	 * @param userId
	 * @return List<UserSubsidiaryLov>
	 */
	public List<UserSubsidiaryLov> findByUserId(String userId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from UserSubsidiaryLov e where e.clientId = :clientId and e.user.id = :userId",
						UserSubsidiaryLov.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("userId", userId).getResultList();
	}
	/**
	 * Find by reference: subsidiary
	 *
	 * @param subsidiary
	 * @return List<UserSubsidiaryLov>
	 */
	public List<UserSubsidiaryLov> findBySubsidiary(Customer subsidiary) {
		return this.findBySubsidiaryId(subsidiary.getId());
	}
	/**
	 * Find by ID of reference: subsidiary.id
	 *
	 * @param subsidiaryId
	 * @return List<UserSubsidiaryLov>
	 */
	public List<UserSubsidiaryLov> findBySubsidiaryId(Integer subsidiaryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from UserSubsidiaryLov e where e.clientId = :clientId and e.subsidiary.id = :subsidiaryId",
						UserSubsidiaryLov.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("subsidiaryId", subsidiaryId).getResultList();
	}
}
