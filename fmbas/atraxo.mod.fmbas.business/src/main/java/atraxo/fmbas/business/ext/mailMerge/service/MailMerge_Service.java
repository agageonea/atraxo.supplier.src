package atraxo.fmbas.business.ext.mailMerge.service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.soap.MTOMFeature;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fuelplus.mailmerge.ws.ApplicationFault;
import com.fuelplus.mailmerge.ws.Attachment;
import com.fuelplus.mailmerge.ws.Client;
import com.fuelplus.mailmerge.ws.ClientAlreadyExistFault;
import com.fuelplus.mailmerge.ws.ClientContext;
import com.fuelplus.mailmerge.ws.ClientNotFoundFault;
import com.fuelplus.mailmerge.ws.CreateDocumentRequest;
import com.fuelplus.mailmerge.ws.CreateDocumentsRequest;
import com.fuelplus.mailmerge.ws.FileAttachment;
import com.fuelplus.mailmerge.ws.MailMergeWsService;
import com.fuelplus.mailmerge.ws.MailMergeWsServiceImplService;
import com.fuelplus.mailmerge.ws.SendEmailRequest;
import com.fuelplus.mailmerge.ws.SendEmailResponse.Return;
import com.fuelplus.mailmerge.ws.SendEmailResponse.Return.Entry;
import com.fuelplus.mailmerge.ws.Template;
import com.fuelplus.mailmerge.ws.TemplateAlreadyExistFault;
import com.fuelplus.mailmerge.ws.TemplateNotFoundFault;
import com.fuelplus.mailmerge.ws.UpdateTemplateRequest;
import com.fuelplus.mailmerge.ws.UploadTemplateRequest;
import com.fuelplus.mailmerge.ws.ValidationFault;

import atraxo.fmbas.business.ext.exceptions.mailMerge.MailMergeApplicationFaulExeption;
import atraxo.fmbas.business.ext.exceptions.mailMerge.MailMergeClientNotExistsExeption;
import atraxo.fmbas.business.ext.exceptions.mailMerge.MailMergeServerDownException;
import atraxo.fmbas.business.ext.exceptions.mailMerge.MailMergeTemplateAlreadyExistsExeption;
import atraxo.fmbas.business.ext.exceptions.mailMerge.MailMergeTemplateNotExistsExeption;
import atraxo.fmbas.business.ext.exceptions.mailMerge.MailMergeValidationFaultExeption;
import atraxo.fmbas.business.ext.mailMerge.delegate.ClientContextManager;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * @author apetho
 */
public class MailMerge_Service extends AbstractBusinessBaseService {

	private MailMergeWsService ws;

	private String serverAddress;

	private static final Logger LOG = LoggerFactory.getLogger(MailMerge_Service.class);

	/**
	 * @param url - {@link String}
	 * @throws BusinessException
	 */
	public MailMerge_Service(String url) throws BusinessException {
		super();
		this.serverAddress = url;
		try {
			this.ws = this.getMailMergeWsService(url);
		} catch (Exception e) {
			LOG.warn("Mail merge srvice url is invalid or the server is stoped!", e);
		}
	}

	/**
	 * Create a client on mail merge service.
	 *
	 * @return - true if the client is created.
	 * @throws MailMergeApplicationFaulExeption
	 * @throws MailMergeValidationFaultExeption
	 */
	public boolean createClient() throws BusinessException {
		try {
			LOG.info("Trying to create mail merge client.");
			if (this.ws == null) {
				this.ws = this.getMailMergeWsService(this.serverAddress);
			}
			Client client = this.ws.createClient(ClientContextManager.getClientContext(Session.user.get().getClient().getId()));
			return client != null;
		} catch (ApplicationFault | NullPointerException e) {
			throw new MailMergeApplicationFaulExeption(e);
		} catch (ValidationFault e) {
			throw new MailMergeValidationFaultExeption(e);
		} catch (ClientAlreadyExistFault e) {
			LOG.info("Client already exists !", e);
			return true;
		}
	}

	/**
	 * @param file - file content byte array.
	 * @param fileName - {@link String} File name.
	 * @param description {@link String} Description.
	 * @return - true if is uploaded successfully.
	 * @throws MailMergeApplicationFaulExeption
	 * @throws MailMergeValidationFaultExeption
	 * @throws MailMergeClientNotExistsExeption
	 * @throws MailMergeTemplateAlreadyExistsExeption
	 */
	public boolean uploadTemplate(byte[] file, String fileName, String description) throws BusinessException {
		LOG.info("Trying to upload a new template.");
		UploadTemplateRequest request = new UploadTemplateRequest();
		request.setClientContext(ClientContextManager.getClientContext(Session.user.get().getClient().getId()));
		Template template = this.createTemplate(fileName, description);
		request.setTemplate(template);
		request.setFilecontent(file);
		this.createClient();
		try {
			if (this.ws == null) {
				this.ws = this.getMailMergeWsService(this.serverAddress);
			}
			this.ws.createTemplate(request);
			return true;
		} catch (ApplicationFault e) {
			throw new MailMergeApplicationFaulExeption(e);
		} catch (ValidationFault e) {
			throw new MailMergeValidationFaultExeption(e);
		} catch (ClientNotFoundFault e) {
			throw new MailMergeClientNotExistsExeption(e);
		} catch (TemplateAlreadyExistFault e) {
			throw new MailMergeTemplateAlreadyExistsExeption(e);
		}
	}

	/**
	 * Create a new template.
	 *
	 * @param fileName
	 * @param description
	 * @return
	 */
	private Template createTemplate(String fileName, String description) {
		Template template = new Template();
		template.setFilename(fileName);
		template.setDescription(description);
		return template;
	}

	/**
	 * @param file - File in byte array.
	 * @param fileName - {@link String} File name.
	 * @param description - {@link String} Description.
	 * @throws MailMergeApplicationFaulExeption
	 * @throws MailMergeValidationFaultExeption
	 * @throws MailMergeTemplateNotExistsExeption
	 * @throws MailMergeClientNotExistsExeption
	 */
	public void updateTemplate(byte[] file, String fileName, String description) throws BusinessException {
		LOG.info("Trying to upload a new template.");
		UpdateTemplateRequest request = new UpdateTemplateRequest();
		request.setClientContext(ClientContextManager.getClientContext(Session.user.get().getClient().getId()));
		Template template = this.createTemplate(fileName, description);
		request.setTemplate(template);
		request.setFilecontent(file);
		this.createClient();
		try {
			if (this.ws == null) {
				this.ws = this.getMailMergeWsService(this.serverAddress);
			}
			this.ws.updateTemplate(request);
		} catch (ApplicationFault e) {
			throw new MailMergeApplicationFaulExeption(e);
		} catch (ValidationFault e) {
			throw new MailMergeValidationFaultExeption(e);
		} catch (TemplateNotFoundFault e) {
			throw new MailMergeTemplateNotExistsExeption(e);
		} catch (ClientNotFoundFault e) {
			throw new MailMergeClientNotExistsExeption(e);
		}
	}

	/**
	 * @param fileName - {@link String} Reference file name.
	 * @param jsonStr - {@link String} Json string.
	 * @return - The generated file in byte array.
	 * @throws MailMergeApplicationFaulExeption
	 * @throws MailMergeValidationFaultExeption
	 * @throws MailMergeTemplateNotExistsExeption
	 * @throws MailMergeClientNotExistsExeption
	 */
	public byte[] generateDocument(String fileName, String jsonStr) throws BusinessException {
		LOG.info("Trying generate a document.");
		CreateDocumentRequest request = this.generateDocumentRequest(fileName, jsonStr);
		this.createClient();
		try {
			if (this.ws == null) {
				this.ws = this.getMailMergeWsService(this.serverAddress);
			}
			return this.ws.createAndDownloadDocument(request);
		} catch (ApplicationFault e) {
			throw new MailMergeApplicationFaulExeption(e);
		} catch (ValidationFault e) {
			throw new MailMergeValidationFaultExeption(e);
		} catch (TemplateNotFoundFault e) {
			throw new MailMergeTemplateNotExistsExeption(e);
		} catch (ClientNotFoundFault e) {
			throw new MailMergeClientNotExistsExeption(e);
		}
	}

	/**
	 * @param fileName
	 * @param jsonStr
	 * @param template - {@link Template} Document template.
	 * @return - The generated file in byte array.
	 * @throws MailMergeApplicationFaulExeption
	 * @throws MailMergeValidationFaultExeption
	 * @throws MailMergeClientNotExistsExeption
	 * @throws MailMergeTemplateNotExistsExeption
	 */
	public byte[] generateDocumentFromTemplate(String fileName, String jsonStr, atraxo.fmbas.domain.impl.templates.Template template)
			throws BusinessException {
		LOG.info("Trying generate a document.");
		CreateDocumentRequest request = this.gerenateDocumentFromTemplateRequest(fileName, jsonStr, template);
		try {
			return this.ws.createAndDownloadDocument(request);
		} catch (ApplicationFault e) {
			throw new MailMergeApplicationFaulExeption(e);
		} catch (ValidationFault e) {
			throw new MailMergeValidationFaultExeption(e);
		} catch (TemplateNotFoundFault e) {
			throw new MailMergeTemplateNotExistsExeption(e);
		} catch (ClientNotFoundFault e) {
			throw new MailMergeClientNotExistsExeption(e);
		}
	}

	/**
	 * @param documentTemplate - {@link Template} Document template.
	 * @return - The generated file in byte array.
	 * @throws MailMergeApplicationFaulExeption
	 * @throws MailMergeValidationFaultExeption
	 * @throws MailMergeClientNotExistsExeption
	 * @throws MailMergeTemplateNotExistsExeption
	 */
	public byte[] downloadTemplate(atraxo.fmbas.domain.impl.templates.Template documentTemplate) throws BusinessException {
		LOG.info("Trying to download a template from the mail merge.");
		try {
			if (this.ws == null) {
				this.ws = this.getMailMergeWsService(this.serverAddress);
			}
			return this.ws.findTemplateByFilename(ClientContextManager.getClientContext(Session.user.get().getClient().getId()),
					documentTemplate.getFileReference());
		} catch (ApplicationFault | NullPointerException e) {
			throw new MailMergeApplicationFaulExeption(e);
		} catch (ValidationFault e) {
			throw new MailMergeValidationFaultExeption(e);
		} catch (ClientNotFoundFault e) {
			throw new MailMergeClientNotExistsExeption(e);
		} catch (TemplateNotFoundFault e) {
			throw new MailMergeTemplateNotExistsExeption(e);
		}
	}

	/**
	 * @param emailTemplateFile -the email template file reference
	 * @param jsonData -the JSON data as String
	 * @throws BusinessException
	 */
	public Map<String, String> sendMail(String emailTemplateFile, String jsonData) throws BusinessException {
		LOG.info("Send an e-mail with the mail merge.");
		if (this.ws == null) {
			this.ws = this.getMailMergeWsService(this.serverAddress);
		}
		try {
			SendEmailRequest request = new SendEmailRequest();
			request.setEmailTemplate(emailTemplateFile);
			request.setData(jsonData);
			ClientContext value = new ClientContext();
			value.setApplicationCode(Session.user.get().getClientId());
			request.setClientContext(value);
			Return response = this.ws.sendEmail(request);
			return this.toMap(response);
		} catch (ApplicationFault e) {
			throw new MailMergeApplicationFaulExeption(e);
		} catch (ValidationFault e) {
			throw new MailMergeValidationFaultExeption(e);
		} catch (TemplateNotFoundFault e) {
			throw new MailMergeTemplateNotExistsExeption(e);
		} catch (ClientNotFoundFault e) {
			throw new MailMergeClientNotExistsExeption(e);
		}
	}

	/**
	 * @param emailTemplateFile -the email template file reference
	 * @param jsonData -the JSON data as String
	 * @param attachments -the attachments
	 * @throws BusinessException
	 */
	public Map<String, String> sendMail(String emailTemplateFile, String jsonData, List<FileAttachment> attachments) throws BusinessException {
		LOG.info("Send an e-mail with the mail merge.");
		if (this.ws == null) {
			this.ws = this.getMailMergeWsService(this.serverAddress);
		}
		try {
			SendEmailRequest request = new SendEmailRequest();
			request.setEmailTemplate(emailTemplateFile);
			request.setData(jsonData);
			ClientContext value = new ClientContext();
			value.setApplicationCode(Session.user.get().getClientId());
			request.setClientContext(value);
			for (FileAttachment fileAttach : attachments) {
				request.getFiles().add(fileAttach);
			}
			Return response = this.ws.sendEmail(request);
			return this.toMap(response);
		} catch (ApplicationFault e) {
			throw new MailMergeApplicationFaulExeption(e);
		} catch (ValidationFault e) {
			throw new MailMergeValidationFaultExeption(e);
		} catch (TemplateNotFoundFault e) {
			throw new MailMergeTemplateNotExistsExeption(e);
		} catch (ClientNotFoundFault e) {
			throw new MailMergeClientNotExistsExeption(e);
		}
	}

	/**
	 * @param documentTemplate- {@link Template} Document template.
	 * @param bodyTemplate - {@link Template} Email body template.
	 * @param dataJson - {@link String} Json string.
	 * @throws MailMergeApplicationFaulExeption
	 * @throws MailMergeValidationFaultExeption
	 * @throws MailMergeTemplateNotExistsExeption
	 * @throws MailMergeClientNotExistsExeption
	 */
	public void sendMail(atraxo.fmbas.domain.impl.templates.Template documentTemplate, atraxo.fmbas.domain.impl.templates.Template bodyTemplate,
			String dataJson) throws BusinessException {
		LOG.info("Send an e-mail with the mail merge.");
		CreateDocumentRequest request = this.generateEmailRequest(documentTemplate, bodyTemplate, dataJson);
		try {
			if (this.ws == null) {
				this.ws = this.getMailMergeWsService(this.serverAddress);
			}
			this.ws.createDocument(request);
		} catch (ApplicationFault e) {
			throw new MailMergeApplicationFaulExeption(e);
		} catch (ValidationFault e) {
			throw new MailMergeValidationFaultExeption(e);
		} catch (TemplateNotFoundFault e) {
			throw new MailMergeTemplateNotExistsExeption(e);
		} catch (ClientNotFoundFault e) {
			throw new MailMergeClientNotExistsExeption(e);
		}
	}

	/**
	 * @param documentTemplate
	 * @param bodyTemplate
	 * @param emailJson
	 * @param fileDataJsons
	 * @throws BusinessException
	 * @throws MailMergeApplicationFaulExeption
	 * @throws MailMergeValidationFaultExeption
	 */
	public void sendMail(atraxo.fmbas.domain.impl.templates.Template documentTemplate, atraxo.fmbas.domain.impl.templates.Template bodyTemplate,
			String emailJson, List<String> fileDataJsons) throws BusinessException {
		LOG.info("Send an e-mail with the mail merge.");
		CreateDocumentsRequest request = this.generateEmailRequests(documentTemplate, bodyTemplate, emailJson, fileDataJsons);
		try {
			if (this.ws == null) {
				this.ws = this.getMailMergeWsService(this.serverAddress);
			}
			this.ws.createDocuments(request);
		} catch (ApplicationFault e) {
			throw new MailMergeApplicationFaulExeption(e);
		} catch (ValidationFault e) {
			throw new MailMergeValidationFaultExeption(e);
		}
	}

	private HashMap<String, String> toMap(Return response) {
		HashMap<String, String> map = new HashMap<>();
		for (Entry e : response.getEntry()) {
			map.put(e.getKey(), e.getValue());
		}
		return map;
	}

	/**
	 * @param documentTemplate
	 * @param bodyTemplate
	 * @param dataJson
	 * @return
	 */
	private CreateDocumentRequest generateEmailRequest(atraxo.fmbas.domain.impl.templates.Template documentTemplate,
			atraxo.fmbas.domain.impl.templates.Template bodyTemplate, String dataJson) {
		CreateDocumentRequest request = new CreateDocumentRequest();
		request.setClientContext(ClientContextManager.getClientContext(Session.user.get().getClient().getId()));
		request.setData(dataJson);
		request.setEmailTemplate(bodyTemplate.getFileReference());
		if (documentTemplate != null) {
			request.setTemplate(documentTemplate.getFileReference());
		}
		return request;
	}

	private CreateDocumentsRequest generateEmailRequests(atraxo.fmbas.domain.impl.templates.Template documentTemplate,
			atraxo.fmbas.domain.impl.templates.Template bodyTemplate, String emailJson, List<String> fileDataJsons) {

		CreateDocumentsRequest request = new CreateDocumentsRequest();
		request.setClientContext(ClientContextManager.getClientContext(Session.user.get().getClient().getId()));
		request.setEmailTemplate(bodyTemplate.getFileName());
		request.setEmailData(emailJson);

		for (String fileJson : fileDataJsons) {
			Attachment attachment = new Attachment();
			attachment.setTemplate(documentTemplate.getFileReference());
			attachment.setData(fileJson);
			request.getAttachment().add(attachment);
		}
		return request;
	}

	/**
	 * @param fileName
	 * @param jsonStr
	 * @return
	 */
	private CreateDocumentRequest generateDocumentRequest(String fileName, String jsonStr) {
		CreateDocumentRequest request = new CreateDocumentRequest();
		request.setClientContext(ClientContextManager.getClientContext(Session.user.get().getClient().getId()));
		request.setTemplate(fileName);
		request.setData(jsonStr);
		return request;
	}

	/**
	 * @param fileName
	 * @param jsonStr
	 * @param template
	 * @return
	 */
	private CreateDocumentRequest gerenateDocumentFromTemplateRequest(String fileName, String jsonStr,
			atraxo.fmbas.domain.impl.templates.Template template) {
		CreateDocumentRequest request = new CreateDocumentRequest();
		request.setClientContext(ClientContextManager.getClientContext(Session.user.get().getClient().getId()));
		request.setTemplate(fileName);
		request.setData(jsonStr);
		request.setTemplate(template.getFileReference());
		return request;
	}

	/**
	 * @param url
	 * @return
	 * @throws BusinessException
	 */
	private MailMergeWsService getMailMergeWsService(String url) throws BusinessException {
		try {
			MailMergeWsServiceImplService mailMergeWsService = new MailMergeWsServiceImplService(new URL(url));
			return mailMergeWsService.getMailMergeWsServiceImplPort(new MTOMFeature());
		} catch (MalformedURLException e) {
			throw new BusinessException(ErrorCode.COMMUNICATION_ERROR, e);
		} catch (Exception e) {
			throw new MailMergeServerDownException(e);
		}
	}

}
