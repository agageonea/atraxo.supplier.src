/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.quotation.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import atraxo.fmbas.business.api.quotation.IQuotationService;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link Quotation} domain entity.
 */
public class Quotation_Service extends atraxo.fmbas.business.impl.quotation.Quotation_Service implements IQuotationService {

	@Override
	public Quotation getByTimeseriesAndAvgMethod(Integer timeSeriesNo, AverageMethod averageMethod) {
		List<Quotation> quotations = this
				.getEntityManager()
				.createQuery(
						"select e from Quotation e where e.clientId = :clientId and e.avgMethodIndicator.id = :avgMethodIndicatorId and e.timeseries.id = :timeSeriesNo ",
						Quotation.class).setParameter("clientId", Session.user.get().getClient().getId())
				.setParameter("avgMethodIndicatorId", averageMethod.getId()).setParameter("timeSeriesNo", timeSeriesNo).getResultList();
		if (quotations.isEmpty()) {
			throw new EntityNotFoundException();
		}
		return quotations.get(0);
	}

}
