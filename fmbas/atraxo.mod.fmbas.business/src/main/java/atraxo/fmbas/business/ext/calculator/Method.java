package atraxo.fmbas.business.ext.calculator;

/**
 * Enumeration for average methods.
 *
 * @author zspeter
 */
public enum Method {

	DC {
		@Override
		public Calculator getCalculator() {
			return new DCCalculator();
		}
	},
	WC {

		@Override
		public Calculator getCalculator() {
			return new WCCalculator();
		}
	},
	WT {
		@Override
		public Calculator getCalculator() {
			return new WTCalculator();
		}
	},
	MC {
		@Override
		public Calculator getCalculator() {
			return new MCCalculator();
		}
	},
	MT {
		@Override
		public Calculator getCalculator() {
			return new MTCalculator();
		}
	},
	SC {
		@Override
		public Calculator getCalculator() {
			return new SCCalculator();
		}
	},
	ST {
		@Override
		public Calculator getCalculator() {
			return new STCalculator();
		}
	},
	FC {
		@Override
		public Calculator getCalculator() {
			return new FCCalculator();
		}
	},
	FT {
		@Override
		public Calculator getCalculator() {
			return new FTCalculator();
		}
	};

	public static Method getByName(String name) {
		for (Method method : Method.values()) {
			if (method.name().equalsIgnoreCase(name)) {
				return method;
			}
		}
		// TODO - exception handling
		throw new RuntimeException("Invalid method indicator!");
	}

	public abstract Calculator getCalculator();
}
