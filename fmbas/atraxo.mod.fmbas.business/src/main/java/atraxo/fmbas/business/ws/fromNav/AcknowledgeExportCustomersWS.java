package atraxo.fmbas.business.ws.fromNav;

import java.io.IOException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;

@WebService(targetNamespace = "http://fromNav.ws.business.fmbas.atraxo/", endpointInterface = "AcknowledgeExportCustomersEndpoint")
public interface AcknowledgeExportCustomersWS {

	/**
	 * @param request
	 * @return
	 * @throws JAXBException
	 * @throws SAXException
	 * @throws IOException
	 * @throws BusinessException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	@WebMethod(operationName = "acknowledgeExportCustomers", action = "acknowledgeExportCustomers")
	@WebResult(name = "MSG", targetNamespace = "http://Pumaenergy.Common.eAviation.Schemas.Message_RequestResponse")
	MSG acknowledgeExportCustomers(
			@WebParam(name = "MSG", targetNamespace = "http://Pumaenergy.Common.eAviation.Schemas.Message_RequestResponse") MSG request)
			throws JAXBException, SAXException, IOException, BusinessException, InstantiationException, IllegalAccessException;

	/**
	 * @param request
	 * @return
	 * @throws JAXBException
	 * @throws SAXException
	 * @throws IOException
	 * @throws BusinessException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	@WebMethod(operationName = "acknowledgeExportCustomersEBits", action = "acknowledgeExportCustomersEBits")
	@WebResult(name = "MSG", targetNamespace = "http://Pumaenergy.Common.eAviation.Schemas.Message_RequestResponse")
	MSG acknowledgeExportCustomersEBits(
			@WebParam(name = "MSG", targetNamespace = "http://Pumaenergy.Common.eAviation.Schemas.Message_RequestResponse") MSG request)
			throws JAXBException, SAXException, IOException, BusinessException, InstantiationException, IllegalAccessException;
}
