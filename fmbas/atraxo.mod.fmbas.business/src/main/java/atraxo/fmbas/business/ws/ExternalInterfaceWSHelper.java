package atraxo.fmbas.business.ws;

import java.math.BigInteger;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceParameterService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceParameter;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;

public class ExternalInterfaceWSHelper {

	private static final Logger logger = LoggerFactory.getLogger(ExternalInterfaceWSHelper.class);

	/**
	 * Constants for generic external parameters
	 */
	public static final String EXTERNAL_PARAM_NR_RETRIES = "Retries";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_RETRIES_INTERVAL = "Retry interval";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_BULK_MODE = "Bulk mode";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_RS_URL = "Rest Url";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WSDL = "Endpoint address";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_KEY_ALIAS = "Key Alias";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_KEY_STORE = "Keystore file name";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_KEY_STORE_PWD = "Keystore password";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_KEY_STORE_TYPE = "Keystore type";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_VALIDATION_XSD = "Validation XSD";
	public static final String EXTERNAL_PARAM_ENVIRONMENT = "Environment";

	/**
	 * Geneic constants for HeaderCommon
	 */
	public static final String EXTERNAL_PARAM_WS_INTERFACE_TYPE = "Interface Type";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_SOURCE_GROUP_COMPANY = "Source Group Company";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_DESTINATION_GROUP_COMPANY = "Destination Group Company";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_SOURCE_COMPANY = "Source Company";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_DESTINATION_COMPANY = "Destination Company";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_SOURCE_RESPONSE_ADDRESS = "Source Response Address";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_SOURCE_UPDATE_STATUS_ADDRESS = "Source Update Status Address";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_MIDDLEWARE_URL_FOR_PUSH = "Middleware Url For Push";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_EMAIL_NOTIFICATION = "Email Notification";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_CONTINUE_ON_ERROR = "Continue On Error";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_COMPREHENSIVE_LOGGING = "Comprehensive Logging";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_OBJECT_TYPE = "Object Type";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_OBJECT_NAME = "Object Name";//$NON-NLS-1$

	public static final String EXTERNAL_PARAM_WS_DESTINATION_ADDRESS = "Destination Address";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_DESTINATION_UPDATE_STATUS_ADDRESS = "Destination Update Status Address";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_UPDATE_SOURCE_ON_RECEIVE = "Update Source On Receive";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_UPDATE_SOURCE_ON_DELIVERY = "Update Source On Delivery";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_UPDATE_SOURCE_AFTER_PROCESSING = "Update Source After Processing";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_UPDATE_DESTINATION_ON_DELIVERY = "Update Destination On Delivery";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_WS_CALL_DESTINATION_FOR_PROCESSING = "Call Destination For Processing";//$NON-NLS-1$

	public static final String EXTERNAL_PARAM_VALUE_WS_TRANSPORT_STATUS = "Transport Status";//$NON-NLS-1$
	public static final String EXTERNAL_PARAM_VALUE_WS_COMMUNICATION_TYPE = "Communication Type";//$NON-NLS-1$

	/**
	 * constants for Credit Update WS
	 */
	public static final String DEFAULT_SYSTEM_REQUEST_REASON = "external system request";//$NON-NLS-1$
	public static final Integer DEFAUL_CREDIT_ALERT_PERCENTAGE = 100;
	public static final BigInteger DEFAULT_ACK_LOG_ENTRY_NO = BigInteger.valueOf(100l);
	public static final String DEFAULT_CREDIT_ACK_NAME = "CustomerCredit";//$NON-NLS-1$
	public static final String DEFAULT_ACK_DESCRIPTION_SUCCESS = "Success";//$NON-NLS-1$

	public static final String EXTERNAL_PARAM_WS_INTERFACE_TIMEOUT = "Timeout interval";//$NON-NLS-1$

	/**
	 * constant for Send Invoices WS
	 */
	public static final String EXTERNAL_PARAM_WS_PAYABLE = "has_payable_cost";
	public static final String EXTERNAL_PARAM_WS_CREDIT_MEMO = "is_credit_memo";

	/**
	 * Constants for message ID
	 */

	public static final String EXTERNAL_PARAM_FILE_EXTENSION = "File extension";
	public static final String EXTERNAL_PARAM_MESSAGE_ID_PREFIX = "Message ID prefix";
	public static final String EXTERNAL_PARAM_MESSAGE_ID = "Message ID";

	public static final String CAN_MODIFY_DESTINATION_ADDRESS = "Can modify destination address";

	private IExternalInterfaceParameterService externalInterfaceParamService;

	/**
	 * @param externalInterfaceParamService
	 */
	public ExternalInterfaceWSHelper(IExternalInterfaceParameterService externalInterfaceParamService) {
		this.externalInterfaceParamService = externalInterfaceParamService;
	}

	/**
	 * @param paramName
	 * @param result
	 * @return
	 */
	public String retrieveStringParameterValue(String paramName, WSExecutionResult result, ExternalInterface externalInterface) {
		String paramValue = null;
		ExternalInterfaceParameter param = null;
		try {
			param = this.externalInterfaceParamService.findByKey(externalInterface, paramName);
		} catch (Exception e) {
			result.appendExecutionData(String.format(BusinessErrorCode.INTERFACE_PARAM_NOT_LOCATED.getErrMsg(), paramName));
			logger.error("ERROR:could not locate ExternalInterfaceParameter for name '" + paramName + "' !", e);
		}
		if (param != null) {
			paramValue = param.getCurrentValue();
			if (StringUtils.isEmpty(paramValue)) {
				paramValue = param.getDefaultValue();
			}
		}
		return paramValue;
	}

	/**
	 * @param paramName
	 * @param result
	 * @return
	 */
	public Boolean retrieveBooleanParameterValue(String paramName, WSExecutionResult result, ExternalInterface externalInterface) {
		Boolean paramValueBoolean = Boolean.FALSE;
		ExternalInterfaceParameter param = null;
		try {
			param = this.externalInterfaceParamService.findByKey(externalInterface, paramName);
		} catch (Exception e) {
			logger.error("ERROR:could not locate INTEGER ExternalInterfaceParameter for name '" + paramName + "'!", e);
			result.appendExecutionData(String.format(BusinessErrorCode.INTERFACE_PARAM_NOT_LOCATED.getErrMsg(), paramName));
		}
		if (param != null) {
			String paramValue = param.getCurrentValue();
			if (StringUtils.isEmpty(paramValue)) {
				paramValue = param.getDefaultValue();
			}
			paramValueBoolean = Boolean.valueOf(paramValue);
		}
		return paramValueBoolean;
	}

	/**
	 * @param paramName
	 * @param result
	 * @return
	 */
	public Integer retrieveIntegerParameterValue(String paramName, WSExecutionResult result, ExternalInterface externalInterface) {
		Integer paramValueInteger = null;
		ExternalInterfaceParameter param = null;
		try {
			param = this.externalInterfaceParamService.findByKey(externalInterface, paramName);
		} catch (Exception e) {
			logger.error("ERROR:could not locate INTEGER ExternalInterfaceParameter for name '" + paramName + "'!", e);
			result.appendExecutionData(String.format(BusinessErrorCode.INTERFACE_PARAM_NOT_LOCATED.getErrMsg(), paramName));
		}
		if (param != null) {
			String paramValue = param.getCurrentValue();
			if (StringUtils.isEmpty(paramValue)) {
				paramValue = param.getDefaultValue();
			}
			try {
				paramValueInteger = Integer.parseInt(paramValue);
				// check for negative values
				if (paramValueInteger < 0) {
					result.appendExecutionData(String.format(BusinessErrorCode.INTERFACE_PARAM_NEGATIVE.getErrMsg(), paramName));
					paramValueInteger = null;
				}

			} catch (NumberFormatException e) {
				logger.error("ERROR: could not transform parameter " + paramName + " in a number !", e);
				result.appendExecutionData(String.format(BusinessErrorCode.INTERFACE_PARAM_NOT_VALID.getErrMsg(), paramName));
			}
		}
		return paramValueInteger;
	}

}
