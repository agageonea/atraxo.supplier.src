package atraxo.fmbas.business.ext.timeserie.delegate;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.impl.timeserie.CompositeTimeSeriesSource;
import atraxo.fmbas.domain.impl.timeserie.RawTimeSerieItem;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class CompositeCalculator_Bd extends AbstractBusinessDelegate {

	class SourceItems {
		private BigDecimal value;
		private Integer weighting;
		private BigDecimal factor;
		private Unit unit;
		private boolean calculated;

		public SourceItems(BigDecimal value, Integer weighting, BigDecimal factor, Unit unit, boolean calculated) {
			super();
			this.value = value;
			this.weighting = weighting;
			this.factor = factor;
			this.unit = unit;
			this.calculated = calculated;
		}

		public BigDecimal getValue() {
			return this.value;
		}

		public Integer getWeighting() {
			return this.weighting;
		}

		public BigDecimal getFactor() {
			return this.factor;
		}

		public Unit getUnit() {
			return this.unit;
		}

		public boolean getCalculated() {
			return this.calculated;
		}

	}

	/**
	 * @param e
	 * @throws BusinessException
	 */
	public void calculate(TimeSerie e) throws BusinessException {
		Map<Date, List<SourceItems>> map = this.calculateItems(e);
		Set<Date> keySet = map.keySet();
		this.removeObsoleItems(e, keySet);

		Map<Date, RawTimeSerieItem> items = new HashMap<>();
		for (RawTimeSerieItem item : e.getRawTimeserieItems()) {
			items.put(item.getItemDate(), item);
		}

		UnitConverterService unitConverterService = this.getApplicationContext().getBean(UnitConverterService.class);
		for (Date itemDate : keySet) {
			boolean calculated = false;
			BigDecimal p = BigDecimal.ZERO;
			int w = 0;
			for (SourceItems src : map.get(itemDate)) {
				BigDecimal value = unitConverterService.convert(e.getUnitId(), src.getUnit(), src.getValue(), src.getFactor(), e.getUnitId(),
						src.getUnit());
				p = p.add(value.multiply(new BigDecimal(src.getWeighting()), MathContext.DECIMAL64));
				if (src.getCalculated()) {
					calculated = src.getCalculated();
				}
				w += src.getWeighting();
			}
			RawTimeSerieItem item = new RawTimeSerieItem();
			if (items.containsKey(itemDate)) {
				item = items.get(itemDate);
			} else {
				e.addToRawTimeserieItems(item);
			}
			item.setActive(true);
			item.setCalculated(calculated);
			item.setItemDate(itemDate);
			item.setItemValue(p.divide(new BigDecimal(w), MathContext.DECIMAL64));
			item.setTransfered(false);
			item.setTransferedItemUpdate(false);
			item.setUpdated(false);
		}

	}

	private void removeObsoleItems(TimeSerie e, Set<Date> keySet) {
		for (Iterator<RawTimeSerieItem> iter = e.getRawTimeserieItems().iterator(); iter.hasNext();) {
			RawTimeSerieItem item = iter.next();
			if (!keySet.contains(item.getItemDate())) {
				iter.remove();
			}
		}
	}

	private Map<Date, List<SourceItems>> calculateItems(TimeSerie e) {
		Map<Date, List<SourceItems>> map = new HashMap<>();
		for (CompositeTimeSeriesSource src : e.getCompositeTimeSeries()) {
			for (RawTimeSerieItem item : src.getSource().getRawTimeserieItems()) {
				if (map.containsKey(item.getItemDate())) {
					map.get(item.getItemDate()).add(new SourceItems(item.getItemValue(), src.getWeighting(), src.getFactor(),
							item.getTserie().getUnitId(), item.getCalculated()));
				} else {
					List<SourceItems> l = new ArrayList<>();
					l.add(new SourceItems(item.getItemValue(), src.getWeighting(), src.getFactor(), item.getTserie().getUnitId(),
							item.getCalculated()));
					map.put(item.getItemDate(), l);
				}
			}
		}
		return map;
	}

}
