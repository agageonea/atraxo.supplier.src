/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.job.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.quartz.JobListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import atraxo.fmbas.business.api.job.ITriggerService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.fmbas_type.DaysOfMonth;
import atraxo.fmbas.domain.impl.fmbas_type.DaysOfWeek;
import atraxo.fmbas.domain.impl.fmbas_type.Months;
import atraxo.fmbas.domain.impl.fmbas_type.RepeatDuration;
import atraxo.fmbas.domain.impl.fmbas_type.TimeInterval;
import atraxo.fmbas.domain.impl.fmbas_type.TriggerType;
import atraxo.fmbas.domain.impl.fmbas_type.WeekOfMonth;
import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.ActionParameter;
import atraxo.fmbas.domain.impl.job.Trigger;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.domain.batchjob.ActionDetail;
import seava.j4e.domain.batchjob.TriggerDetail;
import seava.j4e.scheduler.quartz.JobLauncherDetails;
import seava.j4e.scheduler.quartz.PersistentJobScheduler;

/**
 * Business extensions specific for {@link Trigger} domain entity.
 */
public class Trigger_Service extends atraxo.fmbas.business.impl.job.Trigger_Service implements ITriggerService {

	private static final int MIN_IN_HOUR = 60;

	private static final Logger LOGGER = LoggerFactory.getLogger(Trigger_Service.class);

	@Autowired
	private PersistentJobScheduler jobScheduler;

	@Override
	protected void preInsert(Trigger e) throws BusinessException {
		super.preInsert(e);
		this.checkInputData(e);
	}

	@Override
	protected void preUpdate(Trigger e) throws BusinessException {
		super.preUpdate(e);
		this.checkInputData(e);
	}

	private void checkInputData(Trigger e) throws BusinessException {
		TriggerType type = e.getType();
		switch (type) {
		case _DAILY_:
			this.checkDailyTrigger(e);
			break;
		case _WEEKLY_:
			this.checkWeeklyTrigger(e);
			break;
		case _MONTHLY_:
			this.checkMonthlyTrigger(e);
			break;
		default:
			break;
		}
	}

	private void checkMonthlyTrigger(Trigger e) throws BusinessException {
		if (Months._EMPTY_.equals(e.getMonths())) {
			throw new BusinessException(BusinessErrorCode.MONTHLY_TRIGGER_MISSING_MONTHS,
					BusinessErrorCode.MONTHLY_TRIGGER_MISSING_MONTHS.getErrMsg());
		}
		if (e.getOnMonthDays()) {
			if (StringUtils.isEmpty(e.getDaysOfMonth())) {
				throw new BusinessException(BusinessErrorCode.MONTHLY_TRIGGER_MISSING_DAYS_OF_MONTH,
						BusinessErrorCode.MONTHLY_TRIGGER_MISSING_DAYS_OF_MONTH.getErrMsg());
			}
		} else if (e.getOnWeekDays()) {
			if (StringUtils.isEmpty(e.getWeekOfMonth()) || StringUtils.isEmpty(e.getDaysOfWeek())) {
				throw new BusinessException(BusinessErrorCode.MONTHLY_TRIGGER_MISSING_WEEKS,
						BusinessErrorCode.MONTHLY_TRIGGER_MISSING_WEEKS.getErrMsg());
			}
		} else {
			throw new BusinessException(BusinessErrorCode.MONTHLY_TRIGGER_MISSING_REPEAT_DEFINIITON,
					BusinessErrorCode.MONTHLY_TRIGGER_MISSING_REPEAT_DEFINIITON.getErrMsg());
		}
	}

	private void checkWeeklyTrigger(Trigger e) throws BusinessException {
		if (e.getIntInWeeks() == null) {
			throw new BusinessException(BusinessErrorCode.WEEKLY_TRIGGER_MISSING_REPEAT_INTERVAL,
					BusinessErrorCode.WEEKLY_TRIGGER_MISSING_REPEAT_INTERVAL.getErrMsg());
		}
		if (DaysOfWeek._EMPTY_.getName().equals(e.getDaysOfWeek())) {
			throw new BusinessException(BusinessErrorCode.WEEKLY_TRIGGER_MISSING_DAYS, BusinessErrorCode.WEEKLY_TRIGGER_MISSING_DAYS.getErrMsg());
		}
	}

	private void checkDailyTrigger(Trigger e) throws BusinessException {
		if (e.getIntInDays() == null) {
			throw new BusinessException(BusinessErrorCode.DAILY_TRIGGER_NOT_VALID, BusinessErrorCode.DAILY_TRIGGER_NOT_VALID.getErrMsg());
		}
	}

	@Override
	public Date scheduleTrigger(Trigger e, JobListener... listeners) throws BusinessException {
		String clientCode = Session.user.get().getClientCode();
		TriggerType type = e.getType();
		TimeZone timezone = TimeZone.getTimeZone("UTC");
		Date nextFireTime = null;
		if (e.getTimeReference() == null || !e.getTimeReference()) {
			IUserSuppService service = (IUserSuppService) this.findEntityService(UserSupp.class);
			UserSupp userSupp = service.findByCode(Session.user.get().getCode());
			try {
				timezone = TimeZone.getTimeZone(userSupp.getLocation().getTimezone().getName());
			} catch (NullPointerException npe) {
				// do nothing, user doesn't have location or location doesn't have timezone
				LOGGER.info("Could not retrieve timzone, will do nothing, the user doesn't have location or location doesn't have timezone!", npe);
			}
		}
		if (e.getActive()) {
			Map<String, ActionDetail> batchJobs = this.getBatchJobs(e);
			TriggerDetail detail = this.buildTriggerDetails(e, timezone);

			switch (type) {
			case _ONCE_:
				nextFireTime = this.jobScheduler.runOnceAt(JobLauncherDetails.class, detail, e.getJobChain().getName(), e.getClientId(), clientCode,
						batchJobs, listeners);
				break;
			case _DAILY_:
				nextFireTime = this.jobScheduler.runDaily(JobLauncherDetails.class, detail, e.getJobChain().getName(), e.getClientId(), clientCode,
						batchJobs, listeners);
				break;
			case _WEEKLY_:
				nextFireTime = this.jobScheduler.runWeekly(JobLauncherDetails.class, e.getJobChain().getName(), batchJobs, detail, e.getClientId(),
						clientCode, listeners);
				break;
			case _MONTHLY_:
				nextFireTime = this.jobScheduler.runMonthly(JobLauncherDetails.class, e.getJobChain().getName(), batchJobs, detail, e.getClientId(),
						clientCode, listeners);
				break;
			default:
				break;
			}
		}
		return nextFireTime;
	}

	@Override
	public void unscheduleTrigger(Trigger e) throws BusinessException {
		this.jobScheduler.unschedulePersistentJob(JobLauncherDetails.class, e.getRefid(), e.getClientId(), Session.user.get().getClientCode());
	}

	private Map<String, ActionDetail> getBatchJobs(Trigger e) {
		List<Action> actions = new ArrayList<>(e.getJobChain().getActions());
		Collections.sort(actions, new Comparator<Action>() {
			@Override
			public int compare(Action arg0, Action arg1) {
				return arg0.getOrder().compareTo(arg1.getOrder());
			}
		});
		Map<String, ActionDetail> batchJobs = new LinkedHashMap<>();
		for (Action action : actions) {
			String actionName = StringUtils.isEmpty(action.getName()) ? action.getBatchJob().getName() : action.getName();
			ActionDetail actionDetail = new ActionDetail(action.getId(), actionName, e.getJobChain().getId(), action.getBatchJob().getBeanName());
			for (ActionParameter param : action.getActionParameters()) {
				String value;
				if (!StringUtils.isEmpty(param.getBusinessValue())) {
					value = param.getBusinessValue();
				} else if (!StringUtils.isEmpty(param.getValue())) {
					value = param.getValue();
				} else {
					value = param.getDefaultValue();
				}
				actionDetail.putToProperties(param.getName(), value);
			}
			batchJobs.put(action.getJobUID(), actionDetail);
		}
		return batchJobs;
	}

	private TriggerDetail buildTriggerDetails(Trigger e, TimeZone timeZone) {
		TriggerDetail detail = new TriggerDetail(e.getRefid());
		detail.setDate(e.getStartDate());
		detail.setTimezone(timeZone);
		if (!RepeatDuration._EMPTY_.equals(e.getRepeatDuration())) {
			int duration = e.getRepeatDuration().getDuration();
			if (duration > 60) {
				detail.setDurationInHour(duration / MIN_IN_HOUR);
			} else {
				detail.setDurationInMinutes(duration);
			}
		}

		detail.setIntervalInDays(e.getIntInDays());
		if (!TimeInterval._EMPTY_.equals(e.getRepeatInterval())) {
			detail.setIntInMinutes(e.getRepeatInterval().getDuration());
		}
		List<Integer> dayList = this.convertListElements(DaysOfMonth.class, e.getDaysOfMonth().split(","), -1);
		detail.setOnDaysOfMonth(new HashSet<>(dayList));
		List<Integer> days = this.convertListElements(DaysOfWeek.class, e.getDaysOfWeek().split(","), 1);
		detail.setOnDaysOfWeek(new HashSet<>(days));
		detail.setOnMonthDays(e.getOnMonthDays());
		List<Integer> monthList = this.convertListElements(Months.class, e.getMonths().split(","), -1);
		detail.setOnMonths(new HashSet<>(monthList));
		if (!days.isEmpty()) {
			detail.setWeekDay(days.get(0));
		}
		detail.setWeekOffset(e.getIntInWeeks());
		if (!StringUtils.isEmpty(e.getWeekOfMonth())) {
			try {
				detail.setWeekOfMonth(WeekOfMonth.valueOf(e.getWeekOfMonth().toUpperCase()).ordinal());
			} catch (IllegalArgumentException iae) {
				LOGGER.warn("Could not set the week on month, will use the default one !", iae);
				detail.setWeekOfMonth(WeekOfMonth.valueOf("_" + e.getWeekOfMonth().toUpperCase() + "_").ordinal());
			}
		}
		return detail;
	}

	private <T extends Enum<T>> List<Integer> convertListElements(Class<T> clazz, String[] arr, int delta) {
		return this.convertListElements(clazz, "", arr, delta);
	}

	private <T extends Enum<T>> List<Integer> convertListElements(Class<T> clazz, String prefix, String[] arr, int delta) {
		List<Integer> list = new ArrayList<>();
		for (String str : arr) {
			if (str.length() > 0) {
				try {
					list.add(Enum.valueOf(clazz, prefix + str.toUpperCase()).ordinal() + delta);
				} catch (IllegalArgumentException e) {
					LOGGER.warn("Could not get the value of Enum, will use the default one !", e);
					list.add(Enum.valueOf(clazz, "_" + prefix + str.toUpperCase() + "_").ordinal() + delta);
				}
			}
		}
		return list;
	}

}
