package atraxo.fmbas.business.ext.customer.job.exportData;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.business.ws.ExternalInterfaceWSHelper;
import atraxo.fmbas.business.ws.client.PumaRestClient;
import atraxo.fmbas.business.ws.toEbits.WSCustomerToDTOTransformer;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;

/**
 * Spring Batch Tasklet for exporting the customers;
 *
 * @author vhojda
 */
public class ExportCustomerTasklet implements Tasklet {

	final static Logger logger = LoggerFactory.getLogger(ExportCustomerTasklet.class);

	public static final String KEY_NO_CUSTOMERS = "no_customers";
	public static final String KEY_INTERFACE_DISABLE = "export_job_details";
	@Autowired
	private ICustomerService service;
	@Autowired
	private IExternalInterfaceService externalInterfaceService;
	@Autowired
	private ExternalInterfaceHistoryFacade historyFacade;
	@Autowired
	private ExternalInterfaceMessageHistoryFacade messageFacade;
	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("START execute()");
		}

		List<Customer> customers = this.service.getCustomer();
		if (customers != null) {
			if (customers.isEmpty()) {
				chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put(KEY_NO_CUSTOMERS, "");
			} else {
				ExternalInterface extInterface = this.externalInterfaceService.findByName(ExtInterfaceNames.EXPORT_CUSTOMERS_TO_EBITS.getValue());
				if (extInterface.getEnabled()) {
					ExtInterfaceParameters params = new ExtInterfaceParameters(extInterface.getParams());
					params.getParamMap().remove(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_DESTINATION_ADDRESS);
					PumaRestClient<Customer> client = new PumaRestClient<>(this.taskExecutor, extInterface, params, customers, this.historyFacade,
							new WSCustomerToDTOTransformer(), this.messageFacade);

					client.start(1);
				} else {
					chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put(KEY_INTERFACE_DISABLE, "");
				}
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("END execute()");
		}
		return RepeatStatus.FINISHED;
	}
}
