/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.customer;

import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.customer.CustomerLocations;
import atraxo.fmbas.domain.impl.customer.CustomerNotification;
import atraxo.fmbas.domain.impl.exposure.Exposure;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.geo.TimeZone;
import atraxo.fmbas.domain.impl.masteragreements.MasterAgreement;
import atraxo.fmbas.domain.impl.profile.AccountingRules;
import atraxo.fmbas.domain.impl.profile.BankAccount;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Customer} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Customer_Service extends AbstractEntityService<Customer> {

	/**
	 * Public constructor for Customer_Service
	 */
	public Customer_Service() {
		super();
	}

	/**
	 * Public constructor for Customer_Service
	 * 
	 * @param em
	 */
	public Customer_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Customer> getEntityClass() {
		return Customer.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Customer
	 */
	public Customer findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Customer.NQ_FIND_BY_CODE, Customer.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Customer", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Customer", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Customer
	 */
	public Customer findByAreas(Area assignedArea, Boolean isSubsidiary) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Customer.NQ_FIND_BY_AREAS, Customer.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("assignedArea", assignedArea)
					.setParameter("isSubsidiary", isSubsidiary)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Customer", "assignedArea, isSubsidiary"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Customer", "assignedArea, isSubsidiary"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Customer
	 */
	public Customer findByAreas(Long assignedAreaId, Boolean isSubsidiary) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Customer.NQ_FIND_BY_AREAS_PRIMITIVE,
							Customer.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("assignedAreaId", assignedAreaId)
					.setParameter("isSubsidiary", isSubsidiary)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Customer", "assignedAreaId, isSubsidiary"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Customer", "assignedAreaId, isSubsidiary"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Customer
	 */
	public Customer findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Customer.NQ_FIND_BY_BUSINESS,
							Customer.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Customer", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Customer", "id"), nure);
		}
	}

	/**
	 * Find by reference: parent
	 *
	 * @param parent
	 * @return List<Customer>
	 */
	public List<Customer> findByParent(Customer parent) {
		return this.findByParentId(parent.getId());
	}
	/**
	 * Find by ID of reference: parent.id
	 *
	 * @param parentId
	 * @return List<Customer>
	 */
	public List<Customer> findByParentId(Integer parentId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Customer e where e.clientId = :clientId and e.parent.id = :parentId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("parentId", parentId).getResultList();
	}
	/**
	 * Find by reference: responsibleBuyer
	 *
	 * @param responsibleBuyer
	 * @return List<Customer>
	 */
	public List<Customer> findByResponsibleBuyer(UserSupp responsibleBuyer) {
		return this.findByResponsibleBuyerId(responsibleBuyer.getId());
	}
	/**
	 * Find by ID of reference: responsibleBuyer.id
	 *
	 * @param responsibleBuyerId
	 * @return List<Customer>
	 */
	public List<Customer> findByResponsibleBuyerId(String responsibleBuyerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Customer e where e.clientId = :clientId and e.responsibleBuyer.id = :responsibleBuyerId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("responsibleBuyerId", responsibleBuyerId)
				.getResultList();
	}
	/**
	 * Find by reference: officeCountry
	 *
	 * @param officeCountry
	 * @return List<Customer>
	 */
	public List<Customer> findByOfficeCountry(Country officeCountry) {
		return this.findByOfficeCountryId(officeCountry.getId());
	}
	/**
	 * Find by ID of reference: officeCountry.id
	 *
	 * @param officeCountryId
	 * @return List<Customer>
	 */
	public List<Customer> findByOfficeCountryId(Integer officeCountryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Customer e where e.clientId = :clientId and e.officeCountry.id = :officeCountryId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("officeCountryId", officeCountryId)
				.getResultList();
	}
	/**
	 * Find by reference: officeState
	 *
	 * @param officeState
	 * @return List<Customer>
	 */
	public List<Customer> findByOfficeState(Country officeState) {
		return this.findByOfficeStateId(officeState.getId());
	}
	/**
	 * Find by ID of reference: officeState.id
	 *
	 * @param officeStateId
	 * @return List<Customer>
	 */
	public List<Customer> findByOfficeStateId(Integer officeStateId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Customer e where e.clientId = :clientId and e.officeState.id = :officeStateId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("officeStateId", officeStateId).getResultList();
	}
	/**
	 * Find by reference: billingCountry
	 *
	 * @param billingCountry
	 * @return List<Customer>
	 */
	public List<Customer> findByBillingCountry(Country billingCountry) {
		return this.findByBillingCountryId(billingCountry.getId());
	}
	/**
	 * Find by ID of reference: billingCountry.id
	 *
	 * @param billingCountryId
	 * @return List<Customer>
	 */
	public List<Customer> findByBillingCountryId(Integer billingCountryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Customer e where e.clientId = :clientId and e.billingCountry.id = :billingCountryId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("billingCountryId", billingCountryId)
				.getResultList();
	}
	/**
	 * Find by reference: billingState
	 *
	 * @param billingState
	 * @return List<Customer>
	 */
	public List<Customer> findByBillingState(Country billingState) {
		return this.findByBillingStateId(billingState.getId());
	}
	/**
	 * Find by ID of reference: billingState.id
	 *
	 * @param billingStateId
	 * @return List<Customer>
	 */
	public List<Customer> findByBillingStateId(Integer billingStateId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Customer e where e.clientId = :clientId and e.billingState.id = :billingStateId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("billingStateId", billingStateId).getResultList();
	}
	/**
	 * Find by reference: timeZone
	 *
	 * @param timeZone
	 * @return List<Customer>
	 */
	public List<Customer> findByTimeZone(TimeZone timeZone) {
		return this.findByTimeZoneId(timeZone.getId());
	}
	/**
	 * Find by ID of reference: timeZone.id
	 *
	 * @param timeZoneId
	 * @return List<Customer>
	 */
	public List<Customer> findByTimeZoneId(Integer timeZoneId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Customer e where e.clientId = :clientId and e.timeZone.id = :timeZoneId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("timeZoneId", timeZoneId).getResultList();
	}
	/**
	 * Find by reference: subsidiaryCurrency
	 *
	 * @param subsidiaryCurrency
	 * @return List<Customer>
	 */
	public List<Customer> findBySubsidiaryCurrency(Currencies subsidiaryCurrency) {
		return this.findBySubsidiaryCurrencyId(subsidiaryCurrency.getId());
	}
	/**
	 * Find by ID of reference: subsidiaryCurrency.id
	 *
	 * @param subsidiaryCurrencyId
	 * @return List<Customer>
	 */
	public List<Customer> findBySubsidiaryCurrencyId(
			Integer subsidiaryCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Customer e where e.clientId = :clientId and e.subsidiaryCurrency.id = :subsidiaryCurrencyId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("subsidiaryCurrencyId", subsidiaryCurrencyId)
				.getResultList();
	}
	/**
	 * Find by reference: defaultVolumeUnit
	 *
	 * @param defaultVolumeUnit
	 * @return List<Customer>
	 */
	public List<Customer> findByDefaultVolumeUnit(Unit defaultVolumeUnit) {
		return this.findByDefaultVolumeUnitId(defaultVolumeUnit.getId());
	}
	/**
	 * Find by ID of reference: defaultVolumeUnit.id
	 *
	 * @param defaultVolumeUnitId
	 * @return List<Customer>
	 */
	public List<Customer> findByDefaultVolumeUnitId(Integer defaultVolumeUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Customer e where e.clientId = :clientId and e.defaultVolumeUnit.id = :defaultVolumeUnitId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("defaultVolumeUnitId", defaultVolumeUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: defaultWeightUnit
	 *
	 * @param defaultWeightUnit
	 * @return List<Customer>
	 */
	public List<Customer> findByDefaultWeightUnit(Unit defaultWeightUnit) {
		return this.findByDefaultWeightUnitId(defaultWeightUnit.getId());
	}
	/**
	 * Find by ID of reference: defaultWeightUnit.id
	 *
	 * @param defaultWeightUnitId
	 * @return List<Customer>
	 */
	public List<Customer> findByDefaultWeightUnitId(Integer defaultWeightUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Customer e where e.clientId = :clientId and e.defaultWeightUnit.id = :defaultWeightUnitId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("defaultWeightUnitId", defaultWeightUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: assignedArea
	 *
	 * @param assignedArea
	 * @return List<Customer>
	 */
	public List<Customer> findByAssignedArea(Area assignedArea) {
		return this.findByAssignedAreaId(assignedArea.getId());
	}
	/**
	 * Find by ID of reference: assignedArea.id
	 *
	 * @param assignedAreaId
	 * @return List<Customer>
	 */
	public List<Customer> findByAssignedAreaId(Integer assignedAreaId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Customer e where e.clientId = :clientId and e.assignedArea.id = :assignedAreaId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("assignedAreaId", assignedAreaId).getResultList();
	}
	/**
	 * Find by reference: bankAccounts
	 *
	 * @param bankAccounts
	 * @return List<Customer>
	 */
	public List<Customer> findByBankAccounts(BankAccount bankAccounts) {
		return this.findByBankAccountsId(bankAccounts.getId());
	}
	/**
	 * Find by ID of reference: bankAccounts.id
	 *
	 * @param bankAccountsId
	 * @return List<Customer>
	 */
	public List<Customer> findByBankAccountsId(Integer bankAccountsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Customer e, IN (e.bankAccounts) c where e.clientId = :clientId and c.id = :bankAccountsId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("bankAccountsId", bankAccountsId).getResultList();
	}
	/**
	 * Find by reference: accountingRules
	 *
	 * @param accountingRules
	 * @return List<Customer>
	 */
	public List<Customer> findByAccountingRules(AccountingRules accountingRules) {
		return this.findByAccountingRulesId(accountingRules.getId());
	}
	/**
	 * Find by ID of reference: accountingRules.id
	 *
	 * @param accountingRulesId
	 * @return List<Customer>
	 */
	public List<Customer> findByAccountingRulesId(Integer accountingRulesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Customer e, IN (e.accountingRules) c where e.clientId = :clientId and c.id = :accountingRulesId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("accountingRulesId", accountingRulesId)
				.getResultList();
	}
	/**
	 * Find by reference: locations
	 *
	 * @param locations
	 * @return List<Customer>
	 */
	public List<Customer> findByLocations(CustomerLocations locations) {
		return this.findByLocationsId(locations.getId());
	}
	/**
	 * Find by ID of reference: locations.id
	 *
	 * @param locationsId
	 * @return List<Customer>
	 */
	public List<Customer> findByLocationsId(Integer locationsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Customer e, IN (e.locations) c where e.clientId = :clientId and c.id = :locationsId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("locationsId", locationsId).getResultList();
	}
	/**
	 * Find by reference: creditLines
	 *
	 * @param creditLines
	 * @return List<Customer>
	 */
	public List<Customer> findByCreditLines(CreditLines creditLines) {
		return this.findByCreditLinesId(creditLines.getId());
	}
	/**
	 * Find by ID of reference: creditLines.id
	 *
	 * @param creditLinesId
	 * @return List<Customer>
	 */
	public List<Customer> findByCreditLinesId(Integer creditLinesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Customer e, IN (e.creditLines) c where e.clientId = :clientId and c.id = :creditLinesId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("creditLinesId", creditLinesId).getResultList();
	}
	/**
	 * Find by reference: customerNotification
	 *
	 * @param customerNotification
	 * @return List<Customer>
	 */
	public List<Customer> findByCustomerNotification(
			CustomerNotification customerNotification) {
		return this.findByCustomerNotificationId(customerNotification.getId());
	}
	/**
	 * Find by ID of reference: customerNotification.id
	 *
	 * @param customerNotificationId
	 * @return List<Customer>
	 */
	public List<Customer> findByCustomerNotificationId(
			Integer customerNotificationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Customer e, IN (e.customerNotification) c where e.clientId = :clientId and c.id = :customerNotificationId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerNotificationId", customerNotificationId)
				.getResultList();
	}
	/**
	 * Find by reference: reseller
	 *
	 * @param reseller
	 * @return List<Customer>
	 */
	public List<Customer> findByReseller(Customer reseller) {
		return this.findByResellerId(reseller.getId());
	}
	/**
	 * Find by ID of reference: reseller.id
	 *
	 * @param resellerId
	 * @return List<Customer>
	 */
	public List<Customer> findByResellerId(Integer resellerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Customer e, IN (e.reseller) c where e.clientId = :clientId and c.id = :resellerId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("resellerId", resellerId).getResultList();
	}
	/**
	 * Find by reference: finalCustomer
	 *
	 * @param finalCustomer
	 * @return List<Customer>
	 */
	public List<Customer> findByFinalCustomer(Customer finalCustomer) {
		return this.findByFinalCustomerId(finalCustomer.getId());
	}
	/**
	 * Find by ID of reference: finalCustomer.id
	 *
	 * @param finalCustomerId
	 * @return List<Customer>
	 */
	public List<Customer> findByFinalCustomerId(Integer finalCustomerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Customer e, IN (e.finalCustomer) c where e.clientId = :clientId and c.id = :finalCustomerId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("finalCustomerId", finalCustomerId)
				.getResultList();
	}
	/**
	 * Find by reference: exposure
	 *
	 * @param exposure
	 * @return List<Customer>
	 */
	public List<Customer> findByExposure(Exposure exposure) {
		return this.findByExposureId(exposure.getId());
	}
	/**
	 * Find by ID of reference: exposure.id
	 *
	 * @param exposureId
	 * @return List<Customer>
	 */
	public List<Customer> findByExposureId(Integer exposureId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Customer e, IN (e.exposure) c where e.clientId = :clientId and c.id = :exposureId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("exposureId", exposureId).getResultList();
	}
	/**
	 * Find by reference: masterAgreement
	 *
	 * @param masterAgreement
	 * @return List<Customer>
	 */
	public List<Customer> findByMasterAgreement(MasterAgreement masterAgreement) {
		return this.findByMasterAgreementId(masterAgreement.getId());
	}
	/**
	 * Find by ID of reference: masterAgreement.id
	 *
	 * @param masterAgreementId
	 * @return List<Customer>
	 */
	public List<Customer> findByMasterAgreementId(Integer masterAgreementId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Customer e, IN (e.masterAgreement) c where e.clientId = :clientId and c.id = :masterAgreementId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("masterAgreementId", masterAgreementId)
				.getResultList();
	}
	/**
	 * Find by reference: aircraft
	 *
	 * @param aircraft
	 * @return List<Customer>
	 */
	public List<Customer> findByAircraft(Aircraft aircraft) {
		return this.findByAircraftId(aircraft.getId());
	}
	/**
	 * Find by ID of reference: aircraft.id
	 *
	 * @param aircraftId
	 * @return List<Customer>
	 */
	public List<Customer> findByAircraftId(Integer aircraftId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Customer e, IN (e.aircraft) c where e.clientId = :clientId and c.id = :aircraftId",
						Customer.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("aircraftId", aircraftId).getResultList();
	}
}
