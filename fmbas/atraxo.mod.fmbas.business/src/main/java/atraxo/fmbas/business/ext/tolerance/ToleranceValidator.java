package atraxo.fmbas.business.ext.tolerance;

import java.math.BigDecimal;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.tolerance.ToleranceException;
import atraxo.fmbas.business.ext.exceptions.tolerance.ToleranceLimitsException;
import atraxo.fmbas.business.ext.exceptions.tolerance.ToleranceTypeException;
import atraxo.fmbas.domain.impl.fmbas_type.ToleranceType;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;

/**
 * Utility class used to validate tolerance data.
 *
 * @author zspeter
 */
public class ToleranceValidator {

	public void validateValues(BigDecimal oldValue, BigDecimal referenceValue) throws ToleranceException {
		if (referenceValue == null) {
			throw new ToleranceException(BusinessErrorCode.INVALID_TOLERANCE_VALUE, "Reference value cannot be null");
		}
		if (oldValue == null) {
			throw new ToleranceException(BusinessErrorCode.INVALID_TOLERANCE_VALUE, "Current value cannot be null");
		}
	}

	/**
	 * @param tolerance
	 * @throws ToleranceLimitsException
	 * @throws ToleranceTypeException
	 */
	public void validateTolerance(Tolerance tolerance) throws ToleranceException, ToleranceTypeException {
		if ((tolerance.getAbsDevDown() == null && tolerance.getRelativeDevDown() == null)
				|| (tolerance.getAbsDevUp() == null && tolerance.getRelativeDevUp() == null)) {
			throw new ToleranceLimitsException();
		}

		if (tolerance.getAbsDevDown() != null || tolerance.getAbsDevUp() != null) {
			this.validateUnit(tolerance);
		}
	}

	/**
	 * @param tolerance
	 * @throws ToleranceTypeException
	 */
	private void validateUnit(Tolerance tolerance) throws ToleranceException {
		ToleranceType toleranceType = tolerance.getToleranceType();

		switch (toleranceType) {
		case _COST_:
			if (tolerance.getCurrency() == null) {
				throw new ToleranceException(BusinessErrorCode.INVALID_TOLERANCE_CURRENCY, "Tolerance type costs must have currency set.");
			}
			break;
		case _PRICE_:
			if (tolerance.getCurrency() == null) {
				throw new ToleranceException(BusinessErrorCode.INVALID_TOLERANCE_CURRENCY, "Tolerance type price must have currency set.");
			}
			if (tolerance.getRefUnit() == null) {
				throw new ToleranceException(BusinessErrorCode.INVALID_TOLERANCE_UNIT, "Tolerance type price must have unit set.");
			}
			break;
		case _QUANTITY_:
			if (tolerance.getRefUnit() == null) {
				throw new ToleranceException(BusinessErrorCode.INVALID_TOLERANCE_UNIT, "Tolerance type quantity must have unit set.");
			}
			break;
		default:
		}
	}

}
