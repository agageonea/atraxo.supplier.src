/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.geo;

import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.geo.Locations;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Area} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Area_Service extends AbstractEntityService<Area> {

	/**
	 * Public constructor for Area_Service
	 */
	public Area_Service() {
		super();
	}

	/**
	 * Public constructor for Area_Service
	 * 
	 * @param em
	 */
	public Area_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Area> getEntityClass() {
		return Area.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Area
	 */
	public Area findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Area.NQ_FIND_BY_CODE, Area.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Area", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Area", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Area
	 */
	public Area findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Area.NQ_FIND_BY_BUSINESS, Area.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Area", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Area", "id"), nure);
		}
	}

	/**
	 * Find by reference: locations
	 *
	 * @param locations
	 * @return List<Area>
	 */
	public List<Area> findByLocations(Locations locations) {
		return this.findByLocationsId(locations.getId());
	}
	/**
	 * Find by ID of reference: locations.id
	 *
	 * @param locationsId
	 * @return List<Area>
	 */
	public List<Area> findByLocationsId(Integer locationsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Area e, IN (e.locations) c where e.clientId = :clientId and c.id = :locationsId",
						Area.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("locationsId", locationsId).getResultList();
	}
}
