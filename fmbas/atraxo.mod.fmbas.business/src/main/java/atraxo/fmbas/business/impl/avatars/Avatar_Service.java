/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.avatars;

import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.ad.domain.impl.security.User;
import atraxo.fmbas.domain.impl.avatars.Avatar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Avatar} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Avatar_Service extends AbstractEntityService<Avatar> {

	/**
	 * Public constructor for Avatar_Service
	 */
	public Avatar_Service() {
		super();
	}

	/**
	 * Public constructor for Avatar_Service
	 * 
	 * @param em
	 */
	public Avatar_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Avatar> getEntityClass() {
		return Avatar.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Avatar
	 */
	public Avatar findById(String id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Avatar.NQ_FIND_BY_ID, Avatar.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Avatar", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Avatar", "id"), nure);
		}
	}

	/**
	 * Find by reference: user
	 *
	 * @param user
	 * @return List<Avatar>
	 */
	public List<Avatar> findByUser(User user) {
		return this.findByUserId(user.getId());
	}
	/**
	 * Find by ID of reference: user.id
	 *
	 * @param userId
	 * @return List<Avatar>
	 */
	public List<Avatar> findByUserId(String userId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Avatar e where e.clientId = :clientId and e.user.id = :userId",
						Avatar.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("userId", userId).getResultList();
	}
	/**
	 * Find by reference: type
	 *
	 * @param type
	 * @return List<Avatar>
	 */
	public List<Avatar> findByType(AttachmentType type) {
		return this.findByTypeId(type.getId());
	}
	/**
	 * Find by ID of reference: type.id
	 *
	 * @param typeId
	 * @return List<Avatar>
	 */
	public List<Avatar> findByTypeId(String typeId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Avatar e where e.clientId = :clientId and e.type.id = :typeId",
						Avatar.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("typeId", typeId).getResultList();
	}
}
