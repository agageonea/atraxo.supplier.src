package atraxo.fmbas.business.ext.util;

import java.math.BigDecimal;

/**
 * Number utility class
 *
 * @author vhojda
 */
public class NumberUtil {

	/**
	 * Private constructor to avoid instantiation
	 */
	private NumberUtil() {

	}

	private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

	/**
	 * @param base
	 * @param pct
	 * @return
	 */
	public static BigDecimal percentage(BigDecimal base, BigDecimal pct) {
		return base.multiply(pct).divide(ONE_HUNDRED);
	}

	/**
	 * @param number
	 * @return
	 */
	public static boolean isWholeNumber(BigDecimal number) {
		return number.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) == 0;
	}
}
