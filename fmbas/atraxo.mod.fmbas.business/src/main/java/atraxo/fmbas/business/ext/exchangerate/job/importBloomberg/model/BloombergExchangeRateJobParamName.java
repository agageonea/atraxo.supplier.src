package atraxo.fmbas.business.ext.exchangerate.job.importBloomberg.model;

public enum BloombergExchangeRateJobParamName {
	SOURCE_URL("SOURCE URL"), USERNAME("USERNAME"), PASSWORD("PASSWORD"), OVERWRITE("OVERWRITE"), PRICING_FIELD("PRICING FIELD");

	private String value;

	private String getValue() {
		return this.value;
	}

	private BloombergExchangeRateJobParamName(String value) {
		this.value = value;
	}

	public static BloombergExchangeRateJobParamName getByValue(String value) {
		BloombergExchangeRateJobParamName param = null;
		for (BloombergExchangeRateJobParamName pn : values()) {
			if (pn.getValue().equals(value)) {
				param = pn;
				break;
			}
		}
		return param;
	}
}
