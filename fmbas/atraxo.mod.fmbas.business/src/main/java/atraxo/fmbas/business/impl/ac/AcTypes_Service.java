/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.ac;

import atraxo.fmbas.business.api.ac.IAcTypesService;
import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link AcTypes} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class AcTypes_Service extends AbstractEntityService<AcTypes>
		implements
			IAcTypesService {

	/**
	 * Public constructor for AcTypes_Service
	 */
	public AcTypes_Service() {
		super();
	}

	/**
	 * Public constructor for AcTypes_Service
	 * 
	 * @param em
	 */
	public AcTypes_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<AcTypes> getEntityClass() {
		return AcTypes.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return AcTypes
	 */
	public AcTypes findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(AcTypes.NQ_FIND_BY_CODE, AcTypes.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"AcTypes", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"AcTypes", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return AcTypes
	 */
	public AcTypes findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(AcTypes.NQ_FIND_BY_BUSINESS,
							AcTypes.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"AcTypes", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"AcTypes", "id"), nure);
		}
	}

	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<AcTypes>
	 */
	public List<AcTypes> findByUnit(Unit unit) {
		return this.findByUnitId(unit.getId());
	}
	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<AcTypes>
	 */
	public List<AcTypes> findByUnitId(Integer unitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from AcTypes e where e.clientId = :clientId and e.unit.id = :unitId",
						AcTypes.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitId", unitId).getResultList();
	}
}
