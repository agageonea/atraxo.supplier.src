package atraxo.fmbas.business.ext.exceptions.conversion;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.IErrorCode;

public class UnitTyeException extends BusinessException {

	private static final long serialVersionUID = -8813739247962745135L;

	public UnitTyeException(IErrorCode errorCode, String errorDetails) {
		super(errorCode, errorDetails);
	}

}
