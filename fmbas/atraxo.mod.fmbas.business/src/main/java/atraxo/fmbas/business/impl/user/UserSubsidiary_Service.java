/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.user;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.user.UserSubsidiary;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link UserSubsidiary} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class UserSubsidiary_Service
		extends
			AbstractEntityService<UserSubsidiary> {

	/**
	 * Public constructor for UserSubsidiary_Service
	 */
	public UserSubsidiary_Service() {
		super();
	}

	/**
	 * Public constructor for UserSubsidiary_Service
	 * 
	 * @param em
	 */
	public UserSubsidiary_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<UserSubsidiary> getEntityClass() {
		return UserSubsidiary.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return UserSubsidiary
	 */
	public UserSubsidiary findByKey(UserSupp user, Customer subsidiary) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(UserSubsidiary.NQ_FIND_BY_KEY,
							UserSubsidiary.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("user", user)
					.setParameter("subsidiary", subsidiary).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"UserSubsidiary", "user, subsidiary"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"UserSubsidiary", "user, subsidiary"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return UserSubsidiary
	 */
	public UserSubsidiary findByKey(Long userId, Long subsidiaryId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(UserSubsidiary.NQ_FIND_BY_KEY_PRIMITIVE,
							UserSubsidiary.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("userId", userId)
					.setParameter("subsidiaryId", subsidiaryId)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"UserSubsidiary", "userId, subsidiaryId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"UserSubsidiary", "userId, subsidiaryId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return UserSubsidiary
	 */
	public UserSubsidiary findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(UserSubsidiary.NQ_FIND_BY_BUSINESS,
							UserSubsidiary.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"UserSubsidiary", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"UserSubsidiary", "id"), nure);
		}
	}

	/**
	 * Find by reference: user
	 *
	 * @param user
	 * @return List<UserSubsidiary>
	 */
	public List<UserSubsidiary> findByUser(UserSupp user) {
		return this.findByUserId(user.getId());
	}
	/**
	 * Find by ID of reference: user.id
	 *
	 * @param userId
	 * @return List<UserSubsidiary>
	 */
	public List<UserSubsidiary> findByUserId(String userId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from UserSubsidiary e where e.clientId = :clientId and e.user.id = :userId",
						UserSubsidiary.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("userId", userId).getResultList();
	}
	/**
	 * Find by reference: subsidiary
	 *
	 * @param subsidiary
	 * @return List<UserSubsidiary>
	 */
	public List<UserSubsidiary> findBySubsidiary(Customer subsidiary) {
		return this.findBySubsidiaryId(subsidiary.getId());
	}
	/**
	 * Find by ID of reference: subsidiary.id
	 *
	 * @param subsidiaryId
	 * @return List<UserSubsidiary>
	 */
	public List<UserSubsidiary> findBySubsidiaryId(Integer subsidiaryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from UserSubsidiary e where e.clientId = :clientId and e.subsidiary.id = :subsidiaryId",
						UserSubsidiary.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("subsidiaryId", subsidiaryId).getResultList();
	}
}
