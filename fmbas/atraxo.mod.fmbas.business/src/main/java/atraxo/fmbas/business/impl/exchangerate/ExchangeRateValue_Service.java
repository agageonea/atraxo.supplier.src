/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.exchangerate;

import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ExchangeRateValue} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ExchangeRateValue_Service
		extends
			AbstractEntityService<ExchangeRateValue> {

	/**
	 * Public constructor for ExchangeRateValue_Service
	 */
	public ExchangeRateValue_Service() {
		super();
	}

	/**
	 * Public constructor for ExchangeRateValue_Service
	 * 
	 * @param em
	 */
	public ExchangeRateValue_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ExchangeRateValue> getEntityClass() {
		return ExchangeRateValue.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ExchangeRateValue
	 */
	public ExchangeRateValue findByBusinessKey(ExchangeRate exchRate,
			Date vldFrDtRef, Date vldToDtRef) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ExchangeRateValue.NQ_FIND_BY_BUSINESSKEY,
							ExchangeRateValue.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("exchRate", exchRate)
					.setParameter("vldFrDtRef", vldFrDtRef)
					.setParameter("vldToDtRef", vldToDtRef).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ExchangeRateValue",
							"exchRate, vldFrDtRef, vldToDtRef"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ExchangeRateValue",
							"exchRate, vldFrDtRef, vldToDtRef"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return ExchangeRateValue
	 */
	public ExchangeRateValue findByBusinessKey(Long exchRateId,
			Date vldFrDtRef, Date vldToDtRef) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							ExchangeRateValue.NQ_FIND_BY_BUSINESSKEY_PRIMITIVE,
							ExchangeRateValue.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("exchRateId", exchRateId)
					.setParameter("vldFrDtRef", vldFrDtRef)
					.setParameter("vldToDtRef", vldToDtRef).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ExchangeRateValue",
							"exchRateId, vldFrDtRef, vldToDtRef"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ExchangeRateValue",
							"exchRateId, vldFrDtRef, vldToDtRef"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return ExchangeRateValue
	 */
	public ExchangeRateValue findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ExchangeRateValue.NQ_FIND_BY_BUSINESS,
							ExchangeRateValue.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ExchangeRateValue", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ExchangeRateValue", "id"), nure);
		}
	}

	/**
	 * Find by reference: exchRate
	 *
	 * @param exchRate
	 * @return List<ExchangeRateValue>
	 */
	public List<ExchangeRateValue> findByExchRate(ExchangeRate exchRate) {
		return this.findByExchRateId(exchRate.getId());
	}
	/**
	 * Find by ID of reference: exchRate.id
	 *
	 * @param exchRateId
	 * @return List<ExchangeRateValue>
	 */
	public List<ExchangeRateValue> findByExchRateId(Integer exchRateId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ExchangeRateValue e where e.clientId = :clientId and e.exchRate.id = :exchRateId",
						ExchangeRateValue.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("exchRateId", exchRateId).getResultList();
	}
}
