package atraxo.fmbas.business.ext.ftp;

import java.util.List;
import java.util.Map;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;

/**
 * @author zspeter
 */
public class FtpCommunicationResult {

	/**
	 * enity map with a list of uploaded entities and a list of failed to upload entities.
	 */
	private Map<String, List<AbstractEntity>> map;
	/**
	 * Result message.
	 */
	private String message;

	public Map<String, List<AbstractEntity>> getMap() {
		return this.map;
	}

	public void setMap(Map<String, List<AbstractEntity>> map) {
		this.map = map;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
