/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.soneViewState.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import atraxo.ad.business.api.security.IUserService;
import atraxo.ad.domain.impl.security.User;
import atraxo.fmbas.business.api.soneDefaultViewState.ISoneDefaultViewStateService;
import atraxo.fmbas.business.api.soneViewState.ISoneViewStateService;
import atraxo.fmbas.business.ext.exceptions.DuplicateViewNameException;
import atraxo.fmbas.domain.impl.soneDefaultViewState.SoneDefaultViewState;
import atraxo.fmbas.domain.impl.soneViewState.SoneViewState;

/**
 * Business extensions specific for {@link SoneViewState} domain entity.
 */
public class SoneViewState_Service extends atraxo.fmbas.business.impl.soneViewState.SoneViewState_Service implements ISoneViewStateService {

	@Override
	protected void preInsert(SoneViewState e) throws BusinessException {
		super.preInsert(e);
		this.updateDefaultView(e);
		this.checkDuplicate(e);
	}

	@Override
	protected void preUpdate(SoneViewState e) throws BusinessException {
		super.preUpdate(e);
		this.updateDefaultView(e);
		// this.checkDuplicate(e);
	}

	// Dan: deleteDefaultViewState

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		// TODO Auto-generated method stub
		super.postDeleteByIds(ids, context);

		ISoneDefaultViewStateService srv = (ISoneDefaultViewStateService) this.findEntityService(SoneDefaultViewState.class);
		ISoneViewStateService vsSrv = (ISoneViewStateService) this.findEntityService(SoneViewState.class);

		IUserService userSrv = (IUserService) this.findEntityService(User.class);
		User user = userSrv.findByCode(Session.user.get().getCode());

		List<SoneViewState> vsList = vsSrv.findByIds(ids);
		List<SoneDefaultViewState> soneDefaultViewState = new ArrayList<SoneDefaultViewState>();

		Map<String, Object> params = new HashMap<String, Object>();

		for (SoneViewState vs : vsList) {

			params.put("view", vs);
			params.put("user", user);

			soneDefaultViewState.addAll(srv.findEntitiesByAttributes(SoneDefaultViewState.class, params));
		}
		srv.delete(soneDefaultViewState);
	}

	private void checkDuplicate(SoneViewState e) throws BusinessException {
		if (e.getName() == null) {
			return;
		}

		StringBuilder sb = new StringBuilder();
		sb.append("select e from ").append(SoneViewState.class.getSimpleName());
		sb.append(" e where e.clientId =:clientId and e.name = :filterName and e.cmp = :cmp and e.createdBy = :createdBy");

		TypedQuery<SoneViewState> query = this.getEntityManager().createQuery(sb.toString(), SoneViewState.class)
				.setParameter("clientId", Session.user.get().getClient().getId());
		query.setParameter("filterName", e.getName());
		query.setParameter("cmp", e.getCmp());
		query.setParameter("createdBy", Session.user.get().getCode());

		List<SoneViewState> viewStateList = query.getResultList();
		Integer listSize = viewStateList.size();

		if (listSize > 0) {

			throw new DuplicateViewNameException("A view with this name already exists!");
		}

	}

	private void updateDefaultView(SoneViewState e) throws BusinessException {
		if (e.getDefaultView() == null || !e.getDefaultView()) {
			return;
		}

		StringBuilder sb = new StringBuilder();
		sb.append("select e from ").append(SoneViewState.class.getSimpleName());
		sb.append(" e where e.clientId =:clientId and e.defaultView = :defaultView and e.cmp = :cmp and e.createdBy = :createdBy");

		TypedQuery<SoneViewState> query = this.getEntityManager().createQuery(sb.toString(), SoneViewState.class)
				.setParameter("clientId", Session.user.get().getClient().getId());
		query.setParameter("defaultView", e.getDefaultView());
		query.setParameter("cmp", e.getCmp());
		query.setParameter("createdBy", Session.user.get().getCode());

		List<SoneViewState> viewStateList = query.getResultList();
		for (SoneViewState soneViewState : viewStateList) {
			soneViewState.setDefaultView(false);
		}
		this.update(viewStateList);
	}

}
