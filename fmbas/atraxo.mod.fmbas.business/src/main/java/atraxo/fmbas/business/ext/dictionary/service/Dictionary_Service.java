/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.dictionary.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.dictionary.IConditionService;
import atraxo.fmbas.business.api.dictionary.IDictionaryService;
import atraxo.fmbas.business.ext.dictionary.delegate.FieldListBuilder;
import atraxo.fmbas.domain.impl.dictionary.Condition;
import atraxo.fmbas.domain.impl.dictionary.Dictionary;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceParameter;
import atraxo.fmbas.domain.impl.fmbas_type.DictionaryOperator;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link Dictionary} domain entity.
 */
public class Dictionary_Service extends atraxo.fmbas.business.impl.dictionary.Dictionary_Service implements IDictionaryService {

	private static final String DICTIONARY_CLASS_NAME = "Dictionary class name";

	private static final Logger LOG = LoggerFactory.getLogger(Dictionary_Service.class);

	@Autowired
	private IConditionService conditionService;

	@Override
	public void translate(ExternalInterface externalInterface, Object entity) throws BusinessException {
		String translateClassName = this.getTranslateClass(externalInterface);
		try {
			Class<?> clazz = Class.forName(translateClassName);
			LOG.debug("Start translate.");
			LOG.debug("Class to translate:" + translateClassName);
			if (!translateClassName.isEmpty() && entity.getClass().isAssignableFrom(clazz)
					&& !CollectionUtils.isEmpty(externalInterface.getDictionaries())) {
				for (Dictionary d : externalInterface.getDictionaries()) {
					HashMap<String, Object> map = this.getValueList(d, entity, clazz);
					if (this.canTranslate(map, d.getConditions())) {
						LOG.debug("Field name to translate:" + d.getFieldName());
						FieldListBuilder.setValue(clazz, entity, d.getFieldName(), d.getNewValue(), d.getOriginalValue());
					}
				}
			}
			LOG.debug("End translate");
		} catch (ClassNotFoundException | NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			LOG.warn("Translate problem.", e);
			LOG.debug("End translate");
		}
	}

	private boolean canTranslate(HashMap<String, Object> map, Collection<Condition> conditions) {
		for (Condition c : conditions) {
			boolean operator = DictionaryOperator._Equal_.equals(c.getOperator());
			Object mapValue = map.get(c.getFieldName());
			if (mapValue.getClass().isEnum()) {
				mapValue = mapValue.toString();
			}
			if ((!c.getValue().equals(mapValue.toString()) && operator) || (c.getValue().equals(mapValue.toString()) && !operator)) {
				return false;
			}
		}
		return true;
	}

	private HashMap<String, Object> getValueList(Dictionary dictionary, Object entity, Class<?> clazz)
			throws NoSuchFieldException, IllegalAccessException {
		HashMap<String, Object> retMap = new HashMap<>();
		for (Condition c : dictionary.getConditions()) {
			Object value = FieldListBuilder.getValue(clazz, entity, c.getFieldName());
			retMap.put(c.getFieldName(), value);
		}
		return retMap;
	}

	private String getTranslateClass(ExternalInterface externalInterface) {
		if (!CollectionUtils.isEmpty(externalInterface.getParams())) {
			for (ExternalInterfaceParameter eip : externalInterface.getParams()) {
				if (DICTIONARY_CLASS_NAME.equals(eip.getName())) {
					return eip.getDefaultValue();
				}
			}
		}
		return "";
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		List<Object> deleteList = new ArrayList<>();
		for (Dictionary dictionary : this.findByIds(ids)) {
			for (Condition c : dictionary.getConditions()) {
				deleteList.add(c.getId());
			}
		}
		if (!deleteList.isEmpty()) {
			this.conditionService.deleteByIds(deleteList);
		}
	}
}
