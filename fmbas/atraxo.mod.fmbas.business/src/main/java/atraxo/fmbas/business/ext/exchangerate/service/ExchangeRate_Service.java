/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.exchangerate.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityNotFoundException;

import atraxo.fmbas.business.api.exchangerate.IExchangeRateService;
import atraxo.fmbas.business.api.exchangerate.IExchangeRateValueService;
import atraxo.fmbas.business.ext.exceptions.ExchangeRateNotFoundException;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link ExchangeRate} domain entity.
 */
public class ExchangeRate_Service extends atraxo.fmbas.business.impl.exchangerate.ExchangeRate_Service implements IExchangeRateService {

	@Override
	public ExchangeRate getByTimeseriesAndAvgMethod(Integer timeSeriesNo, AverageMethod averageMethod) {
		List<ExchangeRate> exchangeRate = this.getEntityManager()
				.createQuery(
						"select e from " + ExchangeRate.class.getSimpleName()
								+ " e where e.clientId = :clientId and e.avgMethodIndicator.id = :avgMethodIndicatorId and e.tserie.id = :timeSeriesNo ",
						ExchangeRate.class)
				.setParameter("clientId", Session.user.get().getClient().getId()).setParameter("avgMethodIndicatorId", averageMethod.getId())
				.setParameter("timeSeriesNo", timeSeriesNo).getResultList();
		if (exchangeRate.isEmpty()) {
			throw new EntityNotFoundException();
		}

		return exchangeRate.get(0);
	}

	@Override
	public ExchangeRate getExchangeRate(Currencies fromCurrency, Currencies toCurrency, FinancialSources financialSource, AverageMethod averageMethod)
			throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("currency1", fromCurrency);
		params.put("currency2", toCurrency);
		params.put("finsource", financialSource);
		params.put("avgMethodIndicator", averageMethod);
		List<ExchangeRate> exchangeRates = this.findEntitiesByAttributes(params);
		if (exchangeRates.isEmpty()) {
			throw new ExchangeRateNotFoundException("Exchange rate not found from " + fromCurrency.getCode() + " to " + toCurrency.getCode()
					+ " using financial source " + financialSource.getCode() + " and average method " + averageMethod.getCode() + ".");
		}
		return exchangeRates.get(0);
	}

	@Override
	protected void preDelete(ExchangeRate e) throws BusinessException {
		super.preDelete(e);
		IExchangeRateValueService service = (IExchangeRateValueService) this.findEntityService(ExchangeRateValue.class);
		List<ExchangeRateValue> exchValues = service.findByExchRate(e);
		service.delete(exchValues);
	}

	@Override
	public ConversionResult convertExchangeRate(Currencies fromCurrency, Currencies toCurrency, Date date, BigDecimal price, Boolean strict)
			throws BusinessException {
		ExchangeRate_Bd bd = this.getBusinessDelegate(ExchangeRate_Bd.class);
		return bd.convert(fromCurrency, toCurrency, date, price, strict);
	}

}
