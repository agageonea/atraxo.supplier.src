package atraxo.fmbas.business.ext.timeserie.job.importPlatts;

import java.io.File;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.file.MultiResourceItemReader;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Platts import files job listener class
 *
 * @author aradu
 * @param <T>
 */

public class PlattsImportJobListener<T> implements StepExecutionListener, ItemReadListener<T> {

	/**
	 *
	 */
	private static final String FILES_NAME_INFO = "filesNameInfo";
	private StepExecution stepExecution;
	private Object multiResourceItemReader;

	private static final Logger LOGGER = LoggerFactory.getLogger(PlattsImportJobListener.class);

	public void setMultiResourceItemReader(Object multiResourceItemReader) {
		this.multiResourceItemReader = multiResourceItemReader;
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
		this.stepExecution.getJobExecution().getExecutionContext().put(FILES_NAME_INFO, new HashMap<File, Boolean>());
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		return stepExecution.getExitStatus();
	}

	@Override
	public void beforeRead() {
		// there is nothing to do before read.
		// can't delete this method because it must me implemented
	}

	@Override
	public void afterRead(T item) {
		try {
			this.logFileProcessStatus(true);
		} catch (BusinessException e) {
			LOGGER.error("Platts afterRead, error", e);
		}
	}

	@Override
	public void onReadError(Exception ex) {
		try {
			this.logFileProcessStatus(false);
		} catch (BusinessException e) {
			LOGGER.error("Platts onReadError, error", e);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void logFileProcessStatus(Boolean successStatus) throws BusinessException {
		HashMap<File, Boolean> filesNameInfo = (HashMap<File, Boolean>) this.stepExecution.getJobExecution().getExecutionContext()
				.get(FILES_NAME_INFO);

		if (this.multiResourceItemReader instanceof Advised) {
			try {
				Advised advised = (Advised) this.multiResourceItemReader;
				Object obj = advised.getTargetSource().getTarget();
				MultiResourceItemReader mrirTarget = (MultiResourceItemReader) obj;
				if (mrirTarget != null && mrirTarget.getCurrentResource() != null) {
					File filePath = mrirTarget.getCurrentResource().getFile();
					if (!filesNameInfo.containsKey(filePath) || !successStatus && filesNameInfo.get(filePath)) {
						filesNameInfo.put(filePath, successStatus);
					}
				}
			} catch (Exception exep) {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(exep.getMessage(), exep);
				}
				throw new BusinessException(BusinessErrorCode.PLATTS_CANT_FIND_FILE_NAME, BusinessErrorCode.PLATTS_CANT_FIND_FILE_NAME.getErrMsg(),
						exep);
			}
		}

		this.stepExecution.getJobExecution().getExecutionContext().put(FILES_NAME_INFO, filesNameInfo);
	}

}
