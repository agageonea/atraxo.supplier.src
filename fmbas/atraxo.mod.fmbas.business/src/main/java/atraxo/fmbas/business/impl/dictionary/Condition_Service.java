/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.dictionary;

import atraxo.fmbas.business.api.dictionary.IConditionService;
import atraxo.fmbas.domain.impl.dictionary.Condition;
import atraxo.fmbas.domain.impl.dictionary.Dictionary;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Condition} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Condition_Service extends AbstractEntityService<Condition>
		implements
			IConditionService {

	/**
	 * Public constructor for Condition_Service
	 */
	public Condition_Service() {
		super();
	}

	/**
	 * Public constructor for Condition_Service
	 * 
	 * @param em
	 */
	public Condition_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Condition> getEntityClass() {
		return Condition.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Condition
	 */
	public Condition findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Condition.NQ_FIND_BY_BUSINESS,
							Condition.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Condition", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Condition", "id"), nure);
		}
	}

	/**
	 * Find by reference: dictionary
	 *
	 * @param dictionary
	 * @return List<Condition>
	 */
	public List<Condition> findByDictionary(Dictionary dictionary) {
		return this.findByDictionaryId(dictionary.getId());
	}
	/**
	 * Find by ID of reference: dictionary.id
	 *
	 * @param dictionaryId
	 * @return List<Condition>
	 */
	public List<Condition> findByDictionaryId(Integer dictionaryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Condition e where e.clientId = :clientId and e.dictionary.id = :dictionaryId",
						Condition.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("dictionaryId", dictionaryId).getResultList();
	}
}
