/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.tolerance.service;

import java.util.HashMap;
import java.util.Map;

import atraxo.fmbas.business.api.tolerance.IToleranceService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;

/**
 * Business extensions specific for {@link Tolerance} domain entity.
 */
public class Tolerance_Service extends atraxo.fmbas.business.impl.tolerance.Tolerance_Service implements IToleranceService {

	@Override
	public Tolerance getVolumeTolerance() throws BusinessException {
		return this.getTolerance("CHECK_CONTRACT_PRICE_VOLUME");
	}

	@Override
	public Tolerance getEventTolerance() throws BusinessException {
		return this.getTolerance("CHECK_CONTRACT_PRICE_EVENT");
	}

	@Override
	public Tolerance getPercentageTolerance() throws BusinessException {
		return this.getTolerance("CHECK_CONTRACT_PRICE_PERCENTAGE");
	}

	@Override
	public Tolerance getMarkUpTolerance() throws BusinessException {
		return this.getTolerance("CHECK_MARKUP");
	}

	@Override
	public Tolerance getTankCapacityTolerance() throws BusinessException {
		return this.getTolerance("CHECK_TANK_CAPACITY");
	}

	@Override
	protected void preInsert(Tolerance e) throws BusinessException {
		super.preInsert(e);
		this.deviationRelativeAbsolute(e);
	}

	@Override
	protected void preUpdate(Tolerance e) throws BusinessException {
		super.preUpdate(e);
		this.deviationRelativeAbsolute(e);
	}

	@Override
	public Tolerance getAccrualAutoCloseTolerance() throws BusinessException {
		return this.getTolerance("CHECK_ACCRUAL_AUTO_CLOSE");
	}

	@Override
	public Tolerance getConversionFactorTolerance() throws BusinessException {
		return this.getTolerance("CHECK_CONVERSION_FACTOR");
	}

	@Override
	public Tolerance getTolerance(String name) throws BusinessException {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("name", name);
			return this.findEntityByAttributes(params);
		} catch (ApplicationException e) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(e.getErrorCode())) {
				throw new BusinessException(BusinessErrorCode.NO_UNIQUE_ENITY, BusinessErrorCode.NO_UNIQUE_ENITY.getErrMsg() + name, e);
			}
			throw new BusinessException(BusinessErrorCode.NO_ENTITY,
					String.format(BusinessErrorCode.NO_ENTITY.getErrMsg(), Tolerance.class.getSimpleName(), name), e);
		}
	}

	/**
	 * Verifies the values on the relative and absolute differences
	 *
	 * @param e
	 * @throws BusinessException
	 */
	private void deviationRelativeAbsolute(Tolerance e) throws BusinessException {
		if (e.getAbsDevDown() == null && e.getAbsDevUp() == null && e.getRelativeDevDown() == null) {
			throw new BusinessException(BusinessErrorCode.TOLERANCE_ABOSLUTE_RELATIVE_DIFFERENCE,
					BusinessErrorCode.TOLERANCE_ABOSLUTE_RELATIVE_DIFFERENCE.getErrMsg());
		} else {
			if ((e.getAbsDevDown() != null && e.getAbsDevUp() == null) || (e.getAbsDevDown() == null && e.getAbsDevUp() != null)) {
				throw new BusinessException(BusinessErrorCode.TOLERANCE_ABSOLUTE_DIFFERENCE_INCOMPLETE,
						BusinessErrorCode.TOLERANCE_ABSOLUTE_DIFFERENCE_INCOMPLETE.getErrMsg());
			} else {
				if ((e.getRelativeDevDown() != null && e.getRelativeDevUp() == null)
						|| (e.getRelativeDevDown() == null && e.getRelativeDevUp() != null)) {
					throw new BusinessException(BusinessErrorCode.TOLERANCE_RELATIVE_DIFFERENCE_INCOMPLETE,
							BusinessErrorCode.TOLERANCE_RELATIVE_DIFFERENCE_INCOMPLETE.getErrMsg());
				}
			}
		}
	}
}
