/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.soneViewState;

import atraxo.fmbas.domain.impl.soneAdvancedFilter.SoneAdvancedFilter;
import atraxo.fmbas.domain.impl.soneViewState.SoneViewState;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link SoneViewState} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class SoneViewState_Service extends AbstractEntityService<SoneViewState> {

	/**
	 * Public constructor for SoneViewState_Service
	 */
	public SoneViewState_Service() {
		super();
	}

	/**
	 * Public constructor for SoneViewState_Service
	 * 
	 * @param em
	 */
	public SoneViewState_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<SoneViewState> getEntityClass() {
		return SoneViewState.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return SoneViewState
	 */
	public SoneViewState findById(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(SoneViewState.NQ_FIND_BY_ID,
							SoneViewState.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"SoneViewState", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"SoneViewState", "id"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return SoneViewState
	 */
	public SoneViewState findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(SoneViewState.NQ_FIND_BY_BUSINESS,
							SoneViewState.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"SoneViewState", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"SoneViewState", "id"), nure);
		}
	}

	/**
	 * Find by reference: advancedFilter
	 *
	 * @param advancedFilter
	 * @return List<SoneViewState>
	 */
	public List<SoneViewState> findByAdvancedFilter(
			SoneAdvancedFilter advancedFilter) {
		return this.findByAdvancedFilterId(advancedFilter.getId());
	}
	/**
	 * Find by ID of reference: advancedFilter.id
	 *
	 * @param advancedFilterId
	 * @return List<SoneViewState>
	 */
	public List<SoneViewState> findByAdvancedFilterId(Integer advancedFilterId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from SoneViewState e where e.clientId = :clientId and e.advancedFilter.id = :advancedFilterId",
						SoneViewState.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("advancedFilterId", advancedFilterId)
				.getResultList();
	}
}
