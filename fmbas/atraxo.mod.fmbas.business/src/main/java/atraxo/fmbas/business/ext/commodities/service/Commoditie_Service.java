/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.commodities.service;

import seava.j4e.api.exceptions.BusinessException;
import atraxo.fmbas.business.api.commodities.ICommoditieService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.commodities.Commoditie;

/**
 * Business extensions specific for {@link Commoditie} domain entity.
 */
public class Commoditie_Service extends atraxo.fmbas.business.impl.commodities.Commoditie_Service implements ICommoditieService {

	@Override
	protected void preInsert(Commoditie e) throws BusinessException {
		super.preInsert(e);
		this.validateDate(e);
	}

	@Override
	protected void preUpdate(Commoditie e) throws BusinessException {
		super.preUpdate(e);
		this.validateDate(e);
	}

	private void validateDate(Commoditie e) throws BusinessException {
		if (e.getValidTo().getTime() < e.getValidFrom().getTime()) {
			throw new BusinessException(BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
					BusinessErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO.getErrMsg());
		}
	}

}