package atraxo.fmbas.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;

public class DuplicateViewNameException extends BusinessException {

	public DuplicateViewNameException(String msg) {
		super(BusinessErrorCode.DUPLICATE_VIEW, msg);
	}

}
