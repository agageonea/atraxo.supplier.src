package atraxo.fmbas.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;

public class ResellerChangeException extends BusinessException {

	public ResellerChangeException() {
		super(BusinessErrorCode.RESELLER_CHANGE, BusinessErrorCode.RESELLER_CHANGE.getErrMsg());
	}
}
