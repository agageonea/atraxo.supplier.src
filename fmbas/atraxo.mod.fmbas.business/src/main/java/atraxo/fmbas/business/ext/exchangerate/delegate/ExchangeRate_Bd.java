package atraxo.fmbas.business.ext.exchangerate.delegate;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.exchangerate.IExchangeRateService;
import atraxo.fmbas.business.api.exchangerate.IExchangeRateValueService;
import atraxo.fmbas.business.api.financialSources.IFinancialSourcesService;
import atraxo.fmbas.business.api.timeserie.IAverageMethodService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieItemService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.business.ext.calculator.Calculator;
import atraxo.fmbas.business.ext.calculator.Method;
import atraxo.fmbas.business.ext.calculator.model.Result;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.ExchangeFactorException;
import atraxo.fmbas.business.ext.exceptions.ExchangeRateNotFoundException;
import atraxo.fmbas.business.ext.exceptions.ExchangeRateValueNotFoundException;
import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.PostTypeInd;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieAverage;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

/**
 * @author apetho
 */
public class ExchangeRate_Bd extends AbstractBusinessDelegate {

	private static final String WARNING_MSG = "Exchange rate not found.";

	private final class ExchangeRateValuesComparator implements Comparator<ExchangeRateValue> {
		@Override
		public int compare(ExchangeRateValue o1, ExchangeRateValue o2) {
			return o1.getVldFrDtRef().compareTo(o2.getVldToDtRef());
		}
	}

	private static final Logger LOG = LoggerFactory.getLogger(ExchangeRate_Bd.class);

	private IExchangeRateValueService exchangeRateValueService;
	private IExchangeRateService exchangeRateService;
	private ITimeSerieService timeSerieService;
	private ICurrenciesService currencyService;
	private IFinancialSourcesService financialSourceService;
	private IAverageMethodService averageMethodService;
	private ITimeSerieItemService timeSerieItemService;

	/**
	 * Update time series quotations.
	 *
	 * @param timeSerie
	 * @throws BusinessException
	 */
	public void updateAverages(TimeSerie timeSerie) throws BusinessException {
		IExchangeRateService srv = (IExchangeRateService) this.findEntityService(ExchangeRate.class);
		List<ExchangeRate> currentExchangeRates = srv.findByTserie(timeSerie);
		List<ExchangeRate> exchangeRates = new ArrayList<>();
		for (TimeSerieAverage avg : timeSerie.getTimeserieAvg()) {
			if (avg.getActive()) {
				exchangeRates.add(this.retriveExchangeRate(timeSerie.getId(), avg.getAveragingMethod()));
			}
		}
		Iterator<ExchangeRate> iter = currentExchangeRates.iterator();
		while (iter.hasNext()) {
			ExchangeRate exch = iter.next();
			if (exchangeRates.contains(exch)) {
				iter.remove();
			}
		}
		srv.delete(currentExchangeRates);
		for (ExchangeRate exch : exchangeRates) {
			List<ExchangeRateValue> newValues = this.calculateAvereages(exch.getAvgMethodIndicator(), new ArrayList<>(timeSerie.getTimeserieItems()),
					exch);
			this.merge(exch, newValues);
		}
		srv.update(exchangeRates);
	}

	private void merge(ExchangeRate exch, List<ExchangeRateValue> newList) {
		List<ExchangeRateValue> oldList = new ArrayList<>(exch.getExchangeRateValue());
		if (CollectionUtils.isEmpty(oldList)) {
			for (ExchangeRateValue e : newList) {
				exch.addToExchangeRateValue(e);
			}
			return;
		}
		ExchangeRateValuesComparator comp = new ExchangeRateValuesComparator();
		Collections.sort(oldList, comp);
		Collections.sort(newList, comp);
		Iterator<ExchangeRateValue> iter = oldList.iterator();
		while (iter.hasNext()) {
			ExchangeRateValue v = iter.next();
			if (!newList.contains(v)) {
				iter.remove();
			} else {
				ExchangeRateValue newValue = newList.get(newList.indexOf(v));
				if (!newValue.getRate().setScale(6, RoundingMode.HALF_UP).equals(v.getRate().setScale(6, RoundingMode.HALF_UP))) {
					v.setRate(newValue.getRate());
				}
			}
		}
		exch.setExchangeRateValue(oldList);
		for (ExchangeRateValue e : newList) {
			if (!oldList.contains(e)) {
				exch.addToExchangeRateValue(e);
			}
		}
	}

	public IExchangeRateValueService getExchangeRateValueService() throws BusinessException {
		if (this.exchangeRateValueService == null) {
			IExchangeRateValueService exchValueService = (IExchangeRateValueService) this.findEntityService(ExchangeRateValue.class);
			this.exchangeRateValueService = exchValueService;
		}
		return this.exchangeRateValueService;
	}

	public IExchangeRateService getExchangeRateService() throws BusinessException {
		if (this.exchangeRateService == null) {
			IExchangeRateService service = (IExchangeRateService) this.findEntityService(ExchangeRate.class);
			this.exchangeRateService = service;
		}
		return this.exchangeRateService;
	}

	public ITimeSerieService getTimeSerieService() throws BusinessException {
		if (this.timeSerieService == null) {
			ITimeSerieService srv = (ITimeSerieService) this.findEntityService(TimeSerie.class);
			this.timeSerieService = srv;
		}
		return this.timeSerieService;
	}

	public ICurrenciesService getCurrencyService() throws BusinessException {
		if (this.currencyService == null) {
			ICurrenciesService srv = (ICurrenciesService) this.findEntityService(Currencies.class);
			this.currencyService = srv;
		}
		return this.currencyService;
	}

	public IFinancialSourcesService getFinancialSourceService() throws BusinessException {
		if (this.financialSourceService == null) {
			IFinancialSourcesService srv = (IFinancialSourcesService) this.findEntityService(FinancialSources.class);
			this.financialSourceService = srv;
		}
		return this.financialSourceService;
	}

	public IAverageMethodService getAverageMethodService() throws BusinessException {
		if (this.averageMethodService == null) {
			IAverageMethodService srv = (IAverageMethodService) this.findEntityService(AverageMethod.class);
			this.averageMethodService = srv;
		}
		return this.averageMethodService;
	}

	public ITimeSerieItemService getTimeSerieItemService() throws BusinessException {
		if (this.timeSerieItemService == null) {
			ITimeSerieItemService srv = (ITimeSerieItemService) this.findEntityService(TimeSerieItem.class);
			this.timeSerieItemService = srv;
		}
		return this.timeSerieItemService;
	}

	/**
	 * Get exchange rate from database, if not exists creates one.
	 *
	 * @param timeSerieNo
	 * @param averageMethod
	 * @return
	 * @throws BusinessException
	 */
	private ExchangeRate retriveExchangeRate(Integer timeSerieNo, AverageMethod averageMethod) throws BusinessException {
		IExchangeRateService service = this.getExchangeRateService();
		try {
			return service.getByTimeseriesAndAvgMethod(timeSerieNo, averageMethod);
		} catch (EntityNotFoundException e) {
			LOG.warn(WARNING_MSG, e);
			return this.createExchangeRate(timeSerieNo, averageMethod, service);
		}
	}

	/**
	 * Create a new exchange rate.
	 *
	 * @param timeSerieNo
	 * @param averageMethod
	 * @param exchangeRateService
	 * @return
	 * @throws BusinessException
	 */
	private ExchangeRate createExchangeRate(Integer timeSerieNo, AverageMethod averageMethod, IExchangeRateService exchangeRateService)
			throws BusinessException {

		ITimeSerieService srv = this.getTimeSerieService();
		TimeSerie timeSerie = srv.findById(timeSerieNo);

		ExchangeRate exchangeRate = exchangeRateService.create();
		exchangeRate.setAvgMethodIndicator(averageMethod);
		exchangeRate.setCurrency1(timeSerie.getCurrency1Id());
		exchangeRate.setCurrency2(timeSerie.getCurrency2Id());
		exchangeRate.setTserie(timeSerie);
		exchangeRate.setName(timeSerie.getSerieName() + "/" + averageMethod.getCode());
		exchangeRate.setDescription(timeSerie.getDescription());
		exchangeRate.setFinsource(timeSerie.getFinancialSource());
		exchangeRateService.insert(exchangeRate);
		// quotationService.
		return exchangeRate;
	}

	/**
	 * Calculate averages.
	 *
	 * @param averageMethod
	 * @param list
	 * @return
	 * @throws BusinessException
	 */
	private List<ExchangeRateValue> calculateAvereages(AverageMethod averageMethod, List<TimeSerieItem> list, ExchangeRate exchangeRate)
			throws BusinessException {
		List<ExchangeRateValue> exchangeRateValues = new ArrayList<>();
		Method averageCalculationMethod = Method.getByName(averageMethod.getCode());
		Calculator calculator = averageCalculationMethod.getCalculator();
		// TODO place for optimization to not recalculate the average for the
		// whole period
		ITimeSerieItemService service = this.getTimeSerieItemService();
		List<TimeSerieItem> itemsForCalculation = calculator.getItemsForCalculation(list, service);
		List<Result> results = calculator.calculate(itemsForCalculation, list);
		for (Result result : results) {
			exchangeRateValues.add(result.getResultAsExchangeRateValue(exchangeRate));
		}

		return exchangeRateValues;
	}

	/**
	 * @param fromCurrency
	 * @param toCurrency
	 * @param date
	 * @param value
	 * @param strict
	 * @return
	 * @throws BusinessException
	 */
	public ConversionResult convert(Currencies fromCurrency, Currencies toCurrency, Date date, BigDecimal value, boolean strict)
			throws BusinessException {
		AverageMethod averageMethod = this.getDefaultAverageMethod();
		FinancialSources standardFinancialSource = this.getStandardFinancialSource();
		return this.convert(fromCurrency, toCurrency, date, value, strict, averageMethod, standardFinancialSource);
	}

	/**
	 * @param fromCurrency
	 * @param toCurrency
	 * @param date
	 * @param value
	 * @param strict
	 * @param avgMethod
	 * @param financialSource
	 * @return
	 * @throws BusinessException
	 */
	public ConversionResult convert(Currencies fromCurrency, Currencies toCurrency, Date date, BigDecimal value, boolean strict,
			AverageMethod avgMethod, FinancialSources financialSource) throws BusinessException {
		BigDecimal factor = this.findFactor(fromCurrency, toCurrency, financialSource, avgMethod, date, strict);
		ConversionResult result = new ConversionResult();
		result.setFactor(factor);
		result.setValue(factor.multiply(value, MathContext.DECIMAL64));
		return result;
	}

	private BigDecimal findFactor(Currencies fromCurrency, Currencies toCurrency, FinancialSources financialSource, AverageMethod avgMethod,
			Date date, boolean strict) throws BusinessException {
		try {
			return this.findDirectFactor(fromCurrency, toCurrency, financialSource, avgMethod, date, strict);
		} catch (ExchangeRateValueNotFoundException e) {
			LOG.warn("Exchange rate value not found.", e);
			// failed to find direct conversion factor. try triangulation
			return this.findFactorByTriangulation(fromCurrency, toCurrency, financialSource, avgMethod, date, strict);
		} catch (ExchangeRateNotFoundException e) {
			LOG.warn(WARNING_MSG, e);
			// failed to find direct conversion factor. try triangulation
			return this.findFactorByTriangulation(fromCurrency, toCurrency, financialSource, avgMethod, date, strict);
		}
	}

	private BigDecimal findFactorByTriangulation(Currencies fromCurrency, Currencies toCurrency, FinancialSources financialSource,
			AverageMethod avgMethod, Date date, boolean strict) throws BusinessException {
		Currencies sysCurrency = this.getSystemCurrency();
		DateFormat sdf = DateFormat.getDateInstance(DateFormat.MEDIUM, java.util.Locale.getDefault());
		try {
			BigDecimal factor1 = this.findDirectFactor(fromCurrency, sysCurrency, financialSource, avgMethod, date, strict);
			BigDecimal factor2 = this.findDirectFactor(sysCurrency, toCurrency, financialSource, avgMethod, date, strict);
			return factor1.multiply(factor2, MathContext.DECIMAL64);
		} catch (ExchangeRateValueNotFoundException e) {
			LOG.warn("Exchange rate value not found.", e);
			throw new ExchangeRateValueNotFoundException(String.format(BusinessErrorCode.NO_EXCHANGE_RATE_ON_DATE.getErrMsg(), fromCurrency.getCode(),
					toCurrency.getCode(), financialSource.getCode(), avgMethod.getCode(), sdf.format(date)));
		} catch (ExchangeRateNotFoundException e) {
			LOG.warn(WARNING_MSG, e);
			throw new ExchangeRateValueNotFoundException(String.format(BusinessErrorCode.NO_EXCHANGE_RATE_ON_DATE.getErrMsg(), fromCurrency.getCode(),
					toCurrency.getCode(), financialSource.getCode(), avgMethod.getCode(), sdf.format(date)));
		}
	}

	private Currencies getSystemCurrency() throws BusinessException {
		ICurrenciesService service = this.getCurrencyService();
		return service.getSystemCurrency();
	}

	private BigDecimal findDirectFactor(Currencies fromCurrency, Currencies toCurrency, FinancialSources financialSource, AverageMethod avgMethod,
			Date date, boolean strict) throws BusinessException {
		Boolean inverted = false;
		if (fromCurrency.getCode().equals(toCurrency.getCode())) {
			return BigDecimal.ONE;
		}
		ExchangeRate exchangeRate = this.getExchangeRate(fromCurrency, toCurrency, financialSource, avgMethod);
		if (exchangeRate.getCurrency1().getCode().equalsIgnoreCase(toCurrency.getCode())) {
			inverted = true;
		}
		ExchangeRateValue exchangeRateValue = this.getExchangeRateValue(exchangeRate, date, strict);
		if (!inverted) {
			return this.getFactor(exchangeRateValue.getRate(), this.getByOrdinal(exchangeRateValue.getPostTypeInd()));
		} else {
			return BigDecimal.ONE.divide(this.getFactor(exchangeRateValue.getRate(), this.getByOrdinal(exchangeRateValue.getPostTypeInd())),
					MathContext.DECIMAL64);
		}
	}

	private PostTypeInd getByOrdinal(int ordinal) {
		for (PostTypeInd ind : PostTypeInd.values()) {
			if (ind.ordinal() == ordinal) {
				return ind;
			}
		}
		return PostTypeInd._EMPTY_;
	}

	private ExchangeRateValue getExchangeRateValue(ExchangeRate exchangeRate, Date date, boolean strict) throws BusinessException {
		IExchangeRateValueService service = this.getExchangeRateValueService();
		return service.getExchangeRateValue(exchangeRate, date, strict);
	}

	/**
	 * @param fromCurrency
	 * @param toCurrency
	 * @param financialSource
	 * @param avgMethod
	 * @return
	 * @throws BusinessException
	 */
	public ExchangeRate getExchangeRate(Currencies fromCurrency, Currencies toCurrency, FinancialSources financialSource, AverageMethod avgMethod)
			throws BusinessException {
		IExchangeRateService service = this.getExchangeRateService();
		ExchangeRate exchangeRate;
		try {
			exchangeRate = service.getExchangeRate(fromCurrency, toCurrency, financialSource, avgMethod);
		} catch (ExchangeRateNotFoundException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
			LOG.warn("Exchange rate not found  from %s to %s ", fromCurrency.getCode(), toCurrency.getCode());
			exchangeRate = service.getExchangeRate(toCurrency, fromCurrency, financialSource, avgMethod);
		}
		return exchangeRate;
	}

	private BigDecimal getFactor(BigDecimal rate, PostTypeInd type) throws ExchangeFactorException {
		try {
			BigDecimal factor;
			switch (type) {
			case _AMOUNT_:
				factor = BigDecimal.ONE.divide(rate, MathContext.DECIMAL64);
				break;
			case _PRICE_:
			default:
				factor = rate;
				break;
			}
			return factor;
		} catch (ArithmeticException e) {
			throw new ExchangeFactorException(e, e.getMessage());
		}
	}

	private FinancialSources getStandardFinancialSource() throws BusinessException {
		IFinancialSourcesService service = this.getFinancialSourceService();
		return service.getStandardFinancialSource();
	}

	private AverageMethod getDefaultAverageMethod() throws BusinessException {
		IAverageMethodService service = this.getAverageMethodService();
		return service.getDefaultMethod();
	}

}
