/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.timeserie;

import atraxo.fmbas.domain.impl.timeserie.RawTimeSerieItem;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link RawTimeSerieItem} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class RawTimeSerieItem_Service
		extends
			AbstractEntityService<RawTimeSerieItem> {

	/**
	 * Public constructor for RawTimeSerieItem_Service
	 */
	public RawTimeSerieItem_Service() {
		super();
	}

	/**
	 * Public constructor for RawTimeSerieItem_Service
	 * 
	 * @param em
	 */
	public RawTimeSerieItem_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<RawTimeSerieItem> getEntityClass() {
		return RawTimeSerieItem.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return RawTimeSerieItem
	 */
	public RawTimeSerieItem findByTsi_business(TimeSerie tserie, Date itemDate) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(RawTimeSerieItem.NQ_FIND_BY_TSI_BUSINESS,
							RawTimeSerieItem.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("tserie", tserie)
					.setParameter("itemDate", itemDate).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"RawTimeSerieItem", "tserie, itemDate"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"RawTimeSerieItem", "tserie, itemDate"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return RawTimeSerieItem
	 */
	public RawTimeSerieItem findByTsi_business(Long tserieId, Date itemDate) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							RawTimeSerieItem.NQ_FIND_BY_TSI_BUSINESS_PRIMITIVE,
							RawTimeSerieItem.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("tserieId", tserieId)
					.setParameter("itemDate", itemDate).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"RawTimeSerieItem", "tserieId, itemDate"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"RawTimeSerieItem", "tserieId, itemDate"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return RawTimeSerieItem
	 */
	public RawTimeSerieItem findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(RawTimeSerieItem.NQ_FIND_BY_BUSINESS,
							RawTimeSerieItem.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"RawTimeSerieItem", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"RawTimeSerieItem", "id"), nure);
		}
	}

	/**
	 * Find by reference: tserie
	 *
	 * @param tserie
	 * @return List<RawTimeSerieItem>
	 */
	public List<RawTimeSerieItem> findByTserie(TimeSerie tserie) {
		return this.findByTserieId(tserie.getId());
	}
	/**
	 * Find by ID of reference: tserie.id
	 *
	 * @param tserieId
	 * @return List<RawTimeSerieItem>
	 */
	public List<RawTimeSerieItem> findByTserieId(Integer tserieId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from RawTimeSerieItem e where e.clientId = :clientId and e.tserie.id = :tserieId",
						RawTimeSerieItem.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("tserieId", tserieId).getResultList();
	}
}
