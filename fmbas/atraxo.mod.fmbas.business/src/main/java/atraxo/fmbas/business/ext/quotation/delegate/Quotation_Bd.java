package atraxo.fmbas.business.ext.quotation.delegate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.quotation.IQuotationService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieItemService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.business.ext.calculator.Calculator;
import atraxo.fmbas.business.ext.calculator.Method;
import atraxo.fmbas.business.ext.calculator.model.Result;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.quotation.QuotationValue;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieAverage;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

/**
 * @author zspeter
 */
public class Quotation_Bd extends AbstractBusinessDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(Quotation_Bd.class);

	private final class QuotationValuesComparator implements Comparator<QuotationValue> {
		@Override
		public int compare(QuotationValue o1, QuotationValue o2) {
			return o1.getValidFromDate().compareTo(o2.getValidFromDate());
		}
	}

	/**
	 * Update time series quotations.
	 *
	 * @param timeSerie
	 * @throws BusinessException
	 */
	public void updateAverages(TimeSerie timeSerie) throws BusinessException {
		IQuotationService srv = (IQuotationService) this.findEntityService(Quotation.class);
		List<Quotation> currentQuotations = srv.findByTimeseries(timeSerie);
		List<Quotation> quotations = new ArrayList<>();
		for (TimeSerieAverage avg : timeSerie.getTimeserieAvg()) {
			if (avg.getActive()) {
				quotations.add(this.retriveQuotation(timeSerie.getId(), avg.getAveragingMethod()));
			}
		}
		Iterator<Quotation> iter = currentQuotations.iterator();
		while (iter.hasNext()) {
			Quotation quot = iter.next();
			if (quotations.contains(quot)) {
				iter.remove();
			}
		}
		srv.delete(currentQuotations);
		for (Quotation quot : quotations) {
			List<QuotationValue> newValues = this.calculateAvereages(quot.getAvgMethodIndicator(), new ArrayList<>(timeSerie.getTimeserieItems()),
					quot);
			this.merge(quot, newValues);
		}
		srv.update(quotations);
	}

	private void merge(Quotation quotation, List<QuotationValue> newList) {
		List<QuotationValue> oldList = new ArrayList<>();
		if (quotation.getQuotationValues() != null) {
			oldList.addAll(quotation.getQuotationValues());
		}
		QuotationValuesComparator comp = new QuotationValuesComparator();
		Collections.sort(oldList, comp);
		Collections.sort(newList, comp);
		Iterator<QuotationValue> iter = oldList.iterator();
		while (iter.hasNext()) {
			QuotationValue v = iter.next();
			if (!newList.contains(v)) {
				iter.remove();
			} else {
				QuotationValue newValue = newList.get(newList.indexOf(v));
				if (!newValue.getRate().equals(v.getRate())) {
					v.setRate(newValue.getRate());
				}
			}
		}
		quotation.setQuotationValues(oldList);
		for (QuotationValue e : newList) {
			if (!oldList.contains(e)) {
				quotation.addToQuotationValues(e);
			}
		}
	}

	/**
	 * Get quotation from database, if not exists creates one.
	 *
	 * @param timeSerieNo
	 * @param averageMethod
	 * @return
	 * @throws BusinessException
	 */
	private Quotation retriveQuotation(Integer timeSerieNo, AverageMethod averageMethod) throws BusinessException {
		IQuotationService service = (IQuotationService) this.findEntityService(Quotation.class);
		try {
			return service.getByTimeseriesAndAvgMethod(timeSerieNo, averageMethod);
		} catch (EntityNotFoundException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
			return this.createQuotation(timeSerieNo, averageMethod, service);
		}
	}

	/**
	 * Builds a quotation.
	 *
	 * @param timeSerieNo
	 * @param averageMethod
	 * @param quotationService
	 * @return
	 * @throws BusinessException
	 */
	private Quotation createQuotation(Integer timeSerieNo, AverageMethod averageMethod, IQuotationService quotationService) throws BusinessException {

		ITimeSerieService timeSerieService = (ITimeSerieService) this.findEntityService(TimeSerie.class);
		TimeSerie timeSerie = timeSerieService.findById(timeSerieNo);

		Quotation quotation = quotationService.create();
		quotation.setName(timeSerie.getSerieName() + "/" + averageMethod.getCode());
		quotation.setAvgMethodIndicator(averageMethod);
		quotation.setCurrency(timeSerie.getCurrency1Id());
		quotation.setTimeseries(timeSerie);
		quotation.setUnit(timeSerie.getUnitId());
		quotation.setValueType(timeSerie.getValueType());
		quotationService.insert(quotation);
		return quotation;
	}

	/**
	 * Calculate averages.
	 *
	 * @param averageMethod
	 * @param list
	 * @return
	 * @throws BusinessException
	 */
	private List<QuotationValue> calculateAvereages(AverageMethod averageMethod, List<TimeSerieItem> list, Quotation quotation)
			throws BusinessException {
		List<QuotationValue> quotationValues = new ArrayList<>();
		Method averageCalculationMethod = Method.getByName(averageMethod.getCode());
		Calculator calculator = averageCalculationMethod.getCalculator();
		ITimeSerieItemService service = (ITimeSerieItemService) this.findEntityService(TimeSerieItem.class);
		List<TimeSerieItem> itemsForCalculation = calculator.getItemsForCalculation(list, service);

		List<Result> results = calculator.calculate(itemsForCalculation, list);
		for (Result result : results) {
			quotationValues.add(result.getResultAsQuotationValue(quotation));
		}

		return quotationValues;
	}

}
