/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.suppliers.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.creditLines.ICreditLinesService;
import atraxo.fmbas.business.api.suppliers.ISuppliersService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.PrimaryContactCannotDetermineException;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link Suppliers} domain entity.
 */
public class Suppliers_Service extends atraxo.fmbas.business.impl.suppliers.Suppliers_Service implements ISuppliersService {

	@Autowired
	private ICreditLinesService creditLinesService;

	@Override
	protected void preUpdate(Suppliers e) throws BusinessException {
		super.preUpdate(e);

		if (!(e.getFuelSupplier() || e.getFixedBaseOperator() || e.getIplAgent())) {
			throw new BusinessException(BusinessErrorCode.SUPPLIER_COMPANY_ROLE, BusinessErrorCode.SUPPLIER_COMPANY_ROLE.getErrMsg());
		}

	}

	@Override
	protected void preInsert(Suppliers e) throws BusinessException {
		super.preInsert(e);

		if (!(e.getFuelSupplier() || e.getFixedBaseOperator() || e.getIplAgent())) {
			throw new BusinessException(BusinessErrorCode.SUPPLIER_COMPANY_ROLE, BusinessErrorCode.SUPPLIER_COMPANY_ROLE.getErrMsg());
		}
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		for (Object id : ids) {
			List<CreditLines> creditLines = this.creditLinesService.findBySupplierId((Integer) id);
			if (!creditLines.isEmpty()) {
				this.creditLinesService.delete(creditLines);
			}
		}

	}

	@Override
	public Contacts getPrimaryContact(Suppliers supplier) throws BusinessException {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("isPrimary", true);
			params.put("objectType", Suppliers.class.getSimpleName());
			params.put("objectId", supplier.getId());
			return this.findEntityByAttributes(Contacts.class, params);
		} catch (ApplicationException e) {
			throw new PrimaryContactCannotDetermineException(Suppliers.class, e);
		}
	}
}
