/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.uom;

import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Unit} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Unit_Service extends AbstractEntityService<Unit> {

	/**
	 * Public constructor for Unit_Service
	 */
	public Unit_Service() {
		super();
	}

	/**
	 * Public constructor for Unit_Service
	 * 
	 * @param em
	 */
	public Unit_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Unit> getEntityClass() {
		return Unit.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Unit
	 */
	public Unit findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Unit.NQ_FIND_BY_CODE, Unit.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Unit", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Unit", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Unit
	 */
	public Unit findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Unit.NQ_FIND_BY_BUSINESS, Unit.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Unit", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Unit", "id"), nure);
		}
	}

}
