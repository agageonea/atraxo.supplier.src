package atraxo.fmbas.business.ext.exchangerate.job.importBloomberg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.job.IActionService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.timeserie.IRawTimeSerieItemService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exchangerate.job.importBloomberg.model.BloombergExchangeRateJobParamName;
import atraxo.fmbas.business.ext.exchangerate.job.importBloomberg.model.ExchangeRateDateDTO;
import atraxo.fmbas.business.ext.exchangerate.job.importBloomberg.model.ExchangeRateResponseDTO;
import atraxo.fmbas.domain.impl.fmbas_type.DataProvider;
import atraxo.fmbas.domain.impl.fmbas_type.PostTypeInd;
import atraxo.fmbas.domain.impl.fmbas_type.SerieFreqInd;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesApprovalStatus;
import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.ActionParameter;
import atraxo.fmbas.domain.impl.timeserie.RawTimeSerieItem;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import seava.j4e.domain.batchjob.JobParameter;

/**
 * Spring Batch Tasklet for importing Bloomberg exchange rates;
 *
 * @author vhojda
 */
public class ImportExchangeRatesTasklet implements Tasklet {

	private static final Logger LOGGER = LoggerFactory.getLogger(ImportExchangeRatesTasklet.class);

	private static final Object OVERWRITE_TRUE_PARAM_VALUE = "Yes";

	private static final String TAB = "\t";
	public static final String CRLF = "\r\n";

	public static final String KEY_LOG = "log_exchange_rates_bloomberg";
	public static final String KEY_NOT_OK = "not_ok_exchange_rates_bloomberg";

	@Autowired
	private IActionService actionService;

	@Autowired
	private ITimeSerieService timeSerieService;

	@Autowired
	private ISystemParameterService systemParameterService;

	@Autowired
	private IRawTimeSerieItemService rawTimeSerieItemService;

	private StringBuilder logBuilder;

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.core.step.tasklet.Tasklet#execute(org.springframework.batch.core.StepContribution,
	 * org.springframework.batch.core.scope.context.ChunkContext)
	 */
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}
		this.logBuilder = new StringBuilder();
		boolean executedWithSuccess = true;

		Map<String, Object> paramsTimeSeries = new HashMap<>();
		paramsTimeSeries.put("serieFreqInd", SerieFreqInd._DAILY_);
		paramsTimeSeries.put("dataProvider", DataProvider._IMPORT_BLOOMBERG_);

		List<TimeSerie> timeSeries = this.timeSerieService.findEntitiesByAttributes(paramsTimeSeries);

		if (timeSeries.isEmpty()) {
			this.logBuilder = new StringBuilder(BusinessErrorCode.IMPORT_ER_NOT_DEFINED.getErrMsg());
		} else {
			this.logBuilder.append(CRLF);

			Map<BloombergExchangeRateJobParamName, String> paramMap = this.getActionParameters(chunkContext);

			Set<TimeSerie> modifiedTimeSeriesSet = new HashSet<>();

			for (TimeSerie ts : timeSeries) {
				List<RawTimeSerieItem> insertedRawTimeSerieItems = new ArrayList<RawTimeSerieItem>();
				List<RawTimeSerieItem> updatedRawTimeSerieItems = new ArrayList<RawTimeSerieItem>();
				List<RawTimeSerieItem> skippedRawTimeSerieItems = new ArrayList<RawTimeSerieItem>();

				this.logBuilder.append(ts.getSerieName()).append(CRLF);

				BloombergExchangeRateDataReader readerREST = new BloombergExchangeRateDataReader(
						paramMap.get(BloombergExchangeRateJobParamName.SOURCE_URL), paramMap.get(BloombergExchangeRateJobParamName.PRICING_FIELD),
						paramMap.get(BloombergExchangeRateJobParamName.USERNAME), paramMap.get(BloombergExchangeRateJobParamName.PASSWORD),
						ts.getPostTypeInd().equals(PostTypeInd._PRICE_) ? ts.getCurrency1Id().getCode() : ts.getCurrency2Id().getCode(),
						ts.getPostTypeInd().equals(PostTypeInd._PRICE_) ? ts.getCurrency2Id().getCode() : ts.getCurrency1Id().getCode());

				// check if the READ operation occured OK for any reason
				ExchangeRateResponseDTO exchangeRateResponse = null;
				try {
					exchangeRateResponse = readerREST.read();
				} catch (Exception e) {
					LOGGER.error("ERROR:could not read exchange rates from Bloomberg !", e);
					this.logBuilder = new StringBuilder(BusinessErrorCode.IMPORT_ER_EXCEPTION.getErrMsg()).append(ExceptionUtils.getStackTrace(e));
					executedWithSuccess = false;
					break;
				}

				if (exchangeRateResponse != null) {

					if (exchangeRateResponse.getCommunicationExceptionMessage() != null) {// check for communication errors
						this.logBuilder.append(TAB).append(BusinessErrorCode.IMPORT_ER_COMMUNICATION.getErrMsg())
								.append(exchangeRateResponse.getCommunicationExceptionMessage()).append(CRLF);
						executedWithSuccess = false;
					} else if (exchangeRateResponse.getSecurityExceptionMessage() != null) {// check for exceptions in the Bloomberg response
						this.logBuilder.append(TAB).append(BusinessErrorCode.IMPORT_ER_API.getErrMsg())
								.append(exchangeRateResponse.getSecurityExceptionMessage()).append(CRLF);
						executedWithSuccess = false;
					} else {
						// iterate throught the received rates and update the exchange rates
						for (ExchangeRateDateDTO exchangeRateDTO : exchangeRateResponse.getRates()) {
							Map<String, Object> paramsRawTimeSerieItems = new HashMap<String, Object>();
							paramsRawTimeSerieItems.put("tserie", ts);
							paramsRawTimeSerieItems.put("active", Boolean.TRUE);
							paramsRawTimeSerieItems.put("itemDate", exchangeRateDTO.getDate());
							List<RawTimeSerieItem> items = this.rawTimeSerieItemService.findEntitiesByAttributes(paramsRawTimeSerieItems);

							if (items.isEmpty()) {
								// no time serie items exist, so create items
								RawTimeSerieItem newItem = this.createNewRawTimeSerieItem(ts, exchangeRateDTO);
								insertedRawTimeSerieItems.add(newItem);
							} else {
								// time serie items exist, so update them
								for (RawTimeSerieItem existingItem : items) {
									if (OVERWRITE_TRUE_PARAM_VALUE.equals(paramMap.get(BloombergExchangeRateJobParamName.OVERWRITE))) {
										existingItem.setItemValue(exchangeRateDTO.getRate());
										existingItem.setCalculated(false);
										updatedRawTimeSerieItems.add(existingItem);
									} else {
										skippedRawTimeSerieItems.add(existingItem);
									}
								}
							}
						}
						// build the log
						this.logBuilder.append(TAB).append(BusinessErrorCode.IMPORT_ER_DAYS_PROCESSED.getErrMsg())
								.append(readerREST.getPricingField().getNrProcessedDays()).append(CRLF);
						this.logBuilder.append(TAB).append(BusinessErrorCode.IMPORT_ER_TOTAL_PROCESSED.getErrMsg())
								.append(insertedRawTimeSerieItems.size() + skippedRawTimeSerieItems.size() + updatedRawTimeSerieItems.size())
								.append(CRLF);
						this.logBuilder.append(TAB).append(BusinessErrorCode.IMPORT_ER_IMPORTED.getErrMsg()).append(insertedRawTimeSerieItems.size())
								.append(CRLF);
						this.logBuilder.append(TAB).append(BusinessErrorCode.IMPORT_ER_SKIPPED.getErrMsg()).append(skippedRawTimeSerieItems.size())
								.append(CRLF);
						this.logBuilder.append(TAB).append(BusinessErrorCode.IMPORT_ER_UPDATED.getErrMsg()).append(updatedRawTimeSerieItems.size())
								.append(CRLF);

						// insert new items (if the case)
						if (!insertedRawTimeSerieItems.isEmpty()) {
							this.rawTimeSerieItemService.insert(insertedRawTimeSerieItems);
						}
						// update existing items (if the case)
						if (!updatedRawTimeSerieItems.isEmpty()) {
							this.rawTimeSerieItemService.update(updatedRawTimeSerieItems);
						}

						// add the modified TimeSerie to the List of modified time series
						if (this.systemParameterService.getExchangeRateApprovalWorkflow()) {
							insertedRawTimeSerieItems.forEach(c -> modifiedTimeSeriesSet.add(c.getTserie()));
							updatedRawTimeSerieItems.forEach(c -> modifiedTimeSeriesSet.add(c.getTserie()));
						}
					}
				}

			}

			// update TimeSerie approval status (if the case)
			if (this.systemParameterService.getExchangeRateApprovalWorkflow()) {
				modifiedTimeSeriesSet.forEach(c -> c.setApprovalStatus(TimeSeriesApprovalStatus._APPROVED_));
				List<TimeSerie> list = new ArrayList<>(modifiedTimeSeriesSet);
				this.timeSerieService.update(list);
			}

		}

		// also, put the so far built LOG
		chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put(KEY_LOG, this.logBuilder.toString());
		// signal that something went wrong (if the case) so the status is updated accordingly
		if (!executedWithSuccess) {
			chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put(KEY_NOT_OK, "");
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
		return RepeatStatus.FINISHED;
	}

	/**
	 * @param ts
	 * @param exchangeRateDTO
	 * @return
	 */
	private RawTimeSerieItem createNewRawTimeSerieItem(TimeSerie ts, ExchangeRateDateDTO exchangeRateDTO) {
		RawTimeSerieItem exchangeRate = new RawTimeSerieItem();
		exchangeRate.setTserie(ts);
		exchangeRate.setItemDate(exchangeRateDTO.getDate());
		exchangeRate.setItemValue(exchangeRateDTO.getRate());
		exchangeRate.setCalculated(false);
		exchangeRate.setActive(true);
		exchangeRate.setTransfered(false);
		exchangeRate.setTransferedItemUpdate(false);
		exchangeRate.setUpdated(false);
		return exchangeRate;
	}

	/**
	 * Using configured service read parameters for current action.
	 *
	 * @param chunkContext
	 * @return
	 */
	private Map<BloombergExchangeRateJobParamName, String> getActionParameters(ChunkContext chunkContext) {
		Long actionId = (Long) chunkContext.getStepContext().getJobParameters().get(JobParameter.ACTION.name());
		Action action = this.actionService.findById(actionId.intValue());
		Map<BloombergExchangeRateJobParamName, String> paramMap = new HashMap<BloombergExchangeRateJobParamName, String>();
		for (ActionParameter param : action.getActionParameters()) {
			paramMap.put(BloombergExchangeRateJobParamName.getByValue(param.getName()),
					!StringUtils.isEmpty(param.getValue()) ? param.getValue() : param.getDefaultValue());
		}
		return paramMap;
	}

}
