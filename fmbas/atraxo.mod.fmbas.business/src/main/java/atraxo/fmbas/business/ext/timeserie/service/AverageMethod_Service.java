/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.timeserie.service;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import atraxo.fmbas.business.api.timeserie.IAverageMethodService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.ext.types.FmbasSysParam;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link AverageMethod} domain entity.
 */
public class AverageMethod_Service extends atraxo.fmbas.business.impl.timeserie.AverageMethod_Service implements IAverageMethodService {

	/**
	 * Return default average method if it can be uniquely determined, otherwise throws exception.
	 *
	 * @return
	 */
	@Override
	public AverageMethod getDefaultMethod() throws BusinessException {
		String sql = "select e from " + AverageMethod.class.getSimpleName() + " e, " + SystemParameter.class.getSimpleName()
				+ " sys where e.clientId =:clientId and sys.clientId =:clientId and e.code = sys.value and sys.code = :code ";
		try {
			return this.getEntityManager().createQuery(sql, AverageMethod.class).setParameter("code", FmbasSysParam.SYSAVGMTHD.name())
					.setParameter("clientId", Session.user.get().getClient().getId()).getSingleResult();
		} catch (NoResultException | NonUniqueResultException nre) {
			throw new BusinessException(BusinessErrorCode.NO_DEFAULT_AVERAGE_METHOD, BusinessErrorCode.NO_DEFAULT_AVERAGE_METHOD.getErrMsg(), nre);
		}
	}
}