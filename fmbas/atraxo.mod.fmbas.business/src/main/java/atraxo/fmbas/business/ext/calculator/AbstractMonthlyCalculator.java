package atraxo.fmbas.business.ext.calculator;

import java.util.Calendar;
import java.util.Date;

/**
 * Abstract class for monthly average calculations.
 *
 * @author zspeter
 *
 */
public abstract class AbstractMonthlyCalculator extends AbstractCalculator implements Calculator {

	@Override
	protected Date calculateAveragePeriodEndDate(Date startDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		Integer lastDayOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		cal.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);
		return cal.getTime();
	}

	@Override
	protected Date calculateAveragePeriodStartDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	/**
	 * @param cal
	 * @param monthDay
	 */
	protected void changeCalToSpecifiedMonthDay(Calendar cal, int monthDay) {
		if (cal.get(Calendar.DAY_OF_MONTH) != monthDay) {
			cal.set(Calendar.DAY_OF_MONTH, monthDay);
		}
	}

	@Override
	protected Date getEndDate(Date itemDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(itemDate);
		this.changeCalToSpecifiedMonthDay(cal, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		return cal.getTime();
	}

}