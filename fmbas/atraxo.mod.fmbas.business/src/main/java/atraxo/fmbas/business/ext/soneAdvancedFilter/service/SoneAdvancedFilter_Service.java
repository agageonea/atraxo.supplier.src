/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.soneAdvancedFilter.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import atraxo.fmbas.business.api.soneAdvancedFilter.ISoneAdvancedFilterService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.soneAdvancedFilter.SoneAdvancedFilter;

/**
 * Business extensions specific for {@link SoneAdvancedFilter} domain entity.
 */
public class SoneAdvancedFilter_Service extends atraxo.fmbas.business.impl.soneAdvancedFilter.SoneAdvancedFilter_Service implements
		ISoneAdvancedFilterService {

	@Override
	protected void preInsert(SoneAdvancedFilter e) throws BusinessException {
		super.preInsert(e);
		this.checkDuplicate(e, false);
		this.updateDefaultFilter(e);
	}

	@Override
	protected void preUpdate(SoneAdvancedFilter e) throws BusinessException {
		super.preUpdate(e);
		this.checkDuplicate(e, true);
		this.updateDefaultFilter(e);
	}

	private void checkDuplicate(SoneAdvancedFilter e, Boolean isUpdate) throws BusinessException {
		if (e.getName() == null) {
			return;
		}

		Map<String, Object> params = new HashMap<>();
		params.put("name", e.getName());
		params.put("cmp", e.getCmp());

		List<SoneAdvancedFilter> advancedFilterList = this.findEntitiesByAttributes(params);
		int count = advancedFilterList.size();
		Boolean duplicate = false;
		if (count > 0) {
			if (isUpdate && count == 1) {
				if (!e.getId().equals(advancedFilterList.get(0).getId())) {
					duplicate = true;
				}
			} else {
				duplicate = true;
			}
		}
		if (duplicate) {
			throw new BusinessException(BusinessErrorCode.DUPLICATE_FILTER, BusinessErrorCode.DUPLICATE_FILTER.getErrMsg());
		}
	}

	private void updateDefaultFilter(SoneAdvancedFilter e) throws BusinessException {
		if (e.getDefaultFilter() == null || !e.getDefaultFilter()) {
			return;
		}

		StringBuilder sb = new StringBuilder();
		sb.append("select e from ").append(SoneAdvancedFilter.class.getSimpleName());
		sb.append(" e where e.clientId =:clientId and e.defaultFilter = :defaultFilter and e.cmp = :cmp and e.createdBy = :createdBy");

		TypedQuery<SoneAdvancedFilter> query = this.getEntityManager().createQuery(sb.toString(), SoneAdvancedFilter.class)
				.setParameter("clientId", Session.user.get().getClient().getId());
		query.setParameter("defaultFilter", e.getDefaultFilter());
		query.setParameter("cmp", e.getCmp());
		query.setParameter("createdBy", Session.user.get().getCode());

		List<SoneAdvancedFilter> advancedFilterList = query.getResultList();
		for (SoneAdvancedFilter soneAdvancedFilter : advancedFilterList) {
			if (!soneAdvancedFilter.getId().equals(e.getId())) {
				soneAdvancedFilter.setDefaultFilter(false);
				this.onUpdate(soneAdvancedFilter);
			}
		}
	}

	@Override
	public List<SoneAdvancedFilter> getDefaultFilters(String cmp) throws BusinessException {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT e FROM ").append(SoneAdvancedFilter.class.getSimpleName()).append(" e WHERE ");
		sb.append("e.clientId =:clientId and e.defaultFilter = 1 AND e.cmp LIKE '%").append(cmp).append("%' AND e.createdBy =:createdBy");

		@SuppressWarnings("unchecked")
		List<SoneAdvancedFilter> soneAdvancedFilter = this.getEntityManager().createQuery(sb.toString())
				.setParameter("clientId", Session.user.get().getClientId()).setParameter("createdBy", Session.user.get().getCode()).getResultList();

		return soneAdvancedFilter;
	}

}
