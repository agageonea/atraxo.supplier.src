package atraxo.fmbas.business.ext.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class StringUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(StringUtil.class);

	private StringUtil() {
		// private constructor because this class is an utility class and it doesn't need to be created objects out of it
	}

	public static String unorderedList(Object... args) {
		StringBuilder errorList = new StringBuilder();

		if (args.length > 0) {
			errorList.append("<ul>");
			for (Object arg : args) {
				errorList.append("<li>").append((String) arg).append("</li>");
			}
			errorList.append("</ul>");
		}

		return errorList.toString();
	}

	public static Map<String, String> splitToMap(String source, String entriesSeparator, String keyValueSeparator) {
		Map<String, String> map = new HashMap<>();
		String[] entries = source.split(entriesSeparator);
		for (String entry : entries) {
			if (!TextUtils.isEmpty(entry) && entry.contains(keyValueSeparator)) {
				String[] keyValue = entry.split(keyValueSeparator);
				map.put(keyValue[0], keyValue[1]);
			}
		}
		return map;
	}

	/**
	 * @param is
	 * @return
	 */
	public static String getStringFromInputStream(InputStream is) throws Exception {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			LOGGER.error("Could not read from InputStream when performing operation !,e");
			throw e;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					LOGGER.warn("Could not close the received InputStream !", e);
				}
			}
		}

		return sb.toString();

	}

}
