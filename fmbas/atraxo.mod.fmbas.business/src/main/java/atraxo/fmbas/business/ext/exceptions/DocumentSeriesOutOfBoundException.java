package atraxo.fmbas.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.IErrorCode;

public class DocumentSeriesOutOfBoundException extends BusinessException {

	public DocumentSeriesOutOfBoundException(IErrorCode errorCode, String errorDetails) {
		super(errorCode, errorDetails);
	}

}
