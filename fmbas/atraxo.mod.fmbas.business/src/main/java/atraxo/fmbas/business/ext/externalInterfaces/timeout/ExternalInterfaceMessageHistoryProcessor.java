package atraxo.fmbas.business.ext.externalInterfaces.timeout;

import org.springframework.batch.item.ItemProcessor;

import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;

/**
 * @author zspeter
 */
public class ExternalInterfaceMessageHistoryProcessor implements ItemProcessor<ExternalInterfaceMessageHistory, ExternalInterfaceMessageHistory> {

	@Override
	public ExternalInterfaceMessageHistory process(ExternalInterfaceMessageHistory e) throws Exception {
		e.setStatus(ExternalInterfaceMessageHistoryStatus._FAILED_TIMEOUT_);
		return e;
	}

}
