package atraxo.fmbas.business.ext.exchangerate.job.importBloomberg;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.net.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import atraxo.fmbas.business.ext.exchangerate.job.importBloomberg.model.BloombergExchangeRatePricingFieldEnum;
import atraxo.fmbas.business.ext.exchangerate.job.importBloomberg.model.ExchangeRateDateDTO;
import atraxo.fmbas.business.ext.exchangerate.job.importBloomberg.model.ExchangeRateResponseDTO;

/**
 * @author vhojda
 */
public class BloombergExchangeRateDataReader {

	private static final Logger LOGGER = LoggerFactory.getLogger(BloombergExchangeRateDataReader.class);

	private static final String SDF = "yyyy-MM-dd";

	private final RestTemplate restTemplate;

	private String sourceURL;
	private String username;
	private String password;
	private String currencyFrom;
	private String currencyTo;
	private BloombergExchangeRatePricingFieldEnum pricingField;

	/**
	 * @param sourceURL
	 * @param pricingField
	 * @param username
	 * @param password
	 * @param currencyFrom
	 * @param currencyTo
	 */
	public BloombergExchangeRateDataReader(String sourceURL, String pricingField, String username, String password, String currencyFrom,
			String currencyTo) {
		super();
		this.sourceURL = sourceURL;
		this.username = username;
		this.password = password;
		this.currencyFrom = currencyFrom;
		this.currencyTo = currencyTo;
		this.pricingField = BloombergExchangeRatePricingFieldEnum.getByValue(pricingField);

		this.restTemplate = new RestTemplate();
	}

	/**
	 * @param username
	 * @param password
	 * @return
	 */
	private HttpHeaders createHeaders(final String username, final String password) {
		HttpHeaders headers = new HttpHeaders();
		String auth = username + ":" + password;
		byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
		String authHeader = "Basic " + new String(encodedAuth);
		headers.add("Authorization", authHeader);
		return headers;
	}

	/**
	 * @return
	 * @throws Exception
	 */
	public ExchangeRateResponseDTO read() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START read()");
		}

		ExchangeRateResponseDTO response = new ExchangeRateResponseDTO();

		ResponseEntity<Map> responseMap = null;
		try {
			responseMap = this.restTemplate.exchange(this.buildApiUrl(), HttpMethod.GET,
					new HttpEntity(this.createHeaders(this.getUsername(), this.getPassword())), Map.class);
		} catch (Exception e) {
			LOGGER.warn("WARNING:could not call the REST Bloomberg API !", e);
			response.setCommunicationExceptionMessage(e.getLocalizedMessage());
		}

		if (responseMap != null) {

			Map responseMapBody = responseMap.getBody();
			Map dataMap = (Map) responseMapBody.get("data");
			Map exceptionsMap = (Map) responseMapBody.get("exceptions");

			String currencyKey = this.getCurrencyFrom() + this.getCurrencyTo() + ":CUR";

			if (exceptionsMap != null) {
				Map exceptionsValueMap = (Map) exceptionsMap.get(currencyKey);
				String securityMessage = (String) exceptionsValueMap.get("security");
				response.setSecurityExceptionMessage(securityMessage);
			} else {
				Map dataValueMap = (Map) dataMap.get(currencyKey);

				if (this.pricingField.equals(BloombergExchangeRatePricingFieldEnum.PRICING_FIELD_PREV_DAY)) {
					Double value = (Double) dataValueMap.get(this.pricingField.getBloombergValue());

					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.DAY_OF_MONTH, -1);

					ExchangeRateDateDTO dto = new ExchangeRateDateDTO();
					dto.setDate(cal.getTime());
					dto.setRate(new BigDecimal(value));
					response.addRate(dto);

				} else if (this.pricingField.equals(BloombergExchangeRatePricingFieldEnum.PRICING_FIELD_HISTORY)) {

					List values = (ArrayList) dataValueMap.get(this.pricingField.getBloombergValue());

					for (Object value : values) {
						Map valueMap = (Map) value;
						String dateValue = (String) valueMap.get("date");
						Double doubleValue = (Double) valueMap.get("value");

						ExchangeRateDateDTO dto = new ExchangeRateDateDTO();
						dto.setDate(this.getDateFormat().parse(dateValue));
						dto.setRate(new BigDecimal(doubleValue));
						response.addRate(dto);
					}
				}
			}
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END read()");
		}
		return response;
	}

	/**
	 * @return
	 */
	private String buildApiUrl() {
		StringBuilder url = new StringBuilder(this.getSourceURL());
		url.append("/");
		url.append(this.getPricingField().getUrlWord());
		url.append("?securities=");
		url.append(this.getCurrencyFrom());
		url.append(this.getCurrencyTo());
		url.append(":CUR&fields=");
		url.append(this.getPricingField().getBloombergValue());
		if (this.pricingField.equals(BloombergExchangeRatePricingFieldEnum.PRICING_FIELD_HISTORY)) {
			Calendar cal = Calendar.getInstance();
			url.append("&startDate=");
			cal.add(Calendar.DAY_OF_MONTH, -this.pricingField.getNrProcessedDays());
			url.append(this.getDateFormat().format(cal.getTime()));
			cal = Calendar.getInstance();
			url.append("&endDate=");
			url.append(this.getDateFormat().format(cal.getTime()));
		}
		return url.toString();
	}

	public String getSourceURL() {
		return this.sourceURL;
	}

	public void setSourceURL(String sourceURL) {
		this.sourceURL = sourceURL;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCurrencyFrom() {
		return this.currencyFrom;
	}

	public void setCurrencyFrom(String currencyFrom) {
		this.currencyFrom = currencyFrom;
	}

	public String getCurrencyTo() {
		return this.currencyTo;
	}

	public void setCurrencyTo(String currencyTo) {
		this.currencyTo = currencyTo;
	}

	public BloombergExchangeRatePricingFieldEnum getPricingField() {
		return this.pricingField;
	}

	public void setPricingField(BloombergExchangeRatePricingFieldEnum pricingField) {
		this.pricingField = pricingField;
	}

	private SimpleDateFormat getDateFormat() {
		return new SimpleDateFormat(SDF);
	}

}
