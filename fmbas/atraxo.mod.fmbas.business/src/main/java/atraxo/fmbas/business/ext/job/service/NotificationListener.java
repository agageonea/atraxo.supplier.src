package atraxo.fmbas.business.ext.job.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.util.CollectionUtils;

import atraxo.ad.domain.impl.ad.Category;
import atraxo.ad.domain.impl.ad.NotificationAction;
import atraxo.ad.domain.impl.ad.Priority;
import atraxo.fmbas.business.api.job.IJobChainService;
import atraxo.fmbas.business.api.notification.INotificationService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.fmbas_type.JobRunResult;
import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.JobChain;
import atraxo.fmbas.domain.impl.job.JobChainUser;
import atraxo.fmbas.domain.impl.notification.Notification;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.domain.batchjob.JobParameter;

/**
 * Job Listener responsible to send notification to subscribed users.
 *
 * @author zspeter
 */
public class NotificationListener implements JobListener {
	private static final Logger LOG = LoggerFactory.getLogger(NotificationListener.class);
	private static final String MSG_PART = "Job : ";
	private static final String LISTENER_NAME = "SONE_JOBCHAIN_NOTIFICATION_LISTENER";

	private INotificationService notifySrv;
	private IJobChainService jobChainSrv;
	private IUserSuppService userSrv;
	private JobChain jobChain;

	/**
	 * @param srv
	 * @param userSrv
	 * @param jobchain
	 */
	public NotificationListener(INotificationService srv, IUserSuppService userSrv, IJobChainService jobChainSrv, JobChain jobchain) {
		this.notifySrv = srv;
		this.userSrv = userSrv;
		this.jobChain = jobchain;
		this.jobChainSrv = jobChainSrv;
	}

	@Override
	public String getName() {
		return LISTENER_NAME;
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext context) {
		String jobName = context.getJobDetail().getKey().toString();
		LOG.info(MSG_PART + jobName + " is going to start...");

	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext context) {
		String jobName = context.getJobDetail().getKey().toString();
		LOG.info("jobExecutionVetoed");
		LOG.info(MSG_PART + jobName + " isVetoed.");

	}

	@Override
	public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
		Object actionId = context.getJobDetail().getJobDataMap().get(JobParameter.ACTION.name());
		Object jobChainId = context.getJobDetail().getJobDataMap().get(JobParameter.JOBCHAIN.name());
		if (jobChainId == null || !jobChainId.equals(this.jobChain.getId())) {
			return;
		}
		List<Action> list = new ArrayList<>(this.jobChain.getActions());
		for (Action a : list) {
			if (a.getId().equals(actionId) && a.getOrder() != list.size()) {
				return;
			}
		}

		try {
			JobRunResult status = JobRunResult._SUCCESS_;
			if (context.getResult() instanceof JobExecution) {
				JobExecution jobExecution = (JobExecution) context.getResult();
				status = (jobExecution.getAllFailureExceptions().isEmpty()
						&& !(jobExecution.getExitStatus().getExitCode().equals(JobRunResult._COMPLETED___FAILED_.getName())
								|| jobExecution.getExitStatus().getExitCode().equals(JobRunResult._FAILURE_.getName()))) ? JobRunResult._SUCCESS_
										: JobRunResult._FAILURE_;
			}

			this.sendNotification(status, jobChainId);
		} catch (BusinessException e) {
			LOG.error("Cannot send notification message after job execution: " + e.getMessage(), e);
		}

	}

	private void sendNotification(JobRunResult status, Object jobChainId) throws BusinessException {
		JobChain chain = this.jobChainSrv.findById(jobChainId);
		if (CollectionUtils.isEmpty(chain.getUsers())) {
			return;
		}
		Notification e = this.buildNotification(chain, chain.getLastRunResult());
		List<UserSupp> emailUsers = new ArrayList<>();
		for (JobChainUser user : chain.getUsers()) {
			if (!user.getEnabled()) {
				continue;
			}
			UserSupp u = this.userSrv.findById(user.getUser().getId());
			switch (user.getNotificationCondition()) {
			case _ALL_EVENTS_:
				this.notifySrv.addUsersToNotification(e, u, user.getNotificationType(), emailUsers);
				break;
			case _ON_SUCCESS_:
				if (status.equals(JobRunResult._SUCCESS_) || status.equals(JobRunResult._COMPLETED___SUCCESSFUL_)) {
					this.notifySrv.addUsersToNotification(e, u, user.getNotificationType(), emailUsers);
				}
				break;
			case _ON_FAILURE_:
				if (status.equals(JobRunResult._FAILURE_) || status.equals(JobRunResult._COMPLETED___FAILED_)) {
					this.notifySrv.addUsersToNotification(e, u, user.getNotificationType(), emailUsers);
				}
				break;
			default:
				break;
			}
		}
		this.notifySrv.sendAndSave(e, emailUsers, Collections.emptyMap());

	}

	private Notification buildNotification(JobChain jobchain, JobRunResult status) throws BusinessException {
		Notification e = new Notification();
		e.setEventDate(jobchain.getLastRunTime());
		e.setLifeTime(jobchain.getLastRunTime());
		e.setTitle(jobchain.getName());
		e.setAction(NotificationAction._NOACTION_);
		e.setLink("link");
		e.setEventDate(GregorianCalendar.getInstance().getTime());
		e.setLifeTime(GregorianCalendar.getInstance().getTime());
		if (JobRunResult._SUCCESS_.equals(status) || JobRunResult._COMPLETED___SUCCESSFUL_.equals(status)) {
			e.setCategory(Category._JOB_);
			e.setDescripion(String.format(BusinessErrorCode.INTERFACE_NOTIFICATION_SUCCESS.getErrMsg(), jobchain.getName()));
			e.setPriority(Priority._DEFAULT_);
		} else {
			e.setCategory(Category._ERROR_);
			e.setDescripion(String.format(BusinessErrorCode.INTERFACE_NOTIFICATION_FAILURE.getErrMsg(), jobchain.getName()));
			e.setPriority(Priority._MAX_);
		}
		e.setStatus(status.getName());
		e.setResult(status.getName());
		e.setUsers(new ArrayList<UserSupp>());
		return e;
	}

}
