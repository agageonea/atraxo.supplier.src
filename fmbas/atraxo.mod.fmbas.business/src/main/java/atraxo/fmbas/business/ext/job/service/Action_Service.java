/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.job.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.transaction.annotation.Transactional;

import seava.j4e.api.exceptions.BusinessException;
import atraxo.fmbas.business.api.job.IActionService;
import atraxo.fmbas.business.ext.job.delegate.ActionParameterBd;
import atraxo.fmbas.domain.impl.job.Action;

/**
 * Business extensions specific for {@link Action} domain entity.
 */
public class Action_Service extends atraxo.fmbas.business.impl.job.Action_Service implements IActionService {

	@SuppressWarnings("unchecked")
	@Override
	protected void preInsert(Action e) throws BusinessException {
		super.preInsert(e);
		e.setJobUID(UUID.randomUUID().toString().toUpperCase());
		List<Action> actions = this.getEntityManager()
				.createQuery("SELECT e FROM Action e WHERE e.clientId = :clientid and e.jobChain =:jobChain and e.order >= :order")
				.setParameter("clientid", e.getClientId()).setParameter("jobChain", e.getJobChain()).setParameter("order", e.getOrder())
				.getResultList();
		for (Action action : actions) {
			action.setOrder(action.getOrder().intValue() + 1);
		}
		this.update(actions);
		ActionParameterBd bd = this.getBusinessDelegate(ActionParameterBd.class);
		bd.addActionParameters(e);
	}

	@Override
	protected void preUpdate(Action e) throws BusinessException {
		super.preUpdate(e);
		ActionParameterBd bd = this.getBusinessDelegate(ActionParameterBd.class);
		bd.addActionParameters(e);
	}

	/**
	 * Move action <code>e</code>, number <code>step</code> forward or backward.
	 *
	 * @param e
	 * @param step
	 * @throws BusinessException
	 */
	@Override
	@Transactional
	public void move(Action e, int step) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("order", e.getOrder() + step);
		params.put("jobChain", e.getJobChain());
		Action otherAction = this.findEntityByAttributes(params);
		int order = otherAction.getOrder();
		otherAction.setOrder(e.getOrder());
		e.setOrder(order);
		this.update(Arrays.asList(e, otherAction));
	}

}
