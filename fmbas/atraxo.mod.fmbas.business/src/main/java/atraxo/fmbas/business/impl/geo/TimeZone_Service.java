/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.geo;

import atraxo.fmbas.business.api.geo.ITimeZoneService;
import atraxo.fmbas.domain.impl.geo.TimeZone;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link TimeZone} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class TimeZone_Service extends AbstractEntityService<TimeZone>
		implements
			ITimeZoneService {

	/**
	 * Public constructor for TimeZone_Service
	 */
	public TimeZone_Service() {
		super();
	}

	/**
	 * Public constructor for TimeZone_Service
	 * 
	 * @param em
	 */
	public TimeZone_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<TimeZone> getEntityClass() {
		return TimeZone.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return TimeZone
	 */
	public TimeZone findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(TimeZone.NQ_FIND_BY_NAME, TimeZone.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TimeZone", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TimeZone", "name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return TimeZone
	 */
	public TimeZone findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(TimeZone.NQ_FIND_BY_BUSINESS,
							TimeZone.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"TimeZone", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"TimeZone", "id"), nure);
		}
	}

}
