/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.templates;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.customer.CustomerNotification;
import atraxo.fmbas.domain.impl.externalInterfaces.InterfaceUser;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import atraxo.fmbas.domain.impl.templates.Template;
import atraxo.fmbas.domain.impl.user.UserSupp;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Template} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Template_Service extends AbstractEntityService<Template> {

	/**
	 * Public constructor for Template_Service
	 */
	public Template_Service() {
		super();
	}

	/**
	 * Public constructor for Template_Service
	 * 
	 * @param em
	 */
	public Template_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Template> getEntityClass() {
		return Template.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Template
	 */
	public Template findByKey(UserSupp user, Customer subsidiary, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Template.NQ_FIND_BY_KEY, Template.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("user", user)
					.setParameter("subsidiary", subsidiary)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Template", "user, subsidiary, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Template", "user, subsidiary, name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Template
	 */
	public Template findByKey(Long userId, Long subsidiaryId, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Template.NQ_FIND_BY_KEY_PRIMITIVE,
							Template.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("userId", userId)
					.setParameter("subsidiaryId", subsidiaryId)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Template", "userId, subsidiaryId, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Template", "userId, subsidiaryId, name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Template
	 */
	public Template findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Template.NQ_FIND_BY_BUSINESS,
							Template.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Template", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Template", "id"), nure);
		}
	}

	/**
	 * Find by reference: type
	 *
	 * @param type
	 * @return List<Template>
	 */
	public List<Template> findByType(NotificationEvent type) {
		return this.findByTypeId(type.getId());
	}
	/**
	 * Find by ID of reference: type.id
	 *
	 * @param typeId
	 * @return List<Template>
	 */
	public List<Template> findByTypeId(Integer typeId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Template e where e.clientId = :clientId and e.type.id = :typeId",
						Template.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("typeId", typeId).getResultList();
	}
	/**
	 * Find by reference: user
	 *
	 * @param user
	 * @return List<Template>
	 */
	public List<Template> findByUser(UserSupp user) {
		return this.findByUserId(user.getId());
	}
	/**
	 * Find by ID of reference: user.id
	 *
	 * @param userId
	 * @return List<Template>
	 */
	public List<Template> findByUserId(String userId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Template e where e.clientId = :clientId and e.user.id = :userId",
						Template.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("userId", userId).getResultList();
	}
	/**
	 * Find by reference: subsidiary
	 *
	 * @param subsidiary
	 * @return List<Template>
	 */
	public List<Template> findBySubsidiary(Customer subsidiary) {
		return this.findBySubsidiaryId(subsidiary.getId());
	}
	/**
	 * Find by ID of reference: subsidiary.id
	 *
	 * @param subsidiaryId
	 * @return List<Template>
	 */
	public List<Template> findBySubsidiaryId(Integer subsidiaryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Template e where e.clientId = :clientId and e.subsidiary.id = :subsidiaryId",
						Template.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("subsidiaryId", subsidiaryId).getResultList();
	}
	/**
	 * Find by reference: interfaceUser
	 *
	 * @param interfaceUser
	 * @return List<Template>
	 */
	public List<Template> findByInterfaceUser(InterfaceUser interfaceUser) {
		return this.findByInterfaceUserId(interfaceUser.getId());
	}
	/**
	 * Find by ID of reference: interfaceUser.id
	 *
	 * @param interfaceUserId
	 * @return List<Template>
	 */
	public List<Template> findByInterfaceUserId(Integer interfaceUserId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Template e, IN (e.interfaceUser) c where e.clientId = :clientId and c.id = :interfaceUserId",
						Template.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("interfaceUserId", interfaceUserId)
				.getResultList();
	}
	/**
	 * Find by reference: customerNotification
	 *
	 * @param customerNotification
	 * @return List<Template>
	 */
	public List<Template> findByCustomerNotification(
			CustomerNotification customerNotification) {
		return this.findByCustomerNotificationId(customerNotification.getId());
	}
	/**
	 * Find by ID of reference: customerNotification.id
	 *
	 * @param customerNotificationId
	 * @return List<Template>
	 */
	public List<Template> findByCustomerNotificationId(
			Integer customerNotificationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Template e, IN (e.customerNotification) c where e.clientId = :clientId and c.id = :customerNotificationId",
						Template.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerNotificationId", customerNotificationId)
				.getResultList();
	}
}
