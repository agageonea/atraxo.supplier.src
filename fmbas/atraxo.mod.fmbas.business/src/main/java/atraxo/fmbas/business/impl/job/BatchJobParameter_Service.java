/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.job;

import atraxo.fmbas.business.api.job.IBatchJobParameterService;
import atraxo.fmbas.domain.impl.job.BatchJob;
import atraxo.fmbas.domain.impl.job.BatchJobParameter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link BatchJobParameter} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class BatchJobParameter_Service
		extends
			AbstractEntityService<BatchJobParameter>
		implements
			IBatchJobParameterService {

	/**
	 * Public constructor for BatchJobParameter_Service
	 */
	public BatchJobParameter_Service() {
		super();
	}

	/**
	 * Public constructor for BatchJobParameter_Service
	 * 
	 * @param em
	 */
	public BatchJobParameter_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<BatchJobParameter> getEntityClass() {
		return BatchJobParameter.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return BatchJobParameter
	 */
	public BatchJobParameter findByName(BatchJob batchJob, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(BatchJobParameter.NQ_FIND_BY_NAME,
							BatchJobParameter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("batchJob", batchJob)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"BatchJobParameter", "batchJob, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"BatchJobParameter", "batchJob, name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return BatchJobParameter
	 */
	public BatchJobParameter findByName(Long batchJobId, String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							BatchJobParameter.NQ_FIND_BY_NAME_PRIMITIVE,
							BatchJobParameter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("batchJobId", batchJobId)
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"BatchJobParameter", "batchJobId, name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"BatchJobParameter", "batchJobId, name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return BatchJobParameter
	 */
	public BatchJobParameter findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(BatchJobParameter.NQ_FIND_BY_BUSINESS,
							BatchJobParameter.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"BatchJobParameter", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"BatchJobParameter", "id"), nure);
		}
	}

	/**
	 * Find by reference: batchJob
	 *
	 * @param batchJob
	 * @return List<BatchJobParameter>
	 */
	public List<BatchJobParameter> findByBatchJob(BatchJob batchJob) {
		return this.findByBatchJobId(batchJob.getId());
	}
	/**
	 * Find by ID of reference: batchJob.id
	 *
	 * @param batchJobId
	 * @return List<BatchJobParameter>
	 */
	public List<BatchJobParameter> findByBatchJobId(Integer batchJobId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from BatchJobParameter e where e.clientId = :clientId and e.batchJob.id = :batchJobId",
						BatchJobParameter.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("batchJobId", batchJobId).getResultList();
	}
}
