/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.timeserie.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.timeserie.IRawTimeSerieItemService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.timeserie.delegate.RawTimeSerieItem_Bd;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesApprovalStatus;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesStatus;
import atraxo.fmbas.domain.impl.timeserie.RawTimeSerieItem;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link RawTimeSerieItem} domain entity.
 */
public class RawTimeSerieItem_Service extends atraxo.fmbas.business.impl.timeserie.RawTimeSerieItem_Service implements IRawTimeSerieItemService {

	private static final String CONTEXT_KEY = "key";
	private static final String CALCULATED2 = "calculated";
	private static final String SERIE_ID = "serieId";
	private static final String CLIENT_ID = "clientId";
	private static final String ITEM_DATE = "itemDate";

	private static final Logger LOG = LoggerFactory.getLogger(RawTimeSerieItem_Service.class);

	@Override
	@Transactional
	public void insertForComposite(List<RawTimeSerieItem> list) throws BusinessException {
		try {
			this.onInsert(list);
		} catch (Exception e) {
			LOG.info("Composite time serie item insert:", e);
		}
	}

	@Override
	protected void preInsert(List<RawTimeSerieItem> list) throws BusinessException {
		super.preInsert(list);
		Collections.sort(list, new Comparator<RawTimeSerieItem>() {
			@Override
			public int compare(RawTimeSerieItem tsi1, RawTimeSerieItem tsi2) {
				return tsi1.getItemDate().compareTo(tsi2.getItemDate());
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.business.service.entity.AbstractEntityWriteService#postInsert (java.util.List)
	 */
	@Override
	protected void postInsert(List<RawTimeSerieItem> list) throws BusinessException {
		super.postInsert(list);
		if (list.isEmpty()) {
			return;
		}

		Map<Integer, List<RawTimeSerieItem>> groupedItems = this.buildGroupedItems(list);
		for (Entry<Integer, List<RawTimeSerieItem>> tserieEntry : groupedItems.entrySet()) {
			List<RawTimeSerieItem> rawItems = tserieEntry.getValue();
			Map<Date, RawTimeSerieItem> newItemsMap = this.buildFullMap(rawItems);
			TimeSerie tserie = rawItems.get(0).getTserie();

			Map<Date, RawTimeSerieItem> existingItems = this.buildFullMap(new ArrayList<RawTimeSerieItem>(tserie.getRawTimeserieItems()));
			newItemsMap = this.merge(newItemsMap, existingItems);
			List<RawTimeSerieItem> newItemList = new ArrayList<>(newItemsMap.values());
			Collections.sort(newItemList, new Comparator<RawTimeSerieItem>() {
				@Override
				public int compare(RawTimeSerieItem tsi1, RawTimeSerieItem tsi2) {
					return tsi1.getItemDate().compareTo(tsi2.getItemDate());
				}
			});
			RawTimeSerieItem lastItem = newItemList.get(newItemList.size() - 1);
			RawTimeSerieItem_Bd bd = this.getBusinessDelegate(RawTimeSerieItem_Bd.class);
			newItemList.addAll(bd.processFriday(lastItem, existingItems));
			lastItem = newItemList.get(newItemList.size() - 1);
			if (tserie.getFirstUsage() == null || tserie.getFirstUsage().after(newItemList.get(0).getItemDate())) {
				tserie.setFirstUsage(newItemList.get(0).getItemDate());
				tserie.setApprovalStatus(TimeSeriesApprovalStatus._NEW_);
				tserie.setStatus(TimeSeriesStatus._WORK_);
			}
			if (tserie.getLastUsage() == null || tserie.getLastUsage().before(lastItem.getItemDate())) {
				tserie.setLastUsage(lastItem.getItemDate());
				tserie.setApprovalStatus(TimeSeriesApprovalStatus._NEW_);
				tserie.setStatus(TimeSeriesStatus._WORK_);
			}
			this.onInsert(newItemList);

			ITimeSerieService timeSerieService = (ITimeSerieService) this.findEntityService(TimeSerie.class);
			timeSerieService.updateStatus(tserie, tserie.getStatus());
		}
	}

	private Map<Integer, List<RawTimeSerieItem>> buildGroupedItems(List<RawTimeSerieItem> list) {
		Map<Integer, List<RawTimeSerieItem>> groupedItems = new HashMap<>();
		for (RawTimeSerieItem item : list) {
			if (groupedItems.containsKey(item.getTserie().getId())) {
				groupedItems.get(item.getTserie().getId()).add(item);
			} else {
				List<RawTimeSerieItem> l = new ArrayList<>();
				l.add(item);
				groupedItems.put(item.getTserie().getId(), l);
			}
		}
		return groupedItems;
	}

	private Map<Date, RawTimeSerieItem> buildFullMap(List<RawTimeSerieItem> list) throws BusinessException {

		Collections.sort(list, new Comparator<RawTimeSerieItem>() {
			@Override
			public int compare(RawTimeSerieItem tsi1, RawTimeSerieItem tsi2) {
				return tsi1.getItemDate().compareTo(tsi2.getItemDate());
			}
		});
		RawTimeSerieItem_Bd bd = this.getBusinessDelegate(RawTimeSerieItem_Bd.class);
		Map<Date, RawTimeSerieItem> filledMap = new HashMap<>();
		RawTimeSerieItem lastProccessed = null;
		for (RawTimeSerieItem tsi : list) {
			Date itemDate = DateUtils.truncate(tsi.getItemDate(), Calendar.DAY_OF_MONTH);
			if (lastProccessed == null) {
				lastProccessed = tsi;
				filledMap.put(itemDate, tsi);
				continue;
			}
			Date date = DateUtils.truncate(DateUtils.addDays(lastProccessed.getItemDate(), 1), Calendar.DAY_OF_MONTH);

			while (date.before(itemDate)) {
				RawTimeSerieItem tempTimeSerieItem = bd.cloneItem(lastProccessed, date);
				filledMap.put(tempTimeSerieItem.getItemDate(), tempTimeSerieItem);
				date = DateUtils.addDays(date, 1);

			}
			filledMap.put(itemDate, tsi);
			lastProccessed = tsi;
		}
		return filledMap;
	}

	private Map<Date, RawTimeSerieItem> merge(Map<Date, RawTimeSerieItem> map1, Map<Date, RawTimeSerieItem> map2) throws BusinessException {
		map1.putAll(map2);
		Map<Date, RawTimeSerieItem> map = this.buildFullMap(new ArrayList<RawTimeSerieItem>(map1.values()));
		RawTimeSerieItem lastItem = null;
		for (Entry<Date, RawTimeSerieItem> entry : map2.entrySet()) {
			Date key = entry.getKey();
			map.remove(key);
			if (lastItem == null || key.after(lastItem.getItemDate())) {
				lastItem = map2.get(key);
			}
		}
		if (lastItem != null) {
			for (Entry<Date, RawTimeSerieItem> entry : map.entrySet()) {
				Date key = entry.getKey();
				if (key.after(lastItem.getItemDate()) && map.get(key).getCalculated()) {
					map.get(key).setItemValue(lastItem.getItemValue());
				}
			}
		}
		return map;
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.business.service.entity.AbstractEntityWriteService#postUpdate (java.util.List)
	 */
	@Override
	protected void postUpdate(List<RawTimeSerieItem> list) throws BusinessException {

		super.postUpdate(list);
		ITimeSerieService timeSerieService = (ITimeSerieService) this.findEntityService(TimeSerie.class);
		Map<Integer, TimeSerie> tsMap = new HashMap<>();
		for (RawTimeSerieItem tsi : list) {
			if (!tsi.getCalculated()) {
				TimeSerie tserie = tsi.getTserie();
				if (!tsMap.containsKey(tserie.getId())) {
					tsMap.put(tserie.getId(), tserie);
				}
				this.updateTimeSerieItemList(tserie, tsi);
				tserie.setApprovalStatus(TimeSeriesApprovalStatus._NEW_);
				tserie.setStatus(TimeSeriesStatus._WORK_);
			}
		}
		timeSerieService.update(new ArrayList<TimeSerie>(tsMap.values()));
	}

	/**
	 * Update the time serie item list
	 *
	 * @param ts
	 * @param tsi
	 * @param tsiBD
	 * @throws BusinessException
	 */
	private void updateTimeSerieItemList(TimeSerie ts, RawTimeSerieItem tsi) throws BusinessException {

		// List declaration
		List<RawTimeSerieItem> tsiModifyList = new ArrayList<>();
		List<RawTimeSerieItem> tsiNewList = new ArrayList<>();
		List<RawTimeSerieItem> tsiDeleteList = new ArrayList<>();
		// check if the item date is bigger than than old list's last
		// element

		if (tsi.getItemDate().after(ts.getLastUsage())) {
			ts = this.updateItemsAfterList(tsi, ts, tsiNewList, tsiModifyList);
		} else

		// check if the item date is smaller than than old list first
		// element
		if (ts.getFirstUsage().after(tsi.getItemDate())) {
			ts = this.updateItemsBeforeList(tsi, ts, tsiNewList, tsiModifyList);
		} else {
			this.updateItemsOnList(tsi, ts, tsiNewList, tsiModifyList, tsiDeleteList);
		}

		// on case if the last item will be updated and will be first
		this.setFirstAndLastUsage(ts);

		tsiDeleteList.addAll(this.getCalculatedOutsideTheRange(ts.getFirstUsage(), ts.getLastUsage(), ts.getId()));

		this.saveModification(tsiNewList, tsiModifyList, tsiDeleteList);
		this.fillTheMissingItem(ts);

	}

	/**
	 * Recalculate existing items.
	 *
	 * @param tsi
	 * @param ts
	 * @param newList
	 * @param updateList
	 * @param deleteList
	 * @throws BusinessException
	 */
	private void updateItemsOnList(RawTimeSerieItem tsi, TimeSerie ts, List<RawTimeSerieItem> newList, List<RawTimeSerieItem> updateList,
			List<RawTimeSerieItem> deleteList) throws BusinessException {

		RawTimeSerieItem_Bd s = this.getBusinessDelegate(RawTimeSerieItem_Bd.class);

		RawTimeSerieItem tsiNext = this.findNextNotCalculatedItem(tsi);
		RawTimeSerieItem tsiLast = this.findPrevNotCalculatedItem(tsi);

		RawTimeSerieItem tsiEqualActual = this.findFromDateAndSerieId(tsi.getItemDate(), ts, true);
		if (tsiEqualActual != null) {
			deleteList.add(tsiEqualActual);
		}

		if (tsiNext == null) {
			Date itemDate = tsi.getItemDate();
			ts.setLastUsage(itemDate);
			if (s.isThisWeekDay(itemDate, Calendar.FRIDAY) || s.isThisWeekDay(itemDate, Calendar.SATURDAY)) {
				this.updateWeekend(ts, tsi, s, itemDate, newList, updateList);
			}
		} else {

			List<RawTimeSerieItem> tempList = this.getAllElementsBetweenDates(tsi.getItemDate(), tsiNext.getItemDate(), ts.getId(), true);
			s.changeItemValue(tempList, tsi.getItemValue());
			updateList.addAll(tempList);
		}
		if (tsiLast == null) {
			ts.setFirstUsage(tsi.getItemDate());
		}
	}

	/**
	 * Update the weekend days.
	 *
	 * @param ts
	 * @param tsi
	 * @param s
	 * @param itemDate
	 * @param newList
	 * @param updateList
	 * @param deleteList
	 */
	private void updateWeekend(TimeSerie ts, RawTimeSerieItem tsi, RawTimeSerieItem_Bd s, Date itemDate, List<RawTimeSerieItem> newList,
			List<RawTimeSerieItem> updateList) {
		List<RawTimeSerieItem> tempList;
		if (s.isThisWeekDay(itemDate, Calendar.FRIDAY)) {
			tempList = this.getAllElementsBetweenDates(itemDate, DateUtils.addDays(itemDate, 3), ts.getId(), true);
		} else {
			tempList = this.getAllElementsBetweenDates(itemDate, DateUtils.addDays(itemDate, 2), ts.getId(), true);
		}

		if (tempList != null && (tempList.size() == 2 || tempList.size() == 1)) {
			s.changeItemValue(tempList, tsi.getItemValue());
			updateList.addAll(tempList);
		} else {
			newList.addAll(s.processFriday(tsi, new HashMap<Date, RawTimeSerieItem>()));
			ts.setLastUsage(DateUtils.addDays(tsi.getItemDate(), 2));
		}

	}

	/**
	 * Update the element if the modified element is the last on the list.
	 *
	 * @param tsi
	 * @param ts
	 * @param newList
	 * @param updateList
	 * @throws BusinessException
	 */
	private TimeSerie updateItemsAfterList(RawTimeSerieItem tsi, TimeSerie ts, List<RawTimeSerieItem> newList, List<RawTimeSerieItem> updateList)
			throws BusinessException {

		RawTimeSerieItem_Bd s = this.getBusinessDelegate(RawTimeSerieItem_Bd.class);

		RawTimeSerieItem tsTemp = this.findPrevNotCalculatedItem(tsi);
		if (tsTemp != null) {
			tsTemp.getTserie();
			Date from = ts.getLastUsage();
			Date to = DateUtils.addDays(tsi.getItemDate(), -1);
			newList.addAll(s.generateListOfItem(tsTemp, from, to));
			updateList.addAll(this.getAllElementsBetweenDates(tsTemp.getItemDate(), ts.getLastUsage(), ts.getId(), true));

			ts.setLastUsage(tsi.getItemDate());

		}
		if (s.isThisWeekDay(tsi.getItemDate(), Calendar.FRIDAY)) {
			newList.addAll(s.processFriday(tsi, new HashMap<Date, RawTimeSerieItem>()));
			ts.setLastUsage(DateUtils.addDays(tsi.getItemDate(), 2));
		}
		return ts;
	}

	/**
	 * Update the list if the modified element is the first.
	 *
	 * @param tsi
	 * @param ts
	 * @param newList
	 * @param updateList
	 * @throws BusinessException
	 */
	private TimeSerie updateItemsBeforeList(RawTimeSerieItem tsi, TimeSerie ts, List<RawTimeSerieItem> newList, List<RawTimeSerieItem> updateList)
			throws BusinessException {

		RawTimeSerieItem_Bd tsiBD = this.getBusinessDelegate(RawTimeSerieItem_Bd.class);
		RawTimeSerieItem tsTemp = this.findNextNotCalculatedItem(tsi);

		if (tsTemp != null) {
			Date to = DateUtils.addDays(ts.getFirstUsage(), -1);
			Date from = tsi.getItemDate();

			List<RawTimeSerieItem> temp = this.getAllElementsBetweenDates(ts.getFirstUsage(), tsTemp.getItemDate(), ts.getId(), true);

			tsiBD.changeItemValue(temp, tsi.getItemValue());
			updateList.addAll(temp);

			newList.addAll(tsiBD.generateListOfItem(tsi, from, to));
			ts.setFirstUsage(tsi.getItemDate());
		} else if (tsiBD.isThisWeekDay(tsi.getItemDate(), Calendar.FRIDAY)) {
			newList.addAll(tsiBD.processFriday(tsi, new HashMap<Date, RawTimeSerieItem>()));
			ts.setLastUsage(DateUtils.addDays(tsi.getItemDate(), 2));
		}
		return ts;
	}

	/**
	 * @param newList
	 * @param updateList
	 * @param deleteList
	 * @throws BusinessException
	 */
	private void saveModification(List<RawTimeSerieItem> newList, List<RawTimeSerieItem> updateList, List<RawTimeSerieItem> deleteList)
			throws BusinessException {

		// update the modified elements
		if (!CollectionUtils.isEmpty(updateList)) {
			this.update(updateList);
		}
		// save the new generated elements
		if (!CollectionUtils.isEmpty(newList)) {
			this.insert(newList);
		}
		// delete elements
		if (!CollectionUtils.isEmpty(deleteList)) {
			this.delete(deleteList);
		}
	}

	/**
	 * Set the last and first usage in the time series to the first and the last not calculated item. If the last is Friday set to the Sunday
	 *
	 * @param ts
	 * @throws BusinessException
	 */
	private void setFirstAndLastUsage(TimeSerie ts) throws BusinessException {

		RawTimeSerieItem_Bd tmsbd = new RawTimeSerieItem_Bd();
		RawTimeSerieItem tsiLast = this.getLastNotCalculatedItem(ts);
		RawTimeSerieItem tsiFirst = this.getFirstNotCalculatedItem(ts.getId());

		if (tsiFirst != null) {
			ts.setFirstUsage(tsiFirst.getItemDate());
		} else {
			ts.setFirstUsage(null);
		}

		if (tsiLast != null) {
			if (tmsbd.isThisWeekDay(tsiLast.getItemDate(), Calendar.FRIDAY)) {
				ts.setLastUsage(DateUtils.addDays(tsiLast.getItemDate(), 2));
			} else if (tmsbd.isThisWeekDay(tsiLast.getItemDate(), Calendar.SATURDAY)) {
				ts.setLastUsage(DateUtils.addDays(tsiLast.getItemDate(), 1));
			} else {
				ts.setLastUsage(tsiLast.getItemDate());
			}

		} else {
			ts.setLastUsage(null);
		}
	}

	/**
	 * Iterate in list and if a value are not valid value, set the valid value, and if an item is missing will insert a clon
	 *
	 * @param ts
	 * @throws BusinessException
	 */
	private void fillTheMissingItem(TimeSerie ts) throws BusinessException {

		Date firstUsage = ts.getFirstUsage();
		Date lastUsage = ts.getLastUsage();
		RawTimeSerieItem_Bd s = new RawTimeSerieItem_Bd();
		List<RawTimeSerieItem> tsiList = this.getAllElementsBetweenDates(DateUtils.addDays(firstUsage, -1), DateUtils.addDays(lastUsage, 1),
				ts.getId(), null);
		List<RawTimeSerieItem> tsiModifyList = new ArrayList<>();
		List<RawTimeSerieItem> tsiNewList = new ArrayList<>();
		if (CollectionUtils.isEmpty(tsiList)) {
			return;
		}

		RawTimeSerieItem actTsi = tsiList.get(0);
		RawTimeSerieItem lastTsi = tsiList.get(0);
		BigDecimal value = lastTsi.getItemValue();
		Date actDay = lastTsi.getItemDate();
		Date iterDay = lastTsi.getItemDate();
		int k = 0;

		while (!actDay.equals(lastUsage) && k < tsiList.size() - 1 && lastUsage.after(iterDay)) {
			if (actDay.equals(actTsi.getItemDate())) {
				if (actTsi.getItemValue().compareTo(value) != 0) {
					actTsi.setItemValue(value);
					tsiModifyList.add(actTsi);
				}
				actDay = DateUtils.addDays(actDay, 1);
				if (k <= tsiList.size() - 2) {
					actTsi = tsiList.get(++k);
					if (!actTsi.getCalculated()) {
						value = actTsi.getItemValue();
						lastTsi = actTsi;
					}
				}
			}
			if (actTsi.getItemDate().after(actDay)) {

				RawTimeSerieItem temp = s.cloneItem(lastTsi, null);
				temp.setCalculated(true);
				temp.setItemDate(actDay);
				tsiNewList.add(temp);
				actDay = DateUtils.addDays(actDay, 1);
			}
			iterDay = DateUtils.addDays(iterDay, 1);
		}
		if (!CollectionUtils.isEmpty(tsiModifyList)) {
			this.update(tsiModifyList);
		}
		if (!CollectionUtils.isEmpty(tsiNewList)) {
			this.insert(tsiNewList);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.business.service.entity.AbstractEntityWriteService#preDeleteByIds (java.util.List, java.util.Map)
	 */
	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {

		super.preDeleteByIds(ids, context);
		IRawTimeSerieItemService tsis = (IRawTimeSerieItemService) this.findEntityService(RawTimeSerieItem.class);
		List<RawTimeSerieItem> list = tsis.findByIds(ids);

		for (RawTimeSerieItem tsi : list) {
			tsi.setActive(false);
			if (tsi.getCalculated()) {
				throw new BusinessException(BusinessErrorCode.TSI_CANNOT_DELETE,
						String.format(BusinessErrorCode.TSI_CANNOT_DELETE.getErrMsg(), SimpleDateFormat.getInstance().format(tsi.getItemDate())));
			}
		}

		context.put(CONTEXT_KEY, list);
	}

	@Override
	protected void postDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.postDeleteByIds(ids, context);
		@SuppressWarnings("unchecked")
		List<RawTimeSerieItem> list = (List<RawTimeSerieItem>) context.get(CONTEXT_KEY);
		for (RawTimeSerieItem tsi : list) {
			TimeSerie ts = tsi.getTserie();
			if (!tsi.getCalculated()) {
				this.deleteTimeSerieItem(ts, tsi);
				IEntityService<TimeSerie> timeSerieService = this.findEntityService(TimeSerie.class);
				ts.setApprovalStatus(TimeSeriesApprovalStatus._NEW_);
				ts.setStatus(TimeSeriesStatus._WORK_);
				timeSerieService.update(ts);
			}
		}
	}

	/**
	 * Process the time serie item delete.
	 *
	 * @param ts
	 * @param tsi
	 * @throws BusinessException
	 */
	protected void deleteTimeSerieItem(TimeSerie ts, RawTimeSerieItem tsi) throws BusinessException {

		RawTimeSerieItem_Bd tsiBD = this.getBusinessDelegate(RawTimeSerieItem_Bd.class);

		Date firstUsage = ts.getFirstUsage();
		Date lastUsage = ts.getLastUsage();

		RawTimeSerieItem nextTsi = this.findNextNotCalculatedItem(tsi);
		RawTimeSerieItem prevTsi = this.findPrevNotCalculatedItem(tsi);

		if (nextTsi != null && nextTsi.getItemDate().after(lastUsage)) {
			nextTsi = null;
		}

		if (prevTsi != null && prevTsi.getItemDate().before(firstUsage)) {
			prevTsi = null;
		}

		if (nextTsi != null || prevTsi != null) {
			// if is the first item. Delete to the next not calculated item
			if (firstUsage != null && tsi.getItemDate().equals(firstUsage)) {
				this.deleteFirstItem(ts, tsi, nextTsi, tsiBD);
				// if is the last item. delete from the last not calculated
				// item to the and
			} else if (this.isLastNonProvisional(tsi, tsiBD, lastUsage)) {
				this.deleteLastItem(tsiBD, ts, tsi, prevTsi);
			} else {
				this.deleteItem(tsi, prevTsi, nextTsi, tsiBD);
			}
		} else {
			this.deleteSingleItem(ts, tsi, firstUsage, lastUsage);
		}
		IEntityService<TimeSerie> timeSerieService = this.findEntityService(TimeSerie.class);
		timeSerieService.update(ts);
	}

	private boolean isLastNonProvisional(RawTimeSerieItem tsi, RawTimeSerieItem_Bd tsiBD, Date lastUsage) {
		return lastUsage != null && tsi.getItemDate().equals(lastUsage)
				|| (tsiBD.isThisWeekDay(tsi.getItemDate(), Calendar.FRIDAY) && DateUtils.addDays(tsi.getItemDate(), 2).equals(lastUsage))
				|| (tsiBD.isThisWeekDay(tsi.getItemDate(), Calendar.SATURDAY) && DateUtils.addDays(tsi.getItemDate(), 1).equals(lastUsage));
	}

	/**
	 * Delete item if exists a single not calculated item.
	 *
	 * @param ts
	 * @param tsi
	 * @param tsiBD
	 * @param firstUsage
	 * @param lastUsage
	 * @throws BusinessException
	 */
	private void deleteSingleItem(TimeSerie ts, RawTimeSerieItem tsi, Date firstUsage, Date lastUsage) throws BusinessException {
		if ((firstUsage != null && lastUsage != null) && lastUsage.after(firstUsage)) {
			Date itemDate = tsi.getItemDate();
			List<RawTimeSerieItem> tempList = this.getAllElementsBetweenDates(itemDate, DateUtils.addDays(itemDate, 3), ts.getId(), true);
			this.delete(tempList);
		}
		ts.setFirstUsage(null);
		ts.setLastUsage(null);

	}

	/**
	 * Delete and recalculate item from the list.
	 *
	 * @param tsi
	 * @param prevTsi
	 * @param nextTsi
	 * @param tsiBD
	 * @throws BusinessException
	 */
	private void deleteItem(RawTimeSerieItem tsi, RawTimeSerieItem prevTsi, RawTimeSerieItem nextTsi, RawTimeSerieItem_Bd tsiBD)
			throws BusinessException {
		List<RawTimeSerieItem> tsiList = this.getElementsBetweenItems(tsi, nextTsi);
		RawTimeSerieItem temp = tsiBD.cloneItem(tsi, tsi.getItemDate());
		temp.setItemValue(prevTsi.getItemValue());
		this.getEntityManager().flush();
		this.onInsert(temp);

		tsiBD.changeItemValue(tsiList, prevTsi.getItemValue());
		this.update(tsiList);
	}

	/**
	 * Delete the first item and the calculated elements
	 *
	 * @param ts
	 * @param tsi
	 * @param nextTsi
	 * @param tsiBD
	 * @throws BusinessException
	 */
	private void deleteFirstItem(TimeSerie ts, RawTimeSerieItem tsi, RawTimeSerieItem nextTsi, RawTimeSerieItem_Bd tsiBD) throws BusinessException {
		ts.setFirstUsage(nextTsi.getItemDate());
		List<RawTimeSerieItem> tsiList = this.getElementsBetweenItems(tsi, nextTsi);
		this.delete(tsiList);

		List<RawTimeSerieItem> itemsForCalculation = new ArrayList<>();
		RawTimeSerieItem cloneItem = tsiBD.cloneItem(nextTsi, nextTsi.getItemDate());
		cloneItem.setCalculated(nextTsi.getCalculated());
		itemsForCalculation.add(cloneItem);

	}

	/**
	 * Delete the items if the deleted item is the last
	 *
	 * @param tsiBD
	 * @param ts
	 * @param tsi
	 * @param firstDelDate
	 * @throws BusinessException
	 */
	private void deleteLastItem(RawTimeSerieItem_Bd tsiBD, TimeSerie ts, RawTimeSerieItem tsi, RawTimeSerieItem prevTsi) throws BusinessException {
		Date firstDelDate = prevTsi.getItemDate();
		if (tsiBD.isThisWeekDay(firstDelDate, Calendar.FRIDAY) && !firstDelDate.equals(DateUtils.addDays(tsi.getItemDate(), -1))) {
			firstDelDate = DateUtils.addDays(firstDelDate, 2);
		}
		if (tsiBD.isThisWeekDay(firstDelDate, Calendar.SATURDAY)) {
			firstDelDate = DateUtils.addDays(firstDelDate, 1);
		}

		List<RawTimeSerieItem> tsiList = this.getAllElementsBetweenDates(firstDelDate, DateUtils.addDays(tsi.getItemDate(), 3), ts.getId(), null);

		RawTimeSerieItem nextTsi = null;
		boolean isAllCalculated = true;
		for (RawTimeSerieItem tsiTemp : tsiList) {

			if (!tsiTemp.getCalculated() && !tsiTemp.equals(tsi)) {
				isAllCalculated = false;
				nextTsi = tsiTemp;
			}
		}

		if (isAllCalculated) {
			ts.setLastUsage(firstDelDate);
			this.delete(tsiList);
		} else {
			this.deleteItem(tsi, prevTsi, nextTsi, tsiBD);
		}

		List<RawTimeSerieItem> itemsForCalculation = new ArrayList<>();
		RawTimeSerieItem cloneItem = tsiBD.cloneItem(prevTsi, prevTsi.getItemDate());
		cloneItem.setCalculated(prevTsi.getCalculated());
		itemsForCalculation.add(cloneItem);
	}

	/**
	 * Find the time serie item from date and serie id
	 *
	 * @param itemDate
	 * @param serieId
	 * @param calculated
	 * @return an item or null if are multiple items
	 */
	private RawTimeSerieItem findFromDateAndSerieId(Date itemDate, TimeSerie timeSerie, Boolean calculated) {
		if (itemDate != null) {
			Map<String, Object> params = new HashMap<>();
			params.put("tserie", timeSerie);
			params.put(CALCULATED2, calculated);
			params.put("itemDate", itemDate);
			List<RawTimeSerieItem> list = this.findEntitiesByAttributes(params);
			if (!CollectionUtils.isEmpty(list) && list.size() == 1) {
				return list.get(0);
			}
		}
		return null;
	}

	/**
	 * Find the next not calculated time serie item
	 *
	 * @param tsi
	 * @return
	 * @throws BusinessException
	 */

	private RawTimeSerieItem findNextNotCalculatedItem(RawTimeSerieItem tsi) throws BusinessException {

		String sql = "select e from " + RawTimeSerieItem.class.getSimpleName()
				+ " e where e.clientId=:clientId and e.itemDate > :itemDate and e.tserie.id = :serieId and e.calculated = false order by e.itemDate";

		List<RawTimeSerieItem> list = this.getEntityManager().createQuery(sql, RawTimeSerieItem.class)
				.setParameter(CLIENT_ID, Session.user.get().getClient().getId()).setParameter(ITEM_DATE, tsi.getItemDate())
				.setParameter(SERIE_ID, tsi.getTserie().getId()).getResultList();
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}
		return list.get(0);
	}

	/**
	 * @param serieId
	 * @return
	 * @throws BusinessException
	 */
	private RawTimeSerieItem getFirstNotCalculatedItem(Integer serieId) throws BusinessException {

		String sql = "select e from " + RawTimeSerieItem.class.getSimpleName()
				+ " e where e.clientId =:clientId and e.tserie.id = :serieId and e.calculated = false order by e.itemDate";

		List<RawTimeSerieItem> list = this.getEntityManager().createQuery(sql, RawTimeSerieItem.class)
				.setParameter(CLIENT_ID, Session.user.get().getClient().getId()).setParameter(SERIE_ID, serieId).getResultList();

		if (CollectionUtils.isEmpty(list)) {
			return null;
		}
		return list.get(0);
	}

	/**
	 * Find the previously not calculated item
	 *
	 * @param tsi
	 * @return
	 * @throws BusinessException
	 */
	private RawTimeSerieItem findPrevNotCalculatedItem(RawTimeSerieItem tsi) throws BusinessException {

		String sql = "select e from " + RawTimeSerieItem.class.getSimpleName()
				+ " e where e.clientId =:clientId and e.itemDate < :itemDate and e.tserie.id = :serieId and e.calculated = false order by e.itemDate";

		List<RawTimeSerieItem> list = this.getEntityManager().createQuery(sql, RawTimeSerieItem.class)
				.setParameter(CLIENT_ID, Session.user.get().getClient().getId()).setParameter(ITEM_DATE, tsi.getItemDate())
				.setParameter(SERIE_ID, tsi.getTserie().getId()).getResultList();

		if (CollectionUtils.isEmpty(list)) {
			return null;
		}
		return list.get(list.size() - 1);
	}

	/**
	 * Get the bigger not calculated item
	 *
	 * @param serieId
	 * @return
	 * @throws BusinessException
	 */
	private RawTimeSerieItem getLastNotCalculatedItem(TimeSerie timeSerie) throws BusinessException {

		Map<String, Object> params = new HashMap<>();
		params.put("tserie", timeSerie);
		params.put(CALCULATED2, false);

		List<RawTimeSerieItem> list = this.findEntitiesByAttributes(params);
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}
		Collections.sort(list, new Comparator<RawTimeSerieItem>() {
			@Override
			public int compare(RawTimeSerieItem o1, RawTimeSerieItem o2) {
				return o1.getItemDate().compareTo(o2.getItemDate());
			}
		});
		return list.get(list.size() - 1);

	}

	/**
	 * @param tsiFirst
	 * @param tsiLast
	 * @return
	 */
	private List<RawTimeSerieItem> getElementsBetweenItems(RawTimeSerieItem tsiFirst, RawTimeSerieItem tsiLast) {

		return this.getAllElementsBetweenDates(tsiFirst.getItemDate(), tsiLast.getItemDate(), tsiFirst.getTserie().getId(), null);
	}

	/**
	 * Retrieve from database all elements which between <code>tsiFirst</code> and <code>tsiLast</code> excluding margins for the time series with
	 * <code>serieId</code>.
	 *
	 * @param tsiFirst
	 * @param tsiLast
	 * @param serieId
	 * @param calculated
	 *            On null retrieves all elements, on false retrieves only non calculated elements, on true retrieves only calculated elements.
	 * @return
	 */
	private List<RawTimeSerieItem> getAllElementsBetweenDates(Date tsiFirst, Date tsiLast, Integer serieId, Boolean calculated) {

		if (tsiFirst == null || tsiLast == null) {
			return new ArrayList<>();
		}

		// eliminate non-calculated items because they will be deleted on delete
		// action
		// this function are called on pre-delete event
		String sql = "select e from RawTimeSerieItem "
				+ " e where e.clientId=:clientId and e.itemDate > :itemFirst and e.itemDate < :itemLast and e.tserie.id = :serieId ";
		if (calculated != null) {
			sql += "and e.calculated = :calculated";
		}

		sql += " order by e.itemDate";
		TypedQuery<RawTimeSerieItem> query = this.getEntityManager().createQuery(sql, RawTimeSerieItem.class)
				.setParameter(CLIENT_ID, Session.user.get().getClient().getId()).setParameter("itemFirst", tsiFirst, TemporalType.DATE)
				.setParameter("itemLast", tsiLast, TemporalType.DATE).setParameter(SERIE_ID, serieId);

		if (calculated != null) {
			query.setParameter(CALCULATED2, calculated);
		}
		return query.getResultList();
	}

	/**
	 * @param tsiFirst
	 * @param tsiLast
	 * @param serieId
	 * @return
	 */
	private List<RawTimeSerieItem> getCalculatedOutsideTheRange(Date tsiFirst, Date tsiLast, Integer serieId) {

		if (tsiFirst == null || tsiLast == null) {
			return new ArrayList<>();
		}

		String sql = "select e from " + RawTimeSerieItem.class.getSimpleName()
				+ " e where e.clientId=:clientId and e.tserie.id = :serieId and e.calculated = true and (e.itemDate < :itemFirst or e.itemDate > :itemLast) order by e.itemDate";

		return this.getEntityManager().createQuery(sql, RawTimeSerieItem.class).setParameter(CLIENT_ID, Session.user.get().getClient().getId())
				.setParameter("itemFirst", tsiFirst).setParameter("itemLast", tsiLast).setParameter(SERIE_ID, serieId).getResultList();
	}
}
