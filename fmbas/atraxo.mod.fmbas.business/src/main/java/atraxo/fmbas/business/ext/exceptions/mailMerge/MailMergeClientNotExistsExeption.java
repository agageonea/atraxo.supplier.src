package atraxo.fmbas.business.ext.exceptions.mailMerge;

import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author apetho
 */
public class MailMergeClientNotExistsExeption extends BusinessException {

	private static final long serialVersionUID = -6168728611098274755L;

	/**
	 * @param exception
	 */
	public MailMergeClientNotExistsExeption(Throwable exception) {
		super(BusinessErrorCode.MAIL_MERGE_CLIENT_NOT_EXISTS, BusinessErrorCode.MAIL_MERGE_CLIENT_NOT_EXISTS.getErrMsg(), exception);
	}

}
