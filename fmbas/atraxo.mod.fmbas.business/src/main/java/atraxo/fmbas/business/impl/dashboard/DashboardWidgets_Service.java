/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.dashboard;

import atraxo.fmbas.domain.impl.dashboard.AssignedWidgetParameters;
import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;
import atraxo.fmbas.domain.impl.dashboard.Dashboards;
import atraxo.fmbas.domain.impl.dashboard.Layouts;
import atraxo.fmbas.domain.impl.dashboard.Widgets;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link DashboardWidgets} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class DashboardWidgets_Service
		extends
			AbstractEntityService<DashboardWidgets> {

	/**
	 * Public constructor for DashboardWidgets_Service
	 */
	public DashboardWidgets_Service() {
		super();
	}

	/**
	 * Public constructor for DashboardWidgets_Service
	 * 
	 * @param em
	 */
	public DashboardWidgets_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<DashboardWidgets> getEntityClass() {
		return DashboardWidgets.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return DashboardWidgets
	 */
	public DashboardWidgets findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DashboardWidgets.NQ_FIND_BY_BUSINESS,
							DashboardWidgets.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DashboardWidgets", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DashboardWidgets", "id"), nure);
		}
	}

	/**
	 * Find by reference: dashboard
	 *
	 * @param dashboard
	 * @return List<DashboardWidgets>
	 */
	public List<DashboardWidgets> findByDashboard(Dashboards dashboard) {
		return this.findByDashboardId(dashboard.getId());
	}
	/**
	 * Find by ID of reference: dashboard.id
	 *
	 * @param dashboardId
	 * @return List<DashboardWidgets>
	 */
	public List<DashboardWidgets> findByDashboardId(Integer dashboardId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DashboardWidgets e where e.clientId = :clientId and e.dashboard.id = :dashboardId",
						DashboardWidgets.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("dashboardId", dashboardId).getResultList();
	}
	/**
	 * Find by reference: layout
	 *
	 * @param layout
	 * @return List<DashboardWidgets>
	 */
	public List<DashboardWidgets> findByLayout(Layouts layout) {
		return this.findByLayoutId(layout.getId());
	}
	/**
	 * Find by ID of reference: layout.id
	 *
	 * @param layoutId
	 * @return List<DashboardWidgets>
	 */
	public List<DashboardWidgets> findByLayoutId(Integer layoutId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DashboardWidgets e where e.clientId = :clientId and e.layout.id = :layoutId",
						DashboardWidgets.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("layoutId", layoutId).getResultList();
	}
	/**
	 * Find by reference: widget
	 *
	 * @param widget
	 * @return List<DashboardWidgets>
	 */
	public List<DashboardWidgets> findByWidget(Widgets widget) {
		return this.findByWidgetId(widget.getId());
	}
	/**
	 * Find by ID of reference: widget.id
	 *
	 * @param widgetId
	 * @return List<DashboardWidgets>
	 */
	public List<DashboardWidgets> findByWidgetId(Integer widgetId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DashboardWidgets e where e.clientId = :clientId and e.widget.id = :widgetId",
						DashboardWidgets.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("widgetId", widgetId).getResultList();
	}
	/**
	 * Find by reference: assignedWidgetParameters
	 *
	 * @param assignedWidgetParameters
	 * @return List<DashboardWidgets>
	 */
	public List<DashboardWidgets> findByAssignedWidgetParameters(
			AssignedWidgetParameters assignedWidgetParameters) {
		return this.findByAssignedWidgetParametersId(assignedWidgetParameters
				.getId());
	}
	/**
	 * Find by ID of reference: assignedWidgetParameters.id
	 *
	 * @param assignedWidgetParametersId
	 * @return List<DashboardWidgets>
	 */
	public List<DashboardWidgets> findByAssignedWidgetParametersId(
			Integer assignedWidgetParametersId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from DashboardWidgets e, IN (e.assignedWidgetParameters) c where e.clientId = :clientId and c.id = :assignedWidgetParametersId",
						DashboardWidgets.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("assignedWidgetParametersId",
						assignedWidgetParametersId).getResultList();
	}
}
