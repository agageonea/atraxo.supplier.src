/**
 *
 */
package atraxo.fmbas.business.ext.bpm.delegate.timeseries.approval;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public abstract class AbstractUserActionTimeSeriesDelegate extends AbstractJavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractUserActionTimeSeriesDelegate.class);
	@Autowired
	protected ITimeSerieService timeSerieService;

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#execute()
	 */
	@Override
	public void execute() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}

		Object timeSerieId = this.execution.getVariable(WorkflowVariablesConstants.TIME_SERIE_ID);
		if (timeSerieId != null) {
			Integer tSeriesId = Integer.parseInt(timeSerieId.toString());
			TimeSerie timeSeries = null;
			try {
				timeSeries = this.timeSerieService.findByBusiness(tSeriesId);
			} catch (Exception e) {
				LOGGER.error("ERROR:could not find an energy price for ID " + tSeriesId, e);
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						String.format(BusinessErrorCode.WORKFLOW_COULD_NOT_FIND_AN_ENERGY_PRICE_FOR_ID.getErrMsg(), tSeriesId), e);
			}
			if (timeSeries != null) {
				this.executeAction(timeSeries);
			}
		}
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
	}

	protected abstract void executeAction(TimeSerie timeSerie) throws BusinessException;

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#getSessionUserCode()
	 */
	@Override
	public String getSessionUserCode() {
		return this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME);
	}

}
