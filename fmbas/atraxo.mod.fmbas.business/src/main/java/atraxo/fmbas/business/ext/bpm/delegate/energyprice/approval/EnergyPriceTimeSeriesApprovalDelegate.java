package atraxo.fmbas.business.ext.bpm.delegate.energyprice.approval;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.ApprovalDelegate;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailAddressesDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EnergyPriceTimeSerieApprovalDto;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.user.UserSupp;

/**
 * Delegate class used for the approval process of an Energy Price Time Series
 *
 * @author aradu
 */
public class EnergyPriceTimeSeriesApprovalDelegate extends ApprovalDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnergyPriceTimeSeriesApprovalDelegate.class);

	@Autowired
	private ITimeSerieService timeSerieService;

	@Override
	public void execute() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}

		this.setHasUsersToApproveVariable(true);

		List<UserSupp> users = this.extractApprovers(WorkflowVariablesConstants.APPROVER_ROLE);
		if (!CollectionUtils.isEmpty(users)) {
			TimeSerie energyPriceTimeSerie = this.extractEnergyPrice();
			if (energyPriceTimeSerie != null) {
				EmailDto emailDTO = this.extractEmailDto(energyPriceTimeSerie, users);
				if (emailDTO != null) {
					this.execution.setVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_DTO, emailDTO);
				}
			}
		} else {
			this.setHasUsersToApproveVariable(false);
		}

		this.resetMailComponentVariables();

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
	}

	/**
	 * Method that extracts the energy price time series that was submitted for approval
	 *
	 * @return
	 * @throws Exception
	 */
	private TimeSerie extractEnergyPrice() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}
		TimeSerie energyPrice = null;
		Object energyPriceObjId = this.execution.getVariable(WorkflowVariablesConstants.TIME_SERIE_ID);
		if (energyPriceObjId != null) {
			String energyPriceIdString = energyPriceObjId.toString();
			if (!"".equals(energyPriceIdString)) {
				try {
					Integer energyPriceTimeSerieId = Integer.parseInt(energyPriceIdString);
					try {
						energyPrice = this.timeSerieService.findByBusiness(energyPriceTimeSerieId);
					} catch (Exception e) {
						LOGGER.error("ERROR:could not find an Energy Price time serie for ID " + energyPriceTimeSerieId, e);
						this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
								String.format(BusinessErrorCode.WORKFLOW_COULD_NOT_FIND_AN_ENERGY_PRICE_FOR_ID.getErrMsg(), energyPriceTimeSerieId),
								e);
					}
				} catch (NumberFormatException e) {
					LOGGER.error("ERROR:could not parse the received Energy Price time serie ID ", energyPriceObjId);
					this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
							String.format(BusinessErrorCode.WORKFLOW_COULD_NOT_PARSE_THE_ENERGY_PRICE_ID.getErrMsg(), energyPriceObjId), e);
				}
			} else {
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						String.format(BusinessErrorCode.WORKFLOW_ENERGY_PRICE_TIME_SERIE_EMPTY_ID.getErrMsg(), energyPriceIdString));
			}
		} else {
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
					BusinessErrorCode.WORKFLOW_ENERGY_PRICE_TIME_SERIE_VARIABLE_NOT_FOUND.getErrMsg());
		}
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
		return energyPrice;

	}

	/**
	 * Builds the email DTO from EnergyPriceTimeSerie
	 *
	 * @param energyPriceTimeSerie
	 * @param users
	 * @return
	 * @throws Exception
	 */
	private EmailDto extractEmailDto(TimeSerie energyPriceTimeSerie, List<UserSupp> users) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}
		EmailDto emailDTO = new EmailDto();
		String timeSeriesName = null;
		try {
			timeSeriesName = energyPriceTimeSerie.getSerieName();
		} catch (Exception e) {
			LOGGER.error("ERROR:could not extract data from the Energy Price time serie  " + energyPriceTimeSerie.getId()
					+ " in order to send the mail ! ", e);
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
					String.format(BusinessErrorCode.DTO_CANT_EXTRACT_DATA_FROM_ENTITY_WORKFLOW.getErrMsg(), "Energy Price Time Serie",
							energyPriceTimeSerie.getId()));
			emailDTO = null;
		}

		if (emailDTO != null) {
			// set the subject
			emailDTO.setSubject("An energy price change for " + energyPriceTimeSerie.getSerieName() + " is awaiting your approval.");

			// set the email content
			List<EmailAddressesDto> emailAddressesDtoList = new ArrayList<>();
			for (UserSupp user : users) {
				// extract email dto data from Exchange Rate time series and user
				EnergyPriceTimeSerieApprovalDto dto = new EnergyPriceTimeSerieApprovalDto();
				dto.setApprovalRequestNote(this.getVariableFromWorkflow(WorkflowVariablesConstants.APPROVE_NOTE));
				dto.setFullNameOfRequester(this.getVariableFromWorkflow(WorkflowVariablesConstants.FULLNAME_REQUESTER_VAR));
				dto.setTimeSeries(timeSeriesName);
				dto.setFullName(user.getFirstName() + " " + user.getLastName());
				dto.setTitle(user.getTitle().getName());

				EmailAddressesDto emailAddressesDto = new EmailAddressesDto();
				emailAddressesDto.setAddress(user.getEmail());
				emailAddressesDto.setData(dto);
				emailAddressesDtoList.add(emailAddressesDto);
			}
			emailDTO.setTo(emailAddressesDtoList);
		}
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}

		return emailDTO;
	}

	@Override
	public String getSessionUserCode() {
		return null;
	}

	/**
	 * @param required
	 */
	private void setHasUsersToApproveVariable(boolean required) {
		this.execution.setVariable(WorkflowVariablesConstants.HAS_USERS_TO_APPROVE, required);
	}

}
