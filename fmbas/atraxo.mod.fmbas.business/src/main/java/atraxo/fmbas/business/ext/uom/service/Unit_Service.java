/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.uom.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link Unit} domain entity.
 */
public class Unit_Service extends atraxo.fmbas.business.impl.uom.Unit_Service implements IUnitService {

	@Autowired
	private UnitConverterService unitConverterService;

	@Override
	public Unit getBaseUnit(UnitType indicator, BigDecimal factor) throws BusinessException {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("factor", factor);
			params.put("unittypeInd", indicator);
			return this.findEntityByAttributes(params);
		} catch (ApplicationException | IllegalStateException e) {
			throw new BusinessException(BusinessErrorCode.DUPLICATE_ENTITY,
					String.format(BusinessErrorCode.DUPLICATE_ENTITY.getErrMsg(), Unit.class.getSimpleName(), indicator + " " + factor.toString()),
					e);
		}
	}

	@Override
	public BigDecimal convert(Unit fromUnit, Unit toUnit, BigDecimal value, Double density) throws BusinessException {
		return this.unitConverterService.convert(fromUnit, toUnit, value, density);
	}

	@Override
	public BigDecimal convert(Unit fromUnit, Unit toUnit, BigDecimal value, Double density, Unit dFromUnit, Unit dToUnit) throws BusinessException {
		return this.unitConverterService.convert(fromUnit, toUnit, value, new Density(dFromUnit, dToUnit, density));
	}
}
