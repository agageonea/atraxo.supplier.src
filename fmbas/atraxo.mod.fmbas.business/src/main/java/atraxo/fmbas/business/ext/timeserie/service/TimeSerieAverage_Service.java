/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.timeserie.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.timeserie.ITimeSerieAverageService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesApprovalStatus;
import atraxo.fmbas.domain.impl.fmbas_type.TimeSeriesStatus;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieAverage;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link TimeSerieAverage} domain entity.
 */
public class TimeSerieAverage_Service extends atraxo.fmbas.business.impl.timeserie.TimeSerieAverage_Service implements ITimeSerieAverageService {

	@Autowired
	private ITimeSerieService srv;

	@Override
	protected void preUpdate(List<TimeSerieAverage> list) throws BusinessException {
		super.preUpdate(list);
		List<TimeSerieAverage> deactivatedAvgs = new ArrayList<>();
		for (TimeSerieAverage avg : list) {
			if (avg.getActive().equals(Boolean.FALSE)) {
				deactivatedAvgs.add(avg);
			}
		}
		if (!deactivatedAvgs.isEmpty()) {
			this.sendMessage("averageMethodsUsageChannel", deactivatedAvgs);
		}
	}

	@Override
	protected void postUpdate(List<TimeSerieAverage> list) throws BusinessException {

		super.postUpdate(list);
		Map<Integer, TimeSerie> timeSeriesMap = new HashMap<>();
		Set<TimeSerie> set = new HashSet<>();

		TimeSerie timeserie = list.get(0).getTserie();

		for (TimeSerieAverage timeSerieAverage : list) {

			timeserie = timeSerieAverage.getTserie();

			if (!CollectionUtils.isEmpty(timeserie.getTimeserieItems())) {
				timeSeriesMap.put(timeserie.getId(), timeserie);
			} else {
				set.add(timeserie);
			}
		}

		timeserie.setApprovalStatus(TimeSeriesApprovalStatus._NEW_);
		timeserie.setStatus(TimeSeriesStatus._WORK_);

		if (!set.isEmpty()) {
			this.srv.update(new ArrayList<>(set));
		} else {
			this.srv.update(timeserie);
		}

	}

	@Override
	@Transactional
	public void update(List<TimeSerieAverage> list) throws BusinessException {
		super.update(list);
	}

}
