/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.dashboard;

import atraxo.fmbas.domain.impl.dashboard.DashboardWidgets;
import atraxo.fmbas.domain.impl.dashboard.Dashboards;
import atraxo.fmbas.domain.impl.dashboard.Layouts;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Dashboards} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Dashboards_Service extends AbstractEntityService<Dashboards> {

	/**
	 * Public constructor for Dashboards_Service
	 */
	public Dashboards_Service() {
		super();
	}

	/**
	 * Public constructor for Dashboards_Service
	 * 
	 * @param em
	 */
	public Dashboards_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Dashboards> getEntityClass() {
		return Dashboards.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Dashboards
	 */
	public Dashboards findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Dashboards.NQ_FIND_BY_BUSINESS,
							Dashboards.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Dashboards", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Dashboards", "id"), nure);
		}
	}

	/**
	 * Find by reference: layout
	 *
	 * @param layout
	 * @return List<Dashboards>
	 */
	public List<Dashboards> findByLayout(Layouts layout) {
		return this.findByLayoutId(layout.getId());
	}
	/**
	 * Find by ID of reference: layout.id
	 *
	 * @param layoutId
	 * @return List<Dashboards>
	 */
	public List<Dashboards> findByLayoutId(Integer layoutId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Dashboards e where e.clientId = :clientId and e.layout.id = :layoutId",
						Dashboards.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("layoutId", layoutId).getResultList();
	}
	/**
	 * Find by reference: dashboardWidgets
	 *
	 * @param dashboardWidgets
	 * @return List<Dashboards>
	 */
	public List<Dashboards> findByDashboardWidgets(
			DashboardWidgets dashboardWidgets) {
		return this.findByDashboardWidgetsId(dashboardWidgets.getId());
	}
	/**
	 * Find by ID of reference: dashboardWidgets.id
	 *
	 * @param dashboardWidgetsId
	 * @return List<Dashboards>
	 */
	public List<Dashboards> findByDashboardWidgetsId(Integer dashboardWidgetsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Dashboards e, IN (e.dashboardWidgets) c where e.clientId = :clientId and c.id = :dashboardWidgetsId",
						Dashboards.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("dashboardWidgetsId", dashboardWidgetsId)
				.getResultList();
	}
}
