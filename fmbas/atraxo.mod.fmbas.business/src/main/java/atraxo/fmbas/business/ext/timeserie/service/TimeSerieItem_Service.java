/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.ext.timeserie.service;

import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import atraxo.fmbas.business.api.timeserie.ITimeSerieItemService;
import atraxo.fmbas.domain.impl.timeserie.TimeSerieItem;

public class TimeSerieItem_Service extends atraxo.fmbas.business.impl.timeserie.TimeSerieItem_Service implements ITimeSerieItemService {

	/**
	 * Find the next not calculated time serie item
	 *
	 * @param tsi
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public TimeSerieItem findNextNotCalculatedItem(TimeSerieItem tsi) throws BusinessException {

		String sql = "select e from " + TimeSerieItem.class.getSimpleName()
				+ " e where e.clientId=:clientId and e.itemDate > :itemDate and e.tserie.id = :serieId and e.calculated = false order by e.itemDate";

		List<TimeSerieItem> list = this.getEntityManager().createQuery(sql, TimeSerieItem.class)
				.setParameter("clientId", Session.user.get().getClient().getId()).setParameter("itemDate", tsi.getItemDate())
				.setParameter("serieId", tsi.getTserie().getId()).getResultList();
		if (list == null || list.size() == 0) {
			return null;
		}
		return list.get(0);
	}

	/**
	 * Find the previously not calculated item
	 *
	 * @param tsi
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public TimeSerieItem findPrevNotCalculatedItem(TimeSerieItem tsi) throws BusinessException {

		String sql = "select e from " + TimeSerieItem.class.getSimpleName()
				+ " e where e.clientId=:clientId and e.itemDate < :itemDate and e.tserie.id = :serieId and e.calculated = false order by e.itemDate";

		List<TimeSerieItem> list = this.getEntityManager().createQuery(sql, TimeSerieItem.class)
				.setParameter("clientId", Session.user.get().getClient().getId()).setParameter("itemDate", tsi.getItemDate())
				.setParameter("serieId", tsi.getTserie().getId()).getResultList();

		if (list == null || list.size() == 0) {
			return null;
		}

		return list.get(list.size() - 1);

	}

	/**
	 * @param tsiFirst
	 * @param tsiLast
	 * @param serieId
	 * @param calculated
	 * @return
	 */
	@Override
	public List<TimeSerieItem> getAllElementsBetweenDatesWithMargins(Date tsiFirst, Date tsiLast, Integer serieId, Boolean calculated) {

		if (tsiFirst == null || tsiLast == null) {
			return null;
		}

		// eliminate non-calculated items because they will be deleted on delete
		// action
		// this function are called on pre-delete event
		String sql = "select e from TimeSerieItem "
				+ " e where e.clientId =:clientId and e.tserie.id = :serieId and e.active = true and e.itemDate >= :itemFirst and e.itemDate <= :itemLast";
		if (calculated != null) {
			sql += " and e.calculated = :calculated";
		}

		sql += " order by e.itemDate";
		TypedQuery<TimeSerieItem> query = this.getEntityManager().createQuery(sql, TimeSerieItem.class)
				.setParameter("itemFirst", tsiFirst, TemporalType.DATE).setParameter("itemLast", tsiLast, TemporalType.DATE)
				.setParameter("serieId", serieId).setParameter("clientId", Session.user.get().getClient().getId());

		if (calculated != null) {
			query.setParameter("calculated", calculated);
		}
		List<TimeSerieItem> list = query.getResultList();

		return list;
	}
}
