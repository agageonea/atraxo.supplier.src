package atraxo.fmbas.business.ext.sys.delegate;

import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class SystemParameterDelegate extends AbstractBusinessDelegate {

	public Density getSystemDensity() throws BusinessException {
		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		IUnitService unitSrv = (IUnitService) this.findEntityService(Unit.class);
		Unit sysVol = unitSrv.findByCode(paramSrv.getSysVol());
		Unit sysUnit = unitSrv.findByCode(paramSrv.getSysWeight());
		return new Density(sysUnit, sysVol, Double.parseDouble(paramSrv.getDensity()));
	}

}
