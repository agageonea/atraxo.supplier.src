package atraxo.fmbas.business.ext.timeserie.job.importPlatts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import atraxo.fmbas.domain.ext.platts.PlattsData;
import atraxo.fmbas.domain.ext.platts.PlattsHeader;
import atraxo.fmbas.domain.ext.platts.Symbol;

public class PlattsItemProcessor implements ItemProcessor<Object, PlattsData> {

	private static final Logger logger = LoggerFactory.getLogger(PlattsItemProcessor.class);

	@Override
	public PlattsData process(Object item) throws Exception {
		if (item instanceof PlattsHeader) {
			if (!((PlattsHeader) item).getType().equalsIgnoreCase("FINAL")) {
				throw new RuntimeException("Source file is not final, data has not been imported.");
			}
		}
		if (item instanceof PlattsData) {
			PlattsData data = (PlattsData) item;
			if (data.getClassification().equals(Symbol.F.name()) || data.getClassification().equals(Symbol.X.name())) {
				logger.warn("Data has been filtered by import process: " + data.toString());
				return null;
			}
			return data;
		}
		logger.warn("Header has been filtered by import process: " + item.toString());
		return null;
	}
}
