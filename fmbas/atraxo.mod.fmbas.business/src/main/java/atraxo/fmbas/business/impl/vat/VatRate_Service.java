/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.fmbas.business.impl.vat;

import atraxo.fmbas.domain.impl.vat.Vat;
import atraxo.fmbas.domain.impl.vat.VatRate;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link VatRate} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class VatRate_Service extends AbstractEntityService<VatRate> {

	/**
	 * Public constructor for VatRate_Service
	 */
	public VatRate_Service() {
		super();
	}

	/**
	 * Public constructor for VatRate_Service
	 * 
	 * @param em
	 */
	public VatRate_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<VatRate> getEntityClass() {
		return VatRate.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return VatRate
	 */
	public VatRate findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(VatRate.NQ_FIND_BY_BUSINESS,
							VatRate.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"VatRate", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"VatRate", "id"), nure);
		}
	}

	/**
	 * Find by reference: vat
	 *
	 * @param vat
	 * @return List<VatRate>
	 */
	public List<VatRate> findByVat(Vat vat) {
		return this.findByVatId(vat.getId());
	}
	/**
	 * Find by ID of reference: vat.id
	 *
	 * @param vatId
	 * @return List<VatRate>
	 */
	public List<VatRate> findByVatId(Integer vatId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from VatRate e where e.clientId = :clientId and e.vat.id = :vatId",
						VatRate.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("vatId", vatId).getResultList();
	}
}
