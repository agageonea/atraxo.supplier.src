Ext.override(atraxo.fmbas.i18n.ds.RoleSuppActive_Ds, {
	withPrivilegeId__lbl: "With privilege ID",
	withPrivilege__lbl: "With privilege",
	withUserId__lbl: "With user ID",
	withUser__lbl: "With user"
});
