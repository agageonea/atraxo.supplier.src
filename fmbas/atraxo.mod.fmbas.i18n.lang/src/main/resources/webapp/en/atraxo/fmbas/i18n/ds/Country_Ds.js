Ext.override(atraxo.fmbas.i18n.ds.Country_Ds, {
	active__lbl: "Active?",
	active__tlp: "Active?",
	code__lbl: "Code",
	code__tlp: "Country code",
	continent__lbl: "Continent",
	continent__tlp: "Continent",
	countryCode__lbl: "Country reference",
	countryCode__tlp: "Country reference",
	countryId__lbl: "Country (ID)",
	name__lbl: "Name",
	name__tlp: "Name of the country",
	text__lbl: ""
});
