Ext.override(atraxo.fmbas.i18n.ds.VatLov_Ds, {
	code__lbl: "VAT code",
	code__tlp: "VAT code as per country of origin: TVA, GST, etc",
	countryCode__lbl: "Country",
	countryCode__tlp: "Code",
	countryName__lbl: "Country",
	countryName__tlp: "Name"
});
