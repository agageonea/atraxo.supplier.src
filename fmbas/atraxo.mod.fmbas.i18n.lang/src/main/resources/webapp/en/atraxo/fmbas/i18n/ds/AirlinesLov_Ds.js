Ext.override(atraxo.fmbas.i18n.ds.AirlinesLov_Ds, {
	code__lbl: "Account code",
	code__tlp: "Account code",
	id__lbl: "ID",
	name__lbl: "Name",
	name__tlp: "Name",
	natureOfBusiness__lbl: "Nature of business",
	natureOfBusiness__tlp: "Nature of business"
});
