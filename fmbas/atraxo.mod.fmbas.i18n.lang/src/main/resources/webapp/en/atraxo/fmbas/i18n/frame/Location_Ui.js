Ext.override(atraxo.fmbas.i18n.frame.Location_Ui, {
	/* view */
	airportInfList__ttl: "Airport Information",
	contactWdw__ttl: "Contact",
	contactsList__ttl: "Contacts",
	notamList__ttl: "NOTAM",
	noteWdw__ttl: "Note",
	notesList__ttl: "Notes",
	taxesFeesList__ttl: "Taxes & Fees",
	wdwLocation__ttl: "Locations",
	/* menu */
	/* button */
	btnCloseContactWdw__lbl: "Cancel",
	btnCloseContactWdw__tlp: "Cancel",
	btnCloseNoteWdw__lbl: "Cancel",
	btnCloseNoteWdw__tlp: "Cancel",
	btnOnEditWdwClose__lbl: "Cancel",
	btnOnEditWdwClose__tlp: "Cancel",
	btnSaveCloseContactWdw__lbl: "Save & Close",
	btnSaveCloseContactWdw__tlp: "Save & Close",
	btnSaveCloseNoteWdw__lbl: "Save & Close",
	btnSaveCloseNoteWdw__tlp: "Save & Close",
	btnSaveCloseWdw__lbl: "Save & Close",
	btnSaveCloseWdw__tlp: "Save & Close",
	btnSaveEditWdw__lbl: "Save & Edit",
	btnSaveEditWdw__tlp: "Save & Edit",
	btnSaveNewContactWdw__lbl: "Save & New",
	btnSaveNewContactWdw__tlp: "Save & New",
	btnSaveNewWdw__lbl: "Save & New",
	btnSaveNewWdw__tlp: "Save & New",
	helpWdw__lbl: "Help",
	helpWdw__tlp: "Help",
	helpWdw1__lbl: "Help",
	helpWdw1__tlp: "Help",
	
	title: "Manage Locations"
});
