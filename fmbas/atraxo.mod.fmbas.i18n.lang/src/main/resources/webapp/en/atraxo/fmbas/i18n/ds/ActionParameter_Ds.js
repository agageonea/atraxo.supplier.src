Ext.override(atraxo.fmbas.i18n.ds.ActionParameter_Ds, {
	actionId__lbl: "Job action (ID)",
	actionName__lbl: "Job action",
	actionOrder__lbl: "Order",
	assignType__lbl: "Assign type",
	businessValue__lbl: "Business value",
	customerId__lbl: "",
	defaultValue__lbl: "Default value",
	description__lbl: "Description",
	filePath__lbl: "",
	jobUID__lbl: "Generated action ID",
	jobUID__tlp: "Generated action ID",
	name__lbl: "Name",
	readOnly__lbl: "Read only",
	refBusinessValues__lbl: "Reference business values",
	refValues__lbl: "Possible values",
	subsidiaryId__lbl: "",
	type__lbl: "Type",
	value__lbl: "Value"
});
