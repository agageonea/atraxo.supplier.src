Ext.override(atraxo.fmbas.i18n.frame.PriceCategories_Ui, {
	/* view */
	wdwPriceCategory__ttl: "New Price Category",
	/* menu */
	/* button */
	btnClosePriceWdw__lbl: "Cancel",
	btnClosePriceWdw__tlp: "Cancel",
	btnSaveClosePriceWdw__lbl: "Save & Close",
	btnSaveClosePriceWdw__tlp: "Save & Close",
	btnSaveNewWdw__lbl: "Save & New",
	btnSaveNewWdw__tlp: "Save & New",
	helpWdw__lbl: "Help",
	helpWdw__tlp: "Help",
	
	title: "Price Categories"
});
