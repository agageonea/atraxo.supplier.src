Ext.override(atraxo.fmbas.i18n.ds.ActionResultHistory_Ds, {
	actionName__lbl: "Action name",
	duration__lbl: "Duration",
	endTime__lbl: "Finished at",
	exitCode__lbl: "Result",
	exitMessage__lbl: "Exit message",
	jobExId__lbl: "Job execution (ID)",
	msg__lbl: "Result message",
	name__lbl: "Key name",
	stacktrace__lbl: "",
	startTime__lbl: "Started at",
	status__lbl: "Status",
	value__lbl: "Long value"
});
