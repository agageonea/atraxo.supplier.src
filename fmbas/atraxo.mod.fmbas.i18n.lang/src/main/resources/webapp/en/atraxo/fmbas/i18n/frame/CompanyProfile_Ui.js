Ext.override(atraxo.fmbas.i18n.frame.CompanyProfile_Ui, {
	/* view */
	AccountingRules__ttl: "Accounting Rules",
	BankAccount__ttl: "Bank Accounts",
	List__ttl: "Subsidiary",
	customerAsgnWdw__ttl: "Assign Customers",
	wdwEdit__ttl: "Edit Subsidiary Company",
	wdwNew__ttl: "New Subsidiary Company",
	/* menu */
	/* button */
	btnCancelAll__lbl: "Cancel all",
	btnCancelAll__tlp: "Cancel all",
	btnCancelAllRules__lbl: "Cancel all",
	btnCancelAllRules__tlp: "Cancel all",
	btnCancelSelected__lbl: "Cancel",
	btnCancelSelected__tlp: "Cancel",
	btnCancelSelectedRule__lbl: "Cancel",
	btnCancelSelectedRule__tlp: "Cancel",
	btnCancelWithMenu__lbl: "Cancel",
	btnCancelWithMenu__tlp: "Either abort all changes, or only the changes of the selected line.",
	btnCancelWithMenuRules__lbl: "Cancel",
	btnCancelWithMenuRules__tlp: "Either abort all changes, or only the changes of the selected line",
	btnOnEditWdwClose__lbl: "Cancel",
	btnOnEditWdwClose__tlp: "Cancel",
	btnOnNewWdwClose__lbl: "Cancel",
	btnOnNewWdwClose__tlp: "Cancel",
	btnSaveCloseEditWdw__lbl: "Save & Close",
	btnSaveCloseEditWdw__tlp: "Save & Close",
	btnSaveCloseWdw__lbl: "Save & Close",
	btnSaveCloseWdw__tlp: "Save & Close",
	btnSaveNewWdw__lbl: "Save & New",
	btnSaveNewWdw__tlp: "Save & New",
	helpWdw__lbl: "Help",
	helpWdw__tlp: "Help",
	
	title: "Organization Profile"
});
