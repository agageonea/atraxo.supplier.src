Ext.override(atraxo.fmbas.i18n.ds.ExternalInterfaceMessageHistory_Ds, {
	executionTime__lbl: "Execution time",
	externalInterfaceId__lbl: "External Interface (ID)",
	externalInterfaceName__lbl: "External interface",
	externalInterfaceName__tlp: "Name",
	messageContent__lbl: "Message content",
	messageId__lbl: "Message ID",
	objectId__lbl: "Object ID",
	objectRefId__lbl: "Object Ref ID",
	objectType__lbl: "Object type",
	responseMessageId__lbl: "Response message ID",
	responseMessageTime__lbl: "Response message time",
	status__lbl: "Status",
	status__tlp: "Indicates the status of the message "
});
