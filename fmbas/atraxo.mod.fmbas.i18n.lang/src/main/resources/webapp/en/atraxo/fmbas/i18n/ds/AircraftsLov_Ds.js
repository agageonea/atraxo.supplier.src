Ext.override(atraxo.fmbas.i18n.ds.AircraftsLov_Ds, {
	acTypeCode__lbl: "Aircraft type",
	acTypeCode__tlp: "IATA code",
	acTypeId__lbl: "AC types (ID)",
	custCode__lbl: "Customer",
	custCode__tlp: "Account code",
	custId__lbl: "Customer (ID)",
	registration__lbl: "Aircraft",
	registration__tlp: "Aircraft registration number",
	validFrom__lbl: "Valid from",
	validFrom__tlp: "Valid from",
	validTo__lbl: "Valid to",
	validTo__tlp: "Valid to"
});
