
Ext.override(atraxo.fmbas.i18n.dc.AccountingRules_Dc$AccountingRulesList, {
	ruleType__lbl: "Rule type",
	subsidiary__lbl: "For subsidiary"
});

Ext.override(atraxo.fmbas.i18n.dc.AccountingRules_Dc$Filter, {
	ruleType__lbl: "Rule type",
	subsidiary__lbl: "Subsidiary"
});
