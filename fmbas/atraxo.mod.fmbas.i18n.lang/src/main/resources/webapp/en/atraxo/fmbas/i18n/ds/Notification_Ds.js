Ext.override(atraxo.fmbas.i18n.ds.Notification_Ds, {
	action__lbl: "Action",
	category__lbl: "Category",
	descripion__lbl: "Description",
	eventDate__lbl: "Timestamp",
	lifeTime__lbl: "Life time",
	link__lbl: "Link",
	methodName__lbl: "Method name",
	methodParam__lbl: "Method param",
	priority__lbl: "Priority",
	separator__lbl: "<hr style='height:1px; border:none; color:#D0D0D0; background-color:#D0D0D0;'>",
	title__lbl: "Title",
	userCode__lbl: ""
});
