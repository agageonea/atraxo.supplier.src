
Ext.override(atraxo.fmbas.i18n.dc.EnergyPrice_Dc$Edit, {
	currency__lbl: "Currency/Unit"
});

Ext.override(atraxo.fmbas.i18n.dc.EnergyPrice_Dc$List, {
	currency__lbl: "Currency",
	unit__lbl: "Unit"
});
