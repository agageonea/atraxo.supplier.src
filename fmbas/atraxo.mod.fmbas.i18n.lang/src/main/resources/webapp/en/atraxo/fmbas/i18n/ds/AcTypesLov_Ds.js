Ext.override(atraxo.fmbas.i18n.ds.AcTypesLov_Ds, {
	active__lbl: "Active?",
	active__tlp: "Active?",
	code__lbl: "IATA code",
	code__tlp: "IATA code",
	name__lbl: "Description",
	name__tlp: "Description",
	tankCapacity__lbl: "Tank capacity",
	tankCapacity__tlp: "Tank capacity"
});
