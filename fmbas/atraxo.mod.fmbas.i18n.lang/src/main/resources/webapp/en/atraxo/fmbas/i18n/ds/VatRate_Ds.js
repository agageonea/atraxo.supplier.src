Ext.override(atraxo.fmbas.i18n.ds.VatRate_Ds, {
	note__lbl: "Note",
	note__tlp: "A note for the rate",
	rate__lbl: "Rate value",
	rate__tlp: "The rate for the VAT",
	validFrom__lbl: "Valid from",
	validFrom__tlp: "Starting date of the rate validity",
	validTo__lbl: "Valid to",
	validTo__tlp: "End date for the rate validity",
	vatCode__lbl: "VAT",
	vatCode__tlp: "VAT code as per country of origin: TVA, GST, etc",
	vatId__lbl: "VAT (ID)"
});
