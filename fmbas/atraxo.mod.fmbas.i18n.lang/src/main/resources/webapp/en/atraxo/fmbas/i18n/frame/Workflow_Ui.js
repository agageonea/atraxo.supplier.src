Ext.override(atraxo.fmbas.i18n.frame.Workflow_Ui, {
	/* view */
	SelectSubsidiaryWdw__ttl: "Copy Workflow",
	rolesAsgnWdw__ttl: "Assign Roles",
	workflowInstancesList__ttl: "Workflow Instances",
	workflowNotification__ttl: "User Notifications ",
	workflowParameterList__ttl: "Parameters",
	/* menu */
	/* button */
	btnAddNotification__lbl: "Add",
	btnAddNotification__tlp: "Add",
	btnCancelCopy__lbl: "Cancel",
	btnCancelCopy__tlp: "Cancel",
	btnCancelNotification__lbl: "Cancel",
	btnCancelNotification__tlp: "Cancel",
	btnContinueCopy__lbl: "Continue",
	btnContinueCopy__tlp: "Continue",
	btnCopyWorkflow__lbl: "Copy workflow",
	btnCopyWorkflow__tlp: "Copy workflow",
	btnDeployWkfRpc__lbl: "Deploy workflow",
	btnDeployWkfRpc__tlp: "Deploy workflow",
	btnRemoveNotification__lbl: "Remove",
	btnRemoveNotification__tlp: "Remove",
	btnResumeWkfInstanceRpc__lbl: "Resume instance",
	btnResumeWkfInstanceRpc__tlp: "Resume instance",
	btnSaveNotification__lbl: "Save",
	btnSaveNotification__tlp: "Save",
	btnTerminateWkfAllInstance__lbl: "All",
	btnTerminateWkfAllInstance__tlp: "Terminate all workflow instances",
	btnTerminateWkfInstanceMenu__lbl: "Terminate instance",
	btnTerminateWkfInstanceMenu__tlp: "Terminate workflow instances",
	btnTerminateWkfSelectedInstance__lbl: "Selected",
	btnTerminateWkfSelectedInstance__tlp: "Terminate selected workflow instances",
	helpWdw__lbl: "Help",
	helpWdw__tlp: "Help",
	
	title: "Workflows"
});
