Ext.override(atraxo.fmbas.i18n.ds.Note_Ds, {
	createdAfter__lbl: "Created after",
	createdBefore__lbl: "Created before",
	notes__lbl: "Note",
	notes__tlp: "Note",
	objectId__lbl: "Object ID",
	objectType__lbl: "Object type"
});
