Ext.override(atraxo.fmbas.i18n.ds.WorkflowParameter_Ds, {
	defaultValue__lbl: "Default value",
	description__lbl: "Description",
	description__tlp: "Description",
	mandatory__lbl: "Mandatory?",
	name__lbl: "Name",
	name__tlp: "Name",
	paramValue__lbl: "Parameter value",
	refValues__lbl: "Reference values",
	refValues__tlp: "Editable",
	type__lbl: "Parameter type",
	workflowCode__lbl: "Workflow",
	workflowCode__tlp: "Workflow code",
	workflowId__lbl: "Workflow (ID)",
	xxx__lbl: "Xxx"
});
