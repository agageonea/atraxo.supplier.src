
Ext.override(atraxo.fmbas.i18n.dc.CustomerCreditLine_Dc$Edit, {
	activeLabel__lbl: "Active?",
	alertLimitLabel__lbl: "Alert limit",
	availabilityLabel__lbl: "Available credit",
	creditLimitLabel__lbl: "Credit limit",
	creditTypeLabel__lbl: "Credit type",
	validFromLabel__lbl: "Valid from",
	validToLabel__lbl: "Valid to"
});

Ext.override(atraxo.fmbas.i18n.dc.CustomerCreditLine_Dc$EditContext, {
	activeLabel__lbl: "Active?",
	alertLimitLabel__lbl: "Alert limit",
	availabilityLabel__lbl: "Available credit",
	creditLimitLabel__lbl: "Credit limit",
	creditTypeLabel__lbl: "Credit type",
	validFromLabel__lbl: "Valid from",
	validToLabel__lbl: "Valid to"
});
