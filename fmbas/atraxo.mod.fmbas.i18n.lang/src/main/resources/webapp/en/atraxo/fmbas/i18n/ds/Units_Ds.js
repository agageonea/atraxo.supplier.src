Ext.override(atraxo.fmbas.i18n.ds.Units_Ds, {
	active__lbl: "Active?",
	active__tlp: "Active?",
	code__lbl: "Code",
	code__tlp: "Code",
	factor__lbl: "Factor",
	factor__tlp: "Factor",
	iataCode__lbl: "IATA code",
	iataCode__tlp: "IATA code",
	id__lbl: "ID",
	name__lbl: "Name",
	name__tlp: "Name",
	unittypeInd__lbl: "Unit type indicator",
	unittypeInd__tlp: "Unit type indicator"
});
