Ext.override(atraxo.fmbas.i18n.ds.CustomerLocations_Ds, {
	code__lbl: "Code",
	code__tlp: "Code",
	countryCode__lbl: "Country",
	countryCode__tlp: "Code",
	countryId__lbl: "Country (ID)",
	countryName__lbl: "Country",
	countryName__tlp: "Name",
	customerId__lbl: "Customer ID",
	iataCode__lbl: "IATA code",
	iataCode__tlp: "IATA code",
	icaoCode__lbl: "ICAO code",
	icaoCode__tlp: "ICAO code",
	isAirport__lbl: "Airport?",
	isAirport__tlp: "Airport?",
	name__lbl: "Name",
	name__tlp: "Name"
});
