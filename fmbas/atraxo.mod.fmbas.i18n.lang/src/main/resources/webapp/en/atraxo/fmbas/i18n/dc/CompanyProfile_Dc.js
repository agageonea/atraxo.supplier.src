
Ext.override(atraxo.fmbas.i18n.dc.CompanyProfile_Dc$Form, {
	codeLabel__lbl: "Code",
	currLabel__lbl: "System currency",
	fsLabel__lbl: "Default financial source",
	nameLabel__lbl: "Organization name",
	unitLabel__lbl: "System of units"
});
