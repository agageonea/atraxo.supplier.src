Ext.override(atraxo.fmbas.i18n.ds.ExternalInterfaceParameters_Ds, {
	currentValue__lbl: "Current value",
	defaultValue__lbl: "Default value",
	description__lbl: "Description",
	description__tlp: "Description",
	externalInterfaceId__lbl: "External Interface (ID)",
	externalInterfaceName__lbl: "External interface",
	externalInterfaceName__tlp: "Name",
	name__lbl: "Name",
	name__tlp: "Name",
	readOnly__lbl: "Read only?",
	refBusinessValues__lbl: "Reference business values",
	refValues__lbl: "Possible values",
	type__lbl: "Parameter type"
});
