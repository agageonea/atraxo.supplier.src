Ext.override(atraxo.fmbas.i18n.ds.MainCategories_Ds, {
	active__lbl: "Active?",
	active__tlp: "Active?",
	code__lbl: "Code",
	code__tlp: "Code",
	isProd__lbl: "Category is product",
	isProd__tlp: "Category is product",
	isSys__lbl: "System main categories",
	isSys__tlp: "System main categories",
	name__lbl: "Name",
	name__tlp: "Name"
});
