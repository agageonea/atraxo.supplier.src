Ext.override(atraxo.fmbas.i18n.ds.ExchangeRateToFinancialSourceAndAvgFiltered_Ds, {
	avgMthdCode__lbl: "Averaging method",
	avgMthdCode__tlp: "Code",
	avgMthdId__lbl: "Averaging Method Indicator (ID)",
	avgMthdName__lbl: "Averaging method",
	avgMthdName__tlp: "Name",
	currencyId__lbl: "",
	financialSourceCode__lbl: "Financial source",
	financialSourceCode__tlp: "Code",
	financialSourceId__lbl: "Finsource (ID)",
	financialSourceName__lbl: "Financial source",
	financialSourceName__tlp: "Name",
	fromCurrencyCode__lbl: "Currency1",
	fromCurrencyCode__tlp: "Currency code",
	fromCurrencyId__lbl: "Currency1 (ID)",
	name__lbl: "Name",
	name__tlp: "Name",
	toCurrencyCode__lbl: "Currency2",
	toCurrencyCode__tlp: "Currency code",
	toCurrencyId__lbl: "Currency2 (ID)"
});
