Ext.override(atraxo.fmbas.i18n.ds.LocationsLov_Ds, {
	code__lbl: "Code",
	code__tlp: "Code",
	iataCode__lbl: "IATA code",
	iataCode__tlp: "IATA code",
	icaoCode__lbl: "ICAO code",
	icaoCode__tlp: "ICAO code",
	isAirport__lbl: "Is airport?",
	isAirport__tlp: "Is airport?",
	name__lbl: "Name",
	name__tlp: "Name",
	validFrom__lbl: "Valid from",
	validFrom__tlp: "Valid from",
	validTo__lbl: "Valid to",
	validTo__tlp: "Valid to"
});
