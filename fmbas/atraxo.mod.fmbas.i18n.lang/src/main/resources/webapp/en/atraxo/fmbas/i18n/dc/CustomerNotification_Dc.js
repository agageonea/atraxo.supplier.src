
Ext.override(atraxo.fmbas.i18n.dc.CustomerNotification_Dc$Filter, {
	assignedArea__lbl: "Area"
});

Ext.override(atraxo.fmbas.i18n.dc.CustomerNotification_Dc$List, {
	assignedArea__lbl: "Area",
	emailBody__lbl: "Email template",
	notificationType__lbl: "Notification type",
	templateName__lbl: "Document template"
});
