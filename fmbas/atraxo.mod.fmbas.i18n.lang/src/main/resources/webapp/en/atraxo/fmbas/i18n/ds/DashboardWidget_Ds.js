Ext.override(atraxo.fmbas.i18n.ds.DashboardWidget_Ds, {
	dashboardId__lbl: "Dashboard (ID)",
	dashboardName__lbl: "Dashboard",
	dashboardName__tlp: "Name",
	id__lbl: "ID",
	layoutId__lbl: "Layout (ID)",
	layoutName__lbl: "Layout",
	layoutName__tlp: "Name",
	layoutRegionId__lbl: "Layout region",
	layoutRegionId__tlp: "Layout region",
	rpcResponse__lbl: "Rpc Response",
	widgetDs__lbl: "Data store name",
	widgetId__lbl: "Widget (ID)",
	widgetIndex__lbl: "Widget index",
	widgetIndex__tlp: "Widget index",
	widgetMethod__lbl: "Method name",
	widgetName__lbl: "Name",
	widgetName__tlp: "Name"
});
