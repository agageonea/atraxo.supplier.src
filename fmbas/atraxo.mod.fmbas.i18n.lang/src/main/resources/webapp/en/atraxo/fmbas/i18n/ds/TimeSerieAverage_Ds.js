Ext.override(atraxo.fmbas.i18n.ds.TimeSerieAverage_Ds, {
	active__lbl: "Active?",
	active__tlp: "Active?",
	avgActive__lbl: "Active?",
	avgActive__tlp: "Active?",
	avgCode__lbl: "Code",
	avgCode__tlp: "Code",
	avgName__lbl: "Description",
	avgName__tlp: "Description",
	effectiveOffsetDay__lbl: "Effective offset day",
	effectiveOffsetDay__tlp: "Effective offset day",
	effectiveOffsetInterval__lbl: "Effective offset interval",
	effectiveOffsetInterval__tlp: "Effective offset interval",
	tserieId__lbl: "Tserie (ID)"
});
