Ext.override(atraxo.fmbas.i18n.ds.JobChainUsers_Ds, {
	enabled__lbl: "Enabled",
	enabled__tlp: "Enabled?",
	jobChainId__lbl: "Job Chain (ID)",
	jobChainName__lbl: "Job chain",
	loginName__lbl: "Login name",
	loginName__tlp: "Login name",
	notificationCondition__lbl: "Condition",
	notificationCondition__tlp: "Condition",
	notificationType__lbl: "Type",
	notificationType__tlp: "Type",
	userEmail__lbl: "Email address",
	userEmail__tlp: "Email",
	userId__lbl: "User (ID)",
	userName__lbl: "Notification receiver"
});
