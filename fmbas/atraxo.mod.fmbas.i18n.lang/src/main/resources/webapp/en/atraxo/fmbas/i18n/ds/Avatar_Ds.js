Ext.override(atraxo.fmbas.i18n.ds.Avatar_Ds, {
	active__lbl: "Active?",
	baseUrl__lbl: "Base Url",
	contentType__lbl: "Content type",
	fileName__lbl: "File name",
	filePath__lbl: "",
	location__lbl: "Location",
	loginName__lbl: "Login name",
	name__lbl: "Name",
	typeId__lbl: "Type (ID)",
	type__lbl: "Type",
	userId__lbl: "User (ID)",
	userName__lbl: "User name"
});
