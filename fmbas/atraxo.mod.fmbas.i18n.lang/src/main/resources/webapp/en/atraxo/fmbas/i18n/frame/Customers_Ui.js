Ext.override(atraxo.fmbas.i18n.frame.Customers_Ui, {
	/* view */
	HistoryList__ttl: "History",
	aircraftsList__ttl: "Fleet",
	attachmentList__ttl: "Documents",
	contactTab__ttl: "Contact Details",
	contactWdw__ttl: "Contact",
	contactsList__ttl: "Contacts",
	creditLineHistoryList__ttl: "History of Changes",
	creditLineHistoryWdw__ttl: "Remark",
	creditLineList__ttl: "Credit Terms",
	creditLineWdw__ttl: "Credit Term",
	customerAddresses__ttl: "Addresses",
	finalCustList__ttl: "Final Customers",
	finalCustomerEditWdw__ttl: "Edit Final Customer",
	finalCustomerWdw__ttl: "New Final Customer",
	masterAgreementHistoryWdw__ttl: "Remark",
	masterAgreementWdw__ttl: "Master Agreement",
	masterAgrementsList__ttl: "Master Agreements",
	neList__ttl: "Notifications",
	noteWdw__ttl: "Note",
	notesList__ttl: "Notes",
	notificationList__ttl: "Notifications",
	tradeRefList__ttl: "Trade References",
	tradeRefWdw__ttl: "Trade Reference",
	wdwBlockReason__ttl: "Remark",
	wdwCustomer__ttl: "New Customer Account",
	wdwNotifAssign__ttl: "Select Notifications",
	/* menu */
	/* button */
	assignCustomers__lbl: "Assign",
	assignCustomers__tlp: "Assign final customer to customer",
	btnApprove__lbl: "Activate",
	btnApprove__tlp: "Approve",
	btnApproveEdit__lbl: "Activate",
	btnApproveEdit__tlp: "Approve",
	btnAssignContactNotif__lbl: "Assign",
	btnAssignContactNotif__tlp: "Assign",
	btnAssignNotifEvents__lbl: "Assign",
	btnAssignNotifEvents__tlp: "Assign",
	btnBlock__lbl: "Deactivate",
	btnBlock__tlp: "Block",
	btnBlockCancel__lbl: "Cancel",
	btnBlockCancel__tlp: "Cancel",
	btnBlockEdit__lbl: "Deactivate",
	btnBlockEdit__tlp: "Block",
	btnCancelAssign__lbl: "Cancel",
	btnCancelAssign__tlp: "Cancel",
	btnCloseContactWdw__lbl: "Cancel",
	btnCloseContactWdw__tlp: "Cancel",
	btnCloseCreditWdw__lbl: "Cancel",
	btnCloseCreditWdw__tlp: "Cancel",
	btnCloseMasterAgreementWdw__lbl: "Cancel",
	btnCloseMasterAgreementWdw__tlp: "Cancel",
	btnCloseNoteWdw__lbl: "Cancel",
	btnCloseNoteWdw__tlp: "Cancel",
	btnCloseTradeRefWdw__lbl: "Cancel",
	btnCloseTradeRefWdw__tlp: "Cancel",
	btnExportFailed__lbl: "Failed",
	btnExportFailed__tlp: "Resend customer accounts that have failed to be transmitted to financial system",
	btnExportSelected__lbl: "Selected",
	btnExportSelected__tlp: "Resend selected customer accounts to financial system",
	btnExportWithMenu__lbl: "Resend",
	btnExportWithMenu__tlp: "Resend customer accounts to financial system",
	btnFinalCustWdwClose__lbl: "Cancel",
	btnFinalCustWdwClose__tlp: "Cancel",
	btnFinalCustWdwEditClose__lbl: "Cancel",
	btnFinalCustWdwEditClose__tlp: "Cancel",
	btnOnEditWdwClose__lbl: "Cancel",
	btnOnEditWdwClose__tlp: "Cancel",
	btnPromote__lbl: "Promote",
	btnPromote__tlp: "Promote final customer to customer",
	btnRemoveAssign__lbl: "Remove",
	btnRemoveAssign__tlp: "Remove final customer link to this customer",
	btnRemoveNotifEvents__lbl: "Remove",
	btnRemoveNotifEvents__tlp: "Remove",
	btnRequestCreditInfo__lbl: "Request credit info",
	btnRequestCreditInfo__tlp: "Request credit info",
	btnSaveCloseBlockHistoryWdw__lbl: "Save",
	btnSaveCloseBlockHistoryWdw__tlp: "Save",
	btnSaveCloseCLHistoryWdw__lbl: "Save",
	btnSaveCloseCLHistoryWdw__tlp: "Save",
	btnSaveCloseContactWdw__lbl: "Save & Close",
	btnSaveCloseContactWdw__tlp: "Save & Close",
	btnSaveCloseCreditWdw__lbl: "Save & Close",
	btnSaveCloseCreditWdw__tlp: "Save & Close",
	btnSaveCloseFinalCustEditWdw__lbl: "Save & Close",
	btnSaveCloseFinalCustEditWdw__tlp: "Save & Close",
	btnSaveCloseFinalCustWdw__lbl: "Save & Close",
	btnSaveCloseFinalCustWdw__tlp: "Save & Close",
	btnSaveCloseHistoryWdw__lbl: "Save",
	btnSaveCloseHistoryWdw__tlp: "Save",
	btnSaveCloseMasterAgreementWdw__lbl: "Save & Close",
	btnSaveCloseMasterAgreementWdw__tlp: "Save & Close",
	btnSaveCloseNoteWdw__lbl: "Save & Close",
	btnSaveCloseNoteWdw__tlp: "Save & Close",
	btnSaveCloseTradeRefWdw__lbl: "Save & Close",
	btnSaveCloseTradeRefWdw__tlp: "Save & Close",
	btnSaveCloseWdw__lbl: "Save & Close",
	btnSaveCloseWdw__tlp: "Save & Close",
	btnSaveCreditLine__lbl: "Save",
	btnSaveCreditLine__tlp: "Save",
	btnSaveEditWdw__lbl: "Save & Edit",
	btnSaveEditWdw__tlp: "Save & Edit",
	btnSaveNewContactWdw__lbl: "Save & New",
	btnSaveNewContactWdw__tlp: "Save & New",
	btnSaveNewCreditWdw__lbl: "Save & New",
	btnSaveNewCreditWdw__tlp: "Save & New",
	btnSaveNewFinalCustWdw__lbl: "Save & New",
	btnSaveNewFinalCustWdw__tlp: "Save & New",
	btnSaveNewMasterAgreementWdw__lbl: "Save & New",
	btnSaveNewMasterAgreementWdw__tlp: "Save & New",
	btnSaveNewTradeRefWdw__lbl: "Save & New",
	btnSaveNewTradeRefWdw__tlp: "Save & New",
	btnSaveNewWdw__lbl: "Save & New",
	btnSaveNewWdw__tlp: "Save & New",
	btnStatusCancel__lbl: "Cancel",
	btnStatusCancel__tlp: "Cancel",
	helpWdw__lbl: "Help",
	helpWdw__tlp: "Help",
	helpWdwEditor__lbl: "Help",
	helpWdwEditor__tlp: "Help",
	
	title: "Customer Accounts"
});
