Ext.override(atraxo.fmbas.i18n.ds.TimeSerieAvgMthdLov_Ds, {
	active__lbl: "Active?",
	active__tlp: "Active?",
	averagingMethodCode__lbl: "Averaging method",
	averagingMethodCode__tlp: "Code",
	averagingMethodId__lbl: "Averaging Method (ID)",
	averagingMethodName__lbl: "Averaging method",
	averagingMethodName__tlp: "Name",
	tserieId__lbl: "Tserie (ID)",
	tserieName__lbl: "Series name",
	tserieName__tlp: "Series name"
});
