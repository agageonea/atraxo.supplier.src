Ext.override(atraxo.fmbas.i18n.ds.FisLov_Ds, {
	active__lbl: "Active?",
	attribut1__lbl: "Attribute1",
	attribut2__lbl: "Attribute2",
	code1__lbl: "Code1",
	code2__lbl: "Code2",
	pos__lbl: "Pos",
	system__lbl: "Lov System",
	table__lbl: "Lov Table"
});
