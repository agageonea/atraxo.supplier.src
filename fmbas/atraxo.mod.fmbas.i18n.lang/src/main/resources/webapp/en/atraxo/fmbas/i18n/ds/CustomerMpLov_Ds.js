Ext.override(atraxo.fmbas.i18n.ds.CustomerMpLov_Ds, {
	business__lbl: "Business type",
	buyerCode__lbl: "Responsible buyer",
	buyerId__lbl: "Responsible Buyer (ID)",
	buyerName__lbl: "Responsible buyer",
	code__lbl: "Customer code",
	code__tlp: "Customer code",
	isCustomer__lbl: "Is customer?",
	isCustomer__tlp: "Is customer?",
	name__lbl: "Customer name",
	name__tlp: "Customer name",
	primaryContactId__lbl: "",
	primaryContactName__lbl: "",
	refid__lbl: "RefID",
	status__lbl: "Customer status",
	status__tlp: "Customer status",
	type__lbl: "Company type",
	type__tlp: "Company type"
});
