Ext.override(atraxo.fmbas.i18n.ds.ContactsLov_Ds, {
	businessPhone__lbl: "Business",
	businessPhone__tlp: "Business",
	email__lbl: "Email",
	email__tlp: "Email",
	firstName__lbl: "First name",
	firstName__tlp: "First name",
	fullNameField__lbl: "Full name",
	fullNameField__tlp: "Full name",
	lastName__lbl: "Last name",
	lastName__tlp: "Last name",
	objectId__lbl: "Object ID",
	objectId__tlp: "Object ID",
	objectType__lbl: "Object type",
	objectType__tlp: "Object type"
});
