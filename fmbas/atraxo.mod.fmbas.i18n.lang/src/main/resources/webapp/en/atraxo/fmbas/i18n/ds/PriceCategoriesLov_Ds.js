Ext.override(atraxo.fmbas.i18n.ds.PriceCategoriesLov_Ds, {
	active__lbl: "Active?",
	active__tlp: "Active?",
	name__lbl: "Price category",
	name__tlp: "Price category",
	pricePer__lbl: "Price per",
	pricePer__tlp: "Price per",
	type__lbl: "Type",
	type__tlp: "Type"
});
