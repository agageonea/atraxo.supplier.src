Ext.override(atraxo.fmbas.i18n.frame.Units_Ui, {
	/* view */
	Lenght__ttl: "Length",
	Mass__ttl: "Mass",
	Time__ttl: "Time",
	Volume__ttl: "Volume",
	/* menu */
	/* button */
	btnCancelAll1__lbl: "Cancel all",
	btnCancelAll1__tlp: "Cancel all",
	btnCancelAll2__lbl: "Cancel all",
	btnCancelAll2__tlp: "Cancel all",
	btnCancelAll3__lbl: "Cancel all",
	btnCancelAll3__tlp: "Cancel all",
	btnCancelAll4__lbl: "Cancel all",
	btnCancelAll4__tlp: "Cancel all",
	btnCancelSelected1__lbl: "Cancel",
	btnCancelSelected1__tlp: "Cancel",
	btnCancelSelected2__lbl: "Cancel",
	btnCancelSelected2__tlp: "Cancel",
	btnCancelSelected3__lbl: "Cancel",
	btnCancelSelected3__tlp: "Cancel",
	btnCancelSelected4__lbl: "Cancel",
	btnCancelSelected4__tlp: "Cancel",
	btnCancelWithMenu1__lbl: "Cancel",
	btnCancelWithMenu1__tlp: "Abort action",
	btnCancelWithMenu2__lbl: "Cancel",
	btnCancelWithMenu2__tlp: "Abort action",
	btnCancelWithMenu3__lbl: "Cancel",
	btnCancelWithMenu3__tlp: "Abort action",
	btnCancelWithMenu4__lbl: "Cancel",
	btnCancelWithMenu4__tlp: "Abort action",
	helpWdw__lbl: "Help",
	helpWdw__tlp: "Help",
	
	title: "Units of Measurement"
});
